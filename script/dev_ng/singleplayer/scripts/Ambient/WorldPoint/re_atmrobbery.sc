//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "lineActivation.sch"
USING "Ambient_Approach.sch"
USING "dialogue_public.sch"
USING "Ambient_Speech.sch"
USING "Ambient_Common.sch"

USING "commands_debug.sch"
USING "cutscene_public.sch"
USING "script_blips.sch"
USING "area_checks.sch"
USING "taxi_functions.sch"
USING "completionpercentage_public.sch"
USING "rc_helper_functions.sch"
USING "rc_threat_public.sch"

USING "chase_hint_cam.sch"
USING "LineActivation.sch"
USING "commands_event.sch"
USING "locates_public.sch"
USING "random_events_public.sch"
USING "load_queue_public.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

VECTOR vInput
FLOAT fInput

PED_INDEX pedRobber, pedVictim
OBJECT_INDEX objHandbag
BLIP_INDEX blipVictim, blipRobber, blipBag
SEQUENCE_INDEX seqVictim, seqRobber
SCENARIO_BLOCKING_INDEX blockedArea

INT iVictimBlipFlash
INT iCurrentVariation = 3, iHintCamTimer, iAimingCounter
INT sceneId, iShockingEvent = 0, iFlashTimestamp
CONST_INT BIKE_VARIATION 1		//1
CONST_INT CAR_VARIATION 2		//2
CONST_INT NORMAL_VARIATION 3 	//4
CONST_INT HILL_VARIATION 4		//3
CONST_INT RANDOM_VARIATION  5
INT chosenRobbery = 0
INT robberyID = 0

VEHICLE_INDEX vehEscape
VECTOR vEscapeVeh
VECTOR vPlayersVeh
//VECTOR vPlayersVeh1
//VECTOR vPlayersVeh2
FLOAT fEscapeVeh, fPlayersVeh
VECTOR scenePosition
VECTOR sceneRotation

BOOL bEndSceneIsOnRoad = TRUE
BOOL bLoadQueuePopulated = FALSE

FLOAT storedDistance, fLocateSize, fStoppingDistance

BOOL bAttDone = FALSE
BOOL bNavFail = FALSE

BOOL bHelpRequested, bSpecificHelpRequested

PICKUP_INDEX pickupPurse
BOOL hasPickupBeenCreated, bPickupIsNearVictim

STRING animDictMugging = "RANDOM@ATMROBBERYGEN"
STRING sPickupDict = "RANDOM@ATMROBBERYGEN"
STRING sClipSetOverride

// Extra vars for Vicki's pickup cut

BOOL bBagRetrieved, bForceCleanup
CAMERA_INDEX CamIDPickingUpBag
INT iHandOver = 0
BOOL bRunHandover, bBagReturned
MODEL_NAMES modelBag = PROP_LD_WALLET_01_S
MODEL_NAMES modelBagAlt = prop_ld_wallet_01_s
BOOL bStopRefreshingBagModel = FALSE

MODEL_NAMES modelRobber
MODEL_NAMES modelVictim
MODEL_NAMES modelVehicle

structPedsForConversation dialogueStruct
STRING playerVoice, sWarn, victimVoice
STRING sAttr, sHelp, sPrompt, sChase, sFlee, sThx, sDamn, sCurse 
STRING sSubGroup, sReturn, sGotcha
BOOL bWarningIssued, bRobberEscaped, bToldToFlee, bRunHint, bPlayerIsChasing
BOOL bReturnSpeechSaid = FALSE
FLOAT fRetreatDist

INT stolenCash = 500
INT iEarlyInterruption = -1
//INT iEventDelay = 20
//INT iExtendedEventDelay = 40

INT iEarlyLaunchTimer = -1

VECTOR vMin, vMax
VECTOR vMoveToPos
FLOAT fPanicHeading = 0
VECTOR vVehiclePanicPos
FLOAT fVehiclePanicHead = 0

STRING sHandoverDict, sHandoverPlayer, sHandoverPed, sHandoverCam
STRING sAmbientVoiceCrim, sAmbientVoiceVic
STRING sAgitatedDict, sAgitated1, sAgitated2, sAgitated3

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

LoadQueueMedium sLoadQueue

#IF IS_DEBUG_BUILD
BOOL bDebugKillRobber
#ENDIF

// Functions ----------------------------------------------//

// Clean Up
PROC missionCleanup()
	IF bRobberEscaped
		IF IS_VEHICLE_DRIVEABLE(vehEscape)
			IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehEscape)
				REMOVE_VEHICLE_STUCK_CHECK(vehEscape)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedRobber)
		SET_PED_AS_NO_LONGER_NEEDED(pedRobber)
	ENDIF
	
	REMOVE_ALL_SHOCKING_EVENTS(FALSE)
	IF iShockingEvent != 0
		REMOVE_SHOCKING_EVENT(iShockingEvent)
	ENDIF
	
	REMOVE_ALL_SHOCKING_EVENTS(FALSE)
	
	IF NOT IS_PED_INJURED(pedVictim)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
		SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
	ENDIF
	
	IF iCurrentVariation = HILL_VARIATION
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<179.5463, 6625.6890, 30.2739>>, <<184.2450, 6635.6606, 31.5188>>, TRUE) // Reset car gen space in front of the ATM
	ENDIF 
	
	// Ensure the chase hint cam is killed
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//			RESET_RANDOM_EVENT_LAUNCH_TIMER()
		SET_WANTED_LEVEL_MULTIPLIER(1)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	CLEANUP_LOAD_QUEUE_MEDIUM(sLoadQueue)

	RANDOM_EVENT_OVER()
	
	SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
	REMOVE_SCENARIO_BLOCKING_AREA(blockedArea)

	TERMINATE_THIS_THREAD()
ENDPROC

PROC missionPassed(BOOL bReturnedBag = FALSE)
	g_vLastCompletedATMRobbery = vInput	
//	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ATM)
	//added in the pass block here which will set the completion percentage
	IF bReturnedBag
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - passed with pass type RE_PASS_RETURN_ITEM")
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
	ENDIF
	RANDOM_EVENT_PASSED(RE_ATMROBBERY, 0)
	IF NOT bForceCleanup
		LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC() 
	ENDIF
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC



PROC RETURN_ROBBERY_INFO()
	
	IF chosenRobbery = 0
	AND iCurrentVariation = RANDOM_VARIATION
		chosenRobbery = GET_RANDOM_INT_IN_RANGE(1, 5)
	ELSE
		IF iCurrentVariation != RANDOM_VARIATION
			chosenRobbery = iCurrentVariation
		ENDIF
	ENDIF
	
	INT iVariation
	
	CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - chosenRobbery = ", chosenRobbery, ", iCurrentVariation = ", iCurrentVariation)
	SWITCH chosenRobbery
		CASE BIKE_VARIATION
			modelRobber = G_M_Y_ArmGoon_02
			sAmbientVoiceCrim = "G_M_Y_ArmGoon_02_White_Armenian_MINI_01"
			modelVictim = A_F_M_Tourist_01
			sAmbientVoiceVic = "A_F_M_BEVHILLS_02_WHITE_FULL_02"
			modelVehicle = SANCHEZ
			sAttr	= "REAR1_ATTR"
			sHelp	= "REAR1_HELP"
			sPrompt = "REAR1_PROMPT"
			sChase 	= "REAR1_CHASE"
			sFlee 	= "REAR1_FLEE"
			sThx 	= "REAR1_THX"
			sDamn 	= "REAR1_DAMN"
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL  //0
					sReturn = "REAR1_REM"
					sCurse	= "REAR1_FKM"
					sWarn 	= "REAR1_WM"
					sGotcha = "REAR1_GYM"
				BREAK
		    	CASE CHAR_FRANKLIN //1
					sReturn = "REAR1_REF"
					sCurse	= "REAR1_FKF"
					sWarn 	= "REAR1_WF"
					sGotcha = "REAR1_GYF"
				BREAK
		    	CASE CHAR_TREVOR   //2
					sReturn = "REAR1_RET"	
					sCurse	= "REAR1_FKT"
					sWarn 	= "REAR1_WT"
					sGotcha = "REAR1_GYT"
				BREAK
			ENDSWITCH
			victimVoice	= "ATMRobVictim1"
			sSubGroup	= "REAR1AU"
			
			IF bStopRefreshingBagModel = FALSE
			
				iVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
				IF iVariation = 0
					modelBag = Prop_LD_Purse_01
				ELIF iVariation = 1
					modelBag = Prop_LD_Wallet_01
				ELIF iVariation = 2
					BOOL bExtraChoice
					bExtraChoice = GET_RANDOM_BOOL()
					IF bExtraChoice
						modelBag = Prop_LD_Wallet_Pickup
					ELSE
						modelBag = Prop_LD_Purse_01
					ENDIF
				ENDIF
				
				bStopRefreshingBagModel = TRUE
			
			ENDIF
			
			sHandoverDict 	= "RANDOM@ATMROBBERY1"
			sHandoverPlayer = "Return_Wallet_Positive_A_Player"
			sHandoverPed 	= "Return_Wallet_Positive_A_Female"
			sHandoverCam 	= "Return_Wallet_Positive_A_Cam"
			
			sAgitatedDict	= "random@car_thief@waiting_ig_4"
			sAgitated1 		= "waiting"
			sAgitated2 		= "waiting"
			sAgitated3		= "waiting"
		BREAK
		CASE CAR_VARIATION
			modelRobber = G_M_Y_FamFor_01
			sAmbientVoiceCrim = "G_M_Y_FamFor_01_BLACK_MINI_01"
			modelVictim = A_M_Y_Hipster_02
			sAmbientVoiceVic = "A_M_Y_Hipster_02_White_Full_01"
			modelVehicle = RUINER
			sAttr	= "REAR2_ATTR"
			sHelp	= "REAR2_HELP"
			sPrompt = "REAR2_PROMPT"
			sChase 	= "REAR2_CHASE"
			sFlee 	= "REAR2_FLEE"
			sThx 	= "REAR2_THX"
			sDamn 	= "REAR2_DAMN"
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL  //0
					sReturn = "REAR2_REM"
					sCurse	= "REAR2_FKM"
					sWarn 	= "REAR2_WM"
					sGotcha = "REAR2_GYM"
				BREAK
		    	CASE CHAR_FRANKLIN //1
					sReturn = "REAR2_REF"
					sCurse	= "REAR2_FKF"
					sWarn 	= "REAR2_WF"
					sGotcha = "REAR2_GYF"
				BREAK
		    	CASE CHAR_TREVOR   //2
					sReturn = "REAR2_RET"
					sCurse	= "REAR2_FKT"
					sWarn 	= "REAR2_WT"
					sGotcha = "REAR2_GYT"
				BREAK
			ENDSWITCH
			victimVoice	= "ATMRobVictim2"
			sSubGroup	= "REAR2AU"
			
			modelBag = PROP_LD_WALLET_PICKUP
			
			sHandoverDict 	= "RANDOM@ATMROBBERY2"
			sHandoverPlayer = "Return_Wallet_Positive_A_Player"
			sHandoverPed 	= "Return_Wallet_Positive_A_Male"
			sHandoverCam 	= "Return_Wallet_Positive_A_Cam"
			
			sAgitatedDict	= "RANDOM@BICYCLE_THIEF@IDLE_A"
			sAgitated1 		= "IDLE_A"
			sAgitated2 		= "IDLE_B"
			sAgitated3		= "IDLE_C"
		BREAK
		CASE NORMAL_VARIATION
			modelVictim = A_F_M_Tourist_01
			sAmbientVoiceVic = "A_F_Y_Tourist_01_White_Mini_01"
			modelRobber = G_M_Y_SalvaGoon_02
			sAmbientVoiceCrim = "G_M_Y_SalvaGoon_02_SALVADORIAN_MINI_03"
			sAttr	= "REAR5_ATTR"
			sHelp	= "REAR5_HELP"
			sPrompt = "REAR5_PROMPT"
			sChase 	= "REAR5_CHASE"
			sFlee 	= "REAR5_FLEE"
			sThx 	= "REAR5_THX"
			sDamn 	= "REAR5_DAMN"
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL  //0
					sReturn = "REAR5_REM"
					sCurse	= "REAR5_FKM"
					sWarn 	= "REAR5_WM"
					sGotcha = "REAR5_GYM"
				BREAK
		    	CASE CHAR_FRANKLIN //1
					sReturn = "REAR5_REF"
					sCurse	= "REAR5_FKF"
					sWarn 	= "REAR5_WF"
					sGotcha = "REAR5_GYF"
				BREAK
		    	CASE CHAR_TREVOR   //2
					sReturn = "REAR5_RET"
					sCurse	= "REAR5_FKT"
					sWarn 	= "REAR5_WT"
					sGotcha = "REAR5_GYT"
				BREAK
			ENDSWITCH
			victimVoice	= "ATMRobVictim5"
			sSubGroup	= "REAR5AU"
			
			IF bStopRefreshingBagModel = FALSE
			
				iVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
				IF iVariation = 0
					modelBag = Prop_LD_Purse_01
				ELIF iVariation = 1
					modelBag = Prop_LD_Wallet_01
				ELIF iVariation = 2
					BOOL bExtraChoice
					bExtraChoice = GET_RANDOM_BOOL()
					IF bExtraChoice
						modelBag = Prop_LD_Wallet_Pickup
					ELSE
						modelBag = Prop_LD_Wallet_01
					ENDIF
				ENDIF
				
				bStopRefreshingBagModel = TRUE
				
			ENDIF
			
			sHandoverDict 	= "RANDOM@ATMROBBERY3"
			sHandoverPlayer = "Return_Wallet_Positive_B_Player"
			sHandoverPed 	= "Return_Wallet_Positive_B_Female"
			sHandoverCam 	= "Return_Wallet_Positive_B_Cam"
			
			sAgitatedDict	= "random@car_thief@waiting_ig_4"
			sAgitated1 		= "waiting"
			sAgitated2 		= "waiting"
			sAgitated3		= "waiting"
		BREAK
		CASE HILL_VARIATION
			modelVictim = A_F_M_TOURIST_01
			sAmbientVoiceVic = "A_F_M_BevHills_02_WHITE_FULL_01"
			modelVehicle = RUINER
			modelRobber = G_M_Y_Azteca_01//_Latino_MINI_01
			sAmbientVoiceCrim = "G_M_Y_Latino01_Latino_MINI_01"
			sAttr	= "REAR3_ATTR"
			sHelp	= "REAR3_HELP"
			sPrompt = "REAR3_PROMPT"
			sChase 	= "REAR3_CHASE"
			sFlee 	= "REAR3_FLEE"
			sThx 	= "REAR3_THX"
			sDamn 	= "REAR3_DAMN"
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL  //0
					sReturn = "REAR3_REM"
					sCurse	= "REAR3_FKM"
					sWarn 	= "REAR3_WM"
					sGotcha = "REAR3_GYM"
				BREAK
		    	CASE CHAR_FRANKLIN //1
					sReturn = "REAR3_REF"
					sCurse	= "REAR3_FKF"
					sWarn 	= "REAR3_WF"
					sGotcha = "REAR3_GYF"
				BREAK
		    	CASE CHAR_TREVOR   //2
					sReturn = "REAR3_RET"
					sCurse	= "REAR3_FKT"
					sWarn 	= "REAR3_WT"
					sGotcha = "REAR3_GYT"
				BREAK
			ENDSWITCH
			victimVoice	= "ATMRobVictim3"
			sSubGroup	= "REAR3AU"
			
			IF bStopRefreshingBagModel = FALSE
			
				iVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
				IF iVariation = 0
					modelBag = Prop_LD_Purse_01
				ELIF iVariation = 1
					modelBag = Prop_LD_Wallet_01
				ELIF iVariation = 2
					BOOL bExtraChoice
					bExtraChoice = GET_RANDOM_BOOL()
					IF bExtraChoice
						modelBag = Prop_LD_Wallet_Pickup
					ELSE
						modelBag = Prop_LD_Purse_01
					ENDIF
				ENDIF
				
				bStopRefreshingBagModel = TRUE
				
			ENDIF
			
			sHandoverDict 	= "RANDOM@ATMROBBERY4"
			sHandoverPlayer = "Return_Wallet_Positive_C_Player"
			sHandoverPed 	= "Return_Wallet_Positive_C_Female"
			sHandoverCam 	= "Return_Wallet_Positive_C_Cam"
			
			sAgitatedDict	= "random@car_thief@waiting_ig_4"
			sAgitated1 		= "waiting"
			sAgitated2 		= "waiting"
			sAgitated3		= "waiting"
		BREAK
	ENDSWITCH
	
	// Work out which robbery we've loaded, and set the vMoveToPos appropriately
	IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-203.72, -861.80, 29.27>>) < 5 // Robbery 1
		vMoveToPos = <<-197.64, -863.25, 28.33>>
		fPanicHeading = 334.5007
		vVehiclePanicPos = <<-187.24, -856.77, 28.97>>
		fVehiclePanicHead = 162.69
		robberyID = 1
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<288.46, -1256.71, 28.44>>) < 5 // Robbery 2
		vMoveToPos = <<286.50, -1256.73, 28.29>>
		fPanicHeading = 167.3216
		vVehiclePanicPos = <<250.55, -1239.27, 28.84>>
		fVehiclePanicHead = 265.51
		robberyID = 2
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-3044.11, 594.34, 6.73>>) < 5 // Robbery 3
		vMoveToPos = <<-3044.66, 595.70, 6.59>>
		fPanicHeading = 296.7338
		vVehiclePanicPos = <<-3037.55, 608.53, 7.34>>
		fVehiclePanicHead = 202.30
		robberyID = 3
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<174.53, 6637.64, 30.57>>) < 5 // Robbery 4
		vMoveToPos = <<175.57, 6636.58, 30.57>>
		fPanicHeading = 260.4868
		vVehiclePanicPos = <<188.97, 6616.20, 31.48>>
		fVehiclePanicHead = 93.97
		robberyID = 4
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<24.13, -946.84, 28.36>>) < 5 // Robbery 6
		vMoveToPos = <<23.65, -948.26, 28.36>>
		fPanicHeading = 258.8732
		vVehiclePanicPos = <<22.19, -955.82, 28.94>>
		fVehiclePanicHead = 70.66
		robberyID = 6
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<296.13, -894.01, 28.23>>) < 5 // Robbery 8
		vMoveToPos = <<294.64, -893.60, 28.18>>
		fPanicHeading = 156.7222
		vVehiclePanicPos = <<286.40, -897.28, 28.57>>
		fVehiclePanicHead = 341.97
		robberyID = 8
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<1077.69, -775.73, 57.21>>) < 5 // Robbery 11
		vMoveToPos = <<1077.64, -773.86, 57.09>>
		fPanicHeading = 275.6593
		vVehiclePanicPos = <<1081.93, -764.08, 57.35>>
		fVehiclePanicHead = 268.57
		robberyID = 11
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-2072.88, -317.19, 12.32>>) < 5 // Robbery 16
		vMoveToPos = <<-2074.93, -316.75, 12.16>>
		fPanicHeading = 157.3363
		vVehiclePanicPos = <<-2101.20, -293.74, 12.74>>
		fVehiclePanicHead = 263.12
		robberyID = 16
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-821.33, -1082.43, 10.14>>) < 5 // Robbery 17
		vMoveToPos = <<-819.39, -1084.12, 10.03>>
		fPanicHeading = 236.6294
		vVehiclePanicPos = <<-803.18, -1075.91, 11.13>>
		fVehiclePanicHead = 209.53
		robberyID = 17
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-712.90, -819.32, 22.73>>) < 5 // Robbery 18
		vMoveToPos = <<-712.88, -820.47, 22.61>>
		fPanicHeading = 268.2568
		vVehiclePanicPos = <<-721.63, -826.84, 22.82>>
		fVehiclePanicHead = 93.14
		robberyID = 18
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-387.12, 6045.79, 30.50>>) < 5 // Robbery 19
		vMoveToPos = <<-389.5633, 6042.1528, 30.4989>>
		fPanicHeading = 223.2838
		vVehiclePanicPos = <<-406.23, 6045.69, 31.06>>
		fVehiclePanicHead = 229.52
		robberyID = 19
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-301.84, -830.39, 31.38>>) < 5 // Robbery 22
		vMoveToPos = <<-304.1459, -837.4778, 30.6799>>
		fPanicHeading = 286.1390
		vVehiclePanicPos = <<-304.56, -840.76, 31.26>>
		fVehiclePanicHead = 77.59
		robberyID = 22
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-254.30, -691.96, 32.61>>) < 5 // Robbery 23
		vMoveToPos = <<-253.51, -689.26, 32.57>>
		fPanicHeading = 209.9227
		vVehiclePanicPos = <<-250.91, -684.01, 33.00>>
		fVehiclePanicHead = 230.65
		robberyID = 23
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<89.39, 2.00, 67.34>>) < 5 // Robbery 25
		vMoveToPos = <<88.70, 0.01, 67.38>>
		fPanicHeading = 71.0114
		vVehiclePanicPos = <<91.35, -7.07, 67.82>>
		fVehiclePanicHead = 70.28
		robberyID = 25
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-526.42, -1222.51, 17.46>>) < 5 // Robbery 30
		vMoveToPos = <<-530.11, -1217.33, 17.26>>
		fPanicHeading = 53.7680
		vVehiclePanicPos = <<-522.45, -1196.35, 18.45>>
		fVehiclePanicHead = 297.22
		robberyID = 30
	ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-2956.78, 488.19, 14.47>>) < 5 // Robbery 31
		vMoveToPos = <<-2956.78, 488.19, 14.47>>
		fPanicHeading = 80.0912
		vVehiclePanicPos = <<-2974.34, 491.96, 15.00>>
		fVehiclePanicHead = 1.95
		robberyID = 31
	ELSE
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - RETURN_ROBBERY_INFO() - Can't work out which robbery this is, setting vMoveToPos to vInput")
		SCRIPT_ASSERT("RE ATM ROBBERY: An invalid robbery has spawned. Bug Ian Gander with details about where this occurred (check the blip) so this instance can be removed.")
		vMoveToPos = vInput
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads models
/// RETURNS:
///    
FUNC BOOL LOADED_STAGE()

	REQUEST_ANIM_DICT("random@atmrobberygen@male")
	REQUEST_ANIM_DICT("random@atmrobberygen@female")

	IF NOT bLoadQueuePopulated
		SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(sLoadQueue, 3)	//Wait 3 frames in between making each of these load requests.
		sLoadQueue.iLastFrame = GET_FRAME_COUNT()			//Trick the load queue in to thinking it loaded this frame.
		
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelRobber)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelVictim)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelBag)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelBagAlt)
		LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, animDictMugging)
		LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, sAgitatedDict)
		LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, sHandoverDict)
		LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, sPickupDict)
		
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> Filled load queue.")
		bLoadQueuePopulated = TRUE
	ENDIF
	
	IF HAS_ANIM_DICT_LOADED("random@atmrobberygen@male")
	AND HAS_ANIM_DICT_LOADED("random@atmrobberygen@female")
	AND HAS_LOAD_QUEUE_MEDIUM_LOADED(sLoadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Loads anim dicts
/// RETURNS:
///    
//FUNC BOOL LOADED_STAGE_2()
//
//	REQUEST_ANIM_DICT(animDictMugging)
//	REQUEST_ANIM_DICT(sAgitatedDict)
//	REQUEST_ANIM_DICT(sHandoverDict)
//	REQUEST_ANIM_DICT(sPickupDict)
//	REQUEST_ANIM_DICT("random@atmrobberygen@male")
//	REQUEST_ANIM_DICT("random@atmrobberygen@female")
//	
//	IF HAS_ANIM_DICT_LOADED(animDictMugging)
//	AND HAS_ANIM_DICT_LOADED(sAgitatedDict)
//	AND HAS_ANIM_DICT_LOADED(sHandoverDict)
//	AND HAS_ANIM_DICT_LOADED(sPickupDict)
//	AND HAS_ANIM_DICT_LOADED("random@atmrobberygen@male")
//	AND HAS_ANIM_DICT_LOADED("random@atmrobberygen@female")
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//
//ENDFUNC

FUNC BOOL LOADED_AND_CREATED_Robbery()

	VECTOR scenePositionStart = vInput
	VECTOR sceneRotationStart = << 0.000, 0.000, fInput >>
	VECTOR vBlockRange = <<10,10,5>>
	VECTOR vAnimOffsetRot
	
	IF iCurrentVariation = BIKE_VARIATION
		
		RETURN_ROBBERY_INFO()
		
		// Robbery 1
		IF VDIST(vInput, << -203.758, -861.738, 29.2684 >>) < 5
			vEscapeVeh = <<-197.9643, -868.5554, 28.2738>>
			fEscapeVeh = 196 
			
			vPlayersVeh = <<-187.5415, -858.1728, 28.1863>>
			fPlayersVeh = 160.3910
			vMin = <<-259.035095,-865.711853,28>> 
			vMax = <<-130.398117,-908.809631,40>>
	//		fWidth = 132.500000
			ASSISTED_MOVEMENT_REQUEST_ROUTE("ATM_1")
			REQUEST_MODEL(modelVehicle)
			WHILE NOT HAS_MODEL_LOADED(modelVehicle)
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					MissionCleanup()
				ENDIF
				WAIT(0)
			ENDWHILE
			SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
		ELSE // Robbery 31
			vEscapeVeh = <<-2968.50, 494.48, 14.82>>
			fEscapeVeh = 55.24 
			
			vPlayersVeh = <<-2973.03, 496.38, 14.96>>
			fPlayersVeh = 3.18
			vMin = <<-259.035095,-865.711853,28>> 
			vMax = <<-130.398117,-908.809631,40>>
	//		fWidth = 132.500000
			REQUEST_MODEL(modelVehicle)
			WHILE NOT HAS_MODEL_LOADED(modelVehicle)
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					MissionCleanup()
				ENDIF
				WAIT(0)
			ENDWHILE
			SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
		ENDIF
		
	ELIF iCurrentVariation = CAR_VARIATION
		RETURN_ROBBERY_INFO()
		
		vEscapeVeh = << 283.31, -1249.51, 28.95 >>  
		fEscapeVeh = 78.25
		vPlayersVeh = <<288.9906, -1248.4432, 28.2847>>
		fPlayersVeh = 75.9756
		vMin = <<266.364258,-1313.829346,28.591026>>
		vMax = <<265.148926,-1215.931763,36.201920>> 
//		fWidth = 104.750000
		REQUEST_MODEL(modelVehicle)
		WHILE NOT HAS_MODEL_LOADED(modelVehicle)
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
				MissionCleanup()
			ENDIF
			WAIT(0)
		ENDWHILE
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
	ELIF iCurrentVariation = NORMAL_VARIATION
	
		RETURN_ROBBERY_INFO()		
		
		vPlayersVeh = << -1587.1504, -539.9152, 34.3644 >>
		fPlayersVeh = 327
		vMin = <<-1596.202515,-474.884430,32>>
		vMax = <<-1595.572144,-565.836792,48>>
//		fWidth = 93.250000
		
	ELIF iCurrentVariation = HILL_VARIATION
		//bEndSceneIsOnRoad = FALSE
		RETURN_ROBBERY_INFO()
		
		vEscapeVeh = << 181.2355, 6632.6274, 30.5732 >>
		fEscapeVeh = 182
		vPlayersVeh = << 200.0727, 6630.5874, 30.5170 >>
		fPlayersVeh = 165
		vMin = <<178.181717,6660.375000,31>>
		vMax = <<179.124939,6530.042969,38>> 
//		fWidth = 135.500000
		SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<179.5463, 6625.6890, 30.2739>>, <<184.2450, 6635.6606, 31.5188>>, FALSE) // Block car gen space in front of the ATM
		REQUEST_MODEL(modelVehicle)
		WHILE NOT HAS_MODEL_LOADED(modelVehicle)
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
				MissionCleanup()
			ENDIF
			WAIT(0)
		ENDWHILE
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
	
	ELIF iCurrentVariation = RANDOM_VARIATION
	
		RETURN_ROBBERY_INFO()
		
	ENDIF
	
	IF LOADED_STAGE()
//		IF LOADED_STAGE_2()
			CLEAR_AREA(vInput, vBlockRange.x, FALSE)
			blockedArea = ADD_SCENARIO_BLOCKING_AREA((vInput-vBlockRange), (vInput+vBlockRange))
			pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, vInput, fInput)
			SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_JUST_SEEK_COVER, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, FALSE)
			SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_FEET, 1, 0) // Remove camera from ped.
			SET_ENTITY_LOAD_COLLISION_FLAG(pedVictim, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedVictim, TRUE)
			SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
			SET_AMBIENT_VOICE_NAME(pedVictim, sAmbientVoiceVic)
			SET_PED_MONEY(pedVictim, 0)
			IF chosenRobbery = BIKE_VARIATION
				SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			ENDIF

			
			pedRobber = CREATE_PED(PEDTYPE_CRIMINAL, modelRobber, vInput)
			SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_ALWAYS_FLEE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(pedRobber, FA_DISABLE_HANDS_UP, TRUE)
			SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRobber, TRUE)
			SET_PED_CONFIG_FLAG(pedRobber, PCF_DontInfluenceWantedLevel, TRUE)
			SET_PED_CONFIG_FLAG(pedRobber, PCF_DisableGoToWritheWhenInjured, TRUE)
			SET_PED_CONFIG_FLAG(pedRobber, PCF_CanDiveAwayFromApproachingVehicles, FALSE)
			SET_PED_CONFIG_FLAG(pedRobber, PCF_RemoveDeadExtraFarAway, TRUE)
			DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(pedRobber)
			SET_AMBIENT_VOICE_NAME(pedRobber, sAmbientVoiceCrim)
			SET_PED_TARGET_LOSS_RESPONSE(pedRobber, TLR_NEVER_LOSE_TARGET)
			
			
			/* START SYNCHRONIZED SCENE */
	//		sceneId = CREATE_SYNCHRONIZED_SCENE(scenePositionStart, sceneRotationStart)
	//		
	//		TASK_SYNCHRONIZED_SCENE (pedVictim, sceneId, animDictMugging, "a_atm_mugging", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
	//		TASK_SYNCHRONIZED_SCENE (pedRobber, sceneId, animDictMugging, "b_atm_mugging", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
	//		
	//		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)

			//set ped's position for anim
			SET_ENTITY_COORDS_NO_OFFSET(pedRobber,  GET_ANIM_INITIAL_OFFSET_POSITION(animDictMugging, "b_atm_mugging",  scenePositionStart, sceneRotationStart))
			//set ped's heading for anim
			vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(animDictMugging, "b_atm_mugging",  scenePositionStart, sceneRotationStart)
			SET_ENTITY_HEADING(pedRobber, vAnimOffsetRot.z)
			
			//set ped's position for anim
			IF IS_ENTITY_ALIVE(pedVictim)
				IF IS_PED_MALE(pedVictim)
					SET_ENTITY_COORDS_NO_OFFSET(pedVictim,  GET_ANIM_INITIAL_OFFSET_POSITION("random@atmrobberygen@male", "idle_a",  scenePositionStart, sceneRotationStart))
					vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION("random@atmrobberygen@male", "idle_a",  scenePositionStart, sceneRotationStart)
				ELSE
					SET_ENTITY_COORDS_NO_OFFSET(pedVictim,  GET_ANIM_INITIAL_OFFSET_POSITION("random@atmrobberygen@female", "idle_a",  scenePositionStart, sceneRotationStart))
					vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION("random@atmrobberygen@female", "idle_a",  scenePositionStart, sceneRotationStart)
				ENDIF
				//set ped's heading for anim
				SET_ENTITY_HEADING(pedVictim, vAnimOffsetRot.z)
			ENDIF
			
			TASK_PLAY_ANIM(pedRobber, animDictMugging, "b_atm_mugging", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING )
			IF IS_PED_MALE(pedVictim)
				TASK_PLAY_ANIM(pedVictim, "random@atmrobberygen@male", "idle_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING )
				REMOVE_ANIM_DICT("random@atmrobberygen@female") // Don't need the female one anymore...
			ELSE
				TASK_PLAY_ANIM(pedVictim, "random@atmrobberygen@female", "idle_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING  )
				REMOVE_ANIM_DICT("random@atmrobberygen@male") // Don't need the male one anymore...
			ENDIF
			
			//blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim, FALSE, TRUE)
			
			//blipRobber = CREATE_AMBIENT_BLIP_FOR_PED(pedRobber, TRUE)
			
			SET_PED_AS_ENEMY(pedRobber, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
			
			GIVE_WEAPON_TO_PED(pedRobber, WEAPONTYPE_PISTOL, -1, TRUE)
			
			SET_PED_AMMO(pedRobber, WEAPONTYPE_PISTOL, 34)
			SET_CURRENT_PED_WEAPON(pedRobber, WEAPONTYPE_PISTOL)
			
			SET_ENTITY_IS_TARGET_PRIORITY(pedRobber, TRUE)
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL  //0
					playerVoice = "MICHAEL"
				BREAK
		    	CASE CHAR_FRANKLIN //1
					playerVoice = "FRANKLIN"
				BREAK
		    	CASE CHAR_TREVOR   //2
					playerVoice = "TREVOR"
				BREAK
			ENDSWITCH
			ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), playerVoice)
			ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedVictim, victimVoice)
			ADD_PED_FOR_DIALOGUE(dialogueStruct, 2, pedRobber, "ATMRobber")

			/* CLEAN UP SYNCHRONIZED SCENE */
			
			IF iCurrentVariation = BIKE_VARIATION
			OR iCurrentVariation = CAR_VARIATION
				vehEscape = CREATE_VEHICLE(modelVehicle, vEscapeVeh, fEscapeVeh)
				SET_VEHICLE_ENGINE_ON(vehEscape, TRUE, TRUE)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehEscape)
				SET_DISABLE_PRETEND_OCCUPANTS(vehEscape, TRUE)
				IF iCurrentVariation = CAR_VARIATION
					SET_VEHICLE_DOOR_OPEN(vehEscape, SC_DOOR_FRONT_LEFT)
				ELIF iCurrentVariation = BIKE_VARIATION
					SET_PED_HELMET(pedRobber, FALSE)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedRobber, KNOCKOFFVEHICLE_HARD)
				ELIF iCurrentVariation = HILL_VARIATION
					SET_VEHICLE_DOORS_LOCKED(vehEscape, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				ENDIF
			ENDIF
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		
			RETURN TRUE
//		ELSE
//			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
//				MissionCleanup()
//			ENDIF
//		ENDIF
	ELSE
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
			MissionCleanup()
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC

PROC DETACH_AND_BLIP_PURSE()
	PICKUP_TYPE pickupType = PICKUP_MONEY_PURSE
	IF NOT hasPickupBeenCreated
		INT iPlacementFlags
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		pickupPurse = CREATE_PICKUP(pickupType, GET_DEAD_PED_PICKUP_COORDS(pedRobber), iPlacementFlags, stolenCash, TRUE, modelBag)
		hasPickupBeenCreated = TRUE
		blipBag = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupPurse)
		CREATE_CONVERSATION(dialogueStruct, sSubGroup, sFlee, CONV_PRIORITY_AMBIENT_HIGH)
		IF DOES_BLIP_EXIST(blipRobber)
			REMOVE_BLIP(blipRobber)
		ENDIF
		IF IS_ENTITY_ALIVE(pedVictim)
			CLEAR_PED_TASKS(pedVictim)
		ENDIF
	ELSE
		
		IF NOT bPickupIsNearVictim
			IF NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE)
			AND NOT IS_PED_INJURED(pedVictim)
				OPEN_SEQUENCE_TASK(seqVictim)
					IF NOT IS_ENTITY_AT_COORD(pedVictim, vMoveToPos, <<0.5,0.5,0.5>>)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vMoveToPos, PEDMOVEBLENDRATIO_WALK) // Make the ped walk to the MoveToPos to prepare for the handover cutscene of doom
					ENDIF
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1, DEFAULT, REALLY_SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3, DEFAULT, REALLY_SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, DEFAULT, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqVictim)
				TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
				CLEAR_SEQUENCE_TASK(seqVictim)
			ENDIF
		ENDIF
		
		IF bPickupIsNearVictim
			IF NOT bBagRetrieved
				IF NOT IS_PED_INJURED(pedVictim)
					IF IS_ENTITY_PLAYING_ANIM(pedVictim, sPickupDict, "pickup_low")
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), IS_ENTITY_PLAYING_ANIM")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedVictim, sPickupDict, "pickup_low") > 0.35
							IF DOES_PICKUP_EXIST(pickupPurse)
								CREATE_CONVERSATION(dialogueStruct, sSubGroup, sThx, CONV_PRIORITY_AMBIENT_HIGH)
							
								REMOVE_PICKUP(pickupPurse)
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - DETACH_AND_BLIP_PURSE(), REMOVE_PICKUP(pickupStolenItem)")
								SET_PED_MONEY(pedVictim, stolenCash)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
								SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
								missionPassed(TRUE)
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							CLEAR_PED_TASKS(pedVictim)
							OPEN_SEQUENCE_TASK(seqVictim)
								TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low")		
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
								TASK_WANDER_STANDARD(NULL)
							CLOSE_SEQUENCE_TASK(seqVictim)
							TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
							CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - DETACH_AND_BLIP_PURSE(), REMOVE_PICKUP(pickupStolenItem)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedVictim)
				IF DOES_PICKUP_EXIST(pickupPurse)
					IF IS_ENTITY_AT_COORD(pedVictim, GET_PICKUP_COORDS(pickupPurse), <<10,10,7>>)
						CLEAR_PED_TASKS(pedVictim)
						OPEN_SEQUENCE_TASK(seqVictim)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PICKUP_COORDS(pickupPurse), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 1.0, ENAV_STOP_EXACTLY)
							TASK_TURN_PED_TO_FACE_COORD(NULL, GET_PICKUP_COORDS(pickupPurse))
							TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low")	
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
							TASK_WANDER_STANDARD(NULL)
						CLOSE_SEQUENCE_TASK(seqVictim)
						TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
						CLEAR_SEQUENCE_TASK(seqVictim)
						SET_PED_KEEP_TASK(pedVictim, TRUE)
						bPickupIsNearVictim = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PlayerRetrievingBag()
	IF NOT bBagRetrieved
		IF bWarningIssued
			DETACH_AND_BLIP_PURSE()
		ELSE
			IF NOT IS_PED_INJURED(pedRobber)
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRobber, <<25,25,25>>)
						bWarningIssued = TRUE		
					ENDIF
				ENDIF
			ELSE
				bWarningIssued = TRUE	
			ENDIF
			FLASH_RANDOM_EVENT_DECISION_BLIP(blipRobber, iFlashTimestamp)
		ENDIF
		
		
		IF NOT bRobberEscaped
			IF hasPickupBeenCreated
				IF HAS_PICKUP_BEEN_COLLECTED(pickupPurse)
					INT iDebug
					QUICK_INCREMENT_INT_STAT(RC_WALLETS_RECOVERED, 1)
					STAT_GET_INT(RC_WALLETS_RECOVERED, iDebug)
					CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - RC_WALLETS_RECOVERED increased, is now: ", iDebug)
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 250)
					PRINT_LIMITED_RANDOM_EVENT_HELP(REHLP_RETURN_ITEM)
					bBagRetrieved = TRUE
				ELIF DOES_BLIP_EXIST(blipBag)
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupPurse)) > 150
						IF IS_PED_INJURED(pedVictim)
							missionCleanup()
						ELSE
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim)) > 100
								missionCleanup()
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_TARGET_IN_RANGE()
	IF NOT IS_PED_INJURED(pedRobber)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRobber, <<180,180,50>>)
		OR NOT IS_ENTITY_OCCLUDED(pedRobber)
			UPDATE_CHASE_BLIP(blipRobber, pedRobber, 180)
			RETURN TRUE
		ELSE
			IF dialogueStruct.PedInfo[2].ActiveInConversation
				REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
			ENDIF
			DELETE_PED(pedRobber)
			IF IS_VEHICLE_DRIVEABLE(vehEscape)
				IF IS_ENTITY_OCCLUDED(vehEscape)
					DELETE_VEHICLE(vehEscape)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedRobber, FALSE)) < 300
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_FADES_IF_CUT_IS_SKIPPED()
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		DO_SCREEN_FADE_OUT(500)
		WHILE IS_SCREEN_FADING_OUT()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		WAIT(0)
		ENDWHILE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If the robber is <8m away from the victim, we want a firing amnesty on the victim
/// RETURNS:
///    True if amnesty is valid
FUNC BOOL CHECK_FIRING_AMNESTY_ON_VICTIM()
	
	IF IS_ENTITY_ALIVE(pedRobber)
		IF GET_DISTANCE_BETWEEN_ENTITIES(pedVictim, pedRobber) < 22.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_SCENE()
	BOOL bReturn
	VECTOR vTempMax = <<6,6,6>>, vTempMin = <<-6,-6,-6>>
	IF NOT IS_PED_INJURED(pedVictim)
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
				iAimingCounter++
				IF iAimingCounter > 60
					CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE() - Aim counter")
					bReturn = TRUE	
				ENDIF
			ELSE
				iAimingCounter = 0
			ENDIF
		ENDIF
		
		IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 2)
			CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE() - Bullet in area")
			bReturn = TRUE
		ENDIF

		vTempMax = vTempMax+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
		vTempMin = vTempMin+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
		IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
			CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE() - Projectile in area")
			bReturn = TRUE
		ENDIF

		IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 12)
			CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE() - Petrol decal in area")
			bReturn = TRUE
		ENDIF
		
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 50)
			CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE() - Explosion in area")
			bReturn = TRUE
		ENDIF
	ENDIF
	IF bReturn
		IF NOT CHECK_FIRING_AMNESTY_ON_VICTIM()
			CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE()")
		ELSE
			bReturn = FALSE
			IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) != PERFORMING_TASK
				IF IS_PED_UNINJURED(pedVictim)
					CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> Player interfering with scene but amnesty in progress, make victim cower")
					TASK_COWER(pedVictim, 2000)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PLAYER_TALKING_ON_FOOT()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks if the player is in a 'restricted' area (i.e in a road I guess)
/// RETURNS:
///    TRUE if the player is somewhere we don't want the scene to play out, FALSE otherwise
FUNC BOOL IS_PLAYER_IN_RESTRICTED_AREA()
	
	// This kind of works backwards - if the player isn't in a pre-defined safe area for the robbery, then we say they're in a restricted zone
	// This seems safer than defining several larger areas where the player shouldn't go and dealing with potential issues beyond those locates
	SWITCH robberyID
		CASE 1
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-190.815247,-801.322388,25.454014>>, <<-214.821198,-866.647156,35.031799>>, 40.0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<270.580139,-1290.954346,24.165716>>, <<270.498993,-1238.311279,34.527382>>, 58.25)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-3038.986816,606.477417,2.589385>>, <<-3057.038574,599.710327,12.709482>>, 19.25)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<170.858109,6619.894043,30.319410>>, <<183.952164,6632.881836,34.060883>>, 22.25)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-9.663248,-938.216736,24.290241>>, <<48.125340,-959.442810,34.609818>>, 7.5)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<283.048706,-923.309082,24.139559>>, <<301.367004,-871.692383,34.470699>>, 6.75)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 11
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1053.799072,-772.760132,53.312317>>, <<1112.017944,-772.286377,60.161556>>, 14.750)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 16
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2097.209717,-292.776611,8.141721>>, <<-2101.908203,-345.409973,18.232346>>, 54.25)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 17
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.684143,-1117.781250,5.659806>>, <<-820.555481,-1080.584839,16.387640>>, 29.5)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 18
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-731.174561,-820.052979,18.067131>>, <<-670.658325,-821.272400,29.661249>>, 8.0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 19
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-379.955536,6033.261230,29.748749>>, <<-394.291382,6047.678223,33.998596>>, 7.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-393.642151,6056.546875,29.750107>>, <<-402.483368,6047.817383,33.993122>>, 17.50)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 22
			// Complex area, needs a two locates
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-294.337982,-833.590515,26.651785>>, <<-330.661011,-827.250793,38.649826>>, 16.5)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-294.130890,-836.860229,26.720949>>, <<-284.693787,-814.233032,37.386120>>, 11.0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 23
			// Also needs more than one locate...
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-253.936188,-689.096008,28.569336>>, <<-278.469788,-682.077271,38.525021>>, 9.5)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-253.313797,-687.662842,28.561104>>, <<-247.236679,-703.118408,38.814396>>, 9.5)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-255.670868,-726.274231,28.518696>>, <<-247.236679,-703.118408,38.814396>>, 9.5)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 25
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<114.264069,-9.324284,62.788185>>, <<75.478912,4.897117,73.960037>>, 7.0)
				RETURN TRUE
			ENDIF
		BREAK 
		CASE 30
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-544.521057,-1200.006348,12.879658>>, <<-509.391052,-1216.772339,23.725426>>, 41.0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 31
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2945.132324,491.479187,13.289896>>, <<-2971.824951,492.910126,18.067097>>, 10.0)
				RETURN TRUE
			ENDIF
		BREAK
		DEFAULT
			SCRIPT_ASSERT("ATM Robberies -> IS_PLAYER_IN_RESTRICTED_AREA() -> This is not a valid ATM Robbery!")
		BREAK
	ENDSWITCH
	
	// Stop this from playing if the player is in an interior
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC RETURN_BAG_SYNCED_SCENE()
	VEHICLE_INDEX tempCar
	VECTOR vTempPlayer, vTempPed, vTempHeading, vTemp, vDistance, vMaxVehSize, vBackup
	BOOL bAllowPlayerLastVehicle
	FLOAT fOpposite, fAdjacent
	TEXT_LABEL sReturnTemp, sThxTemp
	IF NOT IS_PED_INJURED(pedVictim)
		IF NOT bRunHandover
			IF bRunHint
				IF NOT bReturnSpeechSaid
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<15,15,20>>, FALSE, TRUE, TM_ANY)
						PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_HI", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
						bReturnSpeechSaid = TRUE
					ENDIF
				ENDIF
				// For awkward ATM Robbery locations, reduce the locate trigger size to make these less likely to screw up
				IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<296.13, -894.01, 28.23>>) < 5 // ATM robbery 8
					IF fLocateSize = 6
						fLocateSize = 5 // Scale these down even lower to help avoid bad edge cases
					ENDIF
				ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-2072.88, -317.19, 12.32>>) < 5 // Robbery 16
					IF fLocateSize = 6
						fLocateSize = 4 // Scale these down even lower to help avoid bad edge cases
					ENDIF
				ENDIF
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<fLocateSize,fLocateSize,fLocateSize>>)
				AND NOT IS_PLAYER_IN_RESTRICTED_AREA()
					CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RETURN_BAG_SYNCED_SCENE(), locate distance was ", fLocateSize)
					IF IS_THIS_RANDOM_EVENT_HELP_BEING_DISPLAYED(REHLP_RETURN_ITEM)
						CLEAR_HELP(TRUE)
					ENDIF
					IF NOT IS_PLAYER_INTERFERING_WITH_SCENE()
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())	
							IF CAN_PLAYER_START_CUTSCENE()
								IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR))
								OR IS_PLAYER_TALKING_ON_FOOT()
									IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
										CLEAR_HELP()
										IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											KILL_ANY_CONVERSATION()
										ENDIF
										
										// If the pickup model is prop_ld_wallet_pickup (the open wallet), use the alternate
										// closed wallet during the handover synced scene instead
										IF modelBag = prop_ld_wallet_pickup
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RETURN_BAG_SYNCED_SCENE(), using alternate wallet model!!")
											objHandbag = CREATE_OBJECT(modelBagAlt, GET_ENTITY_COORDS(PLAYER_PED_ID()))
										ELSE
											objHandbag = CREATE_OBJECT(modelBag, GET_ENTITY_COORDS(PLAYER_PED_ID()))
										ENDIF
										
										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
											ATTACH_ENTITY_TO_ENTITY(objHandbag, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
										ENDIF
										SET_ENTITY_VISIBLE(objHandbag, FALSE)
										SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF DOES_ENTITY_EXIST(objHandbag)
												WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fStoppingDistance)
												OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												OR NOT DOES_ENTITY_HAVE_DRAWABLE(objHandbag)
													IF NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fStoppingDistance)
														CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RETURN_BAG_SYNCED_SCENE(), still stopping player's car")
													ENDIF
													IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RETURN_BAG_SYNCED_SCENE(), still waiting for convo prior to end")
													ENDIF
													IF NOT DOES_ENTITY_HAVE_DRAWABLE(objHandbag)
														CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RETURN_BAG_SYNCED_SCENE(), objHandbag still doesn't have drawable")
													ENDIF
													WAIT(0)
												ENDWHILE
												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
												ENDIF
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(objHandbag)
												WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												OR NOT DOES_ENTITY_HAVE_DRAWABLE(objHandbag)
												WAIT(0)
												ENDWHILE
											ENDIF
										ENDIF
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										IF NOT IS_PED_INJURED(pedVictim)
											SET_PED_CAN_RAGDOLL(pedVictim, FALSE)
										ENDIF
										REMOVE_ALL_SHOCKING_EVENTS(FALSE)
										REMOVE_SHOCKING_EVENT(iShockingEvent)
										SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										DISABLE_CELLPHONE_THIS_FRAME_ONLY()
										WAIT(0)
										IF IS_ENTITY_ALIVE(pedVictim)
											IF IS_PED_MALE(pedVictim)
												sClipSetOverride = "move_m@hurry@b"
											ELSE
												sClipSetOverride = "move_f@hurry@a"
											ENDIF
										ENDIF
										REQUEST_CLIP_SET(sClipSetOverride)
										bRunHandover = TRUE
										SETTIMERA(0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SCALE_CUTSCENE_TRIGGERS_BASED_ON_PLAYER_SPEED(GET_ENTITY_COORDS(pedVictim), fLocateSize, fStoppingDistance)
					IF NOT IS_PLAYER_IN_RESTRICTED_AREA()
						IF NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_GO_TO_ENTITY)
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							CLEAR_PED_TASKS(pedVictim)
							TASK_GO_TO_ENTITY(pedVictim, PLAYER_PED_ID(), DEFAULT, DEFAULT, PEDMOVEBLENDRATIO_WALK)
							CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - tasking victim to walk to player")
						ENDIF
					ELSE
						IF IsPedPerformingTask(pedVictim, SCRIPT_TASK_GO_TO_ENTITY)
							CLEAR_PED_TASKS(pedVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
							OPEN_SEQUENCE_TASK(seqVictim)
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1)
								TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3)
								TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, DEFAULT, DEFAULT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seqVictim)
							TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
							CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - player in restricted area, making ped animate")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<25,25,20>>, FALSE, TRUE, TM_ON_FOOT)
				OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<30,30,20>>, FALSE, TRUE, TM_IN_VEHICLE)
					IF NOT IS_PLAYER_IN_RESTRICTED_AREA()
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							CLEAR_PED_TASKS(pedVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
							OPEN_SEQUENCE_TASK(seqVictim)
		//						IF NOT IS_ENTITY_AT_COORD(pedVictim, vMoveToPos, <<0.5,0.5,0.5>>)
		//							TASK_GO_STRAIGHT_TO_COORD(NULL, vMoveToPos, PEDMOVEBLENDRATIO_WALK) // Make the ped walk to the MoveToPos to prepare for the handover cutscene of doom
		//						ENDIF
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
								//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								//TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1)
								//TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3)
								//TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, DEFAULT, DEFAULT, -1, AF_LOOPING)
								TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT, DEFAULT, PEDMOVEBLENDRATIO_WALK)
							CLOSE_SEQUENCE_TASK(seqVictim)
							TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
						ENDIF
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player in range to trigger return reactions")
						bRunHint = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//HAS_ANIM_EVENT_FIRED()
		
		IF bRunHandover
			SWITCH iHandOver
				CASE 0
					
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
					AND NOT IS_PED_PRONE(PLAYER_PED_ID())
					AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
						
						IF bEndSceneIsOnRoad
							IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-712.90, -819.32, 22.73>>) < 5
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-709.922913,-824.086304,22.075096>>, <<-715.213745,-824.224670,25.012171>>, 3.25)
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Bus stop at #18 going to cause problems, special case handover")
								scenePosition = <<-710.1279, -821.3084, 22.6169>>
								sceneRotation = << 0.000, 0.000, 266.9620>>
								
							ELIF NOT IS_PED_INJURED(pedVictim)
								vBackup = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
								GET_POSITION_FOR_WALLET_SCENE(pedVictim, scenePosition, sceneRotation, sHandoverDict, sHandoverPed)
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - scenePosition: ", scenePosition, " vBackup: ", vBackup)
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - sceneRotation: ", sceneRotation)
								IF scenePosition.z - vBackup.z > 20
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Wallet position Z difference was greater than 30m - reverting to backup coords")
									scenePosition = vBackup
								ENDIF
								scenePosition.z +=2
								GET_GROUND_Z_FOR_3D_COORD(scenePosition, scenePosition.z)
								IF scenePosition.z = 0
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - scenePosition.z after getting ground Z was 0! Using backup... ")
									scenePosition = vMoveToPos
									sceneRotation.z = fPanicHeading
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-203.72, -861.80, 29.27>>) < 5 // If this is Robbery 1
									// If we're in this area (below the Go Postal signage)
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-202.421768,-864.448975,27.880587>>, <<-211.448639,-861.527405,31.754005>>, 9.500000)
										IF sceneRotation.z > 315.0
										AND sceneRotation.z < 360.0
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery #1 special case - rotating scene to prevent victim walking through objects")
											sceneRotation.z = 251.4238 // If we don't do this, the victim may walk through the tree/electricity box
										ELIF sceneRotation.z > 120.0
										AND sceneRotation.z < 212.0
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery #1 special case - rotating scene to prevent victim walking through ATM overhand")
											sceneRotation.z = 67.2304 // If we don't do this, the victim may walk through the building
										ENDIF
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery #1 special case - setting handover position")
										scenePosition = <<-205.81, -865.20, 28.50>>
									ENDIF
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-821.33, -1082.43, 10.14>>) < 5 // Robbery 17
								AND IS_POINT_IN_ANGLED_AREA(scenePosition, <<-826.174438,-1083.942871,9.137519>>, <<-811.655945,-1075.567017,13.887788>>, 6.000000)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery 17 special case - handover location is very close to or inside shop, re-placing")
									scenePosition = <<-814.0593, -1082.4833, 10.0671>>
									sceneRotation.z = 275.5752
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-387.12, 6045.79, 30.50>>) < 5 // Robbery 19
								AND IS_POINT_IN_ANGLED_AREA(scenePosition, <<-377.578827,6033.820801,29.291185>>, <<-373.013794,6029.139648,33.753822>>, 4.75)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery 19 special case - re-placing handover for shitty area")
									scenePosition = <<-375.0627, 6030.5332, 30.5313>>
									sceneRotation.z = 222.9049
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-387.12, 6045.79, 30.50>>) < 5 // Robbery 19
								AND IS_POINT_IN_ANGLED_AREA(scenePosition, <<-381.460724,6031.481445,29.249712>>, <<-376.676575,6026.680176,33.766556>>, 3.0)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery 19 special case - re-placing handover for shitty area")
									scenePosition = <<-379.7835, 6029.4717, 30.5014>>
									sceneRotation.z = 213.2611
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<89.39, 2.00, 67.34>>) < 5 // Robbery 25 - for B*1732868
									// If we're in this area (on the sidewalk by the ATM)
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<73.303474,4.781172,66.794388>>, <<107.657219,-8.330787,69.168327>>, 9.750000)
										IF sceneRotation.z > 128
										AND sceneRotation.z < 195
											IF sceneRotation.z > 162 // about 162 Z is the midpoint, looking straight away from the ATM
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery #25 special case - rotating scene left to prevent victim walking into wall")
												sceneRotation.z = 220 // turn a bit more left (from perspective of victim)
											ELSE // otherwise, we're facing further left
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery #25 special case - rotating scene right to prevent victim walking into wall")
												sceneRotation.z = 107 // turn a bit more right (from perspective of victim)
											ENDIF
											
										ENDIF
									ENDIF
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<174.53, 6637.64, 30.57>>) < 5 // Robbery 4 randomly generated - for B*1869013
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<176.389450,6635.983398,29.877991>>, <<181.758514,6641.301758,33.556004>>, 7.750)
										IF sceneRotation.z > 244.4440
										AND sceneRotation.z < 326.2103
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery 4 special case - rotating handover to stop ped walking into bench")
											sceneRotation.z = 343.1367
										ENDIF
									ENDIF
								ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-3044.11, 594.34, 6.73>>) < 5 // Robbery 3 - for B*1992135
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-3042.548584,597.111755,6.612578>>, <<-3046.964600,595.697266,9.065809>>, 5.00)
										scenePosition = <<-3045.40, 598.65, 6.51>>
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robbery 3 special case - re-placing player to avoid scene taking place in store")
									ENDIF
								ENDIF
								
								vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 0)
								vDistance = vTemp - scenePosition
								vDistance.z = 0
								fAdjacent = VMAG(vDistance)
								GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
								fOpposite = scenePosition.z - vTemp.z
																
								sceneRotation.x = ATAN2(fOpposite, fAdjacent)
								//sceneRotation.x = ATAN(fOpposite/fAdjacent)
								
								IF sceneRotation.x > 20.0
								OR sceneRotation.x < -330.0
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - sceneRotation.x is really extreme (", sceneRotation.x,")! Using backup and recalculating... ")
									scenePosition = vMoveToPos
									sceneRotation.x = 0.0
									sceneRotation.z = fPanicHeading
									
									vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 0)
									vDistance = vTemp - scenePosition
									vDistance.z = 0
									fAdjacent = VMAG(vDistance)
									GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
									fOpposite = scenePosition.z - vTemp.z
								ENDIF
								
								CPRINTLN(DEBUG_AMBIENT,"HANDOVER ROTATION, ATAN2 = ", sceneRotation.x, ", fOpposite = ", fOpposite, ", fAdjacent = ", fAdjacent)
							
								//sceneRotation.x = 10
							ENDIF
							
							IF ARE_VECTORS_ALMOST_EQUAL(vInput, << -526.58, -1222.79, 17.46 >>, 10) // Extra checks for robbery 30 (B*1840774)
								// If the player is in the area by the front of the garage store, and the scene rotation is facing such a way that the victim will
								// walk out of the wall or through the petrol pumps, so rotate the scene a bit more
								IF IS_POINT_IN_ANGLED_AREA(scenePosition, <<-538.441223,-1214.902710,16.797941>>, <<-520.326843,-1223.248657,20.555996>>, 6.500000)
									IF sceneRotation.z < 35.0
									OR sceneRotation.z > 275.0 // If the scene rotation is between these, the victim may walk through the garage store wall (see above bug)
										// If the angle is <335, rotate the victim to their right
										IF sceneRotation.z < 335.0
											sceneRotation.z = 260.0
										ELSE
											sceneRotation.z = 55.0
										ENDIF											
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "Special case robbery #30 rotation - victim may walk through garage store wall, rotating scene (z = ", sceneRotation.z, ")")
									ELIF sceneRotation.z > 107.2072
									AND sceneRotation.z < 210.0 // If the scene rotation is between these, the victim may walk through the garage petrol pumps
										IF sceneRotation.z > 154.6742
											sceneRotation.z = 233.0
										ELSE
											sceneRotation.z = 73.0
										ENDIF
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), "Special case robbery #30 rotation - victim may walk through garage gas pumps, rotating scene(z = ", sceneRotation.z, ")")
									ENDIF
									// This might cause other problems like the victim ped walkthrough a prop trash can or something, but that's not as bad
									// There'll almost certainly be ways to break this with vehicles etc, but there's only so much we can do...
								ENDIF
							ENDIF
						ELSE	
							IF iCurrentVariation = HILL_VARIATION
								
								CPRINTLN(DEBUG_AMBIENT,"ATM Robbery #4 - setting specific handover coords")
								
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),  <<183.6065, 6636.6533, 30.6299>>) < VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),  <<172.2291, 6631.0068, 30.7398>>)
									scenePosition = <<183.6065, 6636.6533, 30.6299>>
									sceneRotation = << 0.000, 0.000, 265>>
								ELSE
									scenePosition = <<172.2291, 6631.0068, 30.7398>>
									sceneRotation = << 0.000, 0.000, 132>>
								ENDIF
								
							ELSE
								vTempPlayer		= GET_ENTITY_COORDS(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(pedVictim)
									vTempPed		= GET_ENTITY_COORDS(pedVictim)	
								ENDIF
								vTempHeading = vTempPlayer - vTempPed
								scenePosition 	= vTempPlayer
								scenePosition 	= <<scenePosition.x, scenePosition.y, scenePosition.z-1>>
								sceneRotation 	= << 0.000, 0.000, GET_HEADING_FROM_VECTOR_2D(vTempHeading.x, vTempHeading.y)>>
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_COORDS(scenePosition, <<-535.795,-1222.23,17.4567>>) < 5 // Robbery #30 - for B*1992135
							CPRINTLN(DEBUG_AMBIENT,"// Robbery #30 - for B*1992135")
							CPRINTLN(DEBUG_AMBIENT,"vInput = ",vInput)
							CPRINTLN(DEBUG_AMBIENT,"scenePosition = ",scenePosition)
							scenePosition = <<-521.475,-1210.47,17.1848>>
							sceneRotation = <<0,0,308.502>>
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehEscape)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEscape)
						ENDIF

						IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
							IF NOT IS_VEHICLE_DRIVEABLE(tempCar)
								tempCar = GET_PLAYERS_LAST_VEHICLE()
								GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempCar), vMin, vMax)
								IF IS_ENTITY_AT_COORD(tempCar, scenePosition, <<vMax.y+1, vMax.y+1, 3>>)
								OR IS_ENTITY_AT_COORD(tempCar, GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation), <<vMax.y+1, vMax.y+1, 3>>)
									IF ARE_VECTORS_ALMOST_EQUAL(vInput, << -526.58, -1222.79, 17.46 >>, 10) // Special case shit for #30 because the forecourt area is tiny
									AND 
									(IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-529.747742,-1218.398682,16.784821>>, <<-538.977844,-1213.756104,20.536852>>, 7.5)
										OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-529.747742,-1218.398682,16.784821>>, <<-520.979797,-1222.305420,20.552042>>, 7.500000))
										
										IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-529.747742,-1218.398682,16.784821>>, <<-538.977844,-1213.756104,20.536852>>, 7.5)
											vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
											vPlayersVeh = <<-543.34, -1216.80, 17.96>>
											fPlayersVeh = 337.50
											bAllowPlayerLastVehicle = TRUE
										ELSE
											vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
											vPlayersVeh = <<-522.4418, -1214.5704, 17.1848>>
											fPlayersVeh = 246.7714
											bAllowPlayerLastVehicle = TRUE
										ENDIF
									
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_DOOR_LATCHED(tempCar, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-203.72, -861.80, 29.27>>) < 5 // Robbery 1 special case shit
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-197.761581,-865.977234,27.311165>>, <<-191.160995,-849.054626,32.892746>>, 10.250)
										vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
										vPlayersVeh = <<-187.00, -855.51, 29.02>>
										fPlayersVeh = 158.28
										bAllowPlayerLastVehicle = TRUE
									
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-387.12, 6045.79, 30.50>>) < 5 // Robbery 19 if the car is in the small ATM area
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-390.453583,6044.050293,29.998678>>, <<-380.477112,6032.984863,33.248787>>, 6.0)
										vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
										vPlayersVeh = <<-395.84, 6051.16, 31.19>>
										fPlayersVeh = 139.92
										bAllowPlayerLastVehicle = TRUE
									
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-2956.78, 488.19, 14.47>>) < 5 // Robbery 31
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2955.233643,492.462616,13.803072>>, <<-2967.116455,493.051147,17.056633>>, 9.75)
										vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
										vPlayersVeh = <<-2972.31, 490.07, 15.03>>
										fPlayersVeh = 357.64
										bAllowPlayerLastVehicle = TRUE
									
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<174.53, 6637.64, 30.57>>) < 5 // Robbery 4 special car stuff
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<178.245972,6638.823730,30.527403>>, <<173.471298,6634.083496,33.682301>>, 6.0)
									AND IS_ENTITY_IN_ANGLED_AREA(tempCar, <<180.122452,6640.329590,30.781290>>, <<171.622986,6631.793945,33.699341>>, 8.0) // If player and car are around the ATM, move the car
										vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
										vPlayersVeh = <<184.05, 6628.75, 31.27>>
										fPlayersVeh = 89.37
										bAllowPlayerLastVehicle = TRUE
									
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										vMaxVehSize = <<2.9, 12, 8.0>> // Trying to avoid buses causing problems by deleting them. Heavy handed, but easiest. (see B*1086706, 1083190, 1086706)
										fPlayersVeh = GET_ENTITY_HEADING(tempCar)
										vPlayersVeh = GET_UNOCCUPIED_POSITION_FOR_PLAYERS_CAR(scenePosition, GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation))
										
										IF ARE_VECTORS_EQUAL(<<0,0,0>>, vPlayersVeh) 
											//We couldn't get a place to move the player's vehicle to, so let's leave it where it is...
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Couldn't get a place to move the player's vehicle to, let's leave it where it is...")PRINTNL()
											vPlayersVeh = GET_ENTITY_COORDS(tempCar)
										ENDIF
										
										If IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempCar))
											//choppers seem to have unreasonably large dimensions. Much larger than their blades.
											vMax.x += 3
											vMax.y += 3
										ENDIF
										
										bAllowPlayerLastVehicle = TRUE
										
										IF vMax.x - vMin.x > vMaxVehSize.x
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle X is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.y - vMin.y > vMaxVehSize.y
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Y is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ELIF vMax.z - vMin.z > vMaxVehSize.z
											CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Player's vehicle Z is too large")PRINTNL()
											bAllowPlayerLastVehicle = FALSE
										ENDIF
										IF bAllowPlayerLastVehicle
											IF IS_VEHICLE_OK(tempCar)
												SET_ENTITY_COORDS(tempCar, vPlayersVeh)
												SET_ENTITY_HEADING(tempCar, fPlayersVeh)
												SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
												CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh), pos is: ", vPlayersVeh)PRINTNL()
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(tempCar)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempCar)
													CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Deleted player's vehicle - sorry!")PRINTNL()
													DELETE_VEHICLE(tempCar)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " GET_UNOCCUPIED_POSITION_FOR_PLAYERS_CAR - Player's vehicle is out of shot, leave it alone")PRINTNL()
								ENDIF
							ENDIF
						ENDIF
						
						//FINAL VEHICLE CHECK - if the vehicle is still <=1m from either ped, move it to the PANIC POSITION
						IF IS_VEHICLE_OK(tempCar)
							GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempCar), vMin, vMax)
							IF IS_ENTITY_AT_COORD(tempCar, scenePosition, <<vMax.y+1, vMax.y+1, 1>>)
							OR IS_ENTITY_AT_COORD(tempCar, GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation), <<vMax.y+1, vMax.y+1, 1>>)
								SET_ENTITY_COORDS(tempCar, vVehiclePanicPos)
								SET_ENTITY_HEADING(tempCar, fVehiclePanicHead)
								SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FINAL VEHICLE CHECK FAILED, GOING TO VEHICLE PANIC POS")PRINTNL()
							ENDIF
						ENDIF
										
						IF DOES_ENTITY_EXIST(objHandbag)
							SET_ENTITY_VISIBLE(objHandbag, TRUE)
						ENDIF
						
						REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(scenePosition-<<10,10,10>>, scenePosition+<<10,10,10>>)
						//CLEAR_AREA_OF_OBJECTS(scenePosition, 20, CLEAROBJ_FLAG_FORCE)
						CLEAR_AREA_OF_PROJECTILES(scenePosition, 20)
						CLEAR_AREA_OF_PEDS(scenePosition, 10)
						CLEAR_AREA(scenePosition, vMax.y+1, TRUE)
						STOP_FIRE_IN_RANGE(scenePosition, 25)
						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FINAL scenePosition = ", scenePosition)
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FINAL sceneRotation = ", sceneRotation)
						sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneId, FALSE)
						CamIDPickingUpBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
						SET_CAM_FOV(CamIDPickingUpBag, 15)
						PLAY_SYNCHRONIZED_CAM_ANIM(CamIDPickingUpBag, sceneId, sHandoverCam, sHandoverDict)
						
						
						CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - RETURN_BAG_SYNCED_SCENE, GET_CAM_FOV(CamIDPickingUpBag) = ", GET_CAM_FOV(CamIDPickingUpBag))
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						//recreate bag
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, sHandoverDict, sHandoverPlayer, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_HIDE_WEAPON)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(pedVictim)
							CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
							TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, sHandoverDict, sHandoverPed, INSTANT_BLEND_IN, SLOW_BLEND_OUT )
						ENDIF
						
						SET_CAM_ACTIVE(CamIDPickingUpBag, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)		
						sReturnTemp = sReturn
						sReturnTemp+="_1"
						sThxTemp = sThx
						sThxTemp+="_1"
						CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(dialogueStruct, sSubGroup, sReturn, sReturnTemp, sThx, sThxTemp, CONV_PRIORITY_MEDIUM)
						SETTIMERA(0)
						REMOVE_ALL_SHOCKING_EVENTS(FALSE)
						QUICK_INCREMENT_INT_STAT(RC_WALLETS_RETURNED, 1)
						INT iDebug
						STAT_GET_INT(RC_WALLETS_RETURNED, iDebug)
						CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - RC_WALLETS_RETURNED increased, is now: ", iDebug)
						iHandOver ++
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Waiting to start handover - player is still in vehicle")
						ENDIF
						IF IS_PED_RAGDOLL(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Waiting to start handover - player is ragdoll")
						ENDIF
						IF IS_PED_PRONE(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Waiting to start handover - player is prone")
						ENDIF
						IF IS_PED_GETTING_UP(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Waiting to start handover - player is prone")
						ENDIF
					ENDIF
					HIDE_HUD_AND_RADAR_THIS_FRAME()
				BREAK
				
				CASE 1
					
					//IF TIMERA() > 3500
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) = 1)// AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
					OR DO_FADES_IF_CUT_IS_SKIPPED()
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), FALSE)
						
						IF NOT IS_PED_INJURED(pedVictim)
							SET_PED_MONEY(pedVictim, (stolenCash-(stolenCash/10)))
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
							SET_PED_MOVEMENT_CLIPSET(pedVictim, sClipSetOverride)
						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
							CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
							vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 1)
							GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
							SET_ENTITY_COORDS(pedVictim, vTemp)
							vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 1)
							SET_ENTITY_HEADING(pedVictim, vTemp.z)
						ENDIF
						vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 1)
						
//						IF ARE_VECTORS_ALMOST_EQUAL(vInput, << -2072.89, -317.213, 12.3157 >>) // ATM Robbery 16 takes place in forecourt, can't wander properly.
//						OR ARE_VECTORS_ALMOST_EQUAL(vInput, << 288.973, -1256.79, 28.4407 >>) // ATM Robbery 2 takes place in forecourt, can't wander properly.
//						OR ARE_VECTORS_ALMOST_EQUAL(vInput, << -3043.93, 594.84, 6.6115 >>) // ATM Robbery 31 takes place in drive in, can't wander properly.
//						OR ARE_VECTORS_ALMOST_EQUAL(vInput, << 174.29, 6638.08, 31.573 >>) // ATM Robbery 4 takes place in forecourt, can't wander properly.
//						OR ARE_VECTORS_ALMOST_EQUAL(vInput, <<-2956.7017, 487.7254, 14.4687>>)
//						OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<-518, -1220, 20>>) < 25 // ATM Robbery 30, garage forecourt, can't wander
//							IF ARE_VECTORS_ALMOST_EQUAL(vInput, << 288.973, -1256.79, 28.4407 >>) // ATM Robbery 2 extra stupid crap niggly shit
//								IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), <<284.89, -1272.70, 28.27>>, 120.0) // If the player is facing the convenience store
//									OPEN_SEQUENCE_TASK(seqVictim)
//										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
//										TASK_WANDER_STANDARD(NULL, vTemp.z, EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
//									CLOSE_SEQUENCE_TASK(seqVictim)
//									TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
//									CLEAR_SEQUENCE_TASK(seqVictim)
//									
//									FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK, TRUE)
//								ELSE // If the player is facing towards the underneath of the highway
//									OPEN_SEQUENCE_TASK(seqVictim)
//										TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vInput, 6, 10000)
//										TASK_WANDER_STANDARD(NULL, vTemp.z, EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
//									CLOSE_SEQUENCE_TASK(seqVictim)
//									TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
//									CLEAR_SEQUENCE_TASK(seqVictim)
//									FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK, TRUE)
//									SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(pedVictim)
//									SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(pedVictim, TRUE)
//								ENDIF
//							ELSE
//								OPEN_SEQUENCE_TASK(seqVictim)
//									TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vInput, 6, 10000)
//									TASK_WANDER_STANDARD(NULL, vTemp.z, EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
//								CLOSE_SEQUENCE_TASK(seqVictim)
//								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
//								CLEAR_SEQUENCE_TASK(seqVictim)
//								FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK, TRUE)
//								SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(pedVictim)
//								SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(pedVictim, TRUE)
//							ENDIF
//						ELSE
						IF ARE_VECTORS_ALMOST_EQUAL(vInput, << -526.58, -1222.79, 17.46 >>, 10) // Stupid extra special case shit for #30 because OH NO SHE TURNS AROUND ffs
							IF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 215.68, 60)
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #30 exit - #1")
								OPEN_SEQUENCE_TASK(seqVictim)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-503.46, -1199.39, 19.02>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ELIF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 122.0371, 60)
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #30 exit - #2")
								OPEN_SEQUENCE_TASK(seqVictim)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-543.35, -1208.09, 16.73>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ELSE
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #30 exit - default")
								OPEN_SEQUENCE_TASK(seqVictim)
									//TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vInput, 6, 10000)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ENDIF
						ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<288.46, -1256.71, 28.44>>) < 5
							IF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 200.8887, 60)
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #2 exit - #1")
								OPEN_SEQUENCE_TASK(seqVictim)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<289.08, -1286.56, 28.68>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ELIF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 347.8599, 60)
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #2 exit - #2")
								OPEN_SEQUENCE_TASK(seqVictim)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<241.22, -1236.76, 28.26>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ELSE
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Special case #2 exit - default")
								OPEN_SEQUENCE_TASK(seqVictim)
									//TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vInput, 6, 10000)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 3.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
								CLOSE_SEQUENCE_TASK(seqVictim)
								TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
								CLEAR_SEQUENCE_TASK(seqVictim)
							ENDIF
						ELSE
							OPEN_SEQUENCE_TASK(seqVictim)
								//TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vInput, 6, 10000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 2.5, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
								TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
							CLOSE_SEQUENCE_TASK(seqVictim)
							TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
							CLEAR_SEQUENCE_TASK(seqVictim)
						ENDIF
						
						FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK, TRUE)
//						ENDIF
						SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
						SET_PED_KEEP_TASK(pedVictim, TRUE)
						SAFE_RELEASE_VEHICLE(vehEscape)
						SETTIMERB(0)
						STOP_GAMEPLAY_HINT()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(objHandbag)
							DELETE_OBJECT(objHandbag)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedVictim)
							SET_PED_MONEY(pedVictim, (stolenCash-(stolenCash/10)))
							//DELETE_PED(pedVictim)
						ENDIF
						
						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_CAM_ACTIVE(CamIDPickingUpBag, FALSE)
						SET_CAMERA_OVER_THE_SHOULDER_FOR_CATCH_UP()	
						DESTROY_CAM(CamIDPickingUpBag)	
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
							WHILE IS_SCREEN_FADING_IN()
							WAIT(0)
							ENDWHILE	
						ENDIF
						INT debitCash
						debitCash = stolenCash/100 
						debitCash = debitCash*90 // take away 90% of the stolen cash, player keeps 10%
						WAIT(0)
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, debitCash)
						PRINTSTRING("\nBag has been returned\n")
						missionPassed(TRUE)
					ELSE
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Detach"))
							IF NOT IS_PED_INJURED(pedVictim)
							AND NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_ENTITY_ATTACHED_TO_ENTITY(objHandbag, PLAYER_PED_ID())
									//CREATE_CONVERSATION(dialogueStruct, sSubGroup, sThx, CONV_PRIORITY_AMBIENT_HIGH)
									DETACH_ENTITY(objHandbag, FALSE)
									IF IS_PED_MALE(pedVictim)
										ATTACH_ENTITY_TO_ENTITY(objHandbag, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
									ELSE
										ATTACH_ENTITY_TO_ENTITY(objHandbag, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
				BREAK
			ENDSWITCH
			
		ENDIF
		
	ENDIF
ENDPROC

PROC RUN_HINT_CAM()
    IF hasPickupBeenCreated
	OR IS_ENTITY_DEAD(pedRobber)
		//CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - KILL HINT CAM")
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ELSE
		//CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - RUNNING HINT CAM")
        CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedRobber, "ATM_CHASEHINT")
	ENDIF
ENDPROC

PROC START_FLEEING_ROBBER()
	SWITCH iCurrentVariation
		CASE BIKE_VARIATION
		FALLTHRU
		CASE CAR_VARIATION
			IF IS_VEHICLE_DRIVEABLE(vehEscape)
				IF bWarningIssued
					IF IS_PED_SITTING_IN_VEHICLE(pedRobber, vehEscape)
					OR GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(pedRobber, vehEscape, vInput, MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS, 10, 10)
					ELSE
						TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
					ENDIF
				ELSE
					IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehEscape)
						ADD_VEHICLE_STUCK_CHECK_WITH_WARP(vehEscape, 0.1, 1000, FALSE, FALSE, FALSE, -1)
					ENDIF
					//TASK_VEHICLE_DRIVE_WANDER(pedRobber, vehEscape, 35, DF_SwerveAroundAllCars)
					TASK_VEHICLE_MISSION_COORS_TARGET(pedRobber, vehEscape, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS, 10, 10)
				ENDIF
			ELSE
				TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
			ENDIF
		BREAK
		CASE HILL_VARIATION
			// Make the robber initially flee up the hill, then if the player bumps them
			IF NOT bWarningIssued
				OPEN_SEQUENCE_TASK(seqVictim)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 182.61, 6637.91, 30.54 >>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 240.53, 6694.10, 28.62 >>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
				CLOSE_SEQUENCE_TASK(seqVictim)
				TASK_PERFORM_SEQUENCE(pedRobber, seqVictim)
				CLEAR_SEQUENCE_TASK(seqVictim)
				CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Made HILL_VARIATION robber flee up hill specifically")
			ELSE
				TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
				CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Made HILL_VARIATION robber flee up from player")
			ENDIF
		BREAK
		DEFAULT
			IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-3044.11, 594.34, 6.73>>) < 5 // Robbery 3
				TASK_SMART_FLEE_COORD(pedRobber, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
				CLEAR_SEQUENCE_TASK(seqVictim)
			ELSE
				IF bWarningIssued
					TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
				ELSE
					TASK_SMART_FLEE_COORD(pedRobber, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	IF NOT IS_PED_INJURED(pedRobber)
		//FORCE_PED_MOTION_STATE(pedRobber, MS_ON_FOOT_SPRINT)
		SET_PED_KEEP_TASK(pedRobber, TRUE)
	ENDIF
ENDPROC

PROC FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD()
	BOOL bFlee = FALSE
	SWITCH iCurrentVariation
		CASE BIKE_VARIATION
		FALLTHRU
		CASE CAR_VARIATION
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber, PLAYER_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedRobber)
				bFlee = TRUE
				CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD, HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY")
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehEscape)
			AND IS_PED_IN_VEHICLE(pedRobber, vehEscape)	
				IF DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehEscape)
					IF IS_VEHICLE_STUCK_ON_ROOF(vehEscape)
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD, IS_VEHICLE_STUCK_ON_ROOF")
						bFlee = TRUE
					ENDIF
//					IF IS_PED_IN_VEHICLE(pedRobber, vehEscape)
//						IF IS_VEHICLE_STUCK_TIMER_UP(vehEscape, VEH_STUCK_HUNG_UP|VEH_STUCK_JAMMED|VEH_STUCK_ON_ROOF|VEH_STUCK_ON_SIDE, 4000)
//							REMOVE_VEHICLE_STUCK_CHECK(vehEscape)
//							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEscape)
//							CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD, IS_VEHICLE_STUCK_TIMER_UP")
//						ENDIF
//					ENDIF
				ENDIF
			ELSE
				IF NOT IS_VEHICLE_DRIVEABLE(vehEscape)
				OR NOT IsPedPerformingTask(pedRobber, SCRIPT_TASK_VEHICLE_MISSION)
					//CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD, NOT IS_VEHICLE_DRIVEABLE")
					bFlee = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_PED_FLEEING(pedRobber)
				IF GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_VEHICLE_MISSION) = FINISHED_TASK
					bFlee = TRUE
				ENDIF
			ENDIF
			
			IF bFlee
				IF NOT bNavFail
					IF IS_PED_IN_VEHICLE(pedRobber, vehEscape)
						IF GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
							OPEN_SEQUENCE_TASK(seqRobber)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
							CLOSE_SEQUENCE_TASK(seqRobber)
							TASK_PERFORM_SEQUENCE(pedRobber, seqRobber)
							CLEAR_SEQUENCE_TASK(seqRobber)
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
							OPEN_SEQUENCE_TASK(seqRobber)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
							CLOSE_SEQUENCE_TASK(seqRobber)
							TASK_PERFORM_SEQUENCE(pedRobber, seqRobber)
							CLEAR_SEQUENCE_TASK(seqRobber)
						ENDIF
					ENDIF
					
					// for B*1737850 - if the robber gets trapped somewhere with no nav and can't work their way out, go to combat
					NAVMESH_ROUTE_RESULT eNavResult
					eNavResult = GET_NAVMESH_ROUTE_RESULT(pedRobber)
					IF eNavResult = NAVMESHROUTE_ROUTE_NOT_FOUND
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - robber couldn't find any navmesh! Making him attack the player instead")
						CLEAR_PED_TASKS(pedRobber)
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							TASK_COMBAT_PED(pedRobber, PLAYER_PED_ID())
						ENDIF
						bNavFail = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER()
	BOOL bReturn = FALSE
	IF DOES_ENTITY_EXIST(pedVictim)
		IF IS_PED_INJURED(pedVictim)
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER() - injured")
			bReturn = TRUE
		ELSE
			IF (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID()) AND IS_PED_RAGDOLL(pedVictim))
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER() - damaged by player")
				bReturn = TRUE
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedVictim)
			ENDIF
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedVictim), 1)
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER() - bullet in area")
				bReturn = TRUE
			ENDIF
			IF HAS_PLAYER_THREATENED_PED(pedVictim, FALSE, DEFAULT, DEFAULT, FALSE, TRUE)
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER() - player threatened ped")
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bReturn
		IF CHECK_FIRING_AMNESTY_ON_VICTIM()
			bReturn = FALSE
			IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) != PERFORMING_TASK
				IF IS_PED_UNINJURED(pedVictim)
					CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> Amnesty in progress, make victim cower")
					TASK_COWER(pedVictim, 2000)
				ENDIF
			ENDIF
		ELIF NOT IS_PED_INJURED(pedRobber)
			IF GET_SCRIPT_TASK_STATUS(pedRobber, SCRIPT_TASK_SMART_FLEE_PED) = FINISHED_TASK
				SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_ALWAYS_FLEE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_AGGRESSIVE, FALSE)
				TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
			ENDIF
		ENDIF
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER()")
	ENDIF
	RETURN bReturn
ENDFUNC



PROC RUN_DEBUG()
#IF IS_DEBUG_BUILD 
	// Event Passed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		missionPassed()
	ENDIF
	// Event Failed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		missionCleanup()
	ENDIF
	IF bDebugKillRobber
		IF NOT IS_PED_INJURED(pedRobber)
			EXPLODE_PED_HEAD(pedRobber)
		ENDIF
	ENDIF
#ENDIF
ENDPROC

PROC SETUP_DEBUG()
#IF IS_DEBUG_BUILD 
	START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())			
		ADD_WIDGET_BOOL("Insta-kill robber", bDebugKillRobber)
	STOP_WIDGET_GROUP()
#ENDIF
ENDPROC

PROC RETRIEVE_PICKUP()
	IF NOT IS_PED_INJURED(pedVictim)
		TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 100, -1)
		PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_FRIGHTENED_HIGH")
		SET_PED_KEEP_TASK(pedVictim, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
		SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
	ENDIF
	PlayerRetrievingBag()
	IF hasPickupBeenCreated
		IF DOES_BLIP_EXIST(blipBag)
			IF DOES_PICKUP_EXIST(pickupPurse)
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupPurse)) > 100
					missionCleanup()
				ENDIF
			ENDIF
		ELSE
			IF bBagRetrieved
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - RETRIEVE_PICKUP()")
				missionPassed()
			ELSE
				IF IS_PED_INJURED(pedRobber)
					CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - NOT bBagRetrieved & pedRobber is dead.")
					missionCleanup()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_BLIP_EXIST(blipRobber)
		IF IS_PED_INJURED(pedRobber)
			REMOVE_BLIP(blipRobber)
		ELSE
			IF NOT IS_TARGET_IN_RANGE()
				missionCleanup()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
	IF iEarlyInterruption = -1
		IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
			IF IS_ENTITY_ON_SCREEN(pedRobber)
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH(), Timer started.")
				iEarlyInterruption = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > (iEarlyInterruption+1000)
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH() Robber in view of sniper scope")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vInput, 50)
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH() - Explosion detected!")
		RETURN TRUE
	ENDIF
	
	IF (GET_GAME_TIMER() - iEarlyLaunchTimer) > 30000
		IF NOT IS_ENTITY_OCCLUDED(pedRobber)
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH() - Timer expired, event visible, just go!")
			RETURN TRUE
		ELSE
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH() - Timer expired, event not visible, cleanup...")
			missionCleanup()
		ENDIF
		
	ENDIF
	
	IF DOES_BLIP_EXIST(blipRobber)
		IF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-3044.11, 594.34, 6.73>>) > 5 // NOT Robbery 3
		AND GET_DISTANCE_BETWEEN_COORDS(vInput, <<-712.90, -819.32, 22.73>>) > 5 // NOT Robbery 18
		AND GET_DISTANCE_BETWEEN_COORDS(vInput, <<24.13, -946.84, 28.36>>) > 5 // NOT Robbery 6
			IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedRobber, FALSE), 2.5)
				CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_PROMPTED_EARLY_LAUNCH() - Robber is visible!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	

	RETURN FALSE
ENDFUNC

PROC START_CHASE()
	OPEN_SEQUENCE_TASK(seqVictim)
		TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedRobber, 750)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
		//TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedRobber)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
		TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)

	CLOSE_SEQUENCE_TASK(seqVictim)
	TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
	CLEAR_SEQUENCE_TASK(seqVictim)
	IF DOES_BLIP_EXIST(blipVictim)
		REMOVE_BLIP(blipVictim)
	ENDIF
	//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRobber, FALSE)
	IF NOT DOES_BLIP_EXIST(blipRobber)
		blipRobber = CREATE_AMBIENT_BLIP_FOR_PED(pedRobber, TRUE)
	ENDIF
	SHOW_HEIGHT_ON_BLIP(blipRobber, TRUE)
	SET_CREATE_RANDOM_COPS(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	START_FLEEING_ROBBER()
	IF VDIST(GET_ENTITY_COORDS(pedVictim, FALSE), << -2956.26, 487.97, 15.46 >>) > 5.0 // Robbery 31
	AND GET_DISTANCE_BETWEEN_COORDS(vInput, <<-203.72, -861.80, 29.27>>) > 5 // Robbery 1
	AND GET_DISTANCE_BETWEEN_COORDS(vInput, <<288.46, -1256.71, 28.44>>) > 5 // Robbery 2
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedVictim) < 50
			CREATE_CONVERSATION(dialogueStruct, sSubGroup, sAttr, CONV_PRIORITY_AMBIENT_MEDIUM)
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - did normal attract conv")
			bAttDone = TRUE
		ELSE
			CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Not close enough to do normal attract conv...")
		ENDIF
	ENDIF
	iHintCamTimer = GET_GAME_TIMER()
	SETTIMERA(0)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE) // Stop player ped saying ambient dialogue (1535367)
	ENDIF
	SET_RANDOM_EVENT_ACTIVE()
	CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - ACTIVE!")
ENDPROC

/// PURPOSE:
///    If the player enters a larger locate around the robbery, blip the robber to alert the player that something is going on
PROC MANAGE_PRE_EVENT_BLIPPING()

	// Robbery 3 needs to be rectanguar to not catch players going past on the freeway
	IF VDIST(vInput, << -3044.06, 594.93, 6.74 >>) < 5
		IF NOT DOES_BLIP_EXIST(blipRobber)
			IF IS_ENTITY_ALIVE(pedVictim)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<60, 90, 12>>)
					blipRobber = CREATE_AMBIENT_BLIP_FOR_PED(pedRobber, TRUE)
					SHOW_HEIGHT_ON_BLIP(blipRobber, FALSE)
				ENDIF
			ENDIF
		ELSE
			FLASH_RANDOM_EVENT_DECISION_BLIP(blipRobber, iFlashTimestamp)
		ENDIF
	ELSE
		IF NOT DOES_BLIP_EXIST(blipRobber)
			IF IS_ENTITY_ALIVE(pedVictim)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<90, 90, 8>>)
					blipRobber = CREATE_AMBIENT_BLIP_FOR_PED(pedRobber, TRUE)
					SHOW_HEIGHT_ON_BLIP(blipRobber, FALSE)
				ENDIF
			ENDIF
		ELSE
			FLASH_RANDOM_EVENT_DECISION_BLIP(blipRobber, iFlashTimestamp)
		ENDIF
	ENDIF
	
	// Only set this once, when the RE first loads
	IF iEarlyLaunchTimer = -1
		iEarlyLaunchTimer = GET_GAME_TIMER()
	ENDIF

ENDPROC


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)
vInput = in_coords.vec_coord[0]
fInput = in_coords.headings[0]
//vInput.z += 1.0
IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	IF bBagRetrieved
		bForceCleanup = TRUE
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - bForceCleanup, bBagRetrieved = TRUE")
		missionPassed()
	ELSE
		CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - bForceCleanup, bBagRetrieved = FALSE")
		missionCleanup()
	ENDIF
ENDIF

IF VDIST(vInput, << -203.758, -861.738, 29.2684 >>) < 5
	iCurrentVariation = BIKE_VARIATION
ELIF VDIST(vInput, << -2957.61, 488.29, 14.47 >>) < 5
	iCurrentVariation = BIKE_VARIATION
ELIF VDIST(vInput, << 288.973, -1256.79, 28.4407 >>) < 5
	iCurrentVariation = CAR_VARIATION
ELIF VDIST(vInput, << -3043.93, 594.84, 6.6115 >>) < 5
	iCurrentVariation = NORMAL_VARIATION
ELIF VDIST(vInput, << 174.715, 6637.77, 30.5733 >>) < 5
	iCurrentVariation = HILL_VARIATION
ELSE
	iCurrentVariation = RANDOM_VARIATION
ENDIF

CPRINTLN(DEBUG_AMBIENT,"re_atmrobbery launched at ", vInput, " with heading ", fInput, ", is variation ", iCurrentVariation)

// PD - commented out as delay is now 90 game hours for all robberies
// Small section to stop the same ATM robbery being played multiple times in quick succession.
//IF ARE_VECTORS_ALMOST_EQUAL(g_vLastCompletedATMRobbery, vInput)
//	iEventDelay = iExtendedEventDelay
//ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_ATMROBBERY, iCurrentVariation, TRUE)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// PD - commented out as delay is now 90 game hours for all robberies
// Reset Last Completed Robbery pos
//IF iEventDelay = iExtendedEventDelay
//	g_vLastCompletedATMRobbery = <<0,0,0>>
//ENDIF

SETTIMERA(0)
SETUP_DEBUG()

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	RUN_DEBUG()
	FLASH_RANDOM_EVENT_RETURN_ITEM_BLIP(blipVictim, iVictimBlipFlash)
	UPDATE_LOAD_QUEUE_MEDIUM(sLoadQueue)
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
			missionCleanup()
		ENDIF
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			SWITCH ambStage
				CASE ambCanRun
					IF LOADED_AND_CREATED_Robbery()

						ambStage = (ambRunning)
					ENDIF
				BREAK
				CASE ambRunning
				
					MANAGE_PRE_EVENT_BLIPPING()
					
					IF iShockingEvent = 0
						iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, vInput, 30000)
					ENDIF
					IF DOES_ENTITY_EXIST(pedRobber)
					ENDIF
					IF DOES_ENTITY_EXIST(pedVictim)
					ENDIF
					IF NOT IS_PED_INJURED(pedRobber)
						IF IS_PED_INJURED(pedVictim)
							TASK_SMART_FLEE_COORD(pedRobber, vInput, 200, -1)
							SET_PED_KEEP_TASK(pedRobber, TRUE)
							WAIT(0)
							missionCleanup()
						ENDIF
						IF NOT IS_PED_INJURED(pedVictim)
							IF IS_SPHERE_VISIBLE(vInput, 2.5)
								IF iCurrentVariation = CAR_VARIATION
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<120, 120, 8>>)
									OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
										START_CHASE()
									ENDIF
								ELIF iCurrentVariation = BIKE_VARIATION
									// If robbery 1, launch at 100m
									IF VDIST(vInput, << -203.758, -861.738, 29.2684 >>) < 5
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<100, 100, 8>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ELSE // If robbery 31, launch at 170m
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<170, 170, 8>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ENDIF
								ELSE
									// Robbery 3 needs to be rectanguar to not catch players going past on the freeway
									IF VDIST(vInput, << -3044.06, 594.93, 6.74 >>) < 5
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<65, 65, 5>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-712.90, -819.32, 22.73>>) < 5 // Robbery 18 needs to be a bit bigger (B*1439835 & 1439826)
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<50, 50, 5>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<24.13, -946.84, 28.36>>) < 5 // Robbery 6 needs a bigger locate for Les (B*1439864)
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<60, 60, 5>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ELSE
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<30, 30, 8>>)
										OR HAS_PLAYER_PROMPTED_EARLY_LAUNCH()
											START_CHASE()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF dialogueStruct.PedInfo[2].ActiveInConversation
							REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
						ENDIF
						IF NOT IS_PED_INJURED(pedVictim)
							TASK_SMART_FLEE_COORD(pedVictim, vInput, 200, -1)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
							WAIT(0)
						ENDIF
						missionCleanup()
					ENDIF
								
				BREAK
			ENDSWITCH
			
		ELSE
			printDebugString("IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()\n")
			missionCleanup()
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(pedRobber)
			IF NOT IS_PED_INJURED(pedRobber)
				IF IsPedPerformingTask(pedRobber, SCRIPT_TASK_SMART_FLEE_POINT)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedRobber, vInput) >= 190
						CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - robber got >190m from flee point, switch to flee player from now")
						CLEAR_PED_TASKS(pedRobber)
						TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1, FALSE)
					ENDIF
				ELIF IsPedPerformingTask(pedRobber, SCRIPT_TASK_STAND_STILL)
					CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - robber seemed to be standing still, switch to flee player from now")
					CLEAR_PED_TASKS(pedRobber)
					TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1, FALSE)
				ENDIF
				
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedVictim)
		ENDIF
		
		IF GET_GAME_TIMER() > iHintCamTimer+3000
			RUN_HINT_CAM()
		ENDIF
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_VICTIM_INJURED_OR_DAMAGED_BY_PLAYER()
			AND NOT IS_PLAYER_INTERFERING_WITH_SCENE()
			
				SUPPRESS_WITNESSES_CALLING_POLICE_THIS_FRAME(PLAYER_ID())
				
				// If the player wasn't close enough to do the attract conv when the robbery launched, try it here
				IF NOT bAttDone
				AND NOT hasPickupBeenCreated
					IF DOES_ENTITY_EXIST(pedVictim)
						IF VDIST(GET_ENTITY_COORDS(pedVictim, FALSE), << -2956.26, 487.97, 15.46 >>) < 5.0 // Robbery 31
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<100, 100, 15>>)
								IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sAttr, CONV_PRIORITY_AMBIENT_MEDIUM)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - did special 31 attract conv")
									bAttDone = TRUE
								ENDIF
							ENDIF
						ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<-203.72, -861.80, 29.27>>) < 5 // Robbery 1
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<40, 40, 15>>)
								IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sAttr, CONV_PRIORITY_AMBIENT_MEDIUM)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - did special 1 attract conv")
									bAttDone = TRUE
								ENDIF
							ENDIF
						ELIF GET_DISTANCE_BETWEEN_COORDS(vInput, <<288.46, -1256.71, 28.44>>) < 5 // Robbery 2
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<50, 50, 15>>)
								IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sAttr, CONV_PRIORITY_AMBIENT_MEDIUM)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - did special 2 attract conv")
									bAttDone = TRUE
								ENDIF
							ENDIF
						ELSE // Try a catch-all for all other ATM Robberies in case they time out when the player is far away
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<50, 50, 15>>)
								IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sAttr, CONV_PRIORITY_AMBIENT_MEDIUM)
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - did generic post-launch attract conv")
									bAttDone = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF DOES_BLIP_EXIST(blipRobber)				
				
					IF NOT IS_PED_INJURED(pedRobber)
						
						IF bWarningIssued
						
							IF IS_ENTITY_AT_ENTITY(pedRobber, PLAYER_PED_ID(), <<fRetreatDist, fRetreatDist, fRetreatDist>>)
								IF NOT IS_PED_IN_ANY_VEHICLE(pedRobber)
									IF TIMERA() > 1000
										//IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber)
										IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber)
											fRetreatDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRobber))
											fRetreatDist*=1.5
											SETTIMERA(0)
										ENDIF
										SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_ALWAYS_FLEE, FALSE)
										//SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_AGGRESSIVE, TRUE)
									ENDIF
								ENDIF
								
							ELIF NOT bToldToFlee
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRobber, <<25, 25, 25>>)
									IF NOT IS_PED_IN_ANY_VEHICLE(pedRobber)
										SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_ALWAYS_FLEE, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_AGGRESSIVE, FALSE)
										START_FLEEING_ROBBER()
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - bWarningIssued, SET bToldToFlee")
										bToldToFlee = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT bHelpRequested
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber)
								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber, PLAYER_PED_ID(), FALSE)
									CREATE_CONVERSATION(dialogueStruct, sSubGroup, sWarn, CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									fRetreatDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRobber))
									fRetreatDist*=1.5
									bWarningIssued = TRUE
									bHelpRequested = TRUE
									CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - NOT bWarningIssued, SET bToldToFlee & bWarningIssued")
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(vehEscape)
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehEscape, PLAYER_PED_ID())
										SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_ALWAYS_FLEE, FALSE)
										//SET_PED_COMBAT_ATTRIBUTES(pedRobber, CA_AGGRESSIVE, TRUE)
										IF IS_PED_SITTING_IN_ANY_VEHICLE(pedRobber)
										
											//TASK_VEHICLE_DRIVE_WANDER(pedRobber, vehEscape, 35, DF_SwerveAroundAllCars)
											TASK_VEHICLE_MISSION_PED_TARGET(pedRobber, vehEscape, PLAYER_PED_ID(), MISSION_FLEE, 35, DF_SwerveAroundAllCars, 10, 10)
											
										ELSE
											TASK_SMART_FLEE_PED(pedRobber, PLAYER_PED_ID(), 300, -1)
										ENDIF
										CREATE_CONVERSATION(dialogueStruct, sSubGroup, sWarn, CONV_PRIORITY_AMBIENT_HIGH)
										SETTIMERA(0)
										fRetreatDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRobber))
										fRetreatDist*=1.5
										bWarningIssued = TRUE
										bHelpRequested = TRUE
										CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY vehEscape, NOT bWarningIssued, SET bToldToFlee & bWarningIssued")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						IF bToldToFlee
						OR bWarningIssued
						OR bPlayerIsChasing
							IF DOES_BLIP_EXIST(blipRobber)
								IF GET_BLIP_COLOUR(blipRobber) = BLIP_COLOUR_RED//ENUM_TO_INT(HUD_COLOUR_FRIENDLY) 
								ELSE
									SET_BLIP_AS_FRIENDLY(blipRobber, FALSE)
									SET_BLIP_COLOUR(blipRobber, BLIP_COLOUR_RED)
								ENDIF
							ENDIF
							IF NOT bToldToFlee
								START_FLEEING_ROBBER()
								bToldToFlee = TRUE
							ENDIF
						ELSE
							IF IS_PED_IN_COMBAT(pedRobber, PLAYER_PED_ID())
								bPlayerIsChasing = TRUE
							ENDIF
							FLASH_RANDOM_EVENT_DECISION_BLIP(blipRobber, iFlashTimestamp)
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedVictim)
							IF NOT bHelpRequested
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<15,15,10>>)
									IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sHelp, CONV_PRIORITY_AMBIENT_HIGH)
										OPEN_SEQUENCE_TASK(seqVictim)
											//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated1, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated2, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
											TASK_PLAY_ANIM(NULL, sAgitatedDict, sAgitated3, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
										CLOSE_SEQUENCE_TASK(seqVictim)
										TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
										CLEAR_SEQUENCE_TASK(seqVictim)
										SETTIMERA(0)
										bHelpRequested = TRUE
									ENDIF
								ENDIF
								
							ELIF NOT bSpecificHelpRequested
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<15,15,10>>)
									IF NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber)
									AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber)
									AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())
									AND GET_DISTANCE_BETWEEN_ENTITIES(pedVictim, pedRobber) > 30
										IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sPrompt, CONV_PRIORITY_AMBIENT_HIGH)
											SETTIMERA(0)
											bSpecificHelpRequested = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ANY_SPEECH_PLAYING(pedVictim)
									PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_CURSE_MED")
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_TARGET_IN_RANGE()
							FLEE_ON_FOOT_IF_VEHICLE_IS_DEAD()
							
							VECTOR vVictimLoc
							IF DOES_ENTITY_EXIST(pedVictim)
								vVictimLoc = GET_ENTITY_COORDS(pedVictim)
							ENDIF
							
//							IF NOT IS_ENTITY_AT_ENTITY(pedVictim, pedRobber, <<25, 25, 25>>)
							IF NOT IS_ENTITY_AT_COORD(pedRobber, vVictimLoc, <<25, 25, 25>>)
							AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRobber, <<12, 12, 12>>)
								IF TIMERA() > 20000
									IF IS_ENTITY_ALIVE(pedRobber)
									AND NOT IS_PED_RAGDOLL(pedRobber)
										IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sChase, CONV_PRIORITY_AMBIENT_HIGH)
											bPlayerIsChasing = TRUE
											SETTIMERA(0)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
						 	// Victim gives up and walks away - turns to player if they're in range
							IF IS_ENTITY_ALIVE(pedVictim)
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, g_vAnyMeansLocate)
									IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sDamn, CONV_PRIORITY_AMBIENT_HIGH)
										OPEN_SEQUENCE_TASK(seqVictim)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
											TASK_WANDER_STANDARD(NULL)
										CLOSE_SEQUENCE_TASK(seqVictim)
										TASK_PERFORM_SEQUENCE(pedVictim, seqVictim)
										CLEAR_SEQUENCE_TASK(seqVictim)
										bRobberEscaped = TRUE
										missionCleanup()
									ENDIF
								ELSE
									IF bPlayerIsChasing
										IF CREATE_CONVERSATION(dialogueStruct, sSubGroup, sCurse, CONV_PRIORITY_AMBIENT_HIGH)
											TASK_WANDER_STANDARD(pedVictim)
											FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK)
											bRobberEscaped = TRUE
											missionCleanup()
										ENDIF
									ELSE
										TASK_WANDER_STANDARD(pedVictim)
										FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK)
										bRobberEscaped = TRUE
										missionCleanup()
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_AMBIENT,GET_THIS_SCRIPT_NAME(), " - Robber escaped, victim dead, clean up now")
								bRobberEscaped = TRUE
								missionCleanup()
							ENDIF
						ENDIF
						
						
					ELSE
						IF IS_TARGET_IN_RANGE()
							IF IS_PED_INJURED(pedRobber)
							OR IS_PED_DEAD_OR_DYING(pedRobber)
							OR IsPedPerformingTask(pedRobber, SCRIPT_TASK_WRITHE)
								CREATE_CONVERSATION(dialogueStruct, sSubGroup, sGotcha, CONV_PRIORITY_AMBIENT_HIGH)
								DETACH_AND_BLIP_PURSE()
								REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
								REMOVE_BLIP(blipRobber)
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
				
					IF NOT DOES_BLIP_EXIST(blipVictim)
						IF bBagRetrieved
							IF DOES_BLIP_EXIST(blipBag)
								REMOVE_BLIP(blipBag)
							ENDIF
							blipBag = NULL
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
							START_RETURN_ITEM_BLIP_FLASHING(iVictimBlipFlash)
							IF DOES_ENTITY_EXIST(pedVictim)
								storedDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim, FALSE))
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedVictim)
							ENDIF
							IF storedDistance > 750
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Player is >750m from the victim when picking up the bag. Too far away to return it. Passing mission.")
								missionPassed()
							ENDIF
						ELSE
							PlayerRetrievingBag()
						ENDIF
					ELSE
					
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Player got a wanted level while returning the cash, cleanup")
							IF bBagRetrieved
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - bBagRetrieved = TRUE, pass RE")
								missionPassed()
							ELSE
								CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - bBagRetrieved = FALSE, just clean up RE")
								missionCleanup()
							ENDIF
						ENDIF

						IF bBagReturned
							
						ELSE
							RETURN_BAG_SYNCED_SCENE()
						ENDIF

						// Player moves more than 50m further from robbery victim, mission ends, player keeps cash.
						// Or if the player shoots the victim the mission cleans up.
						IF NOT bBagReturned
							IF NOT IS_PED_INJURED(pedVictim)
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim, FALSE)) > (storedDistance+100)
								//OR GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < stolenCash //####Commented out as broken####

									IF bBagRetrieved
										CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - Player moves more than 100m further from robbery victim. Total cash is ", GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()))
										missionPassed()
									ELSE
										TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 100, -1)
										missionCleanup()
									ENDIF
									
									
								ELSE
									IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim, FALSE)) < storedDistance
										storedDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim, FALSE))
									ENDIF
								ENDIF
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
									IF bBagRetrieved
										CPRINTLN(DEBUG_AMBIENT,"<", GET_THIS_SCRIPT_NAME(), "> - HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())")
										missionPassed()
//									ELSE
//										TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 100, -1)
//										missionCleanup()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE 
				RETRIEVE_PICKUP()	
			ENDIF
		ELSE
			RETRIEVE_PICKUP()
		ENDIF
	ENDIF
	
ENDWHILE


ENDSCRIPT


