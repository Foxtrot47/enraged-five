
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING"commands_path.sch"
USING "AggroSupport.sch"
USING "script_blips.sch"
USING "script_buttons.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "script_ped.sch"
USING "commands_fire.sch"
USING "commands_path.sch"
USING "Ambient_Common.sch"
USING "commands_event.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "blip_control_public.sch"
USING "locates_public.sch"
USING "random_events_public.sch"
USING "RC_Helper_Functions.sch"
USING "load_queue_public.sch"

ENUM ambStageFlag
	ambLoadingAssets,
	ambWaitForTrigger,
	ambAction,
	ambChaseThieves,
	ambPickUpItem,
	ambReturnItem,
	ambThankYou
ENDENUM
ambStageFlag ambStage = ambLoadingAssets

ENUM fleeingStages
	fleeROBBERY_TRIGGERED,
	fleeENTER_VEHICLE,
	fleeFLEE_IN_VEHICLE,
	fleeFLEE_ON_FOOT
ENDENUM
fleeingStages fleeStage = fleeROBBERY_TRIGGERED

VECTOR vInput

BOOL bSpedUp  = FALSE, bLostAttack, bLostSpeedUp

BOOL bShopkeeperReacted2 = FALSE
BOOL bPlayerBagCollected, bForcedCleanup, bVictimIsNearBag, bVictimPickedUpBag

BOOL bPlayedPlayerSpottedBikersDialogue, bPlayedPlayerWouldKillBikersDialogue
INT iPlayedPlayerYellAtBikersDialogue
INT iPlayerYellAtBikersTimer = -1

INT iPhoneCallStage
INT iPhoneStartTimer
INT iPhoneCallReturnStage
INT iThiefTauntTimer

INT iTimeInRange

PED_INDEX pedThief, pedDriver, pedBackLeft, pedBackRight
FLOAT fCreateThiefHdg 

PED_INDEX pedVictim
VECTOR vCreateVictimPos 
FLOAT fCreateVictimHdg

VEHICLE_INDEX vehGetawayCar
VECTOR vCreateGetawayCarPos 
FLOAT fCreateGetawayCarHdg

VEHICLE_INDEX vehBike[2] 
PED_INDEX pedRider[2]
PED_INDEX pedShooter[2]

VEHICLE_INDEX vehPlayerCar

AI_BLIP_STRUCT pedThiefBlips, pedDriverBlips, pedBackLeftBlips, pedBackRightBlips, pedRiderBlips[2], pedShooterBlips[2]

OBJECT_INDEX objStolenItem

BLIP_INDEX blipThief
BLIP_INDEX blipVictim
BLIP_INDEX blipStolenItem

VECTOR vVictimRunToPos
FLOAT fVictimStartCutsceneHeading

VEHICLE_INDEX vehVictimCar
VECTOR vVictimCarPos
FLOAT fVictimCarHeading

SEQUENCE_INDEX seq

SCENARIO_BLOCKING_INDEX blockedArea

INT i, iBumpedCounter, iFlashTimestamp

BOOL bHasBeenBumped

BOOL bInterrupted = FALSE
BOOL bShoutedForHelp = FALSE
BOOL bPlayerIsCloseWithItem = FALSE, bThiefHasFled
FLOAT fStoreBagCollectDist = -1
BOOL bPlayedGottaGetConversation = FALSE
BOOL bPlayedThanksCollectedOwnItemConversation = FALSE

INT iItemValue

INT sceneId
CAMERA_INDEX CamIDPickingUpBag	
INT iHandOver = 0			
INT iWhichWorldPoint
VECTOR vWorldPoint1 = << 80.73, -212.88, 54.50 >> // City1
VECTOR vWorldPoint2 = <<330.9180, -837.8148, 28.2916>> // City2
VECTOR vWorldPoint3 = << 1655.12, 4868.17, 42.03 >> // Country1
VECTOR vWorldPoint4 = << 1171.82, 2695.75, 37.96 >> // Country2
CONST_INT CHASE_THIEVES_CITY_1 1
CONST_INT CHASE_THIEVES_CITY_2 2
CONST_INT CHASE_THIEVES_COUNTRY_1 3
CONST_INT CHASE_THIEVES_COUNTRY_2 4

INT iAlreadyComplete

INT iBitFieldDontCheck = 0
INT iLockOnTimer
EAggro aggroReason

INT iStoredPhonecallProgress

INT iPlacementFlags

MODEL_NAMES modelThief
MODEL_NAMES modelVictim
MODEL_NAMES modelGetwayVeh
MODEL_NAMES modelVictimVeh = INGOT
MODEL_NAMES modelStolenItem

STRING sHandoverDict, sHandoverPlayer, sHandoverPed, sHandoverCam

VECTOR animScenePosition
VECTOR animSceneRotation
VECTOR vAnimOffsetRot

VECTOR vDroppedItemPlacement

VECTOR vThiefOffset = <<1,1,0>>
BOOL bPlayerScaredVictim, bPlayedVictimDialogue
structPedsForConversation ChaseThievesSpeechStruct
structPedsForConversation dialogueStruct

REL_GROUP_HASH relGroupBaddies

INT iShockingEvent

STRING sStruggleDict 			
STRING sStruggleLoopPed			
STRING sStruggleLoopThief		

STRING sStruggleFleeDict 		= "RANDOM@CHASETHIEVESGEN"
STRING sStruggleFleePed			= "FLEE_BACKWARD_Shopkeeper"
STRING sStruggleFleeThief		= "FLEE_BACKWARD_Thief"
STRING sComplainDict 			= "RANDOM@BICYCLE_THIEF@ASK_HELP"
STRING sComplainAnim 			= "MY_DADS_GOING_TO_KILL_ME"

PICKUP_INDEX pickupItem
STRING sPickupDict  = "pickup_object"

CONST_FLOAT CRUISE_SPEED_NORMAL 25.0
CONST_FLOAT CRUISE_SPEED_FAST 40.0

FLOAT fFailDistance = 300

INT iHandoverCutsceneSafetyTimer

INT iBlipFlashTimer

STREAMVOL_ID streamVolumeChaseThieves

LoadQueueLight sLoadQueue // Medium causes a stack overflow so have to use Light queue

BOOL bDone1stPersonCameraFlash = FALSE

BOOL bSetNoCopsForRE = FALSE

FUNC VECTOR GET_WORLD_POINT()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint1)
		iWhichWorldPoint = CHASE_THIEVES_CITY_1
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2) 
			iWhichWorldPoint = CHASE_THIEVES_CITY_2
		ENDIF
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint3) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint3) 
			iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
		ENDIF
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint4) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint4) 
			iWhichWorldPoint = CHASE_THIEVES_COUNTRY_2
		ENDIF
		IF iWhichWorldPoint = CHASE_THIEVES_CITY_1
			RETURN vWorldPoint1
		ENDIF
		IF iWhichWorldPoint = CHASE_THIEVES_CITY_2
			RETURN vWorldPoint2
		ENDIF
		IF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
			RETURN vWorldPoint3
		ENDIF
		IF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_2
			RETURN vWorldPoint4
		ENDIF
	ENDIF
	RETURN << 0, 0, 0 >>
ENDFUNC

PROC HANDLE_LOST_TASKS_AFTER_MISSION(PED_INDEX ped_lost)
	IF IS_ENTITY_ALIVE(ped_lost)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(ped_lost)
			AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(ped_lost))
			AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(ped_lost), VS_DRIVER) = ped_lost
				TASK_VEHICLE_MISSION_PED_TARGET(ped_lost, GET_VEHICLE_PED_IS_IN(ped_lost), PLAYER_PED_ID(), MISSION_FLEE, CRUISE_SPEED_FAST, DRIVINGMODE_AVOIDCARS_RECKLESS, -1, -1)
			ELSE
				TASK_SMART_FLEE_PED(ped_lost, PLAYER_PED_ID(), 150, -1)
			ENDIF
		ELSE
			TASK_SMART_FLEE_COORD(ped_lost, GET_ENTITY_COORDS(ped_lost), 150, -1)
		ENDIF
		SET_PED_KEEP_TASK(ped_lost, TRUE)
	ENDIF
ENDPROC

PROC CLEANUP_LOST_PED(PED_INDEX ped_lost)
	IF IS_ENTITY_ALIVE(ped_lost)	
		SET_PED_COMBAT_ATTRIBUTES(ped_lost, CA_USE_VEHICLE, TRUE)
		//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_lost, FALSE) // This was causing B*1306736 - the ambient gang AI would acquire them post-mission and make them attack the player
		SET_PED_FLEE_ATTRIBUTES(ped_lost, FA_NEVER_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(ped_lost, FA_USE_VEHICLE, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped_lost, TRUE)
		SET_PED_CONFIG_FLAG(ped_lost, PCF_DontInfluenceWantedLevel, FALSE)
		SET_ENTITY_LOAD_COLLISION_FLAG(ped_lost, FALSE)
		HANDLE_LOST_TASKS_AFTER_MISSION(ped_lost)
	ENDIF
	SAFE_RELEASE_PED(ped_lost)
ENDPROC

PROC RELEASE_SURVIVING_LOST_PEDS()
	CLEANUP_LOST_PED(pedThief)
	CLEANUP_LOST_PED(pedDriver)
	CLEANUP_LOST_PED(pedBackLeft)
	CLEANUP_LOST_PED(pedBackRight)
	CLEANUP_LOST_PED(pedRider[0])
	CLEANUP_LOST_PED(pedRider[1])
	CLEANUP_LOST_PED(pedShooter[0])
	CLEANUP_LOST_PED(pedShooter[1])
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Released Lost peds") #ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()
	SAFE_REMOVE_BLIP(blipThief)
	STOP_RETURN_ITEM_BLIP_FLASHING(iBlipFlashTimer)
	SAFE_REMOVE_BLIP(blipVictim)
	SAFE_REMOVE_BLIP(blipStolenItem)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Removed all blips") #ENDIF
ENDPROC

PROC CHASE_THIEVES_START_AUDIO_SCENE(BOOL b_start)
	IF b_start = TRUE
		IF NOT IS_AUDIO_SCENE_ACTIVE("RE_CHASE_THIEVES_SCENE")
			START_AUDIO_SCENE("RE_CHASE_THIEVES_SCENE")
			IF IS_ENTITY_ALIVE(vehGetawayCar)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehGetawayCar, "RE_CHASE_THIEVES_BIKE")
				SET_AUDIO_VEHICLE_PRIORITY(vehGetawayCar, AUDIO_VEHICLE_PRIORITY_HIGH)
			ENDIF
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started audio scene RE_CHASE_THIEVES_SCENE") #ENDIF
		ENDIF
	ELIF IS_AUDIO_SCENE_ACTIVE("RE_CHASE_THIEVES_SCENE")
		IF IS_ENTITY_ALIVE(vehGetawayCar)
			SET_AUDIO_VEHICLE_PRIORITY(vehGetawayCar, AUDIO_VEHICLE_PRIORITY_NORMAL)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehGetawayCar)
		ENDIF
		STOP_AUDIO_SCENE("RE_CHASE_THIEVES_SCENE")
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Stopped audio scene RE_CHASE_THIEVES_SCENE") #ENDIF
	ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Doing cleanup...") #ENDIF
	REMOVE_SCENARIO_BLOCKING_AREA(blockedArea)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(modelThief)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelGetwayVeh)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVictimVeh)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelStolenItem)
	REMOVE_ANIM_DICT(sStruggleDict)
	REMOVE_ANIM_DICT(sStruggleFleeDict)
	REMOVE_ANIM_DICT(sHandoverDict)
	REMOVE_ANIM_DICT(sPickupDict)
	REMOVE_ANIM_DICT(sComplainDict)
	REMOVE_VEHICLE_ASSET(modelGetwayVeh)
	
	//IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
	IF bSetNoCopsForRE
		SET_CREATE_RANDOM_COPS(TRUE)
		SET_MAX_WANTED_LEVEL(6)
		SET_WANTED_LEVEL_MULTIPLIER(1)
	ENDIF
	IF IS_ENTITY_ALIVE(pedVictim)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
		SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, TRUE)
		IF iHandOver = 0 // If > 0 the outro cutscene will have given a task
		AND bVictimPickedUpBag = FALSE // Picking up the bag will have given a task
		AND NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_SMART_FLEE_PED)
		AND NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim) // B*1461860 Don't start mobile phone scenario if already using phone
			TASK_START_SCENARIO_IN_PLACE(pedVictim, "WORLD_HUMAN_STAND_MOBILE")
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim set to do WORLD_HUMAN_STAND_MOBILE scenario") #ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
		ENDIF
		SET_PED_KEEP_TASK(pedVictim, TRUE)
	ENDIF
	
	CHASE_THIEVES_START_AUDIO_SCENE(FALSE)
	
	REMOVE_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 0)
	REMOVE_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 1)
	REMOVE_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 2)
	REMOVE_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 3)
	REMOVE_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 4)
	
	REMOVE_ALL_BLIPS()
	RELEASE_SURVIVING_LOST_PEDS()
	SAFE_RELEASE_VEHICLE(vehGetawayCar)
	SAFE_RELEASE_VEHICLE(vehVictimCar)
	SAFE_RELEASE_VEHICLE(vehPlayerCar)
	SAFE_RELEASE_PED(pedVictim)
	
	SAFE_DELETE_OBJECT(objStolenItem)
	IF iShockingEvent <> 0
		REMOVE_SHOCKING_EVENT(iShockingEvent)
	ENDIF
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	CLEANUP_LOAD_QUEUE_LIGHT(sLoadQueue)
	
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Done cleanup") #ENDIF
	RANDOM_EVENT_OVER()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	IF NOT bForcedCleanup
		LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	ENDIF
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
	ENDIF
	RANDOM_EVENT_PASSED(RE_CHASETHIEVES, iWhichWorldPoint)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Done mission passed") #ENDIF
	MISSION_CLEANUP()
ENDPROC

FUNC BOOL LOADED_ASSETS()

	REQUEST_ANIM_DICT(sStruggleDict)
	REQUEST_ANIM_DICT(sStruggleFleeDict)
	REQUEST_ANIM_DICT(sComplainDict)
	REQUEST_VEHICLE_ASSET(modelGetwayVeh) // B*1411188 Ensure vehicle assets are loaded

	IF sLoadQueue.iLastFrame <= 0
		sLoadQueue.iLastFrame = GET_FRAME_COUNT()
	ENDIF
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, modelThief)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, modelVictim)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, modelGetwayVeh)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, modelVictimVeh)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, modelStolenItem)
	SET_LOAD_QUEUE_LIGHT_FRAME_DELAY(sLoadQueue, 2)
	
	IF HAS_ANIM_DICT_LOADED(sStruggleDict)
	AND HAS_ANIM_DICT_LOADED(sStruggleFleeDict)
	AND HAS_ANIM_DICT_LOADED(sComplainDict)
	AND HAS_VEHICLE_ASSET_LOADED(modelGetwayVeh) // B*1411188 Ensure vehicle assets are loaded
	AND HAS_LOAD_QUEUE_LIGHT_LOADED(sLoadQueue)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Loaded all assets") #ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC INITIALISE_VARIABLES()
		
	modelStolenItem = PROP_LD_WALLET_PICKUP // B*1455908
		
	IF iWhichWorldPoint = CHASE_THIEVES_CITY_1
		fCreateThiefHdg  = 339.6604
		vCreateVictimPos = << 103.8348, -216.6055, 53.6412 >> 
		fCreateVictimHdg = 319.2591 
		vCreateGetawayCarPos =  <<113.8783, -193.8459, 53.4281>>
		fCreateGetawayCarHdg = 265.1665
		vVictimRunToPos = <<116.9988, -197.4567, 53.6658>>
		vVictimCarPos = <<0, 0, 0>>
		fVictimCarHeading = 0
		animScenePosition = <<110.9119, -196.8555, 53.6768>>
		animSceneRotation =  << 0.000, 0.000, 140 >>
		modelThief = G_M_Y_Lost_02
		modelVictim = S_M_Y_BusBoy_01
		modelGetwayVeh = GBURRITO
		iItemValue = 2000
		sStruggleDict 			= "RANDOM@CHASETHIEVES1"
		sStruggleLoopPed		= "STRUGGLE_Loop_A_Shopkeeper"
		sStruggleLoopThief		= "STRUGGLE_Loop_A_Thief"
		sHandoverDict 			= "RANDOM@CHASETHIEVES1"
		sHandoverPlayer 		= "Return_Wallet_Positive_A_Player"
		sHandoverPed 			= "Return_Wallet_Positive_A_Male"
		sHandoverCam 			= "Return_Wallet_Positive_A_Cam"
	ELIF iWhichWorldPoint = CHASE_THIEVES_CITY_2
		fCreateThiefHdg  = 353.5540
		vCreateVictimPos = << 346.1016, -874.9264, 28.2916 >>  
		fCreateVictimHdg = 33.5758
		vCreateGetawayCarPos = <<321.6273, -841.5636, 28.1381>> 
		fCreateGetawayCarHdg =  109.3726 
		vVictimRunToPos = <<327.0495, -839.6594, 28.2922>>
		vVictimCarPos = <<0, 0, 0>>
		fVictimCarHeading = 0
		animScenePosition = <<330.2017, -838.6019, 28.2922>>
		animSceneRotation = << 0.000, 0.000, 287.6646 >>
		modelThief = G_M_Y_Lost_02
		modelVictim = S_M_Y_BusBoy_01
		modelGetwayVeh = HEXER
		iItemValue = 500
		sStruggleDict 			= "RANDOM@CHASETHIEVES2"
		sStruggleLoopPed		= "STRUGGLE_Loop_B_Shopkeeper"
		sStruggleLoopThief		= "STRUGGLE_Loop_B_Thief"
		sHandoverDict 			= "RANDOM@CHASETHIEVES2"
		sHandoverPlayer 		= "Return_Wallet_Positive_B_Player"
		sHandoverPed 			= "Return_Wallet_Positive_B_Male"
		sHandoverCam 			= "Return_Wallet_Positive_B_Cam"
	ELIF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
		fCreateThiefHdg  = 194.6499
		vCreateVictimPos = << 1660.5450, 4864.6509, 40.1540 >> 
		fCreateVictimHdg = 194.6499
		vCreateGetawayCarPos = <<1665.7468, 4824.5820, 40.8960>>
		fCreateGetawayCarHdg = 203.4643
		animScenePosition = <<1661.5680, 4827.7710, 41.0597>>
		animSceneRotation = << -0.000, 0.000, 30 >>
		vVictimRunToPos = <<1663.0087, 4827.7690, 41.1046>>
		vVictimCarPos = <<1653.9741, 4826.0293, 41.0037>>
		fVictimCarHeading = 277.9738
		modelThief = G_M_Y_Lost_02
		modelVictim = S_M_Y_BusBoy_01
		modelGetwayVeh = GBURRITO
		iItemValue = 250
		sStruggleDict 			= "RANDOM@CHASETHIEVES3"
		sStruggleLoopPed		= "STRUGGLE_Loop_C_Shopkeeper"
		sStruggleLoopThief		= "STRUGGLE_Loop_C_Thief"
		sHandoverDict 			= "RANDOM@CHASETHIEVES3"
		sHandoverPlayer 		= "Return_Wallet_Positive_C_Player"
		sHandoverPed 			= "Return_Wallet_Positive_C_Male"
		sHandoverCam 			= "Return_Wallet_Positive_C_Cam"
	ELIF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_2
		fCreateThiefHdg  = 84.7987 
		vCreateVictimPos = << 1218.7180, 2715.3376, 37.0041 >>
		fCreateVictimHdg = 148.6610 
		vCreateGetawayCarPos = <<1207.0947, 2694.2141, 36.7963>>
		fCreateGetawayCarHdg = 226.0923 
		vVictimRunToPos =  <<1213.2637, 2690.3936, 36.5998>>
		vVictimCarPos = <<1207.2872, 2706.8787, 37.0047>>
		fVictimCarHeading = 179.4160
		animScenePosition = <<1201.6560, 2696.7625, 36.9226>>
		animSceneRotation = << 0.000, 0.000, 42.7858 >>
		modelThief = G_M_Y_Lost_02
		modelVictim = S_M_Y_BusBoy_01
		modelGetwayVeh = GBURRITO
		iItemValue = 120
		sStruggleDict 			= "RANDOM@CHASETHIEVES1"
		sStruggleLoopPed		= "STRUGGLE_Loop_A_Shopkeeper"
		sStruggleLoopThief		= "STRUGGLE_Loop_A_Thief"
		sHandoverDict 			= "RANDOM@CHASETHIEVES1"
		sHandoverPlayer 		= "Return_Wallet_Positive_A_Player"
		sHandoverPed 			= "Return_Wallet_Positive_A_Male"
		sHandoverCam 			= "Return_Wallet_Positive_A_Cam"
	ENDIF
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Initialised variables") #ENDIF
ENDPROC

PROC SET_LOST_ATTRIBUTES(PED_INDEX ped)
	IF IS_ENTITY_ALIVE(ped)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_AGGRESSIVE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
		SET_PED_FLEE_ATTRIBUTES(ped, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_FLEE_ATTRIBUTES(ped, FA_NEVER_FLEE, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped, FALSE)
		SET_ENTITY_IS_TARGET_PRIORITY(ped, TRUE)
		SET_PED_CONFIG_FLAG(ped, PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_CONFIG_FLAG(ped, PCF_DontTakeOffHelmet, TRUE)
		SET_PED_CONFIG_FLAG(ped, PCF_ForcedAim, TRUE)
		SET_PED_CONFIG_FLAG(ped, PCF_DisableGoToWritheWhenInjured, TRUE)
		SET_PED_CONFIG_FLAG(ped, PCF_AllowMedicsToAttend, FALSE) // B*1408689
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(ped, TRUE)
		SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(ped, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(ped, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped, relGroupBaddies)
		SET_PED_MONEY(ped, 0)
		SET_PED_ACCURACY(ped, 15)
		SET_PED_RESET_FLAG(ped, PRF_UseFastEnterExitVehicleRates, TRUE)
		SET_AMBIENT_VOICE_NAME(ped, "G_M_Y_Lost_02_WHITE_FULL_01")
	ENDIF
ENDPROC

PROC CREATE_INITIAL_SCENE()

	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Setting up initial scene...") #ENDIF
	
	CLEAR_AREA(vCreateVictimPos, 20.0, TRUE)
	
	pedVictim = CREATE_PED(PEDTYPE_MISSION, modelVictim, vCreateVictimPos, fCreateVictimHdg)
	SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_USE_VEHICLE, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, FALSE)
	SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_DISABLE_HANDS_UP, TRUE)
	SET_PED_CONFIG_FLAG(pedVictim, PCF_DisableWeirdPedEvents, TRUE)
	SET_AMBIENT_VOICE_NAME(pedVictim, "S_M_Y_BusBoy_01_WHITE_MINI_01")
	
	IF NOT IS_VECTOR_ZERO(vVictimCarPos)
		vehVictimCar = CREATE_VEHICLE(modelVictimVeh, vVictimCarPos, fVictimCarHeading) // B*1426620 Create car for the victim to drive off in
		SET_VEHICLE_ON_GROUND_PROPERLY(vehVictimCar)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehVictimCar, FALSE)
		SET_VEHICLE_COLOUR_COMBINATION(vehVictimCar, 0)
		SET_VEHICLE_DIRT_LEVEL(vehVictimCar, 10)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVictimVeh)
		
	SET_ENTITY_LOAD_COLLISION_FLAG(pedVictim, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
	SET_PED_MONEY(pedVictim, 0)
	SET_PED_LOD_MULTIPLIER(pedVictim, 1.5)
	
	pedThief = CREATE_PED(PEDTYPE_MISSION, modelThief, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, vThiefOffset), fCreateThiefHdg)
	vehGetawayCar = CREATE_VEHICLE(modelGetwayVeh, vCreateGetawayCarPos, fCreateGetawayCarHdg)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehGetawayCar)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelGetwayVeh, TRUE)
	SET_DISABLE_PRETEND_OCCUPANTS(vehGetawayCar, TRUE)
	//SET_PED_SUFFERS_CRITICAL_HITS(pedThief, FALSE) // B*1521148
	SET_VEHICLE_DISABLE_TOWING(vehGetawayCar, TRUE)
	SET_VEHICLE_ENGINE_ON(vehGetawayCar, TRUE, TRUE)
	//SET_PED_COMPONENT_VARIATION(pedThief, PED_COMP_HEAD, 1, 0, 0)
	
	ADD_RELATIONSHIP_GROUP("re_chasethieves badGuys", relGroupBaddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBaddies, RELGROUPHASH_PLAYER)
	
	SWITCH iWhichWorldPoint
		CASE CHASE_THIEVES_CITY_1	
			// TWO GUYS - BOTH FLEE
			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief)
			GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_PISTOL, -1)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1)
			SET_LOST_ATTRIBUTES(pedDriver)
			SET_LOST_ATTRIBUTES(pedThief)
		BREAK
		CASE CHASE_THIEVES_CITY_2	
			// TWO GUYS - BOTH FIGHT
			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief)
			GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_SAWNOFFSHOTGUN, -1)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1)
			SET_LOST_ATTRIBUTES(pedDriver)
			SET_LOST_ATTRIBUTES(pedThief)
		BREAK
		CASE CHASE_THIEVES_COUNTRY_1
			// FULL VEHICLE - ALL FIGHT
			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief)
			GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_PISTOL, -1)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1)
			SET_LOST_ATTRIBUTES(pedDriver)
			SET_LOST_ATTRIBUTES(pedThief)
			IF modelGetwayVeh = GBURRITO
				pedBackLeft = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief, VS_BACK_LEFT)
				pedBackRight = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief, VS_BACK_RIGHT)
				GIVE_WEAPON_TO_PED(pedBackLeft, WEAPONTYPE_PISTOL, -1)
				GIVE_WEAPON_TO_PED(pedBackRight, WEAPONTYPE_PISTOL, -1)
				SET_LOST_ATTRIBUTES(pedBackLeft)
				SET_LOST_ATTRIBUTES(pedBackRight)
			ENDIF
		BREAK
		CASE CHASE_THIEVES_COUNTRY_2	
			// FULL VEHICLE - CREATE_BACKUP BIKES
			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief)
			GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_PISTOL, -1)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1)
			SET_LOST_ATTRIBUTES(pedDriver)
			SET_LOST_ATTRIBUTES(pedThief)
			IF modelGetwayVeh = GBURRITO
				pedBackLeft = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief, VS_BACK_LEFT)
				pedBackRight = CREATE_PED_INSIDE_VEHICLE(vehGetawayCar, PEDTYPE_CRIMINAL, modelThief, VS_BACK_RIGHT)
				GIVE_WEAPON_TO_PED(pedBackLeft, WEAPONTYPE_PISTOL, -1)
				GIVE_WEAPON_TO_PED(pedBackRight, WEAPONTYPE_PISTOL, -1)
				SET_LOST_ATTRIBUTES(pedBackLeft)
				SET_LOST_ATTRIBUTES(pedBackRight)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		bPlayedPlayerSpottedBikersDialogue = TRUE // Only Trevor has this dialogue
		iPlayedPlayerYellAtBikersDialogue = 9 // Only Trevor has this dialogue
		bPlayedPlayerWouldKillBikersDialogue = TRUE // Only Trevor has this dialogue
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		bPlayedPlayerSpottedBikersDialogue = TRUE // Only Trevor has this dialogue
		iPlayedPlayerYellAtBikersDialogue = 9 // Only Trevor has this dialogue
		bPlayedPlayerWouldKillBikersDialogue = TRUE // Only Trevor has this dialogue
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 2, PLAYER_PED_ID(), "TREVOR")
		bPlayedPlayerSpottedBikersDialogue = FALSE // Only Trevor has this dialogue
		iPlayedPlayerYellAtBikersDialogue = 0 // Only Trevor has this dialogue
		bPlayedPlayerWouldKillBikersDialogue = FALSE // Only Trevor has this dialogue
	ENDIF
	ADD_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 3, pedVictim, "ChaseWorker")
	ADD_PED_FOR_DIALOGUE(ChaseThievesSpeechStruct, 4, pedThief, "ChaseThief")
	
	IF iWhichWorldPoint = CHASE_THIEVES_CITY_2
		//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGetawayCar, FALSE)
		//TASK_VEHICLE_TEMP_ACTION(pedDriver, vehGetawayCar, TEMPACT_REV_ENGINE, -1)
	ELSE
		SET_VEHICLE_DOOR_OPEN(vehGetawayCar, SC_DOOR_FRONT_RIGHT)
	ENDIF

	//SET_PED_SUFFERS_CRITICAL_HITS(pedDriver, FALSE) // B*1521148
	
	SET_ENTITY_COORDS_NO_OFFSET(pedThief,  GET_ANIM_INITIAL_OFFSET_POSITION(sStruggleDict, sStruggleLoopThief,  animScenePosition, animSceneRotation))
	vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sStruggleDict, sStruggleLoopThief,  animScenePosition, animSceneRotation)
	SET_ENTITY_HEADING(pedThief, vAnimOffsetRot.z)

	SET_ENTITY_COORDS_NO_OFFSET(pedVictim,  GET_ANIM_INITIAL_OFFSET_POSITION(sStruggleDict, sStruggleLoopPed,  animScenePosition, animSceneRotation))
	vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sStruggleDict, sStruggleLoopPed,  animScenePosition, animSceneRotation)
	SET_ENTITY_HEADING(pedVictim, vAnimOffsetRot.z)

	TASK_PLAY_ANIM(pedThief, sStruggleDict, sStruggleLoopThief, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE )
	TASK_PLAY_ANIM(pedVictim, sStruggleDict, sStruggleLoopPed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE )

	IF iWhichWorldPoint = CHASE_THIEVES_CITY_2
		GIVE_PED_HELMET(pedThief, TRUE)
		VEHICLE_INDEX vehBikes[2]
		VECTOR vBikesPos[2]
		FLOAT fBikesHdg[2]
		vBikesPos[0] = << 381.5702, -867.3578, 28.1573 >>
		vBikesPos[1] = << 379.5334, -867.1062, 28.1713 >>
		fBikesHdg[0] = 236.3457
		fBikesHdg[1] = 232.1922  
		FOR i = 0 to 1
			vehBikes[i] = CREATE_VEHICLE(modelGetwayVeh, vBikesPos[i], fBikesHdg[i])
			SET_VEHICLE_ON_GROUND_PROPERLY(vehBikes[i])
		ENDFOR
	ENDIF
	
	IF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
		blockedArea = ADD_SCENARIO_BLOCKING_AREA(<<1608.8, 4824.81,0.0>>, <<1730.9,4856.1,51.1>>)
		CLEAR_AREA_OF_PEDS(<<1973.98,4817.32,41.53>>, 10.0)
	ENDIF
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_HeardShot))
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Danger))
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_ShotNear))
	
	IF iShockingEvent = 0
		iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, vInput, 60000)
	ENDIF
		
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Finished setting up initial scene") #ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_DAMAGED_THIEF()
	IF NOT IS_PED_INJURED(pedThief)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedThief, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehGetawayCar, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_PLAYER_HURT_THIEF_IN_FRONT_OF_SHOPKEEPER()
	IF IS_ENTITY_ALIVE(pedVictim)
		IF HAS_PLAYER_DAMAGED_THIEF()
			IF NOT IS_PED_INJURED(pedThief)
				IF IS_ENTITY_AT_ENTITY(pedVictim, pedThief, << 20, 20, 10 >>)
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedVictim, pedThief)
					RETURN TRUE
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(pedThief)
				AND IS_ENTITY_DEAD(pedThief)
				AND IS_ENTITY_AT_COORD(pedVictim, GET_ENTITY_COORDS(pedThief, FALSE), << 20, 20, 10 >>)
				AND	HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedVictim, pedThief)
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LOADED_AND_CREATED_ATTACKING_BIKERS()
	VECTOR vGen, fGen, v_temp
	i = 0
	VEHICLE_MISSION vehicleMission = MISSION_ESCORT_LEFT
	IF IS_VEHICLE_DRIVEABLE(vehBike[0])
	AND IS_VEHICLE_DRIVEABLE(vehBike[1])
		// Backup attack
		IF NOT IS_PED_INJURED(pedRider[0])
			PLAY_PED_AMBIENT_SPEECH(pedRider[0], "GENERIC_FUCK_YOU")
			IF GET_SCRIPT_TASK_STATUS(pedRider[0], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedRider[0]) 
					TASK_DRIVE_BY(pedRider[0], PLAYER_PED_ID(), NULL, <<0,0,0>>, 200, 70, TRUE)
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedRider[1])
			IF GET_SCRIPT_TASK_STATUS(pedRider[1], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedRider[1]) 
					TASK_DRIVE_BY(pedRider[1], PLAYER_PED_ID(), NULL, <<0,0,0>>, 200, 70, TRUE)
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		REQUEST_MODEL(HEXER)
		IF HAS_MODEL_LOADED(HEXER)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				v_temp = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(v_temp, vGen, fGen, DEFAULT, DEFAULT, 75)
					REPEAT 2 i
						vGen.x += TO_FLOAT(i)
						vehBike[i] = CREATE_VEHICLE(HEXER, vGen, fGen.z)	
						pedRider[i] = CREATE_PED_INSIDE_VEHICLE(vehBike[i], PEDTYPE_CRIMINAL, modelThief)
						pedShooter[i] = CREATE_PED_INSIDE_VEHICLE(vehBike[i], PEDTYPE_CRIMINAL, modelThief, VS_FRONT_RIGHT)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedShooter[i], relGroupBaddies)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedRider[i], relGroupBaddies)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							TASK_VEHICLE_MISSION(pedRider[i], vehBike[i], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehicleMission, 40, DRIVINGMODE_AVOIDCARS, 5, 10)
						ELSE
							TASK_VEHICLE_MISSION_PED_TARGET(pedRider[i], vehBike[i], PLAYER_PED_ID(), vehicleMission, 40, DRIVINGMODE_AVOIDCARS, 5, 10)
						ENDIF
						GIVE_WEAPON_TO_PED(pedRider[i], WEAPONTYPE_SAWNOFFSHOTGUN, 999999, TRUE)
						SET_VEHICLE_FORWARD_SPEED(vehBike[i], GET_ENTITY_SPEED(PLAYER_PED_ID()))
						vehicleMission = MISSION_ESCORT_RIGHT
						GIVE_WEAPON_TO_PED(pedShooter[i], WEAPONTYPE_SAWNOFFSHOTGUN, 999999, TRUE)
						TASK_DRIVE_BY(pedShooter[i], PLAYER_PED_ID(), NULL, <<0,0,0>>, 200, 70)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedRider[i], KNOCKOFFVEHICLE_HARD)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedShooter[i], KNOCKOFFVEHICLE_HARD)
						SET_PED_ACCURACY(pedRider[i], 15)
						SET_PED_ACCURACY(pedShooter[i], 15)
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(PED_INDEX ped)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, TRUE)
	TASK_COMBAT_PED(ped, PLAYER_PED_ID()) // B*1415943 Don't task them to leave their vehicle, let the above flag do it
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER") #ENDIF
ENDPROC

PROC START_MUSIC_EVENTS()
	STRING musicEvent
	SWITCH iWhichWorldPoint
		CASE CHASE_THIEVES_CITY_1	
			musicEvent = "RE12A_OS"
		BREAK
		CASE CHASE_THIEVES_CITY_2
			musicEvent = "RE12B_OS"
		BREAK
		CASE CHASE_THIEVES_COUNTRY_1
			musicEvent = "RE13A_OS"
		BREAK
		CASE CHASE_THIEVES_COUNTRY_2
			musicEvent = "RE13B_OS"	
		BREAK
	ENDSWITCH
	WHILE NOT PREPARE_MUSIC_EVENT(musicEvent) 
	    WAIT(0)
    ENDWHILE
	TRIGGER_MUSIC_EVENT(musicEvent)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: TRIGGER_MUSIC_EVENT ", musicEvent) #ENDIF
ENDPROC

PROC LOST_ATTACK_PLAYER()
	IF bLostAttack = FALSE
		bLostAttack = TRUE
		fleeStage = fleeFLEE_ON_FOOT
		SWITCH iWhichWorldPoint
		
			CASE CHASE_THIEVES_CITY_1
			CASE CHASE_THIEVES_CITY_2
				IF IS_ENTITY_ALIVE(pedThief)
					IF CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_ATT", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_ATT") #ENDIF
					ENDIF
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedThief)
				ENDIF
				IF IS_ENTITY_ALIVE(pedDriver)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedDriver)
				ENDIF
			BREAK
			
			CASE CHASE_THIEVES_COUNTRY_1
				IF IS_ENTITY_ALIVE(pedThief)
					IF CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_ATT2", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_ATT2") #ENDIF
					ENDIF
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedThief)
				ENDIF
				IF IS_ENTITY_ALIVE(pedDriver)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedDriver)
				ENDIF
				IF IS_ENTITY_ALIVE(pedBackLeft)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedBackLeft)
				ENDIF
				IF IS_ENTITY_ALIVE(pedBackRight)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedBackRight)
				ENDIF
			BREAK
			
			CASE CHASE_THIEVES_COUNTRY_2
				IF IS_ENTITY_ALIVE(pedThief)
					IF CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_BACK", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_BACK") #ENDIF
					ENDIF
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedThief)
				ENDIF
				IF IS_ENTITY_ALIVE(pedDriver)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedDriver)
				ENDIF
				IF IS_ENTITY_ALIVE(pedBackLeft)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedBackLeft)
				ENDIF
				IF IS_ENTITY_ALIVE(pedBackRight)
					SET_PED_LEAVE_VEHICLE_AND_COMBAT_PLAYER(pedBackRight)
				ENDIF
				WHILE NOT LOADED_AND_CREATED_ATTACKING_BIKERS()
					WAIT(0)
				ENDWHILE
			BREAK
			
		ENDSWITCH
		
		SAFE_RELEASE_VEHICLE(vehGetawayCar)
		
		IF IS_PED_INJURED(pedThief)
		AND NOT IS_PED_INJURED(pedDriver)
			PLAY_PED_AMBIENT_SPEECH(pedDriver, "GENERIC_FUCK_YOU")
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started GENERIC_FUCK_YOU on pedDriver") #ENDIF
		ENDIF
		
		START_MUSIC_EVENTS()
		
	ENDIF
ENDPROC

PROC SET_DRIVEBY_TASKS()
	IF bLostAttack = FALSE
		IF IS_ENTITY_ALIVE(pedThief)
		AND IS_PED_IN_ANY_VEHICLE(pedThief)
		AND NOT IS_PED_IN_COMBAT(pedThief)
			TASK_DRIVE_BY(pedThief, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 60)
		ENDIF
		IF IS_ENTITY_ALIVE(pedDriver)
		AND IS_PED_IN_ANY_VEHICLE(pedDriver)
		AND GET_SCRIPT_TASK_STATUS(pedDriver, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
		AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedDriver) 
		AND NOT IS_PED_IN_COMBAT(pedDriver)
			TASK_DRIVE_BY(pedDriver, PLAYER_PED_ID(), NULL, <<0,0,0>>, 30, 60, TRUE)
			SET_DRIVE_TASK_CRUISE_SPEED(pedDriver, CRUISE_SPEED_FAST)
		ENDIF
		IF IS_ENTITY_ALIVE(pedBackLeft)
		AND IS_PED_IN_ANY_VEHICLE(pedBackLeft)
		AND GET_SCRIPT_TASK_STATUS(pedBackLeft, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
			TASK_DRIVE_BY(pedBackLeft, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 60)
		ENDIF
		IF IS_ENTITY_ALIVE(pedBackRight)
		AND IS_PED_IN_ANY_VEHICLE(pedBackRight)
		AND GET_SCRIPT_TASK_STATUS(pedBackRight, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
			TASK_DRIVE_BY(pedBackRight, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 60)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_GETAWAY_VEHICLE_TYRE_BURST()
    IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
        IF IS_VEHICLE_TYRE_BURST(vehGetawayCar, SC_WHEEL_CAR_FRONT_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehGetawayCar, SC_WHEEL_CAR_FRONT_RIGHT)
        OR IS_VEHICLE_TYRE_BURST(vehGetawayCar, SC_WHEEL_CAR_REAR_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehGetawayCar, SC_WHEEL_CAR_REAR_RIGHT)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: One vehGetawayCar tyre is burst") #ENDIF
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE    
ENDFUNC

FUNC BOOL HAS_GETAWAY_CAR_BEEN_BUMPED_ENOUGH()
    IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehGetawayCar, <<20,20,10>>)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehGetawayCar, PLAYER_PED_ID())
                iBumpedCounter++
				bHasBeenBumped = TRUE
                CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehGetawayCar)
            ENDIF
            IF iBumpedCounter > 25
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
	IF bHasBeenBumped = TRUE
		SET_DRIVEBY_TASKS()
	ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIEF_FLEE_ON_FOOT()	

	IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehGetawayCar)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - player is in vehGetawayCar") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(pedThief)
			IF fleeStage > fleeENTER_VEHICLE
				IF NOT IS_PED_IN_VEHICLE(pedThief, vehGetawayCar)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedThief not in vehGetawayCar") #ENDIF
					RETURN TRUE
				ENDIF
			ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedThief, PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - Player damaged pedThief") #ENDIF
				RETURN TRUE
			ENDIF
			IF IS_PED_BEING_JACKED(pedThief)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedThief being jacked out") #ENDIF
				RETURN TRUE
			ENDIF
			IF fleeStage = fleeENTER_VEHICLE
			AND NOT IS_PED_IN_VEHICLE(pedThief, vehGetawayCar, TRUE)
			AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedThief, vehGetawayCar, VS_FRONT_RIGHT)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedThief entrance into vehGetawayCar is blocked") #ENDIF
				RETURN TRUE	
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedThief isn't alive") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(pedDriver)
			IF IS_PED_BEING_JACKED(pedDriver)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedDriver being jacked out") #ENDIF
				RETURN TRUE
			ENDIF
			IF NOT IS_PED_IN_VEHICLE(pedDriver, vehGetawayCar)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedDriver isn't in vehGetawayCar") #ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - pedDriver isn't alive") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ANY_GETAWAY_VEHICLE_TYRE_BURST()
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - at least one vehGetawayCar tyre is burst") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_IN_WATER(vehGetawayCar)
		OR IS_VEHICLE_STUCK_TIMER_UP(vehGetawayCar, VEH_STUCK_HUNG_UP, 10000)
		OR IS_VEHICLE_STUCK_TIMER_UP(vehGetawayCar, VEH_STUCK_JAMMED, 10000)
		OR IS_VEHICLE_STUCK_TIMER_UP(vehGetawayCar, VEH_STUCK_ON_ROOF, 10000)
		OR IS_VEHICLE_STUCK_TIMER_UP(vehGetawayCar, VEH_STUCK_ON_SIDE, 10000 )
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - vehGetawayCar is stuck") #ENDIF
			RETURN TRUE
		ENDIF
		IF HAS_GETAWAY_CAR_BEEN_BUMPED_ENOUGH()
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - vehGetawayCar has been bumped enough") #ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT - vehGetawayCar isn't drivable") #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC HANDLE_THIEF_TAUNTING_PLAYER()
	IF bHasBeenBumped = TRUE
	AND (GET_GAME_TIMER() - iThiefTauntTimer) > 8000
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND IS_ENTITY_ALIVE(pedThief)
	AND IS_ENTITY_ALIVE(pedVictim)
	AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedThief, <<20,20,20>>, FALSE)
	AND NOT IS_ENTITY_AT_ENTITY(pedThief,pedVictim, <<30,30,30>>, FALSE)
	AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_taunt", CONV_PRIORITY_AMBIENT_HIGH)
		iThiefTauntTimer = GET_GAME_TIMER()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_taunt") #ENDIF
	ENDIF
ENDPROC

PROC SHOPKEEPER_USE_MOBILE_PHONE(BOOL b_use_phone)
	IF IS_ENTITY_ALIVE(pedVictim)
		IF b_use_phone = TRUE
			IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
				TASK_USE_MOBILE_PHONE(pedVictim, TRUE)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim starting phone call") #ENDIF
			ENDIF
		ELIF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
			TASK_USE_MOBILE_PHONE(pedVictim, FALSE)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim stopping phone call") #ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_SHOPKEEPER_ON_PHONE_BEHAVIOUR()
	IF IS_ENTITY_ALIVE(pedVictim)
	
		IF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
		AND iPhoneCallStage > 1
			IF DID_PLAYER_HURT_THIEF_IN_FRONT_OF_SHOPKEEPER()
			AND iPhoneCallStage < 6
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim saw player hurt thief or damage vehGetaway") #ENDIF
				iPhoneCallStage = 6
			ELIF bInterrupted = FALSE
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<8, 8, 8>>)
				AND IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(), 40)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player moved in front of pedVictim so interrupting phone call") #ENDIF
					TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 3500, SLF_WHILE_NOT_IN_FOV)
					iPhoneCallStage = 8
				ENDIF
			ENDIF
		ENDIF

		SWITCH iPhoneCallStage
		
			CASE 0
				IF bPlayedGottaGetConversation = FALSE
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<40, 40, 40>>, FALSE, FALSE)		
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_GET", CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_GET") #ENDIF
					bPlayedGottaGetConversation = TRUE
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
				AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
					PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_CURSE_MED")
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started ambient speech GENERIC_CURSE_MED") #ENDIF
					SHOPKEEPER_USE_MOBILE_PHONE(TRUE)
					iPhoneStartTimer = GET_GAME_TIMER()
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iPhoneStartTimer) > 1000
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20, 20, 20>>, FALSE, FALSE)													
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_PCA", CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_PCA") #ENDIF
					iPhoneStartTimer = GET_GAME_TIMER()
					iStoredPhonecallProgress = 2
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iPhoneStartTimer) > 5000
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20, 20, 20>>, FALSE, FALSE)														
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_PCB", CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_PCB") #ENDIF
					iPhoneStartTimer = GET_GAME_TIMER()
					iStoredPhonecallProgress = 3
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iPhoneStartTimer) > 6000
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20, 20, 20>>, FALSE, FALSE)														
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_PCC", CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_PCC") #ENDIF
					iPhoneStartTimer = GET_GAME_TIMER()
					iStoredPhonecallProgress = 4
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 4
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iPhoneStartTimer) > 6000
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<15, 15, 15>>, FALSE, FALSE)														
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_PCD", CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_PCD") #ENDIF
					iStoredPhonecallProgress = 5
					iPhoneCallStage++
				ENDIF
			BREAK 
			
			CASE 5
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Conversation complete") #ENDIF
					iPhoneCallStage = 100
				ENDIF
			BREAK
						
			CASE 6
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_SRP", CONV_PRIORITY_AMBIENT_HIGH) // "You still there? No, the robber's getting his ass kicked! I don't know... some random guy."
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_SRP") #ENDIF
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 7
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_GO", CONV_PRIORITY_AMBIENT_HIGH) // "Shit, I gotta go."
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_GO") #ENDIF
					SHOPKEEPER_USE_MOBILE_PHONE(FALSE)
					iPhoneCallStage = 100
				ENDIF
			BREAK
			
			CASE 8
				bInterrupted = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_INT", CONV_PRIORITY_AMBIENT_HIGH) // "Hey, did you see that? Oh my God..."
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_INT") #ENDIF
					TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
					iPhoneCallStage++
				ENDIF
			BREAK
			
			CASE 9
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_RES", CONV_PRIORITY_AMBIENT_HIGH) // "What? Oh, yeah, sorry, just this guy saw it too. Crazy."
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_RES") #ENDIF
					iPhoneStartTimer = GET_GAME_TIMER()
					iPhoneCallStage = iStoredPhonecallProgress
				ENDIF
			BREAK
			
			DEFAULT
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC SHOPKEEPER_FACE_PLAYER()
	IF NOT IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(), 20)
	AND NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
		TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
		TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim told to face player") #ENDIF
	ENDIF
ENDPROC

PROC UPDATE_SHOPKEEPER_RETURN_BEHAVIOUR()
	IF IS_ENTITY_ALIVE(pedVictim)
		SHOPKEEPER_FACE_PLAYER()

		SWITCH iPhoneCallReturnStage
			
			CASE 0
				IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<30, 30, 30>>, FALSE, FALSE)								
					AND PLAY_SINGLE_LINE_FROM_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_TK", "RECHA_TK_1", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_TK_1 at 30m") #ENDIF
						iPhoneCallReturnStage++
					ENDIF
				ELSE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<40, 40, 40>>, FALSE, FALSE)								
					AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_WT", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_WT at 40m") #ENDIF
						iPhoneCallReturnStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SHOPKEEPER_USE_MOBILE_PHONE(FALSE)
					iPhoneCallReturnStage++
				ENDIF
			BREAK
			
			DEFAULT
			BREAK
									
		ENDSWITCH
	ENDIF
ENDPROC

PROC UPDATE_INITIAL_SHOPKEEPER_BEHAVIOUR()
	IF IS_ENTITY_ALIVE(pedVictim)
	AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sStruggleFleeDict, sStruggleFleePed)
	AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) = FINISHED_TASK
		IF IS_BULLET_IN_AREA(vInput, 50, FALSE)
			SHOPKEEPER_USE_MOBILE_PHONE(FALSE)
			TASK_COWER(pedVictim)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim now cowering") #ENDIF
		ELSE
			UPDATE_SHOPKEEPER_ON_PHONE_BEHAVIOUR()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IF_THIEF_DEAD()
	IF IS_ENTITY_DEAD(pedThief)
	AND GET_ENTITY_SPEED(pedThief) < 1
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Thief is now dead") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_DEATH_SHOPKEEPER_BEHAVIOUR()
	IF bVictimIsNearBag = FALSE
	AND IS_ENTITY_ALIVE(pedVictim)
		IF bShopkeeperReacted2 = TRUE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			AND TIMERA() > 8000
			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<12, 12, 5>>, FALSE, FALSE)	
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					//TASK_COWER(NULL, 5000)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedVictim, seq)
				CLEAR_SEQUENCE_TASK(seq)
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "NEED_SOME_HELP", SPEECH_PARAMS_FORCE)
				SETTIMERA(0)
			ENDIF
		ELIF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
			IF TIMERA() > 1000
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<12, 12, 5>>, FALSE, FALSE)														
					IF CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_SRDP", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_SRDP") #ENDIF
						bShopkeeperReacted2 = TRUE
					ENDIF
				ENDIF
				SETTIMERA(0)
			ENDIF
		ELIF TIMERA() > 1000
			OPEN_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				//TASK_COWER(NULL, 5000)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedVictim, seq)
			CLEAR_SEQUENCE_TASK(seq)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<12, 12, 5>>, FALSE, FALSE)		
			AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_SRDN", CONV_PRIORITY_AMBIENT_HIGH)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_SRDN") #ENDIF
				bShopkeeperReacted2 = TRUE
			ENDIF
			SETTIMERA(0)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_THIEF_FLEE_BEHAVIOUR()

	SWITCH fleeStage
	
		CASE fleeROBBERY_TRIGGERED
			IF IS_ENTITY_ALIVE(pedThief)
			AND IS_ENTITY_ALIVE(pedDriver)
			AND IS_VEHICLE_DRIVEABLE(vehGetawayCar)
				IF iWhichWorldPoint = CHASE_THIEVES_CITY_1
				OR iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
					TASK_ENTER_VEHICLE(pedThief, vehGetawayCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT, ECF_USE_RIGHT_ENTRY|ECF_RESUME_IF_INTERRUPTED) 
				ELSE
					TASK_ENTER_VEHICLE(pedThief, vehGetawayCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT, ECF_RESUME_IF_INTERRUPTED)
				ENDIF
				SET_VEHICLE_ENGINE_ON(vehGetawayCar, TRUE, TRUE)
				FORCE_PED_MOTION_STATE(pedThief, MS_ON_FOOT_SPRINT)
				fleeStage = fleeENTER_VEHICLE
			ELSE
				fleeStage = fleeFLEE_ON_FOOT
			ENDIF
		BREAK
		
		CASE fleeENTER_VEHICLE
			IF IS_ENTITY_ALIVE(pedThief)
			AND IS_ENTITY_ALIVE(pedDriver)
			AND IS_VEHICLE_DRIVEABLE(vehGetawayCar)
				IF IS_PED_IN_VEHICLE(pedThief, vehGetawayCar)
					OPEN_SEQUENCE_TASK(seq)
						IF iWhichWorldPoint = CHASE_THIEVES_CITY_2
							//TASK_VEHICLE_TEMP_ACTION(NULL, vehGetawayCar, TEMPACT_BURNOUT, 2000)
						ENDIF
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehGetawayCar, PLAYER_PED_ID(), MISSION_FLEE, CRUISE_SPEED_NORMAL, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_GoOffRoadWhenAvoiding, -1, -1) // B*1416122
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDriver, seq)
					CLEAR_SEQUENCE_TASK(seq)
					fleeStage = fleeFLEE_IN_VEHICLE
				ENDIF
			ELSE
				fleeStage = fleeFLEE_ON_FOOT
			ENDIF
		BREAK
		
		CASE fleeFLEE_IN_VEHICLE
			IF bSpedUp = FALSE
				IF NOT IS_PED_INJURED(pedThief)
				AND NOT IS_PED_INJURED(pedDriver)
					IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
						IF NOT IS_ENTITY_AT_COORD(vehGetawayCar, vCreateGetawayCarPos, <<30,30, 5>>)
						AND IS_PED_IN_ANY_VEHICLE(pedDriver)
							SET_DRIVE_TASK_CRUISE_SPEED(pedDriver, 40)
							bSpedUp = TRUE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: fleeFLEE_ON_FOOT - IF NOT IS_VEHICLE_DRIVEABLE(vehGetawayCar)") #ENDIF
						fleeStage = fleeFLEE_ON_FOOT
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: fleeFLEE_ON_FOOT - Someone's injured") #ENDIF
					fleeStage = fleeFLEE_ON_FOOT
				ENDIF
			ENDIF
			
		BREAK
		
		CASE fleeFLEE_ON_FOOT
			IF iPlayerYellAtBikersTimer = -1
				iPlayerYellAtBikersTimer = GET_GAME_TIMER()
			ENDIF
			IF iPlayedPlayerYellAtBikersDialogue < 4
			AND (GET_GAME_TIMER() - iPlayerYellAtBikersTimer) > 10000
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_PED_INJURED(pedThief)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedThief) < 40
			AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_YELL", CONV_PRIORITY_AMBIENT_HIGH)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_YELL") #ENDIF
				iPlayedPlayerYellAtBikersDialogue++
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: iPlayedPlayerYellAtBikersDialogue = ", iPlayedPlayerYellAtBikersDialogue) #ENDIF
				iPlayerYellAtBikersTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF fleeStage < fleeFLEE_ON_FOOT
	AND SHOULD_THIEF_FLEE_ON_FOOT()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: SHOULD_THIEF_FLEE_ON_FOOT returned true") #ENDIF
		LOST_ATTACK_PLAYER()
	ENDIF
		
	 HANDLE_THIEF_TAUNTING_PLAYER()
	
ENDPROC

PROC MISSION_END()
	IF DOES_PICKUP_EXIST(pickupItem)
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupItem)) > 100
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player is out of range of pickup") #ENDIF
			MISSION_CLEANUP()
		ENDIF
	ELIF bPlayerBagCollected
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Passing mission - bPlayerBagCollected is TRUE") #ENDIF
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
		MISSION_PASSED()
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Cleaning up mission - Pickup doesn't exist") #ENDIF
		MISSION_CLEANUP()
	ENDIF
ENDPROC

// reaction for player attacking the shopkeeper
PROC PLAYER_ATTACKED_SHOPKEEPER()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player attacked skopkeeper") #ENDIF
	STOP_RETURN_ITEM_BLIP_FLASHING(iBlipFlashTimer)
	SAFE_REMOVE_BLIP(blipVictim)
	bPlayerScaredVictim = TRUE
	IF IS_ENTITY_ALIVE(pedVictim)
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, TRUE)
		TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
		IF CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_OUT", CONV_PRIORITY_AMBIENT_HIGH)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_OUT") #ENDIF
		ENDIF
	ENDIF
	IF bPlayerBagCollected = TRUE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: bPlayerBagCollected = TRUE so pass mission") #ENDIF
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
		MISSION_PASSED()
	ENDIF
ENDPROC

PROC CHECK_FOR_PLAYER_ATTACKING_SHOPKEEPER()
	IF bPlayerScaredVictim = FALSE
	AND IS_ENTITY_ALIVE(pedVictim)
	AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sStruggleFleeDict, sStruggleFleePed)
		IF HAS_PLAYER_AGGROED_PED(pedVictim, aggroReason, iLockOnTimer, iBitFieldDontCheck)
			//bAggroed = TRUE
			SWITCH aggroReason
				CASE EAggro_HostileOrEnemy
					PLAYER_ATTACKED_SHOPKEEPER()
				BREAK
				CASE EAggro_Attacked
					PLAYER_ATTACKED_SHOPKEEPER()
				BREAK	
			ENDSWITCH
		ELIF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedVictim), 20)
			PLAYER_ATTACKED_SHOPKEEPER()
		ENDIF
	ENDIF
ENDPROC

// interaction with stolen item
PROC HANDLE_PLAYER_PICKING_UP_ITEM()
	IF HAS_PICKUP_BEEN_COLLECTED(pickupItem)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player collected pickupItem") #ENDIF
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 250)
		bPlayerBagCollected = TRUE
		fStoreBagCollectDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInput)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: fStoreBagCollectDist = ", fStoreBagCollectDist) #ENDIF
		QUICK_INCREMENT_INT_STAT(RC_WALLETS_RECOVERED, 1)
		SAFE_REMOVE_BLIP(blipStolenItem)
		IF DOES_ENTITY_EXIST(pedVictim)
			IF NOT IS_PED_INJURED(pedVictim)
				IF bPlayerScaredVictim = TRUE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: bPlayerScaredVictim = TRUE so pass mission") #ENDIF
					SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
					MISSION_PASSED()
				ELSE
					REMOVE_ALL_BLIPS()
					blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
					START_RETURN_ITEM_BLIP_FLASHING(iBlipFlashTimer)
					ambStage = ambReturnItem
				ENDIF
			ELSE
				SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
				MISSION_PASSED()
			ENDIF
		ELSE
			SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
			MISSION_PASSED()
		ENDIF
	ELSE
		PRINT_LIMITED_RANDOM_EVENT_HELP(REHLP_RETURN_ITEM)
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDroppedItemPlacement) > 150
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Cleaning up mission - Player didn't collect pickupItem") #ENDIF
			MISSION_END()
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SHOPKEEPER_PICKING_UP_ITEM()
	IF bVictimPickedUpBag = TRUE
		IF bPlayedThanksCollectedOwnItemConversation = FALSE
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND PLAY_SINGLE_LINE_FROM_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_TK", "RECHA_TK_1", CONV_PRIORITY_AMBIENT_HIGH)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_TK_1") #ENDIF
			bPlayedThanksCollectedOwnItemConversation = TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(pedVictim)
		AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sPickupDict, "pickup_low")
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim back in idle after picking up his item") #ENDIF
			REMOVE_ALL_SHOCKING_EVENTS(FALSE)
			OPEN_SEQUENCE_TASK(seq)
				IF NOT IS_ENTITY_AT_COORD(pedVictim, vVictimRunToPos, <<1, 1, 2>>)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vVictimRunToPos, PEDMOVEBLENDRATIO_WALK)
				ENDIF
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedVictim, seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
			MISSION_PASSED()
		ENDIF
	ELSE
		IF bPlayerBagCollected = FALSE
		AND DOES_PICKUP_EXIST(pickupItem)
		AND IS_ENTITY_ALIVE(pedVictim)
		AND IS_ENTITY_AT_COORD(pedVictim, GET_PICKUP_COORDS(pickupItem), <<15, 15, 2>>)
		AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sStruggleFleeDict, sStruggleFleePed)
			REQUEST_ANIM_DICT(sPickupDict)
			VECTOR vMin = GET_PICKUP_COORDS(pickupItem) - <<15,15,15>>
			VECTOR vMax = GET_PICKUP_COORDS(pickupItem) + <<15,15,15>>
			IF HAS_ANIM_DICT_LOADED(sPickupDict)
			AND NOT IS_ANY_PED_SHOOTING_IN_AREA(vMin, vMax, FALSE)
				bVictimIsNearBag = TRUE
				IF NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE)
					OPEN_SEQUENCE_TASK(seq)
						IF NOT IS_ENTITY_AT_COORD(pedVictim, GET_PICKUP_COORDS(pickupItem), <<1, 1, 2>>)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PICKUP_COORDS(pickupItem), PEDMOVEBLENDRATIO_RUN)
						ENDIF
						TASK_TURN_PED_TO_FACE_COORD(NULL, GET_PICKUP_COORDS(pickupItem))
						TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low")				
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedVictim, seq)
					CLEAR_SEQUENCE_TASK(seq)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Playing sequence on pedVictim to pick up his wallet") #ENDIF
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(pedVictim, sPickupDict, "pickup_low")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedVictim, sPickupDict, "pickup_low") > 0.5
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim picked up his own item") #ENDIF
					SAFE_REMOVE_PICKUP(pickupItem)
					SET_PED_MONEY(pedVictim, iItemValue)
					bVictimPickedUpBag = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_FADES_IF_CUT_IS_SKIPPED()
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		SAFE_FADE_SCREEN_OUT_TO_BLACK()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GIVE_VICTIM_TASK_AFTER_CUTSCENE()
	IF IS_ENTITY_ALIVE(pedVictim)
	AND NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE)
		SAFE_TELEPORT_ENTITY(pedVictim, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,2,1>>), fVictimStartCutsceneHeading, TRUE) // B*1524627, 1519696, 1528469 Ensure ped is on ground
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
		OPEN_SEQUENCE_TASK(seq)
			IF IS_VECTOR_ZERO(vVictimCarPos)
			OR NOT IS_ENTITY_ALIVE(vehVictimCar)
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim doing WORLD_HUMAN_STAND_MOBILE") #ENDIF
			ELSE
				TASK_ENTER_VEHICLE(NULL, vehVictimCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
				TASK_VEHICLE_DRIVE_WANDER(NULL, vehVictimCar, 20.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedVictim driving off in his car") #ENDIF
			ENDIF
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedVictim, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

PROC RETURN_BAG_SYNCED_SCENE()
	VECTOR v_scene_pos
	VECTOR v_scene_rot
	STRING sThx = "RECHA_THX", sThxLine = "RECHA_THX_1"

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(pedVictim)
		
		SWITCH iHandOver
		
			CASE 0 // Wait for player vehicle to stop
				IF NOT STREAMVOL_IS_VALID(streamVolumeChaseThieves)
					streamVolumeChaseThieves = STREAMVOL_CREATE_SPHERE(GET_ENTITY_COORDS(pedVictim), 200.0, FLAG_MAPDATA)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Created streamVolumeChaseThieves") #ENDIF
				ENDIF
				REQUEST_ANIM_DICT(sHandoverDict)
				IF IS_THIS_RANDOM_EVENT_HELP_BEING_DISPLAYED(REHLP_RETURN_ITEM)
					CLEAR_HELP()
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Cleared help text REHLP_RETURN_ITEM") #ENDIF
				ENDIF
				IF NOT IS_ENTITY_ALIVE(vehPlayerCar)
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehPlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					SET_LAST_DRIVEN_VEHICLE(vehPlayerCar)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Grabbed vehPlayerCar") #ENDIF
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player in vehicle at cutscene trigger so halting vehicle") #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player at cutscene trigger and car has stopped so making player exit vehicle") #ENDIF
					SHOPKEEPER_USE_MOBILE_PHONE(FALSE) // Just call this here regardless to get the victim to put away his phone
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_CLEAR_LOOK_AT(pedVictim)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					iHandoverCutsceneSafetyTimer = -1
					iHandOver++
				ENDIF
			BREAK

			CASE 1 // Wait for player to exit vehicle
				REQUEST_ANIM_DICT(sHandoverDict)
				REQUEST_MODEL(Prop_LD_Wallet_01) // B*1521850 Use closed walley version for the cutscene
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND HAS_ANIM_DICT_LOADED(sHandoverDict)
				AND HAS_MODEL_LOADED(Prop_LD_Wallet_01)
					IF iHandoverCutsceneSafetyTimer = -1
						iHandoverCutsceneSafetyTimer = GET_GAME_TIMER()
					ENDIF
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
					OR (GET_GAME_TIMER() - iHandoverCutsceneSafetyTimer) > 2000 // Wait a maximum of 2 seconds for the shopkeeper to put his phone away, to prevent B*1467883
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Ok to begin cutscene") #ENDIF
						RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
						REMOVE_ALL_SHOCKING_EVENTS(FALSE)
						REMOVE_SHOCKING_EVENT(iShockingEvent)
					
						objStolenItem = CREATE_OBJECT(Prop_LD_Wallet_01, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						ATTACH_ENTITY_TO_ENTITY(objStolenItem, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)

						fVictimStartCutsceneHeading = GET_ENTITY_HEADING(pedVictim)
						v_scene_pos = vVictimRunToPos // B*1915903 use the end pos of the victim rather than his current pos, in case he gets moved near a wall
						v_scene_pos.z += 50.0 // B*1930398 to ensure we get the proper ground z
						GET_GROUND_Z_FOR_3D_COORD(v_scene_pos, v_scene_pos.z)
						v_scene_rot = GET_ENTITY_ROTATION(pedVictim)
						sceneId = CREATE_SYNCHRONIZED_SCENE(v_scene_pos, v_scene_rot)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneId, FALSE)
						CamIDPickingUpBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
						PLAY_SYNCHRONIZED_CAM_ANIM(CamIDPickingUpBag, sceneId, sHandoverCam, sHandoverDict)
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, sHandoverDict, sHandoverPlayer, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
						CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
						TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, sHandoverDict, sHandoverPed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)

						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
						
						// B*1777021 - Move these bins outta the way
						OBJECT_INDEX oBin
						oBin = GET_CLOSEST_OBJECT_OF_TYPE(<<335.98, -839.47, 28.31>>, 10, prop_rub_binbag_03b)
						IF DOES_ENTITY_EXIST(oBin)
							SET_ENTITY_COORDS(oBin, <<317.94, -824.33, 28.27>>)
						ENDIF
						SAFE_RELEASE_OBJECT(oBin, FALSE)
						oBin = GET_CLOSEST_OBJECT_OF_TYPE(<<336.86, -840.08, 28.27>>, 10, prop_rub_binbag_05)
						IF DOES_ENTITY_EXIST(oBin)
							SET_ENTITY_COORDS(oBin, <<315.63, -826.01, 28.27>>)
						ENDIF
						SAFE_RELEASE_OBJECT(oBin, FALSE)
						
						CLEAR_AREA_OF_OBJECTS(v_scene_pos, 20, CLEAROBJ_FLAG_FORCE)
						CLEAR_AREA_OF_PROJECTILES(v_scene_pos, 20)
						CLEAR_AREA_OF_VEHICLES(v_scene_pos, 5)
						CLEAR_AREA(v_scene_pos, 2, TRUE)
						CLEAR_AREA(GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, v_scene_pos, v_scene_rot), 2, TRUE)
						
						SET_CAM_ACTIVE(CamIDPickingUpBag, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
								PLAY_SINGLE_LINE_FROM_CONVERSATION(dialogueStruct, "REAR1AU", "REAR1_REM", "REAR1_REM_1", CONV_PRIORITY_AMBIENT_HIGH)
							BREAK
							CASE CHAR_FRANKLIN
								ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
								PLAY_SINGLE_LINE_FROM_CONVERSATION(dialogueStruct, "REAR1AU", "REAR1_REF", "REAR1_REF_1", CONV_PRIORITY_AMBIENT_HIGH)
							BREAK
							CASE CHAR_TREVOR
								ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
								PLAY_SINGLE_LINE_FROM_CONVERSATION(dialogueStruct, "REAR1AU", "REAR1_RET", "REAR1_RET_1", CONV_PRIORITY_AMBIENT_HIGH)
							BREAK
						ENDSWITCH
						iHandOver++
					ELSE // Player is locked into the cutscene but we can't start it yet
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Not yet ok to begin cutscene - waiting for shopkeeper to put away phone or safety timer to end") #ENDIF
					ENDIF
				ELSE // Player is locked into the cutscene but we can't start it yet
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Not yet ok to begin cutscene - waiting for player to exit or current conversation to end or anim dict to load") #ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF bDone1stPersonCameraFlash = FALSE
				AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.96
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					bDone1stPersonCameraFlash = TRUE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Doing 1st person camera flash") #ENDIF
				ENDIF
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) = 1)
				OR DO_FADES_IF_CUT_IS_SKIPPED()
				OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("WalkInterruptable"))
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Finishing synched scene...") #ENDIF
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_ALL_CAMS()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					IF IS_ENTITY_ALIVE(pedVictim) // Recheck due to potential wait while fading
						GIVE_VICTIM_TASK_AFTER_CUTSCENE()
						SET_PED_MONEY(pedVictim, (iItemValue-(iItemValue/10)))
					ENDIF
					SAFE_DELETE_OBJECT(objStolenItem)
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Wallet_01)
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF
					RC_END_CUTSCENE_MODE(TRUE, FALSE) // B*1519841 Don't re-equip weapon
					DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, FLOOR(iItemValue*0.9)) // B*1417752
					QUICK_INCREMENT_INT_STAT(RC_WALLETS_RETURNED, 1)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Finished synched scene") #ENDIF
					iHandOver++
				ELSE
					SWITCH iAlreadyComplete
						CASE 0
							sThxLine = "RECHA_THX_1"
						BREAK
						CASE 1
							sThxLine = "RECHA_THX_2"
						BREAK
						CASE 2
							sThxLine = "RECHA_THX_3"
						BREAK
						CASE 3
							sThxLine = "RECHA_THX_4"
						BREAK
					ENDSWITCH
					IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Detach"))
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objStolenItem, PLAYER_PED_ID())
						AND bPlayedVictimDialogue = FALSE
						AND PLAY_SINGLE_LINE_FROM_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", sThx, sThxLine, CONV_PRIORITY_AMBIENT_HIGH)
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Victim dialogue tried to trigger") #ENDIF
							DETACH_ENTITY(objStolenItem, FALSE)
							ATTACH_ENTITY_TO_ENTITY(objStolenItem, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
							bPlayedVictimDialogue = TRUE
						ENDIF
					ELSE
						IF bPlayedVictimDialogue = FALSE
						AND PLAY_SINGLE_LINE_FROM_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", sThx, sThxLine, CONV_PRIORITY_AMBIENT_HIGH)
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Victim dialogue tried to trigger") #ENDIF
							bPlayedVictimDialogue = TRUE
						ENDIF
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.9 // B*1524634 Ensure guy walks off before the end of the cutscene
							GIVE_VICTIM_TASK_AFTER_CUTSCENE()
						ENDIF
					ENDIF
				ENDIF	
			BREAK
			
			CASE 3
				IF bPlayedPlayerWouldKillBikersDialogue = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_RESP", CONV_PRIORITY_AMBIENT_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_RESP") #ENDIF
						bPlayedPlayerWouldKillBikersDialogue = TRUE
					ENDIF
				ELSE
					SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
					MISSION_PASSED()
				ENDIF
			BREAK
		ENDSWITCH
			
	ENDIF
ENDPROC

FUNC INT GET_COMPLETE_COUNT()
	INT iReturn
	IF IS_RANDOM_EVENT_COMPLETE(RE_CHASETHIEVES, CHASE_THIEVES_CITY_1)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_CITY_1 is complete") #ENDIF
		iReturn++
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_CITY_1 is not complete") #ENDIF
	ENDIF
	IF IS_RANDOM_EVENT_COMPLETE(RE_CHASETHIEVES, CHASE_THIEVES_CITY_2)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_CITY_2 is complete") #ENDIF
		iReturn++
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_CITY_2 is not complete") #ENDIF
	ENDIF
	IF IS_RANDOM_EVENT_COMPLETE(RE_CHASETHIEVES, CHASE_THIEVES_COUNTRY_1)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_COUNTRY_1 is complete") #ENDIF
		iReturn++
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_COUNTRY_1 is not complete") #ENDIF
	ENDIF
	IF IS_RANDOM_EVENT_COMPLETE(RE_CHASETHIEVES, CHASE_THIEVES_COUNTRY_2)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_COUNTRY_2 is complete") #ENDIF
		iReturn++
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: CHASE_THIEVES_COUNTRY_2 is not complete") #ENDIF
	ENDIF
	RETURN iReturn
ENDFUNC

PROC RUN_DEBUG()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player pressed S") #ENDIF
			MISSION_PASSED()
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player pressed F") #ENDIF
			MISSION_CLEANUP()
		ENDIF
	#ENDIF
ENDPROC

PROC UPDATE_BAD_GUY_BLIPS()
	IF bLostAttack = TRUE
		UPDATE_AI_PED_BLIP(pedThief, pedThiefBlips)
		UPDATE_AI_PED_BLIP(pedDriver, pedDriverBlips)
		UPDATE_AI_PED_BLIP(pedBackLeft, pedBackLeftBlips)
		UPDATE_AI_PED_BLIP(pedBackRight, pedBackRightBlips)
		UPDATE_AI_PED_BLIP(pedRider[0], pedRiderBlips[0])
		UPDATE_AI_PED_BLIP(pedRider[1], pedRiderBlips[1])
		UPDATE_AI_PED_BLIP(pedShooter[0], pedShooterBlips[0])
		UPDATE_AI_PED_BLIP(pedShooter[1], pedShooterBlips[1])
	ENDIF
ENDPROC

PROC UPDATE_FLASHING_BLIP()
	IF bLostAttack = TRUE
	OR bLostSpeedUp = TRUE
	OR bHasBeenBumped = TRUE
		IF DOES_BLIP_EXIST(blipThief)
			SHOW_HEIGHT_ON_BLIP(blipThief, TRUE) // B*1461594 Only show blip height after interacting with the robbery
			IF GET_BLIP_COLOUR(blipThief) = BLIP_COLOUR_RED//ENUM_TO_INT(HUD_COLOUR_FRIENDLY) 
			ELSE
				SET_BLIP_AS_FRIENDLY(blipThief, FALSE)
				SET_BLIP_COLOUR(blipThief, BLIP_COLOUR_RED)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipThief)
			SHOW_HEIGHT_ON_BLIP(blipThief, FALSE) // B*1461594 Only show blip height after interacting with the robbery
			FLASH_RANDOM_EVENT_DECISION_BLIP(blipThief, iFlashTimestamp)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehGetawayCar, <<25,25,25>>, FALSE, FALSE)
			AND GET_ENTITY_SPEED(vehGetawayCar) > 5
				IF GET_GAME_TIMER() > iTimeInRange+5000
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: bLostSpeedUp Player was within 25m of Thief for 5s") #ENDIF
					bLostSpeedUp = TRUE
				ENDIF
			ELSE
				iTimeInRange = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_TARGET_IN_RANGE()
	IF NOT IS_ENTITY_DEAD(vehGetawayCar)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehGetawayCar, <<fFailDistance, fFailDistance, fFailDistance>>, FALSE, FALSE)
		OR NOT IS_ENTITY_OCCLUDED(vehGetawayCar)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedThief)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedThief, <<fFailDistance, fFailDistance, fFailDistance>>, FALSE, FALSE)
		OR NOT IS_ENTITY_OCCLUDED(pedThief)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ROBBERY_BEEN_TRIGGERED()
	VECTOR v_angled_point_1, v_angled_point_2
	FLOAT v_angled_width
	IF iWhichWorldPoint = CHASE_THIEVES_CITY_1
		v_angled_point_1 = <<18.221767,-141.860138,65.033386>>
		v_angled_point_2 = <<212.932556,-210.491470,46.548691>>
		v_angled_width = 167.500000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWorldPoint1, << 20, 20, 20 >>)
		OR IS_BULLET_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width, WEAPONTYPE_RPG)
		OR IS_PED_INJURED(pedThief)
			RETURN TRUE
		ENDIF
	ELIF iWhichWorldPoint = CHASE_THIEVES_CITY_2
		v_angled_point_1 = <<445.474030,-851.122559,26.598591>>
		v_angled_point_2 = <<211.775604,-850.927368,39.314484>>
		v_angled_width = 100.000000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWorldPoint2, << 20, 20, 20 >>)
		OR IS_BULLET_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width, WEAPONTYPE_RPG)
		OR IS_PED_INJURED(pedThief)
			RETURN TRUE
		ENDIF
	ELIF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_1
		v_angled_point_1 = <<1665.565186,4932.070800,51.114944>>
		v_angled_point_2 = <<1695.932129,4731.497559,34.675159>>
		v_angled_width = 96.750000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vVictimRunToPos, << 20, 20, 20 >>)
		OR IS_BULLET_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width, WEAPONTYPE_RPG)
		OR IS_PED_INJURED(pedThief)
			RETURN TRUE
		ENDIF
	ELIF iWhichWorldPoint = CHASE_THIEVES_COUNTRY_2
		v_angled_point_1 = <<1322.305908,2690.290039,46.556957>>
		v_angled_point_2 = <<1092.250488,2690.501221,31.295490>>
		v_angled_width = 197.250000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vVictimRunToPos, << 20, 20, 20 >>)
		OR IS_BULLET_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(v_angled_point_1, v_angled_point_2, v_angled_width, WEAPONTYPE_RPG)
		OR IS_PED_INJURED(pedThief)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ALIVE_PEDS_FLEE_PLAYER()

	IF IS_ENTITY_ALIVE(pedThief)
		TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 1000, -1)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedThief fleeing player") #ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(pedDriver)
		SEQUENCE_INDEX seq_flee
		OPEN_SEQUENCE_TASK(seq_flee)
			IF IS_PED_IN_ANY_VEHICLE(pedDriver)
				TASK_LEAVE_ANY_VEHICLE(NULL)
			ENDIF
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
		CLOSE_SEQUENCE_TASK(seq_flee)
		TASK_PERFORM_SEQUENCE(pedDriver, seq_flee)
		CLEAR_SEQUENCE_TASK(seq_flee)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedDriver fleeing player") #ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(pedBackLeft)
		SEQUENCE_INDEX seq_flee
		OPEN_SEQUENCE_TASK(seq_flee)
			IF IS_PED_IN_ANY_VEHICLE(pedBackLeft)
				TASK_LEAVE_ANY_VEHICLE(NULL)
			ENDIF
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
		CLOSE_SEQUENCE_TASK(seq_flee)
		TASK_PERFORM_SEQUENCE(pedBackLeft, seq_flee)
		CLEAR_SEQUENCE_TASK(seq_flee)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedBackLeft fleeing player") #ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(pedBackRight)
		SEQUENCE_INDEX seq_flee
		OPEN_SEQUENCE_TASK(seq_flee)
			IF IS_PED_IN_ANY_VEHICLE(pedBackRight)
				TASK_LEAVE_ANY_VEHICLE(NULL)
			ENDIF
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
		CLOSE_SEQUENCE_TASK(seq_flee)
		TASK_PERFORM_SEQUENCE(pedBackRight, seq_flee)
		CLEAR_SEQUENCE_TASK(seq_flee)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: pedBackRight fleeing player") #ENDIF
	ENDIF

ENDPROC

PROC UPDATE_ROBBERY()
	IF bThiefHasFled = TRUE
		IF IS_ENTITY_ALIVE(pedVictim)
		AND IS_ENTITY_PLAYING_ANIM(pedVictim, sStruggleFleeDict, sStruggleFleePed)
			SET_ENTITY_ANIM_SPEED(pedVictim, sStruggleFleeDict, sStruggleFleePed, 1.4)
		ENDIF
		IF IS_ENTITY_ALIVE(pedThief)
			IF IS_ENTITY_PLAYING_ANIM(pedThief, sStruggleFleeDict, sStruggleFleeThief)
				SET_ENTITY_ANIM_SPEED(pedThief, sStruggleFleeDict, sStruggleFleeThief, 1.4)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehGetawayCar)
			AND NOT SHOULD_THIEF_FLEE_ON_FOOT()
				IF IS_ENTITY_PLAYING_ANIM(pedThief, sStruggleFleeDict, sStruggleFleeThief)
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedThief, sStruggleFleeDict, sStruggleFleeThief) > 0.75
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Updating state to ambChaseThieves - vehGetawayCar is driveable and SHOULD_THIEF_FLEE_ON_FOOT returned TRUE") #ENDIF
					ambStage = ambChaseThieves
					fleeStage = fleeROBBERY_TRIGGERED
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Updating state to ambChaseThieves - vehGetawayCar isn't driveable or SHOULD_THIEF_FLEE_ON_FOOT returned FALSE") #ENDIF
				ALIVE_PEDS_FLEE_PLAYER()
				ambStage = ambChaseThieves
				fleeStage = fleeFLEE_ON_FOOT
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Updating state to ambChaseThieves - pedThief isn't alive") #ENDIF
			ALIVE_PEDS_FLEE_PLAYER()
			ambStage = ambChaseThieves
			fleeStage = fleeFLEE_ON_FOOT
		ENDIF
	ELSE
		KILL_ANY_CONVERSATION() // B*1521139 - Ensure any other conversation is killed since the CREATE_CONVERSATION below has to return true to kick off the robbery
		IF IS_ENTITY_ALIVE(pedVictim)
			IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_STO", CONV_PRIORITY_AMBIENT_HIGH))
			OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedVictim) < 20 //Failsafe to ensure B*1521139 doesn't happen again if the conversation being killed doesn't work for whatever reason
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_STO") #ENDIF
				OPEN_SEQUENCE_TASK(seq)
					TASK_PLAY_ANIM(NULL, sStruggleFleeDict, sStruggleFleePed, DEFAULT, SLOW_BLEND_OUT, 8200)//8800)
					IF IS_ENTITY_ALIVE(pedThief)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<10,10,10>>)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						ELSE
							TASK_LOOK_AT_ENTITY(NULL, vehGetawayCar, -1, SLF_WHILE_NOT_IN_FOV)
						ENDIF
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vVictimRunToPos, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2, ENAV_SUPPRESS_EXACT_STOP)	
						TASK_PLAY_ANIM(NULL, sComplainDict, sComplainAnim)
					ENDIF
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedVictim, seq)
				CLEAR_SEQUENCE_TASK(seq)
				IF IS_ENTITY_ALIVE(pedThief)
					TASK_PLAY_ANIM(pedThief, sStruggleFleeDict, sStruggleFleeThief, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
				ENDIF
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: bThiefHasFled set to TRUE") #ENDIF
				bThiefHasFled = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CHASE()
	IF NOT IS_TARGET_IN_RANGE() // Check for losing thief
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Failing mission - vehGetawayCar too far away") #ENDIF
		SAFE_DELETE_PED(pedThief)
		SAFE_DELETE_PED(pedDriver)
		SAFE_DELETE_VEHICLE(vehGetawayCar)
		MISSION_CLEANUP()
	ELIF IF_THIEF_DEAD() // Check for thief being killed and dropping the money
		vDroppedItemPlacement = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(pedThief, FALSE))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		pickupItem = CREATE_PICKUP(PICKUP_MONEY_WALLET, vDroppedItemPlacement, iPlacementFlags, iItemValue, TRUE, modelStolenItem)
		REMOVE_ALL_BLIPS()
		blipStolenItem = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupItem)
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		CHASE_THIEVES_START_AUDIO_SCENE(FALSE)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Created pickupItem") #ENDIF
		ambStage = ambPickUpItem
	ELSE
		IF IS_ENTITY_ALIVE(pedThief)		
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedThief) // B*1499596 - No hint cam when on foot
		ENDIF
		UPDATE_THIEF_FLEE_BEHAVIOUR()
		IF bShoutedForHelp = FALSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_HLP", CONV_PRIORITY_AMBIENT_HIGH)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_HLP") #ENDIF
				SETTIMERA(0)
				bShoutedForHelp = TRUE
			ENDIF
		ELSE
			UPDATE_CHASE_BLIP(blipThief, pedThief, fFailDistance)
			IF bPlayedPlayerSpottedBikersDialogue = FALSE
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_ENTITY_ALIVE(vehGetawayCar)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehGetawayCar) < 30
			AND CREATE_CONVERSATION(ChaseThievesSpeechStruct, "RECHAAU", "RECHA_SPOT", CONV_PRIORITY_AMBIENT_HIGH)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Started conversation RECHA_SPOT") #ENDIF
				bPlayedPlayerSpottedBikersDialogue = TRUE
			ENDIF
		ENDIF
		UPDATE_INITIAL_SHOPKEEPER_BEHAVIOUR()
	ENDIF
ENDPROC

PROC HANDLE_PLAYER_RETURNING_ITEM()
	IF IS_ENTITY_ALIVE(pedVictim)
		FLASH_RANDOM_EVENT_RETURN_ITEM_BLIP(blipVictim, iBlipFlashTimer)
		UPDATE_SHOPKEEPER_RETURN_BEHAVIOUR()
		IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < iItemValue
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Passing mission - player has less money than picked up") #ENDIF
			SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
			MISSION_PASSED()
		ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20, 20, 20>>)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Player close enough to progress to outro cutscene") #ENDIF
			RELEASE_SURVIVING_LOST_PEDS()
			ambStage = ambThankYou
		ELIF bPlayerIsCloseWithItem = TRUE
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<50, 50, 20>>)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Passing mission - player returned with item then left again") #ENDIF
				SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
				MISSION_PASSED()
			ENDIF
		ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<30, 30, 20>>)
			bPlayerIsCloseWithItem = TRUE
		ELIF fStoreBagCollectDist > -1
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInput) > (fStoreBagCollectDist+120)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Passing mission - player collected item and didn't return it") #ENDIF
				SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
				MISSION_PASSED()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT (coords_struct in_coords)

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Forced cleanup occurred, so cleaning up mission") #ENDIF
	IF bPlayerBagCollected
		bForcedCleanup = TRUE
		MISSION_PASSED()
	ELSE
		MISSION_CLEANUP()
	ENDIF
ENDIF

vInput = in_coords.vec_coord[0]
#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Launched at ", vInput) #ENDIF

GET_WORLD_POINT()

//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_CHASETHIEVES, iWhichWorldPoint)
	LAUNCH_RANDOM_EVENT(RE_CHASETHIEVES)
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

iAlreadyComplete = GET_COMPLETE_COUNT()
#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: iAlreadyComplete = ", iAlreadyComplete) #ENDIF

INITIALISE_VARIABLES()

WHILE TRUE
	WAIT(0)
	RUN_DEBUG()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR ambStage > ambWaitForTrigger
		SWITCH ambStage
		
			CASE ambLoadingAssets
				UPDATE_LOAD_QUEUE_LIGHT(sLoadQueue)
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					MISSION_CLEANUP()
				ELIF LOADED_ASSETS()
					CREATE_INITIAL_SCENE()
					ambStage = ambWaitForTrigger
				ENDIF
			BREAK

			CASE ambWaitForTrigger
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					MISSION_CLEANUP()
				ELIF HAS_ROBBERY_BEEN_TRIGGERED()
					SET_RANDOM_EVENT_ACTIVE()
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Robbery triggered at iWhichWorldPoint ", iWhichWorldPoint) #ENDIF
					IF NOT IS_PED_INJURED(pedThief)
						blipThief = CREATE_AMBIENT_BLIP_FOR_PED(pedThief, TRUE)
						SET_BLIP_COLOUR(blipThief, BLIP_COLOUR_RED)
					ENDIF
					CHASE_THIEVES_START_AUDIO_SCENE(TRUE)
					SET_WANTED_LEVEL_MULTIPLIER(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					SET_MAX_WANTED_LEVEL(0)
					bSetNoCopsForRE = TRUE
					iThiefTauntTimer = GET_GAME_TIMER()
					ambStage = ambAction
				ENDIF
			BREAK
					
			CASE ambAction
				UPDATE_FLASHING_BLIP()
				UPDATE_ROBBERY()
			BREAK

			CASE ambChaseThieves
				UPDATE_FLASHING_BLIP()
				UPDATE_CHASE()
			BREAK

			CASE ambPickUpItem
				HANDLE_SHOPKEEPER_PICKING_UP_ITEM()
				HANDLE_PLAYER_PICKING_UP_ITEM()
				UPDATE_DEATH_SHOPKEEPER_BEHAVIOUR()
			BREAK

			CASE ambReturnItem
				HANDLE_PLAYER_RETURNING_ITEM()
			BREAK
					
			CASE ambThankYou
				RETURN_BAG_SYNCED_SCENE()
			BREAK
								
		ENDSWITCH		
		
		IF ambStage > ambLoadingAssets
			UPDATE_BAD_GUY_BLIPS()
			CHECK_FOR_PLAYER_ATTACKING_SHOPKEEPER()
			IF IS_ENTITY_DEAD(pedVictim)
				IF DOES_PICKUP_OBJECT_EXIST(pickupItem)
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupItem)) > 100
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Cleaning up mission - pedVictim is dead and player didn't collect the pickup") #ENDIF
						MISSION_CLEANUP()
					ENDIF
				ELIF bPlayerBagCollected = TRUE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Passing mission - pedVictim is dead and bPlayerBagCollected is TRUE") #ENDIF
					SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
					MISSION_PASSED()
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: Cleaning up mission - pedVictim is dead and bPlayerBagCollected is FALSE") #ENDIF
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupBaddies, RELGROUPHASH_PLAYER)
					IF IS_ENTITY_ALIVE(pedThief)
						SET_PED_MONEY(pedThief, iItemValue)
					ENDIF
					MISSION_CLEANUP()
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_AMBIENT, "CHASE THIEVES: NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()") #ENDIF
		MISSION_CLEANUP()
	ENDIF
	
ENDWHILE

ENDSCRIPT

