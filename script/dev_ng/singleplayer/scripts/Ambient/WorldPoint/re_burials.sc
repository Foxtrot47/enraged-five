//														Random Events: Burials - 14
//																		Vicki

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "cellphone_public.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "script_maths.sch"
USING "commands_debug.sch"
USING "LineActivation.sch"
USING "AggroSupport.sch"
USING "dialogue_public.sch"
USING "script_blips.sch"
USING "Ambient_Common.sch"
USING "Dialogue_public.sch" 
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "altruist_cult.sch"
USING "buddy_head_track_public.sch"
USING "random_events_public.sch"
USING "comms_control_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventStage
	eS_SET_UP_SCENE,
	eS_EVENT_STAGES,		// event plays out with guy & girls speech and anims
	eS_INTERVENTION_STAGES,
	eS_PLAYER_CAN_HELP_VIC,
	eS_DRIVE_GIRL_HOME,
	eS_DROP_OFF_GIRL_IN_VEH,
	eS_DROP_OFF_GIRL_ON_FOOT,
	eS_GUN_FIGHT,
	eS_STAT_OUTCOME,
	AT_CULT
ENDENUM
eventStage thisEventStage = eS_SET_UP_SCENE

ENUM eventSubStages
	eSS_INITIAL_BURIAL_SEQ,
	eSS_BURIAL_STAGE2
ENDENUM
eventSubStages thisSubStage = eSS_INITIAL_BURIAL_SEQ

ENUM playerInterferenceStage
	interfere_NONE,
	interfere_CLOSE_BY,
	interfere_BUMP_INTO,
	interfere_ATTACK_CAR,
	//interfere_ENTER_CAR,
	interfere_HARM_VICTIM,
	interfere_DRIVEN_AT,
	interfere_INSTANT_KILL
ENDENUM
playerInterferenceStage interferenceType = interfere_NONE

BOOL bAssetsLoaded

PED_INDEX pedVictim
PED_INDEX pedKidnapper[2]
PED_INDEX pedMafia

VEHICLE_INDEX vehKidnappersVeh
VEHICLE_INDEX vehMafia
OBJECT_INDEX objSpade
OBJECT_INDEX objRope[2]

INT i //counter

SEQUENCE_INDEX Seq

BOOL bVariablesInitialised

BOOL bHurtVicRemark

BOOL bTurningToPlayer0
BOOL bLookingAtPlayer0
BOOL bLookingAtPlayer1

INT iStandoffSpeech = 0
INT iChatInVehicle
INT iChatTimer

BOOL bCaptorsKilledSetUp

BOOL bInterfered
 
BOOL bWarnedBeforeShooting
 
CAMERA_INDEX camFreeingPed

VECTOR vCreateVictim
VECTOR vCreateKidnapper[2]
VECTOR vKidnappersVehicle
VECTOR vDefenceArea1 
VECTOR vDefenceArea2

FLOAT fCreateVictim
FLOAT fCreateKidnapper[2]
FLOAT fKidnappersVehicle

MODEL_NAMES modelVictim
MODEL_NAMES modelKidnapper0
MODEL_NAMES modelKidnapper1
MODEL_NAMES modelKidnappersVehicle

INT iHostageSpeechOutburstTimer

VECTOR vWorldPoint = << 154.92, 6841.12, 19.14 >> 
CONST_INT iWorldPoint1 		1

//INT iTurnStage[2]
INT iArgumentSpeechStage

BLIP_INDEX blipKidnappers[2]
BLIP_INDEX blipVictim

BOOL bSetUpUntieScene
BOOL bSetUpUntieSpeech
BOOL bUntieSceneEnding
BOOL bCutsceneSkipped
INT iCurrentUntieSceneTimer
INT iStartUntieSceneTimer
BOOL bThankYouUntie
BOOL bCleanUpUntieScene
BOOL bPushInToFirstPerson
BOOL bDropOffDone

INT iVictimStartHealth = 200 //100000000

INT iBitFieldDontCheck = 0
INT iLockOnTimer
EAggro aggroReason
BOOL bAggroed 
BOOL bHelpingVic
BOOL bDrivingHome
BOOL bUnsuitableVehRemark
BOOL bToldForGettingOut
BOOL bToldForJacking
BOOL bToldForShooting

INT iDropOffCutStage

BLIP_INDEX blipGirlsDropOff
BLIP_INDEX blipCult
VECTOR vDropGirl = << -1161.1992, 934.5912, 196.7591 >>
//VECTOR vWalkToGirl = << 1741.6982, 4649.0591, 42.3138 >>
VECTOR vCult = << -1034.6, 4918.6, 205.9 >>

BOOL bGunFight0
BOOL bGunFight1
BOOL bOneGuyLeftLeave
BOOL bCloseLine

BOOL bConvOnHold
TEXT_LABEL_23 ConvResumeLabel = ""
TEXT_LABEL_23 strCurrentRoot
//STRING strDialogueBlock

BOOL bDoFullCleanUp
//CAMERA_INDEX camCutscene
//BOOL bAgainstTraffic = FALSE
//INT iCutsceneWaitTimer

BLIP_INDEX blipInitialMarker
BOOL bLastGuyTask
BOOL bToldFuckOff
BOOL bToldFuckOff2
BOOL bToldShoot

INT iHelpSpeech

VEHICLE_INDEX tempPlayersVehID

BOOL bStartOnFootTimer
INT iOnFootStartTimer
INT iOnFootCurrentTimer
BOOL bStartStationaryTimer
INT iStationaryStartTimer
INT iStationaryCurrentTimer
//INT iCloseByStartTimer = -1

REL_GROUP_HASH rghKidnappers
REL_GROUP_HASH rghVictim

structPedsForConversation BurialSpeechStruct
INT sceneId
#IF IS_DEBUG_BUILD
	INT sceneId1
#ENDIF
//=======================================================
//	PROCEDURES FOR INITIALISATION, SET UP, AND CLEAN UP
//=======================================================
// Clean Up
PROC missionCleanup()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@\n")
	
//    #IF IS_DEBUG_BUILD
//        IF can_cleanup_script_for_debug
//			IF DOES_ENTITY_EXIST(pedKidnapper[0])
//				DELETE_PED(pedKidnapper[0])
//			ENDIF
//			IF DOES_ENTITY_EXIST(pedKidnapper[1])
//				DELETE_PED(pedKidnapper[1])
//			ENDIF
//			IF DOES_ENTITY_EXIST(pedVictim)
//				DELETE_PED(pedVictim)
//			ENDIF
//			IF DOES_ENTITY_EXIST(vehKidnappersVeh)
//				DELETE_VEHICLE(vehKidnappersVeh)
//			ENDIF
//		ENDIF	
//    #ENDIF
	
	IF bDoFullCleanUp
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		CANCEL_MUSIC_EVENT("RE6_BOTH_DEAD_OS")
		IF bGunFight0
		OR bToldFuckOff
			IF g_bAltruistCultFinalDialogueHasPlayed
				TRIGGER_MUSIC_EVENT("AC_STOP")
			ELSE
				TRIGGER_MUSIC_EVENT("RE6_END")
			ENDIF
		ENDIF
		RESET_CULT_STATUS()
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghVictim, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, rghVictim)
//		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		ENDIF
//		
//		IF bVariablesInitialised
//			SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
//			SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnapper1)
//			SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnapper0)
//			SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnappersVehicle)
//			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_SHOVEL)
//		ENDIF
//		IF DOES_ENTITY_EXIST(pedKidnapper[0])
//			SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[0])
//		ENDIF
//		IF DOES_ENTITY_EXIST(pedKidnapper[1])
//			SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[1])
//		ENDIF
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			// This is so the pedVictim won't stand up if the player has been killed.
			IF NOT IS_PED_INJURED(pedVictim)
				SET_PED_CONFIG_FLAG(pedVictim, PCF_CanSayFollowedByPlayerAudio, TRUE)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
					RESET_PED_LAST_VEHICLE(pedVictim)
				ENDIF
//				FREEZE_ENTITY_POSITION(pedVictim, FALSE)
				IF IS_PED_IN_GROUP(pedVictim)
					REMOVE_PED_FROM_GROUP(pedVictim)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
				SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
			ELSE
				IF NOT IS_ENTITY_DEAD(pedVictim)
					SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
					SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
//					IF IS_ENTITY_PLAYING_ANIM(pedVictim, "random@burial", "b_burial")
//						STOP_ANIM_TASK(pedVictim, "random@burial", "b_burial")
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		INT index
		REPEAT COUNT_OF(pedKidnapper) index
			IF NOT IS_PED_INJURED(pedKidnapper[index])
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedKidnapper[index])
				IF bLookingAtPlayer0
				OR bWarnedBeforeShooting
					TASK_COMBAT_PED(pedKidnapper[index], PLAYER_PED_ID())
				ELSE
					TASK_SMART_FLEE_PED(pedKidnapper[index], PLAYER_PED_ID(), 1000, -1)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				SET_PED_KEEP_TASK(pedKidnapper[index], TRUE)
			ENDIF
		ENDREPEAT
		
		DELETE_OBJECT(objRope[0])
		DELETE_OBJECT(objRope[1])
		
		IF DOES_ENTITY_EXIST(vehKidnappersVeh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehKidnappersVeh)
		ENDIF
		IF DOES_ENTITY_EXIST(objSpade)
			IF IS_ENTITY_ATTACHED(objSpade)
				DETACH_ENTITY(objSpade)
			ENDIF
			SET_OBJECT_AS_NO_LONGER_NEEDED(objSpade)
		ENDIF
		IF DOES_BLIP_EXIST(blipInitialMarker)
			REMOVE_BLIP(blipInitialMarker)
		ENDIF
		FOR i = 0 to 1
			IF DOES_BLIP_EXIST(blipKidnappers[i])
				REMOVE_BLIP(blipKidnappers[i])
			ENDIF
		ENDFOR
		IF DOES_BLIP_EXIST(blipVictim)
			REMOVE_BLIP(blipVictim)
		ENDIF
		IF DOES_CAM_EXIST(camFreeingPed)
			DESTROY_CAM(camFreeingPed)
		ENDIF
		
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
//		IF IS_PLAYER_PLAYING(PLAYER_ID())
//			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			ENDIF
//		ENDIF
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -133.8410, -38.5734, -100>>, << -126.2403, -29.1940, 100 >>, TRUE)
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	// Do none of the above, just what is below, if the script has been set to clean up straight away
	//  v               v               v         v             v            v           v           v            v         v         v
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@\n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	IF NOT HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_RE_BURIAL_M_DONE, CT_AMBIENT, BIT_MICHAEL, CHAR_ANTONIA, 3, 60000, 30000, TEXT_RE_BURIAL_END, VID_BLANK, CID_MAKE_RE_BURIAL_PAYMENT, FLOW_CHECK_NONE, BIT_MISSED_TEXT)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_RE_BURIAL_F_DONE, CT_AMBIENT, BIT_FRANKLIN, CHAR_ANTONIA, 3, 60000, 30000, TEXT_RE_BURIAL_END, VID_BLANK, CID_MAKE_RE_BURIAL_PAYMENT, FLOW_CHECK_NONE, BIT_MISSED_TEXT)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_RE_BURIAL_T_DONE, CT_AMBIENT, BIT_TREVOR, CHAR_ANTONIA, 3, 60000, 30000, TEXT_RE_BURIAL_END, VID_BLANK, CID_MAKE_RE_BURIAL_PAYMENT, FLOW_CHECK_NONE, BIT_MISSED_TEXT)
		ENDIF
	ENDIF
	SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@\n")
	TRIGGER_MUSIC_EVENT("RE6_END")
	missionCleanup()
ENDPROC

PROC initialiseEventVariables()
	modelVictim = U_F_Y_Mistress
	modelKidnapper0 = A_M_M_Salton_01
	modelKidnapper1 = A_M_M_Salton_01
	modelKidnappersVehicle = BISON
	
	vCreateVictim = << 163.4486, 6839.9932, 18.86 >>
	fCreateVictim = 0
	vCreateKidnapper[0] = << 162.8193, 6837.2570, 18.9614 >>
	fCreateKidnapper[0] = 297.0056
	vCreateKidnapper[1] = << 162.5046, 6841.6797, 18.8426 >>
	fCreateKidnapper[1] = 193.3866
	vKidnappersVehicle = << 169.3462, 6837.8047, 19.1421 >>
	fKidnappersVehicle = 265.6862
	
	vDefenceArea1 = << 164.089645, 6836.922852, 19.038990 >>
	vDefenceArea2 = << 168.963806, 6834.302246, 24.990570 >>
	
	bVariablesInitialised = TRUE
ENDPROC

PROC loadAssets()
	REQUEST_MODEL(modelVictim)
	REQUEST_MODEL(modelKidnapper1)
	REQUEST_MODEL(modelKidnapper0)
	REQUEST_MODEL(modelKidnappersVehicle)
	REQUEST_MODEL(PROP_LD_SHOVEL)
	REQUEST_MODEL(p_arm_bind_cut_s)
	REQUEST_ANIM_DICT("random@burial")
	REQUEST_PTFX_ASSET()
	REQUEST_SCRIPT_AUDIO_BANK("ROPE_CUT")
	PREPARE_MUSIC_EVENT("RE6_BOTH_DEAD_OS")
	
	IF HAS_MODEL_LOADED(modelVictim)
	AND HAS_MODEL_LOADED(modelKidnapper1)
	AND HAS_MODEL_LOADED(modelKidnapper0)
	AND HAS_MODEL_LOADED(modelKidnappersVehicle)
	AND HAS_MODEL_LOADED(PROP_LD_SHOVEL)
	AND HAS_MODEL_LOADED(p_arm_bind_cut_s)
	AND HAS_ANIM_DICT_LOADED("random@burial")
	AND HAS_PTFX_ASSET_LOADED()
	AND REQUEST_SCRIPT_AUDIO_BANK("ROPE_CUT")
	AND PREPARE_MUSIC_EVENT("RE6_BOTH_DEAD_OS")
		bAssetsLoaded = TRUE
		
	ELSE
		REQUEST_MODEL(modelVictim)
		REQUEST_MODEL(modelKidnapper1)
		REQUEST_MODEL(modelKidnapper0)
		REQUEST_MODEL(modelKidnappersVehicle)
		REQUEST_MODEL(PROP_LD_SHOVEL)
		REQUEST_MODEL(p_arm_bind_cut_s)
		REQUEST_ANIM_DICT("random@burial")
		REQUEST_PTFX_ASSET()
		REQUEST_SCRIPT_AUDIO_BANK("ROPE_CUT")
		PREPARE_MUSIC_EVENT("RE6_BOTH_DEAD_OS")
	ENDIF
ENDPROC

PROC changeBlips()
	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	IF NOT DOES_BLIP_EXIST(blipVictim)
		IF NOT IS_ENTITY_DEAD(pedVictim)
			blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
		ENDIF
	ENDIF
	FOR i = 0 to 1
		IF NOT DOES_BLIP_EXIST(blipKidnappers[i])
			IF NOT IS_ENTITY_DEAD(pedKidnapper[i])
				blipKidnappers[i] = CREATE_BLIP_FOR_PED(pedKidnapper[i], TRUE)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC createScene()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	ADD_SCENARIO_BLOCKING_AREA(vDropGirl - << 40, 40, 20 >>, vDropGirl + << 40, 40, 20 >>)
	ADD_SCENARIO_BLOCKING_AREA(vCreateVictim - << 60, 50, 20 >>, vCreateVictim + << 50, 50, 20 >>)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelKidnappersVehicle, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(0)
	
	pedVictim = CREATE_PED(PEDTYPE_MISSION, modelVictim, vCreateVictim, fCreateVictim)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
	SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_USE_VEHICLE, FALSE)
	SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_DISABLE_HANDS_UP, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
	
	SET_PED_CONFIG_FLAG(pedVictim, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
	SET_PED_CONFIG_FLAG(pedVictim, PCF_ActivateRagdollFromMinorPlayerContact, FALSE)

	SET_PED_CONFIG_FLAG(pedVictim, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(pedVictim, PCF_DisableExplosionReactions, TRUE)

	SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
	SET_ENTITY_HEALTH(pedVictim, iVictimStartHealth)
	
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
	
	ADD_RELATIONSHIP_GROUP("rghKidnappers", rghKidnappers)
	ADD_RELATIONSHIP_GROUP("rghVictim", rghVictim)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, rghVictim)
	
	FOR i = 0 to 1
		pedKidnapper[i] = CREATE_PED(PEDTYPE_MISSION, modelKidnapper0, vCreateKidnapper[i],  fCreateKidnapper[i])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedKidnapper[i], TRUE)
		SET_PED_AS_ENEMY(pedKidnapper[i], TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedKidnapper[i], CA_USE_COVER, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedKidnapper[i], FA_USE_VEHICLE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedKidnapper[i], FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_CONFIG_FLAG(pedKidnapper[i], PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_COMBAT_MOVEMENT(pedKidnapper[0], CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(pedKidnapper[0], CA_CAN_CHARGE, TRUE)
		SET_PED_SHOOT_RATE(pedKidnapper[i], 50)
		SET_PED_ACCURACY(pedKidnapper[i], 13)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedKidnapper[i], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedKidnapper[i], rghKidnappers)
		SET_PED_MONEY(pedKidnapper[i], GET_RANDOM_INT_IN_RANGE(800, 2000))
	ENDFOR
	SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnapper1)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnapper0)
	
	SET_PED_SHOOT_RATE(pedKidnapper[1], 100)
	SET_PED_FIRING_PATTERN(pedKidnapper[1], FIRING_PATTERN_BURST_FIRE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghKidnappers, RELGROUPHASH_PLAYER)
	
	GIVE_WEAPON_TO_PED(pedKidnapper[0], WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
	GIVE_WEAPON_TO_PED(pedKidnapper[1], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, FALSE)
	
	vehKidnappersVeh = CREATE_VEHICLE(modelKidnappersVehicle, vKidnappersVehicle,  fKidnappersVehicle)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehKidnappersVeh)
	SET_VEHICLE_LIGHTS(vehKidnappersVeh, SET_VEHICLE_LIGHTS_ON)
	SET_VEHICLE_DOOR_OPEN(vehKidnappersVeh, SC_DOOR_BOOT)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnappersVehicle)
	
	objSpade = CREATE_OBJECT(PROP_LD_SHOVEL, vWorldPoint)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_SHOVEL)
	objRope[0] = CREATE_OBJECT(p_arm_bind_cut_s, vWorldPoint)
	objRope[1] = CREATE_OBJECT(p_arm_bind_cut_s, vWorldPoint + << 1, 1, 1 >>)
	REQUEST_MODEL(p_arm_bind_cut_s)
//	ATTACH_ENTITY_TO_ENTITY(objSpade, pedKidnapper[0], GET_PED_BONE_INDEX(pedKidnapper[0], BONETAG_R_HAND), << 0.1, 0, 0 >>, << 45, 0, -90 >>, FALSE)//R_Hand
	ATTACH_ENTITY_TO_ENTITY(objSpade, pedKidnapper[0], GET_PED_BONE_INDEX(pedKidnapper[0], BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, FALSE)
	
//	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 1, 0, 0) //(head)
//	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HAIR, 0, 1, 0) //(hair)
//	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TORSO, 1, 1, 0) //(uppr)
//	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_LEG, 0, 1, 0) //(lowr)
//	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
	
	SET_PED_COMPONENT_VARIATION(pedKidnapper[0], PED_COMP_HEAD, 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedKidnapper[0], PED_COMP_TORSO, 0, 1, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedKidnapper[0], PED_COMP_LEG, 1, 0, 0) //(lowr)
	
	SET_PED_COMPONENT_VARIATION(pedKidnapper[1], PED_COMP_HEAD, 0, 2, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedKidnapper[1], PED_COMP_TORSO, 2, 1, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedKidnapper[1], PED_COMP_LEG, 1, 1, 0) //(lowr)
	
	VECTOR scenePosition = vCreateVictim //<< 163.632, 6838.878, 18.939 >>
	VECTOR sceneRotation = << -2.000, -4.000, 18.000 >>
	sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//	TASK_PLAY_ANIM(pedVictim, "random@burial", "b_burial", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_DELAY_BLEND_WHEN_FINISHED)
//	TASK_PLAY_ANIM(pedVictim, "random@burial", "b_burial", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
	TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@burial", "b_burial", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[0], sceneId, "b_burial_legstie", "random@burial", SLOW_BLEND_IN, SLOW_BLEND_OUT)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[1], sceneId, "b_burial_wristtie", "random@burial", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//	CLEAR_RAGDOLL_BLOCKING_FLAGS(pedVictim)
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
	SET_PED_KEEP_TASK(pedVictim, TRUE)
	
//	SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
//	SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
	
//	scenePosition = << 164.6617, 6837.0928, 18.7722 >>
//	sceneRotation = << 5.000, -4.000, -117.000 >>
//	sceneId1 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//	TASK_SYNCHRONIZED_SCENE(pedKidnapper[0], sceneId1, "random@burial", "a_burial", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//	SET_SYNCHRONIZED_SCENE_LOOPED(sceneId1, TRUE)
	TASK_PLAY_ANIM(pedKidnapper[0], "random@burial", "a_burial", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
//	TASK_START_SCENARIO_IN_PLACE(pedKidnapper[1], "WORLD_HUMAN_SMOKING")
	TASK_PLAY_ANIM(pedKidnapper[1], "random@burial", "c_burial", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
	
	SET_PED_ANGLED_DEFENSIVE_AREA(pedKidnapper[0], vDefenceArea1, vDefenceArea2, 2.75)
	SET_PED_ANGLED_DEFENSIVE_AREA(pedKidnapper[1], vDefenceArea1, vDefenceArea2, 2.75)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	SET_AMBIENT_VOICE_NAME(pedKidnapper[0], "A_M_M_HillBilly_02_WHITE_MINI_03")
	SET_AMBIENT_VOICE_NAME(pedKidnapper[1], "A_M_M_HillBilly_02_WHITE_MINI_02")
	SET_AMBIENT_VOICE_NAME(pedVictim, "KIDNAPPEDFEMALE")
	ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 4, pedKidnapper[0], "KIDNAPPER2")
	ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 3, pedKidnapper[1], "KIDNAPPER1")
	ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 5, pedVictim, "KIDNAPPEDFEMALE")
	
//	blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vWorldPoint)
//	changeBlips()
	
ENDPROC

FUNC BOOL allPedsInjured()
	IF IS_PED_INJURED(pedVictim)
//	AND IS_PED_INJURED(pedKidnapper[0])
//	AND IS_PED_INJURED(pedKidnapper[1])
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC reactionsToAttackingVic()
	IF NOT bHurtVicRemark
	AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWorldPoint, << 30, 30, 30 >>)
	//	Temporary speech "Fuck! Poor bastard, everyone wants her dead!"
		IF NOT IS_PED_INJURED(pedKidnapper[1])
			IF CREATE_CONVERSATION(BurialSpeechStruct,  "REBU2AU", "REBU2_ATVF", CONV_PRIORITY_AMBIENT_HIGH)
				bHurtVicRemark = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehKidnappersVeh)
		IF NOT IS_PED_INJURED(pedKidnapper[0])
			TASK_VEHICLE_MISSION_PED_TARGET(pedKidnapper[0], vehKidnappersVeh, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_PLOUGHTHROUGH, -1, 10)
			SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(pedKidnapper[1])
			TASK_ENTER_VEHICLE(pedKidnapper[1],vehKidnappersVeh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT, ECF_RESUME_IF_INTERRUPTED)
			SET_PED_KEEP_TASK(pedKidnapper[1], TRUE)
		ENDIF
	ELSE
		FOR i = 0 to 1
			IF NOT IS_PED_INJURED(pedKidnapper[i])
				TASK_SMART_FLEE_PED(pedKidnapper[i], PLAYER_PED_ID(), 250, -1)
				SET_PED_KEEP_TASK(pedKidnapper[i], TRUE)
			ENDIF
		ENDFOR
	ENDIF	
//	HAS_PED_BEEN_DAMAGED_BY_WEAPON
	WAIT(0)
	
	MISSION_FAILED()
ENDPROC

PROC continualTurnToFacePlayer(PED_INDEX targetPed, INT& stage)
	SWITCH stage
		CASE 0 // TURN TASK
			IF NOT IS_PED_INJURED(targetPed)
			//	CLEAR_PED_TASKS(targetPed)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(targetPed, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				stage ++
			ENDIF
		BREAK
		CASE 1 // TURNING 
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), 15)
					// ACHIEVED TASK
					stage ++
				ENDIF
			ENDIF
		BREAK
		CASE 2 // WAITING TO TURN
			IF NOT IS_PED_INJURED(targetPed)
				IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), 25)
					stage = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC blipRemoval()
	IF IS_PED_INJURED(pedKidnapper[0])
		IF DOES_BLIP_EXIST(blipKidnappers[0])
			IF DOES_ENTITY_EXIST(objSpade)
				IF IS_ENTITY_ATTACHED(objSpade)
					DETACH_ENTITY(objSpade)
				ENDIF
			ENDIF
			REMOVE_PED_FOR_DIALOGUE (BurialSpeechStruct, 4)
			REMOVE_BLIP(blipKidnappers[0])
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedKidnapper[1])
		IF DOES_BLIP_EXIST(blipKidnappers[1])
			REMOVE_PED_FOR_DIALOGUE (BurialSpeechStruct, 3)
			REMOVE_BLIP(blipKidnappers[1])
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedVictim)
		IF NOT IS_ENTITY_DEAD(pedVictim)
//			IF IS_ENTITY_PLAYING_ANIM(pedVictim, "random@burial", "b_burial")
//				STOP_ANIM_TASK(pedVictim, "random@burial", "b_burial")
//			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(blipVictim)
			REMOVE_PED_FOR_DIALOGUE (BurialSpeechStruct, 5)
			REMOVE_BLIP(blipVictim)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 1
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
			IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) != RHINO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), TRUE)
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ANY_THREATENING_VEHICLE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
//				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), AMBULANCE)
//				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), FIRETRUK)
				OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), ANNIHILATOR)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), BUZZARD)
	//			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), LAZER)
				OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
//				OR g_bPlayerIsInTaxi = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//
//PROC replaceShovel()
//	IF DOES_ENTITY_EXIST(objSpade)
//		IF iCloseByStartTimer = -1
//			IF NOT IS_ENTITY_ATTACHED(objSpade)
//				iCloseByStartTimer = GET_GAME_TIMER() + 1000
//			ENDIF
//		ENDIF
//		
//		IF (iCloseByStartTimer < GET_GAME_TIMER() AND iCloseByStartTimer != -1)
//			VECTOR vSpadeCoords
//			VECTOR vSpadeRotation
//			vSpadeCoords = GET_ENTITY_COORDS(objSpade)
//			vSpadeRotation = GET_ENTITY_ROTATION(objSpade) + << 0, 180, 0 >>
//			CREATE_PICKUP_ROTATE(PICKUP_WEAPON_GOLFCLUB, vSpadeCoords, vSpadeRotation)
//			DELETE_OBJECT(objSpade)
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL IS_PLAYER_ABLE_TO_HELP_HOSTAGE()
	IF NOT bHelpingVic
		IF NOT DOES_ENTITY_EXIST(pedKidnapper[0])
		AND NOT DOES_ENTITY_EXIST(pedKidnapper[1])
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedKidnapper[0])
		AND DOES_ENTITY_EXIST(pedKidnapper[1])
			IF IS_PED_INJURED(pedKidnapper[0])
			AND IS_PED_INJURED(pedKidnapper[1])
				RETURN TRUE
			ENDIF
			IF IS_PED_HURT(pedKidnapper[0])
			AND IS_PED_HURT(pedKidnapper[1])
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedKidnapper[0])
		AND NOT DOES_ENTITY_EXIST(pedKidnapper[1])
			IF IS_PED_INJURED(pedKidnapper[0])
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedKidnapper[1])
		AND NOT DOES_ENTITY_EXIST(pedKidnapper[0])
			IF IS_PED_INJURED(pedKidnapper[1])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_LABEL_PLAYING(STRING sRootLabel)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	    
	    IF ARE_STRINGS_EQUAL(sRootLabel, txtRootLabel)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
	IF IS_THIS_CONVERSATION_PLAYING("REBU2_WV")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_UNS1")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_UNS2")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_CULT")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_NEAR")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_WRONG")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_STOP")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_GETOUT")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REBU2_SHOOT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_CONVERSATION(BOOL bCanPlay)
	IF bCanPlay
		IF bConvOnHold
			IF NOT IS_STRING_NULL_OR_EMPTY(strCurrentRoot)
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(BurialSpeechStruct, "REBU2AU", strCurrentRoot, ConvResumeLabel, CONV_PRIORITY_HIGH)
					PRINTLN("@@@@@@@@@@ CREATE_CONVERSATION_FROM_SPECIFIC_LINE: ", strCurrentRoot, " ", ConvResumeLabel, " @@@@@@@@@@")
					bConvOnHold = FALSE
				ENDIF
			ELSE
				bConvOnHold = FALSE
			ENDIF
		ENDIF
	ELIF NOT bConvOnHold
	AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND NOT IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
		strCurrentRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		ConvResumeLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		PRINTLN("@@@@@@@@@@ SAVED ROOT: ", strCurrentRoot, " @@@ SAVED LABEL: ", ConvResumeLabel, " @@@@@@@@@@")
		KILL_FACE_TO_FACE_CONVERSATION()
		bConvOnHold = TRUE
	ENDIF
ENDPROC

PROC isPlayerMessingAbout()
	IF DOES_ENTITY_EXIST(pedVictim)
		IF NOT IS_PED_INJURED(pedVictim)
			SET_PED_RESET_FLAG(pedVictim, PRF_NoCollisionDamageFromOtherPeds, TRUE)
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(pedVictim)
				TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
				SET_PED_KEEP_TASK(pedVictim, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedMafia)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghVictim, RELGROUPHASH_PLAYER)
				TASK_COMBAT_PED(pedMafia, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedMafia, TRUE)
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(BurialSpeechStruct,  "REBU2AU", "REBU2_CRAZY", CONV_PRIORITY_AMBIENT_HIGH)
			MISSION_FAILED()
			interferenceType = interfere_HARM_VICTIM
		ENDIF
		IF DOES_ENTITY_EXIST(pedMafia)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMafia, PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedVictim)
					TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(pedMafia)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghVictim, RELGROUPHASH_PLAYER)
					TASK_COMBAT_PED(pedMafia, PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedMafia, TRUE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(BurialSpeechStruct,  "REBU2AU", "REBU2_CRAZY", CONV_PRIORITY_AMBIENT_HIGH)
				MISSION_FAILED()
				interferenceType = interfere_HARM_VICTIM
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedVictim)
//		IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
//		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
//			IF IS_PED_SHOOTING(PLAYER_PED_ID())
//				TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
//				SET_PED_KEEP_TASK(pedVictim, TRUE)
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//				WAIT(0)
//				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CRAZY", CONV_PRIORITY_AMBIENT_HIGH)
//				MISSION_FAILED()
//				interferenceType = interfere_HARM_VICTIM
//			ENDIF
//		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 1
//		OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(PLAYER_PED_ID())
			TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
			SET_PED_KEEP_TASK(pedVictim, TRUE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(BurialSpeechStruct,  "REBU2AU", "REBU2_CRAZY", CONV_PRIORITY_AMBIENT_HIGH)
			MISSION_FAILED()
			interferenceType = interfere_HARM_VICTIM
		ENDIF
	ENDIF
ENDPROC

PROC spadeDirtPfx()
	IF DOES_ENTITY_EXIST(objSpade)
		IF IS_ENTITY_ATTACHED(objSpade)
			IF NOT IS_PED_INJURED(pedKidnapper[0])
				IF IS_ENTITY_PLAYING_ANIM(pedKidnapper[0], "random@burial", "a_burial_stop")
					IF (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial_stop") > 0.124 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.127)
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_burial_dirt", objSpade, << 0, 0, -0.95 >>, << 0, 180, 0 >>)
					ENDIF
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(pedKidnapper[0], "random@burial", "a_burial")
					IF (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") > 0.040 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.043)
					OR (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") > 0.240 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.243)
					OR (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") > 0.440 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.443)
					OR (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") > 0.640 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.643)
					OR (GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") > 0.840 AND GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial") < 0.843)
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_burial_dirt", objSpade, << 0, 0, -0.95 >>, << 0, 180, 0 >>)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC pleadDialogue()
	IF NOT IS_PED_INJURED(pedVictim)
//		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 30, 30, 30 >>)
			SWITCH iHelpSpeech
				CASE 0
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_PD", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_PD", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_PD", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
//						//Temporary debug help text "I can't get free!" etc
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_PD", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6500))
							iHelpSpeech = 0
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
//		ENDIF
	ENDIF
	
ENDPROC

PROC fightDialogue()
	IF NOT IS_PED_INJURED(pedVictim)
		IF NOT IS_PED_INJURED(pedKidnapper[0])
		OR NOT IS_PED_INJURED(pedKidnapper[1])
//			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 30, 30, 30 >>)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SWITCH iHelpSpeech
					CASE 0
						IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
							IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_ATT", CONV_PRIORITY_AMBIENT_HIGH)
								iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 5000))
								iHelpSpeech ++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
							IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_ATT", CONV_PRIORITY_AMBIENT_HIGH)
								iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 5000))
								iHelpSpeech ++
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
							IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_ATT", CONV_PRIORITY_AMBIENT_HIGH)
								iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 5000))
								iHelpSpeech ++
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
	//						Temporary debug help text "I can't get free!" etc
							IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_ATT", CONV_PRIORITY_AMBIENT_HIGH)
								iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 5000))
								iHelpSpeech = 0
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC helpDialogue()
	IF NOT IS_ENTITY_DEAD(pedVictim)
//		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 30, 30, 30 >>)
			SWITCH iHelpSpeech
				CASE 0
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5500))
							iHelpSpeech ++
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF iHostageSpeechOutburstTimer < GET_GAME_TIMER()
//						Temporary debug help text "I can't get free!" etc
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
							iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5500))
							iHelpSpeech = 0
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
//		ENDIF
	ENDIF
	
ENDPROC

PROC closeDialogue()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 8, 8, 8 >>)//, FALSE, TRUE, TM_ON_FOOT)
		AND NOT bCloseLine
		AND TIMERA() > 8500
			//Temporary "help"
			IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
				bCloseLine = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC chatInterruptions()

	IF NOT IS_PED_INJURED(pedVictim)
	
		IF NOT bToldForGettingOut
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE(), TRUE)
				AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
					IF NOT IS_THIS_CONVERSATION_PLAYING("REBU2_LV2")
						MANAGE_CONVERSATION(FALSE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						//"Why are you leaving, come back here"
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_GETOUT", CONV_PRIORITY_AMBIENT_HIGH)
						bToldForGettingOut = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
					bToldForGettingOut = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bToldForJacking	
			IF IS_PED_JACKING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Ok, whatever it takes to get back home"
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_JACK", CONV_PRIORITY_AMBIENT_HIGH)
				bToldForJacking = TRUE
			ENDIF
		ELIF NOT IS_PED_JACKING(PLAYER_PED_ID())
			bToldForJacking = FALSE
		ENDIF
		
		IF NOT bToldForShooting
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Why are you shooting!?"
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_SHOOT", CONV_PRIORITY_AMBIENT_HIGH)
				bToldForShooting = TRUE
			ENDIF
		ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
			bToldForShooting = FALSE
		ENDIF
		
	ENDIF
	
ENDPROC

PROC groupSeparationCheck()
	IF NOT IS_PED_INJURED(pedVictim)
//		IF NOT IS_PED_IN_GROUP(pedVictim)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 8, 8, 8 >>)
			OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PED_IN_VEHICLE(pedVictim, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
					IF NOT DOES_BLIP_EXIST(blipVictim)
						blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
					ENDIF
					IF DOES_BLIP_EXIST(blipGirlsDropOff)
						REMOVE_BLIP(blipGirlsDropOff)
					ENDIF
//					IF DOES_BLIP_EXIST(blipCult)
//						REMOVE_BLIP(blipCult)
//					ENDIF
				ELSE
					IF NOT IS_PED_ON_FOOT(PLAYER_PED_ID())
						IF DOES_BLIP_EXIST(blipVictim)
							REMOVE_BLIP(blipVictim)
						ENDIF
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipGirlsDropOff)
//						SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
//						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
						blipGirlsDropOff = CREATE_BLIP_FOR_COORD(vDropGirl, TRUE)
						//SET_BLIP_ROUTE(blipGirlsDropOff, TRUE)
					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//						IF NOT DOES_BLIP_EXIST(blipCult)
//							blipCult = CREATE_BLIP_FOR_COORD(vCult)
//							SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//							PRINT_CULT_HELP()
//						ENDIF
//					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipVictim)
					blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
				ENDIF
//				IF DOES_BLIP_EXIST(blipGirlsDropOff)
//					REMOVE_BLIP(blipGirlsDropOff)
//				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			ENDIF
			IF NOT IS_PED_IN_GROUP(pedVictim)
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 8, 8, 8 >>)
					CLEAR_PED_TASKS(pedVictim)
					SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
					SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
				ENDIF
//				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
//					AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
						CLEAR_PED_TASKS(pedVictim)
						SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
						SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
					ENDIF
//				ENDIF
			ENDIF
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 150, 150, 150 >>)
				MISSION_FAILED()
			ENDIF
//		ENDIF
	ENDIF
ENDPROC

PROC onFootCheck()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_ON_FOOT(pedVictim)
			IF NOT bStartOnFootTimer
				iOnFootStartTimer = GET_GAME_TIMER()
				bStartOnFootTimer = TRUE
			ELSE
				iOnFootCurrentTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			bStartOnFootTimer = FALSE
		ENDIF
	ENDIF
	
	IF ((iOnFootCurrentTimer - iOnFootStartTimer) > 180000)
		IF NOT IS_PED_INJURED(pedVictim)
			IF IS_PED_IN_GROUP(pedVictim)
				REMOVE_PED_FROM_GROUP(pedVictim)
				IF IS_ENTITY_AT_COORD(pedVictim, vDropGirl, << 100, 100, 100 >>)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, << 1740.7965, 4648.7563, 42.6529 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_WK", CONV_PRIORITY_AMBIENT_HIGH)
			//	"Well, if we're going to walk I can do that by myself.
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC stationaryCheck()
	IF IS_PED_STOPPED(PLAYER_PED_ID())
		IF NOT bStartStationaryTimer
			iStationaryStartTimer = GET_GAME_TIMER()
			bStartStationaryTimer = TRUE
		ELSE
			iStationaryCurrentTimer = GET_GAME_TIMER()
		ENDIF 
	ELSE
		bStartStationaryTimer = FALSE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 120000)
		IF NOT IS_PED_INJURED(pedVictim)
			IF IS_PED_IN_GROUP(pedVictim)
				REMOVE_PED_FROM_GROUP(pedVictim)
				IF IS_ENTITY_AT_COORD(pedVictim, vDropGirl, << 100, 100, 100 >>)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, vDropGirl, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
				ELSE
					TASK_LEAVE_ANY_VEHICLE(pedVictim, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_QM", CONV_PRIORITY_AMBIENT_HIGH)
				//"I can probably get there quicker myself."
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC dontFollowPlayerInWrongVehicle()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE DOESN'T FIT @@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
				IF IS_PED_IN_GROUP(pedVictim)
					REMOVE_PED_FROM_GROUP(pedVictim)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
					TASK_GO_TO_ENTITY(pedVictim, PLAYER_PED_ID(), -1, 6)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedVictim)
					TASK_LEAVE_ANY_VEHICLE(pedVictim, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
				ENDIF
				IF NOT bUnsuitableVehRemark
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bUnsuitableVehRemark = TRUE
						IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
							CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_UNS1", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_UNS2", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
//					ENDIF
				ENDIF
			ELSE
				bUnsuitableVehRemark = FALSE
				IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = WAITING_TO_START_TASK
					CLEAR_PED_TASKS(pedVictim)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_IN_GROUP(pedVictim)
			AND NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
			AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
				SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_GIRL_IN_PLAYERS_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_PED_SITTING_IN_VEHICLE(pedVictim, tempPlayersVehID)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC chatInVehicle()
	
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_THIS_LABEL_PLAYING("REBU2_LV2_2")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_3")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_4")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_5")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_6")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_7")
		OR IS_THIS_LABEL_PLAYING("REBU2_LV2_8")
//			IF IS_PED_IN_ANY_VEHICLE(pedVictim)
				TASK_USE_MOBILE_PHONE(pedVictim, TRUE)
				TASK_CLEAR_LOOK_AT(pedVictim)
//			ENDIF
		ENDIF
		IF IS_THIS_LABEL_PLAYING("REBU2_LV2_9")
		OR iChatInVehicle > 1
//		OR NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
			TASK_USE_MOBILE_PHONE(pedVictim, FALSE)
		ENDIF
	ENDIF
	
	IF IS_GIRL_IN_PLAYERS_VEHICLE()
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		IF NOT IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
			MANAGE_CONVERSATION(TRUE)
		ENDIF
		
		SWITCH iChatInVehicle
			
			CASE 0
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_LV2", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 1
				iChatTimer = GET_GAME_TIMER() + 1500
				iChatInVehicle ++
			BREAK
			
			CASE 2
				IF iChatTimer < GET_GAME_TIMER()
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT0", CONV_PRIORITY_AMBIENT_HIGH)
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 3
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT0M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT0F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT0T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 4
				iChatTimer = GET_GAME_TIMER() + 500
				iChatInVehicle ++
			BREAK
			
			CASE 5
				IF iChatTimer < GET_GAME_TIMER()
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT1", CONV_PRIORITY_AMBIENT_HIGH)
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 6
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT1M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT1F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT1T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 7
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT2", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 8
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT2M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT2F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT2T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 9
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT3", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 10
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT3M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT3F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT3T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 11
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT4", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 12
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT4M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT4F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT4T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 13
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT5", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 14
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT5M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT5F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT5T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 15
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT6", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 16
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT6M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT6F", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR		  
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT6T", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 17
				iChatTimer = GET_GAME_TIMER() + 1000
				iChatInVehicle ++
			BREAK
			
			CASE 18
				IF iChatTimer < GET_GAME_TIMER()
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT7", CONV_PRIORITY_AMBIENT_HIGH)
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 19
				iChatTimer = GET_GAME_TIMER() + 2500
				iChatInVehicle ++
			BREAK
			
			CASE 20
				IF iChatTimer < GET_GAME_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT7M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN		  
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT7F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR		  
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BANT7T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					iChatInVehicle ++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC playerHelpingVic()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 1.2, 1.2, 5 >>)
		AND IS_PED_ON_FOOT(PLAYER_PED_ID())
		AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			IF CAN_PLAYER_START_CUTSCENE()
				bHelpingVic = TRUE
				IF NOT bSetUpUntieScene
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_FIRES|SPC_REMOVE_EXPLOSIONS)
					CLEAR_HELP(TRUE)
					DISPLAY_HUD(FALSE)
					DISPLAY_RADAR(FALSE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					// Move player vehicle
					IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
						SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
						IF IS_ENTITY_ON_FIRE(GET_PLAYERS_LAST_VEHICLE())
							STOP_VEHICLE_FIRE(GET_PLAYERS_LAST_VEHICLE())
							SET_VEHICLE_ENGINE_HEALTH(GET_PLAYERS_LAST_VEHICLE(), 200) // Already set in STOP_VEHICLE_FIRE
							PRINTLN("@@@@@@@@@@@ SET_VEHICLE_ENGINE_HEALTH(vehKidnappersVeh, 200) @@@@@@@@@@@")
						ENDIF
						IF IS_ENTITY_AT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedVictim, << 5, 5, 5 >>)
						OR IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), vKidnappersVehicle, << 5, 5, 5 >>)
							SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_COORDS(pedVictim) - << 5, 3, 0 >>, FALSE)
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
								SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
							ENDIF
						ENDIF
					ENDIF
					
					// Move kidnapper vehicle
					IF DOES_ENTITY_EXIST(vehKidnappersVeh)
						IF IS_ENTITY_ON_FIRE(vehKidnappersVeh)
								STOP_VEHICLE_FIRE(vehKidnappersVeh)
								SET_VEHICLE_ENGINE_HEALTH(vehKidnappersVeh, 200) // Already set in STOP_VEHICLE_FIRE
								PRINTLN("@@@@@@@@@@@ SET_VEHICLE_ENGINE_HEALTH(vehKidnappersVeh, 200) @@@@@@@@@@@")
							ENDIF
						IF IS_ENTITY_AT_ENTITY(vehKidnappersVeh, pedVictim, << 8, 8, 8 >>)
							SET_ENTITY_COORDS(vehKidnappersVeh, vKidnappersVehicle/*GET_ENTITY_COORDS(pedVictim) + << 5, 0, 0 >>*/, FALSE)
							IF NOT IS_ENTITY_DEAD(vehKidnappersVeh)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehKidnappersVeh)
							ENDIF
						ENDIF
					ENDIF
					
					// Move random vehicle
//					WAIT(0)
					IF NOT IS_PED_INJURED(pedVictim)
						tempPlayersVehID = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(pedVictim), 5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
						IF NOT IS_ENTITY_DEAD(tempPlayersVehID)
							IF IS_ENTITY_ON_FIRE(tempPlayersVehID)
								STOP_VEHICLE_FIRE(tempPlayersVehID)
								SET_VEHICLE_ENGINE_HEALTH(tempPlayersVehID, 200) // Already set in STOP_VEHICLE_FIRE
								PRINTLN("@@@@@@@@@@@ SET_VEHICLE_ENGINE_HEALTH(vehKidnappersVeh, 200) @@@@@@@@@@@")
							ENDIF
//							IF IS_ENTITY_AT_ENTITY(vehicleCandidate, pedVictim, << 5, 5, 5 >>)
								SET_ENTITY_COORDS(tempPlayersVehID, GET_ENTITY_COORDS(pedVictim) + << 5, 5, 0 >>)
//							ENDIF
						ENDIF
					ENDIF
//					ENDWHILE
					
//					CLEAR_AREA(vCreateVictim, 10, FALSE)
					STOP_FIRE_IN_RANGE(vCreateVictim, 10)
					CLEAR_AREA_OF_PROJECTILES(vCreateVictim, 10)
					CLEAR_AREA_OF_VEHICLES(vCreateVictim, 4.5)
					
					SET_ENTITY_COORDS(pedVictim, vCreateVictim)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateVictim)
					
//					VECTOR scenePosition
//					FLOAT sceneHeading
					
					IF NOT IS_PED_INJURED(pedVictim)
//						scenePosition = GET_ENTITY_COORDS(pedVictim)
//						sceneHeading = GET_ENTITY_HEADING(pedVictim)
					ENDIF
					
//					WHILE IS_ANY_VEHICLE_NEAR_POINT(GET_ENTITY_COORDS(pedVictim), 5)
//					WAIT(0)
					
					// Delete kidnapper bodies obstructing cutscene
					INT index
					REPEAT COUNT_OF(pedKidnapper) index
						IF DOES_ENTITY_EXIST(pedKidnapper[index])
							IF IS_ENTITY_AT_COORD(pedKidnapper[index], GET_ENTITY_COORDS(pedVictim, FALSE), << 1.5, 1.5, 1.5 >>)
//							OR IS_ENTITY_AT_COORD(pedKidnapper[index], << 163.449, 6839.993, 18.88 >>, << 1, 1, 1 >>)
								DELETE_PED(pedKidnapper[index])
							ENDIF
						ENDIF
					ENDREPEAT
					
//					WAIT(0)
					
					IF NOT IS_PED_INJURED(pedVictim)
						sceneId = CREATE_SYNCHRONIZED_SCENE(<< 163.4490, 6839.9930, 18.8800 >>, << 0, 0, 0 >>)//<< -3, -2.5, 0 >>)
						camFreeingPed = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
						PLAY_SYNCHRONIZED_CAM_ANIM(camFreeingPed, sceneId, "untie_cam", "random@burial")
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 162.6699, 6839.2378, 18.8314 >>, FALSE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 18.2311)
						
						SET_ENTITY_HEALTH(pedVictim, 200)
						SET_ENABLE_HANDCUFFS(pedVictim, FALSE)
						SET_ENABLE_BOUND_ANKLES(pedVictim, FALSE)
						CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
						TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@burial", "untie_ped", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@burial", "untie_player", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[0], sceneId, "untie_legstie", "random@burial", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[1], sceneId, "untie_wristtie", "random@burial", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						
						SET_CAM_ACTIVE(camFreeingPed, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
												
						iStartUntieSceneTimer = GET_GAME_TIMER()
						bSetUpUntieScene = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSetUpUntieScene
	AND NOT bUntieSceneEnding
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
			IF (GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.115 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) < 0.120)
			OR (GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.355 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) < 0.360)
				PLAY_SOUND_FRONTEND(-1, "ROPE_CUT", /*<< -456, -985, 25 >>,*/ "ROPE_CUT_SOUNDSET")
				PRINTSTRING("\n@@@@@@@@@@@@@@@ PLAY_SOUND_FRONTEND @@@@@@@@@@@@@@@\n")
			ENDIF
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.6
				IF NOT bSetUpUntieSpeech
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_UNT", CONV_PRIORITY_AMBIENT_HIGH)
						bSetUpUntieSpeech = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iCurrentUntieSceneTimer = GET_GAME_TIMER()
		IF (iCurrentUntieSceneTimer - iStartUntieSceneTimer) > 10000 - 300
		AND NOT bPushInToFirstPerson
		AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
			PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
			bPushInToFirstPerson = TRUE
		ENDIF
		IF (iCurrentUntieSceneTimer - iStartUntieSceneTimer) > 10000
			bUntieSceneEnding = TRUE
		ENDIF
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			WHILE IS_SCREEN_FADING_OUT()
				WAIT(0)
			ENDWHILE
			bUntieSceneEnding = TRUE
			bCutsceneSkipped = TRUE
		ENDIF
	ENDIF
	
	IF bUntieSceneEnding
	AND NOT bCleanUpUntieScene
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		
		SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 1)
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())

		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 162.5284, 6839.6582, 18.8198 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 256.7693)
		IF NOT IS_PED_INJURED(pedVictim)
			CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
			SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
			SET_ENTITY_COORDS(pedVictim, << 164.4963, 6839.3335, 18.9657 >>)
			SET_ENTITY_HEADING(pedVictim, 73.4490)
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(26.1578)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-8.1198)
		SET_CAM_ACTIVE(camFreeingPed, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camFreeingPed)
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
		
		IF bCutsceneSkipped
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			WHILE IS_SCREEN_FADING_IN()
				WAIT(0)
			ENDWHILE
		ENDIF
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		WAIT(0)
		bCleanUpUntieScene = TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedVictim)
		IF bCleanUpUntieScene
			IF NOT bThankYouUntie
				//Temporary "I'm miles from home, can you take me back?"
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HM0", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HM1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HM2", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghVictim, RELGROUPHASH_PLAYER)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, rghVictim)
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
//						TASK_STAND_STILL(NULL, 4000)
						IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
							IF IS_ENTITY_AT_ENTITY(pedVictim, GET_PLAYERS_LAST_VEHICLE(), << 20, 20, 20 >>)
							AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_PLAYERS_LAST_VEHICLE()) > 0
								TASK_ENTER_VEHICLE(NULL, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
							ELIF NOT IS_PED_IN_GROUP(pedVictim)
								SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
							ENDIF
						ELIF NOT IS_PED_IN_GROUP(pedVictim)
							SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
							SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
							SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
						ENDIF
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedVictim, Seq)
					CLEAR_SEQUENCE_TASK(Seq)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
					IF DOES_BLIP_EXIST(blipVictim)
						REMOVE_BLIP(blipVictim)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipGirlsDropOff)
						blipGirlsDropOff = CREATE_BLIP_FOR_COORD(vDropGirl, TRUE)
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					AND NOT IS_CULT_FINISHED()
						IF NOT DOES_BLIP_EXIST(blipCult)
							blipCult = CREATE_BLIP_FOR_COORD(vCult)
							SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
							PRINT_CULT_HELP()
						ENDIF
					ENDIF
//					IF NOT IS_PED_IN_GROUP(pedVictim)
//						SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
//						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
//					ENDIF
					REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
					bDrivingHome = TRUE
					bThankYouUntie = TRUE
					SETTIMERA(0)
					REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -133.8410, -38.5734, -100>>,  << -126.2403, -29.1940, 100>>)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -133.8410, -38.5734, -100>>, << -126.2403, -29.1940, 100>>, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_CLEAR_TASKS)
//					SET_WANTED_LEVEL_MULTIPLIER(1)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
					
					thisEventStage = eS_DRIVE_GIRL_HOME
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC reactionsToPlayerShooting()
	fightDialogue()
	TRIGGER_MUSIC_EVENT("RE6_START")
	
	IF NOT bGunFight0
		IF DOES_ENTITY_EXIST(pedKidnapper[0])
			IF NOT IS_PED_INJURED(pedKidnapper[0])
				SET_PED_RESET_FLAG(pedKidnapper[0], PRF_InstantBlendToAim, TRUE)
				IF DOES_ENTITY_EXIST(objSpade)
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedKidnapper[0])
					IF IS_ENTITY_ATTACHED(objSpade)
						IF NOT bLookingAtPlayer1
//							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId1)
//								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId1) > 0.9
									DETACH_ENTITY(objSpade, FALSE)
									ATTACH_ENTITY_TO_ENTITY(objSpade, pedKidnapper[0], GET_PED_BONE_INDEX(pedKidnapper[0], BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, FALSE)
									CLEAR_PED_TASKS(pedKidnapper[0])
									OPEN_SEQUENCE_TASK(seq)
										TASK_CLEAR_LOOK_AT(NULL)
										TASK_PLAY_ANIM(NULL, "random@burial", "a_burial_stop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 2000)
										TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedKidnapper[0], seq)
									CLEAR_SEQUENCE_TASK(seq)
									SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedKidnapper[0])
									bGunFight0 = TRUE
//								ENDIF
//							ENDIF
						ENDIF
					ELSE
//						CLEAR_PED_TASKS(pedKidnapper[0])
						OPEN_SEQUENCE_TASK(seq)
							TASK_CLEAR_LOOK_AT(NULL)
						//	TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 3000)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedKidnapper[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
						bGunFight0 = TRUE
					ENDIF
				ENDIF
			ELSE
				bGunFight0 = TRUE
			ENDIF
		ELSE
			bGunFight0 = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(objSpade)
		IF IS_ENTITY_ATTACHED(objSpade)
			IF NOT IS_PED_INJURED(pedKidnapper[0])	
				IF IS_ENTITY_PLAYING_ANIM(pedKidnapper[0], "random@burial", "a_burial_stop")
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial_stop") > 0.355
						DETACH_ENTITY(objSpade)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bGunFight1
		IF DOES_ENTITY_EXIST(pedKidnapper[1])
			IF NOT IS_PED_INJURED(pedKidnapper[1])
				SET_PED_RESET_FLAG(pedKidnapper[1], PRF_InstantBlendToAim, TRUE)
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedKidnapper[1])
//				CLEAR_PED_TASKS(pedKidnapper[1])
				OPEN_SEQUENCE_TASK(seq)
					TASK_CLEAR_LOOK_AT(NULL)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 500)
				//	TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000)
				//	TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 7000)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedKidnapper[1], seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(pedKidnapper[1], TRUE)
				bGunFight1 = TRUE
			ELSE
				bGunFight1 = TRUE
			ENDIF
		ELSE
			bGunFight1 = TRUE
		ENDIF
	ENDIF
	
//	IF NOT bToldFuckOff2
//		IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_FO", CONV_PRIORITY_AMBIENT_HIGH)
//			bToldFuckOff2 = TRUE
//		ENDIF
//	ENDIF
	
	IF bGunFight1 AND bGunFight0
		iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + 8500)
		SETTIMERA(0)
		thisEventStage = eS_GUN_FIGHT
	ENDIF
ENDPROC

PROC reactionsForAcknowledgingPlayer()
	FLOAT fPlayerKidnapperDist
	
	IF NOT IS_PED_INJURED(pedKidnapper[0])
	AND NOT IS_PED_INJURED(pedKidnapper[1])
		
		IF NOT bLookingAtPlayer0
			IF NOT bTurningToPlayer0
				OPEN_SEQUENCE_TASK(Seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedKidnapper[1], Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				bTurningToPlayer0 = TRUE
			ENDIF
			IF IS_PED_FACING_PED(pedKidnapper[1], PLAYER_PED_ID(), 90)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_GUY", CONV_PRIORITY_AMBIENT_HIGH)
				IF NOT IS_PED_INJURED(pedKidnapper[0])
				AND NOT IS_PED_INJURED(pedKidnapper[1])
					fPlayerKidnapperDist = GET_DISTANCE_BETWEEN_ENTITIES(pedKidnapper[0], PLAYER_PED_ID())
					IF fPlayerKidnapperDist > 5
						fPlayerKidnapperDist = (fPlayerKidnapperDist - 2)
					ELSE
						fPlayerKidnapperDist = (fPlayerKidnapperDist - 0.5)
					ENDIF
					DETACH_ENTITY(objSpade, FALSE)
					ATTACH_ENTITY_TO_ENTITY(objSpade, pedKidnapper[0], GET_PED_BONE_INDEX(pedKidnapper[0], BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, FALSE)
					CLEAR_PED_TASKS(pedKidnapper[0])
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						TASK_PLAY_ANIM(NULL, "random@burial", "a_burial_stop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 2000)
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedKidnapper[0], Seq)
					CLEAR_SEQUENCE_TASK(Seq)
					
					SET_CURRENT_PED_WEAPON(pedKidnapper[1], WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
					CLEAR_PED_TASKS(pedKidnapper[1])
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
	//					TASK_PLAY_ANIM(NULL, "random@burial", "c_burial_stop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 2000)
						TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerKidnapperDist, PEDMOVEBLENDRATIO_WALK)
//						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 3.5)//fPlayerKidnapperDist)
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedKidnapper[1], Seq)
					CLEAR_SEQUENCE_TASK(Seq)
					bLookingAtPlayer0 = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(objSpade)
			IF IS_ENTITY_ATTACHED(objSpade)
				IF IS_ENTITY_PLAYING_ANIM(pedKidnapper[0], "random@burial", "a_burial_stop")
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial_stop") > 0.355
						DETACH_ENTITY(objSpade)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bLookingAtPlayer0
		AND NOT bToldFuckOff
			IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_WHO", CONV_PRIORITY_AMBIENT_HIGH)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 3.5)//fPlayerKidnapperDist)
					TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedKidnapper[1], Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				TRIGGER_MUSIC_EVENT("RE6_START")
				bToldFuckOff = TRUE
			ENDIF
		ENDIF
		
		IF bToldFuckOff
		AND NOT bToldFuckOff2
			IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_WAR", CONV_PRIORITY_AMBIENT_HIGH)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[0], << 30, 30, 30 >>)
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[1], << 30, 30, 30 >>)
					TASK_AIM_GUN_AT_ENTITY(pedKidnapper[0], PLAYER_PED_ID(), -1)
				ENDIF
				bToldFuckOff2 = TRUE
				SETTIMERB(0)
			ENDIF
		ENDIF
		
		IF bToldFuckOff2
		AND NOT bToldShoot
			IF TIMERB() > 15000
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[0], << 30, 30, 30 >>)
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[1], << 30, 30, 30 >>)
					TASK_TURN_PED_TO_FACE_ENTITY(pedKidnapper[0], PLAYER_PED_ID())
					TASK_TURN_PED_TO_FACE_ENTITY(pedKidnapper[1], PLAYER_PED_ID())
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_SHO", CONV_PRIORITY_AMBIENT_HIGH)
					bToldShoot = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bLookingAtPlayer1
		AND bToldFuckOff
		AND NOT IS_PED_INJURED(pedKidnapper[0])
		AND NOT IS_PED_INJURED(pedKidnapper[1])
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[0], << 30, 30, 30 >>)
			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedKidnapper[1], << 30, 30, 30 >>)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_RANDOM_BOOL()
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_FO", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_ATT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
			ELSE
				OPEN_SEQUENCE_TASK(seq)
					TASK_GO_TO_ENTITY(NULL, pedKidnapper[1], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedKidnapper[1])
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
	//				TASK_CHAT_TO_PED(pedKidnapper[0], pedKidnapper[1], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
	//				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedKidnapper[0], seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_GO_TO_ENTITY(NULL, pedKidnapper[0], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedKidnapper[0])
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
	//				TASK_CHAT_TO_PED(pedKidnapper[1], pedKidnapper[0], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
	//				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedKidnapper[1], seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				SETTIMERB(15000)
				bGunFight0 = FALSE
				bToldFuckOff2 = TRUE
				bLookingAtPlayer1 = TRUE
			ENDIF
		ENDIF
		
	//	IF NOT bLookingAtPlayer1
	//	AND bToldShoot
	//		IF NOT IS_PED_INJURED(pedKidnapper[0])	
	//			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId1)
	//				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId1) > 0.9
	//					fPlayerKidnapperDist = GET_DISTANCE_BETWEEN_ENTITIES(pedKidnapper[0], PLAYER_PED_ID())
	//					IF fPlayerKidnapperDist > 5
	//						fPlayerKidnapperDist = (fPlayerKidnapperDist - 1)
	//					ELSE
	//						fPlayerKidnapperDist = (fPlayerKidnapperDist - 0.5)
	//					ENDIF
	//					SET_CURRENT_PED_WEAPON(pedKidnapper[0], WEAPONTYPE_PISTOL, FALSE)
	//					DETACH_ENTITY(objSpade, FALSE)
	//					ATTACH_ENTITY_TO_ENTITY(objSpade, pedKidnapper[0], GET_PED_BONE_INDEX(pedKidnapper[0], BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, FALSE)
	//					CLEAR_PED_TASKS(pedKidnapper[0])
	//					OPEN_SEQUENCE_TASK(Seq)
	//						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
	//						TASK_PLAY_ANIM(NULL, "random@burial", "a_burial_stop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
	//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
	//					CLOSE_SEQUENCE_TASK(Seq)
	//					TASK_PERFORM_SEQUENCE(pedKidnapper[0], Seq)
	//					CLEAR_SEQUENCE_TASK(Seq)
	//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedKidnapper[0])
	//					bLookingAtPlayer1 = TRUE
	//				ENDIF
	//			ENDIF
	//		ENDIF
	//	ENDIF
		
		
	//		IF NOT IS_PED_INJURED(pedKidnapper[1])
	//			continualTurnToFacePlayer(pedKidnapper[1], iTurnStage[1])
	//		ENDIF
			
	//		IF DOES_ENTITY_EXIST(objSpade)
	//			IF NOT IS_ENTITY_ATTACHED(objSpade)
	//				IF NOT IS_PED_INJURED(pedKidnapper[0])
	//					continualTurnToFacePlayer(pedKidnapper[0], iTurnStage[0])
	//				ENDIF
	//			ENDIF
	//		ENDIF
		
		IF bToldShoot //bLookingAtPlayer1
			fightDialogue()
			reactionsToPlayerShooting()
		ENDIF
		
		IF CAN_PED_SEE_HATED_PED(pedKidnapper[0], PLAYER_PED_ID())
		OR CAN_PED_SEE_HATED_PED(pedKidnapper[1], PLAYER_PED_ID())
		OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedKidnapper[0])
		OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedKidnapper[1])
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
			AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF NOT bWarnedBeforeShooting
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_GUN", CONV_PRIORITY_AMBIENT_HIGH)
							bWarnedBeforeShooting = TRUE
							reactionsToPlayerShooting()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
			AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF NOT bWarnedBeforeShooting
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_WEP", CONV_PRIORITY_AMBIENT_HIGH)
							bWarnedBeforeShooting = TRUE
							reactionsToPlayerShooting()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF (IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID()) OR IS_PED_SHOOTING(PLAYER_PED_ID()))
				reactionsToPlayerShooting()
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC reactionsToPlayerDamagingCar()
//	Temporary "What the fuck are you doing to my car?"
	IF NOT IS_PED_INJURED(pedKidnapper[0])
	AND NOT IS_PED_INJURED(pedKidnapper[1])
		IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedKidnapper[0])
		AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedKidnapper[1])
			IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_VH", CONV_PRIORITY_AMBIENT_HIGH)
				IF NOT IS_PED_SHOOTING(pedKidnapper[1])
					CLEAR_PED_TASKS(pedKidnapper[1])
					TASK_SHOOT_AT_ENTITY(pedKidnapper[1], PLAYER_PED_ID(), -1, FIRING_TYPE_CONTINUOUS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	thisEventStage = eS_GUN_FIGHT
ENDPROC

PROC reactionsToPlayerRammingPedsInCar()
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedKidnapper[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//		AND GET_ENTITY_HEALTH(pedKidnapper[0]) < 195
			CLEAR_PED_TASKS(pedKidnapper[0])
			TASK_SMART_FLEE_PED(pedKidnapper[0], PLAYER_PED_ID(), 1000, 60000)
			SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
			
			CLEAR_PED_TASKS(pedKidnapper[1])
			TASK_SHOOT_AT_ENTITY(pedKidnapper[1], PLAYER_PED_ID(), -1, FIRING_TYPE_CONTINUOUS)
			SET_PED_KEEP_TASK(pedKidnapper[1], TRUE)
//		ENDIF
//	ENDIF
	thisEventStage = eS_GUN_FIGHT
ENDPROC

PROC firstArgument()
	SWITCH iArgumentSpeechStage
		CASE 0
			IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CH1", CONV_PRIORITY_AMBIENT_HIGH)
				iArgumentSpeechStage ++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CH2", CONV_PRIORITY_AMBIENT_HIGH)
					iArgumentSpeechStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CH3", CONV_PRIORITY_AMBIENT_HIGH)
					iArgumentSpeechStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CH4", CONV_PRIORITY_AMBIENT_HIGH)
					iArgumentSpeechStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			iHostageSpeechOutburstTimer  = (GET_GAME_TIMER() + 3000)
			iArgumentSpeechStage ++
		BREAK
		CASE 5
		BREAK
	ENDSWITCH
ENDPROC

PROC standOffSituation()
	SWITCH iStandoffSpeech
		CASE 0
			IF NOT IS_PED_INJURED(pedKidnapper[1])
//				TASK_AIM_GUN_AT_ENTITY(pedKidnapper[1], PLAYER_PED_ID(), -1)
				IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_FO", CONV_PRIORITY_AMBIENT_HIGH)
	//				Temporary "You! Get the fuck away from here."
					SETTIMERA(0)
					iStandoffSpeech ++
				ENDIF
				fightDialogue()
			ELSE
				SETTIMERA(0)
				iStandoffSpeech ++
			ENDIF
		BREAK
		CASE 1
			IF TIMERA() > 2000
			OR IS_PED_SHOOTING(PLAYER_PED_ID())
				reactionsToPlayerShooting()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_OTHERWISE_INTERFERING_WITH_EVENT()
	IF NOT bAggroed
		
		IF IS_BULLET_IN_AREA(vKidnappersVehicle, 100)
		OR IS_BULLET_IN_AREA(vKidnappersVehicle, 100)
			IF IS_PED_INJURED(pedKidnapper[0])
			OR IS_PED_INJURED(pedKidnapper[1])
				interferenceType = interfere_CLOSE_BY
				RETURN TRUE
			ELSE
				IF NOT IS_BULLET_IN_AREA(vKidnappersVehicle, 100)
				OR NOT IS_BULLET_IN_AREA(vKidnappersVehicle, 100)
					interferenceType = interfere_CLOSE_BY
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_SMOKEGRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_GRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_STICKYBOMB, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_GRENADELAUNCHER, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_GRENADELAUNCHER_SMOKE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_RPG, TRUE)
		OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<< 194.3872, 6833.9409, 62.4419 >>, <<131.3547, 6845.6728, 15.4613 >>, 36.9375, WEAPONTYPE_MOLOTOV, TRUE)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, vKidnappersVehicle - << 100, 100, 100 >>, vKidnappersVehicle + << 100, 100, 100 >>)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, vKidnappersVehicle - << 100, 100, 100 >>, vKidnappersVehicle + << 100, 100, 100 >>)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, vKidnappersVehicle - << 100, 100, 100 >>, vKidnappersVehicle + << 100, 100, 100 >>)
			interferenceType = interfere_CLOSE_BY
			RETURN TRUE
		ENDIF
		
//		IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, vCreateVictim - << 5, 5, 5 >>, vCreateVictim + << 2, 2, 2 >>)
//			IF DOES_ENTITY_EXIST(pedVictim)
////				SET_ENTITY_HEALTH(pedVictim, 200)
//				IF NOT IS_PED_INJURED(pedVictim)
//					CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
//					SET_ENTITY_HEALTH(pedVictim, 0)
//				ELSE
////					IF NOT IS_ENTITY_DEAD(pedVictim)
//					IF DOES_ENTITY_EXIST(pedVictim)
//						IF IS_ENTITY_PLAYING_ANIM(pedVictim, "random@burial", "b_burial")
//							STOP_ANIM_TASK(pedVictim, "random@burial", "b_burial")
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF DOES_ENTITY_EXIST(vehKidnappersVeh)
			IF NOT IS_ENTITY_DEAD(vehKidnappersVeh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehKidnappersVeh, PLAYER_PED_ID())
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehKidnappersVeh)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehKidnappersVeh)
					reactionsToPlayerShooting()
					interferenceType = interfere_ATTACK_CAR
					RETURN TRUE
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK)
					OR IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK2)
						IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehKidnappersVeh)
							reactionsToPlayerShooting()
							interferenceType = interfere_ATTACK_CAR
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bInterfered
			IF NOT IS_PED_INJURED(pedKidnapper[0])
			AND NOT IS_PED_INJURED(pedKidnapper[1])
				IF CAN_PED_SEE_HATED_PED(pedKidnapper[0], PLAYER_PED_ID())
				OR CAN_PED_SEE_HATED_PED(pedKidnapper[1], PLAYER_PED_ID())
				OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedKidnapper[0])
				OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedKidnapper[1])
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vKidnappersVehicle - << 7, 0, 0 >>, << 20, 15, 35 >>)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vKidnappersVehicle - << 7, 0, 0 >>, << 28, 18, 35 >>)
						interferenceType = interfere_CLOSE_BY
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(pedKidnapper[0])
			AND NOT IS_PED_INJURED(pedKidnapper[1])
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF IS_ENTITY_TOUCHING_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedKidnapper[0])
					OR IS_ENTITY_TOUCHING_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedKidnapper[1])
						interferenceType = interfere_DRIVEN_AT
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedVictim)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID(), FALSE)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedVictim, WEAPONTYPE_STUNGUN)
//				MISSION_FAILED()
				interferenceType = interfere_HARM_VICTIM
				RETURN TRUE
			ENDIF
		ELSE
			interferenceType = interfere_HARM_VICTIM
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_DEAD(pedKidnapper[0])
		OR IS_ENTITY_DEAD(pedKidnapper[1])
			interferenceType = interfere_INSTANT_KILL
			RETURN TRUE
		ENDIF
		IF bInterfered
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC endDropOffSceneInVehicle()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghVictim, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, rghVictim)
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), 5, 2)
				IF NOT IS_PED_INJURED(pedVictim)
				AND IS_VEHICLE_DRIVEABLE(vehMafia)
					IF IS_PED_IN_GROUP(pedVictim)
						REMOVE_PED_FROM_GROUP(pedVictim)
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
					SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//						TASK_PAUSE(NULL, 1000)
						TASK_ENTER_VEHICLE(NULL, vehMafia, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkToGirl, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedVictim, Seq)
					CLEAR_SEQUENCE_TASK(Seq)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BACK", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
			
			WAIT(2700)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				VEHICLE_INDEX vehTempForScene
//				vehTempForScene = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//				WAIT(0)
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//				IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//					IF NOT IS_PED_INJURED(pedVictim)
//						REMOVE_PED_FROM_GROUP(pedVictim)
//						IF NOT IS_PED_SITTING_IN_VEHICLE(pedVictim, vehTempForScene)
//							SET_PED_INTO_VEHICLE(pedVictim, vehTempForScene, VS_FRONT_RIGHT)
//						ENDIF
//						TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 10000)
//						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, 10000)
//					ENDIF
//				ENDIF
//				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BACK", CONV_PRIORITY_AMBIENT_HIGH)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		
		CASE 1
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF NOT IS_VEHICLE_DOOR_DAMAGED(GET_PLAYERS_LAST_VEHICLE(), SC_DOOR_FRONT_RIGHT)
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(GET_PLAYERS_LAST_VEHICLE(), SC_DOOR_FRONT_RIGHT) > 0.1
			//			OR GET_VEHICLE_DOOR_ANGLE_RATIO(GET_PLAYERS_LAST_VEHICLE(), SC_DOOR_REAR_LEFT) > 0
			//			OR GET_VEHICLE_DOOR_ANGLE_RATIO(GET_PLAYERS_LAST_VEHICLE(), SC_DOOR_REAR_RIGHT) > 0
//							WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								WAIT(0)
//							ENDWHILE
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_DOOR0", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_DOOR1", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_DOOR2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			SET_CAM_ACTIVE(camCutscene,TRUE)
//			SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
//			SET_CAM_PARAMS(camCutscene, <<-134.200989,-31.355909,58.515556>>,<<-1.479176,-0.000000,-74.151367>>,30.428715)
//			SET_CAM_PARAMS(camCutscene, <<-134.075226,-31.231642,58.515556>>,<<0.372558,-0.000000,-77.074844>>,30.428715, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 1000)
			iDropOffCutStage ++
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(vehMafia)
				IF IS_PED_IN_VEHICLE(pedVictim, vehMafia)
					IF NOT IS_PED_INJURED(pedMafia)
						TASK_VEHICLE_DRIVE_WANDER(pedMafia, vehMafia, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						SET_PED_KEEP_TASK(pedMafia, TRUE)
					ENDIF
					SET_DISABLE_PRETEND_OCCUPANTS(vehMafia, TRUE)
//			IF NOT IS_PED_INJURED(pedMafia)
//			AND NOT IS_PED_INJURED(pedVictim)
//				CLEAR_PED_TASKS(pedMafia)
//				OPEN_SEQUENCE_TASK(Seq)
//					TASK_LOOK_AT_ENTITY(NULL, pedVictim, 8000, SLF_WHILE_NOT_IN_FOV)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim, 5000)
//					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
//				CLOSE_SEQUENCE_TASK(Seq)
//				TASK_PERFORM_SEQUENCE(pedMafia, Seq)
//				CLEAR_SEQUENCE_TASK(Seq)
//				SET_PED_KEEP_TASK(pedMafia, TRUE)
//			ENDIF
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedVictim)
//					TASK_LEAVE_ANY_VEHICLE(pedVictim, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 500)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_PED_INJURED(pedMafia)
//					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BACK", CONV_PRIORITY_AMBIENT_HIGH)
//					bDropOffDone = TRUE
//				ELSE
					bDropOffDone = TRUE
//				ENDIF
			ENDIF
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedVictim)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, vWalkToGirl, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 7500)
//					iDropOffCutStage ++
//				ENDIF
//			ENDIF
		BREAK
//		CASE 4
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				iDropOffCutStage ++
//			ENDIF
//		BREAK
//		CASE 5
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedVictim)
//			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
//			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			bDropOffDone = TRUE
//		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			iDropOffCutStage = 5
//		ENDIF
//	ENDIF
	
ENDPROC

PROC endDropOffSceneOnFoot()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			IF NOT IS_PED_INJURED(pedVictim)
			AND IS_VEHICLE_DRIVEABLE(vehMafia)
				IF IS_PED_IN_GROUP(pedVictim)
					REMOVE_PED_FROM_GROUP(pedVictim)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
				SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//					TASK_PAUSE(NULL, 1000)
					TASK_ENTER_VEHICLE(NULL, vehMafia, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkToGirl, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedVictim, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				SET_PED_KEEP_TASK(pedVictim, TRUE)
	//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
	//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
	//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -127.3029, -32.2337, 57.2064 >>)
	//			SET_ENTITY_HEADING(PLAYER_PED_ID(),  11.3182 )
	//			IF NOT IS_PED_INJURED(pedVictim)
	//				SET_ENTITY_COORDS(pedVictim, << -127.3677, -30.1165, 57.1958 >>)
	//				SET_ENTITY_HEADING(pedVictim, 249.1115)
	//				REMOVE_PED_FROM_GROUP(pedVictim)
	//				TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 10000)
	//				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, 10000)
	//			ENDIF
				CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BACK", CONV_PRIORITY_AMBIENT_HIGH)
//				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
			ENDIF
			iDropOffCutStage ++
		BREAK
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehMafia)
				IF IS_PED_IN_VEHICLE(pedVictim, vehMafia)
					IF NOT IS_PED_INJURED(pedMafia)
						TASK_VEHICLE_DRIVE_WANDER(pedMafia, vehKidnappersVeh, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						SET_PED_KEEP_TASK(pedMafia, TRUE)
					ENDIF
					SET_DISABLE_PRETEND_OCCUPANTS(vehMafia, TRUE)
					iDropOffCutStage ++
				ENDIF
			ENDIF
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			SET_CAM_ACTIVE(camCutscene,TRUE)
//			SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
//			SET_CAM_PARAMS(camCutscene, << -134.200989, -31.355909, 58.515556 >>, << -1.479176, -0.000000, -74.151367 >>, 30.428715)
//			SET_CAM_PARAMS(camCutscene, << -134.075226, -31.231642, 58.515556 >>, << 0.372558, -0.000000, -77.074844 >>, 30.428715, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedVictim)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, vWalkToGirl, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 3500)
					iDropOffCutStage ++
//				ENDIF
//			ENDIF
		BREAK
		CASE 3
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedMafia)
//				AND NOT IS_PED_INJURED(pedVictim)
//					CLEAR_PED_TASKS(pedMafia)
//					OPEN_SEQUENCE_TASK(Seq)
//						TASK_LOOK_AT_ENTITY(NULL, pedVictim, 8000, SLF_WHILE_NOT_IN_FOV)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim, 5000)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
//					CLOSE_SEQUENCE_TASK(Seq)
//					TASK_PERFORM_SEQUENCE(pedMafia, Seq)
//					CLEAR_SEQUENCE_TASK(Seq)
//					SET_PED_KEEP_TASK(pedMafia, TRUE)
//				ENDIF
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 4
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_PED_INJURED(pedMafia)
//					CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_BACK", CONV_PRIORITY_AMBIENT_HIGH)
//					iDropOffCutStage ++
//				ELSE
					iDropOffCutStage ++
//				ENDIF
			ENDIF
		BREAK
		CASE 5
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedVictim)
			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			MISSION_PASSED()
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			iDropOffCutStage = 4
//		ENDIF
//	ENDIF
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

//vInput = in_coords.vec_coord[0]
PRINTNL()
PRINTSTRING("re_burials launched")
PRINTVECTOR(in_coords.vec_coord[0])
PRINTNL()


IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_IN_GROUP(pedVictim)
			REMOVE_PED_FROM_GROUP(pedVictim)
		ENDIF
	ENDIF
	missionCleanup()
ENDIF

//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
IF CAN_RANDOM_EVENT_LAUNCH(in_coords.vec_coord[0])
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR bDrivingHome
//		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)

			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_BU")
			
			SWITCH ambStage
				CASE ambCanRun
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
						missionCleanup()
					ENDIF
					IF NOT bVariablesInitialised
						initialiseEventVariables()
					ELSE
						loadAssets()
					ENDIF
					
					IF bAssetsLoaded 
						bDoFullCleanUp = TRUE
						ambStage = (ambRunning)
					ENDIF
					
				BREAK
				
				CASE ambRunning
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
						SWITCH thisEventStage
						
							CASE eS_SET_UP_SCENE
								createScene()
								thisEventStage = eS_EVENT_STAGES
							BREAK
							
							CASE eS_EVENT_STAGES
								IF NOT HAS_PLAYER_AGGROED_PED(pedKidnapper[0], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
								AND NOT HAS_PLAYER_AGGROED_PED(pedKidnapper[1], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
								AND NOT IS_PLAYER_OTHERWISE_INTERFERING_WITH_EVENT()
									
									SWITCH thisSubStage
										CASE eSS_INITIAL_BURIAL_SEQ
											spadeDirtPfx()
//											IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 281.9148, 6816.3447, 4.3197 >>, << 48.3924, 6885.9213, 43.0037 >>, 226.5000)
											IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateVictim, << 50, 35, 50 >>)
												firstArgument()
											ENDIF
											IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateVictim, << 80, 65, 50 >>)
												pleadDialogue()
											ENDIF
											IF NOT IS_ENTITY_DEAD(vehKidnappersVeh)
												IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 281.9148, 6816.3447, 4.3197 >>, << 48.3924, 6885.9213, 43.0037 >>, 226.5000)
//												AND NOT IS_ENTITY_OCCLUDED(vehKidnappersVeh)
												OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vKidnappersVehicle - << 7, 0, 0 >>, << 20, 15, 50 >>)
													changeBlips()
													IF DOES_BLIP_EXIST(blipVictim)
														SHOW_HEIGHT_ON_BLIP(blipVictim, FALSE)
													ENDIF
													FOR i = 0 to 1
														IF DOES_BLIP_EXIST(blipKidnappers[i])
															SHOW_HEIGHT_ON_BLIP(blipKidnappers[i], FALSE)
														ENDIF
													ENDFOR
													IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
														SET_RANDOM_EVENT_ACTIVE()
													ENDIF
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
									
								ELSE
									changeBlips()
//									KILL_ANY_CONVERSATION()
									thisEventStage = eS_INTERVENTION_STAGES
								ENDIF
							BREAK
							
							CASE eS_INTERVENTION_STAGES
								IF HAS_PLAYER_AGGROED_PED(pedKidnapper[0], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
								OR HAS_PLAYER_AGGROED_PED(pedKidnapper[1], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
								OR bAggroed
								
									bAggroed = TRUE
									IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
										SET_RANDOM_EVENT_ACTIVE()
									ENDIF
									IF DOES_BLIP_EXIST(blipVictim)
										SHOW_HEIGHT_ON_BLIP(blipVictim, TRUE)
									ENDIF
									FOR i = 0 to 1
										IF DOES_BLIP_EXIST(blipKidnappers[i])
											SHOW_HEIGHT_ON_BLIP(blipKidnappers[i], TRUE)
										ENDIF
									ENDFOR
									
									SWITCH aggroReason
										CASE EAggro_Danger //player is wanted or shot a victim near them
											reactionsToPlayerShooting()
										BREAK
										CASE EAggro_ShotNear //shot near or near miss
											reactionsToPlayerShooting()
										BREAK
										CASE EAggro_HostileOrEnemy //stick up
											reactionsToPlayerShooting()
//											standOffSituation()
										BREAK
										CASE EAggro_Attacked //attacked
											reactionsToPlayerShooting()
										BREAK
										CASE EAggro_HeardShot
											reactionsToPlayerShooting()
										BREAK
										CASE EAggro_Shoved
											reactionsToPlayerShooting()
										BREAK
									ENDSWITCH
								ENDIF
								
								IF IS_PLAYER_OTHERWISE_INTERFERING_WITH_EVENT()
									bInterfered = TRUE
									IF DOES_BLIP_EXIST(blipVictim)
										SHOW_HEIGHT_ON_BLIP(blipVictim, TRUE)
									ENDIF
									FOR i = 0 to 1
										IF DOES_BLIP_EXIST(blipKidnappers[i])
											SHOW_HEIGHT_ON_BLIP(blipKidnappers[i], TRUE)
										ENDIF
									ENDFOR
									
									IF NOT IS_PLAYER_IN_ANY_THREATENING_VEHICLE()
										SWITCH interferenceType
											CASE interfere_CLOSE_BY
												reactionsForAcknowledgingPlayer()
											BREAK
											CASE interfere_ATTACK_CAR
												reactionsToPlayerDamagingCar()
											BREAK
											CASE interfere_HARM_VICTIM
												reactionsToAttackingVic()
											BREAK
											CASE interfere_DRIVEN_AT
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												WAIT(0)
												IF NOT IS_PED_INJURED(pedKidnapper[0])
													PLAY_PED_AMBIENT_SPEECH(pedKidnapper[0], "GENERIC_WAR_CRY")
												ELIF NOT IS_PED_INJURED(pedKidnapper[1])
													PLAY_PED_AMBIENT_SPEECH(pedKidnapper[1], "GENERIC_WAR_CRY")
												ENDIF
												reactionsToPlayerShooting()
	//											reactionsToPlayerRammingPedsInCar()
											BREAK
											CASE interfere_INSTANT_KILL
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												WAIT(0)
												IF NOT IS_PED_INJURED(pedKidnapper[0])
													PLAY_PED_AMBIENT_SPEECH(pedKidnapper[0], "GENERIC_SHOCKED_HIGH")
												ELIF NOT IS_PED_INJURED(pedKidnapper[1])
													PLAY_PED_AMBIENT_SPEECH(pedKidnapper[1], "GENERIC_SHOCKED_HIGH")
												ENDIF
												reactionsToPlayerShooting()
											BREAK
										ENDSWITCH
									ELSE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										INT index
										REPEAT COUNT_OF(pedKidnapper) index
											IF NOT IS_PED_INJURED(pedKidnapper[index])
												PLAY_PAIN(pedKidnapper[index], AUD_DAMAGE_REASON_SCREAM_SCARED)
//												CLEAR_PED_TASKS(pedKidnapper[index])
												TASK_SMART_FLEE_PED(pedKidnapper[index], PLAYER_PED_ID(), 1000, -1)
												SET_PED_KEEP_TASK(pedKidnapper[index], TRUE)
												IF DOES_ENTITY_EXIST(objSpade)
													IF IS_ENTITY_ATTACHED(objSpade)
														DETACH_ENTITY(objSpade)
													ENDIF
												ENDIF
												IF DOES_BLIP_EXIST(blipKidnappers[index])
													REMOVE_BLIP(blipKidnappers[index])
												ENDIF
											ENDIF
										ENDREPEAT
										thisEventStage = eS_PLAYER_CAN_HELP_VIC
									ENDIF
								ENDIF
							BREAK
							
							CASE eS_PLAYER_CAN_HELP_VIC
								IF NOT bHelpingVic
									helpDialogue()
//									closeDialogue()
								ENDIF
								IF IS_PLAYER_PLAYING(PLAYER_ID())
									playerHelpingVic()
								ENDIF
							BREAK
							
							CASE eS_DRIVE_GIRL_HOME
								isPlayerMessingAbout()
								
								IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWorldPoint, << 100, 100, 100 >>, TRUE)
									SET_WANTED_LEVEL_MULTIPLIER(1)
									IF DOES_ENTITY_EXIST(pedKidnapper[0])
									AND DOES_ENTITY_EXIST(pedKidnapper[1])
										SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[0])
										SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[1])
									ENDIF
								ENDIF
								IF NOT DOES_ENTITY_EXIST(pedMafia)
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropGirl, << 200, 200, 200 >>, TRUE)
										REQUEST_MODEL(S_M_M_HighSec_01)
										REQUEST_MODEL(Granger)
										IF HAS_MODEL_LOADED(S_M_M_HighSec_01)
										AND HAS_MODEL_LOADED(Granger)
											vehMafia = CREATE_VEHICLE(Granger, << -1155.0431, 942.9685, 197.4921 >>, 161.1247)
											SET_ENTITY_LOD_DIST(vehMafia, 200)
											SET_VEHICLE_LOD_MULTIPLIER(vehMafia, 1.5)
											pedMafia = CREATE_PED_INSIDE_VEHICLE(vehMafia, PEDTYPE_MISSION, S_M_M_HighSec_01)
											SET_PED_COMPONENT_VARIATION(pedMafia, PED_COMP_HEAD, 0, 2, 0) //(head)
											SET_PED_COMPONENT_VARIATION(pedMafia, PED_COMP_TORSO, 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(pedMafia, PED_COMP_LEG, 0, 0, 0) //(lowr)
											GIVE_WEAPON_TO_PED(pedMafia, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(pedMafia, rghVictim)
//											TASK_START_SCENARIO_IN_PLACE(pedMafia, "WORLD_HUMAN_SMOKING", 0, TRUE)
//											SET_PED_KEEP_TASK(pedMafia, TRUE)
//											ADD_PED_FOR_DIALOGUE(BurialSpeechStruct, 6, pedMafia, "INCESTBRO1")
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(pedVictim)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 1
											IF NOT bUnsuitableVehRemark
												MANAGE_CONVERSATION(FALSE)
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												WAIT(0)
//												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_WV", CONV_PRIORITY_AMBIENT_HIGH)
													bUnsuitableVehRemark = TRUE
//												ENDIF
											ENDIF
										ENDIF
									ELSE
										bUnsuitableVehRemark = FALSE
									ENDIF
									
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropGirl, g_vOnFootLocate, TRUE)
									AND IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), << 10, 10, 10 >>)
										IF IS_PED_IN_GROUP(pedVictim)
											IF DOES_BLIP_EXIST(blipGirlsDropOff)
												REMOVE_BLIP(blipGirlsDropOff)
											ENDIF
											IF DOES_BLIP_EXIST(blipCult)
												REMOVE_BLIP(blipCult)
											ENDIF
											
											IF IS_GIRL_IN_PLAYERS_VEHICLE()
//												iCutsceneWaitTimer = (GET_GAME_TIMER() + DEFAULT_CAR_STOPPING_TO_CUTSCENE)
												thisEventStage = eS_DROP_OFF_GIRL_IN_VEH
											ELSE
												thisEventStage = eS_DROP_OFF_GIRL_ON_FOOT
											ENDIF
										ENDIF
									ELSE
										groupSeparationCheck()
										stationaryCheck()
										onFootCheck()
										dontFollowPlayerInWrongVehicle()
										chatInterruptions()
										chatInVehicle()
										
										IF READY_FOR_CULT_DIALOGUE(vDropGirl)
											MANAGE_CONVERSATION(FALSE)
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_CULT", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
										IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
											MANAGE_CONVERSATION(FALSE)
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_NEAR", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE eS_DROP_OFF_GIRL_IN_VEH
								isPlayerMessingAbout()
								IF CAN_PLAYER_START_CUTSCENE()
									endDropOffSceneInVehicle()
								ENDIF
								IF bDropOffDone
									thisEventStage = eS_STAT_OUTCOME
								ENDIF
							BREAK
							
							CASE eS_DROP_OFF_GIRL_ON_FOOT
								isPlayerMessingAbout()
								IF CAN_PLAYER_START_CUTSCENE()
									endDropOffSceneOnFoot()
								ENDIF
								IF bDropOffDone
									thisEventStage = eS_STAT_OUTCOME
								ENDIF
							BREAK
							
							CASE eS_STAT_OUTCOME
								MISSION_PASSED()
							BREAK
							
							CASE eS_GUN_FIGHT
								fightDialogue()
								
								IF DOES_ENTITY_EXIST(objSpade)
									IF IS_ENTITY_ATTACHED(objSpade)
										IF NOT IS_PED_INJURED(pedKidnapper[0])
											IF IS_ENTITY_PLAYING_ANIM(pedKidnapper[0], "random@burial", "a_burial_stop")
												IF GET_ENTITY_ANIM_CURRENT_TIME(pedKidnapper[0], "random@burial", "a_burial_stop") > 0.355
													DETACH_ENTITY(objSpade)
												ENDIF
											ELSE
												DETACH_ENTITY(objSpade)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_PED_INJURED(pedKidnapper[0])
								OR IS_PED_INJURED(pedKidnapper[1])
									bOneGuyLeftLeave = TRUE
								ENDIF
								
								IF bOneGuyLeftLeave
									IF IS_VEHICLE_DRIVEABLE(vehKidnappersVeh)
									
										IF NOT IS_PED_INJURED(pedKidnapper[0])
											SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedKidnapper[0])
											IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedKidnapper[0], vehKidnappersVeh, VS_DRIVER)
											OR IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedKidnapper[0], vehKidnappersVeh, VS_FRONT_RIGHT)
												IF NOT bLastGuyTask
													CLEAR_PED_TASKS(pedKidnapper[0])
													IF DOES_ENTITY_EXIST(objSpade)
														IF IS_ENTITY_ATTACHED(objSpade)
															DETACH_ENTITY(objSpade)
														ENDIF
													ENDIF
													OPEN_SEQUENCE_TASK(Seq)
														TASK_CLEAR_LOOK_AT(NULL)
														TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnappersVeh, << -0.5, 0.5, 0 >>), PLAYER_PED_ID(), PEDMOVE_SPRINT, TRUE)
														TASK_ENTER_VEHICLE(NULL, vehKidnappersVeh, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
//														TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//														TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnappersVeh, << 199.5183, 6832.7974, 21.8374 >>, 30, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(vehKidnappersVeh), DRIVINGMODE_AVOIDCARS, 1, -1)
														TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehKidnappersVeh, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_PLOUGHTHROUGH, -1, 10)
													CLOSE_SEQUENCE_TASK(Seq)
													TASK_PERFORM_SEQUENCE(pedKidnapper[0], Seq)
													CLEAR_SEQUENCE_TASK(Seq)
													SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
													SET_DISABLE_PRETEND_OCCUPANTS(vehKidnappersVeh, TRUE)
													bLastGuyTask = TRUE
												ELSE
													IF GET_SCRIPT_TASK_STATUS(pedKidnapper[0], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
														IF GET_SEQUENCE_PROGRESS(pedKidnapper[0]) = 3
															IF NOT IS_ENTITY_AT_ENTITY(pedKidnapper[0], PLAYER_PED_ID(), << 10, 10, 10 >>)
															AND NOT IS_ENTITY_AT_COORD(pedKidnapper[0], vKidnappersVehicle, << 10, 10, 10 >>)
																SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[0])
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ELSE
												TASK_COMBAT_PED(pedKidnapper[0], PLAYER_PED_ID())
												SET_PED_KEEP_TASK(pedKidnapper[0], TRUE)
											ENDIF
										ENDIF
										
										IF NOT IS_PED_INJURED(pedKidnapper[1])
											SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedKidnapper[1])
											IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedKidnapper[1], vehKidnappersVeh, VS_DRIVER)
											OR IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedKidnapper[1], vehKidnappersVeh, VS_FRONT_RIGHT)
												IF NOT bLastGuyTask
													CLEAR_PED_TASKS(pedKidnapper[1])
													OPEN_SEQUENCE_TASK(Seq)
														TASK_CLEAR_LOOK_AT(NULL)
														TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnappersVeh, << -0.5, 0.5, 0 >>), PLAYER_PED_ID(), PEDMOVE_SPRINT, TRUE)
														TASK_ENTER_VEHICLE(NULL, vehKidnappersVeh, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
//														TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//														TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnappersVeh, << 199.5183, 6832.7974, 21.8374  >>, 30, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(vehKidnappersVeh), DRIVINGMODE_AVOIDCARS, 1, -1)
														TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehKidnappersVeh, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_PLOUGHTHROUGH, -1, 10)
													CLOSE_SEQUENCE_TASK(Seq)
													TASK_PERFORM_SEQUENCE(pedKidnapper[1], Seq)
													CLEAR_SEQUENCE_TASK(Seq)
													SET_PED_KEEP_TASK(pedKidnapper[1], TRUE)
													SET_DISABLE_PRETEND_OCCUPANTS(vehKidnappersVeh, TRUE)
													bLastGuyTask = TRUE
												ELSE
													IF GET_SCRIPT_TASK_STATUS(pedKidnapper[1], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
														IF GET_SEQUENCE_PROGRESS(pedKidnapper[1]) = 3
															IF NOT IS_ENTITY_AT_ENTITY(pedKidnapper[1], PLAYER_PED_ID(), << 10, 10, 10 >>)
															AND NOT IS_ENTITY_AT_COORD(pedKidnapper[1], vKidnappersVehicle, << 10, 10, 10 >>)
																SET_PED_AS_NO_LONGER_NEEDED(pedKidnapper[1])
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ELSE
												TASK_COMBAT_PED(pedKidnapper[1], PLAYER_PED_ID())
												SET_PED_KEEP_TASK(pedKidnapper[1], TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE AT_CULT
								//WAIT FOR ALTRUIST CULT TO DO ITS THING
							BREAK
						ENDSWITCH
						
						IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
						AND NOT IS_CULT_FINISHED()
						AND thisEventStage != AT_CULT
							IF NOT IS_PED_INJURED(pedVictim)
								IF IS_ENTITY_AT_COORD(pedVictim, vCult, << 5, 5, 5 >>)
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									WAIT(0)
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									thisEventStage = AT_CULT
								ENDIF
							ENDIF
						ENDIF
						IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
							IF DOES_ENTITY_EXIST(pedVictim)
								DELETE_PED(pedVictim)
							ENDIF
							MISSION_PASSED()
						ENDIF
						
						IF allPedsInjured()
							MISSION_FAILED()
						ENDIF
						
						blipRemoval()
//						replaceShovel()
						
						IF thisEventStage <> eS_PLAYER_CAN_HELP_VIC
							IF IS_PLAYER_ABLE_TO_HELP_HOSTAGE()
								TRIGGER_MUSIC_EVENT("RE6_BOTH_DEAD")
								IF NOT bCaptorsKilledSetUp
									SETTIMERA(0)
									iHostageSpeechOutburstTimer = (GET_GAME_TIMER() + 8500)
									bCaptorsKilledSetUp = TRUE
								ENDIF
								IF NOT IS_PED_INJURED(pedVictim)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 20, 20, 20 >>, FALSE, TRUE, TM_ON_FOOT)
										IF CREATE_CONVERSATION(BurialSpeechStruct, "REBU2AU", "REBU2_HPF", CONV_PRIORITY_AMBIENT_HIGH)
											thisEventStage = eS_PLAYER_CAN_HELP_VIC
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF thisEventStage = eS_EVENT_STAGES
						OR thisEventStage = eS_INTERVENTION_STAGES
						OR thisEventStage = eS_PLAYER_CAN_HELP_VIC
						OR thisEventStage = eS_GUN_FIGHT
							IF NOT IS_PED_INJURED(pedVictim)
							
								IF NOT bHelpingVic
	//								IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@burial", "b_burial")
	//								IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
									IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) != WAITING_TO_START_TASK
										CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
										sceneId = CREATE_SYNCHRONIZED_SCENE(vCreateVictim, << -2.000, -4.000, 18.000 >>)
										TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@burial", "b_burial", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[0], sceneId, "b_burial_legstie", "random@burial", SLOW_BLEND_IN, SLOW_BLEND_OUT)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(objRope[1], sceneId, "b_burial_wristtie", "random@burial", SLOW_BLEND_IN, SLOW_BLEND_OUT)
										SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
										SET_PED_KEEP_TASK(pedVictim, TRUE)
	//									SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
	//									SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
	//									SET_ENABLE_HANDCUFFS(pedVictim, FALSE)
	//									SET_ENABLE_BOUND_ANKLES(pedVictim, FALSE)
	//									TASK_PLAY_ANIM(pedVictim, "random@burial", "b_burial", INSTANT_BLEND_IN, INSTANT_BLEND_IN, -1, AF_NOT_INTERRUPTABLE|AF_DELAY_BLEND_WHEN_FINISHED)
	//									SET_PED_KEEP_TASK(pedVictim, TRUE)
									ENDIF
									IF IS_ENTITY_ON_FIRE(pedVictim)
										SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
										SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
										SET_ENTITY_HEALTH(pedVictim, 0)
									ENDIF
								ENDIF
								
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedVictim)
								//OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedVictim)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedVictim)
								OR iVictimStartHealth - GET_ENTITY_HEALTH(pedVictim) >= 50
									#IF IS_DEBUG_BUILD
										PRINTLN("pedVictim - SETTING PED HEALTH TO 0")
									#ENDIF
									
									SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
									SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
									SET_ENTITY_HEALTH(pedVictim, 0)
								ENDIF
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 5
	//									FREEZE_ENTITY_POSITION(pedVictim, TRUE)
									ELSE
	//									FREEZE_ENTITY_POSITION(pedVictim, FALSE)
										IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedVictim)
	//										CLEAR_PED_TASKS(pedVictim)
											SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
											SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
											SET_ENTITY_HEALTH(pedVictim, 0)
										ENDIF
									ENDIF
								ENDIF
								IF NOT IS_ENTITY_DEAD(vehKidnappersVeh)
									IF IS_ENTITY_TOUCHING_ENTITY(vehKidnappersVeh, pedVictim)
										SET_ENABLE_HANDCUFFS(pedVictim, TRUE)
										SET_ENABLE_BOUND_ANKLES(pedVictim, TRUE)
										SET_ENTITY_HEALTH(pedVictim, 0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						// debug warp
						#IF IS_DEBUG_BUILD
						
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
								MISSION_PASSED()
							ENDIF
							IF  IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
								MISSION_FAILED()
							ENDIF
							
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
								IF thisEventStage = eS_EVENT_STAGES
								OR thisEventStage = eS_INTERVENTION_STAGES
								OR thisEventStage = eS_PLAYER_CAN_HELP_VIC
								OR thisEventStage = eS_GUN_FIGHT
								
									INT index
									REPEAT COUNT_OF(pedKidnapper) index
										IF DOES_ENTITY_EXIST(pedKidnapper[index])
											DELETE_PED(pedKidnapper[index])
										ENDIF
									ENDREPEAT
									
									REMOVE_RELATIONSHIP_GROUP(rghKidnappers)
									
									IF DOES_ENTITY_EXIST(pedVictim)
										DELETE_PED(pedVictim)
									ENDIF
									IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
										SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
										SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 119.6647, 6832.1885, 16.4251 >>)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), 277.2985)
									ELSE
										SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 119.6647, 6832.1885, 16.4251 >>)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), 277.2985)
									ENDIF
									
									IF DOES_ENTITY_EXIST(vehKidnappersVeh)
										DELETE_VEHICLE(vehKidnappersVeh)
									ENDIF
									
									bGunFight0 = FALSE
									bGunFight1 = FALSE
									bLookingAtPlayer0 = FALSE
									bToldFuckOff = FALSE
									bToldFuckOff2 = FALSE
									bToldShoot = FALSE
									bLookingAtPlayer1 = FALSE
									bWarnedBeforeShooting = FALSE
									bAggroed = FALSE
									
									thisEventStage = eS_SET_UP_SCENE
								ENDIF
								
								IF thisEventStage = eS_DRIVE_GIRL_HOME
									IF NOT DOES_BLIP_EXIST(blipVictim)
										blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
									ENDIF
									IF DOES_BLIP_EXIST(blipGirlsDropOff)
										REMOVE_BLIP(blipGirlsDropOff)
									ENDIF
									IF NOT IS_PED_INJURED(pedVictim)
										IF IS_PED_IN_GROUP(pedVictim)
											REMOVE_PED_FROM_GROUP(pedVictim)
										ENDIF
										sceneId1 = CREATE_SYNCHRONIZED_SCENE(vCreateVictim - << 0, 0, 1 >>, << -2.000, -4.000, 18.000 >>)
										TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId1, "random@burial", "b_burial", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
										SET_PED_KEEP_TASK(pedVictim, TRUE)
									ENDIF
									SET_ENTITY_COORDS(PLAYER_PED_ID(), << 157.8200, 6839.9448, 19.0566 >>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 269.7481)
									
									bHelpingVic = FALSE
									bSetUpUntieScene = FALSE
									bSetUpUntieSpeech = FALSE
									bUntieSceneEnding = FALSE
									bCutsceneSkipped = FALSE
									bCleanUpUntieScene = FALSE
									bDrivingHome = FALSE
									bThankYouUntie = FALSE
									bInterfered = FALSE
									bCloseLine = FALSE
									bDropOffDone = FALSE
									bOneGuyLeftLeave = FALSE
									bLastGuyTask = FALSE
									bCaptorsKilledSetUp = FALSE
									
									iArgumentSpeechStage = 0
									iStandoffSpeech = 0
									iDropOffCutStage = 0
									
									thisEventStage = eS_PLAYER_CAN_HELP_VIC
								ENDIF
							ENDIF
							
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
								IF thisEventStage = eS_EVENT_STAGES
								OR thisEventStage = eS_INTERVENTION_STAGES
								OR thisEventStage = eS_PLAYER_CAN_HELP_VIC
								OR thisEventStage = eS_GUN_FIGHT
									INT index
									REPEAT COUNT_OF(pedKidnapper) index
										IF NOT IS_PED_INJURED(pedKidnapper[index])
											EXPLODE_PED_HEAD(pedKidnapper[index])
										ENDIF
									ENDREPEAT
									SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedVictim, FALSE))
								ENDIF
								
								IF thisEventStage = eS_DRIVE_GIRL_HOME
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF NOT IS_PED_INJURED(pedVictim)
											SET_PED_INTO_VEHICLE(pedVictim, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
										ENDIF
										IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1200.7468, 925.7507, 193.4938 >>, << 10, 10, 10 >>)
											SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vDropGirl)
											SET_ENTITY_HEADING(PLAYER_PED_ID(), 290.9852)
										ELSE
											SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1200.7468, 925.7507, 193.4938 >>)
											SET_ENTITY_HEADING(PLAYER_PED_ID(), 290.9852)
										ENDIF
									ELSE
										IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1200.7468, 925.7507, 193.4938 >>, << 10, 10, 10 >>)
											SET_ENTITY_COORDS(PLAYER_PED_ID(), vDropGirl)
											IF NOT IS_PED_INJURED(pedVictim)
												SET_ENTITY_COORDS(pedVictim, vDropGirl)
											ENDIF
										ELSE
											SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1200.7468, 925.7507, 193.4938 >>)
											SET_ENTITY_HEADING(PLAYER_PED_ID(), 290.9852)
											IF NOT IS_PED_INJURED(pedVictim)
												SET_ENTITY_COORDS(pedVictim, << -1200.7468, 925.7507, 193.4938 >>)
												SET_ENTITY_HEADING(pedVictim, 290.9852)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						#ENDIF
					
					ENDIF
				BREAK
			ENDSWITCH
//		ELSE
//			missionCleanup()
//		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE

ENDSCRIPT


//FUNC BOOL hasBurialsVariationSelectionFinished()
////    IF g_bHoldRandomEventForSelection
////        IF bRunDebugSelection
////            thisEvent = INT_TO_ENUM(eventVariations, iEventVariation)
////			
////            RETURN TRUE
////        ENDIF
////    ELSE
//		
//		IF
//			thisEvent = eV_MALE_VICTIM
//		ENDIF
//		IF
//			thisEvent = eV_FEMALE_VICTIM
//		ENDIF
//		
//         RETURN TRUE
//    ENDIF
//		
//    RETURN FALSE
//
//ENDFUNC
//FUNC BOOL UPDATE_RUN_DROPOFF_SCENE(DROPOFF& ThankYouStruct)
//
//	VECTOR CameraRotation
//	VECTOR vVehiclePosition
//	
//	SWITCH  ThankYouStruct.DoNotTouch.ThankState
//	
//		CASE EThanks_Start
//			IF GET_ENTITY_SPEED(tempPlayersVehID) < 2.0
//			
//				vVehiclePosition = GET_ENTITY_COORDS(tempPlayersVehID)
//				CLEAR_AREA_OF_VEHICLES(vVehiclePosition, 250)
//				CLEAR_AREA_OF_PEDS(vVehiclePosition, 25)
//				
//				CameraRotation = vVehiclePosition
//	//			CameraRotation.x -= 0.75
//				CameraRotation.y += 0.1
//				CameraRotation.z += 0.8
//				
//				ThankYouStruct.DoNotTouch.CameraSet.CamFov = 37
//				ThankYouStruct.DoNotTouch.CameraSet.ShotTime = 20000
//				
//				RUN_CAMERA_ATTACH_CAR(ThankYouStruct.DoNotTouch.CameraSet, tempPlayersVehID, <<1, 3, 1>>,CameraRotation, FALSE)
//				SET_CUTSCENE_RUNNING(TRUE)
//				
//				IF NOT IS_PED_INJURED(pedVictim)
//					CLEAR_PED_TASKS(pedVictim)
//					TASK_LEAVE_VEHICLE(pedVictim, tempPlayersVehID, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
//				ENDIF
//				ThankYouStruct.DoNotTouch.ThankState = EThanks_Follow
//			ENDIF
//		
//		BREAK
//		
//		CASE EThanks_Follow
//			IF NOT IS_PED_INJURED(pedVictim)
//				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, 4000) 
//				IF NOT IS_PED_IN_VEHICLE(pedVictim, tempPlayersVehID)
//					IF IS_STRING_NULL(ThankYouStruct.ThanksSpeech)
//						//Context speech!
//						PRINT_STRING_WITH_LITERAL_STRING("STRING", "Thank you!", 5000, 1)
//					ELSE
//						CREATE_CONVERSATION(ThankYouStruct.SpeechBlock, ThankYouStruct.SubtitleBlock, ThankYouStruct.ThanksSpeech, CONV_PRIORITY_AMBIENT_HIGH)
//					ENDIF
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//						TASK_PLAY_ANIM(NULL, ThankYouStruct.AnimDict, ThankYouStruct.AnimName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGirlWalkTo, PEDMOVE_WALK)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedVictim, seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					SET_PED_KEEP_TASK(pedVictim, TRUE)
//					SETTIMERA(0)
//					ThankYouStruct.DoNotTouch.ThankState = EThanks_ResetAnimSet
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE EThanks_ResetAnimSet
//			IF NOT IS_PED_INJURED(pedVictim)
//				IF TIMERA() > 3500
//					IF NOT IS_ENTITY_DEAD(tempPlayersVehID)
//						SET_CUTSCENE_RUNNING(FALSE)
//						SET_VEHICLE_DOORS_SHUT(tempPlayersVehID)
//						SET_ENTITY_VISIBLE(pedVictim, FALSE)
//						CLEAR_PRINTS()
//						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//						ThankYouStruct.DoNotTouch.ThankState = EThanks_WrapUp
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE EThanks_WrapUp
//			ThankYouStruct.bSuccess = TRUE
//			RETURN TRUE
//		BREAK
//	
//	ENDSWITCH
//	
//	RETURN TRUE
//ENDFUNC
