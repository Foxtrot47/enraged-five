//|=======================================================================================|
//|                                											  Prisoner Lift

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES =====================================

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "AggroSupport.sch"
USING "Ambient_Approach.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "ambient_common.sch"
USING "taxi_functions.sch"
USING "rc_helper_functions.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

//================================ CONSTANTS AND VARIABLES ================================

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM MAIN_SECTIONS
	MISSION_STAGES,
	AGGRO_REACTIONS
ENDENUM
MAIN_SECTIONS thisSection = MISSION_STAGES

ENUM RUNNING_STAGE_ENUM
	SETUP_STAGE,
	STAGE_1,
	STAGE_2,
	STAGE_3,
	STAGE_4,
	AT_DESTINATION_IN_VEH,
	AT_DESTINATION_ON_FOOT,
	POST_DROPOFF_1,
	POST_DROPOFF_2,
	NEAR_POLICE_STATION,
	LOSING_POLICE,
	HIJACKING_THE_PLAYER,
	MAKE_A_RUN,
	ENDING_EVENT
ENDENUM
RUNNING_STAGE_ENUM eventStage = SETUP_STAGE

ENUM VARIATION_ENUM
	BIKER_GANG_LIFT,
	HIJACK_PLAYER
ENDENUM
VARIATION_ENUM thisVariation = BIKER_GANG_LIFT

BOOL bAssetsLoaded = FALSE
BOOL bVariablesInitialised = FALSE

VECTOR vInput
INT iWhichWorldPoint
VECTOR vAmbWorldPoint1 = << 1492.31, 2136.53, 89.15 >>
VECTOR vAmbWorldPoint2 = << 1408.2397, 2522.8027, 41.0419 >>
CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
INT iSelectedVariation

BLIP_INDEX blipLocation
BLIP_INDEX blipInitialMarker

PED_INDEX pedPrisoner
REL_GROUP_HASH rghPrisoner

SEQUENCE_INDEX seq

VEHICLE_INDEX tempPlayersVehID
VECTOR vCreatePrisonerPos

VEHICLE_INDEX vehBrokenDownVeh
VEHICLE_INDEX vehTowed
ENTITY_INDEX entTowed

PED_INDEX pedTempCop[2]
VEHICLE_INDEX vehTempCop

VECTOR vPrisonerDestinationPos
FLOAT fCreatePrisonerHdg
//VECTOR vPrisonerWalkToPos
MODEL_NAMES modelPrisoner
VECTOR vDropOffWalkToPos

INT iBitFieldDontCheck = 0
//INT iLockOnTimer
EAggro aggroReason
//BOOL bAggroed

BOOL bLeavingActivationArea
//BOOL bLookAtCar = FALSE

BOOL bJumpInSpeech
BOOL bLiftSpeech
//BOOL bApproachPlayersVeh = FALSE
BOOL bHailingDown
//BOOL bShootAt = FALSE
BOOL bPoliceSpawned
BOOL bInCombat
BOOL bAskDialoguePlayed
BOOL bStealDialoguePlayed
BOOL bThankingDone
//BOOL bFreaked = FALSE
//BOOL bPlaceholderLine = FALSE

BLIP_INDEX blipPrisoner
//VECTOR vInitialMarkerPos

INT iFreakedStage = 0

INT iTurnStage 

//CAMERA_INDEX camFight
//VECTOR vCamcoords
INT iApproachStage

FLOAT fPlayerPrisonerDist

BOOL bAskedForVeh
BOOL bAskedForVehAgain
BOOL bClearHailingDown
BOOl bVehicleImmobilized

VECTOR vPoliceStation1 = << 370.257629,-1597.488281,35.949543 >>
VECTOR vPoliceStation2 = << 824.992004,-1289.266846,27.245644 >>
VECTOR vPoliceStation3 = << 450.188873,-981.675415,42.691738 >>
VECTOR vPoliceStation4 = << -1088.369751,-842.291077,30.275543 >>
VECTOR vPoliceStation5 = << 608.907593,0.806411,100.249680 >>
VECTOR vPoliceStation6 = << -562.015869,-130.811310,37.436901 >>
VECTOR vPoliceStation7 = << 1855.237061,3683.236084,33.291649 >>
VECTOR vPoliceStation8 = << -443.506348,6016.230957,30.717875 >>
VECTOR vPrison = << 1693.518066, 2579.209717, 44.957134 >>

BOOL bDoFullCleanUp

BOOL bStartOnFootTimer
INT iOnFootStartTimer
INT iOnFootCurrentTimer
BOOL bStartStationaryTimer
INT iStationaryStartTimer
INT iStationaryCurrentTimer

//APPROACHPLAYER DoThanking

structPedsForConversation PrisonerLiftDialogueStruct 
//============================ GENERAL FUNCTIONS AND PROCEDURES ===========================

// Mission Cleanup
PROC missionCleanup()
	PRINTLN("@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@")
	
	IF bDoFullCleanUp
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		KILL_FACE_TO_FACE_CONVERSATION()
		
		DISABLE_TAXI_HAILING(FALSE)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreatePrisonerPos - << 10, 10, 10 >>, vCreatePrisonerPos + << 10, 10, 10 >>, TRUE)
//		CLEAR_AREA(vCreatePrisonerPos, 10, FALSE)
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<1449.851318,2452.661377,81.565575>>, <<1203.850464,2579.351074,35.154903>>, 224.125000)
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedPrisoner)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
			IF thisVariation = BIKER_GANG_LIFT
				SET_PED_CONFIG_FLAG(pedPrisoner, PCF_CanSayFollowedByPlayerAudio, TRUE)
				IF IS_PED_INJURED(PLAYER_PED_ID())
					OPEN_SEQUENCE_TASK(seq)
						TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
						TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedPrisoner, TRUE)
				ENDIF
			ELIF thisVariation = HIJACK_PLAYER
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					OPEN_SEQUENCE_TASK(seq)
						TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
						TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedPrisoner, TRUE)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
						OPEN_SEQUENCE_TASK(seq)
							TASK_ENTER_VEHICLE(NULL, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_RESUME_IF_INTERRUPTED|ECF_JACK_ANYONE)
							TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedPrisoner, TRUE)
					ELSE
						OPEN_SEQUENCE_TASK(seq)
							TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
							TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedPrisoner, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, TRUE)
		REMOVE_SCENARIO_BLOCKING_AREAS()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF can_cleanup_script_for_debug
			IF DOES_ENTITY_EXIST(pedPrisoner)
				DELETE_PED(pedPrisoner)
			ENDIF
		ENDIF
 	#ENDIF
	
	RANDOM_EVENT_OVER()
	
////////////////////	SET_VFX_IGNORE_CAM_DIST_CHECK(FALSE)
	
	TERMINATE_THIS_THREAD()

ENDPROC

PROC MISSION_PASSED()
	PRINTLN("@@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@")
	IF NOT IS_PED_INJURED(pedPrisoner)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
	ENDIF
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	IF thisVariation =  BIKER_GANG_LIFT
		INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, 3)
	ENDIF
	RANDOM_EVENT_PASSED(RE_PRISONERLIFT, iSelectedVariation)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTLN("@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@")
	missionCleanup()
ENDPROC

PROC changeBlips()

	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipPrisoner)
		blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner)
		SHOW_HEIGHT_ON_BLIP(blipPrisoner, FALSE)
	ENDIF
	
ENDPROC

FUNC VECTOR getWorldPoint()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint1)
		iWhichWorldPoint = iWorldPoint1
		
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) 
			iWhichWorldPoint = iWorldPoint2
		ENDIF
//		
//		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) < fDistanceToClosestWorldPoint
//			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) 
//			iWhichWorldPoint = iWorldPoint3
//		ENDIF
		
		IF iWhichWorldPoint = iWorldPoint1
			iSelectedVariation = iWorldPoint1
			thisVariation =  BIKER_GANG_LIFT
			RETURN vAmbWorldPoint1
		ENDIF
		IF iWhichWorldPoint = iWorldPoint2
			iSelectedVariation = iWorldPoint2
			thisVariation = HIJACK_PLAYER 
			RETURN vAmbWorldPoint2
		ENDIF
//		IF iWhichWorldPoint = iWorldPoint3
//			RETURN vAmbWorldPoint3
//		ENDIF
		
	ENDIF
	
	RETURN << 0, 0, 0 >>
	
ENDFUNC

PROC initialiseEventVariables()
	
	IF iWhichWorldPoint = iWorldPoint1
		modelPrisoner = G_M_Y_Lost_02
		vCreatePrisonerPos = << 1538.0898, 2172.3438, 77.8260>>
		fCreatePrisonerHdg = 142.0201
//		vInitialMarkerPos = << 1540.5347, 2167.9949, 77.8019 >>
	//	vPrisonerWalkToPos =  << 1936.0112, -753.5767, 85.3142 >>
		vDropOffWalkToPos = << 984.3975, -106.9935, 73.3531 >>
		vPrisonerDestinationPos = << 960.1481, -139.4141, 73.4760 >>
	ENDIF
	
	IF iWhichWorldPoint = iWorldPoint2
		modelPrisoner = S_M_Y_PRISONER_01
		vCreatePrisonerPos = << 1401.6923, 2528.0850, 40.3762>>
		fCreatePrisonerHdg = 301.2458
//		vInitialMarkerPos = << 1411.2563, 2524.2654, 40.9778 >>
	//	vPrisonerWalkToPos = << -197.1860, 6536.4854, 10.0975 >>
		vDropOffWalkToPos = << 938.5384, -2167.4197, 29.5153 >>
		vPrisonerDestinationPos = << 928.9882, -2173.2480, 29.2873 >>
	ENDIF
	bVariablesInitialised = TRUE
ENDPROC

PROC loadAssets()

	REQUEST_MODEL(modelPrisoner)
	REQUEST_ANIM_DICT("random@prisoner_lift")
	IF thisVariation = BIKER_GANG_LIFT
		REQUEST_MODEL(S_M_Y_Ranger_01)
		REQUEST_MODEL(SHERIFF)
		REQUEST_MODEL(RATLOADER)
		REQUEST_MODEL(BFINJECTION)
	ENDIF
	IF HAS_MODEL_LOADED(modelPrisoner)
	AND HAS_ANIM_DICT_LOADED("random@prisoner_lift")
		IF thisVariation = BIKER_GANG_LIFT
			IF HAS_MODEL_LOADED(S_M_Y_Ranger_01)
			AND HAS_MODEL_LOADED(SHERIFF)
			AND HAS_MODEL_LOADED(RATLOADER)
			AND HAS_MODEL_LOADED(BFINJECTION)
				bAssetsLoaded = TRUE
			ENDIF
		ELSE
			bAssetsLoaded = TRUE
		ENDIF
	ELSE
		REQUEST_MODEL(modelPrisoner)
		REQUEST_ANIM_DICT("random@prisoner_lift")
		IF thisVariation = BIKER_GANG_LIFT
			REQUEST_MODEL(S_M_Y_Ranger_01)
			REQUEST_MODEL(SHERIFF)
			REQUEST_MODEL(RATLOADER)
			REQUEST_MODEL(BFINJECTION)
		ENDIF
	ENDIF
	
ENDPROC

PROC createScene()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
//	ADD_SCENARIO_BLOCKING_AREA(<< 939.3091, -159.8780, 67.9425 >>, << 1019.5811, -79.9157, 88.0416 >>)
	
	pedPrisoner = CREATE_PED(PEDTYPE_MISSION, modelPrisoner, vCreatePrisonerPos, fCreatePrisonerHdg)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, TRUE)
	SET_PED_CONFIG_FLAG(pedPrisoner, PCF_PreventPedFromReactingToBeingJacked, TRUE)
	SET_PED_CONFIG_FLAG(pedPrisoner, PCF_RemoveDeadExtraFarAway, TRUE)
	ADD_RELATIONSHIP_GROUP("PedPrisoner", rghPrisoner)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedPrisoner, rghPrisoner)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rghPrisoner)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghPrisoner, RELGROUPHASH_COP)
//	blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(pedPrisoner)
//	changeBlips()
	
	TASK_PLAY_ANIM(pedPrisoner, "random@prisoner_lift", "loop2_idlelook2", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
	//TASK_START_SCENARIO_AT_POSITION(pedPrisoner, "THUMBING_LIFT", vCreatePrisonerPos, fCreatePrisonerHdg)
	SET_PED_MONEY(pedPrisoner, 0)
//	SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedPrisoner, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedPrisoner, CA_PLAY_REACTION_ANIMS, FALSE)
	SET_PED_FLEE_ATTRIBUTES(pedPrisoner, FA_DISABLE_HANDS_UP, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedPrisoner, FA_USE_VEHICLE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedPrisoner, FA_USE_COVER, FALSE)
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Attacked))
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Danger))
	
	IF thisVariation = BIKER_GANG_LIFT
		SET_AMBIENT_VOICE_NAME(pedPrisoner, "REPRI1Lost")
		ADD_PED_FOR_DIALOGUE(PrisonerLiftDialogueStruct, 3, pedPrisoner, "REPRI1Lost")
		CLEAR_AREA_OF_PEDS(vCreatePrisonerPos, 50)
		CLEAR_AREA(vCreatePrisonerPos, 10, FALSE)
		ADD_SCENARIO_BLOCKING_AREA(vCreatePrisonerPos - << 50, 50, 50 >>, vCreatePrisonerPos + << 50, 50, 50 >>)
		ADD_SCENARIO_BLOCKING_AREA(vPrisonerDestinationPos - << 10, 10, 10 >>, vPrisonerDestinationPos + << 10, 10, 10 >>)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreatePrisonerPos - << 10, 10, 10 >>, vCreatePrisonerPos + << 10, 10, 10 >>, FALSE)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghPrisoner, RELGROUPHASH_AMBIENT_GANG_LOST)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_LOST, rghPrisoner)
		SET_PED_COMBAT_ATTRIBUTES(pedPrisoner, CA_ALWAYS_FLEE, TRUE)
		vehBrokenDownVeh = CREATE_VEHICLE(SHERIFF, << 1524.3323, 2173.3474, 79.0619 >>, 201.6806)
		SET_VEHICLE_ENGINE_HEALTH(vehBrokenDownVeh, 0)
		SET_VEHICLE_DOOR_OPEN(vehBrokenDownVeh, SC_DOOR_BONNET)
		SET_VEHICLE_TYRE_BURST(vehBrokenDownVeh, SC_WHEEL_CAR_FRONT_LEFT)
		SET_VEHICLE_INFLUENCES_WANTED_LEVEL(vehBrokenDownVeh, FALSE)
//		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBrokenDownVeh, FALSE)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<< 1525.5659, 2172.5120, 80.1985 >>, << 1524.4229, 2173.8943, 80.1342 >>, 100, TRUE)
		
		PED_INDEX pedCop
		VEHICLE_INDEX vehTemp[2]
		pedCop = CREATE_PED_INSIDE_VEHICLE(vehBrokenDownVeh, PEDTYPE_MISSION, S_M_Y_Ranger_01, VS_DRIVER)
		SET_PED_CONFIG_FLAG(pedCop, PCF_DisablePoliceInvestigatingBody, TRUE)
		SET_ENTITY_HEALTH(pedCop, 99)
		SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(pedCop, TRUE)
		vehTemp[0] = CREATE_VEHICLE(RATLOADER, << 1527.4001, 2240.3298, 73.8459 >>, 216.8133)
		vehTemp[1] = CREATE_VEHICLE(BFINJECTION, << 1594.0929, 2198.6572, 77.8709 >>, 85.3536)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTemp[0])
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTemp[1])
		
	ELIF thisVariation = HIJACK_PLAYER
		SET_ROADS_IN_ANGLED_AREA(<<1449.851318,2452.661377,81.565575>>, <<1203.850464,2579.351074,35.154903>>, 224.125000, FALSE, FALSE)
		SET_AMBIENT_VOICE_NAME(pedPrisoner, "G_M_Y_STREETPUNK02_BLACK_MINI_04")
		ADD_PED_FOR_DIALOGUE(PrisonerLiftDialogueStruct, 3, pedPrisoner, "REPRI2Prisoner")
		GIVE_WEAPON_TO_PED(pedPrisoner, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(PrisonerLiftDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(PrisonerLiftDialogueStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(PrisonerLiftDialogueStruct, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	
//	WAIT(1)
//	IF NOT IS_ENTITY_DEAD(vehBrokenDownVeh)
//		SET_HORN_PERMANENTLY_ON_TIME(vehBrokenDownVeh, 600000)
//	ENDIF
	eventStage = STAGE_1
ENDPROC

FUNC BOOL IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), TRUE)
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	
	IF NOT IS_PED_INJURED(pedPrisoner)
	
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPrisoner, GET_PLAYERS_LAST_VEHICLE())
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPrisoner, PLAYER_PED_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			RETURN TRUE
		ENDIF
		
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 250, 250, 250 >>)
			RETURN TRUE
		ENDIF
		
		IF thisVariation = BIKER_GANG_LIFT
			INT i = 0
			IF Has_Ped_Been_Injured()
				REPEAT Get_Number_Of_Ped_Injured_Events() i
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Get_Index_Of_Injured_Ped(i), PLAYER_PED_ID(), FALSE)
						IF GET_PED_RELATIONSHIP_GROUP_HASH(GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Injured_Ped(i))) = RELGROUPHASH_AMBIENT_GANG_LOST
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		
		IF eventStage < STAGE_2
			
			IF HAS_PED_RECEIVED_EVENT(pedPrisoner, EVENT_SHOT_FIRED|EVENT_SHOT_FIRED_WHIZZED_BY)
	//			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedPrisoner, 50)
				RETURN TRUE
			ENDIF
			
			IF thisVariation = BIKER_GANG_LIFT
				IF DOES_ENTITY_EXIST(vehBrokenDownVeh)
//					DRAW_DEBUG_BOX(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 45, 0, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 45, 0, 0 >> + << 40, 40, 40 >>)
//					DRAW_DEBUG_BOX(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 45, 0, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 45, 0, 0 >> + << 40, 40, 40 >>)
//					DRAW_DEBUG_BOX(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 0, 45, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 0, 45, 0 >> + << 40, 40, 40 >>)
//					DRAW_DEBUG_BOX(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 0, 45, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 0, 45, 0 >> + << 40, 40, 40 >>)
					IF IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 45, 0, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 45, 0, 0 >> + << 40, 40, 40 >>)
					OR IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 45, 0, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 45, 0, 0 >> + << 40, 40, 40 >>)
					OR IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 0, 45, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) + << 0, 45, 0 >> + << 40, 40, 40 >>)
					OR IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 0, 45, 0 >> - << 40, 40, 40 >>, GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE) - << 0, 45, 0 >> + << 40, 40, 40 >>)
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@ IS_COP_PED_IN_AREA_3D @@@@@@@@@@@@@@@@@\n")
						TASK_SMART_FLEE_PED(pedPrisoner, PLAYER_PED_ID(), 1000, -1)
						SET_PED_KEEP_TASK(pedPrisoner, TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ELIF thisVariation = HIJACK_PLAYER
				IF IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(pedPrisoner, FALSE) - << 70, 70, 70 >>, GET_ENTITY_COORDS(pedPrisoner, FALSE) + << 70, 70, 70 >>)
					PRINTSTRING("\n@@@@@@@@@@@@@@@@@ IS_COP_PED_IN_AREA_3D @@@@@@@@@@@@@@@@@\n")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedPrisoner), 20)
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@ IS_BULLET_IN_AREA @@@@@@@@@@@@@@@@@\n")
				RETURN TRUE
			ENDIF
			
			IF IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedPrisoner) - << 15, 15, 15 >>, GET_ENTITY_COORDS(pedPrisoner) + << 15, 15, 15 >>, WEAPONTYPE_SMOKEGRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedPrisoner) - << 15, 15, 15 >>, GET_ENTITY_COORDS(pedPrisoner) + << 15, 15, 15 >>, WEAPONTYPE_GRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedPrisoner) - << 15, 15, 15 >>, GET_ENTITY_COORDS(pedPrisoner) + << 15, 15, 15 >>, WEAPONTYPE_GRENADELAUNCHER, TRUE)
//			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedPrisoner) - << 15, 15, 15 >>, GET_ENTITY_COORDS(pedPrisoner) + << 15, 15, 15 >>, WEAPONTYPE_STICKYBOMB, TRUE)
				RETURN TRUE
			ENDIF
			
			IF GET_IS_PETROL_DECAL_IN_RANGE(GET_ENTITY_COORDS(pedPrisoner), 3)
				RETURN TRUE
			ENDIF
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				RETURN TRUE
			ENDIF
			
//			IF thisVariation = HIJACK_PLAYER
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedPrisoner, PLAYER_PED_ID())
				AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedPrisoner)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedPrisoner)
						RETURN TRUE
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
	ELSE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(VEHICLE_INDEX TheVehicle)

	IF IS_VEHICLE_STUCK_TIMER_UP(TheVehicle, VEH_STUCK_ON_ROOF, 3000)
	OR IS_ENTITY_ON_FIRE(TheVehicle)
	OR GET_NUMBER_OF_FIRES_IN_RANGE(GET_ENTITY_COORDS(TheVehicle, FALSE), 5) > 0
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TheVehicle, WEAPONTYPE_MOLOTOV)
	OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(TheVehicle, FALSE) - << 5, 5, 5 >>, GET_ENTITY_COORDS(TheVehicle, FALSE) + << 5, 5, 5 >>, WEAPONTYPE_MOLOTOV)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC continualTurnToFacePlayer(INT& stage)
	
	SWITCH stage
		CASE 0 // TURN TASK
			IF NOT IS_PED_INJURED(pedPrisoner)
				IF NOT IS_PED_FACING_PED(pedPrisoner, PLAYER_PED_ID(), 90)
				//	CLEAR_PED_TASKS(targetPed)
	//				OPEN_SEQUENCE_TASK(Seq)
	//					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
	//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
	//				CLOSE_SEQUENCE_TASK(Seq)
	//				TASK_PERFORM_SEQUENCE(pedCelebGirl, Seq)
	//				CLEAR_SEQUENCE_TASK(Seq)
					TASK_TURN_PED_TO_FACE_ENTITY(pedPrisoner, PLAYER_PED_ID())
					stage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1 // TURNING 
			IF NOT IS_PED_INJURED(pedPrisoner)
				IF IS_PED_FACING_PED(pedPrisoner, PLAYER_PED_ID(), 90)
					// ACHIEVED TASK
					stage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2 // WAITING TO TURN
			IF NOT IS_PED_INJURED(pedPrisoner)
				IF NOT IS_PED_FACING_PED(pedPrisoner, PLAYER_PED_ID(), 90)
					stage = 0
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

FUNC BOOL GET_PLAYER_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		tempPlayersVehID = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC groupSeparationCheck()

	IF NOT IS_PED_INJURED(pedPrisoner)
//		IF NOT IS_PED_IN_GROUP(pedPrisoner)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 8, 8, 8 >>)
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_PED_IN_VEHICLE(pedPrisoner, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF DOES_BLIP_EXIST(blipPrisoner)
								REMOVE_BLIP(blipPrisoner)
							ENDIF
							IF eventStage != LOSING_POLICE
								IF NOT DOES_BLIP_EXIST(blipLocation)				
									blipLocation = CREATE_BLIP_FOR_COORD(vPrisonerDestinationPos, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipPrisoner)
							REMOVE_BLIP(blipPrisoner)
						ENDIF
						IF eventStage != LOSING_POLICE
							IF NOT DOES_BLIP_EXIST(blipLocation)				
								blipLocation = CREATE_BLIP_FOR_COORD(vPrisonerDestinationPos, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipLocation)
						REMOVE_BLIP(blipLocation)
					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipPrisoner)
					blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner)
				ENDIF
				IF DOES_BLIP_EXIST(blipLocation)
					REMOVE_BLIP(blipLocation)
				ENDIF
			ENDIF
//		ENDIF
	ENDIF

ENDPROC

PROC onFootCheck()
	IF NOT IS_PED_INJURED(pedPrisoner)
		IF IS_PED_ON_FOOT(pedPrisoner)
			IF NOT bStartOnFootTimer
				iOnFootStartTimer = GET_GAME_TIMER()
				bStartOnFootTimer = TRUE
			ELSE
				iOnFootCurrentTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			bStartOnFootTimer = FALSE
		ENDIF
	ENDIF
	
	IF ((iOnFootCurrentTimer - iOnFootStartTimer) > 120000)
		IF NOT IS_PED_INJURED(pedPrisoner)
			IF IS_PED_IN_GROUP(pedPrisoner)
				// "Well, if we're going to walk I can do that by myself."
				IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_WLK", CONV_PRIORITY_AMBIENT_HIGH)
					REMOVE_PED_FROM_GROUP(pedPrisoner)
					MISSION_FAILED()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC stationaryCheck()
	IF IS_PED_STOPPED(PLAYER_PED_ID())
		IF NOT bStartStationaryTimer
			iStationaryStartTimer = GET_GAME_TIMER()
			bStartStationaryTimer = TRUE
		ELSE
			iStationaryCurrentTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		bStartStationaryTimer = FALSE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 60000)
	// "I can probably get there quicker myself."
		IF NOT IS_PED_INJURED(pedPrisoner)
			IF IS_PED_IN_GROUP(pedPrisoner)
				IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SLO", CONV_PRIORITY_AMBIENT_HIGH)
					REMOVE_PED_FROM_GROUP(pedPrisoner)
					MISSION_FAILED()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_BIG_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 1
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_NEAREST_CAR_WINDOW()
	VECTOR vRightWindow
	VECTOR vLeftWindow
	VECTOR vPrisonerCoords
	vPrisonerCoords = GET_ENTITY_COORDS(pedPrisoner)
	vRightWindow = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempPlayersVehID, << 1, 2.5, 0>>)
	vLeftWindow = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempPlayersVehID, <<-1, 2.5, 0>>)
	
	IF (GET_DISTANCE_BETWEEN_COORDS(vRightWindow, vPrisonerCoords) < GET_DISTANCE_BETWEEN_COORDS(vLeftWindow, vPrisonerCoords))
		RETURN vRightWindow
	ELSE
		RETURN vLeftWindow
	ENDIF

ENDFUNC

FUNC BOOL ARE_THEY_CLOSE_TO_POLICE_STATION()
	
	IF NOT IS_PED_INJURED(pedPrisoner)
		IF IS_PED_IN_GROUP(pedPrisoner)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation1, << 50, 50, 50 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation2, << 50, 50, 50 >>)
	        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation3, << 50, 50, 50 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation4, << 50, 50, 50 >>)
	        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation5, << 50, 50, 50 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation6, << 50, 50, 50 >>)
	        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation7, << 50, 50, 50 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation8, << 50, 50, 50 >>)
			OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vPrison) < 205
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
 
ENDFUNC

PROC dontFollowPlayerInWrongVehicle()
	
	IF NOT IS_PED_INJURED(pedPrisoner)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE WON'T DO @@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
				IF GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
					TASK_GO_TO_ENTITY(pedPrisoner, PLAYER_PED_ID(), -1, 6)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedPrisoner)
					TASK_LEAVE_ANY_VEHICLE(pedPrisoner)
				ENDIF
				IF NOT bAskedForVehAgain
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
							CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_UVH2", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_UVH", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bAskedForVehAgain = TRUE
					ENDIF
				ENDIF
			ELSE
				bAskedForVehAgain = FALSE
				IF GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
					CLEAR_PED_TASKS(pedPrisoner)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC hailDownVeh()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
		AND NOT IS_PED_INJURED(pedPrisoner)
		AND NOT bHailingDown
			fPlayerPrisonerDist = GET_DISTANCE_BETWEEN_ENTITIES(pedPrisoner, PLAYER_PED_ID())
			IF fPlayerPrisonerDist > 5
				fPlayerPrisonerDist = (fPlayerPrisonerDist - 2.5)
			ELSE
				fPlayerPrisonerDist = (fPlayerPrisonerDist - 1)
			ENDIF
			CLEAR_PED_TASKS(pedPrisoner)
			OPEN_SEQUENCE_TASK(seq)
			IF thisVariation = BIKER_GANG_LIFT
				TASK_LOOK_AT_ENTITY(NULL, tempPlayersVehID, 10000, SLF_WHILE_NOT_IN_FOV)
//				TASK_PLAY_ANIM(NULL, "random@prisoner_lift", "loop8_wavebig", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY|AF_LOOPING)
				TASK_PLAY_ANIM(NULL, "random@prisoner_lift", "arms_waving", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY|AF_LOOPING)
				TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, fPlayerPrisonerDist, PEDMOVEBLENDRATIO_WALK)
			ELIF thisVariation = HIJACK_PLAYER
				TASK_LOOK_AT_ENTITY(NULL, tempPlayersVehID, 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				TASK_PLAY_ANIM(NULL, "random@prisoner_lift", "arms_waving", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY|AF_LOOPING)
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, PLAYER_PED_ID(), << -1, 0, 0 >>, PEDMOVEBLENDRATIO_WALK, -1, 0.5)
//				TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 1, PEDMOVEBLENDRATIO_WALK)
//				TASK_ENTER_VEHICLE(NULL, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE)
//				TASK_OPEN_VEHICLE_DOOR(NULL, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
			ENDIF
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
			CLEAR_SEQUENCE_TASK(seq)
			IF thisVariation = BIKER_GANG_LIFT
				CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_STO", CONV_PRIORITY_AMBIENT_HIGH)
					SETTIMERA(0)
					bHailingDown = TRUE
//				ENDIF
			ELIF thisVariation = HIJACK_PLAYER
				IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_STO", CONV_PRIORITY_AMBIENT_HIGH)
					SETTIMERA(0)
					bHailingDown = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC keepVehicleHornOn()
	
	IF NOT IS_ENTITY_DEAD(vehBrokenDownVeh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBrokenDownVeh)
		AND NOT IS_VEHICLE_SEAT_FREE(vehBrokenDownVeh)
			SET_HORN_PERMANENTLY_ON_TIME(vehBrokenDownVeh, 60000)
			SET_HORN_PERMANENTLY_ON(vehBrokenDownVeh)
		ENDIF
	ENDIF
	
ENDPROC

PROC pickingUpPrisoner()
	// trying to get  tidy-ish approach to a vehicle that could be moving
	SWITCH iApproachStage
		CASE 0
////////////////////			SET_VFX_IGNORE_CAM_DIST_CHECK(FALSE)
			IF NOT IS_PED_INJURED(pedPrisoner)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
//				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedPrisoner) < 20
//					IF IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//						tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
							IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2.5
							OR NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 15, 15, 8 >>))
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 30000, SLF_WHILE_NOT_IN_FOV|SLF_FAST_TURN_RATE)
									TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
								CLEAR_SEQUENCE_TASK(seq)
								IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_ASK", CONV_PRIORITY_AMBIENT_HIGH)
									bHailingDown = TRUE
									iApproachStage ++
								ENDIF
							ENDIF
//						ENDIF
//					ELSE
//						IF NOT bAskedForVeh
//							fPlayerPrisonerDist = GET_DISTANCE_BETWEEN_ENTITIES(pedPrisoner, PLAYER_PED_ID())
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV)
//								TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerPrisonerDist -3, PEDMOVEBLENDRATIO_WALK)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
//							CLEAR_SEQUENCE_TASK(seq)
//							// "Dude, you got anything a bit more inconspicuous?"
//							IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_UVH", CONV_PRIORITY_AMBIENT_HIGH) //CONV_PRIORITY_AMBIENT_HIGH)
//								bAskedForVeh = TRUE
//								bHailingDown = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
				ELSE
//					continualTurnToFacePlayer(iTurnStage)
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			continualTurnToFacePlayer(iTurnStage)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				IF IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_ENTITY_SPEED(tempPlayersVehID) < 9
						IF NOT IS_PED_INJURED(pedPrisoner)
							IF IS_ENTITY_AT_ENTITY(pedPrisoner, tempPlayersVehID, << 20, 20, 5 >>)
//								IF NOT bAskedForVeh
//									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_ASK", CONV_PRIORITY_AMBIENT_HIGH)
//											iApproachStage ++
//										ENDIF
//									ENDIF
//								ELSE
									iApproachStage ++
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bAskedForVeh
						fPlayerPrisonerDist = GET_DISTANCE_BETWEEN_ENTITIES(pedPrisoner, PLAYER_PED_ID())
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerPrisonerDist -3, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
						CLEAR_SEQUENCE_TASK(seq)
						// "Dude, you got anything a bit more inconspicuous?"
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_UVH2", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_UVH", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							bAskedForVeh = TRUE
							bHailingDown = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			continualTurnToFacePlayer(iTurnStage)
			IF NOT bClearHailingDown
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2.5
					IF NOT IS_PED_INJURED(pedPrisoner)
						CLEAR_PED_TASKS(pedPrisoner)
						TASK_OPEN_VEHICLE_DOOR(pedPrisoner, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						bClearHailingDown = TRUE
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(pedPrisoner)
						CLEAR_PED_TASKS(pedPrisoner)
						TASK_OPEN_VEHICLE_DOOR(pedPrisoner, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						bClearHailingDown = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_ENTITY_SPEED(tempPlayersVehID) < 2.5
					IF IS_ENTITY_AT_COORD(tempPlayersVehID, GET_ENTITY_COORDS(pedPrisoner), << 20, 20, 5 >>)
						//"Jump in."
						IF thisVariation = BIKER_GANG_LIFT
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
										CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_GETONM", CONV_PRIORITY_AMBIENT_HIGH)
									ELSE
										CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_JIM", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
										CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_GETONF", CONV_PRIORITY_AMBIENT_HIGH)
									ELSE
										CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_JIF", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_JIT", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								iApproachStage ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_ENTITY_SPEED(tempPlayersVehID) < 0.5
					IF NOT IS_PED_INJURED(pedPrisoner)
						CLEAR_PED_TASKS(pedPrisoner)
						IF NOT IS_PED_IN_GROUP(pedPrisoner)
							SET_PED_AS_GROUP_MEMBER(pedPrisoner, GET_PLAYER_GROUP(PLAYER_ID()))
							SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedPrisoner, VS_FRONT_RIGHT)
							SET_PED_NEVER_LEAVES_GROUP(pedPrisoner, TRUE)
						ENDIF
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_LOOK_AT_ENTITY(NULL, tempPlayersVehID, 10000)
//							TASK_ENTER_VEHICLE(NULL, tempPlayersVehID, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
//						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ENDIF
			ENDIF
		//	continualTurnToFacePlayer(iTurnStage)
		BREAK
		
		CASE 4
			IF thisVariation = BIKER_GANG_LIFT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SH", CONV_PRIORITY_AMBIENT_HIGH)
						iApproachStage ++
					ENDIF
				ENDIF
			ELSE
				iApproachStage ++
			ENDIF
		BREAK
		
		CASE 5
			IF IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF NOT IS_PED_INJURED(pedPrisoner)
						IF IS_PED_SITTING_IN_VEHICLE(pedPrisoner, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF DOES_BLIP_EXIST(blipPrisoner)
								REMOVE_BLIP(blipPrisoner)
							ENDIF
							eventStage = STAGE_2
						ENDIF
					ENDIF
				ENDIF
			ELSE
				dontFollowPlayerInWrongVehicle()
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_PRISONER_IN_PLAYERS_VEHICLE()
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
			IF NOT IS_PED_INJURED(pedPrisoner)
				IF IS_PED_SITTING_IN_VEHICLE(pedPrisoner, tempPlayersVehID)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC destinationInstructions()
	IF NOT IS_PED_INJURED(pedPrisoner)
	AND IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND IS_PED_SITTING_IN_VEHICLE(pedPrisoner, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SETTIMERA(0)
				IF NOT IS_PED_IN_GROUP(pedPrisoner)
					SET_PED_AS_GROUP_MEMBER(pedPrisoner, GET_PLAYER_GROUP(PLAYER_ID()))
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedPrisoner, VS_FRONT_RIGHT)
					SET_PED_NEVER_LEAVES_GROUP(pedPrisoner, TRUE)
				ENDIF
				IF thisVariation = BIKER_GANG_LIFT
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CH", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT DOES_BLIP_EXIST(blipLocation)
								blipLocation = CREATE_BLIP_FOR_COORD(vPrisonerDestinationPos, TRUE)
							ENDIF
							CLEAR_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Attacked))
							bLeavingActivationArea = TRUE
							eventStage = STAGE_3
						ENDIF
					ENDIF
				ELIF thisVariation = HIJACK_PLAYER
					// "So where shall I take you?"
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_WHTM", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_WHTF", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_WHTT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					SET_PED_FLEE_ATTRIBUTES(pedPrisoner, FA_USE_VEHICLE, TRUE)
					eventStage = STAGE_3
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC policeCloseCheck()
	VECTOR tempCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		CONST_INT iSearchArea 50
		IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//			IF IS_COP_VEHICLE_IN_AREA_3D(<< tempCoords.x-iSearchArea, tempCoords.y-iSearchArea, tempCoords.z-iSearchArea >>, << tempCoords.x+iSearchArea, tempCoords.y+iSearchArea, tempCoords.z+iSearchArea >>)
				// Fuck, they've seen me. Lose 'em! Go!
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_POL", CONV_PRIORITY_AMBIENT_HIGH)
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
//					IF DOES_BLIP_EXIST(blipLocation)
//						REMOVE_BLIP(blipLocation)
//					ENDIF
					eventStage = LOSING_POLICE
				ENDIF
//			ENDIF
		ELSE
			IF IS_COP_VEHICLE_IN_AREA_3D(<< tempCoords.x-iSearchArea, tempCoords.y-iSearchArea, tempCoords.z-iSearchArea >>, << tempCoords.x+iSearchArea, tempCoords.y+iSearchArea, tempCoords.z+iSearchArea >>)
				IF NOT IS_PED_INJURED(pedPrisoner)
					CLEAR_PED_TASKS(pedPrisoner)
					IF IS_PED_IN_GROUP(pedPrisoner)
						REMOVE_PED_FROM_GROUP(pedPrisoner)
					ENDIF
					TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
					SET_PED_KEEP_TASK(pedPrisoner, TRUE)
					WAIT(0)
				ENDIF
				MISSION_FAILED()
			ENDIF
		ENDIF
	ELSE
		CONST_INT iSearchArea 10
		IF IS_PRISONER_IN_PLAYERS_VEHICLE()
			IF IS_COP_VEHICLE_IN_AREA_3D(<< tempCoords.x-iSearchArea, tempCoords.y-iSearchArea, tempCoords.z-iSearchArea >>, << tempCoords.x+iSearchArea, tempCoords.y+iSearchArea, tempCoords.z+iSearchArea >>)
				// Fuck, they've seen me. Lose 'em! Go!
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_POL", CONV_PRIORITY_AMBIENT_HIGH)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
					IF DOES_BLIP_EXIST(blipLocation)
						REMOVE_BLIP(blipLocation)
					ENDIF
					eventStage = LOSING_POLICE
				ENDIF
			ENDIF
		ELSE
			IF IS_COP_VEHICLE_IN_AREA_3D(<< tempCoords.x-iSearchArea, tempCoords.y-iSearchArea, tempCoords.z-iSearchArea >>, << tempCoords.x+iSearchArea, tempCoords.y+iSearchArea, tempCoords.z+iSearchArea >>)
				IF NOT IS_PED_INJURED(pedPrisoner)
					CLEAR_PED_TASKS(pedPrisoner)
					IF IS_PED_IN_GROUP(pedPrisoner)
						REMOVE_PED_FROM_GROUP(pedPrisoner)
					ENDIF
					TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
					SET_PED_KEEP_TASK(pedPrisoner, TRUE)
					WAIT(0)
				ENDIF
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC whilstDriving()

	groupSeparationCheck()
	stationaryCheck()
	onFootCheck()
	dontFollowPlayerInWrongVehicle()
	
	IF NOT bJumpInSpeech
	AND NOT bLiftSpeech
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT1", CONV_PRIORITY_AMBIENT_HIGH)
				bJumpInSpeech = TRUE
			ENDIF
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT2", CONV_PRIORITY_AMBIENT_HIGH)
				bJumpInSpeech = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF bJumpInSpeech
	AND NOT bLiftSpeech
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT1b", CONV_PRIORITY_AMBIENT_HIGH)
				bLiftSpeech = TRUE
			ENDIF
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT2b", CONV_PRIORITY_AMBIENT_HIGH)
				bLiftSpeech = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF bJumpInSpeech
	AND bLiftSpeech
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT1c", CONV_PRIORITY_AMBIENT_HIGH)
				bJumpInSpeech = FALSE
			ENDIF
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_CHAT2c", CONV_PRIORITY_AMBIENT_HIGH)
				bJumpInSpeech = FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PLAYER_INTERFERING_WITH_EVENT()
		IF NOT IS_PED_INJURED(pedPrisoner)
			CLEAR_PED_TASKS(pedPrisoner)
			IF IS_PED_IN_GROUP(pedPrisoner)
				REMOVE_PED_FROM_GROUP(pedPrisoner)
			ENDIF
			TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
			SET_PED_KEEP_TASK(pedPrisoner, TRUE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SOB", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		MISSION_FAILED()
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vInput) > 200
		IF NOT bPoliceSpawned
			VECTOR vPoliceCoords[4]
			
			vPoliceCoords[0] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 0, 9, 0 >>
			GET_SAFE_COORD_FOR_PED(vPoliceCoords[0], FALSE, vPoliceCoords[0])
			vPoliceCoords[1] = GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 9, 0 >>
			GET_SAFE_COORD_FOR_PED(vPoliceCoords[1], FALSE, vPoliceCoords[1])
			vPoliceCoords[2] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 9, 0, 0 >>
			GET_SAFE_COORD_FOR_PED(vPoliceCoords[2], FALSE, vPoliceCoords[2])
			vPoliceCoords[3] = GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 9, 0, 0 >>
			GET_SAFE_COORD_FOR_PED(vPoliceCoords[3], FALSE, vPoliceCoords[3])
			
			IF NOT IS_SPHERE_VISIBLE(vPoliceCoords[0], 3)
				vehTempCop = CREATE_VEHICLE(SHERIFF, vPoliceCoords[0], GET_HEADING_BETWEEN_VECTORS_2D(vPoliceCoords[0], GET_PLAYER_COORDS(PLAYER_ID())))
				pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_DRIVER)
				pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_FRONT_RIGHT)
				GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				bPoliceSpawned = TRUE
			ELIF NOT IS_SPHERE_VISIBLE(vPoliceCoords[1], 3)
				vehTempCop = CREATE_VEHICLE(SHERIFF, vPoliceCoords[1], GET_HEADING_BETWEEN_VECTORS_2D(vPoliceCoords[1], GET_PLAYER_COORDS(PLAYER_ID())))
				pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_DRIVER)
				pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_FRONT_RIGHT)
				GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				bPoliceSpawned = TRUE
			ELIF NOT IS_SPHERE_VISIBLE(vPoliceCoords[2], 3)
				vehTempCop = CREATE_VEHICLE(SHERIFF, vPoliceCoords[2], GET_HEADING_BETWEEN_VECTORS_2D(vPoliceCoords[2], GET_PLAYER_COORDS(PLAYER_ID())))
				pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_DRIVER)
				pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_FRONT_RIGHT)
				GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				bPoliceSpawned = TRUE
			ELIF NOT IS_SPHERE_VISIBLE(vPoliceCoords[3], 3)
				vehTempCop = CREATE_VEHICLE(SHERIFF, vPoliceCoords[3], GET_HEADING_BETWEEN_VECTORS_2D(vPoliceCoords[3], GET_PLAYER_COORDS(PLAYER_ID())))
				pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_DRIVER)
				pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_Ranger_01, VS_FRONT_RIGHT)
				GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				bPoliceSpawned = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// don't want the script created broken down veh to set off the check
	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehBrokenDownVeh, FALSE), << 10, 10, 10 >>, FALSE, FALSE)
	OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		policeCloseCheck()
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPrisoner)
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND IS_ENTITY_IN_ANGLED_AREA(pedPrisoner, << 990.7705, -105.5940, 73.3055 >>, << 954.8849, -145.3864, 79.1073 >>, 8.8125)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 990.7705, -105.5940, 73.3055 >>, << 954.8849, -145.3864, 79.1073 >>, 8.8125)
		OR IS_ENTITY_AT_COORD(pedPrisoner, vPrisonerDestinationPos, g_vAnyMeansLocate, TRUE)
		AND CAN_PLAYER_START_CUTSCENE()
			SETTIMERA(0)
			IF IS_PRISONER_IN_PLAYERS_VEHICLE()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 2)
				ENDIF
				CLEAR_PRINTS()
				eventStage = AT_DESTINATION_IN_VEH
			ELSE
//				DoThanking.aApproachPed = pedPrisoner
//				DoThanking.AnimName = "thanks_male_07"
//				DoThanking.AnimDict =  "amb@thanks"
				eventStage = AT_DESTINATION_ON_FOOT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC pedDroppedOff()
	IF NOT bThankingDone
	AND NOT IS_PED_INJURED(pedPrisoner)
	AND IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
		REMOVE_PED_FROM_GROUP(pedPrisoner)
//		DoThanking.aApproachPed = pedPrisoner
//		DoThanking.	aApproachVehicle = tempPlayersVehID
//		DoThanking.AnimName = "hello"
//		DoThanking.AnimDict = "gestures@niko"
//		DoThanking.bDoNotRemovePed = FALSE
//		DoThanking.bLeaveDoorsOpen = FALSE
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
			TASK_LEAVE_ANY_VEHICLE(NULL)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//			TASK_PAUSE(NULL, 1500)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffWalkToPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 333.9002)
			TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
		CLEAR_SEQUENCE_TASK(seq)
		SET_PED_KEEP_TASK(pedPrisoner, TRUE)
//		IF UPDATE_RUN_INCAR_THANKYOU(DoThanking)
//			IF DoThanking.bSuccess
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bThankingDone = TRUE
//			ENDIF
//		ENDIF
	ENDIF
	IF IS_PLAYER_INTERFERING_WITH_EVENT()
		IF NOT IS_PED_INJURED(pedPrisoner)
			CLEAR_PED_TASKS(pedPrisoner)
			IF IS_PED_IN_GROUP(pedPrisoner)
				REMOVE_PED_FROM_GROUP(pedPrisoner)
			ENDIF
			TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
			SET_PED_KEEP_TASK(pedPrisoner, TRUE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SOB", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		MISSION_FAILED()
	ENDIF
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		MISSION_PASSED()
	ENDIF
ENDPROC

PROC pedThankInVehicle()

	IF DOES_BLIP_EXIST(blipLocation)
		SET_BLIP_ROUTE(blipLocation, FALSE)
		REMOVE_BLIP(blipLocation)
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		IF NOT IS_PED_INJURED(pedPrisoner)
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_THK", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
		ENDIF
	ENDIF
	
	IF TIMERA() > DEFAULT_CAR_STOPPING_TO_CUTSCENE
		eventStage = POST_DROPOFF_1
	ENDIF
	
ENDPROC

PROC prisonerFreakout()
	
		SWITCH iFreakedStage
			CASE 0
				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
					IF IS_PED_IN_GROUP(pedPrisoner)
						REMOVE_PED_FROM_GROUP(pedPrisoner)
					ENDIF
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_STA", CONV_PRIORITY_AMBIENT_HIGH)
					SETTIMERA(0)
					iFreakedStage ++
				ENDIF
			BREAK
			
			CASE 1
				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
					IF TIMERA() > 500
						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -1.0)
							IF DOES_BLIP_EXIST(blipLocation)
								REMOVE_BLIP(blipLocation)
							ENDIF
							iFreakedStage ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
					IF TIMERA() > 2000	
						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_BRAKE, 2000)
							iFreakedStage ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
					IF TIMERA() > 4000
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
							IF NOT IS_PED_INJURED(pedPrisoner)
								REMOVE_PED_FROM_GROUP(pedPrisoner)
								SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0)
								TASK_LEAVE_VEHICLE(pedPrisoner, tempPlayersVehID, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_DONT_CLOSE_DOOR)
								IF NOT DOES_BLIP_EXIST(blipPrisoner)
									blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner, TRUE)
								ENDIF
								iFreakedStage ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
					IF NOT IS_PED_INJURED(pedPrisoner)
					AND NOT IS_PED_IN_VEHICLE(pedPrisoner, tempPlayersVehID)		
						TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
						SET_PED_KEEP_TASK(pedPrisoner, TRUE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SOB", CONV_PRIORITY_AMBIENT_HIGH)
						iFreakedStage ++
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				WAIT(0)
				MISSION_FAILED()
			BREAK
		ENDSWITCH
		
		
ENDPROC

PROC prisonerHijacksPlayer()
	IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		IF NOT IS_PED_INJURED(pedPrisoner)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PLAYER_ON_OR_ENTERING_ANY_TYPE_OF_BIKE()
					CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_THREAT", CONV_PRIORITY_AMBIENT_HIGH)
				ELSE
					CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_HIJ", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				IF DOES_BLIP_EXIST(blipPrisoner)
					SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
				ENDIF
				CLEAR_PED_TASKS(pedPrisoner)
				OPEN_SEQUENCE_TASK(seq)
					TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 500, TRUE)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_WALK, FALSE, 1/*5*/, 0.5)
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
					TASK_ENTER_VEHICLE(NULL, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_RESUME_IF_INTERRUPTED|ECF_JACK_ANYONE)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SETTIMERA(0)
				bAskedForVeh = TRUE
				eventStage = HIJACKING_THE_PLAYER
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//=================================== START OF MAIN SCRIPT ================================
SCRIPT (coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTSTRING("re_prisonerlift launched @@@@@@@@@@@@@@@@@@@@")
	PRINTVECTOR(vInput)
	PRINTNL()
	
	getWorldPoint()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedPrisoner)
			IF IS_PED_IN_GROUP(pedPrisoner)
				REMOVE_PED_FROM_GROUP(pedPrisoner)
			ENDIF
		ENDIF
		missionCleanup()
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // RUN ONLY IF THE PLAYER IS IN A VEHICLE
			#IF IS_DEBUG_BUILD
				PRINTNL()
				PRINTSTRING("re_prisonerlift cleaned up - player not in vehicle")
				PRINTNL()
			#ENDIF
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_PRISONERLIFT, iSelectedVariation)
		LAUNCH_RANDOM_EVENT(RE_PRISONERLIFT)
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	// Mission Loop: Loops forever until mission is passed or failed
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bLeavingActivationArea
//			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)

				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
						missionCleanup()
					ENDIF
				ENDIF
				
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_PL")
				
				SWITCH ambStage
				
					CASE ambCanRun
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						IF NOT bVariablesInitialised
							initialiseEventVariables()
						ELSE
							loadAssets()
						ENDIF
						IF bAssetsLoaded
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ENDIF
					BREAK
					
					CASE ambRunning
						IF IS_PLAYER_PLAYING(PLAYER_ID())
						
							SWITCH thisSection
							
								CASE MISSION_STAGES
									//IF NOT HAS_PLAYER_AGGROED_PED(pedPrisoner, aggroReason, iLockOnTimer, iBitFieldDontCheck)
										
										SWITCH eventStage
											CASE SETUP_STAGE
////////////////////												SET_VFX_IGNORE_CAM_DIST_CHECK(TRUE)
												createScene()
											BREAK
											
												// If player is close enough to Prisoner and driving slow enough the Prisoner will approach the vehicle and get in.
												CASE STAGE_1
													IF thisVariation = BIKER_GANG_LIFT
														IF NOT IS_PED_INJURED(pedPrisoner)
															IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 100, 100, 100 >>)
															AND NOT IS_ENTITY_OCCLUDED(pedPrisoner)
															OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
																changeBlips()
																IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
																	SET_RANDOM_EVENT_ACTIVE()
																	DISABLE_TAXI_HAILING(TRUE)
																ENDIF
															ENDIF
															IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
																IF DOES_BLIP_EXIST(blipPrisoner)
																	SHOW_HEIGHT_ON_BLIP(blipPrisoner, TRUE)
																ENDIF
																hailDownVeh()
																IF NOT IS_PLAYER_IN_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
																	IF IS_ENTITY_PLAYING_ANIM(pedPrisoner, "random@prisoner_lift", "arms_waving")
																	OR IS_ENTITY_PLAYING_ANIM(pedPrisoner, "random@prisoner_lift", "loop2_idlelook2")
																		CLEAR_PED_TASKS(pedPrisoner)
																	ENDIF
																	continualTurnToFacePlayer(iTurnStage)
																ENDIF
															ENDIF
														ELSE
															MISSION_FAILED()
														ENDIF
														pickingUpPrisoner()
														keepVehicleHornOn()
													ELIF thisVariation = HIJACK_PLAYER
														IF NOT IS_PED_INJURED(pedPrisoner)
															IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 105, 105, 105 >>)//<< 55, 55, 55 >>)
															AND NOT IS_ENTITY_OCCLUDED(pedPrisoner)
															OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
																changeBlips()
																IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
																	SET_RANDOM_EVENT_ACTIVE()
																	DISABLE_TAXI_HAILING(TRUE)
																ENDIF
															ENDIF
															IF NOT IS_ENTITY_DEAD(pedPrisoner)
																IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
																	IF DOES_BLIP_EXIST(blipPrisoner)
																		SHOW_HEIGHT_ON_BLIP(blipPrisoner, TRUE)
																	ENDIF
																	hailDownVeh()
																ENDIF
															ENDIF
															
															IF bHailingDown
																IF TIMERA() > 2000
																AND TIMERA() < 2100
																	CLEAR_PED_SECONDARY_TASK(pedPrisoner)
																ENDIF
																IF TIMERA() > 3000
//																	continualTurnToFacePlayer(iTurnStage)
																	IF NOT bAskDialoguePlayed
																		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																			CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_ASK", CONV_PRIORITY_AMBIENT_HIGH)
																			bAskDialoguePlayed = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
															
															IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
																	IF NOT IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(GET_PLAYERS_LAST_VEHICLE())
																		IF IS_ENTITY_AT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedPrisoner, << 40, 40, 20 >>)
																		AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedPrisoner, GET_PLAYERS_LAST_VEHICLE())
																			CLEAR_PED_TASKS(pedPrisoner)
																			IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
																				SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
																				PRINTLN("@@@@@@@@@@@@ SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE()) @@@@@@@@@@@@@")
																			ENDIF
																			TASK_VEHICLE_MISSION_PED_TARGET(pedPrisoner, GET_PLAYERS_LAST_VEHICLE(), PLAYER_PED_ID(), MISSION_FLEE, 22, DRIVINGMODE_AVOIDCARS, 1, 1)
																			SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																			eventStage = ENDING_EVENT
																		ELSE
																			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 20, 20, 8 >>)
																			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedPrisoner, PLAYER_PED_ID())
																				CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
																				MISSION_FAILED()
																			ENDIF
																		ENDIF
																	ELSE
																		CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
																		MISSION_FAILED()
																	ENDIF
																ELSE
																	CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
																	MISSION_FAILED()
																ENDIF
															ENDIF
															IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
																	IF IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
																	OR IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
																	OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
																	OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
																		CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
																		MISSION_FAILED()
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
														
														IF GET_PLAYER_VEHICLE()
															IF NOT IS_ENTITY_DEAD(pedPrisoner)
																IF NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), BUS)
																	IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedPrisoner) < 2.5
																		IF GET_ENTITY_SPEED(tempPlayersVehID) < 1
																			prisonerHijacksPlayer()
																		ENDIF
																	ENDIF
																ELSE
																	IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedPrisoner) < 10
																		IF GET_ENTITY_SPEED(tempPlayersVehID) < 1
																			prisonerHijacksPlayer()
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
													
													IF IS_PLAYER_INTERFERING_WITH_EVENT()
														IF NOT IS_PED_INJURED(pedPrisoner)
															CLEAR_PED_TASKS(pedPrisoner)
															IF IS_PED_IN_GROUP(pedPrisoner)
																REMOVE_PED_FROM_GROUP(pedPrisoner)
															ENDIF
														ENDIF
														MISSION_FAILED()
													ENDIF
													IF NOT IS_PED_INJURED(pedPrisoner)
														SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedPrisoner, FALSE)
													ENDIF
												BREAK
												
												// If the Prisoner is in the vehicle they tell the player to drive them to their destination
												CASE STAGE_2
													destinationInstructions()
													keepVehicleHornOn()
													IF NOT IS_PED_INJURED(pedPrisoner)
														SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedPrisoner, FALSE)
													ENDIF
												BREAK
												
												// Checks if the player has reached the destination with the Prisoner (plus checks along the way)
												CASE STAGE_3
													IF ARE_THEY_CLOSE_TO_POLICE_STATION()
														eventStage = NEAR_POLICE_STATION
													ENDIF
													whilstDriving()
													keepVehicleHornOn()
													IF NOT IS_PED_INJURED(pedPrisoner)
														SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedPrisoner, FALSE)
													ENDIF
												BREAK
												
												CASE AT_DESTINATION_IN_VEH
													pedThankInVehicle()
												BREAK
												
												CASE AT_DESTINATION_ON_FOOT
//													IF UPDATE_RUN_THANKYOU(DoThanking)
//														IF DoThanking.bSuccess
															IF NOT bThankingDone
															AND NOT IS_PED_INJURED(pedPrisoner)
																OPEN_SEQUENCE_TASK(seq)
																	TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
																	TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
																	TASK_PAUSE(NULL, 1500)
																	TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffWalkToPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 333.9002)
																	TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_THK", CONV_PRIORITY_AMBIENT_HIGH)
																REMOVE_PED_FROM_GROUP(pedPrisoner)
																bThankingDone = TRUE
															ENDIF
//														ENDIF
//													ENDIF
													IF IS_PLAYER_INTERFERING_WITH_EVENT()
														IF NOT IS_PED_INJURED(pedPrisoner)
															CLEAR_PED_TASKS(pedPrisoner)
															IF IS_PED_IN_GROUP(pedPrisoner)
																REMOVE_PED_FROM_GROUP(pedPrisoner)
															ENDIF
															KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
															WAIT(0)
															CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SOB", CONV_PRIORITY_AMBIENT_HIGH)
															TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
															SET_PED_KEEP_TASK(pedPrisoner, TRUE)
															WAIT(0)
														ENDIF
														MISSION_FAILED()
													ENDIF
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														MISSION_PASSED()
													ENDIF
												BREAK
												
												CASE POST_DROPOFF_1	
													pedDroppedOff()
												BREAK
												
												CASE NEAR_POLICE_STATION
													prisonerFreakout()
												BREAK
												
												CASE LOSING_POLICE
													groupSeparationCheck()
													dontFollowPlayerInWrongVehicle()
													
													IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
														IF NOT DOES_BLIP_EXIST(blipLocation)
															blipLocation = CREATE_BLIP_FOR_COORD(vPrisonerDestinationPos, TRUE)
														ENDIF
														IF CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_LOS", CONV_PRIORITY_AMBIENT_HIGH)
															eventStage = STAGE_3
														ENDIF
													ENDIF
													
													IF bPoliceSpawned
														IF DOES_ENTITY_EXIST(vehTempCop)
															IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
															OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTempCop) > 200
																SET_PED_AS_NO_LONGER_NEEDED(pedTempCop[0])
																SET_PED_AS_NO_LONGER_NEEDED(pedTempCop[1])
																SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTempCop)
															ENDIF
														ENDIF
													ENDIF
													
													IF IS_PLAYER_INTERFERING_WITH_EVENT()
														IF NOT IS_PED_INJURED(pedPrisoner)
															CLEAR_PED_TASKS(pedPrisoner)
															IF IS_PED_IN_GROUP(pedPrisoner)
																REMOVE_PED_FROM_GROUP(pedPrisoner)
															ENDIF
															TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
															SET_PED_KEEP_TASK(pedPrisoner, TRUE)
															KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
															WAIT(0)
															CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI1AU", "PRI1_SOB", CONV_PRIORITY_AMBIENT_HIGH)
														ENDIF
														MISSION_FAILED()
													ENDIF
												BREAK
												
												CASE HIJACKING_THE_PLAYER
													
													SET_WANTED_LEVEL_MULTIPLIER(0.1)
													
													IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
														IF NOT IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(tempPlayersVehID)
														
															IF NOT bInCombat
																IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																AND NOT IS_PED_IN_ANY_VEHICLE(pedPrisoner)
																	IF NOT IS_PED_INJURED(pedPrisoner)
																		IF NOT bStealDialoguePlayed
																			CREATE_CONVERSATION(PrisonerLiftDialogueStruct, "PRI2AU", "PRI2_STEAL", CONV_PRIORITY_AMBIENT_HIGH)
																			bStealDialoguePlayed = TRUE
																		ENDIF
																		TASK_ENTER_VEHICLE(pedPrisoner, tempPlayersVehID, -1)
	//																	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, TRUE)
																		SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																		eventStage = MAKE_A_RUN
																	ENDIF
																ENDIF
															
//															IF NOT bShootAt
//															AND NOT IS_ENTITY_DEAD(tempPlayersVehID)
//																IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempPlayersVehID)
//																	IF TIMERA() > 5000
//																//	OR IS_VEHICLE_STOPPED(tempPlayersVehID)
//																		IF NOT IS_PED_INJURED(pedPrisoner)
//																			TASK_SHOOT_AT_ENTITY(pedPrisoner, PLAYER_PED_ID(), -1, FIRING_TYPE_RANDOM_BURSTS)
//																			bShootAt = TRUE
//																			SETTIMERA(0)
//																		ENDIF
//																	ENDIF
//																ENDIF
//															ENDIF
															
																IF NOT IS_PED_INJURED(pedPrisoner)
																	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPrisoner, PLAYER_PED_ID())
																	OR (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedPrisoner) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
																	OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedPrisoner) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
//																	OR (bShootAt AND (TIMERA() > 20000))
																	OR (GET_ENTITY_SPEED(tempPlayersVehID) > 2.5 AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempPlayersVehID))
																		TASK_COMBAT_PED(pedPrisoner, PLAYER_PED_ID())
																		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
																		bInCombat = TRUE
																	ENDIF
																ENDIF
															ENDIF
															
														ELSE
															IF DOES_BLIP_EXIST(blipPrisoner)
//																SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
																REMOVE_BLIP(blipPrisoner)
															ENDIF
															IF NOT IS_PED_INJURED(pedPrisoner)
																OPEN_SEQUENCE_TASK(seq)
																	TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
																	TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																eventStage = ENDING_EVENT
															ENDIF
														ENDIF
													ENDIF
													
													IF NOT IS_PED_INJURED(pedPrisoner)
														IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 150, 150, 150 >>)
															OPEN_SEQUENCE_TASK(seq)
																TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
																TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
															CLOSE_SEQUENCE_TASK(seq)
															TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
															CLEAR_SEQUENCE_TASK(seq)
															SET_PED_KEEP_TASK(pedPrisoner, TRUE)
															eventStage = ENDING_EVENT
														ENDIF
													ELSE
														eventStage = ENDING_EVENT
													ENDIF
												BREAK
												
												CASE MAKE_A_RUN
												
													SET_WANTED_LEVEL_MULTIPLIER(0.1)
													
													IF NOT IS_PED_INJURED(pedPrisoner)
														IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPrisoner, PLAYER_PED_ID())
															IF DOES_BLIP_EXIST(blipPrisoner)
																SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
																CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedPrisoner)
															ENDIF
														ENDIF
														IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
															IF NOT IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(tempPlayersVehID)
																IF IS_PED_IN_VEHICLE(pedPrisoner, tempPlayersVehID)
																	IF NOT IS_ENTITY_A_MISSION_ENTITY(tempPlayersVehID)
																		SET_ENTITY_AS_MISSION_ENTITY(tempPlayersVehID)
																		PRINTLN("@@@@@@@@@@@@ SET_ENTITY_AS_MISSION_ENTITY(tempPlayersVehID) @@@@@@@@@@@@@")
																	ENDIF
																	TASK_VEHICLE_MISSION_PED_TARGET(pedPrisoner, tempPlayersVehID, PLAYER_PED_ID(), MISSION_FLEE, 22, DRIVINGMODE_AVOIDCARS, 1, 1)
																	SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																	eventStage = ENDING_EVENT
																ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempPlayersVehID)
																	IF bAskedForVeh
																		TASK_COMBAT_PED(pedPrisoner, PLAYER_PED_ID())
																		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
																		bInCombat = TRUE
																		eventStage = HIJACKING_THE_PLAYER
																	ELSE
																		prisonerHijacksPlayer()
																	ENDIF
																ELSE
																	IF GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
																	AND GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
																		IF GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
																		AND GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_COMBAT) != WAITING_TO_START_TASK
																		ELSE
																			eventStage = HIJACKING_THE_PLAYER
																		ENDIF
																	ENDIF
																ENDIF
															ELSE
																IF DOES_BLIP_EXIST(blipPrisoner)
//																	SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
																	REMOVE_BLIP(blipPrisoner)
																ENDIF
																OPEN_SEQUENCE_TASK(seq)
																	TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
																	TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																eventStage = ENDING_EVENT
															ENDIF
														ELSE
															OPEN_SEQUENCE_TASK(seq)
																TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
																TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
															CLOSE_SEQUENCE_TASK(seq)
															TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
															CLEAR_SEQUENCE_TASK(seq)
															SET_PED_KEEP_TASK(pedPrisoner, TRUE)
															eventStage = ENDING_EVENT
														ENDIF
														
														IF NOT bInCombat
															IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPrisoner, PLAYER_PED_ID())
															OR (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedPrisoner) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
															OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedPrisoner) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE))
//															OR (bShootAt AND (TIMERA() > 20000))
															OR (GET_ENTITY_SPEED(tempPlayersVehID) > 2.5 AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempPlayersVehID))
																TASK_COMBAT_PED(pedPrisoner, PLAYER_PED_ID())
																KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
																bInCombat = TRUE
															ENDIF
														ENDIF
													ELSE
														eventStage = ENDING_EVENT
													ENDIF
												BREAK
												
												CASE ENDING_EVENT
													bLeavingActivationArea = TRUE
													IF NOT IS_PED_INJURED(pedPrisoner)
														IF IS_PED_BEING_JACKED(pedPrisoner)
	//													OR NOT IS_PED_IN_VEHICLE(pedPrisoner)
															KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
														ENDIF
														IF IS_PED_IN_ANY_VEHICLE(pedPrisoner)
															IF DOES_BLIP_EXIST(blipPrisoner)
																SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
															ENDIF
															IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pedPrisoner), TOWTRUCK)
															OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pedPrisoner), TOWTRUCK2)
																entTowed = GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(pedPrisoner))
																IF DOES_ENTITY_EXIST(entTowed)
																	vehTowed = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entTowed)
																	IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(pedPrisoner))
																	AND NOT IS_ENTITY_DEAD(vehTowed)
																		IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(pedPrisoner), vehTowed)
																			DETACH_VEHICLE_FROM_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(pedPrisoner), vehTowed)
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
															IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(pedPrisoner))
																OPEN_SEQUENCE_TASK(seq)
																	TASK_LEAVE_ANY_VEHICLE(NULL)
																	TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//																	TASK_VEHICLE_MISSION_PED_TARGET(NULL, tempPlayersVehID, PLAYER_PED_ID(), MISSION_FLEE, 22, DRIVINGMODE_AVOIDCARS, 1, 1)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedPrisoner, TRUE)
//																eventStage = HIJACKING_THE_PLAYER
															ENDIF
															IF NOT bVehicleImmobilized
																IF IS_PLAYER_TOWING_VEHICLE(GET_VEHICLE_PED_IS_IN(pedPrisoner))
																OR NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(pedPrisoner))
																OR IS_VEHICLE_ON_FIRE_OR_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(pedPrisoner))
																	IF DOES_BLIP_EXIST(blipPrisoner)
																		SET_BLIP_AS_FRIENDLY(blipPrisoner, FALSE)
																	ENDIF
																	OPEN_SEQUENCE_TASK(seq)
																		TASK_SMART_FLEE_COORD(NULL, << 1449.0497, 2507.2576, 44.8843 >>, 1000, -1)
																		TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
																	CLOSE_SEQUENCE_TASK(seq)
																	TASK_PERFORM_SEQUENCE(pedPrisoner, seq)
																	CLEAR_SEQUENCE_TASK(seq)
																	SET_PED_KEEP_TASK(pedPrisoner, TRUE)
																	bVehicleImmobilized = TRUE
																ENDIF
															ENDIF
															IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 245, 245, 245 >>)
																MISSION_FAILED()
															ENDIF
														ELIF GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
														AND GET_SCRIPT_TASK_STATUS(pedPrisoner, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
															eventStage = HIJACKING_THE_PLAYER
														ELSE
															IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPrisoner, << 200, 200, 200 >>)
																MISSION_PASSED()
															ENDIF
														ENDIF
													ELSE
														MISSION_PASSED()
													ENDIF
												BREAK
											
										ENDSWITCH
//									ELSE
//										IF eventStage <> POST_DROPOFF_2
//											thisSection = AGGRO_REACTIONS
//										ENDIF
//									ENDIF
								BREAK
								
								CASE AGGRO_REACTIONS
								
									SWITCH aggroReason
//										CASE EAggro_Danger //player is wanted or shot a victim near them
//											//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
//											IF NOT IS_PED_INJURED(pedPrisoner)
//												CLEAR_PED_TASKS(pedPrisoner)
//												IF IS_PED_IN_GROUP(pedPrisoner)
//													REMOVE_PED_FROM_GROUP(pedPrisoner)
//												ENDIF
//												TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
//												SET_PED_KEEP_TASK(pedPrisoner, TRUE)
//												WAIT(0)
//												missionCleanup()
//											ENDIF
//										BREAK
										
										CASE EAggro_ShotNear //shot near or near miss
											//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
											IF NOT IS_PED_INJURED(pedPrisoner)
												CLEAR_PED_TASKS(pedPrisoner)
												IF IS_PED_IN_GROUP(pedPrisoner)
													REMOVE_PED_FROM_GROUP(pedPrisoner)
												ENDIF
												TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
												SET_PED_KEEP_TASK(pedPrisoner, TRUE)
												WAIT(0)
												MISSION_FAILED()
											ENDIF
										BREAK
										
										CASE EAggro_HostileOrEnemy //stick up
										//	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPrisoner, FALSE)
											IF NOT IS_PED_INJURED(pedPrisoner)
												CLEAR_PED_TASKS(pedPrisoner)
												IF IS_PED_IN_GROUP(pedPrisoner)
													REMOVE_PED_FROM_GROUP(pedPrisoner)
												ENDIF
												TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
												SET_PED_KEEP_TASK(pedPrisoner, TRUE)
												WAIT(0)
												MISSION_FAILED()
											ENDIF
										BREAK
										
										CASE EAggro_Attacked //attacked
											IF NOT IS_PED_INJURED(pedPrisoner)
												CLEAR_PED_TASKS(pedPrisoner)
												IF IS_PED_IN_GROUP(pedPrisoner)
													REMOVE_PED_FROM_GROUP(pedPrisoner)
												ENDIF
												TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
												SET_PED_KEEP_TASK(pedPrisoner, TRUE)
												WAIT(0)
												MISSION_FAILED()
											ENDIF
										BREAK
										
										CASE EAggro_HeardShot
											IF NOT IS_PED_INJURED(pedPrisoner)
												CLEAR_PED_TASKS(pedPrisoner)
												IF IS_PED_IN_GROUP(pedPrisoner)
													REMOVE_PED_FROM_GROUP(pedPrisoner)
												ENDIF
												TASK_SMART_FLEE_COORD(pedPrisoner, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, -1)
												SET_PED_KEEP_TASK(pedPrisoner, TRUE)
												WAIT(0)
												MISSION_FAILED()
											ENDIF
										BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
						ENDIF
					BREAK
				ENDSWITCH
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
		
		//------------------------------ DEBUGGING STUFF ------------------------------ 
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF  IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				IF thisVariation =  BIKER_GANG_LIFT
					IF eventStage <= STAGE_2
						IF NOT DOES_BLIP_EXIST(blipPrisoner)
							blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner)
						ENDIF
						IF DOES_BLIP_EXIST(blipLocation)
							REMOVE_BLIP(blipLocation)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedPrisoner)
							REMOVE_PED_FROM_GROUP(pedPrisoner)
							SET_ENTITY_COORDS(pedPrisoner, vCreatePrisonerPos)
							SET_ENTITY_HEADING(pedPrisoner, fCreatePrisonerHdg)
						ENDIF
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 1539.5553, 2141.7122, 78.9180 >>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 355.1148)
						ELSE
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 1539.5553, 2141.7122, 78.9180 >>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 355.1148)
						ENDIF
						
						bLeavingActivationArea = TRUE
						IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							SET_RANDOM_EVENT_ACTIVE()
							DISABLE_TAXI_HAILING(TRUE)
						ENDIF
						bAskedForVeh = TRUE
						bAskedForVehAgain = TRUE
						bHailingDown = TRUE
						bClearHailingDown = TRUE
						
						iApproachStage = 0
						
						eventStage = STAGE_1
					ENDIF
					IF eventStage >= STAGE_3
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 1506.8655, 2203.4036, 79.3566 >>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 93.4418)
							IF NOT IS_PED_INJURED(pedPrisoner)
								SET_PED_INTO_VEHICLE(pedPrisoner, GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
							ENDIF
						ELSE
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 1506.8655, 2203.4036, 79.3566 >>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 93.4418)
							IF NOT IS_PED_INJURED(pedPrisoner)
								SET_PED_COORDS_KEEP_VEHICLE(pedPrisoner, << 1506.8655, 2203.4036, 79.3566 >>)
								SET_ENTITY_HEADING(pedPrisoner, 93.4418)
							ENDIF
						ENDIF
						eventStage = STAGE_2
					ENDIF
				ENDIF
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				// If the keyboard key J has been pressed it warps the player to the destination the Prisoner wants to get to
				IF thisVariation = BIKER_GANG_LIFT
					IF eventStage = STAGE_1
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 1543.0889, 2149.7290, 77.8933 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 15.6833)
					ENDIF
					IF eventStage >= STAGE_3
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 942.6251, -138.5538, 73.5839 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 239.3284)
					ENDIF
				ENDIF
			ENDIF
			
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT

//PROC struggleForControl()
//
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UP)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//		
//		SWITCH iStruggleStage
//			CASE 0
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF IS_PED_IN_GROUP(pedPrisoner)
//						REMOVE_PED_FROM_GROUP(pedPrisoner)
//					ENDIF
//					IF DOES_BLIP_EXIST(blipLocation)
//						REMOVE_BLIP(blipLocation)
//					ENDIF
//					SETTIMERA(0)
////					IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
////						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), tempPlayersVehID, << -242.6217, 6497.0474, 10.2356 >>, 18, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(tempPlayersVehID), DRIVINGMODE_AVOIDCARS, -1, -1)
////					ENDIF
//					iStruggleStage ++
//				ENDIF
//			BREAK
//			
//			CASE 1
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.4)
//							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNLEFT, 500)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 2
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 1000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							//SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.05)
//							TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNRIGHT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 3
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 2000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//						//	TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), tempPlayersVehID, << -242.6217, 6497.0474, 10.2356 >>, 18, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(tempPlayersVehID), DRIVINGMODE_AVOIDCARS, -1, -1)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.15)
//							//TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), tempPlayersVehID, 18, DRIVINGMODE_AVOIDCARS)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 4
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 4500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.1)
//							//TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), tempPlayersVehID, 22, DRIVINGMODE_AVOIDCARS)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 5
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 5000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//						//	SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.2)
//							TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNRIGHT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 6
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 7000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.1)
//						//	TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_HANDBRAKETURNLEFT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 7
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 8500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//						//	TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_HANDBRAKETURNLEFT, 1000)
//							//TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), tempPlayersVehID, 22, DRIVINGMODE_AVOIDCARS)						
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 8
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 9500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.0)
////							TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), tempPlayersVehID, 22, DRIVINGMODE_AVOIDCARS)						
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 9
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 11000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.15)
//						//	TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNLEFT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 10
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 12000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.2)
//							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNRIGHT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 11
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 12500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.25)
//							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNLEFT, 1000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 12
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 13000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.3)
//						//	TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), tempPlayersVehID, 22, DRIVINGMODE_AVOIDCARS)						
//							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_BRAKE,2000)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 13
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 14500
//						PRINT_HELP_FOREVER("PLIFT_HELP_1")
//						iStruggleStage ++
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 14
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 15000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNLEFT, 1000)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.2)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 15
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 17500
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//						//	TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_TURNRIGHT, 1000)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.25)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 16
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 18500
//						CLEAR_HELP()
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//						//	TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_BRAKE,2000)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, -0.1)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 17
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 20000
//						IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//							SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.25)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 18
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF TIMERA() > 20800
//						CLEAR_PED_TASKS(PLAYER_PED_ID())
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
//						TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempPlayersVehID, TEMPACT_BRAKE,2000)
//						iStruggleStage ++
//					ENDIF
//				ENDIF
//			BREAK
//			
//			// if player has not activated cinematic in car punch up
//			CASE 19
//				IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//					IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID) 
//						IF GET_ENTITY_SPEED(tempPlayersVehID) < 0.5
//							IF NOT IS_PED_INJURED(pedPrisoner)
//								TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(pedPrisoner, tempPlayersVehID)
//								PRINTNL()
//								PRINTSTRING("TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT")
//								PRINTNL()
//							ENDIF
//							
//							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//							SET_VEHICLE_DOOR_OPEN(tempPlayersVehID, SC_DOOR_FRONT_LEFT)
//							SETTIMERA(0)
//							iStruggleStage ++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 20
//				IF NOT bFleeInVeh
//					IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID) 
//						IF NOT IS_PED_INJURED(pedPrisoner)
//							IF GET_PED_IN_VEHICLE_SEAT(tempPlayersVehID, VS_DRIVER) = pedPrisoner
//								TASK_VEHICLE_MISSION_PED_TARGET(pedPrisoner, tempPlayersVehID, PLAYER_PED_ID(), MISSION_FLEE, 22, DRIVINGMODE_AVOIDCARS, 1, 1)				
//								bFleeInVeh  = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				IF TIMERA() > 2000
//					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					ENDIF
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				ENDIF
//			BREAK
//			
//		ENDSWITCH
//		
//		// if player has left the vehicle
//		IF iStruggleStage <> 20
//			IF NOT IS_PRISONER_IN_PLAYERS_VEHICLE()
//				IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//					SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0.0)
//					IF IS_PED_IN_VEHICLE(pedPrisoner, tempPlayersVehID)
//						IF NOT DOES_BLIP_EXIST(blipVehicle)
//							blipVehicle = CREATE_BLIP_FOR_VEHICLE(tempPlayersVehID, TRUE)
//						ENDIF
//					ENDIF
//					CLEAR_HELP()
//					eventStage =  MAKE_A_RUN
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		// if vehicle has stopped or is stuck
//		IF iStruggleStage <> 20
//			IF IS_PRISONER_IN_PLAYERS_VEHICLE()
//				IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//					IF IS_VEHICLE_STOPPED(tempPlayersVehID)
//					OR IS_VEHICLE_STUCK_TIMER_UP(tempPlayersVehID, VEH_STUCK_HUNG_UP, 4000)
//					OR IS_VEHICLE_STUCK_TIMER_UP(tempPlayersVehID, VEH_STUCK_JAMMED, 4000)
//					OR IS_VEHICLE_STUCK_TIMER_UP(tempPlayersVehID, VEH_STUCK_ON_ROOF, 4000)
//					OR IS_VEHICLE_STUCK_TIMER_UP(tempPlayersVehID, VEH_STUCK_ON_SIDE, 4000 )
//						IF NOT DOES_BLIP_EXIST(blipPrisoner)
//							blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner, TRUE)
//						ENDIF
//						CLEAR_HELP()
//						iStruggleStage =  19
//					ENDIF
//				ELSE
//					IF NOT DOES_BLIP_EXIST(blipPrisoner)
//						blipPrisoner = CREATE_BLIP_FOR_PED(pedPrisoner, TRUE)
//					ENDIF
//					CLEAR_HELP()
//					iStruggleStage =  19
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF iStruggleStage > 13
//			IF IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
//				eventStage = FIGHT_IN_CAR
//			ENDIF
//		ENDIF
//ENDPROC

//PROC carFight()
//	SWITCH	iFightStage
//			CASE 0
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
//				camFight = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
//				tempPlayersVehID = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//				IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
//					SET_VEHICLE_STEER_BIAS(tempPlayersVehID, 0)
//					vCamCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempPlayersVehID, (<<0, 4, 1>>))
//				ENDIF
//				SET_CAM_COORD(camFight, vCamcoords)
//				POINT_CAM_AT_ENTITY(camFight, tempPlayersVehID, <<0, 0, 0.5>> )
//				SET_CAM_FOV(camFight, 22.0)
//				SET_CAM_ACTIVE(camFight, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				IF NOT IS_PED_INJURED(pedPrisoner)
//				CLEAR_PED_TASKS(pedPrisoner)
//				ENDIF
//				SETTIMERA(0)
//				PRINT_HELP_FOREVER("PLIFT_HELP_2")
//				
//				IF NOT IS_PED_INJURED(pedPrisoner)
//					TASK_PLAY_ANIM(pedPrisoner, "random@prisoner_lift", "Wheel_Grab_L", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//				ENDIF
//				
//				iFightStage ++
//			BREAK
//			CASE 1
//			// print help isn't working properly - fix this when it is
//			
//				IF TIMERA() > 5000
//					CLEAR_HELP()
//				ENDIF
//				IF IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
//					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@prisoner_lift", "Player_Slapped_Girl")
//						TASK_PLAY_ANIM(PLAYER_PED_ID(), "random@prisoner_lift", "Player_Slapped_Girl", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//						iPunchCounter ++
//					ENDIF
//				ENDIF
//				IF iPunchCounter > 4
//					SETTIMERA(0)
//					IF NOT IS_PED_INJURED(pedPrisoner)
//						TASK_LEAVE_ANY_VEHICLE(pedPrisoner)
//					ENDIF
//					iFightStage ++
//				ENDIF
//			BREAK
//			CASE 2
//				IF TIMERA() > 2000
//					IF NOT IS_ENTITY_DEAD(pedPrisoner)
//						SET_ENTITY_HEALTH(pedPrisoner, 99)
//					ENDIF
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//					SET_CAM_ACTIVE(camFight, FALSE)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					missionCleanup()
//				ENDIF
//			BREAK
//			
//		ENDSWITCH
//ENDPROC
