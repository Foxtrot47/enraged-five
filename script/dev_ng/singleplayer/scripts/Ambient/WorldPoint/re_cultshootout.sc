//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_interiors.sch"
USING "lineactivation.sch"
USING "script_blips.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch"
USING "Ambient_Common.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM cultshootout_STAGES
	STAGE_CREATE,
	STAGE_RUN_cultshootout,
	STAGE_CLEAN_UP
ENDENUM
cultshootout_STAGES cultshootoutStage = STAGE_CREATE

ENUM RUN_Drug_deal_STAGES
	Drug_deal_get_briefcase,
	Drug_deal_get_briefcase_2
ENDENUM
RUN_Drug_deal_STAGES RunDrug_dealStage = Drug_deal_get_briefcase

#IF IS_DEBUG_BUILD //WIDGETS
	INT iRuncultshootoutStage
	BOOL bRunDebugSelection
#ENDIF

CONST_INT NUM_CULT_DRUG_PEDS 6
CONST_INT NUM_CULT_DRUG_CARS 2

VECTOR vInput
VECTOR vBriefcaseCoord = << 2194.134521, 5593.278320, 53.703827 >>
VECTOR vDrugPedGang[NUM_CULT_DRUG_PEDS]

VECTOR vCompoundArea1 = << 2203.270264, 5549.917969, 93.094849 >>
VECTOR vCompoundArea2 = << 2199.629395, 5640.292969, 52.239262 >>
FLOAT fAreaWidth = 81.500000

FLOAT fDrugPedGang[NUM_CULT_DRUG_PEDS]

BOOL bPreCleanup
BOOL bDoFullCleanUp
BOOL bEventComplete

BOOL bWarningGiven[NUM_CULT_DRUG_PEDS]
//BOOL bStartStationaryTimer
BOOL b2ndWarningGiven
BOOL b3rdWarningGiven
BOOL bWarnedAboutStash

BOOL bBriefcasePickedUp

BOOL bMusicFadeTriggered
BOOL bMusicEventTriggered

PED_INDEX piDrugCult[NUM_CULT_DRUG_PEDS]
VEHICLE_INDEX viDrugCult[NUM_CULT_DRUG_CARS]
PICKUP_INDEX piBriefcase

BLIP_INDEX biRandomEvent
BLIP_INDEX biBriefcase
BLIP_INDEX biDrugCult[NUM_CULT_DRUG_PEDS]

REL_GROUP_HASH rghDrugCult
//CAMERA_INDEX ciPickingUpcase
//enumCharacterList current_player_enum
SEQUENCE_INDEX seq

INT index
INT iRandomWeapon
INT iWarnPlayerStage
INT iStationaryStartTimer, iStationaryCurrentTimer
INT iCounterTimeStartedCultrescue //, iCounterCurrentTimeCultrescue, iCounterTimeStartedJumper
//TEXT_LABEL cultPatrol0 = "miss_cult0"
//TEXT_LABEL cultPatrol1 = "miss_cult1"
//TEXT_LABEL cultPatrol2 = "miss_cult2"
structPedsForConversation cultshootConversation


// Functions ----------------------------------------------//

PROC loadAssets()

	REQUEST_MODEL(G_M_Y_SalvaGoon_01)
	REQUEST_MODEL(BobcatXL)
	REQUEST_MODEL(prop_cash_case_01)
//	REQUEST_ANIM_DICT("gestures@male")
	
	IF HAS_MODEL_LOADED(G_M_Y_SalvaGoon_01)
	AND HAS_MODEL_LOADED(BobcatXL)
	AND HAS_MODEL_LOADED(prop_cash_case_01)
//	AND HAS_ANIM_DICT_LOADED("gestures@male")
		
		ADD_SCENARIO_BLOCKING_AREA(vInput - << 50, 50, 20 >>, vInput + << 50, 50, 20 >>)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(BobcatXL, TRUE)
		SET_WANTED_LEVEL_MULTIPLIER(0)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		
//		gang_car[0] = CREATE_VEHICLE(bison, << -1626.9401, 4729.8892, 51.3463  >>, 347.1188)
//		SET_ENTITY_AS_MISSION_ENTITY(gang_car[0])
//		SET_VEHICLE_ON_GROUND_PROPERLY(gang_car[0])
//		SET_VEHICLE_ENGINE_ON(gang_car[0], TRUE, TRUE)
//		SET_VEHICLE_LIGHTS(gang_car[0], FORCE_VEHICLE_LIGHTS_ON)
//		SET_VEHICLE_ENGINE_HEALTH(gang_car[0], 500)
//		SET_VEHICLE_PETROL_TANK_HEALTH(gang_car[0], 500)
		
		viDrugCult[0] = CREATE_VEHICLE(BobcatXL, << 2209.1704, 5572.8975, 52.7565 >>, 255.5161)
		SET_ENTITY_AS_MISSION_ENTITY(viDrugCult[0])
		SET_VEHICLE_ON_GROUND_PROPERLY(viDrugCult[0])
		SET_VEHICLE_EXTRA(viDrugCult[0], 1, TRUE)
		
		viDrugCult[1] = CREATE_VEHICLE(BobcatXL, << 2195.4729, 5607.5063, 52.5648 >>, 168.4012)
		SET_ENTITY_AS_MISSION_ENTITY(viDrugCult[1])
		SET_VEHICLE_EXTRA(viDrugCult[1], 1, FALSE)
		
		INT iPlacementFlags = 0
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		piBriefcase = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, vBriefcaseCoord, << -1.368082, 0.230633, 176.874863 >>, iPlacementFlags, GET_RANDOM_INT_IN_RANGE(40000, 100000), EULER_YXZ, TRUE, prop_cash_case_01)
//		piBriefcase = CREATE_OBJECT(PROP_LD_CASE_01, vBriefcaseCoord) //Briefcase
//		SET_ENTITY_ROTATION(piBriefcase, <<-2.5500, 0.0000, -180.0000>>)
//		FREEZE_ENTITY_POSITION(piBriefcase, TRUE)
//		ADD_OBJECT_TO_INTERIOR_ROOM_BY_NAME(piBriefcase, "hickbrestbck")
//		RETAIN_ENTITY_IN_INTERIOR(piBriefcase, GET_INTERIOR_AT_COORDS(vBriefcaseCoord))
		
		vDrugPedGang[0] = << 2205.7690, 5631.9849, 55.5530 >> //Guard at front entrance
		fDrugPedGang[0] = 352.2203
		
		vDrugPedGang[1] = << 2210.5842, 5580.6836, 52.8132 >> //Guard at warehouse
		fDrugPedGang[1] = 300.5012
		
		vDrugPedGang[2] = << 2201.3025, 5589.2793, 52.9354 >> //Guard patrolling the centre
		fDrugPedGang[2] = 53.1602
		
		vDrugPedGang[3] = << 2202.5352, 5613.3667, 52.6781 >> //Chat1
		fDrugPedGang[3] = 70.0
		
		vDrugPedGang[4] = << 2200.7639, 5614.0762, 52.6852 >> //Chat2
		fDrugPedGang[4] = 53.0
		
		vDrugPedGang[5] = << 2193.7141, 5595.4561, 52.7615 >> //Guard by case
		fDrugPedGang[5] = 338.8346
		
		ADD_RELATIONSHIP_GROUP("DrugCult", rghDrugCult)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rghDrugCult, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_COP, rghDrugCult)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghDrugCult, RELGROUPHASH_PLAYER)
		
		REPEAT NUM_CULT_DRUG_PEDS index
			piDrugCult[index] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, vDrugPedGang[index], fDrugPedGang[index])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piDrugCult[index], TRUE)
			SET_PED_COMBAT_MOVEMENT(piDrugCult[index], CM_WILLADVANCE)
			SET_PED_COMBAT_ATTRIBUTES(piDrugCult[index], CA_CAN_CHARGE, TRUE)
//			SET_PED_COMBAT_ATTRIBUTES(piDrugCult[index], CA_USE_COVER, TRUE)
			SET_PED_COMBAT_RANGE (piDrugCult[index], CR_NEAR)
			SET_PED_ACCURACY(piDrugCult[index], 13)
			SET_PED_RELATIONSHIP_GROUP_HASH(piDrugCult[index], rghDrugCult)
//			RETAIN_ENTITY_IN_INTERIOR(piDrugCult[index], GET_INTERIOR_AT_COORDS(vDrugPedGang[index]))
			iRandomWeapon = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandomWeapon = 0
				GIVE_WEAPON_TO_PED(piDrugCult[index], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, TRUE)
			ELIF iRandomWeapon = 1
				GIVE_WEAPON_TO_PED(piDrugCult[index], WEAPONTYPE_MICROSMG, INFINITE_AMMO, TRUE)
			ELSE
				GIVE_WEAPON_TO_PED(piDrugCult[index], WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE)
			ENDIF
//			TASK_COMBAT_HATED_TARGETS_IN_AREA(piDrugCult[index], vDrugPedGang[index], 10.0)
			SET_PED_CONFIG_FLAG(piDrugCult[index], PCF_DontInfluenceWantedLevel, TRUE)
			ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(piDrugCult[0])
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2205.7690, 5631.9849, 55.5530 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2214.8330, 5635.6274, 55.0029 >>, 8000)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2215.9932, 5613.2988, 53.6157 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2221.9507, 5593.6255, 52.9483 >>, 10000)
				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(piDrugCult[0], seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
//		OPEN_PATROL_ROUTE(cultPatrol0) //Guard at front entrance
//			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_SMOKING", << 2205.7690, 5631.9849, 55.5530 >>, << 2214.8330, 5635.6274, 55.0029 >>, 8000)
//			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND", << 2215.0388, 5624.4556, 53.9246 >>, << 2213.3818, 5632.7310, 55.0604 >>, 10000)
//			ADD_PATROL_ROUTE_LINK(1, 2)
//			ADD_PATROL_ROUTE_LINK(2, 1)
//		CLOSE_PATROL_ROUTE()
//		CREATE_PATROL_ROUTE()
//		TASK_PATROL(piDrugCult[0], cultPatrol0, PAS_CASUAL, TRUE)
		SET_PED_NAME_DEBUG(piDrugCult[0], "piDrugCult[0]")
		
		IF NOT IS_PED_INJURED(piDrugCult[1])
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2210.5842, 5580.6836, 52.8132 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2205.4661, 5583.6309, 52.8682 >>, 8000)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2203.0386, 5567.4077, 52.7996 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2199.4983, 5560.7021, 52.8606 >>, 10000)
				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(piDrugCult[1], seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
//		OPEN_PATROL_ROUTE(cultPatrol1) //Guard at warehouse
//			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND", << 2210.5842, 5580.6836, 52.8132 >>, << 2205.4661, 5583.6309, 52.8682 >>, 8000)
//			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND", << 2203.0386, 5567.4077, 52.7996 >>, << 2199.4983, 5560.7021, 52.8606 >>, 10000)
//			ADD_PATROL_ROUTE_LINK(1, 2)
//			ADD_PATROL_ROUTE_LINK(2, 1)
//		CLOSE_PATROL_ROUTE()
//		CREATE_PATROL_ROUTE()
//		TASK_PATROL(piDrugCult[1], cultPatrol1, PAS_CASUAL, TRUE)
		SET_PED_NAME_DEBUG(piDrugCult[1], "piDrugCult[1]")
		
		IF NOT IS_PED_INJURED(piDrugCult[2])
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2201.0137, 5589.3569, 52.9573 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2210.6458, 5593.5757, 52.8312 >>, 8000)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2229.6667, 5598.3682, 53.2195 >>, PEDMOVE_WALK, -1)
				TASK_TURN_PED_TO_FACE_COORD(NULL, << 2237.9163, 5599.5894, 53.2311 >>, 12000)
				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(piDrugCult[2], seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
//		OPEN_PATROL_ROUTE(cultPatrol2) //Guard patrolling the centre
//			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND", << 2201.3025, 5589.2793, 52.9354 >>, << 2210.6458, 5593.5757, 52.8312 >>, 8000)
//			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_SMOKING", << 2228.8340, 5594.4355, 53.0266 >>, << 2237.9163, 5599.5894, 53.2311 >>, 12000)
//			ADD_PATROL_ROUTE_LINK(1, 2)
//			ADD_PATROL_ROUTE_LINK(2, 1)
//		CLOSE_PATROL_ROUTE()
//		CREATE_PATROL_ROUTE()
//		TASK_PATROL(piDrugCult[2], cultPatrol2, PAS_CASUAL, TRUE)
		SET_PED_NAME_DEBUG(piDrugCult[2], "piDrugCult[2]")
		
		IF NOT IS_PED_INJURED(piDrugCult[3])
		AND NOT IS_PED_INJURED(piDrugCult[4])
			OPEN_SEQUENCE_TASK(seq)
				TASK_GO_TO_ENTITY(NULL, piDrugCult[4], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, piDrugCult[4])
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
//				TASK_CHAT_TO_PED(piDrugCult[3], piDrugCult[4], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
//				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(piDrugCult[3], seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
//		TASK_CHAT_TO_PED(piDrugCult[3], piDrugCult[4], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
		SET_PED_NAME_DEBUG(piDrugCult[3], "piDrugCult[3]")
//		MAKE_CHATTING_PED_PLAY_ANIM(piDrugCult[3], "gestures@male", "u_understand")

		IF NOT IS_PED_INJURED(piDrugCult[4])
			OPEN_SEQUENCE_TASK(seq)
				TASK_GO_TO_ENTITY(NULL, piDrugCult[3], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, piDrugCult[3])
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
//				TASK_CHAT_TO_PED(piDrugCult[4], piDrugCult[3], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
//				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(piDrugCult[4], seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
//		TASK_CHAT_TO_PED(piDrugCult[4], piDrugCult[3], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
		SET_PED_NAME_DEBUG(piDrugCult[4], "piDrugCult[4]")
//		MAKE_CHATTING_PED_PLAY_ANIM(piDrugCult[4], "gestures@male", "give_me_a_break")
		
		TASK_GUARD_CURRENT_POSITION(piDrugCult[5], 0, 3, TRUE)
//		TASK_STAND_GUARD(piDrugCult[5], vDrugPedGang[5], fDrugPedGang[5], "WORLD_HUMAN_GUARD_STAND")
		SET_PED_NAME_DEBUG(piDrugCult[5], "piDrugCult[5]")
		biRandomEvent = CREATE_BLIP_FOR_COORD(vBriefcaseCoord)
		SET_BLIP_SPRITE(biRandomEvent, RADAR_TRACE_WEED_STASH)
		PRINT_HELP("DRUG_BLIP_START")
		
		cultshootoutStage = STAGE_RUN_CULTSHOOTOUT
		PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ loadAssets() @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
		
	ENDIF
	
ENDPROC

// Clean Up
PROC missionCleanup()
	PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
	
	IF bDoFullCleanUp
		IF NOT bPreCleanup
			TRIGGER_MUSIC_EVENT("RE20_FAIL")
			IF IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_HAS_ATTACKED_DRUG_FARM(FALSE)
			ENDIF
			REPEAT NUM_CULT_DRUG_PEDS index
				IF NOT IS_PED_INJURED(piDrugCult[index])
					IF DOES_BLIP_EXIST(biDrugCult[index])
						REMOVE_BLIP(biDrugCult[index])
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(piDrugCult[index])
				ENDIF
			ENDREPEAT
			#IF IS_DEBUG_BUILD
				IF can_cleanup_script_for_debug
				
	//				REPEAT NUM_CULT_DRUG_PEDS index 
	//					IF DOES_ENTITY_EXIST(piDrugCult[index])
	//						DELETE_PED(piDrugCult[index])
	//					ENDIF
	//				ENDREPEAT
					
	//				IF DOES_ENTITY_EXIST(piBriefcase)
	//					DELETE_OBJECT(piBriefcase)
	//				ENDIF
				ENDIF
			#ENDIF
			
	//		REPEAT NUM_CULT_DRUG_PEDS index
	//			UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vDrugPedGang[index]))
	//		ENDREPEAT
			
			SET_WANTED_LEVEL_MULTIPLIER(1)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
			
			IF bEventComplete
				APPLY_SPECIFIC_DELAY_TO_RANDOM_EVENT(RE_CULTSHOOTOUT, 7)
			ELSE
				APPLY_SPECIFIC_DELAY_TO_RANDOM_EVENT(RE_CULTSHOOTOUT, 1)
			ENDIF
			
			PRINTSTRING("re_cultshootout cleaned up")
		ENDIF
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
	PRINT_HELP("DRUG_BLIP_END")
	TRIGGER_MUSIC_EVENT("RE20_END")
	SET_PLAYER_HAS_ATTACKED_DRUG_FARM(TRUE)
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	bEventComplete = TRUE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	cultshootoutStage = STAGE_CLEAN_UP
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	SET_PLAYER_HAS_ATTACKED_DRUG_FARM(FALSE)
//	TRIGGER_MUSIC_EVENT("RE20_FAIL")
	cultshootoutStage = STAGE_CLEAN_UP
ENDPROC

PROC preCleanup()
	IF bDoFullCleanUp
	AND NOT bPreCleanup
		TRIGGER_MUSIC_EVENT("RE20_FAIL")
		IF IS_PED_INJURED(PLAYER_PED_ID())
			SET_PLAYER_HAS_ATTACKED_DRUG_FARM(FALSE)
		ENDIF
		REPEAT NUM_CULT_DRUG_PEDS index
			IF NOT IS_PED_INJURED(piDrugCult[index])
				IF DOES_BLIP_EXIST(biDrugCult[index])
					REMOVE_BLIP(biDrugCult[index])
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(piDrugCult[index])
			ENDIF
		ENDREPEAT
		SET_WANTED_LEVEL_MULTIPLIER(1)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		
		IF bEventComplete
			APPLY_SPECIFIC_DELAY_TO_RANDOM_EVENT(RE_CULTSHOOTOUT, 7)
		ELSE
			APPLY_SPECIFIC_DELAY_TO_RANDOM_EVENT(RE_CULTSHOOTOUT, 1)
		ENDIF
		
		PRINTSTRING("re_cultshootout cleaned up")
		bPreCleanup = TRUE
	ENDIF
	
	// We do the following to keep the scenario blocking around until player leaves the area
//	WHILE IS_SPHERE_VISIBLE(vInput, 30)
	IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vInput) > 150
		missionCleanup()
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	
	INT i
	REPEAT COUNT_OF(viDrugCult) i
		IF IS_VEHICLE_DRIVEABLE(viDrugCult[i])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viDrugCult[i], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(piDrugCult) i
		IF NOT IS_PED_INJURED(piDrugCult[i])
			IF CAN_PED_SEE_HATED_PED(piDrugCult[i], PLAYER_PED_ID())
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), piDrugCult[i])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), piDrugCult[i])
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(piDrugCult[i]), 5)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piDrugCult[i], PLAYER_PED_ID())
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), piDrugCult[i])
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(piDrugCult[i]) - << 15, 15, 15 >>, GET_ENTITY_COORDS(piDrugCult[i]) + << 15, 15, 15 >>, WEAPONTYPE_SMOKEGRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(piDrugCult[i]) - << 15, 15, 15 >>, GET_ENTITY_COORDS(piDrugCult[i]) + << 15, 15, 15 >>, WEAPONTYPE_GRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(piDrugCult[i]) - << 15, 15, 15 >>, GET_ENTITY_COORDS(piDrugCult[i]) + << 15, 15, 15 >>, WEAPONTYPE_GRENADELAUNCHER, TRUE)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(piDrugCult[i]) - << 15, 15, 15 >>, GET_ENTITY_COORDS(piDrugCult[i]) + << 15, 15, 15 >>, WEAPONTYPE_STICKYBOMB, TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, vCompoundArea1, vCompoundArea2, fAreaWidth)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCompoundArea1, vCompoundArea2, fAreaWidth)
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 2202.4975, 5603.4243, 52.7476 >>, << 20, 20, 20 >>)
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 15
			PRINTSTRING("\n@@@@@@@@@@@@@@@@ GET_ENTITY_SPEED @@@@@@@@@@@@@@@@\n")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC blipBriefcase()

	IF DOES_BLIP_EXIST(biRandomEvent)
		REMOVE_BLIP(biRandomEvent)
		IF DOES_PICKUP_EXIST(piBriefcase)
			IF NOT DOES_BLIP_EXIST(biBriefcase)
				biBriefcase = CREATE_BLIP_FOR_PICKUP(piBriefcase)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC drugGuardsPatrol()
	
	REPEAT NUM_CULT_DRUG_PEDS index
	
		IF IS_PED_INJURED(piDrugCult[index])
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ IS_PED_INJURED @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
			IF DOES_BLIP_EXIST(biDrugCult[index])
				REMOVE_BLIP(biDrugCult[index])
			ENDIF
			iWarnPlayerStage = 2
		ELSE
			IF (IS_PLAYER_INTERFERING_WITH_EVENT() AND iWarnPlayerStage != 2)
			OR (iWarnPlayerStage = 1 AND IS_PED_SHOOTING(PLAYER_PED_ID()))
				PRINTSTRING("\n@@@@@@@@@@@@@@@@ IS_PLAYER_INTERFERING_WITH_EVENT @@@@@@@@@@@@@@@@\n")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF HAS_PLAYER_BEEN_TO_DRUG_FARM()
					ADD_PED_FOR_DIALOGUE(cultshootConversation, 4, piDrugCult[index], "DRUGFARMGOON")
					CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_BACK", CONV_PRIORITY_AMBIENT_HIGH) //We are getting raded again!
				ENDIF
				iWarnPlayerStage = 2
			ENDIF
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ iWarnPlayerStage - CASE ")PRINTINT(iWarnPlayerStage)PRINTSTRING(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
			
			SWITCH iWarnPlayerStage
			
				CASE 0
					IF NOT bWarningGiven[index]
						IF IS_ENTITY_AT_ENTITY(piDrugCult[index], PLAYER_PED_ID(), << 15, 15, 40 >>)
						AND CAN_PED_HEAR_PLAYER(PLAYER_ID(), piDrugCult[index])
						OR CAN_PED_SEE_HATED_PED(piDrugCult[index], PLAYER_PED_ID())
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCompoundArea1, vCompoundArea2, fAreaWidth)
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 2205.9185, 5633.4546, 55.7472 >>, << 15, 15, 40 >>)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
									CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_GOAW", CONV_PRIORITY_AMBIENT_HIGH) //Hey you! Leave now or I'll open fire
								ENDIF
								INT i
								REPEAT COUNT_OF(piDrugCult) i
//									IF IS_ENTITY_AT_ENTITY(piDrugCult[i], PLAYER_PED_ID(), << 20, 20, 40 >>)
//									AND CAN_PED_HEAR_PLAYER(PLAYER_ID(), piDrugCult[index])
//									OR CAN_PED_SEE_HATED_PED(piDrugCult[i], PLAYER_PED_ID())
										SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piDrugCult[index])
										OPEN_SEQUENCE_TASK(seq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
			//								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 4.0)
											TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(piDrugCult[i], seq)
										CLEAR_SEQUENCE_TASK(Seq)
										SET_PED_USING_ACTION_MODE(piDrugCult[i], TRUE)
//									ENDIF
								ENDREPEAT
								bWarningGiven[index] = TRUE
								
								iCounterTimeStartedCultrescue = GET_GAME_TIMER()
								iWarnPlayerStage = 1
							ENDIF
//						ELSE
//							IF NOT IS_PED_INJURED(piDrugCult[3])
//								IF (GET_SCRIPT_TASK_STATUS(piDrugCult[3], SCRIPT_TASK_CHAT_TO_PED) != PERFORMING_TASK)
//								AND (GET_SCRIPT_TASK_STATUS(piDrugCult[3], SCRIPT_TASK_CHAT_TO_PED) != WAITING_TO_START_TASK)
//									TASK_CHAT_TO_PED(piDrugCult[3], piDrugCult[4], CF_DO_QUICK_CHAT, << 0, 0, 0 >>, 0, 0)
//								ENDIF
//							ENDIF
//							IF NOT IS_PED_INJURED(piDrugCult[4])
//								IF (GET_SCRIPT_TASK_STATUS(piDrugCult[4], SCRIPT_TASK_CHAT_TO_PED) != PERFORMING_TASK)
//								AND (GET_SCRIPT_TASK_STATUS(piDrugCult[4], SCRIPT_TASK_CHAT_TO_PED) != WAITING_TO_START_TASK)
//									TASK_CHAT_TO_PED(piDrugCult[4], piDrugCult[3], CF_DO_QUICK_CHAT, << 0, 0, 0 >>, 0, 0)
//								ENDIF
//							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					INT i
					REPEAT COUNT_OF(piDrugCult) i
						IF NOT bWarningGiven[i]
//							IF IS_ENTITY_AT_ENTITY(piDrugCult[i], PLAYER_PED_ID(), << 20, 20, 40 >>)
//							AND CAN_PED_HEAR_PLAYER(PLAYER_ID(), piDrugCult[index])
//							OR CAN_PED_SEE_HATED_PED(piDrugCult[i], PLAYER_PED_ID())
								SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piDrugCult[index])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
	//								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 4.0)
									TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(piDrugCult[i], seq)
								CLEAR_SEQUENCE_TASK(Seq)
								SET_PED_USING_ACTION_MODE(piDrugCult[i], TRUE)
								bWarningGiven[i] = TRUE
//							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT IS_PED_INJURED(piDrugCult[index])
						IF NOT IS_PED_FACING_PED(piDrugCult[index], PLAYER_PED_ID(), 50)
//							TASK_TURN_PED_TO_FACE_ENTITY(piDrugCult[index], PLAYER_PED_ID(), -1)
							TASK_LOOK_AT_ENTITY(piDrugCult[index], PLAYER_PED_ID(), -1)
							SET_PED_KEEP_TASK(piDrugCult[index], TRUE)
						ENDIF
					ENDIF
					
					PRINTLN("@@@@@@@@@@ ", iStationaryCurrentTimer, " - ", iStationaryStartTimer, " = ", iStationaryCurrentTimer  - iStationaryStartTimer, " > 5000 @@@@@@@@@@@@@")
					IF IS_PED_STOPPED(PLAYER_PED_ID())
						IF iStationaryStartTimer = 0
							iStationaryStartTimer = GET_GAME_TIMER()
						ELSE
							iStationaryCurrentTimer = GET_GAME_TIMER()
						ENDIF
					ELSE
						iStationaryCurrentTimer = 0
						iStationaryStartTimer = 0
					ENDIF
					
					IF bWarningGiven[index]
					AND NOT b2ndWarningGiven
						IF GET_GAME_TIMER() - iCounterTimeStartedCultrescue > 6000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
								CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_GOAW", CONV_PRIORITY_AMBIENT_HIGH) //Hey you! Leave now or I'll open fire
								b2ndWarningGiven = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bWarningGiven[index]
					AND NOT b3rdWarningGiven
						IF GET_GAME_TIMER() - iCounterTimeStartedCultrescue > 12000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
								CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_GOAW", CONV_PRIORITY_AMBIENT_HIGH) //Hey you! Leave now or I'll open fire
								b3rdWarningGiven = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bWarningGiven[index]
						IF GET_GAME_TIMER() - iCounterTimeStartedCultrescue > 18000
						OR iStationaryCurrentTimer - iStationaryStartTimer > 5000
						OR IS_PED_IN_COMBAT(piDrugCult[index])
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCompoundArea1, vCompoundArea2, fAreaWidth)
							OR IS_ENTITY_AT_ENTITY(piDrugCult[index], PLAYER_PED_ID(), << 15, 15, 5 >>)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								WAIT(0)
//								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF HAS_PLAYER_BEEN_TO_DRUG_FARM()
										ADD_PED_FOR_DIALOGUE(cultshootConversation, 4, piDrugCult[index], "DRUGFARMGOON")
										CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_BACK", CONV_PRIORITY_AMBIENT_HIGH) //We are getting raded again!
									ELSE
										ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
										CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_FIRE", CONV_PRIORITY_AMBIENT_HIGH) //Open fire!
									ENDIF
//								ENDIF
//								IF NOT IS_PED_INJURED(piDrugCult[index])
//									CLEAR_PED_TASKS(piDrugCult[index])
//									TASK_COMBAT_PED(piDrugCult[index], PLAYER_PED_ID())
//									SET_PED_KEEP_TASK(piDrugCult[index], TRUE)
//								ENDIF
								
	//							bWarningGiven[index] = FALSE
								iWarnPlayerStage = 2
							ELSE
//								PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ IF IS_ENTITY_IN_AREA @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
								IF NOT IS_PED_IN_COMBAT(piDrugCult[index])
//									PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ IF NOT IS_PED_IN_COMBAT @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
									CLEAR_PED_TASKS(piDrugCult[index])
//									IF index = 0
										IF NOT IS_PED_INJURED(piDrugCult[0])
											OPEN_SEQUENCE_TASK(seq)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2205.7690, 5631.9849, 55.5530 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2214.8330, 5635.6274, 55.0029 >>, 8000)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2222.2588, 5613.2202, 53.5576 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2221.9507, 5593.6255, 52.9483 >>, 10000)
												SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(piDrugCult[0], seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_USING_ACTION_MODE(piDrugCult[0], FALSE)
										ENDIF
//										TASK_PATROL(piDrugCult[0], cultPatrol0, PAS_CASUAL, TRUE)
//									ELIF index = 1
										IF NOT IS_PED_INJURED(piDrugCult[1])
											OPEN_SEQUENCE_TASK(seq)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2210.5842, 5580.6836, 52.8132 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2205.4661, 5583.6309, 52.8682 >>, 8000)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2203.0386, 5567.4077, 52.7996 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2199.4983, 5560.7021, 52.8606 >>, 10000)
												SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(piDrugCult[1], seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_USING_ACTION_MODE(piDrugCult[1], FALSE)
										ENDIF
//										TASK_PATROL(piDrugCult[1], cultPatrol1, PAS_CASUAL, TRUE)
//									ELIF index = 2
										IF NOT IS_PED_INJURED(piDrugCult[2])
											OPEN_SEQUENCE_TASK(seq)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2199.6792, 5590.6729, 52.7784 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2210.6458, 5593.5757, 52.8312 >>, 8000)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2232.7615, 5593.5635, 53.0542 >>, PEDMOVE_WALK, -1)
												TASK_TURN_PED_TO_FACE_COORD(NULL, << 2237.9163, 5599.5894, 53.2311 >>, 12000)
												SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(piDrugCult[2], seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_USING_ACTION_MODE(piDrugCult[2], FALSE)
										ENDIF
//										TASK_PATROL(piDrugCult[2], cultPatrol2, PAS_CASUAL, TRUE)
//									ELIF index = 3
										IF NOT IS_PED_INJURED(piDrugCult[3])
										AND NOT IS_PED_INJURED(piDrugCult[4])
											OPEN_SEQUENCE_TASK(seq)
												TASK_GO_TO_ENTITY(NULL, piDrugCult[4], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, piDrugCult[4])
												TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
//												TASK_CHAT_TO_PED(piDrugCult[3], piDrugCult[4], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
//												SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(piDrugCult[3], seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_USING_ACTION_MODE(piDrugCult[3], FALSE)
										ENDIF
//									ELIF index = 4
										IF NOT IS_PED_INJURED(piDrugCult[4])
											OPEN_SEQUENCE_TASK(seq)
												TASK_GO_TO_ENTITY(NULL, piDrugCult[3], DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, piDrugCult[3])
												TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
//												TASK_CHAT_TO_PED(piDrugCult[4], piDrugCult[3], CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
//												SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(piDrugCult[4], seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_USING_ACTION_MODE(piDrugCult[4], FALSE)
										ENDIF
//									ELIF index = 5
										IF NOT IS_PED_INJURED(piDrugCult[5])
											TASK_GUARD_CURRENT_POSITION(piDrugCult[5], 0, 3, TRUE)
											SET_PED_USING_ACTION_MODE(piDrugCult[5], FALSE)
	//										TASK_STAND_GUARD(piDrugCult[5], vDrugPedGang[5], fDrugPedGang[5], "WORLD_HUMAN_GUARD_STAND")
										ENDIF
//									ENDIF
								ENDIF
								REPEAT COUNT_OF(bWarningGiven) i
									bWarningGiven[i] = FALSE
								ENDREPEAT
								bWarningGiven[index] = FALSE
								iWarnPlayerStage = 0
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					blipBriefcase()
					REPEAT NUM_CULT_DRUG_PEDS index
						IF NOT IS_PED_INJURED(piDrugCult[index])
							IF NOT IS_PED_IN_COMBAT(piDrugCult[index])
//								CLEAR_PED_TASKS_IMMEDIATELY(piDrugCult[index])
								SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piDrugCult[index])
								TASK_COMBAT_PED(piDrugCult[index], PLAYER_PED_ID())
								PRINTLN("\n@@@@@@@@@@@@@@@@@@@@@@@@@ piDrugCult[", index, "] IN COMBAT @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n")
								SET_PED_KEEP_TASK(piDrugCult[index], TRUE)
								IF NOT DOES_BLIP_EXIST(biDrugCult[index])
									biDrugCult[index] = CREATE_BLIP_FOR_PED(piDrugCult[index], TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bWarnedAboutStash
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								ADD_PED_FOR_DIALOGUE(cultshootConversation, 3, piDrugCult[index], "SalvadorGang")
								CREATE_CONVERSATION(cultshootConversation, "RECSHAU", "RECSH_STASH", CONV_PRIORITY_AMBIENT_HIGH)
								bWarnedAboutStash = TRUE
							ENDIF
						ENDIF
						
					ENDREPEAT
					
					
//					iWarnPlayerStage ++
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
//	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCompoundArea1, vCompoundArea2, fWidth)

ENDPROC

PROC pickUpBriefcase()
	IF HAS_PICKUP_BEEN_COLLECTED(piBriefcase)
//	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), piBriefcase, <<1.2, 1.2, 2.5>>, FALSE, TRUE, TM_ON_FOOT)
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		IF DOES_BLIP_EXIST(biRandomEvent)
			REMOVE_BLIP(biRandomEvent)
		ENDIF
//		IF DOES_BLIP_EXIST(biBriefcase)
//			REMOVE_BLIP(biBriefcase)
//		ENDIF
//		
		SETTIMERA(0)
//		DELETE_OBJECT(piBriefcase)
		
//		current_player_enum = GET_CURRENT_PLAYER_PED_ENUM()
//		CREDIT_BANK_ACCOUNT(current_player_enum, BAAC_LESTER, 100000,FALSE,TRUE)
		
//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		REMOVE_ANIM_DICT("AMB@MUGGING")
//		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CASE_01)
		bBriefcasePickedUp = TRUE
		
		iWarnPlayerStage = 2
		RunDrug_dealStage = Drug_deal_get_briefcase_2
	ENDIF
	
ENDPROC

PROC drugShootout()
	drugGuardsPatrol()
	pickUpBriefcase()
	
	SWITCH RunDrug_dealStage
		
		CASE Drug_deal_get_briefcase
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ Drug_deal_get_briefcase @@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
			IF DOES_PICKUP_EXIST(piBriefcase)
			
//				vBriefcaseCoord = GET_ENTITY_COORDS(piBriefcase)
//				DRAW_LIGHT_WITH_RANGE(vBriefcaseCoord, 220, 220, 220, 4, 1.0)
				
				IF DOES_BLIP_EXIST(biRandomEvent)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCompoundArea1, vCompoundArea2, fAreaWidth)
						blipBriefcase()
						
						IF NOT bMusicFadeTriggered
							TRIGGER_MUSIC_EVENT("RE20_FADE_RADIO_OUT")
							iCounterTimeStartedCultrescue = GET_GAME_TIMER()
							bMusicFadeTriggered = TRUE
						ENDIF
						
						IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							SET_RANDOM_EVENT_ACTIVE()
						ENDIF
//						biBriefcase = ADD_BLIP_FOR_ENTITY(piBriefcase)
//						SET_BLIP_SCALE(biBriefcase, RE_BLIP_SCALE )
					ENDIF
				ENDIF
			ENDIF
			
			IF bMusicFadeTriggered
				IF GET_GAME_TIMER() - iCounterTimeStartedCultrescue > 6000
					IF NOT bMusicEventTriggered
						TRIGGER_MUSIC_EVENT("RE20_START")
						bMusicEventTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE Drug_deal_get_briefcase_2
			IF IS_PED_INJURED(piDrugCult[0])
			AND IS_PED_INJURED(piDrugCult[1])
			AND IS_PED_INJURED(piDrugCult[2])
			AND IS_PED_INJURED(piDrugCult[3])
			AND IS_PED_INJURED(piDrugCult[4])
				MISSION_PASSED()
			ELIF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 100, 100, 100 >>)
				MISSION_PASSED()
			ENDIF
		BREAK
		
	ENDSWITCH
	
	REPEAT NUM_CULT_DRUG_PEDS index
		IF IS_PED_INJURED(piDrugCult[index])
//			PRINTLN("\n@@@@@@@@@@@@@@@@@@@@@@@@@ piDrugCult[", index, "] KILLED @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n")
			IF DOES_BLIP_EXIST(biDrugCult[index])
				REMOVE_BLIP(biDrugCult[index])
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_cultshootout launched ")
PRINTVECTOR(vInput)
PRINTNL()

//////////////////////////////////

#IF IS_DEBUG_BUILD

	START_WIDGET_GROUP("re_cultshootout")
	
		START_WIDGET_GROUP("Variation Selection Debug")
            START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("cultshootout_PERP_IN_CAR")
				ADD_TO_WIDGET_COMBO("cultshootout_PERP_IN_CAR_2")
            STOP_WIDGET_COMBO("cultshootout_TYPE_VARIATIONS", iRuncultshootoutStage)
			
            IF g_bHoldRandomEventForSelection
                ADD_WIDGET_BOOL("Run mission with debug selection", bRunDebugSelection)
            ENDIF
        STOP_WIDGET_GROUP()
		
    STOP_WIDGET_GROUP()
	
#ENDIF

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	 missionCleanup()
ENDIF

//IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_CULT1)
//    missionCleanup()
//ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR bBriefcasePickedUp
	OR (IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE() AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 500, 500, 500 >>))
//		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
			//IF NOT IS_AREA_OCCUPIED(<<vInput.X-3, vInput.Y-3, vInput.Z-3>>, <<vInput.X+3, vInput.Y+3, vInput.Z+3>>, FALSE, TRUE, TRUE, TRUE, FALSE)
			
				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
						missionCleanup()
					ENDIF
				ENDIF
				
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_DS")
				
				SWITCH cultshootoutStage
				
					CASE STAGE_CREATE
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						loadAssets()
						bDoFullCleanUp = TRUE
					BREAK
					
					CASE STAGE_RUN_CULTSHOOTOUT
						drugShootout()
					BREAK
					
					CASE STAGE_CLEAN_UP
						preCleanup()
					BREAK
					
				ENDSWITCH
				
			#IF IS_DEBUG_BUILD
			
				IF bRunDebugSelection
					bRunDebugSelection = FALSE
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					MISSION_PASSED()
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
					MISSION_FAILED()
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					missionCleanup()
				ENDIF
			#ENDIF
			
//		ELSE
//			missionCleanup()
//		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE

ENDSCRIPT
