//|=======================================================================================|
//|                                   RE_HOMELAND_SECURITY
//|=======================================================================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES =====================================

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_path.sch"
USING "LineActivation.sch"
USING "Minigame_private.sch"
USING "AggroSupport.sch"
USING "Ambient_Speech.sch"
USING "Ambient_approach.sch"
USING "script_blips.sch"
USING "Locates_public.sch"
USING "Ambient_Common.sch"
USING "cutscene_public.sch"
USING "altruist_cult.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventStage
	eS_SET_UP_SCENE,
	eS_EVENT_STAGES,		// event plays out with cops & girls speech and anims
	eS_INTERVENTION_STAGES,
	eS_TAKE_OUT_SECURITY_OFFICERS,
	eS_LOSE_COPS,
	eS_DRIVE_GIRL_HOME,
	eS_DROP_OFF_GIRL_IN_VEH,
	eS_DROP_OFF_GIRL_ON_FOOT,
	eS_EVENT_ENDING,
	eS_CLEAN_UP
ENDENUM
eventStage thisEventStage = eS_SET_UP_SCENE

ENUM playerInterferenceStage
	interfere_NONE,
	interfere_CLOSE_BY,
	interfere_ATTACK_CAR,
	interfere_ENTER_CAR,
	interfere_HARM_VICTIM,
	interfere_DRIVEN_AT
ENDENUM
playerInterferenceStage interferenceType = interfere_NONE

ENUM arrestSceneStage
	arr_FIRST_SPEECH,
	arr_SECOND_SPEECH,
	arr_THIRD_SPEECH,
	arr_FOURTH_SPEECH,
	arr_FIFTH_SPEECH,
	arr_GET_IN_VEHICLE,
	arr_GIRL_ENTER_VEHICLE,
	arr_OFFICER_ENTER_VEHICLE,
	arr_DRIVE_OFF,
	arr_CLOSE_WITH_WEAPON,
	arr_ALLOW_PLAYER_TO_DISARM,
	arr_SECOND_WARNING,
	arr_WAIT_OUT_LAST_CHANCE,
	arr_OFFICER_LOOK_AT_PLAYER,
	arr_TELL_PLAYER_LEAVE,
	arr_GET_IN_QUICK
ENDENUM
arrestSceneStage sceneStage = arr_FIRST_SPEECH

PED_INDEX pedSecurityOfficer[2]
VECTOR vOfficerPos[2]
FLOAT fOfficerHdg[2]
VEHICLE_INDEX vehOfficerCar
VECTOR vOfficerCarPos
FLOAT fOfficerCarHdg
MODEL_NAMES modelOfficer
MODEL_NAMES modelOfficerVehicle

PED_INDEX pedVictim
VECTOR vVictimPos
//VECTOR vVictimAnimRot
MODEL_NAMES modelVictim

BOOL bConvOnHold
TEXT_LABEL_23 ConvResumeLabel = ""
TEXT_LABEL_23 strCurrentRoot

//STRING strDialogueBlock
//INT iGetUpTimer
BOOL bGotUp

BOOL bCorruptCopsLine
BOOL bTrevorsLine

VECTOR vDropOffPos
VECTOR vPostDropOff
VECTOR vCult

BOOL bDoFullCleanUp
BOOL bVariablesInitialised
BOOL bAssetsLoaded

BOOL bToldForGettingOut
BOOL bToldForJacking
BOOL bToldForShooting

BOOL bLosingWantedLevel
BOOL bFightingPlayer0
BOOL bFightingPlayer1
BOOL bEventTriggered

BOOL bToldToPutGunAway
BOOL bToldToPutGunAwayAgain
BOOL bToldToLeave
BOOL bTurnToFaceVic
BOOL bWantedLevSpeech
BOOL bGirlCowering
//BOOL bDriveGirlHome

VEHICLE_INDEX tempPlayersVehID

BLIP_INDEX blipInitialMarker
BLIP_INDEX blipVictim
BLIP_INDEX blipOfficers[2]
BLIP_INDEX blipDropOff
BLIP_INDEX blipCult

INT i // counter

INT iArrestStartTimer
INT iArrestCurrentTimer
INT iTimerDialogue
INT iChatInVehicle
//INT iAggroIndex
INT iBitFieldDontCheck = 0
//INT iLockOnTimer = 0
EAggro aggroReason
BOOL bAggroed

//BOOL bInterfered
INT iToldPutGunAwayTimer
INT iStillArmedTimer

SEQUENCE_INDEX Seq

BOOL bVicInCar
BOOL bArrestFinished
BOOL bBackToIdles
BOOL bAiming
 
BOOL bClearSyncScene

//INT iTurnStage

BOOL bStartOnFootTimer
INT iOnFootStartTimer
INT iOnFootCurrentTimer

BOOL bStartStationaryTimer
BOOL bStationaryWarned
INT iStationaryStartTimer
INT iStationaryCurrentTimer

INT sceneId

BOOL bBundledIn
BOOL bAttachedInVeh

INT iDropOffCutStage
//BOOL bStartScene = FALSE
//CAMERA_INDEX camCutsceneOrigin
//CAMERA_INDEX camCutsceneDest
//INT iCutsceneWaitTimer
//INT iWaitCutTimer
//VEHICLE_INDEX vehTempForScene

REL_GROUP_HASH officersRelGroup

//APPROACHPLAYER DoThanking
structPedsForConversation HomelandDialogueStruct 

PROC missionCleanup()
	PRINTSTRING("@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@")PRINTNL()
    #IF IS_DEBUG_BUILD
        IF can_cleanup_script_for_debug
			IF DOES_ENTITY_EXIST(pedSecurityOfficer[0])
				DELETE_PED(pedSecurityOfficer[0])
			ENDIF
			IF DOES_ENTITY_EXIST(pedSecurityOfficer[1])
				DELETE_PED(pedSecurityOfficer[1])
			ENDIF
			IF DOES_ENTITY_EXIST(pedVictim)
				DELETE_PED(pedVictim)
			ENDIF
			IF DOES_ENTITY_EXIST(vehOfficerCar)
				DELETE_VEHICLE(vehOfficerCar)
			ENDIF
		ENDIF
    #ENDIF
	
	IF bDoFullCleanUp
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		IF g_bAltruistCultFinalDialogueHasPlayed
			TRIGGER_MUSIC_EVENT("AC_STOP")
		ENDIF
		RESET_CULT_STATUS()
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<420.5, -1387, 28>>, <<427, -1379, 29>>)
//		SET_DISPATCH_IDEAL_SPAWN_DISTANCE(200)
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		IF bVariablesInitialised
			SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelOfficer)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelOfficerVehicle)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(modelOfficerVehicle, FALSE)
		ENDIF
		
		INT index
		REPEAT COUNT_OF(pedSecurityOfficer) index
			IF DOES_ENTITY_EXIST(pedSecurityOfficer[index])
				IF NOT IS_PED_INJURED(pedSecurityOfficer[index])
					IF IS_PED_INJURED(PLAYER_PED_ID())
						TASK_AIM_GUN_AT_COORD(pedSecurityOfficer[index], GET_PLAYER_COORDS(PLAYER_ID()), -1)
						SET_PED_KEEP_TASK(pedSecurityOfficer[index], TRUE)
						IF NOT IS_PED_INJURED(pedVictim)
							TASK_COWER(pedVictim, -1)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
						ENDIF
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityOfficer[index], FALSE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(pedSecurityOfficer[index])
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(pedVictim)
			IF NOT IS_PED_INJURED(pedVictim)
				SET_PED_CONFIG_FLAG(pedVictim, PCF_CanSayFollowedByPlayerAudio, TRUE)
				IF IS_PED_IN_GROUP(pedVictim)
					REMOVE_PED_FROM_GROUP(pedVictim)
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
					RESET_PED_LAST_VEHICLE(pedVictim)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
		ENDIF
		IF DOES_ENTITY_EXIST(vehOfficerCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehOfficerCar)
		ENDIF
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
		
	// Do none of the above, just what is below, if the script has been set to clean up straight away
	//  v               v               v         v             v            v           v           v            v         v         v
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@")PRINTNL()
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@")PRINTNL()
	missionCleanup()
ENDPROC

PROC loadAssets()
	REQUEST_MODEL(modelVictim)
	REQUEST_MODEL(modelOfficer)
	REQUEST_MODEL(modelOfficerVehicle)
	REQUEST_ANIM_DICT("random@homelandsecurity")
	IF HAS_MODEL_LOADED(modelVictim)
	AND HAS_MODEL_LOADED(modelOfficer)
	AND HAS_MODEL_LOADED(modelOfficerVehicle)
	AND HAS_ANIM_DICT_LOADED("random@homelandsecurity")
		bAssetsLoaded = TRUE
	ELSE
		REQUEST_MODEL(modelVictim)
		REQUEST_MODEL(modelOfficer)
		REQUEST_MODEL(modelOfficerVehicle)
		REQUEST_ANIM_DICT("random@homelandsecurity")
	ENDIF
ENDPROC

PROC initialiseEventVariables()

	modelVictim = A_F_Y_Tourist_01
	modelOfficer = A_M_O_ACULT_01//S_M_M_Security_01
	modelOfficerVehicle = FUGITIVE//POLICE3
	
	vVictimPos =  << -174.0522, 1905.6106, 197.0466 >>//, 194.8776
//	vVictimAnimRot =  << 4.000, 0.000, -154.000 >>
	
	vOfficerPos[0] = << -174.3279, 1903.9724, 197.0502 >>
	fOfficerHdg[0] = 147.8596 
	
	vOfficerPos[1] = << -174.3233, 1906.3843, 197.0145 >>
	fOfficerHdg[1] = 164.5974
	
	vOfficerCarPos = << -172.4207, 1905.1830, 197.1163 >>
	fOfficerCarHdg = 187.3899 
	
	vDropOffPos = << 469.8768, 2617.5325, 42.2566 >>
	vPostDropOff = << 472.2393, 2611.6108, 42.2676 >>
	
	vCult = << -1034.6, 4918.6, 205.9 >>
	
	bVariablesInitialised = TRUE
	
ENDPROC

PROC changeBlips()
	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipVictim)
		IF NOT IS_ENTITY_DEAD(pedVictim)
			blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
			SHOW_HEIGHT_ON_BLIP(blipVictim, FALSE)
		ENDIF
	ELSE
		SHOW_HEIGHT_ON_BLIP(blipVictim, TRUE)
	ENDIF
//	SET_GROUP_FORMATION_SPACING
	FOR i = 0 to 1
		IF NOT DOES_BLIP_EXIST(blipOfficers[i])
			IF NOT IS_ENTITY_DEAD(pedSecurityOfficer[i])
				blipOfficers[i] = CREATE_BLIP_FOR_PED(pedSecurityOfficer[i])
				SHOW_HEIGHT_ON_BLIP(blipOfficers[i], FALSE)
			ENDIF
		ELSE
			SHOW_HEIGHT_ON_BLIP(blipOfficers[i], TRUE)
		ENDIF
	ENDFOR
ENDPROC

PROC createScene()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	ADD_SCENARIO_BLOCKING_AREA(<< 374.869, -1358.043, 25.667 >>, << 452.410, -1423.882, 38.337 >>)
	ADD_SCENARIO_BLOCKING_AREA(vPostDropOff - << 20, 20, 20 >>, vPostDropOff + << 20, 20, 20 >>)
	CLEAR_AREA_OF_PEDS(vVictimPos, 50)
	SET_PED_PATHS_IN_AREA(<< 419, -1387, 28 >>, << 427, -1370, 29 >>, FALSE)
	
	SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
	
	pedVictim = CREATE_PED(PEDTYPE_MISSION, modelVictim, vVictimPos)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
	SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
	SET_PED_CONFIG_FLAG(pedVictim, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(pedVictim, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_USE_VEHICLE, FALSE)
	STOP_PED_SPEAKING(pedVictim, TRUE)
	SET_AMBIENT_VOICE_NAME(pedVictim, "REHOMGirl")
	
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 2, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HAIR, 0, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TORSO, 1, 4, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_LEG, 0, 5, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_FEET, 1, 0, 0) //(feet)
	
	ADD_RELATIONSHIP_GROUP("OFFICERS", officersRelGroup)
	FOR i = 0 to 1
		pedSecurityOfficer[i] = CREATE_PED(PEDTYPE_COP, modelOfficer, vOfficerPos[i], fOfficerHdg[i])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityOfficer[i], TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelOfficer)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedSecurityOfficer[i], officersRelGroup)
		SET_PED_CONFIG_FLAG(pedSecurityOfficer[i], PCF_DontInfluenceWantedLevel, TRUE)
	ENDFOR
//	SET_AMBIENT_VOICE_NAME(pedSecurityOfficer[0], "S_M_M_GENERICSECURITY_01_WHITE_MINI_02")
	GIVE_WEAPON_TO_PED(pedSecurityOfficer[0], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
	GIVE_WEAPON_TO_PED(pedSecurityOfficer[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, officersRelGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, officersRelGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, officersRelGroup)
	
	#IF IS_DEBUG_BUILD
		SET_PED_NAME_DEBUG(pedSecurityOfficer[0], "pedOfficer[0]")
		SET_PED_NAME_DEBUG(pedSecurityOfficer[1], "pedOfficer[1]")
	#ENDIF
	
//	TASK_AIM_GUN_AT_COORD(pedSecurityOfficer[1], GET_PED_BONE_COORDS(pedVictim, BONETAG_L_FOOT, <<0,0,0>>), -1)
//	
//	OPEN_SEQUENCE_TASK(seq)
//		TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1)
//		TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim)
//		TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_lookaround_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
//		TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_look_at_watch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
//		TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_shake_feet", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
//	SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//	CLOSE_SEQUENCE_TASK(seq)
//	TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], seq)
//	CLEAR_SEQUENCE_TASK(seq)
	
	vehOfficerCar = CREATE_VEHICLE(modelOfficerVehicle, vOfficerCarPos, fOfficerCarHdg)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelOfficerVehicle, TRUE)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehOfficerCar)
	SET_VEHICLE_SIREN(vehOfficerCar, TRUE)
	
	ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 5, pedSecurityOfficer[0], "ACULTMember1")
	ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 4, pedSecurityOfficer[1], "ACULTMember2")
	ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 3, pedVictim, "REHOMGirl")
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
//	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		ADD_PED_FOR_DIALOGUE(HomelandDialogueStruct, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	
	sceneId = CREATE_SYNCHRONIZED_SCENE(<< 0, 0, 0 >>, << 0, 0, 0 >>)
//	sceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehOfficerCar) - << 0, 0, 0 >>, GET_ENTITY_ROTATION(vehOfficerCar))
	ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, vehOfficerCar, 0)//GET_ENTITY_BONE_INDEX_BY_NAME(vehOfficerCar, "chassis"))
	TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@homelandsecurity", "idle_girl", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[0], sceneId, "random@homelandsecurity", "idle_cop_gun", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[1], sceneId, "random@homelandsecurity", "idle_cop_ground", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
	
//	blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vOfficerCarPos)
//	changeBlips()
	
ENDPROC

FUNC BOOL IS_EVERYONE_GONE_OR_DEAD()
	BOOL bPedGone[3]
	bPedGone[0] = FALSE
	bPedGone[1] = FALSE
	bPedGone[2] = FALSE
	
	IF DOES_ENTITY_EXIST(pedSecurityOfficer[0])
		IF IS_PED_INJURED(pedSecurityOfficer[0])
			bPedGone[0] = TRUE
		ENDIF
	ELSE
		bPedGone[0] = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedSecurityOfficer[1])
		IF IS_PED_INJURED(pedSecurityOfficer[1])
			bPedGone[1] = TRUE
		ENDIF
	ELSE
		bPedGone[1] = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedVictim)
		IF IS_PED_INJURED(pedVictim)
			bPedGone[2] = TRUE
		ENDIF
	ELSE
		bPedGone[2] = TRUE
	ENDIF
	
	IF bPedGone[0]
	AND bPedGone[1]
	AND bPedGone[2]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEARBY_AND_ARMED()
	IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1], << 6, 6, 6 >>, FALSE, TRUE, TM_ON_FOOT)
			IF IS_PED_FACING_PED(pedSecurityOfficer[1], PLAYER_PED_ID(), 120)
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 6, 6, 6 >>, FALSE, TRUE, TM_ON_FOOT)
			IF IS_PED_FACING_PED(pedSecurityOfficer[0], PLAYER_PED_ID(), 120)
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
	IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
//		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1], << 6, 6, 6 >>, FALSE)
				IF IS_PED_FACING_PED(pedSecurityOfficer[1], PLAYER_PED_ID(), 120)
					IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 6, 6, 6 >>, FALSE)
			//	IF IS_PED_FACING_PED(pedSecurityOfficer[0], PLAYER_PED_ID(),120)
					IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						RETURN TRUE
					ENDIF
			//	ENDIF
			ENDIF
//		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_REALLY_CLOSE_TO_EVENT()
	IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
	AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
	AND NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 1.5, 1.5, 5 >>, FALSE, TRUE, TM_IN_VEHICLE)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1], << 2.5, 2.5, 5 >>, FALSE, TRUE, TM_IN_VEHICLE)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 1.5, 1.5, 5 >>, FALSE, TRUE, TM_IN_VEHICLE)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 0.5, 0.5, 5 >>, FALSE, TRUE, TM_ON_FOOT)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1], << 0.5, 0.5, 5 >>, FALSE, TRUE, TM_ON_FOOT)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 0.5, 0.5, 5 >>, FALSE, TRUE, TM_ON_FOOT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_INSTANTLY_KILLED_A_GUARD()
	FOR i = 0 to 1
		IF IS_ENTITY_DEAD(pedSecurityOfficer[i])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[i], PLAYER_PED_ID(), TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AND_GIRL_IN_POLICE_VEHICLE()
	IF DOES_ENTITY_EXIST(vehOfficerCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
		AND IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
		AND NOT IS_PED_IN_VEHICLE(pedSecurityOfficer[0], vehOfficerCar)
		AND NOT IS_PED_IN_VEHICLE(pedSecurityOfficer[1], vehOfficerCar)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_LABEL_PLAYING(STRING sRootLabel)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	    
	    IF ARE_STRINGS_EQUAL(sRootLabel, txtRootLabel)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
	IF IS_THIS_CONVERSATION_PLAYING("REHOM_QM")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_GETOUT")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_SHOOT")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_WRONG")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_CULT")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_STOP")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_NOVEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_UNS1")
	OR IS_THIS_CONVERSATION_PLAYING("REHOM_UNS2")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_CONVERSATION(BOOL bCanPlay)
	IF bCanPlay
		IF bConvOnHold
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(HomelandDialogueStruct, "REHOMAU", strCurrentRoot, ConvResumeLabel, CONV_PRIORITY_HIGH)
				PRINTLN("@@@@@@@@@@ CREATE_CONVERSATION_FROM_SPECIFIC_LINE: ", ConvResumeLabel, " ", ConvResumeLabel, " @@@@@@@@@@")
				bConvOnHold = FALSE
			ENDIF
		ENDIF
	ELIF NOT bConvOnHold
	AND IS_SCRIPTED_CONVERSATION_ONGOING()
		strCurrentRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		ConvResumeLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		PRINTLN("@@@@@@@@@@ SAVED ROOT: ", strCurrentRoot, " @@@ SAVED LABEL: ", ConvResumeLabel, " @@@@@@@@@@")
		KILL_FACE_TO_FACE_CONVERSATION()
		bConvOnHold = TRUE
	ENDIF
ENDPROC

PROC wantedLevelCheck()
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		IF NOT IS_PED_INJURED(pedVictim)
			IF NOT bWantedLevSpeech
				IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_COP", CONV_PRIORITY_AMBIENT_HIGH)
					bWantedLevSpeech = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(blipDropOff)
			REMOVE_BLIP(blipDropOff)
		ENDIF
		IF DOES_BLIP_EXIST(blipCult)
			REMOVE_BLIP(blipCult)
		ENDIF
	ENDIF
ENDPROC

PROC screamForHelp()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
					IF GET_SEAT_PED_IS_IN(pedVictim) = VS_BACK_RIGHT
						IF iTimerDialogue < GET_GAME_TIMER()
							CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_FOLL", CONV_PRIORITY_AMBIENT_HIGH)
							iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 4000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC playOutArrestScene()
	iArrestCurrentTimer = GET_GAME_TIMER()
	IF NOT bArrestFinished
		SWITCH sceneStage
		
			CASE arr_FIRST_SPEECH
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
				IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_PLEA", CONV_PRIORITY_AMBIENT_HIGH)
					iArrestStartTimer = GET_GAME_TIMER()
					sceneStage = arr_SECOND_SPEECH
				ENDIF
			BREAK
			
			CASE arr_SECOND_SPEECH
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
				IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_COM", CONV_PRIORITY_AMBIENT_HIGH)
					iArrestStartTimer = GET_GAME_TIMER()
					sceneStage = arr_THIRD_SPEECH
				ENDIF
			BREAK
			
			CASE arr_THIRD_SPEECH
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
				IF (iArrestCurrentTimer - iArrestStartTimer) > 6000
					IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_PLEA", CONV_PRIORITY_AMBIENT_HIGH)
						iArrestStartTimer = GET_GAME_TIMER()
						sceneStage = arr_FOURTH_SPEECH
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_FOURTH_SPEECH
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
				IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_COM2", CONV_PRIORITY_AMBIENT_HIGH)
					iArrestStartTimer = GET_GAME_TIMER()
					sceneStage = arr_FIFTH_SPEECH
				ENDIF
			BREAK
			
			CASE arr_FIFTH_SPEECH
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
				IF (iArrestCurrentTimer - iArrestStartTimer) > 5500
					IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_PLEA", CONV_PRIORITY_AMBIENT_HIGH)
						iArrestStartTimer = GET_GAME_TIMER()
						sceneStage = arr_GET_IN_VEHICLE
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_GET_IN_VEHICLE
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
				AND NOT bToldToLeave
					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				ENDIF
//				IF IS_THIS_LABEL_PLAYING("REHOM_AWAY2_2")
				IF NOT bTurnToFaceVic
				AND TIMERA() > 5000
					PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@ TASK_TURN_PED_TO_FACE_ENTITY @@@@@@@@@@@@@@@@@@@@@\n")
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1, SLF_WHILE_NOT_IN_FOV|SLF_FAST_TURN_RATE|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim, -1)
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], Seq)
					SET_PED_KEEP_TASK(pedSecurityOfficer[0], TRUE)
					bTurnToFaceVic = TRUE
				ENDIF
				IF ((iArrestCurrentTimer - iArrestStartTimer) > 3000)
				OR bToldToLeave
					IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
					AND NOT IS_PED_INJURED(pedSecurityOfficer[0])
					AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.98
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF IS_PED_FACING_PED(pedSecurityOfficer[0], pedVictim, 10)
										IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_ARR", CONV_PRIORITY_AMBIENT_HIGH)
											TASK_CLEAR_LOOK_AT(pedSecurityOfficer[0])
	//										CLEAR_PED_TASKS(pedSecurityOfficer[0])
	//										CLEAR_PED_TASKS(pedSecurityOfficer[1])
	//										CLEAR_PED_TASKS(pedVictim)
											sceneId = CREATE_SYNCHRONIZED_SCENE(<< 0, 0, 0 >>, << 0, 0, 0 >>)
											ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, vehOfficerCar, 0)//GET_ENTITY_BONE_INDEX_BY_NAME(vehOfficerCar, "chassis"))
											TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@homelandsecurity", "exit_girl", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
											TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[0], sceneId, "random@homelandsecurity", "exit_cop_gun", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
											TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[1], sceneId, "random@homelandsecurity", "exit_cop_ground", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
											PLAY_ENTITY_ANIM(vehOfficerCar, "exit_car", "random@homelandsecurity",  SLOW_BLEND_IN, FALSE, FALSE)
											bBundledIn = TRUE
											iArrestStartTimer = GET_GAME_TIMER()
											sceneStage = arr_GIRL_ENTER_VEHICLE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PLAYER_REALLY_CLOSE_TO_EVENT()
//					IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
////						CLEAR_PED_TASKS(pedSecurityOfficer[1])
//						IF NOT IS_PED_INJURED(pedVictim)
//							TASK_AIM_GUN_AT_ENTITY(pedSecurityOfficer[1], pedVictim, -1)
//						ENDIF
//					ENDIF
//					IF NOT IS_PED_INJURED(pedVictim)
////						CLEAR_PED_TASKS(pedVictim)
//						IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
//							TASK_ENTER_VEHICLE(pedVictim, vehOfficerCar, DEFAULT_TIME_NEVER_WARP, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_SPRINT, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
//						ENDIF
//					ENDIF
//					IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//						TASK_ENTER_VEHICLE(pedSecurityOfficer[0], vehOfficerCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
//					ENDIF
					sceneStage = arr_GET_IN_QUICK
				ENDIF
			BREAK
			
			CASE arr_GET_IN_QUICK
//				IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
//					IF NOT IS_PED_INJURED(pedVictim)
//						IF IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
//							IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
//								TASK_ENTER_VEHICLE(pedSecurityOfficer[1], vehOfficerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
//								SET_PED_KEEP_TASK(pedSecurityOfficer[1], TRUE)
//								sceneStage = arr_DRIVE_OFF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
				AND NOT IS_PED_INJURED(pedSecurityOfficer[0])
				AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
				AND NOT IS_PED_INJURED(pedVictim)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_PED_FACING_PED(pedSecurityOfficer[0], pedVictim, 10)
								IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_ARR", CONV_PRIORITY_AMBIENT_HIGH)
									TASK_CLEAR_LOOK_AT(pedSecurityOfficer[0])
									sceneId = CREATE_SYNCHRONIZED_SCENE(<< 0, 0, 0 >>, << 0, 0, 0 >>)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, vehOfficerCar, 0)//GET_ENTITY_BONE_INDEX_BY_NAME(vehOfficerCar, "chassis"))
									TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, "random@homelandsecurity", "exit_girl", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[0], sceneId, "random@homelandsecurity", "exit_cop_gun", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									TASK_SYNCHRONIZED_SCENE(pedSecurityOfficer[1], sceneId, "random@homelandsecurity", "exit_cop_ground", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									PLAY_ENTITY_ANIM(vehOfficerCar, "exit_car", "random@homelandsecurity", REALLY_SLOW_BLEND_IN, FALSE, FALSE)
//									PLAY_SYNCHRONIZED_ENTITY_ANIM(SYNCED_SCENE_BLOCK_MOVER_UPDATE|SYNCED_SCENE_USE_PHYSICS)
									bBundledIn = TRUE
									iArrestStartTimer = GET_GAME_TIMER()
									sceneStage = arr_GIRL_ENTER_VEHICLE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_GIRL_ENTER_VEHICLE
				screamForHelp()
//				IF IS_PLAYER_NEARBY_AND_ARMED()
//					sceneStage = arr_CLOSE_WITH_WEAPON
//				ENDIF
//				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
//				AND NOT bToldToLeave
//					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
//				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.45
						IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
						AND NOT IS_PED_INJURED(pedVictim)
						AND NOT IS_PED_INJURED(pedSecurityOfficer[0])
//							SET_ENTITY_NO_COLLISION_ENTITY(pedVictim, vehOfficerCar, TRUE)
//							SET_ENTITY_NO_COLLISION_ENTITY(pedSecurityOfficer[0], vehOfficerCar, TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.90
						IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
							IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//								CLEAR_PED_TASKS(pedSecurityOfficer[0])
								TASK_ENTER_VEHICLE(pedSecurityOfficer[0], vehOfficerCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
								sceneStage = arr_OFFICER_ENTER_VEHICLE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_OFFICER_ENTER_VEHICLE
				screamForHelp()
//				IF IS_PLAYER_NEARBY_AND_ARMED()
//					sceneStage = arr_CLOSE_WITH_WEAPON
//				ENDIF
//				IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
//				AND NOT bToldToLeave
//					sceneStage = arr_OFFICER_LOOK_AT_PLAYER
//				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.95
						IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
							IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
//								CLEAR_PED_TASKS(pedSecurityOfficer[1])
								TASK_ENTER_VEHICLE(pedSecurityOfficer[1], vehOfficerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
								SET_PED_KEEP_TASK(pedSecurityOfficer[1], TRUE)
								sceneStage = arr_DRIVE_OFF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_DRIVE_OFF
				screamForHelp()
				IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
				AND NOT IS_PED_INJURED(pedSecurityOfficer[0])
				AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
					IF IS_PED_SITTING_IN_VEHICLE(pedVictim, vehOfficerCar)
					AND IS_PED_SITTING_IN_VEHICLE(pedSecurityOfficer[0], vehOfficerCar)
					AND IS_PED_SITTING_IN_VEHICLE(pedSecurityOfficer[1], vehOfficerCar)
						TASK_VEHICLE_DRIVE_WANDER(pedSecurityOfficer[0], vehOfficerCar, 15, DRIVINGMODE_AVOIDCARS_RECKLESS)
						SET_VEHICLE_SIREN(vehOfficerCar, TRUE)
						bArrestFinished = TRUE
					ENDIF
				ENDIF
			BREAK
			
			// reacting to player close with a gun
			CASE arr_CLOSE_WITH_WEAPON
				OPEN_SEQUENCE_TASK(Seq)
					TASK_CLEAR_LOOK_AT(NULL)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000)
					TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
				CLOSE_SEQUENCE_TASK(Seq)
				IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//					CLEAR_PED_TASKS(pedSecurityOfficer[0])
					TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], Seq)
				ENDIF
				CLEAR_SEQUENCE_TASK(Seq)
				
				IF NOT bToldToPutGunAway
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
					AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_GUN", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
					AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
							PLAY_PED_AMBIENT_SPEECH(pedSecurityOfficer[0], "GUN_COOL")
						ENDIF
					ENDIF
					bToldToPutGunAway = TRUE
					iToldPutGunAwayTimer = GET_GAME_TIMER()
				ENDIF
				sceneStage = arr_ALLOW_PLAYER_TO_DISARM
			BREAK
			
			// check whether player puts weapon away after being told to do so
			CASE arr_ALLOW_PLAYER_TO_DISARM
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 15, 15, 15 >>)
					iStillArmedTimer = GET_GAME_TIMER()
				ELSE
				//	IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
						sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				//	ENDIF
				ENDIF
				IF (iStillArmedTimer - iToldPutGunAwayTimer) > 6000
				AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				AND NOT bToldToPutGunAwayAgain
					IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_STAY3", CONV_PRIORITY_AMBIENT_HIGH)
						iToldPutGunAwayTimer = GET_GAME_TIMER()
						bToldToPutGunAwayAgain = TRUE
					ENDIF
					sceneStage = arr_SECOND_WARNING
				ENDIF
			BREAK
			
			CASE arr_SECOND_WARNING
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 15, 15, 15 >>)
					iStillArmedTimer = GET_GAME_TIMER()
				ELSE
				//	IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
						sceneStage = arr_OFFICER_LOOK_AT_PLAYER
				//	ENDIF
				ENDIF
				IF (((iStillArmedTimer - iToldPutGunAwayTimer) > 9000) AND (IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)))
				OR bAiming
					IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_SHOO", CONV_PRIORITY_AMBIENT_HIGH)
						sceneStage = arr_WAIT_OUT_LAST_CHANCE
					ENDIF
				ENDIF
			BREAK
			
			CASE arr_WAIT_OUT_LAST_CHANCE
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					iStillArmedTimer = GET_GAME_TIMER()
				ELSE
				//	IF IS_PLAYER_NEARBY_WITHOUT_A_WEAPON()
						sceneStage = arr_OFFICER_LOOK_AT_PLAYER
//					ELSE
//						sceneStage = arr_CLOSE_WITH_WEAPON
//					ENDIF
				ENDIF
				IF (iStillArmedTimer - iToldPutGunAwayTimer) > 13000
					thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
				ENDIF
			BREAK
			
			CASE arr_OFFICER_LOOK_AT_PLAYER
				IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//					CLEAR_PED_TASKS(pedSecurityOfficer[0])
					OPEN_SEQUENCE_TASK(Seq)
						TASK_CLEAR_LOOK_AT(NULL)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], Seq)
					CLEAR_SEQUENCE_TASK(Seq)
				ENDIF
				IF bClearSyncScene
					IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
						IF NOT IS_PED_INJURED(pedVictim)
							IF GET_SCRIPT_TASK_STATUS(pedSecurityOfficer[1], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
//								CLEAR_PED_TASKS(pedSecurityOfficer[1])
								OPEN_SEQUENCE_TASK(Seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim)
									TASK_LOOK_AT_ENTITY(NULL, pedVictim, 20000)
									TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, pedVictim, << -1, 0, 0 >>, PEDMOVEBLENDRATIO_WALK)
								CLOSE_SEQUENCE_TASK(Seq)
								TASK_PERFORM_SEQUENCE(pedSecurityOfficer[1], Seq)
								CLEAR_SEQUENCE_TASK(Seq)
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedSecurityOfficer[1], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
//								CLEAR_PED_TASKS(pedSecurityOfficer[1])
								OPEN_SEQUENCE_TASK(Seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000)
								CLOSE_SEQUENCE_TASK(Seq)
								TASK_PERFORM_SEQUENCE(pedSecurityOfficer[1], Seq)
								CLEAR_SEQUENCE_TASK(Seq)
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedVictim)
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
//							CLEAR_PED_TASKS(pedVictim)
							TASK_SEEK_COVER_FROM_PED(pedVictim, PLAYER_PED_ID(), -1)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
						ENDIF
					ENDIF
				ENDIF
				sceneStage = arr_TELL_PLAYER_LEAVE
			BREAK
			
			CASE arr_TELL_PLAYER_LEAVE
				IF IS_PLAYER_NEARBY_AND_ARMED()
					sceneStage = arr_CLOSE_WITH_WEAPON
				ENDIF
				IF IS_PLAYER_REALLY_CLOSE_TO_EVENT()
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						IF NOT bClearSyncScene
							IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
//								CLEAR_PED_TASKS(pedSecurityOfficer[1])
							ENDIF
							IF NOT IS_PED_INJURED(pedVictim)
//								CLEAR_PED_TASKS(pedVictim)
								TASK_SEEK_COVER_FROM_PED(pedVictim, PLAYER_PED_ID(), -1)
								SET_PED_KEEP_TASK(pedVictim, TRUE)
							ENDIF
							bClearSyncScene = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//					IF IS_PED_FACING_PED(pedSecurityOfficer[0], PLAYER_PED_ID(), 150)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT bToldToLeave
							IF GET_RANDOM_BOOL()
								CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_AWAY1", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_AWAY2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							bToldToLeave = TRUE
						ENDIF
						bTurnToFaceVic = FALSE
						SETTIMERA(0)
						
						iArrestStartTimer = (GET_GAME_TIMER() - 4500)
						sceneStage = arr_GET_IN_VEHICLE
//					ENDIF
				ENDIF
			
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC continualTurnToFacePlayer(PED_INDEX targetPed, INT& stage) //, INT &turnTimer, INT timeGapTurn)
	
	FLOAT fFacingAngle
	
	SWITCH stage
		CASE 0 // TURN TASK
			IF NOT IS_PED_INJURED(targetPed)
			//	CLEAR_PED_TASKS(targetPed)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(targetPed, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				stage ++
			ENDIF
		BREAK
		
		CASE 1 // TURNING 
			fFacingAngle = 50
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
					// ACHIEVED TASK
					stage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2 // WAITING TO TURN
			fFacingAngle = 120
			IF NOT IS_PED_INJURED(targetPed)
				IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
					stage = 0
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC guardGoBackToIdles()
	IF NOT bBackToIdles
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim)
			TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_lookaround_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
			TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_look_at_watch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
			TASK_PLAY_ANIM(NULL, "cop_wander_idles", "idle_shake_feet", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
		SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], seq)
		CLEAR_SEQUENCE_TASK(seq)
		bBackToIdles = TRUE
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	
	IF NOT bAggroed
		
		IF IS_PROJECTILE_IN_AREA(vOfficerCarPos - << 30, 30, 30 >>, vOfficerCarPos + << 30, 30, 30 >>, TRUE)
		OR IS_BULLET_IN_BOX(vOfficerCarPos - << 30, 30, 30 >>, vOfficerCarPos + << 30, 30, 30 >>, TRUE)
			interferenceType = interfere_ATTACK_CAR
			RETURN TRUE
		ENDIF
		
		IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
		AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
			IF IS_ENTITY_AT_ENTITY(pedSecurityOfficer[0], PLAYER_PED_ID(), << 20, 20, 20 >>)
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
				OR (IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()) AND IS_ENTITY_AT_ENTITY(pedSecurityOfficer[0], PLAYER_PED_ID(), << 6, 6, 6 >>))
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurityOfficer[0])
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurityOfficer[0])
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurityOfficer[1])
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurityOfficer[1])
						IF CAN_PED_SEE_HATED_PED(pedSecurityOfficer[0], PLAYER_PED_ID())
						OR CAN_PED_SEE_HATED_PED(pedSecurityOfficer[1], PLAYER_PED_ID())
							PRINTSTRING("\n@@@@@@@@@@@@@@ IS_PLAYER_INTERFERING_WITH_EVENT @@@@@@@@@@@@@@\n")
							interferenceType = interfere_ATTACK_CAR
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		VEHICLE_INDEX tempPlayersLastVehicle
		tempPlayersLastVehicle = GET_PLAYERS_LAST_VEHICLE()
		
		IF DOES_ENTITY_EXIST(vehOfficerCar)
			IF NOT IS_ENTITY_DEAD(vehOfficerCar)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehOfficerCar, PLAYER_PED_ID())
					interferenceType = interfere_ATTACK_CAR
					RETURN TRUE
				ENDIF
				IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
				AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.1
					interferenceType = interfere_ATTACK_CAR
					RETURN TRUE
				ENDIF
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
					interferenceType = interfere_ATTACK_CAR
					RETURN TRUE
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(tempPlayersLastVehicle)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehOfficerCar, tempPlayersLastVehicle)
						interferenceType = interfere_ATTACK_CAR
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehOfficerCar, PLAYER_PED_ID())
					interferenceType = interfere_ATTACK_CAR
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK2)
				VEHICLE_INDEX tempTowVehicle
				tempTowVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(vehOfficerCar)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempTowVehicle, vehOfficerCar)
						interferenceType = interfere_ATTACK_CAR
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
			OR IS_ENTITY_DEAD(pedSecurityOfficer[0])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[0], PLAYER_PED_ID())
					interferenceType = interfere_DRIVEN_AT
					RETURN TRUE
				ENDIF
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0])
				AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.1
					interferenceType = interfere_DRIVEN_AT
					RETURN TRUE
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(tempPlayersLastVehicle)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[0], tempPlayersLastVehicle)
						interferenceType = interfere_DRIVEN_AT
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
			OR IS_ENTITY_DEAD(pedSecurityOfficer[1])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[1], PLAYER_PED_ID())
					interferenceType = interfere_DRIVEN_AT
					RETURN TRUE
				ENDIF
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1])
				AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.1
					interferenceType = interfere_DRIVEN_AT
					RETURN TRUE
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(tempPlayersLastVehicle)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[1], tempPlayersLastVehicle)
						interferenceType = interfere_DRIVEN_AT
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
		
		IF NOT IS_ENTITY_DEAD(pedVictim)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
				interferenceType = interfere_HARM_VICTIM
				RETURN TRUE
			ENDIF
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedVictim)
				interferenceType = interfere_HARM_VICTIM
				RETURN TRUE
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(tempPlayersLastVehicle)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, tempPlayersLastVehicle)
					interferenceType = interfere_HARM_VICTIM
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
				interferenceType = interfere_HARM_VICTIM
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

PROC chatInterruptions()

	IF NOT IS_PED_INJURED(pedVictim)
	
		IF NOT bToldForGettingOut
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE(), TRUE)
				AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					//"Why are you leaving, come back here"
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_GETOUT", CONV_PRIORITY_AMBIENT_HIGH)
					bToldForGettingOut = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
					bToldForGettingOut = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bToldForJacking	
			IF IS_PED_JACKING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Ok, whatever it takes to get back home"
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_JACK", CONV_PRIORITY_AMBIENT_HIGH)
				bToldForJacking = TRUE
			ENDIF
		ELIF NOT IS_PED_JACKING(PLAYER_PED_ID())
			bToldForJacking = FALSE
		ENDIF
		
		IF NOT bToldForShooting
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Why are you shooting!?"
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_SHOOT", CONV_PRIORITY_AMBIENT_HIGH)
				bToldForShooting = TRUE
			ENDIF
		ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
			bToldForShooting = FALSE
		ENDIF
		
	ENDIF
	
ENDPROC

PROC groupSeparationCheck()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 8, 8, 8 >>)
		OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
			IF DOES_BLIP_EXIST(blipVictim)
				REMOVE_BLIP(blipVictim)
			ENDIF
			IF NOT IS_PED_IN_GROUP(pedVictim)
				SET_PED_AS_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
			ENDIF
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF NOT DOES_BLIP_EXIST(blipDropOff)
					blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPos, TRUE)
				ENDIF
				IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
					IF NOT DOES_BLIP_EXIST(blipDropOff)
						blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPos, TRUE)
					ENDIF
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				AND NOT IS_CULT_FINISHED()
					IF NOT DOES_BLIP_EXIST(blipCult)
						blipCult = CREATE_BLIP_FOR_COORD(vCult)
						SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
						PRINT_CULT_HELP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipVictim)
				blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
			ENDIF
			IF DOES_BLIP_EXIST(blipDropOff)
				REMOVE_BLIP(blipDropOff)
			ENDIF
//			IF DOES_BLIP_EXIST(blipCult)
//				REMOVE_BLIP(blipCult)
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC onFootCheck()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_ON_FOOT(pedVictim)
			IF NOT bStartOnFootTimer
				iOnFootStartTimer = GET_GAME_TIMER()
				bStartOnFootTimer = TRUE
			ELSE
				iOnFootCurrentTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			bStartOnFootTimer = FALSE
		ENDIF
		
		IF ((iOnFootCurrentTimer - iOnFootStartTimer) > 180000)
			IF IS_PED_IN_GROUP(pedVictim)
				REMOVE_PED_FROM_GROUP(pedVictim)
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_WK", CONV_PRIORITY_AMBIENT_HIGH)
			//	"Well, if we're going to walk I can do that by myself.
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC stationaryCheck()
	IF IS_PED_STOPPED(PLAYER_PED_ID())
		IF NOT bStartStationaryTimer
			iStationaryStartTimer = GET_GAME_TIMER()
			bStartStationaryTimer = TRUE
		ELSE
			iStationaryCurrentTimer = GET_GAME_TIMER()
		ENDIF 
	ELSE
		iStationaryCurrentTimer = 0
		bStartStationaryTimer = FALSE
		bStationaryWarned = FALSE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 50000)
	AND NOT bStationaryWarned
		MANAGE_CONVERSATION(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_QM", CONV_PRIORITY_AMBIENT_HIGH)
		//"I can probably get there quicker myself"
		bStationaryWarned = TRUE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 60000)
	AND bStationaryWarned
		IF NOT IS_PED_INJURED(pedVictim)
//			IF IS_ENTITY_AT_COORD(pedDrunk, vHitcherDestinationPos, << 200, 200, 200 >>)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//			ELSE
				TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
				Make_Ped_Drunk(pedVictim, 120000)
//			ENDIF
			SET_PED_KEEP_TASK(pedVictim, TRUE)
			IF IS_PED_IN_GROUP(pedVictim)
				REMOVE_PED_FROM_GROUP(pedVictim)
			ENDIF
		ENDIF
		MISSION_FAILED()
	ENDIF
ENDPROC

PROC chatInVehicle()

	IF NOT IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
		MANAGE_CONVERSATION(TRUE)
	ENDIF


	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SWITCH iChatInVehicle
			CASE 0
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_WTF_M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_WTF_F", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 1
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_CORR", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 2
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_TALK", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 3
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_CAR_M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_CAR_F", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 4
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_TALK2", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 5
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_REJ_M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_REJ_F", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 6
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_TALK2b", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 7
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_TALK3", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
			
			CASE 8
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_INT_M", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_INT_F", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 9
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_NOBAN", CONV_PRIORITY_AMBIENT_HIGH)
				iChatInVehicle ++
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL IS_GIRL_IN_PLAYERS_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		tempPlayersVehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(tempPlayersVehID)
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_PED_SITTING_IN_VEHICLE(pedVictim, tempPlayersVehID)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC setOfficersFightingPlayer()
//	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
//		IF NOT bClearSyncScene
//			IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
////				CLEAR_PED_TASKS(pedSecurityOfficer[1])
//			ENDIF
//			IF NOT IS_PED_INJURED(pedVictim)
////				CLEAR_PED_TASKS(pedVictim)
//				TASK_SEEK_COVER_FROM_PED(pedVictim, PLAYER_PED_ID(), -1)
//				SET_PED_KEEP_TASK(pedVictim, TRUE)
//			ENDIF
//			bClearSyncScene = TRUE
//		ENDIF
//	ENDIF
	
	IF NOT bFightingPlayer0
//////////		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
//////////		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//////////		SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
		IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
			SET_PED_COMBAT_MOVEMENT(pedSecurityOfficer[0], CM_STATIONARY)
			CLEAR_PED_TASKS(pedSecurityOfficer[0])
			OPEN_SEQUENCE_TASK(Seq)
				TASK_CLEAR_LOOK_AT(NULL)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(Seq)
			TASK_PERFORM_SEQUENCE(pedSecurityOfficer[0], Seq)
			CLEAR_SEQUENCE_TASK(Seq)
			SET_PED_KEEP_TASK(pedSecurityOfficer[0], TRUE)
			IF DOES_BLIP_EXIST(blipOfficers[0])
				SET_BLIP_AS_FRIENDLY(blipOfficers[0], FALSE)
			ENDIF
			SETTIMERB(0)
			bFightingPlayer0 = TRUE
		ELSE
			SETTIMERB(500)
			bFightingPlayer0 = TRUE
		ENDIF
	ENDIF
	
	IF NOT bFightingPlayer1
//	AND bFightingPlayer0
//		IF TIMERB() > 750
		IF DOES_BLIP_EXIST(blipOfficers[1])
			SET_BLIP_AS_FRIENDLY(blipOfficers[1], FALSE)
		ENDIF
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
			PLAY_PED_AMBIENT_SPEECH(pedSecurityOfficer[1], "GENERIC_INSULT_HIGH")//GENERIC_CURSE_HIGH or GENERIC_SHOCKED_HIGH
			SET_PED_COMBAT_MOVEMENT(pedSecurityOfficer[1], CM_WILLADVANCE)
			IF (IS_ENTITY_PLAYING_ANIM(pedSecurityOfficer[1], "random@homelandsecurity", "idle_cop_ground") OR IS_SYNCHRONIZED_SCENE_RUNNING(sceneId))
			AND NOT IS_PED_RAGDOLL(pedSecurityOfficer[1])
				SET_RAGDOLL_BLOCKING_FLAGS(pedSecurityOfficer[1], RBF_PLAYER_IMPACT)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_PLAY_ANIM(NULL, "random@homelandsecurity", "idle_to_stand_cop_ground", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, 1400, AF_EXIT_AFTER_INTERRUPTED)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedSecurityOfficer[1], Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				SET_PED_KEEP_TASK(pedSecurityOfficer[1], TRUE)
				bFightingPlayer1 = TRUE
			ELSE
	//			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim, TRUE)
				TASK_COMBAT_PED(pedSecurityOfficer[1], PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedSecurityOfficer[1], TRUE)
				bFightingPlayer1 = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
			IF NOT IS_ENTITY_PLAYING_ANIM(pedSecurityOfficer[1], "random@homelandsecurity", "idle_cop_ground")
			AND NOT IS_ENTITY_PLAYING_ANIM(pedSecurityOfficer[1], "random@homelandsecurity", "idle_to_stand_cop_ground", ANIM_SCRIPT)
			AND GET_SCRIPT_TASK_STATUS(pedSecurityOfficer[1], SCRIPT_TASK_COMBAT) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(pedSecurityOfficer[1], SCRIPT_TASK_COMBAT) != PERFORMING_TASK
	//			CLEAR_PED_TASKS(pedVictim)
				bFightingPlayer1 = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC reactionsToPlayerDamagingCar()
	thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
ENDPROC

PROC reactionsToAttackingVic()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	WAIT(0)
	CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_AGG", CONV_PRIORITY_AMBIENT_HIGH)
	
	thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
ENDPROC

PROC reactionsToPlayerRammingPedsInCar()
	thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
ENDPROC

PROC endDropOffScene()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
//			vehTempForScene = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//			IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_HOME", CONV_PRIORITY_AMBIENT_HIGH)
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//				DISPLAY_HUD(FALSE)
//				DISPLAY_RADAR(FALSE)
//				IF NOT IS_PED_INJURED(pedVictim)
//				blipVictim = ADD_BLIP_FOR_ENTITY(pedVictim)
//					CLEAR_PED_TASKS(pedVictim)
//					TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 10000)
//					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, 10000)
//				ENDIF
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 1
//			IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//				camCutsceneOrigin = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//				camCutsceneDest = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehTempForScene, <<-1.7797, 1.0093, 0.9447>>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehTempForScene, <<0.9229, -0.2740, 0.7245>>)
//				SET_CAM_FOV(camCutsceneOrigin, 30.5154)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehTempForScene, <<-1.7797, 1.0093, 0.9447>>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehTempForScene, <<1.0537, 0.0522, 0.7094>>)
//				SET_CAM_FOV(camCutsceneDest, 30.5154)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				SHAKE_CAM(camCutsceneDest, "HAND_SHAKE", 0.3)
//				SHAKE_CAM(camCutsceneOrigin, "HAND_SHAKE", 0.3)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 3500)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedVictim)
//					TASK_LEAVE_ANY_VEHICLE(pedVictim)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 2500)
					iDropOffCutStage ++
//				ENDIF
//			ENDIF
		BREAK
		CASE 3
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedVictim)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPostDropOff, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 38.9844)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedVictim, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
				ENDIF
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				camCutsceneDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//					ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehTempForScene, <<-0.9464, 4.0200, 1.0435>>)
//					POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehTempForScene, <<0.0274, 1.1896, 0.8425>>)
//					SET_CAM_FOV(camCutsceneOrigin, 30.5154)
//					ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehTempForScene, <<-0.9464, 4.0200, 1.0435>>)
//					POINT_CAM_AT_ENTITY(camCutsceneDest, vehTempForScene, <<-0.1900, 1.1239, 0.8431>>)
//				ENDIF
//				SET_CAM_FOV(camCutsceneDest, 30.5154)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2500)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 4
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//					IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehTempForScene, SC_DOOR_FRONT_RIGHT) > 0.0
//						SET_VEHICLE_DOOR_SHUT(vehTempForScene, SC_DOOR_FRONT_RIGHT, TRUE)
//					ENDIF
//				ENDIF
//				DISPLAY_HUD(TRUE)
//				DISPLAY_RADAR(TRUE)
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//				SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
//				SET_CAM_ACTIVE(camCutsceneDest, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				DELETE_PED(pedVictim)
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				MISSION_PASSED()
//			ENDIF
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			iDropOffCutStage = 4
//		ENDIF
//	ENDIF
ENDPROC

//=================================== START OF MAIN SCRIPT ================================

SCRIPT (coords_struct in_coords)

	PRINTNL()
	PRINTSTRING("re_homeland_security")
	PRINTVECTOR(in_coords.vec_coord[0])
	PRINTNL()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedVictim)
			IF IS_PED_IN_GROUP(pedVictim)
				REMOVE_PED_FROM_GROUP(pedVictim)
			ENDIF
		ENDIF
		missionCleanup()
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(in_coords.vec_coord[0])
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	// Mission Loop -------------------------------------------//
	WHILE TRUE
	WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bLosingWantedLevel
		OR bVicInCar
//			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				
				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
						missionCleanup()
					ENDIF
				ENDIF
				
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_RA")
				
				SWITCH ambStage
					CASE ambCanRun
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						
						IF NOT bVariablesInitialised
							initialiseEventVariables()
						ELSE
							loadAssets()
						ENDIF
						
						IF bAssetsLoaded 
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ENDIF
						
					BREAK
					
					CASE ambRunning
						IF IS_PLAYER_PLAYING(PLAYER_ID())
						
							SWITCH thisEventStage
							
								CASE eS_SET_UP_SCENE
									createScene()
									thisEventStage = eS_EVENT_STAGES
								BREAK
								
								CASE eS_EVENT_STAGES
								
//									IF NOT HAS_PLAYER_AGGROED_PEDS(pedSecurityOfficer, iAggroIndex, aggroReason, iLockOnTimer, iBitFieldDontCheck, bAggroed, 7)
									//AND NOT HAS_PLAYER_AGGROED_PED(pedSecurityOfficer[1], aggroReason, iLockOnTimer, iBitFieldDontCheck, bAggroed, 7)
									IF NOT IS_PLAYER_INTERFERING_WITH_EVENT()
										
										//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 362.4543, -1344.9595, 39.3181 >>, << 472.9133, -1440.7440, 28.0954 >>, 141.2500)
										IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vVictimPos, << 75, 75, 75 >>)
										AND NOT IS_ENTITY_OCCLUDED(pedVictim)
										OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 15, 15, 15 >>)
											IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
												TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, -1)
												SET_WANTED_LEVEL_MULTIPLIER(0.1)
												changeBlips()
												SET_RANDOM_EVENT_ACTIVE()
											ENDIF
											bEventTriggered = TRUE
										ENDIF
										
										IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
										AND NOT IS_PED_INJURED(pedSecurityOfficer[1])
											IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[0], PLAYER_PED_ID())
											OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityOfficer[1], PLAYER_PED_ID())
												changeBlips()
												bEventTriggered = TRUE
											ENDIF
										ENDIF
										
										IF bEventTriggered
											playOutArrestScene()
										ENDIF
										
	//									IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
	//										IF iArrestSceneStage < 5
	//											IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], <<5, 5, 5>>)
	//												continualTurnToFacePlayer(pedSecurityOfficer[0], iTurnStage)
	//											ELSE
	//												guardGoBackToIdles()
	//											ENDIF
	//										ENDIF
	//									ENDIF
										
										IF NOT IS_PED_INJURED(pedVictim)
										AND IS_VEHICLE_DRIVEABLE(vehOfficerCar)
											IF IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
												bVicInCar = TRUE
											ENDIF
										ENDIF
										
										IF bArrestFinished
											IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
												IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehOfficerCar, << 150, 150, 150 >>)
													thisEventStage = eS_CLEAN_UP
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
									
									IF HAS_PLAYER_INSTANTLY_KILLED_A_GUARD()
										changeBlips()
										thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
									ELSE
//										IF HAS_PLAYER_AGGROED_PEDS(pedSecurityOfficer, iAggroIndex,  aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
									//	OR HAS_PLAYER_AGGROED_PED(pedSecurityOfficer[1], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)
										IF IS_PLAYER_INTERFERING_WITH_EVENT()
											changeBlips()
											KILL_FACE_TO_FACE_CONVERSATION()
											thisEventStage = eS_INTERVENTION_STAGES
										ENDIF
									ENDIF
								BREAK
								
								CASE eS_INTERVENTION_STAGES
									
//									IF HAS_PLAYER_AGGROED_PEDS(pedSecurityOfficer,	iAggroIndex, aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed,7)
								//	OR HAS_PLAYER_AGGROED_PED(pedSecurityOfficer[1], aggroReason, iLockOnTimer,  iBitFieldDontCheck, bAggroed, 7)	
									
										SWITCH aggroReason
										
											CASE EAggro_Danger //player is wanted or shot a victim near them
												PRINTSTRING("\n@@@@@@@@@@@@@@ EAggro_Danger @@@@@@@@@@@@@@\n")
												thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
											BREAK
											
											CASE EAggro_ShotNear //shot near or near miss
												PRINTSTRING("\n@@@@@@@@@@@@@@ EAggro_ShotNear @@@@@@@@@@@@@@\n")
												thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
											BREAK
											
//											CASE EAggro_HostileOrEnemy //stick up
//												bAiming = TRUE
//												thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
//											BREAK
											
											CASE EAggro_Attacked //attacked
												PRINTSTRING("\n@@@@@@@@@@@@@@ EAggro_Attacked @@@@@@@@@@@@@@\n")
												thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
											BREAK
											
											CASE EAggro_HeardShot
												PRINTSTRING("\n@@@@@@@@@@@@@@ EAggro_HeardShot @@@@@@@@@@@@@@\n")
												thisEventStage = eS_TAKE_OUT_SECURITY_OFFICERS
											BREAK
											
											CASE EAggro_Shoved
												PRINTSTRING("\n@@@@@@@@@@@@@@ EAggro_Shoved @@@@@@@@@@@@@@\n")
												IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
													bAiming = TRUE
													sceneStage = arr_CLOSE_WITH_WEAPON
												ELSE
													sceneStage = arr_OFFICER_LOOK_AT_PLAYER
												ENDIF
												thisEventStage = eS_EVENT_STAGES
												// temporary reaction 
											BREAK
											
										ENDSWITCH
										
//									ENDIF
									
									IF IS_PLAYER_INTERFERING_WITH_EVENT()
										SWITCH interferenceType
											CASE interfere_ATTACK_CAR
												reactionsToPlayerDamagingCar()
											BREAK
											CASE interfere_HARM_VICTIM
												reactionsToAttackingVic()
											BREAK
											CASE interfere_DRIVEN_AT
												reactionsToPlayerRammingPedsInCar()
											BREAK
										ENDSWITCH
									ENDIF
									
								BREAK
								
								CASE eS_TAKE_OUT_SECURITY_OFFICERS
//									PRINTLN("@@@@@@@@@@@@ eS_TAKE_OUT_SECURITY_OFFICERS @@@@@@@@@@@@@@")
									setOfficersFightingPlayer()
									TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//									IF NOT IS_ENTITY_DEAD(vehOfficerCar)
//										STOP_SYNCHRONIZED_ENTITY_ANIM(vehOfficerCar, 0, TRUE)
//									ENDIF
									IF NOT IS_PED_INJURED(pedVictim)
										PLAY_PAIN(pedVictim, AUD_DAMAGE_REASON_SCREAM_PANIC)
//										PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_SHOCKED_HIGH")
//										IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
											IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
//												IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
//												OR GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) = WAITING_TO_START_TASK
												IF NOT bGirlCowering
													IF IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "idle_girl")
													AND NOT IS_PED_RAGDOLL(pedVictim)
														SET_RAGDOLL_BLOCKING_FLAGS(pedVictim, RBF_PLAYER_IMPACT)
//														CLEAR_PED_TASKS(pedVictim)
														OPEN_SEQUENCE_TASK(Seq)
															TASK_PLAY_ANIM(NULL, "random@homelandsecurity", "idle_to_knees_girl", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
															TASK_PLAY_ANIM(NULL, "random@homelandsecurity", "knees_loop_girl", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_ABORT_ON_PED_MOVEMENT|AF_EXIT_AFTER_INTERRUPTED)
	//														TASK_COWER(NULL, -1)
														CLOSE_SEQUENCE_TASK(Seq)
														TASK_PERFORM_SEQUENCE(pedVictim, Seq)
														CLEAR_SEQUENCE_TASK(Seq)
														bGirlCowering = TRUE
													ELSE
//														FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim, TRUE)
														TASK_COWER(pedVictim)
														bGirlCowering = TRUE
													ENDIF
	//												IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "idle_to_knees_girl", ANIM_SCRIPT)
	//												AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "knees_loop_girl", ANIM_SCRIPT)
	//													CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
	//													OPEN_SEQUENCE_TASK(Seq)
	//														TASK_PLAY_ANIM(NULL, "random@homelandsecurity", "idle_to_knees_girl", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
	//														TASK_PLAY_ANIM(NULL, "random@homelandsecurity", "knees_loop_girl", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
	//													CLOSE_SEQUENCE_TASK(Seq)
	//													TASK_PERFORM_SEQUENCE(pedVictim, Seq)
	//													CLEAR_SEQUENCE_TASK(Seq)
	//												ENDIF
												ELSE
													IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "idle_girl")
													AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "idle_to_knees_girl", ANIM_SCRIPT)
													AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "knees_loop_girl", ANIM_SCRIPT)
													AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) != WAITING_TO_START_TASK
													AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) != PERFORMING_TASK
//														CLEAR_PED_TASKS(pedVictim)
														bGirlCowering = FALSE
													ENDIF
												ENDIF
											ELSE
												IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
														IF DOES_BLIP_EXIST(blipVictim)
															REMOVE_BLIP(blipVictim)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
//										ENDIF
									ELSE
//										IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
//											IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[0], << 150, 150, 150 >>)
												MISSION_FAILED()
//											ENDIF
//										ENDIF
//										IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
//											IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityOfficer[1], << 150, 150, 150 >>)
//												MISSION_FAILED()
//											ENDIF
//										ENDIF
									ENDIF
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF iTimerDialogue < GET_GAME_TIMER()
											IF GET_RANDOM_BOOL()
												IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
													CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_YELL", CONV_PRIORITY_AMBIENT_HIGH)
												ENDIF
											ELSE
												IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
													CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_YELL2", CONV_PRIORITY_AMBIENT_HIGH)
												ENDIF
											ENDIF
											iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 5000)
										ENDIF
									ENDIF
									IF IS_PED_INJURED(pedSecurityOfficer[0])
										IF DOES_BLIP_EXIST(blipOfficers[0])
											REMOVE_BLIP(blipOfficers[0])
	//										IF NOT IS_PED_INJURED(pedVictim)
	//											IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
	//												IF NOT bGirlCowering
	//													CLEAR_PED_TASKS(pedVictim)
	//													TASK_COWER(pedVictim, -1)
	//													bGirlCowering = TRUE
	//												ENDIF
	//											ENDIF
	//										ENDIF
										ENDIF
										REMOVE_PED_FOR_DIALOGUE(HomelandDialogueStruct, 5)
									ELSE
										IF CAN_PED_SEE_HATED_PED(pedSecurityOfficer[0], PLAYER_PED_ID())
										OR IS_ENTITY_AT_ENTITY(pedSecurityOfficer[0], PLAYER_PED_ID(), << 20, 20, 20 >>)
//////////											SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
										ELSE
											IF DOES_BLIP_EXIST(blipOfficers[0])
												REMOVE_BLIP(blipOfficers[0])
											ENDIF
										ENDIF
									ENDIF
									IF IS_PED_INJURED(pedSecurityOfficer[1])
										IF DOES_BLIP_EXIST(blipOfficers[1])
											REMOVE_BLIP(blipOfficers[1])
	//										IF NOT IS_PED_INJURED(pedVictim)
	//											IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
	//												IF NOT bGirlCowering
	//													CLEAR_PED_TASKS(pedVictim)
	//													TASK_COWER(pedVictim, -1)
	//													bGirlCowering = TRUE
	//												ENDIF
	//											ENDIF
	//										ENDIF
										ENDIF
										REMOVE_PED_FOR_DIALOGUE(HomelandDialogueStruct, 4)
									ELSE
										IF CAN_PED_SEE_HATED_PED(pedSecurityOfficer[1], PLAYER_PED_ID())
										OR IS_ENTITY_AT_ENTITY(pedSecurityOfficer[1], PLAYER_PED_ID(), << 20, 20, 20 >>)
//////////											SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
										ELSE
											IF DOES_BLIP_EXIST(blipOfficers[1])
												REMOVE_BLIP(blipOfficers[1])
											ENDIF
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(pedSecurityOfficer[0])
									AND DOES_ENTITY_EXIST(pedSecurityOfficer[1])
										IF (HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedSecurityOfficer[0], WEAPONTYPE_STUNGUN) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedSecurityOfficer[1], WEAPONTYPE_STUNGUN))
										OR (IS_PED_DEAD_OR_DYING(pedSecurityOfficer[0]) AND IS_PED_DEAD_OR_DYING(pedSecurityOfficer[1]))
										OR IS_PLAYER_AND_GIRL_IN_POLICE_VEHICLE()
//////////											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
//////////												SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
//////////												SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//////////												SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
//////////	//											SET_DISPATCH_IDEAL_SPAWN_DISTANCE(80)
//////////											ENDIF
											IF DOES_BLIP_EXIST(blipOfficers[0])
												REMOVE_BLIP(blipOfficers[0])
											ENDIF
											IF DOES_BLIP_EXIST(blipOfficers[1])
												REMOVE_BLIP(blipOfficers[1])
											ENDIF
		//									IF DOES_BLIP_EXIST(blipVictim)
		//										REMOVE_BLIP(blipVictim)
		//									ENDIF
											IF NOT IS_PED_INJURED(pedVictim)
												IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 15, 15, 15 >>)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_THNK", CONV_PRIORITY_AMBIENT_HIGH) //REHOM_LOSE
														bGotUp = FALSE
														bLosingWantedLevel = TRUE
	//													iGetUpTimer = (GET_GAME_TIMER() + 2000)
														CLEAR_RAGDOLL_BLOCKING_FLAGS(pedVictim, RBF_PLAYER_IMPACT)
														CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedVictim)
														thisEventStage = eS_LOSE_COPS
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								BREAK
								
								CASE eS_LOSE_COPS
//									PRINTLN("@@@@@@@@@@@@ eS_LOSE_COPS @@@@@@@@@@@@@@")
//									IF iGetUpTimer < GET_GAME_TIMER()
										IF NOT bGotUp
											IF NOT IS_PED_INJURED(pedVictim)
//												CLEAR_PED_TASKS(pedVictim)
												IF (IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "idle_to_knees_girl", ANIM_SCRIPT) OR IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "knees_loop_girl", ANIM_SCRIPT))
												AND NOT IS_PED_RAGDOLL(pedVictim)
													TASK_PLAY_ANIM(pedVictim, "random@homelandsecurity", "knees_to_stand_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT, 0.3)
												ELSE
													PRINTSTRING("\n@@@@@@@@@@@@@@ CLEAR_PED_TASKS @@@@@@@@@@@@@@\n")
													CLEAR_PED_TASKS(pedVictim)
												ENDIF
												SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
												SET_PED_AS_GROUP_MEMBER(pedVictim, PLAYER_GROUP_ID())
												SET_PED_NEVER_LEAVES_GROUP(pedVictim, TRUE)
												REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
												bGotUp = TRUE
											ENDIF
										ENDIF
//									ENDIF
									IF NOT IS_PED_INJURED(pedVictim)
										IF IS_ENTITY_PLAYING_ANIM(pedVictim, "random@homelandsecurity", "knees_to_stand_girl")
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedVictim, "random@homelandsecurity", "knees_to_stand_girl") > 0.8
												CLEAR_PED_TASKS(pedVictim)
											ENDIF
										ENDIF
									ENDIF
									IF NOT bTrevorsLine
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_ASK", CONV_PRIORITY_AMBIENT_HIGH)
												bTrevorsLine = TRUE
											ENDIF
										ENDIF
									ELSE
//										IF IS_GIRL_IN_PLAYERS_VEHICLE()
											IF NOT bCorruptCopsLine
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
														CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_RES1_M", CONV_PRIORITY_AMBIENT_HIGH)
													ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
														CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_GOGO", CONV_PRIORITY_AMBIENT_HIGH)
													ENDIF
													bCorruptCopsLine = TRUE
												ENDIF
											ENDIF
//										ENDIF
									ENDIF
									IF bGotUp
//										groupSeparationCheck()
										IF NOT IS_PED_INJURED(pedVictim)
											IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 8, 8, 8 >>)
											OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
												IF DOES_BLIP_EXIST(blipVictim)
													REMOVE_BLIP(blipVictim)
												ENDIF
											ELSE
												IF NOT DOES_BLIP_EXIST(blipVictim)
													blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF NOT IS_PED_INJURED(pedSecurityOfficer[0])
										IF CAN_PED_SEE_HATED_PED(pedSecurityOfficer[0], PLAYER_PED_ID())
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
//////////												SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//////////												SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//////////												SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
											ENDIF
										ENDIF
									ENDIF
									IF NOT IS_PED_INJURED(pedSecurityOfficer[1])
										IF CAN_PED_SEE_HATED_PED(pedSecurityOfficer[1], PLAYER_PED_ID())
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
//////////												SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//////////												SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//////////												SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
											ENDIF
										ENDIF
									ENDIF
									
									IF bCorruptCopsLine
									AND IS_GIRL_IN_PLAYERS_VEHICLE()
//////////									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_COME", CONV_PRIORITY_AMBIENT_HIGH)
											SET_PED_AS_NO_LONGER_NEEDED(pedSecurityOfficer[0])
											SET_PED_AS_NO_LONGER_NEEDED(pedSecurityOfficer[1])
											IF NOT IS_PED_INJURED(pedVictim)
												IF IS_PED_IN_GROUP(pedVictim)
//													IF NOT DOES_BLIP_EXIST(blipDropOff)
//														blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPos, TRUE)
//													ENDIF
//													IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//														IF NOT DOES_BLIP_EXIST(blipCult)
//															blipCult = CREATE_BLIP_FOR_COORD(vCult)
//															SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//															PRINT_CULT_HELP()
//														ENDIF
//													ENDIF
//													SET_DISPATCH_IDEAL_SPAWN_DISTANCE(200)
													INT index
													REPEAT COUNT_OF(pedSecurityOfficer) index
														IF DOES_ENTITY_EXIST(pedSecurityOfficer[index])
															SET_PED_AS_NO_LONGER_NEEDED(pedSecurityOfficer[index])
														ENDIF
													ENDREPEAT
													SET_WANTED_LEVEL_MULTIPLIER(1)
													thisEventStage = eS_DRIVE_GIRL_HOME
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								BREAK
								
								CASE eS_DRIVE_GIRL_HOME
//									PRINTLN("@@@@@@@@@@@@@@ eS_DRIVE_GIRL_HOME @@@@@@@@@@@@@@@@")
//									IF IS_THIS_LABEL_PLAYING("REHOM_COME_1")
//									OR bDriveGirlHome
//										bDriveGirlHome = TRUE
										groupSeparationCheck()
										stationaryCheck()
										onFootCheck()
										chatInVehicle()
										chatInterruptions()
//										wantedLevelCheck()
										IF READY_FOR_CULT_DIALOGUE(vDropOffPos)
											MANAGE_CONVERSATION(FALSE)
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_WRONG", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
										IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
											MANAGE_CONVERSATION(FALSE)
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_CULT", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
										IF NOT IS_PED_INJURED(pedVictim)
											IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffPos, g_vOnFootLocate, TRUE)
											AND IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), << 8, 8, 8 >>)
												IF IS_PED_IN_GROUP(pedVictim)
												
													IF DOES_BLIP_EXIST(blipVictim)
														REMOVE_BLIP(blipVictim)
													ENDIF
													IF DOES_BLIP_EXIST(blipDropOff)
														REMOVE_BLIP(blipDropOff)
													ENDIF
													IF DOES_BLIP_EXIST(blipCult)
														REMOVE_BLIP(blipCult)
													ENDIF
													IF IS_PED_IN_GROUP(pedVictim)
														REMOVE_PED_FROM_GROUP(pedVictim)
													ENDIF
													
	//												SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
													
													IF IS_GIRL_IN_PLAYERS_VEHICLE()
	//													iWaitCutTimer = (GET_GAME_TIMER() + DEFAULT_CAR_STOPPING_TO_CUTSCENE)
														IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
															BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), 5, 2)
														ENDIF
														thisEventStage = eS_DROP_OFF_GIRL_IN_VEH
													ELSE
														KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
														WAIT(0)
														CREATE_CONVERSATION(HomelandDialogueStruct, "REHOMAU", "REHOM_HOME", CONV_PRIORITY_AMBIENT_HIGH)
														IF NOT IS_PED_INJURED(pedVictim)
															OPEN_SEQUENCE_TASK(seq)
																TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
																TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPostDropOff, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 38.9844)
																TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
															CLOSE_SEQUENCE_TASK(seq)
															TASK_PERFORM_SEQUENCE(pedVictim, seq)
															CLEAR_SEQUENCE_TASK(seq)
															SET_PED_KEEP_TASK(pedVictim, TRUE)
														ENDIF
														CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
	//													DoThanking.aApproachPed = pedVictim
	//													DoThanking.AnimName = "thanks_male_02"
	//													DoThanking.AnimDict = "amb@thanks"
	//													DoThanking.SpeechBlock = HomelandDialogueStruct
														thisEventStage = eS_DROP_OFF_GIRL_ON_FOOT
													ENDIF
												ENDIF
											ENDIF
										ENDIF
//									ENDIF
								BREAK
								
								CASE eS_DROP_OFF_GIRL_IN_VEH
//									IF iWaitCutTimer < GET_GAME_TIMER()
//									AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
//										bStartScene = TRUE
//									ENDIF
//									IF bStartScene
										endDropOffScene()
//									ENDIF
								BREAK
								
								CASE eS_DROP_OFF_GIRL_ON_FOOT
//									IF CAN_PLAYER_START_CUTSCENE()
//										IF UPDATE_RUN_ONFOOT_THANKYOU(DoThanking)
//											IF DoThanking.bSuccess
												MISSION_PASSED()
//											ENDIF
//										ENDIF
//									ENDIF
								BREAK
								
								CASE eS_CLEAN_UP
									MISSION_FAILED()
								BREAK
								
							ENDSWITCH
							
							// ensure girl is set into vehicle when bundled in to avoid her clipping through vehicle at any point
							IF bBundledIn
							AND NOT bAttachedInVeh
								IF thisEventStage <> eS_DROP_OFF_GIRL_IN_VEH
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.85
											IF IS_VEHICLE_DRIVEABLE(vehOfficerCar)
												IF NOT IS_PED_INJURED(pedVictim)
													IF NOT IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
														IF NOT IS_PED_RAGDOLL(pedVictim)
															CLEAR_PED_TASKS(pedVictim)
															SET_PED_INTO_VEHICLE(pedVictim, vehOfficerCar, VS_BACK_RIGHT)
															SET_PED_KEEP_TASK(pedVictim, TRUE)
															bAttachedInVeh = TRUE
														ELSE
															bAttachedInVeh = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
								IF DOES_ENTITY_EXIST(pedVictim)
									DELETE_PED(pedVictim)
								ENDIF
								MISSION_PASSED()
							ENDIF
							
							IF thisEventStage <> eS_SET_UP_SCENE
							AND thisEventStage <> eS_DROP_OFF_GIRL_IN_VEH
							AND thisEventStage <> eS_DROP_OFF_GIRL_ON_FOOT
								IF IS_EVERYONE_GONE_OR_DEAD()
									thisEventStage = eS_CLEAN_UP
								ENDIF
							ENDIF
							
							IF thisEventStage = eS_LOSE_COPS
							OR thisEventStage = eS_DRIVE_GIRL_HOME
								IF NOT IS_PED_INJURED(pedVictim)
									IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, << 150, 150, 150 >>)
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
										TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										MISSION_FAILED()
									ENDIF
								ENDIF
							ENDIF
							
							IF thisEventStage < eS_DRIVE_GIRL_HOME
								SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
							ENDIF
							
							IF IS_PED_INJURED(pedSecurityOfficer[0])
								IF DOES_BLIP_EXIST(blipOfficers[0])
									REMOVE_BLIP(blipOfficers[0])
								ENDIF
							ENDIF
							IF IS_PED_INJURED(pedSecurityOfficer[1])
								IF DOES_BLIP_EXIST(blipOfficers[1])
									REMOVE_BLIP(blipOfficers[1])
								ENDIF
							ENDIF
							IF IS_PED_INJURED(pedVictim)
								IF DOES_BLIP_EXIST(blipVictim)
									REMOVE_BLIP(blipVictim)
								ENDIF
							ENDIF
							
							// debug warp
							#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
									MISSION_PASSED()
								ENDIF
								IF  IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
									MISSION_FAILED()
								ENDIF
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//									REMOVE_BLIP(blipInitialMarker)
									INT index
									REPEAT COUNT_OF(pedSecurityOfficer) index
										IF NOT IS_PED_INJURED(pedSecurityOfficer[index])
											EXPLODE_PED_HEAD(pedSecurityOfficer[index])
										ENDIF
									ENDREPEAT
									IF NOT IS_ENTITY_DEAD(vehOfficerCar)
									AND NOT IS_PED_INJURED(pedVictim)
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
										AND NOT IS_PED_IN_VEHICLE(pedVictim, vehOfficerCar)
											SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehOfficerCar)
											SET_PED_INTO_VEHICLE(pedVictim, vehOfficerCar, VS_FRONT_RIGHT)
											CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
											bLosingWantedLevel = TRUE
											thisEventStage = eS_LOSE_COPS
										ELSE
											SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vDropOffPos)
											SET_ENTITY_HEADING(PLAYER_PED_ID(), 34.4438)
										ENDIF
									ENDIF
									IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
										CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
									ENDIF
								ENDIF
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
									
								ENDIF
							#ENDIF
							
						ENDIF
					BREAK
				ENDSWITCH
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE
	
ENDSCRIPT
