//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "Ambient_approach.sch"
USING "Ambient_Common.sch"
USING "script_blips.sch"
USING "cutscene_public.sch"
USING "clearmissionarea.sch"
USING "completionpercentage_public.sch"
USING "script_heist.sch"
USING "taxi_functions.sch"
USING "locates_public.sch"
USING "random_events_public.sch"
USING "rc_helper_functions.sch"
USING "rc_area_public.sch"

// Variables ----------------------------------------------//
ENUM main_stage_enum
	CRASH_STAGE,
	RESCUE_STAGE
ENDENUM
main_stage_enum main_stage = CRASH_STAGE

ENUM crash_STAGES
	CSTAGE_CREATE,
	CSTAGE_RUN_CRASHRESCUE,
	CSTAGE_INTRO_CUTSCENE
//	STAGE_CLEAN_UP,
//	STAGE_HELP_PED
ENDENUM
crash_STAGES crashStage = CSTAGE_CREATE

ENUM rescuePed_STAGES
	RSTAGE_DRIVE,
	RSTAGE_OUTRO_CUTSCENE,
	RSTAGE_TALINA_DYING
ENDENUM
rescuePed_STAGES rescueStage = RSTAGE_DRIVE

ENUM end_cut_enum
	stopTheCar,
	ENDCUT_SETUP,
	ENDCUT_CAM1,
	ENDCUT_CAM2,
	ENDCUT_CLEANUP
ENDENUM
end_cut_enum end_cut_stage = stopTheCar
		
ENUM ped_chatter_enum
	PED_CHATTER_INIT,
	PED_CHATTER_DESTINATION,
	PED_CHATTER_CHAT_ALTRUISTS, // Trevor-specific Altruists comment
	PED_CHATTER_CHAT_MEETNGREET, // New conversation introducing Taliana
	PED_CHATTER_CHAT_RANDOM,
	PED_CHATTER_PLAYER_CHAT,
	PED_CHATTER_VICTIM_CHAT,
	PED_CHATTER_VICTIM_CHAT_DONE
ENDENUM
ped_chatter_enum pedChatStage = PED_CHATTER_INIT

#IF IS_DEBUG_BUILD //WIDGETS
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebugRenderBrainRange = FALSE
	BOOL bDebugDontShortenOutro = FALSE
	FLOAT debug_warp_heading
	VECTOR v_debug_warp
#ENDIF

// Constants for outro synched scene truncation - hopefully remove once cameras are reviewed
CONST_FLOAT	OUTRO_START_PHASE	0.0
CONST_FLOAT	OUTRO_END_PHASE		0.975

// How long it is until Talina (crash victim) goes into her dying state (seconds)
CONST_INT TALINA_DEATH_TIME 180

VECTOR vInput, vEndLocation, ped_coord
INT iPedChatterTimer				// General conversation timer
INT iPedChatSS						// Substage counter for complex conversations
INT iHelpOutVictimStage				// Substage progress for victim pleading at start
INT counter_current_time_hazards	// Flashing the hazard lights
INT counter_time_started_hazards	// Flashing the hazard lights
INT iIntroCutsceneStage = 0			// Intro cutscene substage progression
INT random_speech_counter = 0
//BLIP_INDEX blipRandomEvent
BLIP_INDEX blipInjuredPed, blipDoctors
PED_INDEX pedVictim, pedSafehouse, pedDead
BOOL bDeadPedSpawned = FALSE
VEHICLE_INDEX vehCrashed
VECTOR scenePosition, sceneRotation
CAMERA_INDEX pickupCam, camEndCut
structPedsForConversation crashrescueConversation
//SEQUENCE_INDEX seq
SCENARIO_BLOCKING_INDEX blockedArea
BOOL bSkipped, bMoved, bEndSceneCreated
OBJECT_INDEX oiDocsChair

MODEL_NAMES modelGang = A_M_M_EastSA_02
MODEL_NAMES modelGirl = IG_TALINA //U_F_Y_BIKERCHIC
MODEL_NAMES modelCrashed = EMPEROR

STRING animDictOnFloor		= "RANDOM@CRASH_RESCUE@WOUNDED@BASE"		// Pre-cutscene initial setup anims
STRING animDictMoveInjured	= "MOVE_INJURED_GENERIC"					// Injured movement clipset
STRING animDictHelpUpAnims	= "RANDOM@CRASH_RESCUE@HELP_VICTIM_UP"		// Intro cutscene synched scene anims
STRING animDictDeadPed		= "random@crash_rescue@dead_ped"			// For dead ped in intial scene setup

STRING animDictDeathLow		= "RANDOM@CRASH_RESCUE@CAR_DEATH@LOW_CAR"	// Injured seated in vehicle anims
STRING animDictDeathStd		= "RANDOM@CRASH_RESCUE@CAR_DEATH@STD_CAR"	// Injured seated in vehicle anims
STRING animDictDeathVan		= "RANDOM@CRASH_RESCUE@CAR_DEATH@VAN"		// Injured seated in vehicle anims
STRING animDictDeathCurrent // Used to track which seated vehicle anim dictionary is currently in use

STRING animDictEnd = "random@crash_rescue@get_victim_to_friend"			// Synched scene anim set for outro cutscene

INT iSceneID, endSceneId
VECTOR vInjuredPed = << 1933.5060, 6280.5791, 41.70 >>
FLOAT fInjuredPed = 218.0731
BOOL bReverseSetup
BOOL bPlayedWalkDialogue
BOOL bWalkDialogueNeeded = FALSE
PTFX_ID i_ptfx_fire
DECAL_ID decalBlood
FLOAT fOutroStartPhase = OUTRO_START_PHASE
FLOAT fOutroEndPhase = OUTRO_END_PHASE
INT iTalinaDyingStage		// Substage progress control in STAGE_TALINA_DYING
INT iDyingTimer				// General death timer
STRING sDyingLine
BOOL bPlayerRuinedThings = FALSE // Indicates that the ped death is being triggered because the player interfered with something rather than simple timeout
BOOL bToggledTaxisOff
BOOL bFPFlashNeeded = FALSE

// Launch-related functions ----------------------------------------------//

/// PURPOSE:
///    Safe check for inappropriate mission vehicle - cannot launch mission in cop car
/// RETURNS:
///    TRUE if uninjured and in cop car
FUNC BOOL IS_PLAYER_IN_A_POLICE_VEHICLE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines whether the player is on the "wrong" side of the road
/// RETURNS:
///    TRUE if player is in opposite lane to the one the ped is spawned on the edge of
FUNC BOOL IS_PLAYER_IN_NORTHBOUND_LANE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vTmpPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
//		IF vTmpPlayer.x > 2036
//			RETURN TRUE
//		ENDIF
		TEST_POLY tpLaneArea
		OPEN_TEST_POLY(tpLaneArea)
			ADD_TEST_POLY_VERT(tpLaneArea, << 1751, 6363, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 1790, 6518, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 2078, 6449, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 2354, 6301, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 2043, 6112, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 1973, 6217, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 1925, 6312, 0 >>)
			ADD_TEST_POLY_VERT(tpLaneArea, << 1862, 6343, 0 >>)
		CLOSE_TEST_POLY(tpLaneArea)
		IF IS_POINT_IN_POLY_2D(tpLaneArea, vTmpPlayer)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    bTurnOffTaxis - 
PROC TOGGLE_TAXIS_OFF(BOOL bTurnOffTaxis)
	IF bTurnOffTaxis
		DISABLE_TAXI_HAILING()
	ELSE
		DISABLE_TAXI_HAILING(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determine if the vehicle has a low profile so we can use the right animations
/// RETURNS:
///    TRUE if the vehicle has a low profile
FUNC BOOL IS_PLAYERS_VEHICLE_LOW()
	IF IS_PED_IN_ANY_VEHICLE(pedVictim)
		VEHICLE_INDEX viTmp = GET_VEHICLE_PED_IS_IN(pedVictim)
		IF NOT IS_ENTITY_DEAD(viTmp)
			VEHICLE_LAYOUT_TYPE LayoutHash = INT_TO_ENUM(VEHICLE_LAYOUT_TYPE, GET_VEHICLE_LAYOUT_HASH(viTmp))
			IF LayoutHash = LAYOUT_LOW
			OR LayoutHash = LAYOUT_LOW_INFERNUS
			OR LayoutHash = LAYOUT_LOW_DUNE
			OR LayoutHash = LAYOUT_LOW_BFINJECTION
			OR LayoutHash = LAYOUT_LOW_CHEETAH
			OR LayoutHash = LAYOUT_LOW_RESTRICTED
			OR LayoutHash = LAYOUT_LOW_SENTINEL2
			OR LayoutHash = LAYOUT_LOW_BLADE		// DLC vehicle
			OR LayoutHash = LAYOUT_LOW_TURISMOR 	// DLC vehicle
			OR LayoutHash = LAYOUT_LOW_FURORE 		// DLC vehicle
			OR LayoutHash = LAYOUT_LOW_OSIRIS 		// DLC vehicle
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determine if the vehicle is a van so we can use the right animations
/// RETURNS:
///    TRUE if the vehicle has a low profile
FUNC BOOL IS_PLAYERS_VEHICLE_VAN()
	IF IS_PED_IN_ANY_VEHICLE(pedVictim)
		VEHICLE_INDEX viTmp = GET_VEHICLE_PED_IS_IN(pedVictim)
		IF NOT IS_ENTITY_DEAD(viTmp)
			VEHICLE_LAYOUT_TYPE LayoutHash = INT_TO_ENUM(VEHICLE_LAYOUT_TYPE, GET_VEHICLE_LAYOUT_HASH(viTmp))
			IF LayoutHash = LAYOUT_VAN
			OR LayoutHash = LAYOUT_VAN_BODHI
			OR LayoutHash = LAYOUT_VAN_BOXVILLE
			OR LayoutHash = LAYOUT_VAN_JOURNEY
			OR LayoutHash = LAYOUT_VAN_MULE
				RETURN TRUE 
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Return if in a dodgy vehicle we don't otherwise detect. Must have already checked player is in a vehicle
/// RETURNS:
///    TRUE if in unsuitable vehicle
FUNC BOOL USING_OTHER_FORBIDDEN_VEHICLE()
	MODEL_NAMES mTmp
	mTmp = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	IF mTmp = FIRETRUK
	OR mTmp = TRASH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Find out if the player's getting onto a bike
/// RETURNS:
///    TRUE if entering a bike-type vehicle
FUNC BOOL ENTERING_A_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether the player is set to give Taliana a lift
/// RETURNS:
///    TRUE if the player is in a suitable vehicle with a free seat
FUNC BOOL IS_PLAYER_IN_VEHICLE_WITH_FREE_SEAT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
			IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
			AND NOT USING_OTHER_FORBIDDEN_VEHICLE()
				IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)	
				OR IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_DRIVER)	
				OR IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)		
				OR IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)	
					bWalkDialogueNeeded = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_PED_IN_VEHICLE(pedVictim, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					bWalkDialogueNeeded = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		// Extra bike check
		ELIF ENTERING_A_BIKE()
			CPRINTLN(DEBUG_MISSION, "Player is entering a bike - drop through for false return and possible dialogue trigger")
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT bPlayedWalkDialogue
		bWalkDialogueNeeded = TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_GROUPED()
	SET_PED_MAX_MOVE_BLEND_RATIO(pedVictim, PEDMOVEBLENDRATIO_WALK)
	IF IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
		IF DOES_BLIP_EXIST(blipInjuredPed)
			// If not in vehicle prompt to get one with dialogue
			REMOVE_BLIP(blipInjuredPed)
		ELSE
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20,20,20>>)
			OR NOT IS_PLAYER_IN_VEHICLE_WITH_FREE_SEAT()
				REMOVE_PED_FROM_GROUP(pedVictim)
			ELSE
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF NOT IS_PED_ON_FOOT(pedVictim)
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = FINISHED_TASK
							CLEAR_PED_TASKS(pedVictim)
							TASK_LEAVE_ANY_VEHICLE(pedVictim)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		IF DOES_BLIP_EXIST(blipInjuredPed)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, g_vAnyMeansLocate)
			AND IS_PLAYER_IN_VEHICLE_WITH_FREE_SEAT()
				SET_PED_AS_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
			ENDIF
		ELSE
			blipInjuredPed = CREATE_AMBIENT_BLIP_FOR_ENTITY(pedVictim)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// Clean Up
PROC missionCleanup()

	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	#ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(blockedArea)
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		SET_WANTED_LEVEL_MULTIPLIER(1)
	ENDIF
	
	IF IS_ENTITY_ALIVE(vehCrashed)
		SET_ENTITY_PROOFS(vehCrashed, FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_VEHICLE_ENGINE_HEALTH(vehCrashed, -400)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehCrashed, -100)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(i_ptfx_fire)
		STOP_PARTICLE_FX_LOOPED(i_ptfx_fire)
	ENDIF
	IF DOES_ENTITY_EXIST(oiDocsChair)
		FREEZE_ENTITY_POSITION(oiDocsChair, FALSE)
		SAFE_RELEASE_OBJECT(oiDocsChair)
	ENDIF
	IF bToggledTaxisOff
		TOGGLE_TAXIS_OFF(FALSE)
	ENDIF

	SET_ROADS_BACK_TO_ORIGINAL(vInput-<<3,3,3>>, vInput+<<3,3,3>>)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC missionPassed()
	SET_HEIST_CREW_MEMBER_UNLOCKED(CM_DRIVER_G_TALINA_UNLOCK)
	
	IF GET_MISSION_COMPLETE_STATE( SP_HEIST_FINALE_2_INTRO )
		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_TALINA, FALSE )
	ENDIF
	
	LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

/// PURPOSE:
///    Progress to next step. Set timer for a 'natural' delay for B*1574779
PROC WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER(INT iTimerBump = 100, BOOL bUpStageCount = TRUE)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF bWalkDialogueNeeded
			IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_WALK", CONV_PRIORITY_AMBIENT_MEDIUM)
				bPlayedWalkDialogue = TRUE
				bWalkDialogueNeeded = FALSE
			ENDIF
		ELSE
			IF bUpStageCount
				iPedChatSS++
			ENDIF
			iPedChatterTimer = GET_GAME_TIMER()+iTimerBump
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check for timer expiry
/// RETURNS:
///    TRUE if game timer exceeds the time we set
FUNC BOOL CHECK_DIALOGUE_TIMER()
	IF GET_GAME_TIMER() >= iPedChatterTimer
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles en-route conversations. No longer controls mission's overall timing.
PROC PED_ROUTE_CHAT()
//	INT timeTilDeath = 35000
//
//	IF NOT IS_PED_INJURED(pedVictim)
//		IF IS_PED_ON_FOOT(pedVictim)
//			timeTilDeath = 15000
//		ELSE
//			IF IS_PLAYER_A_TAXI_PASSENGER()
//				timeTilDeath = 45000
//			ELSE
//				timeTilDeath = 25000
//			ENDIF
//		ENDIF
//	ENDIF
	
	SWITCH pedChatStage

		CASE PED_CHATTER_INIT
			
			iPedChatSS = 0 // Init the substage count
			pedChatStage = PED_CHATTER_DESTINATION
			iPedChatterTimer = GET_GAME_TIMER() + 1000

		BREAK
		
		CASE PED_CHATTER_DESTINATION
			
			// Initial "Taking you to hospital" from player ped
			IF iPedChatSS = 0
				
				IF CHECK_DIALOGUE_TIMER()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_WHEREM", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_WHEREF", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_WHERET", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait for time to play next line
			ELIF iPedChatSS = 1
				
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Taliana "No hospital" line
			ELIF iPedChatSS = 2
			
				IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_APPR", CONV_PRIORITY_AMBIENT_MEDIUM)
					iPedChatSS++
					IF NOT DOES_BLIP_EXIST(blipDoctors)
						blipDoctors = CREATE_AMBIENT_BLIP_FOR_COORD(VEndLocation, TRUE)
					ENDIF
				ENDIF
			
			// Wait for time to play next line
			ELIF iPedChatSS = 3

				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER(0)
				
			ELIF iPedChatSS = 4
				
				// ARE WE TREVOR? He has more to say...
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_DESTINATION: Player is Trevor, go to play extras")
					iPedChatSS++
				ELSE
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_DESTINATION: Player is not Trevor, go straight to delay before meet-n-greet lines")
					iPedChatSS = 6
				ENDIF
			
			// Trevor only "practically neighbours"
			ELIF iPedChatSS = 5
				IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_TSS", CONV_PRIORITY_AMBIENT_MEDIUM) // I assume you've heard of TPE
					iPedChatSS++
				ENDIF
			
			// Wait for conversation to finish
			ELIF iPedChatSS = 6
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER(1500)

			// Altruists or straight to Meet n Greet?
			ELIF iPedChatSS = 7
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
						CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_DESTINATION: Player is Trevor, go to play Altruist comment")
						iPedChatterTimer = GET_GAME_TIMER()
						iPedChatSS = 0
						pedChatStage = PED_CHATTER_CHAT_ALTRUISTS
					ELSE
						CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_DESTINATION: Player is not Trevor, go straight to meet n greet")
						iPedChatterTimer = GET_GAME_TIMER()
						iPedChatSS = 0
						pedChatStage = PED_CHATTER_CHAT_MEETNGREET
					ENDIF
				ENDIF				
			ENDIF
		
		BREAK
		
		CASE PED_CHATTER_CHAT_ALTRUISTS
		
			// Trigger Trevor's Altruist comment
			IF iPedChatSS = 0
				IF CHECK_DIALOGUE_TIMER()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_GROUPED()
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_ALTRU", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS ++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait for conversation to finish
			ELIF iPedChatSS = 1
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER(0)

			// Go on to next bit
			ELIF iPedChatSS = 2
				CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_ALTRUISTS: Player is Trevor, go to play meet-n-greet lines")
				iPedChatterTimer = GET_GAME_TIMER() + 1500
				iPedChatSS = 0
				pedChatStage = PED_CHATTER_CHAT_MEETNGREET
				
			ENDIF
		
		BREAK
		
		CASE PED_CHATTER_CHAT_MEETNGREET
			
			// Taliana thanks line
			IF iPedChatSS = 0
				
				IF CHECK_DIALOGUE_TIMER()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_GROUPED()
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT1", CONV_PRIORITY_AMBIENT_MEDIUM)
							CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 1")
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 1
				
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Player chatter line 1
			ELIF iPedChatSS = 2
				
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT1M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT1F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT1T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 3
				
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
				
			// Taliana line 2
			ELIF iPedChatSS = 4
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT2", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 2")
					iPedChatSS++
				ENDIF
			
			// Wait to trigger next line
			ELIF iPedChatSS = 5
				
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Player line 2
			ELIF iPedChatSS = 6
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT2M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT2F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT2T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 7
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Taliana line 3
			ELIF iPedChatSS = 8
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT3", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 3")
					iPedChatSS++
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 9
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Player line 3
			ELIF iPedChatSS = 10
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT3M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT3F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT3T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 11
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Taliana line 4
			ELIF iPedChatSS = 12
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT4", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 4")
					iPedChatSS++
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 13
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Player line 4
			ELIF iPedChatSS = 14
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT4M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT4F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT4T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 15
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Taliana line 5
			ELIF iPedChatSS = 16
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT5", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 5")
					iPedChatSS++
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 17
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
			
			// Player line 5
			ELIF iPedChatSS = 18
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT5M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT5F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT5T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 19
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Taliana line 6
			ELIF iPedChatSS = 20
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT6", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 6")
					iPedChatSS++
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 21
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Player line 6
			ELIF iPedChatSS = 22
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT6M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT6F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT6T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 23
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Taliana line 7
			ELIF iPedChatSS = 24
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT7", CONV_PRIORITY_AMBIENT_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - ROUND 7")
					iPedChatSS++
				ENDIF
				
			// Wait to trigger next line
			ELIF iPedChatSS = 25
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Player line 7
			ELIF iPedChatSS = 26
			
				IF CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT7M", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT7F", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT7T", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF

			// Wait to trigger next line
			ELIF iPedChatSS = 27
			
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()

			// Taliana line 8 - FINAL ROUND!
			ELIF iPedChatSS = 28
			
				IF CHECK_DIALOGUE_TIMER()
				AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_CHAT8", CONV_PRIORITY_AMBIENT_MEDIUM)
					iPedChatSS++
				ENDIF
				
			// Wait for conversation to finish
			ELIF iPedChatSS = 29
				CPRINTLN(DEBUG_MISSION, "PED_ROUTE_CHAT: PED_CHATTER_CHAT_MEETNGREET - done")
				iPedChatterTimer = GET_GAME_TIMER() + 6000
				iPedChatSS = 0
				pedChatStage = PED_CHATTER_CHAT_RANDOM
			ENDIF
		
		BREAK

		CASE PED_CHATTER_CHAT_RANDOM
			
			IF iPedChatSS = 0
				IF GET_GAME_TIMER() > iPedChatterTimer
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					IF IS_PED_GROUPED()
						IF IS_PLAYER_IN_VEHICLE_WITH_FREE_SEAT()
						AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_PAIN", CONV_PRIORITY_AMBIENT_MEDIUM) //Shit! I'm gonna die here! Hurry the fuck up man!
								iPedChatSS++
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_GETOUT", CONV_PRIORITY_AMBIENT_MEDIUM)
								iPedChatSS++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELIF iPedChatSS = 1
				
				WAIT_TO_FINISH_AND_SET_DIALOGUE_TIMER()
				
			ELIF iPedChatSS = 2
				
				IF IS_PED_GROUPED()
				AND CHECK_DIALOGUE_TIMER()
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_MC1", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_FC1", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_TC1", CONV_PRIORITY_AMBIENT_MEDIUM)
							iPedChatSS++
						ENDIF
					ENDIF
				ENDIF

			ELIF iPedChatSS = 3

				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					random_speech_counter++
					IF random_speech_counter < 3
						iPedChatterTimer = GET_GAME_TIMER() + 20000
						iPedChatSS = 0
					ELSE
						pedChatStage = PED_CHATTER_VICTIM_CHAT_DONE
					ENDIF
				ENDIF
				
			ENDIF
		
		BREAK

		CASE PED_CHATTER_VICTIM_CHAT_DONE
			// Done
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Check for player attempting early breakout from intro synched scene
/// RETURNS:
///    TRUE if player wants out
FUNC BOOL CHECK_FOR_EARLY_BREAKOUT(INT iSynchSceneID, FLOAT fInterruptPhase)
	
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
	IF iLeftX < -64 OR iLeftX > 64 // Left/Right
	OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
		
		FLOAT fScenePhase
		fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iSynchSceneID)
		
		IF fScenePhase >= fInterruptPhase
			CPRINTLN(DEBUG_MISSION, "Allowed interruption at phase ", GET_SYNCHRONIZED_SCENE_PHASE(iSynchSceneID))
			RETURN TRUE
		ELSE
			CPRINTLN(DEBUG_MISSION, "Denied interruption at phase ", GET_SYNCHRONIZED_SCENE_PHASE(iSynchSceneID))
			RETURN FALSE
		ENDIF

	ENDIF
	RETURN FALSE
ENDFUNC

PROC flash_hazards()

	IF DOES_ENTITY_EXIST(vehCrashed)
		IF IS_VEHICLE_DRIVEABLE(vehCrashed)

			counter_current_time_hazards = GET_GAME_TIMER()

			IF (counter_current_time_hazards - counter_time_started_hazards) < 1000
				SET_VEHICLE_BRAKE_LIGHTS(vehCrashed, TRUE)
			ELIF (counter_current_time_hazards - counter_time_started_hazards) > 2000
				SET_VEHICLE_BRAKE_LIGHTS(vehCrashed, FALSE)
				counter_time_started_hazards = GET_GAME_TIMER()
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Do we need to resolve the mission start vehicle to a location that is further away?
/// RETURNS:
///    TRUE if the vehicle is not a concern (small enough or doesn't exist)
FUNC BOOL IS_RE_PLAYER_VEHICLE_UNDER_SIZE_LIMIT()
	VECTOR vecVehicleSizes[2], vecMaxVehicleSize
	VEHICLE_INDEX vehTmp = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehTmp)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTmp), vecVehicleSizes[0], vecVehicleSizes[1])
		vecMaxVehicleSize = GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
		IF vecVehicleSizes[1].x - vecVehicleSizes[0].x > vecMaxVehicleSize.x
			RETURN FALSE
		ENDIF
		IF vecVehicleSizes[1].y - vecVehicleSizes[0].y > vecMaxVehicleSize.y
			RETURN FALSE
		ENDIF
		IF vecVehicleSizes[1].z - vecVehicleSizes[0].z > vecMaxVehicleSize.z
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Do we need to resolve the cutscene trigger vehicle to a location that is further away?
/// RETURNS:
///    TRUE if the vehicle is too big
FUNC BOOL IS_END_SCENE_VEHICLE_OVER_SIZE_LIMIT(VEHICLE_INDEX vehTmp)
	VECTOR vecVehicleSizes[2], vecMaxVehicleSize
	IF DOES_ENTITY_EXIST(vehTmp)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTmp), vecVehicleSizes[0], vecVehicleSizes[1])
		vecMaxVehicleSize = GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
		IF vecVehicleSizes[1].x - vecVehicleSizes[0].x > vecMaxVehicleSize.x
			RETURN TRUE
		ENDIF
		IF vecVehicleSizes[1].y - vecVehicleSizes[0].y > vecMaxVehicleSize.y
			RETURN TRUE
		ENDIF
		IF vecVehicleSizes[1].z - vecVehicleSizes[0].z > vecMaxVehicleSize.z
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles cutscene skip detection and fade
/// RETURNS:
///    TRUE once faded out and ready to skip scene
FUNC BOOL DO_FADES_IF_CUT_IS_SKIPPED()
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		DO_SCREEN_FADE_OUT(500)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
		// Updated to set synch scene to end phase so everything is done when we fade back in
		IF GET_SYNCHRONIZED_SCENE_PHASE(endSceneId) < 1
			SET_SYNCHRONIZED_SCENE_PHASE(endSceneId, 1)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Spawn end scene once in range
PROC CREATE_END_SCENE()
	IF NOT bEndSceneCreated
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), VEndLocation, 150)
		REQUEST_MODEL(modelGang)
		REQUEST_ANIM_DICT(animDictEnd)
		IF HAS_MODEL_LOADED(modelGang)
		AND HAS_ANIM_DICT_LOADED(animDictEnd)
		AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 1728.7781, 3851.2869, 33.7826 >>, 5.0, PROP_SKID_CHAIR_01)
			oiDocsChair = GET_CLOSEST_OBJECT_OF_TYPE(<< 1728.7781, 3851.2869, 33.7826 >>, 5.0, PROP_SKID_CHAIR_01)
			FREEZE_ENTITY_POSITION(oiDocsChair, TRUE)
			scenePosition = << 1730.038, 3853.562, 34.661 >>
			sceneRotation = << 0.000, 0.000, 36.000 >>
			pedSafehouse = CREATE_PED(PEDTYPE_MISSION, modelGang, << 1728.7781, 3851.2869, 33.7826 >>, 211.7722)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSafehouse, TRUE)
			ADD_PED_FOR_DIALOGUE(crashrescueConversation, 5, pedSafehouse, "CrashSafehouse")
			endSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (pedSafehouse, endSceneId, animDictEnd, "helping_friend_idle_friend", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			bEndSceneCreated = TRUE
		ENDIF
	ENDIF
ENDPROC

//*************************************************************************************************************************************************
//
//													:STAGE PROCS:
//
//*************************************************************************************************************************************************

/// PURPOSE:
///    MISSION STAGE PROC
///    Requests initial asset set and creates scene
PROC STAGE_LOAD_ASSETS()
	VECTOR vMin, vMax
	REQUEST_MODEL(modelGang)
	REQUEST_MODEL(modelCrashed)
	REQUEST_MODEL(modelGirl)
	REQUEST_ANIM_DICT(animDictOnFloor)
	REQUEST_CLIP_SET(animDictMoveInjured)
	REQUEST_ANIM_DICT(animDictDeathLow)
	REQUEST_ANIM_DICT(animDictDeathStd)
	REQUEST_ANIM_DICT(animDictDeathVan)
	REQUEST_ANIM_DICT(animDictDeadPed)
	REQUEST_PTFX_ASSET()
	
	IF HAS_MODEL_LOADED(modelGang)
	AND HAS_MODEL_LOADED(modelCrashed)
	AND HAS_MODEL_LOADED(modelGirl)
	AND HAS_ANIM_DICT_LOADED(animDictOnFloor)
	AND HAS_CLIP_SET_LOADED(animDictMoveInjured)
	AND HAS_ANIM_DICT_LOADED(animDictDeathLow)
	AND HAS_ANIM_DICT_LOADED(animDictDeathStd)
	AND HAS_ANIM_DICT_LOADED(animDictDeathVan)
	AND HAS_ANIM_DICT_LOADED(animDictDeadPed)
	AND HAS_PTFX_ASSET_LOADED()
	
		bReverseSetup = FALSE //IS_PLAYER_IN_NORTHBOUND_LANE()
		bToggledTaxisOff = FALSE

		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)

		// Create the crashed car
		vehCrashed = CREATE_VEHICLE(modelCrashed, << 1932.98, 6291.91, 41.08 >>, 350.85) //<< 1932.1906, 6291.6475, 40.4098 >>, 247.8583) //Upside down car
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCrashed)
		SET_VEHICLE_ENGINE_ON(vehCrashed, FALSE, FALSE)
		SET_VEHICLE_LIGHTS(vehCrashed, FORCE_VEHICLE_LIGHTS_OFF)
		SET_VEHICLE_TYRE_BURST(vehCrashed, SC_WHEEL_CAR_FRONT_LEFT)
		GET_MODEL_DIMENSIONS(modelCrashed, vMin, vMax)
		SMASH_VEHICLE_WINDOW(vehCrashed, SC_WINDOW_FRONT_LEFT)
		SMASH_VEHICLE_WINDOW(vehCrashed, SC_WINDOW_REAR_LEFT)
		SMASH_VEHICLE_WINDOW(vehCrashed, SC_WINDOW_REAR_RIGHT)
		POP_OUT_VEHICLE_WINDSCREEN(vehCrashed)
		SET_VEHICLE_DAMAGE(vehCrashed, <<0, vMax.y, 0>>, 500.0, 10000.0, TRUE)
		SET_VEHICLE_DAMAGE(vehCrashed, <<vMax.x, vMax.y*0.75, 0>>, 500.0, 10000.0, TRUE)
		SET_VEHICLE_DAMAGE(vehCrashed, <<vMax.x, 0, 0>>, 500.0, 10000.0, TRUE)
		SET_VEHICLE_DAMAGE(vehCrashed, <<0, 0, vMax.z>>, 500.0, 10000.0, TRUE)
		
		SET_VEHICLE_DOOR_OPEN(vehCrashed, SC_DOOR_BOOT, TRUE, TRUE)
		SET_VEHICLE_DIRT_LEVEL(vehCrashed, 12.0)
		
		START_VEHICLE_ALARM(vehCrashed)
		SET_ENTITY_PROOFS(vehCrashed, FALSE, TRUE, FALSE, FALSE, FALSE)
		SET_ENTITY_ROTATION(vehCrashed, <<0, 180, 0>>)
		SET_VEHICLE_CAN_LEAK_PETROL(vehCrashed, TRUE)
		SET_VEHICLE_UNDRIVEABLE(vehCrashed, TRUE)
		i_ptfx_fire = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_recrash_rescue_fire", vehCrashed, <<-0.45, -1.25, -0.5>>, <<0.0, 0.0, 0.0>>, 1.0, FALSE, FALSE, TRUE)
		
		// Create the crash victim ped
		// Spawn ped with facing based on player's approach direction
		IF bReverseSetup
			pedVictim = CREATE_PED(PEDTYPE_MISSION, modelGirl, vInjuredPed, fInjuredPed-180) //Crash survivor
		ELSE
			pedVictim = CREATE_PED(PEDTYPE_MISSION, modelGirl, vInjuredPed, fInjuredPed) //Crash survivor
		ENDIF
		
		APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedVictim, PDZ_TORSO, 0.7, 0.5, BDT_SHOTGUN_SMALL)
		ped_coord = GET_ENTITY_COORDS(pedVictim)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<ped_coord.x, ped_coord.y+0.1, ped_coord.z+0.1>>, <<ped_coord.x, ped_coord.y-0.1, ped_coord.z-0.1>>, 1, TRUE)
		TASK_PLAY_ANIM(pedVictim, animDictOnFloor, "BASE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		IF IS_ENTITY_ALIVE(pedVictim)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, FALSE)
		ENDIF		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
		SET_ENTITY_HEALTH(pedVictim, 115)
		SET_PED_DIES_WHEN_INJURED(pedVictim, TRUE)
		SET_PED_KEEP_TASK(pedVictim, TRUE)
		SET_PED_DIES_IN_VEHICLE(pedVictim, TRUE)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(pedVictim, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_AGGRESSIVE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, TRUE)
		SET_PED_MOVEMENT_CLIPSET(pedVictim, animDictMoveInjured)
		SET_PED_CONFIG_FLAG(pedVictim, PCF_DisablePanicInVehicle, TRUE)
		ADD_PED_FOR_DIALOGUE(crashrescueConversation, 3, pedVictim, "TALINA")
		APPLY_PED_BLOOD(pedVictim, ENUM_TO_INT(PED_COMP_TORSO), <<0,0,0>>, "wound_sheet")
		SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
		
		// Create the dead ped
//		pedDead = CREATE_PED(PEDTYPE_MISSION, modelGang, <<1936.2728, 6291.9287, 39.9876>>, 90)
		pedDead = CREATE_PED(PEDTYPE_MISSION, modelGang, << 1935.530, 6291.888, 41.144 >>, -28.440)
		bDeadPedSpawned = TRUE
		ped_coord = GET_ENTITY_COORDS(pedDead)
		APPLY_PED_BLOOD(pedDead, ENUM_TO_INT(PED_COMP_TORSO), <<0,0,0>>, "wound_sheet")
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<ped_coord.x, ped_coord.y+0.1, ped_coord.z+0.1>>, <<ped_coord.x, ped_coord.y-0.1, ped_coord.z-0.1>>, 1, TRUE)
		SET_PED_CONFIG_FLAG(pedDead, PCF_ForceRagdollUponDeath, TRUE)
		TASK_PLAY_ANIM_ADVANCED(pedDead, animDictDeadPed, "dead_ped", << 1935.530, 6291.888, 41.144 >>, << -0.000, 0.000, -28.440 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		SET_ENTITY_HEALTH(pedDead, 0)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelGang)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
			ADD_PED_FOR_DIALOGUE(crashrescueConversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(crashrescueConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(crashrescueConversation, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF

//		IF NOT DOES_BLIP_EXIST(blipRandomEvent)
//			blipRandomEvent = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(pedVictim)
//		ENDIF
		
		blockedArea = ADD_SCENARIO_BLOCKING_AREA(<<1729.3109, 3850.7671, 33.7286>>-<<25,25,3>>, <<1729.3109, 3850.7671, 33.7286>>+<<25,25,3>>)
		
		CPRINTLN(DEBUG_MISSION, "RE_CrashRescue: Stage Load Assets > Stage Run CrashRescue")
		crashStage = CSTAGE_RUN_CRASHRESCUE

	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks for player interference with initial setup before triggering intro cutscene
/// RETURNS:
///    TRUE if player is messing about with scene
FUNC BOOL IS_PLAYER_INTERFERING_WITH_SCENE()
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 1)
			CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Player shooting at ped")
			RETURN TRUE
		ENDIF

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedVictim)
					CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Player griefing ped with vehicle")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 8)
			CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Explosion near rescue ped")
			RETURN TRUE
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID(), TRUE)
			CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Rescue ped damaged by player")
			RETURN TRUE
		ENDIF
		
		IF IS_PED_RAGDOLL(pedVictim)
			CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Rescue ped in ragdoll, probably player dickery that has not tripped other checks")
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Rescue ped is injured")
		RETURN TRUE
	ENDIF
	IF NOT IS_ENTITY_ALIVE(pedVictim)
		CPRINTLN(DEBUG_MISSION, "Crash Rescue: IS_PLAYER_INTERFERING_WITH_SCENE - Rescue ped is dead")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    MISSION STAGE PROC
///    Play ambient stuff around the mission start until player triggers cutscene
PROC STAGE_FIND_INJURED_PED()
	
	IF NOT IS_PED_INJURED(pedVictim)
	AND NOT IS_PLAYER_INTERFERING_WITH_SCENE()
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<80,80,15>>)
		
			REQUEST_ANIM_DICT(animDictHelpUpAnims)
			IF NOT bToggledTaxisOff
				TOGGLE_TAXIS_OFF(TRUE)
				bToggledTaxisOff = TRUE
			ENDIF

			// Has player triggered cutscene?
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<2.5, 2.5, 2.5>>, FALSE, TRUE, TM_ON_FOOT)
			AND CAN_PLAYER_START_CUTSCENE()
				//CLEAR_PED_TASKS(pedVictim)
				SET_PED_AS_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
				SET_PED_CONFIG_FLAG(pedVictim, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
				crashStage = CSTAGE_INTRO_CUTSCENE
			ELSE
			
				// Check whether to play dialogue
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedVictim, 15.0)
				AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 4.0
				
					// Stop talking if pulled up - B*1426652
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
					
					// Need to check for blipping here as well in case new stopping check prevents its normal creation
					IF NOT DOES_BLIP_EXIST(blipInjuredPed)
						//blipInjuredPed = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
						blipInjuredPed = CREATE_PED_BLIP(pedVictim, TRUE, TRUE)
					ENDIF

				ELSE
				
					// Not in range yet, play the chatter
					SWITCH iHelpOutVictimStage
						
						CASE 0
						
							iPedChatterTimer = GET_GAME_TIMER()
							IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
								SET_RANDOM_EVENT_ACTIVE()
							ENDIF
							iHelpOutVictimStage = 1
							
						BREAK

						CASE 1
							
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
	//							IF DOES_BLIP_EXIST(blipRandomEvent)
	//								REMOVE_BLIP(blipRandomEvent)
	//							ENDIF
								
								IF NOT DOES_BLIP_EXIST(blipInjuredPed)
									//blipInjuredPed = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
									blipInjuredPed = CREATE_PED_BLIP(pedVictim, TRUE, TRUE)
									SHOW_HEIGHT_ON_BLIP(blipInjuredPed, FALSE)
								ENDIF
									
								CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_PLEAD", CONV_PRIORITY_AMBIENT_MEDIUM) //Hey, friend! I need your help. Can you get me out of here?
								
								TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
								iPedChatterTimer = GET_GAME_TIMER() + 18000
								iHelpOutVictimStage = 2
							ENDIF
							
						BREAK
						
						CASE 2
						
							IF GET_GAME_TIMER() > iPedChatterTimer
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_PLEAD2", CONV_PRIORITY_AMBIENT_MEDIUM) //I can't wait for them. I'll give you money! Please!
								iPedChatterTimer = GET_GAME_TIMER() + 12000
								iHelpOutVictimStage = 3
							ENDIF
							
						BREAK
						
						CASE 3
						
							IF GET_GAME_TIMER() > iPedChatterTimer
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_PLEAD3", CONV_PRIORITY_AMBIENT_MEDIUM) //Help me up!
								iPedChatterTimer = GET_GAME_TIMER() + 9500
							ENDIF
							
						BREAK
						
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "Crash Rescue - STAGE_FIND_INJURED_PED - Killing off rescue ped")
		iTalinaDyingStage = 5
		main_stage = RESCUE_STAGE
		rescueStage = RSTAGE_TALINA_DYING
	ENDIF

	flash_hazards()
	
ENDPROC

/// PURPOSE:
///    MISSION STAGE PROC
///    Play the intro help-ped-up cutscene
PROC STAGE_INTRO_CUTSCENE()
	
	VECTOR vTemp
	
	SWITCH iIntroCutsceneStage
	
		CASE 0
			REQUEST_ANIM_DICT(animDictHelpUpAnims)
			IF HAS_ANIM_DICT_LOADED(animDictHelpUpAnims)
				iIntroCutsceneStage++
			ENDIF
		BREAK
	
		CASE 1
		
			IF NOT IS_PED_INJURED(pedVictim)
				RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
				
				PRINTLN("<re_crashrescue> PLAY_HELP_VICTIM_UP_CUTSCENE() - CAN_PLAYER_START_CUTSCENE() ")
				scenePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0,0,5>>)
				GET_GROUND_Z_FOR_3D_COORD(scenePosition, scenePosition.z)
				
				sceneRotation = MAKE_VECTOR_ZERO()
				sceneRotation.z = WRAP(fInjuredPed+180, 0, 360)
				
				iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				
				pickupCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(pickupCam, iSceneID, "HELPING_VICTIM_TO_FEET_CAM", animDictHelpUpAnims)
				SET_CAM_ACTIVE(pickupCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				// Replaced old vehicle grab/respot with resolve area - B*1196000 - Revised area and warp coord B*1315903
				// B*  - Added alt to cope with helicopters, had to write new funcs locally as random events don't use replay system
				IF IS_RE_PLAYER_VEHICLE_UNDER_SIZE_LIMIT()
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<< 1935.59, 6277.31, 40.87 >>, << 1930.11, 6285.06, 44.40 >>, 7.0, << 1936.63, 6275.50, 42.59 >>, -146.8)
				ELSE
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<< 1935.59, 6277.31, 40.87 >>, << 1930.11, 6285.06, 44.40 >>, 7.0, <<1933.3604, 6261.6812, 42.2652>>, 218.0)
				ENDIF
				
				CLEAR_AREA(GET_ENTITY_COORDS(pedVictim), 5, TRUE)
				CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(pedVictim), 20)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(pedVictim), 7)
				
				// Create blood splat now that we've cleared area
				decalBlood = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vInjuredPed, <<0.0, 0.0, -1.0>>, normalise_vector(<<0.0, 1.0, 0.0>>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
				
				// Clear ped states on player
				IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) = TRUE
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				ENDIF
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID()) // B*1965657
				
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(),iSceneID, animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_PLAYER" , INSTANT_BLEND_IN, SLOW_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(pedVictim, iSceneID, animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_VICTIM" , INSTANT_BLEND_IN, SLOW_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID, FALSE)
				SET_SYNCHRONIZED_SCENE_PHASE(iSceneID, 0.25)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)	// In case player has triggered scene in ragdoll - B*1471338
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)		// In case triggered somehow when ped is in an odd state we don't otherwise handle - B*1489536
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_MHELP", CONV_PRIORITY_AMBIENT_MEDIUM) //Ok, I'll help you			
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN 
					CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_FHELP", CONV_PRIORITY_AMBIENT_MEDIUM) //Ok, I'll help you		
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_THELP", CONV_PRIORITY_AMBIENT_MEDIUM) //Ok, I'll help you		
				ENDIF
				
				iIntroCutsceneStage++
		
			ENDIF
		BREAK
		
		CASE 2
			IF bSkipped
				IF IS_SCREEN_FADED_OUT()
					iIntroCutsceneStage = 4
				ENDIF
			ELSE
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
					bSkipped = TRUE
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.50	
						iIntroCutsceneStage++
					ENDIF
				ENDIF
			ENDIF

		BREAK
	
		CASE 3
			IF bSkipped
				IF IS_SCREEN_FADED_OUT()
					iIntroCutsceneStage = 4
				ENDIF
			ELSE
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
					bSkipped = TRUE
				ELIF CHECK_FOR_EARLY_BREAKOUT(iSceneID, 0.9)
					STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(), SLOW_BLEND_OUT, TRUE)
					iIntroCutsceneStage = 4
					bMoved = TRUE
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.99
						iIntroCutsceneStage++				
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
			IF bSkipped
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			IF bMoved
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				IF IS_ENTITY_ALIVE(pedVictim)
					CLEAR_PED_TASKS(pedVictim)
				ENDIF
			ENDIF
			
			REMOVE_ANIM_DICT(animDictHelpUpAnims)
			REMOVE_ANIM_DICT(animDictOnFloor)
			IF IS_ENTITY_ALIVE(pedVictim)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
				SET_PED_CONFIG_FLAG(pedVictim, PCF_CanDiveAwayFromApproachingVehicles, FALSE)
			ENDIF
			REMOVE_DECAL(decalBlood)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			IF DOES_CAM_EXIST(pickupCam)
				DESTROY_CAM(pickupCam)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, RELGROUPHASH_PLAYER)
			
			IF bSkipped
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
				FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_IDLE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
				vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_PLAYER",  scenePosition, sceneRotation, 1)
				GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
	            SET_ENTITY_COORDS(PLAYER_PED_ID(), vTemp)
				vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_PLAYER", scenePosition, sceneRotation, 1)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), vTemp.z)
				
				vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_VICTIM",  scenePosition, sceneRotation, 1)
				GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
	            SET_ENTITY_COORDS(pedVictim, vTemp)
				vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(animDictHelpUpAnims, "HELPING_VICTIM_TO_FEET_VICTIM", scenePosition, sceneRotation, 1)
				SET_ENTITY_HEADING(pedVictim, vTemp.z)
			ELSE
				IF NOT bMoved
					STRING sPlayerAnims
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
						sPlayerAnims = "move_p_m_zero"
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						sPlayerAnims = "move_p_m_one"
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						sPlayerAnims = "move_p_m_two"
					ENDIF
					TASK_PLAY_ANIM(PLAYER_PED_ID(), sPlayerAnims, "idle_intro")
				ENDIF
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
			bSkipped = FALSE
			
			// Set the ped dying and chatter timers up
			iDyingTimer = GET_GAME_TIMER() + (1000*TALINA_DEATH_TIME)
			iPedChatterTimer = GET_GAME_TIMER() + 3000
			
			// Get rid of scenario blocker now
			REMOVE_SCENARIO_BLOCKING_AREA(blockedArea)
			blockedArea = ADD_SCENARIO_BLOCKING_AREA(<<1731.0516, 3847.6455, 33.8130>>+<<-20,-20,-5>>, <<1731.0516, 3847.6455, 33.8130>>+<<35,25,5>>)
			
			// Going to start driving
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			RC_END_CUTSCENE_MODE()
			
			main_stage = RESCUE_STAGE
			rescueStage = RSTAGE_DRIVE
		BREAK
	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    MISSION STAGE PROC
///    Monitors main driving stage.
PROC STAGE_GET_TO_DOCTOR()

	IF NOT IS_PED_INJURED(pedVictim)

		// Check for player nobbling her - HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY checks don't work for peds in player's group
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedVictim)
			bPlayerRuinedThings = TRUE
			rescueStage = RSTAGE_TALINA_DYING
			iTalinaDyingStage = 5
		
		ELIF GET_GAME_TIMER() < iDyingTimer
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedVictim, FALSE)
			//SET_PED_RESET_FLAG(pedVictim, PRF_AllowTasksIncompatibleWithMotion)
			
			// Stop updating dialogue if she's fleeing due to you fighting cops - B*2001463
			IF NOT IS_PED_FLEEING(pedVictim)
				PED_ROUTE_CHAT()
			ENDIF

			IF IS_PED_GROUPED()
				
				// Check for removing initial scene at distance
				IF bDeadPedSpawned
					//IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDead, 100)
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), << 1935.530, 6291.888, 41.144 >>, 100) // Switched to use ped's coords - B*1595023
						SAFE_RELEASE_PED(pedDead)
						SAFE_RELEASE_VEHICLE(vehCrashed)
						IF DOES_PARTICLE_FX_LOOPED_EXIST(i_ptfx_fire)
							STOP_PARTICLE_FX_LOOPED(i_ptfx_fire)
						ENDIF
						bDeadPedSpawned = FALSE
					ELSE
						FLASH_HAZARDS()
					ENDIF
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(blipDoctors)
					blipDoctors = CREATE_AMBIENT_BLIP_FOR_COORD(VEndLocation, TRUE)
				ENDIF
				
				IF bEndSceneCreated
					// See if we should trigger cutscene
					IF IS_PED_INJURED(pedSafehouse)
						// Fail if player killed the doctor
						bPlayerRuinedThings = TRUE
						rescueStage = RSTAGE_TALINA_DYING
					ELSE
						// Check ped
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSafehouse, PLAYER_PED_ID())
						OR IS_ENTITY_IN_RANGE_ENTITY(pedSafehouse, PLAYER_PED_ID(), 1.25)
							// Fail if player messed with the doc
//							IF DOES_ENTITY_EXIST(oiDocsChair)
//								FREEZE_ENTITY_POSITION(oiDocsChair, FALSE)
//							ENDIF
							CLEAR_PED_TASKS(pedSafehouse)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSafehouse, FALSE)
							//SET_ENTITY_COORDS(pedSafehouse, <<1729.8754, 3851.9641, 33.7783>>)
							//SET_ENTITY_HEADING(pedSafehouse, 212.9)
							SET_PED_TO_RAGDOLL(pedSafehouse, 250, 500, TASK_RELAX, FALSE, FALSE)
							TASK_SMART_FLEE_PED(pedSafehouse, PLAYER_PED_ID(), 500.0, -1)
							SET_PED_KEEP_TASK(pedSafehouse, TRUE)
							bPlayerRuinedThings = TRUE
							rescueStage = RSTAGE_TALINA_DYING
							iTalinaDyingStage = 0
						ELSE
							// Check for cutscene triggering
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), VEndLocation, g_vAnyMeansLocate, TRUE)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1722.09, 3843.00, 33.60 >>, << 1740.11, 3854.94, 39.22 >>, 12.0)
								rescueStage = RSTAGE_OUTRO_CUTSCENE
								end_cut_stage = stopTheCar
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CREATE_END_SCENE()
				ENDIF
					
				// Check if we need to update the vehicle anims
				IF IS_PED_SITTING_IN_ANY_VEHICLE(pedVictim)
				AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_ON_ANY_BIKE(pedVictim)
						animDictDeathCurrent = animDictDeathStd
						IF IS_PLAYERS_VEHICLE_LOW()
							animDictDeathCurrent = animDictDeathLow
						ELIF IS_PLAYERS_VEHICLE_VAN()
							animDictDeathCurrent = animDictDeathVan
						ENDIF
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
							TASK_PLAY_ANIM(pedVictim, animDictDeathCurrent, "LOOP", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING)
						ENDIF
					ENDIF
				ENDIF
	
			ELSE
			
				IF DOES_BLIP_EXIST(blipDoctors)
					REMOVE_BLIP(blipDoctors)
				ENDIF
				
				// Check for end scene interference
				IF bEndSceneCreated
					// Check for player messing with end scene
					IF IS_PED_INJURED(pedSafehouse)
						// Fail if player killed the doctor
						bPlayerRuinedThings = TRUE
						rescueStage = RSTAGE_TALINA_DYING
					
					// Check ped
					ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSafehouse, PLAYER_PED_ID())
					OR IS_ENTITY_IN_RANGE_ENTITY(pedSafehouse, PLAYER_PED_ID(), 1.25)
						// Fail if player messed with the doc
//						IF DOES_ENTITY_EXIST(oiDocsChair)
//							FREEZE_ENTITY_POSITION(oiDocsChair, FALSE)
//						ENDIF
						CLEAR_PED_TASKS(pedSafehouse)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSafehouse, FALSE)
						//SET_ENTITY_COORDS(pedSafehouse, <<1729.8754, 3851.9641, 33.7783>>)
						//SET_ENTITY_HEADING(pedSafehouse, 212.9)
						SET_PED_TO_RAGDOLL(pedSafehouse, 250, 500, TASK_RELAX, FALSE, FALSE)
						TASK_SMART_FLEE_PED(pedSafehouse, PLAYER_PED_ID(), 500.0, -1)
						SET_PED_KEEP_TASK(pedSafehouse, TRUE)
						bPlayerRuinedThings = TRUE
						rescueStage = RSTAGE_TALINA_DYING
						iTalinaDyingStage = 0
					ENDIF
				ENDIF
				
				// Check for abandonment
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedVictim, 200.0)
					CPRINTLN(DEBUG_MISSION, "CrashRescue - Player abandoned victim")
					rescueStage = RSTAGE_TALINA_DYING
					iTalinaDyingStage = 5
				ENDIF
			ENDIF

		ELSE
			// Time up - player has taken too long
			bPlayerRuinedThings = FALSE
			rescueStage = RSTAGE_TALINA_DYING
		ENDIF
		
	ELSE
		PRINTLN(GET_THIS_SCRIPT_NAME(), " - Going to fail state, player messed with something")
		bPlayerRuinedThings = TRUE
		rescueStage = RSTAGE_TALINA_DYING
	ENDIF
ENDPROC

/// PURPOSE:
///    MISSION STAGE PROC
///    Runs the outro cutscene - victim ped goes into doctors then mission pass
PROC STAGE_OUTRO_CUTSCENE()

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_F7)
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					PRINTSTRING("\n<re_crashrescue> - F7 has been pressed - player control is on.\n")
				ELSE
					PRINTSTRING("\n<re_crashrescue> - F7 has been pressed - player control is off.\n")
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	BOOL bStartCutscene = FALSE
	
	SWITCH end_cut_stage
	
		CASE stopTheCar
		
			// Stop the vehicle ready for delivery event
			IF NOT IS_PED_INJURED(pedVictim)
			AND NOT IS_PED_INJURED(pedSafehouse)
				IF IS_PED_IN_GROUP(pedVictim)
				OR IS_ENTITY_AT_COORD(pedVictim, VEndLocation, g_vOnFootLocate)	
					//Set blinders on early
					SET_MULTIHEAD_SAFE(TRUE)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 5)
							bStartCutscene = TRUE
						ENDIF
					ELSE
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<< 1722.3253, 3842.0803, 33.5323 >>, << 1741.2690, 3856.8623, 37.1775 >>, 11.0, <<1740.9175, 3831.5713, 33.8387>>, 121.9)
						bStartCutscene = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bStartCutscene = TRUE
				// Do we need the first person flash?
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					bFPFlashNeeded = TRUE
				ELSE
					bFPFlashNeeded = FALSE
				ENDIF
				IF DOES_BLIP_EXIST(blipDoctors)
					REMOVE_BLIP(blipDoctors)
				ENDIF
				RC_START_CUTSCENE_MODE(<<1731.0516, 3847.6455, 33.8130>>)
				CLEAR_AREA(GET_ENTITY_COORDS(pedSafehouse), 20.0, TRUE)
				CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(pedSafehouse), 20)
				WAIT(0)
				end_cut_stage = ENDCUT_SETUP
			ENDIF
		
		BREAK
		
		CASE ENDCUT_SETUP
			IF NOT IS_PED_INJURED(pedVictim)
			AND NOT IS_PED_INJURED(pedSafehouse)
				
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(crashrescueConversation, "RECRAAU", "RECRA_ARRIV", "RECRA_ARRIV_1", CONV_PRIORITY_AMBIENT_MEDIUM) // This is it. He can stitch me up. Thank you.  
					SET_ENTITY_HEALTH(pedVictim, 200)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					VEHICLE_INDEX vTmp
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vTmp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_AS_MISSION_ENTITY(vTmp)
						IF IS_END_SCENE_VEHICLE_OVER_SIZE_LIMIT(vTmp) // Checking for oversized - B*2013933
							SET_ENTITY_COORDS(vTmp,  <<1738.6554, 3836.2266, 33.8790>>)
							SET_ENTITY_HEADING(vTmp, 119)
						ELSE
							SET_ENTITY_COORDS(vTmp, vEndLocation)
							SET_ENTITY_HEADING(vTmp, 119)
						ENDIF
						SET_VEHICLE_ON_GROUND_PROPERLY(vTmp)
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							SET_FIRST_PERSON_FLASH_EFFECT_VEHICLE_MODEL_HASH(ENUM_TO_INT(GET_ENTITY_MODEL(vTmp)))
						ENDIF
					ELSE
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 25)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1734.2593, 3840.5903, 33.7883>>)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					SET_ENTITY_COORDS(pedVictim, <<1738.0475, 3842.4382, 33.7707>>)
					SET_ENTITY_HEADING(pedVictim, 36.8203)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
					CLEAR_AREA(VEndLocation, 4.0, TRUE)
					SAFE_RELEASE_VEHICLE(vTmp)
					
					endSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					TASK_SYNCHRONIZED_SCENE(pedVictim, endSceneId, animDictEnd, "helping_friend_inside_victim", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedSafehouse, endSceneId, animDictEnd, "helping_friend_inside_friend", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					camEndCut = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
					PLAY_SYNCHRONIZED_CAM_ANIM(camEndCut, endSceneId, "HELPING_FRIEND_INSIDE_CAM", animDictEnd)
					SET_SYNCHRONIZED_SCENE_PHASE(endSceneId, fOutroStartPhase)
					SET_CAM_ACTIVE(camEndCut, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)	
					
					end_cut_stage = ENDCUT_CLEANUP
				ENDIF
			ENDIF
		BREAK

		CASE ENDCUT_CLEANUP

			IF bFPFlashNeeded
			AND IS_SYNCHRONIZED_SCENE_RUNNING(endSceneId)
			AND GET_SYNCHRONIZED_SCENE_PHASE(endSceneId) > 0.97  // 425 frames = 14166.6ms, so this is at approx 13866.6ms
				CPRINTLN(DEBUG_MISSION, "REQUESTED FPS FLASH")
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bFPFlashNeeded = FALSE
			ENDIF
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(endSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(endSceneId) >= fOutroEndPhase)
			OR DO_FADES_IF_CUT_IS_SKIPPED()
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				SET_CAM_ACTIVE(camEndCut, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)	
				DESTROY_CAM(camEndCut)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				DELETE_PED(pedVictim)
				DELETE_PED(pedSafehouse)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				RC_END_CUTSCENE_MODE()
				WHILE IS_SCREEN_FADING_IN()
					WAIT(0)
				ENDWHILE
				//Disable multihead blinders
				SET_MULTIHEAD_SAFE(FALSE)
				missionPassed()
			ENDIF
				
		BREAK

	ENDSWITCH
ENDPROC

/// PURPOSE:
///    MISSION STAGE PROC
///    Handle ped dying then ending the mission
PROC STAGE_TALINA_DYING()
	
	SWITCH iTalinaDyingStage
		CASE 0 // Init

			IF IS_ENTITY_ALIVE(pedVictim)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF DOES_BLIP_EXIST(blipDoctors)
						REMOVE_BLIP(blipDoctors)
					ENDIF
					iDyingTimer = GET_GAME_TIMER() + 6000
					// Set up Talina's death line
					IF bPlayerRuinedThings
						// Use recrimination line
						sDyingLine = "RECRA_VDIE2"	// What were you thinking?
					ELSE
						// Use normal timeout line
						sDyingLine = "RECRA_VDIE"	// That motherfucker killed me
					ENDIF
					SET_PED_CONFIG_FLAG(pedVictim, PCF_RemoveDeadExtraFarAway, TRUE)
					iTalinaDyingStage++
				ENDIF
			ELSE
				missionCleanup()
			ENDIF
			
		BREAK
			
		CASE 1 // Talina death comment
			
			IF IS_ENTITY_ALIVE(pedVictim)
				IF CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", sDyingLine, CONV_PRIORITY_AMBIENT_MEDIUM) // That motherfucker.../What were you thinking?
				OR GET_GAME_TIMER() > iDyingTimer // Time out if taking to long to start - progression critical
					iTalinaDyingStage++
				ENDIF
			ELSE
				missionCleanup()
			ENDIF
			
		BREAK

		CASE 2 // Wait for comment, pick player response
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF bPlayerRuinedThings
						sDyingLine = "RECRA_MDIE2" // Shit. What the fuck was I thinking.
					ELSE
						sDyingLine = "RECRA_MDIE" // Great. Another corpse on my hands.
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF bPlayerRuinedThings
						sDyingLine = "RECRA_FDIE2" // Aw shit, that ain't good.
					ELSE
						sDyingLine = "RECRA_FDIE" // Damn. You dead, girl?
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF bPlayerRuinedThings
						sDyingLine = "RECRA_TDIE2" // Sometimes I forget what a terrible friend I can be.
					ELSE
						sDyingLine = "RECRA_TDIE" // Aww. I was rooting for you, little lady.
					ENDIF
				ENDIF
				iDyingTimer = GET_GAME_TIMER() + 500
				iTalinaDyingStage++
			ENDIF
			
		BREAK
		
		CASE 3 // Death and finality
			
			IF GET_GAME_TIMER() > iDyingTimer
			AND IS_SCREEN_FADED_IN()
			
				IF NOT IS_PED_INJURED(pedVictim)
					IF IS_PED_IN_ANY_VEHICLE(pedVictim)
						SET_PED_DIES_IN_VEHICLE(pedVictim, TRUE)
						APPLY_DAMAGE_TO_PED(pedVictim, 200, FALSE)
					ELSE
						SET_PED_TO_RAGDOLL(pedVictim, 500, 1000, TASK_RELAX, FALSE, FALSE)
						SET_ENTITY_HEALTH(pedVictim, 0)
					ENDIF
					iDyingTimer =  GET_GAME_TIMER() + 2000
					iTalinaDyingStage++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 4 // Player ped comment
			
			IF (GET_GAME_TIMER() > iDyingTimer AND CREATE_CONVERSATION(crashrescueConversation, "RECRAAU", sDyingLine, CONV_PRIORITY_AMBIENT_MEDIUM)) //Hey, buddy! Wake up! buddy! Shit, he's dead.
			OR GET_GAME_TIMER() > iDyingTimer + 6000 // Time out if taking to long to start - progression critical
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP - DEAD VICTIM")
				missionCleanup()
			ENDIF
			
		BREAK
		
		CASE 5 // Talina dying on the ground due to player messing with the scene
		
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				CLEAR_PED_TASKS(pedVictim)
				SET_PED_TO_RAGDOLL(pedVictim, 500, 1000, TASK_RELAX, FALSE, FALSE)
				APPLY_DAMAGE_TO_PED(pedVictim, 200, FALSE)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP - PLAYER DAMAGED VICTIM IN START AREA, OFFING HER IN SCRIPT")
				missionCleanup()
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP - PLAYER KILLED VICTIM OUTRIGHT IN START AREA")
				missionCleanup()
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC


// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

/// PURPOSE:
///    Monitor debug keys and widgets.
PROC RUN_DEBUG()
	#IF IS_DEBUG_BUILD
		
		IF bDebugRenderBrainRange
			DRAW_DEBUG_SPHERE(vInput, 209.0, 64, 64, 128, 64)
		ENDIF
		
		IF bDebugDontShortenOutro
			fOutroStartPhase = 0.0
			fOutroEndPhase = 1.0
		ELSE
			fOutroStartPhase = OUTRO_START_PHASE
			fOutroEndPhase = OUTRO_END_PHASE
		ENDIF

		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			SWITCH main_stage
				CASE CRASH_STAGE
					v_debug_warp = << 1929.8025, 6278.3799, 41.8301 >>
					debug_warp_heading = 212.5
				BREAK
				CASE RESCUE_STAGE
					v_debug_warp = << VEndLocation.X, VEndLocation.Y-12, VEndLocation.Z+0.5 >>
					debug_warp_heading = 0.0
				BREAK
			ENDSWITCH
						
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), v_debug_warp)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), debug_warp_heading)
				LOAD_SCENE(v_debug_warp)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF main_stage = RESCUE_STAGE
			AND rescueStage <> RSTAGE_OUTRO_CUTSCENE
				rescueStage = RSTAGE_DRIVE
				iDyingTimer = GET_GAME_TIMER() + (TALINA_DEATH_TIME*1000)
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			can_cleanup_script_for_debug = TRUE
			CPRINTLN(DEBUG_AMBIENT, "RE_CRASHRESCUE DEBUG PASS")
			SAFE_DELETE_PED(pedVictim)
			missionPassed()
		ENDIF
				
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			can_cleanup_script_for_debug = TRUE
			CPRINTLN(DEBUG_AMBIENT, "RE_CRASHRESCUE DEBUG FAIL")
			SAFE_DELETE_PED(pedVictim)
			missionCleanup()
		ENDIF
		
	#ENDIF	
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_crashrescue launched ")
PRINTVECTOR(vInput)
PRINTNL()

VEndLocation = << 1735.79, 3840.59, 33.9 >>

//////////////////////////////////

#IF IS_DEBUG_BUILD
	widgetGroup = START_WIDGET_GROUP("re_crashrescue") 
		ADD_WIDGET_BOOL("Draw the script brain range", bDebugRenderBrainRange)
		ADD_WIDGET_BOOL("Force the whole outro synch scene to play", bDebugDontShortenOutro)
    STOP_WIDGET_GROUP()
#ENDIF

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
			REMOVE_PED_FROM_GROUP(pedVictim)
		ENDIF
		TASK_WANDER_STANDARD(pedVictim)
		SET_PED_KEEP_TASK(pedVictim, TRUE)
		WAIT(0)
	ENDIF
	
	// Kill Trevor lines in case they end up playing over the top of Trevor 2's cutscene
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE() = 2 // Trevor
		CPRINTLN(DEBUG_MISSION, "RE Crash Rescue killed a conversation where Trevor was speaking in case it continued into Trevor 2's cutscene")
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "RE Crash Rescue FORCE CLEANUP")
	missionCleanup()
ENDIF

IF IS_PLAYER_IN_A_POLICE_VEHICLE()
	PRINTSTRING("\nre_crashrescue - IS_PLAYER_IN_A_POLICE_VEHICLE()\n")
	TERMINATE_THIS_THREAD()
ENDIF
IF IS_PLAYER_A_TAXI_PASSENGER()
	PRINTSTRING("\nre_crashrescue - IS_PLAYER_A_TAXI_PASSENGER()\n")
	TERMINATE_THIS_THREAD()
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)

	RUN_DEBUG()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	
	SWITCH main_stage	
	
		CASE CRASH_STAGE
			IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
				SWITCH crashStage
					CASE CSTAGE_CREATE
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP WHILE LOADING ASSETS - CSTAGE_CREATE")
							missionCleanup()
						ELIF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP BEFORE ACTIVATION - CSTAGE_CREATE")
							missionCleanup()
						ELSE
							STAGE_LOAD_ASSETS()
						ENDIF
					BREAK
					CASE CSTAGE_RUN_CRASHRESCUE
						STAGE_FIND_INJURED_PED()
					BREAK
					CASE CSTAGE_INTRO_CUTSCENE
						STAGE_INTRO_CUTSCENE()
					BREAK
				ENDSWITCH		
			ELSE
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - CLEANUP - OUT OF BRAIN ACTIVATION RANGE")
				missionCleanup()
			ENDIF
		BREAK
		
		CASE RESCUE_STAGE
			SWITCH rescueStage
				CASE RSTAGE_DRIVE
					STAGE_GET_TO_DOCTOR()
				BREAK
				CASE RSTAGE_OUTRO_CUTSCENE
					STAGE_OUTRO_CUTSCENE()
				BREAK
				CASE RSTAGE_TALINA_DYING
					STAGE_TALINA_DYING()
				BREAK
			ENDSWITCH
		BREAK

	ENDSWITCH
		
ENDWHILE

ENDSCRIPT


