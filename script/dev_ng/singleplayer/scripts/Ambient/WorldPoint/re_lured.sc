//////////////////////////////////////////////////////////
// 				Pretty girl luring you to get mugged
//		   		Kevin Wong
//
//
//				1
//
//
//
//
//				2
//////////////////////////////////////////////////////////
///
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////    

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING "script_player.sch"
USING "rage_builtins.sch"
USING "LineActivation.sch"
USING "Ambient_Speech.sch"
USING "ambient_common.sch"
USING "script_blips.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM homelessStageFlag
	homelessInit,
	homelessWaitForPlayer,
	homelessCleanup
ENDENUM
//homelessStageFlag = homelessInit

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

//INT random_int 
BOOL bAssetsLoaded
BOOL bDoFullCleanup
VECTOR vInput
//VECTOR voffset_temp

VECTOR vMugger
VECTOR vCopReaction
//VECTOR coords_mugger_2
//VECTOR coords_mugger_3
//VECTOR coords_mugger_4
VECTOR vLure

VECTOR vLureWalksToCoord
VECTOR vLureWalksToCoord2

FLOAT fMugger
REL_GROUP_HASH rghMugger
//FLOAT heading_mugger_2
//FLOAT heading_mugger_3
//FLOAT heading_mugger_4

//WEAPON_TYPE thisWeaponType
//VECTOR coords_hotgirlWEAP

//BLIP_INDEX blip_mugger_2
//BLIP_INDEX blip_mugger_3
//BLIP_INDEX blip_mugger_4

//////////WEAPON_TYPE CurrentWeapon

PED_INDEX pedLure
PED_INDEX pedMugger

BLIP_INDEX blipLure
BLIP_INDEX blipMugger
BLIP_INDEX blipInitialPed

OBJECT_INDEX objWallet

SEQUENCE_INDEX seq

structPedsForConversation cultresConversation

//VEHICLE_INDEX temp_player_vehicle
//PED_INDEX ped_mugger_2
//PED_INDEX ped_mugger_3
//PED_INDEX ped_mugger_4

//PED_INDEX pedestrian
//BOOL has_player_attacked_mugger
BOOL bLureScreamingForHelp
BOOL bPlayerInteractedWithLure
BOOL bPlayerHeldUp
BOOL bMoneyTakenByMugger
//////////BOOL bWalletReached
BOOL bMoneyDebited
BOOL bMuggersFightingPlayer
BOOL bMuggersFleeingPlayer
//////////BOOL bPlayerAndMuggerSyncedUp
BOOL bLureAskedPlayerToGetOutOfCar

INT iLureTalkStage = 0
INT iAmountToBeStolen, iStolenAmount

//BOOL make_goon_approach = FALSE
///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
///    
///    1. Wait For player to get close
///    2. Blip Girl
///    3. Girl walks down alley, she will stop if player is not close. MIssion fail is player is too far away
///    4. create thugs in alley that attack player
///    
///    
///    
PROC missionCleanup()
	PRINTSTRING("@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@")PRINTNL()
	
	IF bDoFullCleanup
		REMOVE_SCENARIO_BLOCKING_AREAS()
		DISABLE_CELLPHONE(FALSE)
		CANCEL_MUSIC_EVENT("RE35_OS")
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		DELETE_OBJECT(objWallet)
		
//		IF IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(pedLure)
//				CLEAR_PED_TASKS(pedLure)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedLure, seq)
				SET_PED_KEEP_TASK(pedLure, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedMugger)
//				CLEAR_PED_TASKS(pedMugger)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedMugger, seq)
				SET_PED_KEEP_TASK(pedMugger, TRUE)
			ENDIF
//		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF can_cleanup_script_for_debug
				IF DOES_ENTITY_EXIST(pedLure)
					DELETE_PED(pedLure)
				ENDIF
				IF DOES_ENTITY_EXIST(pedMugger)
					DELETE_PED(pedMugger)
				ENDIF
			ENDIF
			PRINTNL()
			PRINTSTRING("re_lured cleaned up")
			PRINTNL()
		#ENDIF
	ENDIF
	
	g_bForceNextLuredEvent = FALSE
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@")PRINTNL()
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@")PRINTNL()
	missionCleanup()
ENDPROC

PROC CHECK_EVENT_MAP_LOCATION()

//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 862.9633, -1049.8569, 28.5362 >>, << 150, 150, 150 >>, FALSE)
		vMugger = << 879.8144, -1047.4327, 32.0067 >>
		fMugger = 237.0934
		
		vLure = << 862.9633, -1049.8569, 28.5362 >>
		
		vCopReaction = << 15, 15, 15 >>
		/*coords_mugger_2 = <<-716.1, -722.81, 27.7 >>
		heading_mugger_2 = 180.0
		
		coords_mugger_3 = <<-714.41, -720.81, 27.7 >>
		heading_mugger_3 = 180.0
		
		coords_mugger_4 = <<-714.3, -722.81, 27.7 >>
		heading_mugger_4 = 180.0*/
		vLureWalksToCoord = << 881.1367, -1050.7061, 32.0067 >>
		vLureWalksToCoord2 = << 876.7977, -1052.5198, 32.0067 >>//<< 882.3238, -1051.9349, 32.0067 >>
//	ENDIF
	
//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -419.57, -340.85, 32.5 >>, << 150, 150, 150 >>, FALSE)
//	
//		vMugger = << -419.57, -340.85, 32.5 >>
//		fMugger = 160
//		/*
//		coords_mugger_2 = <<-428.57,-338.85,32.5>>
//		heading_mugger_2 = 220.0
//		coords_mugger_3 = <<-429.57,-340.85,32.5>>
//		heading_mugger_3 = 220.0
//		coords_mugger_4 = <<-419.57,-335.85,32.5>>
//		heading_mugger_4 = 160.0
//		*/
//		vLureWalksToCoord = << -424.42, -340.27, 33.03 >>
//	ENDIF
	
ENDPROC

PROC create_assets()
		CHECK_EVENT_MAP_LOCATION()

//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 150, 150, 150 >>, FALSE)
		REQUEST_MODEL(A_M_M_Tramp_01) //whore
		REQUEST_MODEL(A_F_M_SkidRow_01) //punter
		REQUEST_MODEL(Prop_LD_Wallet_01_S)
		REQUEST_ANIM_DICT("random@mugging5")
		REQUEST_ANIM_DICT("random@gang_intimidation@")
		PREPARE_MUSIC_EVENT("RE35_OS")
//		REQUEST_ANIM_DICT("move_lemar@alley")
		/*
		loopSceneId = CREATE_SYNCHRONIZED_SCENE(GET_ROADSIDE_POS(), sceneRotation)
		SET_SYNCHRONIZED_SCENE_LOOPED(loopSceneId, TRUE)
		TASK_SYNCHRONIZED_SCENE (pedPossum, loopSceneId, "random@mugging5", "carjack_waitLoop_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
		TASK_SYNCHRONIZED_SCENE (pedJacker, loopSceneId, "random@mugging5", "ig_1_girl_agitated_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
		*/
		WHILE NOT HAS_MODEL_LOADED(A_F_M_SkidRow_01)
		OR NOT HAS_MODEL_LOADED(A_M_M_Tramp_01)
		OR NOT HAS_MODEL_LOADED(Prop_LD_Wallet_01_S)
		OR NOT HAS_ANIM_DICT_LOADED("random@mugging5")
		OR NOT HAS_ANIM_DICT_LOADED("random@gang_intimidation@")
//		OR NOT PREPARE_MUSIC_EVENT("RE35_OS")
//		OR NOT HAS_ANIM_DICT_LOADED("move_lemar@alley")
			WAIT(0)
		ENDWHILE
		
		ADD_SCENARIO_BLOCKING_AREA(vInput - << 30, 30, 10 >>, vInput + << 30, 30, 10 >>)
		
		ADD_RELATIONSHIP_GROUP("mugger", rghMugger)
	//	voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedLure, <<0.0,0.0,-1.0>>)
//		voffset_temp.z = vInput.z - 0.3
		pedLure = CREATE_PED(PEDTYPE_DEALER, A_F_M_SkidRow_01, vLure, 79.7650)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLure, TRUE)
		SET_PED_CONFIG_FLAG(pedLure, PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedLure, FA_DISABLE_HANDS_UP, TRUE)
//		SET_ENTITY_IS_TARGET_PRIORITY(pedLure, TRUE)
		STOP_PED_SPEAKING(pedLure, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedLure, rghMugger)
//		voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedLure, << 0.8, 0.8, -1.0 >>)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghMugger, RELGROUPHASH_PLAYER)
//		blipInitialPed = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(pedLure)
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_USE_TORSO)
			TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_1_girl_agitated_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_SECONDARY|AF_UPPERBODY)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedLure, seq)
		CLEAR_SEQUENCE_TASK(seq)
//		IF DOES_BLIP_EXIST(blipInitialPed)
//			REMOVE_BLIP(blipInitialPed)
//		ENDIF
//		blipLure = CREATE_BLIP_FOR_PED(pedLure)
		
		
		pedMugger = CREATE_PED(PEDTYPE_GANG1, A_M_M_Tramp_01, vMugger, fMugger)
		SET_AMBIENT_VOICE_NAME(pedMugger, "A_M_M_TRAMP_01_BLACK_MINI_01")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMugger, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedMugger, FA_DISABLE_HANDS_UP, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(pedMugger, TRUE)
		STOP_PED_SPEAKING(pedMugger, TRUE)
		//SET_PED_DUCKING(pedMugger, TRUE )
		SET_PED_RELATIONSHIP_GROUP_HASH(pedMugger, rghMugger)
		TASK_LOOK_AT_ENTITY(pedMugger, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
	//	TASK_PLAY_ANIM (pedMugger, "random@mugging5", "carjack_waitloop_male", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		
		/*
		ped_mugger_2 = CREATE_PED(PEDTYPE_GANG1,A_M_M_Tramp_01, coords_mugger_2, heading_mugger_2)
		SET_PED_DUCKING(ped_mugger_2, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped_mugger_2, RELGROUPHASH_MISSION1)
		ped_mugger_3 = CREATE_PED(PEDTYPE_GANG1,A_M_M_Tramp_01, coords_mugger_3, heading_mugger_3)
		SET_PED_DUCKING(ped_mugger_3, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped_mugger_3, RELGROUPHASH_MISSION1)
		ped_mugger_4 = CREATE_PED(PEDTYPE_GANG1,A_M_M_Tramp_01, coords_mugger_4, heading_mugger_4)
		SET_PED_DUCKING(ped_mugger_4, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped_mugger_4, RELGROUPHASH_MISSION1)
		*/
		
		
		ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
		ADD_PED_FOR_DIALOGUE(cultresConversation, 4, pedLure, "HOBOFEMALE")
		ADD_PED_FOR_DIALOGUE(cultresConversation, 5, pedMugger, "HOBOMALE")
		bAssetsLoaded = TRUE
//	ENDIF
	
ENDPROC

PROC debug_stuff()
	
	#IF IS_DEBUG_BUILD
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 859.6851, -1050.0599, 28.0616 >>, TRUE, FALSE)
			WAIT(100)
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ENDIF
		
	#ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_MOVING_LEFT_STICK(INT iLEFT_STICK_THRESHOLD = 64)
	
	INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) -128
	INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) -128
	
	IF ReturnLeftX < iLEFT_STICK_THRESHOLD
	AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
	AND ReturnLeftY < iLEFT_STICK_THRESHOLD
	AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC checkIfPlayerIsArmed()
	
	IF NOT IS_PED_INJURED(pedLure)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			AND CAN_PED_SEE_HATED_PED(pedLure, PLAYER_PED_ID())
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Woah, you're armed. Errr, never mind pal", 4000, 1) //
				CLEAR_PED_TASKS(pedLure)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedLure, seq)
				SET_PED_KEEP_TASK(pedLure, TRUE)
				IF NOT IS_PED_INJURED(pedMugger)
					CLEAR_PED_TASKS(pedMugger)
					OPEN_SEQUENCE_TASK(seq)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedMugger, seq)
					SET_PED_KEEP_TASK(pedMugger, TRUE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
					CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_gun", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE)
					CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_other", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC makeLureTalk()
	
	IF NOT IS_ENTITY_DEAD(pedLure)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 15, 15, 15 >>, FALSE)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	IF  TIMERB() > 4000
					IF iLureTalkStage = 0
						//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thank you for helping", 4000, 1) //	
						CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_girlta", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						iLureTalkStage++
						SETTIMERB(0)
					ENDIF
					
					IF  TIMERB() > 3500
					AND iLureTalkStage = 1
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_girltb", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						iLureTalkStage++
						SETTIMERB(0)
						//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "He's just there", 4000, 1) //
					ENDIF
					
					IF  TIMERB() > 7000
					AND iLureTalkStage = 2
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						//iLureTalkStage=0
						SETTIMERB(0)
						//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "HURRY UP!", 4000, 1) //
						TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_girlch", CONV_PRIORITY_AMBIENT_HIGH)
//						TASK_PLAY_ANIM(pedLure, "random@mugging5", "ig_1_girl_agitated_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
					ENDIF
				ENDIF
				
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC playerInteractsWithLure()

	IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF NOT IS_ENTITY_DEAD(pedLure)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 65, 60, 60 >>)
//			AND NOT (IS_ENTITY_OCCLUDED(pedLure) OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 15, 15, 15 >>))
			OR g_bForceNextLuredEvent
//				checkIfPlayerIsArmed()
				IF DOES_BLIP_EXIST(blipInitialPed)
					REMOVE_BLIP(blipInitialPed)
				ENDIF
				blipLure = CREATE_BLIP_FOR_PED(pedLure)
				SHOW_HEIGHT_ON_BLIP(blipLure, FALSE)
				//SET_ENTITY_HEADING(pedLure, voffset_temp)
//				voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
//				TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), 600000, SLF_USE_TORSO)
//				OPEN_SEQUENCE_TASK(seq)
//					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_USE_TORSO)
////					TASK_TURN_PED_TO_FACE_COORD(NULL, voffset_temp)
//					TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_1_girl_agitated_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_SECONDARY|AF_UPPERBODY)
//				//	TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1, 6.0)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
//					
//			//		TASK_PLAY_ANIM (NULL, "random@mugging5", "carjack_waitLoop_female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
//					//TASK_PLAY_ANIM_ADVANCED (NULL, "amb@pim_prover_1", "m_argue01", vInput, << -1.519, 0.000, -0.000 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
//					//TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(pedLure, seq)
//				CLEAR_SEQUENCE_TASK(seq)
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Excuse me!", 6000, 1) //
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//				WAIT(0)
//				CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_help2", CONV_PRIORITY_AMBIENT_HIGH)
				//TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), 4000)
				SET_WANTED_LEVEL_MULTIPLIER(0)
				SET_RANDOM_EVENT_ACTIVE()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bLureScreamingForHelp
		IF NOT IS_PED_INJURED(pedLure)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 25, 25, 25 >>)
			AND IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_help2", CONV_PRIORITY_AMBIENT_HIGH)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT)
						TASK_PLAY_ANIM(NULL, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedLure, seq)
					CLEAR_SEQUENCE_TASK(seq)
					bLureScreamingForHelp = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF TIMERB() > 1000
	IF NOT IS_ENTITY_DEAD(pedLure)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 15, 15, 15 >>)
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
		//IF IS_ENTITY_AT_COORD (PLAYER_PED_ID(), vInput, <<10.0, 10.0, 10.0>>, FALSE)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 8, 8, 8 >>, FALSE)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2.5
				bPlayerInteractedWithLure = TRUE
				IF DOES_BLIP_EXIST(blipLure)
					SHOW_HEIGHT_ON_BLIP(blipLure, TRUE)
				ENDIF
				CLEAR_PED_TASKS(pedLure)
				CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_help", CONV_PRIORITY_AMBIENT_HIGH)
				IF NOT IS_ENTITY_DEAD(pedLure)
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Mister! Please help! My friend is hurt! He's down this allyway.", 4000, 1) //	
//					IF DOES_BLIP_EXIST(blipInitialPed)
//						REMOVE_BLIP(blipInitialPed)
//					ENDIF
					SETTIMERB(0)
//					blipLure = CREATE_BLIP_FOR_PED(pedLure)
//					TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), 30000, SLF_DEFAULT)
//					SET_PED_ALTERNATE_WALK_ANIM(pedLure, "MOVE_LEMAR@ALLEY", "run", 8.0, FALSE)
					CLEAR_PED_SECONDARY_TASK(pedLure)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT)
//						TASK_PLAY_ANIM(NULL, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_STAND_STILL(NULL, 1000)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, vLureWalksToCoord, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 80.1325)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLureWalksToCoord, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 80.1325) //Just commenting out this since navmesh doesn't work around here
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_SECONDARY|AF_UPPERBODY)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedLure, seq)
					CLEAR_SEQUENCE_TASK(seq)
					vCopReaction = << 40, 40, 30 >>
//					SET_PED_ALTERNATE_WALK_ANIM(player_ped_id(), "MOVE_LEMAR@ALLEY", "run", 8.0, FALSE)
				ENDIF
				
			ELSE
				IF NOT bLureAskedPlayerToGetOutOfCar
					KILL_FACE_TO_FACE_CONVERSATION()
					WAIT(0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_PED_INJURED(pedLure)
							CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_girlca", CONV_PRIORITY_AMBIENT_HIGH)
							//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Get out the car! My friend needs help! He needs CPR!", 4000, 1) //
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_USE_TORSO)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedLure, seq)
							CLEAR_SEQUENCE_TASK(seq)
							bLureAskedPlayerToGetOutOfCar = TRUE
							PRINTSTRING("\n girl asks player to get out car \n")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC checkIfPlayerHasBeenMugged()
	
//////////	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_exit_handsup")
////////////		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_exit_handsup") > 0.1//0.454
//////////			IF IS_PLAYER_MOVING_LEFT_STICK()
//////////			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
//////////			OR CurrentWeapon != WEAPONTYPE_UNARMED
//////////			OR GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_exit_handsup") > 0.95
//////////				DISABLE_CELLPHONE(FALSE)
//////////				DELETE_OBJECT(objWallet)
//////////				CLEAR_PED_TASKS(PLAYER_PED_ID())
//////////			ENDIF
////////////		ENDIF
//////////	ENDIF
	
	IF NOT bMoneyTakenByMugger
		IF NOT IS_PED_INJURED(pedMugger)
		//AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_SCRIPT_TASK_STATUS(pedMugger, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK//////////
			AND IS_ENTITY_AT_ENTITY(pedMugger, PLAYER_PED_ID(), << 2.8, 2.8, 2.8 >>)//////////
//////////			IF IS_ENTITY_PLAYING_ANIM(pedMugger, "random@mugging5", "ig_1_guy_handoff")
//////////				IF NOT bWalletReached
//////////					IF GET_ENTITY_ANIM_CURRENT_TIME(pedMugger, "random@mugging5", "ig_1_guy_handoff") > 0.085
//////////						objWallet = CREATE_OBJECT(Prop_LD_Wallet_01_S, GET_ENTITY_COORDS(PLAYER_PED_ID()))
//////////						ATTACH_ENTITY_TO_ENTITY(objWallet, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, TRUE, TRUE)
//////////						bWalletReached = TRUE
//////////					ENDIF
//////////				ENDIF
				IF NOT bMoneyDebited
//////////					IF GET_ENTITY_ANIM_CURRENT_TIME(pedMugger, "random@mugging5", "ig_1_guy_handoff") > 0.540
//////////						IF DOES_ENTITY_EXIST(objWallet)
//////////							DETACH_ENTITY(objWallet)
//////////							ATTACH_ENTITY_TO_ENTITY(objWallet, pedMugger, GET_PED_BONE_INDEX(pedMugger, BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, TRUE, TRUE)
//////////						ENDIF
						iAmountToBeStolen = 500
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) >= iAmountToBeStolen
							DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iAmountToBeStolen)
							iStolenAmount = iAmountToBeStolen
							CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_mugtha", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) <= 0
							iStolenAmount = 0
							CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_mugnom", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							iStolenAmount =	GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM())
							DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()))
							CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_mugtha", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF NOT IS_PED_INJURED(pedMugger)
							SET_PED_MONEY(pedMugger, iStolenAmount)
						ENDIF
						bMoneyDebited = TRUE
//////////					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedMugger)
//////////					IF GET_ENTITY_ANIM_CURRENT_TIME(pedMugger, "random@mugging5", "ig_1_guy_handoff") > 0.807
	//					CLEAR_PED_TASKS(pedMugger)
						OPEN_SEQUENCE_TASK(seq)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedMugger, seq)
						SET_PED_KEEP_TASK(pedMugger, TRUE)
						IF NOT IS_PED_INJURED(pedLure)
							CLEAR_PED_TASKS(pedLure)
							OPEN_SEQUENCE_TASK(seq)
//								TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedLure, seq)
							SET_PED_KEEP_TASK(pedLure, TRUE)
						ENDIF
						bMoneyTakenByMugger = TRUE
//////////					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC makeGoonsMug()
//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-424.42,-340.27,33.03>>, <<7.0, 7.0, 7.0>>, FALSE)
	IF NOT IS_PED_INJURED(pedMugger)
	//AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 873.9561, -1050.8057, 33.0066 >>, << 4.7500, 1.9000, 1.0000 >>, FALSE)
		//OR IS_ENTITY_AT_COORD(pedLure, vLureWalksToCoord, <<2.0, 2.0, 6.0>>, FALSE)
//		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLureWalksToCoord, << 4, 3, 6 >>, FALSE)
			IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
//				SET_PED_CAPSULE(PLAYER_PED_ID(), 0.5)
				//RESET_PED_ANIM_GROUP(player_ped_id())
	//				CLEAR_PED_ALTERNATE_WALK_ANIM(player_ped_id(), -8)
			//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_MISSION1,prostituteGroup )
				TASK_LOOK_AT_ENTITY(pedMugger, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				SET_PED_DUCKING(pedMugger, FALSE)
				GIVE_WEAPON_TO_PED(pedMugger, WEAPONTYPE_PISTOL, 200, TRUE)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_other", CONV_PRIORITY_AMBIENT_HIGH)
					//	temp_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 600000, SLF_USE_TORSO)
////							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//						//TASK_ENTER_VEHICLE(NULL, temp_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JACK_ANYONE)
//						//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,vLureWalksToCoord2, PLAYER_PED_ID(),PEDMOVE_RUN, FALSE, 1.0)
//						TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_1_guy_enter_stickup")
//						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 1.5)
//						TASK_ENTER_VEHICLE(NULL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JUST_PULL_PED_OUT|ECF_JACK_ANYONE)
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 500)
//					CLOSE_SEQUENCE_TASK(seq)
					MISSION_FAILED()
				ELSE
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						//TASK_TURN_PED_TO_FACE_ENTITY( NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_1_guy_enter_stickup", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
//////////						TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_1_guy_stickup_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vLureWalksToCoord, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 1.5)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 2.3, 4, TRUE, TRUE)//////////
						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 500)//////////
					CLOSE_SEQUENCE_TASK(seq)
				ENDIF
				TASK_PERFORM_SEQUENCE(pedMugger, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				SET_BLIP_AS_FRIENDLY(blipLure, FALSE)
				blipMugger = CREATE_BLIP_FOR_PED(pedMugger, TRUE)
//				blipLure = CREATE_BLIP_FOR_PED(pedLure, TRUE)
				
				TRIGGER_MUSIC_EVENT("RE35_OS")
				
				WAIT(500)
				
				IF NOT IS_PED_INJURED(pedLure)
//					CLEAR_PED_TASKS(pedLure)
					TASK_LOOK_AT_ENTITY(pedLure, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					GIVE_WEAPON_TO_PED(pedLure, WEAPONTYPE_PISTOL, 200, TRUE)
					OPEN_SEQUENCE_TASK(seq)
					//	TASK_GO_TO_COORD_ANY_MEANS(NULL,vLureWalksToCoord2, PEDMOVE_RUN, NULL)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vLureWalksToCoord2, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0, 4, TRUE, ENAV_STOP_EXACTLY)
						//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedLure, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					//TASK_SMART_FLEE_PED(pedLure, PLAYER_PED_ID(), 100.0, -1)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedMugger)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, pedMugger, -1, SLF_WHILE_NOT_IN_FOV)
						TASK_PAUSE(NULL, 1000)//////////
//////////						TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedMugger)
//////////						TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_2_guy_enter_handsup", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//////////						TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_2_guy_handsup_loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
					CLEAR_SEQUENCE_TASK(seq)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				// GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), thisWeaponType) 
	//				IF thisWeaponType = WEAPONTYPE_UNARMED
				
					//TASK_HANDS_UP(PLAYER_PED_ID(), 3000)
	//				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_mugger", CONV_PRIORITY_AMBIENT_HIGH)
				SETTIMERA(0)
				bPlayerHeldUp = TRUE
				
	//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//				WAIT(0)
	//			ENDWHILE
			ELSE
				CLEAR_PED_TASKS(pedMugger)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedMugger, seq)
				SET_PED_KEEP_TASK(pedMugger, TRUE)
				IF NOT IS_PED_INJURED(pedLure)
					CLEAR_PED_TASKS(pedLure)
					OPEN_SEQUENCE_TASK(seq)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedLure, seq)
					SET_PED_KEEP_TASK(pedLure, TRUE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_gun", CONV_PRIORITY_AMBIENT_HIGH)
				MISSION_FAILED()
			ENDIF
		ENDIF
		/*
		IF make_goon_approach = FALSE
		
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedMugger,<<10.0, 10.0, 10.0>>, FALSE )
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_GO_TO_ENTITY(pedMugger,PLAYER_PED_ID(),-1, 4.0)
				make_goon_approach = TRUE
			ENDIF
		ENDIF
		*/
	ENDIF
	
ENDPROC

PROC makePlayerAndMuggerSyncUp()

	IF NOT IS_PED_INJURED(pedMugger)
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_handsup_loop")
			OPEN_SEQUENCE_TASK(seq)
//				TASK_LOOK_AT_ENTITY(NULL, pedMugger, 600000, SLF_USE_TORSO)
//				TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedMugger)
				TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_2_guy_handoff", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				TASK_PLAY_ANIM(NULL, "random@mugging5", "ig_2_guy_exit_handsup", SLOW_BLEND_IN, SLOW_BLEND_OUT)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
			CLEAR_SEQUENCE_TASK(seq)
			
			TASK_PLAY_ANIM(pedMugger, "random@mugging5", "ig_1_guy_handoff", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
//////////			bPlayerAndMuggerSyncedUp = TRUE
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()

	VECTOR vTempMax = << 30, 30, 30 >>, vTempMin = << -30, -30, -30 >>
	
	IF NOT IS_PED_INJURED(pedLure)
	AND NOT IS_PED_INJURED(pedMugger)
		IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedLure, BONETAG_HEAD, << 0, 0, 0 >>), 30)
			PRINTSTRING("@@@@@@@@@@@@@@@@ IS_BULLET_IN_AREA() @@@@@@@@@@@@@@@@@@@")PRINTNL()
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedLure, << 30, 30, 30 >>)
		AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedLure)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedMugger)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedLure)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedMugger)
				IF CAN_PED_SEE_HATED_PED(pedLure, PLAYER_PED_ID())
				OR CAN_PED_SEE_HATED_PED(pedMugger, PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_gun", CONV_PRIORITY_AMBIENT_HIGH)
					PRINTSTRING("@@@@@@@@@@@@@@@@ IS_PLAYER_TARGETTING_ENTITY() @@@@@@@@@@@@@@@@@@@")PRINTNL()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		vTempMax = vTempMax + GET_PED_BONE_COORDS(pedLure, BONETAG_HEAD, << 0, 0, 0 >>)
		vTempMin = vTempMin + GET_PED_BONE_COORDS(pedLure, BONETAG_HEAD, << 0, 0, 0 >>)
		IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
			PRINTSTRING("@@@@@@@@@@@@@@@@ IS_PROJECTILE_TYPE_IN_AREA() @@@@@@@@@@@@@@@@@@@")PRINTNL()
			RETURN TRUE
		ENDIF
		
		IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedLure, BONETAG_HEAD, << 0, 0, 0 >>), 3)
			PRINTSTRING("@@@@@@@@@@@@@@@@ GET_IS_PETROL_DECAL_IN_RANGE() @@@@@@@@@@@@@@@@@@@")PRINTNL()
			RETURN TRUE
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), BULLDOZER)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedLure)
				OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedMugger)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), ANNIHILATOR)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), BUZZARD)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), LAZER)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(pedLure, FALSE) - vCopReaction, GET_ENTITY_COORDS(pedLure, FALSE) + vCopReaction)
		OR IS_COP_VEHICLE_IN_AREA_3D(GET_ENTITY_COORDS(pedLure, FALSE) - vCopReaction, GET_ENTITY_COORDS(pedLure, FALSE) + vCopReaction)
			PRINTSTRING("\n@@@@@@@@@@@@@@@@ IS_COP_PED/VEHICLE_IN_AREA_3D @@@@@@@@@@@@@@@@@@@\n")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 894.1762, -1048.2943, 41.8280 >>, << 15.0000, 21.5625, 10.0000 >>)
//	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -693.5698, -738.8224, 27.8521 >>, << 39.5000, 3.0625, 1.6875 >>)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_other", CONV_PRIORITY_AMBIENT_HIGH)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 15, 15, 15 >>)
		IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			PRINTSTRING("\n@@@@@@@@@@@@@@@@ HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED @@@@@@@@@@@@@@@@@@@\n")
			RETURN TRUE
		ENDIF
	ELIF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		PRINTSTRING("\n@@@@@@@@@@@@@@@@ CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED @@@@@@@@@@@@@@@@@@@\n")
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 40, 40, 40 >>)
		IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		PRINTSTRING("@@@@@@@@@@@@@@@@ IS_PLAYER_WANTED_LEVEL_GREATER() @@@@@@@@@@@@@@@@@@@")PRINTNL()
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedLure)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedLure, PLAYER_PED_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedMugger)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMugger, PLAYER_PED_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC makeGoonsOpenFire()

	IF NOT bPlayerHeldUp
		IF IS_PLAYER_INTERFERING_WITH_EVENT()
			bMuggersFightingPlayer = TRUE
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prostituteGroup, RELGROUPHASH_PLAYER)
//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghMugger, RELGROUPHASH_PLAYER)
//			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "We just wanted your money, but you had to be a hero. Die asshole!", 4000, 1) //
//			CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_attack", CONV_PRIORITY_AMBIENT_HIGH)
			IF NOT IS_PED_INJURED(pedMugger)
				CLEAR_PED_TASKS(pedMugger)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedMugger, seq)
				SET_PED_KEEP_TASK(pedMugger, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedLure)
				CLEAR_PED_TASKS(pedLure)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedLure, seq)
				SET_PED_KEEP_TASK(pedLure, TRUE)
			ENDIF
			MISSION_FAILED()
		ENDIF
	ELSE
		IF NOT bMuggersFightingPlayer
		AND NOT bMoneyTakenByMugger
////////////		AND bPlayerAndMuggerSyncedUp
//////////		AND TIMERA() > 2000
//////////			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_enter_handsup")
//////////			OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_handsup_loop")
//////////			OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_handoff")
//////////				SET_PED_RESET_FLAG(pedMugger, PRF_InstantBlendToAim, TRUE)
//////////				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
//////////				IF IS_PLAYER_MOVING_LEFT_STICK()
//////////				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
//////////				OR CurrentWeapon != WEAPONTYPE_UNARMED
////////////					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@mugging5", "ig_2_guy_handoff") < 0.550
//////////						DISABLE_CELLPHONE(FALSE)
//////////						DELETE_OBJECT(objWallet)
//////////						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//////////						
//////////						IF NOT bMoneyDebited
//////////							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghMugger, RELGROUPHASH_PLAYER)
//////////							IF NOT IS_PED_INJURED(pedMugger)
//////////								CLEAR_PED_TASKS(pedMugger)
//////////								TASK_COMBAT_PED(pedMugger, PLAYER_PED_ID())
//////////								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMugger, << 15, 15, 15 >>)
//////////									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//////////									WAIT(0)
//////////									CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_fucked", CONV_PRIORITY_AMBIENT_HIGH)
//////////								ENDIF
//////////							ENDIF
//////////							IF NOT IS_PED_INJURED(pedLure)
//////////								CLEAR_PED_TASKS(pedLure)
//////////								TASK_COMBAT_PED(pedLure, PLAYER_PED_ID())
//////////							ENDIF
//////////							bMuggersFightingPlayer = TRUE
//////////						ENDIF
////////////					ENDIF
//////////				ENDIF
//////////			ENDIF
			
			IF IS_PLAYER_INTERFERING_WITH_EVENT()
			OR IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
//			OR IS_PED_RUNNING(PLAYER_PED_ID())
			OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedMugger, FALSE), << 12, 12, 5 >>)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghMugger, RELGROUPHASH_PLAYER)
				IF NOT IS_PED_INJURED(pedMugger)
					CLEAR_PED_TASKS(pedMugger)
					TASK_COMBAT_PED(pedMugger, PLAYER_PED_ID())
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMugger, << 15, 15, 15 >>)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(cultresConversation, "lurinau", "lurin_fucked", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
//					CLEAR_PED_TASKS(pedLure)
//					CLEAR_PED_TASKS(pedMugger)
				//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "We just wanted your money, but you had to be a hero. Die asshole!", 4000, 1) //
				ENDIF
				IF NOT IS_PED_INJURED(pedLure)
					CLEAR_PED_TASKS(pedLure)
					TASK_COMBAT_PED(pedLure, PLAYER_PED_ID())
				ENDIF
				bMuggersFightingPlayer = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC remove_enemy_blips()
	IF DOES_BLIP_EXIST(blipMugger)
		IF IS_ENTITY_DEAD(pedMugger)
			REMOVE_BLIP(blipMugger)
		ENDIF
	ENDIF
	/* 
	IF DOES_BLIP_EXIST(blip_mugger_2)
		IF IS_ENTITY_DEAD(ped_mugger_2)
			REMOVE_BLIP(blip_mugger_2)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blip_mugger_3)
		IF IS_ENTITY_DEAD(ped_mugger_3)
			REMOVE_BLIP(blip_mugger_3)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blip_mugger_4)
		IF IS_ENTITY_DEAD(ped_mugger_4)
			REMOVE_BLIP(blip_mugger_4)
		ENDIF
	ENDIF
	*/
ENDPROC

PROC make_girl_run_Off_if_player_attacks_mugger()

	IF NOT IS_ENTITY_DEAD(pedMugger)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMugger, PLAYER_PED_ID())
//			has_player_attacked_mugger = TRUE
			IF NOT IS_PED_INJURED(pedLure)
				SET_PED_DROPS_WEAPON(pedLure)
				//TASK_SMART_FLEE_PED(pedLure, PLAYER_PED_ID(), 100.0, -1)
			//	SET_PED_KEEP_TASK(pedLure, TRUE) 
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC fleeIfPlayerIsLeavingAlley()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -734.9749, -728.4423, 29.4985 >>, << 2, 6, 2 >>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -663.9312, -725.6195, 28.2592 >>, << 2, 6, 2 >>)
		IF NOT IS_PED_INJURED(pedMugger)
			OPEN_SEQUENCE_TASK(seq)
//				TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedMugger, seq)
			SET_PED_KEEP_TASK(pedMugger, TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(pedLure)
			OPEN_SEQUENCE_TASK(seq)
//				TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedLure, seq)
			SET_PED_KEEP_TASK(pedLure, TRUE)
		ENDIF
		bMuggersFleeingPlayer = TRUE
	ENDIF
	
ENDPROC

PROC checksToEndMission()

	IF bPlayerHeldUp
		IF IS_PED_INJURED(pedMugger)
			MISSION_PASSED()
		ELSE
			IF bMoneyTakenByMugger
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMugger, << 100, 100, 100 >>)
					MISSION_FAILED()
				ENDIF
			ELSE
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMugger, << 100, 100, 100 >>)
					MISSION_PASSED()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedLure)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedLure, PLAYER_PED_ID())
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedLure)
				MISSION_FAILED()
			ENDIF
		ENDIF
		IF IS_PED_INJURED(pedLure)
			IF NOT IS_PED_INJURED(pedMugger)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedMugger, seq)
				SET_PED_KEEP_TASK(pedMugger, TRUE)
			ENDIF
			MISSION_FAILED()
		ENDIF
		IF IS_PED_INJURED(pedMugger)
			IF NOT IS_PED_INJURED(pedLure)
				CLEAR_PED_TASKS(pedLure)
				OPEN_SEQUENCE_TASK(seq)
//					TASK_GO_STRAIGHT_TO_COORD(NULL, vLure, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedLure, seq)
				SET_PED_KEEP_TASK(pedLure, TRUE)
			ENDIF
			MISSION_FAILED()
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedLure)
		IF DOES_BLIP_EXIST(blipLure)
			REMOVE_BLIP(blipLure)
		ENDIF
	ENDIF
	IF IS_PED_INJURED(pedMugger)
		IF DOES_BLIP_EXIST(blipMugger)
			REMOVE_BLIP(blipMugger)
		ENDIF
	ENDIF
	
ENDPROC


///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTVECTOR(vInput)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("lured")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	
//	random_int = GET_RANDOM_INT_IN_RANGE(0,2)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)		
		missionCleanup()
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bPlayerHeldUp		
		
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_LU")
			
//			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					//PRINTNL()
				//	PRINTSTRING("lured allowed to run")
					//PRINTNL()
					//IF HAVE_ENOUGH_DAYS_PASSED_SINCE_LAST_RUN(g_luredDateStamp)
						//PRINTNL()
						//PRINTSTRING("lured enough days have passed")
					//	PRINTNL()
						SWITCH ambStage
							CASE ambCanRun
								IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
									missionCleanup()
								ENDIF
								IF bAssetsLoaded
									ambStage = (ambRunning)
									bDoFullCleanup = TRUE
								ELSE
									create_assets()
								ENDIF
							BREAK
							CASE ambRunning
								//DO STUFF
								
//								IF NOT has_player_attacked_mugger
//									make_girl_run_off_if_player_attacks_mugger()
//								ENDIF
								
								IF NOT bPlayerHeldUp
									IF NOT bPlayerInteractedWithLure
										playerInteractsWithLure()
									ELIF NOT bMuggersFightingPlayer
										checkIfPlayerIsArmed()
										makeLureTalk()
									ENDIF
									makeGoonsMug()
									IF NOT IS_PED_INJURED(pedLure)
										SET_PED_RESET_FLAG(pedLure, PRF_UseProbeSlopeStairsDetection, TRUE)
									ENDIF
								ELSE
									SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
//									remove_enemy_blips()
									IF NOT bMuggersFightingPlayer
//////////										IF NOT bPlayerAndMuggerSyncedUp
//////////											makePlayerAndMuggerSyncUp()
//////////										ELSE
											checkIfPlayerHasBeenMugged()
//////////										ENDIF
									ELIF NOT bMuggersFleeingPlayer
										fleeIfPlayerIsLeavingAlley()
									ENDIF
								ENDIF
								IF NOT IS_PED_INJURED(pedMugger)
									SET_PED_RESET_FLAG(pedMugger, PRF_InstantBlendToAim, TRUE)
								ENDIF
								makeGoonsOpenFire()
								debug_stuff()
								checksToEndMission()
							BREAK
						ENDSWITCH
					//ENDIF
//				ELSE
//					missionCleanup()
//				ENDIF
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
