
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "ambience_run_checks.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "cutscene_public.sch"
USING "altruist_cult.sch"
USING "achievement_public.sch"
USING "rc_helper_functions.sch"
USING "minigame_big_message.sch"
USING "minigame_uiinputs.sch"
USING "minigames_helpers.sch"
USING "random_events_public.sch"

ENUM altruistStageFlag
	waitingForDelivery,
	playDropoffSyncscene,
	playAttackCutsceneAndSetupShootout,
	replayAttackCutsceneAndSetupShootout,
	cultShootout
ENDENUM
altruistStageFlag altruistStage = waitingForDelivery

//g_bThisPedHasBeenDeliveredToCult[CULT_HITCH_LIFT_2]
//CONST_INT CULT_HITCH_LIFT_2 0
//CONST_INT CULT_HITCH_LIFT_3 1
//CONST_INT CULT_HITCH_LIFT_4 2

CONST_INT MAX_NUM_CULT 16

VECTOR vCultSpawn[MAX_NUM_CULT]
FLOAT fCultHeading[MAX_NUM_CULT]

//VECTOR vDropWalkTo
VECTOR vCultMainCoords = << -1109.213, 4914.744, 216.101 >>
VECTOR vCultEnterence  = << -1034.6, 4918.6, 205.9 >>

VECTOR vCultArea1Coord1 = << -1066.963257, 4873.129883, 207.328064 >>
VECTOR vCultArea1Coord2 = << -1112.946289, 4986.957031, 241.133011 >>
FLOAT fCultArea1 = 72.312500
VECTOR vCultArea2Coord1 = << -1138.386475, 4872.039551, 207.548843 >>
VECTOR vCultArea2Coord2 = << -1091.958618, 4983.270020, 241.169846 >>
FLOAT fCultArea2 = 57.812500
VECTOR vCultArea3Coord1 = << -1171.647949,4888.657715,211.075638 >>
VECTOR vCultArea3Coord2 = << -1137.907227,4926.572266,240.999023 >>
FLOAT fCultArea3 = 17.937500
VECTOR vCultArea4Coord1 = << -1177.981445,4903.230957,212.476990 >>
VECTOR vCultArea4Coord2 = << -1153.397827,4902.631348,241.155029 >>
FLOAT fCultArea4 = 17.937500
VECTOR vCultArea5Coord1 = << -1175.693359,4904.838867,207.520660 >>
VECTOR vCultArea5Coord2 = << -1142.079712,4985.319824,241.288651 >>
FLOAT fCultArea5 = 36.375000
VECTOR vCultArea6Coord1 = << -1106.211914,4860.085938,206.120712 >>
VECTOR vCultArea6Coord2 = << -1105.015991,4873.854492,241.145294 >>
FLOAT fCultArea6 = 11.562500
VECTOR vCultArea7Coord1 = << -1044.268311,4916.586914,209.836487 >>
VECTOR vCultArea7Coord2 = << -1063.605347,4972.879883,241.129913 >>
FLOAT fCultArea7 = 12.875000

//BOOL bDoFullCleanUp
BOOL bReplay
BOOL bHasReplaySplashBeenSetup
BOOL bDropOffDone
BOOL bCultMembersAlive
BOOL bPlayedPassSound
BOOL bSquirrelSuit
BOOL bCultShardOut
//BOOL bWhimpering

BOOL bPlayerInCultArea
BOOL bGatesOpen
BOOL bPushInToFirstPerson
FLOAT fOpenRatioLeft
FLOAT fOpenRatioRight
FLOAT fEndSyncScenePhase
FLOAT fCloseDoorPhase
//BOOL bGatesAddedToSystem
//INT iLeftGate
//INT iRightGate

INT j
INT i
//INT index
INT sceneId
INT iDropOffCutsceneCase
INT iAttackCutsceneCase
//INT iCutsceneWaitTimer
INT iTimerDialogue
INT iNumberOfRequiredPeds = 4//4

PED_INDEX pedVictim[2]
PED_INDEX pedCultMember[MAX_NUM_CULT]
VEHICLE_INDEX vehPlayersTemp
OBJECT_INDEX objCash
OBJECT_INDEX objSawnoff

PICKUP_INDEX pickupBriefcase[4]
OBJECT_INDEX objCrate[6]

BLIP_INDEX blipCult
BLIP_INDEX blipCultMember[MAX_NUM_CULT]

//CAMERA_INDEX camCutsceneDest
CAMERA_INDEX camCutsceneOrigin
SEQUENCE_INDEX seq

COVERPOINT_INDEX coverPlayer
SCRIPT_SCALEFORM_BIG_MESSAGE bigMessageUI
MG_FAIL_FADE_EFFECT deathEffect
MG_FAIL_SPLASH splashStruct
SCALEFORM_INDEX sCultPassed

SCENARIO_BLOCKING_INDEX sbiBlockedArea = NULL

STRING sFailString = "REPLAY_TMG" //REPLAY_T
STRING sFailSubString = "CMN_TDIED"

STRING strGuard1
STRING strGuard2
STRING strTrv
STRING strVictim1
STRING strVictim2
STRING strCam

STRING strGreetDialogue
//STRING strLeaveDialogue

REL_GROUP_HASH rghCult
structPedsForConversation AltruistCultDialogueStruct 

#IF IS_DEBUG_BUILD
	TEXT_LABEL sPedNames[MAX_NUM_CULT]
#ENDIF
//---------------------------------------------------------------------------------------------

FUNC BOOL IS_PLAYER_IN_CULT_AREA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea1Coord1, vCultArea1Coord2, fCultArea1)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea2Coord1, vCultArea2Coord2, fCultArea2)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea3Coord1, vCultArea3Coord2, fCultArea3)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea4Coord1, vCultArea4Coord2, fCultArea4)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea5Coord1, vCultArea5Coord2, fCultArea5)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea6Coord1, vCultArea6Coord2, fCultArea6)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vCultArea7Coord1, vCultArea7Coord2, fCultArea7)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BULLET_IN_CULT_AREA()
	IF IS_BULLET_IN_ANGLED_AREA( vCultArea1Coord1, vCultArea1Coord2, fCultArea1)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea2Coord1, vCultArea2Coord2, fCultArea2)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea3Coord1, vCultArea3Coord2, fCultArea3)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea4Coord1, vCultArea4Coord2, fCultArea4)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea5Coord1, vCultArea5Coord2, fCultArea5)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea6Coord1, vCultArea6Coord2, fCultArea6)
	OR IS_BULLET_IN_ANGLED_AREA(vCultArea7Coord1, vCultArea7Coord2, fCultArea7)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC missionCleanup()
	PRINTLN("@@@@@@@@@@@@@ missionCleanup: altruist_cult @@@@@@@@@@@@@")
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
//	IF bDoFullCleanUp
	IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= iNumberOfRequiredPeds
	AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 1000
		IF bReplay
			//setup for escaping death
			SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
			MG_CLEANUP_FAIL_FADE_EFFECT()
			DISABLE_CELLPHONE(FALSE)
//				REMOVE_SCENARIO_BLOCKING_AREAS()
			PRINTLN("@@@@@@@@@@@@@ REMOVE_SCENARIO_BLOCKING_AREAS - PLAYER DIED DURING SHOOTOUT @@@@@@@@@@@@@")
//				RANDOM_EVENT_OVER(RE_CULTSHOOTOUT)
			altruistStage = replayAttackCutsceneAndSetupShootout
		ELSE
			CLEANUP_MG_BIG_MESSAGE(bigMessageUI)
			
			//setup for escaping death
			g_bBlockDeathFadeout = FALSE
			g_bBlockWastedTitle = FALSE
			g_bBlockUltraSloMoOnDeath = FALSE
			
			MG_DO_FAIL_EFFECT(FALSE, TRUE)
			MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)
			
			SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
			SET_FADE_OUT_AFTER_DEATH(TRUE)
			PAUSE_DEATH_ARREST_RESTART(FALSE)
			IGNORE_NEXT_RESTART(FALSE)
			
//				MG_RESET_FAIL_EFFECT_VARS()	
			DISABLE_CELLPHONE(FALSE)
			
			SET_WANTED_LEVEL_MULTIPLIER(1)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
			CLEAR_WEATHER_TYPE_PERSIST()
			bPlayerInCultArea = FALSE
			RESET_CULT_STATUS()
			g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = iNumberOfRequiredPeds -1
			PRINTLN("@@@@@@@@@@@@@ YOU HAVE DELIVERED: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " PEDS, AND YOU HAVE TO DELIVER: ", iNumberOfRequiredPeds, " PEDS @@@@@@@@@@@@@")
			TERMINATE_THIS_THREAD()
		ENDIF
	ELSE
		REMOVE_SCENARIO_BLOCKING_AREAS()
		PRINTLN("@@@@@@@@@@@@@ REMOVE_SCENARIO_BLOCKING_AREAS @@@@@@@@@@@@@")
//		ENDIF
		
		CLEANUP_MG_BIG_MESSAGE(bigMessageUI)
		
		//setup for escaping death
		g_bBlockDeathFadeout = FALSE
		g_bBlockWastedTitle = FALSE
		g_bBlockUltraSloMoOnDeath = FALSE
		
		MG_DO_FAIL_EFFECT(FALSE, TRUE)
		MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)
		
		SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
		SET_FADE_OUT_AFTER_DEATH(TRUE)
		PAUSE_DEATH_ARREST_RESTART(FALSE)
		IGNORE_NEXT_RESTART(FALSE)
		
//		MG_RESET_FAIL_EFFECT_VARS()	
		DISABLE_CELLPHONE(FALSE)
		
		IF HAS_SCALEFORM_MOVIE_LOADED(sCultPassed)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCultPassed)
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
		ENDIF
		SET_WANTED_LEVEL_MULTIPLIER(1)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		CLEAR_WEATHER_TYPE_PERSIST()
		bPlayerInCultArea = FALSE
//		#IF IS_DEBUG_BUILD
//			g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
//		#ENDIF
		PRINTLN("@@@@@@@@@@@@@ YOU HAVE DELIVERED: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " PEDS, AND YOU HAVE TO DELIVER: ", iNumberOfRequiredPeds, " PEDS @@@@@@@@@@@@@")
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

PROC MISSION_PASSED()
	PRINTLN("@@@@@@@@@@@@@@@ PASSED ALTRUIST CULT SHOOTOUT @@@@@@@@@@@@@@@")
	
	IF NOT bPlayedPassSound
		
		SET_STATIC_EMITTER_ENABLED("COUNTRYSIDE_ALTRUIST_CULT_01", FALSE)
		g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 1000
		UNLOCK_MISSION_NEWS_STORY(ENUM_TO_INT(NEWS_MISC_ALTRU))
		g_savedGlobals.sFinanceData.eCurrentEyeFindNewsStoryState = EYEFIND_NEWS_STORY_STATE_O_CULT
		CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " UPDATE_EYEFIND_CURRENT_NEWS_STORY_STATE() : updated EYEFIND_NEWS_STORY_STATE_O_CULT")
		
		sCultPassed = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
		WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(sCultPassed)
			WAIT(0)
		ENDWHILE
		BEGIN_SCALEFORM_MOVIE_METHOD(sCultPassed, "SHOW_SHARD_MIDSIZED_MESSAGE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CULT_PASS")
		END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		
		WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
			WAIT(0)
		ENDWHILE
//		#IF IS_DEBUG_BUILD
			RESET_CULT_STATUS()
	//		g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
//		#ENDIF
		SETTIMERA(0)
		
		PLAY_SOUND_FRONTEND(-1, "PROPERTY_PURCHASE", "HUD_AWARDS")
		bPlayedPassSound = TRUE
	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sCultPassed, 255, 255, 255, 255)
	
	IF NOT bCultShardOut
		IF TIMERA() > 9500
			BEGIN_SCALEFORM_MOVIE_METHOD(sCultPassed, "SHARD_ANIM_OUT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
			END_SCALEFORM_MOVIE_METHOD()
			bCultShardOut = TRUE
		ENDIF
	ENDIF
	
	IF TIMERA() > 10000
		IF HAS_SCALEFORM_MOVIE_LOADED(sCultPassed)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCultPassed)
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
		ENDIF
		
		WHILE GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vCultMainCoords) < 209
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			WAIT(0)
		ENDWHILE
		
		RANDOM_EVENT_OVER(RE_CULTSHOOTOUT)
		missionCleanup()
	ENDIF
ENDPROC

PROC openGates()
	IF fOpenRatioRight < 1
		fOpenRatioRight = fOpenRatioRight + (0.6 / (1/GET_FRAME_TIME()))//0.02
//		PRINTLN("@@@@@@ OPEN! fOpenRatioRight: ", fOpenRatioRight, "/ FPS: ", (1/GET_FRAME_TIME()), " @@@@@@")
	ELSE
		fOpenRatioRight = 1
	ENDIF
	IF fOpenRatioRight <= 1
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, fOpenRatioRight, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF
	
	IF fOpenRatioLeft > -1
		fOpenRatioLeft = fOpenRatioLeft - (0.6 / (1/GET_FRAME_TIME()))//0.02
//		PRINTLN("@@@@@@ OPEN! fOpenRatioLeft: ", fOpenRatioLeft, "/ FPS: ", (1/GET_FRAME_TIME()), " @@@@@@")
	ELSE
		fOpenRatioLeft = -1
	ENDIF
	IF fOpenRatioLeft >= -1
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, fOpenRatioLeft, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF
ENDPROC

PROC closeGates()
	IF fOpenRatioRight > 0
		fOpenRatioRight = fOpenRatioRight - (0.6 / (1/GET_FRAME_TIME()))//0.02
//		PRINTLN("@@@@@@ CLOSE! fOpenRatioRight: ", fOpenRatioRight, "/ FPS: ", (1/GET_FRAME_TIME()), " @@@@@@")
	ELSE
		fOpenRatioRight = 0
	ENDIF
	IF fOpenRatioRight >= 0
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, fOpenRatioRight, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF
	
	IF fOpenRatioLeft < 0
		fOpenRatioLeft = fOpenRatioLeft + (0.6 / (1/GET_FRAME_TIME()))//0.02
//		PRINTLN("@@@@@@ CLOSE! fOpenRatioLeft: ", fOpenRatioLeft, "/ FPS: ", (1/GET_FRAME_TIME()), " @@@@@@")
	ELSE
		fOpenRatioLeft = 0
	ENDIF
	IF fOpenRatioLeft <= 0
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, fOpenRatioLeft, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF
ENDPROC

PROC handlePlayerInCultArea()

	IF IS_CULT_FINISHED()
	#IF IS_DEBUG_BUILD AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 10 #ENDIF //This is the number debug menu sets when starting the shootout
		
		PRINTLN("@@@@@@@@@@@ YOU HAVE ALREADY FINISHED THE ALTRUIST CULT @@@@@@@@@@@")
		IF sbiBlockedArea = NULL
			sbiBlockedArea = ADD_SCENARIO_BLOCKING_AREA(vCultMainCoords - << 80, 60, 100 >>, vCultMainCoords + << 80, 60, 100 >>)
		ENDIF
		WHILE GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vCultMainCoords) < 209
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER) != ACQUAINTANCE_TYPE_PED_HATE
			IF IS_PLAYER_IN_CULT_AREA()
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
				PRINTLN("@@@@@@@@@@@@@@@@@@ CULT HATES MIKE OR FRANKLIN: IS_PLAYER_IN_CULT_AREA() @@@@@@@@@@@@@@@@@@")
			ELIF IS_BULLET_IN_CULT_AREA()
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
				PRINTLN("@@@@@@@@@@@@@@@@@@ CULT HATES MIKE OR FRANKLIN: IS_BULLET_IN_CULT_AREA() @@@@@@@@@@@@@@@@@@")
			ENDIF
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER) != ACQUAINTANCE_TYPE_PED_HATE
			IF IS_PLAYER_IN_CULT_AREA()
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
				OR (IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()) AND IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID()))
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
					PRINTLN("@@@@@@@@@@@@@@@@@@ CULT HATES TREVOR: IS_PLAYER_IN_CULT_AREA() @@@@@@@@@@@@@@@@@@")
				ENDIF
			ELIF IS_BULLET_IN_CULT_AREA()
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
				PRINTLN("@@@@@@@@@@@@@@@@@@ CULT HATES TREVOR: IS_BULLET_IN_CULT_AREA() @@@@@@@@@@@@@@@@@@")
			ELIF GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER) != ACQUAINTANCE_TYPE_PED_LIKE
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, RELGROUPHASH_PLAYER)
				PRINTLN("@@@@@@@@@@@@@@@@@@ CULT LIKES TREVOR @@@@@@@@@@@@@@@@@@")
			ENDIF
		ENDIF
	ENDIF
	
//	IF bGatesAddedToSystem 
		IF IS_PLAYER_IN_CULT_AREA()
			IF bPlayerInCultArea
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 3)
//					IF NOT AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
						TRIGGER_MUSIC_EVENT("AC_START")
//					ENDIF
				ENDIF
				SET_WANTED_LEVEL_MULTIPLIER(0)
				SET_STATIC_EMITTER_ENABLED("COUNTRYSIDE_ALTRUIST_CULT_01", TRUE)
				PRINTLN("@@@@@@@@@@@@@@@@@@ ALTRUIST GATES OPEN AND SOUND EMITTING @@@@@@@@@@@@@@@@@@")
				// Open
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, FALSE, 0)//, 1)//-1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].openRate)
				ENDIF
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, FALSE, 0)//, -1)//1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].openRate)
				ENDIF
				bGatesOpen = TRUE
//				DOOR_SYSTEM_SET_OPEN_RATIO(iLeftGate, 0, FALSE, TRUE)
//				DOOR_SYSTEM_SET_OPEN_RATIO(iRightGate, 0, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_UNLOCKED, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_UNLOCKED, FALSE, TRUE)
//				FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_LEFT, FALSE)
//				FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_RIGHT, FALSE)
				bPlayerInCultArea = FALSE
			ENDIF
		ELSE
			IF NOT bPlayerInCultArea
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					TRIGGER_MUSIC_EVENT("AC_END")
				ENDIF
				SET_WANTED_LEVEL_MULTIPLIER(1)
				SET_STATIC_EMITTER_ENABLED("COUNTRYSIDE_ALTRUIST_CULT_01", FALSE)
				PRINTLN("@@@@@@@@@@@@@@@@@@ ALTRUIST GATES LOCKED AND NO SOUND EMITTING @@@@@@@@@@@@@@@@@@")
				// Lock
				IF NOT bGatesOpen
					closeGates()
				ENDIF
//				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, TRUE, 0)//, 1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].openRate)
//				ENDIF
//				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, TRUE, 0)//, -1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].openRate)
//				ENDIF
//				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, 0, FALSE, TRUE)
//				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, 0, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_CLOSED_THIS_FRAME, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_CLOSED_THIS_FRAME, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				bPlayerInCultArea = TRUE
			ENDIF
		ENDIF
//	ENDIF
ENDPROC

PROC checkIfPlayerIsDeliveringPedsToCult()

	IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
//		PRINTLN("@@@@@@@@@@@@@@@@@@ ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT @@@@@@@@@@@@@@@@@@")
//		g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 3
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
				GET_GROUP_SIZE(PLAYER_GROUP_ID(), j, i)
				IF i = 1
					pedVictim[0] = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
					IF IS_ENTITY_AT_COORD(pedVictim[0], vCultEnterence , << 5, 5, 5 >>)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
								BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 5)
							ENDIF
						ENDIF
						SETTIMERA(0)
						altruistStage = playDropoffSyncscene
					ENDIF
				ELIF i = 2
					pedVictim[0] = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
					pedVictim[1] = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
					IF IS_ENTITY_AT_COORD(pedVictim[0], vCultEnterence , << 5, 5, 5 >>)
					AND IS_ENTITY_AT_COORD(pedVictim[1], vCultEnterence , << 5, 5, 5 >>)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
								BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 5)
							ENDIF
						ENDIF
						SETTIMERA(0)
						altruistStage = playDropoffSyncscene
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC dropOffOnePedAtCult()
	
//	IF iDropOffCutsceneCase <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			PRINTLN("@@@@@@@@@@@ IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY @@@@@@@@@@@")
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutsceneCase = 4
//		ENDIF
//	ENDIF
	
	SWITCH iDropOffCutsceneCase
		//Setup
		CASE 0
			REQUEST_MODEL(A_M_O_ACULT_02)
			REQUEST_MODEL(PROP_ANIM_CASH_PILE_02)
			REQUEST_ANIM_DICT("random@altruist_cult")
			PREPARE_MUSIC_EVENT("AC_DELIVERED")
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
			IF HAS_MODEL_LOADED(A_M_O_ACULT_02)
			AND HAS_MODEL_LOADED(PROP_ANIM_CASH_PILE_02)
			AND HAS_ANIM_DICT_LOADED("random@altruist_cult")
			AND PREPARE_MUSIC_EVENT("AC_DELIVERED")
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
					strGreetDialogue = "ACULT_HI"
//					strLeaveDialogue = "ACULT_CHANT"
					strGuard1 = "cult_p2_guard"
					strTrv = "cult_p2_trv"
					strVictim1 = "cult_p2_victim"
					strCam = "cult_p2_cam"
					fCloseDoorPhase = 0.80
					fEndSyncScenePhase = 0.968
				ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 1
					strGreetDialogue = "ACULT_LEAVE3"//"ACULT_LEAVE2"
//					strLeaveDialogue = "ACULT_CHANT"
					strGuard1 = "cult_p4_guard"
					strTrv = "cult_p4_trv"
					strVictim1 = "cult_p4_victim"
					strCam = "cult_p4_cam"
					fCloseDoorPhase = 0.77
					fEndSyncScenePhase = 0.999
				ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 2
					strGreetDialogue = "ACULT_LEAVE3"
//					strLeaveDialogue = "ACULT_CHANT"
					strGuard1 = "cult_p4_guard"
					strTrv = "cult_p4_trv"
					strVictim1 = "cult_p4_victim"
					strCam = "cult_p4_cam"
					fCloseDoorPhase = 0.77
					fEndSyncScenePhase = 0.999
				ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 3
					strGreetDialogue = "ACULT_CAPT" //"ACULT_LAST"
//					strLeaveDialogue = "ACULT_CHANT"
					strGuard1 = "cult_p7_guard_01"
					strGuard2 = "cult_p7_guard_02"
					strTrv = "cult_p7_trv"
					strVictim1 = "cult_p7_victim_01"
					strCam = "cult_p7_cam"
					fCloseDoorPhase = 0.98
					fEndSyncScenePhase = 0.98
					REQUEST_MODEL(PROP_BOX_WOOD03A)
					REQUEST_MODEL(PROP_BOX_WOOD04A)
					REQUEST_MODEL(PROP_BOX_WOOD05A)
					REQUEST_MODEL(Prop_Security_case_01)
//					WHILE NOT HAS_MODEL_LOADED(prop_box_wood03a)
//					OR NOT HAS_MODEL_LOADED(prop_box_wood04a)
//					OR NOT HAS_MODEL_LOADED(prop_box_wood05a)
//						WAIT(0)
//					ENDWHILE
					IF NOT IS_CUTSCENE_ACTIVE()
						REQUEST_CUTSCENE("ac_ig_3_p3_b")
					ENDIF
				ENDIF
	//			vDropWalkTo = << -1042.6897, 4911.0391, 207.3255 >>
				
				IF DOES_BLIP_EXIST(blipCult)
					REMOVE_BLIP(blipCult)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_HELP(TRUE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				TRIGGER_MUSIC_EVENT("AC_DELIVERED")
				
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					WAIT(0)
	//			ENDWHILE
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehPlayersTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ vehPlayersTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) 1 @@@@@@@@@@@@@@@@@@@@@")
					IF IS_VEHICLE_DRIVEABLE(vehPlayersTemp)
						IF IS_VEHICLE_STOPPED(vehPlayersTemp)
						
	//						SET_ENTITY_COORDS(vehPlayersTemp, GET_PLAYER_COORDS(PLAYER_ID()) + << 3, 3, -1 >>)
	//						SET_ENTITY_HEADING(vehPlayersTemp, 128.2065)
	//						SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
	//						
	//						IF NOT IS_PED_INJURED(pedVictim[0])
	//							CLEAR_PED_TASKS(pedVictim[0])
	//							REMOVE_PED_FROM_GROUP(pedVictim[0])
	//							SET_ENTITY_COORDS(pedVictim[0], << -1026.1884, 4913.4600, 205.9055 >>)
	//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedVictim[0], vehPlayersTemp)
	//								SET_PED_INTO_VEHICLE(pedVictim[0], vehPlayersTemp, VS_FRONT_RIGHT)
	//							ENDIF
	//						ENDIF
		//					IF NOT IS_PED_INJURED(pedVictim[1])
		//						CLEAR_PED_TASKS(pedVictim[1])
		//						REMOVE_PED_FROM_GROUP(pedVictim[1])
		//						IF NOT IS_PED_SITTING_IN_VEHICLE(pedVictim[1], vehPlayersTemp)
		//							SET_PED_INTO_VEHICLE(pedVictim[1], vehPlayersTemp, VS_BACK_LEFT)
		//						ENDIF
		//					ENDIF
//							CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
							iDropOffCutsceneCase ++
						ENDIF
					ENDIF
				ELSE
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ PLAYER IS NOT IN A VEHICLE?!?!?!?! 1 @@@@@@@@@@@@@@@@@@@@@")
					vehPlayersTemp = GET_PLAYERS_LAST_VEHICLE()
					IF NOT IS_PED_INJURED(pedVictim[0])
						CLEAR_PED_TASKS(pedVictim[0])
						REMOVE_PED_FROM_GROUP(pedVictim[0])
					ENDIF
//					CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
					iDropOffCutsceneCase ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
//			camCutsceneOrigin = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//			camCutsceneDest = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			// Open
//			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, FALSE, -0.5)
//			ENDIF
//			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, FALSE, 0.5)
//			ENDIF
//			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
//			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_LEFT, TRUE)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_RIGHT, TRUE)
//			IF IS_VEHICLE_DRIVEABLE(vehPlayersTemp)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehPlayersTemp, << -0.5345, 2.5675, 0.7588 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehPlayersTemp, << -0.4852, -0.4319, 0.7138 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 21.9541)
//				
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehPlayersTemp, << 1.1031, 2.3050, 0.8525 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehPlayersTemp, << -0.0567, -0.4556, 0.6686 >>)
//				SET_CAM_FOV(camCutsceneDest, 21.9541)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 6000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				SET_ENTITY_COORDS(vehPlayersTemp, GET_PLAYER_COORDS(PLAYER_ID()) + << 3, 3, -1 >>)
//				SET_ENTITY_HEADING(vehPlayersTemp, 128.2065)
//				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
//			ELSE
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.5345, 5.5675, 0.7588 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.4852, -0.4319, 0.7138 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 21.9541)
//				
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, PLAYER_PED_ID(), << 1.1031, 5.3050, 0.8525 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, PLAYER_PED_ID(), << -0.0567, -0.4556, 0.6686 >>)
//				SET_CAM_FOV(camCutsceneDest, 21.9541)
//				camCutsceneOrigin = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1029.4614, 4917.6128, 205.6051 >>, << 19.2293, 0.0000, 115.6820 >>, 50, TRUE)
//				camCutsceneDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1030.4614, 4918.6128, 205.6051 >>, << 19.2293, 0.0000, 115.6820 >>, 45, TRUE)
//				
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 6000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				
//			ENDIF
			
//			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehPlayersTemp)
//					vehPlayersTemp = GET_PLAYERS_LAST_VEHICLE()
					IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayersTemp)
						PRINTLN("@@@@@@@@@@@@@@@@@@@@@ IS_ENTITY_A_MISSION_ENTITY 1 @@@@@@@@@@@@@@@@@@@@@")
						SET_ENTITY_AS_MISSION_ENTITY(vehPlayersTemp)
					ENDIF
					IF IS_ENTITY_AT_COORD(vehPlayersTemp, vCultEnterence, << 13, 13, 13 >>)
						SET_ENTITY_COORDS(vehPlayersTemp, << -1028.4147, 4924.8628, 205.9386 >>, FALSE)//(GET_PLAYERS_LAST_VEHICLE(), GET_PLAYER_COORDS(PLAYER_ID()) + << 9, 9, 0 >>)
						IF NOT IS_ENTITY_DEAD(vehPlayersTemp)
							SET_ENTITY_HEADING(vehPlayersTemp, 149.8882)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ NOT! DOES_ENTITY_EXIST(vehPlayersTemp) 1 @@@@@@@@@@@@@@@@@@@@@")
				ENDIF
//			ENDIF
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@ CLEAR_AREA 1 @@@@@@@@@@@@@@@@@@@@@")
			CLEAR_AREA(vCultEnterence, 12, TRUE)
						
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1035.6248, 4915.1523, 205.5761 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 128.2065)
			IF NOT IS_PED_INJURED(pedVictim[0])
				SET_ENTITY_COORDS(pedVictim[0], << -1035.6248, 4915.1523, 205.5761 >> - << 1, 0, 0 >>)
				SET_ENTITY_HEADING(pedVictim[0], 128.2065)
			ENDIF
			
			pedCultMember[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02, << -1042.5597, 4913.5161, 207.0988 >>, 282.4169)
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 1, pedCultMember[0], "ACULTMember1")
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 3
				pedCultMember[1] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02, << -1041.7574, 4908.4897, 207.2777 >>, 305.3168)
				ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 2, pedCultMember[1], "ACULTMember2")
			ENDIF
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 3, NULL, "ACULTMaster")
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 4, NULL, "ACULTGroup")
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 1
			AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 2
				CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			SETTIMERB(0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[0], TRUE)
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[1], TRUE)
			objCash = CREATE_OBJECT(PROP_ANIM_CASH_PILE_02, vCultMainCoords)
			objSawnoff = CREATE_WEAPON_OBJECT(WEAPONTYPE_ASSAULTRIFLE, -1, GET_ENTITY_COORDS(pedCultMember[0]), TRUE)
//			ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[0], GET_PED_BONE_INDEX(pedCultMember[0], BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
//			GIVE_WEAPON_TO_PED(pedCultMember[0], WEAPONTYPE_SAWNOFFSHOTGUN/*WEAPONTYPE_HAMMER*/, -1, TRUE)
//			GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
			ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[0], GET_PED_BONE_INDEX(pedCultMember[0], BONETAG_PH_L_HAND), << 0.120, 0.028, -0.003 >>, << -82.208, -7.060, -8.038 >>)
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds < 3
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
					ATTACH_ENTITY_TO_ENTITY(objCash, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
				ELSE
					ATTACH_ENTITY_TO_ENTITY(objCash, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
				ENDIF
//				GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
			ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 3
				GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_ASSAULTRIFLE/*WEAPONTYPE_HAMMER*/, -1, TRUE)
//				ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[1], GET_PED_BONE_INDEX(pedCultMember[1], BONETAG_PH_L_HAND), << 0.120, 0.028, -0.003 >>, << -82.208, -7.060, -8.038 >>)
			ENDIF
			
			camCutsceneOrigin = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
			sceneId = CREATE_SYNCHRONIZED_SCENE(<< -1044.636, 4915.204, 212.320 >>, << 0.000, 0.000, 110.520 >>)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@altruist_cult", strTrv, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			TASK_SYNCHRONIZED_SCENE(pedCultMember[0], sceneId, "random@altruist_cult", strGuard1, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 3
				TASK_SYNCHRONIZED_SCENE(pedCultMember[1], sceneId, "random@altruist_cult", strGuard2, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			ENDIF
			PLAY_SYNCHRONIZED_CAM_ANIM(camCutsceneOrigin, sceneId, strCam, "random@altruist_cult")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF NOT IS_PED_INJURED(pedVictim[0])
				TASK_SYNCHRONIZED_SCENE(pedVictim[0], sceneId, "random@altruist_cult", strVictim1, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				
//				OPEN_SEQUENCE_TASK(Seq)
//					TASK_LOOK_AT_ENTITY(NULL, pedVictim[0], -1)
//					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), pedVictim[0], PEDMOVE_WALK, FALSE, 3)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim[0], -1)
//				CLOSE_SEQUENCE_TASK(Seq)
//				TASK_PERFORM_SEQUENCE(pedCultMember[0], Seq)
//				CLEAR_SEQUENCE_TASK(Seq)
				
//				OPEN_SEQUENCE_TASK(Seq)
//					TASK_LOOK_AT_ENTITY(NULL, pedVictim[0], -1)
//					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, pedVictim[0], pedVictim[0], PEDMOVE_WALK, FALSE, 1.7)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim[0], -1)
//				CLOSE_SEQUENCE_TASK(Seq)
//				TASK_PERFORM_SEQUENCE(pedCultMember[1], Seq)
//				CLEAR_SEQUENCE_TASK(Seq)
			ENDIF
			
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
			iDropOffCutsceneCase ++
		BREAK
		
		CASE 2
//			IF NOT bWhimpering
				IF NOT IS_PED_INJURED(pedVictim[0])
					IF IS_PED_MALE(pedVictim[0])
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
					ENDIF
//					bWhimpering = TRUE
				ENDIF
//			ENDIF
			IF TIMERB() > 2000 AND TIMERB() < 2100
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 1
				OR g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
			ENDIF
//			IF(iCutsceneWaitTimer - 2000) < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedVictim[1])
//					IF IS_PED_IN_ANY_VEHICLE(pedVictim[1])
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_LEAVE_ANY_VEHICLE(NULL)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
////							TASK_PLAY_ANIM(NULL, "gestures@niko", "hello", SLOW_BLEND_IN, SLOW_BLEND_OUT,  -1, AF_SECONDARY| AF_UPPERBODY)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(pedVictim[1], seq)
//						CLEAR_SEQUENCE_TASK(seq)
//					ENDIF
//				ENDIF
//			ENDIF
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1033.3081, 4908.4478, 207.1179 >>, << 8.3588, 0.0000, 49.3638 >>, 50, TRUE)
//				camCutsceneDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1034.3081, 4909.4478, 207.1179 >>, << 8.3588, 0.0000, 49.3638 >>, 45, TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehPlayersTemp, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehPlayersTemp, << 0.1969, -0.7989, 0.3845 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 24.2221)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehPlayersTemp, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehPlayersTemp, << 0.1319, -0.6669, 0.3870 >>)
//				SET_CAM_FOV(camCutsceneDest, 24.2221)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 12000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > fCloseDoorPhase
//					CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strLeaveDialogue, CONV_PRIORITY_AMBIENT_HIGH)
					
	//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 4500)
					iDropOffCutsceneCase ++
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF (iCutsceneWaitTimer - 500) < GET_GAME_TIMER()
//				CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_FLW_1", CONV_PRIORITY_AMBIENT_HIGH)
//				IF NOT bWhimpering
					IF NOT IS_PED_INJURED(pedVictim[0])
						IF IS_PED_MALE(pedVictim[0])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
						ELSE
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
						ENDIF
//						bWhimpering = TRUE
					ENDIF
//				ENDIF
//						IF IS_PED_IN_ANY_VEHICLE(pedVictim[0])
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_LEAVE_ANY_VEHICLE(NULL)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedVictim[0], seq)
//							CLEAR_SEQUENCE_TASK(seq)
							IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 1
							OR g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 2
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.974285721632651
								AND NOT bPushInToFirstPerson
								AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
									ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
									PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
									bPushInToFirstPerson = TRUE
								ENDIF
							ENDIF
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > fEndSyncScenePhase
//								iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
								iDropOffCutsceneCase ++
							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
		CASE 4
//			IF (iCutsceneWaitTimer) < GET_GAME_TIMER()
				
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds < 3
					IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 1
					AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 2
						REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT()
						PRINTLN("@@@@@@@@@@@ REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT 1 @@@@@@@@@@@@")
						IF DOES_ENTITY_EXIST(pedCultMember[0])
							DELETE_PED(pedCultMember[0])
						ENDIF
						IF DOES_ENTITY_EXIST(pedCultMember[1])
							DELETE_PED(pedCultMember[1])
						ENDIF
					ELSE
						SETTIMERB(0)
						IF DOES_ENTITY_EXIST(pedCultMember[0])
							SET_ENTITY_VISIBLE(pedCultMember[0], FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(pedCultMember[1])
							SET_ENTITY_VISIBLE(pedCultMember[1], FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(pedVictim[0])
							SET_ENTITY_VISIBLE(pedVictim[0], FALSE)
						ENDIF
						PRINTLN("@@@@@@@@@@@ SHOULD WAIT TO CLEAN UP @@@@@@@@@@@@")
						
						iDropOffCutsceneCase ++
					ENDIF
					IF DOES_ENTITY_EXIST(objCash)
						DELETE_OBJECT(objCash)
					ENDIF
					IF DOES_ENTITY_EXIST(objSawnoff)
						DELETE_OBJECT(objSawnoff)
					ENDIF
					// Lock
//					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, TRUE, 0)//, 1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].openRate)
//					ENDIF
//					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, TRUE, 0)//, -1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].openRate)
//					ENDIF
//					DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
//					DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1033.8966, 4914.2837, 205.4544>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 317.0894)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 750)
					ENDIF
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
//						SET_CAM_ACTIVE(camCutsceneDest, FALSE)
					IF ARE_STRINGS_EQUAL(strCam, "cult_p4_cam")
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ELSE
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					ENDIF
					SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1000)
				ENDIF
				
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 1
				AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 2
					TRIGGER_MUSIC_EVENT("AC_DONE")
					
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds BEFORE: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
					g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds ++
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds AFTER: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
					
					bDropOffDone = TRUE
				ENDIF
//			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR TIMERB() > 7000
				REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT()
				PRINTLN("@@@@@@@@@@@ REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT 1 @@@@@@@@@@@@")
				IF DOES_ENTITY_EXIST(pedCultMember[0])
					DELETE_PED(pedCultMember[0])
				ENDIF
				IF DOES_ENTITY_EXIST(pedCultMember[1])
					DELETE_PED(pedCultMember[1])
				ENDIF
				TRIGGER_MUSIC_EVENT("AC_DONE")
				
				PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds BEFORE: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
				g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds ++
				PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds AFTER: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
				
				bDropOffDone = TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC dropOffTwoPedsAtCult()
	
//	IF iDropOffCutsceneCase <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			PRINTLN("@@@@@@@@@@@ IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY @@@@@@@@@@@")
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutsceneCase = 4
//		ENDIF
//	ENDIF
	
	SWITCH iDropOffCutsceneCase
		//Setup
		CASE 0
			REQUEST_MODEL(A_M_O_ACULT_02)
			REQUEST_MODEL(PROP_ANIM_CASH_PILE_02)
			REQUEST_ANIM_DICT("random@altruist_cult")
			PREPARE_MUSIC_EVENT("AC_DELIVERED")
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
			IF HAS_MODEL_LOADED(A_M_O_ACULT_02)
			AND HAS_MODEL_LOADED(PROP_ANIM_CASH_PILE_02)
			AND HAS_ANIM_DICT_LOADED("random@altruist_cult")
			AND PREPARE_MUSIC_EVENT("AC_DELIVERED")
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
			
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds < 2
					IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
						strGreetDialogue = "ACULT_HI"
//						strLeaveDialogue = "ACULT_CHANT"
					ELSE
						strGreetDialogue = "ACULT_LEAVE5"//"ACULT_LEAVE4"
//						strLeaveDialogue = "ACULT_LEAVE5"
					ENDIF
					strGuard1 = "cult_p5_guard"
					strTrv = "cult_p5_trv"
					strVictim1 = "cult_p5_victim_01"
					strVictim2 = "cult_p5_victim_02"
					strCam = "cult_p5_cam"
					fCloseDoorPhase = 0.75
					fEndSyncScenePhase = 0.999
				ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 2
					strGreetDialogue = "ACULT_CAPT"
//					strLeaveDialogue = "ACULT_CHANT"
					strGuard1 = "cult_p7_guard_01"
					strGuard2 = "cult_p7_guard_02"
					strTrv = "cult_p7_trv"
					strVictim1 = "cult_p7_victim_01"
					strVictim2 = "cult_p7_victim_02"
					strCam = "cult_p7_cam"
					fCloseDoorPhase = 0.98
					fEndSyncScenePhase = 0.98
					REQUEST_MODEL(PROP_BOX_WOOD03A)
					REQUEST_MODEL(PROP_BOX_WOOD04A)
					REQUEST_MODEL(PROP_BOX_WOOD05A)
					REQUEST_MODEL(Prop_Security_case_01)
//					WHILE NOT HAS_MODEL_LOADED(prop_box_wood03a)
//					OR NOT HAS_MODEL_LOADED(prop_box_wood04a)
//					OR NOT HAS_MODEL_LOADED(prop_box_wood05a)
//						WAIT(0)
//					ENDWHILE
					IF NOT IS_CUTSCENE_ACTIVE()
						REQUEST_CUTSCENE("ac_ig_3_p3_b")
					ENDIF
				ENDIF
	//			vDropWalkTo = << -1042.6897, 4911.0391, 207.3255 >>
				
				IF DOES_BLIP_EXIST(blipCult)
					REMOVE_BLIP(blipCult)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_HELP(TRUE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				TRIGGER_MUSIC_EVENT("AC_DELIVERED")
								
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					WAIT(0)
	//			ENDWHILE
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehPlayersTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ vehPlayersTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) 2 @@@@@@@@@@@@@@@@@@@@@")
					IF IS_VEHICLE_DRIVEABLE(vehPlayersTemp)
						IF IS_VEHICLE_STOPPED(vehPlayersTemp)
						
	//						SET_ENTITY_COORDS(vehPlayersTemp, GET_PLAYER_COORDS(PLAYER_ID()) + << 3, 3, -1 >>)
	//						SET_ENTITY_HEADING(vehPlayersTemp, 128.2065)
	//						SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
	//						
	//						IF NOT IS_PED_INJURED(pedVictim[0])
	//							CLEAR_PED_TASKS(pedVictim[0])
	//							REMOVE_PED_FROM_GROUP(pedVictim[0])
	//							SET_ENTITY_COORDS(pedVictim[0], << -1026.1884, 4913.4600, 205.9055 >>)
	//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedVictim[0], vehPlayersTemp)
	//								SET_PED_INTO_VEHICLE(pedVictim[0], vehPlayersTemp, VS_BACK_LEFT)
	//							ENDIF
	//						ENDIF
	//						IF NOT IS_PED_INJURED(pedVictim[1])
	//							CLEAR_PED_TASKS(pedVictim[1])
	//							REMOVE_PED_FROM_GROUP(pedVictim[1])
	//							SET_ENTITY_COORDS(pedVictim[1], << -1025.1884, 4913.4600, 205.9055 >>)
	//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedVictim[1], vehPlayersTemp)
	//								SET_PED_INTO_VEHICLE(pedVictim[1], vehPlayersTemp, VS_BACK_RIGHT)
	//							ENDIF
	//						ENDIF
//							CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
							iDropOffCutsceneCase ++
						ENDIF
					ENDIF
				ELSE
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ PLAYER IS NOT IN A VEHICLE?!?!?!?! 2 @@@@@@@@@@@@@@@@@@@@@")
					vehPlayersTemp = GET_PLAYERS_LAST_VEHICLE()
					IF NOT IS_PED_INJURED(pedVictim[0])
					AND NOT IS_PED_INJURED(pedVictim[1])
						CLEAR_PED_TASKS(pedVictim[0])
						REMOVE_PED_FROM_GROUP(pedVictim[0])
						CLEAR_PED_TASKS(pedVictim[1])
						REMOVE_PED_FROM_GROUP(pedVictim[1])
					ENDIF
//					CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
					iDropOffCutsceneCase ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
//			camCutsceneOrigin = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//			camCutsceneDest = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			// Open
//			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, FALSE, -0.5)
//			ENDIF
//			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, FALSE, 0.5)
//			ENDIF
//			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
//			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_LEFT, TRUE)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_RIGHT, TRUE)
//			IF IS_VEHICLE_DRIVEABLE(vehPlayersTemp)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehPlayersTemp, << -0.5345, 2.5675, 0.7588 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehPlayersTemp, << -0.4852, -0.4319, 0.7138 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 21.9541)
//				
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehPlayersTemp, << 1.1031, 2.3050, 0.8525 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehPlayersTemp, << -0.0567, -0.4556, 0.6686 >>)
//				SET_CAM_FOV(camCutsceneDest, 21.9541)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 6000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				SET_ENTITY_COORDS(vehPlayersTemp, GET_PLAYER_COORDS(PLAYER_ID()) + << 3, 3, -1 >>)
//				SET_ENTITY_HEADING(vehPlayersTemp, 128.2065)
//				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
//			ELSE
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.5345, 5.5675, 0.7588 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.4852, -0.4319, 0.7138 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 21.9541)
//				
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, PLAYER_PED_ID(), << 1.1031, 5.3050, 0.8525 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, PLAYER_PED_ID(), << -0.0567, -0.4556, 0.6686 >>)
//				SET_CAM_FOV(camCutsceneDest, 21.9541)
//				camCutsceneOrigin = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1029.4614, 4917.6128, 205.6051 >>, << 19.2293, 0.0000, 115.6820 >>, 50, TRUE)
//				camCutsceneDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1030.4614, 4918.6128, 205.6051 >>, << 19.2293, 0.0000, 115.6820 >>, 45, TRUE)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 6000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
//			ENDIF
			
//			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehPlayersTemp)
//					vehPlayersTemp = GET_PLAYERS_LAST_VEHICLE()
					IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayersTemp)
						PRINTLN("@@@@@@@@@@@@@@@@@@@@@ IS_ENTITY_A_MISSION_ENTITY 2 @@@@@@@@@@@@@@@@@@@@@")
						SET_ENTITY_AS_MISSION_ENTITY(vehPlayersTemp)
					ENDIF
					IF IS_ENTITY_AT_COORD(vehPlayersTemp, vCultEnterence, << 13, 13, 13 >>)
						SET_ENTITY_COORDS(vehPlayersTemp, << -1028.4147, 4924.8628, 205.9386 >>, FALSE)//(GET_PLAYERS_LAST_VEHICLE(), GET_PLAYER_COORDS(PLAYER_ID()) + << 9, 9, 0 >>)
						IF NOT IS_ENTITY_DEAD(vehPlayersTemp)
							SET_ENTITY_HEADING(vehPlayersTemp, 149.8882)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayersTemp)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@ NOT! DOES_ENTITY_EXIST(vehPlayersTemp) 2 @@@@@@@@@@@@@@@@@@@@@")
				ENDIF
//			ENDIF
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@ CLEAR_AREA 2 @@@@@@@@@@@@@@@@@@@@@")
			CLEAR_AREA(vCultEnterence, 12, TRUE)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1035.6248, 4915.1523, 205.5761 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 128.2065)
			
			IF NOT IS_PED_INJURED(pedVictim[0])
			AND NOT IS_PED_INJURED(pedVictim[1])
				SET_ENTITY_COORDS(pedVictim[0], << -1035.6248, 4915.1523, 205.5761 >> - << 1, 0, 0 >>)
				SET_ENTITY_HEADING(pedVictim[0], 128.2065)
				SET_ENTITY_COORDS(pedVictim[1], << -1035.6248, 4915.1523, 205.5761 >> - << 0, 1, 0 >>)
				SET_ENTITY_HEADING(pedVictim[1], 128.2065)
			ENDIF
			
			pedCultMember[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02, << -1042.5597, 4913.5161, 207.0988 >>, 282.4169)
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 1, pedCultMember[0], "ACULTMember1")
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 2
				pedCultMember[1] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02, << -1041.7574, 4908.4897, 207.2777 >>, 305.3168)
				ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 2, pedCultMember[1], "ACULTMember2")
			ENDIF
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 3, NULL, "ACULTMaster")
			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 4, NULL, "ACULTGroup")
			CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strGreetDialogue, CONV_PRIORITY_AMBIENT_HIGH)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[0], TRUE)
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[1], TRUE)
//			OBJECT_INDEX objSawnoff2
			objSawnoff = CREATE_WEAPON_OBJECT(WEAPONTYPE_ASSAULTRIFLE, -1, GET_ENTITY_COORDS(pedCultMember[0]), TRUE)
//			ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[0], GET_PED_BONE_INDEX(pedCultMember[0], BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
//			objSawnoff2 = CREATE_WEAPON_OBJECT(WEAPONTYPE_SAWNOFFSHOTGUN, -1, GET_ENTITY_COORDS(pedCultMember[1]), TRUE)
//			ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[1], GET_PED_BONE_INDEX(pedCultMember[0], BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
//			GIVE_WEAPON_TO_PED(pedCultMember[0], WEAPONTYPE_SAWNOFFSHOTGUN/*WEAPONTYPE_HAMMER*/, -1, TRUE)
//			GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
			ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[0], GET_PED_BONE_INDEX(pedCultMember[0], BONETAG_PH_L_HAND), << 0.120, 0.028, -0.003 >>, << -82.208, -7.060, -8.038 >>)
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds < 2
				objCash = CREATE_OBJECT(PROP_ANIM_CASH_PILE_02, vCultMainCoords)
				ATTACH_ENTITY_TO_ENTITY(objCash, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
//				GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
			ELIF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 2
				GIVE_WEAPON_TO_PED(pedCultMember[1], WEAPONTYPE_ASSAULTRIFLE/*WEAPONTYPE_HAMMER*/, -1, TRUE)
//				ATTACH_ENTITY_TO_ENTITY(objSawnoff, pedCultMember[1], GET_PED_BONE_INDEX(pedCultMember[1], BONETAG_PH_L_HAND), << 0.120, 0.028, -0.003 >>, << -82.208, -7.060, -8.038 >>)
//				ATTACH_ENTITY_TO_ENTITY_PHYSICALLY
			ENDIF
			
			camCutsceneOrigin = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
			sceneId = CREATE_SYNCHRONIZED_SCENE(<< -1044.636, 4915.204, 212.320 >>, << 0.000, 0.000, 110.520 >>)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@altruist_cult", strTrv, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			TASK_SYNCHRONIZED_SCENE(pedCultMember[0], sceneId, "random@altruist_cult", strGuard1, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= 2
				TASK_SYNCHRONIZED_SCENE(pedCultMember[1], sceneId, "random@altruist_cult", strGuard2, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			ENDIF
			PLAY_SYNCHRONIZED_CAM_ANIM(camCutsceneOrigin, sceneId, strCam, "random@altruist_cult")
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF NOT IS_PED_INJURED(pedVictim[0])
			AND NOT IS_PED_INJURED(pedVictim[1])
				TASK_SYNCHRONIZED_SCENE(pedVictim[0], sceneId, "random@altruist_cult", strVictim1, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(pedVictim[1], sceneId, "random@altruist_cult", strVictim2, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				
//				OPEN_SEQUENCE_TASK(Seq)
//					TASK_LOOK_AT_ENTITY(NULL, pedVictim[0], -1)
//					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), pedVictim[0], PEDMOVE_WALK, FALSE, 3)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim[0], -1)
//				CLOSE_SEQUENCE_TASK(Seq)
//				TASK_PERFORM_SEQUENCE(pedCultMember[0], Seq)
//				CLEAR_SEQUENCE_TASK(Seq)
//				
//				OPEN_SEQUENCE_TASK(Seq)
//					TASK_LOOK_AT_ENTITY(NULL, pedVictim[1], -1)
//					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, pedVictim[1], pedVictim[0], PEDMOVE_WALK, FALSE, 1.7)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedVictim[1], -1)
//				CLOSE_SEQUENCE_TASK(Seq)
//				TASK_PERFORM_SEQUENCE(pedCultMember[1], Seq)
//				CLEAR_SEQUENCE_TASK(Seq)
			ENDIF
			
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
			iDropOffCutsceneCase ++
		BREAK
		
		CASE 2
//			IF NOT bWhimpering
				IF NOT IS_PED_INJURED(pedVictim[0])
					IF IS_PED_MALE(pedVictim[0])
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedVictim[1])
					IF IS_PED_MALE(pedVictim[1])
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[1], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[1], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
					ENDIF
				ENDIF
//				bWhimpering = TRUE
//			ENDIF
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF(iCutsceneWaitTimer - 2000) < GET_GAME_TIMER()
//			ENDIF
			
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1033.3081, 4908.4478, 207.1179 >>, << 8.3588, 0.0000, 49.3638 >>, 50, TRUE)
//				camCutsceneDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1034.3081, 4909.4478, 207.1179 >>, << 8.3588, 0.0000, 49.3638 >>, 45, TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehPlayersTemp, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehPlayersTemp, << 0.1969, -0.7989, 0.3845 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 24.2221)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehPlayersTemp, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehPlayersTemp, << 0.1319, -0.6669, 0.3870 >>)
//				SET_CAM_FOV(camCutsceneDest, 24.2221)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 12000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > fCloseDoorPhase
//					CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", strLeaveDialogue, CONV_PRIORITY_AMBIENT_HIGH)
				
	//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 4500)
					iDropOffCutsceneCase ++
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF (iCutsceneWaitTimer - 500) < GET_GAME_TIMER()
//				CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_FLW_2", CONV_PRIORITY_AMBIENT_HIGH)
//				IF NOT bWhimpering
					IF NOT IS_PED_INJURED(pedVictim[0])
						IF IS_PED_MALE(pedVictim[0])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
						ELSE
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[0], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
						ENDIF
	//					IF IS_PED_IN_ANY_VEHICLE(pedVictim[0])
	//					OPEN_SEQUENCE_TASK(seq)
	//						TASK_LEAVE_ANY_VEHICLE(NULL)
	//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
	//					CLOSE_SEQUENCE_TASK(seq)
	//					TASK_PERFORM_SEQUENCE(pedVictim[0], seq)
	//					CLEAR_SEQUENCE_TASK(seq)
	//					ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedVictim[1])
						IF IS_PED_MALE(pedVictim[1])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[1], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
						ELSE
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim[1], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
	 					ENDIF
	//					IF IS_PED_IN_ANY_VEHICLE(pedVictim[1])
	//					OPEN_SEQUENCE_TASK(seq)
	//						TASK_LEAVE_ANY_VEHICLE(NULL)
	//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
	//					CLOSE_SEQUENCE_TASK(seq)
	//					TASK_PERFORM_SEQUENCE(pedVictim[1], seq)
	//					CLEAR_SEQUENCE_TASK(seq)
	//					ENDIF
					ENDIF
//					bWhimpering = TRUE
//				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > fEndSyncScenePhase
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
					iDropOffCutsceneCase ++
				ENDIF
//			ENDIF
		BREAK
		
		CASE 4
//			IF (iCutsceneWaitTimer) < GET_GAME_TIMER()
				
				IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds < 2
					REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT()
					PRINTLN("@@@@@@@@@@@ REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT 2 @@@@@@@@@@@@")
					IF DOES_ENTITY_EXIST(pedCultMember[0])
						DELETE_PED(pedCultMember[0])
					ENDIF
					IF DOES_ENTITY_EXIST(pedCultMember[1])
						DELETE_PED(pedCultMember[1])
					ENDIF
					IF DOES_ENTITY_EXIST(objCash)
						DELETE_OBJECT(objCash)
					ENDIF
					IF DOES_ENTITY_EXIST(objSawnoff)
						DELETE_OBJECT(objSawnoff)
					ENDIF
					// Lock
//					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, TRUE, 0)//, 1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].openRate)
//					ENDIF
//					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, TRUE, 0)//, -1)//g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].openRate)
//					ENDIF
//					DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
//					DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1033.8966, 4914.2837, 205.4544>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 317.0894)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
//						SET_CAM_ACTIVE(camCutsceneDest, FALSE)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 2000)
				ENDIF
				
				TRIGGER_MUSIC_EVENT("AC_DONE")
				
				PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds BEFORE: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
				g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds +=2
				PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds AFTER: ", g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, " @@@@@@@@@@@@@@@@@@@@@@")
				
				bDropOffDone = TRUE
//			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

OBJECT_INDEX objWeaponTrevor
//OBJECT_INDEX objWeaponCutscene[5]

PROC attackCutscene()
	
//	IF iAttackCutsceneCase <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iAttackCutsceneCase = 4
//		ENDIF
//	ENDIF
	INT iPlacementFlags = 0
	
	SWITCH iAttackCutsceneCase
		//Setup
		CASE 0
//			REQUEST_MODEL(A_M_O_ACult_02)
//			WHILE NOT HAS_MODEL_LOADED(A_M_O_ACult_02)
//				WAIT(0)
//			ENDWHILE
			
//			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
//				SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
//			ENDIF
			
			SET_WEATHER_TYPE_OVERTIME_PERSIST("THUNDER", 20)
			
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE("ac_ig_3_p3_b")
			ENDIF
			REQUEST_MODEL(A_M_O_ACULT_02)
			REQUEST_MODEL(A_M_Y_ACULT_02)
			REQUEST_MODEL(PROP_BOX_WOOD03A)
			REQUEST_MODEL(PROP_BOX_WOOD04A)
			REQUEST_MODEL(PROP_BOX_WOOD05A)
			REQUEST_MODEL(Prop_Security_case_01)
			WHILE NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			OR NOT HAS_MODEL_LOADED(A_M_O_ACULT_02)
			OR NOT HAS_MODEL_LOADED(A_M_Y_ACULT_02)
			OR NOT HAS_MODEL_LOADED(PROP_BOX_WOOD03A)
			OR NOT HAS_MODEL_LOADED(PROP_BOX_WOOD04A)
			OR NOT HAS_MODEL_LOADED(PROP_BOX_WOOD05A)
			OR NOT HAS_MODEL_LOADED(Prop_Security_case_01)
				WAIT(0)
			ENDWHILE
			
			objWeaponTrevor = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE)
			IF DOES_ENTITY_EXIST(objWeaponTrevor)
				REGISTER_ENTITY_FOR_CUTSCENE(objWeaponTrevor, "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Rifle_Mag1^1", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
			
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCultMember[11], "Cult_Master", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_O_ACULT_02)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCultMember[12], "Rear_Cult", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_O_ACULT_02)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCultMember[13], "Left_Old_Cult", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_O_ACULT_02)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCultMember[14], "Carbine_Cult", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_Y_ACULT_02)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCultMember[15], "Shotgun_Cult", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_Y_ACULT_02)
				
//				objWeaponCutscene[0] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedCultMember[11], NULL)
//				IF DOES_ENTITY_EXIST(objWeaponCutscene[0])
//					REGISTER_ENTITY_FOR_CUTSCENE(objWeaponCutscene[0], NULL, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				objWeaponCutscene[1] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedCultMember[12], WEAPONTYPE_ASSAULTRIFLE)
//				IF DOES_ENTITY_EXIST(objWeaponCutscene[1])
//					REGISTER_ENTITY_FOR_CUTSCENE(objWeaponCutscene[1], "Rear_Cult_Rifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Rifle_Mag1", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
//				
//				objWeaponCutscene[2] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedCultMember[13], WEAPONTYPE_ASSAULTRIFLE)
//				IF DOES_ENTITY_EXIST(objWeaponCutscene[2])
//					REGISTER_ENTITY_FOR_CUTSCENE(objWeaponCutscene[2], "Old_Cult_Rifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Rifle_Mag1^2", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
//				
//				objWeaponCutscene[3] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedCultMember[14], WEAPONTYPE_CARBINERIFLE)
//				IF DOES_ENTITY_EXIST(objWeaponCutscene[3])
//					REGISTER_ENTITY_FOR_CUTSCENE(objWeaponCutscene[3], "Carbine_Cult_Rifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
////				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Rifle_Mag1^3", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_CARBINERIFLE_CLIP_01))
//				
//				objWeaponCutscene[4] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedCultMember[15], WEAPONTYPE_PUMPSHOTGUN)
//				IF DOES_ENTITY_EXIST(objWeaponCutscene[4])
//					REGISTER_ENTITY_FOR_CUTSCENE(objWeaponCutscene[4], "Shotgun_Cult_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
				
				START_CUTSCENE()
			ENDIF
			WAIT(0)
			IF NOT bReplay
			AND g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds != 1000
				PRINTLN("@@@@@@@@@@@@@@@ REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT @@@@@@@@@@@@@@@")
				REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT()
			ENDIF
			IF DOES_ENTITY_EXIST(pedCultMember[0])
				DELETE_PED(pedCultMember[0])
			ENDIF
			IF DOES_ENTITY_EXIST(pedCultMember[1])
				DELETE_PED(pedCultMember[1])
			ENDIF
			IF DOES_CAM_EXIST(camCutsceneOrigin)
				SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
			ENDIF
			IF DOES_ENTITY_EXIST(objSawnoff)
				DELETE_OBJECT(objSawnoff)
			ENDIF
			IF DOES_ENTITY_EXIST(objSawnoff)
				DELETE_OBJECT(objSawnoff)
			ENDIF
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
			IF DOES_BLIP_EXIST(blipCult)
				REMOVE_BLIP(blipCult)
			ENDIF
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CLEAR_HELP(TRUE)
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
			ENDIF
			
			CLEAR_AREA(vCultMainCoords, 100, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(vCultMainCoords - << 80, 60, 100 >>, vCultMainCoords + << 80, 60, 100 >>)
			SET_WANTED_LEVEL_MULTIPLIER(0)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
			
			coverPlayer = ADD_COVER_POINT(<< -1173.5094, 4924.3647, 222.2100 >>, 277.2293, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_90, TRUE)
			
			REQUEST_MODEL(A_M_O_ACULT_01)
//			REQUEST_MODEL(A_M_O_ACult_02)
//				REQUEST_MODEL(A_M_Y_ACult_01)
////				REQUEST_MODEL(A_M_Y_ACult_02)
			WHILE NOT HAS_MODEL_LOADED(A_M_O_ACULT_01)
//			OR NOT HAS_MODEL_LOADED(A_M_O_ACult_02)
////				OR NOT HAS_MODEL_LOADED(A_M_Y_ACult_01)
////				OR NOT HAS_MODEL_LOADED(A_M_Y_ACult_02)
				WAIT(0)
			ENDWHILE
			
//			KILL_FACE_TO_FACE_CONVERSATION()
//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				WAIT(0)
//			ENDWHILE
			iAttackCutsceneCase = 6
		BREAK
		
		CASE 1
			
//			camCutsceneOrigin = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -1037.3417, 4911.4043, 207.7337 >>, << -12.7214, 0.0000, -29.4338 >>, 40, TRUE)
//			camCutsceneDest = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//			ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.4852, -0.4319, 0.7138 >>)
//			POINT_CAM_AT_ENTITY(camCutsceneOrigin, PLAYER_PED_ID(), << -0.5345, 2.5675, 0.7588 >>)
//			SET_CAM_FOV(camCutsceneOrigin, 21.9541)
//			ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehPlayersTemp, << 1.1031, 2.3050, 0.8525 >>)
//			POINT_CAM_AT_ENTITY(camCutsceneDest, vehPlayersTemp, << -0.0567, -0.4556, 0.6686 >>)
//			SET_CAM_FOV(camCutsceneDest, 21.9541)
//			SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 8000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			pedCultMember[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACult_02, << -1034.3672, 4920.2002, 204.5249 >>, 133.2147)
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[0], TRUE)
//			SET_ENTITY_HEALTH(pedCultMember[0], 101)
//			GIVE_WEAPON_TO_PED(pedCultMember[0], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
//			
//			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 1, pedCultMember[0], "ACULTMember1")
//			ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 1, pedCultMember[0], "ACULTMember2")
			
//			OPEN_SEQUENCE_TASK(Seq)
//				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
//				TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_WALK, FALSE, 1.7)
//			CLOSE_SEQUENCE_TASK(Seq)
//			TASK_PERFORM_SEQUENCE(pedCultMember[0], Seq)
//			CLEAR_SEQUENCE_TASK(Seq)
			
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
//			iAttackCutsceneCase ++
		BREAK
		
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				camCutsceneOrigin = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1031.6466, 4913.3726, 206.0279 >>, << 5.9134, -0.0000, 67.0184 >>, 50, TRUE)
//				camCutsceneDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1031.6466, 4913.3726, 206.0279 >>, << 5.9134, -0.0000, 67.0184 >>, 45, TRUE)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 12000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
//				CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_CHANT2", CONV_PRIORITY_AMBIENT_HIGH)
				
//				IF NOT IS_PED_INJURED(pedCultMember[0])
//					OPEN_SEQUENCE_TASK(Seq)
//						TASK_LOOK_AT_ENTITY(NULL, pedCultMember[0], -1)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedCultMember[0], -1)
//					CLOSE_SEQUENCE_TASK(Seq)
//					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), Seq)
//					CLEAR_SEQUENCE_TASK(Seq)
//				ENDIF
				
//				iAttackCutsceneCase ++
//			ENDIF
		BREAK
		
		CASE 3
//			IF CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_ALTER", CONV_PRIORITY_AMBIENT_HIGH)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
//				iAttackCutsceneCase ++
//			ENDIF
		BREAK
		
		CASE 4
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				START_CUTSCENE()
//				IF NOT IS_PED_INJURED(pedCultMember[0])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_COMBAT_PED(NULL, pedCultMember[0])
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 4000)
//					iAttackCutsceneCase ++
//				ENDIF
//			ENDIF
		BREAK
		
//		CASE 5
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
////				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
////				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -1172.7603, 4923.4048, 222.1060 >>, 1, TRUE, 0, TRUE)
////				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//			ENDIF
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR")
//				PRINTLN("@@@@@@@@@@@@@@ CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY @@@@@@@@@@@@@@@@@")
//				pedCultMember[11] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CULT_MASTER"))
//				pedCultMember[12] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("REAR_CULT"))
//				pedCultMember[13] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LEFT_OLD_CULT"))
//				pedCultMember[14] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CARBINE_CULT"))
//				pedCultMember[15] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("SHOTGUN_CULT"))
//				
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 120, TRUE, TRUE)
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1173.5309, 4924.3569, 222.2101 >>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 188.9369)
//				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -1173.5094, 4924.3647, 222.2100 >>, -1, FALSE, 0, TRUE, FALSE, coverPlayer)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
//				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(91.3878)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(4.4337)
////				DISPLAY_HUD(TRUE)
////				DISPLAY_RADAR(TRUE)
////				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
////				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
////				
////				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1172.7603, 4923.4048, 222.1060 >>)
////				SET_ENTITY_HEADING(PLAYER_PED_ID(), 247.4160)
////				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//////				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -1172.7603, 4923.4048, 222.1060 >>, 0, TRUE, 0, TRUE)
////				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//				
////				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
////				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -1172.7603, 4923.4048, 222.1060 >>, 1, TRUE, 0, TRUE)
////				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//				
////				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
////				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
//			ENDIF
//			IF HAS_CUTSCENE_FINISHED()
////				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				
////				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1172.7603, 4923.4048, 222.1060 >>)
////				SET_ENTITY_HEADING(PLAYER_PED_ID(), 247.4160)
////				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				DISPLAY_HUD(TRUE)
//				DISPLAY_RADAR(TRUE)
//				iAttackCutsceneCase ++
//			ENDIF
//		BREAK
		
		CASE 6
//			IF (iCutsceneWaitTimer) < GET_GAME_TIMER()
			
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CULT_MASTER")
					IF NOT DOES_ENTITY_EXIST(pedCultMember[11])
						pedCultMember[11] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CULT_MASTER"))
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("REAR_CULT")
					IF NOT DOES_ENTITY_EXIST(pedCultMember[12])
						pedCultMember[12] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("REAR_CULT"))
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LEFT_OLD_CULT")
					IF NOT DOES_ENTITY_EXIST(pedCultMember[13])
						pedCultMember[13] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LEFT_OLD_CULT"))
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CARBINE_CULT")
					IF NOT DOES_ENTITY_EXIST(pedCultMember[14])
						pedCultMember[14] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CARBINE_CULT"))
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("SHOTGUN_CULT")
					IF NOT DOES_ENTITY_EXIST(pedCultMember[15])
						pedCultMember[15] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("SHOTGUN_CULT"))
					ENDIF
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Rear_Cult_Rifle")
//					GIVE_WEAPON_OBJECT_TO_PED(objWeaponCutscene[1], pedCultMember[12])
//				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Old_Cult_Rifle")
//					GIVE_WEAPON_OBJECT_TO_PED(objWeaponCutscene[2], pedCultMember[13])
//				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Carbine_Cult_Rifle")
//					GIVE_WEAPON_OBJECT_TO_PED(objWeaponCutscene[3], pedCultMember[14])
//				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Shotgun_Cult_Gun")
//					GIVE_WEAPON_OBJECT_TO_PED(objWeaponCutscene[4], pedCultMember[15])
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR")
					PRINTLN("@@@@@@@@@@@@@@ CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY @@@@@@@@@@@@@@@@@")
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1173.5309, 4924.3569, 222.2101 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 188.9369)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -1173.5094, 4924.3647, 222.2100 >>, -1, FALSE, 0, TRUE, FALSE, coverPlayer)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
					GIVE_WEAPON_OBJECT_TO_PED(objWeaponTrevor, PLAYER_PED_ID())
	//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 120, TRUE, TRUE)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(91.3878)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(4.4337)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				vCultSpawn[0] = << -1044.6324, 4918.1494, 211.3315 >>
				fCultHeading[0] = 254.8275
				vCultSpawn[1] = << -1054.9197, 4915.1548, 210.8241 >>
				fCultHeading[1] = 270.2025
				vCultSpawn[2] = << -1051.8595, 4903.9023, 209.6199 >>
				fCultHeading[2] = 299.8404
				vCultSpawn[3] = << -1068.2458, 4899.5317, 213.2755 >>
				fCultHeading[3] = 298.4257
				vCultSpawn[4] = << -1083.3221, 4899.5317, 213.2755 >>
				fCultHeading[4] = 283.5311
				vCultSpawn[5] = << -1084.0582, 4933.8047, 228.2246 >>
				fCultHeading[5] = 87.5233
				vCultSpawn[6] = << -1081.9757, 4937.3394, 232.3275 >>
				fCultHeading[6] = 109.6546
				vCultSpawn[7] = << -1092.0750, 4939.9575, 217.3385 >>
				fCultHeading[7] = 210.2433
				vCultSpawn[8] = << -1101.7976, 4930.3301, 217.3544 >>
				fCultHeading[8] = 203.7320
				vCultSpawn[9] = << -1070.6748, 4869.3013, 220.2917 >>
				fCultHeading[9] = 39.1109
				vCultSpawn[10] = << -1115.7025, 4878.4390, 225.8085 >>
				fCultHeading[10] = 25.8472
//				vCultSpawn[11] = << -1138.1639, 4945.3267, 225.7461 >>
//				fCultHeading[11] = 137.5665
//				vCultSpawn[12] = << -1133.1682, 4922.3403, 218.8357 >>
//				fCultHeading[12] = 80.4284
//				vCultSpawn[13] = << -1165.6851, 4927.8574, 222.1158 >>
//				fCultHeading[13] = 118.9903
//				vCultSpawn[14] = << -1163.5170, 4923.0664, 221.7555 >>
//				fCultHeading[14] = 82.4393
//				vCultSpawn[15] = << -1159.7632, 4929.5313, 221.9975 >>
//				fCultHeading[15] = 103.8853
//				vCultSpawn[16] = << -1141.4049, 4913.2261, 219.9731 >>
//				fCultHeading[16] = 260.2733
//				vCultSpawn[17] = << -1136.6714, 4935.0703, 221.2739 >>
//				fCultHeading[17] = 241.2545
//				vCultSpawn[18] = << -1147.8114, 4938.9331, 221.2739 >>
//				fCultHeading[18] = 199.7082
//				vCultSpawn[19] = << -1139.8473, 4945.8823, 226.0575 >>
//				fCultHeading[19] = 247.0022
//				vCultSpawn[20] = << -1123.8096, 4893.3320, 217.4769 >>
//				fCultHeading[20] = 43.3314
//				vCultSpawn[21] = << -1116.2501, 4878.1797, 225.8084 >>
//				fCultHeading[21] = 309.4868
//				vCultSpawn[22] = << -1077.9830, 4883.4873, 221.1869 >>
//				fCultHeading[22] = 330.3281
				
//				IF DOES_ENTITY_EXIST(pedCultMember[0])
//					DELETE_PED(pedCultMember[0])
//				ENDIF
//				IF DOES_ENTITY_EXIST(pedCultMember[1])
//					DELETE_PED(pedCultMember[1])
//				ENDIF
				
				ADD_RELATIONSHIP_GROUP("rghCult", rghCult)
				INT index
				REPEAT COUNT_OF(pedCultMember) index
					IF (index = 0) OR (index = 5) OR (index = 6) OR (index = 10)
						pedCultMember[index] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_01, vCultSpawn[index], fCultHeading[index])
						SET_PED_RANDOM_COMPONENT_VARIATION(pedCultMember[index])
						GIVE_WEAPON_TO_PED(pedCultMember[index], WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCultMember[index], CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_MOVEMENT(pedCultMember[index], CM_STATIONARY)
						SET_PED_COMBAT_ABILITY(pedCultMember[index], CAL_PROFESSIONAL)
						SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedCultMember[index], 50, 20)
//						SET_PED_COMBAT_RANGE(pedCultMember[index], CR_MEDIUM)
						SET_PED_ACCURACY(pedCultMember[index], 5)
						SET_PED_HEARING_RANGE(pedCultMember[index], 100.0)
						SET_PED_SEEING_RANGE(pedCultMember[index], 100.0)
						SET_ENTITY_HEALTH(pedCultMember[index], 150)
						SET_PED_SHOOT_RATE(pedCultMember[index], GET_RANDOM_INT_IN_RANGE(40, 60))
						TASK_COMBAT_PED(pedCultMember[index], PLAYER_PED_ID())
					ELIF (index = 2)
						pedCultMember[index] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02, vCultSpawn[index], fCultHeading[index])
						SET_PED_RANDOM_COMPONENT_VARIATION(pedCultMember[index])
						GIVE_WEAPON_TO_PED(pedCultMember[index], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, TRUE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedCultMember[index], CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_MOVEMENT(pedCultMember[index], CM_WILLADVANCE)
						SET_PED_COMBAT_ABILITY(pedCultMember[index], CAL_AVERAGE)
						SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedCultMember[index], 50, 20)
//						SET_PED_COMBAT_RANGE(pedCultMember[index], CR_MEDIUM)
						SET_PED_ACCURACY(pedCultMember[index], 5)
						SET_PED_HEARING_RANGE(pedCultMember[index], 100.0)
						SET_PED_SEEING_RANGE(pedCultMember[index], 100.0)
						SET_PED_SHOOT_RATE(pedCultMember[index], GET_RANDOM_INT_IN_RANGE(40, 60))
						TASK_COMBAT_PED(pedCultMember[index], PLAYER_PED_ID())
					ELIF (index > 10 AND index < 20)
//						pedCultMember[index] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACult_01/*A_M_Y_ACult_01*/, vCultSpawn[index], fCultHeading[index])
//						SET_PED_RANDOM_COMPONENT_VARIATION(pedCultMember[index]
						IF NOT IS_PED_INJURED(pedCultMember[index])
						AND NOT IS_PED_INJURED(pedCultMember[11])
						AND NOT IS_PED_INJURED(pedCultMember[12])
						AND NOT IS_PED_INJURED(pedCultMember[13])
						AND NOT IS_PED_INJURED(pedCultMember[14])
						AND NOT IS_PED_INJURED(pedCultMember[15])
							GIVE_WEAPON_TO_PED(pedCultMember[11], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
							GIVE_WEAPON_TO_PED(pedCultMember[12], WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
							GIVE_WEAPON_TO_PED(pedCultMember[13], WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
							GIVE_WEAPON_TO_PED(pedCultMember[14], WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
							GIVE_WEAPON_TO_PED(pedCultMember[15], WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedCultMember[index], CA_USE_COVER, TRUE)
							SET_PED_COMBAT_MOVEMENT(pedCultMember[index], CM_WILLRETREAT)
							SET_PED_COMBAT_ABILITY(pedCultMember[index], CAL_POOR)
//							SET_PED_SHOOT_RATE(pedCultMember[index], 100)
//							SET_PED_FIRING_PATTERN(pedCultMember[index], FIRING_PATTERN_SINGLE_SHOT)
//							SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedCultMember[index], 50, 20)
//							SET_PED_COMBAT_RANGE(pedCultMember[index], CR_MEDIUM)
							SET_PED_ACCURACY(pedCultMember[index], 0)
							SET_PED_CHANCE_OF_FIRING_BLANKS(pedCultMember[index], 1, 1)
//							SET_PED_HEARING_RANGE(pedCultMember[index], 100.0)
//							SET_PED_SEEING_RANGE(pedCultMember[index], 100.0)
							
							OPEN_SEQUENCE_TASK(Seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1159.3301, 4924.4116, 221.6576 >>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, TRUE)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 6000)
//								TASK_SHOOT_AT_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 0, 1 >>, 1000, FIRING_TYPE_DEFAULT)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedCultMember[15], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							OPEN_SEQUENCE_TASK(Seq)
//								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1163.2794, 4932.9331, 222.5410 >>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, TRUE)
								TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 5000)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 7500)
//								TASK_SHOOT_AT_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 0, 1 >>, 1000, FIRING_TYPE_DEFAULT)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedCultMember[14], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							OPEN_SEQUENCE_TASK(Seq)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 2500)
								TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 5000)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1156.1754, 4926.0732, 221.3447 >>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, TRUE)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 6500)
//								TASK_SHOOT_AT_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 0, 1 >>, 1000, FIRING_TYPE_DEFAULT)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedCultMember[13], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							OPEN_SEQUENCE_TASK(Seq)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1157.9622, 4922.1958, 221.3090 >>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, TRUE)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 5500)
//								TASK_SHOOT_AT_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 0, 1 >>, 1000, FIRING_TYPE_DEFAULT)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedCultMember[12], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							OPEN_SEQUENCE_TASK(Seq)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 10000)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1161.4755, 4916.0610, 220.5658 >>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, TRUE)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
//								TASK_SHOOT_AT_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 0, 1 >>, 1000, FIRING_TYPE_DEFAULT)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 4500)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedCultMember[11], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							
							SET_PED_KEEP_TASK(pedCultMember[index], TRUE)
							SET_PED_RESET_FLAG(pedCultMember[index], PRF_InstantBlendToAim, TRUE)
						ENDIF
					ELSE
						pedCultMember[index] = CREATE_PED(PEDTYPE_MISSION, A_M_O_ACULT_02/*A_M_Y_ACult_02*/, vCultSpawn[index], fCultHeading[index])
						SET_PED_RANDOM_COMPONENT_VARIATION(pedCultMember[index])
						GIVE_WEAPON_TO_PED(pedCultMember[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedCultMember[index], CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_MOVEMENT(pedCultMember[index], CM_WILLADVANCE)
						SET_PED_COMBAT_ABILITY(pedCultMember[index], CAL_POOR)
						SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedCultMember[index], 50, 20)
						SET_PED_COMBAT_RANGE(pedCultMember[index], CR_MEDIUM)
						SET_PED_ACCURACY(pedCultMember[index], 5)
						SET_PED_HEARING_RANGE(pedCultMember[index], 100.0)
						SET_PED_SEEING_RANGE(pedCultMember[index], 100.0)
						SET_PED_SHOOT_RATE(pedCultMember[index], GET_RANDOM_INT_IN_RANGE(40, 60))
						TASK_COMBAT_PED(pedCultMember[index], PLAYER_PED_ID())
					ENDIF
					SET_PED_RELATIONSHIP_GROUP_HASH(pedCultMember[index], rghCult)
					SET_ENTITY_HEADING(pedCultMember[index], GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(pedCultMember[index]), GET_PLAYER_COORDS(PLAYER_ID())))
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCultMember[index], TRUE)
					SET_PED_CONFIG_FLAG(pedCultMember[index], PCF_DontInfluenceWantedLevel, TRUE)
					
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCultMember[index], TRUE)
					
					blipCultMember[index] = CREATE_BLIP_FOR_PED(pedCultMember[index], TRUE)
					#IF IS_DEBUG_BUILD
						sPedNames[index] = "CultMember["
						sPedNames[index] += index
						sPedNames[index] += "]"
						SET_PED_NAME_DEBUG(pedCultMember[index], sPedNames[index])
					#ENDIF
				ENDREPEAT
				
				ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 3, pedCultMember[11], "ACULTMaster")
				ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 4, NULL, "ACULTGroup")
				
//				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCult, RELGROUPHASH_PLAYER)
				
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				
				IF NOT bReplay
					pickupBriefcase[0] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, <<-1074.1650, 4897.4683, 213.2754>>, <<-72.0000, 0.0000, 42.4800>>, iPlacementFlags, 25000, EULER_YXZ, TRUE, Prop_Security_case_01) //83.8650
					pickupBriefcase[1] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, <<-1094.6711, 4892.7886, 215.0707>>, <<-72.0000, 0.0000, 42.4800>>, iPlacementFlags, 25000, EULER_YXZ, TRUE, Prop_Security_case_01) //312.1291
					pickupBriefcase[2] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, <<-1113.9503, 4904.3848, 217.6001>>, <<-72.0000, 0.0000, 42.4800>>, iPlacementFlags, 25000, EULER_YXZ, TRUE, Prop_Security_case_01) //292.9774
					pickupBriefcase[3] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, <<-1145.2728, 4907.8936, 219.9734>>, <<-72.0000, 0.0000, 42.4800>>, iPlacementFlags, 25000, EULER_YXZ, TRUE, Prop_Security_case_01) //99.4268
				ENDIF
				
				pickupBriefcase[0] = pickupBriefcase[0]
				pickupBriefcase[1] = pickupBriefcase[1]
				pickupBriefcase[2] = pickupBriefcase[2]
				pickupBriefcase[3] = pickupBriefcase[3]
				
				objCrate[0] = CREATE_OBJECT(PROP_BOX_WOOD04A, <<-1158.689941,4915.709473,220.264435>>)
				objCrate[1] = CREATE_OBJECT(PROP_BOX_WOOD03A, <<-1156.246216,4918.599609,220.543228>>)
				objCrate[2] = CREATE_OBJECT(PROP_BOX_WOOD04A, <<-1145.802490,4931.816406,219.968262>>)
				objCrate[3] = CREATE_OBJECT(PROP_BOX_WOOD04A, <<-1133.369019,4919.067383,218.838501>>)
				objCrate[4] = CREATE_OBJECT(PROP_BOX_WOOD05A, <<-1132.025146,4921.482910,218.824402>>)
				objCrate[5] = CREATE_OBJECT(PROP_BOX_WOOD03A, <<-1112.659912,4929.267578,217.175186>>)
				SET_ENTITY_ROTATION(objCrate[0], <<12.242877,-0.014582,27.060556>>)
				SET_ENTITY_ROTATION(objCrate[1], <<7.733739,9.432779,-2.865618>>)
				SET_ENTITY_ROTATION(objCrate[2], <<3.735082,4.551246,86.309166>>)
				SET_ENTITY_ROTATION(objCrate[3], <<0.240341,1.095195,36.590313>>)
				SET_ENTITY_ROTATION(objCrate[4], <<7.677200,1.232743,93.274834>>)
				SET_ENTITY_ROTATION(objCrate[5], <<3.367802,-5.143870,97.305672>>)
//				SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
//				SET_CAM_ACTIVE(camCutsceneDest, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF GET_PLAYER_SPECIAL_ABILITY_VALUE() < 50
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.5, TRUE)
				ENDIF
				
				IF GET_ACTIVE_RANDOM_EVENT() != RE_CULTSHOOTOUT
					LAUNCH_RANDOM_EVENT(RE_CULTSHOOTOUT)
				ENDIF
				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
					SET_RANDOM_EVENT_ACTIVE(FALSE)
				ENDIF
				SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
				bReplay = FALSE
				altruistStage = cultShootout
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT (coords_struct in_coords)

#IF IS_FINAL_BUILD
UNUSED_PARAMETER(in_coords)
#ENDIF

#IF IS_DEBUG_BUILD
PRINTNL()
PRINTLN("altruist_cult")
PRINTVECTOR(in_coords.vec_coord[0])
PRINTNL()
#ENDIF

IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_RANDOM_EVENTS|FORCE_CLEANUP_FLAG_SP_TO_MP)
//AND NOT bReplay
	missionCleanup()
ENDIF

IF GET_IS_PLAYER_IN_ANIMAL_FORM()
	PRINTLN("@@@@@@@@@@@@@ GET_IS_PLAYER_IN_ANIMAL_FORM = TRUE - TERMINATE! @@@@@@@@@@@@@")
	TERMINATE_THIS_THREAD()
ENDIF

WHILE TRUE
	WAIT(0)
//	IF bReplay
//		PRINTLN("@@@@@@@@@@@@@ bReplay = TRUE (script will not clean up) @@@@@@@@@@@@@")
//	ELSE
//		PRINTLN("@@@@@@@@@@@@@ bReplay = FALSE @@@@@@@@@@@@@")
//	ENDIF
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE() OR bHasReplaySplashBeenSetup
	
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				altruistStage = playAttackCutsceneAndSetupShootout
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 0
				missionCleanup()
			ENDIF
		#ENDIF
		
//		bDoFullCleanUp = TRUE
		SET_WANTED_LEVEL_MULTIPLIER(0)
		
//		IF NOT bGatesAddedToSystem
//			iLeftGate = HASH("LeftGate")
//			iRightGate = HASH("RightGate")
//			ADD_DOOR_TO_SYSTEM(iLeftGate, PROP_GATE_CULT_01_L, << -1041.2676, 4906.0967, 209.2002 >>)
//			ADD_DOOR_TO_SYSTEM(iRightGate, PROP_GATE_CULT_01_R, << -1044.7490, 4914.9717, 209.1932 >>)
//			bGatesAddedToSystem = TRUE
//		ENDIF
		
		SWITCH altruistStage
		
			CASE waitingForDelivery
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//				PRINTLN("@@@@@@@@@@@@@@@@@@@@@ waitingForDelivery @@@@@@@@@@@@@@@@@@@@@")
					//Have enough peds been delivered...
					IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds >= iNumberOfRequiredPeds
					AND NOT IS_CULT_FINISHED()
						altruistStage = playAttackCutsceneAndSetupShootout
					ENDIF
					handlePlayerInCultArea()
					checkIfPlayerIsDeliveringPedsToCult()
				ENDIF
			BREAK
			
			CASE playDropoffSyncscene
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//				PRINTLN("@@@@@@@@@@@@@@@@@@@@@ playDropoffSyncscene @@@@@@@@@@@@@@@@@@@@@")
					IF TIMERA() > 2000
						IF i = 1
							dropOffOnePedAtCult()
						ELIF i = 2
							dropOffTwoPedsAtCult()
						ENDIF
					ENDIF
					IF TIMERA() > 1300
						IF iDropOffCutsceneCase < 3
							openGates()
						ELSE
							closeGates()
						ENDIF
					ENDIF
					
					IF bDropOffDone
						AWARD_ACHIEVEMENT(ACH23)	// Altrusit Accolyte
						bDropOffDone = FALSE
	//					RESET_CULT_STATUS() // Restting cult status here fucks with the cleaning up of the RE script
						altruistStage = waitingForDelivery
					ENDIF
				ENDIF
			BREAK
			
			CASE playAttackCutsceneAndSetupShootout
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
//				PRINTLN("@@@@@@@@@@@@@@@@@@@@@ playAttackCutsceneAndSetupShootout @@@@@@@@@@@@@@@@@@@@@")
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					attackCutscene()
				ENDIF
				// Open
//				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model)
//					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].coords, FALSE, 0)//g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].openRate)
//				ENDIF
//				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, 6.0, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model)
//					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].model, g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].coords, TRUE, 0)//g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].openRate)
//				ENDIF
				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_LEFT].doorID, DOORSTATE_UNLOCKED, FALSE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_CULT_GATE_RIGHT].doorID, DOORSTATE_UNLOCKED, FALSE, TRUE)
//				FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_LEFT, FALSE)
//				FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_CULT_GATE_RIGHT, FALSE)
			BREAK
			
			CASE cultShootout
//				PRINTLN("@@@@@@@@@@@@@@@@@@@@@ cultShootout @@@@@@@@@@@@@@@@@@@@@")
				IF NOT IS_PLAYER_PLAYING(PLAYER_ID()) //detect player death
					PRINTLN("@@@@@@@@@@@@@ PLAYER DIED DURING SHOOTOUT @@@@@@@@@@@@@")
					altruistStage = replayAttackCutsceneAndSetupShootout
					BREAK //break out early so we can go to the next state
	//				g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = iNumberOfRequiredPeds -1
				ENDIF
				bCultMembersAlive = FALSE
				SET_WANTED_LEVEL_MULTIPLIER(0)
				INT index
				REPEAT COUNT_OF(pedCultMember) index
					IF IS_PED_INJURED(pedCultMember[index])
						IF DOES_BLIP_EXIST(blipCultMember[index])
							REMOVE_BLIP(blipCultMember[index])
							PRINTLN("@@@@@@@@@ IS_PED_INJURED: REMOVE_BLIP(blipCultMember[", index, "]) @@@@@@@@@")
						ENDIF
					ELSE
						IF IS_PED_IN_WRITHE(pedCultMember[index])
							IF DOES_BLIP_EXIST(blipCultMember[index])
								REMOVE_BLIP(blipCultMember[index])
								PRINTLN("@@@@@@@@@ IS_PED_IN_WRITHE: REMOVE_BLIP(blipCultMember[", index, "]) @@@@@@@@@")
							ENDIF
						ELSE
							IF IS_ENTITY_AT_ENTITY(pedCultMember[index], PLAYER_PED_ID(), << 30, 30, 30 >>)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF iTimerDialogue < GET_GAME_TIMER()
										INT iRandomDialogue
										iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 3)
										IF iRandomDialogue = 0
											CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_SHOUT", CONV_PRIORITY_AMBIENT_HIGH)
										ELIF iRandomDialogue = 1
											ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 1, pedCultMember[index], "ACULTMember1")
											CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_FIGHT1", CONV_PRIORITY_AMBIENT_HIGH)
										ELIF iRandomDialogue = 2
											ADD_PED_FOR_DIALOGUE(AltruistCultDialogueStruct, 2, pedCultMember[index], "ACULTMember2")
											CREATE_CONVERSATION(AltruistCultDialogueStruct, "ACULTAU", "ACULT_FIGHT2", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
										iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
//						PRINTLN("@@@@@@@@@@@@@@@@@@ bCultMembersAlive @@@@@@@@@@@@@@@@@@")
						bCultMembersAlive = TRUE
					ENDIF
				ENDREPEAT
				
				IF NOT bCultMembersAlive
//				OR GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vCultMainCoords) > 200
					MISSION_PASSED()
				ENDIF
			BREAK
			
			CASE replayAttackCutsceneAndSetupShootout
				//once we get here we know the player is dead
//				PRINTLN("@@@@@@@@@@@@@@@@@@@@@ replayAttackCutsceneAndSetupShootout @@@@@@@@@@@@@@@@@@@@@")
				IF NOT bHasReplaySplashBeenSetup
					bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
					IF NOT HAS_SCALEFORM_MOVIE_LOADED(bigMessageUI.siMovie)
						PRINTLN("WAITIN ON BIG MESSAGE SCALEFORM MOVIE")
						BREAK
					ENDIF
					MG_INIT_FAIL_FADE_EFFECT(deathEffect)

					bHasReplaySplashBeenSetup = TRUE
				ENDIF
						
				IF MG_UPDATE_FAIL_FADE_EFFECT(deathEffect, splashStruct, bigMessageUI, sFailString, sFailSubString, bReplay)
					//if we get here the player is back and we should be fading in.
					//cleanup stuffs and move on
					iAttackCutsceneCase = 0
					REMOVE_RELATIONSHIP_GROUP(rghCult)
					REMOVE_COVER_POINT(coverPlayer)
					REPEAT COUNT_OF(pedCultMember) index
						IF DOES_ENTITY_EXIST(pedCultMember[index])
							DELETE_PED(pedCultMember[index])
						ENDIF
					ENDREPEAT
					REPEAT COUNT_OF(objCrate) index
						IF DOES_ENTITY_EXIST(objCrate[index])
							DELETE_OBJECT(objCrate[index])
						ENDIF
					ENDREPEAT
					IF DOES_CAM_EXIST(camCutsceneOrigin)
						SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
					ENDIF
					IF DOES_ENTITY_EXIST(objSawnoff)
						DELETE_OBJECT(objSawnoff)
					ENDIF
					IF DOES_ENTITY_EXIST(objSawnoff)
						DELETE_OBJECT(objSawnoff)
					ENDIF
					IF bReplay
						MG_CLEANUP_FAIL_FADE_EFFECT()
						DISABLE_CELLPHONE(FALSE)
						SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
						bHasReplaySplashBeenSetup = FALSE
						altruistStage = playAttackCutsceneAndSetupShootout
					ELSE
						bHasReplaySplashBeenSetup = FALSE
						missionCleanup()
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ELIF bSquirrelSuit
		//IS SQUIRREL SUIT ACTIVATED - DON'T CLEAN UP!
	ELSE
		PRINTLN("Altruist Cult is not within world point brain activation range")
		missionCleanup()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@ SQUIRREL SUIT ACTIVATED @@@@@@@@@@@@@@@@@@@@@")
			bSquirrelSuit = TRUE
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_X)
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@ SQUIRREL SUIT DEACTIVATED @@@@@@@@@@@@@@@@@@@@@")
			bSquirrelSuit = FALSE
		ENDIF
		IF bSquirrelSuit
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@ SQUIRREL SUIT ACTIVE! @@@@@@@@@@@@@@@@@@@@@")
			IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
				APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, << 0, 37, 2 >>, << 0, 0, 0 >>, 0, TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
	#ENDIF
ENDWHILE

ENDSCRIPT
