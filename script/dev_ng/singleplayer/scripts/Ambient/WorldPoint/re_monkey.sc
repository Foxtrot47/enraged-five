//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════╡  	   		RE Monkey	   	   	   ╞════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//╒═════════════════════════════════════════════════════════════════════════════╕
//│          		Written by Simon Bramwell	Date: 15/08/14	            	│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│   	The player must photograph the monkey graffiti artist to unlock			│
//│     a new car in their player garage.							 			│
//╘═════════════════════════════════════════════════════════════════════════════╛

//-------------------- INCLUDES --------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "select_mission_stage.sch"
USING "rc_helper_functions.sch"
USING "random_events_public.sch"
USING "ambient_common.sch"
USING "vehicle_gen_private.sch"

//-------------------- ENUMS --------------------
ENUM MISSION_STAGE
	MS_DISCOVER,
	MS_COMPLETE
ENDENUM

ENUM MISSION_STAGE_PHASE
	MSP_INIT,
	MSP_LEGACY_POPULATE,
	MSP_BODY
ENDENUM

ENUM MISSION_OBJECTS
	MO_SPRAY_CAN_MONKEY,
	MO_PAINT_CAN_0,
	MO_PAINT_BRUSH,
	MO_SPRAY_CAN_0
ENDENUM

//-------------------- CONSTS --------------------
CONST_INT			NUM_STAGES					COUNT_OF( MISSION_STAGE )
CONST_INT			NUM_OBJECTS					COUNT_OF( MISSION_OBJECTS )
CONST_INT			MONKEY_SPOOKED				3

//-------------------- MISSION VARS --------------------
MISSION_STAGE		eMissionStage				= MS_DISCOVER
MISSION_STAGE_PHASE	eMissionStagePhase			= MSP_INIT
INT 				iBodyStage					= 0

//-------------------- MONKEY VARS --------------------
PED_INDEX			pedMonkeyMan
VECTOR				vMonkeyManStartPos			= <<-336.614625, -93.668075, 46.0005>>
FLOAT				fMonkeyManStartHeading		= 256.1257
INT					iMonkeyManStage				= 0
BOOL				bMonkeyManPhotographed		= FALSE
BLIP_INDEX			blpMonkeyMan

//-------------------- MONKEY STANDUP VARS --------------------
FLOAT 				fStartPhase 				= 0.0		// phase of anim to start standing up from
INT 				iStandupTime 				= 0			// time to player the anim task for
INT 				iMonkeyStandupStage 		= 0			// standup state
VECTOR 				vStart									// position of monkey when standup starts

//-------------------- OBJECT VARS --------------------
OBJECT_INDEX		objMission[ NUM_OBJECTS ]
VECTOR				vObjCreatePos				= <<-334.8087, -85.7130, 47.4504>>
PTFX_ID 			ptSpray
STRING				sParticleName				= "scr_lamgraff_paint_spray"

//-------------------- ANIM VARS --------------------
STRING				sTaggingDict				= "switch@franklin@lamar_tagging_wall"
STRING				sTaggingClipShakingLoop		= "lamar_tagging_wall_loop_lamar" // standing shaking can
STRING				sTaggingClipCrouch			= "lamar_tagging_wall_exit_lamar" // starts standing and crouches down to spray
STRING				sTaggingClipCrouchingLoop	= "lamar_tagging_exit_loop_lamar" // looping crouched spray

//-------------------- MISC VARS --------------------
VECTOR				vZero						= <<0.0, 0.0, 0.0>>
SHAPETEST_INDEX 	sti
SEQUENCE_INDEX		seq
INT					iTimer

//-------------------- DEBUG VARS --------------------
#IF IS_DEBUG_BUILD

BOOL						bSkipHappening		= FALSE
MissionStageMenuTextStruct 	DebugMenuStruct[ NUM_STAGES ]
WIDGET_GROUP_ID				widMainGroup
VECTOR 						vSprayCanPosOffset
VECTOR 						vSprayCanRotOffset
VECTOR 						vSprayCanPosOffsetOld
VECTOR 						vSprayCanRotOffsetOld
BOOL						bSpookMonkey

#ENDIF

//-------------------- MISC PROCS --------------------
PROC SET_ENTITY_COORD_AND_HEADING( ENTITY_INDEX entity, VECTOR vCoord, FLOAT fHeading )
	SET_ENTITY_COORDS( entity, vCoord )
	SET_ENTITY_HEADING( entity, fHeading )
ENDPROC

FUNC BOOL IS_ENTITY_WITHIN_DISTANCE_OF_ENTITY( ENTITY_INDEX entityOne, ENTITY_INDEX entityTwo, FLOAT fDistance )
	IF( DOES_ENTITY_EXIST( entityOne ) AND DOES_ENTITY_EXIST( entityTwo ) )
		FLOAT fDistance2 = fDistance * fDistance
		RETURN VDIST2( GET_ENTITY_COORDS( entityOne, FALSE ), GET_ENTITY_COORDS( entityTwo, FALSE ) ) <= fDistance2
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_PERFORMING_TASK( PED_INDEX ped, SCRIPT_TASK_NAME scriptTask )
	SCRIPTTASKSTATUS status = GET_SCRIPT_TASK_STATUS( ped, scriptTask )
	RETURN status = PERFORMING_TASK OR status = WAITING_TO_START_TASK
ENDFUNC

//-------------------- TEXT MESSAGE PROCS --------------------
FUNC INT GET_CURRENT_PLAYER_CHARACTER_FOR_TEXT_MESSAGE()
	MODEL_NAMES model = GET_ENTITY_MODEL( PLAYER_PED_ID() )
	SWITCH( model )
		CASE PLAYER_ZERO	RETURN BIT_MICHAEL
		CASE PLAYER_ONE		RETURN BIT_FRANKLIN
		CASE PLAYER_TWO		RETURN BIT_TREVOR
		DEFAULT				RETURN -1	
	ENDSWITCH
ENDFUNC

PROC SEND_TEXT_MESSAGE()
	REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER( TEXT_MONKEY_CAR_UNLOCK, CT_AMBIENT, 
		GET_CURRENT_PLAYER_CHARACTER_FOR_TEXT_MESSAGE(), CHAR_MONKEY_MAN, 30000, 10000, VID_BLANK, CID_BLANK, 
		FLOW_CHECK_NONE, DEFAULT, CANNOT_CALL_SENDER )
ENDPROC

//-------------------- GARAGE PROCS --------------------
FUNC VEHICLE_GEN_NAME_ENUM GET_CURRENT_PLAYER_GARAGE()
	MODEL_NAMES model = GET_ENTITY_MODEL( PLAYER_PED_ID() )
	SWITCH( model )
		CASE PLAYER_ZERO	RETURN VEHGEN_WEB_CAR_MICHAEL
		CASE PLAYER_ONE		RETURN VEHGEN_WEB_CAR_FRANKLIN
		CASE PLAYER_TWO		RETURN VEHGEN_WEB_CAR_TREVOR
		DEFAULT				RETURN VEHGEN_NONE
	ENDSWITCH
ENDFUNC

FUNC BOOL IS_CURRENT_PLAYER_GARAGE_PURCHASED()
	RETURN GET_VEHICLE_GEN_SAVED_FLAG_STATE( GET_CURRENT_PLAYER_GARAGE(), VEHGEN_S_FLAG_ACQUIRED )
ENDFUNC

//-------------------- SPRAY PAINTING PROCS --------------------
PROC STOP_SPRAY_PAINTING_PARTICLES()
	IF( ptSpray <> NULL )
		STOP_PARTICLE_FX_LOOPED( ptSpray )
		ptSpray = NULL
	ENDIF
ENDPROC

PROC START_SPRAY_PAINTING_PARTICLES()
	IF( ptSpray = NULL )
		ptSpray = START_PARTICLE_FX_LOOPED_ON_ENTITY( sParticleName, objMission[ MO_SPRAY_CAN_MONKEY ], vZero, vZero )
		SET_PARTICLE_FX_LOOPED_COLOUR( ptSpray, 1.0, 1.0, 1.0 )
		SET_PARTICLE_FX_LOOPED_ALPHA( ptSpray, 0.2 )
	ENDIF
ENDPROC

PROC UPDATE_SPRAY_PAINT_PARTICLES()
	FLOAT fAnimUnitTime
	IF( IS_ENTITY_PLAYING_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch ) )
		fAnimUnitTime = GET_ENTITY_ANIM_CURRENT_TIME( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch )
	
		IF( fAnimUnitTime < 0.383793 )
			STOP_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 0.612112 )
			START_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 1.0 )
			STOP_SPRAY_PAINTING_PARTICLES()
		ENDIF
		
	ELIF( IS_ENTITY_PLAYING_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouchingLoop )	)
		fAnimUnitTime = GET_ENTITY_ANIM_CURRENT_TIME( pedMonkeyMan, sTaggingDict, sTaggingClipCrouchingLoop )

		IF( fAnimUnitTime < 0.1124 )
			STOP_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 0.5566 )
			START_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 0.7546 )
			STOP_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 0.9000 )
			START_SPRAY_PAINTING_PARTICLES()
		ELIF( fAnimUnitTime < 1.0 )
			STOP_SPRAY_PAINTING_PARTICLES()
		ENDIF
	ENDIF
ENDPROC

//-------------------- MONKEY PERCEPTION PROCS --------------------
FUNC BOOL IS_PLAYER_IN_MONKEY_PERIPHERAL_VISION()
	RETURN IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-334.465149,-93.421715,48.573284>>, <<-332.264099,-82.785522,45.810303>>, 3.000000 )
	AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-329.113403,-83.872726,45.799889>>, <<-331.093719,-88.053482,48.718784>>, 3.000000 )
ENDFUNC

FUNC BOOL CAN_MONKEY_HEAR_PLAYER( VECTOR vMonkeyPos, VECTOR vPlayerPos )
	FLOAT fNoise = GET_PLAYER_CURRENT_STEALTH_NOISE( PLAYER_ID() )
	FLOAT fUnitNoise = CLAMP( fNoise / 14.0, 0.0, 1.0 )
	FLOAT fDist = VDIST( vMonkeyPos, vPlayerPos )
	FLOAT fUnitDist = CLAMP( 30.0 - fDist, 0.0, 30.0 ) / 30.0
	FLOAT fNoiseByDist = fUnitNoise * fUnitDist
	RETURN fNoiseByDist > 0.25
ENDFUNC

//-------------------- MONKEY BEHAVIOUR PROCS --------------------
FUNC BOOL TASK_MONKEY_FLEE_IF_NOT_PERFORMING_TASK( SCRIPT_TASK_NAME task )
	IF( NOT IS_PED_PERFORMING_TASK( pedMonkeyMan, task )
	AND NOT IS_PED_PERFORMING_TASK( pedMonkeyMan, SCRIPT_TASK_SMART_FLEE_PED ) )
		SAFE_REMOVE_BLIP( blpMonkeyMan )
		TASK_SMART_FLEE_PED( pedMonkeyMan, PLAYER_PED_ID(), 9999.0, -1, TRUE )
		CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Monkey tasked to flee because intended task was interrupted" )
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_MONKEY_STANDUP()
	CONST_INT 		INIT						0
	CONST_INT		STANDUP_INIT				1
	CONST_INT		STANDUP_UPDATE				2
	CONST_INT		NOT_CROUCHED				3
	
	CONST_FLOAT		DEFAULT_START_PHASE			0.349
	CONST_FLOAT		END_PHASE					0.161
	CONST_FLOAT 	CROUCHING_END_PHASE			0.2
	CONST_INT		DEFAULT_STANDUP_TIME		900
	VECTOR			DEFAULT_START_POS			= <<-335.71, -93.39, 46.00>>
	VECTOR			END_POS						= <<-336.7188, -93.5128, 46.000>>
	
	SWITCH( iMonkeyStandupStage )
		CASE INIT
			IF( IS_ENTITY_PLAYING_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouchingLoop ) ) // crouched
				fStartPhase 		= DEFAULT_START_PHASE
				iStandupTime 		= DEFAULT_STANDUP_TIME
				vStart 				= DEFAULT_START_POS
				iMonkeyStandupStage = STANDUP_INIT
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP monkey crouched" )
			ELIF( IS_ENTITY_PLAYING_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch ) ) // crouching
				FLOAT fTimeMultiplier
				fStartPhase 	= GET_ENTITY_ANIM_CURRENT_TIME( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch )
				fStartPhase 	= CLAMP( fStartPhase, CROUCHING_END_PHASE, DEFAULT_START_PHASE )
				IF( fStartPhase <> CROUCHING_END_PHASE )
					// calculate the time me want a full pass on the anim to take
					fTimeMultiplier = ( 1.0 / ( DEFAULT_START_PHASE - END_PHASE ) ) * TO_FLOAT( DEFAULT_STANDUP_TIME )
					// calculate the time time we this particular section of the phase to take
					iStandupTime 	= ROUND( ( fStartPhase - END_PHASE ) * fTimeMultiplier )
					vStart = GET_ENTITY_COORDS( pedMonkeyMan, FALSE ) - <<0.0, 0.0, 1.0>>
					iMonkeyStandupStage = STANDUP_INIT
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP monkey crouching" )
				ELSE // only just started to crouching so we don't need to standup
					iMonkeyStandupStage = NOT_CROUCHED
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP monkey only just started crouching" )
				ENDIF
			ELSE // the monkey isn't crouching/crouched
				iMonkeyStandupStage = NOT_CROUCHED
			ENDIF
		BREAK
		CASE STANDUP_INIT
			FREEZE_ENTITY_POSITION( pedMonkeyMan, TRUE )
			TASK_PLAY_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1,
				AF_DEFAULT, fStartPhase, TRUE )
			iTimer = GET_GAME_TIMER()
			
			SAFE_REMOVE_BLIP( blpMonkeyMan )
			STOP_SPRAY_PAINTING_PARTICLES()
			IF( DOES_ENTITY_EXIST( objMission[ MO_SPRAY_CAN_MONKEY ] ) )
				DETACH_ENTITY( objMission[ MO_SPRAY_CAN_MONKEY ] ) // drop the spray can
			ENDIF
			iMonkeyStandupStage++
			CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP started standup anim task" )
		BREAK
		CASE STANDUP_UPDATE
			IF( IS_ENTITY_PLAYING_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch ) )
				INT 	iAnimTimeElapsed
				FLOAT 	fAnimTimeUnitProgress, fCurrentPhase, fMoveOffsetUnit
				VECTOR 	vCurrent
				
				iAnimTimeElapsed = GET_GAME_TIMER() - iTimer // game time elapsed since the task was given
				fAnimTimeUnitProgress = TO_FLOAT( iAnimTimeElapsed ) / TO_FLOAT( iStandupTime ) // unit time
				fCurrentPhase = fStartPhase - ( ( fStartPhase - END_PHASE ) * fAnimTimeUnitProgress ) // calculate phase for amount of time elapsed
				fCurrentPhase = CLAMP( fCurrentPhase, END_PHASE, fStartPhase ) 
				SET_ENTITY_ANIM_CURRENT_TIME( pedMonkeyMan, sTaggingDict, sTaggingClipCrouch, fCurrentPhase )
				
				// push the ped backwards (against the mover data) so the monkey standing up looks natural
				fMoveOffsetUnit = fAnimTimeUnitProgress - 0.12 // start moving ped backwards after a short wait
				fMoveOffsetUnit = CLAMP( fMoveOffsetUnit, 0.0, 0.8 )					
				vCurrent = LERP_VECTOR( vStart, END_POS, fMoveOffsetUnit )
				SET_ENTITY_COORDS( pedMonkeyMan, vCurrent, FALSE, TRUE )
				
				IF( fCurrentPhase = END_PHASE ) // if the anim has reached the phase we want to stop
					FREEZE_ENTITY_POSITION( pedMonkeyMan, FALSE )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP monkey finished standing up" )
					RETURN TRUE
				ENDIF
			ELSE
				ASSERTLN( "Anim ", sTaggingClipCrouch, " should be playing for monkey!" )
			ENDIF
		BREAK
		CASE NOT_CROUCHED
			SAFE_REMOVE_BLIP( blpMonkeyMan )
			STOP_SPRAY_PAINTING_PARTICLES()
			IF( DOES_ENTITY_EXIST( objMission[ MO_SPRAY_CAN_MONKEY ] ) )
				DETACH_ENTITY( objMission[ MO_SPRAY_CAN_MONKEY ] ) // drop the spray can
			ENDIF
			CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: HANDLE_MONKEY_STANDUP monkey not chrouching/crouched" )
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//PURPOSE: Handles the behaviour of the monkey man
PROC UPDATE_MONKEY_MAN_BEHAVIOUR()
	IF( IS_ENTITY_ALIVE( pedMonkeyMan ) )
		BOOL 	bMonkeyManAlive = IS_ENTITY_ALIVE( pedMonkeyMan )
		BOOL 	bPlayerAlive 	= IS_ENTITY_ALIVE( PLAYER_PED_ID() )
		VECTOR 	vMonkeyManPos 	= GET_ENTITY_COORDS( pedMonkeyMan, FALSE )
		VECTOR 	vPlayerPos
		FLOAT	fAreaRadius 	= 7.5
		VECTOR 	vAreaOffset 	= <<fAreaRadius, fAreaRadius, fAreaRadius>>
		BOOL 	bIsPlayerInVeh
		BOOL 	bIsPlayerInHeli
		IF( bPlayerAlive )
			vPlayerPos 		= GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE )
			bIsPlayerInVeh 	= IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
			bIsPlayerInHeli = IS_PED_IN_ANY_HELI( PLAYER_PED_ID() )
		ENDIF
		
		// Monkey is spooked if a projectile or bullet is fired at him
		IF( iMonkeyManStage < MONKEY_SPOOKED )
			IF( IS_BULLET_IN_AREA( vMonkeyManPos, fAreaRadius, FALSE ) 
			OR IS_PROJECTILE_IN_AREA( vMonkeyManPos - vAreaOffset, vMonkeyManPos + vAreaOffset, FALSE ) )
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Monkey spooked because bullets/projectiles nearby" )
				iMonkeyManStage = MONKEY_SPOOKED
			ENDIF
		ENDIF
	
		SWITCH( iMonkeyManStage )
			CASE 0 // load anims
				REQUEST_ANIM_DICT( sTaggingDict )
				IF( HAS_ANIM_DICT_LOADED( sTaggingDict ) )
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( pedMonkeyMan, TRUE )
					TASK_PLAY_ANIM( pedMonkeyMan, sTaggingDict, sTaggingClipShakingLoop, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
						-1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED ) // play shaking spray can anim
					iMonkeyManStage++
				ENDIF
			BREAK
			CASE 1 // when the player is close enough monkey will start spray painting
				IF( TASK_MONKEY_FLEE_IF_NOT_PERFORMING_TASK( SCRIPT_TASK_PLAY_ANIM ) )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Forced to flee in iMonkeyManStage:", iMonkeyManStage )
					iMonkeyManStage = 5		
				ENDIF
			
				#IF IS_DEBUG_BUILD
				IF( HAS_PTFX_ASSET_LOADED() )
				#ENDIF
					IF( IS_ENTITY_WITHIN_DISTANCE_OF_ENTITY( PLAYER_PED_ID(), pedMonkeyMan, 35.0 ) )
						OPEN_SEQUENCE_TASK( seq )
							TASK_PLAY_ANIM( NULL, sTaggingDict, sTaggingClipCrouch, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
								-1, AF_EXIT_AFTER_INTERRUPTED )
							TASK_PLAY_ANIM( NULL, sTaggingDict, sTaggingClipCrouchingLoop, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT,
								-1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED )
						CLOSE_SEQUENCE_TASK( seq )
						TASK_PERFORM_SEQUENCE( pedMonkeyMan, seq )
						CLEAR_SEQUENCE_TASK( seq )
						REMOVE_ANIM_DICT( sTaggingDict )
						iMonkeyManStage++
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					ASSERTLN( "Particle effects should have loaded by now!" )
				ENDIF
				#ENDIF
			BREAK
			CASE 2 // wait and see if the player spooks the monkey
				IF( TASK_MONKEY_FLEE_IF_NOT_PERFORMING_TASK( SCRIPT_TASK_PERFORM_SEQUENCE ) )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Forced to flee in iMonkeyManStage:", iMonkeyManStage )
					iMonkeyManStage = 5		
				ENDIF
				
				FLOAT fDist				
				IF( bIsPlayerInHeli )
					fDist = 30.0
				ELIF( bIsPlayerInVeh )
					fDist = 17.5
				ELSE
					fDist = 5.0
				ENDIF
				
				IF( bMonkeyManAlive AND bPlayerAlive 
				AND IS_ENTITY_WITHIN_DISTANCE_OF_ENTITY( PLAYER_PED_ID(), pedMonkeyMan, fDist ) )
					iMonkeyManStage = MONKEY_SPOOKED
				ENDIF
				IF( bMonkeyManPhotographed )
					iMonkeyManStage = MONKEY_SPOOKED
				ENDIF
				IF( IS_PLAYER_IN_MONKEY_PERIPHERAL_VISION() )
					iMonkeyManStage = MONKEY_SPOOKED
				ENDIF
				IF( bPlayerAlive AND CAN_MONKEY_HEAR_PLAYER( vPlayerPos, vMonkeyManPos ) )
					iMonkeyManStage = MONKEY_SPOOKED
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF( iMonkeyManStage = MONKEY_SPOOKED )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Monkey spooked" )
				ENDIF
				#ENDIF
				
				UPDATE_SPRAY_PAINT_PARTICLES()
			BREAK
			CASE MONKEY_SPOOKED // the monkey has been spooked so stand up, turn around and then flee
				// update the standing up anim
				IF( HANDLE_MONKEY_STANDUP() )
					iMonkeyManStage++
				ENDIF
			BREAK
			CASE 4 // turn towards the player then flee
				SET_PED_FLEE_ATTRIBUTES( pedMonkeyMan, FA_PREFER_PAVEMENTS, TRUE )
				SET_PED_FLEE_ATTRIBUTES( pedMonkeyMan, FA_LOOK_FOR_CROWDS, TRUE )
				
				OPEN_SEQUENCE_TASK( seq )					
					TASK_TURN_PED_TO_FACE_ENTITY( NULL, PLAYER_PED_ID(), 800 )
					TASK_SMART_FLEE_PED( NULL, PLAYER_PED_ID(), 9999.0, -1, TRUE )
				CLOSE_SEQUENCE_TASK( seq )
				TASK_PERFORM_SEQUENCE( pedMonkeyMan, seq )
				CLEAR_SEQUENCE_TASK( seq )
				iMonkeyManStage++
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Tasked monkey to turn and flee" )
			BREAK
			CASE 5 // if the monkey is more than 50m away and not on screen start a shapetest between the player position and monkey
				IF( TASK_MONKEY_FLEE_IF_NOT_PERFORMING_TASK( SCRIPT_TASK_PERFORM_SEQUENCE ) )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Forced to flee in iMonkeyManStage:", iMonkeyManStage )
					iMonkeyManStage = 5		
				ENDIF
				
				IF( NOT IS_ENTITY_ON_SCREEN( pedMonkeyMan )
				AND VDIST2( vMonkeyManPos, GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ) ) > 2500.0 )
					sti = START_SHAPE_TEST_LOS_PROBE( vPlayerPos, vMonkeyManPos, SCRIPT_INCLUDE_MOVER )
					iMonkeyManStage++
				ENDIF
			BREAK
			CASE 6 // if there's something blocking the LOS between the player position and monkey then go to next stage
				IF( TASK_MONKEY_FLEE_IF_NOT_PERFORMING_TASK( SCRIPT_TASK_PERFORM_SEQUENCE ) )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Forced to flee in iMonkeyManStage:", iMonkeyManStage )
					iMonkeyManStage = 5
				ENDIF
			
				INT 				iHitSomething
				VECTOR 				vPos
				VECTOR				vNormal
				ENTITY_INDEX 		entity
				SHAPETEST_STATUS	status
				
				status = GET_SHAPE_TEST_RESULT( sti, iHitSomething, vPos, vNormal, entity )
				IF( status = SHAPETEST_STATUS_RESULTS_READY )
					IF( iHitSomething = 1 )
						iMonkeyManStage++
					ELSE
						
						iMonkeyManStage--
					ENDIF
				ELIF( status = SHAPETEST_STATUS_NONEXISTENT )
					iMonkeyManStage++
				ENDIF				
			BREAK
			CASE 7 // delete monkey
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Cleaning up monkey" )
				DELETE_PED( pedMonkeyMan )			
			BREAK
		ENDSWITCH
	ELSE
		IF( DOES_ENTITY_EXIST( pedMonkeyMan ) )
			FREEZE_ENTITY_POSITION( pedMonkeyMan, FALSE )
		ENDIF
	ENDIF
ENDPROC

//-------------------- BACKEND PROCS --------------------
PROC CLEANUP_MISSION()
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Cleaning up script" )
	REMOVE_PTFX_ASSET()
	SAFE_REMOVE_BLIP( blpMonkeyMan )
	RANDOM_EVENT_OVER()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC CLEANUP_MISSION_FOR_SKIP()
	IF( DOES_ENTITY_EXIST( pedMonkeyMan ) )
		DELETE_PED( pedMonkeyMan )
	ENDIF
	INT i
	REPEAT NUM_OBJECTS i
		IF( DOES_ENTITY_EXIST( objMission[ i ] ) )
			DELETE_OBJECT( objMission[ i ] )
		ENDIF
	ENDREPEAT
	iMonkeyManStage = 0
	bMonkeyManPhotographed = FALSE
	iMonkeyStandupStage = 0
ENDPROC

//PURPOSE: Runs a set of checks to see if it's ok for re_Monkey to launch
FUNC BOOL SHOULD_THIS_RANDOM_EVENT_LAUNCH( VECTOR vPos )
	IF( CAN_RANDOM_EVENT_LAUNCH( vPos, RE_MONKEYPHOTO ) )
		IF( IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED( RC_TONYA_1 ) )
			IF( IS_CURRENT_PLAYER_GARAGE_PURCHASED() )
				IF( IS_LAST_GEN_PLAYER() )
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: all SHOULD_THIS_RANDOM_EVENT_LAUNCH checks passed" )
					RETURN TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: The current player's isn't CGtoNG" )
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: The current player's garage hasn't been purchased" )
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: RC_TONYA_1 is incomplete" )
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: CAN_RANDOM_EVENT_LAUNCH checks failed" )
	#ENDIF
	ENDIF
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: one or more of SHOULD_THIS_RANDOM_EVENT_LAUNCH checks failed" )
	RETURN FALSE
ENDFUNC

PROC INIT_MISSION( VECTOR vPos )
	IF( SHOULD_THIS_RANDOM_EVENT_LAUNCH( vPos ) )
		LAUNCH_RANDOM_EVENT( RE_MONKEYPHOTO )
	ELSE
		CLEANUP_MISSION()
	ENDIF

	REQUEST_PTFX_ASSET()
ENDPROC

PROC UPDATE_MISSION()
	UPDATE_MONKEY_MAN_BEHAVIOUR()
	
	// cleanup the script if the player goes away before spooking the monkey
	IF( NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE() AND iMonkeyManStage < MONKEY_SPOOKED )
		CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Player left world point range. Cleaning up..." )
		CLEANUP_MISSION()
	ENDIF
ENDPROC

//-------------------- DEBUG PROCS --------------------
#IF IS_DEBUG_BUILD

FUNC BOOL VECTOR_EQUAL( VECTOR a, VECTOR b )
	RETURN a.x = b.x AND a.y = b.y AND a.z = b.z
ENDFUNC

FUNC STRING GET_CURRENT_PLAYER_NAME_AS_STRING()
	MODEL_NAMES model = GET_ENTITY_MODEL( PLAYER_PED_ID() )
	SWITCH( model )
		CASE PLAYER_ZERO	RETURN "Michael"
		CASE PLAYER_ONE		RETURN "Franklin"
		CASE PLAYER_TWO		RETURN "Trevor"
		DEFAULT				RETURN "Unknown"
	ENDSWITCH
ENDFUNC

FUNC STRING DEBUG_GET_MISSION_STAGE_AS_STRING( MISSION_STAGE ms )
	SWITCH( ms )
		CASE MS_DISCOVER			RETURN "MS_DISCOVER"
		CASE MS_COMPLETE			RETURN "MS_COMPLETE"
		DEFAULT						RETURN "UNKNOWN"
	ENDSWITCH
ENDFUNC

PROC DEBUG_INIT_MISSION()
	// Widget creation
	IF( NOT DOES_WIDGET_GROUP_EXIST( widMainGroup ) )
		widMainGroup = START_WIDGET_GROUP( "re_Monkey" )
			ADD_WIDGET_BOOL( "Spook Monkey", bSpookMonkey )
			ADD_WIDGET_VECTOR_SLIDER( "Spray Can Offset Position", vSprayCanPosOffset, -0.5, 0.5, 0.01 )
			ADD_WIDGET_VECTOR_SLIDER( "Spray Can Offset Rotation", vSprayCanRotOffset, -180.0, 180.0, 1.0 )
		STOP_WIDGET_GROUP()
	ENDIF
	
	// Populate z skipping stage text
	INT i
	REPEAT NUM_STAGES i
		DebugMenuStruct[ i ].sTxtLabel = DEBUG_GET_MISSION_STAGE_AS_STRING( INT_TO_ENUM( MISSION_STAGE, i ) )
	ENDREPEAT	
ENDPROC

FUNC STRING DEBUG_GET_MISSION_STAGE_PHASE_AS_STRING( MISSION_STAGE_PHASE msp )
	SWITCH( msp )
		CASE MSP_INIT				RETURN "MSP_INIT"
		CASE MSP_LEGACY_POPULATE	RETURN "MSP_LEGACY_POPULATE"
		CASE MSP_BODY				RETURN "MSP_BODY"
		DEFAULT						RETURN "UNKNOWN"
	ENDSWITCH
ENDFUNC

//PURPOSE: Jumps the mission to a specified mission stage
PROC MISSION_JUMP_TO_MISSION_STAGE( MISSION_STAGE ms )
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: MISSION_JUMP_TO_MISSION_STAGE jumping to ", DEBUG_GET_MISSION_STAGE_AS_STRING( ms ) )
	eMissionStage = ms
	eMissionStagePhase = MSP_INIT	
	iBodyStage = 0
	bSkipHappening = TRUE
ENDPROC

PROC DEBUG_UPDATE_MISSION()
	// zjp skips
	INT iDebugMenuMissionStage
	IF( LAUNCH_MISSION_STAGE_MENU( DebugMenuStruct, iDebugMenuMissionStage, ENUM_TO_INT( eMissionStage ), TRUE ) )
		DO_SCREEN_FADE_OUT( 0 )
		CLEANUP_MISSION_FOR_SKIP()
		MISSION_JUMP_TO_MISSION_STAGE( INT_TO_ENUM( MISSION_STAGE, iDebugMenuMissionStage ) )
	ENDIF
	
	// fail and pass 
	IF( IS_KEYBOARD_KEY_JUST_PRESSED( KEY_F ) )
		CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Debug fail" )
		CLEANUP_MISSION_FOR_SKIP()
		CLEANUP_MISSION()
	ELIF( IS_KEYBOARD_KEY_JUST_PRESSED( KEY_S ) )
		CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Debug pass" )
		CLEANUP_MISSION_FOR_SKIP()
		eMissionStagePhase = MSP_BODY
		eMissionStage = MS_COMPLETE
		iBodyStage = 0
	ENDIF

	// widget update
	IF( NOT VECTOR_EQUAL( vSprayCanPosOffset, vSprayCanPosOffsetOld )
	OR  NOT VECTOR_EQUAL( vSprayCanRotOffset, vSprayCanRotOffsetOld ) )
		vSprayCanPosOffsetOld = vSprayCanPosOffset
		vSprayCanRotOffsetOld = vSprayCanRotOffset
		
		DETACH_ENTITY( objMission[ MO_SPRAY_CAN_MONKEY ] )
		ATTACH_ENTITY_TO_ENTITY( objMission[ MO_SPRAY_CAN_MONKEY ], pedMonkeyMan, 
			GET_PED_BONE_INDEX( pedMonkeyMan, BONETAG_PH_R_HAND ),	vSprayCanPosOffset, vSprayCanRotOffset, TRUE, TRUE )
	ENDIF
	IF( bSpookMonkey )
		iMonkeyManStage = MONKEY_SPOOKED
		bSpookMonkey = FALSE
	ENDIF
ENDPROC

#ENDIF

//-------------------- BACKEND PROCS --------------------
//PURPOSE: Increments the mission stage as defined in MISSION_STAGE, with checks against running past the last stage
PROC MISSION_GO_TO_NEXT_MISSION_STAGE()
	INT iMissionStage = ENUM_TO_INT( eMissionStage ) + 1
	IF( iMissionStage >= 0 AND iMissionStage < COUNT_OF( MISSION_STAGE ) )
		eMissionStage = INT_TO_ENUM( MISSION_STAGE, iMissionStage )
	ENDIF
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: eMissionStage updated to ", DEBUG_GET_MISSION_STAGE_AS_STRING( eMissionStage ) )
ENDPROC

// Mission phases go as follows:
// MSP_INIT 			- Setup any stuff that the mission stage needs, called first
// MSP_LEGACY_POPULATE 	- Only called once if a skip/repeat/replay is requested, used to spawn entities and changes
//						  values that would have existed had the mission been played naturally to this point
// MSP_BODY				- Where the actual mission stage work takes place
//PURPOSE: Updates the mission phase, this is distinct from the mission stage. Each mission stage has three phases. More info...
PROC MISSION_GO_TO_NEXT_MISSION_STAGE_PHASE()
	SWITCH( eMissionStagePhase )
		CASE MSP_INIT		
			#IF IS_DEBUG_BUILD
			IF( bSkipHappening )
				eMissionStagePhase = MSP_LEGACY_POPULATE
			ELSE
			#ENDIF
				eMissionStagePhase = MSP_BODY
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		BREAK
		CASE MSP_LEGACY_POPULATE	eMissionStagePhase = MSP_BODY 	BREAK
		CASE MSP_BODY				eMissionStagePhase = MSP_INIT 	BREAK
	ENDSWITCH
	#IF IS_DEBUG_BUILD
	bSkipHappening = FALSE
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: eMissionStagePhase updated to ", DEBUG_GET_MISSION_STAGE_PHASE_AS_STRING( eMissionStagePhase ) )
	#ENDIF
ENDPROC

//PURPOSE: Updates the mission stage and phase
PROC MISSION_NEXT_STAGE()
	IF( eMissionStagePhase = MSP_BODY )
		MISSION_GO_TO_NEXT_MISSION_STAGE()
		iBodyStage = 0
	ENDIF
	MISSION_GO_TO_NEXT_MISSION_STAGE_PHASE()
ENDPROC

//-------------------- ENTITY CREATIONS PROCS --------------------
FUNC BOOL CREATE_MONKEY_MAN()
	IF( DOES_ENTITY_EXIST( pedMonkeyMan ) )
		RETURN TRUE
	ELSE
		REQUEST_MODEL( U_M_M_STREETART_01 )
		IF( HAS_MODEL_LOADED( U_M_M_STREETART_01 ) )
			pedMonkeyMan = CREATE_PED( PEDTYPE_MISSION, U_M_M_STREETART_01, vMonkeyManStartPos, fMonkeyManStartHeading )
			SET_MODEL_AS_NO_LONGER_NEEDED( U_M_M_STREETART_01 )
			objMission[ MO_SPRAY_CAN_MONKEY ] = CREATE_OBJECT( PROP_CS_SPRAY_CAN, vObjCreatePos )
			ATTACH_ENTITY_TO_ENTITY( objMission[ MO_SPRAY_CAN_MONKEY ], pedMonkeyMan, 
				GET_PED_BONE_INDEX( pedMonkeyMan, BONETAG_PH_R_HAND ), <<0.0, -0.01, -0.02>>, vZero, TRUE, TRUE )
			SET_PED_KEEP_TASK( pedMonkeyMan, TRUE )
			SET_PED_CONFIG_FLAG( pedMonkeyMan, PCF_DontInfluenceWantedLevel, TRUE )
			blpMonkeyMan = CREATE_BLIP_FOR_PED( pedMonkeyMan, FALSE )
			SET_RANDOM_EVENT_ACTIVE()
			SET_PED_RELATIONSHIP_GROUP_HASH( pedMonkeyMan, RELGROUPHASH_PLAYER )
			SET_PED_LOD_MULTIPLIER( pedMonkeyMan, 2.5 )			
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CREATE_SCENE_OBJECTS()
	INT i
	REPEAT NUM_OBJECTS i
		MISSION_OBJECTS mo = INT_TO_ENUM( MISSION_OBJECTS, i ) 
		SWITCH( mo )
			CASE MO_SPRAY_CAN_MONKEY
			BREAK
			CASE MO_PAINT_CAN_0
				objMission[ mo ] = CREATE_OBJECT( PROP_PAINTS_CAN02, <<-335.66, -95.24, 46.16>> )
				SET_ENTITY_COORDS_NO_OFFSET( objMission[ mo ], <<-335.66, -95.24, 46.16>> )
				SET_ENTITY_ROTATION( objMission[ mo ], <<-2.32, 0.66, -0.80>> )
			BREAK
			CASE MO_PAINT_BRUSH
				objMission[ mo ] = CREATE_OBJECT( PROP_PAINT_BRUSH05, <<-336.04, -94.90, 46.02>> )
				SET_ENTITY_COORDS_NO_OFFSET( objMission[ mo ], <<-336.04, -94.90, 46.02>> )
				SET_ENTITY_ROTATION( objMission[ mo ], <<0.01, 3.85, 2.72>> )
			BREAK
			CASE MO_SPRAY_CAN_0
				objMission[ mo ] = CREATE_OBJECT( PROP_PAINT_SPRAY01B, <<-336.34, -95.66, 46.02>> )
				SET_ENTITY_COORDS_NO_OFFSET( objMission[ mo ], <<-336.34, -95.66, 46.02>> )
				SET_ENTITY_ROTATION( objMission[ mo ], <<-2.86, 0.76, 28.89>> )
			BREAK
		ENDSWITCH		
	ENDREPEAT
ENDPROC

//-------------------- MISSION STAGE PROCS --------------------
//PURPOSE:  Monkey spray painting wall, then flees 
FUNC BOOL MS_DISCOVER_FUNC()
	IF( eMissionStagePhase = MSP_INIT )
		RETURN TRUE
		
	ELIF( eMissionStagePhase = MSP_LEGACY_POPULATE )
		// set player and camera position for skip
		IF( IS_ENTITY_ALIVE( PLAYER_PED_ID() ) )
			SET_ENTITY_COORD_AND_HEADING( PLAYER_PED_ID(), <<-366.6139, -79.7531, 44.6616>>, 230.7299 )
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		DO_SCREEN_FADE_IN( DEFAULT_FADE_TIME_SHORT )
		RETURN TRUE
	
	ELIF( eMissionStagePhase = MSP_BODY )
		IF( DOES_ENTITY_EXIST( pedMonkeyMan ) ) 
			IF( IS_PED_DEAD_OR_DYING( pedMonkeyMan ) ) // monkey is injured at any point so cleanup
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Monkey dead or dying so cleaning up" )
				CLEANUP_MISSION()
			ENDIF
		ELSE
			IF( iBodyStage > 0 )
				IF(	bMonkeyManPhotographed ) // monkey photographed and cleaned up
					RETURN TRUE
				ELSE // monkey not photographed but has been cleaned up
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Monkey cleaned up and not photographed so cleaning up" )
					CLEANUP_MISSION()
				ENDIF
			ENDIF
		ENDIF
	
		SWITCH( iBodyStage )
			CASE 0 // create mission entities
				IF( CREATE_MONKEY_MAN() )
					CREATE_SCENE_OBJECTS()
					CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Assets spawned" )
					iBodyStage++
				ENDIF
			BREAK
			CASE 1 // check for picture of monkey 
				IF( HAS_CELLPHONE_CAM_JUST_TAKEN_PIC() )
					IF( IS_ENTITY_ALIVE( pedMonkeyMan ) )
						IF( GET_FOCUS_PED_ON_SCREEN() = pedMonkeyMan )						
							iTimer = GET_GAME_TIMER() + 2000
							iBodyStage++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2 // set monkey photographed after two seconds
				IF( iTimer < GET_GAME_TIMER() )
					bMonkeyManPhotographed = TRUE
					iBodyStage++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Sets re complete and fires text message
FUNC BOOL MS_COMPLETE_FUNC()
	IF( eMissionStagePhase = MSP_INIT )
		RETURN TRUE
		
	ELIF( eMissionStagePhase = MSP_LEGACY_POPULATE )
		// set player and camera position for skip, create objects
		CREATE_SCENE_OBJECTS()
		IF( IS_ENTITY_ALIVE( PLAYER_PED_ID() ) )
			SET_ENTITY_COORD_AND_HEADING( PLAYER_PED_ID(), <<-347.4067, -93.5801, 44.6639>>, 306.0178 )
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		DO_SCREEN_FADE_IN( DEFAULT_FADE_TIME_SHORT )
		RETURN TRUE
	
	ELIF( eMissionStagePhase = MSP_BODY )
		SWITCH( iBodyStage )
			CASE 0
				CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Complete" )
				CANCEL_COMMUNICATION(TEXT_MONKEY_MOSAIC_LAMAR) //Cancel the ambient flavour text from Lamar if we get this far before it sends.
				SEND_TEXT_MESSAGE() // text message informs player that car reward is in garage
				RANDOM_EVENT_PASSED() // pass the re
				RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS() // save
				CLEANUP_MISSION()
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

//-------------------- MAIN LOOP --------------------
SCRIPT( coords_struct in_coords )
	CPRINTLN( DEBUG_SIMON, " >>> re_Monkey: Starting" )
	IF( HAS_FORCE_CLEANUP_OCCURRED( DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS ) )
		CLEANUP_MISSION()
	ENDIF
	
	INIT_MISSION( in_coords.vec_coord[ 0 ] )
	#IF IS_DEBUG_BUILD
	DEBUG_INIT_MISSION()
	#ENDIF
	
	WHILE( TRUE )
		UPDATE_MISSION()
		#IF IS_DEBUG_BUILD
		DEBUG_UPDATE_MISSION()
		#ENDIF
		
		SWITCH( eMissionStage )
			CASE MS_DISCOVER
				IF( MS_DISCOVER_FUNC() )
					MISSION_NEXT_STAGE()
				ENDIF
			BREAK
			CASE MS_COMPLETE
				IF( MS_COMPLETE_FUNC() )
					MISSION_NEXT_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		WAIT( 0 )
	ENDWHILE	
ENDSCRIPT
