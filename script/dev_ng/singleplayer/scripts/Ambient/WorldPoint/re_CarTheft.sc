//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "script_blips.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "AggroSupport.sch"
USING "LineActivation.sch"
USING "finance_control_public.sch"
USING "context_control_public.sch"
USING "commands_debug.sch"
USING "flow_public_core_override.sch"
USING "rc_helper_functions.sch"

USING "script_maths.sch"
USING "Ambient_Common.sch"


USING "context_control_public.sch"
USING "commands_cutscene.sch"

//Steve T added this.
USING "dialogue_public.sch"
USING "Ambient_Speech.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "locates_public.sch"

USING "random_events_public.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
    ambCanRun,
    ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM carStolenStageFlag
    waitingForTheft,
    thiefIsGettingAway,
    carCaught,
    carReturned
ENDENUM
carStolenStageFlag carStolenStage = waitingForTheft

ENUM victimResponseStageFlag
    turnsToPlayer,
	gestureToPlayer,
	waitPlayerChase,
    waitsOnReturn,
    carIsReturned
ENDENUM
victimResponseStageFlag victimResponseStage = turnsToPlayer

ENUM returnCutStageFlag
	stopTheCar,
    setTheScene,
    runTasks,
	startConvo,
    cleanupCut,
	waitToBlend
ENDENUM
returnCutStageFlag returnCutStage = stopTheCar

VECTOR vInput
FLOAT fInput

FLOAT fLocateSize = 10, fStoppingDistance = 7

BOOL bAgitatedCanReset=TRUE, bExcitedCanReset, bVictimReset, bTopVariation, bPlayedCutDialogue

BLIP_INDEX blipScene, blipThief, blipVehicle
int iVictimBlipFlash

AI_BLIP_STRUCT blipStructRobber

PED_INDEX pedVictim, pedThief
VEHICLE_INDEX vehStolen
SEQUENCE_INDEX seq
CAMERA_INDEX camStart

VEHICLE_INDEX vehPlayerStart

BOOL bHasBeenBumped
INT bumpedCounter//, , excitedCounter
//STRING animExcitedDict = "RANDOM@CAR_THIEF@EXCITED@"
//STRING animAgitatedDict = "RANDOM@CAR_THIEF@AGITATED@IDLE_A"
//STRING animAgBaseDict = "RANDOM@CAR_THIEF@AGITATED@BASE"

STRING animAgitatedIG
STRING animPointsIG = "random@car_thief@waving_ig_1"	// "arms_waving", "pointing"
STRING animPointDir
STRING sPointAnim


VECTOR vPlayer, vCar, vVictim, vTop, vBottom, vWalkAway
FLOAT fCar, fVictim, storedDistance, storedPlayerDistance, storedPlayerVehDistance, fTop, fBottom, fFOV
VECTOR vStartPos, vStartRot

FLOAT vBoostSpeed = 1.0

STRING playerVoice, playerDialogue//, playerGolf
structPedsForConversation dialogueStruct
STRING sAttract, sHelp, sFYou, sJoy, sOK, sBad, sGood//, sOut
STRING sTextBlock
BOOL attractDialoguePlayed, excitedDialoguePlayed, helpdDialoguePlayed, bDriveBy

DRIVINGMODE victimDrivingmode = DRIVINGMODE_STOPFORCARS

DRIVINGMODE thiefDrivingmode = DRIVINGMODE_AVOIDCARS //DF_SwerveAroundAllCars//|DF_AlwaysDriveInFastLane
CONST_INT ONE_PINK		1
CONST_INT TWO_GOLFER	2
INT iSelectedVariation, sceneID, iDialogueTimer

INT iPleaTimer
INT iBlendTimer

STRING animDict, carAnim = "car_returned_peyote", girlAnim = "girl_car_returned", playerAnim = "player_car_returned"
VECTOR scenePosition, sceneRotation
BOOL bDonePlayerBlend = FALSE

VECTOR vWheelPosL
VECTOR vLocalWheelPosL

VECTOR vWheelPosR
VECTOR vLocalWheelPosR

REL_GROUP_HASH relGroupThief

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

// Functions ----------------------------------------------//

PROC missionCleanup()
  	
	IF DOES_ENTITY_EXIST(pedThief)
  	AND NOT IS_ENTITY_DEAD(pedThief) 
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedThief, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
		SAFE_RELEASE_PED(pedThief)
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Cleanup - SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))")
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedVictim)
  	AND NOT IS_ENTITY_DEAD(pedVictim)
		SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipThief)
        REMOVE_BLIP(blipThief)
    ENDIF
	CLEANUP_AI_PED_BLIP(blipStructRobber)
    IF DOES_BLIP_EXIST(blipVehicle)
        REMOVE_BLIP(blipVehicle)
    ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("RE_CAR_STEAL_SCENE")
		IF IS_VEHICLE_OK(vehStolen)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehStolen)
		ENDIF
		STOP_AUDIO_SCENE("RE_CAR_STEAL_SCENE")
	ENDIF
	
	IF iSelectedVariation = TWO_GOLFER
		REMOVE_VEHICLE_ASSET(ROCOTO)
	ELSE
		REMOVE_VEHICLE_ASSET(PEYOTE)
	ENDIF
	
	SAFE_RELEASE_VEHICLE(vehStolen)
	
	IF IS_VEHICLE_OK(vehPlayerStart)
		SET_VEHICLE_TYRES_CAN_BURST(vehPlayerStart, TRUE)
	ENDIF
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1)
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, FALSE)	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, FALSE)
	
	RANDOM_EVENT_OVER()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
    TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL LOADED_AND_CREATED_CAR_THEFT_SCENE()
	VECTOR vTemp, vEscapeVehicle = vBottom
	FLOAT fEscapeVehicle = fBottom
    MODEL_NAMES modelVictim
    MODEL_NAMES modelThief
    MODEL_NAMES modelVehicle
	STRING victimVoice, victimAmbientVoice, thiefAmbientVoice
		
	SWITCH iSelectedVariation
		CASE ONE_PINK
			vTop = <<-2257.4817, 4266.6382, 44.5095>>
			fTop = 293.9091 
			vBottom = <<-2253.7632, 4273.1699, 44.9770>>
			fBottom = 185.5212
			modelVictim 		= A_F_Y_VINEWOOD_04
			modelVehicle 		= PEYOTE
			modelThief		 	= G_M_Y_SalvaGoon_01
			victimVoice			= "CThiefVictim"
			victimAmbientVoice	= "A_F_Y_VINEWOOD_04_WHITE_MINI_02"
			sAttract = "RECT1_ATTR" sHelp = "RECT1_HELP" sFYou = "RECT1_FYOU" //sOut = "RECT1_OUT" 
			sJoy = "RECT1_JOY" sOK = "RECT1_OK" sBad = "RECT1_BAD" sGood = "RECT1_GOOD"
			sTextBlock = "RECT1AU"
			animAgitatedIG = "RANDOM@CAR_THIEF@waiting_ig_4"
			animPointDir = "random@car_thief@waving_ig_1"
			vWalkAway = <<-2270.6321, 4287.4370, 44.9008>>
			fStoppingDistance = 3
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTop) < VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBottom)
				vEscapeVehicle = vTop
				fEscapeVehicle = fTop
				bTopVariation = TRUE
				animPointsIG = "random@car_thief@waving_ig_2"
				sPointAnim = "waving_l"
			ELSE
				sPointAnim = "waving"
				animPointsIG = "random@car_thief@waving_ig_1"
				vEscapeVehicle = vBottom
				fEscapeVehicle = fBottom
			ENDIF
		BREAK
		CASE TWO_GOLFER
			vBottom = <<-538.6718, -2183.4421, 5.2336>> //coord closest to bottom (airport etc)
			vTop = <<-365.7000, -2179.2603, 9.3184>> //coord closest to top
			modelVictim 	= A_F_Y_INDIAN_01
			modelVehicle 	= ROCOTO
			modelThief 		= A_M_M_SOUCENT_04
			victimVoice		= "CThiefGolfer"
			thiefAmbientVoice = "A_M_M_SOUCENT_04_BLACK_MINI_01"
			sAttract = "RECT2_ATTR" sHelp = "RECT2_HELP" sFYou = "RECT2_FYOU" //sOut = "RECT2_OUT" 
			sJoy = "RECT2_JOY" sOK = "RECT2_OK" sBad = "RECT2_BAD" sGood = "RECT2_GOOD"
			sTextBlock = "RECT2AU"
			animAgitatedIG = "RANDOM@CAR_THIEF@waiting_ig_4" //"RANDOM@CAR_THIEF@AGITATED_IG_6" 	// "agitated_loop_a", "agitated_loop_a_ends_in_base"
			animPointsIG = "RANDOM@CAR_THIEF@VICTIMPOINTS_IG_3"	// "arms_waving", "pointing"
			animPointDir = "RANDOM@CAR_THIEF@VICTIMPOINTS_IG_3"
			vWalkAway = <<-505.1966, -2159.2275, 7.6466>>
			fLocateSize = 15
			fStoppingDistance = 3
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()),vTop) < VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()),vBottom)
				bTopVariation = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	//<<-317.7578, -2152.9343, 9.3217>>, 72.6127   //debug warp other way
	//<<-627.0545, -2289.5554, 4.8096>>, 319.7386  //debug warp airport
	
    REQUEST_MODEL(modelVictim)
    REQUEST_MODEL(modelThief)
    REQUEST_MODEL(modelVehicle)
    REQUEST_ANIM_DICT(animAgitatedIG)
	REQUEST_ANIM_DICT(animPointsIG)
	REQUEST_ANIM_DICT(animPointDir)
	REQUEST_ANIM_DICT("RANDOM@CAR_THIEF@WAITING_IG_4")
	REQUEST_PTFX_ASSET()
	
    IF HAS_MODEL_LOADED (modelVictim)
    AND HAS_MODEL_LOADED(modelThief)
    AND HAS_MODEL_LOADED(modelVehicle)
    AND HAS_ANIM_DICT_LOADED(animAgitatedIG)
	AND HAS_ANIM_DICT_LOADED(animPointsIG)
	AND HAS_ANIM_DICT_LOADED(animPointDir)
	AND HAS_ANIM_DICT_LOADED("RANDOM@CAR_THIEF@WAITING_IG_4")
	AND HAS_PTFX_ASSET_LOADED()
    	IF iSelectedVariation = ONE_PINK
			
			printDebugStringAndFloat("re_cartheft - Distance to vTop = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTop))
			printDebugStringAndFloat("re_cartheft - Distance to vBottom = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBottom))
			
			vehStolen = CREATE_VEHICLE(modelVehicle, vEscapeVehicle, fEscapeVehicle)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehStolen, SC_DOOR_FRONT_LEFT, FALSE)
			SET_VEHICLE_TYRES_CAN_BURST(vehStolen, FALSE)
			SET_VEHICLE_HAS_STRONG_AXLES(vehStolen,TRUE)
			SET_VEHICLE_CAN_LEAK_OIL(vehStolen, FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(vehStolen, FALSE)
			SET_VEHICLE_EXTRA(vehStolen, 2, FALSE)
			
			pedThief = CREATE_PED_INSIDE_VEHICLE(vehStolen, PEDTYPE_CRIMINAL, modelThief)
			SET_PED_CONFIG_FLAG(pedThief, PCF_DontInfluenceWantedLevel, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_LEAVE_VEHICLES, TRUE)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1, TRUE)
			SET_PED_ACCURACY(pedThief,10)
			SET_DRIVER_ABILITY(pedThief,1)
			SET_DRIVER_RACING_MODIFIER(pedThief,1)
			SET_VEHICLE_COLOURS(vehStolen, 135, 135) // Hot pink Peyote
			ADD_RELATIONSHIP_GROUP("re_cartheft relGroupThief", relGroupThief)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupThief, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupThief, RELGROUPHASH_CIVMALE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_CIVMALE, relGroupThief)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedThief, relGroupThief)
//			SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehStolen, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehStolen, TRUE)
			//TASK_VEHICLE_TEMP_ACTION(pedThief, vehStolen, TEMPACT_BURNOUT, 120000)

			// Create victim facing the player
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vInput
				fInput = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
			ENDIF
			pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, vInput, fInput)
			//SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, RELGROUPHASH_PLAYER)
			SET_AMBIENT_VOICE_NAME(pedVictim, victimAmbientVoice)
			TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
			//TASK_PLAY_ANIM(pedVictim, animPointsIG, "waving", DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE|AF_LOOPING)
			//SET_PED_CAN_RAGDOLL(pedVictim,FALSE)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
			STOP_PED_SPEAKING(pedVictim, TRUE) //Stop non script related speech. 
			
			IF bTopVariation
			    vCar = << -2254.4131, 4270.2271, 44.8028 >> 
				fCar = 124.6557   
				vPlayer = << -2252.6831, 4274.1597, 45.0612 >> 
				vVictim = << -2257.8250, 4269.1890, 44.6334 >> 
				fVictim = 272.8688 
				
				vStartPos   = <<-2258.040283,4272.243652,45.754662>>
				vStartRot   = <<1.729481,-0.000000,-114.935196>>
			ELSE
				vCar = << -2253.6340, 4269.1191, 44.7686 >> 
				fCar = 336.1430
				vVictim = << -2255.7329, 4271.3198, 44.8166 >> 
				fVictim = 179.4774	
				vPlayer = << -2257.0879, 4268.9380, 44.6456 >>

				vStartPos   = <<-2254.886475,4273.539063,46.228199>>
				vStartRot   = <<-7.492095,-0.000000,-168.190872>>
			ENDIF
		ELSE
			IF bTopVariation
				vehStolen = CREATE_VEHICLE(modelVehicle, <<-488.7506, -2159.3254, 8.2581>>, 15.0886)
			ELSE	
				vehStolen = CREATE_VEHICLE(modelVehicle, <<-486.3563, -2159.9421, 8.2887>>, 288.8187)
			ENDIF
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehStolen, SC_DOOR_FRONT_LEFT, FALSE)
			SET_VEHICLE_TYRES_CAN_BURST(vehStolen, FALSE)
			SET_VEHICLE_COLOUR_COMBINATION(vehStolen,3)
			SET_VEHICLE_HAS_STRONG_AXLES(vehStolen,TRUE)
			
			// Vehicle mod stuff (see B*1529918)
			SET_VEHICLE_MOD_KIT(vehStolen, 0)
			SET_VEHICLE_MOD(vehStolen, MOD_ENGINE, 2)
			SET_VEHICLE_MOD(vehStolen, MOD_ARMOUR, 3)
			SET_VEHICLE_MOD(vehStolen, MOD_BRAKES, 1)
			SET_VEHICLE_MOD(vehStolen, MOD_GEARBOX, 1)
			SET_VEHICLE_WHEEL_TYPE(vehStolen, MWT_SUV)
			SET_VEHICLE_MOD(vehStolen, MOD_WHEELS, 6)
			SET_VEHICLE_WINDOW_TINT(vehStolen, 3)
			TOGGLE_VEHICLE_MOD(vehStolen, MOD_TOGGLE_TURBO, TRUE)
			TOGGLE_VEHICLE_MOD(vehStolen, MOD_TOGGLE_XENON_LIGHTS, TRUE)
			SET_VEHICLE_COLOURS(vehStolen, 143, 0)
			SET_VEHICLE_EXTRA_COLOURS(vehStolen, 31, 2)
			
			SET_VEHICLE_CAN_LEAK_OIL(vehStolen, FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(vehStolen, FALSE)
			IF bTopVariation	
				pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, <<-490.4008, -2160.7551, 8.2498>>, 314.0037)
			ELSE	
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-583.088440,-2234.248779,4.523423>>, <<-685.181702,-2350.903564,17.070732>>, 178.000000)  //coming from airport
					pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, <<-486.81, -2156.99, 8.27>>, -172.34)
				ELSE
					pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, <<-486.2511, -2161.8081, 8.3041>>, 353.4264)
				ENDIF
			ENDIF
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedVictim, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
	        //pedThief = CREATE_PED(PEDTYPE_CRIMINAL, modelThief, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehStolen, <<-2, 0, -0.9>>), wrap((fInput-270), 0,360))
			pedThief = CREATE_PED_INSIDE_VEHICLE(vehStolen, PEDTYPE_CRIMINAL, modelThief)
			GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, -1, TRUE)
       		SET_PED_ACCURACY(pedThief,10)
			SET_DRIVER_ABILITY(pedThief,1)
			SET_DRIVER_RACING_MODIFIER(pedThief,1)
			//TASK_AIM_GUN_AT_ENTITY(pedThief, vehStolen, -1)
			SET_PED_CONFIG_FLAG(pedThief, PCF_DontInfluenceWantedLevel, TRUE)
			SET_AMBIENT_VOICE_NAME(pedThief, thiefAmbientVoice)
		
			vCar = << -501.9105, -2148.0193, 8.0392 >>
			fCar = 294.4744
			vPlayer = << -501.0442, -2150.7988, 8.1390 >>
			vVictim = << -493.8062, -2156.0359, 8.1978 >> 
			
			vStartPos   = << -504.3650, -2144.5342, 9.8585 >>
			vStartRot   = << -6.6935, -0.0000, -127.8147 >>
			
			/*
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	            CASE CHAR_MICHAEL  //0
	                playerGolf = "RECT2_GLFM"
	            BREAK
	            
	            CASE CHAR_FRANKLIN //1
	                playerGolf = "RECT2_GLFF"
	            BREAK
	            
	            CASE CHAR_TREVOR   //2
	                playerGolf = "RECT2_GLFT"
	            BREAK
	        ENDSWITCH
			*/
    	
		ENDIF
		
		SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
		SET_PED_CONFIG_FLAG(pedVictim, PCF_UseKinematicModeWhenStationary, TRUE)
		SET_CAN_RESPRAY_VEHICLE(vehStolen, FALSE)
		SET_VEHICLE_ENGINE_ON(vehStolen, TRUE, TRUE)
		
        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedThief, TRUE)
		
		TASK_LOOK_AT_ENTITY(pedVictim,pedThief,-1)
		
		ADD_VEHICLE_UPSIDEDOWN_CHECK(vehStolen)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehStolen, TRUE)
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehStolen, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
		START_AUDIO_SCENE("RE_CAR_STEAL_SCENE")
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehStolen, "RE_CAR_STEAL_STOLEN_VEHICLE") 
		SET_DISABLE_PRETEND_OCCUPANTS(vehStolen, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_NEVER_FLEE, TRUE)
//		SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
//		SET_PED_CONFIG_FLAG(pedThief, PCF_GetOutBurningVehicle, TRUE)
        
        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
            CASE CHAR_MICHAEL  //0
                playerVoice = "MICHAEL"
            BREAK
            
            CASE CHAR_FRANKLIN //1
                playerVoice = "FRANKLIN"
            BREAK
            
            CASE CHAR_TREVOR   //2
                playerVoice = "TREVOR"
            BREAK
        ENDSWITCH
        
        ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), playerVoice)
        ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedVictim, victimVoice)
        ADD_PED_FOR_DIALOGUE(dialogueStruct, 2, pedThief, "CThief")
        
        RETURN TRUE
    ELSE
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
			MissionCleanup()
		ENDIF
	ENDIF
    
    RETURN FALSE
ENDFUNC

PROC GET_CURRENT_CAR_THEFT_VARIATION()
	IF VDIST(vInput, << -2255.21, 4271.04, 44.875 >>) < 5
		iSelectedVariation = ONE_PINK
	ELIF VDIST(vInput, << -500.924, -2165.36, 7.6988 >>) < 5
		iSelectedVariation = TWO_GOLFER
	ENDIF
	
	PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> - GET_CURRENT_CAR_THEFT_VARIATION() iSelectedVariation = ")PRINTINT(iSelectedVariation)PRINTNL()
ENDPROC

FUNC BOOL IS_VARIATION_COMPLETE()
	BOOL bReturn = FALSE
	
	SWITCH iSelectedVariation
		CASE ONE_PINK	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_CTHF1)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE TWO_GOLFER 
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_CTHF2)
				bReturn = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bReturn
ENDFUNC

PROC RUN_HINT_CAM()
	IF IS_VEHICLE_OK(vehStolen)
	AND DOES_BLIP_EXIST(blipThief)	
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehStolen)
	ENDIF
ENDPROC

PROC GET_PLAYER_INFO()
    FLOAT tempHealth
    BOOL isFine, isBattered
    IF IS_VEHICLE_OK(vehStolen)
    
		tempHealth = GET_VEHICLE_ENGINE_HEALTH(vehStolen)
        IF (tempHealth > 0) AND (tempHealth < 650)
            isBattered = TRUE
        ELIF (tempHealth > 650) AND (tempHealth < 950)
        
        ELSE
            isFine = TRUE
        ENDIF

		IF isFine
		AND ARE_ALL_VEHICLE_WINDOWS_INTACT(vehStolen)
            playerDialogue = sGood
        ELSE
            IF isBattered
                playerDialogue = sBad
            ELSE
                playerDialogue = sOK
            ENDIF
        ENDIF

    ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_STUCK_ON_ROOF_WITH_PLAYER_IN()

	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehStolen)
	AND IS_VEHICLE_STUCK_ON_ROOF(vehStolen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bSkipped = FALSE

FUNC BOOL DO_FADES_IF_CUT_IS_SKIPPED()
	MODEL_NAMES mRequestedVehicle = PEYOTE
	IF iSelectedVariation = TWO_GOLFER
		mRequestedVehicle = ROCOTO
	ENDIF
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		IF HAS_VEHICLE_ASSET_LOADED(mRequestedVehicle)		
			SET_CAM_ACTIVE(camStart, FALSE)
			//RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DO_SCREEN_FADE_OUT(500)
			WHILE IS_SCREEN_FADING_OUT()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				WAIT(0)
			ENDWHILE
			bSkipped = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		IF HAS_VEHICLE_ASSET_LOADED(mRequestedVehicle)
			CPRINTLN(DEBUG_AMBIENT, "DO_FADES_IF_CUT_IS_SKIPPED() - screen already faded out")
			bSkipped = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_RETURNED_CAR_CUT_FINISHED()
	VECTOR vTemp
	STRING chassisBone ="chassis"
    GET_PLAYER_INFO()
    IF NOT IS_PED_INJURED(pedVictim)
    AND NOT IS_PED_INJURED(PLAYER_PED_ID())
    AND IS_VEHICLE_OK(vehStolen)
	AND HAS_ANIM_DICT_LOADED(animDict)
        SWITCH returnCutStage
			CASE stopTheCar
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF CAN_PLAYER_START_CUTSCENE()
						IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR))
							IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_HELP()
									WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fStoppingDistance, 1)
									//OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									HIDE_HUD_AND_RADAR_THIS_FRAME()
									
									//TEST: what happens if the victim dies when we remove the controls.
									//SET_ENTITY_HEALTH(pedVictim, 0)
									
									WAIT(0)
									
									ENDWHILE
									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fStoppingDistance, 0)
//									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//									ENDIF
								ENDIF
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								IF NOT IS_PED_INJURED(pedVictim)
									SET_PED_CAN_RAGDOLL(pedVictim, FALSE)
								ENDIF
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								DISABLE_CELLPHONE_THIS_FRAME_ONLY()
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								
								returnCutStage = setTheScene
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
            CASE setTheScene
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
                SET_VEHICLE_DOOR_CONTROL(vehStolen, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0)
				SET_VEHICLE_DOOR_LATCHED(vehStolen, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				VECTOR vMovePlayerCar
				FLOAT fMovePlayerCar 
				
                IF iSelectedVariation = ONE_PINK
					
					IF IS_VEHICLE_OK(vehStolen)
						IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehStolen), 336, 90)
						    vCar = <<-2254.2998, 4273.3608, 44.9697>>
							fCar = 336.6557   
							vPlayer = << -2252.6831, 4274.1597, 45.0612 >> 

							vVictim = << -2257.8250, 4269.1890, 44.6334 >> 
							fVictim = 272.8688 

							vStartPos   = <<-2254.5876, 4277.3071, 45.6133>>
							vStartRot   = <<6.6248, 0.0369, 179.5595>>
						ELSE
							vCar = <<-2254.2229, 4272.3301, 44.9193>>
							fCar = 148.0287
							vVictim = << -2255.7329, 4271.3198, 44.8166 >> 
							fVictim = 179.4774	
							vPlayer = << -2257.0879, 4268.9380, 44.6456 >>

							vStartPos   = <<-2255.7751, 4274.1440, 46.0666>>
							vStartRot   = <<2.8781, 0.3464, -146.6097>>

						ENDIF
					ENDIF
					
					vMovePlayerCar = <<-2269.34, 4279.89, 45.47>>
					fMovePlayerCar = 53.23
					
	                fFOV = 50.0
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayer)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_ENTITY_COORDS(pedVictim, vVictim)
					SET_ENTITY_HEADING(vehStolen, fCar)
					SET_ENTITY_COORDS(vehStolen, vCar)
				ELSE
					
					//IF bTopVariation	
					IF NOT IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehStolen), 64.6764, 90)
						vStartPos   = <<-478.7095, -2163.5984, 10.3091>>
						vStartRot   = <<-7.4391, 0.0006, 35.6865>>
						
						vCar = <<-484.9581, -2155.2661, 8.2480>>
						fCar = 243.3080
						fFOV = 22.0
					ELSE	
						vStartPos   = <<-488.0143, -2149.4189, 9.9817>>
						vStartRot   = <<-11.7475, 0.0006, -145.0045>>
						
						vCar = <<-484.3195, -2154.1882, 8.2182>>
						fCar = 64.6764
						fFOV = 36.9289
					ENDIF
					
					vMovePlayerCar = <<-486.92, -2169.01, 8.89>>
					fMovePlayerCar = 63.10
			
					SET_ENTITY_COORDS(vehStolen, vCar)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_ENTITY_HEADING(vehStolen, fCar)
                ENDIF
				
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				
				IF IS_VEHICLE_OK(vehStolen)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehStolen)
					
					// Fix for B*1535458
					IF DOES_ENTITY_EXIST(pedThief)
						IF IS_PED_IN_VEHICLE(pedThief, vehStolen)
							DELETE_PED(pedThief)
						ELSE
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Thief isn't in the vehicle?")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Thief doesn't exist?")
					ENDIF
				ENDIF
				
				scenePosition = <<0,0,0>>
				sceneRotation = <<0,0,0>>
				sceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneID, vehStolen, GET_ENTITY_BONE_INDEX_BY_NAME(vehStolen, chassisBone))
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneID, scenePosition, sceneRotation)
				
				TASK_SYNCHRONIZED_SCENE(pedVictim, sceneID, animDict, girlAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneID, animDict, playerAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_HIDE_WEAPON)
				camStart = CREATE_CAM_WITH_PARAMS("DEFAULT_ANIMATED_CAMERA", vStartPos,vStartRot,fFOV)
				PLAY_SYNCHRONIZED_CAM_ANIM(camStart, sceneId, "car_returned_cam", animDict)
				PLAY_ENTITY_ANIM(vehStolen, carAnim, animDict, INSTANT_BLEND_IN, FALSE, FALSE, DEFAULT, DEFAULT, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
				scenePosition = GET_ENTITY_COORDS(vehStolen)
				sceneRotation = GET_ENTITY_ROTATION(vehStolen)
				
				IF IS_VEHICLE_OK(vehPlayerStart)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehPlayerStart, vCar) <=11.0
						SET_ENTITY_COORDS(vehPlayerStart, vMovePlayerCar)
						SET_ENTITY_HEADING(vehPlayerStart, fMovePlayerCar)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerStart)
						SET_ENTITY_AS_MISSION_ENTITY(vehPlayerStart)
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - player's car too close - moved")
					ENDIF
				ENDIF
				
                CLEAR_AREA(vCar, 10, TRUE)
				
				SETTIMERA(0)
				
				IF iSelectedVariation = TWO_GOLFER
					REQUEST_VEHICLE_ASSET(ROCOTO, ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
				ELSE
					REQUEST_VEHICLE_ASSET(PEYOTE, ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
				ENDIF
				
				STOP_FIRE_IN_RANGE(vCar, 15)
				SET_CAM_ACTIVE(camStart, TRUE)
	            RENDER_SCRIPT_CAMS(TRUE, FALSE)
				returnCutStage = startConvo
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - returnCutStage = startConvo")
				
				//Run this every frame, as per bug 1418065
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
            BREAK
			
			CASE startConvo
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF DO_FADES_IF_CUT_IS_SKIPPED()
					returnCutStage = cleanupCut
				ENDIF
				IF iSelectedVariation = ONE_PINK
					IF TIMERA() > 1500
					AND NOT bSkipped
						IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, playerDialogue, CONV_PRIORITY_AMBIENT_HIGH)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - returnCutStage = cleanupCut")
							returnCutStage = cleanupCut
						ENDIF
					ENDIF
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
					AND NOT bSkipped
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 0.2
							IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, playerDialogue, CONV_PRIORITY_AMBIENT_HIGH)
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - returnCutStage = cleanupCut")
								bPlayedCutDialogue = FALSE
								returnCutStage = cleanupCut
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Run this every frame, as per bug 1418065
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			BREAK
			
            
            CASE cleanupCut 
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF iSelectedVariation = TWO_GOLFER
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
						IF bPlayedCutDialogue = FALSE
							IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, "RECT2_REWARD", CONV_PRIORITY_AMBIENT_HIGH)
								bPlayedCutDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Run this every frame, as per bug 1418065
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				IF iSelectedVariation = ONE_PINK
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
						IF bDonePlayerBlend = FALSE
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 0.83
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								//STOP_SYNCHRONIZED_ENTITY_ANIM
								TASK_PLAY_ANIM(PLAYER_PED_ID(), animDict, playerAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON, GET_SYNCHRONIZED_SCENE_PHASE(sceneID))
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
								bDonePlayerBlend = TRUE
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(pedVictim)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 0.83
								IF IS_ENTITY_PLAYING_ANIM(pedVictim, animDict, girlAnim, ANIM_SYNCED_SCENE)
									STOP_SYNCHRONIZED_ENTITY_ANIM(pedVictim, INSTANT_BLEND_OUT, FALSE)
									IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
										IF IS_VEHICLE_OK(vehStolen)
						                    SET_PED_INTO_VEHICLE(pedVictim, vehStolen)
											SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
											CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND (on stop)")
										ENDIF
					                ENDIF
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim, TRUE)
								ELSE
									SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
						IF bDonePlayerBlend = FALSE
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 0.86
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								//STOP_SYNCHRONIZED_ENTITY_ANIM
								TASK_PLAY_ANIM(PLAYER_PED_ID(), animDict, playerAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON, GET_SYNCHRONIZED_SCENE_PHASE(sceneID))
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
								bDonePlayerBlend = TRUE
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(pedVictim)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) > 0.847
								IF IS_ENTITY_PLAYING_ANIM(pedVictim, animDict, girlAnim, ANIM_SYNCED_SCENE)
									STOP_SYNCHRONIZED_ENTITY_ANIM(pedVictim, INSTANT_BLEND_OUT, FALSE)
									IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
										IF IS_VEHICLE_OK(vehStolen)
						                    SET_PED_INTO_VEHICLE(pedVictim, vehStolen)
											SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
											CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND (on stop)")
										ENDIF
					                ENDIF
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim, TRUE)
								ELSE
									SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 0.99)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
				OR DO_FADES_IF_CUT_IS_SKIPPED()
					
					IF NOT IS_ENTITY_DEAD(vehStolen)
					AND IS_ENTITY_PLAYING_ANIM(vehStolen,animDict,carAnim)
						SET_ENTITY_ANIM_CURRENT_TIME(vehStolen,animDict,carAnim,1)
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
						SET_SYNCHRONIZED_SCENE_PHASE(sceneID,1)
					ENDIF
					
					//CLEAR_PED_TASKS(PLAYER_PED_ID())
					IF bSkipped
						vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(animDict, playerAnim,  scenePosition, sceneRotation, 1)
						GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
	               		SET_ENTITY_COORDS(PLAYER_PED_ID(), vTemp)
						vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(animDict, playerAnim, scenePosition, sceneRotation, 1)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), vTemp.z)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE, FAUS_DEFAULT, TRUE)					
					
					IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
						IF IS_VEHICLE_OK(vehStolen)
		                    SET_PED_INTO_VEHICLE(pedVictim, vehStolen)
							SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND (on stop)")
						ENDIF
	                ENDIF
					
					IF NOT IS_PED_INJURED(pedVictim)
						IF IS_PED_IN_ANY_VEHICLE(pedVictim)
							IF IS_VEHICLE_OK(vehStolen)
								SET_VEHICLE_DOOR_CONTROL(vehStolen, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0)
								SET_VEHICLE_DOOR_LATCHED(vehStolen, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
								//WAIT(1000)
								iBlendTimer = GET_GAME_TIMER()
								returnCutStage = waitToBlend
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOING INSTANT IK BLEND (on sync scene finish)")
							ENDIF
		                ENDIF
					ENDIF
				ENDIF
            BREAK
			
			CASE waitToBlend
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF (GET_GAME_TIMER() - iBlendTimer) > 100
					IF IS_ENTITY_ALIVE(pedVictim)
					AND IS_VEHICLE_OK(vehStolen)
						OPEN_SEQUENCE_TASK(seq)
							TASK_VEHICLE_TEMP_ACTION(NULL, vehStolen, TEMPACT_GOFORWARD, 1000)
							TASK_VEHICLE_DRIVE_WANDER(NULL, vehStolen, 25, victimDrivingmode)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedVictim, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					IF bSkipped
						IF DOES_CAM_EXIST(camStart)
							SET_CAM_ACTIVE(camStart, FALSE)
							DESTROY_CAM(camStart)
						ENDIF
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						WAIT(750)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, TRUE)	
					ELSE
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
						SET_CAM_ACTIVE(camStart, FALSE)
					ENDIF
	                SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SAFE_RELEASE_VEHICLE(vehStolen)
	                INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(),PS_DRIVING_ABILITY,5)
					RETURN TRUE
				ELSE
					IF IS_ENTITY_ALIVE(pedVictim)
						SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - returnCutStage = waitToBlend")
				ENDIF
			BREAK
        ENDSWITCH
        
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_ACTIVELY_STEALING_CAR()
	IF NOT IS_PED_INJURED(pedThief)
	AND NOT IS_PED_INJURED(pedVictim)
		IF IS_VEHICLE_OK(vehStolen)
			IF IS_PED_SITTING_IN_VEHICLE(pedThief, vehStolen)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ATTRACT_DIALOGUE_PLAYED()
	IF NOT attractDialoguePlayed
		IF TIMERA() > 2000
			IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sAttract, CONV_PRIORITY_AMBIENT_HIGH)
				attractDialoguePlayed = TRUE	
			ENDIF
		ENDIF
	ENDIF
	RETURN attractDialoguePlayed
ENDFUNC

FUNC BOOL HAS_EXCITED_DIALOGUE_PLAYED()
	IF NOT excitedDialoguePlayed
		IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sJoy, CONV_PRIORITY_AMBIENT_HIGH)
			excitedDialoguePlayed = TRUE	
		ENDIF
	ENDIF
	RETURN excitedDialoguePlayed
ENDFUNC

FUNC BOOL HAS_HELP_DIALOGUE_PLAYED()
	IF NOT helpdDialoguePlayed
		IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sHelp, CONV_PRIORITY_AMBIENT_HIGH)
			helpdDialoguePlayed = TRUE	
		ENDIF
	ENDIF
	RETURN helpdDialoguePlayed
ENDFUNC

/// PURPOSE:
///    checks used to cleanup mission
/// PARAMS:
///    bExcludeChecksForThiefNearby - if TRUE we only perform interfer checks for bullet/projectile close to victim if the thief has moved awsay from here 
/// RETURNS:
///    TRUE if player interfering
FUNC BOOL IS_PLAYER_INTERFERING_WITH_SCENE(BOOL bExcludeChecksForThiefNearby = FALSE)
	BOOL bReturn
	VECTOR vTempMax = <<6,6,6>>, vTempMin = <<-6,-6,-6>>
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		IF NOT IS_PED_INJURED(pedVictim)
			// B*1990700 - don't fail if thief is still right next to the women, giving player benifit doubt that he's attacking thief
			IF bExcludeChecksForThiefNearby = FALSE
			OR (GET_DISTANCE_BETWEEN_ENTITIES(pedVictim, pedThief) > 15)
				IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 4, FALSE)
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 5 - IS_BULLET_IN_AREA")
					bReturn = TRUE
				ENDIF

				vTempMax = vTempMax+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
				vTempMin = vTempMin+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
				IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
				OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
				OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 4 - IS_PROJECTILE_TYPE_IN_AREA")
					bReturn = TRUE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 5 - excluding checks this frame for dist")
			ENDIF
			
			IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 6)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 3 - GET_IS_PETROL_DECAL_IN_RANGE")
				bReturn = TRUE
			ENDIF
			
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 6)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 2 - IS_EXPLOSION_IN_SPHERE")
				bReturn = TRUE
			ENDIF
			
//			IF IS_PED_FLEEING(pedVictim)
//				bReturn = TRUE
//			ENDIF
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_INTERFERING_WITH_SCENE 1 - HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY")
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF bReturn
		PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> IS_PLAYER_INTERFERING_WITH_SCENE()\n")
	ENDIF
	RETURN bReturn
ENDFUNC



PROC RUN_VICTIM_REACTIONS()
   // STRING agitatedAnim//, excitedAnim
    IF NOT IS_PED_INJURED(pedVictim)
    AND NOT IS_PED_INJURED(PLAYER_PED_ID())
    	
        SWITCH victimResponseStage
            CASE turnsToPlayer

				bAgitatedCanReset = bAgitatedCanReset
			
				IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
					IF iSelectedVariation = ONE_PINK
						IF IS_PED_ACTIVELY_STEALING_CAR()
						//AND HAS_ATTRACT_DIALOGUE_PLAYED()		
							victimResponseStage = gestureToPlayer
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - victimResponseStage = gestureToPlayer")
						ENDIF
					ELSE
						//IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
						IF NOT IS_PED_RAGDOLL(pedVictim)	
						AND NOT IS_PED_GETTING_UP(pedVictim)
							TASK_LOOK_AT_ENTITY(pedVictim,PLAYER_PED_ID(),-1)
							//TASK_TURN_PED_TO_FACE_COORD(pedVictim,<<-491.3654, -2148.7222, 8.1671>>,-1)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-539.456604,-2094.490967,6.967173>>, <<-468.429596,-2152.548584,18.991270>>, 26.750000)  //on the road
								TASK_ACHIEVE_HEADING(pedVictim,345)
								victimResponseStage = gestureToPlayer
							ELSE
								//TASK_PLAY_ANIM(pedVictim, animPointsIG, "arms_waving", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,AF_LOOPING)
								OPEN_SEQUENCE_TASK(seq)
									TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1)
									TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
									TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
									//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
									//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
								//SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
								CLOSE_SEQUENCE_TASK(seq)
	                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
	                            CLEAR_SEQUENCE_TASK(seq)
								iPleaTimer = GET_GAME_TIMER()
								victimResponseStage = waitPlayerChase
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - victimResponseStage = waitPlayerChase 1")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
            BREAK
            
			CASE gestureToPlayer
				
				IF iSelectedVariation = ONE_PINK
					//IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animPointsIG, sPointAnim)	
					TASK_LOOK_AT_ENTITY(pedVictim,PLAYER_PED_ID(),-1)
					//TASK_PLAY_ANIM(pedVictim, animPointsIG, sPointAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,AF_LOOPING)
					IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK		
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(NULL, animPointsIG, sPointAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
							//TASK_PLAY_ANIM(NULL, animPointsIG, sPointAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
							//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_a", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						//SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq)
                        TASK_PERFORM_SEQUENCE(pedVictim, seq)
                        CLEAR_SEQUENCE_TASK(seq)
						victimResponseStage = waitPlayerChase
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - victimResponseStage = waitPlayerChase 2")
					ENDIF
					//ELSE
					//	victimResponseStage = waitPlayerChase
					//ENDIF
				ELSE	
					//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20,20,10>>)    //<<40,40,10>>
					//AND IS_ENTITY_AT_COORD(pedVictim,<<-491.3654, -2148.7222, 8.1671>>,<<4,4,4>>)	
					IF IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(), 60.0)
						//IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animPointsIG, "pointing")
						//AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK		
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1)
								IF bTopVariation = FALSE
									TASK_PLAY_ANIM(NULL, animPointsIG, "pointing", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
								ENDIF
								//TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
								//TASK_PLAY_ANIM(NULL, animPointsIG, "arms_waving", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
								//TASK_PLAY_ANIM(NULL, animAgitatedIG, "pointing", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
								//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
								//TASK_PLAY_ANIM(NULL, animAgitatedIG, "agitated_idle_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							//SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
							CLOSE_SEQUENCE_TASK(seq)
                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
                            CLEAR_SEQUENCE_TASK(seq)
							victimResponseStage = waitPlayerChase
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - victimResponseStage = waitPlayerChase 2")
						ENDIF
					ENDIF
				ENDIF	
				
				IF IS_PED_ACTIVELY_STEALING_CAR()
					IF HAS_ATTRACT_DIALOGUE_PLAYED()
//						REMOVE_BLIP(blipScene)
//						IF NOT DOES_BLIP_EXIST(blipThief)
//							blipThief = CREATE_AMBIENT_BLIP_FOR_PED(pedThief, TRUE)
//						ENDIF
					ENDIF 
				ENDIF
			
			BREAK
			
			CASE waitPlayerChase
				
				// Have this here as well, in case the victim gets knocked over or something when the thief drives off
				IF IS_PED_ACTIVELY_STEALING_CAR()
					IF HAS_ATTRACT_DIALOGUE_PLAYED()
//						REMOVE_BLIP(blipScene)
//						IF NOT DOES_BLIP_EXIST(blipThief)
//						AND NOT IS_ENTITY_ALIVE(pedThief)
//							blipThief = CREATE_AMBIENT_BLIP_FOR_PED(pedThief, TRUE)
//						ENDIF
					ENDIF 
				ENDIF
				
				//IF attractDialoguePlayed
				//	HAS_HELP_DIALOGUE_PLAYED()
				//ENDIF
				
				// For the 2nd variation only - make the woman look at the player, and occasionally plead for help if they're close enough
				IF iSelectedVariation = TWO_GOLFER
					IF NOT IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(),60)
					AND NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
					ENDIF
					IF GET_DISTANCE_BETWEEN_ENTITIES(pedVictim, PLAYER_PED_ID()) < 25
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF (GET_GAME_TIMER() - iPleaTimer) > 6000
								//IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sHelp, CONV_PRIORITY_MEDIUM)
								//	iPleaTimer = GET_GAME_TIMER()
								//ENDIF
								HAS_HELP_DIALOGUE_PLAYED()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				AND NOT IsPedPerformingTask(pedVictim, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					IF iSelectedVariation = ONE_PINK
						vVictim = <<-2260.10, 4274.24, 44.90>> 	
						IF NOT IS_ENTITY_AT_COORD(pedVictim, vVictim, <<1.5,1.5,1.5>>)
							IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								OPEN_SEQUENCE_TASK(seq)
									TASK_GO_STRAIGHT_TO_COORD(NULL, vVictim, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, fVictim)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedVictim, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ELIF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animAgitatedIG, "waiting")
							TASK_PLAY_ANIM(pedVictim, animAgitatedIG, "waiting", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							IF GET_DISTANCE_BETWEEN_ENTITIES(pedVictim, PLAYER_PED_ID()) < 25
								HAS_HELP_DIALOGUE_PLAYED()
							ENDIF
						ENDIF
					
					ELSE
						IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animAgitatedIG, "waiting")
							TASK_PLAY_ANIM(pedVictim, animAgitatedIG, "waiting", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						ENDIF
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE waitsOnReturn
				
				IF NOT IS_PED_HEADTRACKING_PED(pedVictim,PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
				ENDIF
				
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<40,40,10>>)    //<<50,50,10>>
                AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<9,9,9>>)
                    //IF CREATE_CONVERSATION(dialogueStruct, "RECTHAU", "RECTH_JOY", CONV_PRIORITY_AMBIENT_HIGH)
					
					IF HAS_EXCITED_DIALOGUE_PLAYED()
                        IF bExcitedCanReset
							IF iSelectedVariation = ONE_PINK
								OPEN_SEQUENCE_TASK(seq)
									//TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000)
	                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									TASK_PLAY_ANIM(NULL, "RANDOM@CAR_THIEF@WAITING_IG_4", "idle_a",  REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	                            CLOSE_SEQUENCE_TASK(seq)
	                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
	                            CLEAR_SEQUENCE_TASK(seq)
	                            bExcitedCanReset= FALSE
								iPleaTimer = GET_GAME_TIMER()
							ELSE
								OPEN_SEQUENCE_TASK(seq)
									//TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000)
	                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
	                            	TASK_PLAY_ANIM(NULL, "RANDOM@CAR_THIEF@WAITING_IG_4", "idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
								CLOSE_SEQUENCE_TASK(seq)
	                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
	                            CLEAR_SEQUENCE_TASK(seq)
	                            bExcitedCanReset= FALSE
								iPleaTimer = GET_GAME_TIMER()
							ENDIF
                        ELSE
							IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, "RANDOM@CAR_THIEF@WAITING_IG_4", "idle_a")
								IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
									IF NOT IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(),45)
										OPEN_SEQUENCE_TASK(seq)
			                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
			                            CLOSE_SEQUENCE_TASK(seq)
			                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
			                            CLEAR_SEQUENCE_TASK(seq)
									ELSE
										IF (GET_GAME_TIMER() - iPleaTimer) > 6000
											bExcitedCanReset= TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(),70)
									CLEAR_PED_TASKS(pedVictim)
									OPEN_SEQUENCE_TASK(seq)
		                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
		                            CLOSE_SEQUENCE_TASK(seq)
		                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
		                            CLEAR_SEQUENCE_TASK(seq)
									iPleaTimer += 4000
								ENDIF
							ENDIF
						ENDIF
                    ENDIF
					IF NOT IS_PED_INJURED(pedThief)
						TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 150, -1)
						SET_PED_KEEP_TASK(pedThief, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(pedThief)
					ENDIF
                ELSE
					IF iSelectedVariation = ONE_PINK  
						vVictim = <<-2260.10, 4274.24, 44.90>> 											
					ELSE
						IF bTopVariation
							vVictim = <<-483.4162, -2160.8735, 8.3590>>
						ELSE	
							vVictim = <<-485.9905, -2153.5420, 8.1999>> 
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_AT_COORD(pedVictim, vVictim, <<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY>>)
						IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
							TASK_GO_STRAIGHT_TO_COORD(pedVictim, vVictim, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, fVictim)
						ENDIF
					ELSE
						IF NOT IS_PED_FACING_PED(pedVictim, PLAYER_PED_ID(),50)
							TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID(),-1)
						ENDIF
					ENDIF
                    bExcitedCanReset = TRUE
                ENDIF
            BREAK
			CASE carIsReturned
            
            BREAK
        ENDSWITCH
        
    ENDIF
	IF IS_PLAYER_INTERFERING_WITH_SCENE(TRUE)
		CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - RUN_VICTIM_REACTIONS() ; IS_PLAYER_INTERFERING_WITH_SCENE triggered")
		IF NOT IS_PED_INJURED(pedVictim)
			TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
			SET_PED_KEEP_TASK(pedVictim, TRUE)
			WAIT(0)
		ENDIF
		missionCleanup()
	ENDIF
ENDPROC

PROC THIEF_ABANDONS_CAR()
    IF NOT IS_PED_INJURED(pedThief)
		SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_DO_DRIVEBYS, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_USE_VEHICLE, FALSE)
        TASK_COMBAT_PED(pedThief, PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedThief, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Thief abandons car - SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))")
    ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Thief abandons car - Tried making thief leave car, but he seems to be dead?")
	ENDIF
	IF IS_VEHICLE_OK(vehStolen)
		SET_VEHICLE_DOORS_LOCKED(vehStolen, VEHICLELOCK_UNLOCKED)
	ENDIF
ENDPROC

FUNC BOOL IS_STOLEN_VEHICLE_TYRE_BURST()
    IF IS_VEHICLE_OK(vehStolen)
        IF IS_VEHICLE_TYRE_BURST(vehStolen, SC_WHEEL_CAR_FRONT_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehStolen, SC_WHEEL_CAR_FRONT_RIGHT)
        OR IS_VEHICLE_TYRE_BURST(vehStolen, SC_WHEEL_CAR_REAR_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehStolen, SC_WHEEL_CAR_REAR_RIGHT)
			PRINTSTRING("\nIS_STOLEN_VEHICLE_TYRE_BURST()\n")
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE    
ENDFUNC

FUNC BOOL HAS_GUNSHOT_SCARED_THIEF()
    IF NOT IS_PED_INJURED(pedThief)
        IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedThief, BONETAG_HEAD, <<0,0,0>>), 1)
			PRINTSTRING("\nHAS_GUNSHOT_SCARED_THIEF()\n")
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL HAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()
    IF IS_VEHICLE_OK(vehStolen)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehStolen, <<40,40,10>>)  //<<20,20,10>>
			IF bHasBeenBumped
				IF NOT IS_PED_INJURED(pedThief)
					//IF GET_SCRIPT_TASK_STATUS(pedThief, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						//IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedThief) 
							IF NOT bDriveBy
								IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(vehStolen)	
									TASK_DRIVE_BY(pedThief, PLAYER_PED_ID(), NULL, <<0,0,0>>, 40, 100, TRUE)
									bDriveBy = TRUE
								ENDIF
							ENDIF
							IF GET_GAME_TIMER()	> iDialogueTimer + 7000 
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehStolen, <<20,20,10>>)
									IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sFYou, CONV_PRIORITY_AMBIENT_HIGH)
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehStolen)
							CLEAR_ENTITY_LAST_WEAPON_DAMAGE(vehStolen)
							bHasBeenBumped = FALSE
						//ENDIF
					//ENDIF
				ENDIF
			ENDIF

            IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehStolen, PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedThief)
					CREATE_CONVERSATION(dialogueStruct, sTextBlock, sFYou, CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
                bumpedCounter++
				bHasBeenBumped = TRUE
            ENDIF
            IF bumpedCounter > 10 //6
				PRINTSTRING("\nHAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()\n")
                RETURN TRUE
            ENDIF
        ELSE
            bDriveBy = FALSE
			bHasBeenBumped = FALSE
			//bumpedCounter = 0
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC



PROC REGISTER_AS_COMPLETE()
	LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	IF iSelectedVariation = ONE_PINK
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_CTHF1)
	ELIF iSelectedVariation = TWO_GOLFER 
		//UNLOCK_GOLF_BUDDY(g_savedGlobals.sGolfData, GB_CAR_THEFT_VICTIM)
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_CTHF2)
	ENDIF
ENDPROC

PROC missionPassed(BOOL bCarReturned = FALSE)
	IF iSelectedVariation = TWO_GOLFER
		enumCharacterList CurrentChar = GET_CURRENT_PLAYER_PED_ENUM()
		
		SWITCH CurrentChar
			CASE CHAR_MICHAEL
				WHILE NOT REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(EMAIL_RECARTHEFT2_M_PASS, CT_AMBIENT, BIT_MICHAEL, CHAR_BLANK_ENTRY, 7200000, 10000, DEFAULT, CID_RE_CARSTEAL2_REWARD)
					IF IS_ENTITY_ALIVE(pedVictim)
						SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF
					WAIT(0)
				ENDWHILE	
			BREAK
			CASE CHAR_FRANKLIN
				WHILE NOT REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(EMAIL_RECARTHEFT2_F_PASS, CT_AMBIENT, BIT_FRANKLIN, CHAR_BLANK_ENTRY, 7200000, 10000, DEFAULT, CID_RE_CARSTEAL2_REWARD)
					IF IS_ENTITY_ALIVE(pedVictim)
						SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF
					WAIT(0)
				ENDWHILE	
			BREAK
			CASE CHAR_TREVOR
				WHILE NOT REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(EMAIL_RECARTHEFT2_T_PASS, CT_AMBIENT, BIT_TREVOR, CHAR_BLANK_ENTRY, 7200000, 10000, DEFAULT, CID_RE_CARSTEAL2_REWARD)
					IF IS_ENTITY_ALIVE(pedVictim)
						SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF
					WAIT(0)
				ENDWHILE
			BREAK
		ENDSWITCH
		
		
	ENDIF
	IF bCarReturned
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " passed with RE_PASS_RETURN_VEHICLE")
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_VEHICLE)
	ENDIF
	RANDOM_EVENT_PASSED(RE_CARTHEFT, iSelectedVariation)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

FUNC BOOL IS_TARGET_IN_RANGE()
	FLOAT fRange = 300 //150

	IF iSelectedVariation = ONE_PINK
		fRange = 220
	ENDIF
	IF IS_VEHICLE_OK(vehStolen)
		UPDATE_CHASE_BLIP(blipThief, vehStolen, fRange)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehStolen, <<fRange,fRange,100>>)
			RETURN TRUE
		ELSE
			IF NOT IS_ENTITY_OCCLUDED(vehStolen)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RUN_DEBUG()
#IF IS_DEBUG_BUILD 
    // Event Passed
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		PRINTSTRING("\nre_CarTheft - cleaned up by IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)\n")
        missionPassed()
    ENDIF
    // Jump to end cut
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//        IF carStolenStage <> carReturned
//            carStolenStage = carReturned
//            victimResponseStage = waitsOnReturn
//            KILL_ANY_CONVERSATION()
            IF NOT IS_PED_INJURED(pedThief)
                DELETE_PED(pedThief)
                REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
            ENDIF
//            IF NOT IS_PED_INJURED(pedVictim)
//                SET_ENTITY_COORDS(pedVictim, vVictim)
//                SET_ENTITY_HEADING(pedVictim, fVictim)
//            ENDIF
//            WAIT(0)
            IF IS_VEHICLE_OK(vehStolen)
            AND NOT IS_PED_INJURED(PLAYER_PED_ID())
                SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehStolen)
                //SET_ENTITY_COORDS(vehStolen, vCar)
            ENDIF
//        ENDIF
    ENDIF
    // Event Failed
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		PRINTSTRING("\nre_CarTheft - cleaned up by IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)\n")
        missionCleanup()
    ENDIF
#ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_TRIGGERING_EVENT()
	FLOAT fRange
	SWITCH iSelectedVariation
		CASE ONE_PINK
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2311.452637,4156.304688,35.977329>>, <<-2177.045410,4387.405762,74.714813>>, 75.5)
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2292.802002,4190.615234,37.671700>>, <<-2200.723633,4351.133789,72.917801>>, 70.75)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2300.316650,4179.612305,36.981594>>, <<-2191.316895,4368.793945,74.896759>>, 70.75)
				RETURN TRUE
			ENDIF
		BREAK
		CASE TWO_GOLFER
			IF bTopVariation
				fRange = 80
			ELSE
				fRange = 80
			ENDIF
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-487.5048, -2157.9895, 8.2627>>, <<fRange,fRange,40>>)  //<< -498.4995, -2154.3574, 8.0289 >>
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
		IF NOT IS_PED_INJURED(pedVictim)
			IF IS_ENTITY_ON_SCREEN(pedVictim)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIEF_DEAD_OR_OUT_OF_RANGE()
	IF IS_PED_INJURED(pedThief)
		RETURN TRUE
	ELSE
		IF IS_VEHICLE_OK(vehStolen)
			IF NOT IS_ENTITY_AT_ENTITY(pedThief, vehStolen, <<10,10,10>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TRACK_PLAYERS_CAR()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != vehStolen
			AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != vehPlayerStart
				vehPlayerStart = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - got player's vehicle index")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Clean Up



// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)
vInput = in_coords.vec_coord[0]
fInput = in_coords.headings[0]

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	PRINTSTRING("\nre_CarTheft - cleaned up by HAS_FORCE_CLEANUP_OCCURRED\n")
    missionCleanup()
ENDIF

// Small section to fool the compiler
    PRINTNL()
    PRINTSTRING("re_CarTheft launched at ")
    PRINTVECTOR(vInput)
    PRINTSTRING(" with heading ")
    PRINTFLOAT(fInput)
    PRINTNL()

//----
GET_CURRENT_CAR_THEFT_VARIATION()

IF iSelectedVariation = TWO_GOLFER

	IF IS_MISSION_AVAILABLE(SP_MISSION_FBI_4_PREP_2) 
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - SP_MISSION_FBI_4_PREP_2 is available, this script will terminate.")
		TERMINATE_THIS_THREAD()
	ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())	
			VECTOR vTrig = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF vTrig.z > 23.0
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-743.392395,-2129.820313,12.076193>>, <<-708.892029,-2160.705322,19.703499>>, 124.500000)  //overpass
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Player probably on overpass, this script will terminate.")
				TERMINATE_THIS_THREAD()
			ENDIF
		ENDIF
	ENDIF
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_CARTHEFT, iSelectedVariation)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF


// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
    RUN_DEBUG()
	FLASH_RANDOM_EVENT_RETURN_ITEM_BLIP(blipScene, iVictimBlipFlash)
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		//SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	TRACK_PLAYERS_CAR()
    IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
			missionCleanup()
		ENDIF
        IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
            SWITCH ambStage
                CASE ambCanRun
                    IF LOADED_AND_CREATED_CAR_THEFT_SCENE()
						SET_CREATE_RANDOM_COPS(FALSE)
                        ambStage = (ambRunning)
                    ENDIF
                BREAK
                CASE ambRunning
					IF NOT IS_PED_INJURED(pedThief)
	                    IF NOT IS_PED_INJURED(PLAYER_PED_ID())	                   
	                    AND NOT IS_PED_INJURED(pedVictim)
	                    AND IS_VEHICLE_OK(vehStolen)
						AND NOT IS_PLAYER_INTERFERING_WITH_SCENE()
							
							IF IS_PLAYER_TRIGGERING_EVENT()
								//IF DOES_BLIP_EXIST(blipScene)
								//	REMOVE_BLIP(blipScene)
								//ENDIF
	                        	
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - player triggering event, doing -1 burnout")
								//TASK_VEHICLE_TEMP_ACTION(pedThief, vehStolen, TEMPACT_BURNOUT, -1)
								
								IF IS_VEHICLE_OK(vehPlayerStart)
									SET_VEHICLE_TYRES_CAN_BURST(vehPlayerStart, FALSE)
								ENDIF
	                            
								SET_RANDOM_EVENT_ACTIVE()
								SET_WANTED_LEVEL_MULTIPLIER(0.1)
								
								// Can't mod the car while event is active - B*1119355
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
								SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
								
								SETTIMERA(0)
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Set attract dialogue timer 0")
	                        ENDIF
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - amb state cleanup fired")
							IF NOT IS_PED_INJURED(pedVictim)
								TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
								SET_PED_KEEP_TASK(pedVictim, TRUE)
								WAIT(0)
							ENDIF
							missionCleanup()
	                    ENDIF
					ELSE
						IF NOT IS_PED_INJURED(pedVictim)
	                    AND IS_VEHICLE_OK(vehStolen)
							TASK_VEHICLE_DRIVE_WANDER(pedVictim, vehStolen, 20, victimDrivingmode)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
							WAIT(0)
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - cleaned up as pedThief is injured prior to theft\n")
						missionPassed()
					ENDIF
                BREAK
            ENDSWITCH
        ELSE
            PRINTNL()
            PRINTSTRING("IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()")
            PRINTNL()
			missionCleanup()
        ENDIF
    ELSE
        IF IS_PED_INJURED(pedThief)
            IF dialogueStruct.PedInfo[2].ActiveInConversation
                REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
            ENDIF
        ENDIF
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		RUN_VICTIM_REACTIONS()
        IF IS_VEHICLE_OK(vehStolen)
		AND NOT IS_VEHICLE_STUCK_ON_ROOF_WITH_PLAYER_IN()
		//AND NOT IS_ENTITY_ON_FIRE(vehStolen)                                                                      
            IF IS_TARGET_IN_RANGE()
                IF NOT IS_PED_INJURED(pedVictim)
                
                    SWITCH carStolenStage
                        CASE waitingForTheft
                        
                            IF NOT IS_PED_INJURED(pedThief)
                                IF IS_PED_IN_VEHICLE(pedThief, vehStolen)
									IF iSelectedVariation = TWO_GOLFER
										SET_ENTITY_LOAD_COLLISION_FLAG(pedThief, TRUE)
										SET_ENTITY_LOAD_COLLISION_FLAG(vehStolen, TRUE)
										OPEN_SEQUENCE_TASK(seq)
											//TASK_VEHICLE_TEMP_ACTION(NULL, vehStolen, TEMPACT_BURNOUT, 1000)
											TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehStolen, PLAYER_PED_ID(), MISSION_FLEE, 30.0, thiefDrivingmode, 200.0, 10.0, FALSE)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedThief, seq)
										CLEAR_SEQUENCE_TASK(seq)
									ELSE
										SET_ENTITY_LOAD_COLLISION_FLAG(pedThief, TRUE)
										OPEN_SEQUENCE_TASK(seq)
											TASK_VEHICLE_TEMP_ACTION(NULL, vehStolen, TEMPACT_BURNOUT, 1000)
											TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehStolen, PLAYER_PED_ID(), MISSION_FLEE, 40.0, thiefDrivingmode, 200.0, 10.0, TRUE)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedThief, seq)
										CLEAR_SEQUENCE_TASK(seq)
										//SET_VEHICLE_FORWARD_SPEED(vehStolen, 10)
									ENDIF
									vWheelPosL = GET_WORLD_POSITION_OF_ENTITY_BONE(vehStolen, GET_ENTITY_BONE_INDEX_BY_NAME(vehStolen, "wheel_lr"))
									vLocalWheelPosL = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehStolen, vWheelPosL)
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_wheel_burnout", vehStolen, vLocalWheelPosL+<<0,-1,-0.5>>, <<0,180,0>>, 0.25)
									
									vWheelPosR = GET_WORLD_POSITION_OF_ENTITY_BONE(vehStolen, GET_ENTITY_BONE_INDEX_BY_NAME(vehStolen, "wheel_rr"))
									vLocalWheelPosR = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehStolen, vWheelPosR)
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_wheel_burnout", vehStolen, vLocalWheelPosR+<<0,-1,-0.5>>, <<0,180,0>>, 0.25)
									SETTIMERA(0)
									
									IF NOT DOES_BLIP_EXIST(blipThief)
	                                    blipThief = CREATE_AMBIENT_BLIP_FOR_VEHICLE(vehStolen, TRUE)
	                                ENDIF
									SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
                                    carStolenStage = thiefIsGettingAway
								ELSE
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehStolen, PLAYER_PED_ID())
										IF NOT IS_PED_INJURED(pedThief)
											TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 150, -1)
											SET_PED_KEEP_TASK(pedThief, TRUE)
											WAIT(0)
										ENDIF
										CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - cleaned up as vehStolen has been damaged by player prior to theft\n")
										missionCleanup()
									ENDIF
                                ENDIF
							ELSE
								//IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sGood, CONV_PRIORITY_AMBIENT_HIGH)
									TASK_VEHICLE_DRIVE_WANDER(pedVictim, vehStolen, 20, victimDrivingmode)
									SET_PED_KEEP_TASK(pedVictim, TRUE)
									WAIT(0)
									CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - cleaned up as pedThief is injured prior to theft\n")
									missionPassed()
								//ENDIF
                            ENDIF
                            
                        BREAK
                        
                        CASE thiefIsGettingAway
						
							IF IS_VEHICLE_OK(vehStolen)
								IF iSelectedVariation = ONE_PINK
									IF TIMERA() < 5000
										IF TIMERA() > 2000 // For the first 2 secs, increase by 0.2 - after this, increase by 0.4
											vBoostSpeed += 0.4
										ELSE
											vBoostSpeed += 0.2
										ENDIF
										IF vBoostSpeed > 30.0
											vBoostSpeed = 30.0
										ENDIF
										SET_VEHICLE_FORWARD_SPEED(vehStolen, vBoostSpeed)
										
										VEHICLE_INDEX vehIdx
										vehIdx = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(vehStolen), 5.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
										
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF IS_VEHICLE_OK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
												IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehStolen)
													SETTIMERA(5000)
													CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - Player bumped stolen car during boost")
												ENDIF
											ENDIF
											// Attempted fix for url:bugstar:1888022
											IF IS_VEHICLE_OK(vehIdx)
												IF vehIdx <> vehStolen // Ensure we aren't checking the same vehicle, just in case
													IF IS_ENTITY_TOUCHING_ENTITY(vehIdx, vehStolen)
														SETTIMERA(5000)
														CPRINTLN(DEBUG_AMBIENT, "re_CarTheft - Stolen car hit random vehicle during boost")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "thiefIsGettingAway - Stolen Car speed = ", GET_ENTITY_SPEED(vehStolen), "ms")
								IF GET_VEHICLE_PETROL_TANK_HEALTH(vehStolen) < 700
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " thiefIsGettingAway - stolen car petrol tank health < 700 (", GET_VEHICLE_PETROL_TANK_HEALTH(vehStolen), "), cleanup")
									missionCleanup()
								ENDIF
							ENDIF
							
                            RUN_HINT_CAM()
                            IF IS_STOLEN_VEHICLE_TYRE_BURST()
                            //OR HAS_GUNSHOT_SCARED_THIEF()
                            OR HAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()
							OR IS_ENTITY_UPSIDEDOWN(vehStolen)
							OR IS_VEHICLE_STUCK_TIMER_UP(vehStolen, VEH_STUCK_ON_SIDE, 5000)
							OR IS_ENTITY_ON_FIRE(vehStolen)         
							OR IS_PED_INJURED(pedThief)
							OR NOT IS_PED_IN_VEHICLE(pedThief, vehStolen)
								PRINTSTRING("Paul - car caught!") PRINTNL()
								IF IS_AUDIO_SCENE_ACTIVE("RE_CAR_STEAL_SCENE")
									IF IS_VEHICLE_OK(vehStolen)
										REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehStolen)
									ENDIF
									STOP_AUDIO_SCENE("RE_CAR_STEAL_SCENE")
								ENDIF
								IF DOES_BLIP_EXIST(blipThief)
                                    REMOVE_BLIP(blipThief)
                                ENDIF
                                THIEF_ABANDONS_CAR()
								IF DOES_BLIP_EXIST(blipScene)
                                    REMOVE_BLIP(blipScene)
                                ENDIF
                                //CREATE_CONVERSATION(dialogueStruct, "RECTHAU", "RECTH_OUT", CONV_PRIORITY_AMBIENT_HIGH)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedVictim) // In case the robber knocked the victim over as they pulled away - that damage would be attributed to the player when we get in the car!
								BRING_VEHICLE_TO_HALT(vehStolen, 50, 5)
                                blipVehicle = CREATE_AMBIENT_BLIP_FOR_VEHICLE(vehStolen)
								storedPlayerVehDistance = VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(PLAYER_PED_ID()))
								storedDistance = VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(pedVictim))
								storedPlayerDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim))
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " carCaught - storedPlayerVehDistance = ", storedPlayerVehDistance, " storedDistance = ", storedDistance, " storedPlayerDistance = ", storedPlayerDistance)
                                KILL_CHASE_HINT_CAM(localChaseHintCamStruct)  
								carStolenStage = carCaught
                            ENDIF
                        BREAK

                        CASE carCaught
							
							IF IS_VEHICLE_OK(vehStolen)
								IF GET_VEHICLE_PETROL_TANK_HEALTH(vehStolen) < 700
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " carCaught - stolen car petrol tank health < 700 (", GET_VEHICLE_PETROL_TANK_HEALTH(vehStolen), "), cleanup")
									missionCleanup()
								ENDIF
							ENDIF
							
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " carCaught - player has wanted level, cleanup")
								missionCleanup()
							ENDIF
						
							// If the Thief's still alive, he'll be running away and combatting the player so blip him as an enemy
							IF NOT IS_ENTITY_DEAD(pedThief)
								IF NOT IS_PED_IN_VEHICLE(pedThief, vehStolen)
                                	UPDATE_AI_PED_BLIP(pedThief, blipStructRobber)
								ENDIF
                            ELSE
								CLEANUP_AI_PED_BLIP(blipStructRobber)
								//SAFE_RELEASE_PED(pedThief)
							ENDIF
							IF NOT IS_PED_INJURED(pedThief)
								IF NOT IS_PED_IN_COMBAT(pedThief, PLAYER_PED_ID())
									TASK_COMBAT_PED(pedThief, PLAYER_PED_ID())
									SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " carCaught - SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))")
								ENDIF
							ENDIF
							IF VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(PLAYER_PED_ID())) > (storedPlayerVehDistance+220)  //Was 150, Increased to fix 1173518
							OR VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(pedVictim)) > (storedDistance+220) 
							OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim)) > (storedPlayerDistance+220)
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " cleaned up as player or stolen vehicle have exceeded stored distances.")
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " storedPlayerVehDistance = ", VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(PLAYER_PED_ID())), " storedDistance = ", VDIST(GET_ENTITY_COORDS(vehStolen), GET_ENTITY_COORDS(pedVictim)), " storedPlayerDistance = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim)))
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehStolen)
									missionPassed()
								ELSE
									missionCleanup()
								ENDIF
							ENDIF
							
                            IF DOES_BLIP_EXIST(blipVehicle)
								//RUN_HINT_CAM()
                                IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehStolen)
                                    REMOVE_BLIP(blipVehicle)
                                    blipScene = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
									START_RETURN_ITEM_BLIP_FLASHING(iVictimBlipFlash)
                                    KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
                                    IF NOT bVictimReset
										IF iSelectedVariation = ONE_PINK  
											vVictim = <<-2258.759033,4274.059082,45.916603>> 											
										ELSE
											IF bTopVariation
												vVictim = <<-483.4162, -2160.8735, 8.3590>>
											ELSE	
												vVictim = <<-485.9905, -2153.5420, 8.1999>> 
											ENDIF
										ENDIF
										IF NOT IS_SPHERE_VISIBLE(vVictim,2)
										AND NOT IS_ENTITY_ON_SCREEN(pedVictim)
											SET_ENTITY_COORDS(pedVictim, vVictim)
	                                        SET_ENTITY_HEADING(pedVictim, fVictim)
	                                        bVictimReset = TRUE
										ENDIF
                                    ENDIF
                                    victimResponseStage = waitsOnReturn
									iPleaTimer = GET_GAME_TIMER()
								ELSE
									// Thief is dead/out of range
									// Entry is clear
									//
									IF IS_ENTITY_AT_ENTITY(vehStolen, pedVictim, <<20,20,7>>)
										GET_PLAYER_INFO()
										IF IS_THIEF_DEAD_OR_OUT_OF_RANGE()
											IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedVictim, vehStolen, VS_DRIVER)
												IF NOT excitedDialoguePlayed
													IF CREATE_CONVERSATION(dialogueStruct, sTextBlock, sJoy, CONV_PRIORITY_AMBIENT_HIGH)
													ENDIF
												ENDIF
					                            TASK_VEHICLE_DRIVE_WANDER(pedVictim, vehStolen, 35, victimDrivingmode)
												SET_PED_KEEP_TASK(pedVictim, TRUE)
												missionPassed(TRUE)
											ENDIF
										ENDIF
									ENDIF
                                ENDIF
                            ELSE
                                IF DOES_BLIP_EXIST(blipScene)
                                    IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehStolen)
                                        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<fLocateSize, fLocateSize, fLocateSize>>)
											IF CAN_PLAYER_START_CUTSCENE()
	                                            REMOVE_BLIP(blipScene)
												IF iSelectedVariation = ONE_PINK
													IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehStolen), 336, 90)
														animDict = "RANDOM@CAR_THEFT_1@MCS_2"
													ELSE
														animDict = "RANDOM@CAR_THEFT_1@MCS_2"
													ENDIF
												ELSE
													animDict = "RANDOM@CAR_THEFT_1@MCS_3"
												ENDIF
												REQUEST_ANIM_DICT(animDict)
												victimResponseStage = carIsReturned
	                                            carStolenStage = carReturned
											ENDIF
										ELSE
											SCALE_CUTSCENE_TRIGGERS_BASED_ON_PLAYER_SPEED(GET_ENTITY_COORDS(pedVictim), fLocateSize, fStoppingDistance)
                                        ENDIF
                                    ELSE
                                        REMOVE_BLIP(blipScene)
                                        blipVehicle = CREATE_AMBIENT_BLIP_FOR_VEHICLE(vehStolen)
                                        victimResponseStage = turnsToPlayer
                                    ENDIF
                                ENDIF
                            ENDIF
                        BREAK
                        CASE carReturned
	                        IF iSelectedVariation = ONE_PINK    
								IF HAS_RETURNED_CAR_CUT_FINISHED()
									IF IS_ENTITY_ALIVE(pedVictim)
										SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
									ENDIF
	                                missionPassed(TRUE)
	                            ENDIF
							ELSE
								//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-481.955933,-2154.127686,7.965325>>, <<-488.140961,-2166.093262,11.005272>>, 17.250000)
									IF HAS_RETURNED_CAR_CUT_FINISHED()
										IF IS_ENTITY_ALIVE(pedVictim)
											SET_PED_RESET_FLAG(pedVictim, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
										ENDIF
		                                missionPassed(TRUE)
		                            ENDIF
								//ENDIF
							ENDIF
                        BREAK
                    ENDSWITCH
                ELSE
					IF NOT IS_PED_INJURED(pedThief)
						IF IS_PED_IN_VEHICLE(pedThief, vehStolen)
							TASK_VEHICLE_MISSION_PED_TARGET(pedThief, vehStolen, PLAYER_PED_ID(), MISSION_FLEE, 50, thiefDrivingmode, 10.0, 10.0, FALSE)
							SET_PED_KEEP_TASK(pedThief, TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " ped injured - SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))")
						ELSE
							TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 150, -1)
							SET_PED_KEEP_TASK(pedThief, TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " ped injured - SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(pedThief))")
						ENDIF
					ENDIF
                    PRINTNL()
                    PRINTSTRING("re_cartheft - victim is dead.")
                    PRINTNL()
					WAIT(0)
                    missionCleanup()
                ENDIF
            ELSE
                PRINTNL()
                PRINTSTRING("re_cartheft - vehStolen is out of range.")
                PRINTNL()
				IF carStolenStage = thiefIsGettingAway
					IF IS_VEHICLE_OK(vehStolen)
						DELETE_VEHICLE(vehStolen)
					ENDIF
					IF NOT IS_PED_INJURED(pedThief)
						DELETE_PED(pedThief)
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedVictim)
					IF IS_ENTITY_PLAYING_ANIM(pedVictim, animAgitatedIG, "agitated_idle_a")
						STOP_ENTITY_ANIM(pedVictim,"waiting",animAgitatedIG,REALLY_SLOW_BLEND_OUT)
					ENDIF
					PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_CURSE_HIGH")
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkAway, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE")
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedVictim, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
                missionCleanup()
            ENDIF
        ELSE
            PRINTNL()
            PRINTSTRING("re_cartheft - vehStolen no longer driveable.")
            PRINTNL()
			IF NOT IS_PED_INJURED(pedThief)
				TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 150, -1)
				SET_PED_KEEP_TASK(pedThief, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(vehStolen)
				SET_VEHICLE_ENGINE_HEALTH(vehStolen, 50)
			ENDIF
            missionCleanup()
        ENDIF

    ENDIF

ENDWHILE


ENDSCRIPT


