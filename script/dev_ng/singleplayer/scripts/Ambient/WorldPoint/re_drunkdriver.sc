//										Random Events: Drunk driver

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "AggroSupport.sch"
USING "commands_path.sch"
USING "dialogue_public.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "cutscene_public.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "altruist_cult.sch"
USING "taxi_functions.sch"
USING "buddy_head_track_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventVariations
	eV_TARGET_NONE,
	eV_DRUNK_DRIVER,				//a		a guy staggers towards his car
	eV_WITH_PASSENGER,				//b		a guy staggers towards his car with an equally drunk passenger
	eV_TARGET_END
ENDENUM
eventVariations thisEvent = eV_TARGET_NONE

ENUM eventStage
	eS_INITIAL_STAGE,
	eS_ARGUE_WITH_BUDDY,
	eS_GETTING_TO_VEHICLE,
	eS_ASK_FOR_LIFT,
	eS_WAIT_FOR_LIFT,
	eS_DRIVE_AWAY_TASK,
	eS_DRIVING_AWAY,
	eS_PLAYER_INTERVENTION,
	eS_DROP_OFF,
	eS_AT_CULT
ENDENUM
eventStage thisStage = eS_INITIAL_STAGE

BOOL bAssetsLoaded
VECTOR vInput

//BOOL bRunDebugSelection

BOOL bVariablesInitialised
BOOL bSceneSetUp
BOOL bDrivingHome

INT i //counter
//INT iEventVariation

INT iInterventionStage = 0 
INT iTurnStage[2]

VECTOR vCreateDriverPos
FLOAT fCreateDriverHdg

VECTOR vCreatePassengerPos
FLOAT fCreatePassengerHdg

VECTOR vCreateVehiclePos
FLOAT fCreateVehicleHdg
INT iDrunksCarHealth

VECTOR vDropOffPoint
VECTOR vDropWalkTo

VECTOR vCult = << -1034.6, 4918.6, 205.9 >>

INT iNumberOfPedsInEvent
PED_INDEX pedDrunk[2]
VEHICLE_INDEX vehDrunksCar
VEHICLE_INDEX vehImpound
//VEHICLE_INDEX vehTempTrain
MODEL_NAMES modelDrunkEventMalePed
MODEL_NAMES modelDrunkEventSecondPed
MODEL_NAMES modelDrunksVehicle

SEQUENCE_INDEX seq
REL_GROUP_HASH rghDrunkPeds

BOOL bSitStill
BLIP_INDEX blipDropOff
BLIP_INDEX blipCult

//BOOL bReminderPrompt = FALSE
BOOL bPointedAtCar
//BOOL bOfferedLift = FALSE

BOOL bVomiting
BOOL bVomiting1
BOOL bVomiting2
BOOL bSickingDone
//BOOL bVomitCamHelp
BOOL bGuyBeSick
//VECTOR vSpeedThisFrame
//VECTOR vSpeedPrevFrame
BOOL bSickWarning
INT iSickStage, SoundId
//INT iVelocityCheckTimer
STRING strDrunkIdleVariation
STRING strVomitVariation

//for Dialogue
BOOL bWaitingChatDialogue1
BOOL bWaitingChatDialogue2
BOOL bJumpInBackLine
BOOL bAskedToWaitToGetIn
BOOL bAskedToGetBackIn
BOOL bConversationPaused
BOOL bPlayerInterfering
BOOL bUnsuitableVehRemark

INT iInitiatedEventTimer

INT iCutsceneToPlay = -1
INT iCutsceneWaitTimer
INT iDropOffCutStage
CAMERA_INDEX camCutscene
CAMERA_INDEX camCutsceneOrigin
//CAMERA_INDEX camVomit
//CHASE_HINT_CAM_STRUCT structHintCam
CAMERA_INDEX camCutsceneDest
BOOL bDropOffDone
BOOL bDropOffSkipped

BOOL bPostSickDialogue
BOOL bPreSickDialogue
BOOL bGenChatDialogue
BOOL bGenChatResponse

BOOL bConvOnHold
TEXT_LABEL_23 ConvResumeLabel = ""
TEXT_LABEL_23 strCurrentRoot

INT iRandomChatLineTimer

INT iBitFieldDontCheck
//INT lockOnTimer
//EAggro aggroReason
//BOOL bAggroed

BLIP_INDEX blipInitialMarker
VECTOR vInitialMarkerPos
BLIP_INDEX blipDrunkPed[2]
BLIP_INDEX blipVehicle

INT time_of_last_frame
VECTOR velocity_last_frame
INT iDrivingCounter
FLOAT fStoredDistance
INT iWheelTimer = -1

INT iStationaryStartTimer, iStationaryCurrentTimer
BOOL bStartStationaryTimer, bStationaryWarned

INT iGettingALiftStage

INT iJourneyTimer

//PTFX_ID ptSpray

BOOL bDoFullCleanUp

structPedsForConversation DrunkDriverDialogueStruct 

INT iWhichWorldPoint
VECTOR vWorldPoint1 = << -1017.3451, -1354.1962, 4.4568 >>
VECTOR vWorldPoint2 = << 2003.4564, 3071.1018, 46.0499>>

CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
INT iSelectedVariation

INT iCarShaggingStage
INT iStageShaggingTimer
//BOOL bOtherSideBounce

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //
PROC missionCleanup()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@@@ \n")
    #IF IS_DEBUG_BUILD
        IF can_cleanup_script_for_debug
			FOR i = 0 to (iNumberOfPedsInEvent -1)
				IF DOES_ENTITY_EXIST(pedDrunk[i])
					DELETE_PED(pedDrunk[i])
				ENDIF
			ENDFOR
			IF DOES_ENTITY_EXIST(vehDrunksCar)
				DELETE_VEHICLE(vehDrunksCar)
			ENDIF
		ENDIF
    #ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_enter_m")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_idle_m")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_exit_m")
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF bDoFullCleanUp
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		IF g_bAltruistCultFinalDialogueHasPlayed
			TRIGGER_MUSIC_EVENT("AC_STOP")
		ENDIF
		RESET_CULT_STATUS()
		
		KILL_FACE_TO_FACE_CONVERSATION()//_DO_NOT_FINISH_LAST_LINE()
		SET_NO_DUCKING_FOR_CONVERSATION(FALSE)
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreateVehiclePos - << 15, 15, 10 >>, vCreateVehiclePos + << 15, 15, 10 >>, TRUE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vDropOffPoint - << 15, 15, 10 >>, vDropOffPoint + << 15, 15, 10 >>, TRUE)
		DISABLE_TAXI_HAILING(FALSE)
		
		IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
				SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehImpound)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpound)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehImpound) > 50
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedDrunk[0])
			IF NOT IS_PED_INJURED(pedDrunk[0])
				SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_CanSayFollowedByPlayerAudio, TRUE)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
					RESET_PED_LAST_VEHICLE(pedDrunk[0])
				ENDIF
				IF IS_PED_IN_GROUP(pedDrunk[0])
					REMOVE_PED_FROM_GROUP(pedDrunk[0])
				ENDIF
			ENDIF
		ENDIF
		FOR i = 0 to (iNumberOfPedsInEvent -1)
			IF DOES_ENTITY_EXIST(pedDrunk[i])
				IF NOT IS_PED_INJURED(pedDrunk[i])
					SET_PED_CONFIG_FLAG(pedDrunk[i], PCF_CanSayFollowedByPlayerAudio, TRUE)
					IF NOT IS_PED_IN_ANY_VEHICLE(pedDrunk[i])
						RESET_PED_LAST_VEHICLE(pedDrunk[i])
					ENDIF
					SET_PED_CAN_BE_TARGETTED(pedDrunk[i], TRUE)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghDrunkPeds, RELGROUPHASH_PLAYER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[i], FALSE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(pedDrunk[i])
			ENDIF
		ENDFOR
		FOR i = 0 to (iNumberOfPedsInEvent - 1)
			IF DOES_BLIP_EXIST(blipDrunkPed[i])
				REMOVE_BLIP(blipDrunkPed[i])
			ENDIF
		ENDFOR
//		IF (ptSpray <> NULL)
//			STOP_PARTICLE_FX_LOOPED(ptSpray)
//			ptSpray = NULL
//		ENDIF
//		STOP_SOUND(SoundId)
		IF thisEvent = eV_DRUNK_DRIVER
			SET_ROADS_BACK_TO_ORIGINAL(<< 42.7808, -1324.4054, -10 >>, << 98.2450, -1277.1740, 10 >>)
		ENDIF
		IF DOES_CAM_EXIST(camCutscene)
			IF IS_CAM_ACTIVE(camCutscene)
				SET_CAM_ACTIVE(camCutscene, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED(RE_DRUNKDRIVER, iSelectedVariation)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	missionCleanup()
ENDPROC

FUNC VECTOR getWorldPoint()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint1)
		iWhichWorldPoint = iWorldPoint1
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2)
			iWhichWorldPoint = iWorldPoint2
		ENDIF
		IF iWhichWorldPoint = iWorldPoint1
			iSelectedVariation = iWorldPoint1
			thisEvent = eV_DRUNK_DRIVER
			RETURN vWorldPoint1
		ENDIF
		IF iWhichWorldPoint = iWorldPoint2
			iSelectedVariation = iWorldPoint2
			thisEvent = eV_WITH_PASSENGER
			RETURN vWorldPoint2
		ENDIF
	ENDIF
	RETURN << 0, 0, 0 >>
ENDFUNC

PROC initialiseEventVariables()
	IF thisEvent = eV_DRUNK_DRIVER
		vInitialMarkerPos = << -1018.2382, -1350.1014, 4.4750 >>
		iNumberOfPedsInEvent = 2
		modelDrunkEventMalePed = a_m_y_beachvesp_01
		modelDrunkEventSecondPed = a_m_y_beachvesp_02
		vCreateDriverPos = << -1016.7623, -1356.4590, 4.5531 >>
		fCreateDriverHdg = 247.8087
		vCreatePassengerPos = << -1015.5720, -1357.0810, 4.5520 >>
		fCreatePassengerHdg = 66.3861
		vCreateVehiclePos = << -1027.7618, -1341.6439, 4.4614 >>
		fCreateVehicleHdg = 76.9418
		modelDrunksVehicle = BALLER2
		vDropOffPoint = << 160.8892, -111.4167, 61.2999 >>
		vDropWalkTo = << 159.2191, -117.0664, 61.2999 >>
		bVariablesInitialised = TRUE
	ENDIF
	IF thisEvent = eV_WITH_PASSENGER
		vInitialMarkerPos = << 1893.8561, 3714.3457, 31.7771 >>
		iNumberOfPedsInEvent = 2
		modelDrunkEventMalePed = A_M_Y_GenStreet_01
		modelDrunkEventSecondPed = A_F_Y_Hipster_03
		vCreateDriverPos = << 1893.8561, 3714.3457, 31.7771 >> //<< 1894.1733, 3711.9258, 31.7785 >>
		fCreateDriverHdg = 252.0142 //238.6517
		vCreatePassengerPos = << 1894.7627, 3714.1604, 31.7605 >> //<< 1895.7488, 3711.3394, 31.7717 >>
		fCreatePassengerHdg = 86.1102 //117.0789
		vCreateVehiclePos = << 1888.2565, 3717.1892, 31.8394 >>
		fCreateVehicleHdg = 299.4672
		modelDrunksVehicle = EMPEROR
		vDropOffPoint =	<< 1120.5507, 2647.3071, 36.9963 >>
		vDropWalkTo = << 1121.8657, 2641.7068, 37.1487 >>
		bVariablesInitialised = TRUE
	ENDIF
 ENDPROC

PROC loadAssets()
	REQUEST_MODEL(modelDrunkEventMalePed)
	REQUEST_MODEL(modelDrunksVehicle)
	REQUEST_MODEL(modelDrunkEventSecondPed)
	IF thisEvent = eV_DRUNK_DRIVER
		REQUEST_PTFX_ASSET()
		REQUEST_ANIM_DICT("random@drunk_driver_1")
		REQUEST_ANIM_SET("MOVE_M@DRUNK@VERYDRUNK")
		REQUEST_SCRIPT_AUDIO_BANK("Taxi_Vomit")
	ELIF thisEvent = eV_WITH_PASSENGER
		REQUEST_ANIM_DICT("random@drunk_driver_2")
		REQUEST_ANIM_DICT("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		REQUEST_ANIM_DICT("MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP")
	ENDIF
	REQUEST_ANIM_SET("MOVE_M@DRUNK@MODERATEDRUNK")
	IF HAS_MODEL_LOADED(modelDrunkEventMalePed)
	AND HAS_MODEL_LOADED(modelDrunksVehicle)
	AND HAS_ANIM_SET_LOADED("MOVE_M@DRUNK@MODERATEDRUNK")
	AND HAS_MODEL_LOADED(modelDrunkEventSecondPed)
		IF thisEvent = eV_DRUNK_DRIVER
			IF HAS_PTFX_ASSET_LOADED()
			AND HAS_ANIM_DICT_LOADED("random@drunk_driver_1")
			AND HAS_ANIM_SET_LOADED("MOVE_M@DRUNK@VERYDRUNK")
			AND REQUEST_SCRIPT_AUDIO_BANK("CONSTRUCTION_ACCIDENT_1")
				bAssetsLoaded = TRUE
			ENDIF
		ELIF thisEvent = eV_WITH_PASSENGER
			IF HAS_ANIM_DICT_LOADED("random@drunk_driver_2")
			AND HAS_ANIM_DICT_LOADED("MOVE_M@DRUNK@SLIGHTLYDRUNK")
			AND HAS_ANIM_DICT_LOADED("MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP")
				bAssetsLoaded = TRUE
			ENDIF
		ENDIF
	ELSE
		REQUEST_MODEL(modelDrunkEventMalePed)
		REQUEST_MODEL(modelDrunksVehicle)
		REQUEST_MODEL(modelDrunkEventSecondPed)
		IF thisEvent = eV_DRUNK_DRIVER
			REQUEST_PTFX_ASSET()
			REQUEST_ANIM_DICT("random@drunk_driver_1")
			REQUEST_ANIM_SET("MOVE_M@DRUNK@VERYDRUNK")
			REQUEST_SCRIPT_AUDIO_BANK("Taxi_Vomit")
		ELIF thisEvent = eV_WITH_PASSENGER
			REQUEST_ANIM_DICT("random@drunk_driver_2")
			REQUEST_ANIM_DICT("MOVE_M@DRUNK@SLIGHTLYDRUNK")
			REQUEST_ANIM_DICT("MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP")
		ENDIF
		REQUEST_ANIM_SET("MOVE_M@DRUNK@MODERATEDRUNK")
	ENDIF
ENDPROC

PROC changeBlips()
	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	FOR i = 0 to (iNumberOfPedsInEvent - 1)
		IF NOT DOES_BLIP_EXIST(blipDrunkPed[i])
			blipDrunkPed[i] = CREATE_BLIP_FOR_PED(pedDrunk[i])
			SHOW_HEIGHT_ON_BLIP(blipDrunkPed[i], FALSE)
		ENDIF
	ENDFOR
ENDPROC

PROC createScene()

	IF NOT bSceneSetUp
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
		
		CLEAR_AREA(vCreateDriverPos, 8, TRUE)
		ADD_SCENARIO_BLOCKING_AREA(vCreateDriverPos - << 8, 8, 8 >>, vCreateDriverPos + << 8, 8, 8 >>)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreateVehiclePos - << 15, 15, 10 >>, vCreateVehiclePos + << 15, 15, 10 >>, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vDropOffPoint - << 15, 15, 10 >>, vDropOffPoint + << 15, 15, 10 >>, FALSE)
		SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		ADD_RELATIONSHIP_GROUP("rghDrunkPeds", rghDrunkPeds)
		
		vehDrunksCar = CREATE_VEHICLE(modelDrunksVehicle, vCreateVehiclePos, fCreateVehicleHdg)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(vehDrunksCar)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehDrunksCar)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelDrunksVehicle, TRUE)
		SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		SET_VEHICLE_DISABLE_TOWING(vehDrunksCar, TRUE)
		iDrunksCarHealth = GET_ENTITY_HEALTH(vehDrunksCar)
		
		// create first guy
		pedDrunk[0] = CREATE_PED(PEDTYPE_MISSION, modelDrunkEventMalePed, vCreateDriverPos, fCreateDriverHdg)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[0] , TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDrunk[0], rghDrunkPeds)
		SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedDrunk[0], FA_FORCE_EXIT_VEHICLE, TRUE)
//		SET_PED_FLEE_ATTRIBUTES(pedDrunk[0], FA_DISABLE_AMBIENT_CLIPS, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelDrunkEventMalePed)
		STOP_PED_SPEAKING(pedDrunk[0], TRUE)
		
		// create other event ped at offset from the first guy 
		pedDrunk[1] = CREATE_PED(PEDTYPE_MISSION, modelDrunkEventSecondPed, vCreatePassengerPos, fCreatePassengerHdg)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[1] , TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDrunk[1], rghDrunkPeds)
		SET_PED_CONFIG_FLAG(pedDrunk[1], PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedDrunk[1], FA_FORCE_EXIT_VEHICLE, TRUE)
//		SET_PED_FLEE_ATTRIBUTES(pedDrunk[1], FA_DISABLE_AMBIENT_CLIPS, TRUE)
		STOP_PED_SPEAKING(pedDrunk[1], TRUE)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghDrunkPeds, RELGROUPHASH_PLAYER)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modelDrunkEventSecondPed)
		
		IF thisEvent = eV_DRUNK_DRIVER
//			VECTOR scenePosition = << -1015.5720, -1357.0810, 4.5520 >>
//			VECTOR sceneRotation = << -0.0000, 0.0000, 150.0000 >>
//			INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			TASK_SYNCHRONIZED_SCENE(pedDrunk[0], sceneId, "random@drunk_driver_1", "a_square_up", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//			TASK_SYNCHRONIZED_SCENE(pedDrunk[1], sceneId, "random@drunk_driver_1", "drunk_driver_stand_loop_dd2", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_1", "drunk_driver_stand_loop_dd1", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//			SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
			
			TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_1", "drunk_driver_stand_loop_dd2", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//			TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_1", "a_square_up", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
			SET_PED_MOVEMENT_CLIPSET(pedDrunk[0], "MOVE_M@DRUNK@VERYDRUNK")
			SET_PED_MOVEMENT_CLIPSET(pedDrunk[1], "MOVE_M@DRUNK@MODERATEDRUNK")
			
			SET_PED_COMPONENT_VARIATION(pedDrunk[0], PED_COMP_HEAD, 1, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedDrunk[0], PED_COMP_TORSO, 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedDrunk[0], PED_COMP_LEG, 0, 1, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_HEAD, 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_TORSO, 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_LEG, 0, 1, 0) //(lowr)
			
			REMOVE_VEHICLE_WINDOW(vehDrunksCar, SC_WINDOW_FRONT_RIGHT)
			
			SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_DisableShockingDrivingOnPavementEvents, TRUE)
			SET_PED_CAN_BE_TARGETTED(pedDrunk[0], FALSE)
			
			SET_AMBIENT_VOICE_NAME(pedDrunk[0], "REDR1Drunk1_AI_Drunk")
			SET_AMBIENT_VOICE_NAME(pedDrunk[1], "A_M_Y_VINEWOOD_01_BLACK_MINI_01")
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 3, pedDrunk[0], "REDR1Drunk1")
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 4, pedDrunk[1] , "REDR1Drunk2")
		ENDIF
		
		IF thisEvent = eV_WITH_PASSENGER
			ADD_SCENARIO_BLOCKING_AREA(vDropOffPoint - << 30, 50, 10 >>, vDropOffPoint + << 30, 50, 10 >>)
			SET_VEHICLE_HAS_STRONG_AXLES(vehDrunksCar, TRUE)
			
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_HEAD, 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_HAIR, 0, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_TORSO, 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_LEG, 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(pedDrunk[1], PED_COMP_SPECIAL, 0, 0, 0) //(accs)
			
			//SO THE PEDS WON'T BE INTERUPTED IF THE PLAYER ENTERS THE CAR DURING SEX
			SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_PreventPedFromReactingToBeingJacked, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[1], PCF_PreventPedFromReactingToBeingJacked, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[1], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[0], PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
			SET_PED_CONFIG_FLAG(pedDrunk[1], PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
			
			SET_PED_CAN_BE_TARGETTED(pedDrunk[0], FALSE)
			SET_PED_CAN_BE_TARGETTED(pedDrunk[1], FALSE)
			
//			VECTOR scenePositionGuy = << 1988.514, 3059.210, 47.073 >>
//			VECTOR sceneRotationGuy = << 0.000, 0.000, -161.000 >>
//			INT sceneIdGuy = CREATE_SYNCHRONIZED_SCENE(scenePositionGuy, sceneRotationGuy)
//			TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_2", "idle_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			TASK_START_SCENARIO_IN_PLACE(pedDrunk[0], "WORLD_HUMAN_BUM_STANDING")
			TASK_LOOK_AT_ENTITY(pedDrunk[0], pedDrunk[1], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//			SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdGuy, TRUE)
			
//			VECTOR scenePositionGirl = << 1989.396, 3057.264, 47.095 >>
//			VECTOR sceneRotationGirl = << -0.000, 0.000, 9.000 >>
//			INT sceneIdCirl = CREATE_SYNCHRONIZED_SCENE(scenePositionGirl, sceneRotationGirl)
//			TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_2", "idle_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			TASK_START_SCENARIO_IN_PLACE(pedDrunk[1], "WORLD_HUMAN_BUM_STANDING")
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(pedDrunk[1], FALSE)
			TASK_LOOK_AT_ENTITY(pedDrunk[1], pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//			SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdCirl, TRUE)
			SET_PED_MOVEMENT_CLIPSET(pedDrunk[0], "MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP")
			SET_PED_MOVEMENT_CLIPSET(pedDrunk[1], "MOVE_M@DRUNK@SLIGHTLYDRUNK")
			
			SET_AMBIENT_VOICE_NAME(pedDrunk[0], "A_M_Y_BeachVesp_01_White_Mini_01")
			SET_AMBIENT_VOICE_NAME(pedDrunk[1], "A_F_Y_EastSA_03_Latino_FULL_01")
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 3, pedDrunk[0], "REDR2DrunkM")
			ADD_PED_FOR_DIALOGUE(DrunkDriverDialogueStruct, 4, pedDrunk[1] , "REDR2DrunkF")
			
//			PRELOAD_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DCA", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		
//		blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vInitialMarkerPos)
//		changeBlips()
		
		bSceneSetUp = TRUE
	ENDIF
	
ENDPROC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
//	IF DOES_ENTITY_EXIST(vehDrunksCar)
////	IF NOT IS_PED_INJURED(pedDrunk[0])
//		vehTempTrain = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(vehDrunksCar, FALSE), 10, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
//		IF DOES_ENTITY_EXIST(vehTempTrain)
//			PRINTSTRING("\n@@@@@@@@@@@@@@ DOES_ENTITY_EXIST @@@@@@@@@@@@@@@@\n")
//			IF IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(vehTempTrain))
//				PRINTSTRING("\n@@@@@@@@@@@@@@ IS_THIS_MODEL_A_TRAIN @@@@@@@@@@@@@@@@\n")
//				IF GET_DISTANCE_BETWEEN_ENTITIES(vehDrunksCar, vehTempTrain) < 10
//					PRINTSTRING("\n@@@@@@@@@@@@@@ GET_DISTANCE_BETWEEN_ENTITIES @@@@@@@@@@@@@@@@\n")
//					IF IS_ENTITY_TOUCHING_ENTITY(vehDrunksCar, vehTempTrain)
//						PRINTSTRING("\n@@@@@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY @@@@@@@@@@@@@@@@\n")
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	SET_DISABLE_RANDOM_TRAINS_THIS_FRAME(TRUE)
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		RETURN TRUE
	ENDIF
	
	IF IS_BULLET_IN_AREA(vInitialMarkerPos, 40)
	OR IS_BULLET_IN_AREA(GET_PLAYER_COORDS(PLAYER_ID()), 10, FALSE)
	OR IS_PROJECTILE_IN_AREA(vInitialMarkerPos - << 40, 40, 40 >>, vInitialMarkerPos + << 40, 40, 40 >>)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedDrunk[0])
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDrunk[0], PLAYER_PED_ID())
		OR IS_ENTITY_IN_WATER(pedDrunk[0])
			RETURN TRUE
		ENDIF
		IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDrunk[0], GET_PLAYERS_LAST_VEHICLE())
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedDrunk[0])
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedDrunk[0])
				IF CAN_PED_SEE_HATED_PED(pedDrunk[0], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedDrunk[1])
		IF NOT IS_PED_INJURED(pedDrunk[1])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDrunk[1], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDrunk[1], GET_PLAYERS_LAST_VEHICLE())
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedDrunk[1])
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedDrunk[1])
					IF CAN_PED_SEE_HATED_PED(pedDrunk[1], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Determine if the vehicle has a low profile so we can use the right animations
/// RETURNS:
///    TRUE if the vehicle has a low profile
FUNC BOOL IS_PLAYERS_VEHICLE_LOW()
	IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
		IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(pedDrunk[0]))
			VEHICLE_LAYOUT_TYPE LayoutHash = INT_TO_ENUM(VEHICLE_LAYOUT_TYPE, GET_VEHICLE_LAYOUT_HASH(GET_VEHICLE_PED_IS_IN(pedDrunk[0])))
			IF (LayoutHash = LAYOUT_LOW)
			OR (LayoutHash = LAYOUT_LOW_BFINJECTION)
			OR (LayoutHash = LAYOUT_LOW_CHEETAH)
			OR (LayoutHash = LAYOUT_LOW_DUNE)
			OR (LayoutHash = LAYOUT_LOW_INFERNUS)
			OR (LayoutHash = LAYOUT_LOW_RESTRICTED)
			OR (LayoutHash = LAYOUT_LOW_SENTINEL2)
			OR (LayoutHash = LAYOUT_LOW_BLADE)			// DLC vehicle
			OR (LayoutHash = LAYOUT_LOW_TURISMOR) 		// DLC vehicle
			OR (LayoutHash = LAYOUT_LOW_FURORE) 		// DLC vehicle
			OR (LayoutHash = LAYOUT_LOW_OSIRIS) 		// DLC vehicle
//			OR IS_PED_IN_MODEL(pedDrunk[0], Phoenix)
//			OR IS_PED_IN_MODEL(pedDrunk[0], coquette)
//			OR IS_PED_IN_MODEL(pedDrunk[0], dune)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Cheetah)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Cogcabrio)
//			OR IS_PED_IN_MODEL(pedDrunk[0], JB700)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Monroe)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Ninef)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Ninef2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], RapidGT)
//			OR IS_PED_IN_MODEL(pedDrunk[0], RapidGT2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Stinger)
//			OR IS_PED_IN_MODEL(pedDrunk[0], StingerGT)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Ztype)
//			OR IS_PED_IN_MODEL(pedDrunk[0], banshee)
//			OR IS_PED_IN_MODEL(pedDrunk[0], bullet)
//			OR IS_PED_IN_MODEL(pedDrunk[0], carbonizzare)
//			OR IS_PED_IN_MODEL(pedDrunk[0], comet2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], ENTITYXF)
//			OR IS_PED_IN_MODEL(pedDrunk[0], f620)
//			OR IS_PED_IN_MODEL(pedDrunk[0], infernus)
//			OR IS_PED_IN_MODEL(pedDrunk[0], surano)
//		//	OR IS_PED_IN_MODEL(pedDrunk[0], veloce)
//		//	OR IS_PED_IN_MODEL(pedDrunk[0], venem)
//			OR IS_PED_IN_MODEL(pedDrunk[0], voltic)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determine if the vehicle is a van so we can use the right animations
/// RETURNS:
///    TRUE if the vehicle is a van
FUNC BOOL IS_PLAYERS_VEHICLE_VAN()
	IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
		IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(pedDrunk[0]))
			VEHICLE_LAYOUT_TYPE LayoutHash = INT_TO_ENUM(VEHICLE_LAYOUT_TYPE, GET_VEHICLE_LAYOUT_HASH(GET_VEHICLE_PED_IS_IN(pedDrunk[0])))
			IF (LayoutHash = LAYOUT_VAN)
			OR (LayoutHash = LAYOUT_VAN_BODHI)
			OR (LayoutHash = LAYOUT_VAN_BOXVILLE)
			OR (LayoutHash = LAYOUT_VAN_CADDY)
			OR (LayoutHash = LAYOUT_VAN_JOURNEY)
			OR (LayoutHash = LAYOUT_VAN_MULE)
			OR (LayoutHash = LAYOUT_VAN_POLICE)
			OR (LayoutHash = LAYOUT_VAN_TRASH)
			OR (LayoutHash = LAYOUT_4X4)
			OR (LayoutHash = LAYOUT_BISON)
			OR (LayoutHash = LAYOUT_RANGER)
			OR (LayoutHash = LAYOUT_RANGER_SWAT)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Granger)
//			OR IS_PED_IN_MODEL(pedDrunk[0], rebel)
//			OR IS_PED_IN_MODEL(pedDrunk[0], rebel2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], sandking)
//			OR IS_PED_IN_MODEL(pedDrunk[0], sandking2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Patriot)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Surfer)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Surfer2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], mesa)
//			OR IS_PED_IN_MODEL(pedDrunk[0], rancherxl)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Tiptruck2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], mixer)
//			OR IS_PED_IN_MODEL(pedDrunk[0], mixer2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], rubble)
//			OR IS_PED_IN_MODEL(pedDrunk[0], scrap)
//			OR IS_PED_IN_MODEL(pedDrunk[0], tiptruck)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Armytanker)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Barracks)
//			OR IS_PED_IN_MODEL(pedDrunk[0], barracks2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Pranger)
//			OR IS_PED_IN_MODEL(pedDrunk[0], ambulance)
//			OR IS_PED_IN_MODEL(pedDrunk[0], firetruk)
//			OR IS_PED_IN_MODEL(pedDrunk[0], policet)
//			OR IS_PED_IN_MODEL(pedDrunk[0], riot)
//			OR IS_PED_IN_MODEL(pedDrunk[0], sheriff2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], stockade)
//			OR IS_PED_IN_MODEL(pedDrunk[0], stockade3)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Biff)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Hauler)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Packer)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Trash)
//			OR IS_PED_IN_MODEL(pedDrunk[0], benson)
//			OR IS_PED_IN_MODEL(pedDrunk[0], phantom)
//			OR IS_PED_IN_MODEL(pedDrunk[0], pounder)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Utillitruck)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Utillitruck2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Utillitruck3)
//			OR IS_PED_IN_MODEL(pedDrunk[0], dloader)
//			OR IS_PED_IN_MODEL(pedDrunk[0], flatbed)
//			OR IS_PED_IN_MODEL(pedDrunk[0], tourbus)
//			OR IS_PED_IN_MODEL(pedDrunk[0], towtruck)
//			OR IS_PED_IN_MODEL(pedDrunk[0], towtruck2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Camper)
//			OR IS_PED_IN_MODEL(pedDrunk[0], Taco)
//			OR IS_PED_IN_MODEL(pedDrunk[0], boxville)
//			OR IS_PED_IN_MODEL(pedDrunk[0], boxville2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], burrito)
//			OR IS_PED_IN_MODEL(pedDrunk[0], burrito2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], burrito3)
//			OR IS_PED_IN_MODEL(pedDrunk[0], burrito4)
//			OR IS_PED_IN_MODEL(pedDrunk[0], gburrito)
//			OR IS_PED_IN_MODEL(pedDrunk[0], journey)
//			OR IS_PED_IN_MODEL(pedDrunk[0], mule)
//			OR IS_PED_IN_MODEL(pedDrunk[0], mule2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], pony)
//			OR IS_PED_IN_MODEL(pedDrunk[0], rumpo)
//			OR IS_PED_IN_MODEL(pedDrunk[0], speedo)
//			OR IS_PED_IN_MODEL(pedDrunk[0], speedo2)
//			OR IS_PED_IN_MODEL(pedDrunk[0], youga)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GIVE_PED_CORRECT_DRUNK_ANIMS()
	IF IS_PLAYERS_VEHICLE_VAN()
		strDrunkIdleVariation = "drunk_idle_van"
		strVomitVariation = "vomit_van"
	ELIF IS_PLAYERS_VEHICLE_LOW()
		strDrunkIdleVariation = "drunk_idle_low"
		strVomitVariation = "vomit_low"
	ELSE
		strDrunkIdleVariation = "drunk_idle"
		strVomitVariation = "vomit_outside"
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_MOVING_LEFT_STICK(INT iLEFT_STICK_THRESHOLD = 64)
	
	INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) -128
	INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) -128
	
	IF ReturnLeftX < iLEFT_STICK_THRESHOLD
	AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
	AND ReturnLeftY < iLEFT_STICK_THRESHOLD
	AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()

	INT iNumberOfSeatsRequired
	IF thisEvent = eV_DRUNK_DRIVER
		iNumberOfSeatsRequired = 1
	ENDIF
	IF thisEvent = eV_WITH_PASSENGER
		iNumberOfSeatsRequired = 2
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= iNumberOfSeatsRequired
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
			IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) != RHINO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF thisEvent = eV_DRUNK_DRIVER
				IF DOES_ENTITY_EXIST(pedDrunk[0])
					IF NOT IS_PED_INJURED(pedDrunk[0])
						IF IS_PED_IN_VEHICLE(pedDrunk[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF thisEvent = eV_WITH_PASSENGER
				IF DOES_ENTITY_EXIST(pedDrunk[0])
				AND DOES_ENTITY_EXIST(pedDrunk[1])
					IF NOT IS_PED_INJURED(pedDrunk[0])
					AND NOT IS_PED_INJURED(pedDrunk[1])
						IF IS_PED_IN_VEHICLE(pedDrunk[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND IS_PED_IN_VEHICLE(pedDrunk[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_CONVERSATION(BOOL bCanPlay)
	IF bCanPlay
		IF bConvOnHold
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(DrunkDriverDialogueStruct, "REDR2AU", strCurrentRoot, ConvResumeLabel, CONV_PRIORITY_HIGH)
				PRINTLN("@@@@@@@@@@ CREATE_CONVERSATION_FROM_SPECIFIC_LINE: ", strCurrentRoot, " ", ConvResumeLabel, " @@@@@@@@@@")
				bConvOnHold = FALSE
			ENDIF
		ENDIF
	ELIF NOT bConvOnHold
	AND IS_SCRIPTED_CONVERSATION_ONGOING()
	AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		strCurrentRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		ConvResumeLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		PRINTLN("@@@@@@@@@@ SAVED ROOT: ", strCurrentRoot, " @@@ SAVED LABEL: ", ConvResumeLabel, " @@@@@@@@@@")
		KILL_FACE_TO_FACE_CONVERSATION()
		bConvOnHold = TRUE
	ENDIF
ENDPROC

FUNC BOOL IS_DRUNKS_VEHICLE_FUCKED()
	IF thisEvent = eV_WITH_PASSENGER
		IF NOT IS_ENTITY_DEAD(vehDrunksCar)
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehDrunksCar, << 200, 200, 200 >>)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_CONVERSATION_PLAYING("REDR2_DC")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_AKA")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_OFFR")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_CULT")
				MANAGE_CONVERSATION(TRUE)
			ENDIF
			
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar)
			IF NOT IS_THIS_CONVERSATION_PLAYING("REDR2_DC")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_AKA")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_AKB")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_TRY")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_JIC")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_CH")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_WH2")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_BCK")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_JIA")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_JIB")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_JIC")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_WHA")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_WHB")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_WHC")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PSM")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PSF")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PST")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_TK")
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehDrunksCar)
				AND GET_ENTITY_HEALTH(vehDrunksCar) < iDrunksCarHealth
					iDrunksCarHealth = GET_ENTITY_HEALTH(vehDrunksCar)
//					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehDrunksCar)
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINTSTRING("\n Car has just been damaged. Guy should complain. \n")
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DC", CONV_PRIORITY_AMBIENT_HIGH)
//					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE(vehDrunksCar)
			OR IS_VEHICLE_STUCK_ON_ROOF(vehDrunksCar)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_LC", CONV_PRIORITY_AMBIENT_HIGH)
						IF NOT IS_PED_INJURED(pedDrunk[0])
							TASK_WANDER_STANDARD(pedDrunk[0])
							SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(pedDrunk[1])
							TASK_GO_TO_ENTITY(pedDrunk[1], pedDrunk[0], DEFAULT_TIME_NEVER_WARP, 0, PEDMOVEBLENDRATIO_WALK)
							SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehDrunksCar, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehDrunksCar, PLAYER_PED_ID(), FALSE)
//			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar, TRUE)
				IF NOT IS_PED_INJURED(pedDrunk[0])
					CLEAR_PED_TASKS(pedDrunk[0])
					TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
				ENDIF
				IF NOT IS_PED_INJURED(pedDrunk[1])
					CLEAR_PED_TASKS(pedDrunk[1])
					TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
				ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_INJURED()
	IF thisEvent = eV_DRUNK_DRIVER
		IF IS_PED_INJURED(pedDrunk[0])
			RETURN TRUE
		ENDIF
	ENDIF
	IF thisEvent = eV_WITH_PASSENGER
		IF IS_PED_INJURED(pedDrunk[0])
		AND IS_PED_INJURED(pedDrunk[1])
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_NO_LONGER_NEEDED()
	IF thisEvent = eV_DRUNK_DRIVER
		IF NOT DOES_ENTITY_EXIST(pedDrunk[0])
			RETURN TRUE
		ENDIF
	ENDIF
	IF thisEvent = eV_WITH_PASSENGER
		IF NOT DOES_ENTITY_EXIST(pedDrunk[0])
		AND NOT DOES_ENTITY_EXIST(pedDrunk[1])
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //

// SHARED PROCS

PROC continualTurnToFacePlayer(PED_INDEX targetPed,  INT& stage)
	SWITCH stage
		CASE 0 // TURN TASK
			IF NOT IS_PED_INJURED(targetPed)
			//	CLEAR_PED_TASKS(targetPed)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_CLEAR_LOOK_AT(NULL)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(targetPed, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				stage ++
			ENDIF
		BREAK
		CASE 1 // TURNING
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), 45)
					// ACHIEVED TASK
					stage ++
				ENDIF
			ENDIF
		BREAK
		CASE 2 // WAITING TO TURN
			IF NOT IS_PED_INJURED(targetPed)
				IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), 45)
					stage = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC groupSeparationCheck()

	IF thisEvent = eV_DRUNK_DRIVER
		IF NOT IS_PED_INJURED(pedDrunk[0])
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 150, 150, 150 >>)
				MISSION_FAILED()
			ENDIF
			IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
			OR IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF NOT IS_PED_IN_GROUP(pedDrunk[0])
					SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
					SET_PED_NEVER_LEAVES_GROUP(pedDrunk[0], TRUE)
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_FRONT_RIGHT)
				ENDIF
			ELSE
				IF IS_PED_IN_GROUP(pedDrunk[0])
					REMOVE_PED_FROM_GROUP(pedDrunk[0])
				ENDIF
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(pedDrunk[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF NOT IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strDrunkIdleVariation)
					AND NOT IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strVomitVariation)
					AND NOT IS_PED_GETTING_INTO_A_VEHICLE(pedDrunk[0])
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@ TASK_PLAY_ANIM @@@@@@@@@@@@@@@@@\n")
						TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_1", strDrunkIdleVariation, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_SECONDARY|AF_UPPERBODY)
					ENDIF
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						bConversationPaused = FALSE
					ENDIF
				ELSE
					IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strDrunkIdleVariation)
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@ STOP_ANIM_TASK 1 @@@@@@@@@@@@@@@@@\n")
//						STOP_ENTITY_ANIM(pedDrunk[0], strDrunkIdleVariation, "random@drunk_driver_1", NORMAL_BLEND_OUT)
//						STOP_ANIM_TASK(pedDrunk[0], strDrunkIdleVariation, "random@drunk_driver_1", NORMAL_BLEND_OUT)
						STOP_ANIM_PLAYBACK(pedDrunk[0])
//						CLEAR_PED_TASKS_IMMEDIATELY(pedDrunk[0])
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
					IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strDrunkIdleVariation)
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@ STOP_ANIM_TASK 2 @@@@@@@@@@@@@@@@@\n")
//						STOP_ANIM_TASK(pedDrunk[0], strDrunkIdleVariation, "random@drunk_driver_1", NORMAL_BLEND_OUT)
						STOP_ANIM_PLAYBACK(pedDrunk[0])
//						CLEAR_PED_TASKS_IMMEDIATELY(pedDrunk[0])
					ENDIF
				ENDIF
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT bConversationPaused
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					bConversationPaused = TRUE
				ENDIF
			ENDIF
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 8 >>)
			OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
				IF DOES_BLIP_EXIST(blipDrunkPed[0])
					REMOVE_BLIP(blipDrunkPed[0])
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipDropOff)
					blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPoint, TRUE)
				ENDIF
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//					IF NOT DOES_BLIP_EXIST(blipCult)
//						blipCult = CREATE_BLIP_FOR_COORD(vCult)
//						SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//						PRINT_CULT_HELP()
//					ENDIF
//				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipDrunkPed[0])
					blipDrunkPed[0] = CREATE_BLIP_FOR_PED(pedDrunk[0])
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOff)
					REMOVE_BLIP(blipDropOff)
				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF thisEvent = eV_WITH_PASSENGER
		IF NOT IS_ENTITY_DEAD(vehDrunksCar)
		AND NOT IS_PED_INJURED(pedDrunk[0])
		AND NOT IS_PED_INJURED(pedDrunk[1])
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 150, 150, 150 >>)
			OR NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[1], << 150, 150, 150 >>)
				IF DOES_ENTITY_EXIST(pedDrunk[0])
					IF NOT IS_PED_INJURED(pedDrunk[0])
						IF IS_PED_IN_GROUP(pedDrunk[0])
							REMOVE_PED_FROM_GROUP(pedDrunk[0])
						ENDIF
						CLEAR_PED_TASKS(pedDrunk[0])
						TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
						Make_Ped_Drunk(pedDrunk[0], 120000)
						SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(pedDrunk[1])
					IF NOT IS_PED_INJURED(pedDrunk[1])
						IF IS_PED_IN_GROUP(pedDrunk[1])
							REMOVE_PED_FROM_GROUP(pedDrunk[1])
						ENDIF
						CLEAR_PED_TASKS(pedDrunk[1])
						TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
						Make_Ped_Drunk(pedDrunk[1], 120000)
						SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
					ENDIF
				ENDIF
				MISSION_FAILED()
			ENDIF
//			// ADDING PEDS TO GROUP WHEN CLOSE ENOUGH
//			IF NOT IS_PED_IN_GROUP(pedDrunk[0])
//			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 8 >>)
//				SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
//				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_BACK_RIGHT)
//				IF DOES_BLIP_EXIST(blipDrunkPed[0])
//				AND iInterventionStage > 4
//					REMOVE_BLIP(blipDrunkPed[0])
//				ENDIF
//			ENDIF
//			IF NOT IS_PED_IN_GROUP(pedDrunk[1])
//			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[1], << 8, 8, 8 >>)
//				SET_PED_AS_GROUP_MEMBER(pedDrunk[1], PLAYER_GROUP_ID())
//				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[1], VS_BACK_LEFT)
//				IF DOES_BLIP_EXIST(blipDrunkPed[1])
//				AND iInterventionStage > 4
//					REMOVE_BLIP(blipDrunkPed[1])
//				ENDIF
//			ENDIF
//			// ADDING DESTINATION BLIPS WHEN BOTH PEDS ARE BACK IN THE GROUP
//			IF IS_PED_IN_GROUP(pedDrunk[0])
//			AND IS_PED_IN_GROUP(pedDrunk[1])
//			AND iInterventionStage > 4
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar)
				IF IS_PED_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
				AND IS_PED_IN_VEHICLE(pedDrunk[1], vehDrunksCar)
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						bConversationPaused = FALSE
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipDropOff)
						blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPoint, TRUE)
					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//						IF NOT DOES_BLIP_EXIST(blipCult)
//							blipCult = CREATE_BLIP_FOR_COORD(vCult)
//							SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//							PRINT_CULT_HELP()
//						ENDIF
//					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(blipVehicle)
					REMOVE_BLIP(blipVehicle)
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PSM")
//				AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PSF")
//				AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_PST")
				AND NOT bConversationPaused
				AND iInterventionStage > 3
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					bConversationPaused = TRUE
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipVehicle)
					blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDrunksCar)
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOff)
					REMOVE_BLIP(blipDropOff)
				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			ENDIF
			// ADDING BLIPS FOR PEDS IF NO LONGER IN GROUP
//			IF NOT IS_PED_IN_GROUP(pedDrunk[0])
//			AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 8 >>)
//				IF NOT DOES_BLIP_EXIST(blipDrunkPed[0])
//					blipDrunkPed[0] = CREATE_BLIP_FOR_PED(pedDrunk[0])
//				ENDIF
//				IF DOES_BLIP_EXIST(blipDropOff)
//					REMOVE_BLIP(blipDropOff)
//				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
//			ENDIF
//			IF NOT IS_PED_IN_GROUP(pedDrunk[1])
//			AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[1], << 8, 8, 8 >>)
//				IF NOT DOES_BLIP_EXIST(blipDrunkPed[1])
//					blipDrunkPed[1] = CREATE_BLIP_FOR_PED(pedDrunk[1])
//				ENDIF
//				IF DOES_BLIP_EXIST(blipDropOff)
//					REMOVE_BLIP(blipDropOff)
//				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC stationaryCheck()
	IF IS_PED_STOPPED(PLAYER_PED_ID())
		IF NOT bStartStationaryTimer
			iStationaryStartTimer = GET_GAME_TIMER()
			bStartStationaryTimer = TRUE
		ELSE
			iStationaryCurrentTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		iStationaryCurrentTimer = 0
		bStartStationaryTimer = FALSE
		bStationaryWarned = FALSE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 50000)
	AND NOT bStationaryWarned
		IF thisEvent = eV_DRUNK_DRIVER
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WLK", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		bStationaryWarned = TRUE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 60000)
	AND bStationaryWarned
		IF thisEvent = eV_WITH_PASSENGER
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_FU", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		IF NOT IS_PED_INJURED(pedDrunk[0])
		AND NOT IS_PED_INJURED(pedDrunk[1])
//			IF IS_ENTITY_AT_COORD(pedDrunk, vHitcherDestinationPos, << 200, 200, 200 >>)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//			ELSE
				TASK_WANDER_STANDARD(pedDrunk[0])
				Make_Ped_Drunk(pedDrunk[0], 120000)
//			ENDIF
			SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
			IF IS_PED_IN_GROUP(pedDrunk[0])
				REMOVE_PED_FROM_GROUP(pedDrunk[0])
			ENDIF
//			IF IS_ENTITY_AT_COORD(pedDrunk, vHitcherDestinationPos, << 200, 200, 200 >>)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//			ELSE
//				TASK_WANDER_STANDARD(pedDrunk[1])
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedDrunk[1], pedDrunk[0], << 0, 1, 0 >>, PEDMOVEBLENDRATIO_WALK)
				Make_Ped_Drunk(pedDrunk[1], 120000)
//			ENDIF
			SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
			IF IS_PED_IN_GROUP(pedDrunk[1])
				REMOVE_PED_FROM_GROUP(pedDrunk[1])
			ENDIF
		ENDIF
		MISSION_FAILED()
	ENDIF
ENDPROC

PROC dontFollowPlayerInWrongVehicle()
	
	IF NOT IS_PED_INJURED(pedDrunk[0])
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE DOESN'T FIT @@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
				IF IS_PED_IN_GROUP(pedDrunk[0])
					REMOVE_PED_FROM_GROUP(pedDrunk[0])
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
					TASK_GO_TO_ENTITY(pedDrunk[0], PLAYER_PED_ID(), -1, 6)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
					TASK_LEAVE_ANY_VEHICLE(pedDrunk[0])
				ENDIF
				IF NOT bUnsuitableVehRemark
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REHH1AU", "REDR1_NO", CONV_PRIORITY_AMBIENT_HIGH)
						bUnsuitableVehRemark = TRUE
					ENDIF
				ENDIF
			ELSE
				bUnsuitableVehRemark = FALSE
				IF GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
					CLEAR_PED_TASKS(pedDrunk[0])
				ENDIF
			ENDIF
		ELIF NOT IS_PED_IN_GROUP(pedDrunk[0])
			SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
			SET_PED_NEVER_LEAVES_GROUP(pedDrunk[0], TRUE)
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_FRONT_RIGHT)
		ENDIF
	ENDIF
	
ENDPROC

//PROC playerDrivingIntoPedsResponse()
//	FOR i = 0 to (iNumberOfPedsInEvent - 1)
//		IF NOT IS_ENTITY_DEAD(pedDrunk[i])
//			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[i], << 12, 12, 8 >>, FALSE, TM_IN_VEHICLE)
//				IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 3
//					IF NOT IS_PED_INJURED(pedDrunk[i])
//						TASK_SMART_FLEE_COORD(pedDrunk[i], GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), 150, -1)
//						SET_PED_KEEP_TASK(pedDrunk[i], TRUE)
//						SET_PED_AS_NO_LONGER_NEEDED(pedDrunk[i])
//					ENDIF
//				ELSE
//					//Temporary debug speech
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDFOR
//ENDPROC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //

// eV_DRUNK_DRIVER - Var. 1 PROCS

FUNC VECTOR DIV_VEC(VECTOR v, INT denom)
	VECTOR retVec
	retVec.x = v.x/denom
	retVec.y = v.y/denom
	retVec.z = v.z/denom
	RETURN retVec
ENDFUNC

PROC GET_LAST_FRAME_VELOCITY()
	IF thisEvent = eV_DRUNK_DRIVER
	AND thisStage = eS_PLAYER_INTERVENTION
	AND iInterventionStage = 5
	AND bSickWarning
		time_of_last_frame = GET_GAME_TIMER()
		IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				velocity_last_frame = GET_ENTITY_SPEED_VECTOR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FORWARD_SPEED(VECTOR last_velocity, INT time_diff)
	VECTOR speed_vector_now
	IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			speed_vector_now = GET_ENTITY_SPEED_VECTOR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	
	VECTOR speed_diff = speed_vector_now - last_velocity
	
//	float height = 0.1
//	TEXT_LABEL_63 temp
	speed_diff = DIV_VEC(speed_diff, time_diff)
	
//	temp = GET_STRING_FROM_FLOAT(speed_diff.x)
	//DRAW_DEBUG_TEXT_2D(temp, <<0.2,height,0.0>>)
//	height += 0.015
//	temp = GET_STRING_FROM_FLOAT(speed_diff.y)
	//DRAW_DEBUG_TEXT_2D(temp, <<0.2,height,0.0>>)
	//height += 0.015
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
				IF iWheelTimer = -1
					iWheelTimer = GET_GAME_TIMER() + 400
				ELIF iWheelTimer < GET_GAME_TIMER()
					iDrivingCounter = (iDrivingCounter + 3)
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ELSE
				iWheelTimer = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF speed_diff.y > 0.025//0.020//0.016
		PRINTNL()
		PRINTSTRING("big accel")
		iDrivingCounter ++
		//big accel
		IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		AND iSickStage < 4
			MANAGE_CONVERSATION(FALSE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
	IF speed_diff.y < -0.025//-0.020//-0.016
		PRINTNL()
		PRINTSTRING("decel")
		iDrivingCounter ++
		//big decel
		IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		AND iSickStage < 4
			MANAGE_CONVERSATION(FALSE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
	
	IF speed_diff.x > 0.025//0.018//0.014
		PRINTNL()
		PRINTSTRING("big right turn/slide")
		iDrivingCounter ++
		//big right turn/slide
//		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		AND iSickStage < 4
			MANAGE_CONVERSATION(FALSE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
	IF speed_diff.x < -0.025//-0.018//-0.014
		PRINTNL()
		PRINTSTRING("big left turn/slide")
		iDrivingCounter ++
		//big left turn/slide
//		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		AND iSickStage < 4
			MANAGE_CONVERSATION(FALSE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
ENDPROC

PROC checkPlayersDriving()
	IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
//	AND NOT bVomiting
	AND iSickStage < 2
		IF (GET_TIME_SINCE_PLAYER_HIT_VEHICLE(PLAYER_ID()) > 0
		AND GET_TIME_SINCE_PLAYER_HIT_VEHICLE(PLAYER_ID()) < 100)
		OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
	//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//			IF NOT bVomiting
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
					iDrivingCounter = (iDrivingCounter + 6)
	//			ENDIF
			ENDIF
		ENDIF
//		IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) > 0
//		AND GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 100)
//		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
////			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
//				iDrivingCounter = (iDrivingCounter + 3)
////			ENDIF
//		ENDIF
//		IF (GET_TIME_SINCE_PLAYER_HIT_BUILDING(PLAYER_ID()) > 0
//		AND GET_TIME_SINCE_PLAYER_HIT_BUILDING(PLAYER_ID()) < 100)
//		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
////			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
//				iDrivingCounter = (iDrivingCounter + 3)
////			ENDIF
//		ENDIF
//		IF (GET_TIME_SINCE_PLAYER_HIT_OBJECT(PLAYER_ID()) > 0
//		AND GET_TIME_SINCE_PLAYER_HIT_OBJECT(PLAYER_ID()) < 100)
//		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
////			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SWV", CONV_PRIORITY_AMBIENT_HIGH)
//				iDrivingCounter = (iDrivingCounter + 3)
////			ENDIF
//		ENDIF
		
		CHECK_FORWARD_SPEED(velocity_last_frame, (GET_GAME_TIMER() - time_of_last_frame))
		
// check:  x:\gta5\script\dev\shared\include\native\stats_enums.sch
	ENDIF
//	PRINTNL()
//	PRINTINT(iDrivingCounter)
//	PRINTNL()
//	PRINTNL()
//	PRINTBOOL(bVomiting)
//	PRINTNL()
//	PRINTLN("@@@@@@@ iDrivingCounter: ", iDrivingCounter, " @@@@@@@@@")
	IF iDrivingCounter > 17//21
		bGuyBeSick = TRUE
	ENDIF
ENDPROC

PROC makeGuySpew()
	FLOAT fSpewStartTime
	CONST_FLOAT iSTART_VOMITING_TIME0	0.243
	CONST_FLOAT iSTOP_VOMITING_TIME0	0.280
	CONST_FLOAT iSTART_VOMITING_TIME1	0.295 //0.246 //0.26 
	CONST_FLOAT iSTOP_VOMITING_TIME1	0.370 //0.379
	CONST_FLOAT iSTART_VOMITING_TIME2	0.49
	CONST_FLOAT iSTOP_VOMITING_TIME2	0.55
	
//	IF iSickStage >= 1
//	AND iSickStage <= 2
//		IF NOT bVomitCamHelp
//			PRINT_HELP("VEH_PUKE_VIEW")
//			bVomitCamHelp = TRUE
//			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
//		ENDIF
//		IF SHOULD_CONTROL_CHASE_HINT_CAM(structHintCam)
//	//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
//			IF NOT DOES_CAM_EXIST(camVomit)
//			AND DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					camVomit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 0, 0, 0 >>, << 0, 0, 0 >>, 40)
//					ATTACH_CAM_TO_ENTITY(camVomit, GET_PLAYERS_LAST_VEHICLE(), <<0.9540, -4.5891, 0.8773>>)
//					POINT_CAM_AT_ENTITY(camVomit, GET_PLAYERS_LAST_VEHICLE(), <<0.6163, -1.6157, 0.6655>>)
//					SET_CAM_ACTIVE(camVomit, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				ENDIF
//			ENDIF
//		ELIF DOES_CAM_EXIST(camVomit)
//			DESTROY_CAM(camVomit)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		ENDIF
//	ENDIF

	IF NOT IS_PED_INJURED(pedDrunk[0])
		SWITCH iSickStage
			CASE 0
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SIK", CONV_PRIORITY_AMBIENT_HIGH)
					IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						REMOVE_VEHICLE_WINDOW(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SC_WINDOW_FRONT_RIGHT)
					ENDIF
					SETTIMERB(0)
					iSickStage ++
				ENDIF
			BREAK
			CASE 1
				IF TIMERB() > 3000
				OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(pedDrunk[0])
						CLEAR_PED_TASKS(pedDrunk[0])
						TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_1", strVomitVariation, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						iSickStage ++
					ENDIF
				ENDIF
			BREAK
			CASE 2
			//	SET_VEHICLE_DOOR_OPEN(GET_VEHICLE_PED_IS_IN(pedDrunk[0]), SC_DOOR_FRONT_RIGHT)//, DT_DOOR_INTACT, 0.8)
				IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strVomitVariation)
					fSpewStartTime = GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0],"random@drunk_driver_1", strVomitVariation)
					IF (fSpewStartTime > iSTART_VOMITING_TIME0 AND fSpewStartTime < iSTOP_VOMITING_TIME0)
						IF NOT bVomiting
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", pedDrunk[0], << 0, 0, 0 >>, << 0, 0, 0 >>, BONETAG_HEAD)
							bVomiting = TRUE
						ENDIF
					ELIF (fSpewStartTime > iSTART_VOMITING_TIME1 AND fSpewStartTime < iSTOP_VOMITING_TIME1)
						IF NOT bVomiting1
//						IF (ptSpray = NULL)
							/*ptSpray = */START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", pedDrunk[0], << 0, 0, 0 >>, << 0, 0, 0 >>, BONETAG_HEAD)
//							PLAY_SOUND_FRONTEND(SoundId, "Vomit_Loop", /*<< -456, -985, 25 >>,*/ "RE_DRUNK_DRIVER_01_SOUNDS")
							bVomiting1 = TRUE
//						ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SIKM", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SIKF", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_SIKT", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ENDIF
					ELIF (fSpewStartTime > iSTART_VOMITING_TIME2 AND fSpewStartTime < iSTOP_VOMITING_TIME2)
						IF NOT bVomiting2
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_puke_in_car", pedDrunk[0], << 0, 0, 0 >>, << 0, 0, 0 >>, BONETAG_HEAD)
							bVomiting2 = TRUE
						ENDIF
					ELSE
//						IF (ptSpray <> NULL)
//							STOP_PARTICLE_FX_LOOPED(ptSpray)
//							ptSpray = NULL
//						ENDIF
//						STOP_SOUND(SoundId)
					ENDIF
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0],"random@drunk_driver_1", strVomitVariation) > 0.8
						iSickStage ++
					ENDIF
				ELSE
//					IF (ptSpray <> NULL)
//						STOP_PARTICLE_FX_LOOPED(ptSpray)
//						ptSpray = NULL
//					ENDIF
//					STOP_SOUND(SoundId)
				ENDIF
			BREAK
			CASE 3
//				IF DOES_CAM_EXIST(camVomit)
//					DESTROY_CAM(camVomit)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				ENDIF
				// just to be safe - make sure spew effects aren't still running
				IF NOT IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_1", strVomitVariation)     
//					IF (ptSpray <> NULL)
//						STOP_PARTICLE_FX_LOOPED(ptSpray)
//						ptSpray = NULL
//			    	ENDIF
//					STOP_SOUND(SoundId)
				ENDIF
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_APO", CONV_PRIORITY_AMBIENT_HIGH)
						//SET_VEHICLE_DOOR_SHUT(GET_VEHICLE_PED_IS_IN(pedDrunk[0]), SC_DOOR_FRONT_RIGHT)
						TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_1", strDrunkIdleVariation, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_SECONDARY|AF_UPPERBODY)
						bVomiting = FALSE
						iSickStage ++
					ENDIF
//				ENDIF
			BREAK
			CASE 4
				STOP_SOUND(SoundId)
				bSickingDone = TRUE
			BREAK
		ENDSWITCH
		
	ENDIF
ENDPROC

PROC inVehDialogue()
	
	IF NOT bSickingDone
		IF NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SWV")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_COM")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_BANT1")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_SIK")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_DC")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_OFFR")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_OFFR")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR1_CULT")
		AND NOT IS_THIS_CONVERSATION_PLAYING("REDR2_CULT")
			MANAGE_CONVERSATION(TRUE)
		ENDIF
	
		// talk
		IF bSickWarning
		AND NOT bGenChatDialogue
		AND (GET_GAME_TIMER() - iJourneyTimer) > 9000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_BANT1", CONV_PRIORITY_AMBIENT_HIGH)
					bGenChatDialogue = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// random lines from player between ped dialogue
	IF bGenChatDialogue
	AND NOT bGenChatResponse
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_RESPM", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_RESPF", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_RESPT", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			iRandomChatLineTimer = (GET_GAME_TIMER() + 6000)//GET_RANDOM_INT_IN_RANGE(12000, 25000))
			bGenChatResponse = TRUE
		ENDIF
	ENDIF
	
	IF NOT bSickingDone
		// random lines (no longer random so have to comment out randomness)
		IF bGenChatDialogue
		AND NOT bPreSickDialogue
		AND iRandomChatLineTimer < GET_GAME_TIMER()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_COM", CONV_PRIORITY_AMBIENT_HIGH)
	//				iRandomChatLineTimer = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 25000))
					bGenChatResponse = FALSE
					bPreSickDialogue = TRUE
				ENDIF
			ENDIF
		ENDIF
		// talk after spewing
	ELIF bSickingDone
	AND NOT bPostSickDialogue
	AND iRandomChatLineTimer < GET_GAME_TIMER()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_BANT2", CONV_PRIORITY_AMBIENT_HIGH)
//				iRandomChatLineTimer = (GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 25000))
				bGenChatResponse = FALSE
				bPostSickDialogue = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC endDropOffScene1DrunksVehicle()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
				IF IS_VEHICLE_STOPPED(vehDrunksCar)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//					IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
//						SET_ENTITY_COORDS(vehDrunksCar, << 162.6622, -113.3979, 61.2995 >>)
//						SET_ENTITY_HEADING(vehDrunksCar, 67.6666)
//						SET_VEHICLE_ON_GROUND_PROPERLY(vehDrunksCar)
						IF NOT IS_PED_INJURED(pedDrunk[0])
							CLEAR_PED_TASKS(pedDrunk[0])
//							IF (ptSpray <> NULL)
//								STOP_PARTICLE_FX_LOOPED(ptSpray)
//								ptSpray = NULL
//							ENDIF
//							STOP_SOUND(SoundId)
							REMOVE_PED_FROM_GROUP(pedDrunk[0])
//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
//								SET_PED_INTO_VEHICLE(pedDrunk[0], vehDrunksCar, VS_FRONT_RIGHT)
//							ENDIF
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDrunk[0])
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
//					ENDIF
					IF bSickingDone
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 1
//			IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
//				camCutsceneOrigin = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//				camCutsceneDest = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, <<-1.7797, 1.0093, 0.9447>>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, <<0.9229, -0.2740, 0.7245>>)
//				SET_CAM_FOV(camCutsceneOrigin, 30.5154)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, <<-1.7797, 1.0093, 0.9447>>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, <<1.0537, 0.0522, 0.7094>>)
//				SET_CAM_FOV(camCutsceneDest, 30.5154)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				SHAKE_CAM(camCutsceneDest, "HAND_SHAKE", 0.3)
//				SHAKE_CAM(camCutsceneOrigin, "HAND_SHAKE", 0.3)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
				iDropOffCutStage ++
//				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//					OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(vehDrunksCar)
//				ENDIF
//			ENDIF
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1800)
//						TASK_PAUSE(NULL, 1500)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_1", "drunk_fall_over", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_NOT_INTERRUPTABLE|AF_ENDS_IN_DEAD_POSE)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_BUM_STANDING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
					
					Make_Ped_Drunk(pedDrunk[0], 120000)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 3000)
					iDropOffCutStage ++
				ENDIF
//			ENDIF
		BREAK
		CASE 3
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 3000)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBM", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBF", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBT", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		CASE 4
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				camCutsceneDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, << 1.9793, 1.0872, 0.8830 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, << -0.7367, -0.1866, 0.9184 >>)
//				SET_CAM_FOV(camCutsceneDest, 30.5154)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << 1.9793, 1.0872, 0.8830 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << -0.7995, -0.0431, 0.9164 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 30.5154)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 1500)
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 5
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 6
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedDrunk[0])
//					TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[0], vDropWalkTo, PEDMOVEBLENDRATIO_WALK)
//					Make_Ped_Drunk(pedDrunk[0], 120000)
//				ENDIF
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				camCutsceneDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << -0.9464, 4.0200, 1.0435 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << 0.0274, 1.1896, 0.8425 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 30.5154)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, << -0.9464, 4.0200, 1.0435 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, << -0.1900, 1.1239, 0.8431 >>)
//				SET_CAM_FOV(camCutsceneDest, 30.5154)
//				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2500)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 7
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				DO_SCREEN_FADE_OUT(1500)
//				WHILE NOT IS_SCREEN_FADED_OUT()
//					WAIT(0)
//				ENDWHILE
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//				SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
//				SET_CAM_ACTIVE(camCutsceneDest, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				DELETE_PED(pedDrunk[0])
//				IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
//					SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
//				ENDIF
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				DO_SCREEN_FADE_IN(1500)
//				WHILE NOT IS_SCREEN_FADED_IN()
//					WAIT(0)
//				ENDWHILE
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80, FALSE, TRUE)
				bDropOffDone = TRUE
//			ENDIF
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutStage = 7
//		ENDIF
//	ENDIF
ENDPROC

PROC endDropOffScene1OwnVehicle()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			VEHICLE_INDEX vehTempForScene
			vehTempForScene = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
				IF IS_VEHICLE_STOPPED(vehTempForScene)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					IF NOT IS_PED_INJURED(pedDrunk[0])
						CLEAR_PED_TASKS(pedDrunk[0])
//						IF (ptSpray <> NULL)
//							STOP_PARTICLE_FX_LOOPED(ptSpray)
//							ptSpray = NULL
//						ENDIF
//						STOP_SOUND(SoundId)
						REMOVE_PED_FROM_GROUP(pedDrunk[0])
//						IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
//							SET_ENTITY_COORDS(vehTempForScene, << 162.6622, -113.3979, 61.2995 >>)
//							SET_ENTITY_HEADING(vehTempForScene, 67.6666)
//							SET_VEHICLE_ON_GROUND_PROPERLY(vehTempForScene)
//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehTempForScene)
//								SET_PED_INTO_VEHICLE(pedDrunk[0], vehTempForScene, VS_FRONT_RIGHT)
//							ENDIF
//						ENDIF
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDrunk[0])
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
					ENDIF
					IF bSickingDone
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 1
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//			SET_CAM_PARAMS(camCutscene, << 157.9689, -112.4709, 62.4786 >>, << -2.8753, 0.3339, -96.8241 >>, 30.440655)
//			SET_CAM_PARAMS(camCutscene, << 157.9689, -112.4709, 62.4786 >>, << -2.8753, 0.3339, -93.0897 >>, 30.440655, 12000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 7000)
			iDropOffCutStage ++
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_PED_INJURED(pedDrunk[0])
//					TASK_LEAVE_ANY_VEHICLE(pedDrunk[0])
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1500)
					iDropOffCutStage ++
//				ENDIF
//			ENDIF
		BREAK
		CASE 3
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1800)
//						TASK_PAUSE(NULL, 1500)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_1", "drunk_fall_over", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_NOT_INTERRUPTABLE|AF_ENDS_IN_DEAD_POSE)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_BUM_STANDING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
				ENDIF
				Make_Ped_Drunk(pedDrunk[0], 120000)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 3000)
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 4
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 3000)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBM", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBF", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBT", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		CASE 5
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 6
//			DO_SCREEN_FADE_OUT(1500)
//			WHILE NOT IS_SCREEN_FADED_OUT()
//				WAIT(0)
//			ENDWHILE
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedDrunk[0])
//			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			DO_SCREEN_FADE_IN(1500)
//			WHILE NOT IS_SCREEN_FADED_IN()
//				WAIT(0)
//			ENDWHILE
			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80, FALSE, TRUE)
			bDropOffDone = TRUE
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutStage = 6
//		ENDIF
//	ENDIF
ENDPROC

PROC endDropOffScene1OnFoot()
	
	// player pos << 162.4110, -113.2895, 61.2995 >> 346.9045
	// drunk guy <<163.153946,-112.155388,62.304127>>, 147.249863
	// over drunk guy's shoulder
	//SET_CAM_PARAMS(_cams_index,<<163.294617,-109.263626,62.915562>>,<<-4.517965,0.000000,173.933578>>,20.717838)
	//SET_CAM_PARAMS(_cams_index,<<162.218842,-115.607864,62.799076>>,<<-2.965224,0.000000,-9.358498>>,20.717838)
	
	//SET_CAM_PARAMS(_cams_index,<<160.433075,-108.895042,62.482849>>,<<0.099160,0.143172,-150.893341>>,33.661560)
	//SET_CAM_PARAMS(_cams_index,<<160.433075,-108.895042,62.482849>>,<<0.099160,0.143172,-155.856155>>,33.661560)
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 162.4110, -113.2895, 61.2995 >>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 346.9045)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedDrunk[0])
//					CLEAR_PED_TASKS_IMMEDIATELY(pedDrunk[0])
//					SET_ENTITY_COORDS_NO_OFFSET(pedDrunk[0], << 163.1539, -112.1553, 62.3041 >>)
//					SET_ENTITY_HEADING(pedDrunk[0], 147.2498)
					REMOVE_PED_FROM_GROUP(pedDrunk[0])
					TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDrunk[0])
				ENDIF
				IF bSickingDone
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK", CONV_PRIORITY_AMBIENT_HIGH)
				ELSE
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TK1", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 1
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//			SET_CAM_PARAMS(camCutscene, << 162.2188, -115.6078, 62.7990 >>, << -2.9652, 0.0000, -9.3584 >>, 20.717838)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
			iCutsceneWaitTimer = (GET_GAME_TIMER() + 3000)
			iDropOffCutStage ++
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				SET_CAM_PARAMS(camCutscene, << 163.2946, -109.2636, 62.9155 >>, << -4.5179, 0.0000, 173.9335 >>, 20.717838)
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBM", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBF", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_GBT", CONV_PRIORITY_AMBIENT_HIGH)
						iDropOffCutStage ++
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		CASE 3
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				SET_CAM_PARAMS(camCutscene, << 160.4330, -108.8950, 62.4828 >>, << 0.0991, 0.1431, -150.8933 >>, 33.6615)
//				SET_CAM_PARAMS(camCutscene, << 160.4330, -108.8950, 62.4828 >>, << 0.0991, 0.1431, -155.8561 >>, 33.6615, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				IF NOT IS_PED_INJURED(pedDrunk[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1800)
//						TASK_PAUSE(NULL, 1500)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_1", "drunk_fall_over", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_NOT_INTERRUPTABLE|AF_ENDS_IN_DEAD_POSE)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_BUM_STANDING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
					iDropOffCutStage ++
				ENDIF
//			ENDIF
		BREAK
		CASE 4
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 5
//			DO_SCREEN_FADE_OUT(1500)
//			WHILE NOT IS_SCREEN_FADED_OUT()
//				WAIT(0)
//			ENDWHILE
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedDrunk[0])
//			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			DO_SCREEN_FADE_IN(1500)
//			WHILE NOT IS_SCREEN_FADED_IN()
//				WAIT(0)
//			ENDWHILE
			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80, FALSE, TRUE)
			bDropOffDone = TRUE
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutStage = 5
//		ENDIF
//	ENDIF
ENDPROC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //

// eV_WITH_PASSENGER - Var. 2 PROCS

FUNC BOOL ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
	VEHICLE_INDEX vehTemp
	IF NOT IS_PED_INJURED(pedDrunk[1])
		IF IS_PED_IN_ANY_VEHICLE(pedDrunk[1])
			vehTemp = GET_VEHICLE_PED_IS_IN(pedDrunk[1])
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
				IF NOT IS_PED_INJURED(pedDrunk[0])
					IF IS_PED_IN_VEHICLE(pedDrunk[0], vehTemp)
						IF GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_BACK_RIGHT) = pedDrunk[0]
						AND GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_BACK_LEFT) = pedDrunk[1]
							RETURN TRUE
				 		ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC waitingDialogue()
	IF NOT IS_PED_INJURED(pedDrunk[0])
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 30, 30, 30 >>)
			IF NOT bWaitingChatDialogue1
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DCB", CONV_PRIORITY_AMBIENT_HIGH)
					bWaitingChatDialogue1 = TRUE
				ENDIF
			ENDIF
			
			IF NOT bWaitingChatDialogue2
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DCC", CONV_PRIORITY_AMBIENT_HIGH)
					bWaitingChatDialogue2 = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC bouncingCar()
	IF NOT IS_PED_INJURED(pedDrunk[1])
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(pedDrunk[0]))
			IF IS_ENTITY_PLAYING_ANIM(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f")
				IF (GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") < 0.15
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") > 0.1)
				
				OR (GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") < 0.35
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") > 0.3)
				
				OR (GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") < 0.65
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") > 0.6)
				
				OR (GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") < 0.9
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f") > 0.85)
					APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedDrunk[0]), APPLY_TYPE_FORCE,  << 0, 0, 15 >>, << -0.4, 0, 0 >>, 0, TRUE, TRUE, TRUE)
					PLAY_SOUND_FROM_ENTITY(-1, "SUSPENSION_SCRIPT_FORCE", GET_VEHICLE_PED_IS_IN(pedDrunk[0]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC shaggingInBackOfCar()

	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_enter_m")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_idle_m")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_exit_m")
		BOOL bInteriorCam = FALSE
		CAM_VIEW_MODE_CONTEXT activeViewModeContext
		CAM_VIEW_MODE activeViewMode
		activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
		IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT
		  	activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
		  	IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON
				bInteriorCam = TRUE
			ENDIF
		ENDIF		
	
		IF IS_PLAYER_MOVING_LEFT_STICK()
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR bInteriorCam
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	SWITCH iCarShaggingStage
	
		CASE 0
			//"Oh baby, I'm really fucking horny."
			//"All those margaritas have gone to my head."
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				TASK_LOOK_AT_ENTITY(pedDrunk[0], pedDrunk[1], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)//, SLF_LOOKAT_VERY_HIGH)
				TASK_LOOK_AT_ENTITY(pedDrunk[1], pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)//, SLF_LOOKAT_VERY_HIGH)
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_HC", CONV_PRIORITY_AMBIENT_HIGH)
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "cardrunkflirt_intro_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "cardrunkflirt_loop_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "cardrunkflirt_intro_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "cardrunkflirt_loop_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
					CLEAR_SEQUENCE_TASK(seq)
					iStageShaggingTimer = GET_GAME_TIMER()
					iCarShaggingStage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iStageShaggingTimer = GET_GAME_TIMER()
				iCarShaggingStage ++
			ENDIF
		BREAK
		
		CASE 2
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				IF GET_GAME_TIMER() - iStageShaggingTimer > 5000
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_CH", CONV_PRIORITY_AMBIENT_HIGH)
						//"Shift yourself over here..."
						//"I reckon I can help you out with that."
						iStageShaggingTimer = GET_GAME_TIMER()
						iCarShaggingStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF GET_GAME_TIMER() - iStageShaggingTimer > 15000//5000
				IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
					TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_intro_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_intro_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					iCarShaggingStage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_intro_m")
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_intro_m") > 0.9
						TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_loop_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						iStageShaggingTimer = GET_GAME_TIMER()
						iCarShaggingStage ++
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedDrunk[0])
				AND NOT IS_PED_INJURED(pedDrunk[1])
					CLEAR_PED_TASKS(pedDrunk[0])
					CLEAR_PED_TASKS(pedDrunk[1])
					iCarShaggingStage = 3
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				bouncingCar()
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(pedDrunk[1]))
					IF GET_GAME_TIMER() - iStageShaggingTimer > 15000
//						IF IS_PED_STOPPED(PLAYER_PED_ID())
							BOOL bInInteriorCam
							bInInteriorCam = FALSE
							CAM_VIEW_MODE_CONTEXT activeViewModeContext
							CAM_VIEW_MODE activeViewMode
							activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
							IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT
							  	activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
							  	IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON
									bInInteriorCam = TRUE
								ENDIF
							ENDIF
							
							IF NOT bInInteriorCam
								OPEN_SEQUENCE_TASK(seq)
									TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "driver_enter_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
									TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "driver_idle_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
//						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_PSM", CONV_PRIORITY_AMBIENT_HIGH)
								iStageShaggingTimer = GET_GAME_TIMER()
								iCarShaggingStage ++
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_PSF", CONV_PRIORITY_AMBIENT_HIGH)
								iStageShaggingTimer = GET_GAME_TIMER()
								iCarShaggingStage ++
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_PST", CONV_PRIORITY_AMBIENT_HIGH)
								iStageShaggingTimer = GET_GAME_TIMER() + 14000
								iCarShaggingStage ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			bouncingCar()
			IF GET_GAME_TIMER() - iStageShaggingTimer > 5000
			AND ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(pedDrunk[1]))
					IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_loop_m")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_loop_m") > 0.9
							IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_idle_m")
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "random@drunk_driver_2", "driver_exit_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							ENDIF
							TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_outro_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							iCarShaggingStage ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m")
					IF (GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m") < 0.25
					AND GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m") > 0.2)
			    	    APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedDrunk[0]), APPLY_TYPE_FORCE,  << 0, 0, 20 >>, << -0.4, 0, 0 >>, 0, TRUE, TRUE, TRUE)
						iStageShaggingTimer = GET_GAME_TIMER()
						iCarShaggingStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF ARE_BOTH_DRUNKS_IN_A_CAR_TOGETHER()
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(pedDrunk[1]))
					IF IS_ENTITY_PLAYING_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_outro_m") < 0.525
							TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_2", "cardrunksex_loop_m", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
							TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_2", "cardrunksex_loop_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
							iCarShaggingStage = 6
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF GET_GAME_TIMER() - iStageShaggingTimer > 10000
				IF NOT IS_PED_INJURED(pedDrunk[0])
				AND NOT IS_PED_INJURED(pedDrunk[1])
					CLEAR_PED_TASKS(pedDrunk[0])
					CLEAR_PED_TASKS(pedDrunk[1])
					iStageShaggingTimer = GET_GAME_TIMER()
					iCarShaggingStage ++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC endDropOffScene2DrunksVehicle()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
//			SETTIMERA(0)
//			INT iTimeNow
//			iTimeNow = GET_GAME_TIMER()
//			WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND (GET_GAME_TIMER() - iTimeNow) < 10000
//				PRINTLN("LOADING AREA AROUND FINAL SCENE")
//				WAIT(0)
//			ENDWHILE
			
			IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
				IF IS_VEHICLE_STOPPED(vehDrunksCar)
//					HIDE_HUD_AND_RADAR_THIS_FRAME()
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
						IF NOT IS_PED_INJURED(pedDrunk[0])
							CLEAR_PED_TASKS(pedDrunk[0])
							REMOVE_PED_FROM_GROUP(pedDrunk[0])
//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
//								SET_PED_INTO_VEHICLE(pedDrunk[0], vehDrunksCar, VS_BACK_RIGHT)
//							ENDIF
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 4000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
						IF NOT IS_PED_INJURED(pedDrunk[1])
							CLEAR_PED_TASKS(pedDrunk[1])
							REMOVE_PED_FROM_GROUP(pedDrunk[1])
//							IF NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[1], vehDrunksCar)
//								SET_PED_INTO_VEHICLE(pedDrunk[1], vehDrunksCar, VS_BACK_LEFT)
//							ENDIF
						ENDIF
					ENDIF
					
					strCurrentRoot = ""	//Fix for 1857528
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TK", CONV_PRIORITY_AMBIENT_HIGH)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
				camCutsceneOrigin = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1121.3348, 2643.9084, 37.8620>>, <<1.5062, -0.0000, 20.2740>>, 33.0256)
				camCutsceneDest = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1121.3016, 2643.9983, 37.8919>>, <<1.5062, 0.0000, 18.7019>>, 33.0256)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << -2.3203, 1.1679, 0.7617 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << 0.1347, -0.5384, 0.5145 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 27.1947)
//				SET_CAM_PARAMS(camCutsceneOrigin, << 1125.4435, 2642.5432, 37.8399 >>, << 0.9058, 0.0000, 21.3606 >>, 34.8925)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << 1631.9979, 2087.8848, -73.8079 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << 1630.4835, 2085.2952, -73.7711 >>)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, << -2.3203, 1.1679, 0.7617 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, << -0.0235, -0.7455, 0.5104 >>)
//				SET_CAM_FOV(camCutsceneDest, 26.2327)
				SHAKE_CAM(camCutsceneOrigin, "HAND_SHAKE", 0.3)
				SHAKE_CAM(camCutsceneDest, "HAND_SHAKE", 0.3)
				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				CLEAR_AREA(vDropOffPoint, 8, TRUE)
				SET_ENTITY_COORDS(vehDrunksCar, <<1120.5507, 2647.3071, 36.9963>>) //<<1125.4380, 2647.9832, 36.9963>>
				SET_ENTITY_HEADING(vehDrunksCar, 179.7675) //180.8005
				SET_VEHICLE_ON_GROUND_PROPERLY(vehDrunksCar)
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2500)
				iDropOffCutStage ++
//				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//					OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(vehDrunksCar)
//				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF(iCutsceneWaitTimer - 500) < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[1])
					IF IS_PED_IN_ANY_VEHICLE(pedDrunk[1])
						OPEN_SEQUENCE_TASK(seq)
//							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//							TASK_PLAY_ANIM(NULL, "gestures@niko", "hello", SLOW_BLEND_IN, SLOW_BLEND_OUT,  -1, AF_SECONDARY| AF_UPPERBODY)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
					ENDIF
				ENDIF
			ENDIF
			IF (iCutsceneWaitTimer - 1000) < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
					IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
						OPEN_SEQUENCE_TASK(seq)
//							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo - << 1, 0, 0 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
					ENDIF
				ENDIF
			ENDIF
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				camCutsceneDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << 0.1969, -0.7989, 0.3845 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 27.1947)
				
				SET_CAM_PARAMS(camCutsceneOrigin, << 1105.8485, 2664.6530, 38.0470 >>, << 3.6983, 0.0242, -140.4768 >>, 41.7126)
				SET_CAM_PARAMS(camCutsceneDest, << 1105.9995, 2664.4695, 38.0625 >>, << 3.6984, 0.0242, -140.4769 >>, 41.7126)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << 1632.3412, 2087.7744, -69.7319 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << 1630.8264, 2085.1851, -69.7002 >>)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, << 2.8543, 0.5857, 0.5277 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, << 0.1319, -0.6669, 0.3870 >>)
//				SET_CAM_FOV(camCutsceneDest, 24.2221)
				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				NEW_LOAD_SCENE_STOP()
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 6000)
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 3
//			IF (iCutsceneWaitTimer - 1000) < GET_GAME_TIMER()
//				IF NOT IS_PED_INJURED(pedDrunk[0])
//					IF IS_PED_IN_ANY_VEHICLE(pedDrunk[0])
//						OPEN_SEQUENCE_TASK(seq)
////							TASK_LEAVE_ANY_VEHICLE(NULL)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo + << 0, 5, 0 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				DESTROY_CAM(camCutsceneOrigin)
//				DESTROY_CAM(camCutsceneDest)
//				camCutsceneOrigin = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				camCutsceneDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//				ATTACH_CAM_TO_ENTITY(camCutsceneOrigin, vehDrunksCar, << -6.7777, -2.0869, 0.7996 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneOrigin, vehDrunksCar, << -3.9569, -1.0666, 0.7560 >>)
//				SET_CAM_FOV(camCutsceneOrigin, 24.2221)
				SET_CAM_PARAMS(camCutsceneOrigin, <<1122.3701, 2646.1082, 38.3339>>, <<-5.6746, -0.1271, 33.7683>>, 50.0000)
				SET_CAM_PARAMS(camCutsceneDest, <<1122.3378, 2646.1575, 38.3281>>, <<-5.6746, -0.1271, 33.7683>>, 50.0000)
//				ATTACH_CAM_TO_ENTITY(camCutsceneDest, vehDrunksCar, << -6.7700, -2.1625, 0.7982 >>)
//				POINT_CAM_AT_ENTITY(camCutsceneDest, vehDrunksCar, << -3.9925, -1.0295, 0.7567 >>)
//				SET_CAM_FOV(camCutsceneDest, 24.2221)
				SET_CAM_ACTIVE_WITH_INTERP(camCutsceneDest, camCutsceneOrigin, 1500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF NOT IS_PED_INJURED(pedDrunk[0])
				AND NOT IS_PED_INJURED(pedDrunk[1])
					SET_ENTITY_COORDS(pedDrunk[0], <<1141.1556, 2643.2046, 37.1487>>)
					SET_ENTITY_HEADING(pedDrunk[0], 262.7369)
					SET_ENTITY_COORDS(pedDrunk[1], <<1140.1520, 2644.1489, 37.1487>>)
					SET_ENTITY_HEADING(pedDrunk[1], 262.1522)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())//, 1200)
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKF", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKM", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iCutsceneWaitTimer = (GET_GAME_TIMER() + 1700) //1500
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 4
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
////			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				iCutsceneWaitTimer = (GET_GAME_TIMER() + 2500)
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//				ENDIF
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 5
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//				ENDIF
//			ENDIF
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKT", CONV_PRIORITY_AMBIENT_HIGH)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1000)
//					iDropOffCutStage = 7
//				ENDIF
//			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKF", CONV_PRIORITY_AMBIENT_HIGH)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1500)
//					iDropOffCutStage = 7
//				ENDIF
//			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKM", CONV_PRIORITY_AMBIENT_HIGH)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 750)
					iDropOffCutStage = 7
//				ENDIF
//			ENDIF
		BREAK
		CASE 6
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_NOT", CONV_PRIORITY_AMBIENT_HIGH)
						iCutsceneWaitTimer = (GET_GAME_TIMER() + 5500)
						iDropOffCutStage ++
					ENDIF
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_MIS", CONV_PRIORITY_AMBIENT_HIGH)
						iCutsceneWaitTimer = (GET_GAME_TIMER() + 5500)
						iDropOffCutStage ++	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
	//			DO_SCREEN_FADE_OUT(1500)
	//			WHILE NOT IS_SCREEN_FADED_OUT()
	//				WAIT(0)
	//			ENDWHILE
				NEW_LOAD_SCENE_STOP()
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
	//			SET_CAM_ACTIVE(camCutsceneDest, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				DELETE_PED(pedDrunk[0])
				DELETE_PED(pedDrunk[1])
				IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
					SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
				ENDIF
				
				IF bDropOffSkipped
					IF NOT IS_ENTITY_DEAD(vehDrunksCar)
	//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1125.4337, 2647.9839, 36.9963>>)
	//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 180.8015)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_VEHICLE_DOORS_SHUT(vehDrunksCar)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
				ELSE
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ENDIF
				SET_CAM_ACTIVE(camCutsceneOrigin, FALSE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	//			DO_SCREEN_FADE_IN(1500)
	//			WHILE NOT IS_SCREEN_FADED_IN()
	//				WAIT(0)
	//			ENDWHILE
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 40, FALSE, TRUE)
				bDropOffDone = TRUE
//			ENDIF
	ENDSWITCH
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//		OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(vehDrunksCar)
//	ENDIF
	IF iDropOffCutStage <> 0
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			bDropOffSkipped = TRUE
			iDropOffCutStage = 7
		ENDIF
	ENDIF
ENDPROC

PROC endDropOffScene2OwnVehicle()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
			VEHICLE_INDEX vehTempForScene
			vehTempForScene = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
				IF IS_VEHICLE_STOPPED(vehTempForScene)
//					HIDE_HUD_AND_RADAR_THIS_FRAME()
//					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//					CLEAR_AREA(vDropOffPoint, 8, TRUE)
					IF IS_VEHICLE_DRIVEABLE(vehTempForScene)
						IF NOT IS_PED_INJURED(pedDrunk[0])
							CLEAR_PED_TASKS(pedDrunk[0])
							REMOVE_PED_FROM_GROUP(pedDrunk[0])
						ENDIF
						IF NOT IS_PED_INJURED(pedDrunk[1])
							CLEAR_PED_TASKS(pedDrunk[1])
							REMOVE_PED_FROM_GROUP(pedDrunk[1])
						ENDIF
					ENDIF
					CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TK", CONV_PRIORITY_AMBIENT_HIGH)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 1
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			SET_CAM_ACTIVE(camCutscene,TRUE)
//			SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
//			SET_CAM_PARAMS(camCutscene, << 342.1918, 2637.8706, 45.0729 >>, << -0.6133, 0.0784, -141.9198 >>, 29.990454)
//			SET_CAM_PARAMS(camCutscene, << 342.1844, 2637.8706, 45.0729 >>, << -0.6133, 0.0784, -139.1297 >>, 29.990454, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 2000)
			iDropOffCutStage ++
		BREAK
		CASE 2
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[1])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_PLAY_ANIM(NULL, "gestures@niko", "hello", SLOW_BLEND_IN, SLOW_BLEND_OUT,  -1, AF_SECONDARY| AF_UPPERBODY)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
					CLEAR_SEQUENCE_TASK(seq)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1500)
					iDropOffCutStage ++
				ENDIF
//			ENDIF
		BREAK
		CASE 3
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
//					TASK_LEAVE_ANY_VEHICLE(pedDrunk[0])
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[0], vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKT", CONV_PRIORITY_AMBIENT_HIGH)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1000)
					iDropOffCutStage = 7
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKF", CONV_PRIORITY_AMBIENT_HIGH)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 1500)
					iDropOffCutStage = 7
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TKM", CONV_PRIORITY_AMBIENT_HIGH)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 750)
					iDropOffCutStage = 7
				ENDIF
			ENDIF
		BREAK
		CASE 5
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
//				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				iDropOffCutStage ++
//			ENDIF
		BREAK
		CASE 6
			IF NOT IS_PED_INJURED(pedDrunk[0])
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_NOT", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[0], vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						iCutsceneWaitTimer = (GET_GAME_TIMER() + 3500)
						iDropOffCutStage ++
					ENDIF
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_MIS", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[0], vDropWalkTo, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						iCutsceneWaitTimer = (GET_GAME_TIMER() + 3500)
						iDropOffCutStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
//			IF iCutsceneWaitTimer < GET_GAME_TIMER()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 8
//			DO_SCREEN_FADE_OUT(1500)
//			WHILE NOT IS_SCREEN_FADED_OUT()
//				WAIT(0)
//			ENDWHILE
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedDrunk[0])
//			DELETE_PED(pedDrunk[1])
//			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			DO_SCREEN_FADE_IN(1500)
//			WHILE NOT IS_SCREEN_FADED_IN()
//				WAIT(0)
//			ENDWHILE
			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
			bDropOffDone = TRUE
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutStage = 8
//		ENDIF
//	ENDIF
ENDPROC

PROC endDropOffScene2OnFoot()
	
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH iDropOffCutStage
		//setup
		CASE 0
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//			CLEAR_AREA(vDropOffPoint, 8, TRUE)
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 349.0129, 2626.1733, 43.4994 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 214.8960)
			IF NOT IS_PED_INJURED(pedDrunk[0])
//				SET_ENTITY_COORDS (pedDrunk[0], << 346.6051, 2627.2771, 43.4994 >>) 
//				SET_ENTITY_HEADING (pedDrunk[0], -114.624832)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDrunk[0])
				REMOVE_PED_FROM_GROUP(pedDrunk[0])
				TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			IF NOT IS_PED_INJURED(pedDrunk[1])
//				SET_ENTITY_COORDS (pedDrunk[1], << 347.7403, 2628.7158, 43.4994 >>) 
//				SET_ENTITY_HEADING (pedDrunk[1], -153.411530)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDrunk[1])
				REMOVE_PED_FROM_GROUP(pedDrunk[0])
				TASK_LOOK_AT_ENTITY(pedDrunk[1], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TK", CONV_PRIORITY_AMBIENT_HIGH)
			iDropOffCutStage ++
		BREAK
		CASE 1
//			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			SET_CAM_ACTIVE(camCutscene,TRUE)
//			SET_CAM_PARAMS(camCutscene, << 346.0737, 2629.3447, 44.8784 >>, << -3.9839, 0.0798, -141.2246 >>, 40.646057)
//			SET_CAM_PARAMS(camCutscene, << 343.6924, 2627.9807, 45.0503 >>, << -5.0663, 0.0798, -110.3972 >>, 40.646057, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			iCutsceneWaitTimer = (GET_GAME_TIMER() + 1000)
			iDropOffCutStage ++
		BREAK
		CASE 2
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
					TASK_GO_STRAIGHT_TO_COORD(pedDrunk[0], vDropWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
					iCutsceneWaitTimer = (GET_GAME_TIMER() + 500)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[1])
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[1], vDropWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
//					iCutsceneWaitTimer = (GET_GAME_TIMER() + 10000)
					iDropOffCutStage ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF iCutsceneWaitTimer < GET_GAME_TIMER()
				iDropOffCutStage ++
			ENDIF
		BREAK
		CASE 5
//			DO_SCREEN_FADE_OUT(1500)
//			WHILE NOT IS_SCREEN_FADED_OUT()
//				WAIT(0)
//			ENDWHILE
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_CAM_ACTIVE(camCutscene, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			DELETE_PED(pedDrunk[0])
//			DELETE_PED(pedDrunk[1])
//			SETTIMERA(0)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			DO_SCREEN_FADE_IN(1500)
//			WHILE NOT IS_SCREEN_FADED_IN()
//				WAIT(0)
//			ENDWHILE
			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 80,FALSE,TRUE)
			bDropOffDone = TRUE
		BREAK
	ENDSWITCH
//	IF iDropOffCutStage <> 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iDropOffCutStage = 5
//		ENDIF
//	ENDIF
ENDPROC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //

// SHARED FLOW STAGES

//Create conversation to offer lift and progress stage
PROC drunkDriverOfferLift()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_OFFA", CONV_PRIORITY_AMBIENT_HIGH)
			//"Need a lift?"
			SETTIMERA(0)
			iGettingALiftStage ++
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_OFFB", CONV_PRIORITY_AMBIENT_HIGH)
			SETTIMERA(0)
			iGettingALiftStage ++
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_OFFC", CONV_PRIORITY_AMBIENT_HIGH)
			SETTIMERA(0)
			iGettingALiftStage ++
		ENDIF
	ENDIF
ENDPROC

PROC drunkDriverSequence()

	SWITCH thisStage
		// peds drunkily chat to one another
		CASE eS_INITIAL_STAGE
		
			IF thisEvent = eV_DRUNK_DRIVER
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1017.4605, -1259.8677, 3.9183 >>, << -1061.8913, -1424.0290, 24.4253 >>, 171.7500)
				AND IS_SPHERE_VISIBLE(vInitialMarkerPos, 1)
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInitialMarkerPos, << 15, 15, 15 >>)
					IF DOES_BLIP_EXIST(blipInitialMarker)
						REMOVE_BLIP(blipInitialMarker)
					ENDIF
					IF NOT IS_PED_INJURED(pedDrunk[0])
						IF NOT DOES_BLIP_EXIST(blipDrunkPed[0])
							blipDrunkPed[0] = CREATE_BLIP_FOR_PED(pedDrunk[0])
							SHOW_HEIGHT_ON_BLIP(blipDrunkPed[0], FALSE)
						ENDIF
					ENDIF
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							vehImpound = GET_PLAYERS_LAST_VEHICLE()
						ENDIF
						PRELOAD_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_DCO", CONV_PRIORITY_AMBIENT_HIGH)
						SET_RANDOM_EVENT_ACTIVE()
						DISABLE_TAXI_HAILING(TRUE)
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1011.144287, -1378.790039, 4.1997 >>, << -1039.684570, -1337.609619, 9.7035 >>, 37.8750)
						IF NOT IS_PED_INJURED(pedDrunk[0])
						AND NOT IS_PED_INJURED(pedDrunk[1])
							BEGIN_PRELOADED_CONVERSATION()
							TASK_PLAY_ANIM(pedDrunk[0], "random@drunk_driver_1", "drunk_argument_dd1", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
							TASK_PLAY_ANIM(pedDrunk[1], "random@drunk_driver_1", "drunk_argument_dd2", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
							iInitiatedEventTimer = (GET_GAME_TIMER() + 60000)
							IF DOES_BLIP_EXIST(blipDrunkPed[0])
								SHOW_HEIGHT_ON_BLIP(blipDrunkPed[0], TRUE)
							ENDIF
							thisStage = eS_ARGUE_WITH_BUDDY
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF thisEvent = eV_WITH_PASSENGER
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1820.2384, 3671.9484, 30.8437 >>, << 1957.8176, 3751.8874, 55.4436 >>, 159.9375)
				AND IS_SPHERE_VISIBLE(vInitialMarkerPos, 1)
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInitialMarkerPos, << 15, 15, 15 >>)
					changeBlips()
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							vehImpound = GET_PLAYERS_LAST_VEHICLE()
						ENDIF
						SET_RANDOM_EVENT_ACTIVE()
						DISABLE_TAXI_HAILING(TRUE)
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1877.7609, 3727.6997, 31.8820 >>, << 1896.961792, 3693.830078, 34.9925 >>, 19.1875)
						CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DCA", CONV_PRIORITY_AMBIENT_HIGH)
						IF DOES_BLIP_EXIST(blipDrunkPed[0])
							SHOW_HEIGHT_ON_BLIP(blipDrunkPed[0], TRUE)
						ENDIF
						IF DOES_BLIP_EXIST(blipDrunkPed[1])
							SHOW_HEIGHT_ON_BLIP(blipDrunkPed[1], TRUE)
						ENDIF
						iInitiatedEventTimer = (GET_GAME_TIMER() + 120000)
						thisStage = eS_ASK_FOR_LIFT
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		// in first variation (eV_DRUNK_DRIVER) the guys break off in separate directions
		CASE eS_ARGUE_WITH_BUDDY
			IF iInitiatedEventTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedDrunk[0])
				AND NOT IS_ENTITY_DEAD(vehDrunksCar)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrunk[0], << -1028.2952, -1344.0914, 4.4656 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.25)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, pedDrunk[1], 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						TASK_PAUSE(NULL, 2500)
						TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF NOT IS_PED_INJURED(pedDrunk[1])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, pedDrunk[0], 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1052.2147, -1354.9374, 4.3754 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1061.4636, -1384.9216, 4.2462 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1, ENAV_NO_STOPPING)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				SETTIMERA(0)//50000)
				thisStage = eS_ASK_FOR_LIFT
			ENDIF
			// skip a stage if player is nearby
			IF NOT IS_PED_INJURED(pedDrunk[0])
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 4 >>)
					IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
						bPlayerInterfering = TRUE
						SETTIMERA(0)
						thisStage = eS_WAIT_FOR_LIFT
					ENDIF
				ELIF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 30, 30, 30 >>)
					SET_NO_DUCKING_FOR_CONVERSATION(TRUE)
				ELSE
					SET_NO_DUCKING_FOR_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// in first variation (eV_DRUNK_DRIVER) the guy walks towards his car
		// his friend shouts out to him if the player hasn't intervened
		// in second variation (eV_WITH_PASSENGER) the guy asks the player for a lift
		CASE eS_ASK_FOR_LIFT
		//===========================================================================================================
			IF thisEvent = eV_DRUNK_DRIVER
				IF NOT IS_PED_INJURED(pedDrunk[0])
					IF NOT IS_PED_INJURED(pedDrunk[1])
						IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_DCL", CONV_PRIORITY_AMBIENT_HIGH)
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_LOOK_AT_ENTITY(NULL, pedDrunk[0], 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
////								TASK_PLAY_ANIM(NULL, "gestures@niko", "numbnuts", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1061.4636, -1384.9216, 4.2462 >> , PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1, ENAV_NO_STOPPING)
//								TASK_WANDER_STANDARD(NULL)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
//							CLEAR_SEQUENCE_TASK(seq)
							SETTIMERA(0)
							thisStage = eS_WAIT_FOR_LIFT
						ENDIF
					ENDIF
					// skip dialogue if player is nearby
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 4 >>)
						IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
							bPlayerInterfering = TRUE
							SETTIMERA(0)
							thisStage = eS_WAIT_FOR_LIFT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//===========================================================================================================	
			IF thisEvent = eV_WITH_PASSENGER
				IF NOT IS_PED_INJURED(pedDrunk[0])
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 30, 30, 30 >>)
						waitingDialogue()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateDriverPos, << 15, 15, 15 >>)
				AND NOT IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedDrunk[0])
						IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
							IF CAN_PED_SEE_HATED_PED(pedDrunk[0], PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(pedDrunk[0])
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
										TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "exit_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
									CLEAR_SEQUENCE_TASK(seq)
								ENDIF
								IF NOT IS_PED_INJURED(pedDrunk[1])
									TASK_LOOK_AT_ENTITY(pedDrunk[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
								ENDIF
								SETTIMERA(0)
								thisStage = eS_WAIT_FOR_LIFT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//bypass other checks if very close and not triggered
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateDriverPos, << 3, 3, 3 >>)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1889.1215, 3715.0566, 31.8440 >>, << 1895.6076, 3703.5864, 34.8272 >>, 12.4375)
					IF NOT IS_PED_INJURED(pedDrunk[0])
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
							TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "exit_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						IF NOT IS_PED_INJURED(pedDrunk[1])
							TASK_LOOK_AT_ENTITY(pedDrunk[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
						SETTIMERA(0)
						thisStage = eS_WAIT_FOR_LIFT
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehDrunksCar, << 1.5, 1.5, 5 >>)
					AND IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_TRY", CONV_PRIORITY_AMBIENT_HIGH)
							iGettingALiftStage = 2
							FOR i = 0 to 1
								IF DOES_BLIP_EXIST(blipDrunkPed[i])
									REMOVE_BLIP(blipDrunkPed[i])
								ENDIF
							ENDFOR
							IF NOT DOES_BLIP_EXIST(blipVehicle)
								blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDrunksCar)
							ENDIF
							IF NOT IS_PED_INJURED(pedDrunk[0])
								CLEAR_PED_TASKS_IMMEDIATELY(pedDrunk[0])
							ENDIF
							thisStage = eS_WAIT_FOR_LIFT
						ENDIF
					ENDIF
				ENDIF
				IF iInitiatedEventTimer < GET_GAME_TIMER()
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_DRV", CONV_PRIORITY_AMBIENT_HIGH)
						IF NOT IS_PED_INJURED(pedDrunk[0])
						AND NOT IS_PED_INJURED(pedDrunk[1])
						AND DOES_ENTITY_EXIST(vehDrunksCar)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, pedDrunk[1], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//								TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "exit_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//								TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedDrunk[1])
								TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//								TASK_PLAY_ANIM(NULL, "random@drunk_driver_2", "exit_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedDrunk[0])
								TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							FOR i = 0 to 1
								IF DOES_BLIP_EXIST(blipDrunkPed[i])
									REMOVE_BLIP(blipDrunkPed[i])
								ENDIF
							ENDFOR
							
							thisStage = eS_GETTING_TO_VEHICLE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eS_WAIT_FOR_LIFT
			//===========================================================================================================	
			IF thisEvent = eV_DRUNK_DRIVER
				SWITCH iGettingALiftStage
					// Player offers lift when close
					CASE 0
						IF NOT IS_PED_INJURED(pedDrunk[0])
						AND NOT IS_PED_INJURED(pedDrunk[1])
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 8, 8, 4 >>)
							OR bPlayerInterfering
								SET_NO_DUCKING_FOR_CONVERSATION(FALSE)
								TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
								TASK_LOOK_AT_ENTITY(pedDrunk[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//								IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
								IF (GET_GAME_TIMER() + 50000) > iInitiatedEventTimer
								OR IS_PLAYER_PRESSING_HORN(PLAYER_ID())
									KILL_FACE_TO_FACE_CONVERSATION()
									IF bPlayerInterfering
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
												IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_HYM", CONV_PRIORITY_AMBIENT_HIGH)
													//"Need a lift?"
													SETTIMERA(0)
													iGettingALiftStage ++
												ENDIF
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
												IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_HYF", CONV_PRIORITY_AMBIENT_HIGH)
													SETTIMERA(0)
													iGettingALiftStage ++
												ENDIF
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
												IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_HYT", CONV_PRIORITY_AMBIENT_HIGH)
													SETTIMERA(0)
													iGettingALiftStage ++
												ENDIF
											ENDIF
										ENDIF
									ELSE
										iGettingALiftStage ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								IF NOT IS_PED_INJURED(pedDrunk[0])
//									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 15, 15, 4 >>)
//										IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
											OPEN_SEQUENCE_TASK(seq)
												TASK_PLAY_ANIM(NULL, "random@drunk_driver_1", "drunk_breakout_dd1", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
											CLEAR_SEQUENCE_TASK(seq)
											IF NOT IS_PED_INJURED(pedDrunk[1])
												OPEN_SEQUENCE_TASK(seq)
													TASK_PLAY_ANIM(NULL, "random@drunk_driver_1", "drunk_breakout_dd2", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
													TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedDrunk[0])
													TASK_PAUSE(NULL, 3000)
//													TASK_STAND_STILL(NULL, 3500)
													TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1052.2147, -1354.9374, 4.3754 >> , PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1)
													TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1061.4636, -1384.9216, 4.2462 >> , PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1, ENAV_NO_STOPPING)
													TASK_WANDER_STANDARD(NULL)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
												CLEAR_SEQUENCE_TASK(seq)
											ENDIF
//											TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//											TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
											drunkDriverOfferLift()
//										ENDIF
//									ENDIF
								ENDIF
							ELSE
//								IF IS_PED_ON_FOOT(PLAYER_PED_ID())
									IF NOT IS_PED_INJURED(pedDrunk[0])
//										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 15, 15, 4 >>)
											IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedDrunk[0], 80)
												TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
												TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
												KILL_FACE_TO_FACE_CONVERSATION()
												WAIT(0)
//												IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WHO", CONV_PRIORITY_AMBIENT_HIGH)
													iGettingALiftStage ++
//												ENDIF
											ELSE
												continualTurnToFacePlayer(pedDrunk[0], iTurnStage[0])
											ENDIF
//										ENDIF
									ENDIF
									
									
									//Force to next stage to check for using vehicle
									//When dialogue complete
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iGettingALiftStage ++
									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					BREAK
					// check whether player has their own vehicle to use or not
					CASE 2
						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							iGettingALiftStage = 6
						ELSE
							iGettingALiftStage ++
						ENDIF
					BREAK
					// player doesn't have a vehicle - drunk guy accepts lift
					CASE 3
						IF NOT bPointedAtCar
							IF TIMERA() > 1500
								IF NOT IS_PED_INJURED(pedDrunk[0])
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//										TASK_PLAY_ANIM(NULL, "gestures@male", "point_fwd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
										TASK_PAUSE(NULL, 500)
//										TASK_PLAY_ANIM(NULL, "gestures@male", "ok_ok", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
									CLEAR_SEQUENCE_TASK(seq)
									SETTIMERA(0)
									bPointedAtCar = TRUE
								ENDIF
							ENDIF
						ENDIF
						continualTurnToFacePlayer(pedDrunk[0], iTurnStage[0])
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_LFT", CONV_PRIORITY_AMBIENT_HIGH)
								iGettingALiftStage ++
							ENDIF
						ENDIF
					BREAK
					// player doesn't have a vehicle - drunk guy offers their car
					CASE 4
						IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_PLS", CONV_PRIORITY_AMBIENT_HIGH)
									TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
									IF DOES_BLIP_EXIST(blipDrunkPed[0])
										REMOVE_BLIP(blipDrunkPed[0])
									ENDIF
									IF DOES_BLIP_EXIST(blipDrunkPed[1])
										REMOVE_BLIP(blipDrunkPed[1])
									ENDIF
//									IF NOT DOES_BLIP_EXIST(blipVehicle)
//										IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
//											blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDrunksCar)
//										ENDIF
//									ENDIF
									IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
										SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_UNLOCKED)
										IF NOT IS_PED_INJURED(pedDrunk[0])
											TASK_ENTER_VEHICLE(pedDrunk[0], vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK) //30000
										ENDIF
									ENDIF
									iGettingALiftStage ++
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(blipDrunkPed[0])
								REMOVE_BLIP(blipDrunkPed[0])
							ENDIF
							IF DOES_BLIP_EXIST(blipDrunkPed[1])
								REMOVE_BLIP(blipDrunkPed[1])
							ENDIF
							iGettingALiftStage ++
						ENDIF
					BREAK
					// waiting for the player to get into a vehicle
					CASE 5
						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							IF NOT IS_PED_INJURED(pedDrunk[0])
								IF NOT IS_PED_IN_GROUP(pedDrunk[0])
									CLEAR_PED_TASKS(pedDrunk[0])
									SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedDrunk[0], TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_FRONT_RIGHT)
								ENDIF
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghDrunkPeds, RELGROUPHASH_PLAYER)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
							ENDIF
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF DOES_BLIP_EXIST(blipVehicle)
//									REMOVE_BLIP(blipVehicle)
//								ENDIF
								IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> vehDrunksCar
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
											IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKM", CONV_PRIORITY_AMBIENT_HIGH)
												//"No, we'll just take mine instead."
												thisStage = eS_PLAYER_INTERVENTION
											ENDIF
										ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
											IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKF", CONV_PRIORITY_AMBIENT_HIGH)
												thisStage = eS_PLAYER_INTERVENTION
											ENDIF
										ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
											IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKT", CONV_PRIORITY_AMBIENT_HIGH)
												thisStage = eS_PLAYER_INTERVENTION
											ENDIF
										ENDIF
									ENDIF
								ELSE
									thisStage = eS_PLAYER_INTERVENTION
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 6
						continualTurnToFacePlayer(pedDrunk[0], iTurnStage[0])
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									// "Yeah, could you?"
									IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_AKB", CONV_PRIORITY_AMBIENT_HIGH)
										iGettingALiftStage = 10
									ENDIF
								ENDIF
							ELSE
								iGettingALiftStage = 4
							ENDIF
						ELSE
							iGettingALiftStage = 4
						ENDIF
					BREAK
					CASE 7
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIA", CONV_PRIORITY_AMBIENT_HIGH)
											//Jump in then, let's go.
											SETTIMERA(0)
											iGettingALiftStage = 10
										ENDIF
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIB", CONV_PRIORITY_AMBIENT_HIGH)
											SETTIMERA(0)
											iGettingALiftStage = 10
										ENDIF
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIC", CONV_PRIORITY_AMBIENT_HIGH)
											SETTIMERA(0)
											iGettingALiftStage = 10
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iGettingALiftStage = 4
							ENDIF
						ELSE
							iGettingALiftStage = 4
						ENDIF
					BREAK
//					CASE 8
//						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_MY", CONV_PRIORITY_AMBIENT_HIGH)
//									IF NOT DOES_BLIP_EXIST(blipVehicle)
//										IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
//											IF NOT IS_PED_INJURED(pedDrunk[0])
//												TASK_GO_TO_ENTITY(pedDrunk[0], vehDrunksCar, DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK)
//											ENDIF
//											blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDrunksCar)
//											SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_UNLOCKED)
//										ENDIF
//									ENDIF
//									iGettingALiftStage ++
//								ENDIF
//							ENDIF
//						ENDIF
//					BREAK
//					CASE 9
//						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) <> vehDrunksCar
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKM", CONV_PRIORITY_AMBIENT_HIGH)
//											//"No, we'll just take mine instead."
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											IF NOT IS_PED_INJURED(pedDrunk[0])
//												CLEAR_PED_TASKS(pedDrunk[0])
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKF", CONV_PRIORITY_AMBIENT_HIGH)
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											IF NOT IS_PED_INJURED(pedDrunk[0])
//												CLEAR_PED_TASKS(pedDrunk[0])
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_TKT", CONV_PRIORITY_AMBIENT_HIGH)
//											IF NOT IS_PED_INJURED(pedDrunk[0])
//												CLEAR_PED_TASKS(pedDrunk[0])
//											ENDIF
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//								ELSE
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIA", CONV_PRIORITY_AMBIENT_HIGH)
//											//Jump in then, let's go.
//											SETTIMERA(0)
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIB", CONV_PRIORITY_AMBIENT_HIGH)
//											SETTIMERA(0)
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_JIC", CONV_PRIORITY_AMBIENT_HIGH)
//											SETTIMERA(0)
//											IF DOES_BLIP_EXIST(blipVehicle)
//												REMOVE_BLIP(blipVehicle)
//											ENDIF
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					BREAK
					CASE 10
//						IF TIMERA() > 1000
						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							IF NOT IS_PED_INJURED(pedDrunk[0])
								IF NOT IS_PED_IN_GROUP(pedDrunk[0])
									CLEAR_PED_TASKS(pedDrunk[0])
									SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedDrunk[0], TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_FRONT_RIGHT)
								ENDIF
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghDrunkPeds, RELGROUPHASH_PLAYER)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
							ENDIF
							iGettingALiftStage ++
						ELSE
							iGettingALiftStage = 4
						ENDIF
					BREAK
					CASE 11
						thisStage = eS_PLAYER_INTERVENTION
					BREAK
				ENDSWITCH
			ENDIF
			//CASE eS_WAIT_FOR_LIFT===========================================================================================================	
			IF thisEvent = eV_WITH_PASSENGER
				SWITCH iGettingALiftStage
					// Check whether Player is on foot or in a suitable vehicle and ask for a lift
					CASE 0
						IF NOT IS_PED_INJURED(pedDrunk[0])
						AND NOT IS_PED_INJURED(pedDrunk[1])
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 10, 10, 10 >>)
								IF IS_PED_FACING_PED(pedDrunk[0], PLAYER_PED_ID(), 50)
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
									TASK_LOOK_AT_ENTITY(pedDrunk[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
									TASK_LOOK_AT_ENTITY(pedDrunk[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT|SLF_EXTEND_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//									IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
										IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
										ENDIF
										KILL_FACE_TO_FACE_CONVERSATION()//_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										//"Hey! Can you drive us?", "My car is just over there."
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_AKA", CONV_PRIORITY_AMBIENT_HIGH)
											iGettingALiftStage ++
										ENDIF
//									ELSE
//										KILL_FACE_TO_FACE_CONVERSATION()
//										WAIT(0)
//										//"Hey man, can you give us a lift? We could just jump in?"
//										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_AKB", CONV_PRIORITY_AMBIENT_HIGH)
//											iGettingALiftStage ++
//										ENDIF
//									ENDIF
								ELSE
									continualTurnToFacePlayer(pedDrunk[0], iTurnStage[0]) 
								ENDIF
							ENDIF
						ENDIF
					BREAK
					// Play some anims to go alongside speech
					CASE 1
						IF NOT IS_PED_INJURED(pedDrunk[0])
							OPEN_SEQUENCE_TASK(seq)
//								TASK_PLAY_ANIM(NULL, "gestures@male", "point_fwd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
								TASK_PAUSE(NULL, 500)
//								TASK_PLAY_ANIM(NULL, "gestures@male", "ok_ok", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							SETTIMERA(0)
							iGettingALiftStage ++
						ENDIF
					BREAK
					
					CASE 2
						continualTurnToFacePlayer(pedDrunk[0], iTurnStage[0])
						IF NOT IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
								SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_UNLOCKED)
							ENDIF
						ENDIF
						IF TIMERA() > 8000
							IF DOES_BLIP_EXIST(blipDrunkPed[0])
								REMOVE_BLIP(blipDrunkPed[0])
							ENDIF
							IF DOES_BLIP_EXIST(blipDrunkPed[1])
								REMOVE_BLIP(blipDrunkPed[1])
							ENDIF
							IF NOT DOES_BLIP_EXIST(blipVehicle)
								IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
									blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDrunksCar)
									SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_UNLOCKED)
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_PED_INJURED(pedDrunk[0])
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[0], << 20, 20, 20 >>)
								AND TIMERA() > 9500
									INT iSpeechPicker
									iSpeechPicker = GET_RANDOM_INT_IN_RANGE(0, 100)
									IF iSpeechPicker < 60
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_PLS", CONV_PRIORITY_AMBIENT_HIGH)
											SETTIMERA(0)
										ENDIF
									ELSE
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_CAR", CONV_PRIORITY_AMBIENT_HIGH)
											SETTIMERA(0)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehDrunksCar)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar, TRUE)
//							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//								IF DOES_BLIP_EXIST(blipVehicle)
//									REMOVE_BLIP(blipVehicle)
//								ENDIF
								thisStage = eS_PLAYER_INTERVENTION
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF thisEvent = eV_DRUNK_DRIVER
				IF NOT IS_PED_INJURED(pedDrunk[0])
				AND NOT IS_ENTITY_DEAD(vehDrunksCar)
					IF TIMERA() > 50000
					OR GET_PED_IN_VEHICLE_SEAT(vehDrunksCar, VS_DRIVER) = pedDrunk[0]
						IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_FU", CONV_PRIORITY_AMBIENT_HIGH)
							// "Fuck you then. I'll drive myself"
							IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
							AND NOT IS_VEHICLE_STUCK_ON_ROOF(vehDrunksCar)
								IF NOT IS_PED_INJURED(pedDrunk[0])
									IF IS_VEHICLE_SEAT_FREE(vehDrunksCar, VS_DRIVER)
									AND IS_PED_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
	//									TASK_ENTER_VEHICLE(pedDrunk[0], vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
										TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(pedDrunk[0], vehDrunksCar)
										thisStage = eS_GETTING_TO_VEHICLE
									ELSE
										IF GET_PED_IN_VEHICLE_SEAT(vehDrunksCar, VS_DRIVER) = pedDrunk[0]
											thisStage = eS_GETTING_TO_VEHICLE
										ELIF GET_PED_IN_VEHICLE_SEAT(vehDrunksCar, VS_DRIVER) = PLAYER_PED_ID()
											thisStage = eS_PLAYER_INTERVENTION
										ELSE
											Make_Ped_Drunk(pedDrunk[0], 120000)
											TASK_WANDER_STANDARD(pedDrunk[0])
											SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
											WAIT(0)
											MISSION_FAILED()
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(pedDrunk[0])
									Make_Ped_Drunk(pedDrunk[0], 120000)
									TASK_WANDER_STANDARD(pedDrunk[0])
									SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
									WAIT(0)
									MISSION_FAILED()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF thisEvent = eV_WITH_PASSENGER
				IF TIMERA() > 60000
					IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_FU", CONV_PRIORITY_AMBIENT_HIGH)
						// "Fuck you then.  I'll drive myself"
						IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
							IF IS_VEHICLE_SEAT_FREE(vehDrunksCar, VS_DRIVER)
								IF NOT IS_PED_INJURED(pedDrunk[0])
									OPEN_SEQUENCE_TASK(seq)
										TASK_CLEAR_LOOK_AT(NULL)
										TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
									CLEAR_SEQUENCE_TASK(seq)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(pedDrunk[1])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							thisStage = eS_GETTING_TO_VEHICLE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		//drunk peds decide to drive away by themselves - get in vehicle
		CASE eS_GETTING_TO_VEHICLE
			IF thisEvent = eV_DRUNK_DRIVER
				IF NOT IS_PED_INJURED(pedDrunk[0])
					IF NOT IS_ENTITY_DEAD(vehDrunksCar)
						IF IS_PED_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
							IF GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
								SET_VEHICLE_ENGINE_HEALTH(vehDrunksCar, 5)
								SET_DISABLE_PRETEND_OCCUPANTS(vehDrunksCar, TRUE)
								
								OPEN_SEQUENCE_TASK(seq)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehDrunksCar, << -1016.1517, -1352.8549, 4.4854 >>, MISSION_GOTO, 50, DRIVINGMODE_PLOUGHTHROUGH, -1, 100)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehDrunksCar, << -1005.7568, -1353.8058, 4.4756 >>, MISSION_GOTO, 50, DRIVINGMODE_PLOUGHTHROUGH, -1, 100)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehDrunksCar, << -989.2302, -1354.2777, -7.3581 >>, MISSION_GOTO, 50, DRIVINGMODE_PLOUGHTHROUGH, -1, 100)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
								CLEAR_SEQUENCE_TASK(seq)
								SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF thisEvent = eV_WITH_PASSENGER
				IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
					IF NOT IS_PED_INJURED(pedDrunk[0])
					AND NOT IS_PED_INJURED(pedDrunk[1])
						IF IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
						AND IS_PED_SITTING_IN_VEHICLE(pedDrunk[1], vehDrunksCar)
							SET_DISABLE_PRETEND_OCCUPANTS(vehDrunksCar, TRUE)
							
							CLEAR_PED_TASKS(pedDrunk[1])
							TASK_STAND_STILL(pedDrunk[1], -1)
							SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
							thisStage = eS_DRIVE_AWAY_TASK
						ENDIF
						IF NOT bSitStill
							IF IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
							AND NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[1], vehDrunksCar)
								CLEAR_PED_TASKS(pedDrunk[0])
								TASK_STAND_STILL(pedDrunk[0], -1)
							ENDIF
							IF NOT IS_PED_SITTING_IN_VEHICLE(pedDrunk[0], vehDrunksCar)
							AND IS_PED_SITTING_IN_VEHICLE(pedDrunk[1], vehDrunksCar)
								CLEAR_PED_TASKS(pedDrunk[1])
								TASK_STAND_STILL(pedDrunk[1], -1)
							ENDIF
							bSitStill = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//drunk peds decide to drive away by themselves - drive away
		CASE eS_DRIVE_AWAY_TASK
			IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
				IF NOT IS_PED_INJURED(pedDrunk[0])
					CLEAR_PED_TASKS(pedDrunk[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehDrunksCar, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehDrunksCar, << 0, 2, 0>>), 10, DRIVINGSTYLE_NORMAL, modelDrunksVehicle, DRIVINGMODE_PLOUGHTHROUGH, 5, -1)
						TASK_VEHICLE_TEMP_ACTION(NULL, vehDrunksCar, TEMPACT_BRAKE, 500)
						TASK_VEHICLE_TEMP_ACTION(NULL, vehDrunksCar, TEMPACT_GOFORWARD,1500)
						TASK_VEHICLE_TEMP_ACTION(NULL, vehDrunksCar, TEMPACT_BRAKE, 500)
						TASK_VEHICLE_TEMP_ACTION(NULL, vehDrunksCar, TEMPACT_SWERVERIGHT, 1500)
//						TASK_VEHICLE_DRIVE_WANDER(NULL, vehDrunksCar, 17, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
						TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehDrunksCar, << 2185.8171, 3915.4714, 27.9257 >>, MISSION_GOTO, 30, DRIVINGMODE_PLOUGHTHROUGH, 0, 0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedDrunk[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
				ENDIF
			ENDIF
			thisStage = eS_DRIVING_AWAY
		BREAK
		
		CASE eS_DRIVING_AWAY
			IF NOT IS_PED_INJURED(pedDrunk[0])
				IF GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					IF GET_SEQUENCE_PROGRESS(pedDrunk[0]) = 5
						MISSION_FAILED()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// player 'offers' to drive ped(s)
		CASE eS_PLAYER_INTERVENTION
			IF thisEvent = eV_DRUNK_DRIVER
				IF NOT IS_PED_INJURED(pedDrunk[1])
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[1], << 50, 50, 50 >>)
						SET_PED_AS_NO_LONGER_NEEDED(pedDrunk[1])
					ENDIF
				ENDIF
				SWITCH iInterventionStage
					CASE 0
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//							IF DOES_BLIP_EXIST(blipVehicle)
//								REMOVE_BLIP(blipVehicle)
//							ENDIF
							IF NOT DOES_BLIP_EXIST(blipDrunkPed[0])
								blipDrunkPed[0] = CREATE_BLIP_FOR_PED(pedDrunk[0])
							ENDIF
							iInterventionStage ++
							SETTIMERA(0)
						ENDIF
					BREAK
					
					CASE 1
						IF TIMERA() > 1000
							IF NOT IS_PED_INJURED(pedDrunk[0])
								IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
									IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
									//	TASK_ENTER_VEHICLE(pedDrunk[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), -1, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_USE_RIGHT_ENTRY| ECF_BLOCK_SEAT_SHUFFLING)
										fStoredDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDrunk[0])
										iInterventionStage ++
									ENDIF
								ELSE
									IF NOT bAskedToGetBackIn
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_LFC", CONV_PRIORITY_AMBIENT_HIGH)
											bAskedToGetBackIn = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						dontFollowPlayerInWrongVehicle()
						IF NOT IS_PED_INJURED(pedDrunk[0])
							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								IF NOT bAskedToWaitToGetIn
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDrunk[0]) > (fStoredDistance + 5)
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_LEA", CONV_PRIORITY_AMBIENT_HIGH)
											bAskedToWaitToGetIn = TRUE
										ENDIF
									ENDIF
								ENDIF
//								IF NOT IS_PED_IN_GROUP(pedDrunk[0])
//									CLEAR_PED_TASKS(pedDrunk[0])
//									SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
//									SET_PED_NEVER_LEAVES_GROUP(pedDrunk[0], TRUE)
//									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_FRONT_RIGHT)
//								ENDIF
							ELSE
								IF NOT bAskedToGetBackIn
									IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_LFC", CONV_PRIORITY_AMBIENT_HIGH)
										bAskedToGetBackIn = TRUE
									ENDIF
								ENDIF
//								IF IS_PED_IN_GROUP(pedDrunk[0])
//									REMOVE_PED_FROM_GROUP(pedDrunk[0])
//									TASK_TURN_PED_TO_FACE_ENTITY(pedDrunk[0], PLAYER_PED_ID(), -1)
//								ENDIF
							ENDIF
						ENDIF
						IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
							IF IS_VEHICLE_DRIVEABLE(vehDrunksCar)
								SET_VEHICLE_DOORS_LOCKED(vehDrunksCar, VEHICLELOCK_UNLOCKED)
							ENDIF
							IF DOES_BLIP_EXIST(blipDrunkPed[0])
								REMOVE_BLIP(blipDrunkPed[0])
							ENDIF
//							IF NOT DOES_BLIP_EXIST(blipDropOff)
//								blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPoint, TRUE)
//							ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							AND NOT IS_CULT_FINISHED()
								IF NOT DOES_BLIP_EXIST(blipCult)
									blipCult = CREATE_BLIP_FOR_COORD(vCult)
									SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
									PRINT_CULT_HELP()
								ENDIF
							ENDIF
							bDrivingHome = TRUE
							iInterventionStage ++
						ENDIF
						IF NOT IS_PED_INJURED(pedDrunk[0])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDrunk[0]) > 50
								MISSION_FAILED()
							ENDIF
						ENDIF
					BREAK
					
					CASE 3
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							WAIT(1500)
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WHA", CONV_PRIORITY_AMBIENT_HIGH)
									iInterventionStage ++
								ENDIF
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WHB", CONV_PRIORITY_AMBIENT_HIGH)
									iInterventionStage ++
								ENDIF
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WHC", CONV_PRIORITY_AMBIENT_HIGH)
									iInterventionStage ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WH1", CONV_PRIORITY_AMBIENT_HIGH)
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_WH2", CONV_PRIORITY_AMBIENT_HIGH)
								iJourneyTimer = GET_GAME_TIMER()
								iInterventionStage ++
							ENDIF
						ENDIF
					BREAK
					
					CASE 5
						groupSeparationCheck()
						stationaryCheck()
						dontFollowPlayerInWrongVehicle()
						IF NOT bSickWarning
							IF (GET_GAME_TIMER() - iJourneyTimer) > 7000
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_ILL", CONV_PRIORITY_AMBIENT_HIGH)
										iJourneyTimer = GET_GAME_TIMER()
										bSickWarning = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
								IF bGuyBeSick
								AND NOT bSickingDone
									makeGuySpew()
								ELSE
									checkPlayersDriving()
 									inVehDialogue()
//									IF DOES_CAM_EXIST(camVomit)
//										RENDER_SCRIPT_CAMS(FALSE, FALSE)
//										DESTROY_CAM(camVomit)
//									ENDIF
									SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
								ENDIF
							ENDIF
						ENDIF
						IF READY_FOR_CULT_DIALOGUE(vDropOffPoint)
							MANAGE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
//							CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_OFFR", CONV_PRIORITY_AMBIENT_HIGH)
							CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_OFFR", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
							MANAGE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_CULT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF NOT IS_PED_INJURED(pedDrunk[0])
							IF IS_ENTITY_AT_COORD(pedDrunk[0], vDropOffPoint, g_vOnFootLocate, TRUE)
								IF DOES_BLIP_EXIST(blipDropOff)
									REMOVE_BLIP(blipDropOff)
								ENDIF
								IF DOES_BLIP_EXIST(blipCult)
									REMOVE_BLIP(blipCult)
								ENDIF
								thisStage = eS_DROP_OFF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF thisEvent = eV_WITH_PASSENGER
				SWITCH iInterventionStage
					CASE 0
						IF NOT IS_ENTITY_DEAD(vehDrunksCar)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar, TRUE)
//							IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//								KILL_FACE_TO_FACE_CONVERSATION()
//								WAIT(0)
								IF NOT DOES_BLIP_EXIST(blipDrunkPed[0])
									blipDrunkPed[0] = CREATE_BLIP_FOR_PED(pedDrunk[0])
								ENDIF
								IF NOT DOES_BLIP_EXIST(blipDrunkPed[1])
									blipDrunkPed[1] = CREATE_BLIP_FOR_PED(pedDrunk[1])
								ENDIF
//								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//								WAIT(0)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									SETTIMERA(0)
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_JIA", CONV_PRIORITY_AMBIENT_HIGH)
											iInterventionStage ++
										ENDIF
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_JIB", CONV_PRIORITY_AMBIENT_HIGH)
											iInterventionStage ++
										ENDIF
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_JIC", CONV_PRIORITY_AMBIENT_HIGH)
											iInterventionStage ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
//						IF TIMERA() > 500
							IF NOT IS_PED_INJURED(pedDrunk[0])
							AND NOT IS_ENTITY_DEAD(vehDrunksCar)
//								CLEAR_PED_TASKS(pedDrunk[0])
								TASK_CLEAR_LOOK_AT(pedDrunk[0])
								TASK_ENTER_VEHICLE(pedDrunk[0], vehDrunksCar, DEFAULT_TIME_BEFORE_WARP +10000, VS_BACK_RIGHT)
//								SET_PED_AS_GROUP_MEMBER(pedDrunk[0], GET_PLAYER_GROUP(PLAYER_ID()))
//								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[0], VS_BACK_RIGHT)
							ENDIF
							SETTIMERA(0)
							iInterventionStage ++
//						ENDIF
					BREAK
					
					CASE 2
//						IF TIMERA() > 500
							IF NOT IS_PED_INJURED(pedDrunk[1])
							AND NOT IS_ENTITY_DEAD(vehDrunksCar)
//								CLEAR_PED_TASKS(pedDrunk[1])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, vehDrunksCar, << 1.5, -3, 0 >>, PEDMOVEBLENDRATIO_WALK, 4000)
									TASK_ENTER_VEHICLE(NULL, vehDrunksCar, DEFAULT_TIME_BEFORE_WARP +10000, VS_BACK_LEFT)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedDrunk[1], seq)
								CLEAR_SEQUENCE_TASK(seq)
//								SET_PED_AS_GROUP_MEMBER(pedDrunk[1], GET_PLAYER_GROUP(PLAYER_ID()))
//								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedDrunk[1], VS_BACK_LEFT)
							ENDIF
							SETTIMERA(0)
							iInterventionStage ++
//						ENDIF
					BREAK
					
					CASE 3
						groupSeparationCheck()
						stationaryCheck()
						IF NOT bJumpInBackLine
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_BCK", CONV_PRIORITY_AMBIENT_HIGH)
									bJumpInBackLine = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NOT IS_ENTITY_DEAD(pedDrunk[0])
								AND NOT IS_ENTITY_DEAD(pedDrunk[1])
									IF IS_PED_IN_VEHICLE(pedDrunk[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									AND IS_PED_IN_VEHICLE(pedDrunk[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghDrunkPeds, RELGROUPHASH_PLAYER)
										REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
										FOR i = 0 to (iNumberOfPedsInEvent - 1)
											IF DOES_BLIP_EXIST(blipDrunkPed[i])
												REMOVE_BLIP(blipDrunkPed[i])
											ENDIF
										ENDFOR
										IF DOES_BLIP_EXIST(blipVehicle)
											REMOVE_BLIP(blipVehicle)
										ENDIF
//										IF NOT DOES_BLIP_EXIST(blipDropOff)
//											blipDropOff = CREATE_BLIP_FOR_COORD(vDropOffPoint, TRUE)
//										ENDIF
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										AND NOT IS_CULT_FINISHED()
											IF NOT DOES_BLIP_EXIST(blipCult)
												blipCult = CREATE_BLIP_FOR_COORD(vCult)
												SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
												PRINT_CULT_HELP()
											ENDIF
										ENDIF
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_WHA", CONV_PRIORITY_AMBIENT_HIGH)
												SETTIMERA(0)
												iInterventionStage ++
											ENDIF
										ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_WHB", CONV_PRIORITY_AMBIENT_HIGH)
												SETTIMERA(0)
												iInterventionStage ++
											ENDIF
										ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										 	IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_WHC", CONV_PRIORITY_AMBIENT_HIGH)
												SETTIMERA(0)
												iInterventionStage ++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						groupSeparationCheck()
						stationaryCheck()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_WH2", CONV_PRIORITY_AMBIENT_HIGH)
								// "Take us to the motel."
								bDrivingHome = TRUE
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								iJourneyTimer = GET_GAME_TIMER()
								iInterventionStage ++
							ENDIF
						ENDIF
					BREAK
					
					CASE 5
						groupSeparationCheck()
						IF iCarShaggingStage = 0
						OR iCarShaggingStage = 9
							stationaryCheck()
						ENDIF
						
						#IF NOT IS_JAPANESE_BUILD
							IF GET_GAME_TIMER() - iJourneyTimer > 9000
								shaggingInBackOfCar()
							ENDIF
							IF iCarShaggingStage = 9
							AND (GET_GAME_TIMER() - iStageShaggingTimer > 6000 AND GET_GAME_TIMER() - iStageShaggingTimer < 6500)
								CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_CHAT", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						#ENDIF
						
						IF READY_FOR_CULT_DIALOGUE(vDropOffPoint)
							MANAGE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_OFFR", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
							MANAGE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_CULT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(vehDrunksCar)
							IF NOT IS_PED_INJURED(pedDrunk[0])
							AND NOT IS_PED_INJURED(pedDrunk[1])
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrunksCar)
								IF IS_ENTITY_AT_COORD(pedDrunk[0], vDropOffPoint, g_vOnFootLocate, TRUE)
								AND IS_ENTITY_AT_COORD(pedDrunk[1], vDropOffPoint, g_vOnFootLocate)
									iInterventionStage ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 6
						IF DOES_BLIP_EXIST(blipDropOff)
							REMOVE_BLIP(blipDropOff)
						ENDIF
						IF DOES_BLIP_EXIST(blipCult)
							REMOVE_BLIP(blipCult)
						ENDIF
						thisStage =	eS_DROP_OFF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE eS_DROP_OFF
			FOR i = 0 to (iNumberOfPedsInEvent -1)
				IF NOT IS_PED_INJURED(pedDrunk[i])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[i], FALSE)
				ENDIF
			ENDFOR
			
			IF thisEvent = eV_DRUNK_DRIVER
				IF CAN_PLAYER_START_CUTSCENE()
				AND iCutsceneToPlay = -1
					KILL_FACE_TO_FACE_CONVERSATION()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE -5, 2)
					ENDIF
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						WAIT(0)
//					ENDWHILE
					IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
						IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = vehDrunksCar
							iCutsceneToPlay = 0
						ELSE
							iCutsceneToPlay = 1
						ENDIF
					ELSE
						iCutsceneToPlay = 2
					ENDIF
				ENDIF
				IF iCutsceneToPlay = 0
					endDropOffScene1DrunksVehicle()
				ENDIF
				IF iCutsceneToPlay = 1
					endDropOffScene1OwnVehicle()
				ENDIF
				IF iCutsceneToPlay = 2
					endDropOffScene1OnFoot()
				ENDIF
				
				IF bDropOffDone
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					MISSION_PASSED()
				ENDIF
			ENDIF
			
			IF thisEvent = eV_WITH_PASSENGER
				IF CAN_PLAYER_START_CUTSCENE()
				AND iCutsceneToPlay = -1
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE -4, 2)
						NEW_LOAD_SCENE_START_SPHERE(<<1143.5865, 2672.2236, 37.1142>>, 70, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						NEW_LOAD_SCENE_START_SPHERE(<< 930.2788, 2856.1450, 59.2884 >>, 70, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					ENDIF
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						WAIT(0)
					ENDWHILE
					IF DOES_PLAYER_HAVE_DRUNKS_IN_HIS_VEHICLE()
						IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = vehDrunksCar
							iCutsceneToPlay = 0
//						ELSE
//							iCutsceneToPlay = 1
						ENDIF
//					ELSE
//						iCutsceneToPlay = 2
					ENDIF
				ENDIF
				IF iCutsceneToPlay = 0
					endDropOffScene2DrunksVehicle()
				ELIF iCutsceneToPlay = 1
					endDropOffScene2OwnVehicle()
				ELIF iCutsceneToPlay = 2
					endDropOffScene2OnFoot()
				ENDIF
				
				IF bDropOffDone
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					MISSION_PASSED()
				ENDIF
			ENDIF
		BREAK
		
		CASE eS_AT_CULT
			//WAIT FOR ALTRUIST CULT TO DO ITS THING
		BREAK
	ENDSWITCH
ENDPROC

// ---------------------------------------------- // // ---------------------------------------------- // // ---------------------------------------------- //


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTNL()
PRINTSTRING("re_drunkdriver launched")
PRINTVECTOR(vInput)
PRINTNL()

// work out which variation is being launched
getWorldPoint()

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	missionCleanup()
ENDIF

//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_DRUNKDRIVER, iSelectedVariation)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

time_of_last_frame = GET_GAME_TIMER()

// Mission Loop -------------------------------------------//
WHILE TRUE

WAIT(0)
	
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR bDrivingHome
//		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
		
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_DD")
			
			SWITCH ambStage
			
				CASE ambCanRun
					IF bAssetsLoaded
						bDoFullCleanUp = TRUE
						ambStage = (ambRunning)
					ELSE
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						IF NOT bVariablesInitialised
							initialiseEventVariables()
						ENDIF
						IF bVariablesInitialised
							loadAssets()
						ENDIF
					ENDIF
				BREAK
				
				CASE ambRunning
					createScene()
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
						GIVE_PED_CORRECT_DRUNK_ANIMS()
						
						IF thisEvent = eV_DRUNK_DRIVER AND NOT DOES_ENTITY_EXIST(pedDrunk[1])
						
							drunkDriverSequence()
							
//							IF HAS_PLAYER_AGGROED_PED(pedDrunk[0], aggroReason, lockOnTimer, iBitFieldDontCheck, bAggroed, 80)
							IF IS_PLAYER_INTERFERING_WITH_EVENT()
							OR IS_ENTITY_DEAD(vehDrunksCar)
//								bAggroed = TRUE  
								IF DOES_ENTITY_EXIST(pedDrunk[0])
									IF NOT IS_ENTITY_DEAD(pedDrunk[0])
										IF IS_PED_IN_GROUP(pedDrunk[0])
											REMOVE_PED_FROM_GROUP(pedDrunk[0])
										ENDIF
										CLEAR_PED_TASKS(pedDrunk[0])
										TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
										Make_Ped_Drunk(pedDrunk[0], 120000)
										SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_CRAZY", CONV_PRIORITY_AMBIENT_HIGH) //REDR1_AT
									ENDIF
								ENDIF
//								SWITCH aggroReason
//									CASE EAggro_Danger //player is wanted or shot a victim near them
//										IF NOT IS_PED_INJURED(pedDrunk[0])
//											CLEAR_PED_TASKS(pedDrunk[0])
//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
//											Make_Ped_Drunk(pedDrunk[0], 120000)
//										ENDIF
//									BREAK
//									CASE EAggro_ShotNear //shot near or near miss
//										IF NOT IS_PED_INJURED(pedDrunk[0])
//											CLEAR_PED_TASKS(pedDrunk[0])
//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
//											Make_Ped_Drunk(pedDrunk[0], 120000)
//										ENDIF
//									BREAK
//									CASE EAggro_HostileOrEnemy //stick up
//										IF NOT IS_PED_INJURED(pedDrunk[0])
//											CLEAR_PED_TASKS(pedDrunk[0])
//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
//											Make_Ped_Drunk(pedDrunk[0], 120000)
//										ENDIF
//									BREAK
//									CASE EAggro_Attacked //attacked
//										IF NOT IS_PED_INJURED(pedDrunk[0])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[0], FALSE)
//											CLEAR_PED_TASKS(pedDrunk[0])
//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
//											Make_Ped_Drunk(pedDrunk[0], 120000)
//										ENDIF
//									BREAK
//									CASE EAggro_HeardShot
//										IF NOT IS_PED_INJURED(pedDrunk[0])
//											CLEAR_PED_TASKS(pedDrunk[0])
//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
//											Make_Ped_Drunk(pedDrunk[0], 120000)
//										ENDIF
//									BREAK
//								ENDSWITCH
								MISSION_FAILED()
							ENDIF
						ENDIF
						
						IF thisEvent = eV_WITH_PASSENGER
						OR (thisEvent = eV_DRUNK_DRIVER AND DOES_ENTITY_EXIST(pedDrunk[1]))
							
							drunkDriverSequence()
							
//							IF HAS_PLAYER_AGGROED_PED(pedDrunk[0], aggroReason,lockOnTimer, iBitFieldDontCheck, bAggroed)
//							OR HAS_PLAYER_AGGROED_PED(pedDrunk[1], aggroReason,lockOnTimer, iBitFieldDontCheck, bAggroed)
							IF thisStage != eS_AT_CULT
								IF IS_PLAYER_INTERFERING_WITH_EVENT()
								OR IS_ENTITY_DEAD(vehDrunksCar)
	//								bAggroed = TRUE  
									IF DOES_ENTITY_EXIST(pedDrunk[0])
										IF NOT IS_PED_INJURED(pedDrunk[0])
											IF IS_PED_IN_GROUP(pedDrunk[0])
												REMOVE_PED_FROM_GROUP(pedDrunk[0])
											ENDIF
											CLEAR_PED_TASKS(pedDrunk[0])
											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
											Make_Ped_Drunk(pedDrunk[0], 120000)
											SET_PED_KEEP_TASK(pedDrunk[0], TRUE)
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(pedDrunk[1])
										IF NOT IS_PED_INJURED(pedDrunk[1])
											IF IS_PED_IN_GROUP(pedDrunk[1])
												REMOVE_PED_FROM_GROUP(pedDrunk[1])
											ENDIF
											CLEAR_PED_TASKS(pedDrunk[1])
											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
											Make_Ped_Drunk(pedDrunk[1], 120000)
											SET_PED_KEEP_TASK(pedDrunk[1], TRUE)
										ENDIF
									ENDIF
									IF thisEvent = eV_DRUNK_DRIVER
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR1AU", "REDR1_CRAZY", CONV_PRIORITY_AMBIENT_HIGH) //REDR1_AT
									ENDIF
									IF thisEvent = eV_WITH_PASSENGER
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										CREATE_CONVERSATION(DrunkDriverDialogueStruct, "REDR2AU", "REDR2_AT", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
	//								SWITCH aggroReason
	//									CASE EAggro_Danger //player is wanted or shot a victim near them
	//										IF NOT IS_PED_INJURED(pedDrunk[0])
	//											CLEAR_PED_TASKS(pedDrunk[0])
	//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[0], 120000)
	//										ENDIF
	//										IF NOT IS_PED_INJURED(pedDrunk[1])
	//											CLEAR_PED_TASKS(pedDrunk[1])
	//											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[1], 120000)
	//										ENDIF
	//									BREAK
	//									CASE EAggro_ShotNear //shot near or near miss
	//										IF NOT IS_PED_INJURED(pedDrunk[0])
	//											CLEAR_PED_TASKS(pedDrunk[0])
	//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[0], 120000)
	//										ENDIF
	//										IF NOT IS_PED_INJURED(pedDrunk[1])
	//											CLEAR_PED_TASKS(pedDrunk[1])
	//											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[1], 120000)
	//										ENDIF
	//									BREAK
	//									CASE EAggro_HostileOrEnemy //stick up
	//										IF NOT IS_PED_INJURED(pedDrunk[0])
	//											CLEAR_PED_TASKS(pedDrunk[0])
	//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[0], 120000)
	//										ENDIF
	//										IF NOT IS_PED_INJURED(pedDrunk[1])
	//											CLEAR_PED_TASKS(pedDrunk[1])
	//											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[1], 120000)
	//										ENDIF
	//									BREAK
	//									CASE EAggro_Attacked //attacked
	//										IF NOT IS_PED_INJURED(pedDrunk[0])
	//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDrunk[0], FALSE)
	//											CLEAR_PED_TASKS(pedDrunk[0])
	//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[0], 120000)
	//										ENDIF
	//										IF NOT IS_PED_INJURED(pedDrunk[1])
	//											CLEAR_PED_TASKS(pedDrunk[1])
	//											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[1], 120000)
	//										ENDIF
	//									BREAK
	//									CASE EAggro_HeardShot
	//										PRINTSTRING("@@@@@@@@@@@@@@@@@ EAggro_HeardShot @@@@@@@@@@@@@@@@@")PRINTNL()
	//										IF NOT IS_PED_INJURED(pedDrunk[0])
	//											CLEAR_PED_TASKS(pedDrunk[0])
	//											TASK_SMART_FLEE_PED(pedDrunk[0], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[0], 120000)
	//										ENDIF
	//										IF NOT IS_PED_INJURED(pedDrunk[1])
	//											CLEAR_PED_TASKS(pedDrunk[1])
	//											TASK_SMART_FLEE_PED(pedDrunk[1], PLAYER_PED_ID(), 250, -1)
	//											Make_Ped_Drunk(pedDrunk[1], 120000)
	//										ENDIF
	//									BREAK
	//								ENDSWITCH
									MISSION_FAILED()
								ENDIF
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(pedDrunk[0])
							IF NOT IS_PED_INJURED(pedDrunk[0])
								SET_PED_MAX_MOVE_BLEND_RATIO(pedDrunk[0], PEDMOVEBLENDRATIO_WALK)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(pedDrunk[1])
							IF NOT IS_PED_INJURED(pedDrunk[1])
								SET_PED_MAX_MOVE_BLEND_RATIO(pedDrunk[1], PEDMOVEBLENDRATIO_WALK)
							ENDIF
						ENDIF
					ENDIF
					
					IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
					AND NOT IS_CULT_FINISHED()
					AND thisStage != eS_AT_CULT
						IF NOT IS_PED_INJURED(pedDrunk[0])
							IF IS_ENTITY_AT_COORD(pedDrunk[0], vCult, << 5, 5, 5 >>)
								IF thisEvent = eV_WITH_PASSENGER
									IF NOT IS_PED_INJURED(pedDrunk[1])
										IF NOT IS_PED_IN_GROUP(pedDrunk[0])
										AND NOT IS_PED_IN_GROUP(pedDrunk[1])
											SET_PED_AS_GROUP_MEMBER(pedDrunk[0], PLAYER_GROUP_ID())
											SET_PED_AS_GROUP_MEMBER(pedDrunk[1], PLAYER_GROUP_ID())
										ENDIF
									ENDIF
								ENDIF
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								WAIT(0)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								thisStage = eS_AT_CULT
							ENDIF
						ENDIF
					ENDIF
					IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
						IF DOES_ENTITY_EXIST(pedDrunk[0])
							DELETE_PED(pedDrunk[0])
						ENDIF
						IF DOES_ENTITY_EXIST(pedDrunk[1])
							DELETE_PED(pedDrunk[1])
						ENDIF
						MISSION_PASSED()
					ENDIF
					
					IF ARE_ALL_PEDS_NO_LONGER_NEEDED()
					OR ARE_ALL_PEDS_INJURED()
					OR IS_DRUNKS_VEHICLE_FUCKED()
						MISSION_FAILED()
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF iInterventionStage = 5
						AND thisStage = eS_PLAYER_INTERVENTION
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
								IF thisEvent = eV_DRUNK_DRIVER
									IF NOT IS_ENTITY_DEAD(pedDrunk[0])
										SET_PED_COORDS_KEEP_VEHICLE(pedDrunk[0], << 167.1268, -131.7064, 57.9895 >>)
										SET_ENTITY_HEADING(pedDrunk[0], 336.8074)
									ENDIF
									SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 167.1268, -131.7064, 57.9895 >>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 336.8074)
								ENDIF
								IF thisEvent = eV_WITH_PASSENGER
									IF NOT IS_ENTITY_DEAD(pedDrunk[0])
										SET_PED_COORDS_KEEP_VEHICLE(pedDrunk[0], vDropOffPoint + << 0, 5, 0 >>)
										SET_ENTITY_HEADING(pedDrunk[0], 180.8009)
									ENDIF
									IF NOT IS_ENTITY_DEAD(pedDrunk[1])
										SET_PED_COORDS_KEEP_VEHICLE(pedDrunk[1], vDropOffPoint + << 0, 5, 0 >>)
										SET_ENTITY_HEADING(pedDrunk[1], 180.8009)
									ENDIF
									SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vDropOffPoint + << 0, 5, 0 >>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 180.8009)
								ENDIF
							ENDIF
						ENDIF
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_L)
							PRINTSTRING("\n@@@@@@@@@@@@@@@@@ STOP_SOUND(SoundId) @@@@@@@@@@@@@@@@@\n")
							STOP_SOUND(SoundId)
						ENDIF
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
							MISSION_PASSED()
						ENDIF
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
							MISSION_FAILED()
						ENDIF
					#ENDIF
					
				BREAK
			
			ENDSWITCH
//		ELSE
//			missionCleanup()
//		ENDIF
	ELSE
		missionCleanup()
	ENDIF
	
	// for driving check in Var. 1
	GET_LAST_FRAME_VELOCITY()
	
ENDWHILE


ENDSCRIPT
////			IF NOT IS_PED_INJURED(pedDrunk[0])
////				IF GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
////				AND GET_SCRIPT_TASK_STATUS(pedDrunk[0], SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
////					continualTurnToFacePlayer(pedDrunk[0], iTurnStage) 
////				ENDIF
////			ENDIF
//			IF thisEvent = eV_WITH_PASSENGER
////				IF NOT IS_PED_INJURED(pedDrunk[1])
////					IF GET_SCRIPT_TASK_STATUS(pedDrunk[1], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
////					AND GET_SCRIPT_TASK_STATUS(pedDrunk[1], SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
////						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDrunk[1], <<7.5, 7.5, 5>>, FALSE, FALSE, TM_ON_FOOT)
////							continualTurnToFacePlayer(pedDrunk[1], iTurnStage) 
////						ENDIF
////					ENDIF
////				ENDIF
//			ENDIF
