// ArmyBase_AirControl_lib.sch

CONST_INT MAX_NUMBER_MISSILE_GUYS 3
CONST_INT MAX_NUMBER_JEEP_ATTACKERS 3

VEHICLE_INDEX vehAttackPlane
VEHICLE_INDEX vehPlayerPlane
VEHICLE_INDEX vehJeepAttacker[MAX_NUMBER_JEEP_ATTACKERS]

PED_INDEX pedAttackPlanePilot
PED_INDEX pedAnnouncer
PED_INDEX pedMissileGuys[MAX_NUMBER_MISSILE_GUYS]
PED_INDEX pedJeepAttacker[MAX_NUMBER_JEEP_ATTACKERS]

//BLIP_INDEX blipJeepAttackers[MAX_NUMBER_JEEP_ATTACKERS]

INT iAttackStages = 0
INT iLineToPlay = 0
INT iJeepAttackerStages = 0

VECTOR vSpawnPos[4]
VECTOR vJeepSpawnPosition[MAX_NUMBER_JEEP_ATTACKERS]
VECTOR vGoToCoordinate[MAX_NUMBER_JEEP_ATTACKERS]

FLOAT fJeepSpawnHeading[MAX_NUMBER_JEEP_ATTACKERS]

BOOL bPlayerInAirVehicle
BOOL bPlayedAnnouncement = FALSE
BOOL bTurnOffAnnouncements = FALSE
BOOL bReleasePlane = FALSE
BOOL bCreatedPlane = FALSE

//STRING sWaypointPath[MAX_NUMBER_JEEP_ATTACKERS]

structTimer tAnnouncementTimer

ARMYBASE_SYSTEM_STATE	eAirControlState = eARMYBASE_SYSTEM_INIT

PROC SETUP_MISSILE_GUYS()
	INT idx

	REPEAT MAX_NUMBER_MISSILE_GUYS idx
		IF DOES_ENTITY_EXIST(pedMissileGuys[idx]) AND NOT IS_ENTITY_DEAD(pedMissileGuys[idx])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMissileGuys[idx], TRUE)
//			GIVE_WEAPON_TO_PED(pedMissileGuys[idx], WEAPONTYPE_RPG, 20, TRUE, TRUE)
			GIVE_WEAPON_TO_PED(pedMissileGuys[idx], WEAPONTYPE_STINGER, 20, TRUE, TRUE)
			SET_PED_ACCURACY(pedMissileGuys[idx], 100)
			TASK_COMBAT_PED(pedMissileGuys[idx], PLAYER_PED_ID())
			SET_PED_RELATIONSHIP_GROUP_HASH(pedMissileGuys[idx], RELGROUPHASH_AMBIENT_ARMY)
			SET_PED_KEEP_TASK(pedMissileGuys[idx], TRUE)
			PRINTLN("SETTING UP MISSILE GUYS")
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC INIT_SPAWN_VECTORS()
	vSpawnPos[0] = <<-2556.8127, 3738.8545, 200.0352>>
	vSpawnPos[1] = <<-2802.7627, 3115.1665, 200.2828>>
	vSpawnPos[2] = <<-1807.9321, 2576.9827, 200.2739>>
	vSpawnPos[3] = <<-1496.8744, 3139.7495, 200.6324>>
ENDPROC

FUNC VECTOR GET_SAFE_SPAWN_POSITION()
	INT idx
	VECTOR vReturn
	
	INIT_SPAWN_VECTORS()
	
	REPEAT 4 idx
		IF NOT IS_SPHERE_VISIBLE(vSpawnPos[idx], 15.0)
			vReturn = vSpawnPos[idx]
			PRINTLN("vReturn = ", vReturn)
		ELSE	
			vReturn = <<0,0,0>>
			PRINTLN("VECTOR: ", idx, " IS VISIBLE")
		ENDIF
	ENDREPEAT
	
	RETURN vReturn
ENDFUNC

PROC HANDLE_NO_FLY_ZONE_ANNOUNCEMENT()
	VEHICLE_INDEX vehPlayersVehicle

	iLineToPlay = GET_RANDOM_INT_IN_RANGE() % 4
//	PRINTLN("iLineToPlay = ", iLineToPlay)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehPlayersVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
	
		IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID()) 
		OR (NOT IS_ENTITY_DEAD(vehPlayersVehicle) AND NOT IS_ENTITY_IN_AIR(vehPlayersVehicle))
		OR bTurnOffAnnouncements
//			PRINTLN("EXITING EARLY")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT bPlayedAnnouncement
		IF iLineToPlay = 0
			IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_ANN1", CONV_PRIORITY_VERY_HIGH)
				IF NOT IS_TIMER_STARTED(tAnnouncementTimer)
					START_TIMER_NOW(tAnnouncementTimer)
					PRINTLN("STARTIGN TIMER - tAnnouncementTimer 01")
				ENDIF
				
				PRINTLN("PLAYING NO FLY ZONE ANNOUNCEMENT - 1")
				bPlayedAnnouncement = TRUE
			ENDIF
		ELIF iLineToPlay = 1
			IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_ANN2", CONV_PRIORITY_VERY_HIGH)
				IF NOT IS_TIMER_STARTED(tAnnouncementTimer)
					START_TIMER_NOW(tAnnouncementTimer)
					PRINTLN("STARTIGN TIMER - tAnnouncementTimer 02")
				ENDIF
				
				PRINTLN("PLAYING NO FLY ZONE ANNOUNCEMENT - 2")
				bPlayedAnnouncement = TRUE
			ENDIF
		ELIF iLineToPlay = 2
			IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_ANN3", CONV_PRIORITY_VERY_HIGH)
				IF NOT IS_TIMER_STARTED(tAnnouncementTimer)
					START_TIMER_NOW(tAnnouncementTimer)
					PRINTLN("STARTIGN TIMER - tAnnouncementTimer 03")
				ENDIF
				
				PRINTLN("PLAYING NO FLY ZONE ANNOUNCEMENT - 3")
				bPlayedAnnouncement = TRUE
			ENDIF
		ELIF iLineToPlay = 3
			IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_ANN4", CONV_PRIORITY_VERY_HIGH)
				IF NOT IS_TIMER_STARTED(tAnnouncementTimer)
					START_TIMER_NOW(tAnnouncementTimer)
					PRINTLN("STARTIGN TIMER - tAnnouncementTimer 04")
				ENDIF
				
				PRINTLN("PLAYING NO FLY ZONE ANNOUNCEMENT - 4")
				bPlayedAnnouncement = TRUE
			ENDIF
		ENDIF 
	ENDIF
	
	IF bPlayedAnnouncement
		IF IS_TIMER_STARTED(tAnnouncementTimer)
			IF GET_TIMER_IN_SECONDS(tAnnouncementTimer) > 25
			
				bPlayedAnnouncement = FALSE
				PRINTLN("bPlayedAnnouncement = FALSE")
				RESTART_TIMER_NOW(tAnnouncementTimer)
				PRINTLN("RESTART TIMER - tAnnouncementTimer")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INIT_VEHICLE_SPAWN_POSITIONS()
	vJeepSpawnPosition[0] = <<-2157.9995, 3254.8635, 31.8106>>
	vJeepSpawnPosition[1] = <<-2138.5723, 3253.4773, 31.8106>> 
	vJeepSpawnPosition[2] = <<-2133.4712, 3243.6260, 31.8106>> 
	
	vGoToCoordinate[0] = <<-2226.6399, 3172.2124, 31.8103>>
	vGoToCoordinate[1] = <<-2156.0825, 3197.5574, 31.8106>>
	vGoToCoordinate[2] = <<-1899.3848, 3054.5457, 31.8102>>
	
	fJeepSpawnHeading[0] = 192.2879
	fJeepSpawnHeading[1] = 149.8821 
	fJeepSpawnHeading[2] = 158.0152
	
//	sWaypointPath[0] = ""
//	sWaypointPath[1] = ""
//	sWaypointPath[2] = ""
ENDPROC

PROC SETUP_JEEP_ATACKERS(PED_INDEX& pedToSet[], INT idx)
	
	IF DOES_ENTITY_EXIST(pedToSet[idx]) AND NOT IS_ENTITY_DEAD(pedToSet[idx])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedToSet[idx], TRUE)
//		GIVE_WEAPON_TO_PED(pedToSet[idx], WEAPONTYPE_RPG, 20, TRUE, TRUE)
		GIVE_WEAPON_TO_PED(pedToSet[idx], WEAPONTYPE_STINGER, 20, TRUE, TRUE)
		SET_PED_ACCURACY(pedToSet[idx], 100)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedToSet[idx], RELGROUPHASH_AMBIENT_ARMY)
		SET_PED_KEEP_TASK(pedToSet[idx], TRUE)
		PRINTLN("SETTING UP JEEP ATTACKERS GUYS")
	ENDIF
ENDPROC

PROC UPDATE_JEEP_ATTACKERS()
	SEQUENCE_INDEX taskSequence[MAX_NUMBER_JEEP_ATTACKERS]
	INT idx

	SWITCH iJeepAttackerStages
		CASE 0
			INIT_VEHICLE_SPAWN_POSITIONS()
		
			IF NOT IS_SPHERE_VISIBLE(<<-2144.5518, 3244.9990, 31.8106>>, 10.0)
				REPEAT MAX_NUMBER_JEEP_ATTACKERS idx
					vehJeepAttacker[idx] = CREATE_VEHICLE(CRUSADER, vJeepSpawnPosition[idx], fJeepSpawnHeading[idx])
					pedJeepAttacker[idx] = CREATE_PED_INSIDE_VEHICLE(vehJeepAttacker[idx], PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01)
//					blipJeepAttackers[idx] = ADD_BLIP_FOR_ENTITY(pedJeepAttacker[idx])
//					SET_BLIP_COLOUR(blipJeepAttackers[idx], BLIP_COLOUR_BLUE)
					SETUP_JEEP_ATACKERS(pedJeepAttacker, idx)
			//		TASK_FOLLOW_WAYPOINT_RECORDING(pedJeepAttacker[idx], sWaypointPath[idx])
					
					PRINTLN("CREATING AND TASKING JEEP ATTACKER: ", idx)
				ENDREPEAT
				
				PRINTLN("iJeepAttackerStages = 1")
				iJeepAttackerStages = 1
			ELSE
				PRINTLN("SPHERE IS VISIBLE")
			ENDIF
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 1
			
			REPEAT MAX_NUMBER_JEEP_ATTACKERS idx 
				IF DOES_ENTITY_EXIST(vehJeepAttacker[idx]) AND NOT IS_ENTITY_DEAD(vehJeepAttacker[idx])
					OPEN_SEQUENCE_TASK(taskSequence[idx])
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehJeepAttacker[idx], vGoToCoordinate[idx], 25.0, DRIVINGSTYLE_NORMAL, CRUSADER, DRIVINGMODE_AVOIDCARS, 5, -1)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, FIRING_TYPE_CONTINUOUS)
					CLOSE_SEQUENCE_TASK(taskSequence[idx])
					IF NOT IS_ENTITY_DEAD(pedJeepAttacker[idx])
						TASK_PERFORM_SEQUENCE(pedJeepAttacker[idx], taskSequence[idx])
						PRINTLN("TASKING JEEP ATTACKER: ", idx)
					ENDIF
					CLEAR_SEQUENCE_TASK(taskSequence[idx])
				ENDIF
			ENDREPEAT
			
			PRINTLN("iJeepAttackerStages = 2")
			iJeepAttackerStages = 2
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 2
			REPEAT MAX_NUMBER_JEEP_ATTACKERS idx 
				IF DOES_ENTITY_EXIST(pedJeepAttacker[idx]) AND NOT IS_ENTITY_DEAD(pedJeepAttacker[idx])
					IF GET_SCRIPT_TASK_STATUS(pedJeepAttacker[idx], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
						SET_PED_COMBAT_ATTRIBUTES(pedJeepAttacker[idx], CA_USE_VEHICLE, FALSE)
						PRINTLN("SETTING COMBAT ATTRIBUTE CA_USE_VEHICLE TO FALSE ON ATTACKER: ", idx)
					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("iJeepAttackerStages = 3")
			iJeepAttackerStages = 3
		BREAK
		//---------------------------------------------------------------------------------------------------------------
		CASE 3
			REPEAT MAX_NUMBER_JEEP_ATTACKERS idx 
				IF DOES_ENTITY_EXIST(pedJeepAttacker[idx]) AND NOT IS_ENTITY_DEAD(pedJeepAttacker[idx])
					IF IS_PED_SHOOTING(pedJeepAttacker[idx])
						bTurnOffAnnouncements = TRUE
						PRINTLN("bTurnOffAnnouncements = TRUE VIA UPDATE_JEEP_ATTACKERS")
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH

ENDPROC

PROC HANDLE_SWITCH_TO_ASSAULT_RIFLES(PED_INDEX& pedsToSwitch[])
	INT idx
	VEHICLE_INDEX vehTemp

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(vehTemp)
				IF IS_ENTITY_DEAD(vehTemp) OR (NOT IS_ENTITY_DEAD(vehTemp) AND NOT IS_ENTITY_IN_AIR(vehTemp))
//					PRINTLN("PLAYER IS NO LONGER IN THE AIR")
					
					REPEAT 3 idx 
						IF DOES_ENTITY_EXIST(pedsToSwitch[idx]) AND NOT IS_ENTITY_DEAD(pedsToSwitch[idx])
							GIVE_WEAPON_TO_PED(pedsToSwitch[idx], WEAPONTYPE_ASSAULTRIFLE, -1)
							SET_CURRENT_PED_WEAPON(pedsToSwitch[idx], WEAPONTYPE_ASSAULTRIFLE, TRUE)
//							PRINTLN("SETTING ATTACKERS WEAPONS TO ASSAULTRIFLE, INDEX: ", idx)
						ENDIF
					ENDREPEAT
				ELSE
//					PRINTLN("PLAYER IS IN THE AIR")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CHECK_FOR_PED_MISSILE_ATTACKERS()
	INT idx
	
	REPEAT MAX_NUMBER_MISSILE_GUYS idx
		IF DOES_ENTITY_EXIST(pedMissileGuys[idx]) AND NOT IS_ENTITY_DEAD(pedMissileGuys[idx])
			IF IS_PED_SHOOTING(pedMissileGuys[idx])
				bTurnOffAnnouncements = TRUE
				PRINTLN("bTurnOffAnnouncements = TRUE VIA PED MISSILE ATTACKERS")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_RELEASE_OF_ATTACK_PLANE()
	IF NOT bReleasePlane
		IF DOES_ENTITY_EXIST(vehAttackPlane) AND IS_ENTITY_DEAD(vehAttackPlane)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehAttackPlane)
			PRINTLN("SETTING ATTACK PLANE AS NO LONGER NEEDED, SINCE IT HAS BEEN DESTROYED")
			bReleasePlane = TRUE
		ENDIF
		IF DOES_ENTITY_EXIST(vehAttackPlane) AND NOT IS_ENTITY_DEAD(vehAttackPlane) AND NOT IS_ENTITY_DEAD(pedAttackPlanePilot)
		AND DOES_ENTITY_EXIST(vehPlayerPlane) AND NOT IS_ENTITY_DEAD(vehPlayerPlane)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				OR NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE, 350)
					CLEAR_PED_TASKS(pedAttackPlanePilot)
					TASK_PLANE_MISSION(pedAttackPlanePilot, vehAttackPlane, vehPlayerPlane, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 70.0, -1, 30.0, 500, 50)
					SET_PED_KEEP_TASK(pedAttackPlanePilot, TRUE)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehAttackPlane)
					PRINTLN("SETTING ATTACK PLANE AS NO LONGER NEEDED, SINCE THE PLAYER IS NO LONGER WANTED")
					bReleasePlane = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bReleasePlane
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				vehPlayerPlane = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(vehPlayerPlane) AND NOT IS_ENTITY_DEAD(vehPlayerPlane)
					IF IS_ENTITY_IN_AIR(vehPlayerPlane) AND IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE, 350)
						bCreatedPlane = FALSE
						PRINTLN("bCreatedPlane = FALSE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the entire AirControl system.
PROC ARMYBASE_UPDATE_AIRCONTROL()

	VECTOR vPlayerPosition, vPlaneSpawnPosition
	FLOAT fPlayerHeading
	FLOAT fTempHeading
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
//		PRINTLN("WE'RE ON A STORY MISSION, AIR CONTROL SHOULD NOT UDPATE")
		EXIT
	ELSE
//		PRINTLN("WE'RE OKAY TO UPDATE AIR CONTROL")
	ENDIF
	
	SWITCH eAirControlState
		CASE eARMYBASE_SYSTEM_INIT
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2836.268311,3392.662598,22.472023>>, <<-1793.299194,2846.216553,281.954468>>, 250.000000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1668.019531,3121.111572,20.719114>>, <<-2352.193359,3484.961426,286.506287>>, 250.000000)
					PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_REQUEST")
					eAirControlState = eARMYBASE_SYSTEM_REQUEST
				ENDIF
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			REQUEST_MODEL(LAZER)
			PRINTLN("REQUESTING MODEL - LAZER")
			REQUEST_MODEL(S_M_Y_MARINE_01)
			PRINTLN("REQUESTING MODEL - S_M_Y_MARINE_01")
			REQUEST_MODEL(CRUSADER)
			PRINTLN("REQUESTING CRUSADER")
			
			// Request assets here.
			eAirControlState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			IF HAS_MODEL_LOADED(LAZER)
			AND HAS_MODEL_LOADED(S_M_Y_MARINE_01)
			AND HAS_MODEL_LOADED(CRUSADER)
				// Stream here.
				PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_SPAWN")
				eAirControlState = eARMYBASE_SYSTEM_SPAWN
			ELSE	
				PRINTLN("WAITING ON LAZER AND S_M_Y_MARINE_01 TO LOAD IN ARMYBASE_UPDATE_AIRCONTROL")
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			
			// Spawn assets here.
			PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_UPDATE")
			eAirControlState = eARMYBASE_SYSTEM_UPDATE
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					vehPlayerPlane = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					
//					PRINTLN("CHECKIING HEIGHT: vPlayerPosition = ", vPlayerPosition)
//					PRINTLN("Z POSITION = ", vPlayerPosition.z)
					
					IF DOES_ENTITY_EXIST(vehPlayerPlane)
						IF IS_ENTITY_IN_AIR(vehPlayerPlane) OR (vPlayerPosition.z > 50.0)
						AND IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE, 350)
							bPlayerInAirVehicle = TRUE
//							PRINTLN("bPlayerInAirVehicle = TRUE")
						ELSE
//							PRINTLN("PLAYER IS NOT IN THE AIR")
						ENDIF
					ELSE
//						PRINTLN("VEHICLE DOES NOT EXIST")
					ENDIF
				ENDIF
			ENDIF
			
			IF bPlayerInAirVehicle
				SWITCH iAttackStages
					CASE 0
						pedAnnouncer = CREATE_PED(PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01, <<-2127.9822, 2824.6860, 50.4370>>, 34.9781)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAnnouncer, TRUE)
						
						pedMissileGuys[0] = CREATE_PED(PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01, <<-1993.8752, 3145.1665, 54.4011>>, 147.2257)
						PRINTLN("CREATING MISSILE GUY 1")
						pedMissileGuys[1] = CREATE_PED(PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01, <<-2176.6558, 3250.5015, 54.3807>>, 140.5236)
						PRINTLN("CREATING MISSILE GUY 2")
						pedMissileGuys[2] = CREATE_PED(PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01, <<-2002.6709, 2960.1484, 31.8103>>, 28.6649)
						PRINTLN("CREATING MISSILE GUY 3")
						
						SETUP_MISSILE_GUYS()
											
						IF DOES_ENTITY_EXIST(pedAnnouncer) AND NOT IS_ENTITY_DEAD(pedAnnouncer)
						
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, pedAnnouncer, "ARMY_ANNOUNCER")
							
							HANDLE_NO_FLY_ZONE_ANNOUNCEMENT()
						ENDIF
						
						PRINTLN("iAttackStages = 1")
						iAttackStages = 1
					BREAK
					//------------------------------------------------------------------------------------------------------
					CASE 1
						
						HANDLE_NO_FLY_ZONE_ANNOUNCEMENT()
						UPDATE_JEEP_ATTACKERS()
						HANDLE_SWITCH_TO_ASSAULT_RIFLES(pedMissileGuys)
						HANDLE_SWITCH_TO_ASSAULT_RIFLES(pedJeepAttacker)
						HANDLE_CHECK_FOR_PED_MISSILE_ATTACKERS()
						HANDLE_RELEASE_OF_ATTACK_PLANE()
						
						IF NOT bCreatedPlane
							IF NOT DOES_ENTITY_EXIST(vehAttackPlane)
								vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
								PRINTLN("vPlayerPosition = ", vPlayerPosition)
								fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
								PRINTLN("fPlayerHeading = ", fPlayerHeading)
								
	//							vPlaneSpawnPosition = GET_SAFE_SPAWN_POSITION()

								vPlaneSpawnPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerPosition, fPlayerHeading, <<0,-150,0>>)
								PRINTLN("vPlaneSpawnPosition = ", vPlaneSpawnPosition)
								fTempHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
								fTempHeading = fTempHeading
								PRINTLN("PLAYER'S HEADING = ", fTempHeading)
								
								IF NOT IS_VECTOR_ZERO(vPlaneSpawnPosition)
									IF NOT IS_SPHERE_VISIBLE(vPlaneSpawnPosition, 10.0)
										vehAttackPlane = CREATE_VEHICLE(LAZER, vPlaneSpawnPosition, GET_ENTITY_HEADING(PLAYER_PED_ID()))
										PRINTLN("CREATED VEHICLE - vehAttackPlane")
										
										fTempHeading = GET_ENTITY_HEADING(vehAttackPlane)
										PRINTLN("ATTACK PLANE'S HEADING = ", fTempHeading)
										
										SET_ENTITY_ROTATION(vehAttackPlane, << 0, 0, fPlayerHeading>>)
										PRINTLN("ATTACK PLANE'S HEADING AFTER ROTATION CALL = ", fTempHeading)
										
										CONTROL_LANDING_GEAR(vehAttackPlane, LGC_RETRACT_INSTANT)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehAttackPlane, FALSE)
										
		//								#IF IS_DEBUG_BUILD
		//									ADD_BLIP_FOR_ENTITY(vehAttackPlane)
		//								#ENDIF
										
										pedAttackPlanePilot = CREATE_PED_INSIDE_VEHICLE(vehAttackPlane, PEDTYPE_CIVFEMALE, S_M_Y_MARINE_01)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAttackPlanePilot, TRUE)
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											SET_VEHICLE_SHOOT_AT_TARGET(pedAttackPlanePilot, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
											SET_CURRENT_PED_VEHICLE_WEAPON(pedAttackPlanePilot, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
										ENDIF
										
										TASK_PLANE_MISSION(pedAttackPlanePilot, vehAttackPlane, vehPlayerPlane, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_ATTACK, 70.0, -1, 30.0, 500, 50)
										
										PRINTLN("bCreatedPlane = TRUE")
										bCreatedPlane = TRUE
									ENDIF
								ELSE
									PRINTLN("SPAWN VECTOR IS ZERO")
								ENDIF
							ELSE
//								PRINTLN("vehAttackPlane ALREADY EXISTS")
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			// General update.
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the AirControl system.
PROC ARMYBASE_CLEANUP_AIRCONTROL()
	INT idx
	
	IF DOES_ENTITY_EXIST(vehAttackPlane)
	
		IF NOT IS_ENTITY_DEAD(pedAttackPlanePilot) AND NOT IS_ENTITY_DEAD(vehAttackPlane)
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			CLEAR_PED_TASKS(pedAttackPlanePilot)
			TASK_PLANE_MISSION(pedAttackPlanePilot, vehAttackPlane, NULL, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 70.0, -1, 30.0, 500, 50)
			SET_PED_KEEP_TASK(pedAttackPlanePilot, TRUE)
		
			SET_PED_AS_NO_LONGER_NEEDED(pedAttackPlanePilot)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - pedAttackPlanePilot")
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehAttackPlane)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehAttackPlane)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - vehAttackPlane")
		ENDIF
	
//		IF IS_ENTITY_OCCLUDED(vehAttackPlane)
//			DELETE_VEHICLE(vehAttackPlane)
//			PRINTLN("DELETING VEHICLE - vehAttackPlane")
//			
//			IF DOES_ENTITY_EXIST(pedAttackPlanePilot)
//				DELETE_PED(pedAttackPlanePilot)
//				PRINTLN("DELETING PED - pedAttackPlanePilot")
//			ENDIF
//		ELSE	
//			IF NOT IS_ENTITY_DEAD(pedAttackPlanePilot) AND NOT IS_ENTITY_DEAD(vehAttackPlane) AND NOT IS_ENTITY_DEAD(vehPlayerPlane)
//				TASK_PLANE_MISSION(pedAttackPlanePilot, vehAttackPlane, vehPlayerPlane, PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 70.0, -1, 30.0, 500, 50)
//				SET_PED_KEEP_TASK(pedAttackPlanePilot, TRUE)
//				PRINTLN("TASKING PLANE TO FLEE BEFORE CLEANUP")
//			ENDIF
//			
//			SET_PED_AS_NO_LONGER_NEEDED(pedAttackPlanePilot)
//			PRINTLN("SETTING PED AS NO LONGER NEEDED - pedAttackPlanePilot")
//			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehAttackPlane)
//			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - vehAttackPlane")
//		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(pedAnnouncer)
		IF IS_ENTITY_OCCLUDED(pedAnnouncer)
			DELETE_PED(pedAnnouncer)
			PRINTLN("DELETING PED - pedAnnouncer")
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedAnnouncer)
			PRINTLN("SETTING PED AS NO LONGER NEEDED - pedAnnouncer")
		ENDIF
	ENDIF
	
	REPEAT MAX_NUMBER_MISSILE_GUYS idx
		IF DOES_ENTITY_EXIST(pedMissileGuys[idx])
			IF IS_ENTITY_OCCLUDED(pedMissileGuys[idx])
				DELETE_PED(pedMissileGuys[idx])
				PRINTLN("DELETING PED - pedMissileGuys: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedMissileGuys[idx])
				PRINTLN("SETTING PED pedMissileGuys AS NO LONGER NEEDED: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUMBER_JEEP_ATTACKERS idx 
		IF DOES_ENTITY_EXIST(pedJeepAttacker[idx])
			IF IS_ENTITY_OCCLUDED(pedJeepAttacker[idx])
				DELETE_PED(pedJeepAttacker[idx])
				PRINTLN("DELETING PED - pedJeepAttacker: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedJeepAttacker[idx])
				PRINTLN("SETTING PED pedJeepAttacker AS NO LONGER NEEDED: ", idx)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehJeepAttacker[idx])
			IF IS_ENTITY_OCCLUDED(vehJeepAttacker[idx])
				DELETE_VEHICLE(vehJeepAttacker[idx])
				PRINTLN("DELETING VEHICLE - vehJeepAttacker: ", idx)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehJeepAttacker[idx])
				PRINTLN("SETTING VEHICLE vehJeepAttacker AS NO LONGER NEEDED: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
