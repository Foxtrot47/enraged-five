// ArmyBase_Tanks_lib.sch

ARMYBASE_SYSTEM_STATE	eTanksState = eARMYBASE_SYSTEM_INIT

CONST_INT MAX_NUMBER_TANKS 2

VEHICLE_INDEX vehTank[MAX_NUMBER_TANKS]
PED_INDEX pedTankDriver[MAX_NUMBER_TANKS]
BLIP_INDEX blipTank[MAX_NUMBER_TANKS]

VECTOR vTankStartPos[MAX_NUMBER_TANKS]
INT iTankRecording[MAX_NUMBER_TANKS]

/// PURPOSE:
///    Handle the entire tanks system.
PROC ARMYBASE_UPDATE_TANKS()
	INT idx
	
	SWITCH eTanksState
		CASE eARMYBASE_SYSTEM_INIT
			iTankRecording[0] = 101
			iTankRecording[1] = 102
			
			vTankStartPos[0] = << -1971.6283, 3136.5908, 32.7399 >>
			vTankStartPos[1] = << -2389.4902, 3378.8816, 32.7600 >>
		
			eTanksState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			REQUEST_MODEL(RHINO)
			PRINTLN("REQUESTING MODEL - RHINO")
			REQUEST_MODEL(S_M_Y_MARINE_01)
			
			REQUEST_VEHICLE_RECORDING(101, "TankRoute")
			PRINTLN("REQUESTING TANK RECORDING - 101")
			REQUEST_VEHICLE_RECORDING(102, "TankRoute")
			PRINTLN("REQUESTING TANK RECORDING - 102")
			// Request assets here.
			eTanksState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			IF HAS_MODEL_LOADED(RHINO) AND HAS_MODEL_LOADED(S_M_Y_MARINE_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "TankRoute")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "TankRoute")
				// Stream here.
				eTanksState = eARMYBASE_SYSTEM_SPAWN
			ELSE
				PRINTLN("WAITING ON RHINO AND TANK RECORDINGS")
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			REPEAT MAX_NUMBER_TANKS idx
				vehTank[idx] = CREATE_VEHICLE(RHINO, vTankStartPos[idx])
				IF DOES_ENTITY_EXIST(vehTank[idx])
					pedTankDriver[idx] = CREATE_PED_INSIDE_VEHICLE(vehTank[idx], PEDTYPE_COP, S_M_Y_MARINE_01)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedTankDriver[idx], RELGROUPHASH_AMBIENT_ARMY)
					pedTankDriver[idx] = pedTankDriver[idx]
					
//					blipTank[idx] = ADD_BLIP_FOR_ENTITY(vehTank[idx])
//					SET_BLIP_COLOUR(blipTank[idx], BLIP_COLOUR_RED)
//					SET_BLIP_SCALE(blipTank[idx], 0.75)
					
					START_PLAYBACK_RECORDED_VEHICLE(vehTank[idx], iTankRecording[idx], "TankRoute")
				ENDIF
			ENDREPEAT
		
			// Spawn assets here.
			eTanksState = eARMYBASE_SYSTEM_UPDATE
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			REPEAT MAX_NUMBER_TANKS idx
				IF DOES_ENTITY_EXIST(vehTank[idx]) AND IS_VEHICLE_DRIVEABLE(vehTank[idx])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTank[idx])
						IF IS_ENTITY_OCCLUDED(vehTank[idx]) AND NOT IS_SPHERE_VISIBLE(vTankStartPos[idx], 50.0)
							START_PLAYBACK_RECORDED_VEHICLE(vehTank[idx], iTankRecording[idx], "TankRoute")
							PRINTLN("RESTARTING TANK LOOP!!!!!!!!!!!!!!!")
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			// General update.
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the tanks system.
PROC ARMYBASE_CLEANUP_TANKS()
	INT idx

	REPEAT MAX_NUMBER_TANKS idx
		IF DOES_ENTITY_EXIST(vehTank[idx])
			IF DOES_BLIP_EXIST(blipTank[idx])
				REMOVE_BLIP(blipTank[idx])
				PRINTLN("REMOVING blipTank: ", idx)
			ENDIF
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTank[idx])
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehTank", idx)
			SET_PED_AS_NO_LONGER_NEEDED(pedTankDriver[idx])
			PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedTankDriver: ", idx)
		ENDIF
	ENDREPEAT
	
	REMOVE_VEHICLE_RECORDING(101, "TankRoute")
	PRINTLN("REMOVING VEHICLE RECORDING - TankRoute, 101")
	REMOVE_VEHICLE_RECORDING(102, "TankRoute")
	PRINTLN("REMOVING VEHICLE RECORDING - TankRoute, 102")
ENDPROC
