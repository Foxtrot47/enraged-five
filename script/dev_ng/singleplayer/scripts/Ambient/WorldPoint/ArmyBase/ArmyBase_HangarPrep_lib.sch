// ArmyBase_HangarPrep_lib.sch

CONST_INT MAX_NUMBER_BARRACKS 4

VEHICLE_INDEX vehBarracks[MAX_NUMBER_BARRACKS]
BLIP_INDEX blipBarracks[MAX_NUMBER_BARRACKS]
PED_INDEX pedBarracksDriver[MAX_NUMBER_BARRACKS]

INT iRecordingSingle[MAX_NUMBER_BARRACKS]
VECTOR vStartingSphere[MAX_NUMBER_BARRACKS]

ARMYBASE_SYSTEM_STATE	eHangarState = eARMYBASE_SYSTEM_INIT

/// PURPOSE:
///    Handle the entire hangar system.
PROC ARMYBASE_UPDATE_HANGAR()
	INT idx
	
	SWITCH eHangarState
		CASE eARMYBASE_SYSTEM_INIT
			iRecordingSingle[0] = 101
			iRecordingSingle[1] = 102
			iRecordingSingle[2] = 103
			iRecordingSingle[3] = 104
			
			vStartingSphere[0] = << -1931.6818, 2957.9592, 32.4592 >>
			vStartingSphere[1] = << -2174.5208, 2881.5908, 32.4594 >>
			vStartingSphere[2] = << -1749.6158, 2971.7146, 32.4580 >>
			vStartingSphere[3] = << -2116.4775, 3323.1252, 32.4689 >>
			
			eHangarState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			REQUEST_MODEL(BARRACKS)
			PRINTLN("REQUESTING MODEL - BARRACKS")
			REQUEST_MODEL(S_M_Y_MARINE_02)
			PRINTLN("REQUESTING MODEL - S_M_Y_MARINE_02")
			
			REQUEST_VEHICLE_RECORDING(101, "SingleRoute")
			REQUEST_VEHICLE_RECORDING(102, "SingleRoute")
			REQUEST_VEHICLE_RECORDING(103, "SingleRoute")
			REQUEST_VEHICLE_RECORDING(104, "SingleRoute")
			
			// Request assets here.
			PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_STREAM")
			eHangarState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			IF HAS_MODEL_LOADED(BARRACKS) AND HAS_MODEL_LOADED(S_M_Y_MARINE_02)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "SingleRoute")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "SingleRoute")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "SingleRoute")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "SingleRoute")
				// Stream here.
				PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_SPAWN")
				eHangarState = eARMYBASE_SYSTEM_SPAWN
			ELSE
				PRINTLN("WAITING ON ARMYTANKER, OR S_M_Y_MARINE_02 TO LOAD")
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			vehBarracks[0] = CREATE_VEHICLE(BARRACKS, << -1931.6818, 2957.9592, 32.4592 >>, 0)
			SET_ENTITY_QUATERNION(vehBarracks[0], 0.0013, 0.0008, 0.5080, 0.8614)
			vehBarracks[1] = CREATE_VEHICLE(BARRACKS, << -2174.5208, 2881.5908, 32.4594 >>, 0)
			SET_ENTITY_QUATERNION(vehBarracks[1], 0.0013, 0.0012, 0.4992, 0.8665)
			vehBarracks[2] = CREATE_VEHICLE(BARRACKS, << -1749.6158, 2971.7146, 32.4580 >>, 0)
			SET_ENTITY_QUATERNION(vehBarracks[2],0.0011, 0.0015, 0.9639, 0.2662)
			vehBarracks[3] = CREATE_VEHICLE(BARRACKS, << -2116.4775, 3323.1252, 32.4689 >>, 0)
			SET_ENTITY_QUATERNION(vehBarracks[3], -0.0010, 0.0023, 0.5061, 0.8624)
			
			REPEAT MAX_NUMBER_BARRACKS idx
				IF DOES_ENTITY_EXIST(vehBarracks[idx]) AND IS_VEHICLE_DRIVEABLE(vehBarracks[idx])
//					blipBarracks[idx] = ADD_BLIP_FOR_ENTITY(vehBarracks[idx])
//					SET_BLIP_COLOUR(blipBarracks[idx], BLIP_COLOUR_RED)
//					SET_BLIP_SCALE(blipBarracks[idx], 0.75)
					
					SET_ENTITY_LOD_DIST(vehBarracks[idx], LOD_DISTANCE)
					pedBarracksDriver[idx] = CREATE_PED_INSIDE_VEHICLE(vehBarracks[idx], PEDTYPE_COP, S_M_Y_MARINE_02)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBarracksDriver[idx], RELGROUPHASH_AMBIENT_ARMY)
					pedBarracksDriver[idx] = pedBarracksDriver[idx]
					
					START_PLAYBACK_RECORDED_VEHICLE(vehBarracks[idx], iRecordingSingle[idx], "SingleRoute")
				ENDIF
			ENDREPEAT
			
			SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
			
			// Spawn assets here.
			eHangarState = eARMYBASE_SYSTEM_UPDATE
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			REPEAT MAX_NUMBER_BARRACKS idx
				IF DOES_ENTITY_EXIST(vehBarracks[idx]) AND IS_VEHICLE_DRIVEABLE(vehBarracks[idx])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBarracks[idx])
						IF IS_ENTITY_OCCLUDED(vehBarracks[idx]) AND NOT IS_SPHERE_VISIBLE(vStartingSphere[idx], 100)
							START_PLAYBACK_RECORDED_VEHICLE(vehBarracks[idx], iRecordingSingle[idx], "SingleRoute")
							PRINTLN("RE-STARTING SINGLE ROUTE RECORDINGS")
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			// General update.
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the hangar system.
PROC ARMYBASE_CLEANUP_HANGAR()
	INT idx

	REPEAT MAX_NUMBER_BARRACKS idx
		IF DOES_ENTITY_EXIST(vehBarracks[idx])
			IF DOES_BLIP_EXIST(blipBarracks[idx])
				REMOVE_BLIP(blipBarracks[idx])
				PRINTLN("REMOVING blipBarracks: ", idx)
			ENDIF
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBarracks[idx])
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehBarracks: ", idx)
			SET_PED_AS_NO_LONGER_NEEDED(pedBarracksDriver[idx])
			PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedBarracksDriver: ", idx)
		ENDIF		
	ENDREPEAT
	
	REMOVE_VEHICLE_RECORDING(101, "SingleRoute")
	PRINTLN("REMOVING VEHICLE RECORDING: SingleRoute - 101")
	REMOVE_VEHICLE_RECORDING(102, "SingleRoute")
	PRINTLN("REMOVING VEHICLE RECORDING: SingleRoute - 102")
	REMOVE_VEHICLE_RECORDING(103, "SingleRoute")
	PRINTLN("REMOVING VEHICLE RECORDING: SingleRoute - 103")
	REMOVE_VEHICLE_RECORDING(104, "SingleRoute")
	PRINTLN("REMOVING VEHICLE RECORDING: SingleRoute - 104")
ENDPROC
