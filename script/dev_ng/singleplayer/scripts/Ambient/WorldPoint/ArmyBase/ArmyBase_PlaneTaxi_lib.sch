// ArmyBase_PlaneTaxi_lib.sch

ARMYBASE_SYSTEM_STATE	ePlaneTaxiState = eARMYBASE_SYSTEM_INIT
VEHICLE_INDEX vehCargoPlane
VEHICLE_INDEX vehLazer01
VEHICLE_INDEX vehLazer02
VEHICLE_INDEX vehLazer03
VEHICLE_INDEX vehLazer04
VEHICLE_INDEX vehCargoPlane02

PED_INDEX pedCargoPlaneDriver01
PED_INDEX pedCargoPlaneDriver02
PED_INDEX pedLazer01
PED_INDEX pedLazer02
PED_INDEX pedLazer03
PED_INDEX pedLazer04

BLIP_INDEX blipCargoPlane
BLIP_INDEX blipLazer01
BLIP_INDEX blipLazer02

FLOAT fRecordingPos, fLazer01Pos, fLazer02Pos, fFighterJet03Pos, fFighterJet04Pos
BOOL bResetLoop = FALSE
BOOL bModelsLoaded = FALSE

VECTOR vCargoPlaneStartingPos01
VECTOR vCargoPlaneStartingPos02

VECTOR vLaserStartingPos01
VECTOR vLaserStartingPos02
VECTOR vLaserStartingPos03
VECTOR vLaserStartingPos04

BOOL bAggroedGuy[6]

ENUM AIR_TAXI_STATE
	AIR_TAXI_STATE_01 = 0,
	AIR_TAXI_STATE_02,
	AIR_TAXI_STATE_03,
	AIR_TAXI_STATE_04,
	AIR_TAXI_STATE_05,
	AIR_TAXI_STATE_06,
	AIR_TAXI_STATE_07,
	AIR_TAXI_STATE_08,
	AIR_TAXI_STATE_09
ENDENUM

AIR_TAXI_STATE airTaxiStages = AIR_TAXI_STATE_01

PROC RESET_LOOP()
	PRINTLN("RESETING TAXI LOOP!!!!!!!!!!!!")
	bResetLoop = TRUE
								
	IF DOES_ENTITY_EXIST(vehLazer02) AND IS_VEHICLE_DRIVEABLE(vehLazer02)
		SET_ENTITY_COORDS(vehLazer01, vLaserStartingPos01)
		SET_ENTITY_HEADING(vehLazer01, 236.5296)
	ENDIF

	PRINTLN("GOING TO STATE - AIR_TAXI_STATE_01")
	airTaxiStages = AIR_TAXI_STATE_01
ENDPROC

PROC INIT_STARTING_POSITIONS()
	 
	vCargoPlaneStartingPos01 = << -2007.8911, 2858.8254, 33.6348 >> // Old Starting on Runway
	vCargoPlaneStartingPos02 = << -3829.9160, 3926.6685, 308.8804 >>  // New Cargo 01
	
	vLaserStartingPos01 = << -2235.4163, 3272.0896, 31.3138 >> // Old Starting in Hanger
	vLaserStartingPos02 = << -3278.7710, 3601.2917, 67.2673 >> // Old Starting in Air
	vLaserStartingPos03 = << -1866.8884, 3078.4258, 31.3118 >> // New Lazer 01, starting near marching peds
	vLaserStartingPos04 = << -2288.8079, 3182.1699, 31.8118 >> // New Lazer 02, starting inside hanger

ENDPROC

PROC MONITOR_VEHICLE_RECORDING_VEHICLE(VEHICLE_INDEX& vehToCheck, PED_INDEX& pedPilot, BOOL& bAggroed[], INT iIndexToCheck, BOOL bTaskToFlee = FALSE)
	
//	SEQUENCE_INDEX iSeq
	armyBaseAggroArgs.iShotIRange = 25
	VECTOR vPlanePosition
	
	// If a recording is going on for a vehicle and the vehicle is undrivable, stop the recording.
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF NOT IS_VEHICLE_DRIVEABLE(vehToCheck) OR IS_ENTITY_DEAD(vehToCheck)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehToCheck)
//				PRINTLN("STOPPING RECORDING BECAUSE VEHICLE IS NOT DRIVEABLE")
				IF NOT IS_PED_INJURED(pedPilot)
					TASK_LEAVE_ANY_VEHICLE(pedPilot)
//					PRINTLN("TASKING PED TO LEAVE VEHICLE")
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehToCheck) AND IS_VEHICLE_DRIVEABLE(vehToCheck)
		vPlanePosition = GET_ENTITY_COORDS(vehToCheck)
//		PRINTLN("vPlanePosition = ", vPlanePosition)

		// Task plane to flee if the recording is over.
		IF bTaskToFlee
			IF NOT IS_ENTITY_DEAD(vehToCheck) AND NOT IS_PED_INJURED(pedPilot)
				IF IS_ENTITY_IN_AIR(vehToCheck)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
						IF GET_SCRIPT_TASK_STATUS(pedPilot, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_PLANE_MISSION(pedPilot, vehToCheck, NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 30.0, 100, 50)
	//							PRINTLN("TASKING PLANE/PILOT TO FLEE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bAggroed[iIndexToCheck]
		IF DO_AGGRO_CHECK(pedPilot, vehToCheck, armyBaseAggroArgs, eAggroReasons, TRUE)
		OR IS_BULLET_IN_AREA(vPlanePosition, 100.0, FALSE)
			PRINTLN("AGGRO CHECK HAS RETURNED TRUE")
		
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehToCheck)
				PRINTLN("STOPPING RECORDING DUE TO AGGRO CHECK RETURNING TRUE")
			ENDIF
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
			
			SET_PED_COMBAT_MOVEMENT(pedPilot, CM_WILLADVANCE) 
			SET_PED_COMBAT_ATTRIBUTES(pedPilot, CA_USE_VEHICLE, FALSE)
			SET_PED_TARGET_LOSS_RESPONSE(pedPilot, TLR_NEVER_LOSE_TARGET) 
			TASK_COMBAT_PED(pedPilot, PLAYER_PED_ID())
			
			IF NOT IS_ENTITY_DEAD(vehToCheck)
				// Unlock the doors to the plane, now that the pilot is outside the plane.
				SET_VEHICLE_DOORS_LOCKED(vehToCheck, VEHICLELOCK_UNLOCKED)
				PRINTLN("UNLOCKING DOORS ON VEHICLE")
			ENDIF
			
//			CLEAR_PED_TASKS_IMMEDIATELY(pedPilot)
//			OPEN_SEQUENCE_TASK(iSeq)
//				TASK_LEAVE_VEHICLE(NULL, vehToCheck)
//				TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
//			CLOSE_SEQUENCE_TASK(iSeq)
//			IF NOT IS_PED_INJURED(pedPilot)
//				TASK_PERFORM_SEQUENCE(pedPilot, iSeq)
//				PRINTLN("TASKING PILOT TO COMBAT")
//			ENDIF
//			CLEAR_SEQUENCE_TASK(iSeq)
			
			bAggroed[iIndexToCheck] = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_PILOT_PED(PED_INDEX& pedTocheck)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedTocheck, RELGROUPHASH_AMBIENT_ARMY)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTocheck, TRUE)
	GIVE_WEAPON_TO_PED(pedTocheck, WEAPONTYPE_PISTOL, -1, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedTocheck, TRUE)
ENDPROC

PROC ARMY_BASE_PLANE_AUTOPILOT(VEHICLE_INDEX Plane, FLOAT fSpeed, BOOL bRotInterp, FLOAT xRot = 0.0, FLOAT yRot = 0.0)

	// Error checking.
	IF IS_ENTITY_DEAD(Plane)
		DEBUG_MESSAGE("PLANE_AUTOPILOT: Plane not alive, exiting!")
		EXIT
	ENDIF
	
	// If Plane speed is below desired speed change it.
	IF (GET_ENTITY_SPEED(Plane) < fSpeed)
		SET_VEHICLE_FORWARD_SPEED(Plane, fSpeed)
	ENDIF
	
	// If Plane isn't level, rotate it (interp if desired).
	VECTOR vRot = GET_ENTITY_ROTATION(Plane)
	BOOL bPlaneRotated = FALSE
	IF bRotInterp
		FLOAT fRotInterp = GET_FRAME_TIME() * 45.0
		IF (vRot.x < -fRotInterp)
			bPlaneRotated = TRUE
			vRot.x += fRotInterp
		ELIF (vRot.x < xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ELIF (vRot.x > fRotInterp)
			bPlaneRotated = TRUE
			vRot.x -= fRotInterp
		ELIF (vRot.x > xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ENDIF
		IF (vRot.y < -fRotInterp)
			bPlaneRotated = TRUE
			vRot.y += fRotInterp
		ELIF (vRot.y < yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ELIF (vRot.y > fRotInterp)
			bPlaneRotated = TRUE
			vRot.y -= fRotInterp
		ELIF (vRot.y > yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ENDIF
	ELSE
		IF (vRot.x <> xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ENDIF
		IF (vRot.y <> yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ENDIF
	ENDIF
	IF bPlaneRotated
		SET_ENTITY_ROTATION(Plane, vRot)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handle the entire planetaxi system.
PROC ARMYBASE_UPDATE_PLANETAXI()
	SWITCH ePlaneTaxiState
		CASE eARMYBASE_SYSTEM_INIT
		
			INIT_STARTING_POSITIONS()		
		
			ePlaneTaxiState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			// Request assets here.
			REQUEST_MODEL(TITAN)
			PRINTLN("REQUESTING MODEL - TITAN")
			REQUEST_MODEL(LAZER)
			PRINTLN("REQUESTING MODEL - LAZER")
			REQUEST_MODEL(S_M_Y_MARINE_01)
		
			REQUEST_VEHICLE_RECORDING(101, "CargoTakeOff")
			PRINTLN("REQUESTING VEHICLE RECORDING")
			REQUEST_VEHICLE_RECORDING(101, "LazerTakeOff")
			REQUEST_VEHICLE_RECORDING(102, "LazerTakeOff")
			PRINTLN("REQUESTING VEHICLE RECORDING - LazerTakeOff")
			REQUEST_VEHICLE_RECORDING(101, "ArmyBaseNew")
			PRINTLN("REQUESTING VEHICLE RECORDING - ArmyBaseNew - 101")
			REQUEST_VEHICLE_RECORDING(103, "ArmyBaseNew")
			PRINTLN("REQUESTING VEHICLE RECORDING - ArmyBaseNew - 103")
			REQUEST_VEHICLE_RECORDING(104, "ArmyBaseNew")
			PRINTLN("REQUESTING VEHICLE RECORDING - ArmyBaseNew - 104")
			
			ePlaneTaxiState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			// Stream here.
			IF NOT bModelsLoaded
				IF HAS_MODEL_LOADED(TITAN) 
				AND HAS_MODEL_LOADED(LAZER) 
				AND HAS_MODEL_LOADED(S_M_Y_MARINE_01)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "CargoTakeOff") 
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "LazerTakeOff")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "LazerTakeOff")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "ArmyBaseNew")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "ArmyBaseNew")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "ArmyBaseNew")
					PRINTLN("bModelsLoaded = TRUE")
					bModelsLoaded = TRUE
				ELSE
					PRINTLN("WAITING FOR MODELS AND RECORDINGS TO LOAD")
				ENDIF
			ENDIF
			
			IF bModelsLoaded
				IF NOT IS_SPHERE_VISIBLE(vCargoPlaneStartingPos01, 50.0)
					PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_SPAWN")
					ePlaneTaxiState = eARMYBASE_SYSTEM_SPAWN
				ELSE
					PRINTLN("CARGO PLANE SPAWN LOCATTION IS VISIBLE")
				ENDIF
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			
			//  ----------------------------------CARGO PLANES----------------------------------
			
			// **************1ST CARGO PLANE**************
			vehCargoPlane = CREATE_VEHICLE(TITAN, vCargoPlaneStartingPos01, 56.4874)
			SET_VEHICLE_ENGINE_ON(vehCargoPlane, TRUE, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehCargoPlane, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			PRINTLN("CREATING CARGO PLANE AND TURNING ON ENGINE")
			SET_ENTITY_LOD_DIST(vehCargoPlane, LOD_DISTANCE)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehCargoPlane, TRUE)
		
			// Create Driver in 1st Cargo Plane
			IF DOES_ENTITY_EXIST(vehCargoPlane) AND IS_VEHICLE_DRIVEABLE(vehCargoPlane)
				pedCargoPlaneDriver01 = CREATE_PED_INSIDE_VEHICLE(vehCargoPlane, PEDTYPE_COP, S_M_Y_MARINE_01)
				#IF IS_DEBUG_BUILD
					SET_PED_NAME_DEBUG(pedCargoPlaneDriver01, "pedCargoPlaneDriver01")
				#ENDIF
				SETUP_PILOT_PED(pedCargoPlaneDriver01)
			ENDIF
			// **************2ND CARGO PLANE**************
			// created and starts in air
			
			
			// ----------------------------------FIGHTER JETS ----------------------------------
			
			// **************1ST FIGHTER JET**************
			CLEAR_AREA_OF_VEHICLES(vLaserStartingPos01, 5)
			vehLazer01 = CREATE_VEHICLE(LAZER, vLaserStartingPos01, 236.5296)
			SET_VEHICLE_DOORS_LOCKED(vehLazer01, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			
			IF DOES_ENTITY_EXIST(vehLazer01)
				pedLazer01 = CREATE_PED_INSIDE_VEHICLE(vehLazer01, PEDTYPE_COP, S_M_Y_MARINE_01)
				SETUP_PILOT_PED(pedLazer01)
				#IF IS_DEBUG_BUILD
					SET_PED_NAME_DEBUG(pedLazer01, "pedLazer01")
				#ENDIF
			ENDIF
			PRINTLN("CREATING LAZER01")
			SET_VEHICLE_ON_GROUND_PROPERLY(vehLazer01)
			SET_ENTITY_LOD_DIST(vehLazer01, LOD_DISTANCE)
			
			// **************2ND FIGHTER JET**************
			// created and starts in air
			
			// **************3RD FIGHTER JET**************
			CLEAR_AREA_OF_VEHICLES(vLaserStartingPos03, 5)
			vehLazer03 = CREATE_VEHICLE(LAZER, vLaserStartingPos03, 0)
			SET_VEHICLE_DOORS_LOCKED(vehLazer03, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_ENTITY_QUATERNION(vehLazer03, 0.0031, 0.0099, 0.9623, 0.2718)
			
			IF DOES_ENTITY_EXIST(vehLazer03)
				pedLazer03 = CREATE_PED_INSIDE_VEHICLE(vehLazer03, PEDTYPE_COP, S_M_Y_MARINE_01)
				SETUP_PILOT_PED(pedLazer03)
				#IF IS_DEBUG_BUILD
					SET_PED_NAME_DEBUG(pedLazer03, "pedLazer03")
				#ENDIF
			ENDIF
			PRINTLN("CREATING LAZER03")
			SET_VEHICLE_ON_GROUND_PROPERLY(vehLazer03)
			SET_ENTITY_LOD_DIST(vehLazer03, LOD_DISTANCE)
			
			// **************4TH FIGHTER JET**************
			CLEAR_AREA_OF_VEHICLES(vLaserStartingPos04, 5)
			vehLazer04 = CREATE_VEHICLE(LAZER, vLaserStartingPos04, 0)
			SET_VEHICLE_DOORS_LOCKED(vehLazer04, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			
			SET_ENTITY_QUATERNION(vehLazer04, -0.0043, 0.0092, 0.8718, -0.4897)
			IF DOES_ENTITY_EXIST(vehLazer04)
				pedLazer04 = CREATE_PED_INSIDE_VEHICLE(vehLazer04, PEDTYPE_COP, S_M_Y_MARINE_01)
				SETUP_PILOT_PED(pedLazer04)
				#IF IS_DEBUG_BUILD
					SET_PED_NAME_DEBUG(pedLazer04, "pedLazer04")
				#ENDIF
			ENDIF
			PRINTLN("CREATING LAZER04")
			SET_VEHICLE_ON_GROUND_PROPERLY(vehLazer04)
			SET_ENTITY_LOD_DIST(vehLazer04, LOD_DISTANCE)
		
			ePlaneTaxiState = eARMYBASE_SYSTEM_UPDATE
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehCargoPlane, pedCargoPlaneDriver01, bAggroedGuy, 0, TRUE)
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehCargoPlane02, pedCargoPlaneDriver02, bAggroedGuy, 1)
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehLazer01, pedLazer01, bAggroedGuy, 2, TRUE)
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehLazer02, pedLazer02, bAggroedGuy, 3)
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehLazer03, pedLazer03, bAggroedGuy, 4, TRUE)
			MONITOR_VEHICLE_RECORDING_VEHICLE(vehLazer04, pedLazer04, bAggroedGuy, 5, TRUE)
			
			SWITCH airTaxiStages
				CASE AIR_TAXI_STATE_01
					IF DOES_ENTITY_EXIST(vehCargoPlane) AND IS_VEHICLE_DRIVEABLE(vehCargoPlane)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCargoPlane)
							IF bResetLoop
								IF NOT IS_SPHERE_VISIBLE(vCargoPlaneStartingPos01, 10.0) AND NOT bAggroedGuy[0]
									IF NOT IS_PED_INJURED(pedCargoPlaneDriver01)
										IF IS_PED_IN_VEHICLE(pedCargoPlaneDriver01, vehCargoPlane)
											START_PLAYBACK_RECORDED_VEHICLE(vehCargoPlane, 101, "CargoTakeOff")
											SET_VEHICLE_ENGINE_ON(vehCargoPlane, TRUE, TRUE)
										
											PRINTLN("STARTING PLAYBACK ON CARGO PLANE - RESET")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT bAggroedGuy[0]
									IF NOT IS_PED_INJURED(pedCargoPlaneDriver01)
										IF IS_PED_IN_VEHICLE(pedCargoPlaneDriver01, vehCargoPlane)
											START_PLAYBACK_RECORDED_VEHICLE(vehCargoPlane, 101, "CargoTakeOff")
											SET_VEHICLE_ENGINE_ON(vehCargoPlane, TRUE, TRUE)
										
											PRINTLN("STARTING PLAYBACK ON CARGO PLANE")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_VEHICLE_DRIVEABLE(vehCargoPlane)
//							PRINTLN("vehCargoPlane IS NOT DRIVEABLE")
						ENDIF
					ENDIF
					
					// If the cargo plane has taken off, start the fighter jet sequence
					IF IS_VEHICLE_DRIVEABLE(vehCargoPlane)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCargoPlane)
							fRecordingPos = GET_POSITION_IN_RECORDING(vehCargoPlane)
	//						PRINTLN("fRecordingPos = ", fRecordingPos)
							IF fRecordingPos > 400.00
								PRINTLN("GOING TO STATE - AIR_TAXI_STATE_02")
								airTaxiStages = AIR_TAXI_STATE_02
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_02
					IF DOES_ENTITY_EXIST(vehLazer01) AND IS_VEHICLE_DRIVEABLE(vehLazer01)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer01)
							IF bResetLoop
								IF NOT IS_SPHERE_VISIBLE(vLaserStartingPos01, 10.0)
									IF NOT IS_PED_INJURED(pedLazer01)
										// Need to check that the other fighter jet nearby has not been aggroed, i.e. bAggroedGuy[5]
										IF IS_PED_IN_VEHICLE(pedLazer01, vehLazer01) AND NOT bAggroedGuy[5]
											START_PLAYBACK_RECORDED_VEHICLE(vehLazer01, 101, "LazerTakeOff")
											SET_VEHICLE_ENGINE_ON(vehLazer01, TRUE, TRUE)
											SET_PLAYBACK_SPEED(vehLazer01, 1.5)
											PRINTLN("STARTING PLAYBACK ON LAZER01 - RESET")
											PRINTLN("GOING TO STATE - AIR_TAXI_STATE_03")
											airTaxiStages = AIR_TAXI_STATE_03
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(pedLazer01)
									// Need to check that the other fighter jet nearby has not been aggroed, i.e. bAggroedGuy[5]
									IF IS_PED_IN_VEHICLE(pedLazer01, vehLazer01) AND NOT bAggroedGuy[5]
										START_PLAYBACK_RECORDED_VEHICLE(vehLazer01, 101, "LazerTakeOff")
										SET_VEHICLE_ENGINE_ON(vehLazer01, TRUE, TRUE)
										SET_PLAYBACK_SPEED(vehLazer01, 1.5)
										PRINTLN("STARTING PLAYBACK ON LAZER01")
										PRINTLN("GOING TO STATE - AIR_TAXI_STATE_03")
										airTaxiStages = AIR_TAXI_STATE_03
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
//						PRINTLN("vehLazer01 HAS PROBLEMS")
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_03
					IF DOES_ENTITY_EXIST(vehLazer01) AND IS_VEHICLE_DRIVEABLE(vehLazer01)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer01)
							fLazer01Pos = GET_POSITION_IN_RECORDING(vehLazer01)
//							PRINTLN("fLazer01Pos = ", fLazer01Pos)
							// Not the best check to perform, but need to check if the player has aggored nearby fighter jet, and if so
							// stop the recording, so it doesn't plow into the other jet, army guys.
							IF bAggroedGuy[5]
								STOP_PLAYBACK_RECORDED_VEHICLE(vehLazer01)
								PRINTLN("STOPPING PLAYBACK BECAUSE PLAYER HAS AGGROED NEARBY JET")
							ENDIF
						ENDIF
					ENDIF
				
					IF DOES_ENTITY_EXIST(vehLazer01) AND IS_VEHICLE_DRIVEABLE(vehLazer01)
						IF fLazer01Pos > 500.00
							PRINTLN("GOING TO STATE - AIR_TAXI_STATE_04")
							airTaxiStages = AIR_TAXI_STATE_04
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_04
					IF NOT DOES_ENTITY_EXIST(vehLazer02)
						vehLazer02 = CREATE_VEHICLE(LAZER, vLaserStartingPos02, 0)
						SET_VEHICLE_DOORS_LOCKED(vehLazer02, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						
						IF DOES_ENTITY_EXIST(vehLazer02)
							IF NOT DOES_ENTITY_EXIST(pedLazer02)
								pedLazer02 = CREATE_PED_INSIDE_VEHICLE(vehLazer02, PEDTYPE_COP, S_M_Y_MARINE_01)
								SETUP_PILOT_PED(pedLazer02)
								#IF IS_DEBUG_BUILD
									SET_PED_NAME_DEBUG(pedLazer02, "pedLazer02")
								#ENDIF
							ENDIF
						ENDIF
						
						SET_ENTITY_LOD_DIST(vehLazer02, LOD_DISTANCE)
						
						IF NOT IS_PED_INJURED(pedLazer02)
							IF IS_PED_IN_VEHICLE(pedLazer02, vehLazer02)
								START_PLAYBACK_RECORDED_VEHICLE(vehLazer02, 102, "LazerTakeOff")
								SET_VEHICLE_ENGINE_ON(vehLazer02, TRUE, TRUE)
								
								PRINTLN("GOING TO STATE - AIR_TAXI_STATE_05")
								airTaxiStages = AIR_TAXI_STATE_05
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_05
					IF DOES_ENTITY_EXIST(vehLazer02) AND IS_VEHICLE_DRIVEABLE(vehLazer02)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer02)
							fLazer02Pos = GET_POSITION_IN_RECORDING(vehLazer02)
//							PRINTLN("fLazer02Pos = ", fLazer02Pos)
							
							// Wait until the plane has at least landed... and if not visible go ahead and kill it and continue on with the loop.
							IF fLazer02Pos > 1200 AND IS_ENTITY_OCCLUDED(vehLazer02)
	
								IF DOES_ENTITY_EXIST(vehLazer02)
									DELETE_VEHICLE(vehLazer02)
									PRINTLN("DELETING - vehLazer02")
								ENDIF
								IF DOES_ENTITY_EXIST(pedLazer02)
									DELETE_PED(pedLazer02)
									PRINTLN("DELETING - pedLazer02")
								ENDIF
								
								PRINTLN("GOING TO STATE AIR_TAXI_STATE_06 DUE TO PLANE BEING OCCLUDED")
								airTaxiStages = AIR_TAXI_STATE_06
							ENDIF
						ELSE
							// The player must have been watching the plane the entire time... the plane is done with the recording, moving on.
							PRINTLN("GOING TO STATE AIR_TAXI_STATE_06 DUE TO LAZER RECORDING BEING DONE")
							airTaxiStages = AIR_TAXI_STATE_06
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_06
				
					// **************2ND CARGO PLANE**************
					// Big cargo jet created in the air, landing, and parking.
					IF NOT DOES_ENTITY_EXIST(vehCargoPlane02)
						vehCargoPlane02 = CREATE_VEHICLE(TITAN, vCargoPlaneStartingPos02, 0.0)
						SET_VEHICLE_DOORS_LOCKED(vehCargoPlane02, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						
						SET_ENTITY_QUATERNION(vehCargoPlane02, 0.0548, -0.0094, 0.8799, -0.4719)
						SET_VEHICLE_ENGINE_ON(vehCargoPlane02, TRUE, TRUE)
						PRINTLN("CREATING CARGO PLANE 02 AND TURNING ON ENGINE")
						SET_ENTITY_LOD_DIST(vehCargoPlane02, LOD_DISTANCE)
						// Create Driver in 2nd Cargo Plane
						IF DOES_ENTITY_EXIST(vehCargoPlane02) AND IS_VEHICLE_DRIVEABLE(vehCargoPlane02)
							pedCargoPlaneDriver02 = CREATE_PED_INSIDE_VEHICLE(vehCargoPlane02, PEDTYPE_COP, S_M_Y_MARINE_01)
							SETUP_PILOT_PED(pedCargoPlaneDriver02)
							#IF IS_DEBUG_BUILD
								SET_PED_NAME_DEBUG(pedCargoPlaneDriver02, "pedCargoPlaneDriver02")
							#ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(vehCargoPlane02) 
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCargoPlane02)
								IF NOT IS_PED_INJURED(pedCargoPlaneDriver02)
									IF IS_PED_IN_VEHICLE(pedCargoPlaneDriver02, vehCargoPlane02)
										START_PLAYBACK_RECORDED_VEHICLE(vehCargoPlane02, 104, "ArmyBaseNew")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("GOING TO STATE - AIR_TAXI_STATE_07")
						airTaxiStages = AIR_TAXI_STATE_07
					ELSE
						// If we haven't deleted the second cargo plane, make sure it's deleted before re-creating.
						IF IS_ENTITY_OCCLUDED(vehCargoPlane02)
							DELETE_VEHICLE(vehCargoPlane02)
							PRINTLN("DELETING vehCargoPlane02 BECAUSE WE NEED TO RESTART")
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_07
					// If the cargo plane 2 isn't running it's recording, start up the figher jet 3
					IF DOES_ENTITY_EXIST(vehCargoPlane02) AND IS_VEHICLE_DRIVEABLE(vehCargoPlane02)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCargoPlane02) 
						
							SET_VEHICLE_ENGINE_ON(vehCargoPlane02, FALSE, TRUE)
							
							// Small fighter jet in outer hanger, waiting until big cargo is done... then taking off.
							IF DOES_ENTITY_EXIST(vehLazer03) AND IS_VEHICLE_DRIVEABLE(vehLazer03)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer03)
									IF bResetLoop
										IF NOT IS_SPHERE_VISIBLE(vLaserStartingPos03, 10.0)
											IF NOT IS_PED_INJURED(pedLazer03)
												IF IS_PED_IN_VEHICLE(pedLazer03, vehLazer03)
													START_PLAYBACK_RECORDED_VEHICLE(vehLazer03, 101, "ArmyBaseNew")
													PRINTLN("STARING PLAYBACK ON LAZER 03 - RESET")
													PRINTLN("GOING TO STATE - AIR_TAXI_STATE_08")
													airTaxiStages = AIR_TAXI_STATE_08
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_PED_INJURED(pedLazer03)
											IF IS_PED_IN_VEHICLE(pedLazer03, vehLazer03)
												START_PLAYBACK_RECORDED_VEHICLE(vehLazer03, 101, "ArmyBaseNew")
												PRINTLN("STARING PLAYBACK ON LAZER 03")
												PRINTLN("GOING TO STATE - AIR_TAXI_STATE_08")
												airTaxiStages = AIR_TAXI_STATE_08
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_08

					IF DOES_ENTITY_EXIST(vehLazer03) AND IS_VEHICLE_DRIVEABLE(vehLazer03)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer03)
							fFighterJet03Pos = GET_POSITION_IN_RECORDING(vehLazer03)
//							PRINTLN("fFighterJet03Pos = ", fFighterJet03Pos)
						ENDIF
					ENDIF
					
					// If the fighter jet 3 is far enough along in it's recording, start up fighter jet 4
					IF fFighterJet03Pos > 1100.0
						IF DOES_ENTITY_EXIST(vehLazer04) AND IS_VEHICLE_DRIVEABLE(vehLazer04)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer04)
								IF bResetLoop
									IF NOT IS_SPHERE_VISIBLE(vLaserStartingPos04, 10.0)
										IF NOT IS_PED_INJURED(pedLazer04)
											IF IS_PED_IN_VEHICLE(pedLazer04, vehLazer04) AND NOT bAggroedGuy[5]
											AND NOT bAggroedGuy[2]
												START_PLAYBACK_RECORDED_VEHICLE(vehLazer04, 103, "ArmyBaseNew")
												PRINTLN("STARING PLAYBACK ON LAZER 04 - RESET")
												PRINTLN("GOING TO STATE - AIR_TAXI_STATE_09")
												airTaxiStages = AIR_TAXI_STATE_09
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(pedLazer04)
										IF IS_PED_IN_VEHICLE(pedLazer04, vehLazer04) AND NOT bAggroedGuy[5]
										AND NOT bAggroedGuy[2]
											START_PLAYBACK_RECORDED_VEHICLE(vehLazer04, 103, "ArmyBaseNew")
											PRINTLN("STARING PLAYBACK ON LAZER 04")
											PRINTLN("GOING TO STATE - AIR_TAXI_STATE_09")
											airTaxiStages = AIR_TAXI_STATE_09
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE AIR_TAXI_STATE_09

					IF DOES_ENTITY_EXIST(vehLazer04) AND IS_VEHICLE_DRIVEABLE(vehLazer04)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer04)
							fFighterJet04Pos = GET_POSITION_IN_RECORDING(vehLazer04)
//							PRINTLN("fFighterJet04Pos = ", fFighterJet04Pos)
						ENDIF
					ENDIF
					
					IF fFighterJet04Pos > 500.0
						RESET_LOOP()	
					ENDIF
				BREAK
			ENDSWITCH
			// General update.
		BREAK
	ENDSWITCH
ENDPROC

PROC SAFE_CLEANUP_VEHICLE(VEHICLE_INDEX& vehToCleanUp, BLIP_INDEX& blipToCleanUp)

	IF DOES_ENTITY_EXIST(vehToCleanUp)
		IF DOES_BLIP_EXIST(blipToCleanUp)
			REMOVE_BLIP(blipToCleanUp)
			PRINTLN("REMOVING blipToCleanUp")
		ENDIF
		IF IS_ENTITY_OCCLUDED(vehToCleanUp)
			DELETE_VEHICLE(vehToCleanUp)
			PRINTLN("DELETING VEHICLE")
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehToCleanUp)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED")
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_SWITCH_PLANE_TASKS(VEHICLE_INDEX& vehToCheck, PED_INDEX& pedToCheck)

	IF DOES_ENTITY_EXIST(vehToCheck) AND DOES_ENTITY_EXIST(pedToCheck)
	AND NOT IS_ENTITY_DEAD(vehToCheck) AND NOT IS_ENTITY_DEAD(pedToCheck)
		IF IS_ENTITY_IN_AIR(vehToCheck)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					TASK_PLANE_MISSION(pedToCheck, vehToCheck, NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 30.0, 100, 50)
					PRINTLN("SAFE_SWITCH_PLANE_TASKS BEING CALLED")
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans everything created/used in the planetaxi system.
PROC ARMYBASE_CLEANUP_PLANETAXI()
	
	IF DOES_ENTITY_EXIST(vehCargoPlane)
		IF DOES_BLIP_EXIST(blipCargoPlane)
			REMOVE_BLIP(blipCargoPlane)
			PRINTLN("REMOVING blipCargoPlane")
		ENDIF
		IF IS_ENTITY_OCCLUDED(vehCargoPlane)
			DELETE_VEHICLE(vehCargoPlane)
			PRINTLN("DELETING VEHICLE - vehCargoPlane")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehCargoPlane, pedCargoPlaneDriver01)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCargoPlane)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehCargoPlane")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehCargoPlane02)
		IF IS_ENTITY_OCCLUDED(vehCargoPlane02)
			DELETE_VEHICLE(vehCargoPlane02)
			PRINTLN("DELETING VEHICLE - vehCargoPlane02")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehCargoPlane02, pedCargoPlaneDriver02)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCargoPlane02)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehCargoPlane02")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehLazer01)
		IF DOES_BLIP_EXIST(blipLazer01)
			REMOVE_BLIP(blipLazer01)
			PRINTLN("REMOVING blipLazer01")
		ENDIF
		IF IS_ENTITY_OCCLUDED(vehLazer01)
			DELETE_VEHICLE(vehLazer01)
			PRINTLN("DELETING VEHICLE - vehLazer01")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehLazer01, pedLazer01)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLazer01)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehLazer01")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehLazer02)
		IF DOES_BLIP_EXIST(blipLazer02)
			REMOVE_BLIP(blipLazer02)
			PRINTLN("REMOVING blipLazer02")
		ENDIF
		IF IS_ENTITY_OCCLUDED(vehLazer02)
			DELETE_VEHICLE(vehLazer02)
			PRINTLN("DELETING VEHICLE - vehLazer02")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehLazer02, pedLazer02)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLazer02)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehLazer02")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehLazer03)
		IF IS_ENTITY_OCCLUDED(vehLazer03)
			DELETE_VEHICLE(vehLazer03)
			PRINTLN("DELETING VEHICLE - vehLazer03")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehLazer03, pedLazer03)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLazer03)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehLazer03")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehLazer04)
		IF IS_ENTITY_OCCLUDED(vehLazer04)
			DELETE_VEHICLE(vehLazer04)
			PRINTLN("DELETING VEHICLE - vehLazer04")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(vehLazer04, pedLazer04)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLazer04)
			PRINTLN("SET_VEHICLE_AS_NO_LONGER_NEEDED vehLazer04")
		ENDIF
	ENDIF
	
	REMOVE_VEHICLE_RECORDING(101, "CargoTakeOff")
	PRINTLN("REMOVING VEHICLE RECORDING - CargoTakeOff, 101")
	REMOVE_VEHICLE_RECORDING(101, "LazerTakeOff")
	PRINTLN("REMOVING VEHICLE RECORDING - CargoTakeOff, 102")
	REMOVE_VEHICLE_RECORDING(102, "LazerTakeOff")
	PRINTLN("REMOVING VEHICLE RECORDING - CargoTakeOff, 103")
	REMOVE_VEHICLE_RECORDING(101, "ArmyBaseNew")
	PRINTLN("REMOVING VEHICLE RECORDING - ArmyBaseNew, 101")
	REMOVE_VEHICLE_RECORDING(103, "ArmyBaseNew")
	PRINTLN("REMOVING VEHICLE RECORDING - ArmyBaseNew, 103")
	REMOVE_VEHICLE_RECORDING(104, "ArmyBaseNew")
	PRINTLN("REMOVING VEHICLE RECORDING - ArmyBaseNew, 104")
ENDPROC

