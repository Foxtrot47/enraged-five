// ArmyBase_Guards_lib.sch

ARMYBASE_SYSTEM_STATE	eGuardsState = eARMYBASE_SYSTEM_INIT

/// PURPOSE:
///    Handle the entire guards system.
PROC ARMYBASE_UPDATE_GUARDS()
	SWITCH eGuardsState
		CASE eARMYBASE_SYSTEM_INIT
			eGuardsState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			// Request assets here.
			eGuardsState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			// Stream here.
			eGuardsState = eARMYBASE_SYSTEM_SPAWN
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			// Spawn assets here.
			eGuardsState = eARMYBASE_SYSTEM_UPDATE
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			// General update.
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the planetaxi system.
PROC ARMYBASE_CLEANUP_GUARDS()
ENDPROC
