// ArmyBase_Accommodations_lib.sch

CONST_INT MAX_NUMBER_OF_ACCOM_MARCHERS 2
CONST_INT MAX_NUMBER_GATE_GUARDS 2

PED_INDEX pedLeadMarcher01
PED_INDEX pedFollowers01[MAX_NUMBER_OF_ACCOM_MARCHERS]
PED_INDEX pedGateGuards[MAX_NUMBER_GATE_GUARDS]
BLIP_INDEX blipMarchingGroup01

VECTOR vAccomStartPos = << -1744.2810, 3247.0291, 31.7987 >>

// Used for ped lead marcher talking to soldiers
VECTOR vAccomTalkingPos = << -1783.2952, 3179.1152, 31.8283 >>
VECTOR vAccomLookAtCoord = << -1780.8508, 3183.6882, 31.8261 >>
VECTOR vAccomFollowOffset[MAX_NUMBER_OF_ACCOM_MARCHERS]
VECTOR vAccomStartingPos[MAX_NUMBER_OF_ACCOM_MARCHERS]

VECTOR vEastGuardSpawnPos = <<-1591.9128, 2799.3704, 15.9362>> //<<-1593.2180, 2797.9155, 15.9459>>
FLOAT fEastGuardSpawnHeading = 218.6321 //221.5183
VECTOR vWestGuardSpawnPos = <<-2303.6736, 3384.3743, 30.0307>>
FLOAT fWestGuardSpawnHeading = 101.3716

BOOL bAccomTaskedLeader = FALSE
BOOL bAccomTaskedLeader01 = FALSE
BOOL bShouldReset = FALSE
BOOL bDoneWithGroup01 = FALSE
BOOL bPlayedWarning01 = FALSE
BOOL bPlayedWarning02 = FALSE
BOOL bResetAI = FALSE
BOOL bOutofArea = FALSE
BOOL bResetAI01 = FALSE
BOOL bOutofArea01 = FALSE
BOOL bClearedTasks01 = FALSE

INT iAccomMarchingState = 0
INT iGuardStagesEast = 0
INT iGuardStagesWest = 0

structTimer tWarningTimer

FLOAT fTime = 0
SEQUENCE_INDEX iSeq01, iSeq02, iSeq03

ARMYBASE_SYSTEM_STATE	eAccommodationsState = eARMYBASE_SYSTEM_INIT

PROC ACCOMMODATIONS_RESET_FOLLOWERS()
	INT idx

	REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
		IF DOES_ENTITY_EXIST(pedLeadMarcher01) AND DOES_ENTITY_EXIST(pedFollowers01[idx])
		AND NOT IS_PED_INJURED(pedLeadMarcher01) AND NOT IS_PED_INJURED(pedFollowers01[idx])
			SET_ENTITY_COORDS(pedFollowers01[idx], vAccomStartingPos[idx])
			TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedFollowers01[idx], pedLeadMarcher01, vAccomFollowOffset[idx], PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SPAWN_TASK_GROUP_01()
	INT idx
	
	SWITCH iAccomMarchingState
		CASE 0
			// Leader
			pedLeadMarcher01 = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vAccomStartPos, 140.0094)
//			blipMarchingGroup01 = ADD_BLIP_FOR_ENTITY(pedLeadMarcher01)
//			SET_BLIP_COLOUR(blipMarchingGroup01, BLIP_COLOUR_RED)
//			SET_BLIP_SCALE(blipMarchingGroup01, 0.75)
			
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLeadMarcher01, TRUE)
			GIVE_WEAPON_TO_PED(pedLeadMarcher01, WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
			SET_ENTITY_LOD_DIST(pedLeadMarcher01, LOD_DISTANCE)
			SET_PED_KEEP_TASK(pedLeadMarcher01, TRUE)
			TASK_FOLLOW_WAYPOINT_RECORDING(pedLeadMarcher01, "AccomMarching01")
			SET_PED_RELATIONSHIP_GROUP_HASH(pedLeadMarcher01, RELGROUPHASH_AMBIENT_ARMY)
			PRINTLN("CREATING ACCOMMODATIONS - LEAD MARCHER PED")
			iAccomMarchingState++
		BREAK
		//---------------------------------------------------------------------------------------------------------------------
		CASE 1
			REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
				pedFollowers01[idx] = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vAccomStartingPos[idx], 140.0094)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFollowers01[idx], TRUE)
				GIVE_WEAPON_TO_PED(pedFollowers01[idx], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
				SET_ENTITY_LOD_DIST(pedFollowers01[idx], LOD_DISTANCE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedFollowers01[idx], RELGROUPHASH_AMBIENT_ARMY)
				SET_PED_KEEP_TASK(pedFollowers01[idx], TRUE)
				SET_PED_STEERS_AROUND_PEDS(pedFollowers01[idx], FALSE)
				PRINTLN("CREATING ACCOMMODATION FOLLOWER PED: ", idx)
			ENDREPEAT
			
			iAccomMarchingState++
		BREAK
		//---------------------------------------------------------------------------------------------------------------------
		CASE 2
			REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
				IF DOES_ENTITY_EXIST(pedLeadMarcher01) AND DOES_ENTITY_EXIST(pedFollowers01[idx])
				AND NOT IS_PED_INJURED(pedLeadMarcher01) AND NOT IS_PED_INJURED(pedFollowers01[idx])
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedLeadMarcher01)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedFollowers01[idx], pedLeadMarcher01, vAccomFollowOffset[idx], PEDMOVEBLENDRATIO_WALK)
						PRINTLN("TASKING ACCOMMODATIONS FOLLOWER PED TO FOLLOW: ", idx)
					ENDIF
				ENDIF
			ENDREPEAT
			
			iAccomMarchingState++
		BREAK
		//---------------------------------------------------------------------------------------------------------------------
		CASE 3
			bDoneWithGroup01 = TRUE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SPAWN_GATE_GUARDS()
	
	REQUEST_MODEL(S_M_Y_MARINE_01)
	
	WHILE NOT HAS_MODEL_LOADED(S_M_Y_MARINE_01)
		PRINTLN("WAITING ON S_M_Y_MARINE_01 TO LOAD")
		WAIT(0)
	ENDWHILE
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_SPHERE_VISIBLE(vEastGuardSpawnPos, 10)
		OR GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vEastGuardSpawnPos) > 100
			IF NOT DOES_ENTITY_EXIST(pedGateGuards[0])
				pedGateGuards[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vEastGuardSpawnPos, fEastGuardSpawnHeading)
				CLEAR_AREA(vEastGuardSpawnPos, 10.0, TRUE)
				ADD_SCENARIO_BLOCKING_AREA((vEastGuardSpawnPos - <<10,10,10>>), (vEastGuardSpawnPos + <<10,10,10>>))
				
				GIVE_WEAPON_TO_PED(pedGateGuards[0], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
				SET_ENTITY_LOD_DIST(pedGateGuards[0], LOD_DISTANCE)
		//		SET_PED_KEEP_TASK(pedGateGuards[0], TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedGateGuards[0], RELGROUPHASH_AMBIENT_ARMY)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGateGuards[0], TRUE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, pedGateGuards[0], "ARMY_Guard01")
				
				TASK_START_SCENARIO_AT_POSITION(pedGateGuards[0], "WORLD_HUMAN_GUARD_STAND_ARMY", vEastGuardSpawnPos, fEastGuardSpawnHeading, -1)
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedGateGuards[0])
					TASK_LOOK_AT_ENTITY(pedGateGuards[0], PLAYER_PED_ID(), -1, SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
					PRINTLN("TASKING GUARD TO LOOK AT THE PLAYER")
				ENDIF
				
				PRINTLN("RETURNING TRUE ON SPAWNING GATE GUARDS - EAST SIDE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_SPHERE_VISIBLE(vWestGuardSpawnPos, 10)
		OR GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), vWestGuardSpawnPos) > 100
			IF NOT DOES_ENTITY_EXIST(pedGateGuards[1])
				pedGateGuards[1] = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vWestGuardSpawnPos, fWestGuardSpawnHeading)
				CLEAR_AREA(vWestGuardSpawnPos, 10.0, TRUE)
				ADD_SCENARIO_BLOCKING_AREA((vWestGuardSpawnPos - <<10,10,10>>), (vWestGuardSpawnPos + <<10,10,10>>))
				
				GIVE_WEAPON_TO_PED(pedGateGuards[1], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
				SET_ENTITY_LOD_DIST(pedGateGuards[1], LOD_DISTANCE)
		//		SET_PED_KEEP_TASK(pedGateGuards[1], TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedGateGuards[1], RELGROUPHASH_AMBIENT_ARMY)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGateGuards[1], TRUE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, pedGateGuards[1], "ARMY_Guard02")
				
				TASK_START_SCENARIO_AT_POSITION(pedGateGuards[1], "WORLD_HUMAN_GUARD_STAND_ARMY", vWestGuardSpawnPos, fWestGuardSpawnHeading, -1)
				PRINTLN("CREATING GATE GUARD 1")
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedGateGuards[1])
					TASK_LOOK_AT_ENTITY(pedGateGuards[1], PLAYER_PED_ID(), -1, SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
					PRINTLN("TASKING GUARD TO LOOK AT THE PLAYER")
				ENDIF
				
				PRINTLN("RETURNING TRUE ON SPAWNING GATE GUARDS - WEST SIDE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_GATE_GUARDS()

	// =====================================================East Side Guard=====================================================
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_ARMY)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
			
		IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
			TASK_COMBAT_PED(pedGateGuards[0], PLAYER_PED_ID())
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
			TASK_COMBAT_PED(pedGateGuards[1], PLAYER_PED_ID())
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
			IF NOT bResetAI AND bOutofArea
				IF GET_SCRIPT_TASK_STATUS(pedGateGuards[0], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					CLEAR_PED_TASKS(pedGateGuards[0])
			
					CLEAR_SEQUENCE_TASK(iSeq02)
					OPEN_SEQUENCE_TASK(iSeq02)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vEastGuardSpawnPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fEastGuardSpawnHeading)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND_ARMY", -1)
					CLOSE_SEQUENCE_TASK(iSeq02)
					IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
						TASK_PERFORM_SEQUENCE(pedGateGuards[0], iSeq02)
						PRINTLN("TASKING GATE GUARD - pedGateGuards[0] TO USE SCENARIO AT POSITION")
					ENDIF
					CLEAR_SEQUENCE_TASK(iSeq02)
				
					bResetAI = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
			IF NOT bResetAI01 AND bOutofArea01
				IF GET_SCRIPT_TASK_STATUS(pedGateGuards[1], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					CLEAR_PED_TASKS(pedGateGuards[1])
					
					CLEAR_SEQUENCE_TASK(iSeq03)
					OPEN_SEQUENCE_TASK(iSeq03)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWestGuardSpawnPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fWestGuardSpawnHeading)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND_ARMY", -1)
					CLOSE_SEQUENCE_TASK(iSeq03)
					IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
						TASK_PERFORM_SEQUENCE(pedGateGuards[1], iSeq03)
						PRINTLN("TASKING GATE GUARD - pedGateGuards[1] TO USE SCENARIO AT POSITION")
					ENDIF
					CLEAR_SEQUENCE_TASK(iSeq03)
					
					bResetAI01 = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF bPlayedWarning01
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1588.976563,2793.941162,14.982886>>, <<-1575.663574,2779.557861,26.115358>>, 29.750000)
		AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
			IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
				bOutofArea = TRUE
				bResetAI = FALSE
				
				IF IS_TIMER_STARTED(tWarningTimer)
					RESTART_TIMER_NOW(tWarningTimer)
//					PRINTLN("RESTARTING TIMER - tWarningTimer")
				ENDIF
				
//				PRINTLN("GOING TO BACK TO STATE - iGuardStagesEast = 0")
				iGuardStagesEast = 0
			ENDIF
		ELSE
//			PRINTLN("WE'RE NOT GOING BACK YET - 01")
		ENDIF
	ENDIF

	// East Side Guard
	SWITCH iGuardStagesEast
		CASE 0
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1588.976563,2793.941162,14.982886>>, <<-1575.663574,2779.557861,26.115358>>, 29.750000)
					IF DOES_ENTITY_EXIST(pedGateGuards[0])
						IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1", CONV_PRIORITY_VERY_HIGH)
							bPlayedWarning01 = TRUE
							bOutofArea = FALSE
												
							IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedGateGuards[0], <<-1587.3274, 2798.5173, 15.8582>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 216.3294)
							ENDIF
							
							PRINTLN("GOING TO STATE - iGuardStagesEast = 1")
							iGuardStagesEast = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 1
			IF bPlayedWarning01
				IF IS_TIMER_STARTED(tWarningTimer)
					fTime = GET_TIMER_IN_SECONDS(tWarningTimer)
					
					IF fTime > 7.0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1588.976563,2793.941162,14.982886>>, <<-1575.663574,2779.557861,26.115358>>, 29.750000)
							IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
								IF GET_SCRIPT_TASK_STATUS(pedGateGuards[0], SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
									TASK_AIM_GUN_AT_ENTITY(pedGateGuards[0], PLAYER_PED_ID(), -1)
									PRINTLN("TASKING GATE GUARD 1 TO AIM AT THE PLAYER")
									
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1a", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("SECOND WARNING GIVEN")
										
										PRINTLN("GOING TO STATE - iGuardStagesEast = 2")
										iGuardStagesEast = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					START_TIMER_NOW(tWarningTimer)
					PRINTLN("STARTING TIMER - tWarningTimer")
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 2
			IF IS_TIMER_STARTED(tWarningTimer)
				fTime = GET_TIMER_IN_SECONDS(tWarningTimer)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedGateGuards[0])
				IF IS_PED_SHOOTING(PLAYER_PED_ID()) 
				OR (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedGateGuards[0]) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedGateGuards[0]))
					IF GET_SCRIPT_TASK_STATUS(pedGateGuards[0], SCRIPT_TASK_SHOOT_AT_ENTITY) <> PERFORMING_TASK
						TASK_SHOOT_AT_ENTITY(pedGateGuards[0], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
						PRINTLN("TASKING GATE GUARD 1 TO SHOOT AT THE PLAYER")
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 5
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							PRINTLN("SETTING PLAYER WANTED LEVEL = 4")
						ENDIF
						
						PRINTLN("GOING TO STATE - iGuardStagesEast = 3")
						iGuardStagesEast = 3
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 3
		
		BREAK
	ENDSWITCH
	
	
	// =====================================================West Side Guard=====================================================
	IF bPlayedWarning02
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2306.916260,3390.545410,29.178257>>, <<-2322.244873,3401.718506,35.631306>>, 36.000000)
		AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
			IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
				bOutofArea01 = TRUE
				bResetAI01 = FALSE
				
				IF IS_TIMER_STARTED(tWarningTimer)
					RESTART_TIMER_NOW(tWarningTimer)
//					PRINTLN("RESTARTING TIMER - tWarningTimer")
				ENDIF
				
//				PRINTLN("GOING TO BACK TO STATE - iGuardStagesWest = 0")
				iGuardStagesWest = 0
			ENDIF
		ELSE
//			PRINTLN("WE'RE NOT GOING BACK YET - 02")
		ENDIF
	ENDIF

	SWITCH iGuardStagesWest
	
		CASE 0
			// Gate 2, West Side
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2306.916260,3390.545410,29.178257>>, <<-2322.244873,3401.718506,35.631306>>, 36.000000)
				IF DOES_ENTITY_EXIST(pedGateGuards[1])
					IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN2", CONV_PRIORITY_VERY_HIGH)
						bPlayedWarning02 = TRUE
						bOutofArea01 = FALSE
						
						IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedGateGuards[0], <<-2307.7634, 3385.5974, 29.9984>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 58.9552)
						ENDIF
						
						PRINTLN("GOING TO STATE - iGuardStagesEast = 1")
						iGuardStagesWest = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 1
			// West Side Guard
			IF bPlayedWarning02
				IF IS_TIMER_STARTED(tWarningTimer)
					fTime = GET_TIMER_IN_SECONDS(tWarningTimer)
					
					IF fTime > 7.0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2306.916260,3390.545410,29.178257>>, <<-2322.244873,3401.718506,35.631306>>, 36.000000)
							IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
								IF GET_SCRIPT_TASK_STATUS(pedGateGuards[1], SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
									TASK_AIM_GUN_AT_ENTITY(pedGateGuards[1], PLAYER_PED_ID(), -1)
									PRINTLN("TASKING GATE GUARD 2 TO AIM AT THE PLAYER")
									
									IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN2a", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("SECOND WARNING GIVEN")
										
										PRINTLN("GOING TO STATE - iGuardStagesWest = 2")
										iGuardStagesWest = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					START_TIMER_NOW(tWarningTimer)
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 2
			IF IS_TIMER_STARTED(tWarningTimer)
				fTime = GET_TIMER_IN_SECONDS(tWarningTimer)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedGateGuards[1])
				IF IS_PED_SHOOTING(PLAYER_PED_ID()) 
				OR (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedGateGuards[1]) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedGateGuards[1]))
					IF GET_SCRIPT_TASK_STATUS(pedGateGuards[1], SCRIPT_TASK_SHOOT_AT_ENTITY) <> PERFORMING_TASK
						TASK_SHOOT_AT_ENTITY(pedGateGuards[1], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
						PRINTLN("TASKING GATE GUARD 2 TO SHOOT AT THE PLAYER")
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 5
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							PRINTLN("SETTING PLAYER WANTED LEVEL = 4")
						ENDIF
						
						PRINTLN("GOING TO STATE - iGuardStagesWest = 3")
						iGuardStagesWest = 3
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//--------------------------------------------------------------------------------------------------------
		CASE 3
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC UPDATE_GROUP_01()
	INT idx
	VECTOR vPedLeaderPosition, vPlayerPosition
	
	IF NOT bClearedTasks01
		IF bPlayerWanted
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF NOT IS_ENTITY_DEAD(pedLeadMarcher01)
					CLEAR_PED_TASKS(pedLeadMarcher01)
				ENDIF
			
				REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
					IF DOES_ENTITY_EXIST(pedFollowers01[idx]) AND NOT IS_PED_INJURED(pedFollowers01[idx])
						CLEAR_PED_TASKS(pedFollowers01[idx])
					ENDIF
				ENDREPEAT
				
				PRINTLN("bClearedTasks01 = TRUE")
				bClearedTasks01 = TRUE
			ELSE
				bClearedTasks01 = FALSE
			ENDIF
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(pedLeadMarcher01) AND NOT IS_PED_INJURED(pedLeadMarcher01)
		IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedLeadMarcher01)
			IF bShouldReset
				vPedLeaderPosition = GET_ENTITY_COORDS(pedLeadMarcher01)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			
				IF IS_ENTITY_OCCLUDED(pedLeadMarcher01) AND NOT IS_SPHERE_VISIBLE(vAccomStartPos, 50.0)
				AND VDIST2(vPlayerPosition, vPedLeaderPosition) > 10000
				
					CLEAR_AREA_OF_VEHICLES(vAccomStartPos, 10.0)
					
					SET_ENTITY_COORDS(pedLeadMarcher01, vAccomStartPos)
					SET_ENTITY_HEADING(pedLeadMarcher01, 140.0094)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedLeadMarcher01, "AccomMarching01")
					
					ACCOMMODATIONS_RESET_FOLLOWERS()
					bShouldReset = FALSE
					
					bAccomTaskedLeader = FALSE
					bAccomTaskedLeader01 = FALSE
					PRINTLN("RESETING MARCHING LOOP - ACCOMODATIONS !!!!!!!!!!!!!!")
				ELSE
					IF NOT bAccomTaskedLeader
						IF NOT bAccomTaskedLeader01
							REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
								IF DOES_ENTITY_EXIST(pedFollowers01[idx]) AND NOT IS_PED_INJURED(pedFollowers01[idx])
									CLEAR_PED_TASKS(pedFollowers01[idx])
									TASK_STAND_STILL(pedFollowers01[idx], -1)
								ENDIF
							ENDREPEAT
							bAccomTaskedLeader01 = TRUE
						ENDIF
				
						IF NOT IS_PED_INJURED(pedLeadMarcher01)
							OPEN_SEQUENCE_TASK(iSeq01)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vAccomTalkingPos, PEDMOVEBLENDRATIO_WALK)
								TASK_TURN_PED_TO_FACE_COORD(NULL, vAccomLookAtCoord)
							CLOSE_SEQUENCE_TASK(iSeq01)
							TASK_PERFORM_SEQUENCE(pedLeadMarcher01, iSeq01)
							CLEAR_SEQUENCE_TASK(iSeq01)
							
							bAccomTaskedLeader = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
//			PRINTLN("WAYPOINT PLAYBACK IS GOING - ACCOMODATIONS MARCH")
			bShouldReset = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the entire accommodations system.
PROC ARMYBASE_UPDATE_ACCOMMODATIONS()

	SWITCH eAccommodationsState
		CASE eARMYBASE_SYSTEM_INIT
			// Row 1 Offset
			vAccomFollowOffset[0] = <<1,0,0>>
			vAccomFollowOffset[1] = <<-1,0,0>>
			// Row 2 Offset
//			vAccomFollowOffset[2] = <<0,-1.5,0>>
//			vAccomFollowOffset[3] = <<1,-1.5,0>>
//			vAccomFollowOffset[4] = <<-1,-1.5,0>>
			
			// ----------------------------------------------------GROUP 1----------------------------------------------------
			// Row 1
			vAccomStartingPos[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vAccomStartPos, 140.0094, <<1,0,0>>)
			vAccomStartingPos[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vAccomStartPos, 140.0094, <<-1,0,0>>)
//			// Row 2
//			vAccomStartingPos[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vAccomStartPos, 140.0094, <<0,-1.5,0>>)
//			vAccomStartingPos[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vAccomStartPos, 140.0094, <<1,-1.5,0>>)
//			vAccomStartingPos[4] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vAccomStartPos, 140.0094, <<-1,-1.5,0>>)
			
			PRINTLN("GOING TO STATE - ACCOMMODATIONS eARMYBASE_SYSTEM_REQUEST")
			eAccommodationsState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			// Request assets here.
			REQUEST_MODEL(S_M_Y_MARINE_01)
			REQUEST_MODEL(S_M_Y_MARINE_03)
			
			REQUEST_WAYPOINT_RECORDING("AccomMarching01")
			
			PRINTLN("GOING TO STATE - ACCOMMODATIONS eARMYBASE_SYSTEM_STREAM")
			eAccommodationsState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			IF HAS_MODEL_LOADED(S_M_Y_MARINE_01) AND HAS_MODEL_LOADED(S_M_Y_MARINE_03)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("AccomMarching01")
				PRINTLN("GOING TO STATE - ACCOMMODATIONS eARMYBASE_SYSTEM_SPAWN")
				// Stream here.
				eAccommodationsState = eARMYBASE_SYSTEM_SPAWN
			ELSE
				PRINTLN("WAITING FOR S_M_Y_MARINE_01, S_M_Y_MARINE_03, OR WAYPOINT RECORDINGS")
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
			SPAWN_TASK_GROUP_01()
			
			IF bDoneWithGroup01
				eAccommodationsState = eARMYBASE_SYSTEM_UPDATE
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			// General update.
			UPDATE_GROUP_01()
			IF NOT g_AmbientAreaData[eAMBAREA_ARMYBASE].bSuppressGateGuards
				UPDATE_GATE_GUARDS()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the planetaxi system.
PROC ARMYBASE_CLEANUP_ACCOMMODATIONS()
	INT idx

	REPEAT MAX_NUMBER_OF_ACCOM_MARCHERS idx
		IF DOES_ENTITY_EXIST(pedFollowers01[idx])
			IF IS_ENTITY_OCCLUDED(pedFollowers01[idx])
			OR (IS_DIRECTOR_MODE_RUNNING() AND IS_SCREEN_FADED_OUT())
				DELETE_PED(pedFollowers01[idx])
				PRINTLN("DELETING pedFollowers01: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedFollowers01[idx])
				PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedFollowers01: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT MAX_NUMBER_GATE_GUARDS idx
		IF DOES_ENTITY_EXIST(pedGateGuards[idx])
			IF IS_ENTITY_OCCLUDED(pedGateGuards[idx])
			OR (IS_DIRECTOR_MODE_RUNNING() AND IS_SCREEN_FADED_OUT())
				DELETE_PED(pedGateGuards[idx])
				PRINTLN("DELETING pedGateGuards: ", idx)
			ELSE
				IF NOT IS_ENTITY_DEAD(pedGateGuards[idx])
					TASK_START_SCENARIO_IN_PLACE(pedGateGuards[idx], "WORLD_HUMAN_GUARD_STAND_ARMY", -1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGateGuards[idx], FALSE)
					SET_PED_KEEP_TASK(pedGateGuards[idx], TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedGateGuards[idx])
					PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedGateGuards: ", idx)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(pedLeadMarcher01)
		IF DOES_BLIP_EXIST(blipMarchingGroup01)
			REMOVE_BLIP(blipMarchingGroup01)
			PRINTLN("REMOVING BLIP - blipMarchingGroup01")
		ENDIF
		IF IS_ENTITY_OCCLUDED(pedLeadMarcher01)
		OR (IS_DIRECTOR_MODE_RUNNING() AND IS_SCREEN_FADED_OUT())
			DELETE_PED(pedLeadMarcher01)
			PRINTLN("DELETING PED - pedLeadMarcher01")
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedLeadMarcher01)
			PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedLeadMarcher01")
		ENDIF
	ENDIF

	REMOVE_WAYPOINT_RECORDING("AccomMarching01")
	PRINTLN("REMOVING WAYPOINT RECORDING: AccomMarching01")
ENDPROC
