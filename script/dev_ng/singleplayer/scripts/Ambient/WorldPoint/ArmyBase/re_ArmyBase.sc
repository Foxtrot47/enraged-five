// re_ArmyBase.sc


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "re_ArmyBase.sch"

// **************************** SHARED VARIABLES ****************************
PED_INDEX 				playerPed
//VECTOR					vPlayerPos
//VECTOR 					vBaseCenter = << -2189.5447, 3129.6125, 31.8100 >>

// *************************** SUBSYSTEN INCLUDES ***************************
USING "ArmyBase_Accommodations_lib.sch"
USING "ArmyBase_AirControl_lib.sch"
USING "ArmyBase_Guards_lib.sch"
USING "ArmyBase_HangarPrep_lib.sch"
USING "ArmyBase_JeepPatrol_lib.sch"
USING "ArmyBase_OfficerMarch_lib.sch"
USING "ArmyBase_PlaneTaxi_lib.sch"
USING "ArmyBase_Tanks_lib.sch"
USING "area_checks.sch"
//USING "z_volumes.sch"

// ***************************** CORE VARIABLES *****************************
ARMYBASE_CORE_STATE eCoreState = eARMYBASE_CORE_INIT
INT 				iWaitTime = 1000
INT 				iAlarmStages
BOOL 				bChangeRelationships = FALSE
BOOL 				bOkayToUpdate = FALSE
// **************************************************************************


/// PURPOSE:
///    Cleans all assets in the army base script. Should invoke individual system cleanups.
PROC CLEANUP_SCRIPT()

	// This is where everything gets wrapped up.
	ARMYBASE_CLEANUP_ACCOMMODATIONS()
	ARMYBASE_CLEANUP_GUARDS()
	ARMYBASE_CLEANUP_HANGAR()
	ARMYBASE_CLEANUP_JEEPPATROL()
	ARMYBASE_CLEANUP_OFFCMARCH()
	ARMYBASE_CLEANUP_PLANETAXI()
	ARMYBASE_CLEANUP_AIRCONTROL()

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_ARMY)
	
	STOP_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS", FALSE)
	PRINTLN("STOPPING ARMY BASE ALARM IN CLEANUP")

	PRINTLN("*********************** Terminating Script: re_ArmyBase.sc ***********************")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC TOGGLE_BLIPS()

	#IF IS_DEBUG_BUILD
		INT idx
	
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_B, KEYBOARD_MODIFIER_ALT, "") 
		
			// "armybase_accommodations_lib.sch"
			IF NOT DOES_BLIP_EXIST(blipMarchingGroup01)
				blipMarchingGroup01 = ADD_BLIP_FOR_ENTITY(pedLeadMarcher01)
				SET_BLIP_COLOUR(blipMarchingGroup01, BLIP_COLOUR_RED)
				SET_BLIP_SCALE(blipMarchingGroup01, 0.75)
			ELSE
				IF DOES_BLIP_EXIST(blipMarchingGroup01)
					REMOVE_BLIP(blipMarchingGroup01)
				ENDIF
			ENDIF
			
			// "ArmyBase_HangarPrep_lib.sch"
			REPEAT MAX_NUMBER_BARRACKS idx
				IF DOES_ENTITY_EXIST(vehBarracks[idx])
					IF NOT DOES_BLIP_EXIST(blipBarracks[idx])
						blipBarracks[idx] = ADD_BLIP_FOR_ENTITY(vehBarracks[idx])
						SET_BLIP_COLOUR(blipBarracks[idx], BLIP_COLOUR_RED)
						SET_BLIP_SCALE(blipBarracks[idx], 0.75)
					ELSE
						IF DOES_BLIP_EXIST(blipBarracks[idx])
							REMOVE_BLIP(blipBarracks[idx])
						ENDIF
					ENDIF
				ENDIF		
			ENDREPEAT
			
			// "ArmyBase_JeepPatrol_lib.sch"
			REPEAT MAX_NUMBER_ROAMING_JEEPS idx
				IF DOES_ENTITY_EXIST(vehRoamingJeeps[idx])
					IF NOT DOES_BLIP_EXIST(blipRoamingJeeps[idx])
						blipRoamingJeeps[idx] = ADD_BLIP_FOR_ENTITY(vehRoamingJeeps[idx])
						SET_BLIP_COLOUR(blipRoamingJeeps[idx], BLIP_COLOUR_RED)
						SET_BLIP_SCALE(blipRoamingJeeps[idx], 0.75)
					ELSE
						IF DOES_BLIP_EXIST(blipRoamingJeeps[idx])
							REMOVE_BLIP(blipRoamingJeeps[idx])
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			// "ArmyBase_OfficerMarch_lib.sch"
			IF DOES_ENTITY_EXIST(pedLeadMarcher)
				IF NOT DOES_BLIP_EXIST(blipMarchingGuys)
					blipMarchingGuys = ADD_BLIP_FOR_ENTITY(pedLeadMarcher)
					SET_BLIP_COLOUR(blipMarchingGuys, BLIP_COLOUR_RED)
					SET_BLIP_SCALE(blipMarchingGuys, 0.75)
				ELSE
					IF DOES_BLIP_EXIST(blipMarchingGuys)
						REMOVE_BLIP(blipMarchingGuys)
					ENDIF
				ENDIF
			ENDIF
			
			// "ArmyBase_PlaneTaxi_lib.sch"
			IF DOES_ENTITY_EXIST(vehCargoPlane)
				IF NOT DOES_BLIP_EXIST(blipCargoPlane)
					blipCargoPlane = ADD_BLIP_FOR_ENTITY(vehCargoPlane)
					SET_BLIP_COLOUR(blipCargoPlane, BLIP_COLOUR_RED)
					SET_BLIP_SCALE(blipCargoPlane, 0.75)
				ELSE	
					IF DOES_BLIP_EXIST(blipCargoPlane)
						REMOVE_BLIP(blipCargoPlane)
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehLazer01)
				IF NOT DOES_BLIP_EXIST(blipLazer01)
					blipLazer01 = ADD_BLIP_FOR_ENTITY(vehLazer01)
					SET_BLIP_COLOUR(blipLazer01, BLIP_COLOUR_RED)
					SET_BLIP_SCALE(blipLazer01, 0.75)
				ELSE
					IF DOES_BLIP_EXIST(blipLazer01)
						REMOVE_BLIP(blipLazer01)
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehLazer02)
				IF NOT DOES_BLIP_EXIST(blipLazer02)
					blipLazer02 = ADD_BLIP_FOR_ENTITY(vehLazer02)
					SET_BLIP_COLOUR(blipLazer02, BLIP_COLOUR_RED)
					SET_BLIP_SCALE(blipLazer02, 0.75)
				ELSE
					IF DOES_BLIP_EXIST(blipLazer02)
						REMOVE_BLIP(blipLazer02)
					ENDIF
				ENDIF
			ENDIF
			
			// "ArmyBase_Tanks_lib.sch"
			REPEAT MAX_NUMBER_TANKS idx
				IF DOES_ENTITY_EXIST(vehTank[idx])
					IF NOT DOES_BLIP_EXIST(blipTank[idx])
						blipTank[idx] = ADD_BLIP_FOR_ENTITY(vehTank[idx])
						SET_BLIP_COLOUR(blipTank[idx], BLIP_COLOUR_RED)
						SET_BLIP_SCALE(blipTank[idx], 0.75)
					ELSE
						IF DOES_BLIP_EXIST(blipTank[idx])
							REMOVE_BLIP(blipTank[idx])
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	#ENDIF
ENDPROC

PROC CHECK_WANTED_LEVEL_TO_CHANGE_RELATIONSHIP_STATUS()
	IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
		IF NOT bChangeRelationships
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_ARMY)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
//			PRINTLN("SETTING THE ARMY TO HATE THE PLAYER!!! - BECAUSE THEY'RE WANTED")
			bPlayerWanted = TRUE
			bChangeRelationships = TRUE
		ENDIF
	ELSE
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) AND bPlayerWanted
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_ARMY)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
//			PRINTLN("PLAYER IS NO LONGER WANTED, RESET RELATIONSHIP STATUS TO IGNORE")
			bChangeRelationships = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_PLAYER_IN_UNDERGROUND_TUNNEL() 
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2080.115, 3292.193, -11.667 >>, << -2112.049, 3224.331, 11.667 >>, 115.000, FALSE, FALSE)
//			PRINTLN("PLAYER IS IN TUNNEL SYSTEM, SETTING bOkayToUpdate = FALSE")
			bOkayToUpdate = FALSE
		ELSE
			bOkayToUpdate = TRUE
//			PRINTLN("SETTING bOkayToUpdate = TRUE")
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_PLAYER_JUMPING_GATES()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1599.593, 2818.150, -17.645 >>, << -1612.423, 2806.997, 17.645 >>, 51.000, TRUE, FALSE)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2301.089, 3385.031, -31.086 >>, << -2305.302, 3379.441, 31.086 >>, 16.000, TRUE, FALSE)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2287.138, 3385.616, 31.124 >>, << -2292.554, 3378.428, 31.124 >>, 19.8, TRUE, FALSE)
//			PRINTLN("PLAYER INSIDE ANGLED AREA CHECK")
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 5
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ARMY_BASE_ALARMS()

	IF iAlarmStages > 0
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			STOP_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS", TRUE)
			iAlarmStages = 0
			PRINTLN("SETTING ALARM STAGE BACK: iAlarmStages = 0")
		ENDIF
	ENDIF

	SWITCH iAlarmStages
		CASE 0
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				IF PREPARE_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS")
					iAlarmStages = 1
					PRINTLN("iAlarmStages = 1")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			START_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS", FALSE)
			iAlarmStages = 2
			PRINTLN("iAlarmStages = 2")
		BREAK
		CASE 2
		
		BREAK
	ENDSWITCH
ENDPROC

// Main script update.
SCRIPT
	PRINTLN("*********************** scriptLaunch: re_ArmyBase.sc ***********************")
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINTLN("********** re_ArmyBase.sc FORCE CLEANUP **********")
		STOP_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS", TRUE)
		CLEANUP_SCRIPT()
	ENDIF
	
	// Grab the player once. This is for all systems to share.
	playerPed = PLAYER_PED_ID()
	
	WHILE TRUE
		// Check to see if we're quitting due to death. This also prevents all subsystems from having to check this.
		IF IS_ENTITY_DEAD(playerPed)
			PRINTLN("re_ArmyBase: 	Player is dead! Terminating.")
			eCoreState = eARMYBASE_CORE_CLEAN
		ELSE
			// Not dead, get his coords.
			//vPlayerPos = GET_ENTITY_COORDS(playerPed)
			
			//// Quitting due to distance check.
			//IF VDIST2(vPlayerPos, vBaseCenter) > ARMYBASE_QUIT_DIST2
			//	PRINTLN("re_ArmyBase: 	Player has left the area! Terminating.")
			//	eCoreState = eARMYBASE_CORE_CLEAN
			//ENDIF
		ENDIF

		IF g_bDisableArmyBase
			PRINTLN("re_ArmyBase: 	Disabled through global! Terminating.")			
			eCoreState = eARMYBASE_CORE_CLEAN
		ENDIF
		
		CLEAR_AREA(<< -1931.5485, 3026.9294, 31.8104 >>, 15.0, TRUE)

		// State-based update.
		SWITCH (eCoreState)
			CASE eARMYBASE_CORE_INIT
			
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_ARMY)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_AMBIENT_ARMY)
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, RELGROUPHASH_AMBIENT_ARMY)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_COP)
				
				PRINTLN("SETTING RELGROUPHASH_PLAYER TO IGNORE RELGROUPHASH_AMBIENT_ARMY - SO THE ARMY DOESN'T SHOOT AT THE PLAYER")
				
//				INIT_ZVOLUME_WIDGETS()
				
				PRINTLN("INSIDE STATE - eARMYBASE_CORE_INIT")
				eCoreState = eARMYBASE_CORE_REQUESTCOMMON
			BREAK
			
			CASE eARMYBASE_CORE_REQUESTCOMMON
				// Make our common requests.
				ARMYBASE_MAKE_COMMON_REQUESTS()
				
				PRINTLN("INSIDE STATE - eARMYBASE_CORE_REQUESTCOMMON")
				eCoreState = eARMYBASE_CORE_STREAMCOMMON
			BREAK
			
			CASE eARMYBASE_CORE_STREAMCOMMON
				PRINTLN("INSIDE STATE - eARMYBASE_CORE_STREAMCOMMON")
				
				// Make sure common requests have streamed.
				IF ARMYBASE_ARE_COMMON_REQUESTS_LOADED()
					eCoreState = eARMYBASE_CORE_SPAWNCOMMON
				ENDIF
			BREAK
			
			CASE eARMYBASE_CORE_SPAWNCOMMON
				PRINTLN("INSIDE STATE - eARMYBASE_CORE_SPAWNCOMMON")
				
				IF SPAWN_GATE_GUARDS()
					eCoreState = eARMYBASE_CORE_UPDATE
				ENDIF
			BREAK
			
			CASE eARMYBASE_CORE_UPDATE
//				PRINTLN("INSIDE STATE - eARMYBASE_CORE_UPDATE")

//				UPDATE_ZVOLUME_WIDGETS()

//				IF g_bTriggerSceneActive
//					PRINTLN("ARMY BASE: GOING TO CLEANUP - g_bTriggerSceneActive IS TRUE")
//					eCoreState = eARMYBASE_CORE_CLEAN
//				ENDIF

				#IF IS_DEBUG_BUILD
					TOGGLE_BLIPS()
					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_P, KEYBOARD_MODIFIER_ALT, "") 
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE, TRUE)
					ENDIF
					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_ALT, "") 
						CLEAR_AREA(<< -2283.6995, 3178.3674, 31.8104 >>, 10.0, TRUE)
						CLEAR_AREA(<< -2262.5730, 3221.4436, 31.8104 >>, 10.0, TRUE)
						PRINTLN("CLEARNING AREA OF VEHICLES")
					ENDIF
//					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_ALT, "")
//						g_bTriggerSceneActive = TRUE
//					ENDIF
				#ENDIF
			
				CHECK_WANTED_LEVEL_TO_CHANGE_RELATIONSHIP_STATUS()
				CHECK_PLAYER_IN_UNDERGROUND_TUNNEL()
				CHECK_PLAYER_JUMPING_GATES()
				HANDLE_ARMY_BASE_ALARMS()
				
				// To Fix Bug # 1528078
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
							REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
						ENDIF
					ENDIF
				ENDIF
				
				IF bOkayToUpdate
					// This is where all subsystems will be updated.
					
					ARMYBASE_UPDATE_AIRCONTROL()
					
					WAIT(100)
					
					ARMYBASE_UPDATE_ACCOMMODATIONS()
					
					WAIT(100)
					
					ARMYBASE_UPDATE_OFFCMARCH()
				ELSE
					PRINTLN("GOING TO CLEANUP - PLAYER HAS ENTERED UNDERGROUND TUNNEL")
					eCoreState = eARMYBASE_CORE_CLEAN
				ENDIF
			BREAK
			
			CASE eARMYBASE_CORE_CLEAN
				// Call this last. It'll terminate script.
				CLEANUP_SCRIPT()
			BREAK
		ENDSWITCH
		
		WAIT(iWaitTime)

	ENDWHILE
ENDSCRIPT

