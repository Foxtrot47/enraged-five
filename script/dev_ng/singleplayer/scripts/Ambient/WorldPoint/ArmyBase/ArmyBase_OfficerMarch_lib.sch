// ArmyBase_OfficerMarch_lib.sch

CONST_INT MAX_NUMBER_OF_MARCHERS 5

PED_INDEX pedLeadMarcher
PED_INDEX pedFollowers[MAX_NUMBER_OF_MARCHERS]
BLIP_INDEX blipMarchingGuys

VECTOR vMarch01StartPos = << -1894.5671, 3092.9028, 31.8103 >>
VECTOR vTalkingPos = << -1963.4568, 3133.1038, 31.8103 >>
VECTOR vLookAtCoord = << -1960.0548, 3130.9900, 31.8103 >>
VECTOR vFollowOffset[MAX_NUMBER_OF_MARCHERS]
VECTOR vStartingPos[MAX_NUMBER_OF_MARCHERS]

BOOL bTaskedLeader = FALSE
BOOL bTaskedLeader01 = FALSE
BOOL bShouldReset01 = FALSE
BOOL bClearedTasks = FALSE

INT iMarchingState = 0

ARMYBASE_SYSTEM_STATE	eOffcMarchState = eARMYBASE_SYSTEM_INIT

PROC RESET_FOLLOWERS()
	INT idx

	REPEAT MAX_NUMBER_OF_MARCHERS idx
		IF DOES_ENTITY_EXIST(pedLeadMarcher) AND DOES_ENTITY_EXIST(pedFollowers[idx])
		AND NOT IS_PED_INJURED(pedLeadMarcher) AND NOT IS_PED_INJURED(pedFollowers[idx])
			SET_ENTITY_COORDS(pedFollowers[idx], vStartingPos[idx])
			TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedFollowers[idx], pedLeadMarcher, vFollowOffset[idx], PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Handle the entire officer march system.
PROC ARMYBASE_UPDATE_OFFCMARCH()
	INT idx
	SEQUENCE_INDEX iSeq
	VECTOR vPedLeaderPosition, vPlayerPosition

	SWITCH eOffcMarchState
		CASE eARMYBASE_SYSTEM_INIT
			// Row 1 Offset
			vFollowOffset[0] = <<-1,0,0>>
			vFollowOffset[1] = <<-2,0,0>>
			
			// Row 2 Offset
			vFollowOffset[2] = <<0,-1.5,0>>
			vFollowOffset[3] = <<-1,-1.5,0>>
			vFollowOffset[4] = <<-2,-1.5,0>>
			
//			// Row 3 Offset
//			vFollowOffset[5] = <<0,-3,0>>
//			vFollowOffset[6] = <<-1,-3,0>>
//			vFollowOffset[7] = <<-2,-3,0>>
			
			// Row 1
			vStartingPos[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[0])
			vStartingPos[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[1])
			// Row 2
			vStartingPos[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[2])
			vStartingPos[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[3])
			vStartingPos[4] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[4])
//			// Row 3
//			vStartingPos[5] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[5])
//			vStartingPos[6] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[6])
//			vStartingPos[7] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMarch01StartPos, 57.3018, vFollowOffset[7])
			
			CLEAR_AREA_OF_VEHICLES(vMarch01StartPos, 10.0)
			
			PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_REQUEST")
			eOffcMarchState = eARMYBASE_SYSTEM_REQUEST
		BREAK
		
		CASE eARMYBASE_SYSTEM_REQUEST
			// Request assets here.
			REQUEST_MODEL(S_M_Y_MARINE_01)
			REQUEST_MODEL(S_M_Y_MARINE_03)
			
			REQUEST_WAYPOINT_RECORDING("OfficerMarch01")
			
			PRINTLN("GOING TO STATE - eARMYBASE_SYSTEM_STREAM")
			eOffcMarchState = eARMYBASE_SYSTEM_STREAM
		BREAK
		
		CASE eARMYBASE_SYSTEM_STREAM
			// Stream here.
			IF HAS_MODEL_LOADED(S_M_Y_MARINE_01) AND HAS_MODEL_LOADED(S_M_Y_MARINE_03)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("OfficerMarch01")
				PRINTLN("GOING TO STATE - OFFICER eARMYBASE_SYSTEM_SPAWN")
				eOffcMarchState = eARMYBASE_SYSTEM_SPAWN
			ELSE
				PRINTLN("WAITING FOR S_M_Y_MARINE_01, S_M_Y_MARINE_03, OR WAYPOINT RECORDINGS")
			ENDIF
		BREAK
		
		CASE eARMYBASE_SYSTEM_SPAWN
		
			SWITCH iMarchingState
				CASE 0
					// Leader
					CLEAR_AREA_OF_VEHICLES(vMarch01StartPos, 10.0)
					
					pedLeadMarcher = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vMarch01StartPos, 57.3018)
//					blipMarchingGuys = ADD_BLIP_FOR_ENTITY(pedLeadMarcher)
//					SET_BLIP_COLOUR(blipMarchingGuys, BLIP_COLOUR_RED)
//					SET_BLIP_SCALE(blipMarchingGuys, 0.75)
					
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLeadMarcher, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedLeadMarcher, RELGROUPHASH_AMBIENT_ARMY)
					GIVE_WEAPON_TO_PED(pedLeadMarcher, WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
					SET_ENTITY_LOD_DIST(pedLeadMarcher, LOD_DISTANCE)
					SET_PED_KEEP_TASK(pedLeadMarcher, TRUE)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedLeadMarcher, "OfficerMarch01")
					PRINTLN("CREATING LEAD MARCHER PED")
					iMarchingState++
				BREAK
				//---------------------------------------------------------------------------------------------------------------------
				CASE 1
					CLEAR_AREA_OF_VEHICLES(vMarch01StartPos, 10.0)
				
					REPEAT MAX_NUMBER_OF_MARCHERS idx
						pedFollowers[idx] = CREATE_PED(PEDTYPE_COP, S_M_Y_MARINE_01, vStartingPos[idx], 57.3018)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFollowers[idx], TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedFollowers[idx], RELGROUPHASH_AMBIENT_ARMY)
						GIVE_WEAPON_TO_PED(pedFollowers[idx], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
						SET_ENTITY_LOD_DIST(pedFollowers[idx], LOD_DISTANCE)
						SET_PED_KEEP_TASK(pedFollowers[idx], TRUE)
						SET_PED_STEERS_AROUND_PEDS(pedFollowers[idx], FALSE)
						PRINTLN("SETTING PEDS TO NO STEER AROUND OTHER PEDS: ", idx)
						PRINTLN("CREATING FOLLOWER PED: ", idx)
					ENDREPEAT
					
					iMarchingState++
				BREAK
				//---------------------------------------------------------------------------------------------------------------------
				CASE 2
					CLEAR_AREA_OF_VEHICLES(vMarch01StartPos, 10.0)
				
					REPEAT MAX_NUMBER_OF_MARCHERS idx
						IF DOES_ENTITY_EXIST(pedLeadMarcher) AND DOES_ENTITY_EXIST(pedFollowers[idx])
						AND NOT IS_PED_INJURED(pedLeadMarcher) AND NOT IS_PED_INJURED(pedFollowers[idx])
							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedLeadMarcher)
								TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedFollowers[idx], pedLeadMarcher, vFollowOffset[idx], PEDMOVEBLENDRATIO_WALK)
								PRINTLN("TASKING FOLLOWER PED TO FOLLOW: ", idx)
							ENDIF
						ENDIF
					ENDREPEAT
					
					iMarchingState++
				BREAK
				//---------------------------------------------------------------------------------------------------------------------
				CASE 3
					PRINTLN("GOING TO STATE - OFFICER eARMYBASE_SYSTEM_UPDATE")
					eOffcMarchState = eARMYBASE_SYSTEM_UPDATE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eARMYBASE_SYSTEM_UPDATE
			
			IF NOT bClearedTasks
				IF bPlayerWanted
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF NOT IS_ENTITY_DEAD(pedLeadMarcher)
							CLEAR_PED_TASKS(pedLeadMarcher)
						ENDIF
					
						REPEAT MAX_NUMBER_OF_MARCHERS idx
							IF DOES_ENTITY_EXIST(pedFollowers[idx]) AND NOT IS_PED_INJURED(pedFollowers[idx])
								CLEAR_PED_TASKS(pedFollowers[idx])
							ENDIF
						ENDREPEAT
						
						PRINTLN("bClearedTasks = TRUE")
						bClearedTasks = TRUE
					ELSE
						bClearedTasks = FALSE
					ENDIF
				ENDIF
			ENDIF
		
			// General update.
			IF DOES_ENTITY_EXIST(pedLeadMarcher) AND NOT IS_PED_INJURED(pedLeadMarcher)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedLeadMarcher)
					IF bShouldReset01
						
						vPedLeaderPosition = GET_ENTITY_COORDS(pedLeadMarcher)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
						ENDIF
					
						IF IS_ENTITY_OCCLUDED(pedLeadMarcher) AND NOT IS_SPHERE_VISIBLE(vMarch01StartPos, 50.0)
						AND VDIST2(vPlayerPosition, vPedLeaderPosition) > 10000
							
							CLEAR_AREA_OF_VEHICLES(vMarch01StartPos, 10.0)
							
							SET_ENTITY_COORDS(pedLeadMarcher, vMarch01StartPos)
							SET_ENTITY_HEADING(pedLeadMarcher, 57.3018)
							TASK_FOLLOW_WAYPOINT_RECORDING(pedLeadMarcher, "OfficerMarch01")
							
							RESET_FOLLOWERS()
							bShouldReset01 = FALSE
							
							bTaskedLeader = FALSE
							bTaskedLeader01 = FALSE
							PRINTLN("RESETING MARCHING LOOP - OFFICER MARCH !!!!!!!!!!!!!!")
						ELSE
							IF NOT bTaskedLeader
								IF NOT bTaskedLeader01
									REPEAT MAX_NUMBER_OF_MARCHERS idx
										IF DOES_ENTITY_EXIST(pedFollowers[idx]) AND NOT IS_PED_INJURED(pedFollowers[idx])
											CLEAR_PED_TASKS(pedFollowers[idx])
											TASK_STAND_STILL(pedFollowers[idx], -1)
										ENDIF
									ENDREPEAT
									bTaskedLeader01 = TRUE
								ENDIF
						
								IF NOT IS_PED_INJURED(pedLeadMarcher)
									OPEN_SEQUENCE_TASK(iSeq)
										TASK_GO_STRAIGHT_TO_COORD(NULL, vTalkingPos, PEDMOVEBLENDRATIO_WALK)
										TASK_TURN_PED_TO_FACE_COORD(NULL, vLookAtCoord)
									CLOSE_SEQUENCE_TASK(iSeq)
									TASK_PERFORM_SEQUENCE(pedLeadMarcher, iSeq)
									CLEAR_SEQUENCE_TASK(iSeq)
									
									bTaskedLeader = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
//					PRINTLN("WAYPOINT PLAYBACK IS GOING - OFFICE MARCH")
					bShouldReset01 = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the officer march system.
PROC ARMYBASE_CLEANUP_OFFCMARCH()
	INT idx

	IF DOES_ENTITY_EXIST(pedLeadMarcher)
		IF DOES_BLIP_EXIST(blipMarchingGuys)
			REMOVE_BLIP(blipMarchingGuys)
			PRINTLN("REMOVING BLIP - blipMarchingGuys")
		ENDIF
		IF IS_ENTITY_OCCLUDED(pedLeadMarcher)
		OR (IS_DIRECTOR_MODE_RUNNING() AND IS_SCREEN_FADED_OUT())
			DELETE_PED(pedLeadMarcher)
			PRINTLN("DELETING PED - pedLeadMarcher")
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedLeadMarcher)
			PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedLeadMarcher")
		ENDIF
	ENDIF
	REPEAT MAX_NUMBER_OF_MARCHERS idx
		IF DOES_ENTITY_EXIST(pedFollowers[idx])
			IF IS_ENTITY_OCCLUDED(pedFollowers[idx])
			OR (IS_DIRECTOR_MODE_RUNNING() AND IS_SCREEN_FADED_OUT())
				DELETE_PED(pedFollowers[idx])
				PRINTLN("DELETING PED - pedFollowers: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedFollowers[idx])
				PRINTLN("SET_PED_AS_NO_LONGER_NEEDED pedFollowers: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REMOVE_WAYPOINT_RECORDING("OfficerMarch01")
	PRINTLN("REMOVING WAYPOINT RECORDING - OfficerMarch01")
ENDPROC
