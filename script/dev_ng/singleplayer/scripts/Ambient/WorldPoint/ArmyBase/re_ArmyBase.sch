// re_ArmyBase.sch
USING "commands_script.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING "commands_streaming.sch"
USING "commands_clock.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "minigames_helpers.sch"
USING "oddjob_aggro.sch"

CONST_FLOAT		ARMYBASE_QUIT_DIST2			360000.0		// 600m
CONST_INT	 	LOD_DISTANCE 				1000

BOOL 				bPlayerWanted = FALSE

structPedsForConversation MyLocalPedStruct

ENUM ARMYBASE_CORE_STATE
	eARMYBASE_CORE_INIT = 0,
	eARMYBASE_CORE_REQUESTCOMMON,
	eARMYBASE_CORE_STREAMCOMMON,
	eARMYBASE_CORE_SPAWNCOMMON,
	eARMYBASE_CORE_UPDATE,	
	eARMYBASE_CORE_CLEAN
ENDENUM


ENUM ARMYBASE_SYSTEM_STATE
	eARMYBASE_SYSTEM_INIT = 0,
	eARMYBASE_SYSTEM_REQUEST,
	eARMYBASE_SYSTEM_STREAM,
	eARMYBASE_SYSTEM_SPAWN,
	eARMYBASE_SYSTEM_UPDATE
ENDENUM


AGGRO_ARGS armyBaseAggroArgs
EAggro eAggroReasons

/// PURPOSE:
///    Handles requesting of assets for the army base that are used across multiple systems.
PROC ARMYBASE_MAKE_COMMON_REQUESTS()
	
ENDPROC

/// PURPOSE:
///    Checks to see if common requests have finished loading.
/// RETURNS:
///    TRUE when common requests have loaded.
FUNC BOOL ARMYBASE_ARE_COMMON_REQUESTS_LOADED()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles any common spawning the army base script needs to do.
PROC ARMYBASE_SPAWN_COMMON()
ENDPROC

