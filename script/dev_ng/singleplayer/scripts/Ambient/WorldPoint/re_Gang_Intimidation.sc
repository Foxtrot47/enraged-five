
//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	.sc															//
//		AUTHOR			:	Tor Sigurdson												//
//		DESCRIPTION		:	Gang lures player into a trap						 		//
//		DATE			:	03/08/2010											 		//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
///    
///    
///    
///    
///    

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES =====================================

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_path.sch"
USING "LineActivation.sch" 
USING "Minigames_helpers.sch"
USING "AggroSupport.sch"
USING "Ambient_approach.sch"
USING "dialogue_public.sch"
USING "script_blips.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

//#IF IS_DEBUG_BUILDly scared those sickos off. I'll never forget what you did for me.
//	USING "SP_Debug_Menu.sch"
//#ENDIF

CONST_INT MAX_NUM_GANG 3

REL_GROUP_HASH rgh_GangOne
SEQUENCE_INDEX seq

INT NumberOfGangPosition
INT issGangPedsIdle, issGangPedsMugging
BOOL f_player_has_been_mugged = FALSE
//INT f_gang_warns_player_after_mugging = 0
//BOOL f_player_Near_lure = FALSE
structPedsForConversation Gang_IntimidationConversation

STRUCT AMBIENTASSET
	MODEL_NAMES mnGangModel[MAX_NUM_GANG]
	MODEL_NAMES mnLeadingPerson
	STRING sAnimDict
ENDSTRUCT

BLIP_INDEX biGang[MAX_NUM_GANG], biInitialActionBlip, biLurePed
VECTOR vMainPosition
VECTOR vGangStandingPosition[MAX_NUM_GANG]
FLOAT fGangStandingRotation[MAX_NUM_GANG]
//VECTOR vGangWalktoPosition[MAX_NUM_GANG]

VECTOR vLureToPos
BOOL bleadingped_waves_down_player_in_car = FALSE
BOOl bScaredByPoliceVehicle
BOOL bEventFailed
BOOL bDoFullCleanUp

//APPROACHPLAYER DoLureApproach

ENUM EUpdate
	EUpdate_Start,
	EUpdate_ApproachPlayer,
	EUpdate_ListenForPlayer,
	EUpdate_ThreatenPlayer,
	EUpdate_WaitForAnswer,
	EUpdate_PostMugging,
	EUpdate_DoFighting,
	EUpdate_ThankPlayer,
	EUpdate_CalledPolice,
	EUpdate_Finish
ENDENUM

//BOOL bIsBrokenDownPlayerControlOn = TRUE

FUNC BOOL LoadOrUnloadFirstAssets(BOOL Load, AMBIENTASSET Asset)
INT I
	IF Load = TRUE
		
		FOR I = 0 TO NumberOfGangPosition-1
			REQUEST_MODEL(Asset.mnGangModel[I])
		ENDFOR

		REQUEST_MODEL(Asset.mnLeadingPerson)
//		REQUEST_ANIM_DICT(Asset.sAnimDict)
		
		REQUEST_ANIM_DICT("random@gang_intimidation@")
		PREPARE_MUSIC_EVENT("RE28_OS")
		
		IF NOT HAS_MODEL_LOADED(Asset.mnLeadingPerson)
			RETURN FALSE
		ENDIF
		
		
		FOR I = 0 TO NumberOfGangPosition-1
			IF NOT HAS_MODEL_LOADED(Asset.mnGangModel[I])
				RETURN FALSE
			ENDIF
		ENDFOR
		
//		IF HAS_ANIM_DICT_LOADED(Asset.sAnimDict)
		IF HAS_ANIM_DICT_LOADED("random@gang_intimidation@")
//		AND PREPARE_MUSIC_EVENT("RE28_OS")
			RETURN TRUE
		ENDIF


	ELSE
		FOR I = 0 TO NumberOfGangPosition-1
			SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnGangModel[I])
		ENDFOR
		
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnLeadingPerson)
//		REMOVE_ANIM_DICT(Asset.sAnimDict)
	ENDIF

RETURN FALSE

ENDFUNC

FUNC BOOL AreAllGangDead(PED_INDEX& AGang[])

	INT I
	FOR I = 0 TO COUNT_OF(AGang)-1
		IF DOES_ENTITY_EXIST(AGang[I])
			IF NOT IS_PED_INJURED(AGang[I])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
//	KILL_ANY_CONVERSATION()
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_AN_INCOMPATIBLE_VEHICLE()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MakeGangCelebrate(PED_INDEX& WinningGang[], AMBIENTASSET Asset)

	INT I
	INT NumberOfMembers = COUNT_OF(WinningGang)
	FOR I = 0 TO NumberOfMembers-1
		IF NOT IS_PED_INJURED(WinningGang[I])
			IF IS_PED_HOLDING_ONE_HANDED_WEAPON(WinningGang[I])
				IF IS_PED_MALE(WinningGang[I])
					TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "male_one_handed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ELSE
					TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "female_one_handed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ELIF IS_PED_HOLDING_TWO_HANDED_WEAPON(WinningGang[I])
				IF IS_PED_MALE(WinningGang[I])
					TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "male_two_handed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ELSE
					TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "female_two_handed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ELSE
				IF IS_PED_MALE(WinningGang[I])
					IF GET_RANDOM_BOOL()
						TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "male_unarmed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					ELSE
						TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "male_unarmed_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					ENDIF
				ELSE
					IF GET_RANDOM_BOOL()
						TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "female_unarmed_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					ELSE
						TASK_PLAY_ANIM(WinningGang[I], Asset.sAnimDict, "female_unarmed_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC RemoveAllGangBlips()

	INT I
	FOR I = 0 TO NumberOfGangPosition-1
		IF DOES_BLIP_EXIST(biGang[I])
			REMOVE_BLIP(biGang[I])
		ENDIF
	ENDFOR

ENDPROC

BOOL bHasActionStarted = FALSE

PROC GetLureOutTheWay(PED_INDEX TheLure)

	IF NOT IS_PED_INJURED(TheLure)
		CLEAR_PED_TASKS(TheLure)
		OPEN_SEQUENCE_TASK(seq)
//			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1288.1150, -1595.8873, 53.2254>>,  PEDMOVE_SPRINT, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1286.0370, -1623.2074, 53.2297 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
			TASK_COWER(NULL, -1)
		//	TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//////////			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
//////////			TASK_STAND_STILL(NULL, 3000)
//			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//			TASK_PLAY_ANIM(NULL, Asset.sAnimDict, "Cower", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(TheLure, seq)
		CLEAR_SEQUENCE_TASK(seq)
		seq = NULL
		//PLAY_AMBIENT_DIALOGUE_LINE(Gang_IntimidationConversation, TheLure, "REGIAU", "REGIN_SUCK")
		SET_PED_CAN_BE_TARGETTED(TheLure, TRUE)
		RemoveBlip(biLurePed)
	ENDIF

ENDPROC

PROC BlipAllGang(PED_INDEX& GangGuys[])

	INT I 
	FOR I = 0 TO COUNT_OF(GangGuys)-1
		IF NOT DOES_BLIP_EXIST(biGang[I])
			IF NOT IS_PED_INJURED(GangGuys[I])
				biGang[I] = CREATE_BLIP_FOR_PED(GangGuys[I], TRUE) 
				SET_ENTITY_IS_TARGET_PRIORITY(GangGuys[I], TRUE)
			ENDIF
			
		ENDIF
	ENDFOR

ENDPROC

PROC RemoveInjuredBlips(PED_INDEX& GangGuys[])

	INT I 
	FOR I = 0 TO COUNT_OF(GangGuys)-1
		IF IS_PED_INJURED(GangGuys[I])
			RemoveBlip(biGang[I])
		ENDIF
	ENDFOR

ENDPROC

PROC AttackPlayer(PED_INDEX& GangGuys[], PED_INDEX TheLure)

	INT I
	FOR I = 0 TO COUNT_OF(GangGuys)-1
		IF NOT IS_PED_INJURED(GangGuys[I])
//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgh_GangOne, RELGROUPHASH_PLAYER)
			TASK_COMBAT_PED(GangGuys[I], PLAYER_PED_ID())
			//SET_PED_COMBAT_ATTRIBUTES(GangGuys[I], CA_AGGRESSIVE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(GangGuys[I], CA_USE_COVER, TRUE)
			SET_PED_KEEP_TASK(GangGuys[I], TRUE)
		ENDIF
	ENDFOR
	IF NOT IS_ENTITY_DEAD(TheLure)
		CLEAR_PED_SECONDARY_TASK(TheLure)
//		TASK_SMART_FLEE_PED(TheLure, PLAYER_PED_ID(), 200, -1)
		TASK_COWER(TheLure, -1)
		SET_PED_KEEP_TASK(TheLure, TRUE)
	ENDIF
	
ENDPROC

INT ThreatTimer
INT MuggingStages = 0
FUNC BOOL MugPlayer(AMBIENTASSET Asset, PED_INDEX MuggingPed, PED_INDEX& TheGang[])

//	INT I
	SWITCH MuggingStages
	
		CASE 0
			IF DO_TIMER(ThreatTimer, 0)
//				IF NOT IS_PED_INJURED(MuggingPed)
//					OPEN_SEQUENCE_TASK(seq)
////						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, PLAYER_PED_ID(), << 3, 3, 3 >>, PEDMOVE_WALK, -1, 0.5)
//						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_WALK, FALSE)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(MuggingPed, seq)
//					CLEAR_SEQUENCE_TASK(seq)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_MONEYM", CONV_PRIORITY_AMBIENT_HIGH)
							MuggingStages = 1
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_MONEYF", CONV_PRIORITY_AMBIENT_HIGH)
							MuggingStages = 1
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_MONEYT", CONV_PRIORITY_AMBIENT_HIGH)
							MuggingStages = 1
						ENDIF
					ENDIF
//				ENDIF
				seq = NULL
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(MuggingPed)
				IF GET_SCRIPT_TASK_STATUS(MuggingPed, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				OR IS_ENTITY_AT_ENTITY(MuggingPed, PLAYER_PED_ID(), << 8, 8, 8 >>)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//					DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, Large_Reward)
						CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_KILL", CONV_PRIORITY_AMBIENT_HIGH)
//						CLEAR_PED_TASKS(MuggingPed)
						SETTIMERA(0) //
//						WAIT(2000)
						f_player_has_been_mugged = TRUE
						MuggingStages = 4
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
//			FOR I = 0 TO COUNT_OF(TheGang)-1
			
			IF NOT IS_PED_INJURED(TheGang[2])
				IF NOT IS_PED_INJURED(TheGang[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_STAND_STILL(NULL, 700)
						//TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, TheGang[2], <<0, 1, 0>>, PEDMOVE_WALK, 5000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[0], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_ACHIEVE_HEADING(NULL, fGangStandingRotation[0])
//						TASK_CHAT_TO_PED(NULL, TheGang[4], CF_AUTO_CHAT,  MAKE_VECTOR_ZERO(), 0.0, 1000)
						TASK_PLAY_ANIM_ADVANCED(NULL, Asset.sAnimDict, "sit_down_idle_01", vGangStandingPosition[0], <<0, 0, fGangStandingRotation[0]>>, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
//						TASK_START_SCENARIO_AT_POSITION(NULL, "WORLD_HUMAN_SMOKING", vGangStandingPosition[0], fGangStandingRotation[0])
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TheGang[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
					seq = NULL
				ENDIF
				IF NOT IS_PED_INJURED(TheGang[1])
					OPEN_SEQUENCE_TASK(seq)
						TASK_STAND_STILL(NULL, 200)
//						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, TheGang[4], <<1, -1, 0>>, PEDMOVE_WALK, 5000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[1], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						//TASK_CHAT_TO_PED(NULL, TheGang[4], CF_AUTO_CHAT,  MAKE_VECTOR_ZERO(), 0.0, 5000)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TheGang[1], seq)
					CLEAR_SEQUENCE_TASK(seq)
					seq = NULL
				ENDIF
				IF NOT IS_PED_INJURED(TheGang[2])
					OPEN_SEQUENCE_TASK(seq)
						TASK_STAND_STILL(NULL, 1000)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, TheGang[0], << 0, -0.5, 0 >>, PEDMOVE_WALK, 10000)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[2], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_CHAT_TO_PED(NULL, TheGang[4], CF_AUTO_CHAT,  vGangStandingPosition[2], fGangStandingRotation[2], 2000)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TheGang[2], seq)
					CLEAR_SEQUENCE_TASK(seq)
					seq = NULL
				ENDIF
//				IF NOT IS_PED_INJURED(TheGang[3])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 500)
////						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, TheGang[4], << -1.5, 0, 0 >>, PEDMOVE_WALK, 5000)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[3], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_CHAT_TO_PED(NULL, TheGang[4], CF_AUTO_CHAT,  vGangStandingPosition[3], fGangStandingRotation[3], 5000)
//						TASK_WANDER_STANDARD(NULL)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[3], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
//				ENDIF
//				IF NOT IS_PED_INJURED(TheGang[4])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 50)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[4], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
////						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//						TASK_STAND_STILL(NULL, 10000)
//						TASK_WANDER_STANDARD(NULL)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[4], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
//				ENDIF
//				IF NOT IS_PED_INJURED(TheGang[5])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 400)
//						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, TheGang[4], << -1.5, 0, 0 >>, PEDMOVE_WALK, 5000)
////						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangStandingPosition[5], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_CHAT_TO_PED(NULL, TheGang[4], CF_AUTO_CHAT,  vGangStandingPosition[5], fGangStandingRotation[5], 2000)
//						TASK_WANDER_STANDARD(NULL)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[5], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
//				ENDIF
			ENDIF
			
			MuggingStages = 3
		BREAK
		
		
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_REALM", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_REALF", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_REALT", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				
				MuggingStages = 4
			ENDIF
		BREAK
		
		CASE 4
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	RETURN FALSE

ENDFUNC
//
//INT PostMug_stages = 0
//FUNC BOOL PostMugHuddle(INT MuggingPedIndex, PED_INDEX& TheGang[])
//
////	VECTOR HuddlePosition[5]
//	INT I
//	
//	SWITCH PostMug_stages
//		
//		CASE 0
//		
////			HuddlePosition[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheGang[MuggingPedIndex], <<-1, 0, 0>>)
////			HuddlePosition[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheGang[MuggingPedIndex], <<-0.5, 0.5, 0>>)
////			HuddlePosition[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheGang[MuggingPedIndex], <<0, 1, 0>>)
////			HuddlePosition[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheGang[MuggingPedIndex], <<0.5, 0.5, 0>>)
////			HuddlePosition[4] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheGang[MuggingPedIndex], <<1, 0, 0>>)
//			
//			FOR I = 0 TO COUNT_OF(TheGang)-1
//				IF I <> MuggingPedIndex
//					IF NOT IS_PED_INJURED(TheGang[I])
//						TASK_FOLLOW_NAV_MESH_TO_COORD(TheGang[I], vGangStandingPosition[I], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					ENDIF
//				ENDIF
//			ENDFOR
//			
//			PostMug_stages++
//			
//		BREAK
//		
//		CASE 1
//		
//			RETURN TRUE
//		BREAK
//	
//	
//	ENDSWITCH
//
//
//
//	RETURN FALSE
//	
//ENDFUNC

BOOL bGangIsAiming = FALSE
FUNC BOOL GetGangAiming(PED_INDEX& TheGang[])

	INT I 
	IF bGangIsAiming = FALSE
		FOR I = 0 TO COUNT_OF(TheGang)-1
			IF NOT IS_PED_INJURED(TheGang[I])
				TASK_AIM_GUN_AT_ENTITY(TheGang[I], PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(8000, 30000))
			ENDIF
		ENDFOR
		bGangIsAiming = TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

BOOL bRunLastAI = FALSE

PROC RunLastGuyAI(PED_INDEX& TheGang[])
	INT iCountGangRemaining = 0
	INT iLastIndex
	INT I 
	IF bRunLastAI = FALSE
		FOR I = 0 TO NumberOfGangPosition-1
			IF NOT IS_PED_INJURED(TheGang[I])
				iCountGangRemaining++
				iLastIndex = I
			ENDIF
		ENDFOR
		
		IF iCountGangRemaining = 1
			IF NOT IS_PED_INJURED(TheGang[iLastIndex])
		 		SET_PED_COMBAT_ATTRIBUTES(TheGang[iLastIndex], CA_USE_COVER, TRUE) 
				//SET_PED_COMBAT_ATTRIBUTES(TheGang[iLastIndex], CA_AGGRESSIVE, TRUE) 
				//SET_PED_COMBAT_MOVEMENT(TheGang[iLastIndex], CM_WILLADVANCE) 
				SET_PED_COMBAT_RANGE(TheGang[iLastIndex], CR_NEAR)
//				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(TheGang[iLastIndex], PLAYER_PED_ID(), <<0, -4, 0>>, PEDMOVE_RUN)
//				SCRIPT_ASSERT("Lasy guy AI turned on")
				bRunLastAI = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC fleeAndFail(PED_INDEX LurePed, PED_INDEX& TheGang[])
	INT index
	REPEAT COUNT_OF(TheGang) index
		IF NOT IS_PED_INJURED(TheGang[index])
		AND NOT IS_PED_INJURED(LurePed)
			IF CAN_PED_SEE_HATED_PED(TheGang[index], PLAYER_PED_ID())
			OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), TheGang[index])
			OR CAN_PED_SEE_HATED_PED(LurePed, PLAYER_PED_ID())
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF NOT IS_PED_INJURED(TheGang[index])
					CLEAR_PED_TASKS(TheGang[index])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TheGang[index], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(TheGang[index], TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(LurePed)
					CLEAR_PED_TASKS(LurePed)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(LurePed, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(LurePed, TRUE)
				ENDIF
				bEventFailed = TRUE
			ENDIF
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF NOT IS_PED_INJURED(TheGang[index])
				CLEAR_PED_TASKS(TheGang[index])
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(TheGang[index], seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(TheGang[index], TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(LurePed)
				CLEAR_PED_TASKS(LurePed)
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(LurePed, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(LurePed, TRUE)
			ENDIF
			bEventFailed = TRUE
		ENDIF
	ENDREPEAT
ENDPROC

INT PreGang_Stages = 0
BOOL bPreGangThingsSuccess = FALSE
//APPROACHPLAYER GangThreats
//BOOL bDoGangApproach = FALSE

FUNC BOOL DoPreGangThings(PED_INDEX LurePed, PED_INDEX& TheGang[])

	SWITCH PreGang_Stages
		CASE 0
//			IF IS_COORD_EVENT_ACTIVATED(vMainPosition, 11)
			IF NOT IS_PED_INJURED(LurePed)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1272.8779, -1532.8402, 36.4104 >>, << 1339.4820, -1623.6365, 71.4792 >>, 105.1875)
				AND NOT IS_ENTITY_OCCLUDED(LurePed)
				OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 15, 15, 15 >>)
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						SET_RANDOM_EVENT_ACTIVE()
					ENDIF
					IF DOES_BLIP_EXIST(biInitialActionBlip)
						RemoveBlip(biInitialActionBlip)
					ENDIF
					IF NOT DOES_BLIP_EXIST(biLurePed)
						biLurePed = CREATE_BLIP_FOR_PED(LurePed)
						SHOW_HEIGHT_ON_BLIP(biLurePed, FALSE)
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
							IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 5
								//DoLureApproach.aApproachPed = LurePed
								//DoLureApproach.SpeechBlock = Gang_IntimidationConversation
								//DoLureApproach.SubtitleBlock = "REGIAU"
								//DoLureApproach.ThanksSpeech = "REGIN_LR1F"
	//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//							AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 10, 10, 10 >>)
										CLEAR_PED_SECONDARY_TASK(LurePed)
										OPEN_SEQUENCE_TASK(seq)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000, SLF_USE_TORSO)
		//									TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 3)
											//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
											//TASK_CHAT_TO_PED(NULL, PLAYER_PED_ID(),CF_DO_QUICK_CHAT)
											//TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000,SLF_USE_TORSO)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(LurePed, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
										IF DOES_BLIP_EXIST(biLurePed)
											SHOW_HEIGHT_ON_BLIP(biLurePed, TRUE)
										ENDIF
										
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
										IF IS_PED_FACING_PED(LurePed, PLAYER_PED_ID(), 45)
											KILL_ANY_CONVERSATION()
											WAIT(0)
											CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_LR1F", CONV_PRIORITY_AMBIENT_HIGH)
											PreGang_Stages = 1
										ENDIF
									ENDIF
//								ENDIF
								
							ELSE
								// make girl wave to player
								IF bleadingped_waves_down_player_in_car = FALSE
									CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_CAR", CONV_PRIORITY_AMBIENT_HIGH) //get out of car help!
									IF NOT IS_PED_INJURED(LurePed)
										OPEN_SEQUENCE_TASK(seq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000, SLF_USE_TORSO)
											TASK_PLAY_ANIM(NULL, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_wave_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
	//										TASK_PLAY_ANIM(NULL, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
											
										//	TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1, 3.0)
											//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
											//TASK_CHAT_TO_PED(NULL, PLAYER_PED_ID(),CF_DO_QUICK_CHAT)
										//	TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000,SLF_USE_TORSO)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(LurePed, seq)
										CLEAR_SEQUENCE_TASK(seq)
									ENDIF
									bleadingped_waves_down_player_in_car = TRUE
								ENDIF
							ENDIF
						ELSE
							KILL_ANY_CONVERSATION()
							WAIT(0)
							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_POLICE", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(LurePed)
								CLEAR_PED_TASKS(LurePed)
								TASK_SMART_FLEE_PED(LurePed, PLAYER_PED_ID(), 200, -1)
								SET_PED_KEEP_TASK(LurePed, TRUE)
								bScaredByPoliceVehicle = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(LurePed)
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 8, 8, 8 >>)
								OPEN_SEQUENCE_TASK(seq)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 20000, SLF_USE_TORSO)
		//							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 3)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(LurePed, seq)
								CLEAR_SEQUENCE_TASK(seq)
								
								IF IS_PED_FACING_PED(LurePed, PLAYER_PED_ID(), 45)
									KILL_ANY_CONVERSATION()
									WAIT(0)
									CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_LR1F", CONV_PRIORITY_AMBIENT_HIGH)
									PreGang_Stages = 1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			IF IS_PED_EVENT_ACTIVATED(TheGang[0], 10)
//				RemoveBlip(biInitialActionBlip)
//				bDoGangApproach = TRUE
//				PreGang_Stages = 3
//				this_script_activated = TRUE
//			ENDIF
			IF NOT IS_PED_INJURED(TheGang[2])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1273.0980, -1604.1722, 53.1535 >>, << 1276.5682, -1615.1275, 56.7879 >>, 10) // is player at the drive way
				AND NOT IS_PLAYER_IN_AN_INCOMPATIBLE_VEHICLE()
					PreGang_Stages = 3
				ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1298.5462, -1617.7420, 53.2254 >>, << 1262.8680, -1629.3601, 64.2894 >>, 30.4375)
					fleeAndFail(LurePed, TheGang)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
//			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLureToPos, << 9.5, 9.5, 2 >>)
//				PreGang_Stages = 3
//			ENDIF
			
			IF NOT IS_PED_INJURED(TheGang[2])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1273.0980, -1604.1722, 53.1535 >>, << 1276.5682, -1615.1275, 56.7879 >>, 10) // is player at the drive way
				AND NOT IS_PLAYER_IN_AN_INCOMPATIBLE_VEHICLE()
					PreGang_Stages = 3
				ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1298.5462, -1617.7420, 53.2254 >>, << 1262.8680, -1629.3601, 64.2894 >>, 30.4375)
					fleeAndFail(LurePed, TheGang)
				ENDIF
			ENDIF
			
			//IF UPDATE_APPROACH_PLAYER(DoLureApproach) 
			IF NOT IS_PED_INJURED(LurePed)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				IF GET_SCRIPT_TASK_STATUS(LurePed, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
					PRINTSTRING("/n STACKSIZE 1 :")
					PRINTINT(GET_CURRENT_STACK_SIZE())
					//IF DoLureApproach.bSuccess = TRUE 
						//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							
							PRINTSTRING("/n STACKSIZE 2 :")
							PRINTINT(GET_CURRENT_STACK_SIZE())
							
							CLEAR_AREA_OF_VEHICLES(vLureToPos, 13)
//							CLEAR_AREA_OF_PEDS(<< 1286.3472, -1608.1975, 54.3195 >>, 30)
							
							CLEAR_PED_SECONDARY_TASK(LurePed)
							
							IF NOT IS_PED_INJURED(LurePed)
								OPEN_SEQUENCE_TASK(seq)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLureToPos, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 32.2195)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(LurePed, seq)
								CLEAR_SEQUENCE_TASK(seq)
								seq = NULL
								IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
								AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 6, 6, 6 >>, FALSE, TRUE)
								AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								
								ELSE
//									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_HERE", CONV_PRIORITY_AMBIENT_HIGH)
//									ENDIF
								ENDIF
							ENDIF
							
							PRINTSTRING("/n STACKSIZE 3 :")
							PRINTINT(GET_CURRENT_STACK_SIZE())
							PreGang_Stages = 2
							
//						ENDIF
//					ENDIF
//				ELSE
//					RETURN FALSE
//				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(LurePed)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_LR1G", CONV_PRIORITY_AMBIENT_HIGH)
					SETTIMERA(0)
					PreGang_Stages = 3
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1273.0980, -1604.1722, 53.1535 >>, << 1276.5682, -1615.1275, 56.7879 >>, 10) // is player at the drive way
			AND NOT IS_PLAYER_IN_AN_INCOMPATIBLE_VEHICLE()
//				IF NOT IS_PED_INJURED(LurePed)
//					CLEAR_PED_TASKS(LurePed)
//					TASK_TURN_PED_TO_FACE_ENTITY(LurePed, PLAYER_PED_ID(), -1)
//				ENDIF
				GetLureOutTheWay(LurePed)
				PreGang_Stages = 4
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 1298.5462, -1617.7420, 53.2254 >>, << 1262.8680, -1629.3601, 64.2894 >>, 30.4375)
				fleeAndFail(LurePed, TheGang)
			ELSE
				IF NOT IS_PED_INJURED(LurePed)
					IF TIMERA() > 8000
					AND IS_ENTITY_AT_COORD(LurePed, vLureToPos, << 3, 3, 3 >>)
						IF IS_PED_FACING_PED(LurePed, PLAYER_PED_ID(), 60)
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF GET_RANDOM_BOOL()
								TASK_PLAY_ANIM(LurePed, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
							ELSE
								TASK_PLAY_ANIM(LurePed, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)//, AF_SECONDARY|AF_UPPERBODY)
							ENDIF
							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_HERE", CONV_PRIORITY_AMBIENT_HIGH)
							SETTIMERA(0)
//						ENDIF
						ENDIF
					ENDIF
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					AND CAN_PED_SEE_HATED_PED(LurePed, PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), LurePed)
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), LurePed)
							fleeAndFail(LurePed, TheGang)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_PED_INJURED(LurePed)
				OPEN_SEQUENCE_TASK(seq)
					TASK_STAND_STILL(NULL, 500)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLureToPos, PEDMOVE_RUN)
				CLOSE_SEQUENCE_TASK(seq)
				//TASK_PERFORM_SEQUENCE(LurePed, seq)
				CLEAR_SEQUENCE_TASK(seq)
				seq = NULL
			ENDIF
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_THRT", CONV_PRIORITY_AMBIENT_HIGH)
				PreGang_Stages = 5
			ENDIF
			
//			GangThreats.aApproachPed = TheGang[2]
			
//			GangThreats.SpeechBlock = Gang_IntimidationConversation
//			GangThreats.SubtitleBlock = "REGIAU"
//			GangThreats.ThanksSpeech = "REGIN_T"
			
//			ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 4, TheGang[2], "PED2")
		BREAK
		
		CASE 5
			BlipAllGang(TheGang)
			RemoveBlip(biLurePed)
			bPreGangThingsSuccess = TRUE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
RETURN TRUE
ENDFUNC

VECTOR vMuggedPosition

//FUNC BOOL UpdateEvent(EUpdate& curstate,AMBIENTASSET Asset, PED_INDEX LurePed, PED_INDEX& TheGang[], INT& WaitTime, BOOL& bTaskGiven)
FUNC BOOL UpdateEvent(EUpdate& curstate, AMBIENTASSET Asset, PED_INDEX LurePed, PED_INDEX& TheGang[], INT& WaitTime, BOOL& bTaskGiven)

	IF curstate >= EUpdate_ThreatenPlayer
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issGangPedsMugging)
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMuggedPosition, << 9, 9, 9 >>)
			AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 1276.21, -1613.05, 53 >>, << 9, 9, 9 >>)
			OR IS_PED_RUNNING(PLAYER_PED_ID())
//				PRINTSTRING("\n Player is not in mugging area, guys are now angry1 \n")
				curstate = EUpdate_DoFighting
			ENDIF
		ENDIF
	ENDIF

	SWITCH curstate
		
		CASE EUpdate_Start
			
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					IF NOT IS_PED_INJURED(LurePed)
						SET_ENTITY_COORDS(LurePed, vLureToPos)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vLureToPos + << 3, 0, 0 >>)
					ENDIF
				ENDIF
			#ENDIF
			
			IF DoPreGangThings(LurePed, TheGang)
				IF bPreGangThingsSuccess
					curstate = EUpdate_ListenForPlayer
				ENDIF
				
				IF NOT IS_PED_INJURED(LurePed)
					SET_PED_RESET_FLAG(LurePed, PRF_UseProbeSlopeStairsDetection, TRUE)
					IF NOT HAS_PLAYER_DAMAGED_PED(LurePed)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 15, 15, 15 >>)
//						AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY()
							IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
							AND CAN_PED_SEE_HATED_PED(LurePed, PLAYER_PED_ID())
							AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
								OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), LurePed)
								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), LurePed)
									CLEAR_PED_SECONDARY_TASK(LurePed)
									TASK_LOOK_AT_ENTITY(LurePed, PLAYER_PED_ID(), 5000, SLF_USE_TORSO)
									PRINTSTRING("***********************************")
									PRINTNL()
									PRINTSTRING("Gang_Intimidation, Ending: PLayer is armed and scared off lady")
									PRINTNL()
									PRINTSTRING("***********************************")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(1000)
									CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_ARM", CONV_PRIORITY_AMBIENT_HIGH)
									INT index
									REPEAT COUNT_OF(TheGang) index
										IF NOT IS_PED_INJURED(TheGang[index])
											CLEAR_PED_SECONDARY_TASK(TheGang[index])
											TASK_SMART_FLEE_PED(TheGang[index], PLAYER_PED_ID(), 200, -1)
											SET_PED_KEEP_TASK(TheGang[index], TRUE)
										ENDIF
									ENDREPEAT
									IF NOT IS_PED_INJURED(LurePed)
										TASK_SMART_FLEE_PED(LurePed, PLAYER_PED_ID(), 200, -1)
										SET_PED_KEEP_TASK(LurePed, TRUE)
									ENDIF
									bEventFailed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						KILL_ANY_CONVERSATION()
						IF NOT IS_PED_INJURED(LurePed)
							CLEAR_PED_TASKS(LurePed)
							TASK_SMART_FLEE_PED(LurePed, PLAYER_PED_ID(), 200, -1)
							SET_PED_KEEP_TASK(LurePed, TRUE)
						ENDIF
						bEventFailed = TRUE
					ENDIF
				ENDIF
				
				IF IS_PED_INJURED(TheGang[0])
				OR IS_PED_INJURED(TheGang[1])
				OR IS_PED_INJURED(TheGang[2])
					AttackPlayer(TheGang, LurePed)
				ENDIF
				
				IF IS_PED_INJURED(LurePed)
					IF PreGang_Stages > 2
						AttackPlayer(TheGang, LurePed)
					ELSE
						curstate = EUpdate_Finish
					ENDIF
				ELSE
					IF IS_PED_RAGDOLL(LurePed)
						IF IS_ENTITY_PLAYING_ANIM(LurePed, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_wave_loop")
							STOP_ANIM_TASK(LurePed, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_wave_loop")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("Gang_Intimidation, Ending: Approach Failed")
				PRINTNL()
				PRINTSTRING("***********************************")
				curstate = EUpdate_Finish
				
			ENDIF
		BREAK
		
		
		CASE EUpdate_ListenForPlayer
		
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLureToPos)
			
			//GetLureOutTheWay(LurePed)
			
			TASK_PAUSE(PLAYER_PED_ID(), 1000)
		//	RemoveBlip(biInitialActionBlip)
		//	RemoveBlip(biLurePed)
			
			vMuggedPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(TheGang[1])
				IF NOT DOES_BLIP_EXIST(biGang[1])
					//biGang[2] = CREATE_BLIP_FOR_PED(TheGang[2], TRUE) 
				ENDIF
			//	ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 4, TheGang[2], "PED2")
				TRIGGER_MUSIC_EVENT("RE28_OS")
				issGangPedsMugging = CREATE_SYNCHRONIZED_SCENE(<< 1283.205, -1622.710, 54.299 >>, << 0, 0, 103.680 >>)
				
				IF NOT IS_PED_INJURED(TheGang[0])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 700)
//						IF bDoGangApproach = FALSE
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[1], PEDMOVE_WALK)
						//	TASK_GOTO_ENTITY_AIMING(NULL, PLAYER_PED_ID(), 5, 5)
//							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5, 5)
//							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 1, 0, 0 >>, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5, 5)
							TASK_SYNCHRONIZED_SCENE(TheGang[0], issGangPedsMugging, "random@gang_intimidation@", "001446_02_gangintimidation_2_thug_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS) // Second guy from the left.
//						ENDIF
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[0], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
				ENDIF
				
				IF NOT IS_PED_INJURED(TheGang[1])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 300)
//						IF bDoGangApproach = FALSE
//							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 5)
							TASK_SYNCHRONIZED_SCENE(TheGang[1], issGangPedsMugging, "random@gang_intimidation@", "001446_02_gangintimidation_2_thug_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)//|SYNCED_SCENE_USE_PHYSICS) // Guy behind wall.
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[0], PEDMOVE_WALK)
//						ENDIF
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[1], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
				ENDIF
				
				IF NOT IS_PED_INJURED(TheGang[2])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 400)
//						IF bDoGangApproach = FALSE
//							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 2.5)
							TASK_SYNCHRONIZED_SCENE(TheGang[2], issGangPedsMugging, "random@gang_intimidation@", "001446_02_gangintimidation_2_thug_d", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS) // First guy from the left.
//							TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, PLAYER_PED_ID(), << 2, 3, 0 >>, PEDMOVE_WALK, 3000, 2.5)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[0], PEDMOVE_WALK)
//						ENDIF
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[2], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
				ENDIF

//				IF NOT IS_PED_INJURED(TheGang[3])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 30)
//						IF bDoGangApproach = FALSE
////							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[2], PEDMOVE_WALK)
//							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_WALK, FALSE, 2.5)
//						ENDIF
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 15000)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[3], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
//				ENDIF
//				
//				IF NOT IS_PED_INJURED(TheGang[5])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_STAND_STILL(NULL, 500)
//						IF bDoGangApproach = FALSE
////							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[3], PEDMOVE_WALK)
//							//TASK_GOTO_ENTITY_AIMING(NULL, PLAYER_PED_ID(), 5, 5)
//							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_WALK, FALSE)
//						ENDIF
//						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 15000)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(TheGang[5], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					seq = NULL
//				ENDIF
				
				curstate = EUpdate_ThreatenPlayer
			ENDIF
			
		BREAK
		
		CASE EUpdate_ThreatenPlayer
			waitTime = 0
			IF IS_SYNCHRONIZED_SCENE_RUNNING(issGangPedsMugging)
				IF GET_SYNCHRONIZED_SCENE_PHASE(issGangPedsMugging) > 0.300
					IF NOT IS_PED_INJURED(TheGang[0])
						SET_PED_RESET_FLAG(TheGang[0], PRF_InstantBlendToAim, TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 1, 0, 0 >>, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5, 5)
							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(TheGang[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						seq = NULL
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(issGangPedsMugging) > 0.350
					IF NOT IS_PED_INJURED(TheGang[1])
						SET_PED_RESET_FLAG(TheGang[1], PRF_InstantBlendToAim, TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 5)
							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(TheGang[1], seq)
						CLEAR_SEQUENCE_TASK(seq)
						seq = NULL
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(issGangPedsMugging) > 0.275
					IF NOT IS_PED_INJURED(TheGang[2])
						SET_PED_RESET_FLAG(TheGang[2], PRF_InstantBlendToAim, TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5)
							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(TheGang[2], seq)
						CLEAR_SEQUENCE_TASK(seq)
						seq = NULL
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(TheGang[2])
				IF IS_ENTITY_AT_ENTITY(TheGang[2], PLAYER_PED_ID(), << 4, 4, 2 >>)
//			IF UPDATE_APPROACH_PLAYER(GangThreats)
//				IF GangThreats.bSuccess = TRUE
				
//						GetLureOutTheWay(LurePed)
						
						RemoveBlip(biInitialActionBlip)
						RemoveBlip(biLurePed)
						
						BlipAllGang(TheGang)
						
						IF NOT IS_PED_INJURED(TheGang[0])
							OPEN_SEQUENCE_TASK(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 1, 0, 0 >>, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5, 5)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(TheGang[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							seq = NULL
						ENDIF
						IF NOT IS_PED_INJURED(TheGang[1])
							OPEN_SEQUENCE_TASK(seq)
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 5)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(TheGang[1], seq)
							CLEAR_SEQUENCE_TASK(seq)
							seq = NULL
						ENDIF
						IF NOT IS_PED_INJURED(TheGang[2])
							OPEN_SEQUENCE_TASK(seq)
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 3.5)
								TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 25000)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(TheGang[2], seq)
							CLEAR_SEQUENCE_TASK(seq)
							seq = NULL
						ENDIF
						
//						IF NOT IS_PED_INJURED(TheGang[1])
//							IF NOT DOES_BLIP_EXIST(biGang[1])
//								biGang[2] = CREATE_BLIP_FOR_PED(TheGang[2], TRUE) 
//							ENDIF
//						ENDIF
//					IF NOT IS_PED_INJURED(TheGang[3])
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_STAND_STILL(NULL, 30)
//							IF bDoGangApproach = FALSE
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[2], PEDMOVE_WALK)
//							ENDIF
//							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 15000)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(TheGang[3], seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						seq = NULL
//					ENDIF
//					
//					IF NOT IS_PED_INJURED(TheGang[5])
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_STAND_STILL(NULL, 500)
//							IF bDoGangApproach = FALSE
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGangWalktoPosition[3], PEDMOVE_WALK)
//							ENDIF
//							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 15000)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(TheGang[5], seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						seq = NULL
//					ENDIF
					
					ThreatTimer = GET_GAME_TIMER()
					curstate = EUpdate_WaitForAnswer
				ENDIF
			ENDIF
		BREAK
		
		CASE EUpdate_WaitForAnswer
		
			IF MugPlayer(Asset, TheGang[2], TheGang)
				RemoveBlip(biGang[2])
				//Wait for player to aggro them
//				curstate = EUpdate_PostMugging
			
			ELSE
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					PRINTSTRING("\n Player drew weapon, guys are now angry \n")
					curstate = EUpdate_DoFighting
				ENDIF
			ENDIF
		BREAK
		
//		CASE EUpdate_PostMugging
//			
//			IF PostMugHuddle(4, TheGang) = TRUE
//				
//			
//			ENDIF
//			
//		BREAK
		
		CASE EUpdate_DoFighting
			IF NOT bHasActionStarted
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_WARN2", CONV_PRIORITY_AMBIENT_HIGH)
				SET_WANTED_LEVEL_MULTIPLIER(0.5)
				//GetLureOutTheWay(LurePed)
//				KILL_ANY_CONVERSATION()
				BlipAllGang(TheGang)
				AttackPlayer(TheGang, LurePed)
				RemoveBlip(biLurePed)
				RemoveBlip(biInitialActionBlip)
				bHasActionStarted = TRUE
			ENDIF
			
			RunLastGuyAI(TheGang)
			
			RemoveInjuredBlips(TheGang)
			
			IF AreAllGangDead(TheGang)
//			OR NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), TheGang, << 100, 100, 100 >>)
				PRINTSTRING("\n @@@@@@@@@@@@@@ AreAllGangDead @@@@@@@@@@@@@@@@@@@ \n")
				bTaskGiven = TRUE
				IF NOT IS_PED_INJURED(LurePed)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), LurePed, << 8, 8, 8 >>)
//					AND CAN_PED_SEE_HATED_PED(PLAYER_PED_ID(), LurePed)
						CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_SORRY", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				IF NOT DOES_BLIP_EXIST(biLurePed)
					//biLurePed = CREATE_BLIP_FOR_PED(LurePed)
				ENDIF
				WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					WAIT(0)
				ENDWHILE
				WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
					WAIT(0)
				ENDWHILE
				RANDOM_EVENT_PASSED()
				RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
				RETURN TRUE
				curstate = EUpdate_Finish
			ENDIF
			
		BREAK
		
//		CASE EUpdate_ThankPlayer
		
//			IF UPDATE_RUN_THANKYOU(LureThankYou)
//				IF LureThankYou.bSuccess = TRUE
//					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, Small_Reward,FALSE,TRUE)
//					curstate = EUpdate_Finish
//				ENDIF
//			ELSE
//				curstate = EUpdate_Finish
//			ENDIF
			
//		BREAK
		
		
		CASE EUpdate_CalledPolice
		
		BREAK
		
		CASE EUpdate_Finish
			PRINTSTRING("\n @@@@@@@@@@@@@@ EUpdate_Finish @@@@@@@@@@@@@@@@@@@ \n")
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH


RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID Gang_IntimidationWidget
#ENDIF

PROC DrawWidgets(BOOL DoDraw)


		IF DoDraw
			#IF IS_DEBUG_BUILD
				IF NOT DOES_WIDGET_GROUP_EXIST(Gang_IntimidationWidget)
					Gang_IntimidationWidget = START_WIDGET_GROUP("Beat: Gang Shootout")
					STOP_WIDGET_GROUP()
				ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF DOES_WIDGET_GROUP_EXIST(Gang_IntimidationWidget)
					DELETE_WIDGET_GROUP(Gang_IntimidationWidget)
				ENDIF
			#ENDIF
		ENDIF
	
ENDPROC

FUNC WEAPON_TYPE PickaRandomWeapon()

	INT Random = GET_RANDOM_INT_IN_RANGE(0, 5)
	
	IF Random = 0
		RETURN WEAPONTYPE_COMBATPISTOL
	ELIF Random = 1
		RETURN WEAPONTYPE_COMBATPISTOL
	ELIF Random = 2
		RETURN WEAPONTYPE_PISTOL
	ELIF Random = 3
		RETURN WEAPONTYPE_PISTOL
	ELSE
		RETURN WEAPONTYPE_PISTOL
	ENDIF
	
	RETURN WEAPONTYPE_PISTOL
ENDFUNC

//=================================== START OF MAIN SCRIPT ================================

PROC Cleanup(AMBIENTASSET Asset, BOOL bTaskGiven, PED_INDEX& GangOne[], PED_INDEX piLeadingPed)
	PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ CLEANUP @@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
	
	IF bTaskGiven = FALSE
	
	ENDIF
	
	IF bDoFullCleanUp
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vLureToPos - << 10, 10, 10 >>, vLureToPos + << 10, 10, 10 >>, TRUE)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		RemoveBlip(biGang[0])
		RemoveBlip(biGang[1])
		RemoveBlip(biGang[2])
	//	RemoveBlip(biGang[3])
	//	RemoveBlip(biGang[4])
		
		INT I
		FOR I = 0 TO NumberOfGangPosition-1
			IF NOT IS_PED_INJURED(GangOne[I])
//				SET_ENTITY_INVINCIBLE(GangOne[I], FALSE)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GangOne[I], FALSE)
			ENDIF
		ENDFOR
		
		IF NOT IS_PED_INJURED(piLeadingPed)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piLeadingPed, FALSE)
		ENDIF
		
		DrawWidgets(FALSE)
		LoadOrUnloadFirstAssets(FALSE, Asset)
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()

ENDPROC

SCRIPT (coords_struct in_coords)

	AMBIENT_EVENT_STATE Event_state = EVENT_INIT
	AMBIENTASSET initialAsset
	EUpdate updateState = EUpdate_Start
	
	INT iBitFieldDontCheck
	INT lockOnTimer
	EAggro aggroReason
	INT iAggroIndex
	BOOL bAggroed
	BOOL bTaskGiven
	
	//NumberOfGangPosition = 5
	NumberOfGangPosition = 3
	vMainPosition = in_coords.vec_coord[0]
	
	
	//Fighting positions
//	vGangStandingPosition[0] = << 1280.6405, -1605.1195, 53.8293 >>
//	fGangStandingRotation[0] = 104.8918
//	vGangStandingPosition[1] =  << 1285.4709, -1623.2998, 53.2254 >>
//	fGangStandingRotation[1] = 9.2610
//	vGangStandingPosition[2] = << 1287.5870, -1622.9451, 53.2254 >>
//	fGangStandingRotation[2] = 127.3
//	vGangStandingPosition[3] = << 1281.2351, -1621.1638, 53.2254 >>
//	fGangStandingRotation[3] = 28.45
//	vGangStandingPosition[4] = << 1275.8745, -1619.9084, 53.2254 >>
//	fGangStandingRotation[4] = 20.5419
//	vGangStandingPosition[5] = << 1278.2249, -1617.7126, 53.2254 >>
//	fGangStandingRotation[5] = 54.2304
//	vGangStandingPosition[6] = << 1281.9088, -1621.2928, 53.2254 >>
//	fGangStandingRotation[6] = 54.2304

	//Hiding positions
	vGangStandingPosition[0] = << 1283.8500, -1622.5010, 53.2255 >>
	fGangStandingRotation[0] = 198.9926
	vGangStandingPosition[1] = << 1286.6546, -1627.0000, 53.2250 >> //<< 1275.7429, -1621.1654, 52.3403 >>
	fGangStandingRotation[1] = 22.3073 //130.4649
	vGangStandingPosition[2] = << 1286.4518, -1624.7819, 53.2250 >>
	fGangStandingRotation[2] = 49.6773
//	vGangStandingPosition[3] = << 1283.70, -1626.55, 54.16 >>
//	fGangStandingRotation[3] = 221.3633
//	vGangStandingPosition[4] = << 1281.9426, -1622.7666, 53.2255 >>
//	fGangStandingRotation[4] = 301.0643
//	vGangStandingPosition[5] = << 1290.2249, -1623.7126, 53.2254 >>
//	fGangStandingRotation[5] = 54.2304
//	vGangStandingPosition[6] = << 1291.9088, -1621.2928, 53.2254 >>
//	fGangStandingRotation[6] = 54.2304
	
	//Walk to positions
//	vGangWalktoPosition[0] = << 1281.2351, -1621.1638, 53.2254 >>
//	vGangWalktoPosition[1] = << 1275.8745, -1619.9084, 53.2254 >>
//	vGangWalktoPosition[2] = << 1278.2249, -1617.7126, 53.2254 >>
//	vGangWalktoPosition[3] = << 1281.9088, -1621.2928, 53.2254 >>
//	vGangWalktoPosition[4] = << 1279.4871, -1617.3275, 53.2255 >>

	VECTOR vLeadingPedPos = <<1298.3907, -1580.8950, 50.7937>>
	FLOAT fLeadingPedHeading = 332.0060
	
	
	vLureToPos = << 1277.7249, -1617.8971, 53.2254 >>
	
	PED_INDEX piGangOne[MAX_NUM_GANG]
	
	PED_INDEX piLeadingPed
	
	IF GET_RANDOM_BOOL()
		initialAsset.mnGangModel[0] 			= G_M_Y_MexGoon_02
		initialAsset.mnGangModel[1] 			= G_M_Y_MexGoon_02
		initialAsset.mnGangModel[2] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[3] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[4] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[5] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[6] 			= G_M_Y_MexGoon_02
		
		
		//initialAsset.mnLeadingPerson			= A_F_Y_Runner_01 //MP_G_F_Mexican_01.
		initialAsset.mnLeadingPerson = A_F_Y_EASTSA_03
	ELSE
		initialAsset.mnGangModel[0] 			= G_M_Y_MexGoon_02
		initialAsset.mnGangModel[1] 			= G_M_Y_MexGoon_02
		initialAsset.mnGangModel[2] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[3] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[4] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[5] 			= G_M_Y_MexGoon_02
//		initialAsset.mnGangModel[6] 			= G_M_Y_MexGoon_02
		
		
	//	initialAsset.mnLeadingPerson			= A_F_Y_Runner_01
		initialAsset.mnLeadingPerson = A_F_Y_EASTSA_03
	ENDIF
	
	initialAsset.sAnimDict = "amb_sit_wall_m"
	
	INT waitTime
	INT I
	
	
//	IF HAS_DEATHARREST_EXECUTED()	
//		Cleanup(initialAsset, bTaskGiven)
//	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		PRINTSTRING("***********************************")
		PRINTNL()
		PRINTSTRING("Gang_Intimidation: External cleanup has occured")
		PRINTNL()
		PRINTSTRING("***********************************")
		Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vMainPosition)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	LoadOrUnloadFirstAssets(TRUE, initialAsset)
	
	WHILE Event_state <> EVENT_OVER
		WAIT(0)
		waitTime = CONST_AmbEventUpdate
		
		#IF IS_DEBUG_BUILD
//			INT timeStamp = SP_Get_Selection_Timestamp("re_gang_shootout")
//			IF timeStamp <> -1
//			AND (GET_GAME_TIMER() - timeStamp) < 10000
//				IF ARE_STRINGS_EQUAL("re_gang_shootout", SP_Get_Latest_Selection())
//					//This will just tell you to cleanup area
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
////						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1310.3204, -1604.6957, 51.4703>> )
//						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//					ENDIF
//				ENDIF
//			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("Gang_Intimidation: Press S to Skip")
				PRINTNL()
				PRINTSTRING("***********************************")
				WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
					WAIT(0)
				ENDWHILE
				RANDOM_EVENT_PASSED()
				RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("Gang_Intimidation: Press F to Fail")
				PRINTNL()
				PRINTSTRING("***********************************")
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
			ENDIF
			
		#ENDIF
		
		
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			waitTime = 0
			event_state = EVENT_OVER
			Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
		ENDIF
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_GI")
		
		SWITCH event_state
		
			CASE EVENT_INIT
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("Gang_Intimidation: EVENT_INIT")
				PRINTNL()
				PRINTSTRING("***********************************")	
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
				ENDIF
				//Load initial things
				IF LoadOrUnloadFirstAssets(TRUE, initialAsset)
					waitTime = 0
					Event_state = EVENT_WAIT_TO_START
				ENDIF
				
				PRINTSTRING("/n STACKSIZE 4 :")
				PRINTINT(GET_CURRENT_STACK_SIZE())
			BREAK
			
			CASE EVENT_WAIT_TO_START
				IF IS_AMBIENT_SAFE_TO_RUN(RUN_ON_MISSION_NEVER)
					
					DrawWidgets(TRUE)
					event_state = EVENT_STARTING
				ELSE
					event_state = EVENT_ENDING
				ENDIF
				waitTime = 0
			BREAK
			
			CASE EVENT_STARTING
				PRINTSTRING("***********************************")
				PRINTNL()
				PRINTSTRING("Gang_Intimidation: EVENT_STARTING")
				PRINTNL()
				PRINTSTRING("***********************************")	
				//Create everything
				waitTime = 0
				
				ADD_SCENARIO_BLOCKING_AREA(vLureToPos - << 30, 30, 10 >>, vLureToPos + << 30, 30, 10 >>)
				SET_PED_NON_CREATION_AREA(vLureToPos - << 50, 50, 50 >>, vLureToPos + << 100, 100, 100 >>)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vLureToPos - << 15, 15, 15 >>, vLureToPos + << 15, 15, 15 >>, FALSE)
				//SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< 1286.3472, -1608.1975, 54.3195 >>, 30.0, 0)
//				CLEAR_AREA_OF_PEDS(<< 1286.3472, -1608.1975, 54.3195 >>, 30)
				
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_HeardShot))
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_ShotNear))
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
				
				// Creates Gang One
				ADD_RELATIONSHIP_GROUP("GangOne", rgh_GangOne)	
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgh_GangOne, RELGROUPHASH_PLAYER)
				
				piLeadingPed = CREATE_PED(PEDTYPE_GANG6, initialAsset.mnLeadingPerson, vLeadingPedPos, fLeadingPedHeading)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piLeadingPed, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(initialAsset.mnLeadingPerson)
				SET_PED_RELATIONSHIP_GROUP_HASH(piLeadingPed, rgh_GangOne)
				SET_PED_CAN_BE_TARGETTED(piLeadingPed, FALSE)
				SET_PED_COMPONENT_VARIATION(piLeadingPed, PED_COMP_HAIR, 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(piLeadingPed, PED_COMP_HEAD, 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piLeadingPed, PED_COMP_TORSO, 2, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piLeadingPed, PED_COMP_LEG, 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piLeadingPed, PED_COMP_SPECIAL, 1, 1, 0) //(accs)
				SET_AMBIENT_VOICE_NAME(piLeadingPed, "G_F_Y_Vagos_01_LATINO_MINI_02")
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 0, PLAYER_PED_ID(), "MICHAEL")
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 0, PLAYER_PED_ID(), "TREVOR")
				ENDIF
				ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 3, piLeadingPed, "REGIFemale")
				ADD_PED_FOR_DIALOGUE(Gang_IntimidationConversation, 4, piGangOne[2], "REGIMale")
				
				//TASK_PLAY_ANIM(piLeadingPed, initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				TASK_PLAY_ANIM (piLeadingPed, "random@gang_intimidation@", "001445_01_gangintimidation_1_female_idle_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
				IF NOT DOES_BLIP_EXIST(biInitialActionBlip)
//					biInitialActionBlip = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(piLeadingPed)
				ENDIF
//				IF DOES_BLIP_EXIST(biInitialActionBlip)
//					RemoveBlip(biInitialActionBlip)
//				ENDIF
//				IF NOT DOES_BLIP_EXIST(biLurePed)
//					biLurePed = CREATE_BLIP_FOR_PED(piLeadingPed)
//				ENDIF
				
				FOR I = 0 TO NumberOfGangPosition-1
					IF NOT DOES_ENTITY_EXIST(piGangOne[I])
						piGangOne[I] = CREATE_PED(PEDTYPE_GANG6, initialAsset.mnGangModel[I], vGangStandingPosition[I], fGangStandingRotation[I])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piGangOne[I], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(initialAsset.mnGangModel[I])
						SET_PED_RELATIONSHIP_GROUP_HASH(piGangOne[I], rgh_GangOne)
						SET_PED_CONFIG_FLAG(piGangOne[I], PCF_DontInfluenceWantedLevel, TRUE)
						GIVE_WEAPON_TO_PED(piGangOne[I], PickaRandomWeapon(), INFINITE_AMMO, TRUE)
						SET_PED_COMBAT_MOVEMENT(piGangOne[I], CM_WILLADVANCE)
						SET_PED_COMBAT_RANGE(piGangOne[I], CR_NEAR)
						SET_PED_ACCURACY(piGangOne[I], 13)
						SET_PED_MONEY(piGangOne[I], GET_RANDOM_INT_IN_RANGE(200, 500))
					ENDIF
				ENDFOR
				
				GIVE_WEAPON_TO_PED(piGangOne[0], WEAPONTYPE_SAWNOFFSHOTGUN, 40, TRUE)
				
				issGangPedsIdle = CREATE_SYNCHRONIZED_SCENE(<< 1283.205, -1622.710, 54.299 >>, << 0, 0, 103.680 >>)
				
				IF NOT IS_PED_INJURED(piGangOne[0])
//					TASK_PLAY_ANIM(piGangOne[0], initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					TASK_SYNCHRONIZED_SCENE(piGangOne[0], issGangPedsIdle, "random@gang_intimidation@", "001446_02_gangintimidation_2_idle_thug_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS)// Second guy from the left.
				ENDIF
				IF NOT IS_PED_INJURED(piGangOne[1])
//					TASK_START_SCENARIO_AT_POSITION(piGangOne[1], "PROP_HUMAN_SEAT_BENCH", vGangStandingPosition[1], fGangStandingRotation[1])
//					TASK_PLAY_ANIM(piGangOne[1], initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					TASK_SYNCHRONIZED_SCENE(piGangOne[1], issGangPedsIdle, "random@gang_intimidation@", "001446_02_gangintimidation_2_idle_thug_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS)// Guy behind wall.
				ENDIF
				IF NOT IS_PED_INJURED(piGangOne[2])
//					TASK_START_SCENARIO_AT_POSITION(piGangOne[2], "WORLD_HUMAN_HANG_OUT_STREET", vGangStandingPosition[2], fGangStandingRotation[2])
					TASK_SYNCHRONIZED_SCENE(piGangOne[2], issGangPedsIdle, "random@gang_intimidation@", "001446_02_gangintimidation_2_idle_thug_d", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS)// First guy from the left.
//					TASK_PLAY_ANIM(piGangOne[2], initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(issGangPedsIdle, TRUE)
//				IF NOT IS_PED_INJURED(piGangOne[3])
//					TASK_START_SCENARIO_AT_POSITION(piGangOne[3], "PROP_HUMAN_SEAT_BENCH", vGangStandingPosition[3], fGangStandingRotation[3])
////					TASK_DUCK(piGangOne[3], -1)		
//				ENDIF
//				IF NOT IS_PED_INJURED(piGangOne[4])
//					TASK_START_SCENARIO_AT_POSITION(piGangOne[3], "WORLD_HUMAN_SMOKING", vGangStandingPosition[3], fGangStandingRotation[3])
////					TASK_PLAY_ANIM(piGangOne[4], initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//				ENDIF
//				IF NOT IS_PED_INJURED(piGangOne[5])
//					TASK_PLAY_ANIM(piGangOne[5], initialAsset.sAnimDict, "sit_down_idle_01", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
//				ENDIF
				//BREAK_ON_NATIVE_COMMAND("WAIT", FALSE)
								
				bDoFullCleanUp = TRUE
				
				event_state = EVENT_RUNNING
				
			BREAK
			
			CASE EVENT_RUNNING
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				IF updateState < EUpdate_ThreatenPlayer
					IF HAS_PLAYER_AGGROED_PEDS(piGangOne, iAggroIndex, aggroReason, lockOnTimer, iBitFieldDontCheck, bAggroed)
					OR HAS_PLAYER_DAMAGED_PED(piGangOne[0])
					OR HAS_PLAYER_DAMAGED_PED(piGangOne[1])
					OR HAS_PLAYER_DAMAGED_PED(piGangOne[2])
					OR IS_BULLET_IN_AREA(vGangStandingPosition[0], 40)
					OR IS_PROJECTILE_TYPE_IN_AREA(vGangStandingPosition[0] - << 25, 25, 25 >>, vGangStandingPosition[0] + << 25, 25, 25 >>, WEAPONTYPE_SMOKEGRENADE, TRUE)
					OR IS_PROJECTILE_TYPE_IN_AREA(vGangStandingPosition[0] - << 25, 25, 25 >>, vGangStandingPosition[0] + << 25, 25, 25 >>, WEAPONTYPE_GRENADE, TRUE)
					OR IS_PROJECTILE_TYPE_IN_AREA(vGangStandingPosition[0] - << 25, 25, 25 >>, vGangStandingPosition[0] + << 25, 25, 25 >>, WEAPONTYPE_GRENADELAUNCHER, TRUE)
					OR IS_PROJECTILE_TYPE_IN_AREA(vGangStandingPosition[0] - << 25, 25, 25 >>, vGangStandingPosition[0] + << 25, 25, 25 >>, WEAPONTYPE_STICKYBOMB, TRUE)
						PRINTSTRING("\n@@@@@@@@@@@@@@@@ player spooked gang somehow @@@@@@@@@@@@@@@@\n")
						INT index
						REPEAT COUNT_OF(piGangOne) index
							IF NOT IS_PED_INJURED(piGangOne[index])
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								IF NOT IS_PED_INJURED(piGangOne[index])
									CLEAR_PED_TASKS(piGangOne[index])
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
										TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(piGangOne[index], seq)
									CLEAR_SEQUENCE_TASK(seq)
									SET_PED_KEEP_TASK(piGangOne[index], TRUE)
								ENDIF
							ENDIF
						ENDREPEAT
						IF NOT IS_PED_INJURED(piLeadingPed)
							IF NOT IS_PED_INJURED(piLeadingPed)
								CLEAR_PED_TASKS(piLeadingPed)
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(piLeadingPed, seq)
								CLEAR_SEQUENCE_TASK(seq)
								SET_PED_KEEP_TASK(piLeadingPed, TRUE)
							ENDIF
						ENDIF
						bEventFailed = TRUE
//						fleeAndFail(piLeadingPed, piGangOne)
//						bAggroed = TRUE
//						updateState = EUpdate_DoFighting
					ENDIF
				ELSE
					INT index
					REPEAT COUNT_OF(piGangOne) index
						IF NOT IS_PED_INJURED(piGangOne[index])
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piGangOne[index], PLAYER_PED_ID())
								PRINTSTRING("\n Player damaged the guy/s\n")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								updateState = EUpdate_DoFighting
							ENDIF
						ELSE
							PRINTSTRING("\n Player killed the guy/s\n")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							updateState = EUpdate_DoFighting
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF bScaredByPoliceVehicle
					event_state = EVENT_ENDING
				ENDIF
				
				//make gang kill player if he does not scram after mugging
				IF f_player_has_been_mugged
//					IF f_gang_warns_player_after_mugging = 0
//					AND TIMERA() > 10000
//						//are you still here? get out of here. We'll kill you hang around here
//						f_gang_warns_player_after_mugging = 1
//						IF NOT IS_ENTITY_DEAD(piGangOne[2])
//							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_WARN1", CONV_PRIORITY_AMBIENT_HIGH)
//						ENDIF
//					ENDIF
//					
//					IF TIMERA() > 20000
//					AND f_gang_warns_player_after_mugging = 1
//						IF NOT IS_ENTITY_DEAD(piGangOne[2])
//							CREATE_CONVERSATION(Gang_IntimidationConversation, "REGIAU", "REGIN_WARN2", CONV_PRIORITY_AMBIENT_HIGH)
//						ENDIF
//						f_gang_warns_player_after_mugging = 2
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							updateState = EUpdate_DoFighting
						ENDIF
//					ENDIF
				ENDIF
				
				//Add in all the player messing up stuff
				IF IS_PED_INJURED(PLAYER_PED_ID())
					waitTime = 0
					PRINTSTRING("***********************************")
					PRINTNL() 
					PRINTSTRING("Gang_Intimidation: Player is injured")
					PRINTNL()
					PRINTSTRING("***********************************")
					event_state = EVENT_ENDING
					BREAK
				ENDIF
				
				
				IF HAS_PLAYER_LEFT_RANDOM_EVENT(vMainPosition, RANGE_LARGE)
					PRINTSTRING("***********************************")
					PRINTNL()
					PRINTSTRING("Gang_Intimidation: HAS_PLAYER_LEFT_RANDOM_EVENT = TRUE")
					PRINTNL()
					PRINTSTRING("***********************************")	
					event_state = EVENT_ENDING
					BREAK
				ENDIF
				
//				IF bAggroed
//					PRINTSTRING("***********************************")
//					PRINTNL()
//					PRINTSTRING("Gang_Intimidation: bAggroed = TRUE")
//					PRINTNL()
//					PRINTSTRING("***********************************")	
//					event_state = EVENT_ENDING
//					BREAK
//				ENDIF
				
				//Normal running loop
				
				IF event_state <> EVENT_ENDING
					IF UpdateEvent(updateState, initialAsset, piLeadingPed, piGangOne, waitTime, bTaskGiven)
					OR bEventFailed
						PRINTSTRING("***********************************")
						PRINTNL()
						PRINTSTRING("Gang_Intimidation: UpdateEvent has finished fine")
						PRINTNL()
						PRINTSTRING("***********************************")
						event_state = EVENT_ENDING
						BREAK
					ENDIF
				ENDIF
				
			BREAK
			
			CASE EVENT_ENDING
				waitTime = 0
				Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
				event_state = EVENT_OVER
				BREAK
			BREAK
			
			CASE EVENT_OVER
				waitTime = 0
				
			BREAK
			
		ENDSWITCH
		
		IF Event_state <> EVENT_OVER 

			//WAIT(waitTime)
		ENDIF
	ENDWHILE
	
	Cleanup(initialAsset, bTaskGiven, piGangOne, piLeadingPed)
	
ENDSCRIPT
