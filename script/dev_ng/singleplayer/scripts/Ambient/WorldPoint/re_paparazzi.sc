
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING "commands_path.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "dialogue_public.sch"
USING "cutscene_public.sch"
USING "Ambient_Common.sch"
USING "locates_public.sch"
USING "flow_help_public.sch"
USING "buddy_head_track_public.sch"
USING "random_events_public.sch"
USING "altruist_cult.sch"
USING "commands_recording.sch"
USING "load_queue_public.sch"

CONST_INT NUMBER_OF_PAPS 8
CONST_INT NUMBER_OF_PAP_VEHS (NUMBER_OF_PAPS/2)
CONST_INT NUMBER_OF_STANDING_ANIMS 5

ENUM escapePapStages
	pescape_PICKUPGIRL,
	pescape_LOSEPAPS,
	pescape_DRIVEBACK,
	pescape_DROPOFF,
	pescape_DROPOFFONFOOT,
	pescape_CELEBFLEES,
	pescape_STROPSOFF,
	pescape_STOLEVEH,
	pescape_AT_CULT,
	pescape_TEST,
	pescape_CLEAN_UP,
	pescape_LOSECOPS
ENDENUM

escapePapStages thisStage = pescape_PICKUPGIRL
escapePapStages savedStage

#IF IS_DEBUG_BUILD
	
	FUNC STRING GET_STRING_FROM_MISSION_STAGE(escapePapStages stage)
		SWITCH stage
			CASE pescape_PICKUPGIRL		RETURN "pescape_PICKUPGIRL		"	BREAK
			CASE pescape_LOSEPAPS		RETURN "pescape_LOSEPAPS		"	BREAK
			CASE pescape_DRIVEBACK		RETURN "pescape_DRIVEBACK		"	BREAK
			CASE pescape_DROPOFF		RETURN "pescape_DROPOFF			"	BREAK
			CASE pescape_DROPOFFONFOOT	RETURN "pescape_DROPOFFONFOOT	"	BREAK
			CASE pescape_CELEBFLEES		RETURN "pescape_CELEBFLEES		"	BREAK
			CASE pescape_STROPSOFF		RETURN "pescape_STROPSOFF		"	BREAK
			CASE pescape_STOLEVEH		RETURN "pescape_STOLEVEH		"	BREAK
			CASE pescape_TEST			RETURN "pescape_TEST			"	BREAK
			CASE pescape_CLEAN_UP		RETURN "pescape_CLEAN_UP		"	BREAK
			CASE pescape_LOSECOPS		RETURN "pescape_LOSECOPS		"	BREAK
		ENDSWITCH
		RETURN "Invalid mission stage"
	ENDFUNC
	
#ENDIF

ENUM PAP_STAGE
	pap_init = 0,
	pap_wait,
	pap_first_seq,
	pap_chase,
	pap_magdemo_back_off,
	pap_unhandled,
	pap_cleanup
ENDENUM

PAP_STAGE currentPapStage = pap_init

VECTOR vInput
VECTOR vDropOffPos = << 226.6825, 681.0297, 188.4987 >>
VECTOR vDropOffOnFootPos = <<231.8002, 672.5261, 188.9459>>
FLOAT fDropOffOnFootHeading = 32.0905

//VECTOR vTriggerAreaPos1 = <<281.007416,192.480484,58.088165>>
//VECTOR vTriggerAreaPos2 = <<228.829086,50.886669,111.610443>> 
//FLOAT fTriggerAreaWidth = 82.750000

//VECTOR vShoutAreaPos1 = <<243.644272,123.719276,97.598976>>
//VECTOR vShoutAreaPos2 = <<262.435333,116.874611,107.476166>> 
//FLOAT fShoutAreaWidth = 32.250000

//VECTOR vAlleyWayPos1 = <<243.557053,77.512650,90.080025>>
//VECTOR vAlleyWayPos2 = <<268.208313,144.218857,109.853004>> 
//FLOAT fAlleyWayWidth = 23.250000

VECTOR vCurrentPlayerCoords
VECTOR vCurrentCelebCoords
VECTOR vCurrentCelebCarCoords

//VECTOR vFlashOffset = <<0.019, -0.010, 0.046>>
//VECTOR vFlashRotation = <<0,0,-180.0>>

REL_GROUP_HASH rgCeleb

BOOL bDebugTestingMagdemo = FALSE
BOOL bOnBike
//BOOL bRouteBLipped

BOOL bStarted = FALSE
BOOL bOnHold = FALSE
BOOL bMikeInsult = FALSE
TEXT_LABEL_23 sConvResumeLabel = ""
INT iConvTimer

INT iCumulativePapDamage = 0

INT iCelebCarDamageLastFrame
INT iDamageToCelebCar
INT iTimeOfBackOff

//#IF IS_DEBUG_BUILD
//
//	WIDGET_GROUP_ID papWidget
//	
//	PROC BUILD_WIDGETS()
//		papWidget = START_WIDGET_GROUP("RE_paparrazi")
//		ADD_WIDGET_BOOL("Test MAGDEMO2", bDebugTestingMagdemo)
//		ADD_WIDGET_BOOL("Conv on hold", bOnHold)
//		ADD_WIDGET_BOOL("Conv started", bStarted)
//		ADD_WIDGET_INT_READ_ONLY("iConv", iConvTimer)
//		ADD_WIDGET_VECTOR_SLIDER("cam offset", <<0.019, -0.010, 0.046>>, -0.5, 0.5, 0.001)
//		ADD_WIDGET_VECTOR_SLIDER("cam offset", <<0,0,-180.0>>, -180, 170, 0.001)
//		STOP_WIDGET_GROUP()
//	ENDPROC
//	
//#ENDIF

PED_INDEX pedCelebGirl
VECTOR vCreateCelebGirlPos = <<259.4603, 125.8718, 100.9754>>
FLOAT fCreateCelebGirlHdg = 283.0367

VEHICLE_INDEX vehCelebsCar
VECTOR vCreateCelebVehPos = <<245.1998, 76.8231, 90.7069>>
FLOAT fCreateCelebVehHdg = -18.4743

BOOL bPopulatedLoadQueue = FALSE
LoadQueueLight sLoadQueue

FUNC VECTOR GET_OFFSET_FROM_VEHICLE_FOR_PAP(INT iPap)
	
	SWITCH iPap
		CASE 0
			RETURN <<-0.6578, 3.3280, 0.2129>>
		BREAK	   
		CASE 1	   
			RETURN <<0.5781, 3.1584, 0.2203>>
		BREAK	   
		CASE 2
			RETURN <<1.6953, 2.4036, 0.1413>>
		BREAK	   
		CASE 3
			RETURN <<2.2778, 1.3153, 0.1534>>
		BREAK	   
		CASE 4
			RETURN <<2.0457, 0.0073, 0.2707>>
		BREAK
		CASE 5
			RETURN <<2.0970, -1.2978, 0.2300>>
		BREAK
		CASE 6
			RETURN <<2.0693, -3.2216, 0.3726>>
		BREAK
		CASE 7
			RETURN <<1.0275, -3.6726, 0.1550>>
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Invalid pap number passed to , GET_OFFSET_FROM_VEHICLE_FOR_PAP()")
			PRINTLN("Invalid pap number", iPap, " passed to , GET_OFFSET_FROM_VEHICLE_FOR_PAP()")
		BREAK
	ENDSWITCH	
	
	RETURN <<0,0,0>>
	
ENDFUNC

FUNC STRING GET_STANDING_ANIM_INDEX_FOR_PAP(INT iPap)

	SWITCH iPap
	
		CASE 0 CASE 5 RETURN "idle_a"	BREAK
		CASE 1 CASE 6 RETURN "idle_b"	BREAK
		CASE 2 CASE 7 RETURN "idle_c"	BREAK
		CASE 3 RETURN "idle_d"	BREAK
		CASE 4 RETURN "idle_e"	BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Invalid pap number passed to , GET_STANDING_ANIM_INDEX_FOR_PAP()")
			PRINTLN("Invalid pap number ", iPap, " passed to , GET_STANDING_ANIM_INDEX_FOR_PAP()")
		BREAK
		
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC INT GET_DRIVER_INDEX_FROM_PAP_VEH(INT iVeh)
	RETURN iVeh*2
ENDFUNC

FUNC INT GET_PASSENGER_INDEX_FROM_PAP_VEH(INT iVeh)
	RETURN iVeh*2+1
ENDFUNC

STRUCT PAPARAZZO

	PED_INDEX m_pap
	BLIP_INDEX m_blip
	OBJECT_INDEX m_cam
	VECTOR m_vFootTarget
	VECTOR m_vCoords
	BOOL m_bAiming
	BOOL m_bCamWalksSet
	BOOL m_bGrip
	INT m_iLastFlash
	INT m_iAimStart
	INT m_iVehIndex
	INT m_iPapAnims
	INT m_iFootChaseState
	INT m_iHealth
	
ENDSTRUCT

STRUCT PAP_VEH

	VEHICLE_INDEX m_veh
	BLIP_INDEX m_blip
	VECTOR m_vWarpOffset
	VECTOR m_vWarpPosition
	VECTOR m_vCoords
	VECTOR m_vTargetPosition
	FLOAT m_fTargetHeading
	FLOAT m_fWarpHeading
	INT m_iWarpTimer
	INT m_iChaseState
	
ENDSTRUCT

PAP_VEH vehPaps[NUMBER_OF_PAP_VEHS]
PAPARAZZO pedPaps[NUMBER_OF_PAPS]

FUNC PED_INDEX GET_PAP_FROM_PAP_VEH(INT iVehIndex, INT iPap)
	IF iPap = 0 
		RETURN pedPaps[GET_DRIVER_INDEX_FROM_PAP_VEH(iVehIndex)].m_pap
	ELSE
		RETURN pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(iVehIndex)].m_pap
	ENDIF
ENDFUNC

FUNC OBJECT_INDEX GET_CAM_FROM_PAP_VEH(INT iVehIndex, INT iPap)
	IF iPap = 0 
		RETURN pedPaps[GET_DRIVER_INDEX_FROM_PAP_VEH(iVehIndex)].m_cam
	ELSE
		RETURN pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(iVehIndex)].m_cam
	ENDIF
ENDFUNC

PROC INIT_PAP(INT iPapIndex)
	IF (iPapIndex%2) = 1
		pedPaps[iPapIndex].m_iVehIndex = (iPapIndex -1)/2
	ELSE
		pedPaps[iPapIndex].m_iVehIndex = iPapIndex/2
	ENDIF
ENDPROC

PROC INIT_PAP_VEH(INT iVehIndex)
	vehPaps[iVehIndex].m_iChaseState = 0
ENDPROC

INT iNumberOfPapsDead
BOOL bHitLinePending
BOOL bFirstHitLineDone
INT iLastHitLine
BOOL bWeaponLinePending
INT iLastWeaponLine
BOOL bShootLinePending
INT iLastShootLine

INT iNumberOfShootWarnings = 0

OBJECT_INDEX objFitTest

BLIP_INDEX blipInitialMarker
BLIP_INDEX blipCelebGirl
BLIP_INDEX blipCelebVeh
BLIP_INDEX blipDropOffInCar
BLIP_INDEX blipDropOffOnFoot
BLIP_INDEX blipCult
SCENARIO_BLOCKING_INDEX sbiGang, sbiHouse, sbiCar

BOOL bPlayersCarUsed = FALSE

INT iStageProgress
INT iTimeStartedChase

STRING sStropString	
STRING sBlockId = "REPAPAU"

STRING standingAnimDict = "random@escape_paparazzi@standing@"

STRING carAnimDict = "random@escape_paparazzi@incar@"
STRING carAnims[9]

INT iHighestDamage = 0
INT iDamageToPaps[NUMBER_OF_PAPS]

ENUM PAP_ANIM
	PAP_STANDING_IDLE_A = 0,
	PAP_STANDING_IDLE_B,
	PAP_STANDING_IDLE_C,
	PAP_STANDING_IDLE_D,
	PAP_STANDING_IDLE_E,
	PAP_CAR_IDLE_A,
	PAP_CAR_IDLE_B,
	PAP_CAR_IDLE_C,
	PAP_CAR_IDLE_D,
	PAP_CAR_IDLE_E,
	PAP_CAR_IDLE_F,
	PAP_CAR_IDLE_G,
	PAP_CAR_IDLE_H,
	PAP_CAR_IDLE_I,
	PAP_GRIP,
	MAX_PAP_ANIMS
ENDENUM

FUNC INT GET_STANDING_ANIM_INDEX_FROM_PAP_ANIM(PAP_ANIM eAnim)
	RETURN ENUM_TO_INT(eAnim)
ENDFUNC

FUNC INT GET_CAR_ANIM_INDEX_FROM_PAP_ANIM(PAP_ANIM eAnim)
	RETURN ENUM_TO_INT(eAnim) - NUMBER_OF_STANDING_ANIMS
ENDFUNC

FUNC BOOL IS_PAP_ANIM_CAR_ANIM(PAP_ANIM eAnim)
	INT iAnimIndex = ENUM_TO_INT(eAnim)
	RETURN iAnimIndex >= NUMBER_OF_STANDING_ANIMS AND iAnimIndex < NUMBER_OF_STANDING_ANIMS + COUNT_OF(carAnims)
ENDFUNC

PROC PLAY_ANIM_ON_PAP_INDEX(INT iPapIndex, PAP_ANIM eAnim)
	IF eAnim < MAX_PAP_ANIMS
		IF NOT IS_BIT_SET(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnim))
			IF IS_PAP_ANIM_CAR_ANIM(eAnim)
				TASK_PLAY_ANIM(pedPaps[iPapIndex].m_pap, carAnimDict, carAnims[GET_CAR_ANIM_INDEX_FROM_PAP_ANIM(eAnim)], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
			ELIF ENUM_TO_INT(eAnim) < NUMBER_OF_STANDING_ANIMS
				TASK_PLAY_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iPapIndex))
			ELSE
				TASK_PLAY_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, "grip", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ENDIF
			pedPaps[ipapIndex].m_iPapAnims = 0
			SET_BIT(pedPaps[ipapIndex].m_iPapAnims, ENUM_TO_INT(eAnim))
		ENDIF
	ENDIF
ENDPROC

PROC ADD_ALTRUIST_CULT_BLIP()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF NOT DOES_BLIP_EXIST(blipCult)
			PRINT_CULT_HELP()
			blipCult = CREATE_AMBIENT_BLIP_FOR_COORD(<< -1034.6, 4918.6, 205.9 >>)
			SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
			REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
			PRINTLN("<re_snatched> - ADD_ALTRUIST_CULT_BLIP()")
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALTRUIST_CULT_BLIP()
	IF DOES_BLIP_EXIST(blipCult)
		REMOVE_BLIP(blipCult)
		PRINTLN("<re_snatched> - REMOVE_ALTRUIST_CULT_BLIP()")
	ENDIF
ENDPROC

PROC UPDATE_DAMAGE_TO_PAPS()	
	INT i
	REPEAT COUNT_OF(pedPaps) i
		IF DOES_ENTITY_EXIST(pedPaps[i].m_pap)
			IF GET_ENTITY_HEALTH(pedPaps[i].m_pap) < pedPaps[i].m_iHealth
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPaps[i].m_pap, PLAYER_PED_ID())
					iDamageToPaps[i] += pedPaps[i].m_iHealth - GET_ENTITY_HEALTH(pedPaps[i].m_pap)
					iCumulativePapDamage += pedPaps[i].m_iHealth - GET_ENTITY_HEALTH(pedPaps[i].m_pap)
					PRINTLN("Did ", pedPaps[i].m_iHealth - GET_ENTITY_HEALTH(pedPaps[i].m_pap), " damage to pap no.", i)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedPaps[i].m_pap)
					IF iDamageToPaps[i] > iHighestDamage
						iHighestDamage = iDamageToPaps[i]
					ENDIF
				ENDIF
			ENDIF
			pedPaps[i].m_iHealth = GET_ENTITY_HEALTH(pedPaps[i].m_pap)
		ENDIF
	ENDREPEAT
ENDPROC

structPedsForConversation sSpeech

////===============================================================================
//// Functions ----------------------------------------------//
////===============================================================================
//
// Clean Up

PROC missionCleanup()

//	#IF IS_DEBUG_BUILD
//		IF DOES_WIDGET_GROUP_EXIST(papWidget)
//			DELETE_WIDGET_GROUP(papWidget)
//		ENDIF
//	#ENDIF
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	SET_ROADS_BACK_TO_ORIGINAL(<<-90, -1488, -50>>, <<55, -1470, 50>>)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
	
	//Unload any assets we may have in memory.
	CLEANUP_LOAD_QUEUE_LIGHT(sLoadQueue)
	REMOVE_ANIM_DICT("random@escape_paparazzi@standing@")
	REMOVE_ANIM_DICT("random@paparazzi@pap_anims")
	REMOVE_ANIM_DICT("random@paparazzi@peek")
	REMOVE_ANIM_DICT("random@paparazzi@trans")
	REMOVE_ANIM_DICT("random@paparazzi@wait")
	REMOVE_ANIM_DICT("veh@std@ps@idle_panic")
	REMOVE_ANIM_DICT("veh@low@front_ps@idle_panic")
	REMOVE_ANIM_DICT(carAnimDict)
	RELEASE_SCRIPT_AUDIO_BANK()
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		SET_WANTED_LEVEL_MULTIPLIER(1)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedCelebGirl)
	AND NOT IS_PED_INJURED(pedCelebGirl)
		IF IS_PED_IN_GROUP(pedCelebGirl)
			REMOVE_PED_FROM_GROUP(pedCelebGirl)
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(pedCelebGirl, TRUE)
			IF NOT IS_PED_FLEEING(pedCelebGirl)
				TASK_LEAVE_ANY_VEHICLE(pedCelebGirl)
			ENDIF
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	CLEAR_GPS_MULTI_ROUTE()
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(VADER, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SURANO, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CAVALCADE2, FALSE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiGang)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiHouse)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiCar)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<246.1673, 92.1704, 93.8361>> + <<6,6,6>>, <<246.1673, 92.1704, 93.8361>> - <<6,6,6>>, TRUE)
	
	IF g_bAltruistCultFinalDialogueHasPlayed
		TRIGGER_MUSIC_EVENT("AC_STOP")
	ENDIF
	
	RESET_CULT_STATUS()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC missionPassed()
	IF NOT IS_PED_INJURED(pedCelebGirl)
		SET_PED_KEEP_TASK(pedCelebGirl, TRUE)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rgCeleb, RELGROUPHASH_PLAYER) 
		IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
			SET_PED_CONFIG_FLAG(pedCelebGirl, PCF_CanSayFollowedByPlayerAudio, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCelebGirl, FALSE)	
		ENDIF
	ENDIF
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	UNLOCK_MISSION_NEWS_STORY(ENUM_TO_INT(NEWS_MISC_RE_PAP))
	PRINTLN("\n\n\n\n mission cleanup 3")
	missionCleanup()
ENDPROC

STRING sAudioScenes[3]

PROC SET_AUDIO_SCENE_ACTIVE(STRING pAudioScene)
	INT iTemp
	REPEAT COUNT_OF(sAudioScenes) iTemp
		IF NOT ARE_STRINGS_EQUAL(sAudioScenes[iTemp], pAudioScene)
			IF IS_AUDIO_SCENE_ACTIVE(sAudioScenes[iTemp])
				STOP_AUDIO_SCENE(sAudioScenes[iTemp])
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(sAudioScenes) iTemp
		IF ARE_STRINGS_EQUAL(sAudioScenes[iTemp], pAudioScene)
			IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioScenes[iTemp])
				START_AUDIO_SCENE(sAudioScenes[iTemp])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_PED_GROUPED(PED_INDEX ped)
	IF IS_PED_GROUP_MEMBER(ped, GET_PLAYER_GROUP(PLAYER_ID()))
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped, <<25,25,25>>)
			REMOVE_PED_FROM_GROUP(ped)
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped, g_vAnyMeansLocate)
			SET_PED_AS_GROUP_MEMBER(ped, GET_PLAYER_GROUP(PLAYER_ID()))
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_ASSETS_LOADED()

	IF NOT bPopulatedLoadQueue
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, SURANO)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, PROP_LD_TEST_01)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, A_F_Y_BEVHILLS_03)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, A_M_Y_GENSTREET_02)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, VADER)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, CAVALCADE2)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, PROP_PAP_CAMERA_01)
		LOAD_QUEUE_LIGHT_ADD_PTFX_ASSET(sLoadQueue)
		bPopulatedLoadQueue = TRUE
	ENDIF
	
	//Load the queued items first. We can't queue the string requests
	//as it will blow the script's stack.
	IF NOT HAS_LOAD_QUEUE_LIGHT_LOADED(sLoadQueue)
		RETURN FALSE
	ENDIF
	
	REQUEST_ANIM_DICT("random@escape_paparazzi@standing@")
	REQUEST_ANIM_DICT("random@paparazzi@pap_anims")
	REQUEST_ANIM_DICT("random@paparazzi@peek")
	REQUEST_ANIM_DICT("random@paparazzi@trans")
	REQUEST_ANIM_DICT("random@paparazzi@wait")
	REQUEST_ANIM_DICT("veh@std@ps@idle_panic")
	REQUEST_ANIM_DICT("veh@low@front_ps@idle_panic")
	REQUEST_ANIM_DICT(carAnimDict)
	REQUEST_ADDITIONAL_TEXT("ESCPAP", MISSION_TEXT_SLOT)
	
	IF HAS_ANIM_DICT_LOADED("random@escape_paparazzi@standing@")
	AND HAS_ANIM_DICT_LOADED("random@paparazzi@pap_anims")
	AND HAS_ANIM_DICT_LOADED("random@paparazzi@peek")
	AND HAS_ANIM_DICT_LOADED("random@paparazzi@trans")
	AND HAS_ANIM_DICT_LOADED("random@paparazzi@wait")
	AND HAS_ANIM_DICT_LOADED("veh@std@ps@idle_panic")
	AND HAS_ANIM_DICT_LOADED("veh@low@front_ps@idle_panic")
	AND HAS_ANIM_DICT_LOADED(carAnimDict)
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	AND REQUEST_SCRIPT_AUDIO_BANK("Distant_Camera_Flash")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



PROC MANAGE_CONVERSATION_FOR_DISTANCE(STRING label, VECTOR vCoord1, VECTOR vCoord2, FLOAT maxDist)
	IF VDIST(vCoord1, vCoord2) < maxDist
		IF NOT bOnHold
			CREATE_CONVERSATION(sSpeech, sBlockId, label, CONV_PRIORITY_AMBIENT_HIGH)
			bOnHold = FALSE
			bOnHold = TRUE
		ENDIF
		IF bOnHold
			CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, label, sConvResumeLabel, CONV_PRIORITY_AMBIENT_HIGH)
			bOnHold = FALSE
		ENDIF
	ENDIF
	IF VDIST(vCoord1, vCoord2) > maxDist
		IF NOT bOnHold
			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			bOnHold = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL MANAGE_CONVERSATION(STRING Label, BOOL bCanRun)

	IF bCanRun
	
		IF NOT bStarted
			IF CREATE_CONVERSATION(sSpeech, sBlockId, label, CONV_PRIORITY_AMBIENT_HIGH)
				bStarted = TRUE
				bOnHold = FALSE
			ENDIF
		ELSE
			IF bOnHold
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, label, sConvResumeLabel, CONV_PRIORITY_AMBIENT_HIGH)
					bOnHold = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bStarted
		AND NOT bOnHold
		AND bCanRun
			IF iConvTimer = -1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iConvTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF GET_GAME_TIMER() - iConvTimer > 500
					iConvTimer = -1
					bStarted = FALSE
					bOnHold = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	
		IF bStarted
		AND NOT bOnHold
			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			bOnHold = TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC FLOAT MIN(FLOAT a, FLOAT b)
	IF a<b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC FLOAT MAX(FLOAT a, FLOAT b)
	IF a>b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC BOOL DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(VEHICLE_INDEX veh, VEHICLE_INDEX vehTarget, OBJECT_INDEX objTest, VECTOR vPosition, VECTOR vRotation)

	BOOL bRet
	
	IF DOES_ENTITY_EXIST(objTest)
		VECTOR vFront, vBack
		VECTOR vDimMin, vDimMax
		VECTOR vPlayerDimMin, vPlayerDimMax
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vDimMin, vDimMax)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTarget), vPlayerDimMin, vPlayerDimMax)
		
		FLOAT fHeight = ABSF(vDimMax.z - vDimMin.z)
		FLOAT fWidth = ABSF(vDimMax.x - vDimMin.x)
		FLOAT fLength = ABSF(vDimMax.y - vDimMin.y)
		
		FLOAT fRad 
		FLOAT fBump
		IF fWidth > fHeight
			fRad = fWidth/2
			fBump = fRad - absf(vDimMin.z)
		ELSE
			fRad = fLength/2
			fBump = fRad - absf(vDimMin.x)
		ENDIF
		
		VECTOR vCoord
		vCoord.z += vPlayerDimMin.z - vDimMin.z
		
		SET_ENTITY_COORDS(objTest, vPosition)
		SET_ENTITY_ROTATION(objTest, vRotation)
		
		vFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,fLength/2 - fRad*0.75,fBump*1.25>> + vCoord)
		vBack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,-fLength/2 + fRad*0.75,fBump*1.25>> + vCoord)
		
		INT		iHitResults
		VECTOR	vReturnPosition
		VECTOR	vReturnNormal
		ENTITY_INDEX hitEntity
		
		SHAPETEST_INDEX ShapeTestCapsuleIndex = START_SHAPE_TEST_CAPSULE(vBack, vFront, fRad, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE, PLAYER_PED_ID())
		
		IF GET_SHAPE_TEST_RESULT(ShapeTestCapsuleIndex, iHitResults, vReturnPosition, vReturnNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test results not ready.")
			#ENDIF
		ENDIF
		
		IF (iHitResults != 0)
			bRet = FALSE
		ELSE
			bRet =  TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			INT iGreen
			INT iRed
			IF bRet 
				iGreen = 255
			ELSE
				iRed = 255
			ENDIF
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMin), <<0.8, 0.3, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMax), <<0.8, 0.32, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fHeight), <<0.8, 0.34, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fWidth), <<0.8, 0.36, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fLength), <<0.8, 0.38, 0>>)
			DRAW_DEBUG_SPHERE(vFront, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vFront, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_SPHERE(vBack, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vBack, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack-<<0, 0, fRad>>, vFront-<<0, 0, fRad>>, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack+<<0, 0, fRad>>, vFront+<<0, 0, fRad>>, iRed, iGreen, 0, 100)
		#ENDIF
		
	ENDIF
	
	RETURN bRet
	
ENDFUNC

PROC RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TargetVehicleIndex, VECTOR &vWarpPosition,
												   FLOAT &fWarpHeading, VECTOR &vLastTargetPosition, FLOAT &fLastTargetHeading,
												   INT &iWarpTimer, VECTOR vWarpOffset, INT iWarpTimeDelay, FLOAT fWarpDistance)
												   
	IF  DOES_ENTITY_EXIST(VehicleIndex)
	AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
		IF 	DOES_ENTITY_EXIST(TargetVehicleIndex)
		AND ( VehicleIndex <> TargetVehicleIndex)
		AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(TargetVehicleIndex))
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vLastTargetPosition) > 20.0
				IF DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(VehicleIndex, TargetVehicleIndex, objFitTest, GET_ENTITY_COORDS(TargetVehicleIndex), GET_ENTITY_ROTATION(TargetVehicleIndex))
					vWarpPosition 	= vLastTargetPosition	//store last target vehicle positon as the new warp position
					fWarpHeading 	= fLastTargetHeading	//store last target vehicle heading as the new warp heading
					vWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarpPosition, fWarpHeading, vWarpOffset)
					vLastTargetPosition = GET_ENTITY_COORDS(TargetVehicleIndex)
					fLastTargetHeading = GET_ENTITY_HEADING(TargetVehicleIndex)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_OCCLUDED(VehicleIndex)
			    iWarpTimer = GET_GAME_TIMER()
			ELIF GET_GAME_TIMER() - iWarpTimer > iWarpTimeDelay
				IF	GET_DISTANCE_BETWEEN_ENTITIES(VehicleIndex, TargetVehicleIndex) > fWarpDistance
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vWarpPosition) > 20.0
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWarpPosition) > 100
					IF NOT IS_VECTOR_ZERO(vWarpPosition)
						IF NOT IS_SPHERE_VISIBLE(vWarpPosition, 6.0)
							CLEAR_AREA_OF_PEDS(vWarpPosition, 6.0)
					        CLEAR_AREA_OF_VEHICLES(vWarpPosition, 6.0)
							SET_ENTITY_COORDS(VehicleIndex, vWarpPosition)
							SET_ENTITY_HEADING(VehicleIndex, fWarpHeading)
				            SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
				            SET_VEHICLE_FORWARD_SPEED(VehicleIndex, CLAMP(GET_ENTITY_SPEED(TargetVehicleIndex)+10, 10.0, 60.0))
				            SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
				            iWarpTimer = GET_GAME_TIMER()
						ENDIF
				    ENDIF
				ENDIF
			ENDIF
		ELSE
			iWarpTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
ENDPROC

PROC createScene()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vCreateCelebGirlPos)
	
	pedCelebGirl = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_03, vCreateCelebGirlPos, fCreateCelebGirlHdg)
	SET_PED_COMBAT_ATTRIBUTES(pedCelebGirl, CA_USE_VEHICLE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCelebGirl, CA_ALWAYS_FLEE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedCelebGirl, FA_DISABLE_HANDS_UP, TRUE)
	SET_PED_KEEP_TASK(pedCelebGirl, TRUE)
	SET_PED_CAN_BE_DRAGGED_OUT(pedCelebGirl, FALSE)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_HEAD, 0, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_HAIR, 0, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_TORSO, 0, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_LEG, 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_TEETH, 1, 0, 0) //(teef)
	SET_PED_COMPONENT_VARIATION(pedCelebGirl, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
	SET_PED_DIES_INSTANTLY_IN_WATER(pedCelebGirl, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCelebGirl, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedCelebGirl, FALSE)
	SET_AMBIENT_VOICE_NAME(pedCelebGirl, "LACEY")
//	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCelebGirl, KNOCKOFFVEHICLE_NEVER)
	
	IF g_bMagDemoActive OR bDebugTestingMagdemo
		SET_ENTITY_INVINCIBLE(pedCelebGirl, TRUE)
	ENDIF
	
	IF ADD_RELATIONSHIP_GROUP("CELEBRITY", rgCeleb)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCelebGirl, rgCeleb)
	ENDIF
	
	vehCelebsCar = CREATE_VEHICLE(SURANO, vCreateCelebVehPos, fCreateCelebVehHdg)
	SET_ENTITY_COORDS_NO_OFFSET(vehCelebsCar, vCreateCelebVehPos)
	SET_ENTITY_QUATERNION(vehCelebsCar, 0.1293, -0.0323, -0.1606, 0.9780)
	
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehCelebsCar, FALSE)
	SET_VEHICLE_COLOUR_COMBINATION(vehCelebsCar, 10)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehCelebsCar)
	SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY )
	SET_VEHICLE_DISABLE_TOWING(vehCelebsCar, TRUE)
	SET_VEHICLE_RADIO_LOUD(vehCelebsCar, TRUE)
	SET_VEH_RADIO_STATION(vehCelebsCar, GET_RADIO_STATION_NAME(1))
//	SET_CUSTOM_RADIO_TRACK_LIST("RADIO_02_POP", "HIDDEN_RADIO_STRIP_CLUB_MUSIC", TRUE)
	
	PRINTSTRING("add peds") PRINTNL()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
		PRINTSTRING("franklin") PRINTNL()
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		PRINTSTRING("michael") PRINTNL()
	ELSE
		ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "TREVOR")
		PRINTSTRING("trevor") PRINTNL()
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedCelebGirl, "LACEY")
	
	carAnims[0] = "idle_a"
	carAnims[1] = "idle_b"
	carAnims[2] = "idle_c"
	carAnims[3] = "idle_d"
	carAnims[4] = "idle_e"
	carAnims[5] = "idle_f"
	carAnims[6] = "idle_g"
	carAnims[7] = "idle_h"
	carAnims[8] = "idle_i"
	
	sAudioScenes[0] = "MAG_2_ESCAPE_PAP_GET_CAR"
	sAudioScenes[1] = "MAG_2_ESCAPE_PAP_CHASE"
	sAudioScenes[2] = "MAG_2_ESCAPE_PAP_TAKE_ACTRESS_HOME"
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(VADER, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SURANO, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CAVALCADE2, TRUE)
	
ENDPROC

FUNC BOOL ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
	IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
		IF NOT IS_PED_INJURED(pedCelebGirl)
			IF IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
				RETURN TRUE
			ENDIF		
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL CAN_PLAYER_AND_CELEB_CHAT()
	IF bPlayersCarUsed
		IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
			IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
				RETURN TRUE
			ENDIF
		ENDIF
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_IN_ANY_VEHICLE(pedCelebGirl)
			IF VDIST2(vCurrentPlayerCoords, vCurrentCelebCoords) < 20*20
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_SIGNED_ANGLE_BETWEEN_2D_VECTORS(VECTOR vVectorA, VECTOR vVectorB)
	FLOAT return_angle
	vVectorA = NORMALISE_VECTOR(vVectorA)
	vVectorB = NORMALISE_VECTOR(vVectorB)
	return_angle = (ATAN2(vVectorB.y, vVectorB.x) - ATAN2(vVectorA.y, vVectorA.x))
	IF return_angle > 180
		return_angle = (return_angle - 360)
	ELIF return_angle < -180
		return_angle = (return_angle + 360)
	ENDIF
	RETURN return_angle
ENDFUNC

FUNC INT GET_CLOSEST_PAP_VEHICLE_INDEX()
	INT iTemp
	INT iClosestVeh = -1
	FLOAT fClosestDist = 999999
	REPEAT COUNT_OF(vehPaps) iTemp
		IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
		AND IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
		AND NOT IS_PED_INJURED(pedCelebGirl)
			IF VDIST2(vCurrentCelebCoords, vehPaps[iTemp].m_vCoords) < POW(fClosestDist,2)
				fClosestDist = VDIST(vCurrentCelebCoords,vehPaps[iTemp].m_vCoords)
				iClosestVeh = iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	IF iClosestVeh <> -1
		RETURN iClosestVeh
	ENDIF
	RETURN -1
ENDFUNC

FUNC PED_INDEX GET_CLOSEST_PAP()
	INT iTemp
	INT iClosestPapPed = -1
	FLOAT fClosestDist = 999999
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedCelebGirl)
			IF VDIST2(vCurrentCelebCoords, pedPaps[iTemp].m_vCoords) < fClosestDist
				fClosestDist = VDIST2(vCurrentCelebCoords, pedPaps[iTemp].m_vCoords)
				iClosestPapPed = iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	IF iClosestPapPed <> -1
		RETURN pedPaps[iClosestPapPed].m_pap
	ENDIF
	RETURN NULL
ENDFUNC

FUNC INT GET_CLOSEST_PAP_INDEX()
	INT iTemp
	INT iClosestPapPed = -1
	FLOAT fClosestDist = 999999
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedCelebGirl)
			IF VDIST2(vCurrentPlayerCoords, pedPaps[iTemp].m_vCoords) < fClosestDist
				fClosestDist = VDIST2(vCurrentCelebCoords, pedPaps[iTemp].m_vCoords)
				iClosestPapPed = iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iClosestPapPed
ENDFUNC

INT iLastPapSpeech
INT iNextPapSpeechLag = 4000

PROC DO_PAP_LINES()
	IF (GET_GAME_TIMER() - iLastPapSpeech) > iNextPapSpeechLag
		IF NOT IS_PED_INJURED(pedCelebGirl)
		AND DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
		AND NOT IS_PED_INJURED(GET_CLOSEST_PAP())
			IF GET_DISTANCE_BETWEEN_COORDS(vCurrentCelebCoords, GET_ENTITY_COORDS(GET_CLOSEST_PAP())) < 8
				INT iRan = GET_RANDOM_INT_IN_RANGE(0,5)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 7)
				SWITCH iRan
					CASE 0
						ADD_PED_FOR_DIALOGUE(sSpeech, 4, GET_CLOSEST_PAP(), "PAPARAZZO")
						IF DOES_ENTITY_EXIST(sSpeech.PedInfo[4].Index)
						AND NOT IS_PED_INJURED(sSpeech.PedInfo[4].Index)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PP", CONV_PRIORITY_AMBIENT_HIGH)
								iLastPapSpeech = GET_GAME_TIMER()
								iNextPapSpeechLag = GET_RANDOM_INT_IN_RANGE(2000, 3000)
							ENDIF
						ENDIF
					BREAK
					CASE 1
						ADD_PED_FOR_DIALOGUE(sSpeech, 4, GET_CLOSEST_PAP(), "PAPARAZZO")
						IF DOES_ENTITY_EXIST(sSpeech.PedInfo[4].Index)
						AND NOT IS_PED_INJURED(sSpeech.PedInfo[4].Index)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CALL1", CONV_PRIORITY_AMBIENT_HIGH)
								iLastPapSpeech = GET_GAME_TIMER()
								iNextPapSpeechLag = GET_RANDOM_INT_IN_RANGE(2000, 3000)
							ENDIF
						ENDIF
					BREAK
					CASE 2
						ADD_PED_FOR_DIALOGUE(sSpeech, 5, GET_CLOSEST_PAP(), "PAP2")
						IF DOES_ENTITY_EXIST(sSpeech.PedInfo[5].Index)
						AND NOT IS_PED_INJURED(sSpeech.PedInfo[5].Index)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CALL2", CONV_PRIORITY_AMBIENT_HIGH)
								iLastPapSpeech = GET_GAME_TIMER()
								iNextPapSpeechLag = GET_RANDOM_INT_IN_RANGE(2000, 3000)
							ENDIF
						ENDIF
					BREAK
					CASE 3
						ADD_PED_FOR_DIALOGUE(sSpeech, 6, GET_CLOSEST_PAP(), "PAP3")
						IF DOES_ENTITY_EXIST(sSpeech.PedInfo[6].Index)
						AND NOT IS_PED_INJURED(sSpeech.PedInfo[6].Index)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CALL3", CONV_PRIORITY_AMBIENT_HIGH)
								iLastPapSpeech = GET_GAME_TIMER()
								iNextPapSpeechLag = GET_RANDOM_INT_IN_RANGE(2000, 3000)
							ENDIF
						ENDIF
					BREAK
					CASE 4
						ADD_PED_FOR_DIALOGUE(sSpeech, 7, GET_CLOSEST_PAP(), "PAP4")
						IF DOES_ENTITY_EXIST(sSpeech.PedInfo[7].Index)
						AND NOT IS_PED_INJURED(sSpeech.PedInfo[7].Index)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CALL4", CONV_PRIORITY_AMBIENT_HIGH)
								iLastPapSpeech = GET_GAME_TIMER()
								iNextPapSpeechLag = GET_RANDOM_INT_IN_RANGE(2000, 3000)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iCurrentFlasher
INT iLastFlashCheck

PROC DO_FLASH_BULBS()
	IF GET_GAME_TIMER() - iLastFlashCheck > 100
		IF (GET_GAME_TIMER() - pedPaps[iCurrentFlasher].m_iLastFlash) > 500
			IF DOES_ENTITY_EXIST(pedPaps[iCurrentFlasher].m_pap)
			AND NOT IS_PED_INJURED(pedPaps[iCurrentFlasher].m_pap)
				BOOL bCanFlash = TRUE
				IF NOT pedPaps[iCurrentFlasher].m_bAiming
				AND IS_PED_IN_ANY_VEHICLE(pedPaps[iCurrentFlasher].m_pap)
					bCanFlash = FALSE
				ENDIF
				IF bCanFlash
					IF (IS_ENTITY_PLAYING_ANIM(pedPaps[iCurrentFlasher].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iCurrentFlasher))
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iCurrentFlasher].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iCurrentFlasher)) > 0.5 
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iCurrentFlasher].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iCurrentFlasher)) < 0.75)
					OR pedPaps[iCurrentFlasher].m_bAiming
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", pedPaps[iCurrentFlasher].m_cam, <<0.019, -0.010, 0.046>>, <<0,0,-180.0>>)
//						START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_rcpap1_camera", pedPaps[iCurrentFlasher].m_pap, <<0,0,0>>, get_entity_rotation(pedPaps[iCurrentFlasher].m_cam), BONETAG_PH_R_HAND)
						IF iCurrentFlasher % 3 = 0
							PRINTLN("playing SHUTTER_FLASH")
							PLAY_SOUND_FROM_COORD(-1, "SHUTTER_FLASH", GET_ENTITY_COORDS(pedPaps[iCurrentFlasher].m_cam), "CAMERA_FLASH_SOUNDSET")
						ELIF iCurrentFlasher % 3 = 1
							PRINTLN("playing SHUTTER")
							PLAY_SOUND_FROM_COORD(-1, "SHUTTER", GET_ENTITY_COORDS(pedPaps[iCurrentFlasher].m_cam), "CAMERA_FLASH_SOUNDSET")
						ELSE
							PRINTLN("playing FLASH")
							PLAY_SOUND_FROM_COORD(-1, "FLASH", GET_ENTITY_COORDS(pedPaps[iCurrentFlasher].m_cam), "CAMERA_FLASH_SOUNDSET")
						ENDIF
						pedPaps[iCurrentFlasher].m_iLastFlash = GET_GAME_TIMER()
					ENDIF
					iLastFlashCheck = GET_GAME_TIMER()
				ENDIF
			ENDIF
			iCurrentFlasher++
			IF iCurrentFlasher >= COUNT_OF(pedPaps)
				iCurrentFlasher = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_STAGE(escapePapStages stage)
	PRINTLN("Setting stage to ", GET_STRING_FROM_MISSION_STAGE(stage))
	thisStage = stage
	iStageProgress = 0
ENDPROC

PROC CREATE_PAP_ON_FOOT(INT iPapNumber, VECTOR vCoords, FLOAT fHeading)

	pedPaps[iPapNumber].m_pap = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GENSTREET_02, vCoords, fHeading)
	SET_PED_COMBAT_ATTRIBUTES(pedPaps[iPapNumber].m_pap, CA_USE_VEHICLE, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPaps[iPapNumber].m_pap, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedPaps[iPapNumber].m_pap, CA_USE_COVER, TRUE)
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedPaps[iPapNumber].m_pap, KNOCKOFFVEHICLE_HARD)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedPaps[iPapNumber].m_pap, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedPaps[iPapNumber].m_pap, CA_ALWAYS_FLEE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedPaps[iPapNumber].m_pap, FA_USE_VEHICLE, FALSE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedPaps[iPapNumber].m_pap, FALSE)
	SET_PED_KEEP_TASK(pedPaps[iPapNumber].m_pap, TRUE)
	
	SET_PED_SHOOT_RATE(pedPaps[iPapNumber].m_pap, 100)
	SET_PED_FIRING_PATTERN(pedPaps[iPapNumber].m_pap, FIRING_PATTERN_FULL_AUTO)                               
	SET_PED_CONFIG_FLAG(pedPaps[iPapNumber].m_pap, PCF_ForcedAim, TRUE)
	SET_PED_CONFIG_FLAG(pedPaps[iPapNumber].m_pap, PCF_UseKinematicModeWhenStationary, FALSE)
	SET_ENTITY_HEALTH(pedPaps[ipapNumber].m_pap, 300)
	
	SET_PED_COMPONENT_VARIATION(pedPaps[iPapNumber].m_pap, PED_COMP_TORSO, iPapNumber%2, FLOOR(TO_FLOAT(ipapNumber)/2.0))
	
	IF g_bMagDemoActive OR bDebugTestingMagdemo
		SET_ENTITY_INVINCIBLE(pedPaps[ipapNumber].m_pap, TRUE)
		SET_ENTITY_HEALTH(pedPaps[ipapNumber].m_pap, 500)
	ENDIF
	
	SET_PED_TARGET_LOSS_RESPONSE(pedPaps[iPapNumber].m_pap, TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(pedPaps[iPapNumber].m_pap, CA_DO_DRIVEBYS, TRUE)
	
	SET_PED_MAX_MOVE_BLEND_RATIO(pedPaps[iPapNumber].m_pap, PEDMOVEBLENDRATIO_RUN)
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedPaps[iPapNumber].m_pap, FALSE)
	SET_PED_HELMET(pedPaps[iPapNumber].m_pap, FALSE)
	
	pedPaps[iPapNumber].m_cam = CREATE_OBJECT(PROP_PAP_CAMERA_01, GET_PED_BONE_COORDS(pedPaps[iPapNumber].m_pap, BONETAG_PH_R_HAND, <<0,0,0>>))
	ATTACH_ENTITY_TO_ENTITY(pedPaps[iPapNumber].m_cam, pedPaps[iPapNumber].m_pap, GET_PED_BONE_INDEX(pedPaps[iPapNumber].m_pap, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
	IF DOES_ENTITY_EXIST(pedPaps[0].m_cam)
		SET_ENTITY_VISIBLE(pedPaps[0].m_cam, FALSE)
	ENDIF
	
	pedPaps[iPapNumber].m_iHealth = GET_ENTITY_HEALTH(pedPaps[iPapNumber].m_pap)
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 strTemp
		strTemp = "pap no. "
		strTemp += iPapNumber
		SET_PED_NAME_DEBUG(pedPaps[iPapNumber].m_pap, strTemp)
	#ENDIF
	
ENDPROC

PROC CREATE_PAP_VEHICLE(INT iPapVehNumber, VECTOR vCoords, FLOAT fHeading, BOOL bAddPeds = FALSE)

	IF (iPapVehNumber%2) = 1
		vehPaps[iPapVehNumber].m_veh = CREATE_VEHICLE(VADER, vCoords, fHeading)
	ELSE
		vehPaps[iPapVehNumber].m_veh = CREATE_VEHICLE(CAVALCADE2, vCoords, fHeading)
		SET_VEHICLE_COLOUR_COMBINATION(vehPaps[iPapVehNumber].m_veh, 0)
	ENDIF
	
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPaps[iPapVehNumber].m_veh)
	SET_VEHICLE_STRONG(vehPaps[iPapVehNumber].m_veh, TRUE)
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL strTemp = "Pap vehicle: "
		strTemp += iPapVehNumber
		SET_VEHICLE_NAME_DEBUG(vehPaps[iPapVehNumber].m_veh, strTemp)
	#ENDIF
	
	IF bAddPeds
		IF NOT DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 0))
			CREATE_PAP_ON_FOOT(GET_DRIVER_INDEX_FROM_PAP_VEH(iPapVehNumber), vCoords, fHeading)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 1))
			CREATE_PAP_ON_FOOT(GET_PASSENGER_INDEX_FROM_PAP_VEH(iPapVehNumber), vCoords, fHeading)
		ENDIF
		IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 0))
		AND IS_VEHICLE_DRIVEABLE(vehPaps[iPapVehNumber].m_veh)
			SET_PED_INTO_VEHICLE(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 0), vehPaps[iPapVehNumber].m_veh)
		ENDIF
		IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 1))
		AND IS_VEHICLE_DRIVEABLE(vehPaps[iPapVehNumber].m_veh)
			SET_PED_INTO_VEHICLE(GET_PAP_FROM_PAP_VEH(iPapVehNumber, 1), vehPaps[iPapVehNumber].m_veh, VS_FRONT_RIGHT)
		ENDIF
		SET_VEHICLE_ENGINE_ON(vehPaps[iPapVehNumber].m_veh, TRUE, TRUE)
	ENDIF
	
ENDPROC

PROC BLIP_PAPS()
	INT iTemp
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_BLIP_EXIST(pedPaps[iTemp].m_blip)
			IF IS_PED_INJURED(pedPaps[iTemp].m_pap)
				REMOVE_BLIP(pedPaps[iTemp].m_blip)
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(pedPaps[iTemp].m_pap)
					REMOVE_BLIP(pedPaps[iTemp].m_blip)
				ENDIF				
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
			AND NOT IS_PED_IN_ANY_VEHICLE(pedPaps[iTemp].m_pap)
				pedPaps[iTemp].m_blip = CREATE_BLIP_FOR_PED(pedPaps[iTemp].m_pap, TRUE)
				SET_BLIP_NAME_FROM_TEXT_FILE(pedPaps[iTemp].m_blip, "EP_PAPBLIP")
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(vehPaps) iTemp
		IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
			IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip) 
			
				VECTOR vBlipPosition 	= GET_BLIP_COORDS(vehPaps[iTemp].m_blip)
				VECTOR vVehiclePosition = vehPaps[iTemp].m_vCoords
				
				vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 10.0)
				vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 10.0)
				vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 10.0)

				SET_BLIP_COORDS(vehPaps[iTemp].m_blip, vBlipPosition)
				IF IS_VEHICLE_EMPTY(vehPaps[iTemp].m_veh, TRUE)
					REMOVE_BLIP(vehPaps[iTemp].m_blip)
				ENDIF
				
			ELSE
			
				IF NOT IS_VEHICLE_EMPTY(vehPaps[iTemp].m_veh, TRUE)
					vehPaps[iTemp].m_blip = CREATE_BLIP_FOR_COORD(vehPaps[iTemp].m_vCoords)
					SET_BLIP_COLOUR(vehPaps[iTemp].m_blip, BLIP_COLOUR_RED)
					SET_BLIP_NAME_FROM_TEXT_FILE(vehPaps[iTemp].m_blip, "EP_PAPBLIP")
					SET_BLIP_PRIORITY(vehPaps[iTemp].m_blip, BLIPPRIORITY_HIGHEST)
				ENDIF
				
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip)
				REMOVE_BLIP(vehPaps[iTemp].m_blip)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_PAP_GROUP_LOST(INT vehNo, FLOAT fDist = 150.0)
	BOOL bLost = TRUE
	IF DOES_ENTITY_EXIST(vehPaps[vehNo].m_veh)
		IF IS_VEHICLE_DRIVEABLE(vehPaps[vehNo].m_veh)
			IF VDIST2(vCurrentPlayerCoords, vehPaps[vehNo].m_vCoords) < fDist * fDist
			OR NOT IS_ENTITY_OCCLUDED(vehPaps[vehNo].m_veh)
				bLost = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(vehNo, 0))
		IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(vehNo, 0))
			IF VDIST2(vCurrentPlayerCoords, pedPaps[GET_DRIVER_INDEX_FROM_PAP_VEH(vehNo)].m_vCoords) < fDist * fDist
			OR NOT IS_ENTITY_OCCLUDED(GET_PAP_FROM_PAP_VEH(vehNo, 0))
				bLost = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(vehNo, 1))
		IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(vehNo, 1))
			IF VDIST2(vCurrentPlayerCoords, pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(vehNo)].m_vCoords) < fDist * fDist
			OR NOT IS_ENTITY_OCCLUDED(GET_PAP_FROM_PAP_VEH(vehNo, 1))
				bLost = FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN bLost
ENDFUNC

PROC DELETE_PAP_GROUP(INT vehNo)
	IF DOES_ENTITY_EXIST(vehPaps[vehNo].m_veh)
	AND IS_ENTITY_OCCLUDED(vehPaps[vehNo].m_veh)
		DELETE_VEHICLE(vehPaps[vehNo].m_veh)
	ENDIF
	IF DOES_BLIP_EXIST(vehPaps[vehNo].m_blip)
		REMOVE_BLIP(vehPaps[vehNo].m_blip)
	ENDIF
	INT iTemp
	FOR iTemp = 0 TO 1
		INT iPap
		IF iTemp = 1
			iPap = GET_PASSENGER_INDEX_FROM_PAP_VEH(vehNo)
		ELSE
			iPap = GET_DRIVER_INDEX_FROM_PAP_VEH(vehNo)
		ENDIF
		IF DOES_ENTITY_EXIST(pedPaps[iPap].m_pap)
		AND IS_ENTITY_OCCLUDED(pedPaps[iPap].m_pap)
			IF DOES_ENTITY_EXIST(pedPaps[iPap].m_pap)
				DELETE_OBJECT(pedPaps[iPap].m_cam)
			ENDIF
			IF DOES_BLIP_EXIST(pedPaps[iPap].m_blip)
				REMOVE_BLIP(pedPaps[iPap].m_blip)
			ENDIF
			DELETE_PED(pedPaps[iPap].m_pap)
		ENDIF
	ENDFOR
ENDPROC

FUNC INT GET_FIRST_AVAILABLE_PAP_GROUP()
	INT iTemp
	REPEAT COUNT_OF(vehPaps) iTemp
		IF NOT DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
		AND NOT DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(iTemp, 0))
		AND NOT DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(iTemp, 1))
			RETURN iTemp
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC MODEL_NAMES GET_RANDOM_PAP_VEH_MODEL()
	MODEL_NAMES model
	IF GET_RANDOM_INT_IN_RANGE(0,2) <> 0
		model = VADER
	ELSE
		model = CAVALCADE2
	ENDIF
	RETURN model
ENDFUNC

FUNC VECTOR GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(VECTOR v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	RETURN n
ENDFUNC

FUNC FLOAT GET_ANGLE_BETWEEN_TWO_VECTORS(VECTOR vVectorA, VECTOR vVectorB)
	FLOAT return_angle
	vVectorA = NORMALISE_VECTOR(vVectorA)
	vVectorB = NORMALISE_VECTOR(vVectorB)
	return_angle = (ATAN2(vVectorB.y, vVectorB.x) - ATAN2(vVectorA.y, vVectorA.x))
	IF return_angle > 180
		return_angle = (return_angle - 360)
	ELIF return_angle < -180
		return_angle = (return_angle + 360)
	ENDIF
	RETURN return_angle
ENDFUNC

FUNC BOOL IS_PAP_PLAYING_ANY_IN_CAR_ANIM(INT iPapIndex)
	BOOL bRet = FALSE
	PAP_ANIM eAnimCounter
	IF NOT IS_STRING_NULL_OR_EMPTY(carAnimDict)
		FOR eAnimCounter = PAP_CAR_IDLE_A TO PAP_CAR_IDLE_I
			IF NOT bRet
				IF IS_BIT_SET(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnimCounter))
					bRet = TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN bRet
ENDFUNC

PROC TAKE_CAR_PIC_TOWARD_CELEB(INT iPapIndex)
	IF NOT IS_PED_INJURED(pedPaps[iPapIndex].m_pap)
	AND NOT IS_PED_INJURED(pedCelebGirl)
		PAP_ANIM eAnim = MAX_PAP_ANIMS
		VECTOR vToCeleb = vCurrentCelebCoords - pedPaps[iPapIndex].m_vCoords
		VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(pedPaps[iPapIndex].m_pap)
		FLOAT fAngle = GET_ANGLE_BETWEEN_TWO_VECTORS(vForward, vToCeleb)
		IF ABSF(fAngle) < 10
			eAnim = PAP_CAR_IDLE_A
		ELIF fAngle >= 10
		AND fAngle < 60
			eAnim = PAP_CAR_IDLE_C
		ELIF fAngle >= 60
		AND fAngle < 100
			eAnim = PAP_CAR_IDLE_B
		ELIF fAngle <= -10
		AND fAngle > -60
			eAnim = PAP_CAR_IDLE_F
		ELIF fAngle < -60
		AND fAngle > -100
			eAnim = PAP_CAR_IDLE_E
		ENDIF
		IF eAnim != MAX_PAP_ANIMS
			PLAY_ANIM_ON_PAP_INDEX(iPapIndex, eAnim)
		ENDIF
	ENDIF
ENDPROC

INT iCurrentVehToCheck = 0

//This function needs addressing TODAY (26/03) as it's too expensive!

PROC TAKE_PICS_FROM_VEHICLE()

	IF NOT IS_PED_INJURED(pedCelebGirl)
	
		INT iPassenger = GET_PASSENGER_INDEX_FROM_PAP_VEH(iCurrentVehToCheck)
		IF DOES_ENTITY_EXIST(vehPaps[iCurrentVehToCheck].m_veh)
		AND IS_VEHICLE_DRIVEABLE(vehPaps[iCurrentVehToCheck].m_veh)
		AND DOES_ENTITY_EXIST(pedPaps[iPassenger].m_pap)
		AND NOT IS_PED_INJURED(pedPaps[iPassenger].m_pap)
			IF IS_VEHICLE_MODEL(vehPaps[iCurrentVehToCheck].m_veh, CAVALCADE2)
				IF VDIST2(pedPaps[iPassenger].m_vCoords, vCurrentCelebCoords) < POW(30,2)
				AND GET_PED_IN_VEHICLE_SEAT(vehPaps[iCurrentVehToCheck].m_veh, VS_FRONT_RIGHT) = pedPaps[iPassenger].m_pap
					IF NOT pedPaps[iPassenger].m_bAiming
						IF NOT IS_PAP_PLAYING_ANY_IN_CAR_ANIM(iPassenger)
							TAKE_CAR_PIC_TOWARD_CELEB(iPassenger)
						ENDIF
					ELSE
						IF NOT IS_PAP_PLAYING_ANY_IN_CAR_ANIM(iPassenger)
						AND (GET_GAME_TIMER() - pedPaps[iPassenger].m_iAimStart) > 1000
							IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPassenger].m_pap, carAnimDict, "base")
								TASK_PLAY_ANIM(pedPaps[iPassenger].m_pap, carAnimDict, "base", NORMAL_BLEND_IN, INSTANT_BLEND_IN, -1, AF_UPPERBODY)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT pedPaps[iPassenger].m_bAiming
					IF VDIST2(pedPaps[iPassenger].m_vCoords, vCurrentCelebCoords) < POW(30,2)
					AND GET_PED_IN_VEHICLE_SEAT(vehPaps[iCurrentVehToCheck].m_veh, VS_FRONT_RIGHT) = pedPaps[iPassenger].m_pap
						TASK_SWEEP_AIM_ENTITY(pedPaps[iPassenger].m_pap, "random@paparazzi@pap_anims", "sweep_low", "sweep_med", "sweep_high", -1, pedCelebGirl)
						pedPaps[iPassenger].m_bAiming = TRUE
						pedPaps[iPassenger].m_iAimStart = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF pedPaps[iPassenger].m_bAiming
						IF (GET_GAME_TIMER() - pedPaps[iPassenger].m_iAimStart) > 2000
							pedPaps[iPassenger].m_bAiming = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iCurrentVehToCheck++
		IF iCurrentVehToCheck >= COUNT_OF(vehPaps)
			iCurrentVehToCheck = 0
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PAP_PHOTOGRAPH_PED_ON_FOOT(PED_INDEX ped, INT iPapIndex)
	IF NOT IS_PED_INJURED(pedPaps[iPapIndex].m_pap)
	AND NOT IS_PED_INJURED(ped)
		IF NOT IS_PED_FACING_PED(pedPaps[iPapIndex].m_pap, ped, 20)
			IF GET_SCRIPT_TASK_STATUS(pedPaps[iPapIndex].m_pap, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
				TASK_TURN_PED_TO_FACE_ENTITY(pedPaps[iPapIndex].m_pap, ped, 0)
			ENDIF
		ELSE
			IF (GET_GAME_TIMER() - pedPaps[iPapIndex].m_iAimStart) > 2000
				IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iPapIndex))
					TASK_PLAY_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iPapIndex), SLOW_BLEND_IN, SLOW_BLEND_OUT)
					pedPaps[iPapIndex].m_iAimStart = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_TO_AND_FACE_PED(VECTOR vRunTo, INT iPapIndex, PED_INDEX ped)
	VECTOR vToPed = NORMALISE_VECTOR(GET_ENTITY_COORDS(ped) - vRunTo)
	FLOAT fHead = GET_HEADING_FROM_VECTOR_2D(vToPed.x, vToPed.y)
	pedPaps[iPapIndex].m_vFootTarget = vRunTo
	CLEAR_PED_TASKS(pedPaps[iPapIndex].m_pap)
	IF VDIST(vRunTo, pedPaps[iPapIndex].m_vCoords) < 2
		TASK_FOLLOW_NAV_MESH_TO_COORD(pedPaps[iPapIndex].m_pap, pedPaps[iPapIndex].m_vFootTarget, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.5, ENAV_STOP_EXACTLY, fHead)
	ELSE
		TASK_FOLLOW_NAV_MESH_TO_COORD(pedPaps[iPapIndex].m_pap, pedPaps[iPapIndex].m_vFootTarget, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 0.5, ENAV_STOP_EXACTLY, fHead)
	ENDIF
ENDPROC

PROC PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(PED_INDEX ped, INT iPapIndex)//, FLOAT radius)

	IF NOT IS_PED_INJURED(pedPaps[iPapIndex].m_pap)
	AND NOT IS_PED_INJURED(ped)
		VECTOR vRunTo = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, GET_OFFSET_FROM_VEHICLE_FOR_PAP(iPapIndex))
		SWITCH pedPaps[iPapIndex].m_iFootChaseState
			CASE 0
				RUN_TO_AND_FACE_PED(vRunTo, iPapIndex, ped)
				pedPaps[iPapIndex].m_iFootChaseState++
			BREAK
			CASE 1
				IF GET_SCRIPT_TASK_STATUS(pedPaps[iPapIndex].m_pap, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				OR VDIST(vRunTo, pedPaps[iPapIndex].m_vCoords) > 5
					pedPaps[iPapIndex].m_iFootChaseState++
				ENDIF
			BREAK
			CASE 2
				IF VDIST(vRunTo, pedPaps[iPapIndex].m_vCoords) > 10
					RUN_TO_AND_FACE_PED(vRunTo, iPapIndex, ped)
					pedPaps[iPapIndex].m_iFootChaseState++
				ELSE
					PAP_PHOTOGRAPH_PED_ON_FOOT(ped, iPapIndex)
				ENDIF
			BREAK
			CASE 3
				IF VDIST2(vRunTo, pedPaps[iPapIndex].m_vFootTarget) > 5*5
					RUN_TO_AND_FACE_PED(vRunTo, iPapIndex, ped)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedPaps[iPapIndex].m_pap, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
					pedPaps[iPapIndex].m_iFootChaseState--
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC VECTOR GET_RANDOM_VECTOR_OF_LENGTH(FLOAT length, BOOL bXYOnly = TRUE)
	VECTOR retVec
	retVec.x = GET_RANDOM_FLOAT_IN_RANGE(-1, 1) 
	retVec.y = GET_RANDOM_FLOAT_IN_RANGE(-1, 1)
	IF NOT bXYOnly
		retVec.z = GET_RANDOM_FLOAT_IN_RANGE(-1, 1)
	ELSE
		retVec.z = 0
	ENDIF
	RETURN GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(retVec, length)	
ENDFUNC

FUNC BOOL IS_POSITION_INFRONT_OF_ENTITY(VECTOR pos, ENTITY_INDEX ent)
	VECTOR vEntPos = GET_ENTITY_COORDS(ent)
	VECTOR v1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ent, <<0,1,0>>) - vEntPos
	VECTOR v2 = pos - vEntPos
	FLOAT fDot = DOT_PRODUCT(v1, v2)
	IF fDot > 0.5
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_SPOOKED_OR_KILLED_PED(PED_INDEX ped, BOOL bDeadCheck = TRUE, BOOL bIsCeleb = FALSE)
	IF NOT IS_PED_INJURED(ped)
		VECTOR vPedCoors = GET_ENTITY_COORDS(ped)
		WEAPON_TYPE weapon
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE)
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weapon)
		ENDIF
		IF NOT bIsCeleb
			IF IS_BULLET_IN_AREA(vPedCoors, 5)
				RETURN TRUE
			ENDIF
		ENDIF
		IF VDIST2(vPedCoors, vCurrentPlayerCoords) < 20*20
			IF NOT bIsCeleb
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
				AND IS_PED_SHOOTING(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
			IF weapon = WEAPONTYPE_PETROLCAN
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
			AND VDIST2(vCurrentPlayerCoords, vPedCoors) < 9
				RETURN TRUE
			ENDIF
			IF (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped))
			AND (IS_POSITION_INFRONT_OF_ENTITY(vCurrentPlayerCoords, ped) AND VDIST2(vCurrentPlayerCoords, GET_ENTITY_COORDS(ped)) < 100)
			AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				RETURN TRUE
			ENDIF
			IF iHighestDamage > 150
				PRINTLN("Highest damage to ped is above 40")
				RETURN TRUE
			ENDIF
		ENDIF
		IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(ped, BONETAG_HEAD, <<0,0,0>>), 6)
			RETURN TRUE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(ped)
		AND bDeadCheck
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_SPOOKED_OR_KILLED_A_PAP()
	INT iTemp
	REPEAT COUNT_OF(pedPaps) iTemp
		IF HAS_PLAYER_SPOOKED_OR_KILLED_PED(pedPaps[iTemp].m_pap, FALSE, FALSE)
			RETURN TRUE	
		ENDIF
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
			IF IS_PED_INJURED(pedPaps[iTemp].m_pap)
			OR IS_ENTITY_DEAD(pedPaps[iTemp].m_pap)
				IF iDamageToPaps[iTemp] > 50
					iNumberOfPapsDead++
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FLOAT fTotalStillTimeNearPaps = 0
FLOAT fTotalOutOfCarTime = 0

FUNC BOOL HAS_PLAYER_SCARED_A_PAP()

	BOOL bScaredOff = FALSE
	
	INT iTemp
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
			IF IS_ENTITY_VISIBLE(pedPaps[iTemp].m_pap)
				IF IS_BULLET_IN_AREA(pedPaps[iTemp].m_vCoords, 5)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, pedPaps[iTemp].m_vCoords, 10)
				OR IS_PED_FLEEING(pedPaps[iTemp].m_pap)
				OR (iDamageToPaps[iTemp] > 10 AND thisStage = pescape_PICKUPGIRL)
					bScaredOff = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumberOfPapsDead > 1
		bScaredOff = TRUE
	ENDIF
	
	REPEAT COUNT_OF(vehPaps) iTemp
		IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
			IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
			AND IS_ENTITY_VISIBLE(vehPaps[iTemp].m_veh)
				IF IS_ENTITY_ON_FIRE(vehPaps[iTemp].m_veh)
					bScaredOff = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bScaredOff
	
ENDFUNC

//fails checks for a given vehicle
FUNC BOOL MISSION_VEHICLE_FAIL_CHECKS(VEHICLE_INDEX VEHICLE)
	IF DOES_ENTITY_EXIST(VEHICLE)
		IF IS_VEHICLE_DRIVEABLE(VEHICLE)
			IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_JAMMED, JAMMED_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_SIDE, SIDE_TIME)
				RETURN TRUE
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL HAS_PLAYER_PISSED_OFF_CELEB()

	IF NOT IS_PED_INJURED(pedCelebGirl)
	
		BOOL bNearPapsAndSlow = FALSE
		IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
			IF DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
				IF VDIST(GET_ENTITY_COORDS(GET_CLOSEST_PAP()), vCurrentCelebCoords) < 5
				AND GET_ENTITY_SPEED(vehCelebsCar) < 1
					bNearPapsAndSlow = TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
				AND IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
					fTotalOutOfCarTime += GET_FRAME_TIME()
				ELSE
					IF fTotalOutOfCarTime > 0
						fTotalOutOfCarTime = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bNearPapsAndSlow
			fTotalStillTimeNearPaps += GET_FRAME_TIME()
		ELSE
			IF fTotalStillTimeNearPaps > 0
				fTotalStillTimeNearPaps = 0
			ENDIF
		ENDIF
		
		BOOL bPissed
		
		IF fTotalStillTimeNearPaps > 30.0
			PRINTLN("fTotalStillTimeNearPaps > 30.0 PISSED!")
			bPissed = TRUE
		ENDIF
		
		IF bOnBike
			IF NOT IS_PED_SITTING_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
				IF IS_PED_RAGDOLL(pedCelebGirl)
					bPissed = TRUE
				ENDIF
				bOnBike = FALSE
			ENDIF
		ENDIF
		
		IF NOT bOnBike
			IF IS_PED_SITTING_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
				bOnBike = TRUE
			ENDIF
		ENDIF
		
		IF fTotalOutOfCarTime > 30.0
			PRINTLN("fTotalOutOfCarTime > 30.0")
			bPissed = TRUE
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(vehCelebsCar)
		AND (NOT bPlayersCarUsed OR thisStage = pescape_LOSEPAPS)
			PRINTLN("NOT IS_VEHICLE_DRIVEABLE(vehCelebsCar) PISSED!")
			bPissed = TRUE
		ENDIF
		
		IF VDIST(vCurrentCelebCoords, vCurrentPlayerCoords) > 30
			PRINTLN("VDIST(vCurrentCelebCoords, vCurrentPlayerCoords) > 30 PISSED!")
			bPissed = TRUE
		ENDIF
		
		IF (IS_VEHICLE_DRIVEABLE(vehCelebsCar) 
		AND MISSION_VEHICLE_FAIL_CHECKS(vehCelebsCar))
		AND (NOT bPlayersCarUsed OR thisStage = pescape_LOSEPAPS)
		AND VDIST(GET_ENTITY_COORDS(pedCelebGirl), vDropOffOnFootPos) > 15
			PRINTLN("IS_VEHICLE_DRIVEABLE(vehCelebsCar) AND MISSION_VEHICLE_FAIL_CHECKS(vehCelebsCar) PISSED!")
			bPissed = TRUE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehCelebsCar) AND IS_ENTITY_IN_WATER(vehCelebsCar)
			PRINTLN("IS_VEHICLE_DRIVEABLE(vehCelebsCar) AND IS_ENTITY_IN_WATER(vehCelebsCar) PISSED!")
			bPissed = TRUE
		ENDIF
		
		IF bPissed
			IF GET_RANDOM_INT_IN_RANGE(0, 2) = 1
				sStropString = "REPAP_GUP"
			ELSE
				sStropString = "REPAP_GUP2"
			ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

PROC FLEE_ALL()
	IF NOT IS_PED_INJURED(pedCelebGirl)
		SET_PED_FLEE_ATTRIBUTES(pedCelebGirl, FA_USE_VEHICLE, FALSE)
		TASK_SMART_FLEE_PED(pedCelebGirl, PLAYER_PED_ID(), 300, -1)
	ENDIF
	INT iTemp
	REPEAT COUNT_OF(pedPaps) iTemp
		IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
			TASK_SMART_FLEE_PED(pedPaps[iTemp].m_pap, PLAYER_PED_ID(), 300, -1)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedPaps[iTemp].m_pap, RELGROUPHASH_HATES_PLAYER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPaps[iTemp].m_pap, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

BOOL bCarsTasked[2]
FLOAT fStillTime = 0.0

VECTOR vCar1DriveTo = <<272.6356, 155.5618, 103.3306>>
VECTOR vCar2DriveTo = <<247.5795, 88.7094, 92.9859>>

VECTOR vStandingPapRunTo1 = <<259.0348, 114.0369, 98.9984>>
VECTOR vStandingPapRunTo2 = <<255.4971, 116.8744, 99.8162>>
VECTOR vStandingPapRunTo3 = <<266.8217, 139.7698, 103.2269>>
VECTOR vStandingPapRunTo4 = <<270.0174, 139.1181, 103.2195>>
VECTOR vStandingPapRunTo5 = <<265.4954, 144.6143, 103.3326>>


VECTOR vPapVehStartCoord[4]
FLOAT fPapVehStartHeading[4]

INT iLastWarper
INT iLastWarp
INT iFirstSeqStage

FUNC FLOAT GET_UNIFORM_ANGLE(FLOAT fAngle)
	IF fAngle >= 180
		fAngle -= 360
	ENDIF
	IF fAngle < -180
		fAngle += 360
	ENDIF
	RETURN fAngle
ENDFUNC

PROC KEEP_PAP_ON_SPOT_PLAYING_ANIM(INT iPap, VECTOR vCoords, FLOAT fHeading, STRING sDict, STRING sAnim1, STRING sAnim2 = NULL)
	IF NOT IS_PED_INJURED(pedPaps[iPap].m_pap)
		IF VDIST(GET_ENTITY_COORDS(pedPaps[iPap].m_pap), vCoords) > 1.5
			IF GET_SCRIPT_TASK_STATUS(pedPaps[iPap].m_pap, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedPaps[iPap].m_pap, vCoords, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHeading)
			ENDIF
		ELSE
			IF ABSF(GET_UNIFORM_ANGLE(GET_ENTITY_HEADING(pedPaps[iPap].m_pap)) - GET_UNIFORM_ANGLE(fHeading)) > 10
				IF GET_SCRIPT_TASK_STATUS(pedPaps[iPap].m_pap, SCRIPT_TASK_ACHIEVE_HEADING) <> PERFORMING_TASK
					TASK_ACHIEVE_HEADING(pedPaps[iPap].m_pap, fHeading)
				ENDIF
			ELSE
				SWITCH pedPaps[iPap].m_iFootChaseState
					CASE 0
						IF IS_STRING_NULL_OR_EMPTY(sAnim2)
							IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPap].m_pap, sDict, sAnim1)
								TASK_PLAY_ANIM(pedPaps[iPap].m_pap, sDict, sAnim1, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							ENDIF
						ELSE
							IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPap].m_pap, sDict, sAnim1)
								TASK_PLAY_ANIM(pedPaps[iPap].m_pap, sDict, sAnim1, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							ELSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iPap].m_pap, sDict, sAnim1) > 0.99
									TASK_PLAY_ANIM(pedPaps[iPap].m_pap, sDict, sAnim2, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
									pedPaps[iPap].m_iFootChaseState++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPap].m_pap, sDict, sAnim2)
							TASK_PLAY_ANIM(pedPaps[iPap].m_pap, sDict, sAnim2, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						ELSE
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iPap].m_pap, sDict, sAnim2) > 0.99
								TASK_PLAY_ANIM(pedPaps[iPap].m_pap, sDict, sAnim1, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
								pedPaps[iPap].m_iFootChaseState=0
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PAP_ANIM_BITSET(INT iPapIndex)
	PAP_ANIM eAnimCounter
	REPEAT MAX_PAP_ANIMS eAnimCounter
		IF IS_BIT_SET(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnimCounter))
		
			INT iCarOffset = NUMBER_OF_STANDING_ANIMS
			INT iGripOffset = iCarOffset + COUNT_OF(carAnims)
			INT iAnimIndex = ENUM_TO_INT(eAnimCounter)
			
			IF iAnimIndex < iCarOffset
				IF IS_ENTITY_PLAYING_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iPapIndex))
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iPapIndex].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iPapIndex)) > 0.9
						CLEAR_BIT(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnimCounter))
					ENDIF
				ENDIF
			ELIF iAnimIndex < iGripOffset
				IF IS_ENTITY_PLAYING_ANIM(pedPaps[iPapIndex].m_pap, carAnimDict, carAnims[iAnimIndex - iCarOffset])
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedPaps[iPapIndex].m_pap, carAnimDict, carAnims[iAnimIndex - iCarOffset]) > 0.9
						CLEAR_BIT(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnimCounter))
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iPapIndex].m_pap, standingAnimDict, "grip")
					CLEAR_BIT(pedPaps[iPapIndex].m_iPapAnims, ENUM_TO_INT(eAnimCounter))
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

INT iCurrentCleanupGroup
BOOL bslowdowndone
INT iBreakFirstTaskBits

PROC RUN_PAPS()

	//VEHICLES 0 and 2 are CAVALCADE2s
	// 1 and 3 are vaders
	//peds start in the CAVALCADE2s and vader peds start on foot
	
	INT iTemp
	
	SWITCH currentPapStage
	
		CASE pap_init
		
			iFirstSeqStage = 0
			
			REQUEST_MODEL(A_M_Y_GENSTREET_02)
			REQUEST_MODEL(PROP_PAP_CAMERA_01)
			REQUEST_MODEL(CAVALCADE2)
			REQUEST_MODEL(VADER)
			REQUEST_ANIM_DICT(standingAnimDict)
			REQUEST_ANIM_DICT("random@paparazzi@pap_anims")
			
			IF HAS_MODEL_LOADED(A_M_Y_GENSTREET_02)
			AND HAS_MODEL_LOADED(PROP_PAP_CAMERA_01)
			AND HAS_MODEL_LOADED(CAVALCADE2)
			AND HAS_MODEL_LOADED(VADER)
			AND HAS_ANIM_DICT_LOADED(standingAnimDict)
			AND HAS_ANIM_DICT_LOADED("random@paparazzi@pap_anims")
			
				vPapVehStartCoord[0] = <<266.6049, 157.7677, 104.2417>>
				vPapVehStartCoord[1] = <<280.2455, 152.1227, 103.7096>>
				vPapVehStartCoord[2] = <<223.2588, 50.6347, 83.6598>>
				vPapVehStartCoord[3] = <<244.1302, 37.4709, 83.4804>>
				
				fPapVehStartHeading[0] = -111.1625
				fPapVehStartHeading[1] = 68.8570
				fPapVehStartHeading[2] = -108.2056
				fPapVehStartHeading[3] = 50.8189
								
				REPEAT COUNT_OF(vehPaps) iTemp
					INIT_PAP_VEH(iTemp)
					INIT_PAP(GET_DRIVER_INDEX_FROM_PAP_VEH(iTemp))
					INIT_PAP(GET_PASSENGER_INDEX_FROM_PAP_VEH(iTemp))
				ENDREPEAT
				
				iLastWarper = 0
				
				CREATE_PAP_ON_FOOT(GET_DRIVER_INDEX_FROM_PAP_VEH(1), <<274.9306, 147.1640, 103.3794>>, 2.2109)
				CREATE_PAP_ON_FOOT(GET_PASSENGER_INDEX_FROM_PAP_VEH(1), <<275.6894, 148.1107, 103.3723>>, 117.9416)
				
				CREATE_PAP_ON_FOOT(GET_PASSENGER_INDEX_FROM_PAP_VEH(2), vPapVehStartCoord[1], fPapVehStartHeading[1])
				CREATE_PAP_ON_FOOT(GET_PASSENGER_INDEX_FROM_PAP_VEH(3), vPapVehStartCoord[1], fPapVehStartHeading[1])
				
				CREATE_PAP_VEHICLE(0, vPapVehStartCoord[0], fPapVehStartHeading[0], TRUE)
				CREATE_PAP_VEHICLE(1, vPapVehStartCoord[1], fPapVehStartHeading[1])
				
				SET_ENTITY_COORDS(GET_PAP_FROM_PAP_VEH(0, 1),  <<265.4080, 151.2875, 103.5673>>)
				SET_ENTITY_HEADING(GET_PAP_FROM_PAP_VEH(0, 1), 217.1033)
				
				SET_ENTITY_COORDS(GET_PAP_FROM_PAP_VEH(2, 1),  <<245.9477, 72.6610, 89.1067>>)
				SET_ENTITY_HEADING(GET_PAP_FROM_PAP_VEH(2, 1), 38.6283)
				
				SET_ENTITY_COORDS(GET_PAP_FROM_PAP_VEH(3, 1), <<244.1872, 73.3336, 89.0917>>)
				SET_ENTITY_HEADING(GET_PAP_FROM_PAP_VEH(3, 1), 272.9875)
				
				TASK_PLAY_ANIM(GET_PAP_FROM_PAP_VEH(0, 1), "random@paparazzi@pap_anims", "base_pap", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				TASK_PLAY_ANIM(GET_PAP_FROM_PAP_VEH(1, 0), "random@paparazzi@pap_anims", "pap_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				TASK_PLAY_ANIM(GET_PAP_FROM_PAP_VEH(1, 1), "random@paparazzi@pap_anims", "pap_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				TASK_PLAY_ANIM(GET_PAP_FROM_PAP_VEH(2, 1), "random@paparazzi@pap_anims", "pap_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				TASK_PLAY_ANIM(GET_PAP_FROM_PAP_VEH(3, 1), "random@paparazzi@pap_anims", "pap_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				
				currentPapStage = pap_wait
				
			ENDIF
			
		BREAK
		
		CASE pap_wait
		
			KEEP_PAP_ON_SPOT_PLAYING_ANIM(GET_PASSENGER_INDEX_FROM_PAP_VEH(0), <<265.4080, 151.2875, 103.5673>>, 217.1033, "random@paparazzi@pap_anims", "base_pap")
			KEEP_PAP_ON_SPOT_PLAYING_ANIM(GET_DRIVER_INDEX_FROM_PAP_VEH(1), <<274.9306, 147.1640, 103.3794>>, 2.2109, "random@paparazzi@pap_anims", "pap_idle_a", "base_pap")
			KEEP_PAP_ON_SPOT_PLAYING_ANIM(GET_PASSENGER_INDEX_FROM_PAP_VEH(1), <<277.1364, 148.4023, 103.3493>>, 104.6318, "random@paparazzi@pap_anims", "pap_idle_b")
			KEEP_PAP_ON_SPOT_PLAYING_ANIM(GET_PASSENGER_INDEX_FROM_PAP_VEH(2), <<245.9477, 72.6610, 89.1067>>, 38.6283, "random@paparazzi@pap_anims", "pap_idle_b")
			KEEP_PAP_ON_SPOT_PLAYING_ANIM(GET_PASSENGER_INDEX_FROM_PAP_VEH(3), <<244.1872, 73.3336, 89.0917>>, 272.9875, "random@paparazzi@pap_anims", "pap_idle_a")
			
		BREAK
		
		CASE pap_first_seq
		
			IF DOES_ENTITY_EXIST(pedPaps[2].m_cam)
				IF NOT IS_ENTITY_VISIBLE(pedPaps[2].m_cam)
					SET_ENTITY_VISIBLE(pedPaps[2].m_cam, TRUE)
				ENDIF
			ENDIF
			
			SWITCH iFirstSeqStage
			
				CASE 0
				
					//create the rest of the paps
					IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,0))
						CLEAR_PED_TASKS(GET_PAP_FROM_PAP_VEH(1,0))
					ENDIF
					
					IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,1))
						CLEAR_PED_TASKS(GET_PAP_FROM_PAP_VEH(1,1))
					ENDIF
					
					//create second CAVALCADE2 down the alleyway
					IF NOT DOES_ENTITY_EXIST(vehPaps[2].m_veh)
						CREATE_PAP_VEHICLE(2, vPapVehStartCoord[2], fPapVehStartHeading[2])
						IF IS_VEHICLE_DRIVEABLE(vehPaps[2].m_veh)
							CREATE_PAP_ON_FOOT(GET_DRIVER_INDEX_FROM_PAP_VEH(2), GET_ENTITY_COORDS(vehPaps[2].m_veh), 0)
							SET_PED_INTO_VEHICLE(GET_PAP_FROM_PAP_VEH(2, 0), vehPaps[2].m_veh)
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(vehPaps[3].m_veh)
						CREATE_PAP_VEHICLE(3, vPapVehStartCoord[3], fPapVehStartHeading[3])
					ENDIF
					
					//two peds further down the alley run up to behind car
					IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(2, 1))
						TASK_FOLLOW_NAV_MESH_TO_COORD(GET_PAP_FROM_PAP_VEH(2,1), vStandingPapRunTo1, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
					ENDIF
					IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(3, 1))
						TASK_FOLLOW_NAV_MESH_TO_COORD(GET_PAP_FROM_PAP_VEH(3,1), vStandingPapRunTo2, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(GET_PAP_FROM_PAP_VEH(3, 0))
						CREATE_PAP_ON_FOOT(GET_DRIVER_INDEX_FROM_PAP_VEH(3), <<253.8376, 85.4544, 92.8933>>, 85.1702)
					ENDIF
					
					// set paps to stand at the end of the alleyway instead of running down to the celeb's car
					IF g_bMagDemoActive OR bDebugTestingMagdemo
						IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,0))
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_PAP_FROM_PAP_VEH(1,0), vStandingPapRunTo3, PEDMOVEBLENDRATIO_RUN)
						ENDIF
						IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,1))
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_PAP_FROM_PAP_VEH(1,1), vStandingPapRunTo4, PEDMOVEBLENDRATIO_RUN)
						ENDIF
						IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(0,1))
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_PAP_FROM_PAP_VEH(0,1), vStandingPapRunTo5, PEDMOVEBLENDRATIO_RUN)
						ENDIF
					ENDIF
					
					SETTIMERB(0)
					
					INT i
					
					REPEAT COUNT_OF(pedPaps) i
						IF DOES_ENTITY_EXIST(pedPaps[i].m_pap)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedPaps[i].m_pap, "MAG_2_PAPARAZZI_GROUP")
						ENDIF
					ENDREPEAT
					
					REPEAT COUNT_OF(vehPaps) i
						IF DOES_ENTITY_EXIST(vehPaps[i].m_veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehPaps[i].m_veh, "MAG_2_PAPARAZZI_GROUP")
						ENDIF
					ENDREPEAT
					
					iFirstSeqStage++
					
				BREAK
				
				CASE 1
				
					BLIP_PAPS()
					
					// send the second CAVALCADE2 into the top part of the alleyway
					IF IS_VEHICLE_DRIVEABLE(vehPaps[0].m_veh)
					AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(0, 0))
					AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(0, 1))
						IF NOT bCarsTasked[1]
							bCarsTasked[1] = TRUE
							TASK_VEHICLE_DRIVE_TO_COORD(GET_PAP_FROM_PAP_VEH(0, 0), vehpaps[0].m_veh, vCar1DriveTo, 10, DRIVINGSTYLE_NORMAL, CAVALCADE2, DRIVINGMODE_AVOIDCARS, 2, 10)
						ENDIF
					ENDIF
					
					// send the CAVALCADE2 into middle of alley pathway
					IF IS_VEHICLE_DRIVEABLE(vehPaps[2].m_veh)	
					AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(2, 0))
					AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(2, 1))
						IF NOT bCarsTasked[0]
							TASK_VEHICLE_DRIVE_TO_COORD(GET_PAP_FROM_PAP_VEH(2, 0), vehpaps[2].m_veh, vCar2DriveTo, 10, DRIVINGSTYLE_NORMAL, CAVALCADE2, DRIVINGMODE_AVOIDCARS, 2, 10)
							bCarsTasked[0] = TRUE		
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedCelebGirl)
					AND NOT IS_PED_INJURED(pedCelebGirl)
						PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, 6)
						//PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, 7)
						IF g_bMagDemoActive
						OR bDebugTestingMagdemo
							IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,0))
								IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(1,0), vStandingPapRunTo3, g_vOnFootLocate)
									PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_DRIVER_INDEX_FROM_PAP_VEH(1))
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(1,1))
								IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(1,1), vStandingPapRunTo4, g_vOnFootLocate)
									PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(1))
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(0,1))
								IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(0,1), vStandingPapRunTo4, g_vOnFootLocate)
									PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(0))
								ENDIF
							ENDIF
						ELSE
							PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_DRIVER_INDEX_FROM_PAP_VEH(1))
							PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(1))
							IF TIMERB() > 1000
								PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_DRIVER_INDEX_FROM_PAP_VEH(1))
							ENDIF
						ENDIF
					ENDIF
					
					IF VDIST2(GET_ENTITY_COORDS(vehCelebsCar), vCreateCelebGirlPos) < 9
						IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(2,1), vStandingPapRunTo1, g_vOnFootLocate)
							PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(2))
						ENDIF
						IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(3,1), vStandingPapRunTo2, g_vOnFootLocate)
							PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(3))
						ENDIF
						IF IS_ENTITY_AT_COORD(GET_PAP_FROM_PAP_VEH(0,1), vStandingPapRunTo2, g_vOnFootLocate)
							PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(0))
						ENDIF
					ELSE
					
						IF NOT IS_BIT_SET(iBreakFirstTaskBits, 3)
							CLEAR_PED_TASKS(pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(3)].m_pap)
							SET_BIT(iBreakFirstTaskBits, 3)
						ENDIF
						IF NOT IS_BIT_SET(iBreakFirstTaskBits, 2)
							CLEAR_PED_TASKS(pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(2)].m_pap)
							SET_BIT(iBreakFirstTaskBits, 2)
						ENDIF
						IF NOT IS_BIT_SET(iBreakFirstTaskBits, 0)
							CLEAR_PED_TASKS(pedPaps[GET_PASSENGER_INDEX_FROM_PAP_VEH(0)].m_pap)
							SET_BIT(iBreakFirstTaskBits, 0)
						ENDIF
						
						IF g_bMagDemoActive OR bDebugTestingMagdemo
							PAP_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(3))
						ELSE
							PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(3))
						ENDIF
						PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(2))
						PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, GET_PASSENGER_INDEX_FROM_PAP_VEH(0))
						
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
			DO_FLASH_BULBS()
			
		BREAK
		
		CASE pap_chase
		
			BLIP_PAPS()
			
			IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				IF GET_ENTITY_SPEED(vehCelebsCar) < 1
					fStillTime += GET_FRAME_TIME()
				ELSE
					IF fStillTime > 0.0
						fStillTime = 0.0
					ENDIF
				ENDIF
			ENDIF
			
			REPEAT COUNT_OF(vehPaps) iTemp
			
				IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
				AND thisStage != pescape_STROPSOFF
				
					IF iTemp < 2
					AND (iTemp != iLastWarper OR NOT DOES_ENTITY_EXIST(vehPaps[0].m_veh) OR NOT DOES_ENTITY_EXIST(vehPaps[1].m_veh))
					AND GET_GAME_TIMER() - iLastWarp > 500
						IF VDIST2(vCurrentPlayerCoords, vCreateCelebGirlPos) > 20*20
							IF VDIST2(vCurrentPlayerCoords, vCreateCelebGirlPos) < 250*250
							OR (GET_GAME_TIMER() - iTimeStartedChase) < 30000
								RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehPaps[iTemp].m_veh, vehCelebsCar, vehPaps[iTemp].m_vWarpPosition, vehPaps[iTemp].m_fWarpHeading,  vehPaps[iTemp].m_vTargetPosition, vehPaps[iTemp].m_fTargetHeading, vehPaps[iTemp].m_iWarpTimer, vehPaps[iTemp].m_vWarpOffset, 800, 50.0)
								iLastWarper = iTemp
								iLastWarp = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
					
					INT iDriver
					INT iPassenger
					iDriver = GET_DRIVER_INDEX_FROM_PAP_VEH(iTemp)
					
					IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
						iPassenger = GET_PASSENGER_INDEX_FROM_PAP_VEH(iTemp)
					ELSE
						iPassenger = GET_DRIVER_INDEX_FROM_PAP_VEH(iTemp)
					ENDIF		
					
					SWITCH vehPaps[iTemp].m_iChaseState
						CASE 0 //Celeb is in vehicle driving away
							IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPaps[iTemp].m_veh)
									IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
										IF NOT IS_PED_IN_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
											IF GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
											AND IS_VEHICLE_SEAT_FREE(vehPaps[iTemp].m_veh, VS_DRIVER)
												TASK_ENTER_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, DEFAULT_TIME_NEVER_WARP)
											ENDIF
										ENDIF
									ENDIF
									IF iPassenger <> -1
										IF NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap)
											IF NOT IS_PED_IN_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh)
												IF GET_SCRIPT_TASK_STATUS(pedpaps[iPassenger].m_pap, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
												AND IS_VEHICLE_SEAT_FREE(vehPaps[iTemp].m_veh, VS_FRONT_RIGHT)
													TASK_ENTER_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
												ENDIF
											ENDIF
										ENDIF
										IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
										AND (iPassenger <> -1 AND NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap))
											IF IS_PED_IN_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
											AND (iPassenger <> -1 AND IS_PED_IN_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh))
											AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
												IF GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
												AND GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
													SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_LEAVE_VEHICLES, FALSE)
													SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_USE_VEHICLE, TRUE)
													SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_DO_DRIVEBYS, FALSE)
													IF iTemp = 0
														TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_LEFT, 50, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 4, 10, TRUE)
													ELIF iTemp = 1
														TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_RIGHT, 50, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_StopForPeds, 4, 10, TRUE)
													ELIF iTemp = 2
														TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_LEFT, 50, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 4, 10, TRUE)
													ELIF iTemp = 3
														TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_RIGHT, 50, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_StopForPeds, 4, 10, TRUE)
													ENDIF
												ENDIF
												SET_DRIVE_TASK_CRUISE_SPEED(pedpaps[iDriver].m_pap, CLAMP(3*GET_DISTANCE_BETWEEN_COORDS(vCurrentCelebCarCoords, vehPaps[iTemp].m_vCoords), 50, 120))
											ENDIF
										ENDIF
									ENDIF
									IF fStillTime > 3.0
									AND VDIST(vehPaps[iTemp].m_vCoords, vCurrentCelebCarCoords) < 10
										vehPaps[iTemp].m_iChaseState = 1
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
									IF NOT IS_PED_FLEEING(pedpaps[iDriver].m_pap)
										SET_PED_FLEE_ATTRIBUTES(pedpaps[iDriver].m_pap, FA_USE_VEHICLE, FALSE)
										TASK_SMART_FLEE_PED(pedpaps[iDriver].m_pap, PLAYER_PED_ID(), 300, 100000, FALSE)
									ENDIF
								ENDIF
								IF iPassenger <> -1
									IF NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap)
										IF NOT IS_PED_FLEEING(pedpaps[iPassenger].m_pap)
											SET_PED_FLEE_ATTRIBUTES(pedpaps[iPassenger].m_pap, FA_USE_VEHICLE, FALSE)
											TASK_SMART_FLEE_PED(pedpaps[iPassenger].m_pap, PLAYER_PED_ID(), 300, 100000, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 1 //celeb vehicle is stopped or close enough to snap on foot
							IF IS_PED_IN_ANY_VEHICLE(pedpaps[iDriver].m_pap)
								TASK_LEAVE_ANY_VEHICLE(pedpaps[iDriver].m_pap, 1, ECF_DONT_CLOSE_DOOR)
							ELSE
								PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, iDriver)//, 4.5)
							ENDIF
							IF iPassenger <> -1
								IF NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap)
									IF IS_PED_IN_ANY_VEHICLE(pedpaps[iPassenger].m_pap)
										TASK_LEAVE_ANY_VEHICLE(pedpaps[iPassenger].m_pap, 0, ECF_DONT_CLOSE_DOOR)
									ELSE
										PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, iPassenger)//, 4.5)
									ENDIF
								ENDIF
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
								IF VDIST(vehPaps[iTemp].m_vCoords, vCurrentCelebCarCoords) > 15
									vehPaps[iTemp].m_iChaseState = 0
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
					AND(iPassenger = -1 OR NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap))
					AND IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
						BOOL bAllOccluded
						bAllOccluded = TRUE
						IF NOT IS_ENTITY_OCCLUDED(pedpaps[iDriver].m_pap)
							bAllOccluded = FALSE
						ENDIF
						IF iPassenger <> -1
							IF NOT IS_ENTITY_OCCLUDED(pedpaps[iPassenger].m_pap)
								bAllOccluded = FALSE
							ENDIF
						ENDIF
						IF NOT IS_ENTITY_OCCLUDED(vehPaps[iTemp].m_veh)
							bAllOccluded = FALSE
						ENDIF
						IF bAllOccluded
						AND VDIST(vCurrentPlayerCoords, vehPaps[iTemp].m_vCoords) > 25
						AND VDIST(vCurrentPlayerCoords, pedPaps[iTemp].m_vCoords) > 25
							IF NOT IS_PED_IN_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
								SET_PED_INTO_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
							ENDIF
							IF iPassenger <> -1
								IF NOT IS_PED_IN_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh)
									SET_PED_INTO_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh, VS_FRONT_RIGHT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PAP_GROUP_LOST(iTemp)
						DELETE_PAP_GROUP(iTemp)
					ENDIF
					
				ENDIF
				
				
				DO_FLASH_BULBS()
				TAKE_PICS_FROM_VEHICLE()
				
			ENDREPEAT
			
		BREAK
		
		CASE pap_magdemo_back_off
		
			BLIP_PAPS()
			
			IF NOT bslowdowndone
			
				REPEAT COUNT_OF(vehPaps) iTemp
				
					INT iDriver
					INT iPassenger
					iDriver = GET_DRIVER_INDEX_FROM_PAP_VEH(iTemp)
					
					IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
						iPassenger = GET_PASSENGER_INDEX_FROM_PAP_VEH(iTemp)
					ELSE
						iPassenger = GET_DRIVER_INDEX_FROM_PAP_VEH(iTemp)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
						IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
							IF NOT IS_PED_IN_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
								IF GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
									TASK_ENTER_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, DEFAULT_TIME_NEVER_WARP)
								ENDIF
							ENDIF
						ENDIF
						IF iPassenger <> -1
							IF NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap)
								IF NOT IS_PED_IN_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh)
									IF GET_SCRIPT_TASK_STATUS(pedpaps[iPassenger].m_pap, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
										TASK_ENTER_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
							AND (iPassenger <> -1 AND NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap))
								IF IS_PED_IN_VEHICLE(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh)
								AND (iPassenger <> -1 AND IS_PED_IN_VEHICLE(pedpaps[iPassenger].m_pap, vehPaps[iTemp].m_veh))
								AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
									FLOAT fMaxCruiseSpeed
									fMaxCruiseSpeed = CLAMP(70 - (TO_FLOAT(GET_GAME_TIMER() - iTimeOfBackOff)/50.0), 0, 70)
									IF (fMaxCruiseSpeed <= 1 OR GET_GAME_TIMER() - iTimeOfBackOff > 4000)
										SET_DRIVE_TASK_MAX_CRUISE_SPEED(pedpaps[iDriver].m_pap, fMaxCruiseSpeed)
										INT iTemp2
										REPEAT COUNT_OF(vehPaps) iTemp2
											IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp2].m_veh)
											AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(iTemp2, 0))
											AND IS_PED_IN_VEHICLE(GET_PAP_FROM_PAP_VEH(iTemp2, 0), vehPaps[iTemp2].m_veh)
												CLEAR_PED_TASKS(GET_PAP_FROM_PAP_VEH(iTemp2, 0))
												TASK_VEHICLE_MISSION(GET_PAP_FROM_PAP_VEH(iTemp2, 0), vehPaps[iTemp2].m_veh, NULL, MISSION_STOP, 10, DRIVINGMODE_AVOIDCARS_RECKLESS, 10, 10, TRUE)
											ENDIF
										ENDREPEAT
										bslowdowndone = TRUE
									ELSE
										IF GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(pedpaps[iDriver].m_pap, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
											SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_LEAVE_VEHICLES, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_USE_VEHICLE, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(pedpaps[iDriver].m_pap, CA_DO_DRIVEBYS, FALSE)
											IF iTemp = 0
												TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_LEFT, fMaxCruiseSpeed, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 4, 10, TRUE)
											ELIF iTemp = 1
												TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_RIGHT, fMaxCruiseSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_StopForPeds, 4, 10, TRUE)
											ELIF iTemp = 2
												TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_LEFT, fMaxCruiseSpeed, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 4, 10, TRUE)
											ELIF iTemp = 3
												TASK_VEHICLE_MISSION(pedpaps[iDriver].m_pap, vehPaps[iTemp].m_veh, vehCelebsCar, MISSION_ESCORT_RIGHT, fMaxCruiseSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_StopForPeds, 4, 10, TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(pedpaps[iDriver].m_pap)
							IF NOT IS_PED_FLEEING(pedpaps[iDriver].m_pap)
								SET_PED_FLEE_ATTRIBUTES(pedpaps[iDriver].m_pap, FA_USE_VEHICLE, FALSE)
								TASK_SMART_FLEE_PED(pedpaps[iDriver].m_pap, PLAYER_PED_ID(), 300, 100000, FALSE)
							ENDIF
						ENDIF
						IF iPassenger <> -1
							IF NOT IS_PED_INJURED(pedpaps[iPassenger].m_pap)
								IF NOT IS_PED_FLEEING(pedpaps[iPassenger].m_pap)
									SET_PED_FLEE_ATTRIBUTES(pedpaps[iPassenger].m_pap, FA_USE_VEHICLE, FALSE)
									TASK_SMART_FLEE_PED(pedpaps[iPassenger].m_pap, PLAYER_PED_ID(), 300, 100000, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDREPEAT
				
			ELSE
				IF IS_PAP_GROUP_LOST(iCurrentCleanupGroup, 10)
					DELETE_PAP_GROUP(iCurrentCleanupGroup)
				ENDIF
				iCurrentCleanupGroup++
				IF iCurrentCleanupGroup > 3
					iCurrentCleanupGroup = 0
				ENDIF
			ENDIF
			
			DO_FLASH_BULBS()
			TAKE_PICS_FROM_VEHICLE()
			
		BREAK
				
		CASE pap_cleanup
		
			IF IS_PAP_GROUP_LOST(iCurrentCleanupGroup)
				DELETE_PAP_GROUP(iCurrentCleanupGroup)
			ENDIF
			
			iCurrentCleanupGroup++
			IF iCurrentCleanupGroup > 3
				iCurrentCleanupGroup = 0
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	REPEAT COUNT_OF(pedPaps) iTemp
	
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
		AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
			
			IF currentPapStage > pap_wait
			
				IF NOT IS_STRING_NULL_OR_EMPTY(standingAnimDict)
				
					UPDATE_PAP_ANIM_BITSET(iTemp)
					
					IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_cam)
					AND IS_ENTITY_ATTACHED(pedPaps[iTemp].m_cam)
						IF NOT IS_PED_IN_ANY_VEHICLE(pedPaps[iTemp].m_pap)
						
							IF IS_ENTITY_VISIBLE(pedPaps[iTemp].m_cam)
								IF IS_PED_GETTING_UP(pedPaps[iTemp].m_pap)
									SET_ENTITY_VISIBLE(pedPaps[iTemp].m_cam, FALSE)
								ENDIF
							ELSE
								IF NOT IS_PED_GETTING_UP(pedPaps[iTemp].m_pap)
									SET_ENTITY_VISIBLE(pedPaps[iTemp].m_cam, TRUE)
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_OCCLUDED(pedPaps[iTemp].m_pap)
								IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, GET_STANDING_ANIM_INDEX_FOR_PAP(iTemp))
									IF NOT pedPaps[iTemp].m_bGrip
										IF NOT IS_ENTITY_PLAYING_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, "grip")
											TASK_PLAY_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, "grip", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING | AF_NOT_INTERRUPTABLE)
											pedPaps[iTemp].m_bGrip = TRUE
										ENDIF
									ENDIF
								ELSE
									IF pedPaps[iTemp].m_bGrip
										TASK_PLAY_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, "grip", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY, 0.99)
										pedPaps[iTemp].m_bGrip = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT pedPaps[iTemp].m_bCamWalksSet
								REQUEST_ANIM_SET(standingAnimDict)
								IF HAS_ANIM_SET_LOADED(standingAnimDict)
									SET_PED_WEAPON_MOVEMENT_CLIPSET(pedPaps[iTemp].m_pap, standingAnimDict)
									pedPaps[iTemp].m_bCamWalksSet = TRUE
								ENDIF
							ENDIF
						ELSE
							IF pedPaps[iTemp].m_bGrip
								TASK_PLAY_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, "grip", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY, 0.99)
								pedPaps[iTemp].m_bGrip = FALSE
							ENDIF
							IF pedPaps[iTemp].m_bCamWalksSet
								RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedPaps[iTemp].m_pap)
								pedPaps[iTemp].m_bCamWalksSet = FALSE
							ENDIF
						ENDIF
						
						IF IS_PED_SITTING_IN_ANY_VEHICLE(pedPaps[iTemp].m_pap)
						AND GET_PED_VEHICLE_SEAT(pedPaps[iTemp].m_pap, GET_VEHICLE_PED_IS_IN(pedPaps[iTemp].m_pap)) = VS_DRIVER
							IF IS_ENTITY_VISIBLE(pedPaps[iTemp].m_cam)
								SET_ENTITY_VISIBLE(pedPaps[iTemp].m_cam, FALSE)
							ENDIF
						ELSE
							IF NOT IS_ENTITY_VISIBLE(pedPaps[iTemp].m_cam)
								SET_ENTITY_VISIBLE(pedPaps[iTemp].m_cam, TRUE)
							ENDIF
						ENDIF
						
					ELSE
						IF pedPaps[iTemp].m_bGrip
							TASK_PLAY_ANIM(pedPaps[iTemp].m_pap, standingAnimDict, "grip", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY, 0.99)
							pedPaps[iTemp].m_bGrip = FALSE
						ENDIF
						IF pedPaps[iTemp].m_bCamWalksSet
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedPaps[iTemp].m_pap)
							pedPaps[iTemp].m_bCamWalksSet = FALSE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF thisStage <> pescape_STOLEVEH
				IF IS_PED_BEING_JACKED(pedPaps[iTemp].m_pap)
				AND IS_PED_JACKING(PLAYER_PED_ID())
					SET_STAGE(pescape_STOLEVEH)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF thisStage <> pescape_STOLEVEH
		REPEAT COUNT_OF(vehPaps) iTemp
			IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
				IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPaps[iTemp].m_veh)
						SET_STAGE(pescape_STOLEVEH)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_ABLE_TO_PICK_UP_CELEB()
	RETURN IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	AND (IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
	AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 0
	AND DOES_VEHICLE_HAVE_FREE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	AND NOT g_bPlayerIsInTaxi
ENDFUNC

SEQUENCE_INDEX seqTurnTask
//SEQUENCE_INDEX seqAnimLoop
INT iFaceStage = 0
INT iAnimloopStage

PROC DO_CELEB_CONTINUALLY_FACES_PLAYER(BOOL bAnim = FALSE)
	IF NOT IS_PED_INJURED(pedCelebGirl)
		IF iFaceStage > 1
			IF NOT IS_PED_FACING_PED(pedCelebGirl, PLAYER_PED_ID(), 60)
				iFaceStage = 0
				iAnimloopStage = 0
			ENDIF
		ENDIF
		SWITCH iFaceStage
			CASE 0
				CLEAR_PED_TASKS(pedCelebGirl)
				TASK_LOOK_AT_ENTITY(pedCelebGirl, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
				OPEN_SEQUENCE_TASK(seqTurnTask)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
				CLOSE_SEQUENCE_TASK(seqTurnTask)
				TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTurnTask)
				CLEAR_SEQUENCE_TASK(seqTurnTask)
				iFaceStage++
			BREAK
			CASE 1
				IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					iFaceStage++
				ENDIF
			BREAK
			CASE 2
				IF bAnim
					SWITCH iAnimloopStage
					
						CASE 0
							IF NOT IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_a")
								TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							ELSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedCelebGirl, "random@paparazzi@wait", "wait_a") > 0.95
									TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
									iAnimloopStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF NOT IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_b")
								TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							ELSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedCelebGirl, "random@paparazzi@wait", "wait_b") > 0.95
									TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
									iAnimloopStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_c")
								TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							ELSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedCelebGirl, "random@paparazzi@wait", "wait_c") > 0.95
									TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
									iAnimloopStage=0
								ENDIF
							ENDIF
						BREAK
						
					ENDSWITCH
//					IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
//						OPEN_SEQUENCE_TASK(seqAnimLoop)
//							TASK_PLAY_ANIM(NULL, "random@paparazzi@wait", "wait_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
//							TASK_PLAY_ANIM(NULL, "random@paparazzi@wait", "wait_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
//							TASK_PLAY_ANIM(NULL, "random@paparazzi@wait", "wait_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
//						CLOSE_SEQUENCE_TASK(seqAnimLoop)
//						TASK_PERFORM_SEQUENCE(pedCelebGirl, seqAnimLoop)
//						CLEAR_SEQUENCE_TASK(seqAnimLoop)
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL DO_CELEB_WARNING_LINES()
	IF NOT IS_PED_INJURED(pedCelebGirl)
	AND VDIST2(vCurrentPlayerCoords, vCurrentCelebCoords)<20*20
	
		IF NOT bHitLinePending
		AND (GET_GAME_TIMER() - iLastHitLine) > 5000
			IF iCumulativePapDamage > 10
				bHitLinePending = TRUE
				iCumulativePapDamage = 0
			ENDIF
		ENDIF
		
		IF NOT bShootLinePending
		AND NOT bHitLinePending
		AND (GET_GAME_TIMER() - iLastShootLine) > 4000
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				IF iNumberOfShootWarnings > 2
					IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
						SET_STAGE(pescape_CELEBFLEES)
					ENDIF
				ELSE
					bShootLinePending = TRUE
					iNumberOfShootWarnings++
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bWeaponLinePending
		AND NOT bHitLinePending
		AND NOT bShootLinePending
		AND (GET_GAME_TIMER() - iLastWeaponLine) > 15000
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				bWeaponLinePending = TRUE	
			ENDIF
		ENDIF
		
		IF bHitLinePending
			IF bFirstHitLineDone
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_HIT", CONV_PRIORITY_AMBIENT_HIGH)
					iLastHitLine = GET_GAME_TIMER()
					bHitLinePending = FALSE
				ENDIF
			ELSE
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_HUR", CONV_PRIORITY_AMBIENT_HIGH)
					iLastHitLine = GET_GAME_TIMER()
					bHitLinePending = FALSE
					bFirstHitLineDone = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bShootLinePending
			IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_SHOOT", CONV_PRIORITY_AMBIENT_HIGH)
				iLastShootLine = GET_GAME_TIMER()
				bShootLinePending = FALSE
			ENDIF
		ENDIF
		
		IF bWeaponLinePending
			IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_GUN", CONV_PRIORITY_AMBIENT_HIGH)
				iLastWeaponLine = GET_GAME_TIMER()
				bWeaponLinePending = FALSE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bWeaponLinePending OR bShootLinePending OR bHitLinePending
	
ENDFUNC

FUNC BOOL PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER(STRING sStub)
	TEXT_LABEL_23 sTemp
	sTemp += sStub
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		sTemp += "M"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		sTemp += "F"
	ELSE
		sTemp += "T"
	ENDIF
	IF CREATE_CONVERSATION(sSpeech, sBlockId, sTemp, CONV_PRIORITY_AMBIENT_HIGH)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VEHICLE_SEAT GET_FIRST_FREE_NON_DRIVER_VEHICLE_SEAT(VEHICLE_INDEX veh)
	VEHICLE_SEAT returnSeat = VS_DRIVER
	IF IS_VEHICLE_DRIVEABLE(veh)
		INT iCountSeats
		FOR iCountSeats = 0 TO (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(veh) - 1)
			IF returnSeat = VS_DRIVER
				VEHICLE_SEAT currentSeat = INT_TO_ENUM(VEHICLE_SEAT, iCountSeats)
				IF IS_VEHICLE_SEAT_FREE(veh, currentSeat)
					returnSeat = currentSeat
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN returnSeat
ENDFUNC

BOOL bPlayerResponse
//BOOL bFirstShout
INT iTimeLastPrompt

INT iCelebScene

STRING sPeeks[3]
INT iCurrentPeek
STRING sPeekStub = "left_"

VECTOR vLeftOfCelebPos1 = <<261.934052,126.242928,95.154053>>
VECTOR vLeftOfCelebPos2 = <<270.675873,152.476257,109.235565>> 
FLOAT fLeftOfCelebWidth = 16.000000

VECTOR vRightOfCelebPos1 = <<261.934052,126.242928,107.654053>>
VECTOR vRightOfCelebPos2 = <<248.353989,87.017921,92.389000>> 
FLOAT fRightOfCelebWidth = 16.000000

VECTOR vAtCelebPos1 = <<256.677887,126.572968,99.547844>>
VECTOR vAtCelebPos2 = <<266.447144,123.411964,105.672836>> 
FLOAT fAtCelebWidth = 5.750000

VECTOR vCelebAnimPos = << 260.242, 127.423, 101.650 >>
VECTOR fCelebAnimRot = <<0,0,-20>>

PROC CONTINUALLY_FACE_PED_FROM_COORD(VECTOR vCoord, BOOL bAnim = FALSE)
	IF VDIST(vCurrentCelebCoords, vCoord) > 2.5
		IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
			TASK_FOLLOW_NAV_MESH_TO_COORD(pedCelebGirl, vCoord, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS)
		ENDIF
	ELSE
		IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
			DO_CELEB_CONTINUALLY_FACES_PLAYER(bAnim)
			TASK_LOOK_AT_ENTITY(pedCelebGirl, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
		ENDIF
	ENDIF
ENDPROC

BOOL bCelebSteppedForward = FALSE
INT iForceTask = 0
FLOAT fTotalTimeNearCeleb
BOOL bFirstShout
BOOL bLaceyLineDone
BOOL bShowHeightOnBlip = FALSE
FLOAT fLingertime

PROC PICKUP_GIRL()

	IF NOT IS_PED_INJURED(pedCelebGirl)
	
		IF DOES_BLIP_EXIST(blipCelebGirl)
			SHOW_HEIGHT_ON_BLIP(blipCelebGirl, bShowHeightOnBlip)
		ENDIF
		
		SWITCH iStageProgress
		
			CASE 0
				//start left view peek
				REQUEST_ANIM_DICT("random@paparazzi@peek")
				REQUEST_ANIM_DICT("random@paparazzi@trans")
				REQUEST_ANIM_DICT("random@paparazzi@wait")
				IF HAS_ANIM_DICT_LOADED("random@paparazzi@peek")
				AND HAS_ANIM_DICT_LOADED("random@paparazzi@trans")
				AND HAS_ANIM_DICT_LOADED("random@paparazzi@wait")
					iCelebScene = CREATE_SYNCHRONIZED_SCENE(vCelebAnimPos, fCelebAnimRot)
					TASK_SYNCHRONIZED_SCENE(pedCelebGirl, iCelebScene, "random@paparazzi@peek", "left_peek_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_LOOK_AT_ENTITY(pedCelebGirl, PLAYER_PED_ID(), -1, SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					sPeeks[0] = "peek_a"
					sPeeks[1] = "peek_b"
					sPeeks[2] = "peek_c"
					SET_WANTED_LEVEL_MULTIPLIER(0.2)
					bPlayerResponse = FALSE
					bCelebSteppedForward = FALSE
					bShowHeightOnBlip = FALSE
					bMikeInsult = TRUE
					iForceTask = 0
					bOnBike = FALSE
					fTotalTimeNearCeleb = 0.0
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 1
				//loop peeks awaiting player and switch peeks if necessary
				BOOL bLeft
				BOOL bRight
				BOOL bSideChange
				
				IF IS_BIT_SET(iForceTask, 0)
					IF NOT IS_BIT_SET(iForceTask, 1)
						SET_BIT(iForceTask, 1)
					ELSE
						iForceTask = 0
					ENDIF
				ENDIF
				
				bSideChange = FALSE
				bLeft = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vRightOfCelebPos1, vRightOfCelebPos2, fRightOfCelebWidth)
				bRight = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vLeftOfCelebPos1, vLeftOfCelebPos2, fLeftOfCelebWidth)
				
				//switching sides
				IF ARE_STRINGS_EQUAL(sPeekStub, "right_")
					IF bRight
						sPeekStub = "left_"
						bSideChange = TRUE
					ENDIF
				ELSE
					IF bLeft
						sPeekStub = "right_"
						bSideChange = TRUE
					ENDIF
				ENDIF
				
				//looping peek anims
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iCelebScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iCelebScene) > 0.9)
				OR bSideChange
					IF iForceTask = 0
						CLEAR_PED_TASKS(pedCelebGirl)
						iCurrentPeek++
						IF iCurrentPeek >= 3
							iCurrentPeek = 0
						ENDIF
						TEXT_LABEL_23 sTemp
						sTemp = sPeekStub
						sTemp += sPeeks[iCurrentPeek]
						iCelebScene = CREATE_SYNCHRONIZED_SCENE(vCelebAnimPos, fCelebAnimRot)
						TASK_SYNCHRONIZED_SCENE(pedCelebGirl, iCelebScene, "random@paparazzi@peek", sTemp, SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
						SET_BIT(iForceTask, 0)
					ENDIF
				ENDIF
				
				IF bLeft
				OR bRight
				
					IF fTotalTimeNearCeleb <= 20.0
						fTotalTimeNearCeleb += GET_FRAME_TIME()
					ENDIF
					
					//check if a beckon anim is needed and fire one
					IF GET_GAME_TIMER() - iTimeLastPrompt > 5000
					AND iForceTask = 0
					
						IF VDIST2(vCurrentPlayerCoords, vCurrentCelebCoords) < 22.2*22.2
						
							STRING sShout
							
							IF fTotalTimeNearCeleb < 10
								IF bFirstShout
									sShout = "REPAP_SHT1"
								ELSE
									sShout = "REPAP_SHT2"
								ENDIF
							ELSE
								sShout = "REPAP_PRT"
							ENDIF
							
							PRINTSTRING("add peds2") PRINTNL()
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
								PRINTSTRING("franklin2") PRINTNL()
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
								PRINTSTRING("michael2") PRINTNL()
							ELSE
								ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "TREVOR")
								PRINTSTRING("trevor2") PRINTNL()
							ENDIF							
							
							IF CREATE_CONVERSATION(sSpeech, sBlockId, sShout, CONV_PRIORITY_AMBIENT_HIGH)
								bFirstShout = TRUE
								IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCelebGirl, <<7,7,7>>)
									CLEAR_PED_TASKS(pedCelebGirl)
									TEXT_LABEL_23 sTemp
									sTemp = sPeekStub
									sTemp += "come_here"
									iCelebScene = CREATE_SYNCHRONIZED_SCENE(vCelebAnimPos, fCelebAnimRot)
									TASK_SYNCHRONIZED_SCENE(pedCelebGirl, iCelebScene, "random@paparazzi@peek", sTemp, SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
									SET_BIT(iForceTask, 0)
								ENDIF
								iTimeLastPrompt = GET_GAME_TIMER()
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAtCelebPos1, vAtCelebPos2, fAtCelebWidth, FALSE)
				AND iForceTask = 0
					IF DOES_BLIP_EXIST(blipInitialMarker)
						REMOVE_BLIP(blipInitialMarker)
						IF NOT DOES_BLIP_EXIST(blipCelebGirl)
							blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
						ENDIF
					ENDIF
					CLEAR_PED_TASKS(pedCelebGirl)
					iCelebScene = CREATE_SYNCHRONIZED_SCENE(vCelebAnimPos, fCelebAnimRot)
					IF bRight
						TASK_SYNCHRONIZED_SCENE(pedCelebGirl, iCelebScene, "random@paparazzi@trans", "trans_left_to_wait", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					ELSE
						TASK_SYNCHRONIZED_SCENE(pedCelebGirl, iCelebScene, "random@paparazzi@trans", "trans_right_to_wait", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					ENDIF
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
					SET_BIT(iForceTask, 0)
					iStageProgress++
				ENDIF
				
			BREAK
			
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iCelebScene) 
				AND GET_SYNCHRONIZED_SCENE_PHASE(iCelebScene) > 0.9
					IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						CLEAR_PED_TASKS(pedCelebGirl)
						TASK_PLAY_ANIM(pedCelebGirl, "random@paparazzi@wait", "wait_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
						iFaceStage = 2
						SETTIMERA(0)
					ENDIF
					iStageProgress++	
				ENDIF
				//trans to stand
			BREAK
			
			CASE 3
				//check here if the player is in a suitable vehicle
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_PLAYER_ABLE_TO_PICK_UP_CELEB()
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCelebsCar)
						vehCelebsCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_AS_MISSION_ENTITY(vehCelebsCar, TRUE, TRUE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehCelebsCar, FALSE)
						SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_UNLOCKED)
						bPlayersCarUsed = TRUE
					ENDIF
				ENDIF
				IF NOT bPlayersCarUsed
					SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_UNLOCKED)
					//IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_EXP", CONV_PRIORITY_AMBIENT_HIGH)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, "REPAP_EXP", "REPAP_EXP_2", CONV_PRIORITY_AMBIENT_HIGH)
						SETTIMERA(0)
						bShowHeightOnBlip = TRUE
						iStageProgress = 100
					ENDIF
				ELSE
					//IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_EXPB", CONV_PRIORITY_AMBIENT_HIGH)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, "REPAP_EXPB", "REPAP_EXPB_2", CONV_PRIORITY_AMBIENT_HIGH)
						SETTIMERA(0)
						bShowHeightOnBlip = TRUE
						iStageProgress++
					ENDIF
				ENDIF
				IF TIMERA() > 2000
					CONTINUALLY_FACE_PED_FROM_COORD(vCreateCelebGirlPos, TRUE)
				ENDIF
			BREAK
			
			CASE 4
			
				BLIP_PAPS()
				
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				
					IF IS_ENTITY_AT_ENTITY(vehCelebsCar, pedCelebGirl, g_vAnyMeansLocate + <<12,12,2>>)
					AND NOT bPlayersCarUsed
						IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						AND NOT bCelebSteppedForward
							SEQUENCE_INDEX seqMoveTemp
							OPEN_SEQUENCE_TASK(seqMoveTemp)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<259.2552, 122.2986, 100.4410>>, PEDMOVEBLENDRATIO_WALK)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqMoveTemp)
							TASK_PERFORM_SEQUENCE(pedCelebGirl, seqMoveTemp)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
							CLEAR_SEQUENCE_TASK(seqMoveTemp)
							bCelebSteppedForward = TRUE
						ELSE
							SET_PED_MAX_MOVE_BLEND_RATIO(pedCelebGirl, PEDMOVE_WALK)
							CONTINUALLY_FACE_PED_FROM_COORD(<<259.2552, 122.2986, 100.4410>>)
						ENDIF
					ELSE
						CONTINUALLY_FACE_PED_FROM_COORD(vCreateCelebGirlPos, TRUE)
					ENDIF
					
					IF NOT bPlayersCarUsed
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
							IF DOES_BLIP_EXIST(blipCelebVeh)
								REMOVE_BLIP(blipCelebVeh)
							ENDIF
							IF NOT DOES_BLIP_EXIST(blipCelebGirl)
								blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ENTITY_AT_ENTITY(vehCelebsCar, pedCelebGirl, g_vAnyMeansLocate)
					AND DOES_VEHICLE_HAVE_FREE_SEAT(vehCelebsCar)
					AND (TIMERA() > 6000 OR NOT bPlayersCarUsed)
						IF NOT bPlayerResponse
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_ONF", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_ONM", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_ONT", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ENDIF
							ELSE
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_INF", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_INM", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_INT", CONV_PRIORITY_AMBIENT_HIGH)
										bPlayerResponse = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT bPlayersCarUsed
								iStageProgress = -5
							ELSE
								iStageProgress++
							ENDIF
							CLEAR_PED_TASKS(pedCelebGirl)
							TASK_ENTER_VEHICLE(pedCelebGirl, vehCelebsCar, DEFAULT_TIME_NEVER_WARP, GET_FIRST_FREE_NON_DRIVER_VEHICLE_SEAT(vehCelebsCar), PEDMOVE_WALK, ECF_DONT_JACK_ANYONE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE -5
				IF bPlayersCarUsed
					CONTINUALLY_FACE_PED_FROM_COORD(vCreateCelebGirlPos, TRUE)
				ELSE
					CONTINUALLY_FACE_PED_FROM_COORD(<<259.2552, 122.2986, 100.4410>>)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCelebsCar, DEFAULT, 1)
						iStageProgress = 5
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
					IF NOT IS_PED_INJURED(pedCelebGirl)
					AND IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
						IF DOES_BLIP_EXIST(blipCelebGirl)
							REMOVE_BLIP(blipCelebGirl)
						ENDIF
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						SET_STAGE(pescape_LOSEPAPS)						
					ELSE
						IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
							AND NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar, TRUE)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(vehCelebsCar, <<225.177872,125.663177,113.664673>>, <<262.303223,111.877052,97.163391>>, 37.000000)
								IF bPlayersCarUsed
									SET_PED_FLEE_ATTRIBUTES(pedCelebGirl, FA_USE_VEHICLE, TRUE)
									sStropString = "REPAP_GUP"
									SET_STAGE(pescape_STROPSOFF)
								ELSE
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
									SET_PED_FLEE_ATTRIBUTES(pedCelebGirl, FA_USE_VEHICLE, TRUE)
									sStropString = "REPAP_CP"
									SET_STAGE(pescape_STROPSOFF)
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
							IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								CLEAR_PED_TASKS(pedCelebGirl)
								TASK_ENTER_VEHICLE(pedCelebGirl, vehCelebsCar, DEFAULT_TIME_NEVER_WARP, GET_FIRST_FREE_NON_DRIVER_VEHICLE_SEAT(vehCelebsCar), PEDMOVE_WALK, ECF_DONT_JACK_ANYONE)
							ELSE
								IF VDIST(vCurrentCelebCoords, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCelebsCar, <<1,0,0>>)) < 1
									SET_PED_MAX_MOVE_BLEND_RATIO(pedCelebGirl, PEDMOVEBLENDRATIO_WALK)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//**************************************************
			//player has to go pick up the celebs car.
			
			CASE 100 // player confirms they will go and get the car
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_YSF", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_YSM", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
							bMikeInsult = FALSE
						ELSE
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_YST", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF iStageProgress = 101
					fLingertime = 0.0
				ENDIF
				
				CONTINUALLY_FACE_PED_FROM_COORD(vCreateCelebGirlPos, TRUE)
				
			BREAK
			
			CASE 101 //player goes to get car and comes back to celeb
			
				IF VDIST2(vCurrentPlayerCoords, vCurrentCelebCoords) < 7*7
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
					IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 3
						fLingertime += GET_FRAME_TIME()
					ELSE
						fLingertime -= GET_FRAME_TIME()*2
					ENDIF
					
					IF fLingertime > 10.0
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PRT", CONV_PRIORITY_AMBIENT_HIGH)
							fLingertime = 0.0
						ENDIF
					ENDIF
					
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
					AND IS_PLAYER_ABLE_TO_PICK_UP_CELEB()
						bPlayersCarUsed = TRUE
						bMikeInsult = TRUE
						vehCelebsCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_AS_MISSION_ENTITY(vehCelebsCar, TRUE, TRUE)
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
						IF DOES_BLIP_EXIST(blipCelebVeh)
							REMOVE_BLIP(blipCelebVeh)
						ENDIF
						IF NOT DOES_BLIP_EXIST(blipCelebGirl)
							blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipCelebGirl)
							REMOVE_BLIP(blipCelebGirl)
						ENDIF
						IF NOT DOES_BLIP_EXIST(blipCelebVeh)
							blipCelebVeh = CREATE_BLIP_FOR_VEHICLE(vehCelebsCar)
						ENDIF
					ENDIF
					
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
						IF DOES_BLIP_EXIST(blipCelebVeh)
							REMOVE_BLIP(blipCelebVeh)
						ENDIF
						IF NOT DOES_BLIP_EXIST(blipCelebGirl)
							blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
						ENDIF
						SET_AUDIO_SCENE_ACTIVE("MAG_2_ESCAPE_PAP_CHASE")
						iStageProgress = 4
					ENDIF
					
					IF NOT bMikeInsult
						IF VDIST2(vCurrentPlayerCoords, vCurrentCelebCarCoords) < 3*3
						AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_MIKE", CONV_PRIORITY_AMBIENT_HIGH)
								bMikeInsult = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				CONTINUALLY_FACE_PED_FROM_COORD(vCreateCelebGirlPos, TRUE)
			BREAK
			
			//
			//**************************************************
			
		ENDSWITCH
		
		IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
			IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				IF VDIST(vCurrentPlayerCoords, vCreateCelebGirlPos) > 75
					SET_STAGE(pescape_STROPSOFF)
				ENDIF
				IF HAS_PLAYER_SCARED_A_PAP()
					SET_STAGE(pescape_CELEBFLEES)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bPlayersCarUsed
			IF NOT IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
					SET_STAGE(pescape_CELEBFLEES)
				ENDIF
			ELSE
				IF NOT bPlayersCarUsed
					IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
						IF GET_ENTITY_HEALTH(vehCelebsCar) < iCelebCarDamageLastFrame
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehCelebsCar, PLAYER_PED_ID())
								iDamageToCelebCar += iCelebCarDamageLastFrame - GET_ENTITY_HEALTH(vehCelebsCar)
							ENDIF
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehCelebsCar)
						ENDIF
						IF iDamageToCelebCar > 250
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedCelebGirl, vehCelebsCar)
							AND VDIST(vCurrentCelebCoords, vCurrentCelebCarCoords) < 25
								SET_STAGE(pescape_CELEBFLEES)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iCelebCarDamageLastFrame = GET_ENTITY_HEALTH(vehCelebsCar)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedCelebGirl)
			IF IS_PED_FLEEING(pedCelebGirl)
				IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
					SET_STAGE(pescape_CELEBFLEES)
				ENDIF
			ENDIF
		ENDIF
				
	ENDIF
	
ENDPROC

INT iLastWhine

FUNC BOOL DO_CELEB_WHINING()
	INT iClosestPap = GET_CLOSEST_PAP_INDEX()
	IF GET_GAME_TIMER() - iLastWhine > 5000
		IF DOES_ENTITY_EXIST(vehCelebsCar)
		AND IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar, TRUE)
				IF iClosestPap != -1
				AND GET_GAME_TIMER() - pedPaps[iClosestPap].m_iLastFlash < 3000
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_WHINE", CONV_PRIORITY_AMBIENT_HIGH)
						iLastWhine = GET_GAME_TIMER()
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_EXIT", CONV_PRIORITY_AMBIENT_HIGH)
					iLastWhine = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

INT iLastChatter = 0

PROC DO_CELEB_CHATTER(STRING root)
	IF GET_GAME_TIMER() - iLastChatter > 5000
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF CREATE_CONVERSATION(sSpeech, sBlockId, root, CONV_PRIORITY_AMBIENT_HIGH)
				iLastChatter = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_BLIP_SWAPPING(BOOL bBlipHome = TRUE)

	IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
	AND NOT IS_PED_INJURED(pedCelebGirl)
		IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
			IF bBlipHome
				IF NOT DOES_BLIP_EXIST(blipDropOffInCar)
					blipDropOffInCar = CREATE_BLIP_FOR_COORD(vDropOffPos, TRUE)
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(blipCelebGirl)
				REMOVE_BLIP(blipCelebGirl)
			ENDIF
			IF DOES_BLIP_EXIST(blipCelebVeh)
				REMOVE_BLIP(blipCelebVeh)
			ENDIF
		ELSE
		
			IF DOES_BLIP_EXIST(blipDropOffInCar)
				REMOVE_BLIP(blipDropOffInCar)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
				IF NOT DOES_BLIP_EXIST(blipCelebVeh)
					blipCelebVeh = CREATE_BLIP_FOR_VEHICLE(vehCelebsCar)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
				IF NOT DOES_BLIP_EXIST(blipCelebGirl)
					blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipCelebGirl)
					REMOVE_BLIP(blipCelebGirl)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

BOOL bShouldHoldForPaps
BOOL bIsHeldForPaps
INT iChatStage
INT iLastChatFinish

PROC LOSE_PAPS()

	IF NOT IS_PED_INJURED(pedCelebGirl)
	AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
	
		IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar, TRUE)
			IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
				TASK_ENTER_VEHICLE(pedCelebGirl, vehCelebsCar, DEFAULT_TIME_NEVER_WARP, GET_FIRST_FREE_NON_DRIVER_VEHICLE_SEAT(vehCelebsCar), PEDMOVE_RUN, ECF_DONT_JACK_ANYONE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
		AND VDIST2(GET_ENTITY_COORDS(pedCelebGirl), GET_ENTITY_COORDS(GET_CLOSEST_PAP())) < 10*10
		AND (GET_ENTITY_SPEED(vehCelebsCar) < 5 OR NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehCelebsCar)))
			IF bPlayersCarUsed
				IF NOT IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@STD@PS@IDLE_PANIC", "sit")
				AND IS_PED_SITTING_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
					TASK_PLAY_ANIM(pedCelebGirl, "VEH@STD@PS@IDLE_PANIC", "sit", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@LOW@FRONT_PS@IDLE_PANIC", "sit")
				AND IS_PED_SITTING_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
					TASK_PLAY_ANIM(pedCelebGirl, "VEH@LOW@FRONT_PS@IDLE_PANIC", "sit", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
				ENDIF
			ENDIF
		ELSE
			IF (IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@STD@PS@IDLE_PANIC", "sit") OR IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@LOW@FRONT_PS@IDLE_PANIC", "sit"))
			AND IS_PED_SITTING_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
				CLEAR_PED_TASKS(pedCelebGirl)
			ENDIF
		ENDIF
		
		SWITCH iStageProgress
			CASE 0
				IF TIMERA() > 2000
					IF DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
					AND NOT IS_PED_INJURED(GET_CLOSEST_PAP())
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
						ADD_PED_FOR_DIALOGUE(sSpeech, 4, GET_CLOSEST_PAP(), "PAPARAZZO")
					ENDIF
					currentPapStage = pap_first_seq
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_GO", CONV_PRIORITY_AMBIENT_HIGH)
						SET_WANTED_LEVEL_MULTIPLIER(0.2)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCelebGirl, TRUE)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgCeleb, RELGROUPHASH_PLAYER)
//						SET_PED_RELATIONSHIP_GROUP_HASH(pedCelebGirl, RELGROUPHASH_PLAYER)
						objFitTest = CREATE_OBJECT(PROP_LD_TEST_01, vCreateCelebGirlPos)
						SET_ENTITY_COLLISION(objFitTest, FALSE)
						SET_ENTITY_VISIBLE(objFitTest, FALSE)
						FREEZE_ENTITY_POSITION(objFitTest, TRUE)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				DO_PAP_LINES()
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedCelebGirl, <<243.557053,77.512650,90.080025>>, <<268.208313,144.218857,109.853004>> , 23.250000)
					iStageProgress++
					iTimeStartedChase = GET_GAME_TIMER()
					currentPapStage = pap_chase
				ENDIF
			BREAK
			CASE 2
				IF NOT bShouldHoldForPaps
					IF iChatStage != 1
					AND iChatStage != 3
					AND iChatStage != 6
					AND iChatStage != 9
					AND iChatStage != 11
					AND iChatStage != 13
						bIsHeldForPaps = FALSE
						IF DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
						AND IS_ENTITY_AT_ENTITY(GET_CLOSEST_PAP(), vehCelebsCar, <<5,5,3>>)
							bShouldHoldForPaps = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bIsHeldForPaps = TRUE
					ENDIF
					IF DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
						IF NOT IS_ENTITY_AT_ENTITY(GET_CLOSEST_PAP(), vehCelebsCar, <<5,5,3>>)
							bShouldHoldForPaps = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bIsHeldForPaps
				
					IF NOT DO_CELEB_WARNING_LINES()
						IF NOT DO_CELEB_WHINING()
							DO_PAP_LINES()
						ENDIF
					ENDIF
					
				ELSE
				
					SWITCH iChatStage
						CASE 0
//							IF MANAGE_CONVERSATION("REPAP_CHT", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
//								iChatStage++
//							ENDIF
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CHT", CONV_PRIORITY_AMBIENT_HIGH)
									iChatStage++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_RES1")
									iLastChatFinish = GET_GAME_TIMER()
									iChatStage++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF NOT bShouldHoldForPaps
								AND CAN_PLAYER_AND_CELEB_CHAT()
								AND NOT DO_CELEB_WARNING_LINES()
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_CHT2")
										iChatStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 3
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_RESP2L", CONV_PRIORITY_AMBIENT_HIGH)
									iLastChatFinish = GET_GAME_TIMER()
									iChatStage++
								ENDIF
							ENDIF
						BREAK
						CASE 4
							IF GET_GAME_TIMER() - iLastChatFinish > 6000
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF MANAGE_CONVERSATION("REPAP_CHT3M", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF MANAGE_CONVERSATION("REPAP_CHT3F", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELSE
									IF MANAGE_CONVERSATION("REPAP_CHT3T", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ENDIF
								IF iChatStage > 4
									iLastChatFinish = GET_GAME_TIMER()
								ENDIF
							ENDIF
						BREAK
						CASE 5
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF MANAGE_CONVERSATION("REPAP_CHT4M", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF MANAGE_CONVERSATION("REPAP_CHT4F", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELSE
									IF MANAGE_CONVERSATION("REPAP_CHT4T", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ENDIF
								IF iChatStage > 5
									iLastChatFinish = GET_GAME_TIMER()
								ENDIF
							ENDIF
						BREAK
						CASE 6
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF MANAGE_CONVERSATION("REPAP_CHT5M", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									IF MANAGE_CONVERSATION("REPAP_CHT5F", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ELSE
									IF MANAGE_CONVERSATION("REPAP_CHT5T", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES() AND NOT bShouldHoldForPaps)
										iChatStage++
									ENDIF
								ENDIF
								IF iChatStage > 6
									iLastChatFinish = GET_GAME_TIMER()
								ENDIF
							ENDIF
						BREAK
						CASE 7
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF NOT bShouldHoldForPaps
								AND CAN_PLAYER_AND_CELEB_CHAT()
								AND NOT DO_CELEB_WARNING_LINES()
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_CHT6")
										iChatStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 8
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_RESP6L", CONV_PRIORITY_AMBIENT_HIGH)
									iLastChatFinish = GET_GAME_TIMER()
									iChatStage++
								ENDIF
							ENDIF
						BREAK
						CASE 9
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF NOT bShouldHoldForPaps
								AND CAN_PLAYER_AND_CELEB_CHAT()
								AND NOT DO_CELEB_WARNING_LINES()
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_CHT7")
										iChatStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 10
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_RESP7L", CONV_PRIORITY_AMBIENT_HIGH)
									iLastChatFinish = GET_GAME_TIMER()
									iChatStage++
								ENDIF
							ENDIF
						BREAK
						CASE 11
							IF GET_GAME_TIMER() - iLastChatFinish > 8000
								IF NOT bShouldHoldForPaps
								AND CAN_PLAYER_AND_CELEB_CHAT()
								AND NOT DO_CELEB_WARNING_LINES()
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_CHT8")
										iChatStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 12
							IF NOT bShouldHoldForPaps
							AND CAN_PLAYER_AND_CELEB_CHAT()
							AND NOT DO_CELEB_WARNING_LINES()
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_RESP8L", CONV_PRIORITY_AMBIENT_HIGH)
									iChatStage++
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(GET_CLOSEST_PAP())
					SETTIMERA(0)
					iStageProgress++
					KILL_FACE_TO_FACE_CONVERSATION()
					IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
						currentPapStage = pap_cleanup
					ENDIF
				ENDIF
				
				IF g_bMagDemoActive OR bDebugTestingMagdemo
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<244.217529,441.012817,126.276711>>,<<33.500000,40.000000,14.750000>>)
					AND ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
						currentPapStage = pap_magdemo_back_off
						iTimeOfBackOff = GET_GAME_TIMER()
					ENDIF
				ENDIF
				
			BREAK
			CASE 3
				IF TIMERA() > 3000
				AND ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
					SET_AUDIO_SCENE_ACTIVE("MAG_2_ESCAPE_PAP_TAKE_ACTRESS_HOME")
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_LOS", CONV_PRIORITY_AMBIENT_HIGH)
						SET_STAGE(pescape_DRIVEBACK)
					ENDIF
				ENDIF
			BREAK
			CASE 4
//				INT iTemp
//				REPEAT COUNT_OF(vehPaps) iTemp
//					IF IS_VEHICLE_DRIVEABLE(vehPaps[iTemp].m_veh)
//					AND NOT IS_PED_INJURED(GET_PAP_FROM_PAP_VEH(iTemp, 0))
//					AND IS_PED_IN_VEHICLE(GET_PAP_FROM_PAP_VEH(iTemp, 0), vehPaps[iTemp].m_veh)
//						TASK_VEHICLE_MISSION(GET_PAP_FROM_PAP_VEH(iTemp, 0), vehPaps[iTemp].m_veh, NULL, MISSION_STOP, 10, DRIVINGMODE_AVOIDCARS_RECKLESS, 10, 10, TRUE)
//					ENDIF
//				ENDREPEAT
//				iStageProgress++
			BREAK
			CASE 5
				
//				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<244.217529,441.012817,126.276711>>,<<33.500000,40.000000,14.750000>>)
//				AND ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
//					iStageProgress = 3
//					currentPapStage = pap_cleanup
//					SETTIMERA(1000)
//				ENDIF
			BREAK
		ENDSWITCH
		
//		IF g_bMagDemoActive
//		OR bDebugTestingMagdemo
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
//			AND IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
//				IF NOT DOES_BLIP_EXIST(blipDropOffInCar)
//					blipDropOffInCar = CREATE_BLIP_FOR_COORD(vDropOffPos)
//				ENDIF
//				IF NOT bRouteBLipped
//					START_GPS_MULTI_ROUTE(HUD_COLOUR_YELLOW, TRUE)
//					ADD_POINT_TO_GPS_MULTI_ROUTE(<<430.4373, 310.4131, 101.9775>>)
//					ADD_POINT_TO_GPS_MULTI_ROUTE(vDropOffPos)
//					SET_GPS_MULTI_ROUTE_RENDER(TRUE)
//					bRouteBLipped = TRUE
//				ENDIF
//			ELSE
//				IF DOES_BLIP_EXIST(blipDropOffInCar)
//					REMOVE_BLIP(blipDropOffInCar)
//				ENDIF
//				CLEAR_GPS_MULTI_ROUTE()
//				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
//				bRouteBLipped = FALSE
//			ENDIF
//		ENDIF
		
		DO_BLIP_SWAPPING(FALSE)
	ENDIF
	
ENDPROC

PROC LOSE_THE_COPS()
	IF thisStage <> pescape_LOSECOPS
		IF DOES_BLIP_EXIST(blipDropOffInCar)
			REMOVE_BLIP(blipDropOffInCar)
		ENDIF
		IF DOES_BLIP_EXIST(blipDropOffOnFoot)
			REMOVE_BLIP(blipDropOffOnFoot)
		ENDIF
		IF DOES_BLIP_EXIST(blipCelebGirl)
			REMOVE_BLIP(blipCelebGirl)
		ENDIF
		savedStage = thisStage
		thisStage = pescape_LOSECOPS
	ELSE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
				IF NOT DO_CELEB_WARNING_LINES()
					DO_CELEB_CHATTER("REPAP_COP1")
				ENDIF
			ELSE
				SET_STAGE(pescape_CELEBFLEES)
			ENDIF
		ELSE
			IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_COP2", CONV_PRIORITY_AMBIENT_HIGH)
				thisStage = savedStage
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRIVE_BACK()

	IF g_bMagDemoActive OR bDebugTestingMagdemo
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.7)
	ENDIF
	
	IF bPlayersCarUsed
	AND NOT IS_PED_INJURED(pedCelebGirl)
	
		IF NOT IS_PED_GROUPED(pedCelebGirl)
		
			PRINTLN("LACEY IS NOT GROUPED")
			IF NOT DOES_BLIP_EXIST(blipCelebGirl)
				blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
			ENDIF
			
		ELSE
		
			IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
				IF NOT DOES_BLIP_EXIST(blipDropOffInCar)
					blipDropOffInCar = CREATE_BLIP_FOR_COORD(vDropOffPos, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOffOnFoot)
					REMOVE_BLIP(blipDropOffOnFoot)
				ENDIF
			ELSE
				PRINTLN("LACEY OR MIKE NOT IN CAR")
				IF NOT DOES_BLIP_EXIST(blipDropOffOnFoot)
					blipDropOffOnFoot = CREATE_BLIP_FOR_COORD(vDropOffOnFootPos, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOffInCar)
					REMOVE_BLIP(blipDropOffInCar)
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipCelebGirl)
				REMOVE_BLIP(blipCelebGirl)
			ENDIF
			
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND DOES_VEHICLE_HAVE_FREE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				vehCelebsCar = NULL
				vehCelebsCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				SET_ENTITY_AS_MISSION_ENTITY(vehCelebsCar, TRUE, TRUE)
				SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_UNLOCKED)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehCelebsCar, FALSE)
			ENDIF
		ENDIF	
		
		IF DOES_BLIP_EXIST(blipDropOffOnFoot)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedCelebGirl)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffOnFootPos, <<15,15,15>>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<231.79820, 672.76831, 189.2>>, <<1,1,2.5>>, TRUE)
				SET_STAGE(pescape_DROPOFFONFOOT)
			ENDIF
		ELSE
			IF iStageProgress <> 100
				IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
				AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
					IF IS_ENTITY_IN_ANGLED_AREA(vehCelebsCar, <<225.593765,681.148865,188.426193>>, <<232.019989,681.311279,191.984283>>, 8.750000)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<226.67882, 681.06366, 189>>, <<1,1,2.5>>, TRUE)
						SETTIMERA(0)
						iStageProgress = 100
					ENDIF
				ELSE
					IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
						IF GET_SCRIPT_TASK_STATUS(pedCelebGirl, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedCelebGirl, vehCelebsCar, DEFAULT_TIME_NEVER_WARP, GET_FIRST_FREE_NON_DRIVER_VEHICLE_SEAT(vehCelebsCar), PEDMOVE_RUN, ECF_DONT_JACK_ANYONE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	
		IF iStageProgress <> 100
			IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
			AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
				IF IS_ENTITY_IN_ANGLED_AREA(vehCelebsCar, <<225.593765,681.148865,188.426193>>, <<232.019989,681.311279,191.984283>>, 8.750000)
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<226.67882, 681.06366, 189>>, <<1,1,2.5>>, TRUE)
					SETTIMERA(0)
					iStageProgress=100
				ENDIF
			ENDIF
		ENDIF
		
		DO_BLIP_SWAPPING()
		
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0 
			IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()		
				IF (IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@STD@PS@IDLE_PANIC", "sit") OR IS_ENTITY_PLAYING_ANIM(pedCelebGirl, "VEH@LOW@FRONT_PS@IDLE_PANIC", "sit"))
					CLEAR_PED_TASKS(pedCelebGirl)
				ENDIF
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_HM", CONV_PRIORITY_AMBIENT_HIGH)
					SET_WANTED_LEVEL_MULTIPLIER(0.5)
					iStageProgress++
				ENDIF
				IF NOT IS_CULT_FINISHED()
					ADD_ALTRUIST_CULT_BLIP()
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT DO_CELEB_WARNING_LINES()
				IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_LIFTM", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_LIFTF", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_LIFTT", CONV_PRIORITY_AMBIENT_HIGH)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT DO_CELEB_WARNING_LINES()
				IF ARE_PLAYER_AND_CELEB_IN_CELEBS_CAR()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PLEAD", CONV_PRIORITY_AMBIENT_HIGH)
						SETTIMERA(0)
						IF g_bMagDemoActive OR bDebugTestingMagdemo
							iStageProgress = 99
						ELSE
							iStageProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF TIMERA() > 7000
				IF MANAGE_CONVERSATION("REPAP_HOME1", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF MANAGE_CONVERSATION("REPAP_HOME1M", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF MANAGE_CONVERSATION("REPAP_HOME1F", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ELSE
				IF MANAGE_CONVERSATION("REPAP_HOME1T", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF MANAGE_CONVERSATION("REPAP_HOME1L", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
				SETTIMERA(0)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 6
			IF NOT DO_CELEB_WARNING_LINES()
				IF TIMERA() > 7000
					IF CAN_PLAYER_AND_CELEB_CHAT()
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_HOME2", CONV_PRIORITY_AMBIENT_HIGH)
							iStageProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_HOME2")
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF MANAGE_CONVERSATION("REPAP_HOME2L", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
				iStageProgress++
			ENDIF
		BREAk
		
		CASE 9
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_RESP2")
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF TIMERA() > 7000
				IF MANAGE_CONVERSATION("REPAP_HOME3", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 11
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_HOME3")
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 12
			IF TIMERA() > 7000
				IF MANAGE_CONVERSATION("REPAP_HOME4", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 13
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_HOME4")
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 14
			IF TIMERA() > 7000
				IF MANAGE_CONVERSATION("REPAP_NOBAN", CAN_PLAYER_AND_CELEB_CHAT() AND NOT DO_CELEB_WARNING_LINES())
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 15
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("REPAP_NOBAN")
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 16
			IF NOT DO_CELEB_WARNING_LINES()
				IF CAN_PLAYER_AND_CELEB_CHAT()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_NOBANL", CONV_PRIORITY_AMBIENT_HIGH)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 17
			DO_CELEB_WARNING_LINES()
		BREAK
		
		CASE 100
			IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCelebsCar, 5, 5)
				SET_AUDIO_SCENE_ACTIVE("")
				SET_STAGE(pescape_DROPOFF)
			ENDIF
		BREAK
		
	ENDSWITCH
		
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		LOSE_THE_COPS()
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT() 
		AND NOT IS_CULT_FINISHED() 
		AND thisStage != pescape_AT_CULT
	        IF NOT IS_PED_INJURED(pedCelebGirl) 
                IF IS_ENTITY_AT_COORD(pedCelebGirl, << -1034.6, 4918.6, 205.9 >>, << 5, 5, 5 >>)
					IF NOT IS_PED_GROUP_MEMBER(pedCelebGirl, GET_PLAYER_GROUP(PLAYER_ID()))
						SET_PED_AS_GROUP_MEMBER(pedCelebGirl, GET_PLAYER_GROUP(PLAYER_ID()))
					ENDIF
					PRINTLN("\n\n\nGOING TO CULT STAGE! \n\n\n")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
					thisStage = pescape_AT_CULT
                ENDIF
	        ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DROP_OFF()

	SEQUENCE_INDEX seqTemp
	
	IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
		
		SWITCH iStageProgress
		
			CASE 0
			
				IF NOT IS_PED_INJURED(pedCelebGirl)
				AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				
					REMOVE_ALTRUIST_CULT_BLIP()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
					
					IF DOES_BLIP_EXIST(blipDropOffInCar)
						REMOVE_BLIP(blipDropOffInCar)
					ENDIF
					IF DOES_BLIP_EXIST(blipCelebGirl)
						REMOVE_BLIP(blipCelebGirl)
					ENDIF
					IF DOES_BLIP_EXIST(blipCelebVeh)
						REMOVE_BLIP(blipCelebVeh)
					ENDIF
					
					IF IS_PED_IN_GROUP(pedCelebGirl)
						REMOVE_PED_FROM_GROUP(pedCelebGirl)
					ENDIF
					
					KILL_FACE_TO_FACE_CONVERSATION()
					
					IF NOT IS_PED_INJURED(pedCelebGirl)
						TASK_LOOK_AT_COORD(pedCelebGirl, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCelebsCar, <<-10, 5, 0>>), -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					ENDIF
					
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCelebsCar, <<10, 5, 0>>), 15000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)
					
					IF bPlayersCarUsed
					OR g_bMagDemoActive 
					OR bDebugTestingMagdemo
						WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, "REPAP_THK", "REPAP_THK_1", CONV_PRIORITY_AMBIENT_HIGH)
							WAIT(0)
						ENDWHILE
					ELSE
						WHILE NOT CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_THK", CONV_PRIORITY_AMBIENT_HIGH)
							WAIT(0)
						ENDWHILE
					ENDIF
					
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 750,FALSE,TRUE)
					
					IF NOT IS_PED_INJURED(pedCelebGirl)
						TASK_LOOK_AT_COORD(pedCelebGirl, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCelebsCar, <<-10, 5, 0>>), -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCelebGirl)
					ENDIF
					
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCelebsCar, <<10, 5, 0>>), 15000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					
					iStageProgress++
					
				ENDIF
				
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_PED_INJURED(pedCelebGirl)
					OPEN_SEQUENCE_TASK(seqTemp)
					TASK_LEAVE_VEHICLE(NULL, vehCelebsCar)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffOnFootPos, PEDMOVEBLENDRATIO_WALK, 30000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fDropOffOnFootHeading)
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
					iStageProgress++
					SETTIMERA(0)
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_PED_INJURED(pedCelebGirl)
				AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
					IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
						SETTIMERA(0)
						IF bPlayersCarUsed
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
							iStageProgress = 5
						ELSE
							iStageProgress++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_PED_INJURED(pedCelebGirl)
				AND IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				AND TIMERA() > 1000
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehCelebsCar, 10000, SLF_DEFAULT, SLF_LOOKAT_HIGH)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 4
				IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
					SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_LOCKED)
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 5
				IF TIMERA() > 1000
					IF LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
						IF NOT bPlayersCarUsed
							IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_TAXI_PHONE_INTRODUCED)
								ADD_HELP_TO_FLOW_QUEUE("AM_H_CALLTX", FHP_MEDIUM, 0)			
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_TAXI_PHONE_INTRODUCED)
							ENDIF
						ENDIF
						missionPassed()
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC DROP_OFF_ON_FOOT()

	SWITCH iStageProgress
	
		CASE 0
		
			REMOVE_ALTRUIST_CULT_BLIP()
		
			IF NOT IS_PED_INJURED(pedCelebGirl)
				IF NOT DOES_BLIP_EXIST(blipCelebGirl)
					blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOffOnFoot)
					REMOVE_BLIP(blipDropOffOnFoot)
				ENDIF
				IF DOES_BLIP_EXIST(blipDropOffInCar)
					REMOVE_BLIP(blipDropOffInCar)
				ENDIF
				CLEAR_PED_TASKS(pedCelebGirl)
				SEQUENCE_INDEX seqTemp
				OPEN_SEQUENCE_TASK(seqTemp)
				TASK_LEAVE_VEHICLE(NULL, vehCelebsCar)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffOnFootPos, PEDMOVEBLENDRATIO_WALK, 30000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fDropOffOnFootHeading)
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
				CLOSE_SEQUENCE_TASK(seqTemp)
				TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTemp)
				CLEAR_SEQUENCE_TASK(seqTemp)
				IF IS_PED_IN_GROUP(pedCelebGirl)
					REMOVE_PED_FROM_GROUP(pedCelebGirl)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iStageProgress++
			ENDIF
			
//			IF g_bMagDemoActive
//				IF NOT g_bMagDemoREPapDone
//					g_bMagDemoREPapDone = TRUE
//				ENDIF
//			ENDIF
			
		BREAK
		
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_PED_INJURED(pedCelebGirl)
				IF IS_ENTITY_AT_COORD(pedCelebGirl, vDropOffOnFootPos, <<3,3,3>>)
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffOnFootPos, <<5,5,5>>)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(pedCelebGirl)
				DO_CELEB_CONTINUALLY_FACES_PLAYER()
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_THK", CONV_PRIORITY_AMBIENT_HIGH)
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 150,FALSE,TRUE)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_PED_INJURED(pedCelebGirl)
				SETTIMERA(0)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERA() > 2000
				IF LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
					missionPassed()
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC CELEB_FLEES()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	IF NOT IS_PED_INJURED(pedCelebGirl)
	AND VDIST2(vCurrentPlayerCoords, vCurrentCelebCoords) < 30*30
		CLEAR_PED_TASKS(pedCelebGirl)
		IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CRZY", CONV_PRIORITY_AMBIENT_HIGH)
			FLEE_ALL()
			PRINTLN("\n\n\n\n setting cleanup stage 2")
			SET_STAGE(pescape_CLEAN_UP)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCelebGirl, RELGROUPHASH_HATES_PLAYER)
	ELSE
		FLEE_ALL()
		PRINTLN("\n\n\n\n setting cleanup stage 1")
		SET_STAGE(pescape_CLEAN_UP)
	ENDIF
ENDPROC

BOOL bResetPapsChaseStatus

PROC CELEB_STROPS_OFF(STRING root)

	IF currentPapStage <> pap_unhandled
		currentPapStage = pap_unhandled
	ENDIF
	
	INT iTemp
	INT iNumberOfPapsLeft
	
	IF NOT bResetPapsChaseStatus
		REPEAT COUNT_OF(pedPaps) iTemp
			IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
			AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
				CLEAR_PED_TASKS(pedPaps[iTemp].m_pap)
				pedPaps[iTemp].m_iFootChaseState = 2
			ENDIF
		ENDREPEAT
		bResetPapsChaseStatus = TRUE
	ENDIF
	
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_BLIP_EXIST(pedPaps[iTemp].m_blip)
			REMOVE_BLIP(pedPaps[iTemp].m_blip)
		ENDIF
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
			IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
				iNumberOfPapsLeft++
				IF NOT IS_PED_INJURED(pedCelebGirl)
					IF IS_PED_IN_ANY_VEHICLE(pedPaps[iTemp].m_pap)
						TASK_LEAVE_ANY_VEHICLE(pedPaps[iTemp].m_pap)
					ELSE
						PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(pedCelebGirl, iTemp)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedPaps[iTemp].m_pap, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK
						TASK_WANDER_STANDARD(pedPaps[iTemp].m_pap)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_OCCLUDED(pedPaps[iTemp].m_pap)
			AND VDIST2(GET_ENTITY_COORDS(pedPaps[iTemp].m_pap), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 20*20
			AND VDIST2(GET_ENTITY_COORDS(pedPaps[iTemp].m_pap), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 20*20
				DELETE_PED(pedPaps[iTemp].m_pap)
				IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_cam)
					DELETE_OBJECT(pedPaps[iTemp].m_cam)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(vehPaps) iTemp
		IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
			IF IS_ENTITY_OCCLUDED(vehPaps[iTemp].m_veh)
			AND VDIST2(GET_ENTITY_COORDS(vehPaps[iTemp].m_veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50*50
			AND IS_VEHICLE_EMPTY(vehPaps[iTemp].m_veh)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPaps[iTemp].m_veh)
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip)
			REMOVE_BLIP(vehPaps[iTemp].m_blip)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(pedCelebGirl)
		IF IS_ENTITY_OCCLUDED(pedCelebGirl)
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCurrentCelebCoords) > 50*50
			SET_PED_AS_NO_LONGER_NEEDED(pedCelebGirl)
			PRINTLN("\n\n\n\n mission cleanup 2")
			missionCleanup()
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_SPOOKED_OR_KILLED_A_PAP()
	OR HAS_PLAYER_SPOOKED_OR_KILLED_PED(pedCelebGirl,  TRUE, TRUE)
		SET_STAGE(pescape_CELEBFLEES)
	ENDIF
	
	REPEAT COUNT_OF(pedPaps) iTemp
		IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
			IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
				iNumberOfPapsLeft++
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("STROPPING OFF stage ", iStageProgress)
	
	SWITCH iStageProgress
	
		CASE 0
			
			IF DOES_BLIP_EXIST(blipCelebGirl)
				REMOVE_BLIP(blipCelebGirl)
			ENDIF
			IF DOES_BLIP_EXIST(blipCelebVeh)
				REMOVE_BLIP(blipCelebVeh)
			ENDIF
			IF NOT IS_PED_INJURED(pedCelebGirl)
				CLEAR_PED_TASKS(pedCelebGirl)
				SET_PED_MAX_MOVE_BLEND_RATIO(pedCelebGirl, PEDMOVEBLENDRATIO_SPRINT)
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF VDIST(vCurrentPlayerCoords, vCurrentCelebCoords) < 30
				IF CREATE_CONVERSATION(sSpeech, sBlockId, root, CONV_PRIORITY_AMBIENT_HIGH)
					REPEAT COUNT_OF(pedPaps) iTemp
						IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
							IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
								CLEAR_PED_TASKS(pedPaps[iTemp].m_pap)
							ENDIF
						ENDIF
					ENDREPEAT
					iStageProgress++
				ENDIF
			ELSE
				REPEAT COUNT_OF(pedPaps) iTemp
					IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
						IF NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
							CLEAR_PED_TASKS(pedPaps[iTemp].m_pap)
						ENDIF
					ENDIF
				ENDREPEAT
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
			AND IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
			AND NOT bPlayersCarUsed
			AND IS_VEHICLE_SEAT_FREE(vehCelebsCar)
				IF VDIST(vCurrentCelebCoords, vDropOffPos) < 100
					SEQUENCE_INDEX seqTemp
					OPEN_SEQUENCE_TASK(seqTemp)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL, vehCelebsCar)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehCelebsCar, vDropOffPos, 20, DRIVINGSTYLE_NORMAL, SERRANO, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 10, 10)
					TASK_VEHICLE_PARK(NULL, vehCelebsCar, vDropOffPos, 288.7089, PARK_TYPE_PERPENDICULAR_NOSE_IN, 50, FALSE)
					TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehCelebsCar, GET_ENTITY_COORDS(vehCelebsCar), MISSION_STOP, 10, DRIVINGMODE_PLOUGHTHROUGH, 2, 2)
					TASK_LEAVE_VEHICLE(NULL, vehCelebsCar)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffOnFootPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fDropOffOnFootHeading)
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
				ELSE
					SEQUENCE_INDEX seqTemp
					OPEN_SEQUENCE_TASK(seqTemp)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL, vehCelebsCar)
					TASK_VEHICLE_DRIVE_WANDER(NULL, vehCelebsCar, 15, DRIVINGMODE_AVOIDCARS_RECKLESS)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
				ENDIF
				iStageProgress = 1000
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(pedCelebGirl)
					TASK_LEAVE_ANY_VEHICLE(pedCelebGirl)
				ENDIF
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 2
			IF iNumberOfPapsLeft > 0
				TASK_SMART_FLEE_PED(pedCelebGirl, PLAYER_PED_ID(), 3000, -1, TRUE)
				iStageProgress = 1000
			ELSE
				IF VDIST(vCurrentCelebCoords, vDropOffPos) < 100
					SEQUENCE_INDEX seqTemp
					OPEN_SEQUENCE_TASK(seqTemp)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDropOffOnFootPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fDropOffOnFootHeading)
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedCelebGirl, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
				ELSE
					TASK_WANDER_STANDARD(pedCelebGirl)
				ENDIF
				iStageProgress = 1000
			ENDIF
		BREAK
		
		CASE 1000
			IF iNumberOfPapsLeft = 0
				PRINTLN("\n\n\n\n mission cleanup 2")
				missionCleanup()
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

BOOL bPGConv

PROC PLAYER_HAS_STOLEN_VEH()

	IF currentPapStage <> pap_unhandled
		currentPapStage = pap_unhandled
	ENDIF
	
	IF DOES_BLIP_EXIST(blipCelebGirl)
		REMOVE_BLIP(blipCelebGirl)
	ENDIF

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_PED_INJURED(pedCelebGirl)
				TASK_SMART_FLEE_PED(pedCelebGirl, PLAYER_PED_ID(), 300, -1)
			ENDIF
			
			INT iTemp
			
			REPEAT COUNT_OF(pedPaps) iTemp
				IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
					IF IS_ENTITY_OCCLUDED(pedPaps[iTemp].m_pap)
					AND VDIST2(GET_ENTITY_COORDS(pedPaps[iTemp].m_pap), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30*30
						IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_cam)
							DELETE_OBJECT(pedPaps[iTemp].m_cam)
						ENDIF
						DELETE_PED(pedPaps[iTemp].m_pap)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(pedPaps[iTemp].m_blip)
					REMOVE_BLIP(pedPaps[iTemp].m_blip)
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(vehPaps) iTemp
				IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
					IF IS_ENTITY_OCCLUDED(vehPaps[iTemp].m_veh)
					AND VDIST2(GET_ENTITY_COORDS(vehPaps[iTemp].m_veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30*30
						DELETE_VEHICLE(vehPaps[iTemp].m_veh)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip)
					REMOVE_BLIP(vehPaps[iTemp].m_blip)
				ENDIF
			ENDREPEAT
			
			SETTIMERA(0)
			
			PRINTLN("REMOVED PAPS TOO FAR AWAY FOR STOLEN VEHICLE SECTION")
			
			bPGConv = FALSE
			
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			INT iNumberOfPapsLeft
			iNumberOfPapsLeft = 0
			
			REPEAT COUNT_OF(pedPaps) iTemp
				IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
				AND NOT IS_PED_INJURED(pedPaps[iTemp].m_pap)
				AND NOT IS_ENTITY_DEAD(pedPaps[iTemp].m_pap)
					iNumberOfPapsLeft++
					PAP_CHASE_AND_PHOTOGRAPH_PED_ON_FOOT(PLAYER_PED_ID(), iTemp)
					IF IS_ENTITY_OCCLUDED(pedPaps[iTemp].m_pap)
					AND VDIST2(GET_ENTITY_COORDS(pedPaps[iTemp].m_pap), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50*50
						IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_cam)
							DELETE_OBJECT(pedPaps[iTemp].m_cam)
						ENDIF
						DELETE_PED(pedPaps[iTemp].m_pap)
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT bPGConv
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_CLOSEST_PAP())) < 100
					INT iRan
					iRan = GET_RANDOM_INT_IN_RANGE(0,3)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 7)
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 8)
						SWITCH iRan
							CASE 0
								ADD_PED_FOR_DIALOGUE(sSpeech, 6, GET_CLOSEST_PAP(), "PAP1ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[6].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[6].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PAP1", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 1
								ADD_PED_FOR_DIALOGUE(sSpeech, 7, GET_CLOSEST_PAP(), "PAP2ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[7].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[7].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PAP2", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 2
								ADD_PED_FOR_DIALOGUE(sSpeech, 8, GET_CLOSEST_PAP(), "PAP3ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[8].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[8].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_PAP3", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
						iRan = GET_RANDOM_INT_IN_RANGE(0,3)
						SWITCH iRan
							CASE 0
								ADD_PED_FOR_DIALOGUE(sSpeech, 6, GET_CLOSEST_PAP(), "PAP1ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE1T", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 1
								ADD_PED_FOR_DIALOGUE(sSpeech, 5, GET_CLOSEST_PAP(), "PAP2ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE2T", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 2
								ADD_PED_FOR_DIALOGUE(sSpeech, 5, GET_CLOSEST_PAP(), "PAP2ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE2T", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
						iRan = GET_RANDOM_INT_IN_RANGE(0,3)
						SWITCH iRan
							CASE 0
								ADD_PED_FOR_DIALOGUE(sSpeech, 6, GET_CLOSEST_PAP(), "PAP1ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE1M", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 1
								ADD_PED_FOR_DIALOGUE(sSpeech, 5, GET_CLOSEST_PAP(), "PAP2ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE2M", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 2
								ADD_PED_FOR_DIALOGUE(sSpeech, 5, GET_CLOSEST_PAP(), "PAP2ESCAPE")
								IF DOES_ENTITY_EXIST(sSpeech.PedInfo[iRan].Index)
								AND NOT IS_PED_INJURED(sSpeech.PedInfo[iRan].Index)
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_TAKE2M", CONV_PRIORITY_AMBIENT_HIGH)
										bPGConv = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("Remaing paps ", iNumberOfPapsLeft)
			
			IF HAS_PLAYER_SPOOKED_OR_KILLED_A_PAP()
				FLEE_ALL()
				missionCleanup()
			ENDIF
			
			IF iNumberOfPapsLeft = 0 
				PRINTLN("\n\n\n\n mission cleanup 1")
				missionCleanup()
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
ENDPROC


SCRIPT (coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTNL()
	PRINTSTRING("re_paparazzi")
	PRINTVECTOR(vInput)
	PRINTNL()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		PRINTLN(GET_THIS_SCRIPT_NAME(), "- forced cleanup")
		missionCleanup()
	ENDIF
	
	//Leave 3 frames between each load request for this script.
	SET_LOAD_QUEUE_LIGHT_FRAME_DELAY(sLoadQueue, 3) 
	
	IF NOT g_bMagDemoActive
	
		IF CAN_RANDOM_EVENT_LAUNCH(vInput)
			LAUNCH_RANDOM_EVENT()
		ELSE
			TERMINATE_THIS_THREAD()
		ENDIF
		
	ENDIF
		
	sbiGang = ADD_SCENARIO_BLOCKING_AREA(<<276.3459, 82.5239, 93.3633>> + <<10,10,10>>, <<276.3459, 82.5239, 93.3633>> - <<10,10,10>>)
	sbiHouse = ADD_SCENARIO_BLOCKING_AREA(<<227.8598, 675.8706, 188.5935>> + <<4,4,4>>, <<227.8598, 675.8706, 188.5935>> - <<4,4,4>>)
	sbiCar = ADD_SCENARIO_BLOCKING_AREA(<<246.1673, 92.1704, 93.8361>> + <<6,6,6>>, <<246.1673, 92.1704, 93.8361>> - <<6,6,6>>)
	
	IF g_bMagDemoActive
		CLEAR_AREA(<<246.1673, 92.1704, 93.8361>>, 6, TRUE)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<246.1673, 92.1704, 93.8361>> + <<6,6,6>>, <<246.1673, 92.1704, 93.8361>> - <<6,6,6>>, FALSE)
	
//	#IF IS_DEBUG_BUILD
//		BUILD_WIDGETS()
//	#ENDIF
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
	
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_LIGHT(sLoadQueue)
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				missionCleanup()
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_EP")
		
		INT iCountPapCoords
		REPEAT COUNT_OF(vehPaps) iCountPapCoords
			IF DOES_ENTITY_EXIST(vehPaps[iCountPapCoords].m_veh)
			AND NOT IS_ENTITY_DEAD(vehPaps[iCountPapCoords].m_veh)
				vehPaps[iCountPapCoords].m_vCoords = GET_ENTITY_COORDS(vehPaps[iCountPapCoords].m_veh)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(pedPaps) iCountPapCoords
			IF DOES_ENTITY_EXIST(pedPaps[iCountPapCoords].m_pap)
			AND NOT IS_ENTITY_DEAD(pedPaps[iCountPapCoords].m_pap)
				pedPaps[iCountPapCoords].m_vCoords = GET_ENTITY_COORDS(pedPaps[iCountPapCoords].m_pap)
			ENDIF
		ENDREPEAT
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			vCurrentPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		IF NOT IS_PED_INJURED(pedCelebGirl)
			vCurrentCelebCoords = GET_ENTITY_COORDS(pedCelebGirl)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCelebsCar)
		AND NOT IS_ENTITY_DEAD(vehCelebsCar)
			vCurrentCelebCarCoords = GET_ENTITY_COORDS(vehCelebsCar)
		ENDIF
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				
			IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
				SWITCH iStageProgress
					CASE 0
						IF HAVE_ALL_ASSETS_LOADED()
							createScene()
							iStageProgress++
						ELSE
							IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
								missionCleanup()
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF HAS_PLAYER_SCARED_A_PAP()
						OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						OR NOT IS_VEHICLE_DRIVEABLE(vehCelebsCar)
							IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
								FLEE_ALL()
								PRINTLN("pre-activation cleanup")
								missionCleanup()
							ENDIF
						ELSE
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<281.007416,192.480484,58.088165>>, <<228.829086,50.886669,111.610443>> , 82.750000)
								SET_RANDOM_EVENT_ACTIVE()
								blipCelebGirl = CREATE_BLIP_FOR_PED(pedCelebGirl)
								SET_AUDIO_SCENE_ACTIVE("MAG_2_ESCAPE_PAP_GET_CAR")
								SET_STAGE(pescape_PICKUPGIRL)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("Cleanup  from losing world point")
				missionCleanup()
			ENDIF
			
		ELSE
		
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			SWITCH thisStage
				CASE pescape_PICKUPGIRL
					PICKUP_GIRL()
				BREAK
				CASE pescape_LOSEPAPS
					LOSE_PAPS()
				BREAK
				CASE pescape_DRIVEBACK
					DRIVE_BACK()
				BREAK
				CASE pescape_DROPOFF
					DROP_OFF()
				BREAK
				CASE pescape_DROPOFFONFOOT
					DROP_OFF_ON_FOOT()
				BREAK
				CASE pescape_CLEAN_UP
					PRINTLN("\n\n\n\n In cleanup stage \n\n\n\n")
					missionCleanup()
				BREAK
				CASE pescape_CELEBFLEES
					CELEB_FLEES()
				BREAK
				CASE pescape_STROPSOFF
					CELEB_STROPS_OFF(sStropString)
				BREAK
				CASE pescape_STOLEVEH
					PLAYER_HAS_STOLEN_VEH()
				BREAK
				CASE pescape_TEST
				BREAK
				CASE pescape_AT_CULT
					IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
						IF DOES_ENTITY_EXIST(pedCelebGirl)
							DELETE_PED(pedCelebGirl)
						ENDIF
						missionPassed()
					ENDIF
				BREAK
				CASE pescape_LOSECOPS
					LOSE_THE_COPS()
				BREAK
			ENDSWITCH
			
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCurrentCelebCoords, <<210, 210, 210>>)
				FLEE_ALL()
				missionCleanup()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 sTemp = GET_STRING_FROM_MISSION_STAGE(thisStage)
				sTemp += " "
				sTemp += iStageProgress
				DRAW_DEBUG_TEXT_2D(sTemp, <<0.1, 0.3, 0>>)
			#ENDIF
			
			IF thisStage <> pescape_CELEBFLEES
			AND thisStage <> pescape_STROPSOFF
			AND thisStage <> pescape_CLEAN_UP
			AND thisStage <> pescape_STOLEVEH
			
				IF NOT g_bMagDemoActive AND NOT bDebugTestingMagdemo
				
					IF HAS_PLAYER_SPOOKED_OR_KILLED_PED(pedCelebGirl, TRUE, TRUE)
					OR HAS_PLAYER_SPOOKED_OR_KILLED_A_PAP()
						IF thisStage > pescape_PICKUPGIRL
							SET_STAGE(pescape_CELEBFLEES)
						ELSE
							PRINTLN("\n\n\n\n setting cleanup stage 3")
							FLEE_ALL()
							SET_STAGE(pescape_CLEAN_UP)
						ENDIF
					ENDIF
					
					IF thisStage <> pescape_CELEBFLEES
					AND thisStage <> pescape_CLEAN_UP
					AND thisStage <> pescape_PICKUPGIRL
					AND thisStage <> pescape_STOLEVEH
						IF HAS_PLAYER_PISSED_OFF_CELEB()
							SET_STAGE(pescape_STROPSOFF)
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
		RUN_PAPS()
		
		IF thisStage <= pescape_PICKUPGIRL
			PRINTLN(VDIST(vCurrentCelebCoords, vCurrentPlayerCoords))
			INT iClosestPap = GET_CLOSEST_PAP_INDEX()
			IF  iClosestPap <> -1
			AND (bMikeInsult OR GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL)
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<243.644272,123.719276,97.598976>>, <<262.435333,116.874611,107.476166>> , 32.250000)
				AND (GET_GAME_TIMER()-iLastPapSpeech) > 5000
				AND NOT IS_PED_INJURED(pedPaps[iClosestPap].m_pap)
					IF VDIST2(GET_ENTITY_COORDS(pedPaps[iClosestPap].m_pap), vCurrentPlayerCoords) < 30.5*30.5
					AND VDIST2(vCurrentCelebCoords, vCurrentPlayerCoords) > 22.5*22.5
						IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							IF sSpeech.PedInfo[4].Index <> pedPaps[iClosestPap].m_pap
								REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
								ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedPaps[iClosestPap].m_pap, "PAPARAZZO")
							ELSE
								IF NOT bLaceyLineDone
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CALL1", CONV_PRIORITY_AMBIENT_LOW)
										iLastPapSpeech = GET_GAME_TIMER()
										bLaceyLineDone = TRUE
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "REPAP_CHAT", CONV_PRIORITY_AMBIENT_LOW)
										iLastPapSpeech = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ELSE
							TEXT_LABEL_23 sTemp
							sTemp = "REPAP_AZAA_0"
							sTemp += GET_RANDOM_INT_IN_RANGE(1, 13)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedPaps[iClosestPap].m_pap, sTemp, "PAPARAZZO")
							iLastPapSpeech = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisStage <= pescape_LOSEPAPS
			UPDATE_DAMAGE_TO_PAPS()
		ENDIF
		
		HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
		
		#IF IS_DEBUG_BUILD
		
			IF DOES_ENTITY_EXIST(pedPaps[1].m_cam)
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedPaps[1].m_cam, <<0.019, -0.010, 0.046>>), 0.01, 0, 255, 0)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				FLEE_ALL()
				missionCleanup()
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				IF DOES_ENTITY_EXIST(pedPaps[1].m_cam)
					START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", pedPaps[1].m_cam, <<0.019, -0.010, 0.046>>, <<0,0,-180.0>>)
					PRINTLN("playing SHUTTER_FLASH")
					PLAY_SOUND_FROM_COORD(-1, "SHUTTER_FLASH", GET_ENTITY_COORDS(pedPaps[1].m_cam), "CAMERA_FLASH_SOUNDSET")
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			AND IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			
				BOOL bSkipped = FALSE
				INT iTemp
				
				SWITCH thisStage
				
					CASE pescape_PICKUPGIRL
						REPEAT COUNT_OF(vehPaps) iTemp
							IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip)
								REMOVE_BLIP(vehPaps[iTemp].m_blip)
							ENDIF
						ENDREPEAT
						IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							SET_RANDOM_EVENT_ACTIVE()
						ENDIF
						REMOVE_BLIP(blipInitialMarker)
						IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
						AND NOT IS_PED_INJURED(pedCelebGirl)
						AND NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
							PRINTSTRING("\n\n\n\nDoing j skip in pickup girl section\n\n\n\n\n")
							IF IS_PLAYER_ABLE_TO_PICK_UP_CELEB()
								PRINTSTRING("\n\n\n\n\nj skip is setting player's vehicle as the celeb car index\n\n\n\n\n\n")
								vehCelebsCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								SET_ENTITY_AS_MISSION_ENTITY(vehCelebsCar, TRUE, TRUE)
								SET_VEHICLE_DOORS_LOCKED(vehCelebsCar, VEHICLELOCK_UNLOCKED)
								SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehCelebsCar, FALSE)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
								PRINTSTRING("\n\n\n\nPlayer is not in celebs car, setting into the car now\n\n\n\n")
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
							ENDIF
							SET_ENTITY_COORDS(vehCelebsCar, <<256.1716, 119.9548, 100.3651>>)
							SET_ENTITY_HEADING(vehCelebsCar, 337.7691)
							SET_PED_INTO_VEHICLE(pedCelebGirl, vehCelebsCar, VS_FRONT_RIGHT)
						ENDIF
						bSkipped = TRUE
						SET_STAGE(pescape_LOSEPAPS)
					BREAK
					
					CASE pescape_LOSEPAPS
						REPEAT COUNT_OF(pedPaps) iTemp
							IF DOES_ENTITY_EXIST(pedPaps[iTemp].m_pap)
								DELETE_PED(pedPaps[iTemp].m_pap)
							ENDIF
						ENDREPEAT
						REPEAT COUNT_OF(vehPaps) iTemp
							IF DOES_ENTITY_EXIST(vehPaps[iTemp].m_veh)
								DELETE_VEHICLE(vehPaps[iTemp].m_veh)
							ENDIF
						ENDREPEAT 
						REPEAT COUNT_OF(vehPaps) iTemp
							IF DOES_BLIP_EXIST(vehPaps[iTemp].m_blip)
								REMOVE_BLIP(vehPaps[iTemp].m_blip)
							ENDIF
						ENDREPEAT
						IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
							SET_ENTITY_COORDS(vehCelebsCar, <<243.4117, 464.1584, 122.9530>>)
							SET_ENTITY_HEADING(vehCelebsCar, 1.6120)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
								SET_PED_INTO_VEHICLE(pedCelebGirl, vehCelebsCar, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						bSkipped = TRUE
						SET_STAGE(pescape_DRIVEBACK)
					BREAK
					
					CASE pescape_DRIVEBACK
						IF IS_VEHICLE_DRIVEABLE(vehCelebsCar)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCelebsCar)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(pedCelebGirl, vehCelebsCar)
								SET_PED_INTO_VEHICLE(pedCelebGirl, vehCelebsCar, VS_FRONT_RIGHT)
							ENDIF
							IF VDIST(vCurrentPlayerCoords, vDropOffPos) > 100
								SET_ENTITY_COORDS(vehCelebsCar, <<223.3045, 698.0457, 188.2691>>)
								SET_ENTITY_HEADING(vehCelebsCar, 143.8257)
							ELSE
								SET_ENTITY_COORDS(vehCelebsCar, <<227.2411, 680.8247, 188.5121>>)
								SET_ENTITY_HEADING(vehCelebsCar, 299.1867)
							ENDIF
						ENDIF
						bSkipped = TRUE
					BREAK
					
				ENDSWITCH
				
				IF bSkipped
					CLEAR_PRINTS()
					CLEAR_HELP()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
								
			ENDIF
						
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
