//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "script_blips.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "AggroSupport.sch"
USING "script_MATHS.sch"
USING "dialogue_public.sch" 
USING "Ambient_Common.sch"
USING "emergency_call.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "random_events_public.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambWaitForCops,
	ambWaitForAmbulance
ENDENUM
ambStageFlag ambStage = ambCanRun

VECTOR vInput
FLOAT fInput

PED_INDEX pedAttacker, pedVictim, pedBystander, pedMedic
VEHICLE_INDEX vehCopCar
BLIP_INDEX blipAttacker, blipScene
REL_GROUP_HASH relPlayer, relBadGuy, relManager
EAggro eaReturned
INT lockonTimeLimit = 8000
INT iShockingEvent = 0
SCENARIO_BLOCKING_INDEX scenarioBlock
INT iVictimAmbientAudio
INT sceneId, scenePassID = -1, sceneBystander
VECTOR scenePosition
VECTOR sceneRotation
BOOL bHostageShouldBeExecuted, bVictimThanksPlayer, bBystanderThanksPlayer
SEQUENCE_INDEX seqBystander
BOOL bGunDownPrompt, bHostageHasFallenToKnees, bBulletFired, bAttackThePlayer, bAmbulanceFound, bPlayerThanked, bDispatchSuccessful, bPlayBystanderComfort=TRUE
INCIDENT_INDEX tempIncident, girlIncident
STRING playerVoice, playerDialogue, randomDialogue = "NULL"
FLOAT fPerceptionMin = -120
FLOAT fPerceptionMax = 120
FLOAT fPerceptionPeripheralRange = 40
FLOAT fPerceptionCentreAngle = 90
BOOL bAttackerSpokeLast

BOOL bSetBystanderRunOff  = TRUE


INT iTimeUpTimer
BOOL bFailedThroughTimeOut

structPedsForConversation dialogueStruct
enumSubtitlesState displaySubs = DISPLAY_SUBTITLES

STRING animDict = "RANDOM@RESCUE_HOSTAGE"
//INT taskCounter

#if IS_DEBUG_BUILD
	BOOL bMoveScene, bResetPerception
#ENDIF

// Functions ----------------------------------------------//

FUNC BOOL IS_PLAYER_AIMING_FLASHLIGHT_AT_PED( PED_INDEX ped )

		WEAPON_TYPE currentWeapon
		BOOL		bFlashlightInUse = FALSE
		
		IF NOT DOES_ENTITY_EXIST(ped)
			RETURN FALSE
		ENDIF
		
		IF IS_PED_INJURED(ped)
			RETURN FALSE
		ENDIF
		
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
		IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), currentWeapon, WEAPONCOMPONENT_AT_AR_FLSH)
			IF IS_PED_WEAPON_COMPONENT_ACTIVE(PLAYER_PED_ID(), currentWeapon, WEAPONCOMPONENT_AT_AR_FLSH)
				bFlashlightInUse = TRUE
			ENDIF		
		ELIF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), currentWeapon, WEAPONCOMPONENT_AT_PI_FLSH)
			IF IS_PED_WEAPON_COMPONENT_ACTIVE(PLAYER_PED_ID(), currentWeapon, WEAPONCOMPONENT_AT_PI_FLSH)
				bFlashlightInUse = TRUE
			ENDIF
		ENDIF
		
		
		IF bFlashlightInUse
			
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY( PLAYER_ID(), ped )
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped)
				IF VDIST(GET_ENTITY_COORDS(ped), GET_PLAYER_COORDS(PLAYER_ID()))  < 8.0
					RETURN TRUE
				ENDIF
			ENDIF
		
		ENDIF



	RETURN FALSE

ENDFUNC


PROC RUN_BYSTANDER_PANICKED_BY_EXCESSIVE_FORCE()

	IF bSetBystanderRunOff
	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
		TASK_SMART_FLEE_COORD(pedBystander, vInput, 50, -1)
		SET_PED_KEEP_TASK(pedBystander, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedBystander)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
	ENDIF
	
	

ENDPROC

FUNC BOOL LOADED_AND_CREATED_HOSTAGE_SCENE()
	vInput = <<-96.400, 6398.201, 30.4542>>
	fInput = 0
	MODEL_NAMES modelAttacker = A_M_M_Hillbilly_02//_WHITE_MINI_02 //A_M_O_GENWHITE01
	MODEL_NAMES modelManager = A_M_M_BUSINESS_01 //A_F_O_GENWHITE01
	MODEL_NAMES modelVictim = A_F_M_TOURIST_01
	STRING victimAmbientVoice = "A_F_M_BEVHILLS_02_WHITE_FULL_02"
	
	REQUEST_MODEL(modelAttacker)
	REQUEST_MODEL(modelVictim)
	REQUEST_MODEL(modelManager)
	REQUEST_ANIM_DICT(animDict)
	IF HAS_MODEL_LOADED(modelAttacker)
	AND HAS_MODEL_LOADED(modelVictim)
	AND HAS_MODEL_LOADED(modelManager)
	AND HAS_ANIM_DICT_LOADED(animDict)
	
		CLEAR_AREA(vInput, 5, TRUE, TRUE)
		
		pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, vInput, fInput)
		SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
		SET_AMBIENT_VOICE_NAME(pedVictim, victimAmbientVoice)
		SET_PED_CONFIG_FLAG(pedVictim, PCF_AllowMedicsToAttend, TRUE)
		
		pedAttacker = CREATE_PED(PEDTYPE_CRIMINAL, modelAttacker, vInput, fInput)
		SET_ENTITY_HEALTH(pedAttacker, 125)
		SET_PED_MAX_HEALTH(pedAttacker, 125)
		SET_PED_FLEE_ATTRIBUTES(pedAttacker, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_SUFFERS_CRITICAL_HITS(pedAttacker, TRUE)
		SET_PED_CONFIG_FLAG(pedAttacker, PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_HEARING_RANGE(pedAttacker, 1.5)
		STOP_PED_SPEAKING(pedAttacker, TRUE)
		SET_PED_SEEING_RANGE(pedAttacker, 40)
		SET_PED_CONFIG_FLAG(pedAttacker, PCF_AllowMedicsToAttend, TRUE)
		
		SET_PED_VISUAL_FIELD_MIN_ANGLE(pedAttacker, fPerceptionMin)
		SET_PED_VISUAL_FIELD_MAX_ANGLE(pedAttacker, fPerceptionMax)
		SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedAttacker, fPerceptionPeripheralRange)
		SET_PED_VISUAL_FIELD_CENTER_ANGLE(pedAttacker, fPerceptionCentreAngle)
		
		pedBystander = CREATE_PED(PEDTYPE_CIVMALE, modelManager, <<-98.113, 6405.354, 30.6005>>, fInput)
		ADD_COVER_BLOCKING_AREA(<<-98.413887,6405.057617,31.640238>>-<<5,5,5>>, <<-98.413887,6405.057617,31.640238>>+<<5,5,5>>, FALSE, FALSE, TRUE)
		scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(<<-98.413887,6405.057617,31.640238>>-<<40,40,40>>, <<-98.413887,6405.057617,31.640238>>+<<40,40,40>>)
		SET_PED_CAN_BE_TARGETTED(pedBystander, FALSE)
		STOP_PED_SPEAKING(pedBystander, TRUE)
		
		vInput.z += 1
		scenePosition = vInput
		sceneRotation = <<0,0,fInput>>
		
		sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
		
		SET_SYNCHRONIZED_SCENE_LOOPED (sceneID, TRUE)
		
		// Girl is killed
		//"girl_girl_shot"
		//"villain_girl_shot"

		//"bystander_bystander_angry"

		// Villain is shot
		//"bystander_villian_shot"
		//"girl_villian_shot"
		// THEN
		//"bystander_helping_girl_loop"
		//"girl_helping_girl_loop"

		// Waiting loop
		//"bystander_please_help"
		//"bystander_please_help_loop"
		//"girl_struggle_loop"
		//"villian_struggle_loop"
//		TASK_PLAY_ANIM(pedAttacker, animDict, "villian_struggle_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//		TASK_PLAY_ANIM(pedVictim, animDict, "girl_struggle_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		
		TASK_SYNCHRONIZED_SCENE (pedAttacker, sceneId, animDict, "villian_struggle_loop", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		TASK_SYNCHRONIZED_SCENE (pedVictim, sceneId, animDict, "girl_struggle_loop", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		
		sceneBystander = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
		SET_SYNCHRONIZED_SCENE_LOOPED (sceneBystander, TRUE)
		TASK_SYNCHRONIZED_SCENE (pedBystander, sceneBystander, animDict, "bystander_taking_cover", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		SET_PED_CONFIG_FLAG(pedBystander, PCF_UseKinematicModeWhenStationary, TRUE)
		/*
		Animation approved and exported. The bystander "please_help" loop has been left alone. I have added "take_cover" and "take_cover_shout"

		"Take_cover" is a toned down version of the previous "please_help" with less of a frantic attitude.

		"Take_cover_shout" is a loop where dialogue can be used.
		*/
		
		//SET_PED_CAN_RAGDOLL(pedBystander, FALSE)
		GIVE_WEAPON_TO_PED(pedAttacker, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
		SET_PED_AS_ENEMY(pedAttacker, TRUE)
		
		//ADD_RELATIONSHIP_GROUP("re_rescuehostage relPlayer", relPlayer)
		ADD_RELATIONSHIP_GROUP("re_rescuehostage relManager", relManager)
		ADD_RELATIONSHIP_GROUP("re_rescuehostage relBadGuy", relBadGuy)
		//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relPlayer)
		relPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relBadGuy, relPlayer)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPlayer, relBadGuy)
		//SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, relPlayer)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedBystander, relManager)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedAttacker, relBadGuy)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAttacker, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(pedBystander, CA_JUST_SEEK_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBystander, CA_ALWAYS_FLEE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBystander, CA_AGGRESSIVE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedBystander, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(pedBystander, FALSE )
		
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_JUST_SEEK_COVER, TRUE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(pedVictim, FALSE )
		//SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_AGGRESSIVE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FIGHT, FALSE)
		
		// Dialogue
		ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), playerVoice)
		ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedAttacker, "RHCriminal")
		ADD_PED_FOR_DIALOGUE(dialogueStruct, 2, pedVictim, "RHHostage")
		ADD_PED_FOR_DIALOGUE(dialogueStruct, 3, pedBystander, "RHBystander")
		
		SET_CREATE_RANDOM_COPS(FALSE)
		SET_WANTED_LEVEL_MULTIPLIER(0)
		
		FORCE_EMERGENCY_CALL_PRIORITY_HIGH()
		CLEAR_AREA_OF_OBJECTS(<<-89,6392,32>>, 3, CLEAROBJ_FLAG_FORCE)
		
		SETTIMERA(8000)

		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CHECK_SUBTITLE_STATUS()
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, <<50,50,25>>)
		displaySubs = DISPLAY_SUBTITLES
	ELSE
		displaySubs = DO_NOT_DISPLAY_SUBTITLES
	ENDIF
//	IF NOT IS_PED_INJURED(pedVictim)
//		IF GET_GAME_TIMER() > iVictimAmbientAudio+8000
//			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE")
//			iVictimAmbientAudio = GET_GAME_TIMER()
//		ENDIF
//	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_SCENE()
	BOOL bReturn
	VECTOR vTempMax = <<15,15,10>>, vTempMin = <<-15,-15,-10>>
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 15)
			bReturn = TRUE
		ENDIF

		vTempMax = vTempMax+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
		vTempMin = vTempMin+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
		IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
//		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_AMMO_TANK, TRUE)
//		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD, TRUE)
//		OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_VEHICLE_WEAPON_TANK, TRUE)
		      bSetBystanderRunOff = TRUE
			  bReturn = TRUE
		ENDIF
		
		IF IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_DONTCARE, <<-90.686539,6411.882813,36.007286>>, <<-103.859955,6398.847656,30.194529>>, 23.000000)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 20)
			bSetBystanderRunOff = TRUE
			bReturn = TRUE
		ENDIF

		IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 6)
		      bReturn = TRUE
			  bSetBystanderRunOff = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedBystander)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBystander, PLAYER_PED_ID())	
			bReturn = TRUE
		ENDIF
	ELSE
		bReturn = TRUE
	ENDIF
	
	IF bReturn
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_INTERFERING_WITH_SCENE()\n")
	ENDIF
	RETURN bReturn
ENDFUNC

PROC BYSTANDER_REACTS_TO_PLAYER()
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> PROC BYSTANDER_REACTS_TO_PLAYER()")
	
	IF IS_PLAYER_INTERFERING_WITH_SCENE()
				
		bSetBystanderRunOff = TRUE
		
	ENDIF
	
	
	IF bSetBystanderRunOff = FALSE
	
			IF NOT IS_PED_INJURED(pedBystander)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF bGunDownPrompt // Means player has approached bad guy
					OPEN_SEQUENCE_TASK(seqBystander)
						TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 5)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						TASK_PLAY_ANIM(NULL, animDict, "bystander_bystander_angry", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 1500)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1)
					CLOSE_SEQUENCE_TASK(seqBystander)
					TASK_PERFORM_SEQUENCE(pedBystander, seqBystander)
					CLEAR_SEQUENCE_TASK(seqBystander)
					SET_PED_KEEP_TASK(pedBystander, TRUE)
				ELSE
					OPEN_SEQUENCE_TASK(seqBystander)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vInput, PEDMOVEBLENDRATIO_WALK)
						TASK_LOOK_AT_COORD(NULL, vInput, -1, SLF_WHILE_NOT_IN_FOV)
						TASK_PLAY_ANIM(NULL, animDict, "bystander_bystander_angry", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 1500)
					CLOSE_SEQUENCE_TASK(seqBystander)
					TASK_PERFORM_SEQUENCE(pedBystander, seqBystander)
					CLEAR_SEQUENCE_TASK(seqBystander)
					SET_PED_KEEP_TASK(pedBystander, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
ENDPROC

PROC ATTACK_THE_PLAYER()
	IF NOT bAttackThePlayer
		IF NOT IS_PED_INJURED(pedAttacker)
			CLEAR_PED_TASKS(pedAttacker)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relBadGuy, relPlayer)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPlayer, relBadGuy)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relManager, relPlayer)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relManager, relBadGuy)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relBadGuy, relManager)
			//TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedAttacker, 100)
			IF IS_ENTITY_AT_ENTITY(pedAttacker, PLAYER_PED_ID(), <<50,50,50>>)
				TASK_COMBAT_PED(pedAttacker, PLAYER_PED_ID())
				SET_PED_TARGET_LOSS_RESPONSE(pedAttacker, TLR_NEVER_LOSE_TARGET) 
			ELSE
				TASK_SMART_FLEE_PED(pedAttacker, PLAYER_PED_ID(), 200, -1)
			ENDIF
			SET_PED_KEEP_TASK(pedAttacker, TRUE)
			CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_DIE", CONV_PRIORITY_AMBIENT_HIGH)
			bAttackThePlayer = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_DRIVING_TOWARDS_HOSTAGE()
	BOOL bReturn = FALSE, bFlag = FALSE
	VECTOR vTemp, vMin, vMax
	FLOAT fTemp, fDistance
	VEHICLE_INDEX tempVehicle, tempTrailer
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), vMin, vMax)
		fDistance = vMax.y
		IF IS_VEHICLE_ATTACHED_TO_TRAILER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF GET_VEHICLE_TRAILER_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), tempTrailer)
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempTrailer), vMin, vMax)
				fDistance += vMax.y
				fDistance += 3
			ENDIF
		ENDIF
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 15
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAttacker, <<fDistance+17,fDistance+17,6>>)
				bFlag = TRUE	
			ENDIF
		ELIF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 10
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAttacker, <<fDistance+12,fDistance+12,6>>)
				bFlag = TRUE	
			ENDIF	
		ELIF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 5
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAttacker, <<fDistance+8,fDistance+8,6>>)
				bFlag = TRUE	
			ENDIF	
		ENDIF
	ENDIF
	IF bFlag
		vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vInput
		fTemp = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
		IF IS_VEHICLE_HEADING_ACCEPTABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fTemp, 15)
			bReturn = TRUE
		ENDIF
		vTemp = vInput - GET_ENTITY_COORDS(PLAYER_PED_ID())
		fTemp = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
		IF IS_VEHICLE_HEADING_ACCEPTABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fTemp, 15)
			bReturn = TRUE
		ENDIF
	ENDIF
	tempVehicle = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(tempVehicle)
		IF IS_ENTITY_IN_ANGLED_AREA(tempVehicle, <<-91.563408,6391.747070,30.639702>>, <<-101.261101,6401.699707,32.454496>>, 7.500000)
			bReturn = TRUE
		ENDIF
	ENDIF
	IF bReturn
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_DRIVING_TOWARDS_HOSTAGE()\n")
	ENDIF
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PLAYER_FUCKING_ABOUT_IN_A_DOZER_OR_DIGGER()
	BOOL bReturn = FALSE
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = BULLDOZER
			IF NOT IS_PED_INJURED(pedBystander)
				IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedBystander)
					bReturn = TRUE
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedVictim)
				IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedVictim)
					bReturn = TRUE
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedAttacker)
				IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedAttacker)
					bReturn = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bReturn
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> IS_PLAYER_FUCKING_ABOUT_IN_A_DOZER_OR_DIGGER()\n")
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL HAS_PLAYER_BEEN_CAUGHT_CREEPING()
	IF IS_ENTITY_AT_ENTITY(pedAttacker, PLAYER_PED_ID(), <<2,2,1.5>>)
		IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedAttacker)
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> - HAS_PLAYER_BEEN_CAUGHT_CREEPING")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BACKUP_NEAR_CRIMINAL()
	FLOAT checkRange = 75
	VEHICLE_INDEX tempVehicleArray[32]
	
	INT i, iTemp
	
	PED_INDEX tempCop = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(pedAttacker, FALSE), <<checkRange,checkRange,checkRange>>) 
	IF NOT IS_PED_INJURED(tempCop)
		IF GET_ENTITY_MODEL(tempCop) = S_F_Y_COP_01
		OR GET_ENTITY_MODEL(tempCop) = S_M_Y_COP_01
			RETURN TRUE
		ENDIF
	ENDIF

	VEHICLE_INDEX tempCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(pedAttacker, FALSE), checkRange, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
	IF IS_VEHICLE_DRIVEABLE(tempCopCar)
		RETURN TRUE
	ENDIF
	
	//GET_PED_NEARBY_VEHICLES()
	
	iTemp = GET_PED_NEARBY_VEHICLES(pedAttacker, tempVehicleArray)
	
	IF iTemp > 0
		REPEAT iTemp i 
			IF IS_VEHICLE_DRIVEABLE(tempVehicleArray[i])
				IF GET_ENTITY_MODEL(tempVehicleArray[i]) = POLICE
				OR GET_ENTITY_MODEL(tempVehicleArray[i]) = PRANGER
				OR GET_ENTITY_MODEL(tempVehicleArray[i]) = SHERIFF
				OR GET_ENTITY_MODEL(tempVehicleArray[i]) = SHERIFF2
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_TIMER_EXPIRED()
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF GET_GAME_TIMER() > iTimeUpTimer+60000
			bFailedThroughTimeOut = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > iTimeUpTimer+120000
			bFailedThroughTimeOut = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RUN_ATTACKER_INTERACTION()
//CAN_PED_SEE_HATED_PED()
	IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-91.307045,6391.853516,30.640295>>, <<-102.559540,6403.102539,32.454437>>, 8.500000) AND (CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID())) )
	OR (HAS_PLAYER_AGGROED_PED(pedAttacker, eaReturned, lockonTimeLimit) AND (CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID())) )
	OR (IS_PHONE_ONSCREEN(TRUE) AND CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID()))
	OR IS_PLAYER_DRIVING_TOWARDS_HOSTAGE()
	OR IS_PLAYER_FUCKING_ABOUT_IN_A_DOZER_OR_DIGGER()
	OR IS_PLAYER_INTERFERING_WITH_SCENE()
	OR HAS_PLAYER_BEEN_CAUGHT_CREEPING()
	OR IS_PED_RAGDOLL(pedAttacker)
	OR IS_BACKUP_NEAR_CRIMINAL()
	OR HAS_TIMER_EXPIRED()
	OR IS_PLAYER_AIMING_FLASHLIGHT_AT_PED(pedAttacker)
	
		//IF NOT (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-96.751938,6394.994141,30>>, <<-90.506485,6388.886230,32>>, 4.500000) AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())) // Behind the hostage taker
			IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				//CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> PLAYER NOT PERFORMING STEALTH KILL")
				
				IF NOT HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedAttacker, WEAPONTYPE_STUNGUN)
					//CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> PLAYER NOT DAMAGED BY STUNGUN")
					IF NOT IS_PED_INJURED(pedVictim)
					//CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> VICTIM IS NOT INJURED")
						IF NOT IS_PED_RAGDOLL(pedAttacker)
							//CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> ATTACKER IS NOT RAGDOLLING")
							sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
							SET_SYNCHRONIZED_SCENE_LOOPED (sceneID, FALSE)
							TASK_SYNCHRONIZED_SCENE (pedVictim, sceneId, animDict, "girl_girl_shot", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
							TASK_SYNCHRONIZED_SCENE (pedAttacker, sceneId, animDict, "villian_girl_shot", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
							SET_PED_CAN_RAGDOLL(pedVictim, FALSE)
						ENDIF
					ENDIF
					// Girl is killed
					//"girl_girl_shot"
					//"villain_girl_shot"

					//"bystander_bystander_angry"
					IF NOT IS_PED_INJURED(pedBystander)
						SET_PED_CAN_RAGDOLL(pedBystander, TRUE)
					ENDIF
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()		
					bHostageShouldBeExecuted = TRUE
				ELSE
					SET_ENTITY_HEALTH(pedAttacker, 50)	
				ENDIF
			ENDIF
		//ENDIF
	ELSE
		IF TIMERA() > 6000
		
			IF (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedAttacker) AND CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID()))
			OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedAttacker) AND CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID()))
				
				IF DOES_BLIP_EXIST(blipAttacker)
					SHOW_HEIGHT_ON_BLIP(blipAttacker, TRUE)
				ENDIF
				
				IF NOT bGunDownPrompt
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-91.307045,6391.853516,30.640295>>, <<-102.559540,6403.102539,32.454437>>, 8.500000) // Behind the hostage taker
						//PRINT_STRING_WITH_LITERAL_STRING("STRING", "Put the gun down!", 5000, 1)
						CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> - Put the gun down")
						IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", playerDialogue, CONV_PRIORITY_AMBIENT_HIGH)
							lockonTimeLimit = 24000
						ENDIF
						SETTIMERA(0)
						bGunDownPrompt = TRUE
					ENDIF
				ENDIF
			ELSE
				
				IF ARE_STRINGS_EQUAL(randomDialogue, "NULL")
					SWITCH GET_RANDOM_INT_IN_RANGE(0,10)
						CASE 6 FALLTHRU
						CASE 5 FALLTHRU
						CASE 4 FALLTHRU
						CASE 3 FALLTHRU
						CASE 0 FALLTHRU
						CASE 9 FALLTHRU
						CASE 8 FALLTHRU
						CASE 7 FALLTHRU
						CASE 1
							IF bAttackerSpokeLast
								randomDialogue = "RERHO_HELP"
								bAttackerSpokeLast = FALSE
							ELSE
								randomDialogue = "RERHO_RANT"
								bAttackerSpokeLast = TRUE
							ENDIF
						BREAK
						CASE 2
							randomDialogue = "RERHO_SCREAM"
						BREAK
					ENDSWITCH
				ELSE
					IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", randomDialogue, CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
						SETTIMERA(0)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			randomDialogue = "NULL"
		ENDIF
	ENDIF
ENDPROC

PROC RUN_ATTRACT_AND_SWITCH_BLIPS()
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-62.657101,6447.079590,40.499279>>, <<-153.641052,6354.579102,23.990635>>, 144.250000)
	OR IS_SPHERE_VISIBLE(vInput, 15)
	OR bHostageShouldBeExecuted
	OR IS_PLAYER_INTERFERING_WITH_SCENE()
		blipAttacker = CREATE_AMBIENT_BLIP_FOR_PED(pedAttacker, TRUE)
		IF DOES_BLIP_EXIST(blipAttacker)
			SHOW_HEIGHT_ON_BLIP(blipAttacker, FALSE)
		ENDIF
		SET_RANDOM_EVENT_ACTIVE()
		iTimeUpTimer = GET_GAME_TIMER()
		SET_CREATE_RANDOM_COPS(FALSE)
	ELSE
		RUN_ATTACKER_INTERACTION()
	ENDIF
ENDPROC

PROC EXECUTE_THE_HOSTAGE()

	IF DOES_BLIP_EXIST(blipAttacker)
		SHOW_HEIGHT_ON_BLIP(blipAttacker,TRUE)
	ENDIF

	IF NOT IS_PED_INJURED(pedAttacker)
		SET_PED_RESET_FLAG(pedAttacker, PRF_InstantBlendToAim, TRUE)
	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.1
				IF NOT bBulletFired
					IF NOT IS_PED_INJURED(pedVictim)
					
						SET_PED_SHOOTS_AT_COORD(pedAttacker, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,-0.1>>))
						/*SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedAttacker, BONETAG_PH_R_HAND, <<0,0.2,0>>), 
								GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 1, FALSE, WEAPONTYPE_PISTOL, pedAttacker)*/
					ENDIF
					bBulletFired = TRUE
					bGunDownPrompt = TRUE
					ATTACK_THE_PLAYER()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedVictim)
				SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
				SET_ENTITY_HEALTH(pedVictim, 0)
			ENDIF
		ENDIF
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.6
				ATTACK_THE_PLAYER()
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedVictim)
				SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
				SET_ENTITY_HEALTH(pedVictim, 0)
			ENDIF
		ENDIF
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) = 1
				IF NOT IS_PED_INJURED(pedVictim)
					SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
					SET_ENTITY_HEALTH(pedVictim, 0)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedVictim)
				SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
				SET_ENTITY_HEALTH(pedVictim, 0)
			ENDIF
			ATTACK_THE_PLAYER()
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_PLAYER_DAMAGING_HOSTAGE()

	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim ,PLAYER_PED_ID())
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
		SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
		SET_ENTITY_HEALTH(pedVictim, 0)
		bHostageShouldBeExecuted = TRUE
		bGunDownPrompt = TRUE
		ATTACK_THE_PLAYER()
	ELSE
		IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
			//PRINTLN(GET_THIS_SCRIPT_NAME(), " - SCREAM_PANIC")
			//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "SCREAM_PANIC_SHORT", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_FRONTEND)	
			PLAY_PAIN(pedVictim, AUD_DAMAGE_REASON_SCREAM_PANIC_SHORT)
		ENDIF
	ENDIF
ENDPROC


PROC CHECK_FOR_PLAYER_DAMAGING_SURVIVORS()
	
	IF IS_PLAYER_INTERFERING_WITH_SCENE()

		IF NOT IS_PED_INJURED(pedVictim)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)	
			TASK_SMART_FLEE_COORD(pedVictim, vInput, 50, -1)
			SET_PED_KEEP_TASK(pedVictim, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
				
				// Bystander not hurt, flees
		IF NOT IS_PED_INJURED(pedBystander)
			//SET_PED_TO_RAGDOLL(pedBystander, 100, 1000, TASK_RELAX)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
			TASK_SMART_FLEE_COORD(pedBystander, vInput, 50, -1)
			SET_PED_KEEP_TASK(pedBystander, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(pedBystander)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		
	
	ENDIF


	IF NOT IS_PED_INJURED(pedVictim)
	
		// Player attacks victim
	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim ,PLAYER_PED_ID())
			IF IS_PED_RAGDOLL(pedVictim)
				SET_ENTITY_HEALTH(pedVictim, 0)
			ENDIF
		ELSE
		
		// Victim not hurt, flees.
		
			IF IS_PED_RAGDOLL(pedVictim) 
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)	
				TASK_SMART_FLEE_COORD(pedVictim, vInput, 50, -1)
				SET_PED_KEEP_TASK(pedVictim, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				// Bystander not hurt, flees
				IF NOT IS_PED_INJURED(pedBystander)
					//SET_PED_TO_RAGDOLL(pedBystander, 100, 1000, TASK_RELAX)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
					TASK_SMART_FLEE_COORD(pedBystander, vInput, 50, -1)
					SET_PED_KEEP_TASK(pedBystander, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedBystander)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
			
		ENDIF
		
		// Victim is injured
		
	ELSE
		
		// Victim hurt, bystander flees if not injured
	
		IF NOT IS_PED_INJURED(pedBystander)
			//SET_PED_TO_RAGDOLL(pedBystander, 100, 1000, TASK_RELAX)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
			TASK_SMART_FLEE_COORD(pedBystander, vInput, 50, -1)
			SET_PED_KEEP_TASK(pedBystander, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(pedBystander)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		
	ENDIF
	
	
	// Bystander is not hurt
	
	IF NOT IS_PED_INJURED(pedBystander)
	
		// Bystander hurt by player
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBystander ,PLAYER_PED_ID())
			IF IS_PED_RAGDOLL(pedBystander)
				SET_ENTITY_HEALTH(pedBystander, 0)
			ENDIF
		ELSE
		
			// Bystander not hurt - flees.
		
			IF IS_PED_RAGDOLL(pedBystander)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)	
				TASK_SMART_FLEE_COORD(pedBystander, vInput, 50, -1)
				SET_PED_KEEP_TASK(pedBystander, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(pedBystander)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				// Victim isn't hurt - flees
				
				IF NOT IS_PED_INJURED(pedVictim)
					//SET_PED_TO_RAGDOLL(pedVictim, 100, 1000, TASK_RELAX)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
					TASK_SMART_FLEE_COORD(pedVictim, vInput, 50, -1)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		// Bystander is hurt
	
		IF NOT IS_PED_INJURED(pedVictim)
		
			// Victim not hurt - flees
			
			//SET_PED_TO_RAGDOLL(pedVictim, 100, 1000, TASK_RELAX)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
			TASK_SMART_FLEE_COORD(pedVictim, vInput, 50, -1)
			SET_PED_KEEP_TASK(pedVictim, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		ENDIF
	ENDIF
ENDPROC

// Clean Up
PROC missionCleanup()

	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_CREATE_RANDOM_COPS(TRUE)
	ENDIF
	
	IF ambStage <> ambCanRun
		REMOVE_RELATIONSHIP_GROUP(relBadGuy)
		REMOVE_RELATIONSHIP_GROUP(relManager)
		SET_AUDIO_SCRIPT_CLEANUP_TIME(20000)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlock)
		REMOVE_SHOCKING_EVENT(iShockingEvent)
		FORCE_EMERGENCY_CALL_PRIORITY_HIGH(FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(pedAttacker)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		TASK_SMART_FLEE_PED(pedAttacker, PLAYER_PED_ID(), 100, -1)
		SET_PED_KEEP_TASK(pedAttacker, TRUE)
		WAIT(0)
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC missionPassed()
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC RUN_BYSTANDER_REACTION_TO_DEAD_HOSTAGE()
	IF dialogueStruct.PedInfo[2].ActiveInConversation
		REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
	ENDIF
	
	BYSTANDER_REACTS_TO_PLAYER()
		
	GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInput)
	SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS_MULTIPLIER(DT_POLICE_AUTOMOBILE, 0.5) 
	SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_POLICE_AUTOMOBILE, 1)
	
	IF dialogueStruct.PedInfo[1].ActiveInConversation
		IF DOES_BLIP_EXIST(blipAttacker)
			REMOVE_BLIP(blipAttacker)
		ENDIF
		REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)
	ENDIF
	
	IF bGunDownPrompt
		IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedBystander, WEAPONTYPE_PISTOL)
		AND IS_PED_INJURED(pedAttacker)
			//IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_BLAME", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
			IF NOT bFailedThroughTimeOut
				ambStage = ambWaitForCops
			ELSE
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(dialogueStruct, "RERHOAU", "RERHO_BLAME", "RERHO_BLAME_3", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
					ambStage = ambWaitForCops
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedBystander)
				IF NOT IS_PED_INJURED(pedAttacker)
					TASK_SMART_FLEE_PED(pedBystander, pedAttacker, 100, -1)
				ELSE
					TASK_SMART_FLEE_PED(pedBystander, PLAYER_PED_ID(), 100, -1)
				ENDIF
				SET_PED_KEEP_TASK(pedBystander, TRUE)
				IF NOT bFailedThroughTimeOut
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(dialogueStruct, "RERHOAU", "RERHO_BLAME", "RERHO_BLAME_3", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
					ELSE
						PLAY_PED_AMBIENT_SPEECH(pedBystander, "GENERIC_SHOCKED_HIGH")
					ENDIF
				ELSE
					PLAY_PED_AMBIENT_SPEECH(pedBystander, "GENERIC_SHOCKED_HIGH")
				ENDIF
				WAIT(0)
			ENDIF
			SET_WANTED_LEVEL_MULTIPLIER(1)
			SET_CREATE_RANDOM_COPS(TRUE)
			ambStage = ambWaitForCops
		ENDIF
	ELSE
		missionCleanup()	
	ENDIF
	
ENDPROC


PROC RUN_BYSTANDER_REACTION_TO_SAVED_HOSTAGE()
	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedBystander, <<25, 25, 10>>)
		bGunDownPrompt = TRUE
	ENDIF
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
	IF bGunDownPrompt
//		OPEN_SEQUENCE_TASK(seqHostage)
//			TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 4)
//			TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
//			TASK_PLAY_ANIM(NULL, "amb@hostage", "bystander_thanks", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//			TASK_CLEAR_LOOK_AT(NULL)
//			TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1, SLF_USE_TORSO)
//			TASK_GO_TO_ENTITY(NULL, pedVictim, DEFAULT_TIME_BEFORE_WARP, 3, PEDMOVEBLENDRATIO_WALK)
//			TASK_STAND_STILL(NULL, -1)
//		CLOSE_SEQUENCE_TASK(seqHostage)
//		TASK_PERFORM_SEQUENCE(pedBystander, seqHostage)
//		SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
//		SET_PED_KEEP_TASK(pedBystander, TRUE)
		ambStage = ambWaitForAmbulance
	ELSE
//		OPEN_SEQUENCE_TASK(seqHostage)
//			// Look around anim?
//			TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1, SLF_USE_TORSO)
//			TASK_GO_TO_ENTITY(NULL, pedVictim, DEFAULT_TIME_BEFORE_WARP, 3, PEDMOVEBLENDRATIO_WALK)
//			TASK_STAND_STILL(NULL, -1)
//		CLOSE_SEQUENCE_TASK(seqHostage)
//		TASK_PERFORM_SEQUENCE(pedBystander, seqHostage)
//		SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
//		SET_PED_KEEP_TASK(pedBystander, TRUE)
		ambStage = ambWaitForAmbulance
	ENDIF
ENDPROC

FUNC BOOL RUN_HOSTAGE_FALLING_TO_KNEES()
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> RUN_HOSTAGE_FALLING_TO_KNEES")
		
	IF NOT bHostageHasFallenToKnees
	
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> bHostageHasFallenToKnees")
		WAIT(0)
		IF NOT IS_PED_INJURED(pedVictim)
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> IS_PED_INJURED(pedVictim)")
			IF scenePassID = -1
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> scenePassID = -1")
				SET_PED_CAN_RAGDOLL(pedVictim, FALSE)
				SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS_MULTIPLIER(DT_AMBULANCE_DEPARTMENT, 0.5) 
				SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_AMBULANCE_DEPARTMENT, 1)
				
				SET_PED_GENERATES_DEAD_BODY_EVENTS(pedVictim, TRUE)
				//KILL_ANY_CONVERSATION()
				SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
				TASK_PLAY_ANIM(pedVictim, animDict, "girl_villian_shot", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE)
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
				
				scenePassID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
	
				//SET_SYNCHRONIZED_SCENE_LOOPED (scenePassID, TRUE)
				IF NOT IS_PED_INJURED(pedBystander)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBystander, relPlayer)
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-91.307045,6391.853516,30.640295>>, <<-102.559540,6403.102539,32.454437>>, 8.500000)
						TASK_SYNCHRONIZED_SCENE (pedBystander, scenePassID, animDict, "bystander_villian_shot", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBystander, FALSE)
						//bPlayBystanderComfort=FALSE
					ENDIF
				ENDIF
				TASK_SYNCHRONIZED_SCENE (pedVictim, scenePassID, animDict, "girl_villian_shot", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				
			ELSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(scenePassID)
				
					// Wait for a bit before checking the player is doing anything nasty.
					IF GET_SYNCHRONIZED_SCENE_PHASE(scenePassID) > 0.1
						CHECK_FOR_PLAYER_DAMAGING_SURVIVORS()
					ENDIF
				
					IF NOT bBystanderThanksPlayer
						IF bPlayBystanderComfort
							IF GET_SYNCHRONIZED_SCENE_PHASE(scenePassID) > 0.5
								IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_SAINT", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
									bBystanderThanksPlayer = TRUE		
								ENDIF								
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_THANK", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
								bBystanderThanksPlayer = TRUE		
							ENDIF
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(scenePassID) = 1
						scenePassID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
						
						//STOP_PED_SPEAKING(pedVictim, FALSE)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "WHIMPER", "WAVELOAD_PAIN_FEMALE")
						SET_SYNCHRONIZED_SCENE_LOOPED (scenePassID, TRUE)
						IF NOT IS_PED_INJURED(pedBystander)
							IF bPlayBystanderComfort
								TASK_SYNCHRONIZED_SCENE (pedBystander, scenePassID, animDict, "bystander_helping_girl_loop", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
							ELSE
								OPEN_SEQUENCE_TASK(seqBystander)
									// Look around anim?
									TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1, SLF_USE_TORSO)
									TASK_GO_TO_ENTITY(NULL, pedVictim, DEFAULT_TIME_BEFORE_WARP, 3, PEDMOVEBLENDRATIO_WALK)
									TASK_STAND_STILL(NULL, -1)
								CLOSE_SEQUENCE_TASK(seqBystander)
								TASK_PERFORM_SEQUENCE(pedBystander, seqBystander)	
							ENDIF
							SET_PED_KEEP_TASK(pedBystander, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedBystander, relManager)
						ENDIF
						TASK_SYNCHRONIZED_SCENE (pedVictim, scenePassID, animDict, "girl_helping_girl_loop", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
						SET_PED_KEEP_TASK(pedVictim, TRUE)
						//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, relManager)
						SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
						SET_WANTED_LEVEL_MULTIPLIER(1)
						bHostageHasFallenToKnees = TRUE
					ENDIF
				ELSE
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bHostageHasFallenToKnees
ENDFUNC


PROC RUN_DEBUG()
#IF IS_DEBUG_BUILD 
	// Event Passed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		missionCleanup()
	ENDIF
	// Event Failed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		missionCleanup()
	ENDIF
	
	IF bMoveScene
		IF NOT IS_PED_INJURED(pedAttacker)
		AND NOT IS_PED_INJURED(pedVictim)
		AND NOT IS_PED_INJURED(pedBystander)
			vInput.z = 31.4542
			scenePosition = vInput
			sceneRotation = <<0,0,fInput>>

			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			SET_SYNCHRONIZED_SCENE_LOOPED (sceneID, TRUE)

			TASK_SYNCHRONIZED_SCENE (pedAttacker, sceneId, animDict, "villian_struggle_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE (pedVictim, sceneId, animDict, "girl_struggle_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT)
			
			sceneBystander = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			SET_SYNCHRONIZED_SCENE_LOOPED (sceneBystander, TRUE)
			TASK_SYNCHRONIZED_SCENE (pedBystander, sceneBystander, animDict, "bystander_please_help_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		ENDIF
		bMoveScene = FALSE
	ENDIF
	
	IF bResetPerception
		IF NOT IS_PED_INJURED(pedAttacker)
			SET_PED_VISUAL_FIELD_MIN_ANGLE(pedAttacker, fPerceptionMin)
			SET_PED_VISUAL_FIELD_MAX_ANGLE(pedAttacker, fPerceptionMax)
			SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedAttacker, fPerceptionPeripheralRange)
			SET_PED_VISUAL_FIELD_CENTER_ANGLE(pedAttacker, fPerceptionCentreAngle)	
		ENDIF
		bResetPerception = FALSE
	ENDIF
	
#ENDIF
ENDPROC

PROC SETUP_DEBUG()
#IF IS_DEBUG_BUILD 
	START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())
		//<<-96.6, 6397.7705, 30.4542>>
		ADD_WIDGET_FLOAT_SLIDER("vInput.x", vInput.x, -98, -95, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vInput.y", vInput.y, 6396, 6399, 0.01)
			
		ADD_WIDGET_BOOL("bMoveScene", bMoveScene)
		
		ADD_WIDGET_FLOAT_SLIDER("fPerceptionMin", fPerceptionMin, -180, -1, 1)
		ADD_WIDGET_FLOAT_SLIDER("fPerceptionMax", fPerceptionMax, 1, 180, 1)
		ADD_WIDGET_FLOAT_SLIDER("fPerceptionPeripheralRange", fPerceptionPeripheralRange, 1, 180, 1)
		ADD_WIDGET_FLOAT_SLIDER("fPerceptionCentreAngle", fPerceptionCentreAngle, 1, 90, 1)
			
		ADD_WIDGET_BOOL("bResetPerception", bResetPerception)
	STOP_WIDGET_GROUP()
#ENDIF
ENDPROC

PROC IDENTIFY_CURRENT_PLAYER()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL  //0
			playerVoice = "MICHAEL"
			playerDialogue = "RERHO_MDOWN"
		BREAK
    	CASE CHAR_FRANKLIN //1
			playerVoice = "FRANKLIN"
			playerDialogue = "RERHO_FDOWN"
		BREAK
    	CASE CHAR_TREVOR   //2
			playerVoice = "TREVOR"
			playerDialogue = "RERHO_TDOWN"
		BREAK
	ENDSWITCH
ENDPROC


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
fInput = in_coords.headings[0]
IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	missionCleanup()
ENDIF

// Small section to fool the compiler
	PRINTNL()
	PRINTSTRING(GET_THIS_SCRIPT_NAME())
	PRINTSTRING(" launched at ")
	PRINTVECTOR(vInput)
	PRINTSTRING("with heading ")
	PRINTFLOAT(fInput)
	PRINTNL()
	
IF g_eCurrentBuildingState[BUILDINGNAME_RF_GASSTATION01] = BUILDINGSTATE_DESTROYED
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " will terminate as g_eCurrentBuildingState[BUILDINGNAME_RF_GASSTATION02] = BUILDINGSTATE_DESTROYED")
	TERMINATE_THIS_THREAD()
ENDIF

//----
IF CAN_RANDOM_EVENT_LAUNCH(vInput)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

IDENTIFY_CURRENT_PLAYER()
SETUP_DEBUG()

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	RUN_DEBUG()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		SWITCH ambStage
			CASE ambCanRun
				IF LOADED_AND_CREATED_HOSTAGE_SCENE()
					iTimeUpTimer = GET_GAME_TIMER()
					ambStage = (ambRunning)
				ELSE
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
						missionCleanup()
					ENDIF
				ENDIF
			BREAK
			CASE ambRunning
			
				IF iShockingEvent = 0
					iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, <<-104.9820, 6408.7373, 30.4905>>, 180000)
				ENDIF
				
				CHECK_SUBTITLE_STATUS()
				
				IF NOT IS_PED_INJURED(pedVictim)
				
					CHECK_FOR_PLAYER_DAMAGING_HOSTAGE()
					
				ELSE
					IF NOT IS_PED_INJURED(pedBystander)
					
						RUN_BYSTANDER_REACTION_TO_DEAD_HOSTAGE()
						RUN_BYSTANDER_PANICKED_BY_EXCESSIVE_FORCE()
						
					ELSE
					
						missionCleanup()
						
					ENDIF
					
				ENDIF	
				
				IF bHostageShouldBeExecuted
				
					EXECUTE_THE_HOSTAGE()

				ENDIF
				
				IF NOT IS_PED_INJURED(pedAttacker)
//					IF CAN_PED_SEE_HATED_PED(pedAttacker, PLAYER_PED_ID())
//						PRINTLN(GET_THIS_SCRIPT_NAME(), " - pedAttacker can see player.")
//					ELSE
//						PRINTLN(GET_THIS_SCRIPT_NAME(), " - pedAttacker *can't* see player.")
//					ENDIF
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
							missionCleanup()
						ENDIF
						RUN_ATTRACT_AND_SWITCH_BLIPS()
					ELSE
						IF NOT bHostageShouldBeExecuted
							RUN_ATTACKER_INTERACTION()
						ENDIF
					ENDIF
					
				ELSE
					
					// Remove attacker from dialogue
					IF dialogueStruct.PedInfo[1].ActiveInConversation
						IF DOES_BLIP_EXIST(blipAttacker)
							REMOVE_BLIP(blipAttacker)
						ENDIF
						IF DOES_BLIP_EXIST(blipScene)
							REMOVE_BLIP(blipScene)
						ENDIF
						REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					
					
					IF NOT IS_PED_INJURED(pedVictim)
					
						IF bHostageShouldBeExecuted
							// Kills hostage after full death anim has played out.
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) = 1
									IF NOT IS_PED_INJURED(pedVictim)
										SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
										SET_ENTITY_HEALTH(pedVictim, 0)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							// Hostage survived attack
							IF RUN_HOSTAGE_FALLING_TO_KNEES()
								//IF NOT IS_PED_INJURED(pedBystander)
									//missionPassed()
									ambStage = ambWaitForAmbulance
								//ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(pedBystander)
								//RUN_BYSTANDER_REACTION_TO_SAVED_HOSTAGE()
							ENDIF
								
						ENDIF

					ENDIF
				ENDIF
			BREAK
			
			CASE ambWaitForCops
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> ambWaitForCops")
				vehCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 35, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK|VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL|VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				IF IS_VEHICLE_DRIVEABLE(vehCopCar)
					IF NOT bFailedThroughTimeOut
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					missionCleanup()
				ENDIF
				
				IF NOT bDispatchSuccessful
					IF CREATE_INCIDENT(DT_POLICE_VEHICLE_REQUEST, vInput, 2, 0.0, tempIncident)
						CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), ">CREATE_INCIDENT(DT_POLICE_VEHICLE_REQUEST, vInput, 2, 0.0, tempIncident)")
						bDispatchSuccessful = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE ambWaitForAmbulance
				IF bAmbulanceFound
					IF TIMERA() > 5000	
						IF NOT IS_PED_INJURED(pedVictim)
							//TASK_PLAY_ANIM(pedVictim, animDict, "hostage_knees_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							SET_PED_KEEP_TASK(pedVictim, TRUE)
							missionPassed()
						ENDIF
						 
						missionCleanup()
					ENDIF
				ELSE
					IF RUN_HOSTAGE_FALLING_TO_KNEES()
						IF NOT bVictimThanksPlayer
							IF bPlayBystanderComfort
								IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_THANK", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
									bVictimThanksPlayer = TRUE		
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_NOGUN", CONV_PRIORITY_AMBIENT_HIGH, displaySubs)
									bVictimThanksPlayer = TRUE		
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					CHECK_FOR_PLAYER_DAMAGING_SURVIVORS()
					
					IF NOT IS_PED_INJURED(pedVictim)
						IF GET_GAME_TIMER() > iVictimAmbientAudio+100
							//STOP_PED_SPEAKING(pedVictim, FALSE)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "WHIMPER", "WAVELOAD_PAIN_FEMALE")
							iVictimAmbientAudio = GET_GAME_TIMER()
						ENDIF
					ENDIF
					IF bVictimThanksPlayer
						IF NOT bPlayerThanked
							IF NOT IS_PED_INJURED(pedBystander)
								IF CREATE_CONVERSATION(dialogueStruct, "RERHOAU", "RERHO_CONS", CONV_PRIORITY_AMBIENT_HIGH, displaySubs) // PLay RERHO_Saint during run out?
									bPlayerThanked = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					vehCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(vInput, 5, AMBULANCE, 0)
					IF IS_VEHICLE_DRIVEABLE(vehCopCar)
						SETTIMERA(0)
						bAmbulanceFound = TRUE
					ENDIF
					pedMedic = GET_RANDOM_PED_AT_COORD(vInput, <<1.5,1.5,1.5>>)
					IF NOT IS_PED_INJURED(pedMedic)
						SETTIMERA(5000)
						bAmbulanceFound = TRUE
					ENDIF
					IF NOT bDispatchSuccessful
					AND bPlayerThanked
						IF CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, vInput, 2, 0.0, girlIncident)
							CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, vInput, 2, 0.0, girlIncident)")
							//missionPassed()
							bDispatchSuccessful = TRUE
						ENDIF
					ENDIF
					
					IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInput) > 75*75 AND NOT IS_SPHERE_VISIBLE(vInput, 10))
						missionPassed()							
					ENDIF
										
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF ambStage = ambWaitForAmbulance
			missionPassed()
		ELSE
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), "> IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()\n")
			missionCleanup()
		ENDIF
	ENDIF
	
ENDWHILE


ENDSCRIPT


