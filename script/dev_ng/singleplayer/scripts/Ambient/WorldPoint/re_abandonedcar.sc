//////////////////////////////////////////////////////////
// 				Abandoned vehicle attack
//		   		Tor Sigurdson
//
//
//				1
//			
//
//
//
//				2
//////////////////////////////////////////////////////////
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING "script_PLAYER.sch"
USING "rage_builtins.sch"
USING "LineActivation.sch"
USING "Ambient_Speech.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "ambient_common.sch"
USING "taxi_functions.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "ped_component_public.sch"
USING "commands_recording.sch"

///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM homelessStageFlag
	homelessInit,
	homelessWaitForPlayer,
	homelessCleanup
ENDENUM
//homelessStageFlag = homelessInit

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventVariations
	eV_TARGET_NONE,
	eV_TAZER_RAPIST,
	eV_INCEST_BROTHERS,
	eV_TARGET_END
ENDENUM
eventVariations thisEvent = eV_TARGET_NONE

//INT random_int 
BOOL bAssetsLoaded
BOOL bDoFullCleanUp
BOOL bVariablesInitialised
VECTOR v_spawnpoint

VECTOR vRedneckCoords
VECTOR vRedneckCoords1
VECTOR vRedneckCoords2
VECTOR vRedneckCoords3

//VECTOR coords_crazed2_killer
//FLOAT heading_crazed2_killer

VECTOR vVehicleCoords
FLOAT fVehicleHeading

VECTOR vAttackTriggerDistance
VECTOR vFleeDistance
//VECTOR coords_mugger_2
//VECTOR coords_mugger_3
//VECTOR coords_mugger_4

//VECTOR coord_temp_player
REL_GROUP_HASH rghRedneck
//FLOAT heading_mugger_2
//FLOAT heading_mugger_3
//FLOAT heading_mugger_4
streamvol_id streaming_volume

BOOL bRapistBlipped
structPedsForConversation cultresConversation

//BLIP_INDEX blip_abandoned_vehicle

BLIP_INDEX blipRedneck0
BLIP_INDEX blipRedneck1
BLIP_INDEX blipInitialBlip
//BLIP_INDEX blip_mugger_2
//BLIP_INDEX blip_mugger_3
//BLIP_INDEX blip_mugger_4

PED_INDEX pedRedneck0
PED_INDEX pedRedneck1

VEHICLE_INDEX vehAbandonedVehicle
VEHICLE_INDEX vehTrain
VEHICLE_INDEX vehImpound
//PED_INDEX ped_mugger_2
//PED_INDEX ped_mugger_3
//PED_INDEX ped_mugger_

//PED_INDEX pedestrian
WEAPON_TYPE CurrentWeapon
MODEL_NAMES modelAttackingPed
MODEL_NAMES modelAbandonedVehicle

BOOL bPlayerInteractingWithEvent
BOOL bPlayerAttacked
BOOL flag_lights_state
BOOL bRedneckFiringAtPlayer
BOOL bPlayerInRagdoll
BOOL bPlayerFleeing
BOOL bPlayedTrainHorn
BOOL bPlayerPassedOut
BOOL bApproachDialoguePlayed
BOOL bSexTalked
BOOL bPlayerTalked


//BOOL bTimerSet
SEQUENCE_INDEX seq
//BOOL f_is_ped_in_rag_doll = FALSE

INT flag_girl_talk_flag = 0
INT iApproachTriggerDistance
INT iApproachPlayerDialogue
INT iCounterBeforePlayerPassesOut = 0
INT iTimerDialogue
STRING sMusicEvent

INT iEndProgress = 0

VECTOR vWorldPoint1 = << 1435.7700, 2983.1100, 40.7700 >>
VECTOR vWorldPoint2 = << 2162.5518, 2122.6460, 124.7956 >>

CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
INT iSelectedVariation

//BOOL make_goon_approach = FALSE

///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
///    
///    1. Wait For player to get close
///    2. Blip Girl
///    3. Girl walks down alley, she will stop if player is not close. MIssion fail is player is too far away
///    4. create thugs in alley that attack player
///    
///    
///    
PROC missionCleanup()
	PRINTSTRING("@@@@@@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
	
	IF bDoFullCleanUp
		IF IS_PED_INJURED(PLAYER_PED_ID())
			IF ANIMPOSTFX_IS_RUNNING("Dont_tazeme_bro")
				ANIMPOSTFX_STOP("Dont_tazeme_bro")
			ENDIF
		ENDIF
	
		STREAMVOL_DELETE(streaming_volume)
		RELEASE_SCRIPT_AUDIO_BANK()
		CANCEL_MUSIC_EVENT(sMusicEvent)
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_MIN_GROUND_TIME_FOR_STUNGUN(PLAYER_PED_ID(), -1)
		ENDIF
		
		STOP_AUDIO_SCENE("RE_ABANDONED_VEHICLE_SCENE")
		
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		DISABLE_TAXI_HAILING(FALSE)
		
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		IF NOT IS_ENTITY_DEAD(vehImpound)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpound)
			AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehImpound, << 50, 50, 50 >>)
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedRedneck0)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_DROPS_WEAPON(pedRedneck0)
			TASK_SMART_FLEE_PED(pedRedneck0, PLAYER_PED_ID(), 1000, -1)
			SET_PED_KEEP_TASK(pedRedneck0, TRUE) 
		ENDIF
		
		IF NOT IS_PED_INJURED(pedRedneck1)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_DROPS_WEAPON(pedRedneck1)
			TASK_SMART_FLEE_PED(pedRedneck1, PLAYER_PED_ID(), 1000, -1)
			SET_PED_KEEP_TASK(pedRedneck1, TRUE) 
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	#IF IS_DEBUG_BUILD
		IF can_cleanup_script_for_debug
			IF DOES_ENTITY_EXIST(pedRedneck0)
				DELETE_PED(pedRedneck0)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehAbandonedVehicle)
				DELETE_VEHICLE(vehAbandonedVehicle)
			ENDIF
		ENDIF
		
		PRINTNL()
		PRINTSTRING("re_abandoned cleaned up")
		PRINTNL()
	#ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		KILL_FACE_TO_FACE_CONVERSATION()
//		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			PRINTSTRING("\n @@@@@@@@@@@@@@@ STUCK IN IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() @@@@@@@@@@@@@@@ \n")
//			WAIT(0)
//		ENDWHILE
//		WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
//			PRINTSTRING("\n @@@@@@@@@@@@@@@ STUCK IN LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC() @@@@@@@@@@@@@@@ \n")
//			WAIT(0)
//		ENDWHILE
		RANDOM_EVENT_PASSED(RE_ABANDONEDCAR, iSelectedVariation)
		RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
		missionCleanup()
	ENDIF
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	missionCleanup()
ENDPROC

FUNC VECTOR getWorldPoint()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint1)
		iSelectedVariation = iWorldPoint1
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2)
			iSelectedVariation = iWorldPoint2
		ENDIF
		
		IF iSelectedVariation = iWorldPoint1
			thisEvent = eV_TAZER_RAPIST
			RETURN vWorldPoint1
		ELIF iSelectedVariation = iWorldPoint2
			thisEvent = eV_INCEST_BROTHERS
			RETURN vWorldPoint2
		ENDIF
	ENDIF
	RETURN << 0, 0, 0 >>
	
ENDFUNC

PROC initialiseEventVariables()

	IF thisEvent = eV_TAZER_RAPIST //dirt road
	//	vRedneckCoords = << 1422.7804, 2931.1455, 41.4402 >>
		
//		vRedneckCoords = GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 0, 10, 0 >> //<< 1427.6652, 2961.6257, 40.0426 >>
//		fRedneckHeading = 243.0
//		
//		vRedneckCoords1 = GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 10, 0, 0 >> //<< 1440.1327, 2979.9692, 39.7937 >> //create ped behind the camera
//		fRedneckHeading1 = 200.0
		
		
		
 		//coords_crazed2_killer = <<1434.44,2962.24,38.32>>
 		//heading_crazed2_killer = 0.0
		modelAttackingPed = A_M_M_hillbilly_01
		modelAbandonedVehicle = SURFER
		
		vVehicleCoords = << 1430.0726, 2976.3992, 39.7694 >>
		fVehicleHeading = 131.5580
		
		vFleeDistance = << 18, 18, 10 >>
		
		iApproachTriggerDistance = 170
		iApproachPlayerDialogue = 40
		vAttackTriggerDistance = << 20, 20, 10 >>
		
		sMusicEvent = "RE8A_TASERED"
		
	ELIF thisEvent = eV_INCEST_BROTHERS //wind power
		modelAttackingPed = A_M_Y_ACULT_01
		modelAbandonedVehicle = JOURNEY
		
		vRedneckCoords = << 2162.5518, 2122.6460, 124.7956 >>
		
		vVehicleCoords = << 2162.5518, 2122.6460, 124.7956 >>
		fVehicleHeading = 218.6214
		
		vFleeDistance = << 20, 20, 20 >>
		
		iApproachTriggerDistance = 70
		iApproachPlayerDialogue = 25
		vAttackTriggerDistance = << 10, 10, 10 >>
		
		sMusicEvent = "RE8B_CAUGHT"
	ENDIF
	
	bVariablesInitialised = TRUE
	
ENDPROC

PROC create_assets()
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SURFER, TRUE)
	IF thisEvent = eV_TAZER_RAPIST
		ADD_SCENARIO_BLOCKING_AREA(vVehicleCoords - << 50, 50, 20 >>, vVehicleCoords + << 50, 50, 20 >>)
	ELIF thisEvent = eV_INCEST_BROTHERS
		ADD_SCENARIO_BLOCKING_AREA(vVehicleCoords - << 100, 100, 50 >>, vVehicleCoords + << 100, 100, 50 >>)
	ENDIF
	CLEAR_AREA_OF_VEHICLES(v_spawnpoint, 50)
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	
	REQUEST_MODEL(modelAttackingPed)
	REQUEST_MODEL(modelAbandonedVehicle)
	REQUEST_ANIM_DICT("random@train_tracks")
	REQUEST_ANIM_DICT("amb@world_human_guard_patrol@male@idle_b")
	PREPARE_MUSIC_EVENT(sMusicEvent)
	REGISTER_SCRIPT_WITH_AUDIO()
	START_AUDIO_SCENE("RE_ABANDONED_VEHICLE_SCENE")
	
	/*
	loopSceneId = CREATE_SYNCHRONIZED_SCENE(GET_ROADSIDE_POS(), sceneRotation)
	SET_SYNCHRONIZED_SCENE_LOOPED(loopSceneId, TRUE)
	TASK_SYNCHRONIZED_SCENE (pedPossum, loopSceneId, "amb@CARJACK", "carjack_waitLoop_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
	TASK_SYNCHRONIZED_SCENE (pedJacker, loopSceneId, "amb@CARJACK", "carjack_mainAction_female", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
	*/
	WHILE NOT HAS_MODEL_LOADED(modelAttackingPed)
	OR NOT HAS_MODEL_LOADED(modelAbandonedVehicle)
	OR NOT HAS_ANIM_DICT_LOADED("random@train_tracks")
	OR NOT HAS_ANIM_DICT_LOADED("amb@world_human_guard_patrol@male@idle_b")
	OR NOT PREPARE_MUSIC_EVENT(sMusicEvent)
	OR NOT IS_AUDIO_SCENE_ACTIVE("RE_ABANDONED_VEHICLE_SCENE")
		WAIT(0)
	ENDWHILE
	
	//	ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "FRANKLIN")	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(cultresConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(cultresConversation, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	
	//SET_ROADS_IN_AREA(<<316.13,2555.7,27.21>>, <<487.0,2727.97,50.8>>, FALSE) 
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	vehAbandonedVehicle = CREATE_VEHICLE(modelAbandonedVehicle, vVehicleCoords, fVehicleHeading)
	SET_ENTITY_LOD_DIST(vehAbandonedVehicle, 10000)
	SET_LAST_DRIVEN_VEHICLE(vehAbandonedVehicle)
//	SET_VEHICLE_CAN_LOD(vehAbandonedVehicle, FALSE)
	SET_VEHICLE_LOD_MULTIPLIER(vehAbandonedVehicle, 5)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelAbandonedVehicle)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehAbandonedVehicle, "RE_ABANDONED_VEHICLE_MOTORHOME")
	SET_AUDIO_VEHICLE_PRIORITY(vehAbandonedVehicle, AUDIO_VEHICLE_PRIORITY_MAX)
	
	ADD_RELATIONSHIP_GROUP("Redneck", rghRedneck)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghRedneck, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rghRedneck)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghRedneck, RELGROUPHASH_COP)
	
	IF thisEvent = eV_TAZER_RAPIST
		SET_VEHICLE_DOOR_OPEN(vehAbandonedVehicle, SC_DOOR_FRONT_LEFT, TRUE)
		SET_VEHICLE_DOOR_OPEN(vehAbandonedVehicle, SC_DOOR_FRONT_RIGHT, TRUE)
//		SET_VEHICLE_INDICATOR_LIGHTS(vehAbandonedVehicle, TRUE, TRUE)
		SET_VEHICLE_DAMAGE(vehAbandonedVehicle,vVehicleCoords, 0.0, 1700.0, TRUE)
		SET_VEHICLE_ENGINE_ON(vehAbandonedVehicle, FALSE, TRUE)
		SET_VEHICLE_LIGHTS(vehAbandonedVehicle, FORCE_VEHICLE_LIGHTS_ON)
		
	ELIF thisEvent = eV_INCEST_BROTHERS
		SET_VEHICLE_ENGINE_ON(vehAbandonedVehicle, FALSE, TRUE)
		SET_VEHICLE_LIGHTS(vehAbandonedVehicle, FORCE_VEHICLE_LIGHTS_OFF)
		
		pedRedneck0 = CREATE_PED_INSIDE_VEHICLE(vehAbandonedVehicle, PEDTYPE_MISSION, modelAttackingPed)
		SET_PED_COMPONENT_VARIATION(pedRedneck0, PED_COMP_HEAD, 0, 2, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedRedneck0, PED_COMP_HAIR, 1, 2, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedRedneck0, PED_COMP_TORSO, 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedRedneck0, PED_COMP_LEG, 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedRedneck0, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRedneck0, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedRedneck0, rghRedneck)
		GIVE_WEAPON_TO_PED(pedRedneck0, WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO)
		SET_PED_COMBAT_ATTRIBUTES(pedRedneck0, CA_USE_VEHICLE, FALSE)
		SET_PED_ACCURACY(pedRedneck0, 25)
		STOP_PED_SPEAKING(pedRedneck0, TRUE)
		SET_AMBIENT_VOICE_NAME(pedRedneck0, "A_M_M_HillBilly_02_WHITE_MINI_01")
		ADD_PED_FOR_DIALOGUE(cultresConversation, 4, pedRedneck0, "INCESTBRO1")
		
		pedRedneck1 = CREATE_PED_INSIDE_VEHICLE(vehAbandonedVehicle, PEDTYPE_MISSION, modelAttackingPed, VS_FRONT_RIGHT)
		SET_PED_COMPONENT_VARIATION(pedRedneck1, PED_COMP_HEAD, 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedRedneck1, PED_COMP_HAIR, 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedRedneck1, PED_COMP_TORSO, 0, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedRedneck1, PED_COMP_LEG, 1, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedRedneck1, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		
		SET_ENTITY_HEALTH(pedRedneck1, 100000000)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRedneck1, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedRedneck1, rghRedneck)
		GIVE_WEAPON_TO_PED(pedRedneck1, WEAPONTYPE_PISTOL, INFINITE_AMMO)
		SET_PED_COMBAT_ATTRIBUTES(pedRedneck1, CA_USE_VEHICLE, FALSE)
		SET_PED_ACCURACY(pedRedneck1, 25)
		STOP_PED_SPEAKING(pedRedneck1, TRUE)
		SET_AMBIENT_VOICE_NAME(pedRedneck1, "A_M_M_HillBilly_02_WHITE_MINI_02")
		ADD_PED_FOR_DIALOGUE(cultresConversation, 5, pedRedneck1, "INCESTBRO2")
		
//		INT SceneId
//		SceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(pedRedneck0), GET_ENTITY_ROTATION(pedRedneck0))
//		TASK_SYNCHRONIZED_SCENE(pedRedneck0, SceneId, "mini@prostitutes", "frontseat_carsex_loop_m", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
//		TASK_SYNCHRONIZED_SCENE(pedRedneck1, SceneId, "mini@prostitutes", "frontseat_carsex_loop_m", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
		#IF NOT IS_JAPANESE_BUILD
			TASK_PLAY_ANIM(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(pedRedneck1, "random@train_tracks", "frontseat_carsex_loop_top_guy", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		#ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(modelAttackingPed)
	ENDIF
//	blipInitialBlip = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(vehAbandonedVehicle)
//	IF DOES_BLIP_EXIST(blipInitialBlip)
//		REMOVE_BLIP(blipInitialBlip)
//	ENDIF
//	blipInitialBlip = CREATE_BLIP_FOR_VEHICLE(vehAbandonedVehicle)
	
	bAssetsLoaded = TRUE
	
ENDPROC

PROC debug_stuff()
	
		#IF IS_DEBUG_BUILD

//			DRAW_DEBUG_SPHERE(vRedneckCoords, 3)
//			DRAW_DEBUG_SPHERE(vRedneckCoords1, 3)
//			DRAW_DEBUG_SPHERE(vRedneckCoords2, 3)
//			DRAW_DEBUG_SPHERE(vRedneckCoords3, 3)
			
			IF thisEvent = eV_TAZER_RAPIST
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1431.9551, 2974.1677, 40.1066 >>, TRUE, FALSE)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1576.8081, 3104.7634, 39.9188 >>, TRUE, FALSE)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()) + << 0.05, 0.05, 0.05 >>, GET_PLAYER_COORDS(PLAYER_ID()), 0, TRUE, WEAPONTYPE_STUNGUN)
				ENDIF
			ELIF thisEvent = eV_INCEST_BROTHERS
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2168.0273, 2117.5161, 124.2550 >>, TRUE, FALSE)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2214.9387, 2064.0608, 131.3584 >>, TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
			
		#ENDIF
	
ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)

	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_MOVING_LEFT_STICK(INT iLEFT_STICK_THRESHOLD = 64)
	
	INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) -128 
	INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) -128 
	
	IF ReturnLeftX < iLEFT_STICK_THRESHOLD
	AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
	AND ReturnLeftY < iLEFT_STICK_THRESHOLD
	AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC sexTalkAndBounce()
	IF NOT IS_PED_INJURED(pedRedneck0)
		IF IS_PED_IN_ANY_VEHICLE(pedRedneck0)
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(pedRedneck0))
				#IF NOT IS_JAPANESE_BUILD
					IF IS_ENTITY_PLAYING_ANIM(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy")
						IF (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.1
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.075)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.250
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.225)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.380
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.355)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.530
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.505)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.638
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.613)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.7
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.675)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 0.833
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.808)
						
						OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") < 1
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "random@train_tracks", "frontseat_carsex_loop_low_guy") > 0.983)
		//				OR (GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "amb@cardrunksex", "cardrunksex_loop_f") < 0.35
		//				AND GET_ENTITY_ANIM_CURRENT_TIME(pedRedneck0, "amb@cardrunksex", "cardrunksex_loop_f") > 0.3)
						
	//					IF iTimerBounce < GET_GAME_TIMER()
							APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedRedneck0), APPLY_TYPE_FORCE,  << 0, 0, 15 >>, << -0.4, 0, 0 >>, 0, TRUE, TRUE, TRUE)
//							APPLY_FORCE_TO_ENTITY(GET_VEHICLE_PED_IS_IN(pedRedneck0), APPLY_TYPE_IMPULSE,  << 0, 0, 0.5 >>, << -0.4, 0, 0 >>, 0, TRUE, TRUE, TRUE)
							PLAY_SOUND_FROM_ENTITY(-1, "SUSPENSION_SCRIPT_FORCE", GET_VEHICLE_PED_IS_IN(pedRedneck0))
	//						iTimerBounce = GET_GAME_TIMER() + 500
						ENDIF
	//					IF iTimerSexDialogue < GET_GAME_TIMER()
						
	//						iTimerSexDialogue = GET_GAME_TIMER() + 4500
	//					ENDIF
		//				ENDIF
					ENDIF
				#ENDIF
				IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				AND NOT bSexTalked
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_sex", CONV_PRIORITY_AMBIENT_HIGH)
						bSexTalked = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedRedneck1)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRedneck1, PLAYER_PED_ID())
				TASK_PLAY_ANIM(pedRedneck1, "random@train_tracks", "frontseat_carsex_death_exit_top_guy", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC make_girl_talk()
	
	IF NOT IS_ENTITY_DEAD(pedRedneck0)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 10, 10, 10 >>, FALSE)
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
			IF TIMERB() > 3000
			AND flag_girl_talk_flag = 0
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thank you for helping", 4000, 1) //
				CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac2", CONV_PRIORITY_AMBIENT_HIGH)
				TASK_LOOK_AT_ENTITY(pedRedneck0, PLAYER_PED_ID(), 4000)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghRedneck, RELGROUPHASH_PLAYER)
				SETTIMERB(0)
				flag_girl_talk_flag ++
			ENDIF
			
			IF TIMERB() > 4000
			AND flag_girl_talk_flag = 1
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac1", CONV_PRIORITY_AMBIENT_HIGH)
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Don't worry friend. There's a space waiting for you in heaven.", 4000, 1) //	
				TASK_LOOK_AT_ENTITY(pedRedneck0, PLAYER_PED_ID(), 4000)
				SETTIMERB(0)
				flag_girl_talk_flag ++
			ENDIF
			
			IF TIMERB() > 3000
			AND flag_girl_talk_flag = 2
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac2", CONV_PRIORITY_AMBIENT_HIGH)
			//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "I hate to kill you, but i certainly do love to watch you scream.", 4000, 1) //
				TASK_LOOK_AT_ENTITY(pedRedneck0, PLAYER_PED_ID(), 4000)
				
				//TASK_PLAY_ANIM (pedRedneck0, "amb@CARJACK", "carjack_mainAction_female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
				SETTIMERB(0)
				flag_girl_talk_flag = 0
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC interruptions_to_conversation()

	IF thisEvent = eV_TAZER_RAPIST
		IF NOT IS_PED_INJURED(pedRedneck0)
			IF NOT IS_PED_IN_WRITHE(pedRedneck0)
				IF iEndProgress < 3
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 20, 20, 10 >>)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ELIF thisEvent = eV_INCEST_BROTHERS
		IF NOT IS_ENTITY_DEAD(pedRedneck0)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 31, 31, 11 >>)
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
			ENDIF
		ELIF NOT IS_ENTITY_DEAD(pedRedneck1)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck1, << 31, 31, 11 >>)
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
			ENDIF
		ELSE
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
		ENDIF
			
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bPlayerTalked
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 30, 30, 10 >>)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attacM", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attacF", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attacT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				bPlayerTalked = TRUE
			ENDIF
			
			IF iTimerDialogue < GET_GAME_TIMER()
				IF GET_RANDOM_BOOL()
					IF NOT IS_PED_INJURED(pedRedneck0)
						IF NOT IS_PED_IN_WRITHE(pedRedneck0)
							CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attac2", CONV_PRIORITY_AMBIENT_HIGH)
							iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 5000)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(pedRedneck1)
						IF NOT IS_PED_IN_WRITHE(pedRedneck1)
							CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attac3", CONV_PRIORITY_AMBIENT_HIGH)
							iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 5000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedRedneck0)
		IF NOT IS_PED_IN_WRITHE(pedRedneck0)
	//		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_RAGDOLL(pedRedneck0)
				OR IS_PED_BEING_STUNNED(pedRedneck0, WEAPONTYPE_STUNGUN)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRedneck0, PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedRedneck0)
				ENDIF
	//		ENDIF
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, vFleeDistance)
			OR NOT IS_ENTITY_AT_COORD(pedRedneck0, vVehicleCoords, << 290, 290, 290 >>)
				IF NOT bPlayerFleeing
				AND NOT IS_THIS_CONVERSATION_PLAYING("reac2_attac1")
				AND iEndProgress < 3
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF thisEvent = eV_TAZER_RAPIST
						CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_flee", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = eV_INCEST_BROTHERS
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_flee", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bPlayerFleeing = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC isPlayerApproachingOrInteractingWithEvent()

	IF IS_VEHICLE_DRIVEABLE(vehAbandonedVehicle)
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			//IF NOT IS_ENTITY_DEAD(pedRedneck0)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehAbandonedVehicle)) < iApproachTriggerDistance
					IF DOES_BLIP_EXIST(blipInitialBlip)
						REMOVE_BLIP(blipInitialBlip)
					ENDIF
					blipInitialBlip = CREATE_BLIP_FOR_VEHICLE(vehAbandonedVehicle)
					SHOW_HEIGHT_ON_BLIP(blipInitialBlip, FALSE)
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						vehImpound = GET_PLAYERS_LAST_VEHICLE()
					ENDIF
					SET_RANDOM_EVENT_ACTIVE()
					DISABLE_TAXI_HAILING(TRUE)
				ENDIF
			//ENDIF
		ENDIF
	ELSE
		MISSION_FAILED()
	ENDIF
	
	IF thisEvent = eV_TAZER_RAPIST
	//	IF TIMERB() > 1000
		IF NOT IS_ENTITY_DEAD(pedRedneck0)
			//IF IS_ENTITY_AT_COORD (PLAYER_PED_ID(), v_spawnpoint, <<10.0, 10.0, 10.0>>, FALSE)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 13, 13, 13 >>)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PLAYER_PED_ID(), pedRedneck0)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRedneck0, PLAYER_PED_ID())
			OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedRedneck0), 2)
			//AND IS_PLAYER_ACTIVATING_PED(pedRedneck0)
				bPlayerInteractingWithEvent = TRUE
				SETTIMERB(0)
				IF NOT IS_ENTITY_DEAD(pedRedneck0)
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Mister! Please help! My friend is hurt! He's down this allyway.", 4000, 1) //	
					
					IF NOT bRapistBlipped
						IF NOT DOES_BLIP_EXIST(blipRedneck0)
							blipRedneck0 = CREATE_BLIP_FOR_PED(pedRedneck0, TRUE)
						ENDIF
						REMOVE_BLIP(blipInitialBlip)
						bRapistBlipped = TRUE
//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//						WAIT(0)
//						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac1", CONV_PRIORITY_AMBIENT_HIGH)
//						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//						OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//							CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac2", CONV_PRIORITY_AMBIENT_HIGH)
//						ENDIF
					ELSE
						CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_rambl", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					
					SETTIMERB(0)
					//blip_abandoned_vehicle = ADD_BLIP_FOR_ENTITY(vehAbandonedVehicle)
					//SET_BLIP_AS_FRIENDLY(blip_abandoned_vehicle, TRUE)
					//SET_BLIP_SCALE(blip_abandoned_vehicle, 0.6)
					IF NOT IS_PED_INJURED(pedRedneck0)
						TASK_LOOK_AT_ENTITY(pedRedneck0, PLAYER_PED_ID(), 30000, SLF_DEFAULT)
					ENDIF
					
					
			//		SET_PED_ANIM_GROUP_TWO_HANDED(player_ped_id(),  "ANIM_GROUP_MOVE_LEMAR_ALLEY")
				//	SET_PED_ANIM_GROUP(player_ped_id(),  "ANIM_GROUP_MOVE_LEMAR_ALLEY")
	//				SET_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID(), "MOVE_LEMAR@ALLEY", "run", 8.0, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC make_car_lights_flash(VEHICLE_INDEX& vehicle)
	
	IF TIMERA() > 1000
		IF NOT IS_ENTITY_DEAD(vehicle)
			IF flag_lights_state = TRUE
				flag_lights_state = FALSE
				SET_VEHICLE_LIGHTS(vehicle, FORCE_VEHICLE_LIGHTS_ON)
				SETTIMERA(0)
			ELSE
				flag_lights_state = TRUE
				SET_VEHICLE_LIGHTS(vehicle, FORCE_VEHICLE_LIGHTS_OFF)
				SETTIMERA(0)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC makeRednecksAttack()

//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-424.42,-340.27,33.03>>, <<7.0, 7.0, 7.0>>, FALSE)
	IF NOT IS_ENTITY_DEAD(vehAbandonedVehicle)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehAbandonedVehicle)
		OR thisEvent = eV_INCEST_BROTHERS
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehAbandonedVehicle, vAttackTriggerDistance)
			OR bPlayerInteractingWithEvent
				
//				IF NOT DOES_BLIP_EXIST(blipRedneck0)
//					blipRedneck0 = CREATE_BLIP_FOR_PED(pedRedneck0, TRUE)
//				ENDIF
//				REMOVE_BLIP(blipInitialBlip)
				
				IF thisEvent = eV_TAZER_RAPIST
				
//					HANG_UP_AND_PUT_AWAY_PHONE()
					vRedneckCoords = GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 0, 8.8, -1 >> //<< 1427.6652, 2961.6257, 40.0426 >>
					GET_SAFE_COORD_FOR_PED(vRedneckCoords, FALSE, vRedneckCoords)
					vRedneckCoords1 = GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 8.8, 0, -1 >> //<< 1440.1327, 2979.9692, 39.7937 >> //create ped behind the camera
					GET_SAFE_COORD_FOR_PED(vRedneckCoords1, FALSE, vRedneckCoords1)
					vRedneckCoords2 = GET_ENTITY_COORDS(PLAYER_PED_ID()) + << 0, 8.8, -1 >>
					GET_SAFE_COORD_FOR_PED(vRedneckCoords2, FALSE, vRedneckCoords2)
					vRedneckCoords3 = GET_ENTITY_COORDS(PLAYER_PED_ID()) - << 8.8, 0, -1 >>
					GET_SAFE_COORD_FOR_PED(vRedneckCoords3, FALSE, vRedneckCoords3)
					
					IF NOT IS_SPHERE_VISIBLE(vRedneckCoords, 2)
						pedRedneck0 = CREATE_PED(PEDTYPE_DEALER, modelAttackingPed, vRedneckCoords - << 0, 0, 1 >>, GET_HEADING_BETWEEN_VECTORS_2D(vRedneckCoords, GET_PLAYER_COORDS(PLAYER_ID())))
						
						//SET_PED_DEFENSIVE_AREA_DIRECTION
						//SET_ENTITY_VISIBLE(pedRedneck0, FALSE)
						
					/*	IF  IS_ENTITY_ON_SCREEN(pedRedneck0)
							SET_ENTITY_COORDS(pedRedneck0, vRedneckCoords1, FALSE)
							SET_ENTITY_HEADING(pedRedneck0, fRedneckHeading1)
						ENDIF*/
						
						//SET_ENTITY_VISIBLE(pedRedneck0, TRUE)
						
					ELIF NOT IS_SPHERE_VISIBLE(vRedneckCoords1, 2)
						pedRedneck0 = CREATE_PED(PEDTYPE_DEALER, modelAttackingPed, vRedneckCoords1 - << 0, 0, 1 >>, GET_HEADING_BETWEEN_VECTORS_2D(vRedneckCoords1, GET_PLAYER_COORDS(PLAYER_ID())))
					//	SET_ENTITY_HEADING(pedRedneck0, fRedneckHeading1)
					ELIF NOT IS_SPHERE_VISIBLE(vRedneckCoords2, 2)
						pedRedneck0 = CREATE_PED(PEDTYPE_DEALER, modelAttackingPed, vRedneckCoords2 - << 0, 0, 1 >>, GET_HEADING_BETWEEN_VECTORS_2D(vRedneckCoords2, GET_PLAYER_COORDS(PLAYER_ID())))
					ELIF NOT IS_SPHERE_VISIBLE(vRedneckCoords3, 2)
						pedRedneck0 = CREATE_PED(PEDTYPE_DEALER, modelAttackingPed, vRedneckCoords3 - << 0, 0, 1 >>, GET_HEADING_BETWEEN_VECTORS_2D(vRedneckCoords3, GET_PLAYER_COORDS(PLAYER_ID())))
					ENDIF
					
					IF NOT IS_PED_INJURED(pedRedneck0)
						SET_AMBIENT_VOICE_NAME(pedRedneck0, "A_M_M_HillBilly_01_WHITE_MINI_02")
//						SET_MODEL_AS_NO_LONGER_NEEDED(modelAttackingPed) // Needed for temp ped on the train
//						SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(pedRedneck0, PLAYER_PED_ID(), << 0, 0, 0 >>, 5)
						GIVE_WEAPON_TO_PED(pedRedneck0, WEAPONTYPE_STUNGUN, INFINITE_AMMO, TRUE)
							SET_PED_SHOOT_RATE(pedRedneck0, 100)
		//					SET_PED_DUCKING(pedRedneck0, TRUE)
						SET_PED_CONFIG_FLAG(pedRedneck0, PCF_RemoveDeadExtraFarAway, TRUE)
						SET_PED_CONFIG_FLAG(pedRedneck0, PCF_DisableHurt, TRUE)
						SET_PED_CONFIG_FLAG(pedRedneck0, PCF_ShouldChargeNow, TRUE)SET_PED_MIN_GROUND_TIME_FOR_STUNGUN(PLAYER_PED_ID(), 10000)
						SET_PED_COMBAT_ATTRIBUTES(pedRedneck0, CA_USE_COVER, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedRedneck0, CA_PERFECT_ACCURACY, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedRedneck0, CA_AGGRESSIVE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedRedneck0, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_MOVEMENT(pedRedneck0, CM_WILLADVANCE)
						SET_PED_COMBAT_ABILITY(pedRedneck0, CAL_PROFESSIONAL)
						SET_PED_COMBAT_RANGE(pedRedneck0, CR_NEAR)
						SET_COMBAT_FLOAT(pedRedneck0, CCF_MAX_SHOOTING_DISTANCE, 11)
						SET_COMBAT_FLOAT(pedRedneck0, CCF_MIN_DISTANCE_TO_TARGET, 1)
//						SET_PED_ACCURACY(pedRedneck0, 100)
						
						STOP_PED_SPEAKING(pedRedneck0, TRUE)
						//pedRedneck1  = CREATE_PED(PEDTYPE_DEALER,modelAttackingPed, coords_crazed2_killer, heading_crazed2_killer)
						//SET_PED_DUCKING(pedRedneck1, TRUE)
						//SET_PED_RELATIONSHIP_GROUP_HASH(pedRedneck1, rghRedneck)
						
						//SET_BLIP_AS_FRIENDLY(blipInitialBlip, TRUE)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedRedneck0, rghRedneck)
						
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, pedRedneck0, "RAPIST")
						
					//	REMOVE_BLIP(blip_abandoned_vehicle)
						
						CLEAR_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID(), -8)
		//					SET_PED_DUCKING(pedRedneck0, FALSE)
						
						//SET_PED_DUCKING(pedRedneck1, FALSE)
						
						
						//GIVE_WEAPON_TO_PED(pedRedneck1, WEAPONTYPE_PISTOL,200, TRUE)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRedneck0, TRUE)
						IF NOT DOES_BLIP_EXIST(blipRedneck0)
							blipRedneck0 = CREATE_BLIP_FOR_PED(pedRedneck0, TRUE)
						ENDIF
						
						REMOVE_BLIP(blipInitialBlip)
						OPEN_SEQUENCE_TASK(seq)
							//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_DEFAULT)
			//						TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 8, PEDMOVEBLENDRATIO_RUN)
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_SPRINT, TRUE, 3)
							//TASK_AIM_GUN_AT_ENTITY(NULL,PLAYER_PED_ID(), 1000)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							//TASK_PLAY_ANIM_ADVANCED (NULL, "amb@pim_prover_1", "m_argue01", v_spawnpoint, << -1.519, 0.000, -0.000 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
							//TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedRedneck0, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedRedneck0, TRUE)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRedneck0)
						bPlayerAttacked = TRUE
					ENDIF
					
				ELIF thisEvent = eV_INCEST_BROTHERS
					bPlayerInteractingWithEvent = TRUE
					TRIGGER_MUSIC_EVENT(sMusicEvent)
					IF DOES_BLIP_EXIST(blipInitialBlip)
						REMOVE_BLIP(blipInitialBlip)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedRedneck0)
						IF NOT DOES_BLIP_EXIST(blipRedneck0)
							blipRedneck0 = CREATE_BLIP_FOR_PED(pedRedneck0, TRUE)
						ENDIF
						OPEN_SEQUENCE_TASK(seq)
							#IF NOT IS_JAPANESE_BUILD
								TASK_PLAY_ANIM(NULL, "random@train_tracks", "frontseat_carsex_outro_low_guy")
							#ENDIF
//							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_DEFAULT)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedRedneck0, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedRedneck0, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedRedneck1)
						blipRedneck1 = CREATE_BLIP_FOR_PED(pedRedneck1, TRUE)
						SET_ENTITY_HEALTH(pedRedneck1, 200)
						OPEN_SEQUENCE_TASK(seq)
							#IF NOT IS_JAPANESE_BUILD
								TASK_PLAY_ANIM(NULL, "random@train_tracks", "frontseat_carsex_outro_top_guy")
							#ENDIF
							IF NOT IS_PED_INJURED(pedRedneck0)
								TASK_PAUSE(NULL, 400)
							ENDIF
//							TASK_LEAVE_ANY_VEHICLE(NULL, 400)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_DEFAULT)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedRedneck1, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedRedneck1, TRUE)
					ENDIF
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF NOT IS_PED_INJURED(pedRedneck0)
					AND NOT IS_PED_INJURED(pedRedneck1)
						CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_attac1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bPlayerAttacked = TRUE
				ENDIF
				
			/*	OPEN_SEQUENCE_TASK(seq)
						//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 600000,SLF_DEFAULT)
					TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,PLAYER_PED_ID(), PLAYER_PED_ID(),PEDMOVE_RUN, FALSE, 3.9 )
					TASK_AIM_GUN_AT_ENTITY(NULL,PLAYER_PED_ID(), 600000)
					//TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						//TASK_PLAY_ANIM_ADVANCED (NULL, "amb@pim_prover_1", "m_argue01", v_spawnpoint, << -1.519, 0.000, -0.000 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
						//TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				CLOSE_SEQUENCE_TASK(seq)
				*/
				//TASK_PERFORM_SEQUENCE(pedRedneck1, seq)
				
	//				CLEAR_SEQUENCE_TASK(seq)
				
			//	PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, vehAbandonedVehicle, "re_acau", "lurin_mugger")
			ENDIF
			/*
			IF make_goon_approach = FALSE
			
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),vehAbandonedVehicle,<<10.0, 10.0, 10.0>>, FALSE )
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_GO_TO_ENTITY(vehAbandonedVehicle,PLAYER_PED_ID(),-1, 4.0)
					make_goon_approach = TRUE
				ENDIF
			ENDIF*/
		ENDIF
	ENDIF
	
ENDPROC
//
//PROC make_goons_open_fire(PED_INDEX& ped)
//
//	IF NOT IS_PED_INJURED(ped)
//	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),ped,<<4.0, 4.0,4.0>>, FALSE )
//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, PLAYER_PED_ID())
//		OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped)
//		OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped)
//		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped)
//		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped)
////			bRedneckFiringAtPlayer = TRUE
//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghRedneck, RELGROUPHASH_PLAYER)
//		//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "We just wanted your money, but you had to be a hero. Die asshole!", 4000, 1) //	
//			//ELECTROCUTE_PED()
//			
//			IF NOT bRapistBlipped
//				IF NOT DOES_BLIP_EXIST(blipRedneck0)
//					blipRedneck0 = CREATE_BLIP_FOR_PED(pedRedneck0, TRUE)
//				ENDIF
//			//	blipRedneck1 = ADD_BLIP_FOR_ENTITY(pedRedneck1)
//				REMOVE_BLIP(blipInitialBlip)
//				bRapistBlipped = TRUE
//				KILL_ANY_CONVERSATION()
//				WAIT(0)
//				CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_attac1", CONV_PRIORITY_AMBIENT_HIGH)
//			ENDIF
//			
//			//NM
//		ENDIF
//	ENDIF
//	
//ENDPROC

PROC checks_before_player_passes_out()

	IF NOT bPlayerPassedOut
		IF NOT IS_PED_INJURED(pedRedneck0)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceAimAtHead, TRUE)
			IF NOT bPlayerInRagdoll
				//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(player_ped_ID(), pedRedneck0, FALSE)
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
				IF IS_PED_BEING_STUNNED(PLAYER_PED_ID())
	//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//					iCounterBeforePlayerPassesOut ++
	//				ENDIF
					bPlayerInRagdoll = TRUE
					iCounterBeforePlayerPassesOut ++
					PRINTSTRING("\n@@@@@@@@@@@ iCounterBeforePlayerPassesOut: ")PRINTINT(iCounterBeforePlayerPassesOut)PRINTSTRING(" @@@@@@@@@@@\n")
	//				TASK_PAUSE(pedRedneck0, 0)
//					SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 10000, 10000, TASK_RELAX)
	//				bTimerSet = FALSE
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_rambl", CONV_PRIORITY_AMBIENT_HIGH)
					SETTIMERA(0)
				ENDIF
			ELSE
	//			IF NOT IS_PED_BEING_STUNNED(PLAYER_PED_ID())
	//				IF NOT bTimerSet
	//					SETTIMERA(0)
	//					bTimerSet = TRUE
	//				ENDIF
					PRINTSTRING("\n @@@@@@@@@@@@@@ TAZE 4000 @@@@@@@@@@@@@@ \n")
					IF TIMERA() > 4000
						IF NOT IS_PED_INJURED(pedRedneck0)
							TASK_COMBAT_PED(pedRedneck0, PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedRedneck0, TRUE)
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
						WAIT(3000)
						bPlayerInRagdoll = FALSE
					ENDIF
	//			ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedRedneck0)
				IF iCounterBeforePlayerPassesOut >= 1 //2
				AND IS_PED_BEING_STUNNED(PLAYER_PED_ID())
				AND bPlayerInRagdoll
					// make player black out
					TASK_PLAY_ANIM(pedRedneck0, "amb@world_human_guard_patrol@male@idle_b", "idle_e", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					TRIGGER_MUSIC_EVENT(sMusicEvent)
					SHAKE_GAMEPLAY_CAM("DEATH_FAIL_IN_EFFECT_SHAKE", 1.0)
					ANIMPOSTFX_PLAY("Dont_tazeme_bro", 0, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON|SPC_ALLOW_PLAYER_DAMAGE)
					bPlayerPassedOut = TRUE
				ENDIF
			ENDIF
//		DISABLE_CELLPHONE_INTERNET_APP_THIS_FRAME_ONLY()
//		IF NOT IS_ENTITY_DEAD(vehAbandonedVehicle)
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehAbandonedVehicle)
//				BRING_VEHICLE_TO_HALT(vehAbandonedVehicle, 0, -1)
//			ENDIF
//		ENDIF
		ENDIF
	ELSE
		SET_DISABLE_RANDOM_TRAINS_THIS_FRAME(TRUE)
		SWITCH iEndProgress
		
			CASE 0
				IF DOES_BLIP_EXIST(blipRedneck0)
					IF IS_PED_INJURED(pedRedneck0)
						REMOVE_BLIP(blipRedneck0)
					ENDIF
				ENDIF
				
				WAIT(2000)
				
				iEndProgress ++
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(pedRedneck0)
					KILL_FACE_TO_FACE_CONVERSATION()
					streaming_volume = STREAMVOL_CREATE_SPHERE(<< 923.5159, 3203.3860, 40.1730 >>, 30, FLAG_COLLISIONS_MOVER|FLAG_MAPDATA)
	//				SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 10000, 10000, TASK_RELAX)
	//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON|SPC_ALLOW_PLAYER_DAMAGE)
					DISABLE_CELLPHONE(TRUE)
					DO_SCREEN_FADE_OUT(500)
					WAIT(2000)
					IF DOES_ENTITY_EXIST(pedRedneck0)
						SET_PED_AS_NO_LONGER_NEEDED(pedRedneck0)
					ENDIF
					iEndProgress ++
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
			BREAK
			
			CASE 2
				ANIMPOSTFX_STOP("Dont_tazeme_bro")
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BARECHEST_BOXERS, FALSE)
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 26, 0, 0) //uppr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 18, 0, 0)//18, 0, 0) //lowr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 1, 0, 0) //feet
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_UNDERWEAR, FALSE)
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 26, 0, 0) //uppr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 18, 0, 0) //lowr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 5, 0, 0) //feet
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_UNDERWEAR, FALSE)
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 16, 0, 0) //uppr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 26, 0, 0) //lowr
//					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 1, 0, 0) //feet
				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 923.5159, 3203.3860, 40.1730 >>, TRUE, FALSE)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 280.1253) //272.7761)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-5.2078)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.2963)
//				SET_ENTITY_ROTATION(PLAYER_PED_ID(), << 0.000, 0.000, -79.200 >>)
				
				IF DOES_BLIP_EXIST(blipRedneck0)
					REMOVE_BLIP(blipRedneck0)
				ENDIF
				
				REQUEST_MODEL(FREIGHT)
				REQUEST_MODEL(FREIGHTCAR)
				REQUEST_MODEL(FREIGHTGRAIN)
				REQUEST_MODEL(FREIGHTCONT1)
		//			REQUEST_MODEL(FREIGHTCONT2)
		//			REQUEST_MODEL(FREIGHTCRATE)
		//			REQUEST_MODEL(TANKERCAR)
		//			REQUEST_MODEL(METROTRAIN)
				
				WHILE NOT HAS_MODEL_LOADED(FREIGHT)
				OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
				OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
				OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
		//			OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
		//			OR NOT HAS_MODEL_LOADED(FREIGHTCRATE)
		//			OR NOT HAS_MODEL_LOADED(TANKERCAR)
		//			OR NOT HAS_MODEL_LOADED(METROTRAIN)
					WAIT(0)
				ENDWHILE
				
				DELETE_ALL_TRAINS()
				vehTrain = CREATE_MISSION_TRAIN(0, << 1063.5955, 3227.5706, 39.3899 >>, TRUE)
				PED_INDEX pedTrain
				pedTrain = CREATE_PED_INSIDE_VEHICLE(vehTrain, PEDTYPE_MISSION, modelAttackingPed)
				pedTrain = pedTrain
//				START_VEHICLE_HORN(vehTrain, 10000)
				IF NOT bPlayedTrainHorn
					IF REQUEST_SCRIPT_AUDIO_BANK("Train_Horn")
						PLAY_SOUND_FROM_ENTITY(-1, "Warning_Once", vehTrain, "TRAIN_HORN")
//						bPlayedTrainHorn = TRUE
					ENDIF
				ENDIF
				
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "random@train_tracks", "on_back_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
		//			WHILE NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up_back", "get_up_slow")
		//				WAIT(0)
		//			ENDWHILE
				ENDIF
				WAIT(100)
				SETTIMERA(0)
				
				iEndProgress ++
			BREAK
			
			CASE 3
				STREAMVOL_DELETE(streaming_volume)
//				STOP_GAMEPLAY_CAM_SHAKING(TRUE)
				DO_SCREEN_FADE_IN(3000)
				SET_TIMECYCLE_MODIFIER("Drug_deadman")
				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(30)
				ADD_TO_CLOCK_TIME(8, 0, 0)
				ADVANCE_FRIEND_TIMERS(8.0)
		//		WAIT(4000)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				iEndProgress ++
			BREAK
			
			CASE 4
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@train_tracks", "on_back_c")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@train_tracks", "on_back_c") > 0.95
						DISABLE_CELLPHONE(FALSE)
						IF IS_PLAYER_MOVING_LEFT_STICK()
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@train_tracks", "on_back_c") < 0.1
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-5.2078)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.2963)
					ENDIF
				ENDIF
				IF TIMERA() > 2000
				AND TIMERA() < 3000
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_awakeM", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_awakeF", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_awakeT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
//				IF TIMERA() > 1500
//				AND TIMERA() < 5000
					IF NOT IS_ENTITY_DEAD(vehTrain)
//						IF NOT bPlayedTrainHorn
							IF REQUEST_SCRIPT_AUDIO_BANK("Train_Horn")
								PLAY_SOUND_FROM_ENTITY(-1, "Warning_Once", vehTrain, "TRAIN_HORN")
								bPlayedTrainHorn = TRUE
							ENDIF
//						ENDIF
					ENDIF
//				ENDIF
				IF TIMERA() > 10000
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						MISSION_PASSED()
					ELSE
						MISSION_FAILED()
						IF thisEvent = eV_TAZER_RAPIST
							REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ABCAR1)
						ELIF thisEvent = eV_INCEST_BROTHERS
							REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ABCAR2)
						ENDIF
						RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC checks_if_player_aims_at_rapist()
	
	IF NOT IS_PED_INJURED(pedRedneck0)
		IF thisEvent = eV_TAZER_RAPIST
			IF NOT IS_PED_BEING_STUNNED(PLAYER_PED_ID())
		//		AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
				IF NOT IS_ENTITY_AT_ENTITY(pedRedneck0, PLAYER_PED_ID(), << 8, 8, 8 >>)
				OR NOT IS_ENTITY_AT_COORD(pedRedneck0, vVehicleCoords, << 215, 215, 215 >>)
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRedneck0)
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRedneck0)
					OR NOT IS_ENTITY_AT_COORD(pedRedneck0, vVehicleCoords, << 215, 215, 215 >>)
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						OR NOT IS_ENTITY_AT_COORD(pedRedneck0, vVehicleCoords, << 215, 215, 215 >>)
							TASK_SMART_FLEE_PED(pedRedneck0, PLAYER_PED_ID(), 1000, -1)
							SET_PED_KEEP_TASK(pedRedneck0, TRUE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_aim", CONV_PRIORITY_AMBIENT_HIGH)
							MISSION_PASSED()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), ANNIHILATOR)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), BUZZARD)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), LAZER)
					MISSION_PASSED()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC isPlayerMessingWithAbandonedVehicle()

	IF NOT IS_ENTITY_DEAD(vehAbandonedVehicle)
		IF thisEvent = eV_TAZER_RAPIST
			IF NOT IS_ENTITY_AT_COORD(vehAbandonedVehicle, vVehicleCoords, << 15, 15, 15 >>)
				MISSION_FAILED()
			ENDIF
		ELIF thisEvent = eV_INCEST_BROTHERS
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehAbandonedVehicle, PLAYER_PED_ID())
			OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>, FALSE)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR IS_COP_PED_IN_AREA_3D(vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR IS_COP_VEHICLE_IN_AREA_3D(vVehicleCoords - << 50, 50, 50 >>, vVehicleCoords + << 50, 50, 50 >>)
			OR (IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehAbandonedVehicle, << 50, 50, 50 >>, FALSE, TRUE, TM_IN_VEHICLE) AND IS_PLAYER_PRESSING_HORN(PLAYER_ID()))
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					vehImpound = GET_PLAYERS_LAST_VEHICLE()
				ENDIF
				SET_RANDOM_EVENT_ACTIVE()
				DISABLE_TAXI_HAILING(TRUE)
				bPlayerInteractingWithEvent = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC make_goon_beat_up_player(PED_INDEX& ped)
	
	IF NOT IS_PED_INJURED(Ped)
		IF GET_CURRENT_PED_WEAPON(Ped, CurrentWeapon)
			IF CurrentWeapon = WEAPONTYPE_STUNGUN
//				coord_temp_player = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//			
//				SET_PED_SPHERE_DEFENSIVE_AREA(Ped, coord_temp_player, 5, TRUE) // make stun gun man stay close to player in combat.
				/*					
				IF GET_SCRIPT_TASK_STATUS(ped,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				//OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
				// if is player getting electricuted make him go unarmed 
					//SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_UNARMED)
					PRINTSTRING("\n goon is shooting \n")
					
					OPEN_SEQUENCE_TASK(seq)
					//	TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_COMBAT_PED_TIMED(NULL, PLAYER_PED_ID(),5000)
						TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 8000)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRedneck0,TRUE)
					
					IF bRapistBlipped = FALSE
						blipRedneck0 = ADD_BLIP_FOR_ENTITY(Ped)
					//	blipRedneck1 = ADD_BLIP_FOR_ENTITY(pedRedneck1)
						REMOVE_BLIP (blipInitialBlip)
						bRapistBlipped = TRUE
						KILL_ANY_CONVERSATION()
						WAIT(0)
						PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, Ped, "re_acau", "re_ac_attac1")
					ELSE
						PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped, "re_acau", "re_ac_rambl")
					ENDIF
					
					
					
				ENDIF
				
				
					IF IS_PED_RAGDOLL(PLAYER_PED_ID())
						IF f_is_ped_in_rag_doll = FALSE
							f_is_ped_in_rag_doll = TRUE
							CLEAR_PED_TASKS(ped)
							TASK_AIM_GUN_AT_ENTITY(ped, PLAYER_PED_ID(), 12000)
						ENDIF
					ELSE
						IF f_is_ped_in_rag_doll = TRUE
							f_is_ped_in_rag_doll = FALSE
						ENDIF
					ENDIF
				*/
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC removeEnemyBlips()

	IF DOES_BLIP_EXIST(blipRedneck0)
		IF IS_PED_INJURED(pedRedneck0)
			REMOVE_BLIP(blipRedneck0)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipRedneck1)
		IF IS_PED_INJURED(pedRedneck1)
			REMOVE_BLIP(blipRedneck1)
		ENDIF
	ENDIF
	/* 
	IF DOES_BLIP_EXIST(blip_mugger_2)
		IF IS_ENTITY_DEAD(ped_mugger_2)
			REMOVE_BLIP(blip_mugger_2)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blip_mugger_3)
		IF IS_ENTITY_DEAD(ped_mugger_3)
			REMOVE_BLIP(blip_mugger_3)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blip_mugger_4)
		IF IS_ENTITY_DEAD(ped_mugger_4)
			REMOVE_BLIP(blip_mugger_4)
		ENDIF
	ENDIF
	*/
	
ENDPROC

PROC checks_to_end_mission()

	IF IS_PED_INJURED(pedRedneck0)
	AND IS_PED_INJURED(pedRedneck1)
	AND NOT bPlayerPassedOut
		MISSION_PASSED()
	ENDIF
	
	IF thisEvent = eV_TAZER_RAPIST
		IF NOT bPlayerPassedOut
			IF NOT IS_PED_INJURED(pedRedneck0)
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 50, 50, 50 >>)
					MISSION_PASSED()
				ENDIF
			ENDIF
		ENDIF
	ELIF thisEvent = eV_INCEST_BROTHERS
		IF NOT IS_PED_INJURED(pedRedneck0)
		AND NOT IS_PED_INJURED(pedRedneck1)
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 200, 200, 200 >>)
			AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedRedneck0, << 200, 200, 200 >>)
				MISSION_PASSED()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

	v_spawnpoint = in_coords.vec_coord[0]
	PRINTVECTOR(v_spawnpoint)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("Abandoned Vehicle")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	
//	random_int = GET_RANDOM_INT_IN_RANGE(0,2)
	getWorldPoint()
	
	IF GET_CLOCK_HOURS() > 7
	AND GET_CLOCK_HOURS() < 19
		CPRINTLN(DEBUG_RANDOM_EVENTS, "Random event Abandoned vehicle only runs between 7pm and 7am \n ")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF CAN_RANDOM_EVENT_LAUNCH(v_spawnpoint, RE_ABANDONEDCAR, iSelectedVariation)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)		
		PRINTSTRING("\n Forced clean up \n ")
		missionCleanup()
	ENDIF
		
	WHILE TRUE
		WAIT(0)
				
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		OR bPlayerAttacked
		
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_AV")
			
//			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				SWITCH ambStage
				
					CASE ambCanRun
						IF bAssetsLoaded
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ELSE
							IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
								missionCleanup()
							ENDIF
							IF NOT bVariablesInitialised
								initialiseEventVariables()
							ELSE
								create_assets()
							ENDIF
						ENDIF
					BREAK
					
					CASE ambRunning
						//DO STUFF
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
						IF NOT IS_ENTITY_DEAD(pedRedneck0)
							IF IS_PED_IN_ANY_VEHICLE(pedRedneck0)
								REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							ENDIF
						ENDIF
						IF NOT IS_ENTITY_DEAD(pedRedneck1)
							IF IS_PED_IN_ANY_VEHICLE(pedRedneck1)
								REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							ENDIF
						ENDIF
						
						IF NOT bPlayerInteractingWithEvent
							isPlayerMessingWithAbandonedVehicle()
							isPlayerApproachingOrInteractingWithEvent()
						ENDIF
						
						IF NOT bPlayerAttacked
							IF NOT IS_ENTITY_DEAD(vehAbandonedVehicle)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehAbandonedVehicle)) < iApproachPlayerDialogue
								AND NOT IS_ENTITY_OCCLUDED(vehAbandonedVehicle)
									IF NOT bApproachDialoguePlayed
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										WAIT(0)
										IF thisEvent = eV_TAZER_RAPIST
											IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
												CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_approM", CONV_PRIORITY_AMBIENT_HIGH)
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
												CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_approF", CONV_PRIORITY_AMBIENT_HIGH)
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
												CREATE_CONVERSATION(cultresConversation, "re_acau", "re_ac_approT", CONV_PRIORITY_AMBIENT_HIGH)
											ENDIF
										ELIF thisEvent = eV_INCEST_BROTHERS
											IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
												CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_approM", CONV_PRIORITY_AMBIENT_HIGH)
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
												CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_approF", CONV_PRIORITY_AMBIENT_HIGH)
											ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
												CREATE_CONVERSATION(cultresConversation, "reac2au", "reac2_approT", CONV_PRIORITY_AMBIENT_HIGH)
											ENDIF
										ENDIF
										IF DOES_BLIP_EXIST(blipInitialBlip)
											SHOW_HEIGHT_ON_BLIP(blipInitialBlip, TRUE)
										ENDIF
										bApproachDialoguePlayed = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF thisEvent = eV_TAZER_RAPIST
								REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220780
							ENDIF
							
							makeRednecksAttack()
							IF thisEvent = eV_INCEST_BROTHERS
								sexTalkAndBounce()
							ENDIF
						ELSE
							removeEnemyBlips()
							IF NOT bRedneckFiringAtPlayer
								IF NOT IS_ENTITY_DEAD(vehAbandonedVehicle)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehAbandonedVehicle)
	//									SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()) + << 0.05, 0.05, 0.05 >>, GET_PLAYER_COORDS(PLAYER_ID()), 0, TRUE, WEAPONTYPE_STUNGUN)
										IF NOT IS_PED_INJURED(pedRedneck0)
											OPEN_SEQUENCE_TASK(seq)
												TASK_ENTER_VEHICLE(NULL, vehAbandonedVehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JUST_PULL_PED_OUT|ECF_JACK_ANYONE)
												TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(pedRedneck0, seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_KEEP_TASK(pedRedneck0, TRUE)
										ENDIF
										bRedneckFiringAtPlayer = TRUE
									ENDIF
								ENDIF
								//make_goons_open_fire(pedRedneck1)
//								make_goons_open_fire(pedRedneck0)
							ENDIF
							interruptions_to_conversation()
							checks_if_player_aims_at_rapist()
							
							checks_to_end_mission()
							
							IF thisEvent = eV_TAZER_RAPIST
								checks_before_player_passes_out()
								make_girl_talk()
							ENDIF
						ENDIF
						
				//		make_car_lights_flash(vehAbandonedVehicle)
//						make_goon_beat_up_player(pedRedneck0)
						
						debug_stuff()
					BREAK
					
				ENDSWITCH
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
