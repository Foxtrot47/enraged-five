//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "Ambient_approach.sch" 
USING "script_blips.sch"
USING "dialogue_public.sch"
USING "Ambient_Speech.sch" 
USING "cutscene_public.sch"
USING "AggroSupport.sch"

USING "completionpercentage_public.sch"
USING "ambient_common.sch"
USING "script_heist.sch"
USING "random_events_public.sch"
USING "rc_helper_functions.sch"

// Variables ----------------------------------------------//
ENUM ARREST_STAGES
	initialiseEvent,
	stageWaitForInteraction,
	STAGE_RUN_ARREST,
	STAGE_CLEAN_UP
ENDENUM
ARREST_STAGES ArrestStage = initialiseEvent

ENUM CHECK_INJURED_STATE
	IGNORED_BOTH,
	ATTACKED_BOTH,
	JUST_INJURED_THE_COP,
	JUST_INJURED_THE_CRIM
ENDENUM
CHECK_INJURED_STATE stage_check_injured_state = IGNORED_BOTH

ENUM copCallInStageFlag
	copTakesOutRadio,
	copRequestsUnit,
	copFinishesRequest,
	copWaitsForRequestedUnit
ENDENUM
copCallInStageFlag copCallInStage = copTakesOutRadio

ENUM copAttendsDeadCriminalStageFlag
	copThanksPlayer,
	copGivesCPR,
	copWaitsForAmbulance,
	copLeavesWithBackup
ENDENUM
copAttendsDeadCriminalStageFlag copAttendsDeadCriminalStage = copThanksPlayer

ENUM copCatchesCriminalStageFlag
	copChasesCriminal,
	copStandStill,
	pedWaitUntilComeToAHalt,
	pedSlowDownUntilComeToAHalt,
	pedStandsUpWithHandsUp,
	copWaitForEndOfCall,
	copWaitsForBackup,
	getToCopcar,
	copLeaves
ENDENUM
copCatchesCriminalStageFlag copCatchesCriminalStage = copChasesCriminal

#IF IS_DEBUG_BUILD //WIDGETS

	BOOL bForceArrest
	
#ENDIF

INT iCurrentArrest
CONST_INT ARREST_1	1
CONST_INT ARREST_2	2

PED_INDEX pedCop, pedCriminal, pedCopGenerated
VEHICLE_INDEX vehCopCarGenerated
BLIP_INDEX blipCop, blipCriminal
OBJECT_INDEX objBinbag


BOOL cop_attacks_player, bCPRStarted, bCrimHasStartedWalking, bCopHasOpenedDoor
VECTOR vInput, vArrestedPos


REL_GROUP_HASH relGrouparrestcop, relGrouparrestcrim
SEQUENCE_INDEX seq


BOOL bCopHasArrestedCriminal, criminal_turned_on_cop, bForceCleanupOccurred, bCopCarGenerated
structPedsForConversation arrestConversation				
INT cop_thanks_stage, random_speech_wait = 8000
INT time_out_criminal_stage, iArrestTimer
INT criminal_thanks_stage = 8 //This is the new start of the criminal thanks stage.

//INCIDENT_INDEX ambulance_arrives
BOOL bCriminalBlip, bCopBlip = TRUE

NAVDATA someNavData
INT iControlTimer

enumSubtitlesState subtitleState = DISPLAY_SUBTITLES
INT iSubTimer
STRING sVoiceNameCrim 
STRING sVoiceNameCop = "S_M_Y_RANGER_01_WHITE_FULL_01"
STRING arrestAnimDict = "RANDOM@ARRESTS"
STRING arrestAnimBanterDict = "RANDOM@ARRESTS@BUSTED"
STRING randomBanterIdle
BOOL bCopRandomBanter, bStartedBanter, bCanPlayCopBanter = TRUE
INT iBanterTimer

STRING sVoiceIdCrim
STRING crimThank
STRING crimOutRun 
STRING crimWTF 
STRING crimTurn 
STRING crimHelp 
STRING crimHate 
STRING crimRand
STRING crimQuickThanks
STRING crimAnnoyedThanks

INT touchingCount = 0
INT touchingTime = 0

INT arrestCriminalTalkTime

BOOL arrestedWaiting = FALSE
//BOOL sayHandsUp = FALSE
// Functions ----------------------------------------------//
VECTOR a1, a2
BOOL navMeshWalkingDone = FALSE

BOOL givenUpSpeech = FALSE
BOOL delayWanted = FALSE

PROC SETUP_RELATIONSHIPS()

	someNavData.m_fSlideToCoordHeading = 0.0
	someNavData.m_fMaxSlopeNavigable = 10.0

	SET_PED_COMBAT_MOVEMENT(pedCop, CM_WILLADVANCE )
	SET_PED_COMBAT_ATTRIBUTES(pedCop, CA_AGGRESSIVE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCop, CA_DO_DRIVEBYS, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCop, CA_USE_VEHICLE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCop, CA_JUST_SEEK_COVER, FALSE)
	SET_PED_COMBAT_RANGE (pedCop, CR_NEAR)
	
	SET_PED_COMBAT_MOVEMENT (pedCriminal, CM_WILLRETREAT)
	SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_ALWAYS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_USE_VEHICLE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_JUST_SEEK_COVER, FALSE)
	SET_PED_FLEE_ATTRIBUTES(pedCriminal, FA_DISABLE_HANDS_UP, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedCriminal, FA_USE_COVER, FALSE)
	
	ADD_RELATIONSHIP_GROUP("RE_ARREST_COP", relGrouparrestcop)
	ADD_RELATIONSHIP_GROUP("RE_ARREST_CRIM", relGrouparrestcrim)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedCop, relGrouparrestcop)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedCriminal, relGrouparrestcrim)

	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_WANTED, relGrouparrestcrim, relGrouparrestcop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relGrouparrestcop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relGrouparrestcrim, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_WANTED, relGrouparrestcrim, RELGROUPHASH_COP)
	
	SET_AMBIENT_VOICE_NAME(pedCop, sVoiceNameCop)
	SET_AMBIENT_VOICE_NAME(pedCriminal, sVoiceNameCrim)
	
ENDPROC

FUNC BOOL HAS_AMBIENT_EVENT_LOADED()

	MODEL_NAMES modelCop = S_M_Y_RANGER_01
	MODEL_NAMES modelCriminal
	VECTOR vCopSpawn, vCrimSpawn
	FLOAT fCopSpawn, fCrimSpawn
	IF iCurrentArrest = ARREST_1
		vCopSpawn	= <<2429.8220, 4952.9380, 44.8892>>
		fCopSpawn 	= 218.3434
		vCrimSpawn	= <<2431.6851, 4945.3101, 44.6996>>
		fCrimSpawn	= 220.0087
		
		a1 = <<2378.284424,4863.026855,32.508835>>
		a2 = <<2512.797363,4998.586426,90.122833>>
	
	
		sVoiceNameCrim = "A_M_M_HillBilly_02_WHITE_MINI_02"
		sVoiceIdCrim = "arrestcriminal"
		
		//List of different text convos that vary between 1 and 2....
		//REARR_THANK2
		//REARR_CORUN2
		//REARR_CWTF2
		//REARR_CCUFF2 not used.
		//REARR_CTURN2
		//REARR_CHELP2
		//REARR_HATE2
		//REARR_CRAND2
		
		crimThank = "REARR_CTHANK"
		crimOutRun = "REARR_CORUN"
		crimWTF = "REARR_CWTF"
		crimTurn = "REARR_CTURN"
		crimHelp = "REARR_CHELP"
		crimHate = "REARR_HATE"
		crimRand = "REARR_CRAND"
		
		
		//crimQuickThanks = "REARR_CQTHA1"
		//crimAnnoyedThanks = "REARR_CTHAN2"
		crimQuickThanks = "REARR_CRAND"
		crimAnnoyedThanks = "REARR_CRAND"
		
		modelCriminal = A_M_M_HillBilly_02
		
	ELIF iCurrentArrest = ARREST_2
		vCopSpawn	= << 2592.6370, 2540.0503, 30.4162 >>
		fCopSpawn 	= 90.5639
		vCrimSpawn	= << 2586.0051, 2541.4102, 31.0379 >>
		fCrimSpawn	= 75.5943  
		
		
		a1 = <<2543.134521,2683.917236,0.307461>>
		a2 = <<2597.602539,2408.885010,121.901031>>
	
		sVoiceNameCrim = "G_M_M_ArmGoon_01_White_Armenian_MINI_01"
		sVoiceIdCrim = "arrestcriminal2"
		
		crimThank = "REARR_THANK2"
		crimOutRun = "REARR_CORUN2"
		crimWTF = "REARR_CWTF2"
		crimTurn = "REARR_CTURN2"
		crimHelp = "REARR_CHELP2"
		crimHate = "REARR_HATE2"
		crimRand = "REARR_CRAND2"
		
		//crimQuickThanks = "REARR_CQTHA2"
		//crimAnnoyedThanks = "REARR_CTHAN2"
		crimQuickThanks = "REARR_CRAND2"
		crimAnnoyedThanks = "REARR_CRAND2"
		
		modelCriminal = G_M_M_ArmGoon_01
	ENDIF

	REQUEST_MODEL(modelCop)
	REQUEST_MODEL(modelCriminal)
	REQUEST_MODEL(PROP_LD_BINBAG_01)
	REQUEST_ANIM_DICT(arrestAnimDict)
	
	REQUEST_ANIM_DICT(arrestAnimBanterDict)
	REQUEST_CLIP_SET("MOVE_M@BAIL_BOND_NOT_TAZERED")
	REQUEST_CLIP_SET("MOVE_M@BAIL_BOND_TAZERED")
	
								
	IF HAS_MODEL_LOADED(modelCop)
	AND HAS_MODEL_LOADED(modelCriminal)
	AND HAS_MODEL_LOADED(PROP_LD_BINBAG_01)
	AND HAS_ANIM_DICT_LOADED(arrestAnimDict)
	AND HAS_ANIM_DICT_LOADED(arrestAnimBanterDict)
	AND HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_NOT_TAZERED")
	AND HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_TAZERED")
		pedCop = CREATE_PED(PEDTYPE_COP, modelCop, vCopSpawn, fCopSpawn)
		SET_PED_CONFIG_FLAG(pedCop, PCF_DisableShockingEvents, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(pedCop, TRUE)
		REMOVE_ALL_PED_WEAPONS(pedCop)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop, TRUE)
		ADD_PED_FOR_DIALOGUE(arrestConversation, 1, pedCop, "ArrestCop")

		SET_ENTITY_LOAD_COLLISION_FLAG(pedCop, TRUE)
		SET_PED_TARGET_LOSS_RESPONSE(pedCop, TLR_NEVER_LOSE_TARGET)  
		
		pedCriminal = CREATE_PED(PEDTYPE_MISSION, modelCriminal, vCrimSpawn, fCrimSpawn)
		
		SET_ENTITY_IS_TARGET_PRIORITY(pedCriminal, TRUE)
		SET_PED_CONFIG_FLAG(pedCriminal, PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_CONFIG_FLAG(pedCriminal, PCF_DisableGoToWritheWhenInjured, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCriminal, TRUE)
		ADD_PED_FOR_DIALOGUE(arrestConversation, 2, pedCriminal, sVoiceIdCrim)
		
		SET_ENTITY_LOAD_COLLISION_FLAG(pedCriminal, TRUE)
		
		
		
		
		IF iCurrentArrest = ARREST_1
			SET_PED_COMPONENT_VARIATION(pedCriminal, PED_COMP_HEAD, 		0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedCriminal, PED_COMP_HAIR, 		0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedCriminal, PED_COMP_TORSO, 		2, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedCriminal, PED_COMP_LEG, 			0, 0, 0) //(lowr)
		ENDIF
		
		SETUP_RELATIONSHIPS()

		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(arrestConversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(arrestConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(arrestConversation, 0, PLAYER_PED_ID(), "TREVOR")
		ENDIF

		IF NOT HAS_PED_GOT_WEAPON(pedCop, WEAPONTYPE_PISTOL)
			GIVE_WEAPON_TO_PED(pedCop, WEAPONTYPE_PISTOL, -1, FALSE)
		ENDIF
		SET_CURRENT_PED_WEAPON(pedCop, WEAPONTYPE_PISTOL, TRUE)
						

		objBinbag = CREATE_OBJECT(PROP_LD_BINBAG_01, <<vInput.x, vInput.y, vInput.z+100>>)
		SET_ENTITY_VISIBLE(objBinbag, FALSE)

		PRINTSTRING("\nre_arrest - MODEL LOADED AND PEDS CREATED\n")

		
		IF VDIST(<< 2411.32, 4958.76, 45.19 >>, vInput) < 10
			iCurrentArrest = ARREST_1 // Hay bales
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, << 2473.2544, 4901.6514, 43.1488 >>, PEDMOVEBLENDRATIO_SPRINT, -1, 2, ENAV_NO_STOPPING, someNavData)//PEDMOVEBLENDRATIO_RUN) // Flee through hay bale field, then randomly
				TASK_SMART_FLEE_PED(NULL, pedCop, 150, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedCriminal, seq)
			SET_PED_KEEP_TASK(pedCriminal, TRUE)
			CLEAR_SEQUENCE_TASK(seq)
			PRINTSTRING("re_arrest - iCurrentArrest = ARREST_1")
			
			DISABLE_NAVMESH_IN_AREA( << 2544.8621, 4729.8926, 27.8986 >>, << 2564.8621, 4749.8926, 28.8986 >>, TRUE)
			
			PRINTNL()
		ELSE
			iCurrentArrest = ARREST_2
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, << 2531.4451, 2521.7493, 38.2072 >>, PEDMOVEBLENDRATIO_SPRINT, -1, 2, ENAV_NO_STOPPING, someNavData)//PEDMOVEBLENDRATIO_RUN) // Flee through hay bale field, then randomly
				TASK_SMART_FLEE_PED(NULL, pedCop, 150, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedCriminal, seq)
			SET_PED_KEEP_TASK(pedCriminal, TRUE)
			CLEAR_SEQUENCE_TASK(seq)
			PRINTSTRING("re_arrest - iCurrentArrest = ARREST_2")
			PRINTNL()
		ENDIF
		IF iCurrentArrest = ARREST_1 ENDIF
		TASK_GO_TO_ENTITY(pedCop, pedCriminal, -1, 1, PEDMOVEBLENDRATIO_SPRINT) 
		SET_PED_KEEP_TASK(pedCop, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_BACKUP_ATTACK_PLAYER()
	//IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		IF DOES_ENTITY_EXIST(pedCopGenerated)
			IF NOT IS_PED_INJURED(pedCopGenerated)
				OPEN_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedCopGenerated, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(pedCopGenerated, TRUE)
				SET_ENTITY_IS_TARGET_PRIORITY(pedCopGenerated, TRUE)
				PRINTSTRING("SET_BACKUP_ATTACK_PLAYER true") PRINTNL()
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC REGISTER_AS_COMPLETE()
	IF NOT bForceCleanupOccurred
		LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	ENDIF
	RANDOM_EVENT_PASSED(RE_ARREST, iCurrentArrest)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
ENDPROC
// Clean Up
PROC missionCleanup()
	PRINTSTRING("[ARREST] re_arrest - Cleanup") PRINTNL()
	IF GET_RANDOM_EVENT_FLAG()
		SWITCH stage_check_injured_state
			CASE IGNORED_BOTH
				PRINTSTRING("[ARREST] re_arrest - Ignored both peds")
				PRINTNL()
			BREAK
			
			CASE ATTACKED_BOTH
				PRINTSTRING("[ARREST] re_arrest - Damaged both peds")
				PRINTNL()
			BREAK
			
			CASE JUST_INJURED_THE_COP
				SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_CRIMINALS)
				REGISTER_AS_COMPLETE()
				PRINTSTRING("[ARREST] re_arrest - Injured Cop")
				PRINTNL()
			BREAK 
			
			CASE JUST_INJURED_THE_CRIM
				SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_COPS)
				REGISTER_AS_COMPLETE()
				PRINTSTRING("[ARREST] re_arrest - Injured criminal")
				PRINTNL()
			BREAK
		ENDSWITCH			
		
		IF DOES_ENTITY_EXIST(pedCriminal)
			//SET_PED_RESET_FLAG(pedCriminal, PRF_DisablePedEnteredMyVehicleEvents, TRUE)
			IF NOT IS_PED_INJURED(pedCriminal) AND NOT IS_ENTITY_DEAD(pedCriminal)
				//Set the ped to flee his initial coords.
				//If we set the ped to flee the cop, he will stop if the cop gets cleaned up.
				IS_ENTITY_DEAD(pedCop) //Satisfy the assert - the cop could actually be dead, so don't do anything with the result.
				IF DOES_ENTITY_EXIST(pedCop)
					TASK_SMART_FLEE_COORD(pedCriminal, GET_ENTITY_COORDS(pedCop), 9000, -1)
				ELSE
					TASK_SMART_FLEE_COORD(pedCriminal, GET_ENTITY_COORDS(pedCriminal), 9000, -1)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCriminal, FALSE)
				SET_PED_KEEP_TASK(pedCriminal, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCop)
			IF NOT IS_PED_INJURED(pedCop)
				SET_PED_KEEP_TASK(pedCop, TRUE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop, FALSE)
				SET_PED_AS_COP(pedCop, TRUE)
				PRINTSTRING("PAUL - SET_PED_CAN_BE_TARGETTED") PRINTNL()
				SET_PED_CAN_BE_TARGETTED(pedCop, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCopGenerated)
			IF NOT IS_PED_INJURED(pedCopGenerated)
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_BACKUP_ATTACK_PLAYER()
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCopGenerated, FALSE)
				SET_PED_AS_COP(pedCopGenerated, TRUE)
			ENDIF
		ENDIF
		
		PRINTSTRING("re_arrests cleaned up")
		
		IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			SET_WANTED_LEVEL_MULTIPLIER(1)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGrouparrestcrim, relGrouparrestcop)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGrouparrestcop, relGrouparrestcrim)
		ENDIF
			
		IF bCopCarGenerated
			SET_RANDOM_TRAINS(TRUE)
		ENDIF
	ENDIF
	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<2551.0378, 4708.6133, 32.6775>>, <<2536.9790, 5022.1787, 43.8519>>, 300)
	
	IF delayWanted	
		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	RANDOM_EVENT_OVER()
	WAIT(0)
	TERMINATE_THIS_THREAD()
	
ENDPROC		
BOOL showHeightOnBlip = FALSE
BOOL setHeightOnBlip = FALSE
PROC flashBlipForDecision()
	IF (GET_GAME_TIMER() - iControlTimer) > 500
		IF stage_check_injured_state <> ATTACKED_BOTH
		AND stage_check_injured_state <> JUST_INJURED_THE_CRIM
		AND stage_check_injured_state <> JUST_INJURED_THE_COP
			IF DOES_BLIP_EXIST(blipCriminal)
				SET_BLIP_AS_FRIENDLY(blipCriminal, bCriminalBlip)
				IF bCriminalBlip = TRUE
					bCriminalBlip = FALSE
				ELSE
					bCriminalBlip = TRUE
				ENDIF
			ENDIF
		ELSE
			//Attacked criminal?, show heights.
			showHeightOnBlip = TRUE
		ENDIF
		IF NOT cop_attacks_player
			IF DOES_BLIP_EXIST(blipCop)
				SET_BLIP_AS_FRIENDLY(blipCop, bCopBlip)
				IF bCopBlip = TRUE
					bCopBlip = FALSE
				ELSE
					bCopBlip = TRUE
				ENDIF
			ENDIF
		ELSE
			//Attacked cop, show heights.
			showHeightOnBlip = TRUE
		ENDIF
		
		iControlTimer = GET_GAME_TIMER()
	ENDIF
	
	
	IF NOT (setHeightOnBlip = showHeightOnBlip)
		IF showHeightOnBlip
			IF DOES_BLIP_EXIST(blipCriminal)
				SHOW_HEIGHT_ON_BLIP(blipCriminal, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(blipCop)
				SHOW_HEIGHT_ON_BLIP(blipCop, TRUE)
			ENDIF		
		ELSE
			IF DOES_BLIP_EXIST(blipCriminal)
				SHOW_HEIGHT_ON_BLIP(blipCriminal, FALSE)
			ENDIF
			IF DOES_BLIP_EXIST(blipCop)
				SHOW_HEIGHT_ON_BLIP(blipCop, FALSE)
			ENDIF
		ENDIF		
		setHeightOnBlip = showHeightOnBlip
	ENDIF
ENDPROC


PROC set_cop_attacks_player(BOOL make_criminal_flee = TRUE)
	IF NOT IS_PED_INJURED(pedCop)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCop)
		
		IF NOT cop_attacks_player
			PRINTSTRING("[arrest] SET_PLAYER_WANTED_LEVEL_NO_DROP") PRINTNL()
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_PED_CAN_BE_TARGETTED(pedCop, TRUE)
			IF DOES_BLIP_EXIST(blipCop)
				SET_BLIP_AS_FRIENDLY(blipCop, FALSE)
			ENDIF
			
			IF make_criminal_flee = TRUE
				IF NOT IS_PED_INJURED(pedCriminal)
					IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_SMART_FLEE_PED) = FINISHED_TASK
						IF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_idle")
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(NULL, arrestAnimDict, "kneeling_arrest_escape", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
								TASK_SMART_FLEE_PED(NULL, pedCop, 150, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedCriminal, seq)
							CLEAR_SEQUENCE_TASK(seq)
							FORCE_PED_MOTION_STATE(pedCriminal, MS_ON_FOOT_RUN)
							SET_PED_KEEP_TASK(pedCriminal, TRUE)
						ELSE
							//Put down the arms if we have them raised
							CLEAR_PED_TASKS(pedCriminal)
							TASK_SMART_FLEE_PED(pedCriminal, pedCop, 150, -1)
						ENDIF
					ENDIF
				ENDIF
				//PRINTSTRING("Paul - crimQuickThanks1") PRINTNL()
				IF NOT IS_ENTITY_DEAD(pedCriminal) AND NOT IS_PED_INJURED(pedCriminal) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
					//PRINTSTRING("Paul - crimQuickThanks2") PRINTNL()
				
					IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCriminal), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20.0)
						//PRINTSTRING("Paul - crimQuickThanks3") PRINTNL()
				
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINTSTRING("Paul - crimQuickThanks") PRINTNL()
							CREATE_CONVERSATION(arrestConversation, "REARRAU", crimQuickThanks, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			CLEAR_PED_TASKS(pedCop)
			OPEN_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedCop, seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_KEEP_TASK(pedCop, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(pedCop, TRUE)
			IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
				SET_VEHICLE_DOORS_LOCKED(vehCopCarGenerated, VEHICLELOCK_UNLOCKED)
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			WAIT(0)
			IF NOT IS_PED_INJURED(pedCop)
				IF NOT IS_PED_RAGDOLL(pedCop)
					//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PWTF") //Attack
					//CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PWTF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
					
				ENDIF
			ENDIF
			
			//Set the backup to also attack the player. 
			//pedCopGenerated
			IF DOES_ENTITY_EXIST(pedCopGenerated)
				SET_BACKUP_ATTACK_PLAYER()
			ENDIF
			
			cop_attacks_player = TRUE
		ENDIF
	ENDIF
	
	
ENDPROC

PROC criminal_attacked_by_player()
	IF NOT IS_PED_INJURED(pedCriminal)
		IF DOES_BLIP_EXIST(blipCriminal)
			SET_BLIP_AS_FRIENDLY(blipCriminal, FALSE)
		ENDIF
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCriminal)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_MESSING_ABOUT_IN_A_PROSCRIBED_VEHICLE()
	BOOL bReturn = FALSE
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = BULLDOZER
			IF NOT IS_PED_INJURED(pedCop)
				IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedCop)
					bReturn = TRUE
				ENDIF
			ENDIF
		ENDIF
//		IF NOT IS_PED_INJURED(pedCriminal)
//			IF bCopHasArrestedCriminal
//				IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedCriminal)
//					bReturn = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
			IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehCopCarGenerated)
				bReturn = TRUE
			ENDIF
		ELSE
			IF bCopCarGenerated
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehCopCarGenerated, PLAYER_PED_ID())
					bReturn = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bReturn
		PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> IS_PLAYER_MESSING_ABOUT_IN_A_PROSCRIBED_VEHICLE()\n")
	ENDIF
	RETURN bReturn
ENDFUNC


FUNC BOOL IS_PLAYER_SHOOTING_AT_COP()
	VECTOR vTempMax = <<3,3,3>>, vTempMin = <<-3,-3,-3>>
	WEAPON_TYPE tempWeapon
	IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedCop, BONETAG_HEAD, <<0,0,0>>), 0.5)
		IF IS_PED_INJURED(pedCriminal)
			PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 0")
			RETURN TRUE
		ELSE
			IF NOT IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedCriminal, BONETAG_HEAD, <<0,0,0>>), 2)
				PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 1")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedCop, BONETAG_HEAD, <<0,0,0>>), 3)
		PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 3")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<5,5,5>>)
		//PRINTSTRING("Paul - IS_ENTITY_AT_ENTITY") PRINTNL()
		IF NOT IS_PED_INJURED(pedCop)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCop)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedCop) 
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), tempWeapon)
					IF tempWeapon <> WEAPONTYPE_UNARMED
						PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 4")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedCriminal)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCriminal)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedCriminal) 
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), tempWeapon)
					IF tempWeapon <> WEAPONTYPE_UNARMED
						IF bCopHasArrestedCriminal
							PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 5")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	vTempMax = vTempMax+GET_PED_BONE_COORDS(pedCop, BONETAG_HEAD, <<0,0,0>>)
	vTempMin = vTempMin+GET_PED_BONE_COORDS(pedCop, BONETAG_HEAD, <<0,0,0>>)
	IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_VEHICLE_WEAPON_TANK, TRUE)
		PRINTLN("IS_PLAYER_SHOOTING_AT_COP() 2")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAYER_TOUCHING()
	PRINTSTRING("[ARREST] TOUCHING!") PRINTNL()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF touchingCount < 3
			TASK_LOOK_AT_ENTITY(pedCop, PLAYER_PED_ID(), 6000, SLF_WHILE_NOT_IN_FOV)
			CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PLEAVE", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
		ENDIF
	ENDIF
	touchingCount++
	touchingTime = GET_GAME_TIMER()
ENDPROC

FUNC BOOL HAS_PLAYER_OBSTRUCTED_ARRESTING_COP()
	IF NOT IS_ENTITY_DEAD(pedCop)
		IF bCopHasArrestedCriminal
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedCop)
				IF ((GET_GAME_TIMER() - touchingTime) > 1000)
					PLAYER_TOUCHING()
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF touchingCount >= 3
		RETURN TRUE
	ENDIF
					
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_OBSTRUCTED_FLEEING_PED()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedCriminal)
			IF bCopHasArrestedCriminal
				IF copCatchesCriminalStage <> getToCopcar
					IF VDIST(GET_ENTITY_COORDS(pedCriminal), vArrestedPos) > 2.5
						PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> HAS_PLAYER_OBSTRUCTED_FLEEING_PED()\n")
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE	
			ENDIF
		ENDIF
	ENDIF
	IF bCopHasArrestedCriminal
		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedCriminal)
			IF IS_PED_RAGDOLL(pedCriminal)
				IF copCatchesCriminalStage <> getToCopcar
					IF VDIST(GET_ENTITY_COORDS(pedCriminal), vArrestedPos) > 2.5
						PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> HAS_PLAYER_OBSTRUCTED_FLEEING_PED()\n")
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				IF bCopHasArrestedCriminal
					IF ((GET_GAME_TIMER() - touchingTime) > 1000)
						PLAYER_TOUCHING()
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
	IF touchingCount >= 3
		RETURN TRUE
	ENDIF
					
	RETURN FALSE
ENDFUNC


INT delayWantedTime
			
PROC check_player_damaged_ped()	
	IF NOT IS_PED_INJURED(pedCriminal)	
		BOOL player_touching_crim = IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedCriminal)
		BOOL player_hurt_crim = HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal, PLAYER_PED_ID())
		
		IF player_hurt_crim
		OR HAS_PLAYER_OBSTRUCTED_FLEEING_PED()
		OR HAS_PLAYER_OBSTRUCTED_ARRESTING_COP()
		OR player_touching_crim
			criminal_attacked_by_player()
			PRINTSTRING("PAUL - criminal_attacked_by_player()	") PRINTNL()
			
			IF bCopHasArrestedCriminal
				PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> check_player_damaged_ped() - set_cop_attacks_player() - 1\n")
				
				PRINTSTRING("PAUL 1 HAS_ENTITY_BEEN_DAMAGED_BY_ENTITYz")  PRINTNL()
				IF (player_touching_crim OR player_hurt_crim) AND NOT IS_PED_RAGDOLL(pedCriminal) AND NOT IS_PED_IN_ANY_VEHICLE(pedCriminal)
					PRINTSTRING("PAUL 1 SET_PED_TO_RAGDOLL")  PRINTNL()
					CLEAR_PED_TASKS(pedCriminal)
					SET_PED_TO_RAGDOLL(pedCriminal, 500, 2000, TASK_RELAX, FALSE, FALSE)
					TASK_COWER(pedCriminal)
					SET_PED_KEEP_TASK(pedCriminal, TRUE)
				ELIF NOT IS_PED_IN_ANY_VEHICLE(pedCriminal) AND NOT IS_PED_RAGDOLL(pedCriminal)
					CLEAR_PED_TASKS(pedCriminal)
					TASK_COWER(pedCriminal)
					SET_PED_KEEP_TASK(pedCriminal, TRUE)
				ENDIF
				set_cop_attacks_player(FALSE)	
				
				PRINTSTRING("PAUL 3 HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY")  PRINTNL()
				
				PRINTSTRING("PAUL - IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT sayHandsUp") PRINTNL()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT sayHandsUp") PRINTNL()
					IF NOT IS_PED_INJURED(pedCop) AND givenUpSpeech = FALSE
						IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PWTF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							givenUpSpeech= TRUE
						ENDIF
					ENDIF
				ENDIF
				
				stage_check_injured_state = ATTACKED_BOTH
			ELSE
				stage_check_injured_state = JUST_INJURED_THE_CRIM
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipCriminal)
			REMOVE_BLIP(blipCriminal)
			REMOVE_PED_FOR_DIALOGUE(arrestConversation, 2)
		ENDIF
		criminal_attacked_by_player()
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal, PLAYER_PED_ID())
			IF bCopHasArrestedCriminal
				PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> check_player_damaged_ped() - set_cop_attacks_player() - 2\n")
				set_cop_attacks_player()
				
				stage_check_injured_state = ATTACKED_BOTH
			ELSE
				stage_check_injured_state = JUST_INJURED_THE_CRIM
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedCop)
		IF cop_attacks_player
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				missionCleanup()
			ENDIF
		ENDIF
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			PRINTSTRING("[arrest] IS_PLAYER_WANTED_LEVEL_GREATER") PRINTNL()
		ENDIF
			
		BOOL attackedBackup = FALSE
		IF DOES_ENTITY_EXIST(pedCopGenerated)
			IF NOT IS_PED_INJURED(pedCopGenerated)
				IF (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCopGenerated, PLAYER_PED_ID()) AND GET_ENTITY_HEALTH(pedCopGenerated) < 190)
				OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCopGenerated, PLAYER_PED_ID()) AND IS_PED_RAGDOLL(pedCopGenerated))
					attackedBackup = TRUE
					PRINTSTRING("[arrest] player attacked the backup") PRINTNL()
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehCopCarGenerated)
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehCopCarGenerated, PLAYER_PED_ID())
					attackedBackup = TRUE
					PRINTSTRING("[arrest] player attacked the backup vehicle") PRINTNL()
				ENDIF
			ELSE
				attackedBackup = TRUE
				PRINTSTRING("[arrest] player attacked killed backup") PRINTNL()
			ENDIF
		ENDIF
		
		IF (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCop, PLAYER_PED_ID()) AND GET_ENTITY_HEALTH(pedCop) < 190)
		OR IS_PLAYER_SHOOTING_AT_COP()
		//OR HAS_PLAYER_AGGROED_PED(pedCop, aggroReason, lockonTimer, 0|1|3|4|5, hasAggroed, 0)
		OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		OR IS_PLAYER_MESSING_ABOUT_IN_A_PROSCRIBED_VEHICLE()
		OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCop, PLAYER_PED_ID()) AND IS_PED_RAGDOLL(pedCop))
		OR attackedbackup = TRUE
			PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> IS_PLAYER_SHOOTING_AT_COP() - set_cop_attacks_player()\n")
			
			set_cop_attacks_player()
			IF stage_check_injured_state = JUST_INJURED_THE_CRIM
			OR stage_check_injured_state = ATTACKED_BOTH
				
				stage_check_injured_state = ATTACKED_BOTH
			ELSE
				PRINTSTRING("[ARREST] check_player_damaged_ped JUST_INJURED_THE_COP 1") PRINTNL() 
				stage_check_injured_state = JUST_INJURED_THE_COP
			ENDIF
		ENDIF
	ELSE
		PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> pedCop is dead.\n")	
		
		PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> pedCop blip does not exist.\n")	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCop, PLAYER_PED_ID())
			PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> pedCop is dead and damaged by player.\n")	
			//SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
			//SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			//This is for bug 1524548
			IF delayWanted = FALSE
				delayWanted = TRUE
				delayWantedTime = GET_GAME_TIMER()
			ENDIF
			
			IF stage_check_injured_state = JUST_INJURED_THE_CRIM
			OR stage_check_injured_state = ATTACKED_BOTH
				
				stage_check_injured_state = ATTACKED_BOTH
			ELSE
				PRINTSTRING("[ARREST] check_player_damaged_ped JUST_INJURED_THE_COP 2") PRINTNL() 
				stage_check_injured_state = JUST_INJURED_THE_COP
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedCriminal)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCriminal)
		ENDIF
		
		IF DOES_BLIP_EXIST(blipCop)
			REMOVE_BLIP(blipCop)
			REMOVE_PED_FOR_DIALOGUE(arrestConversation, 1)
		ENDIF
	ENDIF
	
	IF delayWanted	
		IF ((GET_GAME_TIMER() - delayWantedTime) > 5000)
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			delayWanted = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedCop)
	AND NOT IS_PED_INJURED(pedCriminal)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCop, PLAYER_PED_ID())
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal, PLAYER_PED_ID())
			IF GET_ENTITY_HEALTH(pedCop) < 190
			AND GET_ENTITY_HEALTH(pedCriminal) < 190
				
				criminal_attacked_by_player()
				
				set_cop_attacks_player()
				
				
				
					
				stage_check_injured_state = ATTACKED_BOTH
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_AT_ENTITY(pedCriminal, pedCop, <<110, 110, 40>>)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<20, 20, 20>>)
				KILL_ANY_CONVERSATION()
				WAIT(0)
				//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PORUN") //"Shit! Little bastard got away!"
				CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PORUN", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
			ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCriminal, <<40, 40, 40>>)
				KILL_ANY_CONVERSATION()
				WAIT(0)
				CREATE_CONVERSATION(arrestConversation, "REARRAU", crimOutRun, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
			ENDIF
			missionCleanup()
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedCop)
	AND IS_PED_INJURED(pedCriminal)
		PRINTSTRING("Paul - why are we here?") PRINTNL()
		missionCleanup()
	ENDIF
	
	IF IS_PED_INJURED(pedCriminal)
		IF NOT IS_PED_INJURED(pedCop)
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<100, 100, 40>>)
				missionCleanup()
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

	

PROC CRIMINAL_ATTEMPTS_TO_ESCAPE()

	IF NOT criminal_turned_on_cop
			
		SWITCH time_out_criminal_stage
			
			CASE 0
				
				IF NOT cop_attacks_player
				
					IF TIMERA() > 80000
						IF NOT IS_PED_INJURED(pedCop)
							IF NOT IS_PED_INJURED(pedCriminal)
								KILL_ANY_CONVERSATION()
								WAIT(0)
								//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRAD3") //Where the fuck is back up!
								CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAD3", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							ENDIF
						ENDIF
						time_out_criminal_stage = 1
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 1
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					IF NOT IS_PED_INJURED(pedCriminal)
//						IF NOT HAS_PED_GOT_WEAPON(pedCriminal, WEAPONTYPE_PISTOL)
//							GIVE_WEAPON_TO_PED(pedCriminal, WEAPONTYPE_PISTOL, -1, FALSE)
//						ENDIF
//						
//						SET_CURRENT_PED_WEAPON(pedCriminal, WEAPONTYPE_PISTOL, TRUE)
//						SET_PED_COMBAT_MOVEMENT(pedCriminal, CM_WILLADVANCE )
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_AGGRESSIVE, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_DO_DRIVEBYS, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_USE_VEHICLE, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_JUST_SEEK_COVER, FALSE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_ALWAYS_FLEE, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedCriminal, CA_FLEE_WHILST_IN_VEHICLE, FALSE)



						IF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_idle")
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(NULL, arrestAnimDict, "kneeling_arrest_escape", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
								TASK_SMART_FLEE_PED(NULL, pedCop, 150, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedCriminal, seq)
							CLEAR_SEQUENCE_TASK(seq)
							FORCE_PED_MOTION_STATE(pedCriminal, MS_ON_FOOT_RUN)
							SET_PED_KEEP_TASK(pedCriminal, TRUE)
						ELSE
							TASK_SMART_FLEE_PED(pedCriminal, pedCop, 150, -1)
						ENDIF
						SET_PED_KEEP_TASK(pedCriminal, TRUE)
						SET_ENTITY_HEALTH(pedCriminal, 101)
						
						CREATE_CONVERSATION(arrestConversation, "REARRAU", crimTurn, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)

						time_out_criminal_stage = 2
					ENDIF
					
					IF NOT IS_PED_INJURED(pedCop)
						CLEAR_PED_TASKS(pedCop)
						time_out_criminal_stage = 2
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 2
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(pedCop)
						IF NOT IS_PED_INJURED(pedCriminal)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGrouparrestcrim, relGrouparrestcop)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGrouparrestcop, relGrouparrestcrim)
							TASK_COMBAT_PED(pedCop, pedCriminal)
						ELSE
							TASK_WANDER_STANDARD(pedCop)
							missionCleanup()
						ENDIF
						SET_PED_KEEP_TASK(pedCop, TRUE)
					ENDIF
					criminal_turned_on_cop = TRUE
				//ENDIF
					
			BREAK
			
		ENDSWITCH
			
	ENDIF
	
	
ENDPROC

FUNC BOOL HAS_COP_CAR_BEEN_GENERATED()
	VECTOR vTemp, vDiffVector, vClosestNode, vResultLinkDir
	MODEL_NAMES modelCopCar = PRANGER
	IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
		RETURN TRUE
	ENDIF
	REQUEST_MODEL(modelCopCar)
	IF HAS_MODEL_LOADED(modelCopCar)
		IF GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(pedCop, FALSE), 1, vTemp) //GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(vTemp, vClosestNode, vResultLinkDir, 0, 180, 50, TRUE)
	//			IF GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), iRandomNode, vClosestNode)

				vehCopCarGenerated = CREATE_VEHICLE(modelCopCar, vClosestNode)
				pedCopGenerated = CREATE_PED_INSIDE_VEHICLE(vehCopCarGenerated, PEDTYPE_COP, S_M_Y_RANGER_01)
				SET_PED_CONFIG_FLAG(pedCopGenerated, PCF_DisableShockingEvents, TRUE)
				IF NOT HAS_PED_GOT_WEAPON(pedCopGenerated, WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(pedCopGenerated, WEAPONTYPE_PISTOL, -1, FALSE)
				ENDIF
				SET_CURRENT_PED_WEAPON(pedCopGenerated, WEAPONTYPE_PISTOL, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedCopGenerated, relGrouparrestcop)
				SET_VEHICLE_DOORS_LOCKED(vehCopCarGenerated, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCopGenerated, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCopCarGenerated)
				
				SET_VEHICLE_DISABLE_TOWING(vehCopCarGenerated, TRUE)
				SET_DISABLE_PRETEND_OCCUPANTS(vehCopCarGenerated, TRUE)
				
				vTemp = GET_ENTITY_COORDS(vehCopCarGenerated)
				vDiffVector = (GET_ENTITY_COORDS(PLAYER_PED_ID()) - vTemp)
				SET_ENTITY_HEADING(vehCopCarGenerated, GET_HEADING_FROM_VECTOR_2D(vDiffVector.x, vDiffVector.y))
				
				GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(pedCop, FALSE), 1, vTemp)
				
				IF GET_DISTANCE_BETWEEN_COORDS(vTemp, <<2528.5625, 2639.1147, 36.9446>>)  < 75
					//Arrest 2 - don't go into the scrap yard - this causes lots of woe.
					PRINTSTRING("PAUl - scrapyard") PRINTNL()
					vTemp = <<2473.6006, 2496.7646, 40.8700>>
				ENDIF
				
				IF (GET_DISTANCE_BETWEEN_COORDS(<<2538.7097, 2592.0073, 36.9446>>, vTemp) < 16.0)
					PRINTSTRING("PAUl - scrapyard too?") PRINTNL()
					vTemp = <<2473.6006, 2496.7646, 40.8700>>
				ENDIF
				TASK_VEHICLE_MISSION_COORS_TARGET(pedCopGenerated, vehCopCarGenerated, vTemp, MISSION_GOTO, 7, DRIVINGMODE_AVOIDCARS|DF_StopForPeds, 5, -1)
				SET_VEHICLE_FORWARD_SPEED(vehCopCarGenerated, 4)
				SET_VEHICLE_SIREN(vehCopCarGenerated, TRUE)
				bCopCarGenerated = TRUE
				SET_RANDOM_TRAINS(FALSE)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - HAS_COP_CAR_BEEN_GENERATED()")
				RETURN TRUE
				
			ELSE
				PRINTNL()
				PRINTSTRING("Can't find a node!")
				PRINTNL()
				
			ENDIF
		ENDIF
	ENDIF

	
	RETURN FALSE


ENDFUNC

FUNC BOOL HAS_COP_THANKED_PLAYER()
	IF stage_check_injured_state <> JUST_INJURED_THE_CRIM
		cop_thanks_stage = 3
	ENDIF
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - cop_thanks_stage = ", cop_thanks_stage)
	SWITCH cop_thanks_stage
	
		CASE 0
			IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
			OR GET_GAME_TIMER() > iArrestTimer + 15000
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedCop, <<25,25,25>>)
					PRINTSTRING("cop_thanks_stage 0 ")
							
					IF NOT IS_PED_INJURED(pedCop)
						CLEAR_PED_TASKS(pedCop)
						OPEN_SEQUENCE_TASK(seq)
							IF NOT IS_ENTITY_DEAD(pedCriminal)
								TASK_GO_TO_ENTITY(NULL, pedCriminal, DEFAULT_TIME_NEVER_WARP, 3.5, PEDMOVEBLENDRATIO_RUN)
							ENDIF
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCop, seq)
						CLEAR_SEQUENCE_TASK(seq)

						cop_thanks_stage = 1
					ENDIF
				ELSE
					cop_thanks_stage = 3
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				IF GET_SEQUENCE_PROGRESS(pedCop) > 0 
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF time_out_criminal_stage < 2
							CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PTHANK", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							IF DOES_BLIP_EXIST(blipCop)
								SET_BLIP_AS_FRIENDLY(blipCop, TRUE)
							ENDIF
						ELSE
							//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PHELP") //"You could have helped!"
							CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PHELP", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
						ENDIF

						cop_thanks_stage = 2
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF time_out_criminal_stage < 2
						CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PTHANK", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
						IF DOES_BLIP_EXIST(blipCop)
							SET_BLIP_AS_FRIENDLY(blipCop, TRUE)
						ENDIF
					ELSE
						//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PHELP") //"You could have helped!"
						CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PHELP", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
					ENDIF

					cop_thanks_stage = 2
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				cop_thanks_stage = 3
			ELSE
			ENDIF
			TASK_TURN_PED_TO_FACE_ENTITY(pedCop, PLAYER_PED_ID(), 100)
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC GIVE_CPR()
	IF NOT bCPRStarted
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(pedCriminal, FALSE), -1)
			IF (VDIST(GET_ENTITY_COORDS(pedCriminal, FALSE), GET_ENTITY_COORDS(pedCop, FALSE)) > 4.0)
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_ENTITY_COORDS(pedCriminal, FALSE), PEDMOVEBLENDRATIO_WALK, -1, 4.0, ENAV_NO_STOPPING, someNavData)
			ENDIF
			TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_TEND_TO_DEAD", 300000, TRUE)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedCop, seq)
		CLEAR_SEQUENCE_TASK(seq)
		PRINTSTRING("\nre_arrests - GIVE_CPR()\n")
		SET_CURRENT_PED_WEAPON(pedCop, WEAPONTYPE_UNARMED)
		bCPRStarted = TRUE
	ENDIF
ENDPROC

FUNC BOOL HAS_AMBULANCE_ATTENDED_SCENE()
	VEHICLE_INDEX tempAmbulanceCheck
	PED_INDEX pedRandom
	
	IF TIMERA() > 60000
		RETURN TRUE
	ENDIF
	
	IF GET_CLOSEST_PED (GET_ENTITY_COORDS(pedCriminal, FALSE), 2, TRUE, FALSE, pedRandom)
		IF NOT IS_PED_INJURED(pedRandom)
			IF IS_PED_MODEL(pedRandom, S_M_M_PARAMEDIC_01)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	tempAmbulanceCheck = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(pedCriminal, FALSE), 25, AMBULANCE, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) 
	IF IS_VEHICLE_DRIVEABLE(tempAmbulanceCheck)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PARKED_ON_BODY()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF VDIST(GET_ENTITY_COORDS(pedCriminal, FALSE), GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)) < 5.0
				RETURN TRUE 
			ENDIF
		ELSE
			IF VDIST(GET_ENTITY_COORDS(pedCriminal, FALSE), GET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), FALSE)) < 5.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

INT copWaitTimer
PROC COP_ATTENDS_TO_DEAD_CRIMINAL()
	
	SWITCH copAttendsDeadCriminalStage
		CASE copThanksPlayer
			IF NOT bCopHasArrestedCriminal
				IF stage_check_injured_state = JUST_INJURED_THE_CRIM
					copAttendsDeadCriminalStage = copGivesCPR
				ELIF stage_check_injured_state = IGNORED_BOTH
					copAttendsDeadCriminalStage = copGivesCPR
				ENDIF
				IS_ENTITY_DEAD(pedCop)
				IS_ENTITY_DEAD(pedCriminal)
				IF DOES_ENTITY_EXIST(pedCop) AND DOES_ENTITY_EXIST(pedCriminal) 
					PRINTSTRING("Arrest IS_ENTITY_ALIVE") PRINTNL()
					TASK_GOTO_ENTITY_OFFSET_XY(pedCop, pedCriminal, -1, 3.0, 0, 0, PEDMOVEBLENDRATIO_WALK)
				ENDIF
				SETTIMERB(0)
			ENDIF
			
		BREAK
		
		CASE copGivesCPR
			
			//IF IS_PLAYER_PARKED_ON_BODY()
				IF DOES_BLIP_EXIST(blipCop)
					REMOVE_BLIP(blipCop)
				ENDIF
				copAttendsDeadCriminalStage = copWaitsForAmbulance
				random_speech_wait = 8000
				copWaitTimer = GET_GAME_TIMER()
			//ENDIF
		BREAK
		
		CASE copWaitsForAmbulance
			IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
				//IF HAS_AMBULANCE_ATTENDED_SCENE() //No ambulance now. 
				IF ((GET_GAME_TIMER() - copWaitTimer) > 20000)
					IF NOT IS_PED_INJURED(pedCopGenerated)
						IF GET_SCRIPT_TASK_STATUS(pedCopGenerated, SCRIPT_TASK_VEHICLE_MISSION) = FINISHED_TASK
							TASK_ENTER_VEHICLE(pedCop, vehCopCarGenerated, -1, VS_FRONT_RIGHT, PEDMOVE_WALK)
							copAttendsDeadCriminalStage = copLeavesWithBackup
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
				
		BREAK
		
		CASE copLeavesWithBackup
			IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
				IF NOT IS_PED_INJURED(pedCopGenerated)
					IF IS_PED_SITTING_IN_VEHICLE(pedCop, vehCopCarGenerated)
						TASK_VEHICLE_DRIVE_WANDER(pedCopGenerated, vehCopCarGenerated, 10, DRIVINGMODE_STOPFORCARS)
						SET_PED_KEEP_TASK(pedCopGenerated, TRUE)
						missionCleanup()
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
			
ENDPROC

PROC RESET_RANDOM_BANTER()
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - RESET_RANDOM_BANTER()")
	bCopRandomBanter = GET_RANDOM_BOOL()
	bCopRandomBanter = bCanPlayCopBanter
	random_speech_wait = 20000	
	bStartedBanter = FALSE
ENDPROC

FUNC BOOL HAS_BANTER_TIMER_ELAPSED()
	IF GET_GAME_TIMER() > iBanterTimer+6000
		iBanterTimer = GET_GAME_TIMER()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAY_COP_RADIO_BANTER()
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_COP_RADIO_BANTER()")
	IF NOT bStartedBanter
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM(NULL, arrestAnimDict, "radio_enter", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, arrestAnimDict, "radio_chatter", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, arrestAnimDict, "radio_exit", NORMAL_BLEND_IN, WALK_BLEND_OUT)
			TASK_AIM_GUN_AT_ENTITY(NULL, pedCriminal, -1)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedCop, seq)
		CLEAR_SEQUENCE_TASK(seq)
		bStartedBanter = TRUE
	ELIF IS_ENTITY_PLAYING_ANIM(pedCop, arrestAnimDict, "radio_chatter")
		IF HAS_BANTER_TIMER_ELAPSED()
			CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAD3", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedCop, arrestAnimDict, "radio_exit")
		bCanPlayCopBanter = FALSE
		RESET_RANDOM_BANTER()
	ENDIF
ENDPROC



PROC PLAY_CRIMINAL_ARRESTED_BANTER()
	//PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_CRIMINAL_ARRESTED_BANTER()")
	IF NOT bStartedBanter
		SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
			CASE 0
				randomBanterIdle = "idle_a"
			BREAK
			CASE 1
				randomBanterIdle = "idle_b"
			BREAK
			CASE 2
				randomBanterIdle = "idle_c"
			BREAK
		ENDSWITCH
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM(NULL, arrestAnimBanterDict, "enter", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, arrestAnimBanterDict, randomBanterIdle, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, arrestAnimBanterDict, "exit", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, arrestAnimDict, "kneeling_arrest_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(pedCriminal, seq)
		CLEAR_SEQUENCE_TASK(seq)
		bStartedBanter = TRUE
	ELIF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimBanterDict, "enter")
		//PRINTLN(GET_THIS_SCRIPT_NAME(), " - IS_ENTITY_PLAYING_ANIM enter")
		IF HAS_BANTER_TIMER_ELAPSED()
			CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAND", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimBanterDict, "exit")
		//PRINTLN(GET_THIS_SCRIPT_NAME(), " - IS_ENTITY_PLAYING_ANIM exit")
	ELIF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimBanterDict, randomBanterIdle)
		//IF HAS_BANTER_TIMER_ELAPSED()
			CREATE_CONVERSATION(arrestConversation, "REARRAU", crimRand, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
		//ENDIF
		//PRINTLN(GET_THIS_SCRIPT_NAME(), " - IS_ENTITY_PLAYING_ANIM randomBanterIdle")
	ELSE
		RESET_RANDOM_BANTER()
		//PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_CRIMINAL_ARRESTED_BANTER() all checks failed")
	ENDIF
ENDPROC

//BOOL lookingAtCrim = TRUE
//BOOL atCrimTalked = FALSE
//INT copDoTurnTime

PROC COP_CALLS_IN_RESULT(BOOL gunOut = TRUE)
	STRING r_enter  
	STRING r_chatter
	STRING r_exit
	
	IF gunOut
		r_enter = "radio_enter"
		r_chatter = "radio_chatter"
		r_exit = "radio_exit"
	ELSE
		r_enter = "generic_radio_enter"
		r_chatter = "generic_radio_chatter"
		r_exit = "generic_radio_exit"
	ENDIF
	
	IF NOT IS_PED_INJURED(pedCop)

		SWITCH copCallInStage
		
			CASE copTakesOutRadio
				IF cop_thanks_stage = 3
				OR copCatchesCriminalStage = copWaitForEndOfCall
				OR copCatchesCriminalStage = copWaitsForBackup
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CLEAR_PED_TASKS(pedCop)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(pedCriminal, FALSE), 10000)
							TASK_PLAY_ANIM(NULL, arrestAnimDict, r_enter, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, arrestAnimDict, r_chatter, FAST_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)//TASK_PLAY_ANIM(NULL, arrestAnimDict, "radio_exit", NORMAL_BLEND_IN, WALK_BLEND_OUT)
							//TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_ENTITY_COORDS(pedCriminal, FALSE), PEDMOVEBLENDRATIO_WALK, -1, 3, ENAV_NO_STOPPING, someNavData)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCop, seq)
						CLEAR_SEQUENCE_TASK(seq)
			
						copCallInStage = copRequestsUnit
					ENDIF
				ENDIF
			BREAK
			
			CASE copRequestsUnit
				IF IS_ENTITY_PLAYING_ANIM(pedCop, arrestAnimDict, r_chatter)
					IF NOT IS_PED_INJURED(pedCriminal)
						//IF PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRAD1") //This is officer Biggs, I have apprehended the suspect. Send a car. Over.
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAD1", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
								PRINTSTRING("4") PRINTNL()
								copCallInStage = copFinishesRequest
							ENDIF
						ENDIF
					ELSE
						IF HAS_COP_CAR_BEEN_GENERATED()
							//IF PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRAD2") //This is officer Biggs, suspect is down! Send an ambulance. Over.
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//IF CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, GET_ENTITY_COORDS(pedCriminal, FALSE), 2, 100.0, ambulance_arrives)
								//ENDIF
								IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAD2", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
									copCallInStage = copFinishesRequest
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			CASE copFinishesRequest
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_COP_CAR_BEEN_GENERATED()
						IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
							SET_VEHICLE_HAS_MUTED_SIRENS(vehCopCarGenerated, FALSE)
							SET_VEHICLE_SIREN(vehCopCarGenerated, TRUE)
						ENDIF
						CLEAR_PED_TASKS(pedCop)
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(NULL, arrestAnimDict, r_exit, NORMAL_BLEND_IN, WALK_BLEND_OUT)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCop, seq)
						CLEAR_SEQUENCE_TASK(seq)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCop)
						/*IF DOES_BLIP_EXIST(blipCop)
							REMOVE_BLIP(blipCop)
						ENDIF
						IF DOES_BLIP_EXIST(blipCriminal)
							REMOVE_BLIP(blipCriminal)
						ENDIF*/
						RESET_RANDOM_BANTER()
						copCallInStage = copWaitsForRequestedUnit
					ENDIF
				ENDIF
			BREAK
			
			CASE copWaitsForRequestedUnit
				IF NOT bCopHasArrestedCriminal
					COP_ATTENDS_TO_DEAD_CRIMINAL()
				
				ENDIF
					
					IF DOES_ENTITY_EXIST(pedCriminal) OR NOT IS_ENTITY_DEAD(pedCriminal)
						IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedCriminal)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PWTF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							ENDIF
						ENDIF
					ENDIF
				
					
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<35, 35, 35>>, FALSE, TRUE)
						
						//Make them cop look at the player, but face the crim. 
						SET_IK_TARGET(pedCop, IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
						SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, pedCop, ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
						
						IF TIMERB() > random_speech_wait
						OR bStartedBanter
							IF stage_check_injured_state = JUST_INJURED_THE_CRIM
							OR stage_check_injured_state = IGNORED_BOTH
								IF NOT IS_PED_INJURED(pedCriminal)
									IF random_speech_wait = 8000
									OR bStartedBanter
										//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRA1") //You're lucky I didn't kill you, you little shit!
										IF bCopRandomBanter
											PLAY_COP_RADIO_BANTER()
										ELSE
											PLAY_CRIMINAL_ARRESTED_BANTER()
										ENDIF
									ELSE
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											TASK_LOOK_AT_ENTITY(pedCop, PLAYER_PED_ID(), 500, SLF_WHILE_NOT_IN_FOV)
											//TASK_TURN_PED_TO_FACE_ENTITY(pedCop, PLAYER_PED_ID(), 3000)
											//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PLEAVE") //you got a license for a gun? 
											CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PLEAVE", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
											random_speech_wait = 8000
										ENDIF
									ENDIF
								ELSE
									PRINTSTRING("PAUL ------ random_speech_wait") PRINTNL()
									
									IF random_speech_wait = 8000
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											
											//TASK_LOOK_AT_ENTITY(pedCop, pedCriminal, 6000, SLF_WHILE_NOT_IN_FOV)
											//TASK_TURN_PED_TO_FACE_ENTITY(pedCop, pedCriminal, 3000)
											
											CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRA2", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
											random_speech_wait = 20000
										ENDIF
									ELSE
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											//TASK_LOOK_AT_ENTITY(pedCop, pedCriminal, 6000, SLF_WHILE_NOT_IN_FOV)
											//TASK_TURN_PED_TO_FACE_ENTITY(pedCop, pedCriminal, 3000)
											
											//TASK_LOOK_AT_ENTITY(pedCop, PLAYER_PED_ID(), 6000, SLF_WHILE_NOT_IN_FOV)
											//TASK_TURN_PED_TO_FACE_ENTITY(pedCop, PLAYER_PED_ID(), 3000)
											
											CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRA2", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
											random_speech_wait = 8000
										ENDIF
									ENDIF
								ENDIF
								SETTIMERB(0)
							ELSE
								IF IS_PED_INJURED(pedCriminal)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRA2") //You're God's problem now!
										CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRA2", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
										random_speech_wait = 30000
									ENDIF
								ELSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										TASK_LOOK_AT_ENTITY(pedCop, PLAYER_PED_ID(), 6000, SLF_WHILE_NOT_IN_FOV)
										//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PLEAVE") //you got a license for a gun? 
										CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PLEAVE", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
										random_speech_wait = GET_RANDOM_INT_IN_RANGE(8, 22)
										random_speech_wait *= 30000
									ENDIF
								ENDIF
								SETTIMERB(0)
							ENDIF
						ENDIF
					ENDIF
					
				
				
				IF copCatchesCriminalStage <> copLeaves
					IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
						IF NOT IS_PED_INJURED(pedCriminal)
							IF NOT IS_PED_IN_VEHICLE(pedCriminal, vehCopCarGenerated)
								CRIMINAL_ATTEMPTS_TO_ESCAPE()
							ENDIF
						ENDIF
					ELSE
						CRIMINAL_ATTEMPTS_TO_ESCAPE()
					ENDIF
				ENDIF
				
			BREAK
		
		ENDSWITCH
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_BACKUP_NEAR_CRIMINAL()
	FLOAT checkRange = 50
	
	IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
		IF IS_ENTITY_AT_ENTITY(vehCopCarGenerated, pedCriminal, <<checkRange,checkRange,checkRange>>)
			RETURN TRUE
		ENDIF
	ENDIF
	PED_INDEX tempCop = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(pedCriminal, FALSE), <<checkRange,checkRange,checkRange>>) 
	IF NOT IS_PED_INJURED(tempCop)
		IF GET_ENTITY_MODEL(tempCop) = S_F_Y_COP_01
		OR GET_ENTITY_MODEL(tempCop) = S_M_Y_COP_01
			RETURN TRUE
		ENDIF
	ENDIF
	tempCop = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<checkRange,checkRange,checkRange>>)
	IF NOT IS_PED_INJURED(tempCop)
		IF GET_ENTITY_MODEL(tempCop) = S_F_Y_COP_01
		OR GET_ENTITY_MODEL(tempCop) = S_M_Y_COP_01
			RETURN TRUE
		ENDIF
	ENDIF
	VEHICLE_INDEX tempCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(pedCriminal, FALSE), checkRange, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY)
	IF IS_VEHICLE_DRIVEABLE(tempCopCar)
		RETURN TRUE
	ENDIF
	
	tempCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), checkRange, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY)
	IF IS_VEHICLE_DRIVEABLE(tempCopCar)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
INT criminalArrivedTime = 0
PROC criminal_thanks()
	BOOL criminal_injured_by_player = HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal, PLAYER_PED_ID())	
	BOOL backup_near = IS_BACKUP_NEAR_CRIMINAL()
	IF backup_near
	ENDIF
	IF NOT IS_PED_INJURED(pedCriminal)
		IF NOT criminal_injured_by_player
			IF NOT backup_near
				IF stage_check_injured_state = JUST_INJURED_THE_COP
				OR bCopHasArrestedCriminal = TRUE
	
					SWITCH criminal_thanks_stage
						
						CASE 8 //gah! 
							DRAW_DEBUG_TEXT_2D("criminal_thanks 8", <<0.02, 0.1, 0>>)
							PRINTSTRING("Paul - criminal qwewqewqewq.") PRINTNL()
							SET_BLIP_AS_FRIENDLY(blipCriminal, TRUE)
							
							IF NOT IS_ENTITY_DEAD(pedCriminal) AND IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_idle")
								PRINTSTRING("Paul - 8 - IS_ENTITY_PLAYING_ANIM") PRINTNL()
								OPEN_SEQUENCE_TASK(seq)
									TASK_PLAY_ANIM (NULL, arrestAnimDict, "kneeling_arrest_get_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									//CLEAR_PED_TASKS(NULL)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedCriminal, seq)
								CLEAR_SEQUENCE_TASK(Seq)
								//Jump to teh wait.
								criminal_thanks_stage = 9
							ELSE
								PRINTSTRING("Paul - 8 - NOT IS_ENTITY_PLAYING_ANIM") PRINTNL()
								//Jump straight to what the start was.
								criminal_thanks_stage = 3
							ENDIF
						BREAK
						
						CASE 9
							DRAW_DEBUG_TEXT_2D("criminal_thanks 9", <<0.02, 0.1, 0>>)
							
							
							// Wait until we have finished the getup anim.
							IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								PRINTSTRING("Paul - 9 GET_SCRIPT_TASK_STATUS FINISHED_TASK") PRINTNL()
								criminal_thanks_stage = 0
							ENDIF
						BREAK
						
						CASE 0
							DRAW_DEBUG_TEXT_2D("criminal_thanks 0", <<0.02, 0.1, 0>>)
							
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 0 ")
							IF IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), <<75, 75, 75>>)
								STOP_PED_SPEAKING(pedCriminal, TRUE)
								criminal_thanks_stage = 3
							ENDIF
						BREAK
						
						CASE 1
							DRAW_DEBUG_TEXT_2D("criminal_thanks 1", <<0.02, 0.1, 0>>)
							
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 1 ")
							IF IS_ENTITY_AT_COORD(pedCriminal, GET_ENTITY_COORDS(pedCop, FALSE), <<4, 4, 3>>)
								criminal_thanks_stage = 2
							ELSE
								//IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
									CLEAR_PED_TASKS(pedCriminal)
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE), 1000)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE), PEDMOVEBLENDRATIO_RUN,  -1, 3.0)
										//TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE))
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedCriminal, seq)
									CLEAR_SEQUENCE_TASK(seq)
									criminal_thanks_stage = 2
								//ENDIF
							ENDIF

						BREAK
						
						CASE 2
							DRAW_DEBUG_TEXT_2D("criminal_thanks 2", <<0.02, 0.1, 0>>)
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 2 ")
							
							IF IS_ENTITY_AT_COORD(pedCriminal, GET_ENTITY_COORDS(pedCop, FALSE), <<4, 4, 3>>)
								IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								OR CREATE_CONVERSATION(arrestConversation, "REARRAU", crimHate, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
									criminal_thanks_stage = 6
								ENDIF
							ELIF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE), 1000)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE), PEDMOVEBLENDRATIO_RUN,  -1, 3.0)
									//TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(pedCop, FALSE))
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedCriminal, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							
						BREAK
						
						CASE 3
							DRAW_DEBUG_TEXT_2D("criminal_thanks 3", <<0.02, 0.1, 0>>)
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 3 ")
							
								
							IF IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), <<5, 5, 3>>)
								PRINTSTRING("arrest criminal_thanks 3 IS_ENTITY_AT_ENTITY") PRINTNL()
								criminal_thanks_stage = 4
							ELSE
								PRINTSTRING("arrest criminal_thanks 3 NOT IS_ENTITY_AT_ENTITY") PRINTNL()
								IF IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), <<75, 75, 75>>)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CLEAR_PED_TASKS(pedCriminal)
										OPEN_SEQUENCE_TASK(seq)
											TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 8.0)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedCriminal, seq)
										CLEAR_SEQUENCE_TASK(seq)
										criminal_thanks_stage = 4
									ENDIF
								ELSE
									criminal_thanks_stage = 6
								ENDIF
							ENDIF
								
							
						BREAK
						
						CASE 4
							DRAW_DEBUG_TEXT_2D("criminal_thanks 4", <<0.02, 0.1, 0>>)
							
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 4 ")
							//IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							IF IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), <<9.0, 9.0, 9.0>>)
								criminalArrivedTime = GET_GAME_TIMER()
								PRINTSTRING("criminal arrive time = ") PRINTINT(criminalArrivedTime) PRINTNL()
								PRINTSTRING("GET_GAME_TIMER() = ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
								
								criminal_thanks_stage = 7
							ENDIF
						BREAK
						
						//UGGHHH!!!
						CASE 7
							DRAW_DEBUG_TEXT_2D("criminal_thanks 7", <<0.02, 0.1, 0>>)
							
							
							PRINTSTRING("criminal_thanks 7 ") PRINTINT(GET_GAME_TIMER() - criminalArrivedTime) PRINTNL()
							PRINTSTRING("criminal arrive time = ") PRINTINT(criminalArrivedTime) PRINTNL()
							PRINTSTRING("GET_GAME_TIMER() = ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
								
							IF NOT IS_PED_HEADTRACKING_PED(pedCriminal, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT|SLF_WHILE_NOT_IN_FOV)
							ENDIF
							IF NOT IS_PED_FACING_PED(pedCriminal, PLAYER_PED_ID(), 30.0)
								TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal,PLAYER_PED_ID(),-1)
							ENDIF
							IF ((GET_GAME_TIMER() - criminalArrivedTime) > 2000)
								criminal_thanks_stage = 5
							ENDIF
						BREAK
						
						CASE 5
							DRAW_DEBUG_TEXT_2D("criminal_thanks 5", <<0.02, 0.1, 0>>)
							
							PRINTNL()
							PRINTSTRING("criminal_thanks 5 ")
							
							IF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "Thanks_Male_05")
							OR GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF time_out_criminal_stage < 2
										CREATE_CONVERSATION(arrestConversation, "REARRAU", crimThank, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
										TASK_PLAY_ANIM(pedCriminal, arrestAnimDict, "Thanks_Male_05", FAST_BLEND_IN, FAST_BLEND_OUT, -1, AF_UPPERBODY)
										//SET_HEIST_CREW_MEMBER_UNLOCKED(CM_DRIVER_B_ARREST_UNLOCK)
									ELSE
										CREATE_CONVERSATION(arrestConversation, "REARRAU", crimHelp, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
									ENDIF
									CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 250)
									TASK_CLEAR_LOOK_AT(pedCriminal)
									criminal_thanks_stage = 6
								ENDIF
							ENDIF
							
						BREAK
					
						CASE 6
							DRAW_DEBUG_TEXT_2D("criminal_thanks 6", <<0.02, 0.1, 0>>)
							
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TASK_SMART_FLEE_PED(pedCriminal, PLAYER_PED_ID(), 150, -1)
								missionCleanup()	
							ENDIF
							
						BREAK
							
					ENDSWITCH
							
				ENDIF
			ELSE 
				//Backup is near, say a quick thanks and go. 
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(arrestConversation, "REARRAU", crimQuickThanks, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
				ENDIF
				
				TASK_SMART_FLEE_PED(pedCriminal, PLAYER_PED_ID(), 150, -1)
				missionCleanup()	
			ENDIF
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION()
			TASK_SMART_FLEE_PED(pedCriminal, PLAYER_PED_ID(), 150, -1)
			
			IF criminal_injured_by_player = TRUE
				stage_check_injured_state = ATTACKED_BOTH
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION(arrestConversation, "REARRAU", crimHelp, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
			ENDIF		
			missionCleanup()
		ENDIF
	ENDIF
	
ENDPROC

BOOL initialDialogueSaid = FALSE

PROC PLAY_INSULT_DIALOGUE()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT initialDialogueSaid
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<30,30,30>>)
				CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PSTOP", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
				initialDialogueSaid = TRUE
				SETTIMERB(0)
			ENDIF
		ELSE
			IF TIMERB() > 8000
				IF GET_RANDOM_INT_IN_RANGE(0,99) > 50
					//PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_PRAND")
					CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PRAND", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
				ELSE
					// Add a check here to see if player is near and insult him instead
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCriminal, <<10,10,10>>)
						CREATE_CONVERSATION(arrestConversation, "REARRAU", crimWTF, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
					ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<10,10,10>>)
						CREATE_CONVERSATION(arrestConversation, "REARRAU", crimWTF, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
					ELSE
						CREATE_CONVERSATION(arrestConversation, "REARRAU", crimRand, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
					ENDIF
				ENDIF
				SETTIMERB(0)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PLAYER_STARTED_INTERACTION()
	FLOAT fLocate = 50
	IF NOT IS_PED_INJURED(pedCop)
		IF NOT IS_PED_INJURED(pedCriminal)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<fLocate, fLocate, fLocate>>)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCriminal, <<fLocate, fLocate, fLocate>>)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), a1, a2, 194.750000)
	
		
				IF NOT DOES_BLIP_EXIST(blipCriminal)
					blipCriminal = CREATE_BLIP_FOR_PED(pedCriminal)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipCop)
					blipCop = CREATE_BLIP_FOR_PED(pedCop)
				ENDIF
				SHOW_HEIGHT_ON_BLIP(blipCriminal, FALSE)
				SHOW_HEIGHT_ON_BLIP(blipCop, FALSE)
				//CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PSTOP", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
				
				SETTIMERA(0)
				RETURN TRUE
			ENDIF
		ELSE
		
			IF NOT IS_PED_INJURED(pedCop)
				IF NOT DOES_BLIP_EXIST(blipCop)
					blipCop = CREATE_BLIP_FOR_PED(pedCop)
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipCriminal)
				REMOVE_BLIP(blipCriminal)
			ENDIF
			
			RETURN TRUE
			
		ENDIF
	ELSE
	
		IF NOT IS_PED_INJURED(pedCriminal)
			IF NOT DOES_BLIP_EXIST(blipCriminal)
				blipCriminal = CREATE_BLIP_FOR_PED(pedCriminal)
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(blipCop)
			REMOVE_BLIP(blipCop)
		ENDIF
		
		
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_SUB_DISPLAY_BASED_ON_DISTANCE()
	INT iCheckCounter
	VECTOR vTemp = <<40, 40, 20>>
	
	IF GET_GAME_TIMER() > (iSubTimer + 1000)
		IF NOT IS_PED_INJURED(pedCop)
			IF NOT IS_ENTITY_AT_ENTITY(pedCop, PLAYER_PED_ID(), vTemp)
				iCheckCounter++
			ENDIF
		ELSE
			iCheckCounter++
		ENDIF
		
		IF NOT IS_PED_INJURED(pedCriminal)
			IF NOT IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), vTemp)
				iCheckCounter++
			ENDIF
		ELSE
			iCheckCounter++
		ENDIF
		
		IF iCheckCounter = 2
			subtitleState = DO_NOT_DISPLAY_SUBTITLES
		ELSE
			subtitleState = DISPLAY_SUBTITLES
		ENDIF
		
		iSubTimer = GET_GAME_TIMER()
	ENDIF
	
ENDPROC

FUNC BOOL IS_PED_ON_SLOPE(FLOAT fMinFlat = 0.9)
	FLOAT zRef
	VECTOR vRef
	IF GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(GET_ENTITY_COORDS(pedCriminal), zRef, vRef)
		IF DOT_PRODUCT(vRef, <<0,0,1.0>>) < fMinFlat
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ARREST_BEEN_FORCED()
	#IF IS_DEBUG_BUILD
		RETURN bForceArrest
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_COP_ARREST_PED()
	IF IS_ENTITY_AT_ENTITY(pedCriminal, pedCop, <<3.5, 3.5, 3.5>>)
	OR HAS_ARREST_BEEN_FORCED()
		IF NOT IS_PED_ON_SLOPE()
			PRINTSTRING("Paul - cop can arrest ped!") PRINTNL()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

					
BOOL isCriminalStumbling = FALSE
VECTOR vCopCarOffset
BOOL isPedTazered = FALSE
PROC run_arrest()
	INT iPlacementFlags
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	
	VECTOR crimHeading
	FLOAT dist 
	BOOL canArrest = FALSE
	flashBlipForDecision()		
	FLOAT carDist 
	
	IF NOT IS_PED_INJURED(pedCriminal)
		SET_PED_RESET_FLAG(pedCriminal, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(pedCriminal)
		IF IS_PED_BEING_STUNNED(pedCriminal)	
			isPedTazered = TRUE
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(pedCop)
	AND ((stage_check_injured_state <> JUST_INJURED_THE_COP) AND (stage_check_injured_state <> ATTACKED_BOTH))
		
		IF NOT IS_PED_INJURED(pedCriminal)

			SWITCH copCatchesCriminalStage
				
				CASE copChasesCriminal
					DRAW_DEBUG_TEXT_2D("copChasesCriminal", <<0.02, 0.1, 0>>)
					
					IF TIMERA() > 10 //After time, start the cop after the ped.
						IF NOT bCopHasArrestedCriminal
							IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_GO_TO_ENTITY) = FINISHED_TASK
								TASK_GO_TO_ENTITY(pedCop, pedCriminal, -1, 1, PEDMOVEBLENDRATIO_SPRINT)
							ENDIF
						ENDIF
					ENDIF
					
					IF TIMERA() > 4000 //After time start the crinminal fleeing. Before he was just running to a point. 
						IF NOT bCopHasArrestedCriminal
							IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_SMART_FLEE_PED) = FINISHED_TASK
								
								IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
									STOP_ANIM_PLAYBACK(pedCriminal, AF_PRIORITY_HIGH, true)
								ENDIF
								TASK_SMART_FLEE_PED(pedCriminal, pedCop, 150, -1)
							ENDIF
						ENDIF
					ENDIF
					
					//get diatance between the player and the criminal
					//don't check if we can arrest the ped unless the time is up, OR the distance is a lot smaller. this is so we avoid them bumping into each other.
					dist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop), GET_ENTITY_COORDS(pedCriminal))
					IF TIMERA() > 8000 OR dist < 1.5
						PRINTSTRING("PAul checking CAN_COP_ARREST_PED") PRINTNL()
					
						canArrest = CAN_COP_ARREST_PED()
						IF canArrest = TRUE
							TASK_GO_TO_ENTITY(pedCop, pedCriminal, -1, 1, 0.01)
						ENDIF
					ENDIF
					
					IF TIMERA() > 30000 AND dist < 10.0
						PRINTSTRING("PAul canArrest = TRUE") PRINTNL()
					
						canArrest = TRUE //30 second time limit.
					ENDIF
					
					IF TIMERA() > 2000 
						IF canArrest OR arrestedWaiting = TRUE 	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PCUFF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
								ENDIF
								
								//CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PGSHOT", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
								
								isCriminalStumbling = IS_PED_RAGDOLL(pedCriminal) OR IS_PED_GETTING_UP(pedCriminal)
								IF NOT HAS_PED_GOT_WEAPON(pedCop, WEAPONTYPE_PISTOL)
									GIVE_WEAPON_TO_PED(pedCop, WEAPONTYPE_PISTOL, -1, FALSE)
								ENDIF
								SET_CURRENT_PED_WEAPON(pedCop, WEAPONTYPE_PISTOL, TRUE)
								vArrestedPos = GET_ENTITY_COORDS(pedCriminal)
								
								CLEAR_PED_TASKS(pedCop)
								OPEN_SEQUENCE_TASK(seq)
									IF NOT isCriminalStumbling AND dist > 2.0 //Don't shoot if the crininal is in ragdoll, or close
										TASK_SHOOT_AT_COORD(NULL, <<vArrestedPos.x, vArrestedPos.y, vArrestedPos.z+4>>, 1000, FIRING_TYPE_1_THEN_AIM)
									ENDIF
									TASK_LOOK_AT_ENTITY(NULL, pedCriminal, -1, SLF_WHILE_NOT_IN_FOV)
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, pedCriminal, pedCriminal, PEDMOVE_WALK, FALSE, 3)
									TASK_AIM_GUN_AT_ENTITY(NULL, pedCriminal, 5000)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedCop, seq)
								CLEAR_SEQUENCE_TASK(seq)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCop)
								SET_PED_KEEP_TASK(pedCop, TRUE)
								SETTIMERA(0)
								iArrestTimer = GET_GAME_TIMER()
								
								IF isCriminalStumbling 
									PRINTSTRING("isCriminalStumbling IS TRUE") PRINTNL()
									
									CLEAR_PED_TASKS(pedCriminal)
									SET_PED_DROPS_WEAPON(pedCriminal)
									copCatchesCriminalStage = pedStandsUpWithHandsUp
								ELSE
									//Running, come to a halt
									copCatchesCriminalStage = pedWaitUntilComeToAHalt
								ENDIF
							ELIF NOT arrestedWaiting
								//SLOW DOWN! - wait for the conversations to end. 
								PRINTSTRING("PAul NOT arrestedWaiting") PRINTNL()
					
								TASK_GO_TO_ENTITY(pedCop, pedCriminal, -1, 1, 0.01)
								
								arrestedWaiting = TRUE
							ENDIF
						ELSE
							PLAY_INSULT_DIALOGUE()
						ENDIF
					ELSE 
						DRAW_DEBUG_TEXT_2D("copChasesCriminal TIMERA() < 2000", <<0.02, 0.1, 0>>)
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(pedCop) AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedCop)
								CLEAR_PED_TASKS(pedCop)
								copCatchesCriminalStage = copStandStill
								TASK_STAND_STILL(pedCop, 500)
								//SCRIPT_ASSERT("PAUL")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE copStandStill
					IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_STAND_STILL) = FINISHED_TASK
						TASK_GO_TO_ENTITY(pedCop, pedCriminal, -1, 1, PEDMOVEBLENDRATIO_SPRINT)
						copCatchesCriminalStage = copChasesCriminal 
					ENDIF
				BREAK
				
				CASE pedWaitUntilComeToAHalt
					PRINTSTRING("PAul pedWaitUntilComeToAHalt") PRINTNL()
					IF ((GET_GAME_TIMER() - iArrestTimer) > 1000)
						crimHeading = <<0, 0, GET_ENTITY_HEADING(pedCriminal)*3.0>>
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedCriminal, crimHeading, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_ACCURATE_WALKRUN_START)
						copCatchesCriminalStage = pedSlowDownUntilComeToAHalt
					ENDIF				
				BREAK
				
				CASE pedSlowDownUntilComeToAHalt
					PRINTSTRING("PAul pedSlowDownUntilComeToAHalt") PRINTNL()
					
					/*IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT sayHandsUp
						IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PCUFF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							sayHandsUp = TRUE
						ENDIF
					ENDIF*/
						
					IF ((GET_GAME_TIMER() - iArrestTimer) > 4500)
						copCatchesCriminalStage = pedStandsUpWithHandsUp
					ENDIF
				BREAK
				
				CASE pedStandsUpWithHandsUp
					//CLEAR_PED_TASKS(pedCriminal)
					bCopHasArrestedCriminal = TRUE
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM (NULL, arrestAnimDict, "idle_2_hands_up", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
						TASK_PLAY_ANIM (NULL, arrestAnimDict, "kneeling_arrest_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedCriminal, seq)
					CLEAR_SEQUENCE_TASK(Seq)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCriminal)
					SET_PED_KEEP_TASK(pedCriminal, TRUE)
					REMOVE_ALL_PED_WEAPONS(pedCriminal)
					
					
					PRINTSTRING("PAUL-CREATE_PICKUP") PRINTNL()
					copCatchesCriminalStage = copWaitForEndOfCall
				BREAK
				
				CASE copWaitForEndOfCall
					
					/*IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT sayHandsUp
						IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PCUFF", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
							sayHandsUp = TRUE
						ENDIF
					ENDIF*/
					
					IF copCallInStage = copWaitsForRequestedUnit
						IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							CLEAR_PED_TASKS(pedCop)
							IF NOT HAS_PED_GOT_WEAPON(pedCop, WEAPONTYPE_PISTOL)
								GIVE_WEAPON_TO_PED(pedCop, WEAPONTYPE_PISTOL, -1, FALSE)
							ENDIF
							SET_CURRENT_PED_WEAPON(pedCop, WEAPONTYPE_PISTOL, TRUE)
							OPEN_SEQUENCE_TASK(seq)						
								TASK_AIM_GUN_AT_ENTITY(NULL, pedCriminal, -1)		
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedCop, seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedCop, TRUE)
							copCatchesCriminalStage = copWaitsForBackup
						ELSE	
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - copWaitForEndOfCall IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK")
						ENDIF
					ELSE	
						PRINTLN(GET_THIS_SCRIPT_NAME(), " - copWaitForEndOfCall IF copCallInStage <> copWaitsForRequestedUnit")
					ENDIF
					
					//Check to see if the player is between the cop and the criminal.
					
				BREAK
				
				CASE copWaitsForBackup
					
					IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
					AND NOT IS_PED_INJURED(pedCopGenerated)
					
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehCopCarGenerated, TRUE)
						
//						IF IS_ENTITY_AT_ENTITY(pedCop, vehCopCarGenerated, <<15, 15, 13>>)
						IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
						
							IF GET_SCRIPT_TASK_STATUS(pedCopGenerated, SCRIPT_TASK_VEHICLE_MISSION) = FINISHED_TASK
							
								IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedCriminal, vehCopCarGenerated, VS_BACK_RIGHT)
							
									IF NOT IS_PED_INJURED(pedCriminal)
										IF NOT IS_PED_SITTING_IN_VEHICLE(pedCriminal, vehCopCarGenerated)
											IF NOT IS_PED_IN_COMBAT(pedCriminal)
												IF GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
													//CLEAR_PED_TASKS(pedCriminal)
													
													//SET_PED_KEEP_TASK(pedCriminal, TRUE)
													OPEN_SEQUENCE_TASK(seq)
														//Check if we are playing the kneeling anim, if we are, play the getup anim. 
														//IF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_idle")
															TASK_PLAY_ANIM (NULL, arrestAnimDict, "kneeling_arrest_get_up", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
														//ENDIF
														//TASK_PLAY_ANIM (NULL, arrestAnimDict, "arrest_walk", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY|AF_LOOPING|AF_SECONDARY|AF_TAG_SYNC_CONTINUOUS|AF_NOT_INTERRUPTABLE)
													CLOSE_SEQUENCE_TASK(seq)
													TASK_PERFORM_SEQUENCE(pedCriminal, seq)
													CLEAR_SEQUENCE_TASK(Seq)
													SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relGrouparrestcrim, relGrouparrestcop)
													SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relGrouparrestcop, relGrouparrestcrim)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF NOT IS_PED_INJURED(pedCop)
										SET_PED_INCREASED_AVOIDANCE_RADIUS(pedCop)
										IF NOT IS_PED_IN_COMBAT(pedCop)
											IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
												//IF PLAY_AMBIENT_DIALOGUE_LINE(arrestConversation, pedCop, "REARRAU", "REARR_INCAR")
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_INCAR", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
														IF GET_NEAREST_PASSENGER_SIDE_TO_PED(pedCop, vehCopCarGenerated) = 2 // Right
															PRINTSTRING("Paul - right hamd side.") PRINTNL()
															IF IS_PED_FACING_PED(pedCopGenerated, pedCriminal, 180) //QUICK way of seeing if the crim is in front or behind the car
																vCopCarOffset = <<2.978,-0.75, 1.0>>
															ELSE
																vCopCarOffset = <<2.978,0.75, 1.0>>
															ENDIF
														ELSE
															PRINTSTRING("Paul - left hamd side.") PRINTNL()
															IF IS_PED_FACING_PED(pedCopGenerated, pedCriminal, 180) //QUICK way of seeing if the crim is in front or behind the car
																vCopCarOffset = <<-2.978,-0.75, 1.0>>
															ELSE
																vCopCarOffset = <<-2.978,0.75, 1.0>>
															ENDIF
														ENDIF
														OPEN_SEQUENCE_TASK(seq)
															//NATIVE PROC TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(PED_INDEX PedIndex, VECTOR GoToCoord, ENTITY_INDEX AimAtEntityIndex, FLOAT MoveBlendRatio, BOOL Shoot, FLOAT TargetDistance = 0.5, FLOAT SlowDistance = 4.0, BOOL UseNavMesh = TRUE, ENAV_SCRIPT_FLAGS iNavFlags=ENAV_DEFAULT, BOOL InstantBlendToAim = FALSE, FIRING_TYPE FiringPatternHash = FIRING_PATTERN_FULL_AUTO, INT iTimer = DEFAULT_TIME_BEFORE_WARP)

															TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCopCarGenerated, vCopCarOffset), pedCriminal, 0.75, FALSE, 0.5, 4.0, TRUE, ENAV_DEFAULT, FALSE, FIRING_TYPE_CONTINUOUS, 60000)
															TASK_AIM_GUN_AT_ENTITY(NULL, pedCriminal, -1)
														CLOSE_SEQUENCE_TASK(seq)
														TASK_PERFORM_SEQUENCE(pedCop, seq)
														CLEAR_SEQUENCE_TASK(Seq)
														SET_PED_KEEP_TASK(pedCop, TRUE)
														SET_VEHICLE_SIREN(vehCopCarGenerated, FALSE)
														
														//Set the tiemers for the speech for when the criminal is being taken back. 
														random_speech_wait = 8000
														SETTIMERB(0)
														
														arrestCriminalTalkTime = GET_GAME_TIMER()
														
														copCatchesCriminalStage = getToCopcar
													 ENDIF
												 ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
								
									CRIMINAL_ATTEMPTS_TO_ESCAPE()
								
								ENDIF
							ENDIF
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - Waiting for vehCopCarGenerated, or for cop to finish anim, or for crim to be kneeling.")
						ENDIF
					ENDIF
												
				BREAK
				
				CASE getToCopcar
					//IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehCopCarGenerated) + vCopCarOffset, <<1, 1, 1>>, TRUE)
					//DRAW_DEBUG_SPHERE (GET_ENTITY_COORDS(vehCopCarGenerated) + vCopCarOffset, 0.25)

					//DRAW_DEBUG_SPHERE (GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.0)

					IF NOT IS_PED_INJURED(pedCop)
					
						//Say lines as the criminal is being taken back to the car. 
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCop, <<35, 35, 35>>, FALSE, TRUE)
							IF ((GET_GAME_TIMER() - arrestCriminalTalkTime) > 8000) 
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									TASK_LOOK_AT_ENTITY(pedCop, PLAYER_PED_ID(), 6000, SLF_WHILE_NOT_IN_FOV)
									CREATE_CONVERSATION(arrestConversation, "REARRAU", "REARR_PLEAVE", CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
									random_speech_wait = 8000
									arrestCriminalTalkTime = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
							IF bCopHasOpenedDoor
								IF GET_NEAREST_PASSENGER_SIDE_TO_PED(pedCop, vehCopCarGenerated) = 2 // Right
									TASK_ENTER_VEHICLE(pedCriminal, vehCopCarGenerated, DEFAULT_TIME_NEVER_WARP, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK)
								ELSE
									TASK_ENTER_VEHICLE(pedCriminal, vehCopCarGenerated, DEFAULT_TIME_NEVER_WARP, VS_BACK_LEFT, PEDMOVEBLENDRATIO_WALK)
								ENDIF
								copCatchesCriminalStage = copLeaves
							ELSE
								IF bCrimHasStartedWalking
									carDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCriminal), GET_ENTITY_COORDS(vehCopCarGenerated))
									IF ((carDist < 4.0) AND (navMeshWalkingDone = FALSE))
										//TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(pedCriminal, vehCopCarGenerated, vCopCarOffset, PEDMOVEBLENDRATIO_WALK, 60000)
										
										TASK_GOTO_ENTITY_OFFSET_XY(pedCriminal, vehCopCarGenerated, 60000, 0.5, vCopCarOffset.x, vCopCarOffset.y, PEDMOVEBLENDRATIO_WALK,ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										PRINTSTRING("Paul - ((carDist < 8.0) AND (navMeshWalkingDone = FALSE))") PRINTNL()
										navMeshWalkingDone = TRUE
									ELIF (carDist < 3.0) //GET_SCRIPT_TASK_STATUS(pedCriminal, SCRIPT_TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY) = FINISHED_TASK
										CLEAR_PED_TASKS(pedCriminal)
										bCopHasOpenedDoor = TRUE
									ENDIF
								ELSE
									IF NOT IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_get_up")
										IF GET_NEAREST_PASSENGER_SIDE_TO_PED(pedCop, vehCopCarGenerated) = 2 // Right
											vCopCarOffset = <<1.578,-0.5, 1.0>>
											PRINTSTRING("ARREST - criminal going to right hand side") PRINTNL()
										ELSE
											vCopCarOffset = <<-1.578,-0.5, 1.0>>
											PRINTSTRING("ARREST - criminal going to left hand side") PRINTNL()
										ENDIF
										//TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(pedCriminal, vehCopCarGenerated, vCopCarOffset, PEDMOVEBLENDRATIO_WALK, 60000)
										TASK_GOTO_ENTITY_OFFSET_XY(pedCriminal, vehCopCarGenerated, 60000, 0.5, vCopCarOffset.x, vCopCarOffset.y, PEDMOVEBLENDRATIO_WALK, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										IF isPedTazered
											SET_PED_MOVEMENT_CLIPSET(pedCriminal, "MOVE_M@BAIL_BOND_TAZERED")
										ELSE
											SET_PED_MOVEMENT_CLIPSET(pedCriminal, "MOVE_M@BAIL_BOND_NOT_TAZERED")
										ENDIF
										
										//NATIVE PROC TASK_GOTO_ENTITY_OFFSET_XY( PED_INDEX PedIndex, ENTITY_INDEX EntityIndex, INT Time = DEFAULT_TIME_BEFORE_WARP, FLOAT fTargetRadius = DEFAULT_SEEK_RADIUS, FLOAT fXOffset = 0.0, FLOAT fYOffset = 0.0, FLOAT MoveBlendRatio = PEDMOVEBLENDRATIO_RUN, ESEEK_ENTITY_OFFSET_FLAGS OffsetFlags = ESEEK_DEFAULT )

										bCrimHasStartedWalking = TRUE
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE copLeaves
					
					IF IS_VEHICLE_DRIVEABLE(vehCopCarGenerated)
						IF NOT IS_PED_INJURED(pedCopGenerated)
							IF IS_PED_SITTING_IN_VEHICLE(pedCriminal, vehCopCarGenerated)
								IF DOES_BLIP_EXIST(blipCop)
									REMOVE_BLIP(blipCop)
								ENDIF
								IF DOES_BLIP_EXIST(blipCriminal)
									REMOVE_BLIP(blipCriminal)
								ENDIF
								IF IS_PED_SITTING_IN_VEHICLE(pedCop, vehCopCarGenerated)
									TASK_VEHICLE_DRIVE_WANDER(pedCopGenerated, vehCopCarGenerated, 10, DRIVINGMODE_STOPFORCARS)
									SET_PED_KEEP_TASK(pedCopGenerated, TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop, FALSE)
									missionCleanup()
								ELSE
									IF GET_SCRIPT_TASK_STATUS(pedCop, SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
										CLEAR_PED_TASKS(pedCop)
										TASK_ENTER_VEHICLE(pedCop, vehCopCarGenerated, DEFAULT, VS_FRONT_RIGHT, PEDMOVE_WALK)
										SET_PED_KEEP_TASK(pedCop, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	ENDIF

	
	check_player_damaged_ped()
	
	IF IS_PED_INJURED(pedCriminal)
		//SCRIPT_ASSERT("Paul  = pedCriminal dead now!") PRINTNL()
	ENDIF
	
	IF IS_PED_INJURED(pedCop)
		PRINTSTRING("Paul - or why are we here?") PRINTNL()
		IF NOT IS_PED_INJURED(pedCriminal)
			PRINTSTRING("Paul - stage_check_injured_state") PRINTINT(ENUM_TO_INT(stage_check_injured_state)) PRINTNL()
			IF stage_check_injured_state = JUST_INJURED_THE_COP
				PRINTSTRING("PAUL JUST_INJURED_THE_COP") PRINTNL()
				criminal_thanks()
			ELSE
				PRINTSTRING("PAUL INJURED BOTH") PRINTNL()
				IF stage_check_injured_state = ATTACKED_BOTH
				OR stage_check_injured_state = JUST_INJURED_THE_CRIM
					//Say thanks, but reluctantly. You have injured him then saved him. 
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCriminal), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20.0)
							PRINTSTRING("Paul annoyed thanks.") PRINTNL()
							CREATE_CONVERSATION(arrestConversation, "REARRAU", crimAnnoyedThanks, CONV_PRIORITY_AMBIENT_HIGH, subtitleState)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedCriminal, arrestAnimDict, "kneeling_arrest_idle")
					PRINTSTRING("Arrest - criminal on ground") PRINTNL()
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM (NULL, arrestAnimDict, "kneeling_arrest_get_up", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						//CLEAR_PED_TASKS(NULL)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1)
						//SET_PED_KEEP_TASK(NULL, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedCriminal, seq)
					CLEAR_SEQUENCE_TASK(Seq)
				ELSE
					TASK_SMART_FLEE_PED(pedCriminal, PLAYER_PED_ID(), 150, -1)
					PRINTSTRING("PAUL TASK_SMART_FLEE_PED") PRINTNL()
					SET_PED_KEEP_TASK(pedCriminal, TRUE)
				ENDIF
				WAIT(0)
				IF stage_check_injured_state = IGNORED_BOTH
					stage_check_injured_state = JUST_INJURED_THE_COP
				ENDIF
				missionCleanup()
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_PED_INJURED(pedCriminal)
		IF NOT cop_attacks_player
			IF HAS_COP_THANKED_PLAYER()
				COP_CALLS_IN_RESULT(FALSE)
			ENDIF
		ENDIF
	ENDIF
		
	IF copCatchesCriminalStage = copWaitForEndOfCall
	OR copCatchesCriminalStage = copWaitsForBackup
		IF NOT cop_attacks_player
			IF stage_check_injured_state = JUST_INJURED_THE_CRIM
				IF HAS_COP_THANKED_PLAYER()
					COP_CALLS_IN_RESULT(FALSE)
				ENDIF
			ELSE
				PRINTSTRING("COP_CALLS_IN_RESULT player done nothing") PRINTNL()
				COP_CALLS_IN_RESULT()
			ENDIF
		ENDIF
	ENDIF
	
	SET_SUB_DISPLAY_BASED_ON_DISTANCE()

ENDPROC

FUNC BOOL IS_SCENE_IN_RANGE(FLOAT fRange = 200.0)
	BOOL bReturn = FALSE
	INT iCheckCounter
	VECTOR vTemp = <<fRange, fRange, 50>>
	
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), a1, a2, 194.750000)
		//Don't quit if we are in the area...
		return TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedCop)
		IF IS_ENTITY_AT_ENTITY(pedCop, PLAYER_PED_ID(), vTemp)
			bReturn = TRUE
		ELSE
			iCheckCounter++
		ENDIF
	ELSE
		iCheckCounter++
	ENDIF
	IF NOT IS_PED_INJURED(pedCriminal)
		IF IS_ENTITY_AT_ENTITY(pedCriminal, PLAYER_PED_ID(), vTemp)
			bReturn = TRUE
		ELSE
			iCheckCounter++
		ENDIF
	ELSE
		iCheckCounter++
	ENDIF
	
	IF iCheckCounter = 2
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_VARIATION_COMPLETE()
	BOOL bReturn = FALSE
	
	SWITCH iCurrentArrest
		CASE ARREST_1	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ARR1)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE ARREST_2
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ARR2)
				bReturn = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bReturn
ENDFUNC

PROC RUN_COP_TO_CRIM()
	IF NOT bCopHasArrestedCriminal
	AND copAttendsDeadCriminalStage = copThanksPlayer
		IF NOT IS_PED_INJURED(pedCop)
		AND NOT IS_PED_INJURED(pedCriminal)
			TASK_GO_STRAIGHT_TO_COORD(pedCop, GET_ENTITY_COORDS(pedCriminal), PEDMOVEBLENDRATIO_SPRINT)
			SET_PED_KEEP_TASK(pedCop, TRUE)
			WAIT(0)
		ENDIF
	ENDIF
ENDPROC


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_arrests launched at ")
PRINTVECTOR(vInput)
PRINTNL()

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	PRINTSTRING("\nre_arrests - DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS\n")
	bForceCleanupOccurred = TRUE
	RUN_COP_TO_CRIM()
	missionCleanup()
ENDIF


//////////////////////////////////

#IF IS_DEBUG_BUILD

//    WIDGET_ID missionTab
//    WIDGET_ID holdMissionsWidget

//    missionTab = 
	START_WIDGET_GROUP("re_arrest") 
		ADD_WIDGET_BOOL("Force arrest the criminal", bForceArrest)
    STOP_WIDGET_GROUP()

#ENDIF


IF VDIST(<< 2411.32, 4958.76, 45.19 >>, vInput) < 10
	iCurrentArrest = ARREST_1 // Hay
ELSE
	iCurrentArrest = ARREST_2 // Highway
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_ARREST, iCurrentArrest)
	LAUNCH_RANDOM_EVENT(RE_ARREST)
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
		//IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	SWITCH arrestStage

		CASE initialiseEvent
			IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE() AND NOT SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS() AND NOT SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				IF HAS_AMBIENT_EVENT_LOADED()
					arrestStage = stageWaitForInteraction
				ENDIF
			ELSE
				missionCleanup()
			ENDIF
		BREAK
		
		CASE stageWaitForInteraction
		
			IF HAS_PLAYER_STARTED_INTERACTION()
				SET_ROADS_IN_ANGLED_AREA(<<2551.0378, 4708.6133, 32.6775>>, <<2536.9790, 5022.1787, 43.8519>>, 300.0, FALSE, TRUE)
				SET_WANTED_LEVEL_MULTIPLIER(0)
				SET_RANDOM_EVENT_ACTIVE()
				arrestStage = STAGE_RUN_ARREST
			ELIF NOT IS_SCENE_IN_RANGE(200)
				missionCleanup()	
			ENDIF
			
		BREAK
		
		CASE STAGE_RUN_ARREST
			IF IS_SCENE_IN_RANGE()
				run_arrest()
			ELSE
				missionCleanup()
			ENDIF
			
		BREAK

	ENDSWITCH		

	#IF IS_DEBUG_BUILD 
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			stage_check_injured_state = JUST_INJURED_THE_COP
			missionCleanup()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			missionCleanup()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
			SET_ENTITY_HEALTH(pedCop, 0)
		ENDIF
	#ENDIF
	
ENDWHILE


ENDSCRIPT


