//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "lineactivation.sch"
USING "Ambient_Common.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM Drug_deal_STAGES
	STAGE_CREATE_GANG,
	STAGE_RUN_DRUG_DEAL
ENDENUM
Drug_deal_STAGES Drug_dealStage = STAGE_CREATE_GANG

ENUM RUN_DEALGONEWRONG_STAGES
	DEAL_SETUP,
	DEAL_FUDGE_ACTIVATION_RANGE,
	DEAL_FOLLOW_CHASE,
	DEAL_CLEAN_UP
ENDENUM
RUN_DEALGONEWRONG_STAGES dealgonewrong = DEAL_SETUP

ENUM RUN_GANG_ARRIVE_STAGE
	GANG_ARRIVE_NEAR_ROAD,
	GANG_CHAT,
	GANG_DELAY,
	GANG_ARRIVE_SPAWN_GANG1,
	GANG_ARRIVE_SPAWN_GANG2,
	GANG_ARRIVE_SPAWN_BIKES,
	SHOOT_CASE,
	GANG_ARRIVE_CLEANUP
ENDENUM
RUN_GANG_ARRIVE_STAGE gang_arrive_stage = GANG_ARRIVE_NEAR_ROAD

CONST_INT NUM_DEAD_PEDS 9
CONST_INT NUM_DEAD_DOGS 2
CONST_INT NUM_GANG_CARS 5
CONST_INT NUM_GANGA 4
CONST_INT NUM_GANGB 4
CONST_INT NUM_GANG_WEAPONS 5

VECTOR vInput

VECTOR vDeadPedGang[NUM_DEAD_PEDS]
FLOAT fDeadPedGang[NUM_DEAD_PEDS]

VECTOR vPedCood
VECTOR vPedCoodShootFrom
VECTOR vPedCoodShootTo

VECTOR vVehicleCoord
VECTOR vVehicleCoordShootFrom
VECTOR vVehicleCoordShootTo

VECTOR vGangWarpPosition
FLOAT fGangWarpHeading
//VECTOR vPlayerWarpPosition
//FLOAT fPlayerWarpHeading
FLOAT fPlayerHeadings
VECTOR vPlayerPositions
INT iTimerWarp
TWEAK_FLOAT BLIP_SLIDE_AMOUNT 1.0

VECTOR vDyingGangGuy

FLOAT fRandomfX
FLOAT fRandomfY
FLOAT fRandomfZ

FLOAT fDyingGangGuy//, returned_heading

INT iGangAKilled
INT iGangBKilled

INT iCounterCurrentTimeDying
INT iCounterTimeStartedDying//gang_arrive2, counter_current_time_jumper, counter_time_started_jumper 

INT iGangDyingScene0
INT iGangDyingScene1

INT iGangSpeechStage
INT iGangDyingScene2

INT randomised_node
INT i

INT iNumberOfShots
INT iShotsTimer

PED_INDEX pedDyingGangGuy
PED_INDEX pedDeadPed[NUM_DEAD_PEDS]
PED_INDEX pedDeadDog[NUM_DEAD_DOGS]

PED_INDEX pedGangChasePedA[NUM_GANGA]
PED_INDEX pedGangChasePedB[NUM_GANGB]

VEHICLE_INDEX vehGangCar[NUM_GANG_CARS]

VEHICLE_INDEX vehGangChaseCarA[NUM_GANGA]
VEHICLE_INDEX vehGangChaseCarB[NUM_GANGB]
VEHICLE_INDEX vehGangChaseBikeA[NUM_GANGA]

BLIP_INDEX blipRandomEvent
BLIP_INDEX blipBriefcase

BLIP_INDEX blipDyingGangGuy
BLIP_INDEX blipDeadPeds[NUM_DEAD_PEDS]

BLIP_INDEX blipGangChasePedA[NUM_GANGA]
BLIP_INDEX blipGangChasePedB[NUM_GANGB]
BLIP_INDEX blipVehGangChaseA[NUM_GANGA]
BLIP_INDEX blipVehGangChaseB[NUM_GANGB]

PICKUP_INDEX pickupBriefcase
PICKUP_INDEX pickupGang[NUM_GANG_WEAPONS]

DECAL_ID decalBlood[12]
//STREAMVOL_ID streaming_volume
//CAMERA_INDEX ciDeadPed
//		PICKUP_INDEX pickupBriefcase2

//ENTITY_INDEX briefcase
//enumCharacterList current_player_enum
VECTOR vDealLocation = << -1635.6484, 4737.8013, 52.4304 >>
VECTOR vBriefcaseCoods = << -1640.7454, 4696.3862, 39.2790 >>//<< -1639.6936, 4690.4463, 37.9500 >>
//VECTOR spawn_distance = << 50, 50, 30 >>
VECTOR vReturnedClosestNode
VECTOR vPlayersPosition
VECTOR vPickupGang[NUM_GANG_WEAPONS]
//VECTOR bridge_vector = << -1856.3953, 4653.9287, 56.1612 >>
//VECTOR highway_vector = << -1434.7699, 5064.7783, 60.3333 >>

BOOL bPreCleanup
BOOL bSplatterBlood
BOOL bGang1Spawned
BOOL bGang2Spawned
BOOL bPlayerKilled

BOOL bBikesCreated//, bDogCreated //APPLIED_FORCE[2] //, sync_scene_over //, FORCE_FUDGE, safe_to_spawn_higway_gang

//CAMERA_INDEX CamIDPickingUpcase
//SEQUENCE_INDEX seq
REL_GROUP_HASH rghGroupGangA, rghGroupGangB
REL_GROUP_HASH rghDealGoneWrong

//COMMONENTITYAMBIENTSCENE briefcase_scene
structPedsForConversation dealgonewrongConversation
VECTOR vTemp
VECTOR vSetPieceGangVector[4]
VECTOR vDiffVector
//OBJECT_INDEX magic_binbag
BOOL bAssetsLoaded, bLoadDealerAssets

#IF IS_DEBUG_BUILD //WIDGETS
	INT iRunDrug_dealStage
	BOOL bRunDebugSelection
	VECTOR v_debug_warp
	FLOAT debug_warp_heading
	INT debug_jump
#ENDIF

// Functions ----------------------------------------------//
FUNC BOOL IS_ENEMY_LOST()
	BOOL bEnemies_lost = TRUE
	
	IF NOT bLoadDealerAssets
		bEnemies_lost = FALSE
	ENDIF
	
	INT index
	REPEAT NUM_GANGA index
		IF DOES_ENTITY_EXIST(pedGangChasePedA[index])
			IF NOT IS_PED_INJURED(pedGangChasePedA[index])
				IF IS_ENTITY_AT_ENTITY(pedGangChasePedA[index], PLAYER_PED_ID(), << 200, 200, 200 >>)
					bEnemies_lost = FALSE
//				ELSE DOES_BLIP_EXIST(blipGangChasePedA[index])
//					REMOVE_BLIP(blipGangChasePedA[index])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT NUM_GANGB index
		IF DOES_ENTITY_EXIST(pedGangChasePedB[index])
			IF NOT IS_PED_INJURED(pedGangChasePedB[index])
				IF IS_ENTITY_AT_ENTITY(pedGangChasePedB[index], PLAYER_PED_ID(), << 200, 200, 200 >>)
					bEnemies_lost = FALSE
//				ELSE DOES_BLIP_EXIST(blipGangChasePedB[index])
//					REMOVE_BLIP(blipGangChasePedB[index])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bEnemies_lost
ENDFUNC

// Clean Up
PROC missionCleanup()

	PRINTLN("\n @@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@ \n")
	
	IF bAssetsLoaded
	AND NOT bPreCleanup
//		#IF IS_DEBUG_BUILD
//			IF can_cleanup_script_for_debug
//			
//				INT index
//				REPEAT NUM_DEAD_PEDS index
//					IF DOES_ENTITY_EXIST(pedDeadPed[index])
//						DELETE_PED(pedDeadPed[index])
//					ENDIF
//				ENDREPEAT
//				
//				REPEAT NUM_GANG_CARS index
//					IF DOES_ENTITY_EXIST(vehGangCar[index])
//						DELETE_VEHICLE(vehGangCar[index])
//					ENDIF
//				ENDREPEAT
//				
//				IF DOES_PICKUP_EXIST(pickupBriefcase)
//					REMOVE_PICKUP(pickupBriefcase)
//				ENDIF
//				
//				REPEAT NUM_DEAD_DOGS index
//					IF DOES_ENTITY_EXIST(pedDeadDog[index])
//						DELETE_PED(pedDeadDog[index])
//					ENDIF
//				ENDREPEAT
//			ENDIF
//		#ENDIF
		
//		SET_ROADS_BACK_TO_ORIGINAL(<< -1567.3665, 4676.6924, 36.8010 >>, << -1468.0657, 5000.6758, 76.7051 >>)
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		IF DOES_BLIP_EXIST(blipRandomEvent)
			REMOVE_BLIP(blipRandomEvent)
		ENDIF
		IF DOES_BLIP_EXIST(blipBriefcase)
			REMOVE_BLIP(blipBriefcase)
		ENDIF
		
		INT index
		REPEAT NUM_GANGA index
			IF DOES_BLIP_EXIST(blipGangChasePedA[index])
				REMOVE_BLIP(blipGangChasePedA[index])
			ENDIF
			IF DOES_BLIP_EXIST(blipVehGangChaseA[index])
				REMOVE_BLIP(blipVehGangChaseA[index])
			ENDIF
			IF gang_arrive_stage >= SHOOT_CASE
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedGangChasePedA[index])
						IF IS_ENTITY_AT_ENTITY(pedGangChasePedA[index], PLAYER_PED_ID(), << 100, 100, 100 >>)
							bPlayerKilled = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT NUM_GANGB index
			IF DOES_BLIP_EXIST(blipGangChasePedB[index])
				REMOVE_BLIP(blipGangChasePedB[index])
			ENDIF
			IF DOES_BLIP_EXIST(blipVehGangChaseB[index])
				REMOVE_BLIP(blipVehGangChaseB[index])
			ENDIF
			IF gang_arrive_stage >= SHOOT_CASE
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedGangChasePedB[index])
						IF IS_ENTITY_AT_ENTITY(pedGangChasePedB[index], PLAYER_PED_ID(), << 100, 100, 100 >>)
							bPlayerKilled = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF Drug_dealStage >= STAGE_RUN_DRUG_DEAL
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENDIF
		
		IF bPlayerKilled
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 25000)
		ENDIF
	ENDIF
	
	PRINTLN("re_DealGoneWrong cleaned up")
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	dealgonewrong = DEAL_CLEAN_UP
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	dealgonewrong = DEAL_CLEAN_UP
ENDPROC

PROC preCleanup()
	IF bAssetsLoaded
	AND NOT bPreCleanup
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		IF DOES_BLIP_EXIST(blipRandomEvent)
			REMOVE_BLIP(blipRandomEvent)
		ENDIF
		IF DOES_BLIP_EXIST(blipBriefcase)
			REMOVE_BLIP(blipBriefcase)
		ENDIF
		
		INT index
		REPEAT NUM_GANGA index
			IF DOES_BLIP_EXIST(blipGangChasePedA[index])
				REMOVE_BLIP(blipGangChasePedA[index])
			ENDIF
			IF DOES_BLIP_EXIST(blipVehGangChaseA[index])
				REMOVE_BLIP(blipVehGangChaseA[index])
			ENDIF
			IF gang_arrive_stage >= SHOOT_CASE
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedGangChasePedA[index])
						IF IS_ENTITY_AT_ENTITY(pedGangChasePedA[index], PLAYER_PED_ID(), << 100, 100, 100 >>)
							bPlayerKilled = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT NUM_GANGB index
			IF DOES_BLIP_EXIST(blipGangChasePedB[index])
				REMOVE_BLIP(blipGangChasePedB[index])
			ENDIF
			IF DOES_BLIP_EXIST(blipVehGangChaseB[index])
				REMOVE_BLIP(blipVehGangChaseB[index])
			ENDIF
			IF gang_arrive_stage >= SHOOT_CASE
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedGangChasePedB[index])
						IF IS_ENTITY_AT_ENTITY(pedGangChasePedB[index], PLAYER_PED_ID(), << 100, 100, 100 >>)
							bPlayerKilled = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF Drug_dealStage >= STAGE_RUN_DRUG_DEAL
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENDIF
		
		IF bPlayerKilled
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 25000)
		ENDIF
		
		bPreCleanup = TRUE
	ENDIF
	
	// We do the following to keep the cars around until player leaves the area
	IF NOT IS_SPHERE_VISIBLE(vDealLocation, 30)
	AND VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vDealLocation) > 300
		missionCleanup()
	ENDIF
	
ENDPROC

PROC shootUpVehicles()
	
	INT index
	REPEAT NUM_GANG_CARS index
		WAIT(0)
		IF DOES_ENTITY_EXIST(vehGangCar[index])
			//vVehicleCoord = GET_ENTITY_COORDS(vehGangCar[index])
			vVehicleCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehGangCar[index], << 0, 0, 0.5 >>)
		ENDIF
		
		REPEAT 5 i
			fRandomfX = GET_RANDOM_FLOAT_IN_RANGE(1.5, 2.5)
			fRandomfY = GET_RANDOM_FLOAT_IN_RANGE(2.0, 4.0)
			fRandomfZ = GET_RANDOM_FLOAT_IN_RANGE(0.1, 2.0)
			
			WAIT(0)
			
			vVehicleCoordShootFrom = << vVehicleCoord.X -fRandomfX, vVehicleCoord.Y -fRandomfY, vVehicleCoord.Z +fRandomfZ >>
			vVehicleCoordShootTo = << vVehicleCoord.X +fRandomfX, vVehicleCoord.Y +fRandomfY, vVehicleCoord.Z >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vVehicleCoordShootFrom, vVehicleCoordShootTo, 1, FALSE)
			vVehicleCoordShootFrom = << vVehicleCoord.X +fRandomfX, vVehicleCoord.Y +fRandomfY, vVehicleCoord.Z +fRandomfZ >>
			vVehicleCoordShootTo = << vVehicleCoord.X -fRandomfX, vVehicleCoord.Y -fRandomfY, vVehicleCoord.Z >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vVehicleCoordShootFrom, vVehicleCoordShootTo, 1, FALSE)
			vVehicleCoordShootFrom = << vVehicleCoord.X +fRandomfX, vVehicleCoord.Y -fRandomfY, vVehicleCoord.Z +fRandomfZ >>
			vVehicleCoordShootTo = << vVehicleCoord.X -fRandomfX, vVehicleCoord.Y +fRandomfY, vVehicleCoord.Z >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vVehicleCoordShootFrom, vVehicleCoordShootTo, 1, FALSE)
			vVehicleCoordShootFrom = << vVehicleCoord.X -fRandomfX, vVehicleCoord.Y +fRandomfY, vVehicleCoord.Z +fRandomfZ >>
			vVehicleCoordShootTo = << vVehicleCoord.X +fRandomfX, vVehicleCoord.Y -fRandomfY, vVehicleCoord.Z >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vVehicleCoordShootFrom, vVehicleCoordShootTo, 1, FALSE)
			vVehicleCoordShootFrom = << vVehicleCoord.X, vVehicleCoord.Y +fRandomfY, vVehicleCoord.Z +fRandomfZ >>
			vVehicleCoordShootTo = << vVehicleCoord.X, vVehicleCoord.Y -fRandomfY, vVehicleCoord.Z >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vVehicleCoordShootFrom, vVehicleCoordShootTo, 1, FALSE)
		ENDREPEAT
		
	ENDREPEAT
	
ENDPROC

PROC bloodSpats()
	
	INT index
	REPEAT NUM_DEAD_PEDS index
//		pedDeadPed_vector = GET_pedDeadPed_PICKUP_COORDS( pedDeadPed[index])
//		START_PARTICLE_FX_NON_LOOPED_AT_COORD("blood_mouth", vDeadPedGang[index], <<0, 0, 0>>, 1.0)
//		START_PARTICLE_FX_NON_LOOPED_AT_COORD("blood_exit", vDeadPedGang[index], <<0, 0, 0>>, 1.0)
//		START_PARTICLE_FX_NON_LOOPED_AT_COORD("blood_mouth", GET_ENTITY_COORDS(pedDeadPed[index]), <<0, 0, 0>>, 1.0)
	ENDREPEAT
	
// ADD_DECAL and pass in 1010 as the decal id (the first argument)


ENDPROC

PROC loadAssets()
	
	REQUEST_MODEL(G_M_M_ChiGoon_02)
	REQUEST_MODEL(G_M_Y_SalvaGoon_03)
	REQUEST_MODEL(rebel)
	REQUEST_MODEL(BobcatXL)
	REQUEST_MODEL(SANCHEZ)
	REQUEST_MODEL(A_C_Rottweiler)
	REQUEST_MODEL(Prop_Security_case_01)
	REQUEST_ANIM_DICT("random@dealgonewrong")
	REQUEST_ANIM_DICT("random@dealgonewrongdead_peds")
	
	IF HAS_MODEL_LOADED(G_M_M_ChiGoon_02)
	AND HAS_MODEL_LOADED(G_M_Y_SalvaGoon_03)
	AND HAS_MODEL_LOADED(rebel)
	AND HAS_MODEL_LOADED(BobcatXL)
	AND HAS_MODEL_LOADED(SANCHEZ)
	AND HAS_MODEL_LOADED(A_C_Rottweiler)
	AND HAS_MODEL_LOADED(Prop_Security_case_01)
	AND HAS_ANIM_DICT_LOADED("random@dealgonewrong")
	AND HAS_ANIM_DICT_LOADED("random@dealgonewrongdead_peds")
		
//		SET_ROADS_IN_AREA(<< -1567.3665, 4676.6924, 36.8010 >>, << -1468.0657, 5000.6758, 76.7051 >>, FALSE)
		ADD_SCENARIO_BLOCKING_AREA(vDealLocation - << 150, 150, 150 >>, vDealLocation + << 150, 150, 150 >>)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(BobcatXL, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(rebel, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		
		vehGangCar[0] = CREATE_VEHICLE(BobcatXL, << -1626.9401, 4729.8892, 51.3463  >>, 347.1188)
		SET_ENTITY_AS_MISSION_ENTITY(vehGangCar[0])
		SET_VEHICLE_ON_GROUND_PROPERLY(vehGangCar[0])
		SET_VEHICLE_ENGINE_ON(vehGangCar[0], TRUE, TRUE)
		SET_VEHICLE_LIGHTS(vehGangCar[0], FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_ENGINE_HEALTH(vehGangCar[0], 500)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehGangCar[0], 500)
		SET_VEHICLE_EXTRA(vehGangCar[0], 1, TRUE)
		SET_VEHICLE_TYRE_BURST(vehGangCar[0], SC_WHEEL_CAR_FRONT_LEFT)
		SMASH_VEHICLE_WINDOW(vehGangCar[0], SC_WINDSCREEN_FRONT)
		SET_VEH_RADIO_STATION(vehGangCar[0], "RADIO_06_COUNTRY")
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangCar[0], "76JON418")
		SET_VEHICLE_RADIO_ENABLED(vehGangCar[0], TRUE)
		
		vehGangCar[1] = CREATE_VEHICLE(rebel, << -1619.4318, 4747.7925, 52.7502 >>, 146.6017)
		SET_ENTITY_AS_MISSION_ENTITY(vehGangCar[1])
		SET_VEHICLE_ON_GROUND_PROPERLY(vehGangCar[1])//		SET_ENTITY_PROOFS(vehGangCar[1], TRUE, TRUE, TRUE, TRUE, TRUE) //TEMP
		SET_VEHICLE_ENGINE_ON(vehGangCar[1], TRUE, TRUE)
		SET_VEHICLE_LIGHTS(vehGangCar[1], FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_ENGINE_HEALTH(vehGangCar[1], 600)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehGangCar[1], 400)
		SET_VEHICLE_EXTRA(vehGangCar[1], 1, TRUE)
		SET_VEHICLE_EXTRA(vehGangCar[1], 2, TRUE)
		SET_VEHICLE_EXTRA(vehGangCar[1], 3, FALSE)
		SET_VEHICLE_EXTRA(vehGangCar[1], 4, FALSE)
		SMASH_VEHICLE_WINDOW(vehGangCar[1], SC_WINDSCREEN_FRONT)
		SET_VEH_RADIO_STATION(vehGangCar[1], "RADIO_06_COUNTRY")
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangCar[1], "79ERK121")
		SET_VEHICLE_RADIO_ENABLED(vehGangCar[1], TRUE)
		SET_VEHICLE_RADIO_LOUD(vehGangCar[1], TRUE)
//		SET_VEHICLE_TYRE_BURST(vehGangCar[1], SC_WHEEL_CAR_FRONT_LEFT)
		
		vehGangCar[2] = CREATE_VEHICLE(BobcatXL, << -1636.2161, 4748.0605, 51.5484 >>, 199.3236)
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangCar[2], "83JOH802")
		SET_ENTITY_AS_MISSION_ENTITY(vehGangCar[2])
		SET_VEHICLE_ON_GROUND_PROPERLY(vehGangCar[2])//		SET_ENTITY_PROOFS(vehGangCar[2], TRUE, TRUE, TRUE, TRUE, TRUE) //TEMP
		SET_VEHICLE_ENGINE_ON(vehGangCar[2], TRUE, TRUE)
		SET_VEHICLE_LIGHTS(vehGangCar[2], FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_ENGINE_HEALTH(vehGangCar[2], 60)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehGangCar[2], 1000)
		SET_VEHICLE_EXTRA(vehGangCar[2], 1, FALSE)
		SET_VEHICLE_TYRE_BURST(vehGangCar[2], SC_WHEEL_CAR_FRONT_RIGHT)
//		SET_VEHICLE_TYRE_BURST(vehGangCar[2], SC_WHEEL_CAR_REAR_LEFT)
		
		vehGangCar[3] = CREATE_VEHICLE(rebel, << -1641.5785, 4736.7832, 52.5984 >>, 278.1865)
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangCar[3], "85TOR114")
		SET_ENTITY_AS_MISSION_ENTITY(vehGangCar[3])
		SET_VEHICLE_ON_GROUND_PROPERLY(vehGangCar[3])
		SET_VEHICLE_ENGINE_ON(vehGangCar[3], TRUE, TRUE)
		SET_VEHICLE_LIGHTS(vehGangCar[3], FORCE_VEHICLE_LIGHTS_ON)
		SET_VEHICLE_ENGINE_HEALTH(vehGangCar[3], 500)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehGangCar[3], 500)
		SET_VEHICLE_EXTRA(vehGangCar[3], 1, FALSE)
		SET_VEHICLE_EXTRA(vehGangCar[3], 2, FALSE)
		SET_VEHICLE_EXTRA(vehGangCar[3], 3, FALSE)
		SET_VEHICLE_EXTRA(vehGangCar[3], 4, FALSE)
		SET_VEHICLE_TYRE_BURST(vehGangCar[3], SC_WHEEL_CAR_FRONT_LEFT)
		SET_VEHICLE_TYRE_BURST(vehGangCar[3], SC_WHEEL_CAR_REAR_RIGHT)
		
		IF NOT DOES_PICKUP_EXIST(pickupBriefcase)
//			pickupBriefcase = CREATE_OBJECT(PROP_LD_CASE_01, << -1640.2395, 4688.0044, 40.3856 >>)//<< -1640.3750, 4691.2417, 41.5 >>) //Briefcase
			INT iPlacementFlags = 0
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			pickupBriefcase = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, vBriefcaseCoods, <<-72.0000, 0.0000, 42.4800>>, iPlacementFlags, 25000, EULER_YXZ, TRUE, Prop_Security_case_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Security_case_01)
//			FREEZE_ENTITY_POSITION(pickupBriefcase, TRUE)
		ENDIF
		
		vPickupGang[0] = << -1640.4106, 4740.7017, 52.6218 >>
		vPickupGang[1] = << -1624.6636, 4746.5693, 51.5410 >>
		vPickupGang[2] = << -1626.3386, 4734.2324, 51.6176 >>
		vPickupGang[3] = << -1634.2968, 4741.2134, 51.9737 >>
		vPickupGang[4] = << -1644.7166, 4736.4463, 52.3014 >>
		
//		REQUEST_COLLISION_AT_COORD(vPickupGang[0])
//		REQUEST_COLLISION_AT_COORD(vPickupGang[1])
//		REQUEST_COLLISION_AT_COORD(vPickupGang[2])
//		REQUEST_COLLISION_AT_COORD(vPickupGang[3])
		
		INT index
		REPEAT NUM_GANG_WEAPONS index
			IF NOT DOES_PICKUP_EXIST(pickupGang[index])
		//		pickupBriefcase = CREATE_OBJECT(PROP_LD_CASE_01, << -1640.2395, 4688.0044, 40.3856 >>)//<< -1640.3750, 4691.2417, 41.5 >>) //Briefcase
				INT iPlacementFlags = 0
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				IF index < 2
					pickupGang[index] = CREATE_PICKUP(PICKUP_WEAPON_SAWNOFFSHOTGUN, vPickupGang[index], iPlacementFlags)
				ELIF index = 4
					pickupGang[index] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, vPickupGang[index], <<90.0000, 50.0000, 0.0000>>, 0, 50)
				ELSE
					pickupGang[index] = CREATE_PICKUP(PICKUP_WEAPON_PISTOL, vPickupGang[index], iPlacementFlags)
				ENDIF
			ENDIF
		ENDREPEAT
		
//		IF NOT DOES_PICKUP_EXIST(pickupBriefcase2)
//	//		pickupBriefcase = CREATE_OBJECT(PROP_LD_CASE_01, << -1640.2395, 4688.0044, 40.3856 >>)//<< -1640.3750, 4691.2417, 41.5 >>) //Briefcase
//
//			pickupBriefcase2 = CREATE_PICKUP(PICKUP_WEAPON_MINIGUN, << -1633.2225, 4722.7231, 52.5154 >>)
//
//			PRINTNL()
//			PRINTSTRING("CREATED PICKUP 2!!!!!!!!!!!!!!!!!!!")
//			PRINTNL()
//			PRINTNL()
//		ENDIF
		
		
//		pickupBriefcase = CREATE_PICKUP(PICKUP_WEAPON_PISTOL, << -1640.2395, 4688.0044, 37.7 >>)
		
		vDeadPedGang[0] = << -1636.535, 4708.455, 46.090 >>//<< -1637.5520, 4693.1270, 38.5407 >> // Dead guy close to briefcase guy
		fDeadPedGang[0] = 177.0024
		
		vDeadPedGang[1] = << -1621.1903, 4744.8677, 52.4254 >>
		fDeadPedGang[1] = 146.9645
		
		vDeadPedGang[2] = << -1622.7946, 4735.9746, 51.4740 >>
		fDeadPedGang[2] = 48.6546
		
		vDeadPedGang[3] = << -1632.6947, 4742.9058, 51.8604 >>
		fDeadPedGang[3] = 155.0678
		
		vDeadPedGang[4] = << -1629.5437, 4738.5996, 52.1784 >>
		fDeadPedGang[4] = 278.3085
		
		vDeadPedGang[5] = << -1634.7644, 4745.1875, 51.3926 >>
		fDeadPedGang[5] = 270.3501
		
		vDeadPedGang[7] = << -1628.3207, 4731.5024, 51.7644 >> // Still alive by car
		fDeadPedGang[7] = 352.9167
		
		vDeadPedGang[6] = << -1640.3105, 4738.4, 52.2 >> // Still alive by car
		fDeadPedGang[6] = 34.1111
		
		vDyingGangGuy = << -1641.735, 4692.130, 39.394 >>//<< -1641.5676, 4685, 37.0800 >>
		fDyingGangGuy = 326.7277
		
//		streaming_volume = STREAMVOL_CREATE_SPHERE(vDeadPedGang[0], 10, FLAG_COLLISIONS_MOVER)
//		
//		WHILE NOT STREAMVOL_HAS_LOADED(streaming_volume)
//			WAIT(0)
//		ENDWHILE
//		
//		PRINTSTRING("\n@@@@@@@@@@@@@@ STREAMVOL_HAS_LOADED @@@@@@@@@@@@@@@@@@\n")
		
		REPEAT 6 index
			IF index <= 3
				pedDeadPed[index] = CREATE_PED(PEDTYPE_CRIMINAL, G_M_Y_SalvaGoon_03, vDeadPedGang[index], fDeadPedGang[index])
				SET_PED_CONFIG_FLAG(pedDeadPed[index], PCF_ForceRagdollUponDeath, TRUE)
			ELSE
				pedDeadPed[index] = CREATE_PED(PEDTYPE_CRIMINAL, G_M_M_ChiGoon_02, vDeadPedGang[index], fDeadPedGang[index])
//				SET_PED_CONFIG_FLAG(pedDeadPed[index], PCF_ForceRagdollUponDeath, TRUE)
			ENDIF
			
//			IF index <= 5
//				blipDeadPeds[index] = CREATE_BLIP_FOR_PED(pedDeadPed[index], TRUE)
//				REQUEST_COLLISION_AT_COORD(vDeadPedGang[index])
//				SET_PED_CAN_RAGDOLL(pedDeadPed[index], TRUE)
//			ENDIF
		ENDREPEAT
		
//		SET_ENTITY_ALWAYS_PRERENDER(pedDeadPed[0], TRUE)
//		SET_ENTITY_COLLISION(pedDeadPed[0], TRUE)
//		SET_ENTITY_LOAD_COLLISION_FLAG(pedDeadPed[0], TRUE)
		
//		WAIT(500)
//		WHILE NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDeadPedGang[0], << 40, 40, 40 >>)
//			WAIT(0)
//		ENDWHILE
		
//		IF NOT IS_PED_INJURED(pedDeadPed[0])
//			WHILE NOT HAS_COLLISION_LOADED_AROUND_ENTITY(pedDeadPed[0])
//				IF NOT IS_PED_INJURED(pedDeadPed[0])
//					WAIT(0) //Waiting for collision to load
//				ENDIF
//			ENDWHILE
//		ENDIF
		
		pedDeadDog[0] = CREATE_PED(PEDTYPE_MISSION, A_C_Rottweiler, << -1635.0052, 4737.1807, 53.4995 >>, 33.4155)
		pedDeadDog[1] = CREATE_PED(PEDTYPE_MISSION, A_C_Rottweiler, << -1625.2152, 4741.1187, 52.5762 >>, 316.2733)
		REPEAT COUNT_OF(pedDeadDog) index
//			SET_ENTITY_HEALTH(pedDeadDog[index], 101)
			SET_PED_CAN_RAGDOLL(pedDeadDog[index], TRUE)
			SET_ENTITY_COLLISION(pedDeadDog[index], TRUE)
			SET_PED_CONFIG_FLAG(pedDeadDog[index], PCF_ForceRagdollUponDeath, TRUE)
//			SET_ENTITY_LOAD_COLLISION_FLAG(pedDeadDog[index], TRUE)
			
			vPedCood = GET_ENTITY_COORDS(pedDeadDog[index])
			vPedCoodShootFrom = << vPedCood.X, vPedCood.Y -2, vPedCood.Z -0.5 >>
			vPedCoodShootTo = << vPedCood.X, vPedCood.Y +50, vPedCood.Z -0.5 >>
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vPedCoodShootFrom, vPedCoodShootTo, 100, TRUE)
		ENDREPEAT
		
//		INT sceneId = CREATE_SYNCHRONIZED_SCENE(<< -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[0], sceneId, "random@dealgonewrongdead_peds", "ped_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[1], sceneId, "random@dealgonewrongdead_peds", "ped_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[2], sceneId, "random@dealgonewrongdead_peds", "ped_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[3], sceneId, "random@dealgonewrongdead_peds", "ped_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[4], sceneId, "random@dealgonewrongdead_peds", "ped_e", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
		
//		TASK_SYNCHRONIZED_SCENE(pedDeadDog[0], sceneId, "random@dealgonewrongdead_peds", "dog_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		TASK_SYNCHRONIZED_SCENE(pedDeadDog[1], sceneId, "random@dealgonewrongdead_peds", "dog_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
//		INT sceneId = CREATE_SYNCHRONIZED_SCENE(<< -1635.928, 4708.455, 46.090 >>, << -29.250, 8.250, 171.750 >>)
//		TASK_SYNCHRONIZED_SCENE(pedDeadPed[0], sceneId, "random@dealgonewrongdead_peds", "ped_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[0], "random@dealgonewrongdead_peds", "ped_b", << -1635.928, 4707.941, 46.383 >>, << -29.250, 12.250, 171.750 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[1], "random@dealgonewrongdead_peds", "ped_a", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[2], "random@dealgonewrongdead_peds", "ped_b", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[3], "random@dealgonewrongdead_peds", "ped_c", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[4], "random@dealgonewrongdead_peds", "ped_d", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadPed[5], "random@dealgonewrongdead_peds", "ped_e", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		
		TASK_PLAY_ANIM_ADVANCED(pedDeadDog[0], "random@dealgonewrongdead_peds", "dog_a", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		TASK_PLAY_ANIM_ADVANCED(pedDeadDog[1], "random@dealgonewrongdead_peds", "dog_b", << -1626.923, 4729.901, 52.214 >>, << -1.341, 1.085, -12.902 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS)
		
		SET_DECAL_BULLET_IMPACT_RANGE_SCALE(50)
		shootUpVehicles()
		SET_DECAL_BULLET_IMPACT_RANGE_SCALE(1)
		
		IF IS_VEHICLE_DRIVEABLE(vehGangCar[0])
			pedDeadPed[8] = CREATE_PED_INSIDE_VEHICLE(vehGangCar[0], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_DRIVER)
			SET_ENTITY_AS_MISSION_ENTITY(pedDeadPed[8])
			SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(pedDeadPed[8], TRUE)
			SET_ENTITY_HEALTH(pedDeadPed[8], 0)
//			SET_VEHICLE_SIREN(vehGangCar[0], TRUE)
//			START_VEHICLE_HORN(vehGangCar[0], 100000)
//			SET_HORN_PERMANENTLY_ON_TIME(vehGangCar[0], 3600000)
//			PLAY_SOUND_FROM_ENTITY(soundID, "SOMETHING", vehGangCar[0], )
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(blipRandomEvent)
//			blipRandomEvent = CREATE_AMBIENT_BLIP_INITIAL_COORD(vDeadPedGang[7])
//			blipRandomEvent = ADD_BLIP_FOR_COORD(vDealLocation)
//			SET_BLIP_SPRITE(blipRandomEvent, RADAR_TRACE_RANDOM_CHARACTER)
		ENDIF
		
		ADD_RELATIONSHIP_GROUP("re_DealGoneWrong", rghDealGoneWrong)
		
		pedDeadPed[6] = CREATE_PED(PEDTYPE_CRIMINAL, G_M_M_ChiGoon_02, vDeadPedGang[6], fDeadPedGang[6])
		ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 4, pedDeadPed[6], "REDGWChinese")
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDeadPed[6], TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedDeadPed[6], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDeadPed[6], rghDealGoneWrong)
		INT sceneId0 = CREATE_SYNCHRONIZED_SCENE(vDeadPedGang[6] + << 0, 0, 1.05 >>, << -7, 0.000, 34.1111 >>, EULER_XYZ)
		TASK_SYNCHRONIZED_SCENE(pedDeadPed[6], sceneId0, "random@dealgonewrong", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId0, TRUE)
		SET_PED_KEEP_TASK(pedDeadPed[6], TRUE)
		SET_PED_CONFIG_FLAG(pedDeadPed[6], PCF_ForceRagdollUponDeath, TRUE)
		
		pedDeadPed[7] = CREATE_PED(PEDTYPE_CRIMINAL, G_M_Y_SalvaGoon_03, << -1640.3105, 4738.4, 50.2 >>, fDeadPedGang[7])
//		SET_ENTITY_ROTATION(pedDeadPed[7], << -7, 0.000, 34.1111 >>, EULER_XYZ)
		ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 5, pedDeadPed[7], "REDGWSalvadoran")
//		SET_ENTITY_HEALTH(pedDeadPed[7], 101)
//		SET_PED_CAN_EVASIVE_DIVE(pedDeadPed[7], FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDeadPed[7], TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedDeadPed[7], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDeadPed[7], rghDealGoneWrong)
		INT sceneId1 = CREATE_SYNCHRONIZED_SCENE(vDeadPedGang[7] + << -0.65, 0.65, 0.92 >>, << 0.000, 0.000, 34.1111 >>, EULER_XYZ)
		TASK_SYNCHRONIZED_SCENE(pedDeadPed[7], sceneId1, "random@dealgonewrong", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId1, TRUE)
		SET_SYNCHRONIZED_SCENE_PHASE(sceneId1, 0.5)
//		TASK_PLAY_ANIM(pedDeadPed[7], "random@dealgonewrong", "idle_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING/*|AF_NOT_INTERRUPTABLE*/)
		SET_PED_KEEP_TASK(pedDeadPed[7], TRUE)
		SET_PED_CONFIG_FLAG(pedDeadPed[7], PCF_ForceRagdollUponDeath, TRUE)
		
		pedDyingGangGuy = CREATE_PED(PEDTYPE_CRIMINAL, G_M_M_ChiGoon_02, vDyingGangGuy, fDyingGangGuy)
//		SET_ENTITY_ROTATION(pedDyingGangGuy, << 4.000, 18.000, 326.7277 >>, EULER_XYZ)
		ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 4, pedDyingGangGuy, "REDGWChinese")
//		SET_ENTITY_HEALTH(pedDyingGangGuy, 101)
//		SET_PED_CAN_EVASIVE_DIVE(pedDyingGangGuy, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDyingGangGuy, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedDyingGangGuy, rghDealGoneWrong)
		INT sceneId2 = CREATE_SYNCHRONIZED_SCENE(vDyingGangGuy, << 7.500, -12.750, 51.500 >>, EULER_XYZ)
		TASK_SYNCHRONIZED_SCENE(pedDyingGangGuy, sceneId2, "random@dealgonewrong", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId2, TRUE)
		SET_ENTITY_ROTATION(pedDyingGangGuy, << 4.000, 18.000, 326.7277 >>, EULER_XYZ)
//		TASK_PLAY_ANIM(pedDyingGangGuy, "amb@bums@male@stationary@laying_against_wall@idle_a", "idle_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING|AF_FORCE_START/*|AF_NOT_INTERRUPTABLE*/)
		SET_PED_KEEP_TASK(pedDyingGangGuy, TRUE)
		SET_PED_CONFIG_FLAG(pedDyingGangGuy, PCF_ForceRagdollUponDeath, TRUE)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghDealGoneWrong, RELGROUPHASH_PLAYER)
//		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, RELGROUPHASH_PLAYER, rghDealGoneWrong)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		decalBlood[0] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1632.9529, 4745.3594, 51.7876>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[1] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1634.8696, 4744.4585, 51.8233>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[2] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1636.9706, 4736.5947, 52.2814>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[3] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1625.5612, 4741.2407, 51.7102>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[4] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1621.5326, 4744.5762, 51.9093>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[5] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1621.9626, 4737.0391, 51.4174>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[6] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1628.5171, 4736.9058, 51.8207>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[7] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1629.3240, 4732.1465, 51.6947>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[8] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1640.2866, 4738.4370, 52.1756>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[9] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1640.0551, 4701.9946, 41.2428>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		WAIT(0)
		decalBlood[10] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1641.9241, 4692.2363, 38.3762>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
		
		REPEAT 6 index
//			IF NOT IS_PED_INJURED(pedDeadPed[index])
			IF NOT IS_ENTITY_DEAD(pedDeadPed[index])
				SET_PED_CAN_RAGDOLL(pedDeadPed[index], TRUE)
//				SET_PED_TO_RAGDOLL(pedDeadPed[index], 1000, 2000, TASK_RELAX, FALSE, FALSE)
//				SET_ENTITY_HEALTH(pedDeadPed[index], 101)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedDeadPed[index], PDZ_TORSO, 0.30, 0.60, BDT_SHOTGUN_LARGE)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedDeadPed[index], PDZ_TORSO, 0.65, 0.60, BDT_SHOTGUN_LARGE)
				//GIVE_WEAPON_TO_PED(pedDeadPed[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
////////////////////				vPedCood = GET_ENTITY_COORDS(pedDeadPed[index])
////////////////////				vPedCoodShootFrom = << vPedCood.X, vPedCood.Y -0.5, vPedCood.Z >>
////////////////////				vPedCoodShootTo = << vPedCood.X, vPedCood.Y +50, vPedCood.Z >>
////////////////////				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vPedCoodShootFrom, vPedCoodShootTo, 100, TRUE)
//				EXPLODE_PED_HEAD(pedDeadPed[index])
				WAIT(GET_RANDOM_INT_IN_RANGE(100, 500))
//				IF IS_PED_INJURED(pedDeadPed[index])
				IF index <= 5
					IF DOES_BLIP_EXIST(blipDeadPeds[index])
						REMOVE_BLIP(blipDeadPeds[index])
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		/* CLEAN UP SYNCHRONIZED SCENE */
		//CLEAR_PED_TASKS (pedDyingGangGuy)
		
		PRINTSTRING("re_DealGoneWrong - MODEL LOADED AND PEDS CREATED")
		PRINTNL()
//		magic_binbag = CREATE_OBJECT(PROP_LD_BINBAG_01, <<-1588,4742,0>>)
//		SET_ENTITY_VISIBLE(magic_binbag, FALSE)
		
//		SET_MISSION_VEHICLE_GEN_VEHICLE
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
			IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
				SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
			ENDIF
		ENDIF
		
		bAssetsLoaded = TRUE
		
	ENDIF
	
ENDPROC

PROC loadDealerAssets()
	
	//GANG A
	vehGangChaseCarA[0] = CREATE_VEHICLE(REBEL, << -1595.9641, 4732.4790, 0 >>, 305.0)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseCarA[0])
	SET_VEHICLE_ENGINE_ON(vehGangChaseCarA[0], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseCarA[0], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseCarA[0], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseCarA[0], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseCarA[0], "49GNL112")
	
	vehGangChaseCarA[1] = CREATE_VEHICLE(REBEL, << -1570.3396, 4733.7554, 0 >>, 305.0)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseCarA[1])
	SET_VEHICLE_ENGINE_ON(vehGangChaseCarA[1], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseCarA[1], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseCarA[1], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseCarA[1], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseCarA[1], "47TMS703")
	
	//BIKE A
	vehGangChaseBikeA[0] = CREATE_VEHICLE(SANCHEZ, << -1595.9641, 4732.4790, 0 >> + << 10, 10, 10 >>, 305.0)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseBikeA[0])
	SET_VEHICLE_ENGINE_ON(vehGangChaseBikeA[0], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseBikeA[0], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseBikeA[0], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseBikeA[0], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseBikeA[0], "49GNL112")
	
	vehGangChaseBikeA[1] = CREATE_VEHICLE(SANCHEZ, << -1570.3396, 4733.7554, 0 >> + << 10, 10, 10 >>, 305.0)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseBikeA[1])
	SET_VEHICLE_ENGINE_ON(vehGangChaseBikeA[1], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseBikeA[1], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseBikeA[1], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseBikeA[1], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseBikeA[1], "47TMS703")
	
	pedGangChasePedA[0] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[0], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_DRIVER) //Driver
	pedGangChasePedA[1] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[1], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_DRIVER) //Driver
	IF NOT bBikesCreated
		pedGangChasePedA[2] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[0], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_FRONT_RIGHT)
		pedGangChasePedA[3] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[1], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_FRONT_RIGHT)
	ENDIF
//	pedGangChasePedA[4] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[0], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_BACK_LEFT)
//	pedGangChasePedA[5] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarA[1], PEDTYPE_MISSION, G_M_M_ChiGoon_02, VS_BACK_LEFT)
	
	//GANG B
	vehGangChaseCarB[0] = CREATE_VEHICLE(BobcatXL, << -1356.2806, 5000, 0 >>, 125.2811)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseCarB[0])
	SET_VEHICLE_ENGINE_ON(vehGangChaseCarB[0], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseCarB[0], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseCarB[0], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseCarB[0], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseCarB[0], "49GNL112")
	
	vehGangChaseCarB[1] = CREATE_VEHICLE(BobcatXL, << -1373.5890, 5000, 0 >>, 125.4590)
	SET_ENTITY_AS_MISSION_ENTITY(vehGangChaseCarB[1])
	SET_VEHICLE_ENGINE_ON(vehGangChaseCarB[1], TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehGangChaseCarB[1], FORCE_VEHICLE_LIGHTS_ON)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehGangChaseCarB[1], TRUE)
	FREEZE_ENTITY_POSITION(vehGangChaseCarB[1], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(vehGangChaseCarB[1], "47TMS703")
	
	pedGangChasePedB[0] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[0], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_DRIVER)
	pedGangChasePedB[1] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[1], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_DRIVER)
	pedGangChasePedB[2] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[0], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_FRONT_RIGHT)
	pedGangChasePedB[3] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[1], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_FRONT_RIGHT)
//	pedGangChasePedB[4] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[0], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_BACK_LEFT)
//	pedGangChasePedB[5] = CREATE_PED_INSIDE_VEHICLE(vehGangChaseCarB[1], PEDTYPE_MISSION, G_M_Y_SalvaGoon_03, VS_BACK_LEFT)
	
//	SET_PED_COMBAT_MOVEMENT(temp_cop_ped[0], CM_WILLADVANCE )
//	SET_PED_COMBAT_ATTRIBUTES(temp_cop_ped[0], CA_JUST_SEEK_COVER | CA_USE_COVER, FALSE)
//	SET_PED_COMBAT_RANGE (temp_cop_ped[0], CR_NEAR)
//	SET_PED_COMBAT_MOVEMENT (temp_crim_ped[0], CM_WILLRETREAT)
//	SET_PED_COMBAT_ATTRIBUTES(temp_crim_ped[0], CA_ALWAYS_FLEE | CA_FLEE_WHILST_IN_VEHICLE | CA_USE_VEHICLE, TRUE)
	
	ADD_RELATIONSHIP_GROUP("RE_deal1", rghGroupGangA)
	ADD_RELATIONSHIP_GROUP("RE_deal2", rghGroupGangB)
	
	INT index
	REPEAT NUM_GANGA index
		IF NOT IS_PED_INJURED(pedGangChasePedA[index])
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGangChasePedA[index], rghGroupGangA)
			SET_ENTITY_AS_MISSION_ENTITY(pedGangChasePedA[index])
			IF bBikesCreated
				GIVE_WEAPON_TO_PED(pedGangChasePedA[0], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, TRUE, TRUE)
				IF NOT IS_PED_INJURED(pedGangChasePedA[1])
					GIVE_WEAPON_TO_PED(pedGangChasePedA[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				ENDIF
				SET_PED_ACCURACY(pedGangChasePedA[index], 15)
				SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[index], CA_LEAVE_VEHICLES, FALSE)
			ELSE
				GIVE_WEAPON_TO_PED(pedGangChasePedA[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				SET_PED_ACCURACY(pedGangChasePedA[index], 13)
			ENDIF
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[index], CA_AGGRESSIVE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[index], CA_DO_DRIVEBYS, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[index], CA_USE_VEHICLE, TRUE)
			ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 4, pedGangChasePedA[index], "REDGWChinese")
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_GANGB index
		IF NOT IS_PED_INJURED(pedGangChasePedA[index])
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGangChasePedB[index], rghGroupGangB)
			SET_ENTITY_AS_MISSION_ENTITY(pedGangChasePedB[index])
			IF bBikesCreated
				GIVE_WEAPON_TO_PED(pedGangChasePedA[0], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, TRUE, TRUE)
				IF NOT IS_PED_INJURED(pedGangChasePedA[1])
					GIVE_WEAPON_TO_PED(pedGangChasePedA[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				ENDIF
				SET_PED_ACCURACY(pedGangChasePedA[index], 15)
				SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[index], CA_LEAVE_VEHICLES, FALSE)
				ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 4, pedGangChasePedA[index], "REDGWChinese")
			ELSE
				GIVE_WEAPON_TO_PED(pedGangChasePedB[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				SET_PED_ACCURACY(pedGangChasePedB[index], 13)
				ADD_PED_FOR_DIALOGUE(dealgonewrongConversation, 5, pedGangChasePedB[index], "REDGWSalvadoran")
			ENDIF
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedB[index], CA_AGGRESSIVE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedB[index], CA_DO_DRIVEBYS, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedB[index], CA_USE_VEHICLE, TRUE)
		ENDIF
	ENDREPEAT
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rghGroupGangA, rghGroupGangB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rghGroupGangB, rghGroupGangA)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rghGroupGangA, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rghGroupGangB, RELGROUPHASH_PLAYER)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_M_ChiGoon_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SalvaGoon_03)
	SET_MODEL_AS_NO_LONGER_NEEDED(rebel)
	SET_MODEL_AS_NO_LONGER_NEEDED(BobcatXL)
	SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_C_Rottweiler)
	
	bLoadDealerAssets = TRUE
	Drug_dealStage = STAGE_RUN_DRUG_DEAL
	
ENDPROC

FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC storeEntityPositionAndHeading(ENTITY_INDEX EntityIndex, VECTOR &vPosition, FLOAT &fHeading)

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			vPosition = GET_ENTITY_COORDS(EntityIndex)
			fHeading = GET_ENTITY_HEADING(EntityIndex)
		ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_GOONS_AND_VEHICLE(PED_INDEX &ped, BLIP_INDEX &pedBlip, VEHICLE_INDEX &vehicle, BLIP_INDEX &vehBlip)//, FLOAT fCleanupDistanceInVehicle = 300.0, FLOAT fCleanupDistanceOnFoot = 150.0)

//	INT 	index
	BOOL	bAtLeastOnePedAlive
	BOOL	bAtLeastOnePedInVehicle
	
//	FOR index = 0 TO ( COUNT_OF(ped) - 1 )
	
		IF DOES_ENTITY_EXIST(ped)
		
			IF IS_PED_INJURED(ped)
			
				IF DOES_BLIP_EXIST(pedBlip)
					REMOVE_BLIP(pedBlip)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(ped)
					
			
			ELSE
			
				bAtLeastOnePedAlive = TRUE
				
				IF NOT IS_PED_IN_ANY_VEHICLE(ped)
								
//					IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped) > fCleanupDistanceOnFoot )
//						
//						SET_PED_AS_NO_LONGER_NEEDED(ped)
//
//						IF DOES_BLIP_EXIST(pedBlip)
//							REMOVE_BLIP(pedBlip)
//						ENDIF
//					
//					ENDIF
					
				ELSE
			
					IF DOES_ENTITY_EXIST(vehicle)
			
						IF IS_VEHICLE_DRIVEABLE(vehicle)
						
							IF IS_PED_IN_VEHICLE(ped, vehicle)
							
								bAtLeastOnePedInVehicle = TRUE
							
//								IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped) > fCleanupDistanceInVehicle )
//						
//									SET_PED_AS_NO_LONGER_NEEDED(ped)
//
//									IF DOES_BLIP_EXIST(pedBlip)
//										REMOVE_BLIP(pedBlip)
//									ENDIF
//																
//								ENDIF

							ENDIF
						
						ENDIF
						
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ENDIF
		
//	ENDFOR
	
	IF DOES_ENTITY_EXIST(vehicle)
	
		IF IS_VEHICLE_DRIVEABLE(vehicle)
			
			IF ( bAtLeastOnePedAlive = TRUE )
			OR ( bAtLeastOnePedInVehicle = TRUE )
			
//				IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicle) > fCleanupDistanceInVehicle )
//					
//					REMOVE_VEHICLE_STUCK_CHECK(vehicle)
//					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
//					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
//					
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
//			
//					IF DOES_BLIP_EXIST(vehBlip)
//						REMOVE_BLIP(vehBlip)
//					ENDIF
//												
//				ENDIF
				
			ELSE
			
				REMOVE_VEHICLE_STUCK_CHECK(vehicle)
				REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
					
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
			
				IF DOES_BLIP_EXIST(vehBlip)
					REMOVE_BLIP(vehBlip)
				ENDIF
			
			ENDIF
		
		ELSE
			
			REMOVE_VEHICLE_STUCK_CHECK(vehicle)
			REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
			
			IF DOES_BLIP_EXIST(vehBlip)
				REMOVE_BLIP(vehBlip)
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

PROC UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(PED_INDEX &ped, BLIP_INDEX &pedBlip, VEHICLE_INDEX &vehicle, BLIP_INDEX &vehBlip)

//	INT 	index
	BOOL	bAtLeastOnePedInVehicle
	
//	FOR index = 0 TO ( COUNT_OF(ped) - 1 )
	
		IF DOES_ENTITY_EXIST(vehicle)
		
			IF IS_VEHICLE_DRIVEABLE(vehicle)
			
				IF NOT IS_PED_INJURED(ped)
		
					IF IS_PED_IN_VEHICLE(ped, vehicle)
					
						IF DOES_BLIP_EXIST(pedBlip)
							REMOVE_BLIP(pedBlip)
						ENDIF
						
						bAtLeastOnePedInVehicle = TRUE
					
					ELSE
					
						IF NOT DOES_BLIP_EXIST(pedBlip)
							pedBlip = CREATE_BLIP_FOR_PED(ped, TRUE)
						ENDIF
					
					ENDIF
					
				ENDIF
					
			
				IF NOT DOES_BLIP_EXIST(vehBlip)
				
					IF ( bAtLeastOnePedInVehicle = TRUE )
						vehBlip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehicle))
						SET_BLIP_COLOUR(vehBlip, BLIP_COLOUR_RED)
						SET_BLIP_PRIORITY(vehBlip, BLIPPRIORITY_HIGHEST)
						SET_BLIP_NAME_FROM_TEXT_FILE(vehBlip, "BLIP_VEH")
					ENDIF
					
				ELSE
				
					VECTOR vBlipPosition 	= GET_BLIP_COORDS(vehBlip)
					VECTOR vVehiclePosition = GET_ENTITY_COORDS(vehicle)
					
					vBlipPosition.X = vBlipPosition.X +@((vVehiclePosition.X - vBlipPosition.X) / BLIP_SLIDE_AMOUNT)
					vBlipPosition.Y = vBlipPosition.Y +@((vVehiclePosition.Y - vBlipPosition.Y) / BLIP_SLIDE_AMOUNT)
					vBlipPosition.Z = vBlipPosition.Z +@((vVehiclePosition.Z - vBlipPosition.Z) / BLIP_SLIDE_AMOUNT)

					SET_BLIP_COORDS(vehBlip, vBlipPosition)
				
					IF ( bAtLeastOnePedInVehicle = FALSE )
						REMOVE_BLIP(vehBlip)
					ENDIF
					
				ENDIF
			
			ELSE
			
				IF DOES_BLIP_EXIST(vehBlip)
					REMOVE_BLIP(vehBlip)
				ENDIF
				
				IF NOT IS_PED_INJURED(ped)

					IF NOT DOES_BLIP_EXIST(pedBlip)
						pedBlip = CREATE_BLIP_FOR_PED(ped, TRUE)
					ENDIF
					
				ENDIF
				
			ENDIF
		
		ENDIF
	
//	ENDFOR
	
ENDPROC

PROC STORE_ENTITY_POSITION_AND_HEADING(ENTITY_INDEX EntityIndex, VECTOR &vPosition, FLOAT &fHeading)

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			vPosition 	= GET_ENTITY_COORDS(EntityIndex)
			fHeading 	= GET_ENTITY_HEADING(EntityIndex)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_A_DRIVER(VEHICLE_INDEX VehicleIndex)
	
	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF NOT IS_ENTITY_DEAD(VehicleIndex)
			IF NOT IS_VEHICLE_SEAT_FREE(VehicleIndex, VS_DRIVER)
			
				PED_INDEX DriverPedIndex
			
				DriverPedIndex = GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER)
				
				IF DOES_ENTITY_EXIST(DriverPedIndex)
					IF NOT IS_ENTITY_DEAD(DriverPedIndex)
						RETURN TRUE
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TargetVehicleIndex, VECTOR &vWarpPosition,
												   FLOAT &fWarpHeading, VECTOR &vLastTargetPosition, FLOAT &fLastTargetHeading,
												   INT &iWarpTimer, VECTOR vWarpOffset, INT iWarpTimeDelay, FLOAT fWarpDistance)
												   
												   
//	UPDATE_PLAYER_POSITIONS(vPlayerPositions, fPlayerHeadings)
												   
	IF  DOES_ENTITY_EXIST(VehicleIndex)
	AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
		IF 	DOES_ENTITY_EXIST(TargetVehicleIndex)
		AND ( VehicleIndex <> TargetVehicleIndex)
		AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND IS_VEHICLE_ON_ALL_WHEELS(TargetVehicleIndex)
		AND DOES_VEHICLE_HAVE_A_DRIVER(VehicleIndex)
					
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vLastTargetPosition) > 20.0
				
				vWarpPosition 	= vLastTargetPosition	//store last target vehicle positon as the new warp position
				fWarpHeading 	= fLastTargetHeading	//store last target vehicle heading as the new warp heading
				
				vWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarpPosition, fWarpHeading, vWarpOffset)
				
				STORE_ENTITY_POSITION_AND_HEADING(TargetVehicleIndex, vLastTargetPosition, fLastTargetHeading)
				
			ENDIF
			
			//IF IS_ENTITY_ON_SCREEN(VehicleIndex)
			IF NOT IS_ENTITY_OCCLUDED(VehicleIndex)
			
			      iWarpTimer = GET_GAME_TIMER()
				  
			ELIF GET_GAME_TIMER() - iWarpTimer > iWarpTimeDelay
			
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(VehicleIndex, TargetVehicleIndex) > fWarpDistance
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vWarpPosition) > 20.0
				
					IF NOT IS_VECTOR_ZERO(vWarpPosition)
						IF NOT IS_SPHERE_VISIBLE(vWarpPosition, 3.0)
						  
							CLEAR_AREA_OF_PEDS(vWarpPosition, 1.5)
					        CLEAR_AREA_OF_VEHICLES(vWarpPosition, 3.0)
							
							SET_ENTITY_COORDS(VehicleIndex, vWarpPosition)
							SET_ENTITY_HEADING(VehicleIndex, fWarpHeading)
				            SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
				            
				            SET_VEHICLE_FORWARD_SPEED(VehicleIndex, 10.0)
				            
				            SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
				            
				            iWarpTimer = GET_GAME_TIMER()
						
						ELSE
						
							
							VECTOR vNewWarpPosition
							
//							REPEAT COUNT_OF(vPlayerPositions) index
							
								vNewWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerPositions, fPlayerHeadings, vWarpOffset)
								
								IF NOT IS_VECTOR_ZERO(vNewWarpPosition)
									IF NOT IS_SPHERE_VISIBLE(vNewWarpPosition, 2.0)
									
										CLEAR_AREA_OF_PEDS(vNewWarpPosition, 1.5)
										CLEAR_AREA_OF_VEHICLES(vNewWarpPosition, 3.0)
										
										SET_ENTITY_COORDS(VehicleIndex, vNewWarpPosition)
										SET_ENTITY_HEADING(VehicleIndex, fPlayerHeadings)
										SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
										
										SET_VEHICLE_FORWARD_SPEED(VehicleIndex, 10.0)
										
										SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
										
										iWarpTimer = GET_GAME_TIMER()
										
										EXIT
										
										#IF IS_DEBUG_BUILD
											INT index
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping to new position ", vNewWarpPosition, " at index ", index, ".")
											SCRIPT_ASSERT("Warping to new position")
										#ENDIF
									
									ENDIF
								ENDIF
							
//							ENDREPEAT
						
						ENDIF
				    ENDIF
					
				ENDIF
			ENDIF
		ELSE
			iWarpTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF

ENDPROC

PROC dyingGangMember()
	SWITCH iGangDyingScene0
		CASE 0
			IF NOT IS_PED_INJURED(pedDyingGangGuy)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBriefcaseCoods, << 7, 7, 10 >>)
//				AND CAN_PED_SEE_HATED_PED(pedDyingGangGuy, PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_NTOUCH", CONV_PRIORITY_AMBIENT_HIGH)
					TASK_LOOK_AT_ENTITY(pedDyingGangGuy, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					IF NOT DOES_BLIP_EXIST(blipBriefcase)
						blipBriefcase = CREATE_BLIP_FOR_PICKUP(pickupBriefcase)
					ENDIF
					iCounterTimeStartedDying = GET_GAME_TIMER()
					iGangDyingScene0 ++
				ENDIF
			ELSE
				iGangDyingScene0 ++
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iGangDyingScene1
//		CASE 0
//		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(pedDyingGangGuy)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDyingGangGuy, << 10, 10, 6 >>)
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_NKILLD", CONV_PRIORITY_AMBIENT_HIGH)
//					ENDIF
						IF NOT IS_PED_INJURED(pedDyingGangGuy)
							TASK_LOOK_AT_ENTITY(pedDyingGangGuy, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
	//						iCounterTimeStartedDying = GET_GAME_TIMER()
							SETTIMERB(0)
							iGangDyingScene1 ++
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SETTIMERB(0)
				iGangDyingScene1 ++
			ENDIF
		BREAK
		
		CASE 2
//			iCounterCurrentTimeDying = GET_GAME_TIMER()
//			IF (iCounterCurrentTimeDying - iCounterTimeStartedDying) > 3000
//			IF NOT IS_PED_INJURED(pedDeadPed[6])
//				SET_ENTITY_HEALTH(pedDeadPed[6], 0)
//				IF DOES_BLIP_EXIST(blipDeadPeds[6])
//					REMOVE_BLIP(blipDeadPeds[6])
//				ENDIF
//			ENDIF
//			IF NOT IS_PED_INJURED(pedDeadPed[7])
//				SET_ENTITY_HEALTH(pedDeadPed[7], 0)
//				IF DOES_BLIP_EXIST(blipDeadPeds[7])
//					REMOVE_BLIP(blipDeadPeds[7])
//				ENDIF
//			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR TIMERB() > 4000
				IF NOT IS_PED_INJURED(pedDyingGangGuy)
//					REMOVE_PED_FOR_DIALOGUE(dealgonewrongConversation, 4)
					SET_ENTITY_HEALTH(pedDyingGangGuy, 0)
					REMOVE_BLIP(blipDyingGangGuy)
				ENDIF
				IF TIMERB() > 7000
					IF NOT IS_PED_INJURED(pedDeadPed[6])
						SET_ENTITY_HEALTH(pedDeadPed[6], 0)
						REMOVE_BLIP(blipDeadPeds[6])
					ENDIF
				ENDIF
				IF TIMERB() > 9000
					IF NOT IS_PED_INJURED(pedDeadPed[7])
						SET_ENTITY_HEALTH(pedDeadPed[7], 0)
						REMOVE_BLIP(blipDeadPeds[7])
					ENDIF
					iGangDyingScene1 ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PED_INJURED(pedDeadPed[6])
				SET_ENTITY_HEALTH(pedDeadPed[6], 0)
			ENDIF
			IF NOT IS_PED_INJURED(pedDeadPed[7])
				SET_ENTITY_HEALTH(pedDeadPed[7], 0)
			ENDIF
			IF NOT IS_PED_INJURED(pedDyingGangGuy)
				SET_ENTITY_HEALTH(pedDyingGangGuy, 0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC waitingForPickUpBriefcase()

//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDealLocation, <<45, 45, 20>>)
//		IF bDogCreated = FALSE
//			pedDeadDog[0] = CREATE_PED(PEDTYPE_MISSION, A_C_Rottweiler, << -1635.0052, 4737.1807, 53.4995 >>, 33.4155)
//			pedDeadDog[1] = CREATE_PED(PEDTYPE_MISSION, A_C_Rottweiler, << -1625.2152, 4741.1187, 52.5762 >>, 316.2733)
//			SET_ENTITY_HEALTH(pedDeadDog[0], 0)
//			SET_ENTITY_HEALTH(pedDeadDog[1], 0)
//			bDogCreated = TRUE
//		ENDIF
//	ENDIF
	
	IF HAS_PICKUP_BEEN_COLLECTED(pickupBriefcase)
		IF DOES_BLIP_EXIST(blipRandomEvent)
			REMOVE_BLIP(blipRandomEvent)
		ENDIF
		IF DOES_BLIP_EXIST(blipBriefcase)
			REMOVE_BLIP(blipBriefcase)
		ENDIF
		
//		STREAMVOL_DELETE(streaming_volume)
//		SETTIMERA(0)
		iGangDyingScene1 = 1
		
//		IF NOT IS_PED_INJURED(pedDeadPed[7])
//			SET_ENTITY_HEALTH(pedDeadPed[7], 0)
//		ENDIF
		
		PRINTNL()
		PRINTSTRING("CASE GET! ")
		PRINTNL()
		
//		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CASE_01)
		dealgonewrong = DEAL_FOLLOW_CHASE
		Drug_dealStage = STAGE_CREATE_GANG
		
	ELSE
		SET_WANTED_LEVEL_MULTIPLIER(0)
		
		INT index
		REPEAT NUM_DEAD_PEDS index
			IF DOES_ENTITY_EXIST(pedDeadPed[index])
//				REQUEST_COLLISION_AT_COORD(GET_ENTITY_COORDS(pedDeadPed[index], FALSE))
				IF NOT IS_PED_INJURED(pedDeadPed[index])
					IF index = 0//<= 5
//						SET_ENTITY_HEALTH(pedDeadPed[0], 0)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipDeadPeds[index])
						PRINTSTRING("\n@@@@@@@@@@@@@@ pedDeadPed[")PRINTINT(index)PRINTSTRING("] @@@@@@@@@@@@@@\n")
						REMOVE_BLIP(blipDeadPeds[index])
					ENDIF
				ENDIF
//				SET_ENTITY_LOAD_COLLISION_FLAG(pedDeadPed[index], TRUE) //Only works if you "Check entity is alive this frame"
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(pedDeadDog) index
			IF NOT IS_PED_INJURED(pedDeadDog[index])
//				SET_ENTITY_HEALTH(pedDeadDog[index], 0)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDeadPedGang[7], << 110.0000, 95.0000, 40.0000 >>)
//			AND IS_SPHERE_VISIBLE(vDeadPedGang[7], 1)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBriefcaseCoods, << 10, 10, 10 >>)
				IF DOES_BLIP_EXIST(blipRandomEvent)
					REMOVE_BLIP(blipRandomEvent)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipDyingGangGuy)
				AND NOT DOES_BLIP_EXIST(blipDeadPeds[6])
				AND NOT DOES_BLIP_EXIST(blipDeadPeds[7])
					blipDyingGangGuy = CREATE_BLIP_FOR_PED(pedDyingGangGuy, TRUE)
					blipDeadPeds[6] = CREATE_BLIP_FOR_PED(pedDeadPed[6], TRUE)
					blipDeadPeds[7] = CREATE_BLIP_FOR_PED(pedDeadPed[7], TRUE)
				ENDIF
				SHOW_HEIGHT_ON_BLIP(blipDyingGangGuy, FALSE)
				SHOW_HEIGHT_ON_BLIP(blipDeadPeds[6], FALSE)
				SHOW_HEIGHT_ON_BLIP(blipDeadPeds[7], FALSE)
//				SET_VFX_IGNORE_CAM_DIST_CHECK(FALSE)
			
				SET_RANDOM_EVENT_ACTIVE()
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedDeadPed[6])
			IF GET_SCRIPT_TASK_STATUS(pedDeadPed[6], SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedDeadPed[6], SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != WAITING_TO_START_TASK
			OR IS_ENTITY_ON_FIRE(pedDeadPed[6])
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedDeadPed[6])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedDeadPed[6])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedDeadPed[6])
				SET_ENTITY_HEALTH(pedDeadPed[6], 0)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedDeadPed[7])
			IF GET_SCRIPT_TASK_STATUS(pedDeadPed[7], SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedDeadPed[7], SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != WAITING_TO_START_TASK
			OR IS_ENTITY_ON_FIRE(pedDeadPed[7])
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDeadPedGang[7], << 1.5, 1.5, 1.5 >>)
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedDeadPed[7])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedDeadPed[7])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedDeadPed[7])
				SET_ENTITY_HEALTH(pedDeadPed[7], 0)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedDyingGangGuy)
			IF GET_SCRIPT_TASK_STATUS(pedDyingGangGuy, SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedDyingGangGuy, SCRIPT_TASK_SYNCHRONIZED_SCENE /*SCRIPT_TASK_PLAY_ANIM*/) != WAITING_TO_START_TASK
			OR IS_ENTITY_ON_FIRE(pedDyingGangGuy)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDyingGangGuy, << 1, 1, 1 >>)
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedDyingGangGuy)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedDyingGangGuy)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedDyingGangGuy)
				SET_ENTITY_HEALTH(pedDyingGangGuy, 0)
			ENDIF
		ENDIF
		
		IF NOT bSplatterBlood
//			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDealLocation, << 80, 80, 10 >>)
//				bloodSpats()
//				bSplatterBlood = TRUE
//			ENDIF
		ENDIF
	ENDIF
	
	dyingGangMember()
	
	SWITCH iGangDyingScene2
		CASE 0
			IF NOT IS_PED_INJURED(pedDeadPed[6])
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedDeadPed[6]) + << 0, 2.5, 0 >>, << 9, 5, 5 >>)
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), pedDeadPed[6])
					TASK_LOOK_AT_ENTITY(pedDeadPed[6], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDeadPed[6], 4000, SLF_WHILE_NOT_IN_FOV)
					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_NDIE", CONV_PRIORITY_AMBIENT_HIGH)
					iCounterTimeStartedDying = GET_GAME_TIMER()
					iGangDyingScene2 = 1
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedDeadPed[7])
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedDeadPed[7]) + << 0, 3, 0 >>, << 9, 7, 5 >>)
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), pedDeadPed[7])
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
//					DISPLAY_HUD(FALSE)
//					DISPLAY_RADAR(FALSE)
//					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
//					IF NOT DOES_CAM_EXIST(ciDeadPed)
//						ciDeadPed = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1642.0840, 4739.3652, 52.4429 >>, << 17.6275, 0.0000, -99.5976 >>, 50, TRUE)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					TASK_LOOK_AT_ENTITY(pedDeadPed[7], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDeadPed[7], 4000, SLF_WHILE_NOT_IN_FOV)
					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_NDIE1", CONV_PRIORITY_AMBIENT_HIGH)
					iCounterTimeStartedDying = GET_GAME_TIMER()
					iGangDyingScene2 = 1
//				ENDIF
				ENDIF
			ENDIF
			IF IS_PED_INJURED(pedDeadPed[6])
			AND IS_PED_INJURED(pedDeadPed[7])
//				IF DOES_BLIP_EXIST(blipRandomEvent)
//					REMOVE_BLIP(blipRandomEvent)
//					blipRandomEvent = CREATE_AMBIENT_BLIP_INITIAL_COORD(vBriefcaseCoods)
//				ENDIF
				iGangDyingScene2 = 2
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipDyingGangGuy)
				SHOW_HEIGHT_ON_BLIP(blipDyingGangGuy, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(blipDeadPeds[6])
				SHOW_HEIGHT_ON_BLIP(blipDeadPeds[6], TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(blipDeadPeds[7])
				SHOW_HEIGHT_ON_BLIP(blipDeadPeds[7], TRUE)
			ENDIF
			iCounterCurrentTimeDying = GET_GAME_TIMER()
//			IF (iCounterCurrentTimeDying - iCounterTimeStartedDying) > 4500
//				IF DOES_CAM_EXIST(ciDeadPed)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					DESTROY_CAM(ciDeadPed)
//					DISPLAY_HUD(TRUE)
//					DISPLAY_RADAR(TRUE)
//					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
//				ENDIF
//			ENDIF
			IF (iCounterCurrentTimeDying - iCounterTimeStartedDying) > 6000
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_PED_INJURED(pedDeadPed[6])
//					SET_ENTITY_HEALTH(pedDeadPed[6], 0)
//					REMOVE_PED_FOR_DIALOGUE(dealgonewrongConversation, 4)
				ENDIF
				IF NOT IS_PED_INJURED(pedDeadPed[7])
//					SET_ENTITY_HEALTH(pedDeadPed[7], 0)
//					REMOVE_PED_FOR_DIALOGUE(dealgonewrongConversation, 5)
				ENDIF
//				IF DOES_BLIP_EXIST(blipRandomEvent)
//					REMOVE_BLIP(blipRandomEvent)
					IF NOT DOES_BLIP_EXIST(blipBriefcase)
						blipBriefcase = CREATE_BLIP_FOR_PICKUP(pickupBriefcase)
					ENDIF
					SET_WANTED_LEVEL_MULTIPLIER(0)
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						SET_RANDOM_EVENT_ACTIVE()
					ENDIF
//				ENDIF
				iGangDyingScene2 = 2
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC gangHassleSpeechPlaying()
	
	SWITCH iGangSpeechStage
	
		CASE 0
//			PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ gangHassleSpeechPlaying() - CASE 0 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
			IF bGang1Spawned 
				INT index
				REPEAT NUM_GANGA index
					IF NOT IS_PED_INJURED(pedGangChasePedA[index])
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedGangChasePedA[index], << 100, 100, 20 >>)
							iGangSpeechStage = 3
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bGang2Spawned
				INT index
				REPEAT NUM_GANGB index
					IF NOT IS_PED_INJURED(pedGangChasePedB[index])
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedGangChasePedB[index], << 100, 100, 20 >>)
							iGangSpeechStage = 3
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
		
//		CASE 1
//			CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_HANDO", CONV_PRIORITY_AMBIENT_HIGH)//
//			iCounterTimeStartedDying = GET_GAME_TIMER()
//			iGangSpeechStage = 2
//		BREAK
//		
//		CASE 2
//			iCounterCurrentTimeDying = GET_GAME_TIMER()
//			IF (iCounterCurrentTimeDying - iCounterTimeStartedDying) > 5500
//			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
//					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_MFUCKO", CONV_PRIORITY_AMBIENT_HIGH)//
//				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FFUCKO", CONV_PRIORITY_AMBIENT_HIGH)//
//				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//					CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_TFUCKO", CONV_PRIORITY_AMBIENT_HIGH)//
//				ENDIF
//				
//				iCounterTimeStartedDying = GET_GAME_TIMER()
//				iGangSpeechStage = 3
//			ENDIF
//		BREAK
		
		CASE 3
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ gangHassleSpeechPlaying() - CASE 3 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghGroupGangA, rghGroupGangB)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghGroupGangB, rghGroupGangA)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghGroupGangA, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghGroupGangB, RELGROUPHASH_PLAYER)
			
			IF bGang1Spawned
				INT index
				REPEAT NUM_GANGA index //OTHER GANG ATTACKS
					IF NOT IS_PED_INJURED(pedGangChasePedA[index])
						IF NOT IS_PED_IN_COMBAT(pedGangChasePedA[index])
						AND GET_SCRIPT_TASK_STATUS(pedGangChasePedA[index], SCRIPT_TASK_VEHICLE_CHASE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedGangChasePedA[index], SCRIPT_TASK_VEHICLE_CHASE) != WAITING_TO_START_TASK
							CLEAR_PED_TASKS(pedGangChasePedA[index])
							TASK_COMBAT_PED(pedGangChasePedA[index], PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedGangChasePedA[index], TRUE)
	//						IF IS_ENTITY_AT_ENTITY(pedGangChasePedA[index], PLAYER_PED_ID(), << 50, 50, 20 >>)
//							PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ gangHassleSpeechPlaying() - IF NOT IS_PED_IN_COMBAT @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
						ENDIF
					ENDIF
				ENDREPEAT
				
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FIRE_C", CONV_PRIORITY_AMBIENT_HIGH)
//						WAIT(1000)
//						REPEAT NUM_GANGA index
//							IF NOT IS_PED_INJURED(pedGangChasePedA[index])
//								IF NOT DOES_BLIP_EXIST(blipGangChasePedA[index])
//									blipGangChasePedA[index] = CREATE_BLIP_FOR_PED((pedGangChasePedA[index]), TRUE)
//								ENDIF
//							ENDIF
//						ENDREPEAT
						iCounterTimeStartedDying = GET_GAME_TIMER()
						iGangSpeechStage = 4
					ENDIF
//				ENDIF
			ENDIF
			
			IF bGang2Spawned
				INT index
				REPEAT NUM_GANGB index //OTHER GANG ATTACKS
					IF NOT IS_PED_INJURED(pedGangChasePedB[index])
						IF NOT IS_PED_IN_COMBAT(pedGangChasePedB[index])
						AND GET_SCRIPT_TASK_STATUS(pedGangChasePedB[index], SCRIPT_TASK_VEHICLE_CHASE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedGangChasePedB[index], SCRIPT_TASK_VEHICLE_CHASE) != WAITING_TO_START_TASK
							CLEAR_PED_TASKS(pedGangChasePedB[index])
							TASK_COMBAT_PED(pedGangChasePedB[index], PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedGangChasePedB[index], TRUE)
	//						IF IS_ENTITY_AT_ENTITY(pedGangChasePedB[index], PLAYER_PED_ID(), << 50, 50, 20 >>)
						ENDIF
					ENDIF
				ENDREPEAT
				
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FIRE_S", CONV_PRIORITY_AMBIENT_HIGH)
//						WAIT(1000)
//						REPEAT NUM_GANGB index
//							IF NOT DOES_BLIP_EXIST(blipGangChasePedB[index])
//								blipGangChasePedB[index] = CREATE_BLIP_FOR_PED((pedGangChasePedB[index]), TRUE)
//							ENDIF
//						ENDREPEAT
						iCounterTimeStartedDying = GET_GAME_TIMER()
						iGangSpeechStage = 4
					ENDIF
//				ENDIF
			ENDIF
			
//			iCounterCurrentTimeDying = GET_GAME_TIMER()
//			IF (iCounterCurrentTimeDying - iCounterTimeStartedDying) > 5500
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC setupEnemyVehicle(VEHICLE_INDEX the_viGangVehicle, PED_INDEX the_gang_ped)

	FREEZE_ENTITY_POSITION(the_viGangVehicle, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(the_viGangVehicle)
	
	vTemp = GET_ENTITY_COORDS(the_viGangVehicle)
	vDiffVector = (vPlayersPosition - vTemp)
	SET_ENTITY_HEADING(the_viGangVehicle, GET_HEADING_FROM_VECTOR_2D(vDiffVector.x, vDiffVector.y))
	
	IF bBikesCreated
		IF NOT IS_PED_INJURED(the_gang_ped)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_VEHICLE_CHASE(the_gang_ped, PLAYER_PED_ID())
				WAIT(1)
				IF NOT IS_PED_INJURED(the_gang_ped)
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(the_gang_ped, VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE, TRUE)
				ENDIF
			ELSE
				TASK_VEHICLE_MISSION_PED_TARGET(the_gang_ped, the_viGangVehicle, PLAYER_PED_ID(), MISSION_ATTACK, 20, DRIVINGMODE_AVOIDCARS, -1, -1)
			ENDIF
			IF NOT IS_ENTITY_DEAD(the_viGangVehicle)
				SET_VEHICLE_FORWARD_SPEED(the_viGangVehicle, GET_ENTITY_SPEED(PLAYER_PED_ID()))
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(the_gang_ped)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_VEHICLE_CHASE(the_gang_ped, PLAYER_PED_ID())
//				WAIT(0)
//				IF NOT IS_PED_INJURED(the_gang_ped)
//					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(the_gang_ped, VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE, TRUE)
//				ENDIF
			ELSE
				TASK_VEHICLE_MISSION_PED_TARGET(the_gang_ped, the_viGangVehicle, PLAYER_PED_ID(), MISSION_RAM, 100, DRIVINGMODE_AVOIDCARS, -1, -1)
			ENDIF
//			IF NOT IS_ENTITY_DEAD(the_viGangVehicle)
				SET_VEHICLE_FORWARD_SPEED(the_viGangVehicle, GET_ENTITY_SPEED(PLAYER_PED_ID()))
//			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC runFollowChase()
	
	IF bLoadDealerAssets
	
		IF bBikesCreated
//			IF NOT IS_PED_INJURED(pedGangChasePedA[0])
//			AND NOT IS_ENTITY_DEAD(vehGangChaseBikeA[0])
//				RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedA[0], HowFarAwayIsThisPed, 150, vehGangChaseBikeA[0])
//			ENDIF
//			IF NOT IS_PED_INJURED(pedGangChasePedA[1])
//			AND NOT IS_ENTITY_DEAD(vehGangChaseBikeA[1])
//				RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedA[1], HowFarAwayIsThisPed, 150, vehGangChaseBikeA[1])
//			ENDIF
		ELSE
			IF bGang1Spawned
				IF NOT IS_PED_INJURED(pedGangChasePedA[0])
				AND NOT IS_ENTITY_DEAD(vehGangChaseCarA[0])
//					RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedA[0], HowFarAwayIsThisPed, 150, vehGangChaseCarA[0])
				ENDIF
				IF NOT IS_PED_INJURED(pedGangChasePedA[1])
				AND NOT IS_ENTITY_DEAD(vehGangChaseCarA[1])
//					RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedA[1], HowFarAwayIsThisPed, 150, vehGangChaseCarA[1])
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedGangChasePedB[0])
				AND NOT IS_ENTITY_DEAD(vehGangChaseCarB[0])
//					RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedB[0], HowFarAwayIsThisPed, 150, vehGangChaseCarB[0])
				ENDIF
				IF NOT IS_PED_INJURED(pedGangChasePedB[1])
				AND NOT IS_ENTITY_DEAD(vehGangChaseCarB[1])
//					RUN_GOON_VEHICLE_WARP_CHECK(pedGangChasePedB[1], HowFarAwayIsThisPed, 150, vehGangChaseCarB[1])
				ENDIF
			ENDIF
		ENDIF
		
		INT index
		REPEAT NUM_GANGA index
			REMOVE_DECAL(decalBlood[index])
			IF IS_PED_INJURED(pedGangChasePedA[index])
				IF DOES_BLIP_EXIST(blipGangChasePedA[index])
					REMOVE_BLIP(blipGangChasePedA[index])
					iGangAKilled ++
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(pedGangChasePedA[index])
					IF NOT DOES_BLIP_EXIST(blipGangChasePedA[index])
						blipGangChasePedA[index] = CREATE_BLIP_FOR_PED(pedGangChasePedA[index], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_GANGB index
			REMOVE_DECAL(decalBlood[index])
			IF IS_PED_INJURED(pedGangChasePedB[index])
				IF DOES_BLIP_EXIST(blipGangChasePedB[index])
					REMOVE_BLIP(blipGangChasePedB[index])
					iGangBKilled ++
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(pedGangChasePedB[index])
					IF NOT DOES_BLIP_EXIST(blipGangChasePedB[index])
						blipGangChasePedB[index] = CREATE_BLIP_FOR_PED(pedGangChasePedB[index], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iGangAKilled >= NUM_GANGA
		AND iGangBKilled >= NUM_GANGB
//		OR (bBikesCreated AND iGangAKilled >= 2)
			PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED1 @@@@@@@@@@@@@@@ \n")
			MISSION_PASSED()
		ENDIF
	ENDIF
	
	dyingGangMember()
	gangHassleSpeechPlaying()
	
	SWITCH gang_arrive_stage
	
		CASE GANG_ARRIVE_NEAR_ROAD
//			IF TIMERA() > 50000
//				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
//				gang_arrive_stage = GANG_ARRIVE_SPAWN_GANG1
//			ENDIF
			IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(PLAYER_ID())
//			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), bridge_vector, << 20, 20, 20 >>)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), highway_vector, << 20, 20, 20 >>)
			OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDealLocation + << 0, 20, 0 >>, << 170, 270, 50 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1555.4000, 4730.8174, 49.1791 >>, << 15, 25, 15 >>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1709.2108, 4659.5708, 36.6880 >>, << 15, 25, 15 >>)
				IF NOT bBikesCreated
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1555.4000, 4730.8174, 49.1791 >>, << 15, 25, 15 >>)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1709.2108, 4659.5708, 36.6880 >>, << 15, 25, 15 >>)
						bBikesCreated = TRUE
					ENDIF
				ENDIF
				IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vReturnedClosestNode)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vReturnedClosestNode, << 20, 20, 20 >>)
						ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
						SETTIMERA(0)
						loadDealerAssets()
						gang_arrive_stage = GANG_CHAT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_CHAT
			IF TIMERA() > 5000
			OR bBikesCreated
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_MAWAY", "REDGW_MAWAY_1", CONV_PRIORITY_AMBIENT_HIGH)
//					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FAWAY", "REDGW_FAWAY_1", CONV_PRIORITY_AMBIENT_HIGH)
//					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_TAWAY", "REDGW_TAWAY_1", CONV_PRIORITY_AMBIENT_HIGH)
//					ENDIF
					SETTIMERA(0)
					gang_arrive_stage = GANG_DELAY
//				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_DELAY
			IF bBikesCreated
				IF TIMERA() > 3500
					IF NOT IS_ENTITY_DEAD(vehGangChaseBikeA[0])
					AND NOT IS_ENTITY_DEAD(vehGangChaseBikeA[1])
					AND NOT IS_PED_INJURED(pedGangChasePedA[0])
					AND NOT IS_PED_INJURED(pedGangChasePedA[1])
//					AND NOT IS_PED_INJURED(pedGangChasePedA[2])
//					AND NOT IS_PED_INJURED(pedGangChasePedA[3])
						SET_PED_INTO_VEHICLE(pedGangChasePedA[0], vehGangChaseBikeA[0], VS_DRIVER) //Driver
						SET_PED_INTO_VEHICLE(pedGangChasePedA[1], vehGangChaseBikeA[1], VS_DRIVER) //Driver
//						SET_PED_INTO_VEHICLE(pedGangChasePedA[2], vehGangChaseBikeA[0], VS_FRONT_RIGHT)
//						SET_PED_INTO_VEHICLE(pedGangChasePedA[3], vehGangChaseBikeA[1], VS_FRONT_RIGHT)
						SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[0], CA_LEAVE_VEHICLES, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[1], CA_LEAVE_VEHICLES, FALSE)
//						SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[0], CA_USE_VEHICLE_ATTACK, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedGangChasePedA[1], CA_USE_VEHICLE_ATTACK, TRUE)
						SET_PED_HELMET(pedGangChasePedA[0], FALSE)
						SET_PED_HELMET(pedGangChasePedA[1], FALSE)
						gang_arrive_stage = GANG_ARRIVE_SPAWN_BIKES
					ENDIF
				ENDIF
			ENDIF
			IF TIMERA() > 10000
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), REBEL)
						gang_arrive_stage = GANG_ARRIVE_SPAWN_GANG1
					ELSE
						gang_arrive_stage = GANG_ARRIVE_SPAWN_GANG2
					ENDIF
				ELSE
					gang_arrive_stage = GANG_ARRIVE_SPAWN_GANG1
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_ARRIVE_SPAWN_GANG1
			vPlayersPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			randomised_node = GET_RANDOM_INT_IN_RANGE(10, 15)
//			IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(PLAYER_ID()) //Highway spawn
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1603.1528, 4892.9429, 60.1768 >>, << 225, 225, 50 >>)
				IF GET_NTH_CLOSEST_VEHICLE_NODE(vPlayersPosition, randomised_node, vSetPieceGangVector[0]) //Random spawn
//				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vSetPieceGangVector[0], spawn_distance)
//					storeEntityPositionAndHeading(PLAYER_PED_ID(), )
					vSetPieceGangVector[1] = vSetPieceGangVector[0] + << 0, 5 ,0 >>
					IF NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[0], 7.0)
					AND NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[1], 7.0)
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseCarA[0])
							SET_ENTITY_COORDS(vehGangChaseCarA[0], vSetPieceGangVector[0])
							setupEnemyVehicle(vehGangChaseCarA[0], pedGangChasePedA[0])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseCarA[1])
							SET_ENTITY_COORDS(vehGangChaseCarA[1], vSetPieceGangVector[1])
							setupEnemyVehicle(vehGangChaseCarA[1], pedGangChasePedA[1])
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_MAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_TAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bGang1Spawned = TRUE
						SETTIMERA(0)
						gang_arrive_stage = SHOOT_CASE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_ARRIVE_SPAWN_GANG2
			vPlayersPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			randomised_node = GET_RANDOM_INT_IN_RANGE(30, 35)
//			IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(PLAYER_ID()) //Highway spawn
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1603.1528, 4892.9429, 60.1768 >>, << 225, 225, 50 >>)
				IF GET_NTH_CLOSEST_VEHICLE_NODE(vPlayersPosition, randomised_node, vSetPieceGangVector[0]) //Random spawn
//				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vSetPieceGangVector[0], spawn_distance)
					vSetPieceGangVector[1] = vSetPieceGangVector[0] + << 0, 5 ,0 >>
					IF NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[0], 7.0)
					AND NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[1], 7.0)
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseCarB[0])
							SET_ENTITY_COORDS(vehGangChaseCarB[0], vSetPieceGangVector[0])
							setupEnemyVehicle(vehGangChaseCarB[0], pedGangChasePedB[0])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseCarB[1])
							SET_ENTITY_COORDS(vehGangChaseCarB[1], vSetPieceGangVector[1])
							setupEnemyVehicle(vehGangChaseCarB[1], pedGangChasePedB[1])
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_MAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_TAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bGang2Spawned = TRUE
						SETTIMERA(0)
						gang_arrive_stage = SHOOT_CASE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GANG_ARRIVE_SPAWN_BIKES
			vPlayersPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			randomised_node = GET_RANDOM_INT_IN_RANGE(5, 10)
			
//			IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(PLAYER_ID()) //Highway spawn
//			AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1603.1528, 4892.9429, 60.1768 >>, << 225, 225, 50 >>)
				IF GET_NTH_CLOSEST_VEHICLE_NODE(vPlayersPosition, randomised_node, vSetPieceGangVector[0]) //Random spawn
//				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vSetPieceGangVector[0], spawn_distance)
					vSetPieceGangVector[1] = vSetPieceGangVector[0] + << 0, 5 ,0 >>
					IF NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[0], 7.0)
					AND NOT IS_SPHERE_VISIBLE(vSetPieceGangVector[1], 7.0)
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseBikeA[0])
							SET_ENTITY_COORDS(vehGangChaseBikeA[0], vSetPieceGangVector[0])
							setupEnemyVehicle(vehGangChaseBikeA[0], pedGangChasePedA[0])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehGangChaseBikeA[1])
							SET_ENTITY_COORDS(vehGangChaseBikeA[1], vSetPieceGangVector[1])
							setupEnemyVehicle(vehGangChaseBikeA[1], pedGangChasePedA[1])
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_MAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_FAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(dealgonewrongConversation, "REDGWAU", "REDGW_TAWAY", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bGang1Spawned = TRUE
						SETTIMERA(0)
						gang_arrive_stage = SHOOT_CASE
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		CASE SHOOT_CASE
//			IF TIMERA() > 1000
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					fRandomfX = GET_RANDOM_FLOAT_IN_RANGE(1.5, 2.5)
					fRandomfY = GET_RANDOM_FLOAT_IN_RANGE(2.0, 4.0)
					fRandomfZ = GET_RANDOM_FLOAT_IN_RANGE(0.1, 2.0)
					IF iNumberOfShots < 5
					AND iShotsTimer < GET_GAME_TIMER()
						vPedCood = GET_ENTITY_COORDS(PLAYER_PED_ID())
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<< vPedCood.X +fRandomfX, vPedCood.Y +fRandomfY, vPedCood.Z +fRandomfZ >>, << vPedCood.X -fRandomfX, vPedCood.Y -fRandomfY, vPedCood.Z >> , 10)
						iShotsTimer = (GET_GAME_TIMER()+ GET_RANDOM_INT_IN_RANGE(200, 500))
						iNumberOfShots ++
					ENDIF
				ENDIF
				
//				INT index
//				IF bGang1Spawned
//					REPEAT NUM_GANGA index
//						IF NOT IS_PED_INJURED(pedGangChasePedA[index])
//							IF NOT DOES_BLIP_EXIST(blipGangChasePedA[index])
//								blipGangChasePedA[index] = CREATE_BLIP_FOR_PED((pedGangChasePedA[index]), TRUE)
//							ENDIF
//						ENDIF
//					ENDREPEAT
//				ELIF bGang2Spawned
//					REPEAT NUM_GANGB index
//						IF NOT DOES_BLIP_EXIST(blipGangChasePedB[index])
//							blipGangChasePedB[index] = CREATE_BLIP_FOR_PED((pedGangChasePedB[index]), TRUE)
//						ENDIF
//					ENDREPEAT
//				ENDIF
				
				IF iNumberOfShots > 4
					gang_arrive_stage = GANG_ARRIVE_CLEANUP
				ENDIF
//			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC keepPedRotationAndHorn()
	
	IF NOT IS_ENTITY_DEAD(vehGangCar[0])
		IF GET_PED_IN_VEHICLE_SEAT(vehGangCar[0]) = pedDeadPed[8]
			SET_HORN_PERMANENTLY_ON_TIME(vehGangCar[0], 60000)
			SET_HORN_PERMANENTLY_ON(vehGangCar[0])
		ENDIF
	ENDIF
	
//	IF NOT IS_PED_INJURED(pedDeadPed[7])
//		IF IS_PED_RAGDOLL(pedDeadPed[7])
//			SET_ENTITY_HEALTH(pedDeadPed[7], 0)
//		ENDIF
//	ENDIF
	
	IF NOT IS_PED_INJURED(pedDyingGangGuy)
		IF IS_PED_RAGDOLL(pedDyingGangGuy)
			SET_ENTITY_HEALTH(pedDyingGangGuy, 0)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipDyingGangGuy)
			REMOVE_BLIP(blipDyingGangGuy)
		ENDIF
	ENDIF
	
	INT index
	REPEAT NUM_DEAD_PEDS index
		IF NOT IS_SPHERE_VISIBLE(vDealLocation, 30)
		AND VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vDealLocation) > 300
			IF DOES_ENTITY_EXIST(pedDeadPed[index])
				SET_PED_AS_NO_LONGER_NEEDED(pedDeadPed[index])
			ENDIF
		ENDIF
		IF IS_PED_INJURED(pedDeadPed[index])
			IF DOES_BLIP_EXIST(blipDeadPeds[index])
				PRINTSTRING("\n@@@@@@@@@@@@@@ pedDeadPed[")PRINTINT(index)PRINTSTRING("] @@@@@@@@@@@@@@\n")
				REMOVE_BLIP(blipDeadPeds[index])
			ENDIF
		ENDIF
	ENDREPEAT
	
	
//	IF NOT IS_ENTITY_DEAD(pedDeadPed[7])
//		SET_ENTITY_COORDS(pedDeadPed[7], vDeadPedGang[7])
//		SET_ENTITY_ROTATION(pedDeadPed[7], << -7, 0.000, 34.1111 >>, EULER_XYZ)
//	ENDIF
//	IF NOT IS_ENTITY_DEAD(pedDyingGangGuy)
//		SET_ENTITY_COORDS(pedDyingGangGuy, << -1641.5676, 4685, 37.0800 >>)
//		SET_ENTITY_ROTATION(pedDyingGangGuy, << 4.000, 18.000, 326.7277 >>, EULER_XYZ)
//	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
	PROC runDebug()
	
		IF bRunDebugSelection
			bRunDebugSelection = FALSE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF dealgonewrong = DEAL_FUDGE_ACTIVATION_RANGE
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1614.1083, 4754.4355, 51.9688 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 131.5730)
				ELSE
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1614.1083, 4754.4355, 51.9688 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 131.5730)
				ENDIF
				debug_jump = 0
			ENDIF
			IF dealgonewrong = DEAL_FOLLOW_CHASE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1639.4369, 4690.5132, 38.7797 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 168.4503)
				debug_jump = 1
			ENDIF
			
			IF DOES_BLIP_EXIST(blipBriefcase)
				REMOVE_BLIP(blipBriefcase)
			ENDIF
			REMOVE_PICKUP(pickupBriefcase)
			
			INT index
			IF DOES_ENTITY_EXIST(pedDyingGangGuy)
				DELETE_PED(pedDyingGangGuy)
			ENDIF
			REPEAT COUNT_OF(pedDeadPed) index
				IF DOES_ENTITY_EXIST(pedDeadPed[index])
					DELETE_PED(pedDeadPed[index])
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(vehGangCar) index
				IF DOES_ENTITY_EXIST(vehGangCar[index])
					DELETE_VEHICLE(vehGangCar[index])
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(vehGangChaseCarA) index
				IF DOES_ENTITY_EXIST(vehGangChaseCarA[index])
					DELETE_VEHICLE(vehGangChaseCarA[index])
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(vehGangChaseCarB) index
				IF DOES_ENTITY_EXIST(vehGangChaseCarB[index])
					DELETE_VEHICLE(vehGangChaseCarB[index])
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(vehGangChaseBikeA) index
				IF DOES_ENTITY_EXIST(vehGangChaseBikeA[index])
					DELETE_VEHICLE(vehGangChaseBikeA[index])
				ENDIF
			ENDREPEAT
			REMOVE_RELATIONSHIP_GROUP(rghDealGoneWrong)
			
			bAssetsLoaded = FALSE
			bLoadDealerAssets = FALSE
			bBikesCreated = FALSE
			bGang1Spawned = FALSE
			bGang2Spawned = FALSE
			iGangSpeechStage = 0
			iGangBKilled = 0
			iNumberOfShots = 0
			iGangDyingScene0 = 0
			iGangDyingScene1 = 0
			iGangDyingScene2 = 0
			
			dealgonewrong = DEAL_SETUP
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			SWITCH debug_jump
				CASE 0
					v_debug_warp = << -1626.2683, 4744.5107, 52.0826 >>
					debug_warp_heading = 131.0
					debug_jump = 1
				BREAK
				CASE 1
					v_debug_warp = vBriefcaseCoods
					debug_warp_heading = 176.5553
					debug_jump = 2
				BREAK
				CASE 2
					v_debug_warp = << -1656.9156, 4849.6792, 60.6355 >>
					debug_warp_heading = 136.7132
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehGangCar[1] = CREATE_VEHICLE(BobcatXL, v_debug_warp, 131.6017)
						WAIT(0)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehGangCar[1], VS_DRIVER)
					ENDIF
				BREAK
			ENDSWITCH
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), v_debug_warp)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), debug_warp_heading)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				debug_jump ++
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
			PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())")
			PRINTNL()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			TERMINATE_THIS_THREAD()
			MISSION_FAILED()
		ENDIF
		
	ENDPROC
#ENDIF

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_DealGoneWrong launched ")
PRINTVECTOR(vInput)
PRINTNL()


#IF IS_DEBUG_BUILD

	START_WIDGET_GROUP("re_DealGoneWrong") 
	
		START_WIDGET_GROUP("Variation Selection Debug")
            START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Drug_deal_get_briefcase")
				ADD_TO_WIDGET_COMBO("Drug_deal_get_briefcase_2")
            STOP_WIDGET_COMBO("Drug_deal_TYPE_VARIATIONS", iRunDrug_dealStage) 
			
            IF g_bHoldRandomEventForSelection
                ADD_WIDGET_BOOL("Run mission with debug selection", bRunDebugSelection)
            ENDIF
        STOP_WIDGET_GROUP()
		
    STOP_WIDGET_GROUP()
	
#ENDIF

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	 missionCleanup()
ENDIF

//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
IF CAN_RANDOM_EVENT_LAUNCH(vInput)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1503.5402, 4934.9228, 114.2284 >>, << 100.0000, 100.0000, 100.0000 >>)
	OR dealgonewrong = DEAL_FOLLOW_CHASE
	
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				missionCleanup()
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_DGW")
		
		SWITCH dealgonewrong
			
			CASE DEAL_SETUP
				IF bAssetsLoaded
					dealgonewrong = DEAL_FUDGE_ACTIVATION_RANGE
				ELSE
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
						missionCleanup()
					ENDIF
					loadAssets()
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF bRunDebugSelection
						bRunDebugSelection = FALSE
					ENDIF
				#ENDIF
			BREAK
			
			CASE DEAL_FUDGE_ACTIVATION_RANGE
				keepPedRotationAndHorn()
				waitingForPickUpBriefcase()
				
				#IF IS_DEBUG_BUILD
					runDebug()
				#ENDIF
			BREAK
			
			CASE DEAL_FOLLOW_CHASE
				SWITCH Drug_dealStage
				
					CASE STAGE_CREATE_GANG
						keepPedRotationAndHorn()
//						loadDealerAssets()
						Drug_dealStage = STAGE_RUN_DRUG_DEAL
					BREAK
					
					CASE STAGE_RUN_DRUG_DEAL
						keepPedRotationAndHorn()
						runFollowChase()
						
						IF gang_arrive_stage >= SHOOT_CASE
							IF NOT IS_ENEMY_LOST()
								INT index
								REPEAT NUM_GANGA index
									IF bBikesCreated
										CLEANUP_GOONS_AND_VEHICLE(pedGangChasePedA[index], blipGangChasePedA[index], vehGangChaseBikeA[index], blipVehGangChaseA[index])
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseBikeA[0], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 0, 0 >>, 1000, 0)
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseBikeA[1], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 2, 0 >>, 1000, 0)
										UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(pedGangChasePedA[index], blipGangChasePedA[index], vehGangChaseBikeA[index], blipVehGangChaseA[index])
									ELIF bGang1Spawned
										CLEANUP_GOONS_AND_VEHICLE(pedGangChasePedA[index], blipGangChasePedA[index], vehGangChaseCarA[index], blipVehGangChaseA[index])
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseCarA[0], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 0, 0 >>, 1000, 0)
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseCarA[1], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 5, 0 >>, 1000, 0)
										UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(pedGangChasePedA[index], blipGangChasePedA[index], vehGangChaseCarA[index], blipVehGangChaseA[index])
									ELIF bGang2Spawned
										CLEANUP_GOONS_AND_VEHICLE(pedGangChasePedB[index], blipGangChasePedB[index], vehGangChaseCarB[index], blipVehGangChaseB[index])
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseCarB[0], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 0, 0 >>, 1000, 0)
										RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehGangChaseCarB[1], GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 5, 0 >>, 1000, 0)
										UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(pedGangChasePedB[index], blipGangChasePedB[index], vehGangChaseCarB[index], blipVehGangChaseB[index])
									ENDIF
								ENDREPEAT
							ELSE
								PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED2 @@@@@@@@@@@@@@@ \n")
								MISSION_PASSED()
							ENDIF
						ENDIF
						
						IF (IS_PED_ON_FOOT(PLAYER_PED_ID()) OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID()))
//						AND NOT bBikesCreated
//						AND NOT bGang1Spawned
//						AND NOT bGang2Spawned
						AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDealLocation, << 100, 100, 100 >>)
						AND gang_arrive_stage < GANG_DELAY
							PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED3 @@@@@@@@@@@@@@@ \n")
							MISSION_PASSED()
						ENDIF
					BREAK
					
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
					runDebug()
				#ENDIF
			BREAK
			
			CASE DEAL_CLEAN_UP
				preCleanup()
			BREAK
		ENDSWITCH
		
	ELSE
		missionCleanup()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bRunDebugSelection
			bRunDebugSelection = FALSE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			TERMINATE_THIS_THREAD()
			MISSION_FAILED()
		ENDIF
	#ENDIF
	
ENDWHILE

ENDSCRIPT
