//		re_SecurityVan				

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES =====================================
USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_event.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_path.sch"
USING "Commands_fire.sch"
USING "dialogue_public.sch"
USING "script_blips.sch"
USING "ambient_speech.sch"
USING "Ambient_Common.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"
USING "load_queue_public.sch"

CONST_FLOAT SHOOT_OUT_MIN_DISTANCE 30.0
CONST_INT GUARD_ONE_DRINK_TIME 7000
CONST_INT GUARD_TWO_DRINK_TIME 9000

ENUM eventVariations
	TARGET_NONE,
	ATTENTIVE_GUARD,
	LAZY_GUARD,
	EN_ROUTE
ENDENUM
eventVariations thisEventVariation = TARGET_NONE

ENUM eventHelpTexts
	SVHT_DOORSHOOT = 0,
	SVHT_ATTACHBOMB,
	SVHT_SECVANMAIN
ENDENUM

ENUM eventWorldPoint
	SV_VAR_GAS_STATION = 1,
	SV_VAR_CLUCKING_BELL,
	SV_VAR_PALETO,
	SV_VAR_CITY,
	SV_VAR_DRIVING_VENICE,
	SV_VAR_DRIVING2,
	SV_VAR_DRIVING3,
	SV_VAR_DRIVING4,
	SV_VAR_DRIVING5,
	SV_VAR_DRIVING6
ENDENUM
eventWorldPoint eWhichWorldPoint

ENUM securityVanStages
	SV_SET_UP,
	SV_LOAD_CASE,
	SV_HOSTILE_RESPONSE,
	SV_HOSTILE_RESPONSE_TO_CAR,
	SV_DRIVE_OFF,
	SV_FLEE_ON_FOOT,
	SV_FLEE_IN_VAN,
	SV_RECOVER_CASE,
	SV_SHOOT_OUT,
	SV_CLEAN_UP
ENDENUM
securityVanStages thisStage = SV_SET_UP

ENUM VAN_STATUS
	VANSTATUS_PRE_LOADING = 0,
	VANSTATUS_CASE_LOADING,
	VANSTATUS_CASE_LOADED,
	VANSTATUS_DOORS_BLOWN
ENDENUM

VAN_STATUS eVanStatus

INT iStageProgress = 0

PED_INDEX pedSecurityGuy[2]

VEHICLE_INDEX vehSecurityVan
VEHICLE_INDEX vehOffendingVehicle

OBJECT_INDEX objIdCase
OBJECT_INDEX objShootDummy

SEQUENCE_INDEX Seq

BLIP_INDEX blipSecurityVan 
BLIP_INDEX blipSecurityGuy[2] 
BLIP_INDEX blipInitialMarker
BLIP_INDEX blipCase

SCENARIO_BLOCKING_INDEX sbiSecVan

REL_GROUP_HASH security

VECTOR vCreateSecurityGuyPos[2]
FLOAT fCreateSecurityGuyHdg[2]

VECTOR vCreateSecurityVanPos
FLOAT fCreateSecurityVanHdg

//FLOAT fInitialOffsetDistDoor1
//FLOAT fInitialOffsetDistDoor2

VECTOR vCaseVanAttachOffset = <<0.0203, -3.1441, 0.5027>>
VECTOR vCaseVanAttachRotation = <<90,0,0>>

VECTOR vInput
VECTOR vRightHandAttachOffset = <<0,0,0>>//<< 0.280, 0.100, -0.100 >>
VECTOR vRightHandAttachRot = <<0,0,0>>//<< -76.000, -28.000, 93.000 >>

VECTOR vTriggerPos1
VECTOR vTriggerPos2
FLOAT fTriggerWidth

MODEL_NAMES modelSecurityGuy
MODEL_NAMES modelSecurityVan
MODEL_NAMES modelCase

BOOL bDoneDialogue = FALSE
BOOL bCallPolice
BOOL bPoliceCalled
BOOL bDepositedBag = FALSE
BOOL bHostile = FALSE
BOOL bPickupAvailable = FALSE
BOOL bPlayerJackedVeh = FALSE
BOOL bVanAbandoned = FALSE
BOOL bDoorsBlown = FALSE
BOOL bWeaponHidden = FALSE
BOOL bPlayerHasDamagedVan = FALSE
BOOL bDoorWasBlown = FALSE
BOOL bMovementSetToInjured[2]
BOOL bCaseCollected = FALSE
BOOL bShowHeightForBlips = FALSE
BOOL bDefensiveAreaSet[2]
BOOL bReportDone = FALSE

INT iTimeOfCall
INT iCaseValue
INT iCurrentCaseRecoverer = 1
INT iCurrentCaseHolder = 1
INT iShockingEventAiming = 0
INT iShootDummyHealthLastFrame = 0
INT iTotalPlayerDamageToDummy = 0
INT iDoorHelpStage
INT iLastDoorHelpTime
INT iDropTIme

PICKUP_INDEX pickupCase
INT iPlacementFlags

STRING sBlock = "RESECAU"

structPedsForConversation sSpeech

CHASE_HINT_CAM_STRUCT localChaseHintCam

LoadQueueMedium sLoadQueue

#IF IS_DEBUG_BUILD

	FUNC STRING GET_STRING_FROM_VAN_STAGE(securityVanStages stage)
		SWITCH stage
			CASE SV_SET_UP						RETURN "SV_SET_UP"
			CASE SV_LOAD_CASE					RETURN "SV_LOAD_CASE"
			CASE SV_HOSTILE_RESPONSE			RETURN "SV_HOSTILE_RESPONSE"
			CASE SV_HOSTILE_RESPONSE_TO_CAR		RETURN "SV_HOSTILE_RESPONSE_TO_CAR"
			CASE SV_DRIVE_OFF					RETURN "SV_DRIVE_OFF"
			CASE SV_FLEE_ON_FOOT				RETURN "SV_FLEE_ON_FOOT"
			CASE SV_FLEE_IN_VAN					RETURN "SV_FLEE_IN_VAN"
			CASE SV_RECOVER_CASE				RETURN "SV_RECOVER_CASE"
			CASE SV_SHOOT_OUT					RETURN "SV_SHOOT_OUT"
			CASE SV_CLEAN_UP					RETURN "SV_CLEAN_UP"
			DEFAULT								RETURN "NO STAGE"
		ENDSWITCH
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_VAN_STATUS()
		SWITCH eVanStatus
			CASE VANSTATUS_PRE_LOADING		RETURN "VANSTATUS_PRE_LOADING"
			CASE VANSTATUS_CASE_LOADING		RETURN "VANSTATUS_CASE_LOADING"
			CASE VANSTATUS_CASE_LOADED		RETURN "VANSTATUS_CASE_LOADED"
			CASE VANSTATUS_DOORS_BLOWN		RETURN "VANSTATUS_DOORS_BLOWN"
			DEFAULT							RETURN "NO STATUS"
		ENDSWITCH
	ENDFUNC
	
	BOOL bBlowDoors
	BOOL bInstantBlowDoors
	
	WIDGET_GROUP_ID vanWidget
	TEXT_WIDGET_ID twStage
	TEXT_WIDGET_ID twVanStatus
	
	PROC CREATE_VAN_WIDGET()
		vanWidget = START_WIDGET_GROUP("RE Securityvan")
		twStage = ADD_TEXT_WIDGET("")
		ADD_WIDGET_INT_READ_ONLY("Stage progress", iStageProgress)
		twVanStatus = ADD_TEXT_WIDGET("")
		ADD_WIDGET_BOOL("Open Doors", bBlowDoors)
		ADD_WIDGET_BOOL("Open Doors instantly", bInstantBlowDoors)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC RUN_VAN_WIDGET()
		SET_CONTENTS_OF_TEXT_WIDGET(twStage, GET_STRING_FROM_VAN_STAGE(thisStage))
		SET_CONTENTS_OF_TEXT_WIDGET(twVanStatus, GET_STRING_FROM_VAN_STATUS())
		IF bBlowDoors
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_RIGHT)
			ENDIF
			bBlowDoors = FALSE
		ENDIF
		IF bInstantBlowDoors
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_LEFT, TRUE)
				SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_RIGHT, TRUE)
			ENDIF
			bInstantBlowDoors = FALSE
		ENDIF
	ENDPROC
	
#ENDIF

PROC SET_STAGE(securityVanStages stage)
	#IF IS_DEBUG_BUILD
		PRINTLN("Going to stage: ", GET_STRING_FROM_VAN_STAGE(stage))
	#ENDIF
	thisStage = stage
	iStageProgress = 0
ENDPROC

// Clean Up
PROC missionCleanup()

	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
	
		IF DOES_PICKUP_EXIST(pickupCase)
			//remove case
			IF DOES_BLIP_EXIST(blipCase)
				REMOVE_BLIP(blipCase)
			ENDIF
			//REMOVE_PICKUP(pickupCase)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objShootDummy)
			DELETE_OBJECT(objShootDummy)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objIdCase)
			IF IS_ENTITY_OCCLUDED(objIdCase)
				DELETE_OBJECT(objIdCase)PRINTLN("Deleting case object 4")
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(objIdCase)
			ENDIF
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(modelSecurityVan)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelSecurityGuy)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCase)
		
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
		SET_PED_MODEL_IS_SUPPRESSED(modelSecurityGuy, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelSecurityVan, FALSE)
		
	ENDIF
	
	IF iShockingEventAiming <> 0
		REMOVE_SHOCKING_EVENT(iShockingEventAiming)
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiSecVan)
	
	KILL_CHASE_HINT_CAM(localChaseHintCam)
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(vanWidget)
			DELETE_WIDGET_GROUP(vanWidget)
		ENDIF
	#ENDIF
	
	RANDOM_EVENT_OVER()
	
	CLEANUP_LOAD_QUEUE_MEDIUM(sLoadQueue)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC missionPassed()
	RANDOM_EVENT_PASSED(RE_SECURITYVAN, ENUM_TO_INT(eWhichWorldPoint))
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	missionCleanup()
ENDPROC

FUNC BOOL loadAssets()

	LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelSecurityGuy)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelSecurityVan)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, modelCase)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, "random@security_van")
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, "weapons@holster_1h")
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(sLoadQueue, "move_injured_generic")
	SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(sLoadQueue, 3)
	
	RETURN HAS_LOAD_QUEUE_MEDIUM_LOADED(sLoadQueue)

ENDFUNC

PROC initialiseVariables()

	modelCase = Prop_Security_case_01
	modelSecurityVan = STOCKADE
	modelSecurityGuy = S_M_M_ARMOURED_01
	iCaseValue = GET_RANDOM_INT_IN_RANGE(3000, 8000)
	
	SWITCH eWhichWorldPoint
	
		CASE SV_VAR_GAS_STATION
		
			vTriggerPos1 = 	<<-315.889008,-1550.554443,7.233013>>
			vTriggerPos2 = 	<<-315.235229,-1366.580811,45.296169>> 
			fTriggerWidth =	180.000000
			
			vCreateSecurityVanPos =  <<-331.8429, -1461.0420, 30.1530>>
			fCreateSecurityVanHdg = -64.3539
			vCreateSecurityGuyPos[0] = << -335.5640, -1462.2369, 29.5452 >> 
			fCreateSecurityGuyHdg[0] = 158.3857 
			vCreateSecurityGuyPos[1] = << -342.7024, -1475.0432, 29.6004 >>
			fCreateSecurityGuyHdg[1] =  290.3041
			
			thisEventVariation = ATTENTIVE_GUARD		
		BREAK
		
		CASE SV_VAR_CLUCKING_BELL
			
			vTriggerPos1 =  <<-538.214905,-868.742615,17.960541>>
			vTriggerPos2 =  <<-663.816223,-867.998718,53.662491>> 
			fTriggerWidth = 121.500000
			
			vCreateSecurityVanPos =  <<-588.512756,-866.958374,25.327860>>
			fCreateSecurityVanHdg = -89.026794
			vCreateSecurityGuyPos[0] = <<-589.4673, -865.4055, 24.7622>>
			fCreateSecurityGuyHdg[0]  = 12.5464
			vCreateSecurityGuyPos[1] = <<-578.4414, -874.5964, 24.9183>>
			fCreateSecurityGuyHdg[1] = 352.4951
			
			thisEventVariation = ATTENTIVE_GUARD
			
		BREAK
		
		CASE SV_VAR_PALETO
		
			vTriggerPos1 = 	<<-468.630890,6101.906738,14.891197>>
			vTriggerPos2 = 	<<-359.973358,5994.151855,50.368435>> 
			fTriggerWidth = 152.250000
			
			vCreateSecurityVanPos =  << -395.6852, 6056.9668, 30.5001 >>
			fCreateSecurityVanHdg = 188.9862 
			vCreateSecurityGuyPos[0] = << -398.4612, 6051.2964, 30.5003 >>
			fCreateSecurityGuyHdg[0] = 251.7138
			vCreateSecurityGuyPos[1] = << -378.4080, 6035.6895, 30.4980 >>
			fCreateSecurityGuyHdg[1] =  154.0836
			
			thisEventVariation = LAZY_GUARD	
			
		BREAK
		
		CASE SV_VAR_CITY
		
			vTriggerPos1 = 	<<-631.340759,-1105.580688,7.220509>>
			vTriggerPos2 = 	<<-462.172150,-1103.597412,62.535828>> 
			fTriggerWidth = 177.250000
			
			vCreateSecurityVanPos =  << -595.7766, -1094.0891, 21.1785 >>
			fCreateSecurityVanHdg = 263.4662
			vCreateSecurityGuyPos[0] = << -600.8182, -1103.7783, 21.3292 >>
			fCreateSecurityGuyHdg[0]  = 338.8304 
			vCreateSecurityGuyPos[1] = << -595.3461, -1096.0538, 21.1785 >>
			fCreateSecurityGuyHdg[1] = 151.2970 
			
			thisEventVariation = ATTENTIVE_GUARD
			
		BREAK
		
		CASE SV_VAR_DRIVING_VENICE
			vCreateSecurityVanPos =  << -1042.7682, -1049.2957, 1.0825 >>
			fCreateSecurityVanHdg = 26.9919
			thisEventVariation = EN_ROUTE
		BREAK
		
		CASE SV_VAR_DRIVING2
			vCreateSecurityVanPos =   << -595.5618, -667.7325, 31.0544 >>
			fCreateSecurityVanHdg = 271.1543
			thisEventVariation = EN_ROUTE
		BREAK
		
		CASE SV_VAR_DRIVING3
			vCreateSecurityVanPos = <<3018.3843, 3634.0168, 70.4076>>
			fCreateSecurityVanHdg = 335.9052
			thisEventVariation = EN_ROUTE
		BREAK
		
		CASE SV_VAR_DRIVING4
			vCreateSecurityVanPos = <<-2815.6614, 2208.1707, 27.8382>>
			fCreateSecurityVanHdg = 119.1519
			thisEventVariation = EN_ROUTE
		BREAK
		
		CASE SV_VAR_DRIVING5
			vCreateSecurityVanPos = <<856.7742, -2067.8452, 29.0704>>
			fCreateSecurityVanHdg = 83.1204
			thisEventVariation = EN_ROUTE
		BREAK
		
		CASE SV_VAR_DRIVING6
			vCreateSecurityVanPos = <<805.1932, -703.1327, 28.1632>>
			fCreateSecurityVanHdg = 248.8625
			thisEventVariation = EN_ROUTE
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("secutity van no worldpoint set")
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC FLOAT MIN(FLOAT a, FLOAT b)
	IF a<b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC FLOAT MAX(FLOAT a, FLOAT b)
	IF a>b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC BOOL DO_LINES_INTERSECT(VECTOR p1, VECTOR p2, VECTOR p3, VECTOR p4, VECTOR &vIntersectPoint)
	// Store the values for fast access and easy
	// equations-to-code conversion
	FLOAT x1 = p1.x
	FLOAT x2 = p2.x
	FLOAT x3 = p3.x
	FLOAT x4 = p4.x
	FLOAT y1 = p1.y
	FLOAT y2 = p2.y
	FLOAT y3 = p3.y
	FLOAT y4 = p4.y
	
	float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
	// If d is zero, there is no intersection
	IF d = 0 
		RETURN FALSE 
	ENDIF
	
	// Get the x and y
	FLOAT fPre = (x1*y2 - y1*x2)
	FLOAT fPost = (x3*y4 - y3*x4)
	float x = ( fPre * (x3 - x4) - (x1 - x2) * fPost ) / d
	float y = ( fPre * (y3 - y4) - (y1 - y2) * fPost ) / d
	 
	// Check if the x and y coordinates are within both lines
	IF x < min(x1, x2) 
	OR x > max(x1, x2) 
	OR x < min(x3, x4)
	OR x > max(x3, x4) 
		RETURN FALSE
	ENDIF
	
	IF y < min(y1, y2) 
	OR y > max(y1, y2)
	OR y < min(y3, y4) 
	OR y > max(y3, y4)
		RETURN FALSE
	ENDIF
	
	// Return the point of intersection
	vIntersectPoint.x = x
	vIntersectPoint.y = y
	vIntersectPoint.z = p1.z
	return TRUE
	
ENDFUNC

FUNC BOOL DO_2D_BOXES_INTERSECT(VECTOR &vBox1[], VECTOR &vBox2[], VECTOR &vIntersect[])

	INT i,j
	
	REPEAT COUNT_OF(vBox1) i
	
		INT iNextPoint1 = i+1
		IF iNextPoint1 = COUNT_OF(vBox1)
			iNextPoint1 = 0
		ENDIF
		
		REPEAT COUNT_OF(vBox2) j
			INT iNextPoint2 = j+1
			IF iNextPoint2 = COUNT_OF(vBox2)
				iNextPoint2 = 0
			ENDIF
			IF DO_LINES_INTERSECT(vBox1[i], vBox1[iNextPoint1], vBox2[j], vBox2[iNextPoint2], vIntersect[i])
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(VEHICLE_INDEX veh)

	BOOL bIntersectionFound = FALSE
	
	IF DOES_ENTITY_EXIST(veh)
	AND DOES_ENTITY_EXIST(vehSecurityVan)
	
		VECTOR vPlayerCarDimensionsMin
		VECTOR vPlayerCarDimensionsMax
		VECTOR vPlayerCarPoints[4]
		VECTOR vRearDoorArea[4]
		VECTOR vLeftDoorArea[4]
		VECTOR vRightDoorArea[4]
		VECTOR vIntersectPoint[4]
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vPlayerCarDimensionsMin, vPlayerCarDimensionsMax)
		
		vPlayerCarPoints[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vPlayerCarDimensionsMin.x, vPlayerCarDimensionsMax.y, 0>>)
		vPlayerCarPoints[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vPlayerCarDimensionsMax.x, vPlayerCarDimensionsMax.y, 0>>)
		vPlayerCarPoints[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vPlayerCarDimensionsMax.x, vPlayerCarDimensionsMin.y, 0>>)
		vPlayerCarPoints[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vPlayerCarDimensionsMin.x, vPlayerCarDimensionsMin.y, 0>>)
		
		FLOAT fRearDoorYOffset = -3.7
		FLOAT fSideDoorXOffset = 1.35
		
		vRearDoorArea[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset - 0.25, 	fRearDoorYOffset, 			0>>)
		vRearDoorArea[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << fSideDoorXOffset + 0.25, 	fRearDoorYOffset,			0>>)
		vRearDoorArea[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << fSideDoorXOffset - 0.25, 	fRearDoorYOffset - 1.55,	0>>)
		vRearDoorArea[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset + 0.25, 	fRearDoorYOffset - 1.55,	0>>)
		
		vLeftDoorArea[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset,			0.5, 0>>)
		vLeftDoorArea[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset - 0.75,	0.5, 0>>)
		vLeftDoorArea[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset - 0.75,	1.5, 0>>)
		vLeftDoorArea[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-fSideDoorXOffset,			1.5, 0>>)
		
		vRightDoorArea[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<fSideDoorXOffset,			0.5, 0>>)
		vRightDoorArea[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<fSideDoorXOffset + 0.75,	0.5, 0>>)
		vRightDoorArea[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<fSideDoorXOffset + 0.75,	1.5, 0>>)
		vRightDoorArea[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<fSideDoorXOffset,			1.5, 0>>)
		
		IF NOT bDepositedBag
			IF DO_2D_BOXES_INTERSECT(vRearDoorArea, vPlayerCarPoints, vIntersectPoint)
				bIntersectionFound = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		AND NOT IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan, TRUE)
			IF DO_2D_BOXES_INTERSECT(vLeftDoorArea, vPlayerCarPoints, vIntersectPoint)	
				bIntersectionFound = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedSecurityGuy[1])
		AND NOT IS_PED_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan, TRUE)
			IF DO_2D_BOXES_INTERSECT(vRightDoorArea, vPlayerCarPoints, vIntersectPoint)
				bIntersectionFound = TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			INT i
			IF NOT bDepositedBag
				REPEAT COUNT_OF(vRearDoorArea) i
					INT iNextPoint1 = i+1
					IF iNextPoint1 = COUNT_OF(vRearDoorArea)
						iNextPoint1 = 0
					ENDIF
					IF bIntersectionFound
						DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 255, 0, 0)
						DRAW_DEBUG_LINE(vRearDoorArea[i], vRearDoorArea[iNextPoint1], 255, 0, 0)
					ELSE
						DRAW_DEBUG_LINE(vRearDoorArea[i], vRearDoorArea[iNextPoint1], 0, 255, 0)
						DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 0, 255, 0)
					ENDIF
				ENDREPEAT
			ENDIF
			
			REPEAT COUNT_OF(vLeftDoorArea) i
				INT iNextPoint1 = i+1
				IF iNextPoint1 = COUNT_OF(vLeftDoorArea)
					iNextPoint1 = 0
				ENDIF
				IF bIntersectionFound
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 255, 0, 0)
					DRAW_DEBUG_LINE(vLeftDoorArea[i], vLeftDoorArea[iNextPoint1], 255, 0, 0)
				ELSE
					DRAW_DEBUG_LINE(vLeftDoorArea[i], vLeftDoorArea[iNextPoint1], 0, 255, 0)
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 0, 255, 0)
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(vRightDoorArea) i
				INT iNextPoint1 = i+1
				IF iNextPoint1 = COUNT_OF(vRightDoorArea)
					iNextPoint1 = 0
				ENDIF
				IF bIntersectionFound
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 255, 0, 0)
					DRAW_DEBUG_LINE(vRightDoorArea[i], vRightDoorArea[iNextPoint1], 255, 0, 0)
				ELSE
					DRAW_DEBUG_LINE(vRightDoorArea[i], vRightDoorArea[iNextPoint1], 0, 255, 0)
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 0, 255, 0)
				ENDIF
			ENDREPEAT
			
			IF bIntersectionFound
				REPEAT COUNT_OF(vIntersectPoint) i
					IF NOT IS_VECTOR_ZERO(vIntersectPoint[i])
						DRAW_DEBUG_SPHERE(vIntersectPoint[i], 0.2, 255, 0, 0)
					ENDIF
				ENDREPEAT
			ENDIF
			
		#ENDIF
		
	ENDIF

	RETURN bIntersectionFound

ENDFUNC

PROC SET_CASE_INTO_PED_HAND(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		IF NOT DOES_ENTITY_EXIST(objIdCase)
			objIdCase = CREATE_OBJECT(modelCase, vCreateSecurityVanPos)
			SET_ENTITY_CAN_BE_DAMAGED(objIdCase, FALSE)
		ENDIF
		IF DOES_ENTITY_EXIST(objIdCase)
			ATTACH_ENTITY_TO_ENTITY(objIdCase, ped , GET_PED_BONE_INDEX(ped, BONETAG_PH_R_HAND), vRightHandAttachOffset, vRightHandAttachRot, TRUE, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC createScene()

	vehSecurityVan = CREATE_VEHICLE(modelSecurityVan, vCreateSecurityVanPos, fCreateSecurityVanHdg)
	objShootDummy = CREATE_OBJECT(PROP_SECURITY_CASE_01, vCreateSecurityVanPos)
	ATTACH_ENTITY_TO_ENTITY(objShootDummy, vehSecurityVan, 0, <<0, -2.4589, 1.2195>>, <<0,0,0>>)
	SET_ENTITY_VISIBLE(objShootDummy, FALSE)
	SET_ENTITY_NO_COLLISION_ENTITY(objShootDummy, vehSecurityVan, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehSecurityVan)
	SET_VEHICLE_DOORS_LOCKED(vehSecurityVan, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	SET_ENTITY_PROOFS(objShootDummy, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE)
//	SET_VEHICLE_DOOR_LATCHED(vehSecurityVan, SC_DOOR_REAR_LEFT, FALSE, FALSE)
//	SET_VEHICLE_DOOR_LATCHED(vehSecurityVan, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(vehSecurityVan, TRUE)
	SET_ENTITY_HEALTH(vehSecurityVan, 700)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehSecurityVan, FALSE)
	SET_ENTITY_LOAD_COLLISION_FLAG(vehSecurityVan, TRUE)
	SET_VEHICLE_DROPS_MONEY_WHEN_BLOWN_UP(vehSecurityVan, FALSE)
	SET_VEHICLE_PROVIDES_COVER(vehSecurityVan, TRUE)
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vCreateSecurityVanPos.x - 200, vCreateSecurityVanPos.y - 200, vCreateSecurityVanPos.x + 200, vCreateSecurityVanPos.y + 200)
	
//	fInitialOffsetDistDoor1 = VDIST(GET_ENTITY_COORDS(vehSecurityVan), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 10))
//	fInitialOffsetDistDoor2 = VDIST(GET_ENTITY_COORDS(vehSecurityVan), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 13))
	
	IF thisEventVariation = EN_ROUTE
		pedSecurityGuy[0] = CREATE_PED_INSIDE_VEHICLE (vehSecurityVan, PEDTYPE_MISSION, modelSecurityGuy, VS_DRIVER)
		pedSecurityGuy[1] = CREATE_PED_INSIDE_VEHICLE(vehSecurityVan, PEDTYPE_MISSION, modelSecurityGuy, VS_FRONT_RIGHT)
		SET_VEHICLE_ENGINE_ON(vehSecurityVan, TRUE, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(vehSecurityVan, TRUE)
	ELSE
		pedSecurityGuy[1] = CREATE_PED(PEDTYPE_MISSION, modelSecurityGuy, vCreateSecurityGuyPos[1], fCreateSecurityGuyHdg[1])
		pedSecurityGuy[0] = CREATE_PED(PEDTYPE_MISSION, modelSecurityGuy, vCreateSecurityGuyPos[0], fCreateSecurityGuyHdg[0])
		TASK_LOOK_AT_ENTITY(pedSecurityGuy[1], pedSecurityGuy[0], -1)
	ENDIF
	
	SET_CASE_INTO_PED_HAND(pedSecurityGuy[1])
	ADD_RELATIONSHIP_GROUP("Security_guards", security)
	
	INT i
	REPEAT COUNT_OF(pedSecurityGuy) i
		SET_PED_MONEY(pedSecurityGuy[i], 0)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[i], CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[i], CA_AGGRESSIVE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[i], CA_FLEE_WHILST_IN_VEHICLE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[i], CA_PLAY_REACTION_ANIMS, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[i], CA_IS_A_GUARD, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedSecurityGuy[i], FA_NEVER_FLEE, TRUE)
		//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[i], TRUE)
		SET_PED_CONFIG_FLAG(pedSecurityGuy[i], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedSecurityGuy[i], FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedSecurityGuy[i], FALSE)
		SET_ENTITY_IS_TARGET_PRIORITY(pedSecurityGuy[i], TRUE)
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedSecurityGuy[i], TRUE)
		SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(pedSecurityGuy[i], TRUE)
		GIVE_WEAPON_TO_PED(pedSecurityGuy[i], WEAPONTYPE_PISTOL,INFINITE_AMMO, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedSecurityGuy[i], security)
		SET_PED_KEEP_TASK(pedSecurityGuy[i], TRUE)
		SET_ENTITY_LOD_DIST(pedSecurityGuy[i], 250)
	ENDREPEAT
	
	SET_ENTITY_LOAD_COLLISION_FLAG(pedSecurityGuy[1], TRUE)
	
	#IF IS_DEBUG_BUILD 
		SET_PED_NAME_DEBUG(pedSecurityGuy[0], "Guard zero")
		SET_PED_NAME_DEBUG(pedSecurityGuy[1], "Guard one")
	#ENDIF
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, security)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, security, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, security, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, security)
	
	IF thisEventVariation = ATTENTIVE_GUARD
		SET_PED_PROP_INDEX(pedSecurityGuy[0], ANCHOR_HEAD, 1)
		SET_PED_SUFFERS_CRITICAL_HITS(pedSecurityGuy[0], FALSE)
		SET_PED_PROP_INDEX(pedSecurityGuy[1], ANCHOR_HEAD, 1)
		SET_PED_SUFFERS_CRITICAL_HITS(pedSecurityGuy[1], FALSE)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedSecurityGuy[0], "SECVANGUY1")
	ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedSecurityGuy[1], "SECVANGUY2")
		
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelSecurityVan, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(modelSecurityGuy, TRUE)
	
	IF thisEventVariation != EN_ROUTE
		//blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vCreateSecurityVanPos)
	ELSE
		//blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(vehSecurityVan)
	ENDIF
	
ENDPROC

FUNC BOOL IS_BAD_EXPLOSION_IN_AREA(VECTOR vAreaMax, VECTOR vAreaMin, VECTOR vCentre, FLOAT fRadius = 0.0, BOOL bIsDoorBlownCheck = FALSE)

	BOOL bRet = FALSE
	
	IF NOT IS_VECTOR_ZERO(vAreaMax+vAreaMin)
		IF IS_EXPLOSION_IN_AREA(EXP_TAG_TANKSHELL, 			vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, 		vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, 	vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, 			vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_PROPANE, 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PLANE_ROCKET, 		vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, 	vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PETROL_PUMP, 		vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_HI_OCTANE,			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, 			vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, 	vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_CANISTER, 		vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_TANK, 			vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_GAS_CANISTER, 	vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_FLAME_EXPLODE, vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_FLAME, 		vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_CAR , 				vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_BZGAS , 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_BULLET , 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BOAT , 				vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BIKE , 				vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BARREL , 			vAreaMin, vAreaMax)
			bRet = TRUE
		ENDIF
	ELSE
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_TANKSHELL, 			vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE, 		vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, 				vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROPANE, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PLANE_ROCKET, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROGRAMMABLEAR, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PETROL_PUMP, 			vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_HI_OCTANE,			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_CANISTER, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_TANK, 			vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_GAS_CANISTER, 	vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME_EXPLODE,	vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR , 				vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS , 				vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_BULLET , 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BOAT , 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE , 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BARREL , 				vCentre, fRadius)
			bRet = TRUE										
		ENDIF
	ENDIF
	
	RETURN bRet
	
ENDFUNC

PROC removeAllBlips()
	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	IF DOES_BLIP_EXIST(blipSecurityVan)
		REMOVE_BLIP(blipSecurityVan)
	ENDIF
	IF DOES_BLIP_EXIST(blipSecurityGuy[0])
		REMOVE_BLIP(blipSecurityGuy[0])
	ENDIF
	IF DOES_BLIP_EXIST(blipSecurityGuy[1])
		REMOVE_BLIP(blipSecurityGuy[1])
	ENDIF
ENDPROC

PROC createCasePickup(VECTOR vCoords, BOOL bCreateAtDummy = FALSE)
	IF NOT bCaseCollected
	AND NOT bPickupAvailable
		IF NOT bCreateAtDummy
			IF bDepositedBag
				vCoords = GET_SAFE_PICKUP_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0,-3.3,0>>))
			ELSE
				vCoords = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(pedSecurityGuy[iCurrentCaseHolder], FALSE))
			ENDIF
		ENDIF
		PRINTLN("CREATING CASE PICKUP AT: ", vCoords)
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		IF bCreateAtDummy
			pickupCase = CREATE_PICKUP_ROTATE(PICKUP_MONEY_SECURITY_CASE, vCoords, GET_ENTITY_ROTATION(objIdCase), 0, iCaseValue)
		ELSE
			pickupCase = CREATE_PICKUP(PICKUP_MONEY_SECURITY_CASE, vCoords, iPlacementFlags, iCaseValue)	
		ENDIF
		IF NOT DOES_BLIP_EXIST(blipCase)
			blipCase = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupCase)
		ENDIF
		IF DOES_ENTITY_EXIST(objIdCase)
			DELETE_OBJECT(objIdCase)
		ENDIF
		bPickupAvailable = TRUE
	ENDIF
ENDPROC

INT iTimeOfObjectiveLoss = -1

FUNC BOOL IS_TARGET_IN_RANGE(ENTITY_INDEX target)
	IF DOES_ENTITY_EXIST(target)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(target)) < 150
			RETURN TRUE
		ELIF NOT IS_ENTITY_OCCLUDED(target)
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(target)) < 300
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bBombAttachedToCase
INT iLastBulletCheck
INT iBulletCount
FLOAT fYoffset = -3.55

FUNC BOOL HAS_REAR_DOOR_BEEN_BLOWN_WITHOUT_STICKYBOMB()
	IF DOES_ENTITY_EXIST(objShootDummy)
	AND IS_ENTITY_ATTACHED(objShootDummy)
	
		IF NOT bBombAttachedToCase
			IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(objShootDummy), WEAPONTYPE_STICKYBOMB, 0.25)
			OR DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_ENTITY_COORDS(objShootDummy), 0.3, GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB))
				bBombAttachedToCase = TRUE
				PRINTLN("\n\n\n\n STICKYBOMB ON CASE \n\n\n\n\n")
			ENDIF
		ENDIF
		
		BOOL bExplosion = FALSE
		DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objShootDummy), 0.1, 255, 0,0)
		IF IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(objShootDummy), 1, TRUE)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(objShootDummy) , 0.3)  AND bBombAttachedToCase)
			bDoorWasBlown = TRUE
			PRINTLN("DOOR WAS EXPLODED WITHOUT STICKYBOMB")
			bExplosion = TRUE
		ENDIF
		
		IF GET_ENTITY_HEALTH(objShootDummy) < iShootDummyHealthLastFrame
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objShootDummy, PLAYER_PED_ID(), FALSE)
			iTotalPlayerDamageToDummy += (iShootDummyHealthLastFrame - GET_ENTITY_HEALTH(objShootDummy))
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(objShootDummy)
		ENDIF
		
		IF GET_GAME_TIMER() - iLastBulletCheck > 200
		AND DOES_ENTITY_EXIST(vehSecurityVan)
			IF IS_BULLET_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0,0,1.2195-0.27>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, fYoffset, 1.2195+0.09>>), 0.45)
				iLastBulletCheck = GET_GAME_TIMER()
				iBulletCount++
			ENDIF
		ENDIF		
		
		IF iBulletCount > 5
			RETURN TRUE
		ENDIF
		
		IF iTotalPlayerDamageToDummy > 100 OR bExplosion
			PRINTLN("DOOR BLOWN OPEN")
			RETURN TRUE
		ENDIF
		
		iShootDummyHealthLastFrame = GET_ENTITY_HEALTH(objShootDummy)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_LOST_OBJECTIVE()

	IF NOT DOES_PICKUP_EXIST(pickupCase)
	
		IF bDepositedBag
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("Checking van", <<0.8,0.2,0>>)
				#ENDIF
				IF NOT IS_TARGET_IN_RANGE(vehSecurityVan)
					IF iTimeOfObjectiveLoss = -1
						iTimeOfObjectiveLoss = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF iTimeOfObjectiveLoss <> -1
						iTimeOfObjectiveLoss = -1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bDepositedBag
			IF iCurrentCaseHolder <> -1
				IF NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_2D("Checking guard", <<0.8,0.2,0>>)
					#ENDIF
					IF NOT IS_TARGET_IN_RANGE(vehSecurityVan)
						IF iTimeOfObjectiveLoss = -1
							iTimeOfObjectiveLoss = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF iTimeOfObjectiveLoss <> -1
							iTimeOfObjectiveLoss = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iTimeOfObjectiveLoss <> -1
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("Amount of time lost", <<0.7,0.22,0>>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(GET_GAME_TIMER() - iTimeOfObjectiveLoss), <<0.8,0.22,0>>)
			#ENDIF
			IF (GET_GAME_TIMER() - iTimeOfObjectiveLoss) > 3000
				SET_PED_AS_NO_LONGER_NEEDED(pedSecurityGuy[0])
				SET_PED_AS_NO_LONGER_NEEDED(pedSecurityGuy[1])
				STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, INSTANT_BLEND_OUT, TRUE)
				SET_ENTITY_COLLISION(vehSecurityVan, TRUE)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehSecurityVan)
				RETURN TRUE	
			ENDIF
		ENDIF
		
	ELSE
	
		IF iTimeOfObjectiveLoss <> -1
			iTimeOfObjectiveLoss = -1
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND VDIST(GET_PICKUP_COORDS(pickupCase), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 300
			PRINTLN("Left the case behind")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_TOWING_SECURITY_VAN()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = TOWTRUCK
			OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = TOWTRUCK2
			    IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehSecurityVan)
					RETURN TRUE
			    ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_CLIMBING_ON_BONNET()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << 0.0, 1.7532, 0.5984>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0.0, 3.3477, 3.5984>>), 2.8)
			RETURN TRUE
		ENDIF
		INT i
		REPEAT COUNT_OF(pedSecurityGuy) i
			IF NOT IS_PED_INJURED(pedSecurityGuy[i])
				IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[i], vehSecurityVan)
				AND IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), vehSecurityVan)
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[i])) < 20*20
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLANTING_BOMB_NEAR_VAN()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << 2.7, 7.0, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<2.7, -3.25, 3>>), 3)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << -2.7, 7.0, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<-2.7, -3.25, 3>>), 3)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, << 0, 7.0, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, 0, 3>>), 2.4)
			IF IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_CASE()

	IF NOT bCaseCollected
		IF NOT bDepositedBag
			IF DOES_ENTITY_EXIST(objIdCase)
				IF NOT IS_ENTITY_ATTACHED(objIdCase)
					DELETE_OBJECT(objIdCase)PRINTLN("Deleting case object 1")
					IF NOT DOES_PICKUP_EXIST(pickupCase)
					AND NOT bPickupAvailable
						PRINTLN("Creating case here 1")
						createCasePickup(<<0,0,0>>)
					ENDIF
				ELSE
					IF GET_GAME_TIMER() - iDropTIme > 3000
						IF IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
						OR IS_PED_RAGDOLL(pedSecurityGuy[iCurrentCaseHolder])
							DETACH_ENTITY(objIdCase)
							iDropTime = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_PICKUP_EXIST(pickupCase)
					IF IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
					OR (NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder]) AND IS_PED_RAGDOLL(pedSecurityGuy[iCurrentCaseHolder]))
					AND NOT bPickupAvailable
						createCasePickup(<<0,0,0>>)
						PRINTLN("Creating case here 2")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(objIdCase)
			DELETE_OBJECT(objIdCase)
		ENDIF
		IF DOES_PICKUP_EXIST(pickupCase)
			REMOVE_PICKUP(pickupCase)
		ENDIF
	ENDIF
	
	//	#IF IS_DEBUG_BUILD
	//		IF DOES_ENTITY_EXIST(vehSecurityVan)
	//		AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	//			DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(ABSF(VDIST(GET_ENTITY_COORDS(vehSecurityVan), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 13))) - fInitialOffsetDistDoor1), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 13))
	//			DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(ABSF(VDIST(GET_ENTITY_COORDS(vehSecurityVan), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 10))) - fInitialOffsetDistDoor2), GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 10))
	//		ENDIF
	//	#ENDIF
	
ENDPROC

VECTOR vVanInteriorCheckLineP1 = <<0.0140, -0.9111, 1.2317>>
VECTOR vVanInteriorCheckLineP2 = <<0.0000, -3.4368, 1.2317>>
INT iJetisonTime
VECTOR vForce

PROC UPDATE_VAN_STATUS()

	IF HAS_REAR_DOOR_BEEN_BLOWN_WITHOUT_STICKYBOMB()
		IF DOES_ENTITY_EXIST(objShootDummy)
			SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_RIGHT)
			IF DOES_ENTITY_EXIST(objShootDummy)
				DELETE_OBJECT(objShootDummy)
			ENDIF
		ENDIF
	ENDIF
	
//	IF DOES_ENTITY_EXIST(objShootDummy)
//		SET_ENTITY_COLLISION(objShootDummy, NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE))
//	ENDIF
	
	SWITCH eVanStatus
	
		CASE VANSTATUS_PRE_LOADING
			IF bDepositedBag
				eVanStatus = VANSTATUS_CASE_LOADING
			ENDIF
		BREAK
		
		CASE VANSTATUS_CASE_LOADING
			PRINTLN("SC_DOOR_REAR_LEFT", GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_LEFT))
			PRINTLN("SC_DOOR_REAR_RIGHT", GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_RIGHT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_LEFT) = 0
			AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_RIGHT) = 0
				IF DOES_ENTITY_EXIST(objIdCase)
					IF IS_ENTITY_ATTACHED(objIdCase)
						DETACH_ENTITY(objIdCase)
					ENDIF
					ATTACH_ENTITY_TO_ENTITY(objIdCase, vehSecurityVan, 0, vCaseVanAttachOffset, vCaseVanAttachRotation, FALSE, FALSE, FALSE, FALSE, DEFAULT, TRUE)
					eVanStatus = VANSTATUS_CASE_LOADED
				ENDIF
			ENDIF
		BREAK
		
		CASE VANSTATUS_CASE_LOADED
		
			IF NOT bPickupAvailable
			AND NOT bCaseCollected
				IF NOT IS_ENTITY_DEAD(vehSecurityVan)
					IF (IS_VEHICLE_DRIVEABLE(vehSecurityVan) AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_LEFT) > 0.25 AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_REAR_RIGHT) > 0.25)
					OR NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
						IF DOES_ENTITY_EXIST(objIdCase)
							IF IS_ENTITY_ATTACHED(objIdCase)
								PLAY_SOUND_FROM_COORD(GET_SOUND_ID(), "DOORS_BLOWN", GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 13), "RE_SECURITY_VAN_SOUNDSET")
								DETACH_ENTITY(objIdCase, TRUE, FALSE)
								SET_ENTITY_COLLISION(objIdCase, TRUE)
								ACTIVATE_PHYSICS(objIdCase)
								SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(objIdCase, TRUE)
								vForce = GET_ENTITY_COORDS(objIdCase) - GET_ENTITY_COORDS(vehSecurityVan) * 1.5
								IF VMAG(vForce) > 10
									vForce = NORMALISE_VECTOR(vForce)
									vForce *= 10.0
								ENDIF
								IF iTotalPlayerDamageToDummy < 100
									PRINTLN("DOOR WAS BLOWN WITH STICKYBOMB")
									bDoorWasBlown = TRUE
								ENDIF
								APPLY_FORCE_TO_ENTITY(objIdCase, APPLY_TYPE_IMPULSE, <<0,1,5>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
								iJetisonTime = GET_GAME_TIMER()
							ELSE
								IF GET_RATIO_OF_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(objIdCase), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, vVanInteriorCheckLineP1), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, vVanInteriorCheckLineP2), FALSE) >= 1.0
									eVanStatus = VANSTATUS_DOORS_BLOWN
									bDefensiveAreaSet[0] = FALSE
									bDefensiveAreaSet[1] = FALSE
								ELSE
									IF GET_GAME_TIMER() - iJetisonTime > 5000
										createCasePickup(<<0,0,0>>)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(vehSecurityVan)
						//IF NOT IS_ENTITY_IN_WATER(vehSecurityVan)	
							SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_LEFT)
							SET_VEHICLE_DOOR_OPEN(vehSecurityVan, SC_DOOR_REAR_RIGHT)
							createCasePickup(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0,2,0>>))
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE VANSTATUS_DOORS_BLOWN
			IF thisStage != SV_SHOOT_OUT
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
			IF DOES_ENTITY_EXIST(objIdCase)
			AND NOT DOES_PICKUP_EXIST(pickupCase)
				IF GET_ENTITY_SPEED(objIdCase) = 0
				AND GET_GAME_TIMER() - iJetisonTime > 1000
					createCasePickup(GET_ENTITY_COORDS(objIdCase), TRUE)
					DELETE_OBJECT(objIdCase)PRINTLN("Deleting case object 2")
				ELSE
					IF GET_GAME_TIMER() - iJetisonTime > 5000
						createCasePickup(<<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_CASE_BEEN_COLLECTED()
//
//	INT iEventID
//	STRUCT_PICKUP_EVENT Data
	
//	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
//		IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID) = EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
//			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Data, SIZE_OF(Data))
//				IF Data.PickupType = PICKUP_MONEY_SECURITY_CASE
//					IF Data.PlayerIndex = PLAYER_ID()
//						PRINTLN("PICKUP HAS BEEN COLLECTED")
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//
	IF bPickupAvailable
		IF HAS_PICKUP_BEEN_COLLECTED(pickupCase)
			PRINTLN("PICKUP HAS BEEN COLLECTED")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_BLIPS()

	IF NOT DOES_BLIP_EXIST(blipInitialMarker)
	
		INT iCountPeds
		IF NOT bHostile
		
			IF NOT IS_PED_INJURED(pedSecurityGuy[1])
			AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			AND NOT bCaseCollected
				IF NOT bDepositedBag
					IF NOT DOES_BLIP_EXIST(blipSecurityGuy[1])
						blipSecurityGuy[1] = CREATE_BLIP_FOR_PED(pedSecurityGuy[1])
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipSecurityGuy[1])
						REMOVE_BLIP(blipSecurityGuy[1])
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipSecurityVan)
						blipSecurityVan = CREATE_BLIP_FOR_VEHICLE(vehSecurityVan)
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipSecurityGuy[1])
					REMOVE_BLIP(blipSecurityGuy[1])
				ENDIF
				IF DOES_BLIP_EXIST(blipSecurityVan)
					REMOVE_BLIP(blipSecurityVan)
				ENDIF
			ENDIF
			
		ELSE
		
			BOOL bVanBlipNeeded
			BOOL bPedBlipNeeded[2]
			
			IF DOES_BLIP_EXIST(blipSecurityGuy[0])
			AND GET_BLIP_HUD_COLOUR(blipSecurityGuy[0]) <> HUD_COLOUR_ENEMY
				REMOVE_BLIP(blipSecurityGuy[0])
			ENDIF
			
			IF DOES_BLIP_EXIST(blipSecurityGuy[1])
			AND GET_BLIP_HUD_COLOUR(blipSecurityGuy[1]) <> HUD_COLOUR_ENEMY
				REMOVE_BLIP(blipSecurityGuy[1])
			ENDIF
			
			IF DOES_BLIP_EXIST(blipSecurityVan)
			AND GET_BLIP_HUD_COLOUR(blipSecurityVan) <> HUD_COLOUR_ENEMY
				REMOVE_BLIP(blipSecurityVan)
			ENDIF
			
//			//check all cases for security van blip
//			IF NOT IS_ENTITY_DEAD(vehSecurityVan)
//			AND NOT DOES_PICKUP_EXIST(pickupCase)
//			AND bDepositedBag
//			AND NOT bCaseCollected
//				bVanBlipNeeded = TRUE
//			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				REPEAT COUNT_OF(pedSecurityGuy) iCountPeds
					IF NOT IS_PED_INJURED(pedSecurityGuy[iCountPeds])
						IF IS_PED_IN_VEHICLE(pedSecurityGuy[iCountPeds], vehSecurityVan)
							bVanBlipNeeded = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bVanBlipNeeded
				IF NOT DOES_BLIP_EXIST(blipSecurityVan)
					blipSecurityVan = CREATE_BLIP_FOR_VEHICLE(vehSecurityVan, TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipSecurityVan)
					REMOVE_BLIP(blipSecurityVan)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			AND DOES_BLIP_EXIST(blipSecurityVan)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan)
					IF GET_BLIP_ALPHA(blipSecurityVan) <> 0
						SET_BLIP_ALPHA(blipSecurityVan, 0)
					ENDIF
				ELSE
					IF GET_BLIP_ALPHA(blipSecurityVan) <> 255
						SET_BLIP_ALPHA(blipSecurityVan, 255)
					ENDIF
				ENDIF
			ENDIF
			
			//check cases for ped blips
			
			REPEAT COUNT_OF(pedSecurityGuy) iCountPeds
			
				IF NOT IS_PED_INJURED(pedSecurityGuy[iCountPeds])
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT DOES_PICKUP_EXIST(pickupCase)
					AND NOT IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iCountPeds])
						IF NOT IS_PED_FLEEING(pedSecurityGuy[iCountPeds])
						OR VDIST(GET_ENTITY_COORDS(pedSecurityGuy[iCountPeds]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 30
							bPedBlipNeeded[iCountPeds] = TRUE
						ENDIF
					ENDIF
					IF thisStage = SV_SHOOT_OUT
					AND NOT IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iCountPeds])
						bPedBlipNeeded[iCountPeds] = TRUE
					ENDIF
					IF thisStage = SV_FLEE_ON_FOOT
						bPedBlipNeeded[iCountPeds] = FALSE
					ENDIF
				ENDIF
				
				IF bPedBlipNeeded[iCountPeds]
					IF NOT DOES_BLIP_EXIST(blipSecurityGuy[iCountPeds])
						blipSecurityGuy[iCountPeds] = CREATE_BLIP_FOR_PED(pedSecurityGuy[iCountPeds], TRUE)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipSecurityGuy[iCountPeds])
						REMOVE_BLIP(blipSecurityGuy[iCountPeds])
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			IF DOES_PICKUP_EXIST(pickupCase)
			AND NOT bCaseCollected
				IF NOT DOES_BLIP_EXIST(blipCase)
					blipCase = CREATE_BLIP_FOR_PICKUP(pickupCase)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipCase)
					REMOVE_BLIP(blipCase)
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
	
		IF DOES_ENTITY_EXIST(vehSecurityVan)
		AND DOES_ENTITY_EXIST(pedSecurityGuy[0])
		AND DOES_ENTITY_EXIST(pedSecurityGuy[1])		
			IF (NOT IS_ENTITY_OCCLUDED(vehSecurityVan) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehSecurityVan, <<100, 100, 30>>))
			OR (NOT IS_ENTITY_OCCLUDED(pedSecurityGuy[0]) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[0], <<100, 100, 30>>))
			OR (NOT IS_ENTITY_OCCLUDED(pedSecurityGuy[1]) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[1], <<100, 100, 30>>))
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehSecurityVan, <<50, 50, 20>>)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[0], <<50, 50, 20>>)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[1], <<50, 50, 20>>)
				REMOVE_BLIP(blipInitialMarker)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF DOES_BLIP_EXIST(blipSecurityGuy[0])
		SHOW_HEIGHT_ON_BLIP(blipSecurityGuy[0], bShowHeightForBlips)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipSecurityGuy[1])
		SHOW_HEIGHT_ON_BLIP(blipSecurityGuy[1], bShowHeightForBlips)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipSecurityVan)
		SHOW_HEIGHT_ON_BLIP(blipSecurityVan, bShowHeightForBlips)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipCase)
		SHOW_HEIGHT_ON_BLIP(blipCase, bShowHeightForBlips)
	ENDIF
	
	IF NOT bShowHeightForBlips
		IF bHostile
		OR eVanStatus = VANSTATUS_DOORS_BLOWN
			bShowHeightForBlips = FALSE
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL ARE_SECURITY_PEDS_IN_SECURITY_VAN()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		AND NOT IS_PED_INJURED(pedSecurityGuy[1])
			IF IS_PED_SITTING_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
			AND IS_PED_SITTING_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN()
	BOOL bAllIn = TRUE
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		AND NOT IS_ENTITY_DEAD(pedSecurityGuy[0])
			IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
				PRINTLN("PED 0 NOT IN VAN")
				bAllIn = FALSE
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedSecurityGuy[1])
		AND NOT IS_ENTITY_DEAD(pedSecurityGuy[1])
			IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan)
				PRINTLN("PED 1 NOT IN VAN")
				bAllIn = FALSE
			ENDIF
		ENDIF
	ELSE
		bAllIn = FALSE
	ENDIF
	RETURN bAllIn
ENDFUNC

FUNC BOOL HAS_VAN_TAKEN_SUFFICIENT_DAMAGE_TO_PULL_OVER()
	IF DOES_ENTITY_EXIST(vehSecurityVan)
		IF NOT IS_ENTITY_DEAD(vehSecurityVan)
			IF GET_ENTITY_HEALTH(vehSecurityVan) < 300
			OR GET_VEHICLE_ENGINE_HEALTH(vehSecurityVan) < 200
			OR (IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_FRONT_RIGHT))
			OR (IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_REAR_LEFT) AND IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_REAR_RIGHT))
			OR (IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_REAR_LEFT))
			OR (IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_FRONT_RIGHT) AND IS_VEHICLE_TYRE_BURST(vehSecurityVan, SC_WHEEL_CAR_REAR_RIGHT))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ATTACKED_SECURITY_GUYS_OR_VAN()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF bPlayerHasDamagedVan
			RETURN TRUE
		ENDIF
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		OR IS_ENTITY_DEAD(pedSecurityGuy[0])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[0], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedSecurityGuy[1])
		OR IS_ENTITY_DEAD(pedSecurityGuy[1])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[1], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC aimAtPlayer(INT iGuard)
	IF NOT IS_PED_INJURED(pedSecurityGuy[iGuard])
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(pedSecurityGuy[iGuard])
		OPEN_SEQUENCE_TASK(Seq)
			IF IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iGuard])
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
			ENDIF
			TASK_CLEAR_LOOK_AT(NULL)
			TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
			TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
		CLOSE_SEQUENCE_TASK(Seq)
		TASK_PERFORM_SEQUENCE(pedSecurityGuy[iGuard], Seq)
		CLEAR_SEQUENCE_TASK(Seq)
		IF iShockingEventAiming = 0 
			iShockingEventAiming = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_SEEN_WEAPON_THREAT, GET_ENTITY_COORDS(pedSecurityGuy[iGuard]))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEARBY_AND_SHOOTING()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF (NOT IS_PED_INJURED(pedSecurityGuy[0]) AND IS_ENTITY_AT_ENTITY(pedSecurityGuy[0], PLAYER_PED_ID(), <<20, 20, 5>>))
		OR (NOT IS_PED_INJURED(pedSecurityGuy[1]) AND IS_ENTITY_AT_ENTITY(pedSecurityGuy[1], PLAYER_PED_ID(), <<20, 20, 5>>))
			IF(IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN) AND IS_PED_SHOOTING(PLAYER_PED_ID()))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_NEXT_TO_VAN(VEHICLE_INDEX veh)
	RETURN IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND DOES_ENTITY_EXIST(veh)
	AND IS_ENTITY_IN_ANGLED_AREA(veh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0,6,-2>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0,-9,2>>), 10)
ENDFUNC

FLOAT fStopTime

FUNC BOOL HAS_PLAYER_PARKED_TOO_CLOSE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(GET_PLAYERS_LAST_VEHICLE())
					vehOffendingVehicle = GET_PLAYERS_LAST_VEHICLE()
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					fStopTime += GET_FRAME_TIME()
				ELSE
					IF fStopTime > 0
						fStopTime -= GET_FRAME_TIME() * 10
					ELIF fStopTime < 0
						fStopTime = 0
					ENDIF
				ENDIF
				IF fStopTime > 3.0
					vehOffendingVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PED_INDEX GET_PRIMARY_GUARD()
	IF NOT IS_PED_INJURED(pedSecurityGuy[0])
	AND NOT IS_ENTITY_DEAD(pedSecurityGuy[0])
		RETURN pedSecurityGuy[0]
	ELIF NOT IS_PED_INJURED(pedSecurityGuy[1])
	AND NOT IS_ENTITY_DEAD(pedSecurityGuy[1])
		RETURN pedSecurityGuy[1]
	ELSE
		RETURN NULL
	ENDIF
ENDFUNC

FUNC BOOL IS_PLAYER_NEARBY_AND_ARMED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		WEAPON_TYPE playerWeap
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWeap)
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE)
		OR (playerWeap = WEAPONTYPE_PETROLCAN AND IS_PED_SHOOTING(PLAYER_PED_ID()))
			IF IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
					IF IS_ENTITY_AT_ENTITY(pedSecurityGuy[0], PLAYER_PED_ID(), <<12, 12, 5>>)
						RETURN TRUE
					ENDIF
				ENDIF	
				IF NOT IS_PED_INJURED(pedSecurityGuy[1])
					IF IS_ENTITY_AT_ENTITY(pedSecurityGuy[1], PLAYER_PED_ID(), <<12, 12, 5>>)
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_SECUITY_GUARDS_IN_VAN()

	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND IS_PLAYER_PLAYING(PLAYER_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		VECTOR vVanToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(vehSecurityVan)
		VECTOR vVanForward = GET_ENTITY_FORWARD_VECTOR(vehSecurityVan)
		VECTOR vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehSecurityVan, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		
//		#IF IS_DEBUG_BUILD
//			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vVanForward.x, vVanForward.y, vVanToPlayer.x, vVanToPlayer.y))), <<0.8, 0.7, 0>>)
//			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_ANGLE_BETWEEN_2D_VECTORS(vVanForward.y, vVanForward.z, vVanToPlayer.y, vVanToPlayer.z)) , <<0.8, 0.72, 0>>)
//			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehSecurityVan, GET_ENTITY_COORDS(PLAYER_PED_ID()))), <<0.8, 0.74, 0>>)
//		#ENDIF
//		
		IF vPlayerOffset.y > 1
		AND (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), vehSecurityVan) 
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), vehSecurityVan) 
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurityGuy[0]) 
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurityGuy[1]))
		AND ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vVanForward.x, vVanForward.y, vVanToPlayer.x, vVanToPlayer.y)) < 60
		AND GET_ANGLE_BETWEEN_2D_VECTORS(vVanForward.z, vVanForward.y, vVanToPlayer.z, vVanToPlayer.y) < 30
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

BOOL bPlayerIsVisibleToGuard[2]
INT iLastSightCheck

FUNC BOOL CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
	IF (GET_GAME_TIMER() - iLastSightCheck) > 250
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			INT iTemp
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID(), SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT)
					AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedSecurityGuy[iTemp], PLAYER_PED_ID())
						bPlayerIsVisibleToGuard[iTemp] = TRUE
					ELSE
						bPlayerIsVisibleToGuard[iTemp] = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	RETURN bPlayerIsVisibleToGuard[0] OR bPlayerIsVisibleToGuard[1]
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_CLOSEST_GUARD()
	FLOAT fDist = 999999
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
			fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[0]))
		ENDIF
		IF NOT IS_PED_INJURED(pedSecurityGuy[1])
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[1])) < fDist
				fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[1]))
			ENDIF
		ENDIF
	ENDIF
	RETURN sqrt(fDist)
ENDFUNC

FUNC PED_INDEX GET_CLOSEST_SECURITY_GUARD()
	FLOAT fDist = 999999
	INT iIndex = 0
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
			fDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[0]))
			iIndex = 0
		ENDIF
		IF NOT IS_PED_INJURED(pedSecurityGuy[1])
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[1])) < fDist
				fDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[1]))
				iIndex = 1
			ENDIF
		ENDIF
	ENDIF
	RETURN pedSecurityGuy[iIndex]
ENDFUNC

FUNC BOOL IS_PLAYER_ATTEMPTING_TO_ENTER_THE_SECURITY_VAN()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehSecurityVan
			IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_NO_LONGER_A_THREAT()
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehSecurityVan, <<20, 20, 5>>)
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_TOUCHED_VAN_WITH_VEHICLE()
	
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		VEHICLE_INDEX vehTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
				vehTemp = GET_PLAYERS_LAST_VEHICLE()
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehTemp) AND DOES_ENTITY_EXIST(vehSecurityVan)
			RETURN IS_VEHICLE_DRIVEABLE(vehTemp) AND IS_ENTITY_TOUCHING_ENTITY(vehTemp, vehSecurityVan) AND GET_ENTITY_SPEED(vehTemp) > 10
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_WARRANTED_ARREST()

	BOOL bRet = FALSE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		INT iTemp
		REPEAT COUNT_OF(pedSecurityGuy) iTemp
			IF DOES_ENTITY_EXIST(pedSecurityGuy[iTemp])
				IF (NOT IS_PED_INJURED(pedSecurityGuy[iTemp]) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID()))
				OR IS_PED_INJURED(pedSecurityGuy[iTemp])
				OR IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
					bRet = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		AND NOT IS_PED_INJURED(pedSecurityGuy[1])
		AND bPlayerHasDamagedVan
			bRet = TRUE
		ENDIF
		
//		IF IS_PLAYER_NEARBY_AND_SHOOTING()
//			bRet = TRUE
//		ENDIF
		
	ENDIF
	
	RETURN bRet
	
ENDFUNC

VECTOR vLoadCaseScenePosition, vLoadCaseSceneRotation, vLoadCaseStartPos
INT iLoadCaseScene = -1
INT iIdleScene = -1
BOOL bIsloading
BOOL bDriverTasked

FUNC BOOL SHOULD_PEDS_GO_TO_SHOOT_OUT()

	INT iTemp
	REPEAT COUNT_OF(pedSecurityGuy) iTemp
		IF (NOT IS_PED_INJURED(pedSecurityGuy[iTemp]) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID()))
		OR (NOT IS_PED_INJURED(pedSecurityGuy[iTemp]) AND IS_PED_IN_COMBAT(pedSecurityGuy[iTemp], PLAYER_PED_ID()))
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID())
		OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehSecurityVan, PLAYER_PED_ID()) AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER())
		OR IS_PED_INJURED(pedSecurityGuy[iTemp])
		OR IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	IF IS_PLAYER_PLANTING_BOMB_NEAR_VAN()
	OR IS_PLAYER_NEARBY_AND_SHOOTING()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		REPEAT COUNT_OF(pedSecurityGuy) iTemp
			IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
				IF IS_PED_JACKING(PLAYER_PED_ID())	
				AND IS_PED_BEING_JACKED(pedSecurityGuy[iTemp])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan, TRUE)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateSecurityVanPos, <<25,25,20>>, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (IS_VEHICLE_DRIVEABLE(vehSecurityVan) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(vehSecurityVan), 25))
	OR (NOT IS_PED_INJURED(pedSecurityGuy[0]) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(pedSecurityGuy[0]), 20))
	OR (NOT IS_PED_INJURED(pedSecurityGuy[1]) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(pedSecurityGuy[1]), 20))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PEDS_GO_TO_HOSTILE_RESPONSE()

	INT iTemp
	REPEAT COUNT_OF(pedSecurityGuy) iTemp
		IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
		AND IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurityGuy[iTemp]) OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()))
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[iTemp]))<3*3
		AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE)
		AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
			RETURN TRUE
		ENDIF
		IF IS_PED_RAGDOLL(pedSecurityGuy[iTemp])
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	IF IS_PLAYER_CLIMBING_ON_BONNET()
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_NEARBY_AND_ARMED()OR IS_PLAYER_ATTEMPTING_TO_ENTER_THE_SECURITY_VAN())
	AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
		IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
		OR NOT IS_PED_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


PROC LOAD_CASE() //initial function will only run until something bad happens

	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(objIdCase)
			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objIdCase), 0.4, 128, 25, 100, 125)
		ENDIF
	#ENDIF
	
	IF NOT IS_PED_INJURED(pedSecurityGuy[0])
	AND NOT IS_PED_INJURED(pedSecurityGuy[1])
	AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	
		SWITCH iStageProgress
		
			CASE 0
				vLoadCaseScenePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 0)
				vLoadCaseSceneRotation = GET_ENTITY_ROTATION(vehSecurityVan)
				vLoadCaseStartPos = GET_ANIM_INITIAL_OFFSET_POSITION("random@security_van", "sec_case_into_van_calm", vLoadCaseScenePosition, vLoadCaseSceneRotation)
				bIsloading = FALSE
				bDriverTasked = FALSE
				SET_VEHICLE_ON_GROUND_PROPERLY(vehSecurityVan)
				
				OPEN_SEQUENCE_TASK(Seq)
					TASK_STAND_STILL(NULL, 5000)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLoadCaseStartPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vLoadCaseSceneRotation.z)
					TASK_ACHIEVE_HEADING(NULL, vLoadCaseSceneRotation.z)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedSecurityGuy[1], Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				//TASK_PLAY_ANIM_ADVANCED(pedSecurityGuy[0], "random@security_van", "driver_idle", vLoadCaseScenePosition, vLoadCaseSceneRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET , 0)
				iStageProgress++
			BREAK
			
			CASE 1
				vLoadCaseScenePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 0)
				vLoadCaseSceneRotation = GET_ENTITY_ROTATION(vehSecurityVan)
				vLoadCaseStartPos = GET_ANIM_INITIAL_OFFSET_POSITION("random@security_van", "sec_case_into_van_calm", vLoadCaseScenePosition, vLoadCaseSceneRotation)
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iIdleScene)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCreateSecurityVanPos) < 150*150
					AND HAS_COLLISION_LOADED_AROUND_ENTITY(vehSecurityVan)
						iIdleScene = CREATE_SYNCHRONIZED_SCENE(vLoadCaseScenePosition, vLoadCaseSceneRotation)
						TASK_SYNCHRONIZED_SCENE(pedSecurityGuy[0], iIdleScene, "random@security_van", "driver_idle", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_DONT_INTERRUPT)
						SET_SYNCHRONIZED_SCENE_LOOPED(iIdleScene, TRUE)
					ENDIF
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(pedSecurityGuy[1], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					OPEN_SEQUENCE_TASK(Seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLoadCaseStartPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vLoadCaseSceneRotation.z)
						TASK_ACHIEVE_HEADING(NULL, vLoadCaseSceneRotation.z)
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(pedSecurityGuy[1], Seq)
					CLEAR_SEQUENCE_TASK(Seq)
				ENDIF
				
				FLOAT fHead
				fHead = GET_ENTITY_HEADING(pedSecurityGuy[1])
				
				IF fHead > 180 fHead -= 360 ENDIF
				IF fHead <= -180 fHead += 360 ENDIF
				
				IF vLoadCaseSceneRotation.z > 180 vLoadCaseSceneRotation.z -= 360 ENDIF
				IF vLoadCaseSceneRotation.z <= -180 vLoadCaseSceneRotation.z += 360 ENDIF
				
				IF ABSF(fHead - vLoadCaseSceneRotation.z) < 15
				AND VDIST(vLoadCaseStartPos, GET_ENTITY_COORDS(pedSecurityGuy[1])) < 0.2
					CLEAR_PED_ALTERNATE_WALK_ANIM(pedSecurityGuy[1])
					CLEAR_PED_TASKS(pedSecurityGuy[1])
					bIsloading = TRUE
					iLoadCaseScene = CREATE_SYNCHRONIZED_SCENE(vLoadCaseScenePosition, vLoadCaseSceneRotation)
					TASK_SYNCHRONIZED_SCENE(pedSecurityGuy[1], iLoadCaseScene, "random@security_van", "sec_case_into_van_calm", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, RBF_NONE, SLOW_BLEND_IN)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, iLoadCaseScene, "van_case_into_van_calm", "random@security_van", SLOW_BLEND_IN, INSTANT_BLEND_OUT, 0, INSTANT_BLEND_OUT)//ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecurityGuy[1])
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 2
				IF NOT bDoneDialogue
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > 0.09
					AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) < 0.11
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[1], <<20, 20, 5>>)
							IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CHT1", CONV_PRIORITY_AMBIENT_MEDIUM)
								bDoneDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
					IF NOT bDepositedBag
						IF DOES_ENTITY_EXIST(objIdCase)
						AND IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[1])
						AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > 0.3793
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase,vehSecurityVan)
								DETACH_ENTITY(objIdCase)
								SET_ENTITY_COLLISION(objIdCase, FALSE)
								ATTACH_ENTITY_TO_ENTITY(objIdCase, vehSecurityVan, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehSecurityVan, GET_ENTITY_COORDS(objIdCase)), <<90,0,0>>)
							ENDIF
							bDepositedBag = TRUE
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > 0.546243
							STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, SLOW_BLEND_OUT, TRUE)
							IF DOES_ENTITY_EXIST(objIdCase)
								bDoneDialogue = FALSE
								PRINTLN("\n\n\n\n SHUTTING DOORS 1.0 \n\n\n\n\n\n")
								SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_LEFT)
								SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_RIGHT)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
			
				IF NOT bDoneDialogue
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > 0.56
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedSecurityGuy[1], <<20, 20, 5>>)	
						IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CHT2", CONV_PRIORITY_AMBIENT_MEDIUM)
							bDoneDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bDriverTasked
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > 0.6)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iIdleScene)
							CLEAR_PED_TASKS(pedSecurityGuy[0])
							iIdleScene = CREATE_SYNCHRONIZED_SCENE(vLoadCaseScenePosition, vLoadCaseSceneRotation)
							TASK_SYNCHRONIZED_SCENE(pedSecurityGuy[0], iIdleScene, "random@security_van", "driver_exit_calm", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
						ENDIF
						bDriverTasked = TRUE
					ENDIF
				ELSE
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(iIdleScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iIdleScene) >= 0.92)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iIdleScene)
						IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
						AND GET_SCRIPT_TASK_STATUS(pedSecurityGuy[0], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
							CLEAR_PED_TASKS(pedSecurityGuy[0])
							TASK_ENTER_VEHICLE(pedSecurityGuy[0], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
				AND HAS_ANIM_EVENT_FIRED(pedSecurityGuy[1], GET_HASH_KEY("ENDS_IN_WALK"))
					CLEAR_PED_TASKS(pedSecurityGuy[1])
					TASK_ENTER_VEHICLE(pedSecurityGuy[1], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
					SETTIMERA(0)
				ENDIF
				
				IF ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN()
				AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_LEFT) = 0.0
				AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_RIGHT) = 0.0
					SET_STAGE(SV_DRIVE_OFF)
				ENDIF
				
			BREAK
			
			CASE 4
				IF TIMERA() > 15000
					PRINTLN("\n\n\n\n SHUTTING DOORS 1 \n\n\n\n\n\n")
					SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_FRONT_LEFT, TRUE)
					SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_FRONT_RIGHT, TRUE)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(GET_CLOSEST_SECURITY_GUARD())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_CLOSEST_SECURITY_GUARD())) < 30
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 0
			SET_STAGE(SV_SHOOT_OUT) println("\n\n\n\nHR 6 \n\n\n")
		ENDIF
		
	ENDIF
	
	//check all cases that will change the current stage
	IF NOT IS_PED_INJURED(pedSecurityGuy[1])
		IF DOES_PICKUP_EXIST(pickupCase)
			SET_STAGE(SV_RECOVER_CASE)
		ENDIF
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(VDIST(vLoadCaseStartPos, GET_ENTITY_COORDS(pedSecurityGuy[1]))), <<0.02,0.02,0>>)
			DRAW_DEBUG_LINE(vLoadCaseStartPos, GET_ENTITY_COORDS(pedSecurityGuy[1]))
		#ENDIF
	ENDIF
	
	//perform all checks to see if the player has interrupted loading
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF SHOULD_PEDS_GO_TO_SHOOT_OUT()
		
			IF ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN()
				SET_STAGE(SV_FLEE_IN_VAN)
			ELSE
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
			
		ELIF SHOULD_PEDS_GO_TO_HOSTILE_RESPONSE()
		
			SET_STAGE(SV_HOSTILE_RESPONSE)
			
		ELSE
			
			IF HAS_PLAYER_PARKED_TOO_CLOSE()
				SET_STAGE(SV_HOSTILE_RESPONSE_TO_CAR)
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF thisStage <> SV_LOAD_CASE
		IF thisStage <>  SV_DRIVE_OFF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				IF IS_ENTITY_PLAYING_ANIM(pedSecurityGuy[0], "random@security_van", "driver_idle")
					IF eWhichWorldPoint <> SV_VAR_CLUCKING_BELL
						TASK_PLAY_ANIM_ADVANCED(pedSecurityGuy[0], "random@security_van", "driver_exit_panic", vLoadCaseScenePosition, vLoadCaseSceneRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_EXTRACT_INITIAL_OFFSET, 0)
					ELSE
						CLEAR_PED_TASKS(pedSecurityGuy[0])
						TASK_PLAY_ANIM(pedSecurityGuy[0], "random@security_van", "driver_exit_panic", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					ENDIF
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
			AND NOT IS_PED_INJURED(pedSecurityGuy[1])
				println("\n\n\n\n\n\n\n\n\nBOOM\n\n\n\n\n\n\n\n\n\n\n")
				STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, INSTANT_BLEND_OUT, TRUE)
				CLEAR_PED_TASKS(pedSecurityGuy[1])
			ENDIF
		ENDIF
		IF bDepositedBag
		AND NOT DOES_PICKUP_EXIST(pickupCase)
			PRINTLN("\n\n\n\n SHUTTING DOORS 2 \n\n\n\n\n\n")
			SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_RIGHT)
		ENDIF
	ENDIF
	
ENDPROC

FLOAT fDistLastCheck
INT iTimeLastCheck

FUNC BOOL IS_PLAYER_BACKING_OFF(ENTITY_INDEX targetEntity, INT iTimeBetweenChecks = 1000)
	BOOL bBackingOff = FALSE
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(targetEntity)
			IF (IS_ENTITY_A_PED(targetEntity) AND NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(targetEntity)))
			OR (IS_ENTITY_A_VEHICLE(targetEntity) AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(targetEntity)))
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(targetEntity)) > fDistLastCheck
					bBackingOff = TRUE		
				ENDIF
				IF (GET_GAME_TIMER() - iTimeLastCheck) > iTimeBetweenChecks
					fDistLastCheck = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(targetEntity))
					iTimeLastCheck = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bBackingOff
ENDFUNC

INT iTimeOfLastWarnLine
VECTOR vLastHostilePosition

PROC WRAP_ANGLE(FLOAT &Angle)
	IF Angle > 180
		Angle -= 360
	ELIF Angle < -180
		Angle += 360
	ENDIF
ENDPROC

INT iTimeOfLastAimTask[2]

PROC GO_TO_AIMING_POSITION(INT iGuard = -1, BOOL bWeapons = TRUE)

	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		VECTOR vPlayerOffset = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehSecurityVan, GET_ENTITY_COORDS(PLAYER_PED_ID())))
		FLOAT head0 = GET_ENTITY_HEADING(vehSecurityVan) + GET_HEADING_FROM_VECTOR_2D(vPlayerOffset.x, vPlayerOffset.y) + 45
		FLOAT head1 = GET_ENTITY_HEADING(vehSecurityVan) + GET_HEADING_FROM_VECTOR_2D(vPlayerOffset.x, vPlayerOffset.y) - 45
		
		VECTOR pos[2]
		pos[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehSecurityVan), head0, <<0,4.5,0>>)
		pos[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehSecurityVan), head1, <<0,4.5,0>>)
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_SPHERE(pos[0], 0.1)
			DRAW_DEBUG_SPHERE(pos[1], 0.1)		
		#ENDIF
		
		WRAP_ANGLE(head0)
		WRAP_ANGLE(head1)
		
		INT iTemp
		
		BOOL bRelocate
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLastHostilePosition) > 2
			bRelocate = TRUE
		ENDIF
		
		REPEAT COUNT_OF(pedSecurityGuy) iTemp
			IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
				IF VDIST(GET_ENTITY_COORDS(pedSecurityGuy[iTemp]), pos[iTemp]) > 2
					bRelocate = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bRelocate
			SEQUENCE_INDEX seqTemp 
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND VDIST(GET_ENTITY_COORDS(pedSecurityGuy[iTemp]), pos[iTemp]) > 2
				AND (GET_GAME_TIMER() - iTimeOfLastAimTask[iTemp]) > 1000
				AND NOT IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iTemp])
				AND NOT bPlayerIsVisibleToGuard[iTemp]
				AND (iGuard = -1 OR iGuard = iTemp)
				
					IF bWeapons
					
						IF CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
							vLastHostilePosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
						ENDIF
						OPEN_SEQUENCE_TASK(seqTemp)
						IF CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
						OR IS_VECTOR_ZERO(vLastHostilePosition)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, pos[iTemp], PLAYER_PED_ID(), PEDMOVEBLENDRATIO_RUN, FALSE)
							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, TRUE)
						ELSE
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, pos[iTemp], vLastHostilePosition, PEDMOVEBLENDRATIO_RUN, FALSE)
							TASK_AIM_GUN_AT_COORD(NULL, vLastHostilePosition, -1, TRUE)
						ENDIF
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(pedSecurityGuy[iTemp], seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
						
					ELSE
					
//						VECTOR vPosToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vPos[iTemp]
//						FLOAT fHeadingToPlayer = GET_HEADING_FROM_VECTOR_2D(vPosToPlayer.x, vPosToPlayer.y)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(pedSecurityGuy[iTemp], pos[iTemp], PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fHeadingToPlayer)
//						
					ENDIF
					
					iTimeOfLastAimTask[iTemp] = GET_GAME_TIMER()
					
				ENDIF
			ENDREPEAT
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL HAS_GUARD_PUT_BAG_AWAY(INT iSecIndex)
	IF DOES_ENTITY_EXIST(objIdCase)
		IF NOT IS_PED_INJURED(pedSecurityGuy[iSecIndex])
			IF IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[iSecIndex])
				println("\n case is attached to holder")
				IF IS_ENTITY_PLAYING_ANIM(pedSecurityGuy[iSecIndex], "weapons@holster_1h", "holster")
					println("\n anim is playing at phase ", GET_ENTITY_ANIM_CURRENT_TIME(pedSecurityGuy[iSecIndex], "weapons@holster_1h", "holster"))
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedSecurityGuy[iSecIndex], "weapons@holster_1h", "holster") > 0.9
						println("\n anim over 0.9 in phase")
						DETACH_ENTITY(objIdCase)
						DELETE_OBJECT(objIdCase)PRINTLN("Deleting case object 3")
					ENDIF
				ELSE
					println("started anim")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[iSecIndex], TRUE)
					TASK_PLAY_ANIM(pedSecurityGuy[iSecIndex], "weapons@holster_1h", "holster")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	BOOL bReturn = NOT DOES_ENTITY_EXIST(objIdCase) OR (DOES_ENTITY_EXIST(objIdCase) AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[iSecIndex]))
	IF bReturn
		IF NOT IS_PED_INJURED(pedSecurityGuy[iSecIndex])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[iSecIndex], FALSE)
		ENDIF
	ENDIF
	RETURN bReturn
ENDFUNC

PROC HOSTILE_RESPONSE()

	SWITCH iStageProgress
	
		CASE 0
			IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[0], TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedSecurityGuy[1])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[1], TRUE)
			ENDIF
			
			IF IS_PLAYER_NEARBY_AND_SHOOTING()
				IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_ATT1", CONV_PRIORITY_HIGH)
					SETTIMERA(0)
					SET_STAGE(SV_SHOOT_OUT)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				AND NOT IS_PED_INJURED(pedSecurityGuy[1])
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_ATTEMPTING_TO_ENTER_THE_SECURITY_VAN()
					OR IS_PLAYER_CLIMBING_ON_BONNET()
						CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_ENT1", CONV_PRIORITY_HIGH)
					ELIF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
						CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_WEP1", CONV_PRIORITY_HIGH)
					ELSE
						CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_WARN", CONV_PRIORITY_HIGH)
					ENDIF
					SETTIMERA(0)
					IF DOES_ENTITY_EXIST(objIdCase)
					AND IS_ENTITY_ATTACHED(objIdCase)
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[0])
							aimAtPlayer(0)
						ELSE
							CLEAR_PED_TASKS(pedSecurityGuy[0])
						ENDIF
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[1])
							aimAtPlayer(1)
						ELSE
							CLEAR_PED_TASKS(pedSecurityGuy[1])
						ENDIF
						iStageProgress++
					ELSE
						println("\ncase does not exist")
						aimAtPlayer(0)
						aimAtPlayer(1)
						iStageProgress=2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_GUARD_PUT_BAG_AWAY(iCurrentCaseHolder)
				IF NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
					aimAtPlayer(iCurrentCaseHolder)
				ENDIF
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 2
		
			IF NOT IS_PLAYER_NO_LONGER_A_THREAT()
			
				BOOL bGoToShooting
				IF NOT IS_PLAYER_BACKING_OFF(vehSecurityVan)
				AND TIMERA() > 10000
					PRINTLN("*7* go to shooting 1")
					bGoToShooting = TRUE
				ELSE
					IF TIMERA() > 7000
						SETTIMERA(7000)
					ENDIF
				ENDIF
				
				IF TIMERA() > 5000
					IF IS_PLAYER_ATTEMPTING_TO_ENTER_THE_SECURITY_VAN()
						PRINTLN("*7* go to shooting 2")
						bGoToShooting = TRUE
					ENDIF
				ENDIF
				
				IF TIMERA() > 10000
					IF IS_PLAYER_CLIMBING_ON_BONNET()
						PRINTLN("*7a* go to shooting 2")
						bGoToShooting = TRUE
					ENDIF
				ENDIF
				
				INT iTemp
				REPEAT COUNT_OF(pedSecurityGuy) iTemp
					IF NOT IS_PLAYER_BACKING_OFF(pedSecurityGuy[iTemp])
					AND (GET_DISTANCE_TO_CLOSEST_GUARD() < 2 AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE))
						PRINTLN("*7* go to shooting 3")
						bGoToShooting = TRUE
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurityGuy[iTemp])
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurityGuy[iTemp])
							PRINTLN("*7* go to shooting 4")
							bGoToShooting = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				GO_TO_AIMING_POSITION()
				
				IF(GET_GAME_TIMER() - iTimeOfLastWarnLine) > GET_RANDOM_FLOAT_IN_RANGE(5000, 7500)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_WARN", CONV_PRIORITY_HIGH)
							iTimeOfLastWarnLine = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
				
				IF bGoToShooting
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					PRINTLN("*8* Goint to shootout 1")
					SET_STAGE(SV_SHOOT_OUT)
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	INT iTemp
	REPEAT COUNT_OF(pedSecurityGuy) iTemp
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(pedSecurityGuy[iTemp])
			IF (IS_PED_INJURED(pedSecurityGuy[iTemp]) OR IS_ENTITY_DEAD(pedSecurityGuy[iTemp]))
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID(), FALSE)
				PRINTLN("*8* Goint to shootout 2")
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_PED_INJURED(pedSecurityGuy[0])
	OR IS_PED_INJURED(pedSecurityGuy[1])
	OR bCaseCollected
		PRINTLN("*8* Goint to shootout 3")
		SET_STAGE(SV_SHOOT_OUT)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehSecurityVan, PLAYER_PED_ID())
			PRINTLN("*8* Goint to shootout 4")
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		SET_STAGE(SV_FLEE_ON_FOOT)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN| WF_INCLUDE_PROJECTILE)
	AND IS_PED_SHOOTING(PLAYER_PED_ID())
		PRINTLN("*8* Goint to shootout 5")
		SET_STAGE(SV_SHOOT_OUT)
	ENDIF
	
	IF GET_DISTANCE_TO_CLOSEST_GUARD() > 20
	OR (NOT bPlayerIsVisibleToGuard[0] AND NOT bPlayerIsVisibleToGuard[1] AND GET_DISTANCE_TO_CLOSEST_GUARD() > 5)
		SET_STAGE(SV_RECOVER_CASE)
	ENDIF
	
ENDPROC

PROC HOSTILE_RESPONSE_TO_VEHICLE()

	IF NOT IS_PED_INJURED(pedSecurityGuy[0])
	AND NOT IS_PED_INJURED(pedSecurityGuy[1])
	
		SWITCH iStageProgress
		
			CASE 0
				IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CAR1", CONV_PRIORITY_HIGH)
					IF DOES_ENTITY_EXIST(objIdCase)
					AND IS_ENTITY_ATTACHED(objIdCase)
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[0])
							aimAtPlayer(0)
						ELSE
							CLEAR_PED_TASKS(pedSecurityGuy[0])
						ENDIF
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[1])
							aimAtPlayer(1)
						ELSE
							CLEAR_PED_TASKS(pedSecurityGuy[1])
						ENDIF
						iStageProgress++
					ELSE
						aimAtPlayer(0)
						aimAtPlayer(1)
						iStageProgress=2
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF HAS_GUARD_PUT_BAG_AWAY(iCurrentCaseHolder)
					IF NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
						aimAtPlayer(iCurrentCaseHolder)
					ENDIF
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CAR2", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF TIMERA() > 12000
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CAR3", CONV_PRIORITY_HIGH)
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 4
				IF TIMERA() > 7000
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_CAR4", CONV_PRIORITY_HIGH)
					SETTIMERA(0)
					SET_STAGE(SV_SHOOT_OUT)
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	IF iStageProgress > 0
		GO_TO_AIMING_POSITION()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehOffendingVehicle)
	
		IF NOT IS_ENTITY_DEAD(vehOffendingVehicle)
		AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(vehOffendingVehicle), GET_ENTITY_COORDS(vehSecurityVan))), <<0.5,0.5,0>>)
			#ENDIF
			IF VDIST(GET_ENTITY_COORDS(vehOffendingVehicle), GET_ENTITY_COORDS(vehSecurityVan)) > 10
				PRINTLN("Going to recover case")
				SET_STAGE(SV_RECOVER_CASE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(vehOffendingVehicle)
		OR NOT IS_VEHICLE_DRIVEABLE(vehOffendingVehicle)
		OR (NOT IS_ENTITY_UPRIGHT(vehOffendingVehicle) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehOffendingVehicle, TRUE))
			IF NOT IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(vehOffendingVehicle)
				SET_STAGE(SV_RECOVER_CASE)
			ELSE
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
		ENDIF
		
	ENDIF
	
	//check all cases that will cahnge the current stage
	IF NOT IS_PED_INJURED(pedSecurityGuy[0])
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[0], PLAYER_PED_ID())
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedSecurityGuy[1])
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[1], PLAYER_PED_ID())
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF bPlayerHasDamagedVan
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
	ENDIF
	
	IF bCaseCollected
		SET_STAGE(SV_SHOOT_OUT)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
	AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN| WF_INCLUDE_PROJECTILE |WF_INCLUDE_MELEE)
		SET_STAGE(SV_HOSTILE_RESPONSE) println("\n\n\n\nHR 1 \n\n\n")
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		SET_STAGE(SV_FLEE_ON_FOOT)
	ENDIF
	
ENDPROC

INT iTimeShootingStarted = -1

PROC ATTACK_PLAYER()
	INT iTemp
	
	IF iTimeShootingStarted = -1
		iTimeShootingStarted = GET_GAME_TIMER()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH iStageProgress 
			CASE 0
				bHostile = TRUE
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				OR NOT IS_PED_INJURED(pedSecurityGuy[1])
					IF GET_DISTANCE_TO_CLOSEST_GUARD() < 30
					AND NOT IS_PED_INJURED(sSpeech.PedInfo[3].Index)
						CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_SHT1", CONV_PRIORITY_HIGH)
					ENDIF
				ENDIF
				REPEAT COUNT_OF(pedSecurityGuy) iTemp
					IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					AND DOES_ENTITY_EXIST(objIdCase)
						IF iTemp <> iCurrentCaseHolder
						OR bDepositedBag
						OR NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[iTemp])
							CLEAR_PED_TASKS(pedSecurityGuy[iTemp])
							OPEN_SEQUENCE_TASK(Seq)
								IF IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iTemp])
									IF bDoorWasBlown
										IF HAS_CLIP_SET_LOADED("move_injured_generic")
											SET_PED_MOVEMENT_CLIPSET(pedSecurityGuy[iTemp], "move_injured_generic")
											bMovementSetToInjured[iTemp] = TRUE
											PRINTLN("SETTING PED ", iTemp, " MOVEMENT SET TO INJURED")
										ENDIF
									ENDIF
									TASK_LEAVE_ANY_VEHICLE(NULL, 0,ECF_DONT_CLOSE_DOOR)
								ENDIF
								TASK_CLEAR_LOOK_AT(NULL)
								IF bMovementSetToInjured[iTemp]
									TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1000, 3000))
								ENDIF
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedSecurityGuy[iTemp], Seq)
							CLEAR_SEQUENCE_TASK(Seq)
						ENDIF
						SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[iTemp], CA_FLEE_WHILST_IN_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[iTemp], CA_USE_COVER, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[iTemp], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_MOVEMENT(pedSecurityGuy[iTemp], CM_DEFENSIVE)
						SET_PED_TARGET_LOSS_RESPONSE(pedSecurityGuy[iTemp], TLR_NEVER_LOSE_TARGET)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[iTemp], TRUE)
					ENDIF
				ENDREPEAT
				IF DOES_ENTITY_EXIST(objIdCase)
				AND IS_ENTITY_ATTACHED(objIdCase)
				AND NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
					CLEAR_PED_TASKS(pedSecurityGuy[iCurrentCaseHolder])
				ENDIF
				SETTIMERA(0)
				iStageProgress++
			BREAK
			CASE 1
				IF HAS_GUARD_PUT_BAG_AWAY(iCurrentCaseHolder)
					IF NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
						CLEAR_PED_TASKS(pedSecurityGuy[iCurrentCaseHolder])
						OPEN_SEQUENCE_TASK(Seq)
							IF IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iCurrentCaseHolder])
								TASK_LEAVE_ANY_VEHICLE(NULL, 0,ECF_DONT_CLOSE_DOOR)
							ENDIF
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(Seq)
						TASK_PERFORM_SEQUENCE(pedSecurityGuy[iCurrentCaseHolder], Seq)
						CLEAR_SEQUENCE_TASK(Seq)
					ENDIF
					iStageProgress++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				OR NOT IS_PED_INJURED(pedSecurityGuy[1])
					IF IS_PED_INJURED(pedSecurityGuy[0])
					AND NOT IS_PED_INJURED(pedSecurityGuy[1])
						IF sSpeech.PedInfo[3].Index <> pedSecurityGuy[1]
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
							ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedSecurityGuy[1], "SECVANGUY1")
						ENDIF
					ENDIF
					REPEAT COUNT_OF(pedSecurityGuy) iTemp
						IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
						
							IF eVanStatus = VANSTATUS_CASE_LOADED
							AND NOT bDefensiveAreaSet[iTemp]
							AND NOT IS_ENTITY_DEAD(vehSecurityVan)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(pedSecurityGuy[iTemp], vehSecurityVan, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, -4, 0>>), 5)
								bDefensiveAreaSet[iTemp] = TRUE
							ENDIF
							
							IF DOES_PICKUP_EXIST(pickupCase)
							AND NOT bDefensiveAreaSet[iTemp]
							AND eVanStatus = VANSTATUS_DOORS_BLOWN
							AND NOT bCaseCollected
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(pedSecurityGuy[iTemp], vehSecurityVan, GET_PICKUP_COORDS(pickupCase), 5)
								bDefensiveAreaSet[iTemp] = TRUE
							ENDIF
							
							IF bCaseCollected
								IF bDefensiveAreaSet[iTemp]
									REMOVE_PED_DEFENSIVE_AREA(pedSecurityGuy[iTemp])
									bDefensiveAreaSet[iTemp] =  FALSE
								ENDIF
							ENDIF
							
						ENDIF
					ENDREPEAT
					IF(GET_GAME_TIMER() - iTimeOfLastWarnLine) > GET_RANDOM_FLOAT_IN_RANGE(10000, 15000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND GET_DISTANCE_TO_CLOSEST_GUARD() < 30
							IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
								IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_SHT2", CONV_PRIORITY_HIGH)
									iTimeOfLastWarnLine = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
						IF NOT bDepositedBag
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan)
							AND GET_GAME_TIMER() - iTimeShootingStarted > 5000
								IF GET_DISTANCE_TO_CLOSEST_GUARD() > (SHOOT_OUT_MIN_DISTANCE + (SHOOT_OUT_MIN_DISTANCE/10))
									SET_STAGE(SV_RECOVER_CASE)
								ENDIF
								IF GET_DISTANCE_TO_CLOSEST_GUARD() > SHOOT_OUT_MIN_DISTANCE/2
								AND NOT CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER()
									IF NOT bDepositedBag
										SET_STAGE(SV_RECOVER_CASE)
									ELSE
										SET_STAGE(SV_FLEE_IN_VAN)
									ENDIF
								ENDIF
								IF thisStage != SV_SHOOT_OUT	
									bDefensiveAreaSet[0] = FALSE
									bDefensiveAreaSet[1] = FALSE
									iTimeShootingStarted = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		REPEAT COUNT_OF(pedSecurityGuy) iTemp
			IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
				
				INT iDrunkTime
				IF iTemp = 0
					iDrunkTime = GUARD_ONE_DRINK_TIME
				ELSE
					iDrunkTime = GUARD_TWO_DRINK_TIME
				ENDIF
				
				IF TIMERA() > iDrunkTime 
				AND bMovementSetToInjured[iTemp]
					RESET_PED_MOVEMENT_CLIPSET(pedSecurityGuy[iTemp])
					bMovementSetToInjured[iTemp] = FALSE
				ENDIF
				
//				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSecurityGuy[iTemp])) > 100
//				AND IS_ENTITY_OCCLUDED(pedSecurityGuy[iTemp])
//					IF DOES_BLIP_EXIST(blipSecurityGuy[iTemp])
//						REMOVE_BLIP(blipSecurityGuy[iTemp])
//					ENDIF
//					DELETE_PED(pedSecurityGuy[iTemp])
//				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FLOAT fDetachTime
FLOAT fClosedTime

PROC RECOVER_CASE(PED_INDEX ped)

	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	
		SWITCH iStageProgress
		
			CASE 0
			
				IF NOT bDepositedBag
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
					CLEAR_PED_TASKS(ped)
					vLoadCaseScenePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(vehSecurityVan, 0)
					vLoadCaseSceneRotation = GET_ENTITY_ROTATION(vehSecurityVan)
					vLoadCaseStartPos = GET_ANIM_INITIAL_OFFSET_POSITION("random@security_van", "sec_case_into_van_panic", vLoadCaseScenePosition, vLoadCaseSceneRotation)
					IF NOT IS_PED_INJURED(pedSecurityGuy[1]) AND NOT IS_ENTITY_DEAD(pedSecurityGuy[1])
					AND NOT IS_PED_INJURED(pedSecurityGuy[0]) AND NOT IS_ENTITY_DEAD(pedSecurityGuy[0])
						IF GET_DISTANCE_TO_CLOSEST_GUARD() < 20
							IF bHostile
								CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_REC", CONV_PRIORITY_HIGH)
							ENDIF
						ENDIF
						IF DOES_PICKUP_EXIST(pickupCase)
							IF bHostile
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped, GET_PICKUP_COORDS(pickupCase), PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
							ELSE
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped, GET_PICKUP_COORDS(pickupCase), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							ENDIF
							iStageProgress++
						ELSE
							SET_CASE_INTO_PED_HAND(ped)
							iStageProgress = 2
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(ped)
							IF DOES_PICKUP_EXIST(pickupCase)
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped, GET_PICKUP_COORDS(pickupCase), PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
								iStageProgress++
							ELSE
								SET_CASE_INTO_PED_HAND(ped)
								iStageProgress=2
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iStageProgress = 4
				ENDIF
				
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(ped)
				AND DOES_PICKUP_EXIST(pickupCase)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ped), GET_PICKUP_COORDS(pickupCase)) < 2
					AND NOT IS_PED_RAGDOLL(ped)
					AND GET_GAME_TIMER() - iDropTIme > 3000
						IF IS_PED_ARMED(ped, WF_INCLUDE_GUN)
							SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_UNARMED, TRUE)
						ENDIF
						REMOVE_PICKUP(pickupCase)
						SET_CASE_INTO_PED_HAND(ped)
						SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_UNARMED, TRUE)
						PRINTLN("CASE PICKED UP")
						bPickupAvailable = FALSE
						iCurrentCaseHolder = iCurrentCaseRecoverer
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_PED_INJURED(ped)
				AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
					IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
						IF bHostile
							TASK_FOLLOW_NAV_MESH_TO_COORD(ped, vLoadCaseStartPos, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vLoadCaseSceneRotation.z)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(ped, vLoadCaseStartPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vLoadCaseSceneRotation.z)
						ENDIF
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_PED_INJURED(ped)
					IF DOES_PICKUP_EXIST(pickupCase)
						iStageProgress = 0
					ELSE
						FLOAT fHead
						fHead = GET_ENTITY_HEADING(ped)
						
						IF fHead > 180 fHead -= 360 ENDIF
						IF fHead <= -180 fHead += 360 ENDIF
						
						IF vLoadCaseSceneRotation.z > 180 vLoadCaseSceneRotation.z -= 360 ENDIF
						IF vLoadCaseSceneRotation.z <= -180 vLoadCaseSceneRotation.z += 360 ENDIF
						
						IF VDIST(vLoadCaseStartPos, GET_ENTITY_COORDS(ped)) < 0.3
							IF ABSF(fHead - vLoadCaseSceneRotation.z) < 15
								CLEAR_PED_ALTERNATE_WALK_ANIM(ped)
								CLEAR_PED_TASKS(ped)
								bIsloading = TRUE
								iLoadCaseScene = CREATE_SYNCHRONIZED_SCENE(vLoadCaseScenePosition, vLoadCaseSceneRotation)
								IF bHostile
									TASK_SYNCHRONIZED_SCENE(ped, iLoadCaseScene, "random@security_van", "sec_case_into_van_panic", SLOW_BLEND_IN, SLOW_BLEND_IN, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, RBF_NONE, SLOW_BLEND_IN)
									PLAY_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, iLoadCaseScene, "van_case_into_van_panic", "random@security_van", SLOW_BLEND_IN, INSTANT_BLEND_OUT, 0, INSTANT_BLEND_OUT)//ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
								ELSE
									TASK_SYNCHRONIZED_SCENE(ped, iLoadCaseScene, "random@security_van", "sec_case_into_van_calm", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, RBF_NONE, SLOW_BLEND_IN)
									PLAY_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, iLoadCaseScene, "van_case_into_van_calm", "random@security_van", SLOW_BLEND_IN, INSTANT_BLEND_OUT, 0, INSTANT_BLEND_OUT)//ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
								ENDIF
								FORCE_PED_AI_AND_ANIMATION_UPDATE(ped)
								iStageProgress++
							ENDIF
						ELSE
							IF IS_ENTITY_OCCLUDED(ped)
							AND NOT IS_SPHERE_VISIBLE(vLoadCaseScenePosition, 2)
								SET_ENTITY_COORDS(ped, vLoadCaseScenePosition)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
				
					IF bHostile
						fDetachTime = 0.42
					ELSE
					 	fDetachTime = 0.3893
					ENDIF
					IF bHostile
						fClosedTime = 0.52
					ELSE
					 	fClosedTime = 0.546243
					ENDIF
					
					PRINTLN("Case into van anim phase ", GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene))
					
					IF NOT bDepositedBag
						PRINTLN("check 1")
						IF DOES_ENTITY_EXIST(objIdCase)
						AND IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, ped)
						AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > fDetachTime
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase,vehSecurityVan)
								DETACH_ENTITY(objIdCase)
								SET_ENTITY_COLLISION(objIdCase, FALSE)
								ATTACH_ENTITY_TO_ENTITY(objIdCase, vehSecurityVan, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehSecurityVan, GET_ENTITY_COORDS(objIdCase)), <<90,0,0>>)
								PRINTLN("Swapping bag attach")
							ENDIF
							PRINTLN("Bag deposited")
							bDepositedBag = TRUE
						ENDIF
					ELSE
						PRINTLN("check 2")
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > fClosedTime
							STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, SLOW_BLEND_OUT, TRUE)
							CLEAR_PED_TASKS(ped)
							IF DOES_ENTITY_EXIST(objIdCase)
								bDoneDialogue = FALSE
								SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_LEFT)
								SET_VEHICLE_DOOR_SHUT(vehSecurityVan, SC_DOOR_REAR_RIGHT)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
								PRINTLN("Setting doors shut")
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 5
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene) > fClosedTime)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
					IF NOT bHostile
						IF NOT IS_PED_INJURED(pedSecurityGuy[0])
							TASK_ENTER_VEHICLE(pedSecurityGuy[0], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
						ENDIF
					ENDIF
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 6
				IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
					IF bHostile
						SET_STAGE(SV_FLEE_IN_VAN)
						PRINTLN("HERE 3 \n\n\n\n")	
					ELSE
						SET_STAGE(SV_DRIVE_OFF)
						PRINTLN("HERE 4 \n\n\n\n")
					ENDIF
				ELSE
					SET_STAGE(SV_SHOOT_OUT)
					PRINTLN("HERE 5 \n\n\n\n")
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		IF NOT IS_PED_INJURED(pedSecurityGuy[0])
		OR NOT IS_PED_INJURED(pedSecurityGuy[1])
			PRINTLN("HERE 1 \n\n\n\n")
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
	ENDIF
	
	IF (NOT IS_PED_INJURED(pedSecurityGuy[0]) OR NOT IS_PED_INJURED(pedSecurityGuy[1]))
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT bDepositedBag
			IF (GET_DISTANCE_TO_CLOSEST_GUARD() < 10 AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE) AND CAN_EITHER_SECURITY_GUARDS_SEE_PLAYER())
			OR (GET_DISTANCE_TO_CLOSEST_GUARD() < 20 AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE) AND IS_PED_SHOOTING(PLAYER_PED_ID()))
			OR (GET_DISTANCE_TO_CLOSEST_GUARD() < 5) AND bHostile
			OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
			OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_VEHICLE_IN_THE_WAY_OF_VAN_DOORS(GET_PLAYERS_LAST_VEHICLE()))
				PRINTLN("HERE 2 \n\n\n\n")
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
		ENDIF
	ENDIF
	
	IF thisStage <> SV_RECOVER_CASE
		IF bDepositedBag
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
				STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, NORMAL_BLEND_OUT, TRUE)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(ped)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	AND thisStage <> SV_RECOVER_CASE
	AND IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
		STOP_SYNCHRONIZED_ENTITY_ANIM(vehSecurityVan, NORMAL_BLEND_OUT, TRUE)
	ENDIF
	
ENDPROC

PROC DRIVE_OFF()

	SWITCH iStageProgress
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				AND NOT IS_PED_INJURED(pedSecurityGuy[1])
					SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[0], CA_FLEE_WHILST_IN_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[1], CA_FLEE_WHILST_IN_VEHICLE, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[0], TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[1], TRUE)
					IF IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
					AND IS_PED_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan)
						IF eWhichWorldPoint = SV_VAR_CLUCKING_BELL
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehSecurityVan, <<-574.6195, -847.2320, 25.2925>>, 10, DRIVINGSTYLE_NORMAL, STOCKADE, DRIVINGMODE_STOPFORCARS | DF_StopForPeds, 2, 4)
							TASK_VEHICLE_DRIVE_WANDER(NULL, vehSecurityVan, 10, DRIVINGMODE_STOPFORCARS | DF_StopForPeds)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(pedSecurityGuy[0], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							iStageProgress++
						ELIF eWhichWorldPoint = SV_VAR_CITY
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehSecurityVan, <<-551.2968, -1119.4176, 20.4011>>, 10, DRIVINGSTYLE_NORMAL, STOCKADE, DRIVINGMODE_STOPFORCARS | DF_StopForPeds, 2, 4)
							TASK_VEHICLE_DRIVE_WANDER(NULL, vehSecurityVan, 10, DRIVINGMODE_STOPFORCARS | DF_StopForPeds)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(pedSecurityGuy[0], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							iStageProgress++
						ELIF eWhichWorldPoint = SV_VAR_DRIVING3
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehSecurityVan, <<2786.7524, 4358.0322, 48.6794>>, 10, DRIVINGSTYLE_NORMAL, STOCKADE, DRIVINGMODE_STOPFORCARS | DF_StopForPeds, 2, 4)
							TASK_VEHICLE_DRIVE_WANDER(NULL, vehSecurityVan, 10, DRIVINGMODE_STOPFORCARS | DF_StopForPeds)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(pedSecurityGuy[0], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							iStageProgress++
						ELSE
							TASK_VEHICLE_DRIVE_WANDER(pedSecurityGuy[0], vehSecurityVan, 10, DRIVINGMODE_STOPFORCARS | DF_StopForPeds)
							iStageProgress++
						ENDIF
					ELSE
						IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[0], vehSecurityVan)
						AND GET_SCRIPT_TASK_STATUS(pedSecurityGuy[0], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedSecurityGuy[0], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_DONT_JACK_ANYONE)
						ENDIF
						IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[1], vehSecurityVan)
						AND GET_SCRIPT_TASK_STATUS(pedSecurityGuy[1], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedSecurityGuy[1], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_DONT_JACK_ANYONE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF HAS_PLAYER_ATTACKED_SECURITY_GUYS_OR_VAN()
			OR HAS_PLAYER_TOUCHED_VAN_WITH_VEHICLE()
			OR IS_PLAYER_ATTEMPTING_TO_ENTER_THE_SECURITY_VAN()
			OR IS_PLAYER_CLIMBING_ON_BONNET()
			OR IS_PLAYER_PLANTING_BOMB_NEAR_VAN()
			OR IS_PLAYER_AIMING_AT_SECUITY_GUARDS_IN_VAN()
				IF GET_DISTANCE_TO_CLOSEST_GUARD() < 20
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_VFLEE1", CONV_PRIORITY_HIGH)
				ENDIF
				SET_STAGE(SV_FLEE_IN_VAN)
			ENDIF
			INT iTemp
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				REPEAT COUNT_OF(pedSecurityGuy) iTemp
					IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					AND NOT IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
						IF IS_PED_JACKING(PLAYER_PED_ID())	
						AND IS_PED_BEING_JACKED(pedSecurityGuy[iTemp])
							SET_STAGE(SV_SHOOT_OUT)
						ENDIF
					ELSE
						SET_STAGE(SV_FLEE_IN_VAN)
					ENDIF
				ENDREPEAT
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan, TRUE)
					SET_STAGE(SV_SHOOT_OUT)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF thisStage = SV_DRIVE_OFF
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			WEAPON_TYPE wt	
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wt)
			IF wt = WEAPONTYPE_STICKYBOMB
			OR wt = WEAPONTYPE_MOLOTOV
			OR wt = WEAPONTYPE_GRENADE
				DRAW_DEBUG_TEXT("PLAYER HAS PROJECTILE WEAPON", GET_ENTITY_COORDS(PLAYER_PED_ID()))
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, -3.44, -3>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, -13.44, 3>>), 2.3)
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) < 15*15
					SET_STAGE(SV_FLEE_IN_VAN)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, -3.44, -3>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSecurityVan, <<0, -13.44, 3>>), 2.3)
					DRAW_DEBUG_TEXT_2D("PLAYER IS BEHIND VAN", <<0.1, 0.8, 0>>)
				ENDIF
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) < 15*15
					DRAW_DEBUG_TEXT_2D("PLAYER IS CLOSE TO VAN", <<0.1, 0.82, 0>>)
				ENDIF
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					PRINTLN("\n\n\n\nPLAYER IS SHOOTING \n\n\n")
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_VEHICLE_DRIVEABLE(vehSecurityVan) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(vehSecurityVan), 25))
		OR (NOT IS_PED_INJURED(pedSecurityGuy[0]) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(pedSecurityGuy[0]), 20))
		OR (NOT IS_PED_INJURED(pedSecurityGuy[1]) AND IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(pedSecurityGuy[1]), 20))
			SET_STAGE(SV_FLEE_IN_VAN)
		ENDIF
		
		IF thisStage <> SV_DRIVE_OFF
			IF NOT IS_PED_INJURED(pedSecurityGuy[0])
				SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[0], CA_FLEE_WHILST_IN_VEHICLE, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[0], FALSE)
			ENDIF
			IF NOT IS_PED_INJURED(pedSecurityGuy[1])
				SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[1], CA_FLEE_WHILST_IN_VEHICLE, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[1], FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC FLEE_IN_VAN()
	
	SWITCH iStageProgress
		CASE 0
			INT iTemp
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurityGuy[iTemp], TRUE)
				ENDIF
			ENDREPEAT
			bHostile = TRUE
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				IF DOES_ENTITY_EXIST(GET_PRIMARY_GUARD())
					IF NOT IS_PED_INJURED(GET_PRIMARY_GUARD())
						IF GET_PED_IN_VEHICLE_SEAT(vehSecurityVan) <> GET_PRIMARY_GUARD()
						AND IS_VEHICLE_SEAT_FREE(vehSecurityVan, VS_DRIVER)
							TASK_ENTER_VEHICLE(GET_PRIMARY_GUARD(), vehSecurityVan, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedSecurityGuy[1])
				AND GET_PRIMARY_GUARD() <> pedSecurityGuy[1]
					IF GET_PED_IN_VEHICLE_SEAT(vehSecurityVan, VS_FRONT_RIGHT) <> pedSecurityGuy[1]
					AND IS_VEHICLE_SEAT_FREE(vehSecurityVan, VS_FRONT_RIGHT)
						TASK_ENTER_VEHICLE(pedSecurityGuy[1], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
				iStageProgress++
			ELSE
				SET_STAGE(SV_SHOOT_OUT)
			ENDIF
		BREAK
		CASE 1
			IF ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN()
				IF DOES_ENTITY_EXIST(GET_PRIMARY_GUARD())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_VEHICLE_MISSION_PED_TARGET(GET_PRIMARY_GUARD(), vehSecurityVan, PLAYER_PED_ID(), MISSION_FLEE, 50, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds, 100, 5)						
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF HAS_VAN_TAKEN_SUFFICIENT_DAMAGE_TO_PULL_OVER()
				IF DOES_ENTITY_EXIST(GET_PRIMARY_GUARD())
					TASK_VEHICLE_MISSION(GET_PRIMARY_GUARD(), vehSecurityVan, NULL, MISSION_STOP, 0, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds, 100, 100)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				IF GET_ENTITY_SPEED(vehSecurityVan) < 2
					PRINTLN("GOINT TO SHOOT OUT HERE 1")
					SET_STAGE(SV_SHOOT_OUT)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(vehSecurityVan) 
	AND DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehSecurityVan)) 
	AND (IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(vehSecurityVan)) OR IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehSecurityVan)))
	
		INT iNewDriver = 0
		IF GET_PED_IN_VEHICLE_SEAT(vehSecurityVan) = pedSecurityGuy[0]
			iNewDriver = 1
		ENDIF
		IF VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 10*10
			IF NOT IS_PED_INJURED(pedSecurityGuy[iNewDriver])
			AND GET_SCRIPT_TASK_STATUS(pedSecurityGuy[iNewDriver], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				SET_PED_FLEE_ATTRIBUTES(pedSecurityGuy[iNewDriver], FA_USE_VEHICLE, TRUE)
				SEQUENCE_INDEX seqTemp
				OPEN_SEQUENCE_TASK(seqTemp)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL, vehSecurityVan)
					TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehSecurityVan, PLAYER_PED_ID(), MISSION_FLEE, 50, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds, 100, 5)
				CLOSE_SEQUENCE_TASK(seqTemp)
				TASK_PERFORM_SEQUENCE(pedSecurityGuy[iNewDriver], seqTemp)
				CLEAR_SEQUENCE_TASK(seqTemp)
			ENDIF
		ELSE
			PRINTLN("GOINT TO SHOOT OUT HERE 2")
			SET_STAGE(SV_SHOOT_OUT)
		ENDIF
		
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
	OR DOES_PICKUP_EXIST(pickupCase)
	OR eVanStatus = VANSTATUS_DOORS_BLOWN
	OR IS_VEHICLE_STUCK_TIMER_UP(vehSecurityVan, VEH_STUCK_ON_ROOF, ROOF_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP(vehSecurityVan, VEH_STUCK_JAMMED, JAMMED_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP(vehSecurityVan, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
	OR IS_VEHICLE_STUCK_TIMER_UP(vehSecurityVan, VEH_STUCK_ON_SIDE, SIDE_TIME)
	OR (iStageProgress > 1 AND NOT ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN())
		IF NOT ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN()
			PRINTLN("ARE_ALL_REMAINING_SECURITY_PEDS_IN_SECURITY_VAN() RETURNED FALSE")
		ENDIF
		PRINTLN("GOINT TO SHOOT OUT HERE 3")
		SET_STAGE(SV_SHOOT_OUT)
	ENDIF
	
ENDPROC

PROC FLEE_ON_FOOT()
	INT iTemp
	SWITCH iStageProgress
		CASE 0
			IF NOT bDepositedBag
				IF DOES_ENTITY_EXIST(objIdCase)
				AND IS_ENTITY_ATTACHED(objIdCase)
					DETACH_ENTITY(objIdCase)
				ENDIF
			ENDIF
			SEQUENCE_INDEX seqTemp
			IF (NOT IS_PED_INJURED(pedSecurityGuy[0]) OR NOT IS_PED_INJURED(pedSecurityGuy[1]))
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_DISTANCE_TO_CLOSEST_GUARD() < 20
					CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_FFLEE", CONV_PRIORITY_HIGH)
				ENDIF
				REPEAT COUNT_OF(pedSecurityGuy) iTemp
					IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
						SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[iTemp], CA_ALWAYS_FLEE, TRUE)
						SET_PED_FLEE_ATTRIBUTES(pedSecurityGuy[iTemp], FA_DISABLE_HANDS_UP, TRUE)
						SET_PED_FLEE_ATTRIBUTES(pedSecurityGuy[iTemp], FA_WANDER_AT_END, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedSecurityGuy[iTemp], CA_USE_VEHICLE, FALSE)
						OPEN_SEQUENCE_TASK(seqTemp)
						IF IS_PED_IN_ANY_VEHICLE(pedSecurityGuy[iTemp], TRUE)
							TASK_LEAVE_ANY_VEHICLE(pedSecurityGuy[iTemp], 0, ECF_DONT_CLOSE_DOOR)
						ENDIF
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(pedSecurityGuy[iTemp], seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
					ENDIF
				ENDREPEAT
			ENDIF
			bVanAbandoned = TRUE
			iStageProgress++
		BREAK
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				IF IS_VEHICLE_EMPTY(vehSecurityVan, TRUE)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//=================================== START OF MAIN SCRIPT ================================

FUNC BOOL HAS_PLAYER_AGGRESIVELY_LAUNCHED_EVENT()
	INT iTemp
	REPEAT COUNT_OF(pedSecurityGuy) iTemp
		IF DOES_ENTITY_EXIST(pedSecurityGuy[iTemp])
			IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
			AND NOT IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedSecurityGuy[iTemp]), 5)
				OR IS_BAD_EXPLOSION_IN_AREA(GET_ENTITY_COORDS(pedSecurityGuy[iTemp])+<<10,10,5>>, GET_ENTITY_COORDS(pedSecurityGuy[iTemp])-<<5,5,5>>, <<0,0,0>>)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurityGuy[iTemp], PLAYER_PED_ID())
					PRINTLN("AGGRESSIVE LAUNCH 1")
					bHostile = TRUE
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("AGGRESSIVE LAUNCH 2")
				bHostile = TRUE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VARIATION_COMPLETE()
	BOOL bReturn = FALSE
	
	SWITCH eWhichWorldPoint
		CASE SV_VAR_GAS_STATION	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV1)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_CLUCKING_BELL
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV2)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_PALETO	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV4)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_CITY
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV5)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING_VENICE	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV6)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING2
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV7)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING3
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV8)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING4	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV9)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING5	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV10)
				bReturn = TRUE
			ENDIF
		BREAK
		CASE SV_VAR_DRIVING6	
			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV11)
				bReturn = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_BLOWN_VAN()
	FLOAT checkRange = 50
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND ((IS_VEHICLE_DRIVEABLE(vehSecurityVan) AND bDoorsBlown AND DOES_PICKUP_EXIST(pickupCase)) OR (NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan) AND DOES_PICKUP_EXIST(pickupCase)))
	AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan))<20
	AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupCase))<20
		PED_INDEX tempCop = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<checkRange,checkRange,checkRange>>) 
		IF NOT IS_PED_INJURED(tempCop)
			IF GET_ENTITY_MODEL(tempCop) = S_M_Y_COP_01
			OR GET_ENTITY_MODEL(tempCop) = S_F_Y_COP_01
				RETURN TRUE
			ENDIF
		ENDIF
		tempCop = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<checkRange,checkRange,checkRange>>)
		IF NOT IS_PED_INJURED(tempCop)
			IF GET_ENTITY_MODEL(tempCop) = S_M_Y_COP_01
			OR GET_ENTITY_MODEL(tempCop) = S_F_Y_COP_01
				RETURN TRUE
			ENDIF
		ENDIF
		VEHICLE_INDEX tempCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), checkRange, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY)
		IF IS_VEHICLE_DRIVEABLE(tempCopCar)
			RETURN TRUE
		ENDIF
		tempCopCar = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), checkRange, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES_ONLY)
		IF IS_VEHICLE_DRIVEABLE(tempCopCar)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

INT iWaitProgress = 0
INT iHintStage = 0
BOOL bCalmWalkActive[2]
BOOL bPanicWalkActive[2]

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

	vInput = vInput
	vInput = in_coords.vec_coord[0]
	PRINTLN("re_securityvan ", vInput)
		// 638, -842
	IF VDIST(vInput, << -337.3338, -1460.373, 29.5668>>) < 5
		eWhichWorldPoint = SV_VAR_GAS_STATION
	ELIF VDIST(vInput, <<-588.4711, -866.9462, 25.3292>>) < 5
		eWhichWorldPoint = SV_VAR_CLUCKING_BELL
	ELIF VDIST(vInput, << -389, 6061, 31 >>) < 5
		eWhichWorldPoint = SV_VAR_PALETO
	ELIF VDIST(vInput, << -600, -1094, 22.76>>) < 5
		eWhichWorldPoint = SV_VAR_CITY
	ELIF VDIST(vInput, << -1025, -1086, 2 >>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING_VENICE
	ELIF VDIST(vInput, << -595.5618, -667.7235, 31.0544 >>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING2
	ELIF VDIST(vInput, <<3018.3843, 3634.0168, 70.4076>>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING3
	ELIF VDIST(vInput, <<-2815.6614, 2208.1707, 27.8382>>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING4
	ELIF VDIST(vInput, <<856.7742, -2067.8452, 29.0704>>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING5
	ELIF VDIST(vInput, <<805.1932, -703.1327, 28.1632>>) < 5
		eWhichWorldPoint = SV_VAR_DRIVING6
	ELSE
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - no local worldpoint found near ", vInput)
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF g_bMagDemoActive
	
		IF eWhichWorldPoint != SV_VAR_CLUCKING_BELL
			PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - variation ", ENUM_TO_INT(eWhichWorldPoint), " not allowed during MAGDEMO")
			TERMINATE_THIS_THREAD()
		ENDIF
		
	ELSE
	
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
			INT iTemp
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
						IF NOT IS_PED_IN_VEHICLE(pedSecurityGuy[iTemp], vehSecurityVan)
							TASK_ENTER_VEHICLE(pedSecurityGuy[iTemp], vehSecurityVan, DEFAULT_TIME_NEVER_WARP, GET_FIRST_FREE_VEHICLE_SEAT(vehSecurityVan))
						ENDIF
					ELSE
						TASK_STAND_STILL(pedSecurityGuy[iTemp], 10000)
					ENDIF
				ENDIF
			ENDREPEAT
			missionCleanup()
		ENDIF
		
		IF NOT CAN_RANDOM_EVENT_LAUNCH(vInput, RE_SECURITYVAN, ENUM_TO_INT(eWhichWorldPoint), TRUE)
			TERMINATE_THIS_THREAD()
		ELSE
			LAUNCH_RANDOM_EVENT()
		ENDIF
		
	ENDIF
	
	initialiseVariables()
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		SET_DEBUG_TEXT_VISIBLE(FALSE)
		CREATE_VAN_WIDGET()
	#ENDIF
	
	sbiSecVan = ADD_SCENARIO_BLOCKING_AREA(vCreateSecurityVanPos + <<30,30,30>>, vCreateSecurityVanPos - <<30,30,30>>)
	
	// Mission Loop ------------------------------------------- //
	WHILE TRUE
	
		WAIT(0)
		
		UPDATE_LOAD_QUEUE_MEDIUM(sLoadQueue)
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()

			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				missionCleanup()
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_SV")
			
			PRINTLN("iWaitprog = ", iWaitProgress)
			
			SWITCH iWaitProgress
			
				CASE 0
					IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
					AND NOT SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
						IF loadAssets()
//							DRAW_DEBUG_SPHERE(vCreateSecurityVanPos, 3)
							IF (thisEventVariation = EN_ROUTE AND NOT (IS_SPHERE_VISIBLE(vCreateSecurityVanPos, 3) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCreateSecurityVanPos) < 150))
//							OR (thisEventVariation = EN_ROUTE AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCreateSecurityVanPos) > 175)
							OR thisEventVariation != EN_ROUTE
							
								IF thisEventVariation = EN_ROUTE
									IF NOT IS_SPHERE_VISIBLE(vCreateSecurityVanPos, 8)
										CLEAR_AREA(vCreateSecurityVanPos, 4, TRUE)
									ENDIF
									thisStage = SV_DRIVE_OFF
									bDepositedBag = TRUE
								ELSE
									thisStage =  SV_LOAD_CASE
								ENDIF
								
								createScene()
								
								IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
								AND thisEventVariation = EN_ROUTE
									SET_VEHICLE_FORWARD_SPEED(vehSecurityVan, 5)
								ENDIF
								
								SETTIMERA(0)
								iWaitProgress++
								
							ENDIF
						ENDIF
					ELSE
						missionCleanup()
					ENDIF
				BREAK
				
				CASE 1
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) < 100*100
						OR NOT IS_ENTITY_OCCLUDED(vehSecurityVan)
						OR (NOT IS_PED_INJURED(pedSecurityGuy[0]) AND NOT IS_ENTITY_OCCLUDED(pedSecurityGuy[0]))
						OR (NOT IS_PED_INJURED(pedSecurityGuy[1]) AND NOT IS_ENTITY_OCCLUDED(pedSecurityGuy[1]))
							iWaitProgress++
						ENDIF
					ENDIF
					IF HAS_PLAYER_AGGRESIVELY_LAUNCHED_EVENT()
						iWaitProgress++
					ENDIF
					IF iWaitProgress <> 1
						SETTIMERA(0)
					ENDIF
				BREAK
				
				CASE 2
				
					IF TIMERA() > 2000
						IF thisEventVariation != EN_ROUTE
							LOAD_CASE()
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehSecurityVan)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					
						IF thisEventVariation = EN_ROUTE
							DRIVE_OFF()
							IF NOT IS_ENTITY_OCCLUDED(vehSecurityVan)
							AND(VDIST(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100 OR HAS_PLAYER_AGGRESIVELY_LAUNCHED_EVENT())
								SET_RANDOM_EVENT_ACTIVE()		
							ENDIF
						ELSE
							IF (NOT IS_ENTITY_OCCLUDED(vehSecurityVan) AND thisStage = SV_DRIVE_OFF)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerPos1, vTriggerPos2, fTriggerWidth)
							OR HAS_PLAYER_AGGRESIVELY_LAUNCHED_EVENT()
								SET_RANDOM_EVENT_ACTIVE()		
							ENDIF
						ENDIF
						
						IF thisStage = SV_DRIVE_OFF
						AND IS_ENTITY_OCCLUDED(vehSecurityVan)
						AND thisEventVariation <> EN_ROUTE
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) > 100
								missionCleanup()
							ELSE
								SET_RANDOM_EVENT_ACTIVE()		
							ENDIF
						ENDIF
						
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
			IF iWaitProgress > 0
			
				IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				AND IS_ENTITY_OCCLUDED(vehSecurityVan)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) > 250*250
						missionCleanup()
					ENDIF
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSecurityVan)) > 100*100
					AND NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
						missionCleanup()
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
		
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			IF NOT DOES_BLIP_EXIST(blipInitialMarker)
			
				IF NOT HAS_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_SECVANMAIN))
				AND NOT bDoorsBlown
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SV_VANHELP1", 15000)
						SET_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_SECVANMAIN))
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objIdCase)
					IF iHintStage <> 0
						IF VDIST2(GET_ENTITY_COORDS(objIdCase), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(50,2)
							iHintStage = 0 
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ELIF bDepositedBag
				AND NOT DOES_PICKUP_EXIST(pickupCase)
				AND IS_VEHICLE_DRIVEABLE(vehSecurityVan)
				AND NOT bCaseCollected
					IF iHintStage <> 1
						IF VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(50,2)
							iHintStage = 1 
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ELIF DOES_PICKUP_EXIST(pickupCase)
					IF iHintStage <> 2
						IF VDIST2(GET_PICKUP_COORDS(pickupCase), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(50,2)
							iHintStage = 2
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ENDIF
				
				IF iHintStage = 0
					IF DOES_ENTITY_EXIST(objIdCase)
						CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCam, objIdCase)
						IF VDIST2(GET_ENTITY_COORDS(objIdCase), GET_ENTITY_COORDS(PLAYER_PED_ID())) > POW(50,2)
							iHintStage = -1
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ELIF iHintStage = 1
					IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan, TRUE)
						CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCam, vehSecurityVan)
						IF VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) > POW(50,2)
							iHintStage = -1
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ELIF iHintStage = 2
					IF DOES_PICKUP_EXIST(pickupCase)
						CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCam, GET_PICKUP_COORDS(pickupCase))
						IF VDIST2(GET_PICKUP_COORDS(pickupCase), GET_ENTITY_COORDS(PLAYER_PED_ID())) > POW(50,2)
							iHintStage = -1
							KILL_CHASE_HINT_CAM(localChaseHintCam)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehSecurityVan)
				AND bDepositedBag
				AND NOT bPickupAvailable
				AND (NOT HAS_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_DOORSHOOT)) 
						OR (HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB) AND NOT HAS_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_ATTACHBOMB))) 
						)
					
					SWITCH iDoorHelpStage
						CASE 0
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							AND VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 225 //15^2
								PRINT_HELP("SV_DOORHELP1", 15000)
								SET_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_DOORSHOOT))
								iDoorHelpStage++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								iLastDoorHelpTime = GET_GAME_TIMER()
								iDoorHelpStage++
							ENDIF
						BREAK
						CASE 2
							IF (GET_GAME_TIMER() - iLastDoorHelpTime) > 2000
							AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB)
								PRINT_HELP("SV_DOORHELP2", 15000)
								SET_SECURITY_VAN_HELP_DISPLAYED(ENUM_TO_INT(SVHT_ATTACHBOMB))
								iDoorHelpStage++
							ENDIF
						BREAK
					ENDSWITCH
					
				ENDIF
				
				IF bPickupAvailable
				OR bCaseCollected
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SV_DOORHELP1")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SV_DOORHELP2")
						CLEAR_HELP()
					ENDIF
				ENDIF
				
			ENDIF
			
			SWITCH thisStage
			
				CASE SV_LOAD_CASE
					LOAD_CASE()
				BREAK
				
				CASE SV_HOSTILE_RESPONSE
					HOSTILE_RESPONSE()
				BREAK
				
				CASE SV_HOSTILE_RESPONSE_TO_CAR
					HOSTILE_RESPONSE_TO_VEHICLE()
				BREAK
				
				CASE SV_DRIVE_OFF
					DRIVE_OFF()
				BREAK
				
				CASE SV_FLEE_ON_FOOT
					FLEE_ON_FOOT()
				BREAK
				
				CASE SV_FLEE_IN_VAN
					FLEE_IN_VAN()
				BREAK
				
				CASE SV_RECOVER_CASE
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iCurrentCaseRecoverer), <<0.1,0.12,0>>)
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iCurrentCaseHolder), <<0.1,0.14,0>>)
					#ENDIF
					IF NOT IS_PED_INJURED(pedSecurityGuy[0])
						IF GET_PED_ALERTNESS(pedSecurityGuy[0]) <> AS_MUST_GO_TO_COMBAT
							SET_PED_ALERTNESS(pedSecurityGuy[0], AS_MUST_GO_TO_COMBAT)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedSecurityGuy[1])
						IF GET_PED_ALERTNESS(pedSecurityGuy[1]) <> AS_MUST_GO_TO_COMBAT
							SET_PED_ALERTNESS(pedSecurityGuy[1], AS_MUST_GO_TO_COMBAT)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedSecurityGuy[0]) AND NOT IS_ENTITY_DEAD(pedSecurityGuy[0])
					AND NOT IS_PED_INJURED(pedSecurityGuy[1]) AND NOT IS_ENTITY_DEAD(pedSecurityGuy[1])
						iCurrentCaseRecoverer = 1
						RECOVER_CASE(pedSecurityGuy[1])
						GO_TO_AIMING_POSITION(0)
					ELSE
						IF iCurrentCaseRecoverer = 1
							IF IS_PED_INJURED(pedSecurityGuy[1]) OR IS_ENTITY_DEAD(pedSecurityGuy[1])
								iStageProgress = 0
								iCurrentCaseRecoverer = 0
							ELSE
								RECOVER_CASE(pedSecurityGuy[1])
							ENDIF
						ELIF iCurrentCaseRecoverer = 0
							IF IS_PED_INJURED(pedSecurityGuy[0]) OR IS_ENTITY_DEAD(pedSecurityGuy[0])
								iCurrentCaseRecoverer = -1
							ELSE
								RECOVER_CASE(pedSecurityGuy[0])
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE SV_SHOOT_OUT
					ATTACK_PLAYER()
				BREAK
				
				CASE SV_CLEAN_UP
					missionCleanup()
				BREAK
				
			ENDSWITCH
			
			IF HAS_PLAYER_LOST_OBJECTIVE()
				SET_STAGE(SV_CLEAN_UP)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_PED_IN_VEHICLE_SEAT(vehSecurityVan) = PLAYER_PED_ID()
			AND NOT bVanAbandoned
			AND thisStage <> SV_SHOOT_OUT
				IF bDepositedBag
					set_stage(SV_SHOOT_OUT)
				ELSE
					set_stage(SV_FLEE_ON_FOOT)
				ENDIF
			ENDIF
			
			IF bHostile
				IF GET_RELATIONSHIP_BETWEEN_GROUPS(security, RELGROUPHASH_PLAYER) <> ACQUAINTANCE_TYPE_PED_HATE
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, security, RELGROUPHASH_PLAYER)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, security)
				ENDIF
			ENDIF
			
			IF bPickupAvailable
				IF DOES_PICKUP_EXIST(pickupCase)
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupCase)) > 250
					SET_STAGE(SV_CLEAN_UP)
				ENDIF
			ENDIF
			
			IF NOT bCaseCollected
			
				IF HAS_CASE_BEEN_COLLECTED()
					bCaseCollected = TRUE
				ENDIF
				
			ELSE
			
				BOOL bBothPedsDone = TRUE
				
				INT iCheckForDone
				iCheckForDone = 0
				IF DOES_ENTITY_EXIST(pedSecurityGuy[iCheckForDone])
					IF NOT IS_ENTITY_DEAD(pedSecurityGuy[iCheckForDone])
					AND NOT IS_PED_INJURED(pedSecurityGuy[iCheckForDone])
					AND VDIST2(GET_ENTITY_COORDS(pedSecurityGuy[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100*100
						bBothPedsDone = FALSE
					ENDIF
				ENDIF
				
				BOOL bVanDOne
				IF DOES_ENTITY_EXIST(vehSecurityVan)
					IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan)
							bVanDone = TRUE
						ENDIF
					ENDIF
					IF (VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100 * 100 AND IS_ENTITY_OCCLUDED(vehSecurityVan))
					OR VDIST2(GET_ENTITY_COORDS(vehSecurityVan), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 250*250	
						bVanDone = TRUE
					ENDIF
				ENDIF
				
				IF bVanDone
				AND bBothPedsDone
					missionPassed()
				ENDIF
				
			ENDIF
			
			MANAGE_BLIPS()
			UPDATE_VAN_STATUS()
			MANAGE_CASE()
			
		ENDIF
		
		IF NOT bCallPolice
			IF HAS_PLAYER_WARRANTED_ARREST()
				iTimeOfCall = GET_GAME_TIMER()
				bCallPolice = TRUE
			ENDIF
		ENDIF
		
		IF bCallPolice
			IF NOT bPoliceCalled
				IF (GET_GAME_TIMER() - iTimeOfCall) > 5000
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					IF GET_DISTANCE_TO_CLOSEST_GUARD() < 30
						IF CREATE_CONVERSATION(sSpeech, sBlock, "RESEC_COPS", CONV_PRIORITY_HIGH)
							bPoliceCalled = TRUE
						ENDIF
					ELSE
						bPoliceCalled = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (GET_GAME_TIMER() - iTimeOfCall) > 10000
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		AND NOT bReportDone 
			INT iRand
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRand = 0
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_SEC_TRUCK_01", 0.0)
			ELIF iRand = 1
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_SEC_TRUCK_02", 0.0)
			ELSE
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_SEC_TRUCK_03", 0.0)
			ENDIF
			bReportDone = TRUE
		ENDIF
		
		IF bPoliceCalled
			IF (NOT IS_PED_INJURED(pedSecurityGuy[0]) OR NOT IS_PED_INJURED(pedSecurityGuy[1]))
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_CLOSEST_SECURITY_GUARD())) < POW(50,2)
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		AND IS_PLAYER_NEAR_BLOWN_VAN()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
		
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			SET_STAGE(SV_CLEAN_UP)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_LEFT) > 0
			OR GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_RIGHT) > 0
				IF GET_VEHICLE_DOOR_LOCK_STATUS(vehSecurityVan) = VEHICLELOCK_LOCKOUT_PLAYER_ONLY
					SET_VEHICLE_DOORS_LOCKED(vehSecurityVan, VEHICLELOCK_UNLOCKED)
				ENDIF
			ENDIF
			IF NOT bPlayerJackedVeh
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehSecurityVan)
					bPlayerJackedVeh = TRUE
				ENDIF
			ENDIF
			IF NOT bPlayerJackedVeh
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_LEFT) = 0
				AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehSecurityVan, SC_DOOR_FRONT_RIGHT) = 0
					IF GET_VEHICLE_DOOR_LOCK_STATUS(vehSecurityVan) = VEHICLELOCK_UNLOCKED
						SET_VEHICLE_DOORS_LOCKED(vehSecurityVan, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iShockingEventAiming <> 0
			IF thisStage = SV_SHOOT_OUT
			OR thisStage = SV_FLEE_IN_VAN
			OR thisStage = SV_DRIVE_OFF
				REMOVE_SHOCKING_EVENT(iShockingEventAiming)
			ENDIF
		ENDIF
		
		INT iTemp
		
		IF DOES_ENTITY_EXIST(objIdCase)
		
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
			
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
				AND NOT IS_ENTITY_DEAD(pedSecurityGuy[iTemp])
				
					IF IS_ENTITY_ATTACHED_TO_ENTITY(objIdCase, pedSecurityGuy[iTemp])
						IF thisStage = SV_LOAD_CASE
							IF NOT bCalmWalkActive[iTemp]
								SET_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE, "random@security_van", "sec_idle")
								SET_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK, "random@security_van", "sec_walk_calm")
								bCalmWalkActive[iTemp] = TRUE
							ENDIF
						ENDIF
						IF thisStage = SV_RECOVER_CASE
							IF NOT bPanicWalkActive[iTemp]
								SET_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE, "random@security_van", "sec_idle")
								SET_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK, "random@security_van", "sec_walk_panic")
								bPanicWalkActive[iTemp] = TRUE
							ENDIF
						ENDIF
						IF IS_PED_ARMED(pedSecurityGuy[iTemp], WF_INCLUDE_GUN)
						AND NOT bWeaponHidden
							HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(pedSecurityGuy[iTemp], TRUE)
							bWeaponHidden = TRUE
						ENDIF
					ELSE
						IF bCalmWalkActive[iTemp]
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE)
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK)
							bCalmWalkActive[iTemp] = FALSE
							PRINTLN("CLEAR ALTERNATE WALKS 1")
						ENDIF
						IF bPanicWalkActive[iTemp]
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE)
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK)
							bPanicWalkActive[iTemp] = FALSE
							PRINTLN("CLEAR ALTERNATE WALKS 2")
						ENDIF
						IF bCalmWalkActive[iTemp]
						OR bPanicWalkActive[iTemp]
							bCalmWalkActive[iTemp] = FALSE
							bPanicWalkActive[iTemp] = FALSE
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE)
							CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK)
							PRINTLN("CLEAR ALTERNATE WALKS 3")
						ENDIF
						IF IS_PED_ARMED(pedSecurityGuy[iTemp], WF_INCLUDE_GUN)
						AND bWeaponHidden
							HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(pedSecurityGuy[iTemp], TRUE)
							bWeaponHidden = FALSE
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				IF bCalmWalkActive[0]
					DRAW_DEBUG_TEXT_2D("PED 0 CALM CASE WALK ACTIVE", <<0.1, 0.3, 0>>)
				ENDIF
				IF bPanicWalkActive[0]
					DRAW_DEBUG_TEXT_2D("PED 0 PANIC CASE WALK ACTIVE", <<0.1, 0.3, 0>>)
				ENDIF
				IF bCalmWalkActive[1]
					DRAW_DEBUG_TEXT_2D("PED 1 CALM CASE WALK ACTIVE", <<0.1, 0.32, 0>>)
				ENDIF
				IF bPanicWalkActive[1]
					DRAW_DEBUG_TEXT_2D("PED 1 PANIC CASE WALK ACTIVE", <<0.1, 0.32, 0>>)
				ENDIF
			#ENDIF
			
		ELSE
		
			REPEAT COUNT_OF(pedSecurityGuy) iTemp
				IF NOT IS_PED_INJURED(pedSecurityGuy[iTemp])
					IF bCalmWalkActive[iTemp]
					OR bPanicWalkActive[iTemp]
						bCalmWalkActive[iTemp] = FALSE
						bPanicWalkActive[iTemp] = FALSE
						CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_IDLE)
						CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedSecurityGuy[iTemp], AAT_WALK)
						PRINTLN("CLEAR ALTERNATE WALKS 4")
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
		
		IF NOT bDepositedBag
		AND NOT bIsloading
			IF DOES_ENTITY_EXIST(objIdCase)
			AND IS_ENTITY_ATTACHED(objIdCase)
			AND NOT IS_PED_INJURED(pedSecurityGuy[iCurrentCaseHolder])
			AND NOT IS_ENTITY_PLAYING_ANIM(pedSecurityGuy[iCurrentCaseHolder], "random@security_van", "sec_hand_override")
			AND NOT IS_ENTITY_PLAYING_ANIM(pedSecurityGuy[iCurrentCaseHolder], "random@security_van", "sec_case_into_van_panic")
			AND NOT IS_ENTITY_PLAYING_ANIM(pedSecurityGuy[iCurrentCaseHolder], "random@security_van", "sec_case_into_van_calm")
				TASK_PLAY_ANIM(pedSecurityGuy[iCurrentCaseHolder], "random@security_van", "sec_hand_override", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_LOOPING)
			ENDIF
		ENDIF
		
		IF NOT bPlayerHasDamagedVan
			IF DOES_ENTITY_EXIST(vehSecurityVan)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehSecurityVan, PLAYER_PED_ID(), TRUE)
					bPlayerHasDamagedVan = TRUE
				ENDIF
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehSecurityVan)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			RUN_VAN_WIDGET()
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iLoadCaseScene)), <<0.8, 0.5, 0>>)
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_PRIMARY_GUARD())
			AND NOT IS_PED_INJURED(GET_PRIMARY_GUARD())
				DRAW_DEBUG_SPHERE(GET_PED_BONE_COORDS(GET_PRIMARY_GUARD(), BONETAG_HEAD, <<0,0,0.2>>), 0.1)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				missionCleanup()
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
				IF NOT IS_PED_INJURED(pedSecurityGuy[0])
					EXPLODE_PED_HEAD(pedSecurityGuy[0])
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
				IF NOT IS_PED_INJURED(pedSecurityGuy[1])	
					EXPLODE_PED_HEAD(pedSecurityGuy[1])
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
				IF NOT IS_ENTITY_DEAD(vehSecurityVan)
					SET_ENTITY_HEALTH(vehSecurityVan, 0)
					SET_VEHICLE_ENGINE_HEALTH(vehSecurityVan, 0)
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
				IF NOT IS_ENTITY_DEAD(vehSecurityVan)
					EXPLODE_VEHICLE(vehSecurityVan, TRUE)
				ENDIF
			ENDIF
			
			//IF bCallPolice
			//	IF NOT bPoliceCalled
			//		DRAW_DEBUG_TEXT_2D("POLICE ALERT FLAGGED, WANTED LEVEL AFTER 11000", <<0.1, 0.3, 0>>)
			//		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(GET_GAME_TIMER() - iTimeOfCall), <<0.1, 0.32, 0>>)
			//	ELSE
			//		DRAW_DEBUG_TEXT_2D("WANTED LEVEL APPLIED", <<0.1, 0.3, 0>>)	
			//	ENDIF
			//ENDIF

			
			IF DOES_ENTITY_EXIST(vehSecurityVan)
				
				TEXT_LABEL_63 sTemp
				FLOAT fTemp = 0.6
				sTemp = "Security van health: "
				sTemp += GET_ENTITY_HEALTH(vehSecurityVan)
				DRAW_DEBUG_TEXT_2D(sTemp, <<0.1, fTemp, 0>>)
				fTemp += 0.02
				
				sTemp = "Security van engine health: "
				sTemp += GET_STRING_FROM_FLOAT(GET_VEHICLE_ENGINE_HEALTH(vehSecurityVan))
				DRAW_DEBUG_TEXT_2D(sTemp, <<0.5, fTemp, 0>>)
				fTemp += 0.02
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehSecurityVan)
					DRAW_DEBUG_TEXT_2D("Security van is no longer driveable", <<0.1, fTemp, 0>>)
					fTemp += 0.02
				ENDIF
				
				IF IS_ENTITY_DEAD(vehSecurityVan)
					DRAW_DEBUG_TEXT_2D("Security van is technically dead", <<0.1, fTemp, 0>>)
					fTemp += 0.02
				ENDIF
				
				IF bPickupAvailable
					DRAW_DEBUG_TEXT_2D("Case is available", <<0.1, fTemp, 0>>)
					fTemp += 0.02
				ENDIF
				
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.2,0.1,0>>)
				#ENDIF
				
				IF NOT IS_PED_INJURED(pedSecurityGuy[1])
					IF bCalmWalkActive[1]
						DRAW_DEBUG_SPHERE(GET_PED_BONE_COORDS(pedSecurityGuy[1], BONETAG_PH_R_HAND, <<0,0,0.0>>), 0.1, 0, 255, 0)
					ELSE
						DRAW_DEBUG_SPHERE(GET_PED_BONE_COORDS(pedSecurityGuy[1], BONETAG_PH_R_HAND, <<0,0,0.0>>), 0.1, 255, 0, 0)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehSecurityVan)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLoadCaseScene)
						DRAW_DEBUG_TEXT("van seq playing", GET_ENTITY_COORDS(vehSecurityVan))
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objShootDummy)
				
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
						SET_ENTITY_VISIBLE(objShootDummy, NOT IS_ENTITY_VISIBLE(objShootDummy))
					ENDIF
					
					IF IS_ENTITY_VISIBLE(objShootDummy)
						sTemp = "Damage: "
						sTemp += iTotalPlayerDamageToDummy
						DRAW_DEBUG_TEXT(sTemp, GET_ENTITY_COORDS(objShootDummy)+<<0,0,0.2>>)
					ENDIF
					
				ENDIF
				
			ENDIF
			
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
