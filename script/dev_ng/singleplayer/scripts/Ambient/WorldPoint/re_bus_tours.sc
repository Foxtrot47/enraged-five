
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "AggroSupport.sch"
USING "dialogue_public.sch"
USING "Ambient_Speech.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "ambient_common.sch"
USING "random_events_public.sch"
USING "RC_Helper_Functions.sch"
USING "re_bus_tour_crowd.sch"

// Enums
ENUM BUS_TOUR_STAGES
	STAGE_INIT_BUS_TOURS,
	STAGE_RUN_BUS_TOURS,
	STAGE_CLEAN_UP
ENDENUM
BUS_TOUR_STAGES bus_toursStage = STAGE_INIT_BUS_TOURS

ENUM RUN_bus_tours_STAGES
	bus_tours_enter_as_passenger,
	bus_tours_player_in_bus,
	bus_tours_tour_progress
ENDENUM
RUN_bus_tours_STAGES Runbus_toursStage = bus_tours_enter_as_passenger

ENUM POI_STAGES
	POI_CHECK_COORDS,
	POI_PLAY_DIALOGUE,
	ENDCUTSCENE_WARPS,
	POI_CLEANUP,
	POI_FINISH_TOUR,
	POI_FINISH_TOUR_CLEANUP
ENDENUM
POI_STAGES point_of_interest_speech = POI_CHECK_COORDS

ENUM StageState
	SS_INIT,
	SS_RUNNING,
	SS_CLEANUP
ENDENUM

StageState eStageState = SS_INIT
		
// Structs
STRUCT POI_DATA
	
	STRING sDialogue
	VECTOR vPosition
	VECTOR vLookPosition
	INT    iLookTime
	FLOAT  fSpeed
	BOOL   bVisited
	
	VECTOR vSkipPosition
	FLOAT  fSkipHeading
	BOOL   bSkippedTo
	
	#IF IS_DEBUG_BUILD
		BLIP_INDEX blip
		STRING name
	#ENDIF
ENDSTRUCT

// Constants
CONST_INT POI_CHATEAU			0
CONST_INT POI_VIPER				1
CONST_INT POI_TEQUILA			2
CONST_INT POI_MOVIESTAR1		3
CONST_INT POI_MOVIESTAR2		4
CONST_INT POI_MOVIESTAR3		5
CONST_INT POI_RICHMOND			6
CONST_INT POI_WEAZEL			7
CONST_INT POI_STUDIO			8
CONST_INT POI_PORTOLA			9
CONST_INT POI_EPSILON			10
CONST_INT POI_CITYHALL			11
CONST_INT POI_CHINESE			12
CONST_INT POI_FINAL_STOP		13

CONST_INT NUMBER_OF_POI			14
CONST_INT MAX_NUM_CROWD         5

CONST_INT OPENING_TIME  		6
CONST_INT CLOSING_TIME   		19

CONST_INT iTotalLines			14
// Variables
POI_DATA 				  poiTour[NUMBER_OF_POI]
POI_DATA 				  poiEnd
INT 	 				  iCurrentPOI
			
VECTOR 			  		  vInput, ReturnedCarVec
PED_INDEX 		 		  bus_driver, tour_guide, ped_crowd[MAX_NUM_CROWD]
INT 					  ped_delay_lookattime[MAX_NUM_CROWD]
INT						  start_looking_time
BOOL 					  ped_delay_looking[MAX_NUM_CROWD]
VEHICLE_INDEX 	  		  veh_tour_bus
FLOAT 			  		  ReturnedCarHeading, bus_speed
REL_GROUP_HASH 	  		  relGroupbus_tours
BLIP_INDEX 		  		  veh_tour_bus_blip, blip_random_event
INT 			  		  active_stage, counter_current_time_tour, counter_time_started_tour, irambling_speech
BOOL 					  forceSkip = FALSE
BOOL 			  		  At_Point_Of_Interest, bPathNodesRequested, bFPSActive
VECTOR 			  		  vPathNodeRequestMin = << 0.0, 0.0, 0.0 >>
VECTOR 			 		  vPathNodeRequestMax = << 0.0, 0.0, 0.0 >>

SEQUENCE_INDEX 	  		  seq, seq2
CAMERA_INDEX 	  		  cam_FPS, Cam_1, Cam_2
enumCharacterList 		  current_player_enum
INT 			  		  iBitFieldDontCheck, end_cutscene_stage
INT 			  		  lockOnTimer, crowd_index, iLookAroundTimeStamp
EAggro 			  		  aggroReason
BOOL 			  		  bAggroed, bSuppressHelp, tourguide_get_in

FLOAT 			  		  fClubCamChangeX 
FLOAT 			  	      fClubCamChangeY 

CONST_INT 				  STICK_DEAD_ZONE 28
structPedsForConversation BusTourConversation
INT 					  iCurrentRamble
STRING 					  animDict = "RANDOM@BUS_TOUR_GUIDE@IDLE_A"

VEHICLE_ZOOM_LEVEL 	      StoredCamBusCinematic
DRIVINGMODE 			  tourDrivingMode = DF_StopForCars|DF_StopForPeds|DF_SteerAroundStationaryCars|DF_StopAtLights|DF_SteerAroundObjects|DF_ChangeLanesAroundObstructions
CONST_FLOAT 		 	  TOUR_SPEED_SLOW 3.0
CONST_FLOAT 			  TOUR_SPEED_NORM 8.0
CONST_FLOAT 			  TOUR_SPEED_FAST 15.0

INT 					  iBusCost = 40
BOOL 					  bBusUnlocked = FALSE


STRING 					  iRambleLine[22]
BOOL 					  sayRambleBeforeCleanup = FALSE

BOOL					  endCutsceneStarted
// Debug
#IF IS_DEBUG_BUILD
	INT  iRunbus_toursStage
	BOOL bRunDebugSelection 
	BOOL bAddAllLocationBlips
	BOOL bAddActiveLocationBlips
	BOOL bRouteHasBeenAdded
#ENDIF

FUNC BOOL IS_VEHICLE_STUCK(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, 7000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, 7000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, 7000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Setup AI ped relationships with the player
PROC SETUP_RELATIONSHIPS()

	SET_PED_COMBAT_ATTRIBUTES(bus_driver, CA_ALWAYS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(tour_guide, CA_ALWAYS_FLEE, TRUE)
						
	ADD_RELATIONSHIP_GROUP("re_bus_tours1", relGroupbus_tours)
	SET_PED_RELATIONSHIP_GROUP_HASH(bus_driver, relGroupbus_tours)
	SET_PED_RELATIONSHIP_GROUP_HASH(tour_guide, relGroupbus_tours)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupbus_tours, RELGROUPHASH_PLAYER)
ENDPROC

/// PURPOSE:
///    Requests path nodes if needed
PROC REQUEST_NODES_THIS_FRAME_IF_NEEDED()
	IF bPathNodesRequested
		REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestMin.X, vPathNodeRequestMin.Y, vPathNodeRequestMax.X, vPathNodeRequestMax.Y)
	ENDIF
ENDPROC

/// PURPOSE:
///    Init path nodes
/// PARAMS:
///    vPickupPosition - where the bus picks up the player from
///    vDestinationPosition - where the bus needs to path to
///    vPathNodeMin - the calculated minimum position we are loading paths
///    vPathNodeMax - the calculated maximum position we are loading paths
///    poiNum - the index for the point in the tour you want
///    iExtraDist - used to add leeyway area around the position before the request
PROC INIT_PATH_NODES_IN_AREA_FOR_TOUR_BUS(VECTOR vPickupPosition, VECTOR vDestinationPosition, VECTOR &vPathNodeMin, VECTOR &vPathNodeMax, INT iExtraDist = 500)
	IF NOT bPathNodesRequested
		// Set the area's X values
		IF vDestinationPosition.X <= vPickupPosition.X
			vPathNodeMin.X = vDestinationPosition.X
			vPathNodeMax.X = vPickupPosition.X
		ELSE
			vPathNodeMin.X = vPickupPosition.X
			vPathNodeMax.X = vDestinationPosition.X
		ENDIF
		
		// Set the area's X values
		IF vDestinationPosition.Y <= vPickupPosition.Y
			vPathNodeMin.Y = vDestinationPosition.Y
			vPathNodeMax.Y = vPickupPosition.Y
		ELSE
			vPathNodeMin.Y = vPickupPosition.Y
			vPathNodeMax.Y = vDestinationPosition.Y
		ENDIF
		
		// Extra leeway around the positions
		vPathNodeMin -= << iExtraDist, iExtraDist, iExtraDist >>
		vPathNodeMax += << iExtraDist, iExtraDist, iExtraDist >>
		
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Nodes init for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
		bPathNodesRequested = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Bus Tour cleanup
PROC missionCleanup()	
	
	// Shutdown PC dynamic scripted control schemes.
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	// Restore weather
	CLEAR_WEATHER_TYPE_PERSIST()
	
	// Restore gameplay cameras
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	ENDIF
	
	// Remove script cams
	IF DOES_CAM_EXIST(Cam_1)
		SET_CAM_ACTIVE(Cam_1, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(Cam_1)
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	
	IF DOES_CAM_EXIST(Cam_2)
		DESTROY_CAM(Cam_2)
	ENDIF
	
	IF Runbus_toursStage >= bus_tours_tour_progress
		SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(StoredCamBusCinematic)
	ENDIF
		
	DISPLAY_RADAR(TRUE)
		
	// Restore player control
	IF Runbus_toursStage >= bus_tours_player_in_bus
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	// Force delete in debug
	#IF IS_DEBUG_BUILD
		IF can_cleanup_script_for_debug
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " Cleaned up script for debug...")
			IF DOES_ENTITY_EXIST(bus_driver)
				DELETE_PED(bus_driver)
			ENDIF
			IF DOES_ENTITY_EXIST(tour_guide)
				DELETE_PED(tour_guide)
			ENDIF
		ENDIF
	#ENDIF
		
	// Release bus driver
	IF DOES_ENTITY_EXIST(bus_driver)
		IF NOT IS_PED_INJURED(bus_driver)
			SET_PED_CONFIG_FLAG(bus_driver, PCF_AICanDrivePlayerAsRearPassenger, FALSE)
			SET_PED_CAN_BE_DRAGGED_OUT(bus_driver, TRUE)	
			SET_PED_KEEP_TASK(bus_driver, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(bus_driver, FALSE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(bus_driver)
	ENDIF
	
	// Release tour guide
	IF DOES_ENTITY_EXIST(tour_guide)
		IF NOT IS_PED_INJURED(tour_guide)
			SET_PED_CAN_BE_TARGETTED(tour_guide, TRUE)
			SET_PED_KEEP_TASK(tour_guide, TRUE)
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " Releasing the guide...")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tour_guide, FALSE)			
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(tour_guide)
	ENDIF
		
	// Release crowd
	INT i
	REPEAT MAX_NUM_CROWD i
		IF DOES_ENTITY_EXIST(ped_crowd[i])
			IF IS_ENTITY_ALIVE(ped_crowd[i])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_crowd[i], FALSE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(ped_crowd[i])
		ENDIF
	ENDREPEAT
		
	// Release tour bus
	IF DOES_ENTITY_EXIST(veh_tour_bus)
		SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh_tour_bus, TRUE)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_tour_bus)
	ENDIF
	
	//Set the cinematic view back on. 
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " Cleaned up...")
		
	// Random event specifics
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		RESET_RANDOM_EVENT_LAUNCH_TIMER()			
	ELSE
		Mission_Over(g_iRECandidateID)
	ENDIF
	
	//Make sure we turn back on the exposure tweak, in case we are in FP mode when we end.
	SET_EXPOSURETWEAK(TRUE)
	
	RANDOM_EVENT_OVER()
	WAIT(0)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Loads models, anims, etc for Bus Tour
PROC loadAssets()
	
	IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS() 
		missionCleanup()
	ELSE
		MODEL_NAMES modelTourguide = A_F_M_BevHills_02
		MODEL_NAMES modelBusDriver = S_M_M_GENTRANSPORT
		
		// Asset requests
		REQUEST_MODEL(modelTourguide)
		REQUEST_MODEL(modelBusDriver)
		REQUEST_ANIM_DICT(animDict)
		REQUEST_MODEL(TOURBUS)
		REQUEST_ADDITIONAL_TEXT("BUSTOUR", ODDJOB_TEXT_SLOT)

		// Everything has loaded!
		IF HAS_MODEL_LOADED(modelTourguide)
		AND HAS_MODEL_LOADED(modelBusDriver)
		AND HAS_ANIM_DICT_LOADED(animDict)
		AND HAS_MODEL_LOADED(TOURBUS)
		AND HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)

			REGISTER_SCRIPT_WITH_AUDIO()

			GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vInput, ReturnedCarVec, ReturnedCarHeading)
			
			// Setup vehicle
			veh_tour_bus = CREATE_VEHICLE(TOURBUS, poiEnd.vPosition, poiEnd.fSkipHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(veh_tour_bus)
			SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_CAN_CLIMB_ON_ENTITY(veh_tour_bus, FALSE)
			SET_VEH_RADIO_STATION(veh_tour_bus, "OFF")
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh_tour_bus, FALSE)
			SET_VEHICLE_EXTRA(veh_tour_bus, 2, TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(TOURBUS, TRUE)
			SET_VEHICLE_LOD_MULTIPLIER(veh_tour_bus, 100.0)
			// Bus driver
			bus_driver = CREATE_PED_INSIDE_VEHICLE(veh_tour_bus, PEDTYPE_MISSION, modelBusDriver, VS_DRIVER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(bus_driver, TRUE)
			SET_PED_CONFIG_FLAG(bus_driver, PCF_AICanDrivePlayerAsRearPassenger, TRUE)
			SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(bus_driver, TRUE)
			
			IF DOES_ENTITY_EXIST(bus_driver)
				IF NOT IS_PED_INJURED(bus_driver)
					SET_PED_COMPONENT_VARIATION(bus_driver, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(bus_driver, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
				ENDIF
			ENDIF
			
			// Tour guide
			tour_guide = CREATE_PED(PEDTYPE_MISSION, modelTourguide, << 102.9893, 248.5367, 107.1759 >>, 329)	
			SET_AMBIENT_VOICE_NAME(tour_guide, "A_F_M_BEVHILLS_02_WHITE_FULL_02")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tour_guide, TRUE)
			SET_PED_CAN_BE_TARGETTED(tour_guide, FALSE)
			SET_PED_COMPONENT_VARIATION(tour_guide, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(tour_guide, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(tour_guide, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(tour_guide, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			
			OPEN_SEQUENCE_TASK(seq2)
				TASK_PLAY_ANIM(NULL, animDict, "IDLE_A")
				TASK_PLAY_ANIM(NULL, animDict, "IDLE_B")
				TASK_PLAY_ANIM(NULL, animDict, "IDLE_C")
				SET_SEQUENCE_TO_REPEAT(seq2,REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq2)
			TASK_PERFORM_SEQUENCE(tour_guide, seq2)
			CLEAR_SEQUENCE_TASK(seq2)
			ADD_PED_FOR_DIALOGUE(BusTourConversation, 3, tour_guide, "TOURGUIDE")
			
			// Setup bus icon
			IF NOT DOES_BLIP_EXIST(blip_random_event)
				blip_random_event = ADD_BLIP_FOR_ENTITY(veh_tour_bus)
				SET_BLIP_SPRITE(blip_random_event, RADAR_TRACE_VINEWOOD_TOURS)
			ENDIF
			
			// Set ped relationships and update stage
			SETUP_RELATIONSHIPS()
			bus_toursStage = STAGE_RUN_BUS_TOURS
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - MODEL LOADED AND PEDS CREATED")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player's pressed the quit button for the tour
/// RETURNS:
///    TRUE if player has manually quitted tour, else FALSE
FUNC BOOL MANUAL_QUIT_OF_TOUR_CHECK()
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP) AND NOT endCutsceneStarted
		IF DOES_ENTITY_EXIST(veh_tour_bus)
			IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
						IF NOT IS_PED_INJURED(bus_driver)
							TASK_VEHICLE_TEMP_ACTION(bus_driver, veh_tour_bus, TEMPACT_BRAKE, 5000)
							bus_speed = GET_ENTITY_SPEED(veh_tour_bus)
							WHILE bus_speed > 5
								WAIT(0)
								IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
									bus_speed = GET_ENTITY_SPEED(veh_tour_bus)
								ENDIF
							ENDWHILE

							KILL_ANY_CONVERSATION()
							SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(StoredCamBusCinematic)
							SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
							STOP_AUDIO_SCENE("RE_BUS_TOUR_SCENE")
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh_tour_bus)//,"RE_BUS_TOUR_BUS_VEHICLE")
							IF DOES_CAM_EXIST(cam_FPS)
								DESTROY_CAM(cam_FPS)
								RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
							ENDIF
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///PURPOSE:
///   Tour guide speech when stood outside tour bus
PROC speech_on_street()

	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), tour_guide, <<40, 40, 5>>)
		counter_current_time_tour = GET_GAME_TIMER()
		IF (counter_current_time_tour - counter_time_started_tour) > 10000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_STREET", CONV_PRIORITY_AMBIENT_HIGH) //Random Line
			ENDIF
			counter_time_started_tour = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC


FUNC STRING GET_RANDOM_PASS_NAME(BOOL is_male)
	INT rn
	IF (is_male = FALSE)
		rn = GET_RANDOM_INT_IN_RANGE(0, 16)
		IF rn = 0
			return "A_F_M_DOWNTOWN_01_BLACK_FULL_01"
		ELIF rn = 1
			return "A_F_M_EASTSA_01_LATINO_FULL_01"
		ELIF rn = 2
			return "A_F_M_FATWHITE_01_WHITE_FULL_01"
		ELIF rn = 3
			return "A_F_M_KTOWN_02_KOREAN_FULL_01"
		ELIF rn = 4
			return "A_F_M_SOUCENT_02_BLACK_FULL_01"
		ELIF rn = 5
			return "A_F_M_TOURIST_01_WHITE_MINI_01"
		ELIF rn = 6
			return "A_F_O_KTOWN_01_KOREAN_FULL_01"
		ELIF rn = 7
			return "A_F_O_SOUCENT_01_BLACK_FULL_01"
		ELIF rn = 8
			return "A_F_Y_EASTSA_02_WHITE_FULL_01"
		ELIF rn = 9
			return "A_F_Y_HIKER_01_WHITE_FULL_01"
		ELIF rn = 10
			return "A_F_Y_HIKER_01_WHITE_MINI_01"
		ELIF rn = 11
			return "A_F_Y_HIPSTER_03_WHITE_FULL_01"
		ELIF rn = 12
			return "A_F_Y_TOURIST_01_BLACK_FULL_01"
		ELIF rn = 13
			return "A_F_Y_TOURIST_01_LATINO_FULL_01"
		ELIF rn = 14
			return "A_F_Y_TOURIST_01_WHITE_FULL_01"
		ELSE
			return "A_F_Y_TOURIST_02_WHITE_MINI_01"
		ENDIF
	ELSE
		rn = GET_RANDOM_INT_IN_RANGE(0, 33)
		IF rn = 0
			return "A_M_M_AFRIAMER_01_BLACK_FULL_01"
		ELIF rn = 1
			return "A_M_M_EASTSA_01_LATINO_FULL_01"
		ELIF rn = 2
			return "A_M_M_EASTSA_02_LATINO_FULL_01"
		ELIF rn = 3
			return "A_M_M_FATLATIN_01_LATINO_FULL_01"
		ELIF rn = 4
			return "A_M_M_GENFAT_01_LATINO_FULL_01"
		ELIF rn = 5
			return "A_M_M_POLYNESIAN_01_POLYNESIAN_FULL_01"
		ELIF rn = 6
			return "A_M_M_SALTON_01_WHITE_FULL_01"
		ELIF rn = 7
			return "A_M_M_SALTON_02_WHITE_FULL_01"
		ELIF rn = 8
			return "A_M_M_STLAT_02_LATINO_FULL_01"
		ELIF rn = 9
			return "A_M_M_TOURIST_01_WHITE_MINI_01"
		ELIF rn = 10
			return "A_M_O_GENSTREET_01_WHITE_FULL_01"
		ELIF rn = 11
			return "A_M_Y_BEACH_01_CHINESE_FULL_01"
		ELIF rn = 12
			return "A_M_Y_BEACH_01_CHINESE_MINI_01"
		ELIF rn = 13
			return "A_M_Y_BEACH_01_WHITE_FULL_01"
		ELIF rn = 14
			return "A_M_Y_BEACH_02_LATINO_FULL_01"
		ELIF rn = 15
			return "A_M_Y_BEACH_03_WHITE_FULL_01"
		ELIF rn = 16
			return "A_M_Y_BEACHVESP_01_CHINESE_FULL_01"
		ELIF rn = 17
			return "A_M_Y_BEACHVESP_01_CHINESE_MINI_01"
		ELIF rn = 18
			return "A_M_Y_BEACHVESP_01_WHITE_FULL_01"
		ELIF rn = 19
			return "A_M_Y_BEACHVESP_02_WHITE_FULL_01"
		ELIF rn = 20
			return "A_M_Y_BEACHVESP_02_WHITE_MINI_01"
		ELIF rn = 21
			return "A_M_Y_BEVHILLS_01_WHITE_FULL_01"
		ELIF rn = 22
			return "A_M_Y_BUSINESS_01_BLACK_FULL_01"
		ELIF rn = 23
			return "A_M_Y_BUSINESS_01_CHINESE_FULL_01"
		ELIF rn = 24
			return "A_M_Y_BUSINESS_02_BLACK_FULL_01"
		ELIF rn = 25
			return "A_M_Y_BUSINESS_02_WHITE_FULL_01"
		ELIF rn = 26
			return "A_M_Y_EASTSA_01_LATINO_FULL_01"
		ELIF rn = 27
			return "A_M_Y_GENSTREET_01_CHINESE_FULL_01"
		ELIF rn = 28
			return "A_M_Y_GENSTREET_01_WHITE_FULL_01"
		ELIF rn = 29
			return "A_M_Y_GENSTREET_02_BLACK_FULL_01"
		ELIF rn = 30
			return "A_M_Y_GENSTREET_02_LATINO_FULL_01"
		ELIF rn = 31
			return "A_M_Y_POLYNESIAN_01_POLYNESIAN_FULL_01"
		ELSE
			return "A_M_Y_STLAT_01_LATINO_FULL_01"
		ENDIF

	ENDIF
ENDFUNC
///PURPOSE:
///   Tour guide rambling speech during tour (not used for locations)


BOOL sayCrowdSpeech = FALSE
INT currentCrowdtalker = 0
INT crowdCount = 0
INT crowdTimer = 7500

PROC rambling_speech()
	
	iRambleLine[0] = "BUSTO_RAMB_1"
	iRambleLine[1] = "BUSTO_RAMB_2"
	iRambleLine[2] = "BUSTO_RAMB_3"
	iRambleLine[3] = "BUSTO_RAMB_4"
	iRambleLine[4] = "BUSTO_RAMB_5"
	iRambleLine[5] = "BUSTO_RAMB_6"
	iRambleLine[6] = "BUSTO_RAMB_7"
	iRambleLine[7] = "BUSTO_RAMB_8"
	iRambleLine[8] = "BUSTO_RAMB_9"
	iRambleLine[9] = "BUSTO_RAMB_10"
	iRambleLine[10] = "BUSTO_RAMB_11"
	iRambleLine[11] = "BUSTO_RAMB_12"
	iRambleLine[12] = "BUSTO_RAMB_13"
	iRambleLine[13] = "BUSTO_RAMB_14"
	
	
	
	// How big a pause before we play each line (some lines are played together and so have no pause inbetween them)
	INT iRambleLineTimer[24]
	iRambleLineTimer[0] = 12000 	// Vinewood is often referred to as the anal bleaching capital of the world.
	
	iRambleLineTimer[1] = 12000 	// I should remind you this is a no smoking bus, unless of course it's medical marijuana. Then you're alright.
	
	iRambleLineTimer[2] = 36000 	// For those of you wondering how you know me, I'll put you out of your misery.  
	iRambleLineTimer[3] = 0			// I was a member of the jury in the 1991 movie Last Will & Testament. They cut my line.  
	
	iRambleLineTimer[4] = 36000 	// Is everyone having fun? 
	iRambleLineTimer[5] = 0			// Count yourselves lucky, my prescription just got renewed this morning.  
	
	iRambleLineTimer[6] = 36000		// Just up ahead on the right is where Delancey Medua's septum fell out after a ketamine party.   
	
	iRambleLineTimer[7] = 36000		// No shouting at celebrities, now. Remember.
	iRambleLineTimer[8] = 0			// these people are rich and beautiful, and that makes them better than us.  
		
	iRambleLineTimer[9] = 36000 	// Keep those cameras out and fingers on the trigger - celebs are skinny and fast.
	
	iRambleLineTimer[10] = 36000 	// People, if you look to the left, you'll see the spot where a Filipino pizza boy claims that the rapper 
	iRambleLineTimer[11] = 0 		// Clay PG Jackson asked him to remove his shorts and deliver him an 'American Hot and Spicy.'
	
	iRambleLineTimer[12] = 36000 	// You'll recognize this stretch of road from those videos of young socialite 
	iRambleLineTimer[13] = 0 	 	// Jill Von Crastenburg with the Romanian shotput team.
	
	iRambleLineTimer[14] = 36000 	// Don't you just love living vicariously through people you've never met, and 
	iRambleLineTimer[15] = 0 		// who would have you arrested if you came within ten feet of them? 
	
	iRambleLineTimer[16] = 48000 	// I know many of you are from out of town, so please 
	iRambleLineTimer[17] = 0 	 	// refrain from pointing at and taking photos of ethnic minorities. They might be gang members.
	
	iRambleLineTimer[18] = 48000 	// The next corner is where actor Scott Stephens famously prolapsed after a failed party trick 
	iRambleLineTimer[19] = 0 	 	// with some Burmese balls.
	
	iRambleLineTimer[20] = 48000 	// Remember when Lazlow was arrested in 2009 for masturbating with an eggplant and
	iRambleLineTimer[21] = 0 	 	// shouting at women, "Who wants some of my hero parm?" That happened right here.  
	
	iRambleLineTimer[0] = iRambleLineTimer[0] //temp so we compile.
	STRING ambientLine
	
	SWITCH irambling_speech
		
		CASE 0
			counter_time_started_tour = GET_GAME_TIMER()
			irambling_speech = 1
		BREAK
	
		CASE 1
			counter_current_time_tour = GET_GAME_TIMER()
			IF (counter_current_time_tour - counter_time_started_tour) > 2000 
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					counter_time_started_tour = GET_GAME_TIMER()
					irambling_speech = 2
				ENDIF
			ENDIF
		BREAK

		CASE 2
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				DRAW_DEBUG_TEXT_2D("IS_ANY_CONVERSATION_ONGOING_OR_QUEUED", <<0.02, 0.2, 0>>)
			ENDIF
			
			IF sayCrowdSpeech
				counter_current_time_tour = GET_GAME_TIMER()
				IF (counter_current_time_tour - counter_time_started_tour) > crowdTimer//7500
					IF PED_CAN_TALK(ped_crowd[currentCrowdtalker])
						IF iCurrentPOI = 0
							ambientLine = "TOUR_ABOUT_TO_START"
							//ambientLine = "TOUR_LANDMARK"
						ELIF iCurrentPOI%2 = 0
							ambientLine = "TOUR_CHAT"
						ELSE
							ambientLine = "TOUR_LANDMARK"
						ENDIF
						DRAW_DEBUG_TEXT_2D("AMBIENT LINE", <<0.02, 0.3, 0>>)
						PRINTSTRING("Paul - PLAY_PED_AMBIENT_SPEECH_NATIVE ") PRINTSTRING(ambientLine)PRINTINT(currentCrowdtalker) PRINTNL()
						PLAY_PED_AMBIENT_SPEECH_NATIVE(ped_crowd[currentCrowdtalker], ambientLine, "SPEECH_PARAMS_FORCE")
						crowdCount++
						IF crowdCount = 3
							sayCrowdSpeech = FALSE
						ENDIF
						crowdTimer = GET_RANDOM_INT_IN_RANGE(9000, 14000)
						counter_time_started_tour = GET_GAME_TIMER()
					ELSE
						PRINTSTRING("Paul - CANNOT TALK ") PRINTINT(currentCrowdtalker) PRINTNL()
						
					ENDIF
					currentCrowdtalker++
					IF currentCrowdtalker = MAX_NUM_CROWD
						currentCrowdtalker = 0
					ENDIF
				ENDIF
			ELSE
				IF iCurrentRamble < iTotalLines
					counter_current_time_tour = GET_GAME_TIMER()
					IF (counter_current_time_tour - counter_time_started_tour) > 10000//iRambleLineTimer[iCurrentRamble]
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_RAMB", iRambleLine[iCurrentRamble], CONV_PRIORITY_AMBIENT_HIGH)
								iCurrentRamble++
								counter_time_started_tour = GET_GAME_TIMER()
								sayCrowdSpeech = TRUE
								crowdCount = 0
							ELSE
								DRAW_DEBUG_TEXT_2D("NOT PLAY_SINGLE_LINE_FROM_CONVERSATION", <<0.02, 0.3, 0>>)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					sayCrowdSpeech = TRUE //go back to crowd if we are out.
				ENDIF
			ENDIF
		BREAK		
	ENDSWITCH
ENDPROC

///PURPOSE:
///   Set reaction of ped that has been attacked by the player
PROC AGGRO_REACTION(PED_INDEX tourped)
	IF IS_ENTITY_ALIVE(tour_guide)
		IF NOT IS_PED_INJURED(tour_guide)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tourped, FALSE)
			CLEAR_PED_TASKS(tourped)
			TASK_SMART_FLEE_PED(tourped, PLAYER_PED_ID(), 100, -1)
			SET_PED_KEEP_TASK(tourped, TRUE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///   Detect the player attacking ped
FUNC BOOL HAS_PED_BEEN_AGGROED(PED_INDEX ped)

	IF HAS_PLAYER_AGGROED_PED(ped, aggroReason, lockOnTimer, iBitFieldDontCheck, bAggroed)
	OR IS_PED_RAGDOLL(ped)										
		AGGRO_REACTION(ped)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

///PURPOSE:
///   Make the crowd peds look at the location
PROC SET_ALL_PASSENGERS_LOOKING_AT_POI(VECTOR vLookAt)
	
	INT iTemp, iTempTime
	
	start_looking_time = GET_GAME_TIMER()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		iTempTime = GET_RANDOM_INT_IN_RANGE(6000, 16000)
		TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vLookAt, iTempTime)
	ENDIF
	
	REPEAT MAX_NUM_CROWD iTemp
		IF NOT IS_PED_INJURED(ped_crowd[iTemp])
			iTempTime = GET_RANDOM_INT_IN_RANGE(6000, 16000)
			//TASK_LOOK_AT_COORD(ped_crowd[iTemp], vLookAt, iTempTime, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			ped_delay_lookattime[iTemp] = GET_RANDOM_INT_IN_RANGE(0, 2000)
			ped_delay_looking[iTemp] = FALSE
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_ALL_PASSENGERS_LOOKING_AT_POI(VECTOR vLookAt)
	INT iTemp
	
	REPEAT MAX_NUM_CROWD iTemp
		IF ((GET_GAME_TIMER() - start_looking_time) > ped_delay_lookattime[iTemp]) AND ped_delay_looking[iTemp] = FALSE
			PRINTSTRING("BUSTOUR - ped looking ") PRINTINT(iTemp) PRINTSTRING(" ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
			IF NOT IS_ENTITY_DEAD(ped_crowd[iTemp])
				TASK_LOOK_AT_COORD(ped_crowd[iTemp], vLookAt, 20000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			ped_delay_looking[iTemp] = TRUE
		ENDIF
	ENDREPEAT
ENDPROC

///PURPOSE:
///   Focus on the POI when driving past a location
PROC look_at_poi(VECTOR poi_coord, INT poi_dwellTime)

	IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
		IF NOT DOES_CAM_EXIST(cam_FPS)
		AND NOT IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), poiEnd.vLookPosition, <<12,12,12>>)
			 	SET_GAMEPLAY_COORD_HINT(poi_coord, poi_dwellTime, DEFAULT_INTERP_TO_FROM_GAME, DEFAULT_INTERP_TO_FROM_GAME)
				SET_ALL_PASSENGERS_LOOKING_AT_POI(poi_coord)
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Hint cam is pointing at ", poi_coord)
				counter_time_started_tour = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(bus_driver)
			SET_DRIVE_TASK_CRUISE_SPEED(bus_driver, TOUR_SPEED_SLOW)
		ENDIF
		At_Point_Of_Interest = TRUE
	ENDIF
ENDPROC

///PURPOSE:
///   Makes the bus park up at the end of the tour
PROC end_drive_and_park()

	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : End Drive and Park!")
	
	IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
		IF NOT IS_PED_INJURED(bus_driver)
			CLEAR_PED_TASKS(bus_driver)
			OPEN_SEQUENCE_TASK(seq)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, veh_tour_bus, <<120.8878, 239.3244, 106.5131>>, TOUR_SPEED_SLOW, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 5, 15)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, veh_tour_bus, poiEnd.vPosition, TOUR_SPEED_SLOW, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 3, 15)
				//TASK_VEHICLE_PARK(NULL, veh_tour_bus, poiEnd.vPosition, 75, PARK_TYPE_PULL_OVER)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(bus_driver, seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///   Marks this event as complete
PROC REGISTER_AS_COMPLETE()
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : registering as complete")
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

///PURPOSE:
///    Handles the end cutscene
FUNC BOOL end_cutscene()
	
	FLOAT  ped_crowd_end_warp_heading[MAX_NUM_CROWD]
	VECTOR ped_crowd_end_warp[MAX_NUM_CROWD]
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		end_cutscene_stage = 3
	ENDIF
	
	endCutsceneStarted = TRUE
	
	SWITCH end_cutscene_stage

		CASE 0

			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : end_cutscene_stage = 0")
			IF DOES_CAM_EXIST(cam_FPS)
				DESTROY_CAM(cam_FPS)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_MINIGAME_IN_PROGRESS(TRUE)
			DISPLAY_RADAR(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			//Set the exposure tweak back in case we were in first person. 
			SET_EXPOSURETWEAK(TRUE)
			
			Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<93.8459, 251.8829, 110.4535>>, <<23.0883, 0.1010, -61.7699>>, 40, TRUE)
			Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<97.0995, 249.7653, 109.0195>>, <<-3.0000, 0.0000, -91.5944>>, 50, TRUE)
			SET_CAM_ACTIVE(Cam_1, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 17000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		
			CREATE_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_THANK", CONV_PRIORITY_AMBIENT_HIGH) //And that concludes our tour!
			
			SET_ENTITY_COORDS(veh_tour_bus, poiEnd.vPosition)
			SET_ENTITY_HEADING(veh_tour_bus, poiEnd.fSkipHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(veh_tour_bus)
			SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)

			IF NOT IS_PED_INJURED(bus_driver)
				CLEAR_PED_TASKS(bus_driver)
				SET_DRIVE_TASK_CRUISE_SPEED(bus_driver, 0)
				OPEN_SEQUENCE_TASK(seq)
					TASK_VEHICLE_TEMP_ACTION(NULL, veh_tour_bus, TEMPACT_BRAKE, 10000)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(bus_driver, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<101.4382, 250.0447, 107.2579>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 268.4057)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				
			At_Point_Of_Interest = TRUE
			counter_time_started_tour = GET_GAME_TIMER()
			end_cutscene_stage = 1
		BREAK

		CASE 1
			
			counter_current_time_tour = GET_GAME_TIMER()
			
			IF (counter_current_time_tour - counter_time_started_tour) > 3000
			
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : end_cutscene_stage = 1")
	
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<101.4382, 250.0447, 107.2579>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 268.4057)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				
				ped_crowd_end_warp[0] = << 105.1253, 247.7461, 107.1111 >>
				ped_crowd_end_warp[1] = << 109.2535, 247.9478, 107.0121 >>
				ped_crowd_end_warp[2] = << 110.3568, 249.2771, 107.0133 >>
				ped_crowd_end_warp[3] = << 104.8579, 251.0388, 107.1858 >>
				ped_crowd_end_warp[4] = << 105.4064, 248.0866, 107.1104 >>

				ped_crowd_end_warp_heading[0] = 269.6692
				ped_crowd_end_warp_heading[1] = 283.6581
				ped_crowd_end_warp_heading[2] = 278.7632
				ped_crowd_end_warp_heading[3] = 228.5865
				ped_crowd_end_warp_heading[4] = 263.4406

				REPEAT MAX_NUM_CROWD crowd_index
					IF DOES_ENTITY_EXIST(ped_crowd[crowd_index])
						IF NOT IS_PED_INJURED(ped_crowd[crowd_index])
							CLEAR_PED_TASKS_IMMEDIATELY(ped_crowd[crowd_index])
							SET_ENTITY_HEADING(ped_crowd[crowd_index], ped_crowd_end_warp_heading[crowd_index])
							SET_ENTITY_COORDS(ped_crowd[crowd_index], ped_crowd_end_warp[crowd_index])
							TASK_WANDER_STANDARD(ped_crowd[crowd_index])
						ENDIF
					ENDIF
				ENDREPEAT
				
				counter_time_started_tour = GET_GAME_TIMER()
				end_cutscene_stage = 2
			ENDIF
		BREAK
		
		CASE 2
			counter_current_time_tour = GET_GAME_TIMER()
			IF (counter_current_time_tour - counter_time_started_tour) > 13000
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : end_cutscene_stage = 2")
				end_cutscene_stage = 3
			ENDIF
		BREAK
		
		CASE 3
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Back to gameplay") 
			SET_CAM_ACTIVE(Cam_1, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT, FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			DESTROY_CAM(Cam_1)
			DESTROY_CAM(Cam_2)
			SET_MINIGAME_IN_PROGRESS(FALSE)
			DISPLAY_RADAR(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			counter_time_started_tour = 10000
			REGISTER_AS_COMPLETE()
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

///PURPOSE:
///    Handle tour guide dialogue based on location
PROC specific_speech()
	
	INT i
	INT iNextPOI = iCurrentPOI+1

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		SWITCH point_of_interest_speech
			
			CASE POI_CHECK_COORDS
				
				IF NOT bSuppressHelp
					IF poiTour[POI_FINAL_STOP-1].bVisited
					OR poiTour[POI_FINAL_STOP].bSkippedTo
						bSuppressHelp = TRUE
					ENDIF
				ENDIF
				
				REPEAT NUMBER_OF_POI i
				
					IF NOT poiTour[i].bVisited
					
						IF i = POI_FINAL_STOP 
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), poiTour[i].vPosition, <<30, 30 , 15>>)
								IF poiTour[POI_FINAL_STOP-1].bVisited
									poiTour[POI_FINAL_STOP].bVisited = TRUE
									counter_time_started_tour = GET_GAME_TIMER()
									point_of_interest_speech = ENDCUTSCENE_WARPS
									CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME (), " : Update stage - ENDCUTSCENE_WARPS")
								ENDIF
							ENDIF
						ELSE
							// Player has arrived at next destination
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), poiTour[i].vPosition, <<40, 40, 12>>)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
									
									// Look at point of interest
									look_at_poi(poiTour[i].vLookPosition, poiTour[i].iLookTime)
									iCurrentPOI = i
									CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Current POI is ", iCurrentPOI)
									
									// Release the path nodes ready to get setup for the next go to position
									IF bPathNodesRequested
										bPathNodesRequested = FALSE
										CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : bPathNodesRequested set to FALSE")
									ENDIF
									
									KILL_ANY_CONVERSATION()
									point_of_interest_speech = POI_PLAY_DIALOGUE
									CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : POI_CHECK_COORDS -> POI_PLAY_DIALOGUE")
								ELSE
									point_of_interest_speech = POI_CLEANUP
									CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : POI_CHECK_COORDS -> POI_CLEANUP")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE POI_PLAY_DIALOGUE

				look_at_poi(poiTour[iCurrentPOI].vLookPosition, poiTour[iCurrentPOI].iLookTime)
				counter_current_time_tour = GET_GAME_TIMER()
				
				// Set requesting the path nodes to the next drive position
				IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
					
					IF iNextPOI <= POI_FINAL_STOP
						INIT_PATH_NODES_IN_AREA_FOR_TOUR_BUS(GET_ENTITY_COORDS(veh_tour_bus), poiTour[iNextPOI].vPosition, vPathNodeRequestMin, vPathNodeRequestMax)
					ELSE
						CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Next POI is past final stop")
					ENDIF

					IF ARE_NODES_LOADED_FOR_AREA(vPathNodeRequestMin.X, vPathNodeRequestMin.Y, vPathNodeRequestMax.X, vPathNodeRequestMax.Y)
						IF (counter_current_time_tour - counter_time_started_tour) > 0
							IF CREATE_CONVERSATION(BusTourConversation, "BUSTOAU", poiTour[iCurrentPOI].sDialogue, CONV_PRIORITY_AMBIENT_HIGH)		
								
								IF iNextPOI <= POI_FINAL_STOP
									
									IF iNextPOI = POI_CITYHALL
									
										// Needed to ensure bus takes sensible route to City Hall
										SEQUENCE_INDEX driveSeq
										OPEN_SEQUENCE_TASK(driveSeq)
											TASK_VEHICLE_DRIVE_TO_COORD(NULL, veh_tour_bus, <<-100.4610, -206.1412, 44.4215>>, poiTour[iNextPOI].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 4, 15)
											TASK_VEHICLE_DRIVE_TO_COORD(NULL, veh_tour_bus, poiTour[iNextPOI].vPosition, poiTour[iNextPOI].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 4, 15)
										CLOSE_SEQUENCE_TASK(driveSeq)
										TASK_PERFORM_SEQUENCE(bus_driver, driveSeq)
										CLEAR_SEQUENCE_TASK(driveSeq)
									ELSE
										TASK_VEHICLE_DRIVE_TO_COORD(bus_driver, veh_tour_bus, poiTour[iNextPOI].vPosition, poiTour[iNextPOI].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 4, 15)
									ENDIF
									
									CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Drive task to POI ", iNextPOI, " at speed ", poiTour[iNextPOI].fSpeed)
								ENDIF
								
								counter_time_started_tour = GET_GAME_TIMER()
								poiTour[iCurrentPOI].bSkippedTo = TRUE
								poiTour[iCurrentPOI].bVisited = TRUE
								point_of_interest_speech = POI_CLEANUP
								CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : POI_PLAY_DIALOGUE -> POI_CLEANUP")
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Nodes not loaded for area, ", iNextPOI)
					ENDIF
				ENDIF
			BREAK
		
			CASE ENDCUTSCENE_WARPS
				IF end_cutscene()
					point_of_interest_speech = POI_CLEANUP
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : ENDCUTSCENE_WARPS -> POI_CLEANUP")
				ENDIF
			BREAK
			
			CASE POI_CLEANUP
				counter_current_time_tour = GET_GAME_TIMER()
				
				PROCESS_ALL_PASSENGERS_LOOKING_AT_POI(poiTour[iCurrentPOI].vLookPosition)
				
				IF (counter_current_time_tour - counter_time_started_tour) > 5000
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						At_Point_Of_Interest = FALSE
						counter_time_started_tour = GET_GAME_TIMER()
						
						IF IS_GAMEPLAY_HINT_ACTIVE()
							STOP_GAMEPLAY_HINT(TRUE)
							CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Kill Hint Cam 1")
						ENDIF
						
						IF poiTour[POI_FINAL_STOP].bVisited = TRUE
							point_of_interest_speech = POI_FINISH_TOUR
							CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : POI_CLEANUP -> POI_FINISH_TOUR")
						ELSE
							IF NOT IS_PED_INJURED(bus_driver)
								SET_DRIVE_TASK_CRUISE_SPEED(bus_driver, TOUR_SPEED_FAST)
							ENDIF
							point_of_interest_speech = POI_CHECK_COORDS
							CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : POI_CLEANUP -> POI_CHECK_COORDS")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE POI_FINISH_TOUR	
				missionCleanup()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

///PURPOSE:
///    Can the player afford the bus tour? ($40)
PROC SET_PED_TO_RANDOMLY_LOOK_AROUND()
	
	VECTOR vTemp
	INT iTemp
	
	IF NOT IS_PED_INJURED(ped_crowd[2])
		IF GET_SCRIPT_TASK_STATUS(ped_crowd[2], SCRIPT_TASK_LOOK_AT_COORD) = FINISHED_TASK
			IF GET_GAME_TIMER() > iLookAroundTimeStamp + 8000
				SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped_crowd[2], FALSE)
				vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped_crowd[2], <<GET_RANDOM_FLOAT_IN_RANGE(0,2), GET_RANDOM_FLOAT_IN_RANGE(0,2), 0.5>>)	
				iTemp = GET_RANDOM_INT_IN_RANGE(1000, 6000)
				TASK_LOOK_AT_COORD(ped_crowd[2], vTemp, iTemp, SLF_WHILE_NOT_IN_FOV|SLF_SLOW_TURN_RATE)
				iLookAroundTimeStamp = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Used for first-person camera
PROC SET_CAMERA_ROT_TO_SAME_AS_PED(PED_INDEX inPed, CAMERA_INDEX &inCam, VECTOR vAdditionalRot)
	
	FLOAT  fHead
	VECTOR vCamRot
	
	IF NOT IS_PED_INJURED(inPed)
		IF DOES_CAM_EXIST(inCam)
			fHead = GET_ENTITY_HEADING(inPed)
			vCamRot.x = 0.0
			vCamRot.y = 0.0
			vCamRot.z = fHead
			vCamRot += vAdditionalRot
			SET_CAM_ROT(inCam,<< vCamRot.x, vCamRot.y, vCamRot.z >>)
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Update the FPS camera
PROC runFirstPersonCam()
	
	FLOAT  fTemp		
	FLOAT  fClubCamFOV = 70.0
	FLOAT  fMinYChange = -20
	FLOAT  fMaxYChange = 42.0
	FLOAT  fMinXChange = -89.0
	FLOAT  fMaxXChange = 69.0
	FLOAT  fClubCamSpeed = 4.0
	VECTOR vCamOffset = <<0.2, 0.9, -0.15>>
	

	
	
	IF DOES_CAM_EXIST(cam_FPS)
	
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			fClubCamSpeed = 8.0
		ELSE
			fClubCamSpeed = 4.0
		ENDIF
	
		SET_PED_TO_RANDOMLY_LOOK_AROUND()
		INT left_stick_x, left_stick_y
		INT right_stick_x, right_stick_y	
	
		left_stick_x  = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 127)
		left_stick_y  = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 127)
		right_stick_x = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
		right_stick_y = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
		
		IF NOT IS_LOOK_INVERTED()
			left_stick_y *= -1
			right_stick_y *= -1
		ENDIF
		
		// Invert the vertical
		IF (right_stick_y > STICK_DEAD_ZONE)
		OR (right_stick_y < (STICK_DEAD_ZONE * -1))	
		OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // No deadzone using mouse
			fTemp = TO_FLOAT(right_stick_y)
			fTemp *= fTemp
			fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
			fTemp *= fClubCamSpeed					
			IF (right_stick_y < 0)
				fTemp *= -1.0
			ENDIF 	
			fClubCamChangeY += fTemp					
			IF (fClubCamChangeY < fMinYChange)
				fClubCamChangeY = fMinYChange	
			ENDIF
			IF (fClubCamChangeY > fMaxYChange)
				fClubCamChangeY = fMaxYChange	
			ENDIF					
		ENDIF
		
		IF (right_stick_x > STICK_DEAD_ZONE)
		OR (right_stick_x < (STICK_DEAD_ZONE * -1))
		OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // No deadzone using mouse
			fTemp = TO_FLOAT(right_stick_x)
			fTemp *= fTemp
			fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
			fTemp *= fClubCamSpeed					
			IF (right_stick_x > 0)
				fTemp *= -1.0
			ENDIF					
			fClubCamChangeX += fTemp																	
			IF (fClubCamChangeX < fMinXChange)
				fClubCamChangeX = fMinXChange	
			ENDIF
			IF (fClubCamChangeX > fMaxXChange)
				fClubCamChangeX = fMaxXChange	
			ENDIF	
		ENDIF

		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))
		OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // No deadzone using mouse
			fTemp = TO_FLOAT(left_stick_y)
			fTemp *= fTemp
			fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
			fTemp *= fClubCamSpeed					
			IF (left_stick_y < 0)
				fTemp *= -1.0
			ENDIF 	
			fClubCamChangeY += fTemp					
			IF (fClubCamChangeY < fMinYChange)
				fClubCamChangeY = fMinYChange	
			ENDIF
			IF (fClubCamChangeY > fMaxYChange)
				fClubCamChangeY = fMaxYChange	
			ENDIF					
		ENDIF
		
		IF (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // No deadzone using mouse
			fTemp = TO_FLOAT(left_stick_x)
			fTemp *= fTemp
			fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
			fTemp *= fClubCamSpeed					
			IF (left_stick_x > 0)
				fTemp *= -1.0
			ENDIF					
			fClubCamChangeX += fTemp																	
			IF (fClubCamChangeX < fMinXChange)
				fClubCamChangeX = fMinXChange	
			ENDIF
			IF (fClubCamChangeX > fMaxXChange)
				fClubCamChangeX = fMaxXChange	
			ENDIF	
		ENDIF
		
		SET_CAMERA_ROT_TO_SAME_AS_PED(PLAYER_PED_ID(), cam_FPS, <<fClubCamChangeY, 0.0, fClubCamChangeX>>)
		SET_CAM_FOV(cam_FPS, fClubCamFOV)
		ATTACH_CAM_TO_ENTITY(cam_FPS, PLAYER_PED_ID(), << vCamOffset.x, vCamOffset.y-1, vCamOffset.z+1 >>)	
	ELSE
		cam_FPS = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ATTACH_CAM_TO_ENTITY(cam_FPS, PLAYER_PED_ID(), << vCamOffset.x, vCamOffset.y-1, vCamOffset.z+1 >>)
		SET_CAM_NEAR_CLIP(cam_FPS, 0.01)
		fClubCamChangeX = -70.0
		fClubCamChangeY	= 3.0
		SET_CAMERA_ROT_TO_SAME_AS_PED(PLAYER_PED_ID(), cam_FPS, <<fClubCamChangeY, 0.0, fClubCamChangeX>>)
		SET_CAM_FOV(cam_FPS, fClubCamFOV)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE, 2000)	
	ENDIF
ENDPROC

///PURPOSE:
///    Activates the first-person mode
PROC FPS_CAM_ACTIVATE(BOOL bIgnoreInput=FALSE)
	
	IF NOT bIgnoreInput
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
			IF bFPSActive
				bFPSActive = FALSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				SET_EXPOSURETWEAK(TRUE)
			ELSE
				bFPSActive = TRUE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_EXPOSURETWEAK(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF bFPSActive
	AND NOT poiTour[POI_FINAL_STOP].bVisited
		IF IS_GAMEPLAY_HINT_ACTIVE()
			PRINTLN(GET_THIS_SCRIPT_NAME(), " - Kill hint cam 3")
			STOP_GAMEPLAY_HINT()
		ENDIF
		runFirstPersonCam()
	ELSE
		IF At_Point_Of_Interest
			IF DOES_CAM_EXIST(cam_FPS)
				DESTROY_CAM(cam_FPS)
				RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				look_at_poi(poiTour[iCurrentPOI].vLookPosition, poiTour[iCurrentPOI].iLookTime)
			ENDIF
		ELSE
			IF DOES_CAM_EXIST(cam_FPS)
				DESTROY_CAM(cam_FPS)
				RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Fade out and warp to the next location
PROC FADE_OUT_AND_WARP(VECTOR skipcoord, FLOAT skipheading)
	
	INT iNextPOI = iCurrentPOI+1
	START_AUDIO_SCENE("RE_BUS_TOUR_FADE_OUT_WORLD")
	
	DO_SCREEN_FADE_OUT(1000)
	WHILE NOT IS_SCREEN_FADED_OUT()
		
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		
		IF NOT IS_VEHICLE_DRIVEABLE(veh_tour_bus)
			missioncleanup()
		ENDIF
		
		IF IS_PED_INJURED(bus_driver)
			missioncleanup()
		ENDIF
		
		WAIT(0)
	ENDWHILE

	IF IS_GAMEPLAY_HINT_ACTIVE()
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Kill hint cam 2")
		STOP_GAMEPLAY_HINT(TRUE)
	ENDIF

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

	IF NOT IS_PED_INJURED(bus_driver)
		CLEAR_PED_TASKS(bus_driver)
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
		CLEAR_AREA_OF_VEHICLES(<<skipcoord.x, skipcoord.y, skipcoord.z>>, 5.0)
		SET_ENTITY_COORDS(veh_tour_bus, <<skipcoord.x, skipcoord.y, skipcoord.z>>)
		SET_ENTITY_HEADING(veh_tour_bus, skipheading)
		SET_VEHICLE_FORWARD_SPEED(veh_tour_bus, 0.0)
		TASK_VEHICLE_DRIVE_TO_COORD(bus_driver, veh_tour_bus, GET_ENTITY_COORDS(veh_tour_bus), 0.0, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 5, 15)
		WAIT_FOR_WORLD_TO_LOAD(skipcoord)
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
		SET_VEHICLE_ON_GROUND_PROPERLY(veh_tour_bus)
		SET_VEHICLE_ENGINE_ON(veh_tour_bus, TRUE, TRUE)
		IF iNextPOI < POI_FINAL_STOP
			IF NOT IS_PED_INJURED(bus_driver)
				TASK_VEHICLE_DRIVE_TO_COORD(bus_driver, veh_tour_bus, poiTour[iNextPOI].vPosition, poiTour[iNextPOI].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 5, 15)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(bus_driver)
				TASK_VEHICLE_DRIVE_TO_COORD(bus_driver, veh_tour_bus, poiEnd.vPosition, poiTour[iNextPOI].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 5, 15)
			ENDIF
		ENDIF
		SET_VEHICLE_FORWARD_SPEED(veh_tour_bus, TOUR_SPEED_NORM)
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_CINEMATIC)
	point_of_interest_speech = POI_CLEANUP

	// Maintain first-person view during warp 
	FPS_CAM_ACTIVATE(TRUE)
	
	// Fade back in
	STOP_AUDIO_SCENE("RE_BUS_TOUR_FADE_OUT_WORLD")
	DO_SCREEN_FADE_IN(1000)
	WHILE IS_SCREEN_FADING_IN()
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		FPS_CAM_ACTIVATE(TRUE)
		WAIT(0)
	ENDWHILE

	// Needed to avoid camera popping upon return from skip
	DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	FPS_CAM_ACTIVATE(TRUE)
ENDPROC
		
///PURPOSE:
///    Skip ahead to the next location
PROC skip_to_next_poi()
	
	INT iNextPOI = iCurrentPOI+1
	
	IF NOT bSuppressHelp
	AND IS_SCREEN_FADED_IN()
		DISPLAY_HELP_TEXT_THIS_FRAME("Tour_help", FALSE)
	ENDIF
	
	IF iNextPOI < 14
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN) OR forceSkip = TRUE
			IF IS_SCREEN_FADED_IN()
				IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
					IF NOT IS_PED_INJURED(bus_driver)
					
						IF NOT poiTour[iNextPOI].bSkippedTo AND NOT poiTour[iNextPOI].bVisited
							SETTIMERA(0)
							poiTour[iCurrentPOI].bVisited = TRUE
							PRINTSTRING("BUSTOUR kipping bus tour. Heading and POI: ") PRINTINT (iNextPOI) PRINTSTRING(" ") PRINTFLOAT(poiTour[iNextPOI].fSkipHeading) PRINTNL()
							FADE_OUT_AND_WARP(poiTour[iNextPOI].vSkipPosition, poiTour[iNextPOI].fSkipHeading)
							KILL_ANY_CONVERSATION()

							IF iNextPOI = POI_FINAL_STOP
								bSuppressHelp = TRUE
								end_drive_and_park()
							ENDIF
							
							poiTour[iNextPOI].bSkippedTo = TRUE
							iCurrentPOI = iNextPOI
						ENDIF
						forceSkip = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Is the grabbed ped suitable for riding the tour bus?
FUNC BOOL IS_PED_SUITABLE_MODEL(PED_INDEX tempPed)
	
	BOOL bReturn = TRUE

	IF GET_ENTITY_MODEL(tempPed) = A_M_M_TRAMP_01
	OR GET_ENTITY_MODEL(tempPed) = A_F_M_TRAMP_01
	OR GET_ENTITY_MODEL(tempPed) = A_M_O_TRAMP_01
	OR GET_ENTITY_MODEL(tempPed) = S_M_Y_COP_01
	OR GET_ENTITY_MODEL(tempPed) = S_M_Y_FIREMAN_01
	OR GET_ENTITY_MODEL(tempPed) = A_F_Y_Hiker_01
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn	
ENDFUNC

///PURPOSE:
///    Get a random ped to populate the bus
FUNC BOOL GRABBED_RANDOM_PED(PED_INDEX &crowd_id)
	
	crowd_id = NULL
	STRING crowd_name
	IF GET_CLOSEST_PED(vInput, 100, TRUE, FALSE, crowd_id, DEFAULT, DEFAULT, PEDTYPE_ANIMAL)
		IF NOT IS_PED_INJURED(crowd_id)
			IF IS_PED_SUITABLE_MODEL(crowd_id)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(crowd_id)
					
					SET_ENTITY_AS_MISSION_ENTITY(crowd_id)
					CLEAR_PED_TASKS(crowd_id)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(crowd_id, TRUE)
					SET_PED_NAME_DEBUG(crowd_id, "randompassenger")
					SET_PED_CONFIG_FLAG(crowd_id, PCF_PreventPedFromReactingToBeingJacked, TRUE)
					/*IF PED_CAN_TALK(crowd_id)
						PRINTSTRING("TALK!!!") PRINTNL()
					ELSE
						PRINTSTRING("NO TALK!!!") PRINTNL()
					ENDIF*/
					SWITCH crowd_index
	
						CASE 0
							IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								SET_PED_INTO_VEHICLE(crowd_id, veh_tour_bus, VS_EXTRA_LEFT_1)
								SET_PED_KEEP_TASK(crowd_id, TRUE)
							ENDIF
						BREAK
						
						CASE 1
							IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								SET_PED_INTO_VEHICLE(crowd_id, veh_tour_bus, VS_EXTRA_LEFT_3 )
								SET_PED_KEEP_TASK(crowd_id, TRUE)
							ENDIF
						BREAK
						
						CASE 2
							IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								SET_PED_INTO_VEHICLE(crowd_id, veh_tour_bus, VS_EXTRA_RIGHT_2 )
								SET_PED_KEEP_TASK(crowd_id, TRUE)
							ENDIF
						BREAK
						
						CASE 3
							IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								SET_PED_INTO_VEHICLE(crowd_id, veh_tour_bus, VS_EXTRA_RIGHT_3 )
								SET_PED_KEEP_TASK(crowd_id, TRUE)
							ENDIF
						BREAK
						
						CASE 4
							IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								SET_PED_INTO_VEHICLE(crowd_id, veh_tour_bus, VS_BACK_LEFT)
								SET_PED_KEEP_TASK(crowd_id, TRUE)
							ENDIF
						BREAK
					ENDSWITCH
					crowd_name = GET_RANDOM_PASS_NAME(IS_PED_MALE(crowd_id))
					SET_AMBIENT_VOICE_NAME(crowd_id, crowd_name)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

///PURPOSE:
///    Is the player getting into the tour bus?
FUNC BOOL IS_PLAYER_ON_THE_BUS()

	IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = veh_tour_bus
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Player is on the bus, really getting in..")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

///PURPOSE:
///    Can the player afford the bus tour? ($40)
FUNC BOOL DOES_PLAYER_HAVE_ENOUGH_MONEY()

	IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) >= iBusCost
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

///PURPOSE:
///    Gets Tourguide in bus, starts her conversation and lets script know she's in the bus
PROC MAKE_TOURGUIDE_ENTER_BUS()
	
	IF NOT IS_PED_INJURED(tour_guide)
		
		CLEAR_PED_TASKS(tour_guide)
		TASK_ENTER_VEHICLE(tour_guide, veh_tour_bus, 10000, VS_FRONT_RIGHT, PEDMOVE_RUN, ECF_USE_RIGHT_ENTRY)
		SET_PED_KEEP_TASK(tour_guide, TRUE)
		
		tourguide_get_in = TRUE
	ENDIF
ENDPROC

PROC SET_STAGE(RUN_bus_tours_STAGES stageToSet)
	eStageState = SS_INIT
	Runbus_toursStage = stageToSet
ENDPROC

/// PURPOSE:
///    Checks for and handles certain reasons to shut down the mission whilst the bus is waiting for the Player
///  RETURNS:
///    TRUE if mission reason has been triggered, else false
FUNC BOOL BUS_WAITS_FOR_PLAYER_CLEANUP_CHECKS()
	
	// Check if tour guide is aggroed (if not continue her normal speech)
	IF IS_ENTITY_ALIVE(tour_guide)
		IF NOT IS_PED_INJURED(tour_guide)
			IF HAS_PED_BEEN_AGGROED(tour_guide)	
				PLAY_PED_AMBIENT_SPEECH(tour_guide, "HIT_WOMAN")
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, tour guide aggroed")
				RETURN TRUE
			ELSE
				speech_on_street()
			ENDIF
		ELSE
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, tour guide injured")
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, tour guide dead")
		RETURN TRUE
	ENDIF

	// Check if driver has been aggroed
	IF NOT IS_PED_INJURED(bus_driver)
		IF HAS_PED_BEEN_AGGROED(bus_driver)
		OR NOT IS_PED_IN_ANY_VEHICLE(bus_driver)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), bus_driver)
		OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), bus_driver) 
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, driver aggroed")
			AGGRO_REACTION(tour_guide)
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, driver injured")
		AGGRO_REACTION(tour_guide)
		RETURN TRUE
	ENDIF
	
	// Check if tour bus has been damaged by the player
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(veh_tour_bus, PLAYER_PED_ID())
		
		IF IS_ENTITY_ALIVE(tour_guide)
			IF NOT IS_PED_INJURED(tour_guide)
				AGGRO_REACTION(tour_guide)
				PLAY_PED_AMBIENT_SPEECH(tour_guide, "GENERIC_SHOCKED_HIGH")
			ENDIF
		ENDIF

		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, bus damaged by player")
		missionCleanup()
	ENDIF
	
	// Check if it's closing time
	IF GET_CLOCK_HOURS() >= CLOSING_TIME
	OR GET_CLOCK_HOURS() <= OPENING_TIME
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF CREATE_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_TIME", CONV_PRIORITY_AMBIENT_HIGH)
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, closing time")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Check if player is far away from bus
	IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), veh_tour_bus, <<220.0, 220.0, 30.0>>)
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup reason, player far away from bus")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Waits for the player to start entering the bus and handles any mission cleanup conditions whilst waiting
PROC BUS_WAITS_FOR_PLAYER_TO_ENTER()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		// Mission fail checks
		IF BUS_WAITS_FOR_PLAYER_CLEANUP_CHECKS()
			SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh_tour_bus, FALSE)
			
			missionCleanup()
		ELSE
			// Ok to trigger random event?
			IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_EVENT)
				// Player is on-foot near the bus...
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), veh_tour_bus, <<5.0, 5.0, 5.0>>)
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TX_H02") // Taxi help
					
					// Player can afford the tour
					IF DOES_PLAYER_HAVE_ENOUGH_MONEY()
						
						// Unlock bus
						IF NOT bBusUnlocked
							SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_UNLOCKED)
							bBusUnlocked = TRUE
						ENDIF
						
						// "Hold triangle to enter the tour bus as a passenger.
					    //  The tour costs $40."
						DISPLAY_HELP_TEXT_THIS_FRAME("Enter_bus", FALSE)
						
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_ENTER)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), veh_tour_bus, -1, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE)
						ENDIF
						
						// Random event setup
						IF NOT DOES_BLIP_EXIST(veh_tour_bus_blip)
							
							// Replace bus icon
							IF DOES_BLIP_EXIST(blip_random_event)
								REMOVE_BLIP(blip_random_event)
							ENDIF
							
							// Setup blue blip for bus
							veh_tour_bus_blip = ADD_BLIP_FOR_ENTITY(veh_tour_bus)
							SET_BLIP_AS_FRIENDLY(veh_tour_bus_blip, TRUE)
							SET_BLIP_SCALE(veh_tour_bus_blip, RE_BLIP_SCALE)
							
							// Make tour guide look at the player
							IF NOT IS_PED_INJURED(tour_guide)
								TASK_LOOK_AT_ENTITY(tour_guide, PLAYER_PED_ID(), -1)
							ENDIF
							
							// Set random event as active
							IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
								IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
									missionCleanup()
								ELSE
									SET_RANDOM_EVENT_ACTIVE()
								ENDIF
							ENDIF
						ENDIF
						
						// Detect the player on the bus
						IF IS_PLAYER_ON_THE_BUS()	
							endCutsceneStarted = FALSE
							SET_STAGE(bus_tours_player_in_bus)
						ENDIF
					ELSE
						// No cash... no tour!
						DISPLAY_HELP_TEXT_THIS_FRAME("No_bus_money", FALSE)
						
						// Lock bus
						IF bBusUnlocked
							SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_LOCKED)
							bBusUnlocked = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		// Assign nearby peds to populate bus
		IF crowd_index < MAX_NUM_CROWD
			IF GRABBED_RANDOM_PED(ped_crowd[crowd_index])							
				//IF PED_CAN_TALK(ped_crowd[crowd_index])
				//	PRINTSTRING("Paul  - new can talk!") PRINTINT(crowd_index) PRINTNL()
				//ELSE
				//	PRINTSTRING("Paul  - NOT new can talk!") PRINTINT(crowd_index) PRINTNL()
				//ENDIF
				crowd_index++
				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Crowd_index = ", crowd_index)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets everything sorted for the bus to set off
BOOL enterSaid
INT enterTimer
BOOL playedFlash = FALSE
PROC PLAYER_IS_IN_BUS()

	IF NOT IS_PED_INJURED(tour_guide)
	AND NOT IS_PED_INJURED(bus_driver)
	
		SWITCH eStageState
		
			CASE SS_INIT
				SET_CINEMATIC_BUTTON_ACTIVE(FALSE)

				CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Init - PLAYER_IS_IN_BUS")
				START_AUDIO_SCENE("RE_BUS_TOUR_SCENE")
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(veh_tour_bus,"RE_BUS_TOUR_BUS_VEHICLE")

				SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				CLEAR_AREA_OF_PROJECTILES(poiEnd.vPosition, 30)
				SET_PED_CAN_BE_DRAGGED_OUT(bus_driver, FALSE)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_tour_bus, VS_BACK_RIGHT)
				
				SET_MINIGAME_IN_PROGRESS(TRUE)
				DISPLAY_RADAR(FALSE)
				
				Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<94.5693, 246.0525, 108.9809>>, <<-11.5369, -0.0000, -74.5120>>, 40, TRUE)
				Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<95.7144, 243.8854, 111.2910>>, <<-26.2154, -0.0000, -59.2407>>, 50, TRUE)
			
				SET_CAM_ACTIVE(Cam_1, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
					IF IS_ENTITY_AT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), veh_tour_bus, <<8.0, 8.0, 5.0>>)
						SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), 65)
						SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << 115.1136, 241.6893, 106.6493 >>)
					ENDIF
				ENDIF

				INIT_PC_SCRIPTED_CONTROLS("BusTours")
					
				IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
					INIT_PATH_NODES_IN_AREA_FOR_TOUR_BUS(GET_ENTITY_COORDS(veh_tour_bus), poiTour[0].vPosition, vPathNodeRequestMin, vPathNodeRequestMax)
					enterTimer = GET_GAME_TIMER()
					eStageState = SS_RUNNING
				ENDIF
				
				//Check to see if we are in FP mode when entering.
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					bFPSActive = TRUE
				ENDIF
				playedFlash = FALSE
			BREAK
			
			CASE SS_RUNNING
				
				IF NOT tourguide_get_in
					MAKE_TOURGUIDE_ENTER_BUS()
				ENDIF
				
				IF ((GET_GAME_TIMER() - enterTimer) > 5000) AND NOT enterSaid
					CREATE_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_GO", CONV_PRIORITY_AMBIENT_HIGH)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("Enter_bus")
						CLEAR_HELP()
					ENDIF
					enterSaid = TRUE
				ENDIF
				
				IF ((GET_GAME_TIMER() - enterTimer) > 6200)
					IF bFPSActive = TRUE
						IF playedFlash = FALSE
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE) 
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
							playedFlash = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
					AND IS_PED_SITTING_IN_VEHICLE(tour_guide, veh_tour_bus)
					AND IS_PED_SITTING_IN_VEHICLE(bus_driver, veh_tour_bus)
						eStageState = SS_CLEANUP
					ELIF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
						IF IS_VEHICLE_SEAT_FREE(veh_tour_bus, VS_FRONT_RIGHT)
							SET_PED_INTO_VEHICLE(tour_guide, veh_tour_bus, VS_FRONT_RIGHT)
						ENDIF

						eStageState = SS_CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE SS_CLEANUP
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleanup - PLAYER_IS_IN_BUS")
				#ENDIF
				
				TASK_VEHICLE_DRIVE_TO_COORD(bus_driver, veh_tour_bus, poiTour[0].vPosition, poiTour[0].fSpeed, DRIVINGSTYLE_NORMAL, TOURBUS, tourDrivingMode, 5, 15)			
				SET_CAM_ACTIVE(Cam_1, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_CAM(Cam_1)
				DESTROY_CAM(Cam_2)
				SET_MINIGAME_IN_PROGRESS(FALSE)
				DISPLAY_RADAR(TRUE)
				
				DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				current_player_enum = GET_CURRENT_PLAYER_PED_ENUM()
				DEBIT_BANK_ACCOUNT(current_player_enum, BAAC_UNLOGGED_SMALL_ACTION, iBusCost)

				StoredCamBusCinematic = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
				IF NOT bFPSActive
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_CINEMATIC)
				ELSE
					FPS_CAM_ACTIVATE(TRUE)
				ENDIF

				REMOVE_BLIP(veh_tour_bus_blip)
				Runbus_toursStage = bus_tours_tour_progress
			BREAK
		ENDSWITCH
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleaning up because tour guide or driver injured")
		#ENDIF
		missionCleanup()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the bus tour

PROC BUS_IS_TOURING()
	TEXT_LABEL_63 dtext
	dtext = "curiCurrentRambler "
	dtext	+= iCurrentRamble
	dtext += " "
	dtext += iCurrentPOI
	DRAW_DEBUG_TEXT_2D(dtext, <<0.02, 0.5, 0>>)
				
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	
	PRINTSTRING("DISABLE!") PRINTNL()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)

	IF NOT IS_PED_INJURED(tour_guide)
	AND NOT IS_PED_INJURED(bus_driver)
		
		IF sayRambleBeforeCleanup
			//Ready to cleanup, say a final rambling line.
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF iCurrentRamble < iTotalLines
					PLAY_SINGLE_LINE_FROM_CONVERSATION(BusTourConversation, "BUSTOAU", "BUSTO_RAMB", iRambleLine[iCurrentRamble], CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				missionCleanup()
			ENDIF
		ELIF NOT MANUAL_QUIT_OF_TOUR_CHECK()
			
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)

			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
				AND (iCurrentPOI+1) < POI_FINAL_STOP
					missionCleanup()
				ENDIF
			ENDIF
			IF NOT At_Point_Of_Interest
				IF TIMERA() > 12000
					// Ramble restrictions - to try and pace dialogue
					// and prevent lines getting splitm
					//IF (iCurrentPOI < 1	AND iCurrentRamble < 2)	// Chateau 
					//OR iCurrentPOI > 6
						rambling_speech()
				//	/ENDIF
				ENDIF
			ELSE
				DRAW_DEBUG_TEXT_2D("At_Point_Of_Interest TRUE", <<0.02, 0.1, 0>>)
				
				//SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, dancer, ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
			ENDIF
			
			specific_speech()
			skip_to_next_poi()
			FPS_CAM_ACTIVATE()
			
			//Check to see if we are stuck.
			IF IS_VEHICLE_STUCK(veh_tour_bus)
				forceSkip = TRUE
			ENDIF
			//PRINTSTRING("PAul!!!") PRINTNL()
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(veh_tour_bus) 
		ELSE
			CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleaning up because player manually quit!")
			CREATE_CONVERSATION_FROM_SPECIFIC_LINE(BusTourConversation, "BUSTOAU", "BUSTO_THANK", "BUSTO_THANK_7", CONV_PRIORITY_AMBIENT_HIGH)
			SET_VEHICLE_DOORS_LOCKED(veh_tour_bus, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh_tour_bus, FALSE)
			
			//missionCleanup()
			
			sayRambleBeforeCleanup = TRUE
			
		ENDIF
	ELSE
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Cleaning up because tour guide or driver injured!")
		missionCleanup()
	ENDIF
ENDPROC

/// PURPOSE:
///    Suppress all crimes that could be called against the AI driver
PROC SUPPRESS_ALL_CRIMES_THIS_FRAME()
	SUPPRESS_WITNESSES_CALLING_POLICE_THIS_FRAME(PLAYER_ID()) 
    SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_DRIVE_AGAINST_TRAFFIC) 
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RECKLESS_DRIVING) 
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RUNOVER_PED) 
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RUN_REDLIGHT) 
    SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SPEEDING) 
ENDPROC

 /// PURPOSE:
///    Suppress all code random events that could effect the AI bus driver
PROC SUPPRESS_ALL_RANDOM_EVENTS_THIS_FRAME()
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_STEAL_VEHICLE, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_JAY_WALK_LIGHTS, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_FLEE_SPAWNED, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_VEHICLE_DRIVING_FAST, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_DRIVER_SLOW, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_DRIVER_RECKLESS, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_DRIVER_PRO, TRUE)
	SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_PURSUE_WHEN_HIT_BY_CAR, TRUE)
ENDPROC   
/// PURPOSE:
///    Ensures weather never goes to rain/storm during bus tour
PROC MANAGE_WEATHER()

	IF IS_NEXT_WEATHER_TYPE("RAIN")
	OR IS_NEXT_WEATHER_TYPE("THUNDER")
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : Bad weather detected - forcing weather to SUNNY")
		SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST", 10000)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages the vehicle population levels to try and help the tour bus along its journey
PROC MANAGE_VEHICLE_DENSITY()

	SWITCH iCurrentPOI
		
		CASE POI_CHATEAU
			// Help get away from initial intersection
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
		BREAK
		
		CASE POI_VIPER
		CASE POI_MOVIESTAR1
		CASE POI_MOVIESTAR2
		CASE POI_MOVIESTAR3
		CASE POI_RICHMOND
		CASE POI_WEAZEL
		CASE POI_STUDIO
		CASE POI_PORTOLA
		CASE POI_EPSILON
		CASE POI_CITYHALL
		CASE POI_CHINESE
		CASE POI_FINAL_STOP
			// General reduction to help navigation
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    State machine for player on the bus tour
PROC run_bus_tours()
	
	MANAGE_WEATHER()
	REQUEST_NODES_THIS_FRAME_IF_NEEDED()
	SUPPRESS_ALL_CRIMES_THIS_FRAME()
	SUPPRESS_ALL_RANDOM_EVENTS_THIS_FRAME()
	
	SWITCH Runbus_toursStage
		
		CASE bus_tours_enter_as_passenger
			BUS_WAITS_FOR_PLAYER_TO_ENTER()
		BREAK

		CASE bus_tours_player_in_bus
			PLAYER_IS_IN_BUS()
		BREAK

		CASE bus_tours_tour_progress
			MANAGE_VEHICLE_DENSITY()
			BUS_IS_TOURING()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Adds a point of interest
PROC ADD_POI_TO_TOUR(POI_DATA poi)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " : ADD_POI_TO_TOUR - iCurrentPOI = ", iCurrentPOI)
	#ENDIF
	poiTour[iCurrentPOI] = poi
	iCurrentPOI++
ENDPROC

/// PURPOSE:
///    Initialises all of the POI data
PROC INITIALISE_POI_DATA()
	
	POI_DATA poiChateau, poiViperRoom, poiTequiLaLa
	POI_DATA poiMovieStar1, poiMovieStar2, poiMovieStar3
	POI_DATA poiRichmond, poiWeazelTheatre, poiStudio
	POI_DATA poiPortolaDrive, poiEpsilonism, poiCityHall, poiChineseTheatre

	#IF IS_DEBUG_BUILD
		poiChateau.name				= "poiChateau"
		poiViperRoom.name 			= "poiViperRoom"
		poiTequiLaLa.name 			= "poiTequiLaLa"
		poiMovieStar1.name			= "poiMovieStar1"
		poiMovieStar2.name			= "poiMovieStar2"
		poiMovieStar3.name			= "poiMovieStar3"
		poiRichmond.name 			= "poiRichmond"
		poiWeazelTheatre.name 		= "poiWeazelTheatre"
		poiStudio.name				= "poiStudio"
		poiPortolaDrive.name 		= "poiPortolaDrive"
		poiEpsilonism.name			= "poiEpsilonism"
		poiCityHall.name 			= "poiCityHall"
		poiChineseTheatre.name 		= "poiChineseTheatre"
	#ENDIF							
	
	// Chateau (0)
	poiChateau.vPosition			= << -24.8500, 263.6119, 106.5301 >>
	poiChateau.vLookPosition		= << -62.9588, 335.2273, 136.0550 >>
	poiChateau.iLookTime		    = 10000
	poiChateau.fSpeed		    	= TOUR_SPEED_NORM-1
	poiChateau.vSkipPosition		= << -0.9281, 266.8539, 108.0699 >> 
	poiChateau.fSkipHeading			= 82
	poiChateau.sDialogue			= "BUSTO_P1"
	
	// Dungeon Crawler (1)
	poiViperRoom.vPosition			= << -214.9, 267.4, 91.5 >>
	poiViperRoom.vLookPosition		= << -253.7246, 247.9038, 95.2063 >>
	poiViperRoom.iLookTime		    = 9000
	poiViperRoom.fSpeed		    	= TOUR_SPEED_NORM-1
	poiViperRoom.vSkipPosition		= << -188.67, 265.43, 92.15 >>
	poiViperRoom.fSkipHeading		= 81.79
	poiViperRoom.sDialogue			= "BUSTO_P5"
	
	// Tequila-la (2)
	poiTequiLaLa.vPosition			= << -522.1094, 260.8653, 82.0630 >>
	poiTequiLaLa.vLookPosition		= << -549.5739, 272.7824, 82.9753 >>
	poiTequiLaLa.iLookTime		    = 9000
	poiTequiLaLa.fSpeed		    	= TOUR_SPEED_NORM
	poiTequiLaLa.vSkipPosition		= << -435.0476, 249.8727, 82.0866 >>
	poiTequiLaLa.fSkipHeading		= 85
	poiTequiLaLa.sDialogue			= "BUSTO_P6"
	
	// Movie Star 1 - Mark Fostenburg (3)
	poiMovieStar1.vPosition			= << -575.5643, 512.5427, 105.0293 >>
	poiMovieStar1.vLookPosition		= << -586.4406, 529.1188, 108.8539 >>
	poiMovieStar1.iLookTime		    = 9000
	poiMovieStar1.fSpeed		    = TOUR_SPEED_NORM+2
	poiMovieStar1.vSkipPosition		= << -552.3732, 481.7021, 102.1826 >>
	poiMovieStar1.fSkipHeading		= 25
	poiMovieStar1.sDialogue			= "BUSTO_P7"
	
	// Movie Star 2 - The Craze (4)
	poiMovieStar2.vPosition			= << -732.2224, 474.0897, 105.0424 >>
	poiMovieStar2.vLookPosition		= << -728.4921, 448.0321, 107.9036 >>
	poiMovieStar2.iLookTime		    = 9000
	poiMovieStar2.fSpeed		    = TOUR_SPEED_NORM+3
	poiMovieStar2.vSkipPosition		= << -688.5469, 490.5489, 108.7840 >>
	poiMovieStar2.fSkipHeading		= 105
	poiMovieStar2.sDialogue			= "BUSTO_P8"
	
	// Movie Star 3 - Martha Term (5)
	poiMovieStar3.vPosition			= << -1037.9698, 468.9468, 76.7167 >>
	poiMovieStar3.vLookPosition		= << -1065.7620, 469.4401, 80.3204 >>
	poiMovieStar3.iLookTime		    = 9000
	poiMovieStar3.fSpeed		    = TOUR_SPEED_NORM+2
	poiMovieStar3.vSkipPosition		= << -973.6752, 499.3758, 78.8246 >>
	poiMovieStar3.fSkipHeading		= 47
	poiMovieStar3.sDialogue			= "BUSTO_P9"
	
	// Richmond Hotel (6)
	poiRichmond.vPosition			= << -1231.5812, 239.0140, 64.8339 >>
	poiRichmond.vLookPosition		= << -1309.7784, 261.4181, 65.8700 >>
	poiRichmond.iLookTime		    = 9000
	poiRichmond.fSpeed		        = TOUR_SPEED_NORM+4
	poiRichmond.vSkipPosition		= << -1168.4482, 257.0845, 66.3477 >>
	poiRichmond.fSkipHeading		= 105
	poiRichmond.sDialogue			= "BUSTO_P10"
	
	// Weazel Theatre (7)
	poiWeazelTheatre.vPosition		= << -1406.7286, -160.0764, 46.5244 >>
	poiWeazelTheatre.vLookPosition	= << -1421.4790, -193.5715, 50 >>
	poiWeazelTheatre.iLookTime	    = 9000
	poiWeazelTheatre.fSpeed		    = TOUR_SPEED_NORM+4
	poiWeazelTheatre.vSkipPosition	= << -1398.3905, -114.7998, 50.0280 >>
	poiWeazelTheatre.fSkipHeading	= 178
	poiWeazelTheatre.sDialogue		= "BUSTO_P11"
	
	// Movie Studio (8)
	poiStudio.vPosition				= << -1316.2052, -487.0228, 32.3407 >>
	poiStudio.vLookPosition			= << -1271.7498, -500.2502, 44.4638 >>
	poiStudio.iLookTime	    		= 9000
	poiStudio.fSpeed		        = TOUR_SPEED_FAST+4
	poiStudio.vSkipPosition			= << -1356.9116, -437.5389, 34.0665 >>
	poiStudio.fSkipHeading			= 215
	poiStudio.sDialogue				= "BUSTO_P3"
	
	// Portola Drive (9)
	poiPortolaDrive.vPosition		= << -723.1427, -239.0515, 36.0196 >>
	poiPortolaDrive.vLookPosition	= << -696.7581, -239.2147, 40.8147 >>
	poiPortolaDrive.iLookTime	    = 9000
	poiPortolaDrive.fSpeed		    = TOUR_SPEED_FAST+6
	poiPortolaDrive.vSkipPosition	= << -777.5432, -262.5703, 36.0082 >>
	poiPortolaDrive.fSkipHeading	= 290
	poiPortolaDrive.sDialogue		= "BUSTO_P2"
	
	// Epsilon Building (10)
	poiEpsilonism.vPosition			= << -690.7699, -25.7620, 36.9897 >>
	poiEpsilonism.vLookPosition		= << -695, 22,53 >>
	poiEpsilonism.iLookTime	    	= 9000
	poiEpsilonism.fSpeed		    = TOUR_SPEED_FAST+5
	poiEpsilonism.vSkipPosition		= << -696.6950, -40.7958, 36.8186 >> 
	poiEpsilonism.fSkipHeading		= 27.9347
	poiEpsilonism.sDialogue			= "BUSTO_P14"
	
	// City Hall (11)
	poiCityHall.vPosition			= << 179.6443, -340.3767, 43.0448 >>
	poiCityHall.vLookPosition		= << 235.1390, -393.0182, 50.0242 >>
	poiCityHall.iLookTime	    	= 6000
	poiCityHall.fSpeed		        = TOUR_SPEED_FAST+5
	poiCityHall.vSkipPosition		= << 113.5266, -317.9667, 44.6514 >>
	poiCityHall.fSkipHeading		= 250
	poiCityHall.sDialogue			= "BUSTO_P13"
	
	// Chinese Theatre (12)
	poiChineseTheatre.vPosition		= << 288.2845, 166.6872, 103.3090 >>
	poiChineseTheatre.vLookPosition	= << 299.9585, 198.3281, 113.3522 >>
	poiChineseTheatre.iLookTime	    = 6000
	poiChineseTheatre.fSpeed		= TOUR_SPEED_FAST+3
	poiChineseTheatre.vSkipPosition	= << 344.1458, 148.4554, 102.1590 >>
	poiChineseTheatre.fSkipHeading	= 70
	poiChineseTheatre.sDialogue		= "BUSTO_P4"
	
	// End drop-off
	poiEnd.vPosition				= << 99.4291,  246.9366, 107.2005 >> //<< 97.8811, 245.7975, 107.3682 >>
	poiEnd.vLookPosition			= << 102.6186, 254.6904, 114.0255 >>
	poiEnd.iLookTime	    		= -1
	poiEnd.fSpeed		            = TOUR_SPEED_NORM
	poiEnd.vSkipPosition			= << 177.5471, 209.6570, 105.0543 >>
	poiEnd.fSkipHeading				= 70
	poiEnd.sDialogue				= "BUSTO_Px"
	
	// Add tour locations
	ADD_POI_TO_TOUR(poiChateau)
	ADD_POI_TO_TOUR(poiViperRoom)
	ADD_POI_TO_TOUR(poiTequiLaLa)
	ADD_POI_TO_TOUR(poiMovieStar1)
	ADD_POI_TO_TOUR(poiMovieStar2)
	ADD_POI_TO_TOUR(poiMovieStar3)
	ADD_POI_TO_TOUR(poiRichmond)
	ADD_POI_TO_TOUR(poiWeazelTheatre)
	ADD_POI_TO_TOUR(poiStudio)
	ADD_POI_TO_TOUR(poiPortolaDrive)
	ADD_POI_TO_TOUR(poiEpsilonism)
	ADD_POI_TO_TOUR(poiCityHall)
	ADD_POI_TO_TOUR(poiChineseTheatre)
	ADD_POI_TO_TOUR(poiEnd)

	// Reset index
	iCurrentPOI = 0
ENDPROC

/// PURPOSE:
///    Sets up debug widgets
PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD

		START_WIDGET_GROUP("re_bus_tours") 
		START_WIDGET_GROUP("Variation Selection Debug")
	        START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("bus_tours_player_in_bus")
				ADD_TO_WIDGET_COMBO("bus_tours_tour_progress")
	        STOP_WIDGET_COMBO("bus_tours_TYPE_VARIATIONS", iRunbus_toursStage) 

	        IF g_bHoldRandomEventForSelection
	            ADD_WIDGET_BOOL("Run mission with debug selection", bRunDebugSelection)
	        ENDIF
	    STOP_WIDGET_GROUP()
			
		ADD_WIDGET_BOOL("Toggle All Location Blips", bAddAllLocationBlips)
		ADD_WIDGET_BOOL("Toggle Active Location Blips", bAddActiveLocationBlips)
		STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC

/// PURPOSE:
///   Monitors debug widgets
PROC RUN_DEBUG()

	#IF IS_DEBUG_BUILD
		INT i
		IF bAddActiveLocationBlips
			IF NOT bRouteHasBeenAdded
				START_GPS_RACE_TRACK(HUD_COLOUR_YELLOW)
				//START_GPS_CUSTOM_ROUTE(HUD_COLOUR_YELLOW, TRUE, TRUE)
				REPEAT POI_FINAL_STOP i
					IF NOT DOES_BLIP_EXIST(poiTour[i].blip)
						poiTour[i].blip = ADD_BLIP_FOR_COORD(poiTour[i].vPosition)
						ADD_POINT_TO_GPS_RACE_TRACK(poiTour[i].vPosition)
						SET_BLIP_NAME_FROM_ASCII(poiTour[i].blip, poiTour[i].name)
					ENDIF
				ENDREPEAT
				SET_RACE_TRACK_RENDER(TRUE)
				bRouteHasBeenAdded = TRUE
			ENDIF
		ELSE
			IF bRouteHasBeenAdded
				REPEAT POI_FINAL_STOP i
					IF DOES_BLIP_EXIST(poiTour[i].blip)
						REMOVE_BLIP(poiTour[i].blip)
					ENDIF
				ENDREPEAT
				CLEAR_GPS_RACE_TRACK()
				bRouteHasBeenAdded = FALSE
			ENDIF
		ENDIF
	
		IF bAddAllLocationBlips
			REPEAT POI_FINAL_STOP i
				IF NOT DOES_BLIP_EXIST(poiTour[i].blip)
					poiTour[i].blip = ADD_BLIP_FOR_COORD(poiTour[i].vPosition)
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT POI_FINAL_STOP i
				IF DOES_BLIP_EXIST(poiTour[i].blip)
					REMOVE_BLIP(poiTour[i].blip)
				ENDIF
			ENDREPEAT
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			REGISTER_AS_COMPLETE()
			missionCleanup()
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			missionCleanup()
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
				IF Runbus_toursStage = bus_tours_tour_progress
					iCurrentPOI = NUMBER_OF_POI
					FADE_OUT_AND_WARP(poiEnd.vSkipPosition, poiEnd.fSkipHeading)
					KILL_ANY_CONVERSATION()
					bSuppressHelp = TRUE
					point_of_interest_speech = ENDCUTSCENE_WARPS
				ENDIF
			ENDIF
		ENDIF

		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
				IF Runbus_toursStage = bus_tours_tour_progress
					iCurrentPOI = NUMBER_OF_POI
					FADE_OUT_AND_WARP(poiEnd.vSkipPosition, poiEnd.fSkipHeading)
					KILL_ANY_CONVERSATION()
					bSuppressHelp = TRUE
					end_drive_and_park()
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///   Is the bus tour open?
FUNC BOOL BUS_TOUR_WITHIN_OPERATING_HOURS()

	IF GET_CLOCK_HOURS() > CLOSING_TIME
	OR GET_CLOCK_HOURS() < OPENING_TIME
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

// Mission Script -----------------------------------------
SCRIPT (coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Script launch at z", vInput)
		
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		
		IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_tour_bus)
					
					// Warp player out of bus on force cleanup
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_tour_bus, <<2.5, 1.5, 0>>))
					SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(veh_tour_bus))
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
					
					// Remove audio
					STOP_AUDIO_SCENE("RE_BUS_TOUR_SCENE")
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh_tour_bus)
					
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Player warped out of bus on HAS_FORCE_CLEANUP_OCCURRED")
				ENDIF
			ENDIF
		ENDIF

		// Run cleanup
		missionCleanup()
	ENDIF

	// Shut down bus tours when outside of operating hours
	IF NOT BUS_TOUR_WITHIN_OPERATING_HOURS()
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Outside of open hours.")
		TERMINATE_THIS_THREAD()
	ENDIF

	// Can we launch?
	IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_BUSTOUR, 0, TRUE)
		LAUNCH_RANDOM_EVENT()
	ELSE
		CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Cannot launch.")
		TERMINATE_THIS_THREAD()
	ENDIF

	// Initialisation
	INITIALISE_POI_DATA()
	SETUP_DEBUG()
			
	CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Initialised and going into main update loop.")
	
	// Mission Loop -------------------------------------------
	WHILE TRUE
		
		RUN_DEBUG()
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF

		SWITCH active_stage
			
			CASE 0
				
				// Within activation range of the bus tour
				IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()

					IF BUS_TOUR_WITHIN_OPERATING_HOURS()
					
						SWITCH bus_toursStage
			
							// Load resources for Bus Tour
							CASE STAGE_INIT_BUS_TOURS
								loadAssets()
							BREAK
				
							CASE STAGE_RUN_BUS_TOURS
								
								// Bus tour vehicle is ok
								IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
								
									// Check for injuring mission peds
									IF IS_PED_INJURED(bus_driver) OR IS_PED_INJURED(tour_guide)
										CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Bus driver or tour guide injured, terminating...")
										missionCleanup()
									ELSE
										active_stage = 1
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Outside of open hours.")
						missionCleanup()
					ENDIF
				ELSE
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Left world point range.")
					missionCleanup()
				ENDIF
			BREAK
			
			CASE 1
				IF IS_VEHICLE_DRIVEABLE(veh_tour_bus)
					run_bus_tours()
				ELSE
					CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - Bus is no longer drivable, terminating...")
					missionCleanup()
				ENDIF
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
