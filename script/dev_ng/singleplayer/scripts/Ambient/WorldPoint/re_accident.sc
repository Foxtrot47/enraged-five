//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "script_blips.sch"
USING "dialogue_public.sch"
USING "Ambient_Speech.sch"
USING "cutscene_public.sch"
USING "commands_event.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"
USING "load_queue_public.sch"

CONST_INT MAX_TRAIL_POINTS 14
USING "gas_trails.sch"

// Variables ----------------------------------------------//
ENUM con_accident_STAGES
	STAGE_CREATE_COPSNCRIMS,
	STAGE_RUN_CON_ACCIDENT,
	STAGE_CLEAN_UP
ENDENUM
con_accident_STAGES con_accidentStage = STAGE_CREATE_COPSNCRIMS

ENUM RUN_con_accident_STAGES
	CON_ACCIDENT_FALLING_PIPES,
	CON_ACCIDENT_FALLING_PIPES_2
ENDENUM
RUN_con_accident_STAGES Runcon_accidentStage = CON_ACCIDENT_FALLING_PIPES

ENUM start_cut_enum
	STARTCUT_SETUP,
	STARTCUT_CAM1,
	STARTCUT_CAM2,
	STARTCUT_CAM3,
	STARTCUT_CAM4,
	STARTCUT_CAM5,
	STARTCUT_CAM6,
	STARTCUT_CAM7,
	STARTCUT_CLEANUP
ENDENUM
start_cut_enum start_cut_stage = STARTCUT_SETUP

#IF IS_DEBUG_BUILD //WIDGETS
	WIDGET_GROUP_ID debugWidget
//	INT iRuncon_accidentStage
//	BOOL bRunDebugSelection
#ENDIF

CONST_INT NUMBER_OF_FALLING_PROPS 11

INT time_to_script_fire = 55000//, petrol_time = 250
VECTOR vInput//, vReturnedCarCoord//, trapped_peds_coords, new_coord1, new_coord2
//VECTOR accident_acivation_range = << 100, 100, 100 >>
VECTOR vAccidentProneVehicleStart
VECTOR activate_range = << 30, 30, 5 >>
VECTOR vOffsetCheck

PED_INDEX pedWorker
PED_INDEX pedCraneWorker
VEHICLE_INDEX vehTruck
VEHICLE_INDEX vehBulldozer
VEHICLE_INDEX vehicleCandidate//, current_model, random_front_bumper, random_back_bumper
VEHICLE_INDEX vehImpound
//FLOAT vReturnedCarHeading//, fRandomGrowthRate, fRandomStartSize, fRandomEndSize

BLIP_INDEX blipWorker
BLIP_INDEX blipTruck
BLIP_INDEX blipBulldozer
BLIP_INDEX blipRandomEvent

OBJECT_INDEX objDangerProp[NUMBER_OF_FALLING_PROPS]
OBJECT_INDEX objGenerator
OBJECT_INDEX objPhone

BOOL bInitialCutsceneAllowedToRun
BOOL bCraneLoadDropped
BOOL bFreeSpeachPlayed
BOOL bDriverSeatExit//, bLastLineFired
BOOL bDieDialoguePlayed
BOOL bPushInToFirstPerson

INT index
INT iUpdateTrappedWorker
INT petrol_trail_stage
INT SoundId//, accident_variation
INT iCounterTimeStartedJumper

INT iForceExplosion = -1

INT trapped_guy_speech
INT counter_time_started_dying
INT counter_time_started_petrol

MODEL_NAMES current_veh_model

//CAMERA_INDEX CamID
SEQUENCE_INDEX seq

CONST_FLOAT AUDIO_TRIGGER_THRESHOLD	0.01
CONST_FLOAT SPREADER_START_OFFSET	-15.4

CONST_INT ASSISTED_ROUTE_CRANE_TOP	1
//VECTOR v_crane_pos = << 1098.9637, -2921.8936, 5.0007 >> //<<892.9637, -2921.8936, 5.0007>>
//REL_GROUP_HASH relGroupaccident_ped
structPedsForConversation AccidentConversation

VECTOR vPetrolTrailStart = << -458.4085, -984.6459, 22.5892 >>
VECTOR vPetrolTrailEnd = << -458.1934, -995.4897, 22.7427 >>

CAMERA_INDEX cam_cutscene
CAMERA_INDEX Cam_1, Cam_2//, Cam_3
INCIDENT_INDEX firemen_requested
PTFX_ID ptfx_gen_sparks

BOOL bDoFullCleanUp, bPreCleanup, bCutsceneSkipped

LoadQueueLight sLoadQueue


PROC loadAssets()
	
//	#IF IS_DEBUG_BUILD
//		CLEAR_AREA_OF_VEHICLES(vAccidentProneVehicleStart, 30)
//	#ENDIF
	
//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-494.1428, -964.3085, 22.4918>>, accident_acivation_range)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, bulldozer)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, utillitruck2)
		LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, Prop_Pipe_Stack_01)
		//LOAD_QUEUE_SMALL_ADD_AUDIO_BANK(sLoadQueue, "CONSTRUCTION_ACCIDENT_SOUNDSET")
		
		IF HAS_LOAD_QUEUE_LIGHT_LOADED(sLoadQueue)
//			SET_VEHICLE_MODEL_IS_SUPPRESSED(bulldozer, TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(utillitruck2, TRUE)
//			GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vAccidentProneVehicleStart, vReturnedCarCoord, vReturnedCarHeading)
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			ADD_SCENARIO_BLOCKING_AREA(vInput + << 50, 50, 50 >>, vInput - << 50, 50, 50 >>)
			
			vAccidentProneVehicleStart = << -455.6561, -985.8071, 22.4868 >>
			vehTruck = CREATE_VEHICLE(utillitruck2, vAccidentProneVehicleStart, 80)
//			SET_ENTITY_AS_MISSION_ENTITY(vehTruck)
			SET_VEHICLE_ENGINE_ON(vehTruck, FALSE, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehTruck, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_EXTRA(vehTruck, 1, TRUE)
			SET_VEHICLE_EXTRA(vehTruck, 2, TRUE)
			SET_VEHICLE_EXTRA(vehTruck, 3, FALSE)
			SET_VEHICLE_EXTRA(vehTruck, 4, TRUE)
			SET_VEHICLE_EXTRA(vehTruck, 5, TRUE)
			SET_VEHICLE_EXTRA(vehTruck, 6, FALSE)
			SET_VEHICLE_EXTRA(vehTruck, 7, TRUE)
			FREEZE_ENTITY_POSITION(vehTruck, TRUE)
			SET_ENTITY_PROOFS(vehTruck, FALSE, FALSE, FALSE, TRUE, FALSE)
			SET_ENTITY_LOD_DIST(vehTruck, 10000)
			SET_VEHICLE_LOD_MULTIPLIER(vehTruck, 5)
			
			vehBulldozer = CREATE_VEHICLE(bulldozer, << -472.9444, -966.6710, 22.5534 >>, 181.8365)//<< -491.2934, -972.3405, 22.4918 >>, 271)
//			SET_ENTITY_AS_MISSION_ENTITY(vehBulldozer)
			
			CLEAR_AREA(vAccidentProneVehicleStart, 10, TRUE)
			
			objDangerProp[0] = CREATE_OBJECT_NO_OFFSET(Prop_Pipe_Stack_01, << -453.2, -986.1, 59.45 >>)
			SET_ENTITY_HEADING(objDangerProp[0], 90)
			FREEZE_ENTITY_POSITION(objDangerProp[0], TRUE)
			SET_ENTITY_PROOFS(objDangerProp[0], TRUE, TRUE, TRUE, TRUE, FALSE)
			SET_ENTITY_LOD_DIST(objDangerProp[0], 10000000)
			
			gasTrail[0].coord = vPetrolTrailEnd
			gasTrail[1].coord = vPetrolTrailEnd + << 0, 0.5, 0 >>
			gasTrail[2].coord = vPetrolTrailEnd + << 0, 1, 0 >>
			gasTrail[3].coord = vPetrolTrailEnd + << 0, 2.5, 0 >>
			gasTrail[4].coord = vPetrolTrailEnd + << 0, 3, 0 >>
			gasTrail[5].coord = vPetrolTrailEnd + << 0, 3.5, 0 >>
			gasTrail[6].coord = vPetrolTrailEnd + << 0, 4, 0 >>
			gasTrail[7].coord = vPetrolTrailEnd + << 0, 4.5, 0 >>
			gasTrail[8].coord = vPetrolTrailEnd + << 0, 5, 0 >>
			gasTrail[9].coord = vPetrolTrailEnd + << 0, 5.5, 0 >>
			gasTrail[10].coord = vPetrolTrailEnd + << 0, 6, 0 >>
			gasTrail[11].coord = vPetrolTrailEnd + << 0, 6.5, 0 >>
			gasTrail[12].coord = vPetrolTrailEnd + << 0, 7, 0 >>
			gasTrail[13].coord = vPetrolTrailStart
			
			INT i
			REPEAT COUNT_OF(gasTrail) i
                  gasTrail[i].bPouredOn = TRUE
            ENDREPEAT
			
			IF NOT DOES_BLIP_EXIST(blipRandomEvent)
//				blipRandomEvent = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(pedWorker)
			ENDIF
//			IF NOT DOES_BLIP_EXIST(blipWorker)
//				REMOVE_BLIP(blipRandomEvent)
//				blipWorker = CREATE_AMBIENT_BLIP_FOR_PED(pedWorker)
//			ENDIF

			CLEANUP_LOAD_QUEUE_LIGHT(sLoadQueue)
			
			bDoFullCleanUp = TRUE
			con_accidentStage = STAGE_RUN_CON_ACCIDENT
//			accident_variation = 0
			PRINTSTRING("re_con_accident - MODEL LOADED AND PEDS CREATED")
			PRINTNL()
		ENDIF
		
//	ENDIF
	
ENDPROC

// Clean Up
PROC missionCleanup()
	PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@@@@@@@@\n")
	
	IF bDoFullCleanUp
		IF NOT bPreCleanup
			TRIGGER_MUSIC_EVENT("RE14A_FAIL")
			KILL_FACE_TO_FACE_CONVERSATION()
			
			IF NOT IS_PED_INJURED(pedWorker)
				SET_PED_CONFIG_FLAG(pedWorker, PCF_CanSayFollowedByPlayerAudio, TRUE)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehImpound)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpound)
				AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehImpound, << 50, 50, 50 >>)
					SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
				ENDIF
			ENDIF
	//		IF DOES_ENTITY_EXIST(pedWorker)
	//			IF NOT IS_PED_INJURED(pedWorker)
	//				SET_PED_KEEP_TASK(pedWorker, TRUE)
	//			ENDIF
	//		ENDIF
	//		REMOVE_MODEL_HIDE(v_crane_pos, 200.0, PROP_DOCK_CRANE_01)
	//		REMOVE_MODEL_HIDE(<<1105.2380, -2935.9258, 11.0341>>, 4.0, PROP_DOCK_CRANE_LIFT)
	//		
	//		REMOVE_MODEL_HIDE(<<1099.0309, -2929.5054, 4.9008>>, 6, Prop_crane_01_truck1)
	//		REMOVE_MODEL_HIDE(<<1099.0309, -2929.5054, 4.9008>>, 6, Prop_crane_01_truck2)
	//		
	//		REMOVE_MODEL_HIDE(<<1097.4131, -2902.7424, 4.9008>>, 6, Prop_crane_01_truck1)
	//		REMOVE_MODEL_HIDE(<<1097.4131, -2902.7424, 4.9008>>, 6, Prop_crane_01_truck2)
			
	//		REPEAT NUMBER_OF_FALLING_PROPS index
	//			IF DOES_ENTITY_EXIST(objDangerProp[index])
	//				FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
	//			ENDIF
	//		ENDREPEAT
			
	//		IF IS_VEHICLE_DRIVEABLE(vehTruck)
	//			FREEZE_ENTITY_POSITION(vehTruck, FALSE) //TEMP
	//		ENDIF
			REPEAT NUMBER_OF_FALLING_PROPS index
				IF DOES_ENTITY_EXIST(objDangerProp[index])
					FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
	//				PRINTSTRING("\n@@@@@@@@@@@@@@ UNFREEZE @@@@@@@@@@@@@@\n")
				ENDIF
			ENDREPEAT
			
			IF NOT IS_ENTITY_DEAD(vehTruck)
				SET_VEHICLE_ENGINE_HEALTH(vehTruck, 0)
				FREEZE_ENTITY_POSITION(vehTruck, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objGenerator)
				FREEZE_ENTITY_POSITION(objGenerator, FALSE)
			ENDIF
			
			// If the player blows up the truck during cutscene
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
	//		CLEAR_AREA_OF_PEDS(vAccidentProneVehicleStart, 30)
	//		CLEAR_AREA_OF_VEHICLES(vAccidentProneVehicleStart, 30)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bCraneLoadDropped = TRUE
			
			SET_TIME_SCALE(1)
			
			REMOVE_ANIM_DICT("re@construction")
			IF DOES_CAM_EXIST(cam_cutscene)
				DESTROY_CAM(cam_cutscene)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipWorker)
				REMOVE_BLIP(blipWorker)
	//			SET_PED_CONFIG_FLAG(pedWorker, PCF_GetOutBurningVehicle, TRUE)
	//			SET_PED_CONFIG_FLAG(pedWorker, PCF_GetOutUndriveableVehicle, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(blipTruck)
				REMOVE_BLIP(blipTruck)
			ENDIF
			IF DOES_BLIP_EXIST(blipBulldozer)
				REMOVE_BLIP(blipBulldozer)
			ENDIF
			IF DOES_BLIP_EXIST(blipRandomEvent)
				REMOVE_BLIP(blipRandomEvent)
			ENDIF
			
	//		#IF IS_DEBUG_BUILD
	//			IF can_cleanup_script_for_debug
	//				IF DOES_ENTITY_EXIST(pedWorker)
	//					DELETE_PED(pedWorker)
	//				ENDIF
	//				IF DOES_ENTITY_EXIST(vehTruck)
	//					DELETE_VEHICLE(vehTruck)
	//				ENDIF
	//				REPEAT NUMBER_OF_FALLING_PROPS index
	//					IF DOES_ENTITY_EXIST(objDangerProp[index])
	//						DELETE_OBJECT(objDangerProp[index])
	//					ENDIF
	//				ENDREPEAT
	//			ENDIF
	//		#ENDIF
			#IF IS_DEBUG_BUILD
				IF DOES_WIDGET_GROUP_EXIST(debugWidget)
					DELETE_WIDGET_GROUP(debugWidget)
				ENDIF
			#ENDIF
		ENDIF
		
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-460.301941,-870.360718,21.893250>>, <<-508.485901,-981.119934,27.320866>>, 36.562500)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		RESET_DISPATCH_IDEAL_SPAWN_DISTANCE()
		REMOVE_SCENARIO_BLOCKING_AREAS()
		
	ENDIF
	
	CLEANUP_LOAD_QUEUE_LIGHT(sLoadQueue)
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, 2)
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	con_accidentStage = STAGE_CLEAN_UP
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	TRIGGER_MUSIC_EVENT("RE14A_FAIL")
	con_accidentStage = STAGE_CLEAN_UP
ENDPROC

PROC preCleanup()
	IF bDoFullCleanUp
	AND NOT bPreCleanup
		TRIGGER_MUSIC_EVENT("RE14A_FAIL")
		REPEAT NUMBER_OF_FALLING_PROPS index
			IF DOES_ENTITY_EXIST(objDangerProp[index])
				FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_VEHICLE_ENGINE_HEALTH(vehTruck, 0)
			FREEZE_ENTITY_POSITION(vehTruck, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objGenerator)
			FREEZE_ENTITY_POSITION(objGenerator, FALSE)
		ENDIF
		
		// If the player blows up the truck during cutscene
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		bCraneLoadDropped = TRUE
		
		SET_TIME_SCALE(1)
		
		REMOVE_ANIM_DICT("re@construction")
		IF DOES_CAM_EXIST(cam_cutscene)
			DESTROY_CAM(cam_cutscene)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		IF DOES_BLIP_EXIST(blipWorker)
			REMOVE_BLIP(blipWorker)
		ENDIF
		IF DOES_BLIP_EXIST(blipTruck)
			REMOVE_BLIP(blipTruck)
		ENDIF
		IF DOES_BLIP_EXIST(blipBulldozer)
			REMOVE_BLIP(blipBulldozer)
		ENDIF
		IF DOES_BLIP_EXIST(blipRandomEvent)
			REMOVE_BLIP(blipRandomEvent)
		ENDIF
		bPreCleanup = TRUE
	ENDIF
	// We do the following to keep stack of pipes around until player leaves the area
	IF DOES_ENTITY_EXIST(objDangerProp[0])
	OR DOES_ENTITY_EXIST(objDangerProp[1])
		IF NOT IS_SPHERE_VISIBLE(vAccidentProneVehicleStart + << 0, 0, 20 >>, 30)
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAccidentProneVehicleStart, << 80, 80, 80 >>)
			missionCleanup()
		ENDIF
	ENDIF
ENDPROC

//Handle the truck explosion if failed prior to destruction
PROC HANDLE_TRUCK_EXPLOSION()

	#IF IS_DEBUG_BUILD
		IF NOT IS_ENTITY_DEAD( vehTruck )
			PRINTLN("HANDLING VEHICLE EXPLOSION TIME LEFT: ", GET_GAME_TIMER() - iForceExplosion)
		ENDIF
	#ENDIF

	//Set the force explosion timer
	IF iForceExplosion = -1
		iForceExplosion = GET_GAME_TIMER() + 5000
	ENDIF

	IF GET_GAME_TIMER() > iForceExplosion
		IF DOES_ENTITY_EXIST(vehTruck)
			FREEZE_ENTITY_POSITION(vehTruck, FALSE)
			APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_FORCE, << 0, 0, 50 >>, << 0, 0, 0 >>, 0, TRUE, TRUE, TRUE)
			PRINTLN("@@@@@@@@@@@ APPLY_FORCE_TO_ENTITY 1 @@@@@@@@@@@")
			IF NOT IS_ENTITY_DEAD( vehTruck )
				#IF IS_DEBUG_BUILD
					PRINTLN("FORCE EXPLODING VEHICLE")
				#ENDIF
				EXPLODE_VEHICLE(vehTruck)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC gasTrailTravel()

	SWITCH petrol_trail_stage
	
//		CASE 0
//		PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ petrol_trail_stage - CASE 0 @@@@@@@@@@@@@@@@@@@@@@\n")
//			counter_current_time_petrol = GET_GAME_TIMER()
//			IF (counter_current_time_petrol - counter_time_started_petrol) > 5000
//				ADD_PETROL_DECAL(vPetrolTrailStart, 0.1, 3, 0.1)
//				counter_time_started_petrol = GET_GAME_TIMER()
//				petrol_trail_stage = 1
//			ENDIF
//		BREAK
		
		CASE 0
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ petrol_trail_stage - CASE 1 @@@@@@@@@@@@@@@@@@@@@@\n")
////////////////////			SET_VFX_IGNORE_CAM_DIST_CHECK(TRUE)
//			counter_current_time_petrol = GET_GAME_TIMER()
//			IF (counter_current_time_petrol - counter_time_started_petrol) > petrol_time
//				vPetrolTrailStart.y = vPetrolTrailStart.y -0.25
//				fRandomStartSize = GET_RANDOM_FLOAT_IN_RANGE(0.6, 1.0)
//				fRandomEndSize = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.2)
//				fRandomGrowthRate = 0.1 //GET_RANDOM_FLOAT_IN_RANGE(0.1, 0.2)
//				iRandomDrips = 10//40 GET_RANDOM_INT_IN_RANGE(2, 5)
//				PRINTSTRING("iRandomDrips = ")
//				PRINTINT(iRandomDrips)
//				PRINTNL()
				ADD_PETROL_DECAL(vPetrolTrailEnd + << 0, 1, 0 >>, 1, 1, 1)
				WAIT(0)
				START_PETROL_TRAIL_DECALS(1)
					ADD_PETROL_TRAIL_DECAL_INFO(vPetrolTrailStart, 1)
					WAIT(0)
					ADD_PETROL_TRAIL_DECAL_INFO(vPetrolTrailStart - << 0, 4, 0 >>, 1)
					WAIT(0)
					ADD_PETROL_TRAIL_DECAL_INFO(vPetrolTrailStart - << 0, 8, 0 >>, 1)
					WAIT(0)
					ADD_PETROL_TRAIL_DECAL_INFO(vPetrolTrailEnd, 1)
				END_PETROL_TRAIL_DECALS()
//				REPEAT iRandomDrips index
//					ADD_PETROL_DECAL(vPetrolTrailStart, fRandomStartSize, fRandomEndSize, fRandomGrowthRate)
//					vPetrolTrailStart.y = vPetrolTrailStart.y  - GET_RANDOM_FLOAT_IN_RANGE(1, 3)//-0.3
//					WAIT(0)
//				ENDREPEAT
//				counter_time_started_petrol = GET_GAME_TIMER()
//			ENDIF
////////////////////			SET_VFX_IGNORE_CAM_DIST_CHECK(FALSE)
//			IF trapped_guy_speech >= 3
//			IF vPetrolTrailStart.y <= vPetrolTrailEnd.y
				counter_time_started_petrol = GET_GAME_TIMER()
				petrol_trail_stage = 2
//			ENDIF
		BREAK
		
		CASE 2
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ petrol_trail_stage - CASE 2 @@@@@@@@@@@@@@@@@@@@@@\n")
//			counter_current_time_petrol = GET_GAME_TIMER()
			IF (GET_GAME_TIMER() - counter_time_started_petrol) > time_to_script_fire
			OR IS_GAS_TRAIL_BURNING()
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_gen_sparks)
					STOP_PARTICLE_FX_LOOPED(ptfx_gen_sparks)
				ENDIF
				IGNITE_GAS_TRAIL()
//				START_SCRIPT_FIRE(GET_ENTITY_COORDS(vehTruck, FALSE), 1, TRUE)				
//				START_SCRIPT_FIRE(vPetrolTrailEnd + << 0, 1, 0 >>, 4)
//				START_SCRIPT_FIRE(vPetrolTrailEnd - << 0, 1, 0.1 >>, 4)
				IF NOT bDieDialoguePlayed
					IF NOT IS_PED_INJURED(pedWorker)
					AND NOT IS_ENTITY_DEAD(vehTruck)
						IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedWorker, vehTruck, VS_DRIVER)
						AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedWorker, vehTruck, VS_FRONT_RIGHT)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_DIE", CONV_PRIORITY_AMBIENT_HIGH)
							bDieDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
				SET_DISPATCH_IDEAL_SPAWN_DISTANCE(80)
				PRINTLN("@@@@@@@@@@@@@ CREATE_INCIDENT 1 @@@@@@@@@@@@@@")
				SET_ROADS_IN_ANGLED_AREA(<<-460.301941,-870.360718,21.893250>>, <<-508.485901,-981.119934,27.320866>>, 36.562500, FALSE, TRUE)
				CREATE_INCIDENT(DT_FIRE_DEPARTMENT, <<-539.4481, -962.3162, 22.4918>>, 4, 0, firemen_requested)
				SETTIMERA(0)
				petrol_trail_stage = 3
			ENDIF
		BREAK
		
		CASE 3
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ petrol_trail_stage - CASE 3 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF DOES_ENTITY_EXIST(vehTruck)
				
				IF NOT IS_ENTITY_ON_FIRE(vehTruck)
//				AND GET_NUMBER_OF_FIRES_IN_RANGE(vPetrolTrailEnd, 5) = 0
//					START_SCRIPT_FIRE(vPetrolTrailEnd, 4)
//					START_SCRIPT_FIRE(vPetrolTrailEnd + << 0, 1, 0 >>, 4)
//					START_SCRIPT_FIRE(vPetrolTrailEnd - << 0, 2, 0 >>, 4)
				ENDIF
			ENDIF
			IF iUpdateTrappedWorker < 2
				IF TIMERA() > 5000
					IF NOT IS_ENTITY_DEAD(vehTruck)
						FREEZE_ENTITY_POSITION(vehTruck, FALSE)
						APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_FORCE, << 0, 0, 50 >>, << 0, 0, 0 >>, 0, TRUE, TRUE, TRUE)
						PRINTLN("@@@@@@@@@@@ APPLY_FORCE_TO_ENTITY 2 @@@@@@@@@@@")
						EXPLODE_VEHICLE(vehTruck)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SAFE_DESTROY_CAMERA( CAMERA_INDEX cam )
	IF( DOES_CAM_EXIST( cam ) )
		DESTROY_CAM( cam )
	ENDIF
ENDPROC

PROC playPipeBreakCutscene()
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()

		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
		
		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_VEHICLE_DAMAGE(vehTruck, << 0, 1, 1 >>, 200, 800, TRUE)
		ENDIF
		
		bCutsceneSkipped = TRUE
		start_cut_stage = STARTCUT_CLEANUP
	ENDIF
	
	SWITCH start_cut_stage
		
		CASE STARTCUT_SETUP
			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_SETUP @@@@@@@@@@@@@@@@@@@@@@\n")
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CLEAR_ALL_HELP_MESSAGES()
//			SET_MINIGAME_IN_PROGRESS(TRUE)
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			SET_FRONTEND_RADIO_ACTIVE(FALSE)
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
				SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
			ENDIF
			CLEAR_AREA_OF_PEDS(vAccidentProneVehicleStart, 50)
			CLEAR_AREA_OF_VEHICLES(vAccidentProneVehicleStart, 30)
			CLEAR_AREA_OF_PROJECTILES(vAccidentProneVehicleStart, 100)
			
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), vAccidentProneVehicleStart, << 8, 8, 8 >>)
					vehicleCandidate = GET_PLAYERS_LAST_VEHICLE()
					SET_ENTITY_AS_MISSION_ENTITY(vehicleCandidate)
					DELETE_VEHICLE(vehicleCandidate)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -473.1686, -984.6405, 22.4870 >>, << 15, 15, 5 >>)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -477.3538, -972.6700, 22.5494 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 237.4839)
			ENDIF
			
			Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -462.7110, -992.5728, 23.8729>>, <<-0.1371, -0.0346, -29.6605 >>, 28, TRUE)
			Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -462.7110, -992.5728, 23.8729>>, <<69.4831, -0.0346, -40.8884 >>, 28, TRUE)
			SHAKE_CAM(Cam_1, "HAND_SHAKE", 0.3)
			SHAKE_CAM(Cam_2, "HAND_SHAKE", 0.3)
			
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
			BEGIN_PRELOADED_CONVERSATION() //Only one more day until retirment
			
			SET_CAM_ACTIVE(Cam_1, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			WAIT(500)
			SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 3500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			
			TRIGGER_MUSIC_EVENT("RE14A_START")
			counter_time_started_dying = GET_GAME_TIMER()
			start_cut_stage = STARTCUT_CAM1
		BREAK
		
		CASE STARTCUT_CAM1
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 3500 //-1500
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CAM1 @@@@@@@@@@@@@@@@@@@@@@\n")
//				PLAY_SOUND_FROM_COORD(-1, "CRANE_TOP", << -453, -985, 58 >>, "CONSTRUCTION_ACCIDENT_SOUNDSET")				
				PLAY_SOUND_FRONTEND(-1, "WEAKEN", /*<< -453, -985, 58 >>,*/ "CONSTRUCTION_ACCIDENT_1_SOUNDS")
				PLAY_SOUND_FRONTEND(SoundId, "WIND", /*<< -453, -985, 58 >>m*/ "CONSTRUCTION_ACCIDENT_1_SOUNDS")
//				FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(objDangerProp[0], TRUE)
				
				SAFE_DESTROY_CAMERA( Cam_1 )
				SAFE_DESTROY_CAMERA( Cam_2 )
				Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -455.4617, -990.6310, 61.6885>>, <<-89.0761, -0.0146, -49.8810 >>, 80.5355, TRUE)
				Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -455.4543, -990.6208, 61.0120>>, <<-89.0759, -0.0146, -47.8226 >>, 80.5355, TRUE)
				SHAKE_CAM(Cam_1, "HAND_SHAKE", 0.3)
				SHAKE_CAM(Cam_2, "HAND_SHAKE", 0.3)
				SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 4000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
				IF NOT IS_PED_INJURED(pedWorker)
					CLEAR_PED_TASKS(pedWorker)
					TASK_LOOK_AT_COORD(pedWorker, << -461.4785, -984.8583, 28.0809 >>, 30000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_PITCH_LIMIT)
				ENDIF
				IF DOES_ENTITY_EXIST(objPhone)
					DETACH_ENTITY(objPhone)
					DELETE_OBJECT(objPhone)
				ENDIF
//				gasTrailTravel()
				counter_time_started_dying = GET_GAME_TIMER()
				start_cut_stage = STARTCUT_CAM2
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM2
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 1500 //-1500
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CAM2 @@@@@@@@@@@@@@@@@@@@@@\n")
				PLAY_SOUND_FRONTEND(-1, "CABLE_SNAPS", /*<< -453, -985, 58 >>,*/ "CONSTRUCTION_ACCIDENT_1_SOUNDS")
				counter_time_started_dying = GET_GAME_TIMER()
				start_cut_stage = STARTCUT_CAM3
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM3
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 500 //-1500
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CAM3 @@@@@@@@@@@@@@@@@@@@@@\n")
//				CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_FUCK", CONV_PRIORITY_AMBIENT_HIGH)
				IF DOES_ENTITY_EXIST(objDangerProp[0])
					FREEZE_ENTITY_POSITION(objDangerProp[0], FALSE)
					APPLY_FORCE_TO_ENTITY(objDangerProp[0], APPLY_TYPE_FORCE, << 0, 0, 5 >>, << 0, -2, 2 >>, 0, TRUE, TRUE, TRUE) //<<0, 0, -5>>, <<0, 0, 2>>
	//				BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 2, FALSE) //2
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 11, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 18, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 19, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 23, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 9, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 12, FALSE)
					BREAK_OBJECT_FRAGMENT_CHILD(objDangerProp[0], 17, FALSE)
					START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_reconstruct_pipefall_debris", objDangerProp[0], << 0, 0, 0 >>, << 0, 0, 0 >>)
				ENDIF
				counter_time_started_dying = GET_GAME_TIMER()
				start_cut_stage = STARTCUT_CAM4
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM4
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 2000
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CAM4 @@@@@@@@@@@@@@@@@@@@@@\n")
				SAFE_DESTROY_CAMERA( Cam_1 )
				SAFE_DESTROY_CAMERA( Cam_2 )
				Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -470.5648, -976.9510, 24.2657>>, <<25.4370, -0.0087, -118.3831 >>, 34, TRUE)
				Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -470.5648, -976.9510, 24.2657>>, <<2.0695, -0.0087, -120.2811 >>, 34, TRUE)
				SHAKE_CAM(Cam_1, "HAND_SHAKE", 0.3)
				SHAKE_CAM(Cam_2, "HAND_SHAKE", 0.3)
				SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 1000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				STOP_SOUND(SoundId)
//				CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_SCREAM", CONV_PRIORITY_AMBIENT_HIGH) //WHAT THE!
				counter_time_started_dying = GET_GAME_TIMER()
				start_cut_stage = STARTCUT_CAM5
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM5
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 500
				IF NOT IS_ENTITY_DEAD(vehTruck)
					FREEZE_ENTITY_POSITION(vehTruck, FALSE)
					PLAY_SOUND_FRONTEND(-1, "PIPES_LAND", /*<< -456, -985, 25 >>,*/ "CONSTRUCTION_ACCIDENT_1_SOUNDS")
					PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ CONSTRUCTION_ACCIDENT_1_PIPES_LAND @@@@@@@@@@@@@@@@@@@@@@\n")
//					APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_IMPULSE,  << 0.0 , 0.0, 0.2 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, TRUE)
					SET_VEHICLE_DAMAGE(vehTruck, << 0, 1, 1 >>, 200, 800, TRUE)
					IF NOT IS_PED_INJURED(pedWorker)
						TASK_CLEAR_LOOK_AT(pedWorker)
					ENDIF
					TASK_PLAY_ANIM(pedWorker, "re@construction", "idle_panic", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)//|AF_HOLD_LAST_FRAME) //"die_horn"
					START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_reconstruct_pipe_impact", vehTruck, << 0, 0, 0 >>, << 0, 0, 0 >>)
					CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_SCREAM", CONV_PRIORITY_AMBIENT_HIGH) //WHAT THE!
					IF DOES_CAM_EXIST(Cam_1)
					AND DOES_CAM_EXIST(Cam_2)
						SHAKE_CAM(Cam_1, "HAND_SHAKE", 1.5)
						SHAKE_CAM(Cam_2, "HAND_SHAKE", 1.5)
					ENDIF
					counter_time_started_dying = GET_GAME_TIMER()
					start_cut_stage = STARTCUT_CAM6
				ENDIF
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM6
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 500
//				CLEAR_PED_TASKS(pedWorker)
//				WAIT(0)
//				gasTrailTravel()
				SAFE_DESTROY_CAMERA( Cam_1 )
				SAFE_DESTROY_CAMERA( Cam_2 )
				Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -459.2609, -987.5602, 24.2611>>, <<0.5830, 0.2617, -32.7532 >>, 49.9914, TRUE)
				Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -459.0622, -987.2584, 24.2661>>, <<5.0301, 0.2617, -36.7308 >>, 49.9914, TRUE)
				SHAKE_CAM(Cam_1, "HAND_SHAKE", 0.5)
				SHAKE_CAM(Cam_2, "HAND_SHAKE", 0.5)
				SET_CAM_ACTIVE_WITH_INTERP(Cam_2, Cam_1, 2500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				IF NOT IS_PED_INJURED(pedWorker)
					TASK_LOOK_AT_COORD(pedWorker, GET_ENTITY_COORDS(pedWorker) - << 2, 0, 10 >>, 30000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_PITCH_LIMIT)//<< -458.2319, -994.7391, 22.9688 >>
				ENDIF
//				objDangerProp[10] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, <<-454.6440, -994.1709, 23.3329>>)
//				SET_ENTITY_HEADING(objDangerProp[10], 80)
//				SET_ENTITY_ROTATION(objDangerProp[10], <<5.7811, 104.6058, 104.6964>>)
				counter_time_started_dying = GET_GAME_TIMER()
				start_cut_stage = STARTCUT_CAM7
			ENDIF
		BREAK
		
		CASE STARTCUT_CAM7
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 2500 - 300
			AND NOT bPushInToFirstPerson
				IF (IS_PED_ON_FOOT(PLAYER_PED_ID()) AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
				OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					bPushInToFirstPerson = TRUE
				ENDIF
			ENDIF
			IF (GET_GAME_TIMER() - counter_time_started_dying) > 2500
				IF IS_VEHICLE_DRIVEABLE(vehTruck)
					FREEZE_ENTITY_POSITION(vehTruck, TRUE)
				ENDIF
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CAM7 @@@@@@@@@@@@@@@@@@@@@@\n")
				start_cut_stage = STARTCUT_CLEANUP
			ENDIF
		BREAK
		
		CASE STARTCUT_CLEANUP
			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ STARTCUT_CLEANUP @@@@@@@@@@@@@@@@@@@@@@\n")
			
			TRIGGER_MUSIC_EVENT("RE14A_PIPES")
//			REPEAT NUMBER_OF_FALLING_PROPS index
				IF DOES_ENTITY_EXIST(objDangerProp[0])
					DELETE_OBJECT(objDangerProp[0])
				ENDIF
//			ENDREPEAT
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
//				SET_VEHICLE_CAN_LEAK_PETROL(vehTruck, TRUE)
				SET_VEHICLE_ENGINE_HEALTH(vehTruck, 250)
//				SET_VEHICLE_PETROL_TANK_HEALTH(vehTruck, 250)
				SET_ENTITY_COORDS(vehTruck, vAccidentProneVehicleStart)
				SET_ENTITY_HEADING(vehTruck, 80)
				CLEAR_AREA_OF_OBJECTS(vAccidentProneVehicleStart, 50)
//				CLEAR_AREA(vAccidentProneVehicleStart, 30, TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedWorker)
				CLEAR_PED_TASKS(pedWorker)
				TASK_CLEAR_LOOK_AT(pedWorker)
				TASK_PLAY_ANIM(pedWorker, "re@construction", "idle_panic", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
//			KILL_ANY_CONVERSATION()
			SET_CAM_ACTIVE(Cam_1, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(Cam_1)
			DESTROY_CAM(Cam_2)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//			SET_MINIGAME_IN_PROGRESS(FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			SET_FRONTEND_RADIO_ACTIVE(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			event_complete = TRUE
			
			iCounterTimeStartedJumper = GET_GAME_TIMER()
			
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Pipe_Stack_01)
			
//			REMOVE_ANIM_DICT("re@construction")
			
			objDangerProp[1] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -457.8815, -988.9059, 22.9554 >>)
//			SET_ENTITY_HEADING(objDangerProp[1], 80)
			SET_ENTITY_ROTATION(objDangerProp[1], << -0.1939, 105.0058, 77.3964 >>)
			
			objDangerProp[2] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -451.5131, -986.8079, 24.6947 >>)
//			SET_ENTITY_HEADING(objDangerProp[2], 70)
			SET_ENTITY_ROTATION(objDangerProp[2], << 24.3941, 32.5423, 80.6862 >>)
			
			objDangerProp[3] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -450.8099, -985.9972, 24.7104 >>)
//			SET_ENTITY_HEADING(objDangerProp[3], -80)
			SET_ENTITY_ROTATION(objDangerProp[3], << 24.1841, 110.2870, 78.4357 >>)
			
			objDangerProp[4] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -456.6558, -987.3979, 22.9537 >>)
//			SET_ENTITY_HEADING(objDangerProp[4], 80)
			SET_ENTITY_ROTATION(objDangerProp[4], << -0.0264, 29.5754, 79.1811 >>)
			
			objDangerProp[5] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -456.1279, -988.4162, 22.9889 >>)
//			SET_ENTITY_HEADING(objDangerProp[5], 80)
			SET_ENTITY_ROTATION(objDangerProp[5], << -0.6337, 108.4339, 77.9539 >>)
			
			objDangerProp[6] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -457.0807, -987.3059, 23.7564 >>)
//			SET_ENTITY_HEADING(objDangerProp[6], 80)
			SET_ENTITY_ROTATION(objDangerProp[6], << 0.0929, 74.2657, 79.3839 >>)
			
			objDangerProp[7] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -456.6558, -983.7804, 22.9712 >>)
//			SET_ENTITY_HEADING(objDangerProp[7], 80)
			SET_ENTITY_ROTATION(objDangerProp[7], << -0.0264, 29.5754, 80.9061 >>)
			
			objDangerProp[8] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -455.3279, -983.1712, 22.9564 >>)
//			SET_ENTITY_HEADING(objDangerProp[8], 80)
			SET_ENTITY_ROTATION(objDangerProp[8], << -0.1087, 109.7339, 80.9789 >>)
			
			objDangerProp[9] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -456.1632, -983.9359, 23.7964 >>)
//			SET_ENTITY_HEADING(objDangerProp[9], 80)
			SET_ENTITY_ROTATION(objDangerProp[9], << -0.3321, 74.1907, 81.6089 >>)
			
			objDangerProp[10] = CREATE_OBJECT_NO_OFFSET(PROP_LD_PIPE_SINGLE_01, << -454.6440, -994.1709, 23.3329 >>)
//			SET_ENTITY_HEADING(objDangerProp[10], 80)
			SET_ENTITY_ROTATION(objDangerProp[10], << 5.7811, 104.6058, 104.6964 >>)
			
			REPEAT NUMBER_OF_FALLING_PROPS index
				IF DOES_ENTITY_EXIST(objDangerProp[index])
					SET_ENTITY_DYNAMIC(objDangerProp[index], TRUE)
					FREEZE_ENTITY_POSITION(objDangerProp[index], TRUE)
				ENDIF
			ENDREPEAT
			
			IF bCutsceneSkipped
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				WHILE IS_SCREEN_FADING_IN()
					WAIT(0)
				ENDWHILE
			ENDIF
						
			bCraneLoadDropped = TRUE
			bInitialCutsceneAllowedToRun = FALSE
		BREAK
		
	ENDSWITCH

ENDPROC

PROC updateTrappedWorker()

	#IF IS_DEBUG_BUILD
		PRINTLN("CURRENT iUpdateTrappedWorker val: ", iUpdateTrappedWorker)
	#ENDIF

	SWITCH iUpdateTrappedWorker
	
		CASE 0
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ updateTrappedWorker() - CASE 0 @@@@@@@@@@@@@@@@@@@@@@\n")
//			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedWorker, activate_range)
			IF trapped_guy_speech >= 2
				IF NOT IS_PED_INJURED(pedWorker)
				AND NOT IS_ENTITY_DEAD(vehTruck)
					IF NOT IS_PED_IN_ANY_VEHICLE(pedWorker)
						iUpdateTrappedWorker ++
					ENDIF
					DELETE_OBJECT(objPhone)
//					trapped_peds_coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<-2,2,0>>)
//					
//					new_coord1 = << trapped_peds_coords.x - 0.5, trapped_peds_coords.y + 0.5, trapped_peds_coords.z - 0 >>
//					new_coord2 = << trapped_peds_coords.x + 0.5, trapped_peds_coords.y - 0.5, trapped_peds_coords.z + 1 >>
//					
//					IS_ENTITY_IN_AREA(PLAYER_PED_ID(), new_coord1, new_coord2) //temp
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						players_vehicle = GET_PLAYERS_LAST_VEHICLE(PLAYER_PED_ID())
//					ENDIF
//					random_front_bumper = GET_RANDOM_VEHICLE_FRONT_BUMPER_IN_SPHERE(trapped_peds_coords, 2.5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES, vehTruck)
//					random_back_bumper = GET_RANDOM_VEHICLE_BACK_BUMPER_IN_SPHERE(trapped_peds_coords, 2.5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES, vehTruck)
////					DRAW_DEBUG_SPHERE(trapped_peds_coords, 2.5, 0, 0, 255, 100)
////					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//					
//					IF NOT IS_AREA_OCCUPIED(new_coord1, new_coord2, FALSE, FALSE, FALSE, TRUE, FALSE, vehTruck) //<< -460.98, -981.60, 22.48 >>, << -452.18, -989.05, 22.48 >>
					IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedWorker, vehTruck, VS_DRIVER)
					OR IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedWorker, vehTruck, VS_FRONT_RIGHT)
//						IF NOT IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_COORDS(vehTruck, FALSE) - << 2.6, 0, 0 >>, << 4.5, 4, 5 >>)
							IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(pedWorker, vehTruck, VS_DRIVER)
								bDriverSeatExit = TRUE
								vOffsetCheck = GET_ENTITY_COORDS(vehTruck, FALSE) + << -3, -1, 0 >>
							ELSE
								vOffsetCheck = GET_ENTITY_COORDS(vehTruck, FALSE) + << -3, 1.5, 0 >>
							ENDIF
//							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehTruck, FALSE) - << 2.6, 0, 0 >>, << 4.5, 4, 5 >>)
							IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), vOffsetCheck, << 4.2, 4, 5 >>)
	//							PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ IS_ENTITY_AT_ENTITY << 7, 7, 5 >> @@@@@@@@@@@@@@@@@@@@@@\n")
								IF NOT bFreeSpeachPlayed
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(0)
									CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_FREE", CONV_PRIORITY_AMBIENT_HIGH)
									bFreeSpeachPlayed = TRUE
//									iUpdateTrappedWorker ++
								ENDIF
							ELSE
								bFreeSpeachPlayed = TRUE
								iUpdateTrappedWorker ++
							ENDIF
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ updateTrappedWorker() - CASE 1 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF NOT IS_PED_INJURED(pedWorker)
			AND NOT IS_ENTITY_DEAD(vehTruck)
//				IF IS_PED_ON_FOOT(pedWorker)
//				OR NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehTruck , << 6.5, 6, 5 >>)
	//			AND NOT IS_ENTITY_AT_ENTITY(players_vehicle, vehTruck, << 12, 12, 5 >>)
	//			AND NOT DOES_ENTITY_EXIST(random_front_bumper)
	//			AND NOT DOES_ENTITY_EXIST(random_back_bumper)
	//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	//				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedWorker)
	//				SET_MINIGAME_IN_PROGRESS(TRUE)
	//				DISPLAY_RADAR(FALSE)
	//				DISPLAY_HUD(FALSE)
					IF NOT IS_ENTITY_DEAD(vehBulldozer)
						SET_ENTITY_PROOFS(vehBulldozer, FALSE, TRUE, TRUE, FALSE, FALSE)
					ENDIF
					SET_ENTITY_PROOFS(pedWorker, FALSE, TRUE, FALSE, FALSE, FALSE)
					FREEZE_ENTITY_POSITION(vehTruck, FALSE)
		//			PRINTNL()
		//			PRINTSTRING("SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)")
		//			PRINTNL()
					
	//				IF IS_VEHICLE_DRIVEABLE(vehTruck)
	//					trapped_peds_coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, << 0, 0, 0 >>)
	//				ENDIF
	//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					OPEN_SEQUENCE_TASK(seq)
					IF bDriverSeatExit
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000)
						TASK_LEAVE_ANY_VEHICLE(NULL)
//						TASK_PAUSE(NULL, 0)
	//					TASK_SMART_FLEE_COORD(NULL, trapped_peds_coords, 35, -1)
	//					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -474.7401, -992.7928, 22.4884 >>, PEDMOVE_SPRINT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -476.3904, -986.8360, 22.5569 >>, PEDMOVE_SPRINT, DEFAULT_TIME_NEVER_WARP)
	//					TASK_PLAY_ANIM(NULL, "re@construction", "wipe_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		//				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					ELSE
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000)
						TASK_LEAVE_ANY_VEHICLE(NULL)
//						TASK_PAUSE(NULL, 0)
	//					TASK_GO_STRAIGHT_TO_COORD(NULL, << -463.7827, -982.0370, 22.4870 >>, PEDMOVE_SPRINT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -476.3904, -986.8360, 22.5569 >>, PEDMOVE_SPRINT, DEFAULT_TIME_NEVER_WARP)
	//					TASK_GO_STRAIGHT_TO_COORD(NULL, << -481.7385, -995.0, 22.4923 >>, PEDMOVE_SPRINT)
					ENDIF
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedWorker, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedWorker, TRUE)
					
					iCounterTimeStartedJumper = GET_GAME_TIMER()
					iUpdateTrappedWorker ++
//				ENDIF
			ENDIF
		BREAK
		
		CASE 2
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ updateTrappedWorker() - CASE 2 @@@@@@@@@@@@@@@@@@@@@@\n")
			
//			petrol_time = 0
//			IF bDriverSeatExit
//				time_to_script_fire = 3000 //0
//			ELSE
//				time_to_script_fire = 5000 //2000
//			ENDIF
//			gasTrailTravel()
			
			IF NOT IS_PED_INJURED(pedWorker)
				IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 500
				AND NOT IS_PED_IN_ANY_VEHICLE(pedWorker)
					time_to_script_fire = 0
	//				Cam_1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					
	//				Cam_1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -461.8530, -997.9106, 23.7630 >>, << 1.5658, -0.0103, -22.9834 >>, 45, TRUE)
	//				Cam_2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -440.6591, -979.1392, 32.2452 >>, << -18.7978, -0.0000, 112.7261 >>, 20, TRUE)
	//				Cam_3 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -471.5445, -990.9800, 22.6132 >>, << 26.9757, -0.0576, -67.9896 >>, 75, TRUE)
	//				SHAKE_CAM(Cam_1, "HAND_SHAKE", 0.3)
	//				SHAKE_CAM(Cam_2, "HAND_SHAKE", 0.3)
	//				SHAKE_CAM(Cam_3, "HAND_SHAKE", 0.3)
					
	//				ATTACH_CAM_TO_PED_BONE(Cam_1, pedWorker, BONETAG_HEAD, << 0.5, 0.5, -0.4 >>)
	//				POINT_CAM_AT_PED_BONE(Cam_1, pedWorker, BONETAG_HEAD, << 0, 0, 0 >>)
	//				SET_CAM_FOV(Cam_1, 20)
					
	//				CLEAR_AREA_OF_VEHICLES(<< -466.5287, -987.0811, 22.4867 >>, 15)
	//				SET_CAM_ACTIVE(Cam_1, TRUE)
	//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
	//				IF NOT IS_ENTITY_DEAD(players_vehicle)
	//					IF bDriverSeatExit
	//						IF IS_ENTITY_AT_COORD(players_vehicle, << -470.2714, -992.3054, 22.4867 >>, << 10, 10, 5 >>)
	//							SET_ENTITY_COORDS(players_vehicle, << -471.4247, -985.4575, 22.4867 >>)
	//						ENDIF
	//					ELSE
	//						IF IS_ENTITY_AT_COORD(players_vehicle, << -471.4247, -985.4575, 22.4867 >>, << 10, 10, 5 >>)
	//							SET_ENTITY_COORDS(players_vehicle, << -471.5560, -979.7170, 22.4867 >>)
	//						ENDIF
	//					ENDIF	
	//				ENDIF
					
	//				IF NOT IS_ENTITY_DEAD(players_vehicle)
	//				AND NOT IS_ENTITY_DEAD(vehTruck)
	//				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	//					TASK_VEHICLE_MISSION(PLAYER_PED_ID(), players_vehicle, vehTruck, MISSION_FLEE, 20, DRIVINGMODE_PLOUGHTHROUGH, 10, 20)
	//				ENDIF
					
	//				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You Saved me, Thanks!", 6000, 1) //DEBUG TEXT
					
	//				START_SCRIPT_FIRE(vPetrolTrailEnd, 4)
					IF IS_VEHICLE_DRIVEABLE(vehTruck)
//						SET_VEHICLE_ENGINE_HEALTH(vehTruck, 300)
//						SET_VEHICLE_PETROL_TANK_HEALTH(vehTruck, 300)
//						SET_ENTITY_HEALTH(vehTruck, 300)
					ENDIF
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_BOOM", CONV_PRIORITY_AMBIENT_HIGH) //RUN!
					
					iCounterTimeStartedJumper = GET_GAME_TIMER()
					iUpdateTrappedWorker ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PED_INJURED(pedWorker)
				IF IS_ENTITY_AT_COORD(pedWorker, << -464.7217, -989.7214, 22.4867 >>, << 3, 10, 3 >>)
				OR (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 10000 // if ped is taking too long, explode truck
					CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_AAAH", CONV_PRIORITY_AMBIENT_HIGH)
	//				SET_CAM_ACTIVE(Cam_1, FALSE)
	//				SET_CAM_ACTIVE(Cam_2, TRUE)
					iUpdateTrappedWorker ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_PED_INJURED(pedWorker)
				IF IS_ENTITY_AT_COORD(pedWorker, << -472.7056, -991.1050, 22.4867 >>, << 3, 30, 3 >>)
				OR (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 10000 // if ped is taking too long, explode truck
	//				SET_CAM_ACTIVE(Cam_2, FALSE)
	//				SET_CAM_ACTIVE(Cam_3, TRUE)
	//				TASK_PLAY_ANIM(pedWorker, "cover_enter_unarmed", "dive_high_l", SLOW_BLEND_IN, SLOW_BLEND_OUT)
	//				WAIT(500)
	//				SET_TIME_SCALE(0.1)
					IF NOT IS_ENTITY_DEAD(vehTruck)
						FREEZE_ENTITY_POSITION(vehTruck, FALSE)
						APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_FORCE, << 0, 0, 50 >>, << 0, 0, 0 >>, 0, TRUE, TRUE, TRUE)
						PRINTLN("@@@@@@@@@@@ APPLY_FORCE_TO_ENTITY 3 @@@@@@@@@@@")
						EXPLODE_VEHICLE/*_IN_CUTSCENE*/(vehTruck)
					ENDIF
					iUpdateTrappedWorker ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ updateTrappedWorker() - CASE 3 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 3000
				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				SET_CAM_ACTIVE(Cam_1, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				DESTROY_CAM(Cam_1)
//				DESTROY_CAM(Cam_2)
//				DESTROY_CAM(Cam_3)
//				SET_TIME_SCALE(1)
				
				
				IF IS_VEHICLE_DRIVEABLE(vehTruck)
					FREEZE_ENTITY_POSITION(vehTruck, FALSE)
					APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_FORCE, << 0, 0, 50 >>, << 0, 0, 0 >>, 0, TRUE, TRUE, TRUE)
					PRINTLN("@@@@@@@@@@@ APPLY_FORCE_TO_ENTITY 4 @@@@@@@@@@@")
					EXPLODE_VEHICLE(vehTruck)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedWorker)
					TRIGGER_MUSIC_EVENT("RE14A_SAFE")
					IF NOT IS_ENTITY_DEAD(vehBulldozer)
						SET_ENTITY_PROOFS(vehBulldozer, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					SET_ENTITY_PROOFS(pedWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
					SET_PED_CAN_RAGDOLL(pedWorker, TRUE)
					SET_PED_CAN_BE_TARGETTED(pedWorker, TRUE)
					SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(pedWorker, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker, FALSE)
					SET_DECISION_MAKER(pedWorker, DECISION_MAKER_DEFAULT)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "re@construction", "Out_Of_Breath", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
//						TASK_PLAY_ANIM(NULL, "re@construction", "Wipe_Face")
//						TASK_PLAY_ANIM(NULL, "re@construction", "Wipe_Forehead")
					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedWorker, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				SET_MINIGAME_IN_PROGRESS(FALSE)
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				DISPLAY_RADAR(TRUE)
//				DISPLAY_HUD(TRUE)
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				PRINTLN("@@@@@@@@@@@@@ CREATE_INCIDENT 2 @@@@@@@@@@@@@@")
				SET_ROADS_IN_ANGLED_AREA(<<-460.301941,-870.360718,21.893250>>, <<-508.485901,-981.119934,27.320866>>, 36.562500, FALSE, TRUE)
				CREATE_INCIDENT(DT_FIRE_DEPARTMENT, <<-539.4481, -962.3162, 22.4918>>, 4, 0, firemen_requested)
				
				iCounterTimeStartedJumper = GET_GAME_TIMER()
				iUpdateTrappedWorker ++
				
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_PED_INJURED(pedWorker)
				IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 6400//3000
//				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedWorker, << 30, 30, 30 >>)
					PRINTSTRING("ready to talk...")PRINTNL()
					VECTOR vScenario
					vScenario = << -477.2774, -990.0638, 23.5497 >>
					IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
						IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), << -477.2774, -990.0638, 23.5497 >>, << 3, 3, 3 >>)
							vScenario = << -477.7884, -996.3974, 23.5503 >>
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehImpound)
						IF IS_ENTITY_AT_COORD(vehImpound, << -477.2774, -990.0638, 23.5497 >>, << 3, 3, 3 >>)
							vScenario = << -477.7884, -996.3974, 23.5503 >>
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehBulldozer)
						IF IS_ENTITY_AT_COORD(vehBulldozer, << -477.2774, -990.0638, 23.5497 >>, << 3, 3, 3 >>)
							vScenario = << -477.7884, -996.3974, 23.5503 >>
						ENDIF
					ENDIF
//					IF GET_SCRIPT_TASK_STATUS(pedWorker, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(pedWorker, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 0)
//							TASK_PLAY_ANIM(NULL, "re@construction", "Thanks_Male_04", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -514.0489, -1019.4971, 22.4711 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							TASK_START_SCENARIO_AT_POSITION(NULL, "WORLD_HUMAN_PICNIC", vScenario, 304.8172, 0, TRUE, FALSE)//"WORLD_HUMAN_BUM_STANDING" "WORLD_HUMAN_STUPOR"
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedWorker, seq)
						CLEAR_SEQUENCE_TASK(seq)
//					ENDIF
					
//					IF IS_PED_FACING_PED(pedWorker, PLAYER_PED_ID(), 60)
						CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_THANK", CONV_PRIORITY_AMBIENT_HIGH)
						
						MISSION_PASSED()
//					ENDIF
				ELSE
					PRINTSTRING("NOT ready to talk yet...")PRINTNL()
				ENDIF
//				IF NOT IS_ENTITY_AT_ENTITY(pedWorker, PLAYER_PED_ID(), << 120, 120, 120 >>)
//					MISSION_PASSED()
//				ENDIF
			ELSE
				MISSION_FAILED()
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC playTrappedInTruckSpeech()

	SWITCH trapped_guy_speech
		CASE 0
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ playTrappedInTruckSpeech() - CASE 0 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF NOT IS_PED_INJURED(pedWorker)
				IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 3000
//					PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Help! I'm trapped!", 3000, 1) //DEBUG TEXT
					CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_HELP", CONV_PRIORITY_AMBIENT_HIGH) //HELP!
//					IF IS_VEHICLE_DRIVEABLE(vehTruck)
//						FREEZE_ENTITY_POSITION(vehTruck, TRUE) //TEMP
//					ENDIF
//					IF IS_VEHICLE_DRIVEABLE(vehBulldozer)
//						blipBulldozer = CREATE_AMBIENT_BLIP_FOR_VEHICLE(vehBulldozer)
//					ENDIF
					counter_time_started_petrol = GET_GAME_TIMER()
					iCounterTimeStartedJumper = GET_GAME_TIMER()
					trapped_guy_speech = 2
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ playTrappedInTruckSpeech() - CASE 1 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF NOT IS_PED_INJURED(pedWorker)
				IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 3000
//					PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Clear away this shit from around the truck", 5000, 1) //DEBUG TEXT
					iCounterTimeStartedJumper = GET_GAME_TIMER()
					trapped_guy_speech ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ playTrappedInTruckSpeech() - CASE 2 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF NOT IS_PED_INJURED(pedWorker)
				IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > 5000
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						current_veh_model = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(pedWorker)
						IF IS_VEHICLE_DRIVEABLE(vehBulldozer)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), BULLDOZER)
									IF NOT DOES_BLIP_EXIST(blipBulldozer)
										blipBulldozer = CREATE_BLIP_FOR_VEHICLE(vehBulldozer)
										IF DOES_BLIP_EXIST(blipTruck)
											REMOVE_BLIP(blipTruck)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST(blipBulldozer)
									blipBulldozer = CREATE_BLIP_FOR_VEHICLE(vehBulldozer)
									IF DOES_BLIP_EXIST(blipTruck)
										REMOVE_BLIP(blipTruck)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF current_veh_model = BULLDOZER
						CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_HELP2", CONV_PRIORITY_AMBIENT_HIGH)
//						PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Quick, There's a bulldozer close by!", 5000, 1) //DEBUG TEXT
					ELSE
						CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_HELP3", CONV_PRIORITY_AMBIENT_HIGH) //HELP!
					ENDIF
					iCounterTimeStartedJumper = GET_GAME_TIMER()
					trapped_guy_speech ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ playTrappedInTruckSpeech() - CASE 2 @@@@@@@@@@@@@@@@@@@@@@\n")
			IF (GET_GAME_TIMER() - iCounterTimeStartedJumper) > GET_RANDOM_INT_IN_RANGE(5000, 8000)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_HURRY", CONV_PRIORITY_AMBIENT_HIGH)
					iCounterTimeStartedJumper = GET_GAME_TIMER()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC runConAccident()
	
	SWITCH Runcon_accidentStage
		
		CASE CON_ACCIDENT_FALLING_PIPES
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ CASE CON_ACCIDENT_FALLING_PIPES @@@@@@@@@@@@@@@@@@@@@@\n")
			
			REQUEST_SCRIPT_AUDIO_BANK("CONSTRUCTION_ACCIDENT_1")
			REQUEST_ANIM_DICT("re@construction")
			
			IF sLoadQueue.iLastFrame <= 0
				sLoadQueue.iLastFrame = GET_FRAME_COUNT()
			ENDIF
			LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, S_M_M_DOCKWORK_01)
			LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, PROP_generator_01a)
			LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, P_AMB_PHONE_01)
			LOAD_QUEUE_LIGHT_ADD_PTFX_ASSET(sLoadQueue)
			
			IF REQUEST_SCRIPT_AUDIO_BANK("CONSTRUCTION_ACCIDENT_1")
			AND HAS_ANIM_DICT_LOADED("re@construction")
			AND HAS_LOAD_QUEUE_LIGHT_LOADED(sLoadQueue)
			
				IF IS_VEHICLE_DRIVEABLE(vehTruck)
					pedWorker = CREATE_PED_INSIDE_VEHICLE(vehTruck, PEDTYPE_MISSION, S_M_M_DOCKWORK_01, VS_DRIVER)
					SET_PED_COMPONENT_VARIATION(pedWorker, PED_COMP_HEAD, 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedWorker, PED_COMP_TORSO, 1, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedWorker, PED_COMP_LEG, 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedWorker, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
					TASK_PLAY_ANIM(pedWorker, "re@construction", "idle_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_LOOPING, 0.4)
					objPhone = CREATE_OBJECT(P_AMB_PHONE_01, << -455.6561, -985.8071, 22.4868 >>)
					SET_MODEL_AS_NO_LONGER_NEEDED(P_AMB_PHONE_01)
					ATTACH_ENTITY_TO_ENTITY(objPhone, pedWorker, GET_PED_BONE_INDEX(pedWorker, BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
		//			TASK_USE_MOBILE_PHONE_TIMED(pedWorker, -1)
					
					SET_PED_COMBAT_ATTRIBUTES(pedWorker, CA_ALWAYS_FLEE, TRUE)
		//			SET_PED_CAN_RAGDOLL(pedWorker, FALSE)
					SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(pedWorker, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker, TRUE)
					SET_PED_CAN_BE_TARGETTED(pedWorker, FALSE)
					
		//			SET_PED_RELATIONSHIP_GROUP_HASH(pedWorker, relGroupaccident_ped)
					ADD_PED_FOR_DIALOGUE(AccidentConversation, 3, pedWorker, "RECONACWorker")
					SET_AMBIENT_VOICE_NAME(pedWorker, "S_M_Y_GENERICWORKER_01_WHITE_01")
					
					pedCraneWorker = CREATE_PED(PEDTYPE_MISSION, S_M_M_DOCKWORK_01, << -462.2982, -978.3272, 65.000 >>, 221.4041)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCraneWorker, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(pedCraneWorker, TRUE)
					FREEZE_ENTITY_POSITION(pedCraneWorker, TRUE)
//					ADD_PED_FOR_DIALOGUE(AccidentConversation, 4, pedCraneWorker, "RECONACCrane")
					
		//			ADD_RELATIONSHIP_GROUP("RE_ACCIDENT", relGroupaccident_ped)
		//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupaccident_ped, RELGROUPHASH_PLAYER)
					
					SET_DECISION_MAKER(pedWorker, DECISION_MAKER_EMPTY)
					CLEAR_DECISION_MAKER_EVENT_RESPONSE(DECISION_MAKER_EMPTY, EVENT_FIRE_NEARBY)
					CLEAR_DECISION_MAKER_EVENT_RESPONSE(DECISION_MAKER_EMPTY, EVENT_ON_FIRE)
					CLEAR_DECISION_MAKER_EVENT_RESPONSE(DECISION_MAKER_EMPTY, EVENT_POTENTIAL_WALK_INTO_FIRE)
					CLEAR_DECISION_MAKER_EVENT_RESPONSE(DECISION_MAKER_EMPTY, EVENT_VEHICLE_ON_FIRE)
					CLEAR_DECISION_MAKER_EVENT_RESPONSE(DECISION_MAKER_EMPTY, EVENT_PED_TO_FLEE)
					
					objGenerator = CREATE_OBJECT(PROP_generator_01a, <<vPetrolTrailEnd.x, vPetrolTrailEnd.y+0.5, vPetrolTrailEnd.z-0.25>>) 
					SET_ENTITY_HEADING(objGenerator, 75)
					SET_ENTITY_PROOFS(objGenerator, FALSE, TRUE, TRUE, FALSE, FALSE)
					ptfx_gen_sparks = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sparking_generator", objGenerator, <<0,0,0.2>>, <<0,0,0>>) //TEMP PARTICLE
		//			START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("veh_train_sparks", objGenerator, <<0,0,0.2>>, <<0,0,0>>)
					FREEZE_ENTITY_POSITION(objGenerator, TRUE)
					
					PRELOAD_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_CHAT", CONV_PRIORITY_AMBIENT_HIGH)
					
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//						IF NOT IS_PED_INJURED(pedWorker) 
//							IF IS_VEHICLE_DRIVEABLE(vehTruck)
								//TASK_VEHICLE_MISSION(pedWorker, vehTruck,  MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
		//						TASK_VEHICLE_DRIVE_TO_COORD(pedWorker, vehTruck, << -455.3011, -985.6072, 22.4863 >>, 5, DRIVINGSTYLE_NORMAL, BULLDOZER, DRIVINGMODE_AVOIDCARS, 3.0, -1)
								iCounterTimeStartedJumper = GET_GAME_TIMER()
								Runcon_accidentStage = CON_ACCIDENT_FALLING_PIPES_2
//							ENDIF
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CON_ACCIDENT_FALLING_PIPES_2
//			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ CASE CON_ACCIDENT_FALLING_PIPES_2 @@@@@@@@@@@@@@@@@@@@@@\n")
			
//			IF start_cut_stage <= STARTCUT_CAM4
//				gasTrailTravel()
//			ENDIF
			
			IF bInitialCutsceneAllowedToRun
				playPipeBreakCutscene()
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				IF NOT bCraneLoadDropped
				
					IF NOT IS_PED_INJURED(pedWorker)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -532.2208, -849.2668, 19.7038 >>, << -531.6973, -1065.3409, 60.7893 >>, 190.8125)
							IF NOT IS_ENTITY_DEAD(vehTruck)
								IF NOT DOES_BLIP_EXIST(blipTruck)
									blipTruck = CREATE_BLIP_FOR_VEHICLE(vehTruck)
								ENDIF
							ENDIF
						ENDIF
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedWorker, PLAYER_PED_ID())
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTruck, PLAYER_PED_ID())
							FREEZE_ENTITY_POSITION(vehTruck, FALSE)
							CLEAR_PED_TASKS(pedWorker)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedWorker, seq)
							CLEAR_SEQUENCE_TASK(seq)
							MISSION_FAILED()
						ENDIF
						REQUEST_MODEL(PROP_LD_PIPE_SINGLE_01)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -473.1686, -984.6405, 22.4870 >>, << 40, 40, 5 >>)
						AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
						AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
						AND CAN_PLAYER_START_CUTSCENE()
						AND HAS_MODEL_LOADED(PROP_LD_PIPE_SINGLE_01)
							IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
								IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
									vehImpound = GET_PLAYERS_LAST_VEHICLE()
								ENDIF
								SET_RANDOM_EVENT_ACTIVE()
							ENDIF
							bInitialCutsceneAllowedToRun = TRUE
						ENDIF
					ELSE
						MISSION_FAILED()
					ENDIF
					
				ELSE
					IF NOT IS_PED_INJURED(pedWorker)
						IF IS_PED_IN_ANY_VEHICLE(pedWorker)
							IF NOT IS_ENTITY_DEAD(vehTruck)
							AND NOT DOES_BLIP_EXIST(blipTruck)
		//						REMOVE_BLIP(blipRandomEvent)
								blipTruck = CREATE_BLIP_FOR_VEHICLE(vehTruck)
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(blipWorker)
								IF DOES_BLIP_EXIST(blipTruck)
									REMOVE_BLIP(blipTruck)
								ENDIF
								blipWorker = CREATE_BLIP_FOR_PED(pedWorker)
							ENDIF
						ENDIF
					ENDIF
					
					updateTrappedWorker()
//					IF trapped_guy_speech >= 3
						gasTrailTravel()
//					ENDIF
					IF NOT IS_PED_INJURED(pedWorker)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedWorker, activate_range)
							IF NOT bFreeSpeachPlayed
								playTrappedInTruckSpeech()
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipWorker)
							REMOVE_BLIP(blipWorker)
						ENDIF
						IF DOES_BLIP_EXIST(blipTruck)
							REMOVE_BLIP(blipTruck)
						ENDIF
						IF DOES_BLIP_EXIST(blipBulldozer)
							REMOVE_BLIP(blipBulldozer)
						ENDIF
						IF IS_PED_INJURED(pedWorker)
						OR IS_ENTITY_DEAD(vehTruck)
							MISSION_FAILED()
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(vehBulldozer)
						IF DOES_BLIP_EXIST(blipBulldozer)
							REMOVE_BLIP(blipBulldozer)
							IF NOT IS_ENTITY_DEAD(vehTruck)
							AND NOT DOES_BLIP_EXIST(blipTruck)
								blipTruck = CREATE_BLIP_FOR_VEHICLE(vehTruck)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehBulldozer)
					IF trapped_guy_speech >= 3
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND IS_PED_IN_MODEL(PLAYER_PED_ID(), BULLDOZER)
							IF DOES_BLIP_EXIST(blipBulldozer)
								REMOVE_BLIP(blipBulldozer)
								IF NOT IS_PED_INJURED(pedWorker)
								AND NOT IS_ENTITY_DEAD(vehTruck)
									IF IS_PED_IN_ANY_VEHICLE(pedWorker)
									AND NOT DOES_BLIP_EXIST(blipTruck)
										blipTruck = CREATE_BLIP_FOR_VEHICLE(vehTruck)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_INJURED(pedWorker)
							AND NOT IS_ENTITY_DEAD(vehTruck)
								IF IS_PED_IN_ANY_VEHICLE(pedWorker)
								AND DOES_BLIP_EXIST(blipTruck)
									REMOVE_BLIP(blipTruck)
									IF NOT DOES_BLIP_EXIST(blipBulldozer)
										PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ blipBulldozer = CREATE_BLIP_FOR_VEHICLE(vehBulldozer) @@@@@@@@@@@@@@@@@@@@@@\n")
										blipBulldozer = CREATE_BLIP_FOR_VEHICLE(vehBulldozer)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedWorker)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_PED_IN_MODEL(PLAYER_PED_ID(), BULLDOZER)
//							FREEZE_ENTITY_POSITION(vehTruck, FALSE)
							REPEAT NUMBER_OF_FALLING_PROPS index
								IF DOES_ENTITY_EXIST(objDangerProp[index])
									FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
								ENDIF
							ENDREPEAT
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehTruck)
					IF IS_ENTITY_ON_FIRE(vehTruck)
						REPEAT NUMBER_OF_FALLING_PROPS index
							IF DOES_ENTITY_EXIST(objDangerProp[index])
								FREEZE_ENTITY_POSITION(objDangerProp[index], FALSE)
							ENDIF
						ENDREPEAT
						FREEZE_ENTITY_POSITION(vehTruck, FALSE)
						IF NOT IS_PED_INJURED(pedWorker)
							IF IS_PED_IN_ANY_VEHICLE(pedWorker)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker, TRUE)
								SET_PED_CONFIG_FLAG(pedWorker, PCF_GetOutBurningVehicle, FALSE)
								SET_PED_CONFIG_FLAG(pedWorker, PCF_GetOutUndriveableVehicle, FALSE)
								IF NOT bDieDialoguePlayed
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(0)
									CREATE_CONVERSATION(AccidentConversation, "CONACAU", "CONAC_DIE", CONV_PRIORITY_AMBIENT_HIGH)
									WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										WAIT(0)
									ENDWHILE
									bDieDialoguePlayed = TRUE
								ENDIF
								IF NOT IS_PED_INJURED(pedWorker)
									SET_ENTITY_HEALTH(pedWorker, 99)
//									EXPLODE_PED_HEAD(pedWorker)
								ENDIF
								IF NOT IS_ENTITY_DEAD(vehTruck)
									FREEZE_ENTITY_POSITION(vehTruck, FALSE)
									APPLY_FORCE_TO_ENTITY(vehTruck, APPLY_TYPE_FORCE, << 0, 0, 50 >>, << 0, 0, 0 >>, 0, TRUE, TRUE, TRUE)
									PRINTLN("@@@@@@@@@@@ APPLY_FORCE_TO_ENTITY 5 @@@@@@@@@@@")
									EXPLODE_VEHICLE(vehTruck)
								ENDIF
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_con_accident launched ")
PRINTVECTOR(vInput)
PRINTNL()

//////////////////////////////////

#IF IS_DEBUG_BUILD
//    WIDGET_ID missionTab
//    WIDGET_ID holdMissionsWidget
	
//    missionTab =
	IF NOT DOES_WIDGET_GROUP_EXIST(debugWidget)
		debugWidget = START_WIDGET_GROUP("re_con_accident")
	//        holdMissionsWidget = 
//			START_WIDGET_GROUP("Variation Selection Debug")
//	            START_NEW_WIDGET_COMBO()
//					ADD_TO_WIDGET_COMBO("CON_ACCIDENT_FALLING_PIPES")
//					ADD_TO_WIDGET_COMBO("CON_ACCIDENT_FALLING_PIPES_2")
//	            STOP_WIDGET_COMBO("con_accident_TYPE_VARIATIONS", iRuncon_accidentStage)
//				
//	            IF g_bHoldRandomEventForSelection
//	                ADD_WIDGET_BOOL("Run mission with debug selection", bRunDebugSelection)
//	            ENDIF
//	        STOP_WIDGET_GROUP()
			
	    STOP_WIDGET_GROUP()
	ENDIF
#ENDIF

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	 missionCleanup()
ENDIF

IF GET_CLOCK_HOURS() > 18 OR GET_CLOCK_HOURS() < 5
	PRINTSTRING("\n@@@@@@@@@@@@@ CONSTRUCTION ACCIDENT ONLY PLAYS BETWEEN 6 AM - 6 PM @@@@@@@@@@@@@\n")
	TERMINATE_THIS_THREAD()
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput)
	LAUNCH_RANDOM_EVENT()
ELSE
	TERMINATE_THIS_THREAD()
ENDIF


// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
//		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
		
				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
						missionCleanup()
					ENDIF
				ENDIF
				
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_CA")
				
				SWITCH con_accidentStage
				
					CASE STAGE_CREATE_COPSNCRIMS
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
//						current_time_hours = GET_CLOCK_HOURS()
//						IF current_time_hours > 18
//						OR current_time_hours < 2
							loadAssets()
							UPDATE_LOAD_QUEUE_LIGHT(sLoadQueue)
//						ELSE
//							PRINTSTRING("\n@@@@@@@@@@@@@@@@@@ WRONG TIME OF DAY @@@@@@@@@@@@@@@@@@\n")
//							missionCleanup()
//						ENDIF
					BREAK
					
					CASE STAGE_RUN_CON_ACCIDENT
//						IF accident_variation = 0
							runConAccident()
							UPDATE_LOAD_QUEUE_LIGHT(sLoadQueue)
							CONTROL_GAS_TRAILS(#IF IS_DEBUG_BUILD debugWidget #ENDIF)
//						ENDIF
					BREAK
					
					CASE STAGE_CLEAN_UP
						IF DOES_ENTITY_EXIST( pedWorker )
						AND IS_PED_DEAD_OR_DYING( pedWorker )
						AND iUpdateTrappedWorker < 5
						AND NOT bDieDialoguePlayed
							HANDLE_TRUCK_EXPLOSION()
						ENDIF
						preCleanup()
					BREAK
					
				ENDSWITCH
				
			#IF IS_DEBUG_BUILD
				
//				PRINTSTRING("\n@@@@@@@@@@@@@@@@ ALLOCATED STACK SIZE: ")PRINTINT(GET_ALLOCATED_STACK_SIZE())PRINTSTRING(" @@@@@@@@@@@@@@@@\n")
//				PRINTSTRING("\n@@@@@@@@@@@@@@@@ CURRENT STACK SIZE: ")PRINTINT(GET_CURRENT_STACK_SIZE())PRINTSTRING(" @@@@@@@@@@@@@@@@\n")
				
//				IF bRunDebugSelection
//					bRunDebugSelection = FALSE
//				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					MISSION_PASSED()
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
					MISSION_FAILED()
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					missionCleanup()
				ENDIF
				
			#ENDIF
			
//		ELSE
//			missionCleanup()
//		ENDIF
	ELSE
		missionCleanup()
	ENDIF
	
ENDWHILE


ENDSCRIPT
