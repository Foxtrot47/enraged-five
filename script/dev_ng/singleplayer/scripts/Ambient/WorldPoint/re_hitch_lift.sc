//|=======================================================================================|																				
//|                                    34. HITCH_LIFT                                     
//|                             1) Businessman in a hurry - rush to airport to get him to flight in time
//								2) Girl - gets lift, gives you her number
//								3) Girl gets a lift, you drop her off, her boyfriend thinks she is cheating, attacks you
//								4) Gay military guy gets taken back to base, gives you his number								 
//|=======================================================================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//|=====================================  INCLUDE FILES =====================================|

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "AggroSupport.sch"
USING "dialogue_public.sch"
USING "Ambient_Approach.sch"
USING "script_blips.sch"
USING "finance_control_public.sch"
USING "finance_generated_header.sch"
USING "script_debug.sch"
USING "script_ped.sch"
USING "shared_hud_displays.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "cutscene_public.sch"
USING "altruist_cult.sch"
USING "rgeneral_include.sch"
USING "stripclub_public.sch"		//Add girl to cellphone
USING "random_events_public.sch"
USING "taxi_functions.sch"
USING "commands_recording.sch"
//================================ CONSTANTS AND VARIABLES ================================

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM MAIN_SECTIONS
	MISSION_STAGES,
	AGGRO_REACTIONS
ENDENUM
MAIN_SECTIONS thisSection = MISSION_STAGES

ENUM RUNNING_STAGE_ENUM
	SETUP_STAGE, //0
	SECONDARY_SETUP_STAGE, //1
//	LOAD_REMAINING_ASSETS, //2
	STAGE_1, //3
	STAGE_2, //4
	STAGE_3, //5
	STAGE_4, //6
	AT_DESTINATION, //7
	POST_DROPOFF_1,  //8
	POST_DROPOFF_2,  //9
	TOO_LATE,
	AT_CULT
ENDENUM
RUNNING_STAGE_ENUM eventStage = SETUP_STAGE

ENUM eventVariations
	TARGET_NONE,
	BUSINESSMAN,
	GIRL_NUMBER,
	GIRL_CHEATER,
	GAY_MILITARY,
	RUNAWAY_BRIDE,
	TARGET_END
ENDENUM
eventVariations thisEvent = TARGET_NONE

#IF IS_DEBUG_BUILD
	VECTOR v_temp_player_coord
	VEHICLE_INDEX veh_player_J_car
	BOOL is_first_loc_skipped
#ENDIF

BOOL bAssetsLoaded
BOOL bVariablesInitialised

VECTOR vInput, vSpawnCoords
INT iWhichWorldPoint
VECTOR vAmbWorldPoint1 = << -3092.82, 749.80, 21.48 >>
VECTOR vAmbWorldPoint2 = << 181.8977, 4415.1523, 73.4630 >>
VECTOR vAmbWorldPoint3 = << 2743.4612, 4385.5103, 47.7521 >>
//VECTOR vAmbWorldPoint4 = << 1839.71, 3276.00, 43.84 >>
VECTOR vAmbWorldPoint5 = << -382.1346, 2817.9712, 44.0671 >>

CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
CONST_INT iWorldPoint3	3
CONST_INT iWorldPoint4	4
CONST_INT iWorldPoint5	5
INT iSelectedVariation

BLIP_INDEX blipLocation
BLIP_INDEX blipCult

PED_INDEX pedHitcher
PED_INDEX pedBoyfriend

VEHICLE_INDEX veh_current_player_vehicle //NEED TO UPDATE ALL THESE veh_current_player_vehicle TO GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) OR GET_PLAYERS_LAST_VEHICLE()

OBJECT_INDEX objCard

SEQUENCE_INDEX seq

VECTOR vCreateHitchhikerPos
//VECTOR vHitcherWalkToPos
VECTOR vHitcherDestinationPos
FLOAT fCreateHitchhikerHdg
VECTOR vCult = << -1034.6, 4918.6, 205.9 >>
//VECTOR vDropOffWalkToPos
MODEL_NAMES modelHitcher, modelBoyfriend, modelVehicle

BOOL bHasHitcherThanksAsEnters

VECTOR vCreateBoyfriendPos
FLOAT fCreateBoyfriendHdg

//INT iBitFieldDontCheck = 0
//INT iLockOnTimer
INT iChatInVehicle
INT iChaseDialogue
INT iStartDialogueTimer
//EAggro aggroReason

BOOL bLeavingActivationArea
BOOL bBoyfriendCreated
BOOL bGirlAttacked
BOOL bChasedByGroom
BOOL bBoyfriendCombatPlayer
BOOL bGroomScared
BOOL bPlayerReactingToChase

//BOOL bSaidThanks
BOOL bSetUpDirectedEvent
//BOOL bStartScene
CAMERA_INDEX camConsol

BLIP_INDEX blipInitialMarker
BLIP_INDEX blipHitcher
BLIP_INDEX blipBoyfriend
BLIP_INDEX blipVehBoyfriend

//STRING HailAnim
BOOL bPlaceholderLine
BOOL bConversationPaused
BOOL bPushInToFirstPerson

//BOOL bConvStarted
BOOL bConvOnHold
TEXT_LABEL_23 ConvResumeLabel = ""
TEXT_LABEL_23 strCurrentRoot

INT iApproachStage
BOOL bAskedForVeh
BOOL bUnsuitableVehRemark

BOOL bDoFullCleanUp

BOOL bToldForGettingOut
BOOL bToldForJacking
BOOL bToldForShooting

BOOL bStartOnFootTimer
INT iOnFootStartTimer
INT iOnFootCurrentTimer

BOOL bStartStationaryTimer
BOOL bStationaryWarned
INT iStationaryStartTimer
INT iStationaryHurryTimer
INT iStationaryCurrentTimer

//INT iTurnStage

INT iCurrentGameTimeInMilliseconds
//INT iStartTimeFlight
//INT iEndTimeFlight 
//INT iCurrentTimeFlight
//INT iTimeLeftFlight
//INT iTimeLimitFlight

INT iVehiclePassedCounter
INT iVehiclePassedTimer
INT iEventCurrentTimer

INT iGirlsReactions

VECTOR vWalkIntoCampPos

BOOL bToldToHurryOnce
BOOL bToldToHurryTwice

BOOL bTimerQuick
BOOL bTimerQuicker

BOOL bGirlWalking
BOOL bGoneToInjuredBoyfriend
BOOL bGirlPlayingPanicAnim
BOOL bGirlReactedToWeapon

VEHICLE_INDEX viHitcherVehicle
VEHICLE_INDEX vehicleCandidate

//INT iCurrMin, iCurrHour
INT iTimeLimitMin, iTimeLimitHour
BOOL bTimeOut
BOOL bSaidTime

INT iSoundCountdown

STRING strDialogueBlock
STRING strDialogueCharacter
STRING sDialogueBoyfriend
STRING sAmbientVoiceName

INT iTrackingTrafficStart
INT iTrackingTrafficCurrent

FLOAT fPlayerHitcherDist

VECTOR vGangWarpPosition
FLOAT fGangWarpHeading
//VECTOR vPlayerWarpPosition
//FLOAT fPlayerWarpHeading
FLOAT fPlayerHeadings
VECTOR vPlayerPositions
INT iTimerWarp
TWEAK_FLOAT BLIP_SLIDE_AMOUNT 1.0

//FLOAT fOpenRatio

REL_GROUP_HASH rghBoyfriend
structPedsForConversation HitcherDialogueStruct 

//APPROACHPLAYER DoThanking

//============================ GENERAL FUNCTIONS AND PROCEDURES ===========================

// Mission Cleanup
PROC missionCleanup()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@@@ \n")
	IF bDoFullCleanUp
	
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		IF g_bAltruistCultFinalDialogueHasPlayed
			TRIGGER_MUSIC_EVENT("AC_STOP")
		ENDIF
		RESET_CULT_STATUS()
		REMOVE_SCENARIO_BLOCKING_AREAS()
		DISABLE_TAXI_HAILING(FALSE)
		
		
		//Cancel any hint cams
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT(FALSE)
		ENDIF
		
		IF IS_CODE_GAMEPLAY_HINT_ACTIVE()
			STOP_CODE_GAMEPLAY_HINT(FALSE)
		ENDIF
		
//		IF HAS_ANIM_DICT_LOADED("AMB@HITCHHIKING")
//			REMOVE_ANIM_DICT("AMB@HITCHHIKING")
//		ENDIF
		g_AmbientAreaData[eAMBAREA_ARMYBASE].bSuppressGateGuards = FALSE
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//		SET_IGNORE_NO_GPS_FLAG(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION()
		IF DOES_ENTITY_EXIST(pedHitcher)
//			SET_PED_AS_NO_LONGER_NEEDED(pedHitcher)
			IF NOT IS_PED_INJURED(pedHitcher)
				SET_PED_CONFIG_FLAG(pedHitcher, PCF_CanSayFollowedByPlayerAudio, TRUE)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedHitcher)
					RESET_PED_LAST_VEHICLE(pedHitcher)
				ENDIF
				
				//Don't clear tasks for variation 2 b*1988159
				IF thisEvent != GIRL_NUMBER
					CLEAR_PED_SECONDARY_TASK(pedHitcher)
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_SMART_FLEE_PED) != WAITING_TO_START_TASK
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHitcher, FALSE)
				ENDIF
				SET_PED_CAN_BE_TARGETTED(pedHitcher, TRUE)
				IF IS_PED_IN_GROUP(pedHitcher)
					REMOVE_PED_FROM_GROUP(pedHitcher)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedBoyfriend)
//			SET_PED_AS_NO_LONGER_NEEDED(pedBoyfriend)
			IF NOT IS_PED_INJURED(pedBoyfriend)
				IF NOT IS_PED_FLEEING(pedBoyfriend)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoyfriend, FALSE)
				ENDIF
			ENDIF
		ENDIF
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1585.486938,2797.353271,17.827217>>, 3.5, PROP_SEC_BARIER_04B)
//		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARIER_04B, <<-1585.486938,2797.353271,17.827217>>, TRUE, 0.0)
//	ENDIF
	RELEASE_SCRIPT_AUDIO_BANK()
	
	#IF IS_DEBUG_BUILD
		IF can_cleanup_script_for_debug
			IF DOES_ENTITY_EXIST(pedHitcher)
				DELETE_PED(pedHitcher)
			ENDIF
			
			IF DOES_ENTITY_EXIST(veh_player_J_car)
				IF IS_VEHICLE_DRIVEABLE(veh_player_J_car)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_MODEL_AS_NO_LONGER_NEEDED(FBI)
						
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_player_J_car)
							DELETE_VEHICLE(veh_player_J_car)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_player_J_car)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
 	#ENDIF
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<801.714844,1270.138306,359.285522>>, 6.0, prop_fnclink_03gate1)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<801.714844,1270.138306,359.285522>>, FALSE, 0.0)
		ENDIF
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<802.921814,1281.675049,359.296234>>, 6.0, prop_fnclink_03gate1)
	   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<802.921814,1281.675049,359.296234>>, FALSE, 0.0)
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	// Do none of the above, just what is below, if the script has been set to clean up straight away
	//  v               v               v         v             v            v           v           v            v         v         v
	TERMINATE_THIS_THREAD()

ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, 5)
	RANDOM_EVENT_PASSED(RE_HITCHLIFT, iSelectedVariation)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	KILL_FACE_TO_FACE_CONVERSATION()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	missionCleanup()
ENDPROC

FUNC VECTOR getWorldPoint()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint1)
		iWhichWorldPoint = iWorldPoint1
		
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) 
			iWhichWorldPoint = iWorldPoint2
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) 
			iWhichWorldPoint = iWorldPoint3
		ENDIF
		
//		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint4) < fDistanceToClosestWorldPoint
//			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint4) 
//			iWhichWorldPoint = iWorldPoint4
//		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint5) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint5) 
			iWhichWorldPoint = iWorldPoint5
		ENDIF
		
		IF iWhichWorldPoint = iWorldPoint1
			iSelectedVariation = iWorldPoint1
			thisEvent = BUSINESSMAN
			RETURN vAmbWorldPoint1
		ELIF iWhichWorldPoint = iWorldPoint2
			iSelectedVariation = iWorldPoint2
			thisEvent = GIRL_NUMBER 
			RETURN vAmbWorldPoint2
		ELIF iWhichWorldPoint = iWorldPoint3
			iSelectedVariation = iWorldPoint3
			thisEvent = GIRL_CHEATER
			RETURN vAmbWorldPoint3
//		ELIF iWhichWorldPoint = iWorldPoint4
//			iSelectedVariation = iWorldPoint4
//			thisEvent = GAY_MILITARY
//			RETURN vAmbWorldPoint4
		ELIF iWhichWorldPoint = iWorldPoint5
			iSelectedVariation = iWorldPoint4 //Paul has updated the header to refer to only 4 Hitch Lifts since one has been cut
			thisEvent = RUNAWAY_BRIDE
			RETURN vAmbWorldPoint5
		ENDIF
		
	ENDIF
	
	RETURN << 0, 0, 0 >>
	
ENDFUNC

PROC initialiseEventVariables()
	
	IF thisEvent = BUSINESSMAN
		vCreateHitchhikerPos = << -3086.0525, 735.5091, 20.4883 >>
		fCreateHitchhikerHdg = 17.9234
		vHitcherDestinationPos = << -1053.3442, -2539.3267, 19.0394 >>
		modelHitcher = A_M_M_Business_01
		strDialogueBlock = "REHH1AU"
		strDialogueCharacter = "BUSINESSMAN"
		sAmbientVoiceName = "BUSINESSMAN"
		modelVehicle = SCHAFTER2
		
	ELIF thisEvent = GIRL_NUMBER
		vCreateHitchhikerPos = << 196.7701, 4427.7573, 72.2673 >>
		fCreateHitchhikerHdg = 75.9173
		vHitcherDestinationPos = << 3328.8276, 5154.4561, 17.2894 >>
		modelHitcher = A_F_Y_HIKER_01
	//	HailAnim = "thumbing_lift_b"
		strDialogueBlock = "REHH2AU"
		strDialogueCharacter = "REHH2Hiker"
		sAmbientVoiceName = "REHH2Hiker"
		
	ELIF thisEvent = GIRL_CHEATER
		vCreateHitchhikerPos = << 2745.0264, 4386.8267, 47.8636 >>
		fCreateHitchhikerHdg = 19.4143
		vHitcherDestinationPos = << 814.4550, 1274.7966, 359.5112 >>
		vCreateBoyfriendPos = << 804.2382, 1269.5895, 359.3557 >>
		fCreateBoyfriendHdg = 340.7372
		modelHitcher = A_F_Y_HIPSTER_03
		modelBoyfriend = S_M_M_Security_01
	//	HailAnim = "thumbing_lift_d"
		strDialogueBlock = "REHH3AU"
		strDialogueCharacter = "REHH3Hipster"
		sAmbientVoiceName = "REHH3Hipster"
		sDialogueBoyfriend = "REHH3Security"
		
//	ELIF thisEvent = GAY_MILITARY
//		vCreateHitchhikerPos = << 1822.3809, 3301.2581, 41.8496 >>
//		fCreateHitchhikerHdg = 180.9142
//		vHitcherDestinationPos = << -1567.3652, 2775.1194, 16.2338 >>
//		vWalkIntoCampPos = << -1584.8746, 2791.9741, 15.9009 >>
//		modelHitcher = S_M_Y_MARINE_02
//	//	HailAnim = "thumbing_lift_c"
//		strDialogueBlock = "REHH4AU"
//		strDialogueCharacter = "GAYMILITARY"
//		sAmbientVoiceName = "A_M_Y_Hippy_01_WHITE_MINI_01"
		
	ELIF thisEvent = RUNAWAY_BRIDE
		vCreateHitchhikerPos = << -324.0379, 2818.0339, 58.4498 >>
		fCreateHitchhikerHdg = 56.5422
		vHitcherDestinationPos = << -344.0339, 634.4108, 171.2902 >>
		modelHitcher = IG_Bride //A_F_Y_Bevhills_04
		modelBoyfriend = A_M_Y_Business_01
		strDialogueBlock = "REHH5AU"
		strDialogueCharacter = "REHH5Bride"
		sAmbientVoiceName = "REHH5Bride"
		sDialogueBoyfriend = "REHH5Groom"
		modelVehicle = PATRIOT//GAUNTLET//TORNADO
	ENDIF
	
	bVariablesInitialised = TRUE
ENDPROC

PROC loadInitialAssets()

	REQUEST_MODEL(modelHitcher)
	REQUEST_ANIM_DICT("random@hitch_lift")
	IF thisEvent = BUSINESSMAN
	OR thisEvent = RUNAWAY_BRIDE
		REQUEST_MODEL(modelVehicle)
//		REQUEST_ANIM_DICT("random@thanks")
	ENDIF
	IF thisEvent = BUSINESSMAN
//	OR thisEvent = GAY_MILITARY
		REGISTER_SCRIPT_WITH_AUDIO()
		REQUEST_SCRIPT_AUDIO_BANK("timer")
	ELIF thisEvent = GIRL_CHEATER
		REQUEST_ANIM_DICT("facials@gen_female@base")
	ENDIF
	IF HAS_MODEL_LOADED(modelHitcher)
	AND HAS_ANIM_DICT_LOADED("random@hitch_lift")
		IF thisEvent = BUSINESSMAN
		OR thisEvent = RUNAWAY_BRIDE
			IF HAS_MODEL_LOADED(modelVehicle)
//			AND HAS_ANIM_DICT_LOADED("random@thanks")
				bAssetsLoaded = TRUE
			ENDIF
		ELIF thisEvent = GIRL_CHEATER
			IF HAS_ANIM_DICT_LOADED("facials@gen_female@base")
				bAssetsLoaded = TRUE
			ENDIF
		ELSE
			bAssetsLoaded = TRUE
		ENDIF
	ELSE
		REQUEST_MODEL(modelHitcher)
		REQUEST_ANIM_DICT("random@hitch_lift")
		IF thisEvent = BUSINESSMAN
		OR thisEvent = RUNAWAY_BRIDE
			REQUEST_MODEL(modelVehicle)
//			REQUEST_ANIM_DICT("random@thanks")
		ELIF thisEvent = GIRL_CHEATER
			REQUEST_ANIM_DICT("facials@gen_female@base")
		ENDIF
	ENDIF
	
ENDPROC	
//
//PROC loadRemainingAssets()
//
//	IF thisEvent = GIRL_CHEATER
//	OR thisEvent = RUNAWAY_BRIDE
//		REQUEST_MODEL(modelBoyfriend)
//		IF HAS_MODEL_LOADED(modelBoyfriend)
//			eventStage = STAGE_1
//		ENDIF
//	ELSE
//		eventStage = STAGE_1
//	ENDIF
//	
//ENDPROC

PROC CHANGE_BLIPS_AND_ADD_SPEAKERS()

	IF DOES_BLIP_EXIST(blipInitialMarker)
		REMOVE_BLIP(blipInitialMarker)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedHitcher)
		IF NOT DOES_BLIP_EXIST(blipHitcher)
			IF NOT IS_ENTITY_DEAD(pedHitcher)
				blipHitcher = CREATE_BLIP_FOR_PED(pedHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, FALSE)
			ENDIF
		ENDIF
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			SET_RANDOM_EVENT_ACTIVE()
		ENDIF
		ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 3, pedHitcher, strDialogueCharacter)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 1,  PLAYER_PED_ID(), "FRANKLIN")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 2,  PLAYER_PED_ID(), "TREVOR")
	ENDIF

ENDPROC

PROC createScene()
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	pedHitcher = CREATE_PED(PEDTYPE_MISSION, modelHitcher, vCreateHitchhikerPos, fCreateHitchhikerHdg)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelHitcher)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHitcher, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedHitcher, FALSE)
	SET_AMBIENT_VOICE_NAME(pedHitcher, sAmbientVoiceName)
	SET_PED_CONFIG_FLAG(pedHitcher, PCF_RemoveDeadExtraFarAway, TRUE)
	SET_PED_CONFIG_FLAG(pedHitcher, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedHitcher, FA_DISABLE_COWER, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedHitcher, FA_FORCE_EXIT_VEHICLE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedHitcher, FA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedHitcher, CA_ALWAYS_FLEE, TRUE)
	SET_PED_MAX_TIME_IN_WATER(pedHitcher, 60)
	SET_PED_LOD_MULTIPLIER(pedHitcher, 5)
	//	OR thisEvent = GAY_MILITARY
//		TASK_START_SCENARIO_AT_POSITION(pedHitcher, "THUMBING_LIFT", vCreateHitchhikerPos, fCreateHitchhikerHdg)
	
	IF thisEvent = BUSINESSMAN
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HEAD, 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HAIR, 1, 2, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_TORSO, 0, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_LEG, 0, 2, 0) //(lowr)
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
		viHitcherVehicle = CREATE_VEHICLE(modelVehicle, <<-3088.0684, 730.4819, 20.3028>>, 332.2996)
		SET_VEHICLE_ENGINE_HEALTH(viHitcherVehicle, 150)
		SET_VEHICLE_DOOR_OPEN(viHitcherVehicle, SC_DOOR_BONNET)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viHitcherVehicle, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle)
		
		TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
	ELIF thisEvent = GIRL_NUMBER
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HEAD, 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HAIR, 0, 1, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_TORSO, 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_LEG, 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_SPECIAL, 1, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_DECL, 0, 0, 0) //(decl)
		
		TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
		
		ADD_SCENARIO_BLOCKING_AREA(vHitcherDestinationPos - << 50, 50, 20 >>, vHitcherDestinationPos + << 50, 50, 20 >>)
		DISABLE_TAXI_HAILING(TRUE)
	ELIF thisEvent = GIRL_CHEATER
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HEAD, 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HAIR, 1, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_TORSO, 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_LEG, 1, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		
		TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
		
		ADD_SCENARIO_BLOCKING_AREA(vHitcherDestinationPos - << 50, 50, 20 >>, vHitcherDestinationPos + << 50, 50, 20 >>)
//	ELIF thisEvent = GAY_MILITARY
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HEAD, 0, 2, 0) //(head)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_TORSO, 1, 0, 0) //(uppr)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_LEG, 0, 0, 0) //(lowr)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
//		
//		SET_PED_CONFIG_FLAG(pedHitcher, PCF_DontBlipCop, TRUE)
//		
//		TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)//|AF_UPPERBODY|AF_SECONDARY)
////		ADD_SCENARIO_BLOCKING_AREA(<< -1589.6945, 2794.8577, 15.9075 >> - << 15, 15, 15 >>, << -1589.6945, 2794.8577, 15.9075 >> + << 15, 15, 15 >>)
	ELIF thisEvent = RUNAWAY_BRIDE
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HEAD, 0, 0, 0) //(head)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_HAIR, 0, 0, 0) //(hair)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_TORSO, 0, 1, 0) //(uppr)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_LEG, 0, 0, 0) //(lowr)
//		SET_PED_COMPONENT_VARIATION(pedHitcher, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		
//		TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
	ENDIF
	
	eventStage = SECONDARY_SETUP_STAGE
ENDPROC

PROC setupInitialTasks()
	IF thisEvent = BUSINESSMAN
		IF DOES_ENTITY_EXIST(pedHitcher)
			IF NOT IS_ENTITY_DEAD(pedHitcher)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -3024.2648, 660.8264, 1.1982 >>, << -3140.8234, 803.7070, 35.1310 >>, 93)
				AND NOT IS_ENTITY_OCCLUDED(pedHitcher)
				OR GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 12
					CHANGE_BLIPS_AND_ADD_SPEAKERS()
					eventStage = STAGE_1
				ENDIF
			ENDIF
		ENDIF
	ELIF thisEvent = GIRL_NUMBER
		IF NOT bGirlWalking
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateHitchhikerPos, << 100, 100, 100 >>)
			AND NOT IS_PED_INJURED(pedHitcher)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 178.9930, 4413.0972, 73.6132 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 159.6669, 4413.1973, 74.8915 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 142.7866, 4415.1108, 74.3799 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedHitcher, seq)
				CLEAR_SEQUENCE_TASK(seq)
				bGirlWalking = TRUE
			ENDIF
		ENDIF
//		IF thisEvent = GIRL_CHEATER
//			IF NOT bGirlWalking
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateHitchhikerPos, << 60, 60, 50 >>)
//				AND NOT IS_PED_INJURED(pedHitcher)
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1702.3156, 4668.1382, 42.2453 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1701.8243, 4662.0132, 42.3664 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1701.6089, 4656.5942, 42.4695 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1702.1088, 4650.4458, 42.5375 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1704.0215, 4642.6094, 42.5441 >> , PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1708.2430, 4634.5469, 42.3532 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					bGirlWalking = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
	ELIF thisEvent = RUNAWAY_BRIDE
		IF NOT bGirlWalking
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateHitchhikerPos, << 200, 200, 200 >>)
			AND NOT IS_PED_INJURED(pedHitcher)
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
					TASK_PLAY_ANIM(NULL, "random@hitch_lift", "001445_01_gangintimidation_1_female_idle_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -337.3163, 2828.0181, 55.2198 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -343.4485, 2826.2224, 54.5089 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -356.4033, 2811.0620, 51.5931 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -365.6846, 2809.1584, 48.7646 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -382.4106, 2818.3838, 44.0780 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -370.3838, 2866.6768, 41.0979 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					TASK_PLAY_ANIM(NULL, "random@hitch_lift", "carjack_mainloop_female", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY)//|AF_UPPERBODY)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedHitcher, seq)
				CLEAR_SEQUENCE_TASK(seq)
				bGirlWalking = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF thisEvent != BUSINESSMAN
		IF DOES_ENTITY_EXIST(pedHitcher)
			IF NOT IS_ENTITY_DEAD(pedHitcher)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 95, 95, 50 >>)
				AND NOT IS_ENTITY_OCCLUDED(pedHitcher)
				OR GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 12
					CHANGE_BLIPS_AND_ADD_SPEAKERS()
					eventStage = STAGE_1
				ENDIF
			ENDIF
		ENDIF
	ENDIF
//	blipInitialMarker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vCreateHitchhikerPos)
//	CHANGE_BLIPS_AND_ADD_SPEAKERS()
	
ENDPROC

FUNC BOOL IS_THIS_LABEL_PLAYING(STRING sRootLabel)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	    
	    IF ARE_STRINGS_EQUAL(sRootLabel, txtRootLabel)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
	IF IS_THIS_CONVERSATION_PLAYING("REHH1_VEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_VEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_VEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH4_VEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_VEH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_VEH2")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_VEH2")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_VEH2")
	OR IS_THIS_CONVERSATION_PLAYING("REHH4_VEH2")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_VEH2")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_WLK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_WLK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_WLK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH4_WLK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_WLK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_SLR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH4_SLR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_SLW")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_SLW")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_SLW")
	OR IS_THIS_CONVERSATION_PLAYING("REHH4_SLW")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_SLW")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_OUT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_GETOUT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_GETOUT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_GETOUT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_JACK")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_GUN")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_SHOOT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_SHOOT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_SHOO")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_1HOUR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_1HOUR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_HHOUR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_HHOUR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_CRASH")
	OR IS_THIS_CONVERSATION_PLAYING("REHH1_WAY")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_NEAR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_NEAR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_NEAR")
	OR IS_THIS_CONVERSATION_PLAYING("REHH2_CULT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH3_CULT")
	OR IS_THIS_CONVERSATION_PLAYING("REHH5_CULT")
//	OR IS_THIS_CONVERSATION_PLAYING("REHH1_CHATe")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC MANAGE_CONVERSATION(BOOL bCanPlay)
	IF bCanPlay
		IF bConvOnHold
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(HitcherDialogueStruct, strDialogueBlock, strCurrentRoot, ConvResumeLabel, CONV_PRIORITY_HIGH)
				PRINTLN("@@@@@@@@@@ CREATE_CONVERSATION_FROM_SPECIFIC_LINE: ", ConvResumeLabel, " ", ConvResumeLabel, " @@@@@@@@@@")
				bConvOnHold = FALSE
			ENDIF
		ENDIF
	ELIF NOT bConvOnHold
	AND IS_SCRIPTED_CONVERSATION_ONGOING()
	AND NOT IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
		strCurrentRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		ConvResumeLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		PRINTLN("@@@@@@@@@@ SAVED ROOT: ", strCurrentRoot, " @@@ SAVED LABEL: ", ConvResumeLabel, " @@@@@@@@@@")
		KILL_FACE_TO_FACE_CONVERSATION()
		bConvOnHold = TRUE
	ENDIF
ENDPROC

//FUNC VECTOR GET_OFFSET_FOR_NEAREST_CAR_WINDOW()
//	VECTOR vRightWindow
//	VECTOR vLeftWindow
//	VECTOR vHitcherCoords
//	vHitcherCoords = GET_ENTITY_COORDS(pedHitcher)
//	IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//		vRightWindow = GET_WORLD_POSITION_OF_ENTITY_BONE(veh_current_player_vehicle, GET_ENTITY_BONE_INDEX_BY_NAME(veh_current_player_vehicle, "window_rf"))
//		vLeftWindow = GET_WORLD_POSITION_OF_ENTITY_BONE(veh_current_player_vehicle, GET_ENTITY_BONE_INDEX_BY_NAME(veh_current_player_vehicle, "window_lf"))
//	ENDIF
//	
//	IF ((GET_DISTANCE_BETWEEN_COORDS(vRightWindow, vHitcherCoords)) < (GET_DISTANCE_BETWEEN_COORDS(vLeftWindow, vHitcherCoords)))
//		RETURN GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), vLeftWindow)
//	ELSE
//		RETURN GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), vRightWindow)
//	ENDIF
//
//ENDFUNC

//FUNC VECTOR GET_COORDS_FOR_NEAREST_CAR_WINDOW()
//	VECTOR vRightWindow
//	VECTOR vLeftWindow
//	VECTOR vHitcherCoords
//	vHitcherCoords = GET_ENTITY_COORDS(pedHitcher)
//	veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//	vRightWindow = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_current_player_vehicle, <<-0.5, 0.2, 0>>)
//	vLeftWindow = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_current_player_vehicle, <<-0.5, -0.2, 0>>)
//	
//	IF (GET_DISTANCE_BETWEEN_COORDS(vRightWindow, vHitcherCoords) < GET_DISTANCE_BETWEEN_COORDS(vLeftWindow, vHitcherCoords))
//		RETURN vRightWindow
//	ELSE
//		RETURN vLeftWindow
//	ENDIF
//
//ENDFUNC

PROC trackingCarsHitching()

	IF (iEventCurrentTimer - iVehiclePassedTimer) > 1000
		vehicleCandidate = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(pedHitcher, FALSE), 18, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	ENDIF
	IF vehicleCandidate <> NULL
		iVehiclePassedCounter ++
		iVehiclePassedTimer = GET_GAME_TIMER()
		INT iHowManyVehicles
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			iHowManyVehicles = 1
		ELSE
			iHowManyVehicles = 2
		ENDIF
		IF iVehiclePassedCounter > iHowManyVehicles
			SET_ENTITY_AS_MISSION_ENTITY(vehicleCandidate)
			IF IS_VEHICLE_DRIVEABLE(vehicleCandidate)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicleCandidate)
					IF thisEvent = BUSINESSMAN
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_STO", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(pedHitcher)
								TASK_LOOK_AT_ENTITY(pedHitcher, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							ENDIF
						ENDIF
						iTrackingTrafficStart = GET_GAME_TIMER()
						iVehiclePassedCounter = 0
					ELIF thisEvent = GIRL_NUMBER
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_STO", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(pedHitcher)
								TASK_LOOK_AT_ENTITY(pedHitcher, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							ENDIF
						ENDIF
						iTrackingTrafficStart = GET_GAME_TIMER()
						iVehiclePassedCounter = 0
					ELIF thisEvent = GIRL_CHEATER
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_STO", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(pedHitcher)
								TASK_LOOK_AT_ENTITY(pedHitcher, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							ENDIF
						ENDIF
						iTrackingTrafficStart = GET_GAME_TIMER()
						iVehiclePassedCounter = 0
//					ELIF thisEvent = GAY_MILITARY
//						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_STO", CONV_PRIORITY_AMBIENT_HIGH)
//							IF NOT IS_PED_INJURED(pedHitcher)
//								TASK_LOOK_AT_ENTITY(pedHitcher, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
//							ENDIF
//						ENDIF
//						iTrackingTrafficStart = GET_GAME_TIMER()
//						iVehiclePassedCounter = 0
					ELIF thisEvent = RUNAWAY_BRIDE
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_STO", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(pedHitcher)
								TASK_LOOK_AT_ENTITY(pedHitcher, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							ENDIF
						ENDIF
						iTrackingTrafficStart = GET_GAME_TIMER()
						iVehiclePassedCounter = 0
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iTrackingTrafficCurrent = GET_GAME_TIMER()
			IF (iTrackingTrafficCurrent - iTrackingTrafficStart) > 3500
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleCandidate)
			ENDIF
			vehicleCandidate = NULL
		ENDIF
	ENDIF
	
ENDPROC

//PROC GET_STOP_WATCH_FROM_MILLISECONDS(INT Milliseconds, INT &Minutes, INT &Seconds, INT &CentiSeconds)
//    INT x
// 	CentiSeconds = (Milliseconds/10) % 100
//    x = Milliseconds / 1000
//    Seconds = x % 60
//
// 	minutes = ((milliseconds / 1000) / 60)
//ENDPROC
//PROC DRAW_COUNTDOWN(INT iRaceTime, BOOL bFlash = FALSE)
//    INT iR, iG, iB, iA
//    INT iMinutes, iSeconds,  iTemp
//
//	iminutes = iRaceTime/60000    
//     
//    iseconds = iRaceTime/1000
//    itemp = iminutes *60
//    iseconds -= itemp
//	
//    IF iseconds <=0
//        iseconds +=60
//    ENDIF
//    IF iseconds = 60
//         iseconds =00
//    ENDIF
//       
//    IF iseconds < 10
//	    GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
//	    SET_TEXT_COLOUR(iR, iG, iB, iA)
//    ENDIF
//      
//    IF bFlash
//        GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
//        SET_TEXT_COLOUR(iR, iG, iB, iA)
//    ENDIF
//    
//    DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "RACE_TIME", iminutes, iseconds)
//ENDPROC

PROC timeLimitCountdown()
	
//    iCurrentTimeFlight = GET_GAME_TIMER()
//    iTimeLeftFlight = ((iStartTimeFlight + iTimeLimitFlight) - iCurrentTimeFlight)
	
	iCurrentGameTimeInMilliseconds = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000
	iCurrentGameTimeInMilliseconds += GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60
	
	IF NOT bToldToHurryTwice
		// Time of Day
//		IF iCurrentGameTimeInMilliseconds > 719999 // IF the time is more than 11:59(am)
//			IF iCurrentGameTimeInMilliseconds > 779999 // IF the time is more than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds -720000, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKPM) // show evrything from 13:00 to 23:59 - 12:00 = as 01:00(pm) to 11:59(pm)
//			ELSE // IF the time is more than 11:59(am) but less than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKPM) // show as 24-hour clock (normal)
//			ENDIF
//			
//		ELSE // ELSE the time is less than 12:00(pm)
//			IF iCurrentGameTimeInMilliseconds < 60000 // IF the time is less than 01:00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds +720000, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKAM) // show evrything from 00:00(am) to 00:59(am) + 12:00(am) = as 12:00(am) to 12.59
//			ELSE // IF the time is less than 12:00(pm) but more than 01:00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKAM) // show as 24-hour clock (normal)
//			ENDIF
//		ENDIF
		// Time Limit
		IF iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		OR iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
			DRAW_GENERIC_TIMER((iTimeLimitHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60) 
			+ (iTimeLimitMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000) -1440000, "TIMER_TIME")//, 0, TIMER_STYLE_DONTUSEMILLISECONDS, 60000)
		ELSE
			DRAW_GENERIC_TIMER((iTimeLimitHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60) 
			+ (iTimeLimitMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000), "TIMER_TIME")//, 0, TIMER_STYLE_DONTUSEMILLISECONDS, 60000)
		ENDIF
		
	ELSE
		// Time of Day
//		IF iCurrentGameTimeInMilliseconds > 719999 // IF the time is more than 11:59(am)
//			IF iCurrentGameTimeInMilliseconds > 779999 // IF the time is more than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds -720000, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKPM, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED) // show evrything from 13:00 to 23:59 - 12:00 = as 01:00(pm) to 11:59(pm)
//			ELSE // IF the time is more than 11:59(am) but less than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKPM, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED) // show as 24-hour clock (normal)
//			ENDIF
//			
//		ELSE // ELSE the time is less than 12:00(pm)
//			IF iCurrentGameTimeInMilliseconds < 60000 // IF the time is less than 01:00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds +720000, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKAM, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED) // show evrything from 00:00(am) to 00:59(am) + 12:00(am) = as 12:00(am) to 12.59
//			ELSE // IF the time is less than 12:00(pm) but more than 01.00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "LOB_CAT_15", 0, TIMER_STYLE_CLOCKAM, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED) // show as 24-hour clock (normal)
//			ENDIF
//		ENDIF
		// Time Limit
		IF iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		OR iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
			DRAW_GENERIC_TIMER((iTimeLimitHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60) 
			+ (iTimeLimitMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000) -1440000, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
		ELSE
			DRAW_GENERIC_TIMER((iTimeLimitHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60) 
			+ (iTimeLimitMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000), "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
		ENDIF
	ENDIF
	
//	INT iR, iG, iB, iA
// 	GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
//	SET_TEXT_COLOUR(iR, iG, iB, iA)
//	IF GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) < 12
//		IF GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) < 10
//			DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "CURRENT_TIME_A0", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
//	    ELSE
//			DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "CURRENT_TIME_AM", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
// 		ENDIF
//	ELSE
//		IF GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) < 10
//			DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "CURRENT_TIME_P0", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
//		ELSE
//			DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "CURRENT_TIME_PM", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
//		ENDIF
//	ENDIF
//	PRINTNL()
//	PRINTINT(iTimeLeftFlight)
//	PRINTNL()

//	PRINTNL()
//	PRINTINT(GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())+1)PRINTSTRING(":")PRINTINT(GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())-30)
//	PRINTNL()
//	PRINTNL()
//	PRINTINT(iTimeLimitHour)PRINTSTRING(":")PRINTINT(iTimeLimitMin)
//	PRINTNL()

//	IF iTimeLeftFlight < 120000
	IF (iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +1
	AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
	OR (iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
	AND iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +25
	AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
		IF NOT bToldToHurryOnce
			IF NOT IS_THIS_CONVERSATION_PLAYING("REHH1_1HOUR")
			AND NOT IS_THIS_CONVERSATION_PLAYING("REHH4_1HOUR")
				MANAGE_CONVERSATION(FALSE)
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF thisEvent = BUSINESSMAN
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_1HOUR", CONV_PRIORITY_AMBIENT_HIGH)
//				ELIF thisEvent = GAY_MILITARY
//					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_1HOUR", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				bToldToHurryOnce = TRUE
			ENDIF
		ENDIF
	ENDIF
//	IF iTimeLeftFlight < 60000
	IF iTimeLimitMin = 30
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) +30
		OR (iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		AND iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +24
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) +30)
			IF NOT bToldToHurryTwice
				IF NOT IS_THIS_CONVERSATION_PLAYING("REHH1_HHOUR")
				AND NOT IS_THIS_CONVERSATION_PLAYING("REHH4_HHOUR")
					MANAGE_CONVERSATION(FALSE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
//					ELIF thisEvent = GAY_MILITARY
//						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bToldToHurryTwice = TRUE
				ENDIF
				IF NOT bTimerQuick
					iSoundCountdown = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundCountdown, "VARIABLE_COUNTDOWN_CLOCK_wp")
					SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fadein", 35)
					START_AUDIO_SCENE("TIMER_SCENE")
					bTimerQuick = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) +5
		OR (iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		AND iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +24
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) +5)
			IF NOT bTimerQuicker
				SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fast", 1)
				bTimerQuicker = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF iTimeLimitMin = 0
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +1
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) -30
		OR (iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		AND iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +25
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) -30)
			IF NOT bToldToHurryTwice
				IF NOT IS_THIS_CONVERSATION_PLAYING("REHH1_HHOUR")
				AND NOT IS_THIS_CONVERSATION_PLAYING("REHH4_HHOUR")
					MANAGE_CONVERSATION(FALSE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
//					ELIF thisEvent = GAY_MILITARY
//						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bToldToHurryTwice = TRUE
				ENDIF
				IF NOT bTimerQuick
					iSoundCountdown = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundCountdown, "VARIABLE_COUNTDOWN_CLOCK_wp")
					SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fadein", 35)
					START_AUDIO_SCENE("TIMER_SCENE")
					bTimerQuick = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +1
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) -55
		OR (iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
		AND iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +25
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) -55)
			IF NOT bTimerQuicker
				SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fast", 1)
				bTimerQuicker = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
//	IF iTimeLeftFlight < 0
	IF iTimeLimitHour >= 24 AND iCurrentGameTimeInMilliseconds < 150000
//		PRINTSTRING("\n@@@@@@@@@ HOUR LIMIT: ")PRINTINT(iTimeLimitHour -24)PRINTSTRING(" MINUTE LIMIT: ")PRINTINT(iTimeLimitMin)PRINTSTRING(" @@@@@@@@@")
//		PRINTSTRING("\n@@@@@@@@@ HOUR CURRENT: ")PRINTINT(GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()))PRINTSTRING(" MINUTE CURRENT: ")PRINTINT(GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))PRINTSTRING(" @@@@@@@@@\n")
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) +24
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
			bTimeOut = TRUE
		ENDIF
	ELSE
//		PRINTSTRING("\n@@@@@@@@@ HOUR LIMIT: ")PRINTINT(iTimeLimitHour -24)PRINTSTRING(" MINUTE LIMIT: ")PRINTINT(iTimeLimitMin)PRINTSTRING(" @@@@@@@@@")
//		PRINTSTRING("\n@@@@@@@@@ HOUR CURRENT: ")PRINTINT(GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()))PRINTSTRING(" MINUTE CURRENT: ")PRINTINT(GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))PRINTSTRING(" @@@@@@@@@\n")
		IF iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		AND iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
			bTimeOut = TRUE
		ENDIF
	ENDIF
	IF bTimeOut
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			WAIT(0)
//		ENDWHILE
		STOP_SOUND(iSoundCountdown)
		STOP_AUDIO_SCENE("TIMER_SCENE")
		IF thisEvent = BUSINESSMAN
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TLATE", CONV_PRIORITY_AMBIENT_HIGH)
//		ELIF thisEvent = GAY_MILITARY
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TLATE", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		IF NOT IS_PED_INJURED(pedHitcher)
			IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
				BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(pedHitcher), DEFAULT_VEH_STOPPING_DISTANCE +2, 2)
				TASK_LEAVE_ANY_VEHICLE(pedHitcher, 1)
				REMOVE_PED_FROM_GROUP(pedHitcher)
				IF DOES_BLIP_EXIST(blipLocation)
					REMOVE_BLIP(blipLocation)
				ENDIF
			ENDIF
		ENDIF
		eventStage = TOO_LATE
    ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	
//	IF thisEvent = GAY_MILITARY
//		IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_BSE", CONV_PRIORITY_AMBIENT_HIGH)
//			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				WAIT(0)
//			ENDWHILE
//			MISSION_FAILED()
//		ENDIF
//	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedHitcher)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHitcher, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
		IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHitcher, GET_PLAYERS_LAST_VEHICLE())
				RETURN TRUE
			ENDIF
		ENDIF
		IF NOT bChasedByGroom
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHitcher), 10)
			OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedHitcher) - << 10, 10, 10>>, GET_ENTITY_COORDS(pedHitcher) + << 10, 10, 10>>)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_PED_INJURED(pedBoyfriend)
			AND DOES_ENTITY_EXIST(pedBoyfriend)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBoyfriend, PLAYER_PED_ID())
				AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
					IF NOT IS_PED_INJURED(pedHitcher)
						PLAY_PAIN(pedHitcher, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_IN_GROUP(pedHitcher)
		AND IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 90)
		AND IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 50, 50, 50 >>)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHitcher)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHitcher)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CLEANUP_GOONS_AND_VEHICLE(PED_INDEX &ped, BLIP_INDEX &pedBlip, VEHICLE_INDEX &vehicle, BLIP_INDEX &vehBlip)//, FLOAT fCleanupDistanceInVehicle = 300.0, FLOAT fCleanupDistanceOnFoot = 150.0)

//	INT 	index
	BOOL	bAtLeastOnePedAlive
	BOOL	bAtLeastOnePedInVehicle
	
//	FOR index = 0 TO ( COUNT_OF(ped) - 1 )
	
		IF DOES_ENTITY_EXIST(ped)
		
			IF IS_PED_INJURED(ped)
			
				IF DOES_BLIP_EXIST(pedBlip)
					REMOVE_BLIP(pedBlip)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(ped)
				
			
			ELSE
			
				bAtLeastOnePedAlive = TRUE
				
				IF NOT IS_PED_IN_ANY_VEHICLE(ped)
								
//					IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped) > fCleanupDistanceOnFoot )
//						
//						SET_PED_AS_NO_LONGER_NEEDED(ped)
//						
//						IF DOES_BLIP_EXIST(pedBlip)
//							REMOVE_BLIP(pedBlip)
//						ENDIF
//					
//					ENDIF
					
				ELSE
				
					IF DOES_ENTITY_EXIST(vehicle)
					
						IF IS_VEHICLE_DRIVEABLE(vehicle)
						
							IF IS_PED_IN_VEHICLE(ped, vehicle)
							
								bAtLeastOnePedInVehicle = TRUE
								
//								IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped) > fCleanupDistanceInVehicle )
//								
//									SET_PED_AS_NO_LONGER_NEEDED(ped)
//									
//									IF DOES_BLIP_EXIST(pedBlip)
//										REMOVE_BLIP(pedBlip)
//									ENDIF
//									
//								ENDIF
								
							ENDIF
						
						ENDIF
						
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ENDIF
		
//	ENDFOR
	
	IF DOES_ENTITY_EXIST(vehicle)
	
		IF IS_VEHICLE_DRIVEABLE(vehicle)
			
			IF ( bAtLeastOnePedAlive = TRUE )
			OR ( bAtLeastOnePedInVehicle = TRUE )
			
//				IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicle) > fCleanupDistanceInVehicle )
//					
//					REMOVE_VEHICLE_STUCK_CHECK(vehicle)
//					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
//					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
//					
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
//					
//					IF DOES_BLIP_EXIST(vehBlip)
//						REMOVE_BLIP(vehBlip)
//					ENDIF
//					
//				ENDIF
				
			ELSE
			
				REMOVE_VEHICLE_STUCK_CHECK(vehicle)
				REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
				
				IF DOES_BLIP_EXIST(vehBlip)
					REMOVE_BLIP(vehBlip)
				ENDIF
			
			ENDIF
		
		ELSE
			
			REMOVE_VEHICLE_STUCK_CHECK(vehicle)
			REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle)
			
			IF DOES_BLIP_EXIST(vehBlip)
				REMOVE_BLIP(vehBlip)
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

PROC UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(PED_INDEX &ped, BLIP_INDEX &pedBlip, VEHICLE_INDEX &vehicle, BLIP_INDEX &vehBlip)

//	INT 	index
	BOOL	bAtLeastOnePedInVehicle
	
//	FOR index = 0 TO ( COUNT_OF(ped) - 1 )
	
		IF DOES_ENTITY_EXIST(vehicle)
		
			IF IS_VEHICLE_DRIVEABLE(vehicle)
			
				IF NOT IS_PED_INJURED(ped)
				
					IF IS_PED_IN_VEHICLE(ped, vehicle)
					
						IF DOES_BLIP_EXIST(pedBlip)
							REMOVE_BLIP(pedBlip)
						ENDIF
						
						bAtLeastOnePedInVehicle = TRUE
					
					ELSE
					
						IF NOT DOES_BLIP_EXIST(pedBlip)
							pedBlip = CREATE_BLIP_FOR_PED(ped, TRUE)
						ENDIF
					
					ENDIF
					
				ENDIF
				
				
				IF NOT DOES_BLIP_EXIST(vehBlip)
				
					IF ( bAtLeastOnePedInVehicle = TRUE )
						vehBlip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehicle))
						SET_BLIP_COLOUR(vehBlip, BLIP_COLOUR_RED)
						SET_BLIP_PRIORITY(vehBlip, BLIPPRIORITY_HIGHEST)
						SET_BLIP_NAME_FROM_TEXT_FILE(vehBlip, "BLIP_VEH")
					ENDIF
					
				ELSE
				
					VECTOR vBlipPosition 	= GET_BLIP_COORDS(vehBlip)
					VECTOR vVehiclePosition = GET_ENTITY_COORDS(vehicle)
					
					vBlipPosition.X = vBlipPosition.X +@((vVehiclePosition.X - vBlipPosition.X) / BLIP_SLIDE_AMOUNT)
					vBlipPosition.Y = vBlipPosition.Y +@((vVehiclePosition.Y - vBlipPosition.Y) / BLIP_SLIDE_AMOUNT)
					vBlipPosition.Z = vBlipPosition.Z +@((vVehiclePosition.Z - vBlipPosition.Z) / BLIP_SLIDE_AMOUNT)
					
					SET_BLIP_COORDS(vehBlip, vBlipPosition)
					
					IF ( bAtLeastOnePedInVehicle = FALSE )
						REMOVE_BLIP(vehBlip)
					ENDIF
					
				ENDIF
			
			ELSE
			
				IF DOES_BLIP_EXIST(vehBlip)
					REMOVE_BLIP(vehBlip)
				ENDIF
				
				IF NOT IS_PED_INJURED(ped)
				
					IF NOT DOES_BLIP_EXIST(pedBlip)
						pedBlip = CREATE_BLIP_FOR_PED(ped, TRUE)
					ENDIF
					
				ENDIF
				
			ENDIF
		
		ENDIF
	
//	ENDFOR
	
ENDPROC

PROC STORE_ENTITY_POSITION_AND_HEADING(ENTITY_INDEX EntityIndex, VECTOR &vPosition, FLOAT &fHeading)

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			vPosition 	= GET_ENTITY_COORDS(EntityIndex)
			fHeading 	= GET_ENTITY_HEADING(EntityIndex)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_A_DRIVER(VEHICLE_INDEX VehicleIndex)
	
	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF NOT IS_ENTITY_DEAD(VehicleIndex)
			IF NOT IS_VEHICLE_SEAT_FREE(VehicleIndex, VS_DRIVER)
			
				PED_INDEX DriverPedIndex
				
				DriverPedIndex = GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER)
				
				IF DOES_ENTITY_EXIST(DriverPedIndex)
					IF NOT IS_ENTITY_DEAD(DriverPedIndex)
						RETURN TRUE
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TargetVehicleIndex, VECTOR &vWarpPosition,
												   FLOAT &fWarpHeading, VECTOR &vLastTargetPosition, FLOAT &fLastTargetHeading,
												   INT &iWarpTimer, VECTOR vWarpOffset, INT iWarpTimeDelay, FLOAT fWarpDistance)
												   
												   
//	UPDATE_PLAYER_POSITIONS(vPlayerPositions, fPlayerHeadings)
	
	IF  DOES_ENTITY_EXIST(VehicleIndex)
	AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
		IF 	DOES_ENTITY_EXIST(TargetVehicleIndex)
		AND ( VehicleIndex <> TargetVehicleIndex)
		AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(TargetVehicleIndex))
		AND IS_VEHICLE_ON_ALL_WHEELS(TargetVehicleIndex)
		AND DOES_VEHICLE_HAVE_A_DRIVER(VehicleIndex)
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vLastTargetPosition) > 20.0
				
				vWarpPosition 	= vLastTargetPosition	//store last target vehicle positon as the new warp position
				fWarpHeading 	= fLastTargetHeading	//store last target vehicle heading as the new warp heading
				
				vWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarpPosition, fWarpHeading, vWarpOffset)
				fWarpHeading = GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(VehicleIndex), GET_ENTITY_COORDS(TargetVehicleIndex))
				
				STORE_ENTITY_POSITION_AND_HEADING(TargetVehicleIndex, vLastTargetPosition, fLastTargetHeading)
				
			ENDIF
			
			//IF IS_ENTITY_ON_SCREEN(VehicleIndex)
			IF NOT IS_ENTITY_OCCLUDED(VehicleIndex)
			
			      iWarpTimer = GET_GAME_TIMER()
				  
			ELIF GET_GAME_TIMER() - iWarpTimer > iWarpTimeDelay
			
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(VehicleIndex, TargetVehicleIndex) > fWarpDistance
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vWarpPosition) > 20.0
				
					IF NOT IS_VECTOR_ZERO(vWarpPosition)
						IF NOT IS_SPHERE_VISIBLE(vWarpPosition, 3.0)
						  
							CLEAR_AREA_OF_PEDS(vWarpPosition, 1.5)
					        CLEAR_AREA_OF_VEHICLES(vWarpPosition, 3.0)
							
							SET_ENTITY_COORDS(VehicleIndex, vWarpPosition)
							SET_ENTITY_HEADING(VehicleIndex, fWarpHeading)
				            SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
				            
				            SET_VEHICLE_FORWARD_SPEED(VehicleIndex, 10.0)
				            
				            SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
				            
				            iWarpTimer = GET_GAME_TIMER()
							
						ELSE
							
							VECTOR vNewWarpPosition
							
							vNewWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerPositions, fPlayerHeadings, vWarpOffset)
							
							IF NOT IS_VECTOR_ZERO(vNewWarpPosition)
								IF NOT IS_SPHERE_VISIBLE(vNewWarpPosition, 2.0)
								
									CLEAR_AREA_OF_PEDS(vNewWarpPosition, 1.5)
									CLEAR_AREA_OF_VEHICLES(vNewWarpPosition, 3.0)
									
									SET_ENTITY_COORDS(VehicleIndex, vNewWarpPosition)
									SET_ENTITY_HEADING(VehicleIndex, fPlayerHeadings)
									SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
									
									SET_VEHICLE_FORWARD_SPEED(VehicleIndex, 10.0)
									
									SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
									
									iWarpTimer = GET_GAME_TIMER()
									
									EXIT
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping to new position ", vNewWarpPosition, " at index ", ".")
										SCRIPT_ASSERT("Warping to new position")
									#ENDIF
								
								ENDIF
							ENDIF
						
						ENDIF
				    ENDIF
					
				ENDIF
			ENDIF
		ELSE
			iWarpTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()

	IF thisEvent = RUNAWAY_BRIDE
		IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
//			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 1
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
			IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
//			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) != RHINO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), TRUE)
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC groupSeparationCheck()

	IF NOT IS_PED_INJURED(pedHitcher)
//		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 8, 8, 8 >>)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_VEHICLE(pedHitcher, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF NOT DOES_BLIP_EXIST(blipHitcher)
					blipHitcher = CREATE_BLIP_FOR_PED(pedHitcher)
				ENDIF
				IF DOES_BLIP_EXIST(blipLocation)
					REMOVE_BLIP(blipLocation)
				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipLocation)
					blipLocation = CREATE_BLIP_FOR_COORD(vHitcherDestinationPos, TRUE)
				ENDIF
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				AND thisEvent != BUSINESSMAN
//				AND thisEvent != GAY_MILITARY
//					IF NOT DOES_BLIP_EXIST(blipCult)
//						blipCult = CREATE_BLIP_FOR_COORD(vCult)
//						SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//						PRINT_CULT_HELP()
//					ENDIF
//				ENDIF
				IF DOES_BLIP_EXIST(blipHitcher)
					REMOVE_BLIP(blipHitcher)
				ENDIF
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipHitcher)
				blipHitcher = CREATE_BLIP_FOR_PED(pedHitcher)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(pedHitcher)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHitcherDestinationPos, << 50, 50, 50 >>)
		OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
			IF NOT DOES_BLIP_EXIST(blipLocation)
				blipLocation = CREATE_BLIP_FOR_COORD(vHitcherDestinationPos, TRUE)
			ENDIF
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//			AND thisEvent != BUSINESSMAN
//			AND thisEvent != GAY_MILITARY
//				IF NOT DOES_BLIP_EXIST(blipCult)
//					blipCult = CREATE_BLIP_FOR_COORD(vCult)
//					SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//					PRINT_CULT_HELP()
//				ENDIF
//			ENDIF
		ELSE
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHitcherDestinationPos, << 50, 50, 50 >>)
				IF DOES_BLIP_EXIST(blipLocation)
					REMOVE_BLIP(blipLocation)
				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC dontFollowPlayerInWrongVehicle()
	
	IF NOT IS_PED_INJURED(pedHitcher)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE DOESN'T FIT @@@@@@@@@@@@@@@@@@@@@@@@@@")PRINTNL()
				IF IS_PED_IN_GROUP(pedHitcher)
					REMOVE_PED_FROM_GROUP(pedHitcher)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
					TASK_GO_TO_ENTITY(pedHitcher, PLAYER_PED_ID(), -1, 6)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
					TASK_LEAVE_ANY_VEHICLE(pedHitcher)
				ENDIF
				IF NOT bUnsuitableVehRemark
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF thisEvent = BUSINESSMAN
							IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ELIF thisEvent = GIRL_NUMBER
							IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_VEH", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ELIF thisEvent = GIRL_CHEATER
							IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_VEH", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
//						ELIF thisEvent = GAY_MILITARY
//							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_VEH", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF thisEvent = RUNAWAY_BRIDE
							IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_VEH", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ENDIF
						bUnsuitableVehRemark = TRUE
					ENDIF
				ENDIF
			ELSE
				bUnsuitableVehRemark = FALSE
				IF GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
					CLEAR_PED_TASKS(pedHitcher)
				ENDIF
			ENDIF
		ELIF NOT IS_PED_IN_GROUP(pedHitcher)
			SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
			SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
		ENDIF
	ENDIF
	
ENDPROC

PROC dialogueInterruptionCheck()
	IF NOT IS_PED_INJURED(pedHitcher)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND IS_PED_IN_ANY_VEHICLE(pedHitcher, TRUE)
		OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 5, 5, 5 >>)
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				bConversationPaused = FALSE
			ENDIF
		ELSE
//			IF GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_USE_MOBILE_PHONE) != PERFORMING_TASK
//			AND GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_USE_MOBILE_PHONE) != WAITING_TO_START_TASK
			IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT bConversationPaused
					PRINTSTRING("\n@@@@@@@@@@@@@@@@ HITCH LIFT IS PAUSING CONVERSATION @@@@@@@@@@@@@@@@@\n")
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					bConversationPaused = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
			SET_PED_CAN_PLAY_GESTURE_ANIMS(pedHitcher, FALSE)
		ENDIF
		IF IS_THIS_LABEL_PLAYING("REHH3_CHT3_7")
			TASK_USE_MOBILE_PHONE(pedHitcher, TRUE) //59000
//			ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(pedHitcher, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		IF IS_THIS_LABEL_PLAYING("REHH3_CHT6_5")
			TASK_USE_MOBILE_PHONE(pedHitcher, FALSE)
		ENDIF
		IF IS_THIS_LABEL_PLAYING("REHH3_CHT8_3")
			TASK_USE_MOBILE_PHONE(pedHitcher, TRUE) //21000
//			ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(pedHitcher, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		IF IS_THIS_CONVERSATION_PLAYING("REHH3_CHT8M")
		OR IS_THIS_CONVERSATION_PLAYING("REHH3_CHT8F")
		OR IS_THIS_CONVERSATION_PLAYING("REHH3_CHT8T")
			TASK_USE_MOBILE_PHONE(pedHitcher, FALSE)
		ENDIF
//		IF GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_USE_MOBILE_PHONE) = PERFORMING_TASK
//		OR GET_SCRIPT_TASK_STATUS(pedHitcher, SCRIPT_TASK_USE_MOBILE_PHONE) = WAITING_TO_START_TASK
//		IF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
//			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 5, 5, 5 >>)
//				ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(pedHitcher, GET_ENTITY_COORDS(PLAYER_PED_ID()) + << -1, -1, 0 >>)
//				WAIT(4000)
//			ENDIF
//		ENDIF
	ENDIF
ENDPROC

PROC continualTurnToFacePlayer(PED_INDEX targetPed, INT& stage) //, INT &turnTimer, INT timeGapTurn)
	
	FLOAT fFacingAngle = 90
	
	SWITCH stage
		CASE 0 // TURN TASK
			IF NOT IS_PED_INJURED(targetPed)
				IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
				//	CLEAR_PED_TASKS(targetPed)
					OPEN_SEQUENCE_TASK(Seq)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(targetPed, Seq)
					CLEAR_SEQUENCE_TASK(Seq)
					stage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1 // TURNING
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
					// ACHIEVED TASK
					stage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2 // WAITING TO TURN
			IF NOT IS_PED_INJURED(targetPed)
				IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
					stage = 0
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC pickingUpTheBusinessman()
// this is BUSINESSMAN variation 1
	SWITCH iApproachStage
		CASE 0
			IF NOT IS_PED_INJURED(pedHitcher)
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 12
				OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) //NEED TO UPDATE ALL THESE veh_current_player_vehicle TO GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) OR GET_PLAYERS_LAST_VEHICLE()
						IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, veh_current_player_vehicle, 30000, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
//								TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,-1, AF_UPPERBODY| AF_SECONDARY)
								TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT,-1, AF_UPPERBODY| AF_SECONDARY|AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
						iApproachStage ++
					ELSE
						IF NOT bAskedForVeh
							CLEAR_PED_TASKS(pedHitcher)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
							CLEAR_SEQUENCE_TASK(seq)
							//	"I need a lift - you got a car?"
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_NOV", CONV_PRIORITY_HIGH) //CONV_PRIORITY_AMBIENT_HIGH)
								bAskedForVeh = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, TRUE)
			ENDIF
			IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 10
						IF NOT IS_PED_INJURED(pedHitcher)
	//						IF IS_ENTITY_AT_ENTITY(pedHitcher, veh_current_player_vehicle, << 9, 9, 9 >>)
								IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 150)
									fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
									IF fPlayerHitcherDist > 5
										fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
									ELSE	
										fPlayerHitcherDist = (fPlayerHitcherDist - 0.25)
									ENDIF
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_ENTITY(NULL, veh_current_player_vehicle, 10000, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
										IF IS_VEHICLE_SEAT_FREE(veh_current_player_vehicle, VS_FRONT_RIGHT)
											TASK_OPEN_VEHICLE_DOOR(NULL, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
										ENDIF
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
//										TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedHitcher, seq)
									CLEAR_SEQUENCE_TASK(seq)
									iApproachStage ++
								ELSE
									TASK_LOOK_AT_ENTITY(pedHitcher, veh_current_player_vehicle, 10000, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
	//						ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT bAskedForVeh
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(pedHitcher)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH", CONV_PRIORITY_AMBIENT_HIGH)
							ELSE
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(pedHitcher)
			AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, veh_current_player_vehicle, << 9, 9, 9 >>)
					IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
						KILL_ANY_CONVERSATION()
//						SETTIMERA(0)
						iApproachStage ++
					ENDIF
//				ENDIF
//				continualTurnToFacePlayer(pedHitcher, iTurnStage)
			ENDIF
		BREAK
		
		CASE 3
//			IF TIMERA() > 1000
				IF NOT IS_PED_INJURED(pedHitcher)
				AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//					IF IS_ENTITY_AT_ENTITY(pedHitcher, veh_current_player_vehicle, << 9, 9, 9 >>)
						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_ASK", CONV_PRIORITY_AMBIENT_HIGH)
								CLEAR_PED_SECONDARY_TASK(pedHitcher)
//								TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedHitcher, PLAYER_PED_ID(), << 2, 0, 0 >>, PEDMOVEBLENDRATIO_WALK, -1)
								SETTIMERA(0)
								iApproachStage ++
							ENDIF
						ENDIF
//					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		CASE 4
//			IF TIMERA() > 3000
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_GETTING_INTO_A_VEHICLE(pedHitcher)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
							IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									// "Jump in."
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RPA", CONV_PRIORITY_AMBIENT_HIGH)
										SETTIMERA(0)
										iApproachStage ++
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RPB", CONV_PRIORITY_AMBIENT_HIGH)
										SETTIMERA(0)
										iApproachStage ++
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RPC", CONV_PRIORITY_AMBIENT_HIGH)
										SETTIMERA(0)
										iApproachStage ++
									ENDIF
	//							ELSE
	//								PRINTNL()
	//								PRINTSTRING("++++++++++++++++++++++IS_ANY_CONVERSATION_ONGOING_OR_QUEUED returned true++++++++++++++++++++++++++++")
	//								PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT bAskedForVeh
						//	"I need a lift - you got a car?"
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_NOV", CONV_PRIORITY_HIGH) //CONV_PRIORITY_AMBIENT_HIGH)
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 5
//			IF TIMERA() > 500
			IF NOT IS_PED_INJURED(pedHitcher)
				IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
//								CLEAR_PED_TASKS(pedHitcher)
								SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
								TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
		//						TASK_CLEAR_LOOK_AT(pedHitcher)
								TASK_ENTER_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
								iApproachStage ++
							ELSE
								PED_INDEX pedTempDeadPed
								pedTempDeadPed = GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								IF IS_PED_INJURED(pedTempDeadPed)
									thisSection = AGGRO_REACTIONS
								ELSE
									CLEAR_PED_TASKS(pedHitcher)
									SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_BACK_LEFT)
									TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
							ENDIF
						ELSE
							IF NOT bUnsuitableVehRemark
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH", CONV_PRIORITY_AMBIENT_HIGH)
									ELSE
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
									bUnsuitableVehRemark = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT bAskedForVeh
						//	"I need a lift - you got a car?"
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_NOV", CONV_PRIORITY_HIGH) //CONV_PRIORITY_AMBIENT_HIGH)
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 6
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					SET_PED_MAX_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_WALK)
					IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
						TASK_CLEAR_LOOK_AT(pedHitcher)
						bLeavingActivationArea = TRUE
						eventStage = STAGE_2
					ELSE
						IF TIMERA() > 60000
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_NOH", CONV_PRIORITY_AMBIENT_HIGH)
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					REMOVE_PED_FROM_GROUP(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					iApproachStage = 5
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) > 100
					MISSION_FAILED()
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC pickingUpTheGirl()
// this is GIRL_NUMBER variation 2
	SWITCH iApproachStage
		CASE 0
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 90)
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 12
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						IF fPlayerHitcherDist > 5
							fPlayerHitcherDist = (fPlayerHitcherDist - 1)
						ELSE
							fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
						ENDIF
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 8
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						fPlayerHitcherDist = (fPlayerHitcherDist - 0.25)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 20
							IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 10
							OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
								IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 150)
									//"Hey, hey! Stop!"
									CLEAR_PED_SECONDARY_TASK(pedHitcher)
									OPEN_SEQUENCE_TASK(seq)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
										IF IS_VEHICLE_SEAT_FREE(veh_current_player_vehicle, VS_FRONT_RIGHT)
											TASK_OPEN_VEHICLE_DOOR(NULL, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
										ENDIF
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedHitcher, seq)
									CLEAR_SEQUENCE_TASK(seq)
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(0)
//									IF NOT (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
//										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_STO", CONV_PRIORITY_AMBIENT_HIGH)
//									ENDIF
									iApproachStage ++
								ENDIF
							ENDIF
//						ENDIF
					ENDIF
				ELIF NOT bAskedForVeh
					CLEAR_PED_TASKS(pedHitcher)
					TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					//	"I need a lift - you got a car?"
					IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_NOV", CONV_PRIORITY_AMBIENT_HIGH)
						bAskedForVeh = TRUE
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedHitcher)
					AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, veh_current_player_vehicle) < 13
						OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
							IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
								IF NOT IS_PED_INJURED(pedHitcher)
									// "Can I get a lift?"
									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_ASK", CONV_PRIORITY_AMBIENT_HIGH)
//										CLEAR_PED_TASKS(pedHitcher)
										SETTIMERA(0)
										iApproachStage ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
//						continualTurnToFacePlayer(pedHitcher, iTurnStage)
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF TIMERA() > 3000
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_GETTING_INTO_A_VEHICLE(pedHitcher)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
						AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								// "Jump in."
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_RPA", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_RPB", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_RPC", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 4
//			IF TIMERA() > 250
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
					IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
						veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
							IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
//								CLEAR_PED_TASKS(pedHitcher)
								SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
								TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
	//								TASK_CLEAR_LOOK_AT(pedHitcher)
								TASK_ENTER_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
								iApproachStage ++
							ELSE
								PED_INDEX pedTempDeadPed
								pedTempDeadPed = GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								IF IS_PED_INJURED(pedTempDeadPed)
									thisSection = AGGRO_REACTIONS
								ELSE
									CLEAR_PED_TASKS(pedHitcher)
									SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_BACK_LEFT)
									TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bUnsuitableVehRemark
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_VEH", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								bUnsuitableVehRemark = TRUE
							ENDIF
						ENDIF
					ENDIF
//				ELSE
//					IF NOT bAskedForVeh
//						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000)
//						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_NOV", CONV_PRIORITY_AMBIENT_HIGH)
//							bAskedForVeh = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedHitcher)
				IF NOT bHasHitcherThanksAsEnters
					IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_INA", CONV_PRIORITY_AMBIENT_HIGH)
								bHasHitcherThanksAsEnters = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					SET_PED_MAX_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_WALK)
					IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
						TASK_CLEAR_LOOK_AT(pedHitcher)
						bLeavingActivationArea = TRUE
						eventStage = STAGE_2
					ELSE
						IF TIMERA() > 60000
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_SLW", CONV_PRIORITY_AMBIENT_HIGH)
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					REMOVE_PED_FROM_GROUP(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					iApproachStage = 4
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) > 100
					MISSION_FAILED()
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC pickingUpTheCheater()
// this is GIRL_CHEATER variation 3
	SWITCH iApproachStage
		CASE 0
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 90)
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 12
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						IF fPlayerHitcherDist > 5
							fPlayerHitcherDist = (fPlayerHitcherDist - 1)
						ELSE
							fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
						ENDIF
						CLEAR_PED_SECONDARY_TASK(pedHitcher)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, PLAYER_PED_ID(), << 2.5, 0, 0 >>, PEDMOVEBLENDRATIO_WALK, 5000)
//							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 8
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						fPlayerHitcherDist = (fPlayerHitcherDist - 0.25)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 20
							IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 10
							OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
								IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 150)
									CLEAR_PED_SECONDARY_TASK(pedHitcher)
									//"Hey, hey! Stop!"
//									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_STO", CONV_PRIORITY_AMBIENT_HIGH)
//									OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
										OPEN_SEQUENCE_TASK(seq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
											IF IS_VEHICLE_SEAT_FREE(veh_current_player_vehicle, VS_FRONT_RIGHT)
												TASK_OPEN_VEHICLE_DOOR(NULL, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
											ENDIF
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedHitcher, seq)
										CLEAR_SEQUENCE_TASK(seq)
										iApproachStage ++
//									ENDIF
								ENDIF
							ENDIF
//						ENDIF
					ENDIF
				ELSE
					IF NOT bAskedForVeh
						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					//	"I need a lift - you got a car?"
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_NOV", CONV_PRIORITY_AMBIENT_HIGH)
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedHitcher)
					AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, veh_current_player_vehicle) < 13
						OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
							IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
								IF NOT IS_PED_INJURED(pedHitcher)
									// "Can I get a lift?"
									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_ASK", CONV_PRIORITY_AMBIENT_HIGH)
//										CLEAR_PED_TASKS(pedHitcher)
										SETTIMERA(0)
										iApproachStage ++
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
//						continualTurnToFacePlayer(pedHitcher, iTurnStage)
					ENDIF	
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF TIMERA() > 3000
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_GETTING_INTO_A_VEHICLE(pedHitcher)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
						AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								// "Jump in."
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_RPA", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_RPB", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_RPC", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 4
//			IF TIMERA() > 250
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
					IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
//								CLEAR_PED_TASKS(pedHitcher)
								SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
								TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
	//								TASK_CLEAR_LOOK_AT(pedHitcher)
								TASK_ENTER_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
								iApproachStage ++
							ELSE
								PED_INDEX pedTempDeadPed
								pedTempDeadPed = GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								IF IS_PED_INJURED(pedTempDeadPed)
									thisSection = AGGRO_REACTIONS
								ELSE
									CLEAR_PED_TASKS(pedHitcher)
									SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_BACK_LEFT)
									TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bUnsuitableVehRemark
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_VEH", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								bUnsuitableVehRemark = TRUE
							ENDIF
						ENDIF
					ENDIF
//				ELSE
//					IF NOT bAskedForVeh
//						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000)
//						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_NOV", CONV_PRIORITY_AMBIENT_HIGH)
//							bAskedForVeh = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					SET_PED_MAX_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_WALK)
					IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
						TASK_CLEAR_LOOK_AT(pedHitcher)
						bLeavingActivationArea = TRUE
						eventStage = STAGE_2
					ELSE
						IF TIMERA() > 60000
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_SLW", CONV_PRIORITY_AMBIENT_HIGH)
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) > 100
						MISSION_FAILED()
					ENDIF
				ELSE
					REMOVE_PED_FROM_GROUP(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					iApproachStage = 4
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC pickingUpTheGuy()
// this is GAY_MILITARY variation 4
	SWITCH iApproachStage
		CASE 0
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 90)
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 12
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						IF fPlayerHitcherDist > 5
							fPlayerHitcherDist = (fPlayerHitcherDist - 1)
						ELSE
							fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
						ENDIF
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 8
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						IF fPlayerHitcherDist > 5
							fPlayerHitcherDist = (fPlayerHitcherDist - 1)
						ELSE
							fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
						ENDIF
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_WALK)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 20
							IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 10
							OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
								IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 150)
									//"Hey, hey! Stop!"
//									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_STO", CONV_PRIORITY_AMBIENT_HIGH)
//									OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
//										CLEAR_PED_SECONDARY_TASK(pedHitcher)
										OPEN_SEQUENCE_TASK(seq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 30000, SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
											IF IS_VEHICLE_SEAT_FREE(veh_current_player_vehicle, VS_FRONT_RIGHT)
												TASK_OPEN_VEHICLE_DOOR(NULL, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN)
											ENDIF
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedHitcher, seq)
										CLEAR_SEQUENCE_TASK(seq)
										iApproachStage ++
//									ENDIF
								ENDIF
							ENDIF
//						ENDIF
					ENDIF
				ELSE
					IF NOT bAskedForVeh
						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					//	"I need a lift - you got a car?"
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_NOV", CONV_PRIORITY_AMBIENT_HIGH)
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedHitcher)
					AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, veh_current_player_vehicle) < 13
						OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
							IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
								IF NOT IS_PED_INJURED(pedHitcher)
									// "Can I get a lift?"
									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_ASK", CONV_PRIORITY_AMBIENT_HIGH)
//										CLEAR_PED_TASKS(pedHitcher)
										SETTIMERA(0)
										iApproachStage ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
//						continualTurnToFacePlayer(pedHitcher, iTurnStage)
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF TIMERA() > 3000
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_GETTING_INTO_A_VEHICLE(pedHitcher)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
						AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								// "Jump in."
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_RPA", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_RPB", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_RPC", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 4
//			IF TIMERA() > 250
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 15
					IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
						veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
							IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
//								CLEAR_PED_TASKS(pedHitcher)
								SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
								TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
	//							TASK_CLEAR_LOOK_AT(pedHitcher)
								TASK_ENTER_VEHICLE(pedHitcher, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
								iApproachStage ++
							ELSE
								PED_INDEX pedTempDeadPed
								pedTempDeadPed = GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								IF IS_PED_INJURED(pedTempDeadPed)
									thisSection = AGGRO_REACTIONS
								ELSE
									CLEAR_PED_TASKS(pedHitcher)
									SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_BACK_LEFT)
									TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bUnsuitableVehRemark
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_VEH", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								bUnsuitableVehRemark = TRUE
							ENDIF
						ENDIF
					ENDIF	
//				ELSE
//					IF NOT bAskedForVeh
//						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000)
//						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_NOV", CONV_PRIORITY_AMBIENT_HIGH)
//							bAskedForVeh = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					SET_PED_MAX_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_WALK)
					IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
						REQUEST_MODEL(S_M_M_MARINE_01)
						IF HAS_MODEL_LOADED(S_M_M_MARINE_01)
							PED_INDEX pedTempSoldier
							pedTempSoldier = CREATE_PED(PEDTYPE_ARMY, S_M_M_MARINE_01, <<-1588.5995, 2795.1328, 15.8956>>, 225.5897)
							GIVE_WEAPON_TO_PED(pedTempSoldier, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
							TASK_START_SCENARIO_IN_PLACE(pedTempSoldier, "WORLD_HUMAN_GUARD_STAND_ARMY")
							SET_PED_KEEP_TASK(pedTempSoldier, TRUE)
							TASK_CLEAR_LOOK_AT(pedHitcher)
							bLeavingActivationArea = TRUE
							eventStage = STAGE_2
						ENDIF
					ELSE	
						IF TIMERA() > 60000
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_SLW", CONV_PRIORITY_AMBIENT_HIGH)
								MISSION_FAILED()
							ENDIF	
						ENDIF
					ENDIF
				ELSE
					REMOVE_PED_FROM_GROUP(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					iApproachStage = 4
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) > 100
					MISSION_FAILED()
				ENDIF
			ENDIF	
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC pickingUpTheBride()
// this is GIRL_CHEATER variation 3
	SWITCH iApproachStage
		CASE 0
			IF NOT IS_PED_INJURED(pedHitcher)
				IF iStartDialogueTimer < GET_GAME_TIMER()
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_STO", CONV_PRIORITY_AMBIENT_HIGH)
					iStartDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4500, 6500)
				ENDIF
				IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 90)
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 24
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						IF fPlayerHitcherDist > 5
							fPlayerHitcherDist = (fPlayerHitcherDist - 1)
						ELSE
							fPlayerHitcherDist = (fPlayerHitcherDist - 0.5)
						ENDIF
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_RUN)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 12
					OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
						fPlayerHitcherDist = GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, PLAYER_PED_ID())
						fPlayerHitcherDist = (fPlayerHitcherDist - 0.25)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, fPlayerHitcherDist, PEDMOVEBLENDRATIO_RUN)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						iApproachStage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_BLIP_EXIST(blipHitcher)
				SHOW_HEIGHT_ON_BLIP(blipHitcher, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 20
							IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 25
							OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
								IF IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 150)
									CLEAR_PED_SECONDARY_TASK(pedHitcher)
									//"Hey, hey! Stop!"
//									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_STO", CONV_PRIORITY_AMBIENT_HIGH)
										OPEN_SEQUENCE_TASK(seq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
//											TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, PLAYER_PED_ID(), << 3, 0, 0 >>, PEDMOVEBLENDRATIO_RUN, -1)
//											TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 3, PEDMOVEBLENDRATIO_RUN)
											IF IS_VEHICLE_SEAT_FREE(veh_current_player_vehicle, VS_FRONT_RIGHT)
												TASK_OPEN_VEHICLE_DOOR(NULL, veh_current_player_vehicle, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN)
											ENDIF
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedHitcher, seq)
										CLEAR_SEQUENCE_TASK(seq)
										iApproachStage ++
//									ENDIF
								ENDIF
							ENDIF
//						ENDIF	
					ENDIF
				ELSE
					IF NOT bAskedForVeh
						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
					//	"I need a lift - you got a car?"
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_NOV", CONV_PRIORITY_AMBIENT_HIGH)
							bAskedForVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 2
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedHitcher)
					AND IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_DISTANCE_BETWEEN_ENTITIES(pedHitcher, veh_current_player_vehicle) < 25
						OR (IS_PLAYER_PRESSING_HORN(PLAYER_ID()) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedHitcher) < 30)
							IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
								IF NOT IS_PED_INJURED(pedHitcher)
									// "Can I get a lift?"
									IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_ASK", CONV_PRIORITY_AMBIENT_HIGH)
//										CLEAR_PED_TASKS(pedHitcher)
										SETTIMERA(0)
										iApproachStage ++
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
//						continualTurnToFacePlayer(pedHitcher, iTurnStage)
					ENDIF	
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF TIMERA() > 3000
			IF NOT IS_PED_INJURED(pedHitcher)
//				IF IS_PED_GETTING_INTO_A_VEHICLE(pedHitcher)
				IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 3, 3, 3 >>)
//					CLEAR_PED_TASKS(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
						AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 25
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								// "Jump in."
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RPA", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RPB", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RPC", CONV_PRIORITY_AMBIENT_HIGH)
									SETTIMERA(0)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
//			continualTurnToFacePlayer(pedHitcher, iTurnStage)
		BREAK
		
		CASE 4
//			IF TIMERA() > 250
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) < 25
					IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
//								CLEAR_PED_TASKS(pedHitcher)
								SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
								SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
								SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
								TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
	//							TASK_CLEAR_LOOK_AT(pedHitcher)
								TASK_ENTER_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_RESUME_IF_INTERRUPTED)
								REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
								iApproachStage ++
							ELSE
								PED_INDEX pedTempDeadPed
								pedTempDeadPed = GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								IF IS_PED_INJURED(pedTempDeadPed)
									thisSection = AGGRO_REACTIONS
								ELSE
									CLEAR_PED_TASKS(pedHitcher)
									SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_BACK_LEFT)
									TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
									iApproachStage ++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bUnsuitableVehRemark
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_VEH", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_VEH2", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								bUnsuitableVehRemark = TRUE
							ENDIF
						ENDIF
					ENDIF
//				ELSE
//					IF NOT bAskedForVeh
//						TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000)
//						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_NOV", CONV_PRIORITY_AMBIENT_HIGH)
//							bAskedForVeh = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
//					IF NOT IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 2.5, 2.5, 2.5 >>)
//						SET_PED_MIN_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_RUN)
//					ELSE
//						SET_PED_MAX_MOVE_BLEND_RATIO(pedHitcher, PEDMOVEBLENDRATIO_WALK)
//					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
						TASK_CLEAR_LOOK_AT(pedHitcher)
						bLeavingActivationArea = TRUE
						eventStage = STAGE_2
					ELSE
						IF TIMERA() > 60000
							IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_SLW", CONV_PRIORITY_AMBIENT_HIGH)
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
					IF GET_DISTANCE_BETWEEN_PEDS(pedHitcher, PLAYER_PED_ID()) > 100
						MISSION_FAILED()
					ENDIF
				ELSE
					REMOVE_PED_FROM_GROUP(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					iApproachStage = 4
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL IS_HITCHER_IN_PLAYERS_VEHICLE_OR_CLOSE_TO_DESTINATION()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_PED_SITTING_IN_VEHICLE(pedHitcher, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHitcherDestinationPos, << 50, 50, 50 >>)
	AND NOT IS_PED_INJURED(pedHitcher)
		IF NOT IS_PED_IN_ANY_VEHICLE(pedHitcher)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC createJealousBoyfriend()
	IF NOT bBoyfriendCreated
		IF thisEvent = GIRL_CHEATER
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCreateBoyfriendPos, << 200, 200, 200 >>)
				REQUEST_MODEL(modelBoyfriend)
				REQUEST_CLIP_SET("move_m@brave")
				IF HAS_MODEL_LOADED(modelBoyfriend)
				AND HAS_CLIP_SET_LOADED("move_m@brave")
					pedBoyfriend = CREATE_PED(PEDTYPE_MISSION, modelBoyfriend, vCreateBoyfriendPos, fCreateBoyfriendHdg)
					SET_PED_COMPONENT_VARIATION(pedBoyfriend, PED_COMP_HEAD, 2, 1, 0)
					SET_PED_COMPONENT_VARIATION(pedBoyfriend, PED_COMP_TORSO, 0, 2, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoyfriend, TRUE)
					SET_PED_CONFIG_FLAG(pedBoyfriend, PCF_DontInfluenceWantedLevel, TRUE)
					SET_PED_DIES_WHEN_INJURED(pedBoyfriend, TRUE)
					SET_PED_MOVEMENT_CLIPSET(pedBoyfriend, "move_m@brave")
					SET_PED_CONFIG_FLAG(pedBoyfriend, PCF_RemoveDeadExtraFarAway, TRUE)
					ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 4, pedBoyfriend, sDialogueBoyfriend)
					//TASK_PLAY_ANIM(pedBoyfriend, "amb@lean_balc_i_a", "crack_knuckles", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		//			VECTOR scenePosition = << 802.185, 1269.975, 360.338 >>
		//			VECTOR sceneRotation = << 0.000, 0.000, -91.000 >>
		//			INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					TASK_PLAY_ANIM(pedBoyfriend, "random@hitch_lift", "idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
		//			TASK_SYNCHRONIZED_SCENE(pedBoyfriend, sceneId, "random@hitch_lift", "idle_e", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		//			SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
					TASK_LOOK_AT_ENTITY(pedBoyfriend, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelBoyfriend)
					bBoyfriendCreated = TRUE
				ENDIF
			ENDIF
		ELIF thisEvent = RUNAWAY_BRIDE
			IF NOT IS_PED_INJURED(pedHitcher)
				IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint5) > 320
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(pedHitcher, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_RANDOM_INT_IN_RANGE(2, 5), vSpawnCoords) //Random spawn
		//				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vSpawnCoords, spawn_distance)
							IF NOT IS_SPHERE_VISIBLE(vSpawnCoords, 7)
							AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vSpawnCoords) < 110
								REQUEST_MODEL(modelBoyfriend)
								IF HAS_MODEL_LOADED(modelBoyfriend)
									KILL_FACE_TO_FACE_CONVERSATION()
									WAIT(0)
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHS", "REHH5_CHS_1", CONV_PRIORITY_AMBIENT_HIGH)
									AND NOT IS_PED_INJURED(pedHitcher)
										viHitcherVehicle = CREATE_VEHICLE(modelVehicle, vSpawnCoords, GET_HEADING_FROM_COORDS(vSpawnCoords, GET_PLAYER_COORDS(PLAYER_ID())))
										SET_VEHICLE_COLOURS(viHitcherVehicle, 134, 134)
										SET_VEHICLE_DIRT_LEVEL(viHitcherVehicle, 0)
										pedBoyfriend = CREATE_PED_INSIDE_VEHICLE(viHitcherVehicle, PEDTYPE_MISSION, modelBoyfriend)
										SET_PED_COMPONENT_VARIATION(pedBoyfriend, PED_COMP_HEAD, 1, 1, 0)
										SET_PED_COMPONENT_VARIATION(pedBoyfriend, PED_COMP_HAIR, 1, 0, 0)
										SET_PED_CONFIG_FLAG(pedBoyfriend, PCF_DontInfluenceWantedLevel, TRUE)
										STOP_PED_SPEAKING(pedBoyfriend, TRUE)
	//									ADD_RELATIONSHIP_GROUP("rghBoyfriend", rghBoyfriend)
	//									SET_PED_RELATIONSHIP_GROUP_HASH(pedBoyfriend, rghBoyfriend)
	//									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghBoyfriend, RELGROUPHASH_PLAYER)
										ADD_PED_FOR_DIALOGUE(HitcherDialogueStruct, 4, pedBoyfriend, sDialogueBoyfriend)
		//								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoyfriend, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(viHitcherVehicle)
										TASK_VEHICLE_MISSION_PED_TARGET(pedBoyfriend, viHitcherVehicle, pedHitcher, MISSION_ATTACK, 60, DRIVINGMODE_AVOIDCARS_RECKLESS, -1, -1)
//										TASK_VEHICLE_CHASE(pedBoyfriend, PLAYER_PED_ID())
										TASK_COMBAT_PED(pedBoyfriend, PLAYER_PED_ID())
										SET_PED_KEEP_TASK(pedBoyfriend, TRUE)
										SET_MODEL_AS_NO_LONGER_NEEDED(modelBoyfriend)
										SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle)
										SET_VEHICLE_FORWARD_SPEED(viHitcherVehicle, GET_ENTITY_SPEED(PLAYER_PED_ID()) + 15)
//										IF NOT DOES_BLIP_EXIST(blipBoyfriend)
//											blipBoyfriend = CREATE_BLIP_FOR_PED(pedBoyfriend, TRUE)
//										ENDIF
//										IF NOT DOES_BLIP_EXIST(blipVehBoyfriend)
//											blipVehBoyfriend = CREATE_BLIP_FOR_VEHICLE(viHitcherVehicle, TRUE)
//										ENDIF
										IF DOES_BLIP_EXIST(blipLocation)
											REMOVE_BLIP(blipLocation)
										ENDIF
//										IF DOES_BLIP_EXIST(blipCult)
//											REMOVE_BLIP(blipCult)
//										ENDIF
										KILL_FACE_TO_FACE_CONVERSATION()
										WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											WAIT(0)
										ENDWHILE
	//									IF NOT IS_PED_INJURED(pedBoyfriend)
	//										SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(pedBoyfriend, VEHICLE_CHASE_CANT_BLOCK|VEHICLE_CHASE_CANT_PURSUE|VEHICLE_CHASE_CANT_BLOCK_FROM_PURSUE|VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE|VEHICLE_CHASE_CANT_SPIN_OUT, TRUE)
	//									ENDIF
										CREATE_CONVERSATION_FROM_SPECIFIC_LINE(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHS", "REHH5_CHS_2", CONV_PRIORITY_AMBIENT_HIGH)
										bChasedByGroom = TRUE
										bBoyfriendCreated = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC looseOrKillGroom()
	IF bChasedByGroom
		IF DOES_ENTITY_EXIST(pedBoyfriend)
			IF IS_PED_INJURED(pedBoyfriend)
			OR NOT IS_ENTITY_AT_ENTITY(pedBoyfriend, PLAYER_PED_ID(), << 150, 150, 150 >>)
			OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHitcherDestinationPos) < 250
			OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCult) < 250
			OR bGroomScared
				IF DOES_BLIP_EXIST(blipBoyfriend)
					REMOVE_BLIP(blipBoyfriend)
				ENDIF
				IF DOES_BLIP_EXIST(blipVehBoyfriend)
					REMOVE_BLIP(blipVehBoyfriend)
				ENDIF
				IF NOT IS_PED_INJURED(pedBoyfriend)
					PLAY_PAIN(pedBoyfriend, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
					TASK_SMART_FLEE_PED(pedBoyfriend, PLAYER_PED_ID(), 1000, -1)
					SET_PED_AS_NO_LONGER_NEEDED(pedBoyfriend)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHT2", CONV_PRIORITY_AMBIENT_HIGH)
				SET_PED_AS_NO_LONGER_NEEDED(pedBoyfriend)
				IF DOES_ENTITY_EXIST(viHitcherVehicle)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viHitcherVehicle)
				ENDIF
				bChasedByGroom = FALSE
			ELSE
				IF NOT bPlayerReactingToChase
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_COMM", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_COMF", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_COMT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bPlayerReactingToChase = TRUE
					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(pedBoyfriend)
					CLEANUP_GOONS_AND_VEHICLE(pedBoyfriend, blipBoyfriend, viHitcherVehicle, blipVehBoyfriend)
					UPDATE_BLIPS_FOR_GOONS_AND_VEHICLE(pedBoyfriend, blipBoyfriend, viHitcherVehicle, blipVehBoyfriend)
					RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(viHitcherVehicle, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), vGangWarpPosition, fGangWarpHeading, vPlayerPositions, fPlayerHeadings, iTimerWarp, << 0, 0, 0 >>, 1500, 0)
					IF IS_ENTITY_AT_ENTITY(pedBoyfriend, PLAYER_PED_ID(), << 30, 30, 30 >>)
						IF iChaseDialogue < GET_GAME_TIMER()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_RANDOM_BOOL()
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_FHT", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
									CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_FEAR", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								iChaseDialogue = GET_GAME_TIMER() + 8000
							ENDIF
						ENDIF
						IF IS_PED_SHOOTING(PLAYER_PED_ID())
							IF NOT IS_THIS_CONVERSATION_PLAYING("REHH5_SHO")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								WAIT(0)
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_SHO", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ENDIF
						IF IS_PED_ON_FOOT(PLAYER_PED_ID())
						AND NOT IS_PED_INJURED(pedBoyfriend)
							IF IS_PED_ON_FOOT(pedBoyfriend)
							AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
							AND IS_PLAYER_FREE_AIMING(PLAYER_ID())
								bGroomScared = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF NOT bBoyfriendCombatPlayer
					AND NOT IS_PED_INJURED(pedBoyfriend)
//						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.5
						IF IS_PED_STOPPED(PLAYER_PED_ID())
						AND IS_ENTITY_AT_ENTITY(pedBoyfriend, PLAYER_PED_ID(), << 10, 10, 10 >>)
							TASK_COMBAT_PED(pedBoyfriend, PLAYER_PED_ID())
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_GRM", CONV_PRIORITY_AMBIENT_HIGH)
		//					ADD_RELATIONSHIP_GROUP("rghBoyfriend", rghBoyfriend)
		//					SET_PED_RELATIONSHIP_GROUP_HASH(pedBoyfriend, rghBoyfriend)
		//					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghBoyfriend, RELGROUPHASH_PLAYER)
							bBoyfriendCombatPlayer = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC destinationInstructions()

	IF NOT IS_PED_INJURED(pedHitcher)
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
			AND IS_PED_IN_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE())
				IF DOES_BLIP_EXIST(blipHitcher)
					REMOVE_BLIP(blipHitcher)
				ENDIF
				IF NOT IS_PED_IN_GROUP(pedHitcher)
					SET_PED_AS_GROUP_MEMBER(pedHitcher, PLAYER_GROUP_ID())
					SET_PED_NEVER_LEAVES_GROUP(pedHitcher, TRUE)
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedHitcher, VS_FRONT_RIGHT)
//					TASK_CLEAR_LOOK_AT(pedHitcher)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipLocation)
					blipLocation = CREATE_BLIP_FOR_COORD(vHitcherDestinationPos, TRUE)
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				AND NOT IS_CULT_FINISHED()
				AND thisEvent != BUSINESSMAN
				AND thisEvent != GAY_MILITARY
					IF NOT DOES_BLIP_EXIST(blipCult)
						blipCult = CREATE_BLIP_FOR_COORD(vCult)
						SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
						PRINT_CULT_HELP()
					ENDIF
				ENDIF
				IF thisEvent = BUSINESSMAN
//				OR thisEvent = GAY_MILITARY
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF thisEvent = BUSINESSMAN
							//"Hey, I'm in a rush - got a plane to catch."
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_WTA", CONV_PRIORITY_AMBIENT_HIGH)
//						ELIF thisEvent = GAY_MILITARY
//							// "Thanks for stopping. I need to sneak back into camp."
//							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_WTD", CONV_PRIORITY_AMBIENT_HIGH)
////							SET_IGNORE_NO_GPS_FLAG(TRUE)
						ENDIF
//						iStartTimeFlight = GET_GAME_TIMER()
//						iCurrHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
						iTimeLimitHour = (GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 2) //iCurrHour
//						iCurrMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
						IF GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) >= 30 //iCurrMin
							// if gone half past the hour round up till the next hour
							iTimeLimitMin = 0
							iTimeLimitHour ++
						ELSE
							iTimeLimitMin = 30
						ENDIF
						
//						iTimeLimitFlight = (2*(120000/*4000*/)) // two game hours worth of milliseconds
//						IF iTimeLimitMin > 0
//							iTimeLimitFlight = (iTimeLimitFlight + 12000)
//						ENDIF
						
						bLeavingActivationArea = TRUE
						SETTIMERA(0)
						eventStage = STAGE_3
					ENDIF
				ELIF thisEvent = GIRL_NUMBER
					// "Hey, can you take me back to my place?"
					IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_WTB", CONV_PRIORITY_AMBIENT_HIGH)
						bLeavingActivationArea = TRUE
						SETTIMERA(0)
						eventStage = STAGE_3
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					// "I need to meet my boyfriend from work."
					IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_WTC", CONV_PRIORITY_AMBIENT_HIGH)
						bLeavingActivationArea = TRUE
						SETTIMERA(0)
						eventStage = STAGE_3
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					// "I need to get away from this wedding"
					IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_WTE", CONV_PRIORITY_AMBIENT_HIGH)
						bLeavingActivationArea = TRUE
						SETTIMERA(0)
						eventStage = STAGE_3
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC timeLimitSpeech()
	IF NOT bSaidTime
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF iTimeLimitHour = 6
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM1", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM1", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 6
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM2", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM2", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 7
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM3", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM3", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 7
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM4", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM4", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 8
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM5", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM5", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 8
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM6", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM6", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 9
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM7", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM7", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 9
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM8", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM8", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 10
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM9", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM9", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 10
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM10", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM10", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 11
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM11", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM11", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 11
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM12", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM12", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 12
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM13", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM13", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 12
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM14", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM14", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 13
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM15", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM15", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 13
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM16", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM16", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 14
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM17", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM17", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 14
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM18", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM18", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 15
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM19", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM19", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 15
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM20", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM20", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 16
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM21", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM21", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 16
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM22", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM22", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 17
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM23", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM23", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 17
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM24", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM24", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 18
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM25", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM25", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 18
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM26", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM26", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 19
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM27", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM27", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 19
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM28", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM28", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 20
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM29", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM29", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 20
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM30", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM30", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 21
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM31", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM31", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 21
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM32", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM32", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 22
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM33", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM33", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 22
		AND iTimeLimitMin = 30
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM34", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM34", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
		IF iTimeLimitHour = 23
		AND iTimeLimitMin = 0
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TIM35", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_TIM35", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bSaidTime = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC chatInterruptions()

	IF NOT IS_PED_INJURED(pedHitcher)
		
		IF thisEvent = BUSINESSMAN
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE())
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_PLAYERS_LAST_VEHICLE())
						IF NOT IS_THIS_CONVERSATION_PLAYING("REHH1_CRASH")
							MANAGE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CRASH", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bToldForGettingOut
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE(), TRUE)
				AND IS_PED_IN_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE())
					MANAGE_CONVERSATION(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					//"Why are you leaving, come back here"
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF thisEvent = BUSINESSMAN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_OUT", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF thisEvent = GIRL_NUMBER
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_GETOUT", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF thisEvent = GIRL_CHEATER
							IF NOT IS_PED_INJURED(pedHitcher)
								IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
									CLEAR_PED_SECONDARY_TASK(pedHitcher)
								ENDIF
							ENDIF
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_GETOUT", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF thisEvent = RUNAWAY_BRIDE
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_GETOUT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bToldForGettingOut = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE())
					bToldForGettingOut = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bToldForJacking
			IF IS_PED_JACKING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Ok, whatever it takes to get back home"
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_JACK", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_NUMBER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_JACK", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_CHEATER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_JACK", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = RUNAWAY_BRIDE
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_JACK", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bToldForJacking = TRUE
				ENDIF
			ENDIF
		ELIF NOT IS_PED_JACKING(PLAYER_PED_ID())
			bToldForJacking = FALSE
		ENDIF
		
		IF NOT bToldForShooting
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				//"Why are you shooting!?"
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_GUN", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_NUMBER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_SHOOT", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_CHEATER
						IF NOT IS_PED_INJURED(pedHitcher)
							IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
								CLEAR_PED_SECONDARY_TASK(pedHitcher)
							ENDIF
						ENDIF
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_SHOOT", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = RUNAWAY_BRIDE
	//					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_SHO", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bToldForShooting = TRUE
				ENDIF
			ENDIF
		ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
			bToldForShooting = FALSE
		ENDIF
		
	ENDIF
	
ENDPROC

PROC onFootCheck()
	IF NOT IS_PED_INJURED(pedHitcher)
		IF IS_PED_ON_FOOT(pedHitcher)
			IF NOT bStartOnFootTimer
				iOnFootStartTimer = GET_GAME_TIMER()
				bStartOnFootTimer = TRUE
			ELSE
				iOnFootCurrentTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			bStartOnFootTimer = FALSE
		ENDIF
	ENDIF
	
	IF ((iOnFootCurrentTimer - iOnFootStartTimer) > 90000)
		MANAGE_CONVERSATION(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		//"Well, if we're going to walk I can do that by myself."
		IF thisEvent = BUSINESSMAN
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_WLK", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_NUMBER
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_WLK", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_CHEATER
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_WLK", CONV_PRIORITY_AMBIENT_HIGH)
//		ELIF thisEvent = GAY_MILITARY
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_WLK", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = RUNAWAY_BRIDE
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_WLK", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		IF NOT IS_PED_INJURED(pedHitcher)
			STOP_PED_SPEAKING(pedHitcher, TRUE)
			IF IS_ENTITY_AT_COORD(pedHitcher, vHitcherDestinationPos, << 200, 200, 200 >>)
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//				IF thisEvent = GAY_MILITARY
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1587.5797, 2794.0452, 15.9006 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
////						TASK_STAND_STILL(NULL, -1)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND", 0, TRUE) //WORLD_HUMAN_HANG_OUT_STREET
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
			ELSE
				TASK_WANDER_STANDARD(pedHitcher)
			ENDIF
			IF thisEvent = BUSINESSMAN
				TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 1000, -1)
			ENDIF
			SET_PED_KEEP_TASK(pedHitcher, TRUE)
			IF IS_PED_IN_GROUP(pedHitcher)
				REMOVE_PED_FROM_GROUP(pedHitcher)
			ENDIF
			MISSION_FAILED()
		ENDIF
	ENDIF
	
ENDPROC

PROC stationaryCheck()
	IF IS_PED_STOPPED(PLAYER_PED_ID())
		IF NOT bStartStationaryTimer
			iStationaryStartTimer = GET_GAME_TIMER()
			bStartStationaryTimer = TRUE
		ELSE
			iStationaryCurrentTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		iStationaryCurrentTimer = 0
		iStationaryHurryTimer = 0
		bStartStationaryTimer = FALSE
		bStationaryWarned = FALSE
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 3000)
	AND iStationaryCurrentTimer - iStationaryHurryTimer > 6000
//		IF NOT IS_THIS_CONVERSATION_PLAYING("REHH1_SLR")
//			MANAGE_CONVERSATION(FALSE)
//		ENDIF
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		WAIT(0)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PRINTLN("@@@@@@@@@@@@@ PLAYING: REHH1_SLR @@@@@@@@@@@@@@@@@")
			IF thisEvent = BUSINESSMAN
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_SLR", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF thisEvent = GAY_MILITARY
//				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_SLR", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			iStationaryHurryTimer = iStationaryCurrentTimer
		ENDIF
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 50000)
	AND NOT bStationaryWarned
//		IF NOT IS_PED_INJURED(pedHitcher)
//			IF IS_PED_IN_GROUP(pedHitcher)
//				IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
//					IF IS_ENTITY_AT_COORD(pedHitcher, vHitcherDestinationPos, << 200, 200, 200 >>)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					ELSE
//						TASK_WANDER_STANDARD(pedHitcher)
//					ENDIF
//				ENDIF
				//"I can probably get there quicker myself."
				MANAGE_CONVERSATION(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_SLW", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_NUMBER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_SLW", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_CHEATER
						IF NOT IS_PED_INJURED(pedHitcher)
							IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
								CLEAR_PED_SECONDARY_TASK(pedHitcher)
							ENDIF
						ENDIF
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_SLW", CONV_PRIORITY_AMBIENT_HIGH)
//					ELIF thisEvent = GAY_MILITARY
//						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_SLW", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = RUNAWAY_BRIDE
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_SLW", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bStationaryWarned = TRUE
				ENDIF
//			ENDIF
//		ENDIF
	ENDIF
	
	IF ((iStationaryCurrentTimer - iStationaryStartTimer) > 60000)
	AND bStationaryWarned
		IF NOT IS_PED_INJURED(pedHitcher)
			STOP_PED_SPEAKING(pedHitcher, TRUE)
			IF IS_PED_IN_GROUP(pedHitcher)
				IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
					IF IS_ENTITY_AT_COORD(pedHitcher, vHitcherDestinationPos, << 200, 200, 200 >>)
//						IF thisEvent = GAY_MILITARY
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1587.5797, 2794.0452, 15.9006 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
////								TASK_STAND_STILL(NULL, -1)
//								TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND", 0, TRUE) //WORLD_HUMAN_HANG_OUT_STREET
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//							CLEAR_SEQUENCE_TASK(seq)
//						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						ENDIF
					ELSE
						TASK_WANDER_STANDARD(pedHitcher)
					ENDIF
					IF thisEvent = BUSINESSMAN
						TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 1000, -1)
					ENDIF
					SET_PED_KEEP_TASK(pedHitcher, TRUE)
					REMOVE_PED_FROM_GROUP(pedHitcher)
					MISSION_FAILED()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC atDestination()

	IF thisEvent != GIRL_CHEATER
 		IF IS_HITCHER_IN_PLAYERS_VEHICLE_OR_CLOSE_TO_DESTINATION()
			SETTIMERA(0)
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHitcherDestinationPos, << 3, 3, LOCATE_SIZE_HEIGHT >>, TRUE)
				AND IS_ENTITY_AT_COORD(pedHitcher, vHitcherDestinationPos, g_vAnyMeansLocate)
					IF CAN_PLAYER_START_CUTSCENE()
						KILL_ANY_CONVERSATION()
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE +2, 3)
						ENDIF
						CLEAR_PED_SECONDARY_TASK(pedHitcher)
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							eventStage = AT_DESTINATION
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHitcherDestinationPos, g_vAnyMeansLocate)
//			IF NOT IS_PED_INJURED(pedBoyfriend)
//				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedBoyfriend, <<40, 40, 40>>)
//					IF IS_ENTITY_ON_SCREEN(pedBoyfriend)
//						IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedBoyfriend, 160)
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				CLEAR_PED_TASKS(pedHitcher)
//			ENDIF
	ELSE
		IF bBoyfriendCreated
			IF IS_PED_INJURED(pedBoyfriend)
				eventStage = POST_DROPOFF_1
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 805.5010, 1269.9261, 361.4025 >>, << 25, 30, 2 >>)
				OR IS_ENTITY_AT_COORD(pedHitcher, << 824.8886, 1275.6705, 359.4312 >>, g_vAnyMeansLocate, TRUE)
				AND IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 8, 8, 8 >>)
	//				IF IS_SCRIPTED_CONVERSATION_ONGOING()
	//					KILL_ANY_CONVERSATION()
	//				ENDIF
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT10", CONV_PRIORITY_AMBIENT_HIGH)
					IF NOT IS_PED_INJURED(pedHitcher)
					AND NOT IS_PED_INJURED(pedBoyfriend)
						IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
	//						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	//							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT10M", CONV_PRIORITY_AMBIENT_HIGH)
	//						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
	//							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT10F", CONV_PRIORITY_AMBIENT_HIGH)
	//						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	//							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT10T", CONV_PRIORITY_AMBIENT_HIGH)
	//						ENDIF
							CLEAR_PED_SECONDARY_TASK(pedHitcher)
							OPEN_SEQUENCE_TASK(seq)
								TASK_CLEAR_LOOK_AT(NULL)
								TASK_LOOK_AT_ENTITY(NULL, pedBoyfriend, -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
								TASK_LEAVE_ANY_VEHICLE(NULL, 500)
								TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 5, PEDMOVEBLENDRATIO_WALK)
								TASK_PLAY_ANIM(NULL, "random@hitch_lift", "come_here_idle_c", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedBoyfriend, -1)
	//							TASK_PLAY_ANIM(NULL, "random@hitch_lift", "f_distressed_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE +2, 2)
						ENDIF
					ENDIF
					SETTIMERA(0)
					eventStage = AT_DESTINATION
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC thankingDirectedEvent()

	IF NOT bSetUpDirectedEvent
		
		CLEAR_HELP(TRUE)
		DISPLAY_HUD(FALSE)
		DISPLAY_RADAR(FALSE)
		
		REQUEST_ANIM_DICT("random@hitch_lift_4")
		REQUEST_MODEL(Prop_LD_Contact_Card)
		WHILE NOT HAS_ANIM_DICT_LOADED("random@hitch_lift_4")
		OR NOT HAS_MODEL_LOADED(Prop_LD_Contact_Card)
			WAIT(0)
		ENDWHILE
		
		objCard = CREATE_OBJECT(Prop_LD_Contact_Card, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ATTACH_ENTITY_TO_ENTITY(objCard, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
		/* START SYNCHRONIZED SCENE */
		VECTOR scenePosition = vWalkIntoCampPos
		VECTOR sceneRotation = << 0, 0, 0 >>
		INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.6)
		camConsol = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
		PLAY_SYNCHRONIZED_CAM_ANIM(camConsol, sceneId, "HITCH_LIFT_Thank_Cam", "random@hitch_lift_4")
		
		IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), vWalkIntoCampPos, g_vAnyMeansLocate)
				SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -1579.2551, 2787.7581, 15.9286 >>)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		ENDIF
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
		
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(pedHitcher)
			CLEAR_PED_TASKS(pedHitcher)
			TASK_SYNCHRONIZED_SCENE(pedHitcher, sceneId, "random@hitch_lift_4", "HITCH_LIFT_Thank_Military", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		ENDIF
		TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@hitch_lift_4", "HITCH_LIFT_Thank_Player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		
		SET_CAM_ACTIVE(camConsol, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_MILB", CONV_PRIORITY_AMBIENT_HIGH)
		
		bSetUpDirectedEvent = TRUE
		SETTIMERA(0)
	ENDIF
	
	IF bSetUpDirectedEvent
	AND TIMERA() > 12000
	OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1604.1895, 2828.1694, 15.8870 >>)
		CLEAR_PRINTS()
		SET_CAM_ACTIVE(camConsol, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camConsol)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		DELETE_OBJECT(objCard)
		IF NOT IS_PED_INJURED(pedHitcher)
			CLEAR_PED_TASKS_IMMEDIATELY(pedHitcher)
			SET_ENTITY_COORDS(pedHitcher, << -1587.3643, 2796.0930, 15.8790 >>)
			SET_ENTITY_HEADING(pedHitcher, 40.8596)
//			DELETE_PED(pedHitcher)
//			CLEAR_PED_TASKS (pedHitcher)
//			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, << -1673.1643, 2964.6733, 30.2778 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
				FORCE_PED_MOTION_STATE(pedHitcher, MS_ON_FOOT_RUN)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1597.6281, 2795.5317, 15.8419 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1656.9266, 2917.0696, 29.5325 >>, PEDMOVE_RUN)
//			CLOSE_SEQUENCE_TASK(seq)
//			TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_KEEP_TASK(pedHitcher, TRUE)
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_CONTACT_TO_PHONEBOOK (CHAR_GAYMILITARY, FRANKLIN_BOOK)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_CONTACT_TO_PHONEBOOK (CHAR_GAYMILITARY, MICHAEL_BOOK)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_CONTACT_TO_PHONEBOOK (CHAR_GAYMILITARY, TREVOR_BOOK)
		ENDIF
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//		bSaidThanks = TRUE
	ENDIF
ENDPROC

PROC pedDroppedOff()

	IF thisEvent = GIRL_CHEATER
		IF NOT IS_PED_INJURED(pedHitcher)
			IF IS_PED_IN_GROUP(pedHitcher)
				REMOVE_PED_FROM_GROUP(pedHitcher)
			ENDIF
			eventStage = POST_DROPOFF_2
		ENDIF
		
//	ELIF thisEvent = GAY_MILITARY
//		IF NOT bSaidThanks
//			IF CAN_PLAYER_START_CUTSCENE()
//			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWalkIntoCampPos, g_vOnFootLocate, FALSE, TRUE, TM_ON_FOOT)
//				STOP_SOUND(iSoundCountdown)
//				STOP_AUDIO_SCENE("TIMER_SCENE")
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				IF NOT IS_PED_INJURED(pedHitcher)
//	//				IF IS_PED_IN_GROUP(pedHitcher)
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//	//					REMOVE_PED_FROM_GROUP(pedHitcher)
//						IF DOES_BLIP_EXIST(blipLocation)
//							REMOVE_BLIP(blipLocation)
//						ENDIF
//						IF DOES_BLIP_EXIST(blipCult)
//							REMOVE_BLIP(blipCult)
//						ENDIF
//						bStartScene = TRUE
//	//				ENDIF
//					
//					IF bStartScene
//						//Open the barriers
//						IF fOpenRatio < 1
//							fOpenRatio = fOpenRatio + 0.02
//						ELSE
//							fOpenRatio = 1
//						ENDIF
//						IF fOpenRatio <= 1
//							DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, fOpenRatio, FALSE, TRUE)
//							DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//						ENDIF
//						
//						thankingDirectedEvent()
//					ENDIF
//					
//				ENDIF
//			ELSE
//				onFootCheck()
//				timeLimitCountdown()
//			ENDIF
//		ELSE
//			//Close the barriers
//			IF fOpenRatio > 0 
//				fOpenRatio = fOpenRatio - 0.02
//			ELSE
//				fOpenRatio = 0
//			ENDIF
//			IF fOpenRatio >= 0
//				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, fOpenRatio, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//			ENDIF
//			
//			IF NOT IS_PED_INJURED(pedHitcher)
//				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 100, 100, 100 >>)
//				OR NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedHitcher, FALSE), 15)
//					MISSION_PASSED()
//				ENDIF
//			ENDIF
//		ENDIF
	ENDIF
	
ENDPROC

PROC pedThankInVehicle()

	IF DOES_BLIP_EXIST(blipLocation)
		REMOVE_BLIP(blipLocation)
	ENDIF
	IF DOES_BLIP_EXIST(blipCult)
		REMOVE_BLIP(blipCult)
	ENDIF
//	DoThanking.SpeechBlock = HitcherDialogueStruct
	
	IF thisEvent = BUSINESSMAN
		STOP_SOUND(iSoundCountdown)
		STOP_AUDIO_SCENE("TIMER_SCENE")
//		IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//			DoThanking.vWalkToPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_current_player_vehicle, <<4.5, 0, 0>>)
//		ENDIF
//		
//		DoThanking.aApproachPed = pedHitcher
//		DoThanking.	aApproachVehicle = veh_current_player_vehicle
//		DoThanking.	InCarAnimNameNPC = "indicate_left"
//		DoThanking.	InCarAnimDictNPC = "gestures@female"
//		DoThanking.	InCarAnimNamePlayer	=  "nod_yes"
//		DoThanking.	InCarAnimDictPlayer	=  "gestures@male"
//		DoThanking.AnimName = "nod_yes"
//		DoThanking.AnimDict = "gestures@male"
//		DoThanking.bDoNotRemovePed = FALSE
//		DoThanking.bLeaveDoorsOpen = FALSE
		
		IF NOT bPlaceholderLine
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			//"Just in time. Here, have this."
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_TKA", CONV_PRIORITY_AMBIENT_HIGH)
			SET_GAMEPLAY_COORD_HINT(<< -1055.5146, -2533.2207, 19.3049 >>, -1, DEFAULT_INTERP_TO_FROM_GAME)
//			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(pedHitcher)
	//			IF UPDATE_RUN_INCAR_THANKYOU(DoThanking)
	//				IF DoThanking.bSuccess
						CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 100,FALSE,TRUE)
						//BAWSAQ_SPECIAL_ADD_DELAYED_BOOST_FOR_STOCK(BS_CO_ADI, 48, 2)
						Execute_Code_ID(CID_STOCK_EVENT_RCHITCH1)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 8000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						
						OPEN_SEQUENCE_TASK(seq)
//							TASK_PLAY_ANIM(NULL, "random@thanks", "thanks_low_ps", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, 1200, AF_SECONDARY|AF_UPPERBODY)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							TASK_LEAVE_ANY_VEHICLE(NULL, 3000, ECF_RESUME_IF_INTERRUPTED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1000)
//							TASK_PAUSE(NULL, 3000)
	//						TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, << -1052.9362, -2528.2988, 16.7542 >>, PEDMOVEBLENDRATIO_RUN)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1055.5146, -2533.2207, 19.3049 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_GO_STRAIGHT_TO_COORD(NULL, << -1046.8315, -2518.1458, 12.9447 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
							TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -993.2343, -2426.4866, 19.1692 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedHitcher, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedHitcher, TRUE)
						SETTIMERB(0)
						bPlaceholderLine = TRUE
	//				ENDIF
	//			ENDIF
			ENDIF
		ENDIF
		
		IF TIMERB() < 7000
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
		ENDIF
		IF TIMERB() > 3000 AND TIMERB() < 7000
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ELIF TIMERB() > 7000
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			STOP_GAMEPLAY_HINT()
			IF TIMERB() > 8500
			AND NOT bPushInToFirstPerson
				IF (IS_PED_ON_FOOT(PLAYER_PED_ID()) AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
				OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					bPushInToFirstPerson = TRUE
				ENDIF
			ENDIF
		ENDIF
//		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF DOES_ENTITY_EXIST(pedHitcher)
		OR TIMERB() > 15000
			IF IS_ENTITY_AT_COORD(pedHitcher, << -1046.8315, -2518.1458, 12.9447 >>, << 20, 20, 5 >>)
			OR TIMERB() > 15000
				DELETE_PED(pedHitcher)
				MISSION_PASSED()
			ENDIF
		ENDIF
		
	ELIF thisEvent = GIRL_NUMBER
//		IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//			DoThanking.vWalkToPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_current_player_vehicle, <<4.5, 0, 0>>)
//		ENDIF
//		
//		DoThanking.aApproachPed = pedHitcher
//		DoThanking.	aApproachVehicle = veh_current_player_vehicle
//		DoThanking.	InCarAnimNameNPC = "indicate_left"
//		DoThanking.	InCarAnimDictNPC = "gestures@female"			
//		DoThanking.	InCarAnimNamePlayer	=  "nod_yes"
//		DoThanking.	InCarAnimDictPlayer	=  "gestures@male"	
//		DoThanking.AnimName = "yeah_sure"
//		DoThanking.AnimDict = "gestures@female"	
//		DoThanking.bDoNotRemovePed = FALSE
//		DoThanking.bLeaveDoorsOpen = FALSE
		
		IF NOT bPlaceholderLine
			//"Aww thanks! Take my number - we should meet up again."
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_TKB", CONV_PRIORITY_AMBIENT_HIGH)
			bPlaceholderLine = TRUE
		ENDIF
//		IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//			IF UPDATE_RUN_INCAR_THANKYOU(DoThanking)
//				IF DoThanking.bSuccess
					PRINTNL()
					PRINTSTRING("DoThanking.bSuccess")
					PRINTNL()
					IF NOT IS_PED_INJURED(pedHitcher)
						IF NOT IS_PED_RAGDOLL(pedHitcher)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
								TASK_PAUSE(NULL, 4500)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
	//							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
	//							TASK_PAUSE(NULL, 1000)
	//							TASK_USE_MOBILE_PHONE(NULL, TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 3323.4304, 5166.8315, 17.4060 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 3320.6838, 5186.0166, 17.4016 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 3303.6118, 5185.2686, 18.7155 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
								TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedHitcher, TRUE)
							
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
							
							REMOVE_PED_FROM_GROUP(pedHitcher)
							WAIT(1000)
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								//ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, MICHAEL_BOOK)
								SET_BOOTY_CALL_ID_ACTIVATED(BC_HITCHER_GIRL, TRUE, STRIP_CLUB_PLAYER_CHAR_MICHAEL)
								PRINTNL()
								PRINTSTRING("ADD_CONTACT_TO_PHONEBOOK-- called")
								PRINTNL()
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								//ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, FRANKLIN_BOOK)
								SET_BOOTY_CALL_ID_ACTIVATED(BC_HITCHER_GIRL, TRUE, STRIP_CLUB_PLAYER_CHAR_FRANKLIN)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								//ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, TREVOR_BOOK)
								SET_BOOTY_CALL_ID_ACTIVATED(BC_HITCHER_GIRL, TRUE, STRIP_CLUB_PLAYER_CHAR_TREVOR)
							ENDIF
							WAIT(8000)
							MISSION_PASSED()
						ENDIF
					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
	ELIF thisEvent = GIRL_CHEATER
		IF TIMERA() > 1000
			IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_JB", CONV_PRIORITY_AMBIENT_HIGH)
				IF NOT IS_PED_INJURED(pedBoyfriend)
					STOP_PED_SPEAKING(pedBoyfriend, TRUE)
					IF NOT DOES_BLIP_EXIST(blipHitcher)
						REMOVE_BLIP(blipHitcher)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipBoyfriend)
						blipBoyfriend = CREATE_BLIP_FOR_PED(pedBoyfriend, TRUE)
					ENDIF
					OPEN_SEQUENCE_TASK(seq)
//						TASK_PLAY_ANIM(NULL, "amb@lean@male@stationary@shoulder@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 17, PEDMOVEBLENDRATIO_WALK)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBoyfriend, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedBoyfriend, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoyfriend, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ADD_RELATIONSHIP_GROUP("rghBoyfriend", rghBoyfriend)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBoyfriend, rghBoyfriend)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_WANTED, RELGROUPHASH_COP, rghBoyfriend)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_SECURITY_GUARD, rghBoyfriend)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghBoyfriend, RELGROUPHASH_SECURITY_GUARD)
					SETTIMERA(0)
					eventStage = POST_DROPOFF_1
				ENDIF
			ENDIF
		ENDIF
		
//	ELIF thisEvent = GAY_MILITARY
//		IF NOT bPlaceholderLine
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
//			// "Here we are, can you just walk me in?"
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_MILA", CONV_PRIORITY_AMBIENT_HIGH)
//			IF NOT IS_PED_INJURED(pedHitcher)
//				CLEAR_PED_TASKS(pedHitcher)
//				TASK_LEAVE_ANY_VEHICLE(pedHitcher)
//			ENDIF
//			
////			CLEAR_PED_TASKS(PLAYER_PED_ID())
////			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 1250)
//			
//			bPlaceholderLine = TRUE
//		ENDIF
//		
////		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			IF NOT IS_PED_INJURED(pedHitcher)
//				OPEN_SEQUENCE_TASK(seq)
////					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1597.2518, 2817.4863, 16.2670 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkIntoCampPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//				CLEAR_SEQUENCE_TASK(seq)
//				SET_PED_KEEP_TASK(pedHitcher, TRUE)
//				IF IS_PED_IN_GROUP(pedHitcher)
//					REMOVE_PED_FROM_GROUP(pedHitcher)
//				ENDIF
//			ENDIF
//			blipLocation = CREATE_BLIP_FOR_COORD(vWalkIntoCampPos)
//			eventStage = POST_DROPOFF_1
////		ENDIF
//		
	ELIF thisEvent = RUNAWAY_BRIDE
		IF NOT bPlaceholderLine
			//"Aww thanks! Take my number - we should meet up again."
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_THX", CONV_PRIORITY_AMBIENT_HIGH)
			bPlaceholderLine = TRUE
		ENDIF
		IF NOT IS_PED_INJURED(pedHitcher)
			OPEN_SEQUENCE_TASK(seq)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
//				TASK_PAUSE(NULL, 1000)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -346.0290, 632.2272, 171.2525 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -346.6342, 627.3992, 170.3573 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -355.2473, 622.1964, 170.3572 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -355.6499, 615.3431, 170.3572 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -350.1630, 605.0463, 170.7292 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -333.1770, 594.9910, 170.7553 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
				TASK_PAUSE(NULL, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedHitcher, seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_KEEP_TASK(pedHitcher, TRUE)
			
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
			
			REMOVE_PED_FROM_GROUP(pedHitcher)
		ENDIF
		WAIT(2000)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			ADD_CONTACT_TO_PHONEBOOK(CHAR_HITCHER_GIRL, MICHAEL_BOOK)
//			PRINTSTRING("\nADD_CONTACT_TO_PHONEBOOK-- called\n")
//		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//			ADD_CONTACT_TO_PHONEBOOK(CHAR_HITCHER_GIRL, FRANKLIN_BOOK)
//		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//			ADD_CONTACT_TO_PHONEBOOK(CHAR_HITCHER_GIRL, TREVOR_BOOK)
//		ENDIF
		WAIT(3000)
		MISSION_PASSED()
	ENDIF
	
ENDPROC

PROC jealousBoyfFight()
	//	IF IS_PED_INJURED(pedHitcher)
	//	AND IS_PED_INJURED(pedBoyfriend)
	//		MISSION_FAILED()
	//	ELSE
			IF NOT IS_PED_INJURED(pedBoyfriend)
			AND NOT IS_PED_INJURED(pedHitcher)
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedBoyfriend, << 25, 25, 25 >>)
				AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, << 40, 40, 40 >>)
					CLEAR_PED_TASKS(pedBoyfriend)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedBoyfriend, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
					MISSION_PASSED()
				ENDIF
			ENDIF
	//	ENDIF
	
	IF NOT IS_PED_INJURED(pedBoyfriend)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBoyfriend, PLAYER_PED_ID())
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				TEXT_LABEL_23 sCurrentRootLabel
				sCurrentRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				IF ARE_STRINGS_EQUAL(sCurrentRootLabel, "REHH3_JB_2")
				OR ARE_STRINGS_EQUAL(sCurrentRootLabel, "REHH3_JB_4")
				OR ARE_STRINGS_EQUAL(sCurrentRootLabel, "REHH3_JB_6")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		ENDIF
		IF TIMERB() > 5000
			IF IS_PED_IN_COMBAT(pedBoyfriend, PLAYER_PED_ID())
				iGirlsReactions = 4
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bGirlAttacked
		IF DOES_ENTITY_EXIST(pedHitcher)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHitcher, PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedBoyfriend)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_JB2", CONV_PRIORITY_AMBIENT_HIGH)
					bGirlAttacked = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedHitcher)
		IF IS_PED_RAGDOLL(pedHitcher)
			CLEAR_PED_SECONDARY_TASK(pedHitcher)
//			IF IS_ENTITY_PLAYING_ANIM(pedHitcher, "random@hitch_lift", "001445_01_gangintimidation_1_female_idle_b")
//				STOP_ANIM_PLAYBACK(pedHitcher, AF_PRIORITY_HIGH, TRUE)
//			ENDIF
		ENDIF
		
//		// CHECK FOR GIRL LEAVING VEH
//		IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
//			veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(pedHitcher)
//			IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//				IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 0.5
//					iGirlsReactions = 1
//				ENDIF
//			ENDIF
//		ENDIF
		
		// CHECK WHETHER GIRL HAS GONE TO INJURED GUY
		IF IS_PED_INJURED(pedBoyfriend)
		AND IS_ENTITY_AT_COORD(pedHitcher, GET_ENTITY_COORDS(pedBoyfriend, FALSE), << 1, 1, 5 >>)
			iGirlsReactions = 5
		ENDIF
		
		// CHECK IF GIRL IS RAGDOLLING
//		IF IS_PED_RAGDOLL(pedHitcher)
//			CLEAR_PED_TASKS(pedHitcher)
//		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedBoyfriend)
//			CLEAR_PED_TASKS_IMMEDIATELY(pedBoyfriend)
//			SET_PED_COMBAT_ATTRIBUTES(pedBoyfriend, CA_ALWAYS_FLEE, TRUE)
			TASK_COMBAT_PED(pedBoyfriend, PLAYER_PED_ID())
			MISSION_FAILED()
		ELSE
			MISSION_FAILED()
		ENDIF
	ENDIF
	
	// CHECK WHETHER WE SHOULD MAKE GIRL GO TO THE GUY
	IF NOT bGoneToInjuredBoyfriend
		IF IS_PED_INJURED(pedBoyfriend)
			IF DOES_BLIP_EXIST(blipBoyfriend)
				REMOVE_BLIP(blipBoyfriend)
			ENDIF
			IF NOT IS_PED_INJURED(pedHitcher)
				iGirlsReactions = 3
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bGirlReactedToWeapon
		IF NOT IS_PED_INJURED(pedHitcher)
		AND NOT IS_PED_INJURED(pedBoyfriend)
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
//				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHitcher)
//				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHitcher)
//				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedBoyfriend)
//				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedBoyfriend)
				IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
					iGirlsReactions = 2
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_INJURED(pedBoyfriend)
			IF DOES_BLIP_EXIST(blipBoyfriend)
				REMOVE_BLIP(blipBoyfriend)
			ENDIF
			REMOVE_PED_FOR_DIALOGUE(HitcherDialogueStruct, 4)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBoyfriend, PLAYER_PED_ID())
			AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				iGirlsReactions = 2
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(pedHitcher)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHitcher, pedBoyfriend)
					iGirlsReactions = 2
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iGirlsReactions
	
		// FALL BACK TO ONCE DONE OTHER ACTIONS
		CASE 0
			IF NOT bGoneToInjuredBoyfriend
				IF NOT IS_PED_INJURED(pedHitcher)
				AND NOT IS_PED_INJURED(pedBoyfriend)
					IF NOT IS_PED_IN_ANY_VEHICLE(pedHitcher)
					AND IS_PED_IN_MELEE_COMBAT(pedBoyfriend)
					//	continualTurnToFacePlayer(pedHitcher, iTurnStage)
						IF NOT bGirlPlayingPanicAnim
							OPEN_SEQUENCE_TASK(Seq)
								TASK_LOOK_AT_ENTITY(NULL, pedBoyfriend, -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
								TASK_PLAY_ANIM(NULL, "random@hitch_lift", "f_distressed_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedBoyfriend, -1)
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(pedHitcher, Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							bGirlPlayingPanicAnim = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				MISSION_PASSED()
			ENDIF
		BREAK
		
		// LEAVING CAR
		CASE 1
			IF NOT IS_PED_INJURED(pedHitcher)
				TASK_LEAVE_ANY_VEHICLE(pedHitcher)
				// "Hey baby, what's up?"
				SETTIMERB(0)
				iGirlsReactions = 0
			ENDIF
		BREAK
		
		// AIMING
		CASE 2
			IF NOT bGirlReactedToWeapon
				IF NOT IS_PED_INJURED(pedHitcher)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
						// "Shit, he's got a gun!"
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_GUN", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_AGG", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					IF NOT IS_PED_INJURED(pedHitcher)
						CLEAR_PED_TASKS(pedHitcher)
//						TASK_COWER(pedHitcher, 7000)
						TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 1000, -1)
						IF NOT IS_PED_INJURED(pedBoyfriend)
							PLAY_PAIN(pedBoyfriend, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
							TASK_SMART_FLEE_PED(pedBoyfriend, PLAYER_PED_ID(), 1000, -1)
						ENDIF
					ENDIF
					MISSION_PASSED()
					bGirlReactedToWeapon = TRUE
					iGirlsReactions = 0
				ENDIF
			ENDIF
		BREAK
		
		// INJURED BOYFRIEND
		CASE 3
			IF NOT IS_PED_INJURED(pedHitcher)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_PED_INJURED(pedHitcher)
					PLAY_PAIN(pedHitcher, AUD_DAMAGE_REASON_SCREAM_TERROR)
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedHitcher, "WHIMPER", "WAVELOAD_PAIN_FEMALE")
				ENDIF
				// "You bastard! What have you done?"
				CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_BAS", CONV_PRIORITY_AMBIENT_HIGH)
				IF NOT IS_PED_INJURED(pedHitcher)
				AND DOES_ENTITY_EXIST(pedBoyfriend)
					CLEAR_PED_TASKS(pedHitcher)
					OPEN_SEQUENCE_TASK(seq)
						TASK_CLEAR_LOOK_AT(NULL)
						TASK_PLAY_ANIM(NULL, "random@hitch_lift", "f_distressed_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
						TASK_LOOK_AT_ENTITY(NULL, pedBoyfriend, -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(pedBoyfriend, FALSE), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedBoyfriend, -1)
						TASK_PLAY_ANIM(NULL, "random@hitch_lift", "f_distressed_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						TASK_PLAY_ANIM(NULL, "random@hitch_lift", "f_distressed_loop", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedHitcher, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedHitcher, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHitcher, FALSE)
//					REMOVE_ALL_SHOCKING_EVENTS(FALSE)
//					SET_PED_CONFIG_FLAG(PED_CONFIG_FLAGS IGNORE SHOCKING EVENT)
//					SET_PED_IS_CRYING(pedHitcher, TRUE)
//					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						WAIT(0)
//					ENDWHILE
//					SET_AUDIO_SCRIPT_CLEANUP_TIME(10000)
					WAIT(10000)
					bGoneToInjuredBoyfriend = TRUE
					iGirlsReactions = 0
				ENDIF
			ENDIF
		BREAK
		
		// SHOUTING TO STOP
		CASE 4
			IF GET_RANDOM_BOOL()
				IF NOT IS_PED_INJURED(pedHitcher)
					//"Oh my god, stop!"
					IF IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 15, 15, 15 >>)
						IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_NOO", CONV_PRIORITY_AMBIENT_HIGH)
							SETTIMERB(0)
							iGirlsReactions = 0
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedBoyfriend)
					//"I'm gonna kill you"
					IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_FHT", CONV_PRIORITY_AMBIENT_HIGH)
						SETTIMERB(0)
						iGirlsReactions = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// ATTENDING TO BOYFRIEND
		CASE 5
			IF NOT IS_PED_INJURED(pedHitcher)
				KILL_FACE_TO_FACE_CONVERSATION()
				WAIT(0)
				//"Oh my god, baby!"
				IF CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_ATT", CONV_PRIORITY_AMBIENT_HIGH)
					iGirlsReactions = 0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC keepGatesShut()
	IF NOT IS_PED_INJURED(pedBoyfriend)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 655.776794, 1282.449951, 359.048004 >>, << 802.424072, 1278.076660, 382.486572 >>, 77)
		OR IS_ENTITY_IN_ANGLED_AREA(pedBoyfriend, << 655.776794, 1282.449951, 359.048004 >>, << 802.424072, 1278.076660, 382.486572 >>, 77)
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<801.714844,1270.138306,359.285522>>, 6.0, prop_fnclink_03gate1)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<801.714844,1270.138306,359.285522>>, FALSE, 0.0)
			ENDIF
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<802.921814,1281.675049,359.296234>>, 6.0, prop_fnclink_03gate1)
		   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<802.921814,1281.675049,359.296234>>, FALSE, 0.0)
			ENDIF
		ELSE
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 801.714844, 1270.138306, 359.285522 >>, 6.0, prop_fnclink_03gate1)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, << 801.714844, 1270.138306, 359.285522 >>, TRUE, 0.0)
			ENDIF
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 802.921814, 1281.675049, 359.296234 >>, 6.0, prop_fnclink_03gate1)
		   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, << 802.921814, 1281.675049, 359.296234 >>, TRUE, 0.0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC chatInVehicle()
	
//	IF TIMERA() > 11000
	IF NOT IS_ANY_INTERRUPTION_CONVERSATION_PLAYING()
		MANAGE_CONVERSATION(TRUE)
	ENDIF
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SWITCH iChatInVehicle
		
			CASE 0
				IF thisEvent = BUSINESSMAN
					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = GIRL_NUMBER
					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = GIRL_CHEATER
					iStartDialogueTimer = GET_GAME_TIMER() + 2000
//				ELIF thisEvent = GAY_MILITARY
//					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = RUNAWAY_BRIDE
					iStartDialogueTimer = GET_GAME_TIMER()
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 1
				IF iStartDialogueTimer < GET_GAME_TIMER()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHT", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_NUMBER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = GIRL_CHEATER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT", CONV_PRIORITY_AMBIENT_HIGH)
	//				ELIF thisEvent = GAY_MILITARY
	//					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_CHT", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = RUNAWAY_BRIDE
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 2
				IF iStartDialogueTimer < GET_GAME_TIMER()
					IF thisEvent = BUSINESSMAN
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_ASKM", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_ASKF", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_ASKT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ELIF thisEvent = GIRL_NUMBER
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHTm2", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHTf2", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHTt2", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ELIF thisEvent = GIRL_CHEATER
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHTM", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHTF", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHTT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ELIF thisEvent = RUNAWAY_BRIDE
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RESPM", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RESPF", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF NOT bBoyfriendCreated
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RESPT", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
						ENDIF
					ENDIF
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 3
				IF thisEvent = BUSINESSMAN
					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = GIRL_NUMBER
					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = GIRL_CHEATER
					iStartDialogueTimer = GET_GAME_TIMER() + 500
//				ELIF thisEvent = GAY_MILITARY
//					iStartDialogueTimer = GET_GAME_TIMER()
				ELIF thisEvent = RUNAWAY_BRIDE
					iStartDialogueTimer = GET_GAME_TIMER()
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 4
				IF iStartDialogueTimer < GET_GAME_TIMER()
					IF thisEvent = BUSINESSMAN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHT2", CONV_PRIORITY_AMBIENT_HIGH)
						iChatInVehicle ++
					ELIF thisEvent = GIRL_NUMBER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT2", CONV_PRIORITY_AMBIENT_HIGH)
						iChatInVehicle ++
					ELIF thisEvent = GIRL_CHEATER
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT2", CONV_PRIORITY_AMBIENT_HIGH)
						iChatInVehicle ++
					ELIF thisEvent = RUNAWAY_BRIDE
						IF bBoyfriendCreated
						AND NOT bChasedByGroom
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_OKM", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_OKF", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_OKT", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							iChatInVehicle ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF thisEvent = BUSINESSMAN
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RESPM", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RESPF", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_RESPT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_pchatm", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_pchatf", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_pchatt", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT2M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT2F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT2T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT1", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 6
				IF thisEvent = BUSINESSMAN
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHAT", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT3", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT3", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = RUNAWAY_BRIDE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT1b", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT1c", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT1d", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 7
				IF thisEvent = BUSINESSMAN
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHATb", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHATc", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHATd", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT3M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT3F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT3T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT3M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT3F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT3T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 8
				IF thisEvent = BUSINESSMAN
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CHATe", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT4", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT4", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = RUNAWAY_BRIDE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2b", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2c", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2d", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 9
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT4M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT4F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT4T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT4M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT4F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT4T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2e", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 10
				IF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT5", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT5", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = RUNAWAY_BRIDE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2f", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2g", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT2h", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 11
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT5M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT5F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT5T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT5M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT5F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT5T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT3", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 12
				IF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT6", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT6", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = RUNAWAY_BRIDE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT3b", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT3c", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT3d", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 13
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT6M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT6F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT6T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT6M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT6F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT6T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT4", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 14
				IF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT7", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT7", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = RUNAWAY_BRIDE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT4b", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT4c", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CHAT4d", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 15
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT7M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT7F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT7T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT7M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT7F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT7T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = RUNAWAY_BRIDE
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_RUNOUT", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 16
				IF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT8", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT8", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 17
				IF thisEvent = GIRL_CHEATER
					iStartDialogueTimer = GET_GAME_TIMER() + 700
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 18
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT8M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT8F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT8T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ELIF thisEvent = GIRL_CHEATER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT8M", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT8F", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT8T", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 19
				IF thisEvent = GIRL_NUMBER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CHT9", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF thisEvent = GIRL_CHEATER
					CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT9", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 20
				IF thisEvent = GIRL_CHEATER
					iStartDialogueTimer = GET_GAME_TIMER() + 300
				ENDIF
				iChatInVehicle ++
			BREAK
			
			CASE 21
				IF iStartDialogueTimer < GET_GAME_TIMER()
					IF thisEvent = GIRL_CHEATER
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT9M", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT9F", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CHT9T", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
					iChatInVehicle ++
				ENDIF
			BREAK
			
			CASE 22
				IF NOT IS_PED_INJURED(pedHitcher)
					TASK_PLAY_ANIM(pedHitcher, "facials@gen_female@base", "mood_sleeping_1", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY)
				ENDIF
				iChatInVehicle ++
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC chatCult()
	IF READY_FOR_CULT_DIALOGUE(vHitcherDestinationPos)
		MANAGE_CONVERSATION(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		IF thisEvent = BUSINESSMAN
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_WAY", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_NUMBER
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_CULT", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_CHEATER
			IF NOT IS_PED_INJURED(pedHitcher)
				IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
					CLEAR_PED_SECONDARY_TASK(pedHitcher)
				ENDIF
			ENDIF
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_CULT", CONV_PRIORITY_AMBIENT_HIGH)
//		ELIF thisEvent = GAY_MILITARY
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_CULT", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = RUNAWAY_BRIDE
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_CULT", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
	IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
		MANAGE_CONVERSATION(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		IF thisEvent = BUSINESSMAN
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_CULT", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_NUMBER
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_NEAR", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = GIRL_CHEATER
			IF NOT IS_PED_INJURED(pedHitcher)
				IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedHitcher)
					CLEAR_PED_SECONDARY_TASK(pedHitcher)
				ENDIF
			ENDIF
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_NEAR", CONV_PRIORITY_AMBIENT_HIGH)
//		ELIF thisEvent = GAY_MILITARY
//			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_CULT", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = RUNAWAY_BRIDE
			CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_NEAR", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_IT_THE_RIGHT_TIME_TO_RUN_THIS_EVENT()
	// ensures script cleans up if its not within its availability time frame
	IF thisEvent = BUSINESSMAN
		IF (GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) >= 4 AND GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) < 21)
			RETURN TRUE
		ENDIF
	ENDIF
	IF thisEvent = GIRL_NUMBER
		IF (GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) >= 9 AND GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) < 21)
			RETURN TRUE
		ENDIF
	ENDIF
	IF thisEvent = GIRL_CHEATER
		IF (GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) >= 9 AND GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) < 21)
			RETURN TRUE
		ENDIF
	ENDIF
//	IF thisEvent = GAY_MILITARY
//		IF (GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) >= 4 AND GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) < 21)
//			RETURN TRUE
//		ENDIF
//	ENDIF
	
	// checks so that if the event is already interacted with then it won't clean up
	IF bAskedForVeh
	OR iApproachStage > 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MICHAEL_PLAYING_HITCH_LIFT_2()
	// Michael can't play Hitch Lift 2
	IF thisEvent = GIRL_NUMBER
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		PRINTSTRING("\n @@@@@@@@@@@@@@@ Michael can't play Hitch Lift 2 @@@@@@@@@@@@@@@ \n")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//=================================== START OF MAIN SCRIPT ================================

SCRIPT (coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTSTRING("re_hitch_lift launched ")
	PRINTVECTOR(vInput)
	PRINTNL()
	
	// work out which variation is being launched
	getWorldPoint()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedHitcher)
			IF IS_PED_IN_GROUP(pedHitcher)
				REMOVE_PED_FROM_GROUP(pedHitcher)
			ENDIF
		ENDIF
		missionCleanup()
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_HITCHLIFT, iSelectedVariation)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bLeavingActivationArea
		
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_HL")
//			IF IS_IT_THE_RIGHT_TIME_TO_RUN_THIS_EVENT()
//			IF NOT IS_MICHAEL_PLAYING_HITCH_LIFT_2()
				SWITCH ambStage
				
					CASE ambCanRun
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						IF NOT bVariablesInitialised
							initialiseEventVariables()
						ELSE
							loadInitialAssets()
						ENDIF
						IF bAssetsLoaded 
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ENDIF
					BREAK
					
					CASE ambRunning
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SWITCH thisSection
							
								CASE MISSION_STAGES
//									IF NOT HAS_PLAYER_AGGROED_PED(pedHitcher, aggroReason, iLockOnTimer, iBitFieldDontCheck)
									IF IS_PLAYER_INTERFERING_WITH_EVENT()
									AND eventStage != POST_DROPOFF_2
									AND eventStage != AT_CULT
										IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
											SET_RANDOM_EVENT_ACTIVE()
										ENDIF
										thisSection = AGGRO_REACTIONS
									ENDIF
										SWITCH eventStage
											CASE SETUP_STAGE
												createScene()
											BREAK
											
											CASE SECONDARY_SETUP_STAGE
												setupInitialTasks()
											BREAK
											
//											CASE LOAD_REMAINING_ASSETS
//												loadRemainingAssets()
//											BREAK
											
											CASE STAGE_1
												IF IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
													PRINTLN("@@@@@@@@ PLAYER IS ON A BICYCLE @@@@@@@@")
												ENDIF
												iEventCurrentTimer = GET_GAME_TIMER()
												IF iApproachStage < 1
												AND NOT bAskedForVeh
													trackingCarsHitching()
												ENDIF
												IF thisEvent = BUSINESSMAN
													pickingUpTheBusinessman()
												ELIF thisEvent = GIRL_NUMBER
													pickingUpTheGirl()
												ELIF thisEvent = GIRL_CHEATER
													pickingUpTheCheater()
//												ELIF thisEvent = GAY_MILITARY
//													g_AmbientAreaData[eAMBAREA_ARMYBASE].bSuppressGateGuards = TRUE
//													pickingUpTheGuy()
												ELIF thisEvent = RUNAWAY_BRIDE
													pickingUpTheBride()
												ENDIF
											BREAK
											
											// If the hitcher is in the vehicle they tell the player to drive them to their destination
											CASE STAGE_2
//												HANDLE_BUDDY_PERMANENT_HEAD_TRACK(pedHitcher, FALSE)
												destinationInstructions()
											BREAK
											
											// Checks if the player has reached the destination with the hitcher (plus checks along the way)
											CASE STAGE_3
//												HANDLE_BUDDY_PERMANENT_HEAD_TRACK(pedHitcher, FALSE)
												IF thisEvent = BUSINESSMAN
//												OR thisEvent = GAY_MILITARY
													timeLimitCountdown()
													timeLimitSpeech()
												ELIF thisEvent = GIRL_CHEATER
													createJealousBoyfriend()
													keepGatesShut()
												ELIF thisEvent = RUNAWAY_BRIDE
													createJealousBoyfriend()
													looseOrKillGroom()
												ENDIF
												IF NOT bChasedByGroom
													atDestination()
													chatInVehicle()
													chatCult()
													chatInterruptions()
													dialogueInterruptionCheck()
													groupSeparationCheck()
													onFootCheck()
													stationaryCheck()
												ENDIF
												dontFollowPlayerInWrongVehicle()
											BREAK
											
											CASE AT_DESTINATION
												pedThankInVehicle()
												IF thisEvent = GIRL_CHEATER
													keepGatesShut()
//												ELIF thisEvent = GAY_MILITARY
//													onFootCheck()
//													timeLimitCountdown()
												ENDIF
											BREAK
											
											CASE POST_DROPOFF_1
												pedDroppedOff()
												IF thisEvent = GIRL_CHEATER
													keepGatesShut()
												ENDIF
											BREAK
											
											CASE POST_DROPOFF_2
												IF thisEvent = GIRL_CHEATER
													jealousBoyfFight()
													keepGatesShut()
												ENDIF
											BREAK
											
											CASE TOO_LATE
												IF NOT IS_PED_INJURED(pedHitcher)
													IF IS_ENTITY_AT_COORD(pedHitcher, vHitcherDestinationPos, << 200, 200, 200 >>)
//														IF thisEvent = GAY_MILITARY
//															OPEN_SEQUENCE_TASK(seq)
//																TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1587.5797, 2794.0452, 15.9006 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//								//								TASK_STAND_STILL(NULL, -1)
//																TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_GUARD_STAND", 0, TRUE) //WORLD_HUMAN_HANG_OUT_STREET
//															CLOSE_SEQUENCE_TASK(seq)
//															TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//															CLEAR_SEQUENCE_TASK(seq)
//		//													IF NOT IS_PED_INJURED(pedBoyfriend)
//		//														IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1585.486938,2797.353271,17.827217>>, 3.5, PROP_SEC_BARIER_04B)
//		//															SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARIER_04B, <<-1585.486938,2797.353271,17.827217>>, FALSE, 0.0)
//		//														ENDIF
//		//													ENDIF
//														ELSE
															TASK_FOLLOW_NAV_MESH_TO_COORD(pedHitcher, vHitcherDestinationPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//														ENDIF
													ELSE
														TASK_WANDER_STANDARD(pedHitcher)
													ENDIF
													SET_PED_KEEP_TASK(pedHitcher, TRUE)
													WAIT(5000)
												ENDIF
												MISSION_FAILED()
											BREAK
											
											CASE AT_CULT
												//WAIT FOR ALTRUIST CULT TO DO ITS THING
											BREAK
										ENDSWITCH
									
									IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
									AND NOT IS_CULT_FINISHED()
									AND eventStage != AT_CULT
										IF NOT IS_PED_INJURED(pedHitcher)
											IF IS_ENTITY_AT_COORD(pedHitcher, vCult, << 5, 5, 5 >>)
												PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
												WAIT(0)
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												eventStage = AT_CULT
											ENDIF
										ENDIF
									ENDIF
									IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
										IF DOES_ENTITY_EXIST(pedHitcher)
											DELETE_PED(pedHitcher)
										ENDIF
										MISSION_PASSED()
									ENDIF
								BREAK
								
								CASE AGGRO_REACTIONS
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(0)
									IF thisEvent = BUSINESSMAN
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH1_AGG", CONV_PRIORITY_AMBIENT_HIGH)
									ELIF thisEvent = GIRL_NUMBER
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH2_AGG", CONV_PRIORITY_AMBIENT_HIGH)
									ELIF thisEvent = GIRL_CHEATER
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH3_AGG", CONV_PRIORITY_AMBIENT_HIGH)
										IF NOT IS_PED_INJURED(pedBoyfriend)
											CLEAR_PED_TASKS(pedBoyfriend)
											TASK_SMART_FLEE_PED(pedBoyfriend, PLAYER_PED_ID(), 1000, -1)
											SET_PED_KEEP_TASK(pedBoyfriend, TRUE)
										ENDIF
//									ELIF thisEvent = GAY_MILITARY
//										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH4_AGG", CONV_PRIORITY_AMBIENT_HIGH)
									ELIF thisEvent = RUNAWAY_BRIDE
										CREATE_CONVERSATION(HitcherDialogueStruct, strDialogueBlock, "REHH5_AGG", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
									IF NOT IS_PED_INJURED(pedHitcher)
										CLEAR_PED_TASKS(pedHitcher)
										TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 1000, -1)
										SET_PED_KEEP_TASK(pedHitcher, TRUE)
										MISSION_FAILED()
									ENDIF
//									SWITCH aggroReason
//										CASE EAggro_Danger //player is wanted or shot a victim near them
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//										
//										CASE EAggro_ShotNear //shot near or near miss
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//										
//										CASE EAggro_HostileOrEnemy //stick up
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//										
//										CASE EAggro_Attacked //attacked
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//										
//										CASE EAggro_HeardShot
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//										
//										CASE EAggro_Shoved
//											IF NOT IS_PED_INJURED(pedHitcher)
//												CLEAR_PED_TASKS(pedHitcher)
//												TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
//												SET_PED_KEEP_TASK(pedHitcher, TRUE)
//												MISSION_FAILED()
//											ENDIF
//										BREAK
//									ENDSWITCH
								BREAK
							ENDSWITCH
						ENDIF
					BREAK
					
				ENDSWITCH
//			ELSE
//				PRINTSTRING("\n@@@@@@@@@@@@@@@ NOT RIGHT_TIME_TO_RUN_THIS_EVENT @@@@@@@@@@@@@@@\n")
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF

		//------------------- CHECKS THAT RUN THROUGHOUT THE SCRIPT -------------------
		
		IF eventStage > STAGE_1
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF DOES_ENTITY_EXIST(pedHitcher)
					IF NOT IS_PED_INJURED(pedHitcher)
						IF NOT IS_ENTITY_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), << 100, 100, 100 >>)
							MISSION_FAILED()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Check for dead/injured hitcher
		IF eventStage > SETUP_STAGE
		AND eventStage < POST_DROPOFF_2
			IF IS_PLAYER_PLAYING(PLAYER_ID())
//				IF DOES_ENTITY_EXIST(pedHitcher)
//					IF NOT IS_ENTITY_DEAD(pedHitcher)
						IF IS_PED_INJURED(pedHitcher)
							IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
								SET_RANDOM_EVENT_ACTIVE()
							ENDIF
							MISSION_FAILED()
						ELSE
							IF NOT IS_PED_RAGDOLL(pedHitcher)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHitcher, PLAYER_PED_ID(), FALSE)
									CLEAR_PED_TASKS(pedHitcher)
									TASK_SMART_FLEE_PED(pedHitcher, PLAYER_PED_ID(), 250, -1)
									MISSION_FAILED()
								ENDIF
							ENDIF
						ENDIF
//					ELSE
//						MISSION_FAILED()
//					ENDIF
//				ELSE
//					missionCleanup()
//				ENDIF
			ENDIF
		ENDIF
		
		//------------------------------ DEBUGGING STUFF ------------------------------ 

		#IF IS_DEBUG_BUILD

			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				IF thisEvent = GIRL_NUMBER
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, FRANKLIN_BOOK)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, MICHAEL_BOOK)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, TREVOR_BOOK)
					ENDIF
				ENDIF
//				IF thisEvent = GAY_MILITARY
//					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 50,FALSE,TRUE)
//				ENDIF
				IF thisEvent = BUSINESSMAN
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 100,FALSE,TRUE)
				ENDIF
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				KILL_ANY_CONVERSATION()
				MISSION_FAILED()
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				IF eventStage <= STAGE_2
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vJSkipPos
						FLOAT fJSkipHdg
						IF thisEvent = BUSINESSMAN
							vJSkipPos = << -3093.4092, 768.1236, 18.9140 >>
							fJSkipHdg = 216.5445
						ENDIF
						IF thisEvent = GIRL_NUMBER
							vJSkipPos = << 222.5563, 3055.4683, 41.2894 >>
							fJSkipHdg = 3.6612
						ENDIF
						IF thisEvent = GIRL_CHEATER
							vJSkipPos = << 1691.5544, 4710.9243, 41.4660 >>
							fJSkipHdg = 191.2233
						ENDIF
//						IF thisEvent = GAY_MILITARY
//							vJSkipPos = << 1842.9058, 3261.4016, 43.2928 >>
//							fJSkipHdg = 33.1142
//						ENDIF
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vJSkipPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fJSkipHdg)
						ELSE
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vJSkipPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fJSkipHdg)
						ENDIF
						IF NOT IS_PED_INJURED(pedHitcher)
							SET_ENTITY_COORDS(pedHitcher, vCreateHitchhikerPos)
							SET_ENTITY_HEADING(pedHitcher, fCreateHitchhikerHdg)
							IF thisEvent = BUSINESSMAN
								TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
							ELSE
								TASK_PLAY_ANIM(pedHitcher, "random@hitch_lift", "idle_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
							ENDIF
						ENDIF
						IF IS_PED_IN_GROUP(pedHitcher)
							REMOVE_PED_FROM_GROUP(pedHitcher)
						ENDIF
						REMOVE_BLIP(blipLocation)
						
						bToldToHurryOnce = FALSE
						bToldToHurryTwice = FALSE
						bTimerQuicker = FALSE
						bUnsuitableVehRemark = FALSE
						bAskedForVeh = FALSE
						bHasHitcherThanksAsEnters = FALSE
						bLeavingActivationArea = FALSE
						bSaidTime = FALSE
						bGirlWalking = FALSE
						
						iApproachStage = 0
						
						eventStage = SECONDARY_SETUP_STAGE
					ENDIF
				ENDIF
				IF eventStage >= STAGE_3
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vJSkipPos
						FLOAT fJSkipHdg
						IF thisEvent = BUSINESSMAN
							vJSkipPos = << -3067.9060, 739.9447, 20.5899 >>
							fJSkipHdg = 219.3904
						ENDIF
						IF thisEvent = GIRL_NUMBER
							vJSkipPos = << 218.8873, 3137.2097, 41.3549 >>
							fJSkipHdg = 356.5419
						ENDIF
						IF thisEvent = GIRL_CHEATER
							vJSkipPos = << 1708.9290, 4619.9155, 41.9886 >>
							fJSkipHdg = 222.3405
						ENDIF
//						IF thisEvent = GAY_MILITARY
//							vJSkipPos = << 1783.9132, 3360.5054, 39.3184 >>
//							fJSkipHdg = 28.9975
//						ENDIF
						IF thisEvent = RUNAWAY_BRIDE
							vJSkipPos = << 1783.9132, 3360.5054, 39.3184 >>
							fJSkipHdg = 28.9975
						ENDIF
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vJSkipPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fJSkipHdg)
							IF NOT IS_PED_INJURED(pedHitcher)
								SET_PED_INTO_VEHICLE(pedHitcher, GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
							ENDIF
						ELSE
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vJSkipPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fJSkipHdg)
							IF NOT IS_PED_INJURED(pedHitcher)
								SET_PED_COORDS_KEEP_VEHICLE(pedHitcher, vJSkipPos)
								SET_ENTITY_HEADING(pedHitcher, fJSkipHdg)
							ENDIF
						ENDIF
						
//						bSaidThanks = FALSE
//						bStartScene = FALSE
						bPlaceholderLine = FALSE
						bGirlAttacked = FALSE
						bGirlPlayingPanicAnim = FALSE
						bGirlReactedToWeapon = FALSE
						bGoneToInjuredBoyfriend = FALSE
						bHasHitcherThanksAsEnters = FALSE
						
						eventStage = STAGE_2
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				// If the keyboard key J has been pressed it creates a vehicle and warps the player into it 
				IF eventStage <= STAGE_1
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							v_temp_player_coord = GET_ENTITY_COORDS(PLAYER_PED_ID())
							REQUEST_MODEL(FBI)
							
							IF HAS_MODEL_LOADED(FBI)
								IF NOT DOES_ENTITY_EXIST(veh_player_J_car)
									veh_player_J_car = CREATE_VEHICLE(FBI, <<v_temp_player_coord.x, v_temp_player_coord.y + 4, v_temp_player_coord.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
									SET_VEHICLE_ON_GROUND_PROPERLY(veh_player_J_car)
									TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_player_J_car, VS_DRIVER)
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(blipHitcher)
								CLEAR_PED_TASKS_IMMEDIATELY(pedHitcher)
								veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(pedHitcher)
									veh_player_J_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									SET_PED_INTO_VEHICLE(pedHitcher, veh_player_J_car, VS_FRONT_RIGHT)
									TASK_CLEAR_LOOK_AT(pedHitcher)
									eventStage = STAGE_2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// If the keyboard key J has been pressed it warps the player to the destination the hitcher wants to get to
				IF eventStage >= STAGE_3
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vJSkipPos
						FLOAT fJSkipHdg
						
						
						IF thisEvent = BUSINESSMAN
							IF is_first_loc_skipped
								vJSkipPos = vHitcherDestinationPos 
								fJSkipHdg = 153.1244
							ELSE
								vJSkipPos = << -1036.2357, -2510.7454, 19.0661 >>
								fJSkipHdg = 153.1244
								is_first_loc_skipped = TRUE
							ENDIF
						ENDIF
						
						IF thisEvent = GIRL_NUMBER
							IF is_first_loc_skipped
								vJSkipPos = vHitcherDestinationPos
								fJSkipHdg = 318.5342
							ELSE
								vJSkipPos =  << 3269.8535, 5167.8301, 18.7313 >>
								fJSkipHdg = 271.7643
								is_first_loc_skipped = TRUE
							ENDIF
						ENDIF
						
//						IF thisEvent = GAY_MILITARY
//							IF is_first_loc_skipped
//								vJSkipPos = vHitcherDestinationPos
//								fJSkipHdg = 41.4142
//							ELSE
//								vJSkipPos = << -1550.4100, 2758.7117, 16.7791 >>
//								fJSkipHdg = 41.4142
//								is_first_loc_skipped = TRUE
//							ENDIF
//						ENDIF
						
						IF thisEvent = GIRL_CHEATER
							IF is_first_loc_skipped
								vJSkipPos = vHitcherDestinationPos
								fJSkipHdg = 360.0517
							ELSE
								vJSkipPos = << 847.3947, 1348.3402, 351.2344 >>
								fJSkipHdg = 217.0009
								is_first_loc_skipped = TRUE
							ENDIF
						ENDIF
						
						IF thisEvent = RUNAWAY_BRIDE
							vJSkipPos = vHitcherDestinationPos
							fJSkipHdg = 28.9975
						ENDIF
						
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vJSkipPos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fJSkipHdg)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT

//
//PROC approachThePlayer()
//
//	SWITCH iApproachStage
//		CASE 0
//			IF NOT IS_PED_INJURED(pedHitcher)
//				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedHitcher, <<8, 8, 8>>)
//				AND IS_ENTITY_ON_SCREEN(pedHitcher)
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_LOOK_AT_ENTITY(NULL, veh_current_player_vehicle, 30000, SLF_FAST_TURN_RATE)
//								TASK_PLAY_ANIM(NULL, "amb@lean@male@stationary@shoulder@exit", "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//							CLEAR_SEQUENCE_TASK(seq)
//						ENDIF
//						iApproachStage ++
//					ELSE
//						IF NOT bAskedForVeh
//							TASK_LOOK_AT_ENTITY(pedHitcher, PLAYER_PED_ID(), 7000)
//							PRINT_STRING_WITH_LITERAL_STRING("STRING", "I need a lift - you got a car?", 7500, 1)
//							bAskedForVeh = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 1
//			IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//				IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 9
//					IF NOT IS_PED_INJURED(pedHitcher)
//						IF NOT IS_ENTITY_PLAYING_ANIM(pedHitcher, "amb@lean@male@stationary@shoulder@exit", "exit")
//							IF IS_ENTITY_AT_ENTITY(pedHitcher, veh_current_player_vehicle, <<8.5, 8.5, 8.5>>)
//								OPEN_SEQUENCE_TASK(seq)
//									TASK_LOOK_AT_ENTITY(NULL, veh_current_player_vehicle, 10000, SLF_FAST_TURN_RATE)
//									TASK_TURN_PED_TO_FACE_ENTITY(NULL, veh_current_player_vehicle)
//								//	TASK_PLAY_ANIM(NULL, "AMB@thumbing_lift", HailAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_LOOPING)
//								CLOSE_SEQUENCE_TASK(seq)
//								TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//								CLEAR_SEQUENCE_TASK(seq)
//								iApproachStage ++
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 2
//			IF NOT IS_PED_INJURED(pedHitcher)
//			AND  IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//				IF IS_ENTITY_AT_ENTITY(pedHitcher, veh_current_player_vehicle, <<8.5, 8.5, 8.5>>)
//					IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
//					//	TASK_GO_STRAIGHT_TO_COORD(pedHitcher, GET_COORDS_FOR_NEAREST_CAR_WINDOW(), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//						TASK_GO_TO_ENTITY(pedHitcher, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 2.5)
//						iApproachStage ++
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 3
//			IF NOT IS_PED_INJURED(pedHitcher)
//				CLEAR_PED_TASKS(pedHitcher)
//				OPEN_SEQUENCE_TASK(seq)
//					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000)
//					//TASK_PLAY_ANIM(NULL, "AMB@escaped_convict", "convict_wavevar2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(pedHitcher, seq)
//				CLEAR_SEQUENCE_TASK(seq)
//				IF NOT bAskedForALift
//					IF thisEvent = GIRL_NUMBER
//						PRINT_STRING_WITH_LITERAL_STRING("STRING", "Hey, you gonna give me a ride?", 2000, 1)
//					ENDIF
//					IF thisEvent = GIRL_CHEATER
//						PRINT_STRING_WITH_LITERAL_STRING("STRING", "Oh hi, will you drive me back to my place?", 2000, 1)
//					ENDIF
//					bAskedForALift = TRUE
//				ENDIF
//				SETTIMERA(0)
//				iApproachStage ++
//			ENDIF
//		BREAK
//		
//		CASE 4
//			IF TIMERA() > 2000
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					veh_current_player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//					IF IS_VEHICLE_DRIVEABLE(veh_current_player_vehicle)
//						IF GET_ENTITY_SPEED(veh_current_player_vehicle) < 2.5
//							IF IS_ENTITY_AT_COORD(veh_current_player_vehicle, GET_ENTITY_COORDS(pedHitcher), <<5, 5, 5>>)
//								// ------
//								PRINT_STRING_WITH_LITERAL_STRING("STRING", "Jump in.", 3000, 1)
//								// -----
//								IF NOT IS_PED_INJURED(pedHitcher)
//									CLEAR_PED_TASKS(pedHitcher)
//									TASK_ENTER_VEHICLE(pedHitcher, veh_current_player_vehicle, 60000, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
//									iApproachStage ++
//								ENDIF
//							ELSE
//								iApproachStage = 1
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF NOT IS_PED_INJURED(pedHitcher)
//					IF NOT IS_PED_FACING_PED(pedHitcher, PLAYER_PED_ID(), 40)
//						CLEAR_PED_TASKS(pedHitcher)
//						TASK_TURN_PED_TO_FACE_ENTITY(pedHitcher, PLAYER_PED_ID())
//					ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 5
//			IF NOT IS_PED_INJURED(pedHitcher)
//				IF IS_PED_IN_ANY_VEHICLE(pedHitcher)
//					eventStage = STAGE_2
//				ENDIF
//			ENDIF
//		BREAK
//		
//	ENDSWITCH
//	
//ENDPROC
