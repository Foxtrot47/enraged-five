//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "script_blips.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "AggroSupport.sch"
USING "LineActivation.sch"
USING "finance_control_public.sch"
USING "context_control_public.sch"
USING "commands_debug.sch"
USING "flow_public_core_override.sch"

USING "script_maths.sch"
USING "Ambient_Common.sch"
USING "context_control_public.sch"
USING "commands_cutscene.sch"
USING "commands_event.sch"

USING "random_events_public.sch"

//Steve T added this.
USING "dialogue_public.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "altruist_cult.sch"

USING "select_mission_stage.sch"
USING "locates_public.sch"
USING "commands_recording.sch"

CONST_FLOAT WARP_REFRESH_DISTANCE 900.0 //30
CONST_FLOAT WARP_ALLOWED_DISTANCE 625.0 //25
CONST_FLOAT MIN_DIST_BETWEEN_WARP_POINTS 400.0 // 20
CONST_FLOAT MIN_DIST_PLAYER_TO_WARP 400.0 // 20
CONST_INT MIN_TIME_OCCLUDED 1000
CONST_INT MIN_TIME_BETWEEN_WARPS 2000

// Variables ----------------------------------------------//
ENUM ambStageFlag
    ambCanRun,
    ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM initialSnatchCutStage
	startTheSnatch,
	waitForVisual,
	waitForEnd
ENDENUM
initialSnatchCutStage initialSnatchCut = startTheSnatch

ENUM snatchedStageFlag
    waitingForSnatch,
    kidnapperIsGettingAway,
	fightKidnappers,
	rescueGirl,
    driveHome,
	attackedOnWayBack,
    carReturned
ENDENUM
snatchedStageFlag snatchedStage = waitingForSnatch

ENUM returnCutStageFlag
	stopTheCar,
    setTheScene,
    runTasks,
    cleanupCut
ENDENUM
returnCutStageFlag returnCutStage = stopTheCar

VECTOR vInput
FLOAT fInput
VECTOR vEndLocation = << -1511.1945, 436.5252, 109.7818 >>
VECTOR vGangHouse = << 968.0051, -127.6078, 73.3543 >>
VECTOR vWaypoint
VECTOR vCult = << -1034.6, 4918.6, 205.9 >>

//VECTOR vMidPointForNodes
//VECTOR vMinAreaForNodes
//VECTOR vMaxAreaForNodes
//FLOAT fDistForNodes

//VECTOR vBikerWarpPosition[2]
//FLOAT fBikerWarpHeading[2]
//
//VECTOR vBikerLastTargetPosition[2]
//FLOAT fBikerLastTargetHeading[2]
//
//INT iChaseWarpTimer
//INT iCurrentWarper

INT DISABLE_CAMERA_MOVEMENT_TIMER

INT iTimeOfAttack

VECTOR vCamOffsetPos = <<0.5,0.1,0.56>>

BOOL bExtraAttackersLaunched, bCreateBackupAtGangHouse, bExtraAttackersDone
BOOL bBackupCreated, bBackupDead, bKidnappersDead, bSnatchDialoguePlayed, bVictimScreams
BOOL bLostSpeedUp
BOOL bVanFrozen
BOOL bShowHeightOnBlip
INT bumpedCounter, iTemp, iScreamTimer

BOOL bJacked[2], bUnsuitable[2], bStopped[2], bNoVeh[2], bFinger, bFingerShout, bTrevorLost
FLOAT fStoppedTime, fNoVehTimer
INT iLastUnsuitable

BOOL bAreasBlocked = FALSE

INT sceneID, sceneIDsnatch, sceneIDexit, iTimeInRange
VECTOR scenePosition, sceneRotation
BOOL bPushInToFirstPerson

INT iWaitScene
INT iSnatchScene
INT iTimeOfMoreBikersDialogue

BLIP_INDEX blipScene, blipKidnap, blipVictim, blipDriver, blipPassenger, blipRider[2], blipShooter[2], blipExtraBikes[2], blipBackup[4], blipCult

PED_INDEX pedVictim, pedKidnap, pedDriver, pedPassenger, pedRider[2], pedShooter[2], pedBackup[4]
VEHICLE_INDEX vehKidnap, vehBike[2]
SEQUENCE_INDEX seq
CAMERA_INDEX camStart, camEnd
SCENARIO_BLOCKING_INDEX ganghouseBlocking
VEHICLE_INDEX vehShift

OBJECT_INDEX objVanFixer

//VECTOR vPlayer, vCar, vVictim
//FLOAT fPlayer, fCar, fVictim
VECTOR vStartPos, vStartRot//, vEndPos, vEndRot
VECTOR vEndPos

STRING playerVoice, chassisBone = "chassis"
structPedsForConversation dialogueStruct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

VECTOR vVictimOffset = <<0,-5,0>>
VECTOR vKidnapperOffset = <<1,-1,0>>
FLOAT fVictim, fKidnapper
BOOL widgetsCreated, playFullScene, setOffsets, toggleVanDoors, bSnatchOngoing
BOOL bSetDriveby, bPlayedBackupDialogue, bPlayedCutDialogue

#IF IS_DEBUG_BUILD
//FLOAT fSceneTime
//FLOAT fDoorOpenTime
//BOOL setCurrentSceneTime
CONST_INT Z_SKIP_CAUGHT_VAN				0
CONST_INT Z_SKIP_LOST_HQ				1
CONST_INT Z_SKIP_LOST_CHASE				2
CONST_INT Z_SKIP_ALTRUIST				3
CONST_INT Z_SKIP_ENDCUT					4
CONST_INT Z_SKIP_ENDCUT_FOOT			5
CONST_INT MAX_SKIP_MENU_LENGTH			6
MissionStageMenuTextStruct sSkipMenu[MAX_SKIP_MENU_LENGTH]
INT iSkipToThisStage = -1
BOOL bRestartScene, bResetSceneOffset, bTestCam
#ENDIF	//	IS_DEBUG_BUILD

CONST_INT IdleLoopScene 0
CONST_INT SnatchScene	1
CONST_INT EscapeScene	2

MODEL_NAMES modelVehicle = GBURRITO
MODEL_NAMES modelBike = DAEMON//HEXER
MODEL_NAMES modelKidnap = G_M_Y_LOST_01 
MODEL_NAMES modelDriver = G_M_Y_LOST_02
MODEL_NAMES modelVictim = U_F_Y_BikerChic
REL_GROUP_HASH relGroupBaddies, relGroupVictim

BOOL bStarted = FALSE
BOOL bOnHold = FALSE
TEXT_LABEL_23 sConvResumeLabel = ""
INT iConvTimer = -1

INT iExitSceneStage = 0

STRING animDict = "RANDOM@KIDNAP_GIRL"
STRING sConvBlock = "RESNAAU"

INT iConvSubStage
INT iLastConvFinish

ENUM SNATCHED_CONVERSATIONS_ENUM

	RESNA_LETS_GET_OUT_OF_HERE = -1,
	RESNA_TAKE_ME_HOME = 0,
	RESNA_THANKS_FOR_STEPPING_IN,
	RESNA_WHY_GET_INVOLVED,
	RESNA_HANDLED_YOURSELF,
	RESNA_THATS_IT_IM_DONE,
	RESNA_CELLPHONE,
	RESNA_CONVERSATION_CAP

ENDENUM

#IF IS_DEBUG_BUILD

	FUNC STRING GET_STRING_FROM_SNATCHED_CONVERSATIONS_ENUM(SNATCHED_CONVERSATIONS_ENUM eConvEnum)
	
		SWITCH eConvEnum
		
			CASE RESNA_LETS_GET_OUT_OF_HERE		RETURN "RESNA_LETS_GET_OUT_OF_HERE"
			CASE RESNA_TAKE_ME_HOME				RETURN "RESNA_TAKE_ME_HOME"
			CASE RESNA_THANKS_FOR_STEPPING_IN	RETURN "RESNA_THANKS_FOR_STEPPING_IN"
			CASE RESNA_WHY_GET_INVOLVED			RETURN "RESNA_WHY_GET_INVOLVED"
			CASE RESNA_HANDLED_YOURSELF			RETURN "RESNA_HANDLED_YOURSELF"
			CASE RESNA_THATS_IT_IM_DONE			RETURN "RESNA_THATS_IT_IM_DONE"
			CASE RESNA_CELLPHONE				RETURN "RESNA_CELLPHONE"
			
		ENDSWITCH
		
		RETURN "RESNA_CONVERSATION_CAP"
		
	ENDFUNC
	
#ENDIF

SNATCHED_CONVERSATIONS_ENUM eDriveHomeConv = RESNA_TAKE_ME_HOME
SNATCHED_CONVERSATIONS_ENUM eSavedConv

INTERPCAMS exitVanCam

CAMERA_INDEX  exitCam

// Functions ----------------------------------------------//

FUNC FLOAT MIN(FLOAT a, FLOAT b)
	IF a<b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC FLOAT MAX(FLOAT a, FLOAT b)
	IF a>b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC BOOL DO_LINES_INTERSECT(VECTOR p1, VECTOR p2, VECTOR p3, VECTOR p4, VECTOR &vIntersectPoint)
	// Store the values for fast access and easy
	// equations-to-code conversion
	FLOAT x1 = p1.x
	FLOAT x2 = p2.x
	FLOAT x3 = p3.x
	FLOAT x4 = p4.x
	FLOAT y1 = p1.y
	FLOAT y2 = p2.y
	FLOAT y3 = p3.y
	FLOAT y4 = p4.y
	
	float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
	// If d is zero, there is no intersection
	IF d = 0 
		RETURN FALSE 
	ENDIF
	
	// Get the x and y
	FLOAT fPre = (x1*y2 - y1*x2)
	FLOAT fPost = (x3*y4 - y3*x4)
	float x = ( fPre * (x3 - x4) - (x1 - x2) * fPost ) / d
	float y = ( fPre * (y3 - y4) - (y1 - y2) * fPost ) / d
	 
	// Check if the x and y coordinates are within both lines
	IF x < min(x1, x2) 
	OR x > max(x1, x2) 
	OR x < min(x3, x4)
	OR x > max(x3, x4) 
		RETURN FALSE
	ENDIF
	
	IF y < min(y1, y2) 
	OR y > max(y1, y2)
	OR y < min(y3, y4) 
	OR y > max(y3, y4)
		RETURN FALSE
	ENDIF
	
	// Return the point of intersection
	vIntersectPoint.x = x
	vIntersectPoint.y = y
	vIntersectPoint.z = p1.z
	return TRUE
	
ENDFUNC

FUNC BOOL DOES_PLAYER_CAR_NEED_SHIFTING_FOR_RESCUE(VEHICLE_INDEX veh)

	BOOL bIntersectionFound = FALSE
	
	IF DOES_ENTITY_EXIST(veh)
	AND DOES_ENTITY_EXIST(vehKidnap)
		
		VEHICLE_INDEX vehTemp = veh
		
		VECTOR vPlayerCarDimensionsMin
		VECTOR vPlayerCarDimensionsMax
		VECTOR vPlayerCarDimensions
		VECTOR vPlayerCarPoints[4]
		VECTOR vBadArea[4]		
		VECTOR vIntersectPoint[4]
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTemp), vPlayerCarDimensionsMin, vPlayerCarDimensionsMax)
		vPlayerCarDimensions = vPlayerCarDimensionsMax - vPlayerCarDimensionsMin
		
		vPlayerCarPoints[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<	-vPlayerCarDimensions.x/2, 	 vPlayerCarDimensions.y/2, 	0>>)
		vPlayerCarPoints[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<	 vPlayerCarDimensions.x/2, 	 vPlayerCarDimensions.y/2, 	0>>)
		vPlayerCarPoints[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<	 vPlayerCarDimensions.x/2, 	-vPlayerCarDimensions.y/2, 	0>>)
		vPlayerCarPoints[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<	-vPlayerCarDimensions.x/2, 	-vPlayerCarDimensions.y/2, 	0>>)
		
		vBadArea[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, << -1, 	-2.5, 	0>>)
		vBadArea[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<	1, 	-2.5, 	0>>)
		vBadArea[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<	0.75, 	-5, 	0>>)
		vBadArea[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, << -0.75, 	-5, 	0>>)
		
		INT i,j
		
		REPEAT COUNT_OF(vBadArea) i
			
			INT iNextPoint1 = i+1
			IF iNextPoint1 = COUNT_OF(vBadArea)
				iNextPoint1 = 0
			ENDIF
			
			REPEAT COUNT_OF(vPlayerCarPoints) j
				INT iNextPoint2 = j+1
				IF iNextPoint2 = COUNT_OF(vPlayerCarPoints)
					iNextPoint2 = 0
				ENDIF
				IF DO_LINES_INTERSECT(vBadArea[i], vBadArea[iNextPoint1], vPlayerCarPoints[j], vPlayerCarPoints[iNextPoint2], vIntersectPoint[i])
					bIntersectionFound = TRUE
				ENDIF
			ENDREPEAT
			
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(vBadArea) i
				INT iNextPoint1 = i+1
				IF iNextPoint1 = COUNT_OF(vBadArea)
					iNextPoint1 = 0
				ENDIF
				IF bIntersectionFound
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 255, 0, 0)
					DRAW_DEBUG_LINE(vBadArea[i], vBadArea[iNextPoint1], 255, 0, 0)
					IF NOT IS_VECTOR_ZERO(vIntersectPoint[i])
						DRAW_DEBUG_SPHERE(vIntersectPoint[i], 0.2, 255, 0, 0)
					ENDIF
				ELSE
					DRAW_DEBUG_LINE(vBadArea[i], vBadArea[iNextPoint1], 0, 255, 0)
					DRAW_DEBUG_LINE(vPlayerCarPoints[i], vPlayerCarPoints[iNextPoint1], 0, 255, 0)
				ENDIF
			ENDREPEAT
		#ENDIF
		
	ELSE
		PRINTLN("PLAYER'S LAST VEHICLE DOESN'T EXIST")
	ENDIF

	RETURN bIntersectionFound

ENDFUNC

PROC ADD_REL_GROUPS()
	ADD_RELATIONSHIP_GROUP("re_snatched badGuys", relGroupBaddies)
	ADD_RELATIONSHIP_GROUP("re_snatched pedVictim", relGroupVictim)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBaddies, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupBaddies, relGroupVictim)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, relGroupVictim, RELGROUPHASH_PLAYER)
ENDPROC

PROC SET_LOST_VAN_GUY_ATTRIBUTES()

	SET_PED_CAN_BE_TARGETTED(pedKidnap, FALSE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehKidnap, TRUE)
	SET_VEHICLE_DOORS_LOCKED(vehKidnap, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(pedDriver, relGroupBaddies)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedPassenger, relGroupBaddies)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedKidnap, relGroupBaddies)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPassenger, TRUE)
    SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedKidnap, TRUE)
	
	SET_PED_CONFIG_FLAG(pedDriver, PCF_DontInfluenceWantedLevel, TRUE)
	SET_PED_CONFIG_FLAG(pedKidnap, PCF_DontInfluenceWantedLevel, TRUE)
	SET_PED_CONFIG_FLAG(pedPassenger, PCF_DontInfluenceWantedLevel, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(pedPassenger, CA_ALWAYS_FLEE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedPassenger, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedPassenger, CA_AGGRESSIVE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedPassenger, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_DO_DRIVEBYS, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_ALWAYS_FLEE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_AGGRESSIVE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_DO_DRIVEBYS, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_ALWAYS_FLEE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_AGGRESSIVE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedKidnap, CA_DO_DRIVEBYS, TRUE)
	
	SET_PED_COMBAT_MOVEMENT(pedPassenger, CM_WILLADVANCE)
	SET_PED_COMBAT_MOVEMENT(pedDriver, CM_WILLADVANCE)
	SET_PED_COMBAT_MOVEMENT(pedKidnap, CM_WILLADVANCE)
	
	SET_PED_SUFFERS_CRITICAL_HITS(pedPassenger, FALSE)
	SET_PED_SUFFERS_CRITICAL_HITS(pedDriver, FALSE)
	SET_PED_SUFFERS_CRITICAL_HITS(pedKidnap, FALSE)
	
	GIVE_WEAPON_TO_PED(pedPassenger, WEAPONTYPE_PISTOL, 68)
	GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_SAWNOFFSHOTGUN, 68)
	GIVE_WEAPON_TO_PED(pedKidnap, WEAPONTYPE_MICROSMG, 68)
	
	SET_PED_DIES_IN_VEHICLE(pedPassenger, FALSE)
	SET_PED_DIES_IN_VEHICLE(pedDriver, FALSE)
	SET_PED_DIES_IN_VEHICLE(pedKidnap, FALSE)
	
	SET_PED_CONFIG_FLAG(pedDriver, PCF_FallsOutOfVehicleWhenKilled, TRUE)
	SET_PED_CONFIG_FLAG(pedKidnap, PCF_FallsOutOfVehicleWhenKilled, TRUE)
	SET_PED_CONFIG_FLAG(pedPassenger, PCF_FallsOutOfVehicleWhenKilled, TRUE)
	
	ADD_PED_FOR_DIALOGUE(dialogueStruct, 2, pedDriver, "LostKidnapper")
	
ENDPROC

PROC CREATE_KIDNAP_GIRL()

	INT victimHealth = 500
	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
		pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, vVictimOffset), fInput)//wrap((fInput-180), 0,360)))
	ELSE
		pedVictim = CREATE_PED(PEDTYPE_CIVFEMALE, modelVictim, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vVictimOffset), fInput)//wrap((fInput-180), 0,360)))
	ENDIF
	
	SET_AMBIENT_VOICE_NAME(pedVictim, "LostKidnapGirl")
	SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
	SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, FALSE)
	SET_ENTITY_HEALTH(pedVictim, victimHealth)
	SET_PED_MAX_HEALTH(pedVictim, victimHealth)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedVictim, relGroupVictim)
	
	ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedVictim, "LostKidnapGirl")
//	blipScene = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim, FALSE, TRUE)
ENDPROC

PROC CREATE_LOST_FOR_SNATCH()
	VECTOR vTemp
	vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	IF vTemp.x > vInput.x
		vehKidnap = CREATE_VEHICLE(modelVehicle, <<-1116.9456, 560.8027, 101.3588>>, 116.0592)
		vWaypoint = <<-1247.2330, 515.3996, 94.1352>>
	ELSE
		vehKidnap = CREATE_VEHICLE(modelVehicle, vInput, 305)
		vWaypoint = <<-852.9832, 441.7806, 86.0437>>
	ENDIF
	ADD_VEHICLE_UPSIDEDOWN_CHECK(vehKidnap)
	SET_VEHICLE_ENGINE_ON(vehKidnap, TRUE, TRUE)
	pedDriver = CREATE_PED_INSIDE_VEHICLE(vehKidnap, PEDTYPE_CRIMINAL, modelDriver)
	SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_DO_DRIVEBYS, TRUE)
	GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_SAWNOFFSHOTGUN, 68, TRUE)
	SET_CURRENT_PED_WEAPON(pedDriver, WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
	pedPassenger = CREATE_PED(PEDTYPE_CRIMINAL, modelDriver, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, vKidnapperOffset), fInput)//CREATE_PED_INSIDE_VEHICLE(vehKidnap, PEDTYPE_CRIMINAL, modelDriver, VS_FRONT_RIGHT)
    pedKidnap = CREATE_PED(PEDTYPE_CRIMINAL, modelKidnap, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, vKidnapperOffset), fInput)//wrap((fInput-180), 0,360))
	FREEZE_ENTITY_POSITION(vehKidnap, TRUE)
	bVanFrozen = TRUE
	SET_PED_SHOOT_RATE(pedKidnap, 25)
	SET_PED_FIRING_PATTERN(pedKidnap, FIRING_PATTERN_BURST_FIRE_DRIVEBY)
	SET_PED_SHOOT_RATE(pedPassenger, 35)
	SET_PED_FIRING_PATTERN(pedKidnap, FIRING_PATTERN_SINGLE_SHOT)
	SET_PED_ACCURACY(pedDriver, 12)
	SET_PED_ACCURACY(pedPassenger, 10)
	SET_PED_ACCURACY(pedKidnap, 15)
	SET_VEHICLE_TYRES_CAN_BURST(vehKidnap, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelDriver)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnap)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehKidnap, FALSE)
	SET_VEHICLE_LOD_MULTIPLIER(vehKidnap, 2.0)
ENDPROC

PROC SETUP_SNATCH_SYNC_SCENE()

	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
	AND NOT IS_PED_INJURED(pedVictim)
	AND NOT IS_PED_INJURED(pedKidnap)
	AND NOT IS_PED_INJURED(pedPassenger)
	
		scenePosition = <<0,0,0>>
		sceneRotation = <<0,0,0>>
		sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
		
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
		SET_SYNCHRONIZED_SCENE_LOOPED (sceneID, TRUE)
		SET_SYNCHRONIZED_SCENE_ORIGIN(sceneID, scenePosition, sceneRotation)
		
		TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, animDict, "IG_1_GIRL_ON_PHONE_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
		TASK_SYNCHRONIZED_SCENE(pedKidnap, sceneId, animDict, "IG_1_GUY1_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
		TASK_SYNCHRONIZED_SCENE(pedPassenger, sceneId, animDict, "IG_1_GUY2_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
		
	ENDIF
	
ENDPROC

SCENARIO_BLOCKING_INDEX sbiEnd, sbiStart

PROC SET_BLOCKING_AREAS(BOOL bBlocked)

	CLEAR_AREA(vEndPos, 15, TRUE)
	CLEAR_AREA(<<-1133.992676,558.326660,107.351036>>, 5, TRUE)
	
	IF bBlocked
		sbiStart = ADD_SCENARIO_BLOCKING_AREA(<<-1126.376221,560.491333,104.050316>>-<<39.750000,19.250000,5.750000>>, <<-1126.376221,560.491333,104.050316>>+<<39.750000,19.250000,5.750000>>)
		sbiEnd = ADD_SCENARIO_BLOCKING_AREA(<<-3055.087891,442.695221,9.361914>> + <<10.000000,10.000000,5.000000>>, <<-3055.087891,442.695221,9.361914>> - <<10.000000, 10.000000, 5>>)
	ELSE
		REMOVE_SCENARIO_BLOCKING_AREA(sbiStart)
		REMOVE_SCENARIO_BLOCKING_AREA(sbiEnd)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1133.992676,558.326660,107.351036>> + <<25.000000,25.250000,7.250000>>, <<-1133.992676,558.326660,107.351036>> - <<25.000000,25.250000, 7.25>>, NOT bBlocked)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-3055.087891,442.695221,9.361914>> + <<10.000000,10.000000,5.000000>>, <<-3055.087891,442.695221,9.361914>> - <<10.000000, 10.000000, 5>>, NOT bBlocked)
	
ENDPROC

FUNC BOOL LOADED_AND_CREATED_KIDNAP_SCENE()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
	
	fInput = 321
//	VECTOR vTemp
	
	SET_PED_MODEL_IS_SUPPRESSED(modelDriver, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(modelVictim, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(modelKidnap, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle, TRUE)
	
    REQUEST_MODEL(modelVictim)
    REQUEST_MODEL(modelKidnap)
	REQUEST_MODEL(modelDriver)
    REQUEST_MODEL(modelVehicle)
	REQUEST_ANIM_DICT(animDict)
	REQUEST_MODEL(PROP_LD_TEST_01)
    IF HAS_MODEL_LOADED (modelVictim)
    AND HAS_MODEL_LOADED(modelKidnap)
	AND HAS_MODEL_LOADED(modelDriver)
    AND HAS_MODEL_LOADED(modelVehicle)
    AND HAS_MODEL_LOADED(PROP_LD_TEST_01)
	AND HAS_ANIM_DICT_LOADED(animDict)
//		vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP",  <<0,0,0>>, <<0,0,0>>)
//		PRINTLN(GET_THIS_SCRIPT_NAME(), " - GET_ANIM_INITIAL_OFFSET_POSITION = ", vTemp)
//		vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP",  <<0,0,0>>, <<0,0,0>>)
//		PRINTLN(GET_THIS_SCRIPT_NAME(), " - GET_ANIM_INITIAL_OFFSET_ROTATION = ", vTemp)
		ADD_REL_GROUPS()
		CREATE_LOST_FOR_SNATCH()
		CREATE_KIDNAP_GIRL()
		SET_LOST_VAN_GUY_ATTRIBUTES()
		ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_SEEN_CONFRONTATION, vInput)
        //blipScene = CREATE_AMBIENT_BLIP_INITIAL_COORD(vInput) 
		
		ganghouseBlocking = ADD_SCENARIO_BLOCKING_AREA((vGangHouse-<<50,50,50>>), (vGangHouse+<<50,50,50>>))
		
        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
            CASE CHAR_MICHAEL  //0
                playerVoice = "MICHAEL"
            BREAK
            
            CASE CHAR_FRANKLIN //1
                playerVoice = "FRANKLIN"
            BREAK
            
            CASE CHAR_TREVOR   //2
                playerVoice = "TREVOR"
            BREAK
        ENDSWITCH
        
        ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), playerVoice)
		
		vStartPos   = <<-3057.361572,441.827148,6.151212>>
	    vStartRot   = <<-0.349920,-0.199749,-98.798607>>
		fVictim		= 116
		vEndLocation = << -3053.7212, 440.9221, 5.3566 >>
		
		objVanFixer = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(vehKidnap, FALSE))
		SET_ENTITY_ROTATION(objVanFixer, GET_ENTITY_ROTATION(vehKidnap))
		FREEZE_ENTITY_POSITION(objVanFixer, TRUE)
		SET_ENTITY_COLLISION(objVanFixer, FALSE)
		
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC enumSubtitlesState SET_SUB_DISPLAY_BASED_ON_DISTANCE()
	enumSubtitlesState subtitleState
	VECTOR vTemp = <<75, 75, 20>>
	IF NOT IS_PED_INJURED(pedVictim)
		IF IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), vTemp)
			subtitleState = DISPLAY_SUBTITLES
		ELSE
			subtitleState = DO_NOT_DISPLAY_SUBTITLES 
		ENDIF
	ENDIF
	RETURN subtitleState
ENDFUNC

FUNC BOOL IS_POINT_BEHIND_PLAYER(VECTOR vPoint)
	VECTOR vTowardsPoint = vPoint - GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vPlayerForwards = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vPlayerForwards.x, vPlayerForwards.y, vTowardsPoint.x, vTowardsPoint.y)) >= 90.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_CHASER_BLIPS(INT iIndex, BOOL bBlipNeeded)
	IF bBlipNeeded
		IF NOT DOES_BLIP_EXIST(blipExtraBikes[iIndex])
			blipExtraBikes[iIndex] = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehBike[iIndex], FALSE))
			SET_BLIP_COLOUR(blipExtraBikes[iIndex], BLIP_COLOUR_RED)
			SET_BLIP_PRIORITY(blipExtraBikes[iIndex], BLIPPRIORITY_HIGHEST)
		ELSE
			VECTOR vBlipPosition    = GET_BLIP_COORDS(blipExtraBikes[iIndex])
			VECTOR vVehiclePosition = GET_ENTITY_COORDS(vehBike[iIndex], FALSE)
			vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 25.0)
			vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 25.0)
			vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 25.0)
			SET_BLIP_COORDS(blipExtraBikes[iIndex], vBlipPosition)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipExtraBikes[iIndex])
			REMOVE_BLIP(blipExtraBikes[iIndex])
		ENDIF
	ENDIF
ENDPROC

OBJECT_INDEX objFitTest

FUNC BOOL DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(VEHICLE_INDEX veh, VEHICLE_INDEX vehTarget, OBJECT_INDEX objTest, VECTOR vPosition, VECTOR vRotation)

	BOOL bRet
	
	IF DOES_ENTITY_EXIST(objTest)
		VECTOR vFront, vBack
		VECTOR vDimMin, vDimMax
		VECTOR vPlayerDimMin, vPlayerDimMax
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vDimMin, vDimMax)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTarget), vPlayerDimMin, vPlayerDimMax)
		
		FLOAT fHeight = ABSF(vDimMax.z - vDimMin.z)
		FLOAT fWidth = ABSF(vDimMax.x - vDimMin.x)
		FLOAT fLength = ABSF(vDimMax.y - vDimMin.y)
		
		FLOAT fRad 
		FLOAT fBump
		IF fWidth > fHeight
			fRad = fWidth/2
			fBump = fRad - absf(vDimMin.z)
		ELSE
			fRad = fLength/2
			fBump = fRad - absf(vDimMin.x)
		ENDIF
		
		VECTOR vCoord
		vCoord.z += vPlayerDimMin.z - vDimMin.z
		
		SET_ENTITY_COORDS(objTest, vPosition)
		SET_ENTITY_ROTATION(objTest, vRotation)
		
		vFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,fLength/2 - fRad*0.75,fBump*1.25>> + vCoord)
		vBack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,-fLength/2 + fRad*0.75,fBump*1.25>> + vCoord)
		
		INT		iHitResults
		VECTOR	vReturnPosition
		VECTOR	vReturnNormal
		ENTITY_INDEX hitEntity
			
		SHAPETEST_INDEX ShapeTestCapsuleIndex = START_SHAPE_TEST_CAPSULE(vBack, vFront, fRad, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE, PLAYER_PED_ID())
		
		IF GET_SHAPE_TEST_RESULT(ShapeTestCapsuleIndex, iHitResults, vReturnPosition, vReturnNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test results not ready.")
			#ENDIF
		ENDIF
		
		IF ( iHitResults != 0 )
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test capsule hitting something.")
			#ENDIF
			bRet = FALSE
		ELSE
			bRet =  TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			INT iGreen
			INT iRed
			IF bRet 
				iGreen = 255
			ELSE
				iRed = 255
			ENDIF
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMin), <<0.8, 0.3, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMax), <<0.8, 0.32, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fHeight), <<0.8, 0.34, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fWidth), <<0.8, 0.36, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fLength), <<0.8, 0.38, 0>>)
			DRAW_DEBUG_SPHERE(vFront, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vFront, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_SPHERE(vBack, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vBack, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack-<<0, 0, fRad>>, vFront-<<0, 0, fRad>>, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack+<<0, 0, fRad>>, vFront+<<0, 0, fRad>>, iRed, iGreen, 0, 100)
		#ENDIF
		
	ENDIF
	
	RETURN bRet
	
ENDFUNC

STRUCT WARP_TARGET
	VECTOR vPosition
	VECTOR vRotation
	FLOAT fSpeed
ENDSTRUCT

WARP_TARGET chaserWarpTarget[2]

INT iChaserLastWarp[2]
INT iChaserLastSeen[2]

PROC MANAGE_CHASER_WARPING()

	IF NOT DOES_ENTITY_EXIST(objFitTest)
		REQUEST_MODEL(PROP_LD_TEST_01)
		IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
			objFitTest = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) + <<0,0,2>> )
			SET_ENTITY_COLLISION(objFitTest, FALSE)
			SET_ENTITY_VISIBLE(objFitTest, FALSE)
			FREEZE_ENTITY_POSITION(objFitTest, TRUE)
		ENDIF
	ENDIF
	
	DRAW_DEBUG_TEXT_2D("Checking for warp", <<0.89, 0.02, 0>>)

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	
		VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		INT i
		FOR i = 0 TO 1
		
			IF IS_VEHICLE_DRIVEABLE(vehBike[i])
			AND NOT IS_PED_INJURED(pedRider[i])
			AND IS_PED_SITTING_IN_VEHICLE(pedRider[i], vehBike[i])
			
				VECTOR vWarperCoords = GET_ENTITY_COORDS(vehBike[i], FALSE)
				VECTOR vTargetCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				
				//first check to see if a warp point can be set
				
				//don't set a warp point if the player is too close to either warp points
				IF VDIST2(vTargetCoords, chaserWarpTarget[0].vPosition) > MIN_DIST_BETWEEN_WARP_POINTS
				AND VDIST2(vTargetCoords, chaserWarpTarget[1].vPosition) > MIN_DIST_BETWEEN_WARP_POINTS
					//Set a warp point if the player is far enough from the last version of that warp point
					IF VDIST2(vTargetCoords, chaserWarpTarget[i].vPosition) > WARP_REFRESH_DISTANCE
						//if the chasing vehicle will fit at this warp point then set the warp point.
						IF DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehBike[i], vehTemp, objFitTest, vTargetCoords, GET_ENTITY_ROTATION(vehTemp))
							chaserWarpTarget[i].vPosition = vTargetCoords
							chaserWarpTarget[i].vRotation = GET_ENTITY_ROTATION(vehTemp)
							chaserWarpTarget[i].fSpeed = VMAG(GET_ENTITY_VELOCITY(vehBike[i]))
						ENDIF
					ENDIF
				ENDIF
				
				//Then check if a warp point can be warped to.
				
				IF NOT IS_VECTOR_ZERO(chaserWarpTarget[i].vPosition)
					IF NOT IS_ENTITY_OCCLUDED(vehBike[i])
						iChaserLastSeen[i] = GET_GAME_TIMER()
					ELSE
						IF (GET_GAME_TIMER() - iChaserLastSeen[i]) > MIN_TIME_OCCLUDED
						AND (GET_GAME_TIMER() - iChaserLastWarp[0]) > MIN_TIME_BETWEEN_WARPS
						AND (GET_GAME_TIMER() - iChaserLastWarp[1]) > MIN_TIME_BETWEEN_WARPS
							IF NOT IS_SPHERE_VISIBLE(chaserWarpTarget[i].vPosition, 3.0)
							AND NOT IS_SPHERE_VISIBLE(vWarperCoords, 3.0)
								FLOAT fDistToOtherChaser = 99.0
								INT iOtherGuy = 0
								IF i = 0 iOtherGuy = 1 ENDIF
								IF IS_VEHICLE_DRIVEABLE(vehBike[iOtherGuy])
									fDistToOtherChaser = VDIST(GET_ENTITY_COORDS(vehBike[iOtherGuy], FALSE), chaserWarpTarget[i].vPosition)
								ENDIF
								
								//Check that the warping vehicle is far enough away from the warp point
								IF VDIST2(vWarperCoords, vTargetCoords) > WARP_ALLOWED_DISTANCE
								//check that warping will place the warping vehicle closer to it's target
								AND VDIST2(vWarperCoords, chaserWarpTarget[i].vPosition) < VDIST2(vWarperCoords, vTargetCoords)
								//check the target vehicle is far enough away from the warp location to allow the warping vehicle to go there
								AND VDIST2(vTargetCoords, chaserWarpTarget[i].vPosition) > MIN_DIST_PLAYER_TO_WARP						
								AND fDistToOtherChaser > 5.0
								AND DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehBike[i], vehTemp, objFitTest, chaserWarpTarget[i].vPosition, chaserWarpTarget[i].vRotation)
								
									CLEAR_AREA_OF_PEDS(chaserWarpTarget[i].vPosition, 3.0)
							        CLEAR_AREA_OF_VEHICLES(chaserWarpTarget[i].vPosition, 3.0)
									SET_ENTITY_COORDS(vehBike[i], chaserWarpTarget[i].vPosition)
									SET_ENTITY_ROTATION(vehBike[i], chaserWarpTarget[i].vRotation)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[i])
						            SET_VEHICLE_FORWARD_SPEED(vehBike[i], chaserWarpTarget[i].fSpeed + 5.0)
									TASK_VEHICLE_CHASE(pedRider[i], PLAYER_PED_ID())
						            SET_VEHICLE_ENGINE_ON(vehBike[i], TRUE, TRUE)
						            iChaserLastWarp[i] = GET_GAME_TIMER()
									PRINTLN("Warped ped ", i)
									
									DISABLE_CAMERA_MOVEMENT_TIMER = GET_GAME_TIMER() + 1000
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_SPHERE(chaserWarpTarget[i].vPosition, 3, 0, 255, 0, 50)
					DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(chaserWarpTarget[i].fSpeed), chaserWarpTarget[i].vPosition + <<0,0,1>>, 0, 255, 0)
					DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(GET_GAME_TIMER() - iChaserLastWarp[i]), chaserWarpTarget[i].vPosition + <<0,0,1.5>>, 0, 255, 0)
				#ENDIF
				
			ENDIF
		
		ENDFOR
		
	ENDIF
	
	IF DISABLE_CAMERA_MOVEMENT_TIMER > GET_GAME_TIMER()
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2193182
	ENDIF
	
ENDPROC
//
//PROC RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TargetVehicleIndex, VECTOR &vWarpPosition,
//												   FLOAT &fWarpHeading, VECTOR &vLastTargetPosition, FLOAT &fLastTargetHeading,
//												   INT &iWarpTimer, VECTOR vWarpOffset, INT iWarpTimeDelay, FLOAT fWarpDistance)
//												   
//	IF NOT DOES_ENTITY_EXIST(objFitTest)
//	
//		IF NOT IS_VECTOR_ZERO(vWarpPosition)		
//			REQUEST_MODEL(PROP_LD_TEST_01)
//			IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
//				objFitTest = CREATE_OBJECT(PROP_LD_TEST_01, vWarpPosition)
//				SET_ENTITY_COLLISION(objFitTest, FALSE)
//				SET_ENTITY_VISIBLE(objFitTest, FALSE)
//				FREEZE_ENTITY_POSITION(objFitTest, TRUE)
//			ENDIF
//		ENDIF
//		
//	ELSE
//	
//		IF  DOES_ENTITY_EXIST(VehicleIndex)
//		AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
//			IF 	DOES_ENTITY_EXIST(TargetVehicleIndex)
//			AND ( VehicleIndex <> TargetVehicleIndex)
//			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(TargetVehicleIndex))
//			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(TargetVehicleIndex))
//			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TargetVehicleIndex))
//			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(TargetVehicleIndex))
//			
//				IF VDIST(GET_ENTITY_COORDS(TargetVehicleIndex), vLastTargetPosition) > 20.0
//					IF DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(VehicleIndex, TargetVehicleIndex, objFitTest, GET_ENTITY_COORDS(TargetVehicleIndex), GET_ENTITY_ROTATION(TargetVehicleIndex))
//						vWarpPosition 	= vLastTargetPosition	//store last target vehicle positon as the new warp position
//						fWarpHeading 	= fLastTargetHeading	//store last target vehicle heading as the new warp heading
//						vWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarpPosition, fWarpHeading, vWarpOffset)
//						vLastTargetPosition = GET_ENTITY_COORDS(TargetVehicleIndex)
//						fLastTargetHeading = GET_ENTITY_HEADING(TargetVehicleIndex)
//					ENDIF
//				ENDIF
//				
//				IF NOT IS_ENTITY_OCCLUDED(VehicleIndex)
//				    iWarpTimer = GET_GAME_TIMER()
//				ELIF GET_GAME_TIMER() - iWarpTimer > iWarpTimeDelay
//					IF	VDIST(GET_ENTITY_COORDS(VehicleIndex), GET_ENTITY_COORDS(TargetVehicleIndex)) > fWarpDistance
//					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vWarpPosition) > 20.0
//						IF NOT IS_VECTOR_ZERO(vWarpPosition)
//							IF NOT IS_SPHERE_VISIBLE(vWarpPosition, 6.0)
//								CLEAR_AREA_OF_PEDS(vWarpPosition, 5.0)
//						        CLEAR_AREA_OF_VEHICLES(vWarpPosition, 5.0)
//								SET_ENTITY_COORDS(VehicleIndex, vWarpPosition, TRUE, TRUE)
//								SET_ENTITY_HEADING(VehicleIndex, fWarpHeading)
//					            SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
//					            SET_VEHICLE_FORWARD_SPEED(VehicleIndex, CLAMP(GET_ENTITY_SPEED(TargetVehicleIndex)+10, 10.0, 70.0))
//					            SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
//					            iWarpTimer = GET_GAME_TIMER()
//							ENDIF
//					    ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				iWarpTimer = GET_GAME_TIMER()
//			ENDIF
//		ENDIF
//		
//	ENDIF
//	
//ENDPROC

FUNC BOOL LOADED_AND_CREATED_ATTACKING_BIKERS()
	DRIVINGMODE bikerDrivingMode = DF_SteerAroundPeds|DF_SteerAroundObjects|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_SwerveAroundAllCars
	VEHICLE_MISSION vehicleMission = MISSION_ATTACK
	VECTOR vGen, vTemp, fGen
	INT i
	IF IS_VEHICLE_DRIVEABLE(vehBike[0])
	AND IS_VEHICLE_DRIVEABLE(vehBike[1])
		IF NOT IS_PED_INJURED(pedRider[0])
			IF GET_SCRIPT_TASK_STATUS(pedRider[0], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedRider[0]) 
					TASK_DRIVE_BY(pedRider[0], PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 100, TRUE)
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedRider[1])
			IF GET_SCRIPT_TASK_STATUS(pedRider[1], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedRider[1]) 
					TASK_DRIVE_BY(pedRider[1], PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 100, TRUE)
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		REQUEST_MODEL(modelBike)
		REQUEST_MODEL(modelDriver)
		REQUEST_MODEL(modelKidnap)
		IF HAS_MODEL_LOADED(modelBike)
		AND HAS_MODEL_LOADED(modelDriver)
		AND HAS_MODEL_LOADED(modelKidnap)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(vTemp, vGen, fGen, DEFAULT, DEFAULT, 30)
					IF IS_POINT_BEHIND_PLAYER(vGen)
					AND NOT IS_SPHERE_VISIBLE(vGen, 5)
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vGen) > 10*10
						REPEAT 2 i
							CLEAR_AREA(vGen, 3, FALSE)
							vGen.x += TO_FLOAT(i)
							vehBike[i] = CREATE_VEHICLE(modelBike, vGen, fGen.z)	
							pedRider[i] = CREATE_PED_INSIDE_VEHICLE(vehBike[i], PEDTYPE_CRIMINAL, modelDriver)
							pedShooter[i] = CREATE_PED_INSIDE_VEHICLE(vehBike[i], PEDTYPE_CRIMINAL, modelKidnap, VS_FRONT_RIGHT)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedShooter[i], relGroupBaddies)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedRider[i], relGroupBaddies)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									TASK_VEHICLE_CHASE(pedRider[i], PLAYER_PED_ID())
								ENDIF
							ELSE
								TASK_VEHICLE_MISSION_PED_TARGET(pedRider[i], vehBike[i], PLAYER_PED_ID(), vehicleMission, 50, bikerDrivingMode, 5, 10)
							ENDIF
							SET_PED_SUFFERS_CRITICAL_HITS(pedRider[i], FALSE)
							SET_PED_SUFFERS_CRITICAL_HITS(pedShooter[i], FALSE)
							SET_PED_KEEP_TASK(pedRider[i], TRUE)
							GIVE_WEAPON_TO_PED(pedRider[i], WEAPONTYPE_SAWNOFFSHOTGUN, 999999, TRUE)
							SET_VEHICLE_FORWARD_SPEED(vehBike[i], GET_ENTITY_SPEED(PLAYER_PED_ID())+5)
							vehicleMission = MISSION_ESCORT_RIGHT
							GIVE_WEAPON_TO_PED(pedShooter[i], WEAPONTYPE_SAWNOFFSHOTGUN, 999999, TRUE)
							TASK_DRIVE_BY(pedShooter[i], PLAYER_PED_ID(), NULL, <<0,0,0>>, 200, 70)
							SET_PED_KEEP_TASK(pedShooter[i], TRUE)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedRider[i], KNOCKOFFVEHICLE_NEVER)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedShooter[i], KNOCKOFFVEHICLE_NEVER)
							SET_PED_CONFIG_FLAG(pedRider[i], PCF_DontInfluenceWantedLevel, TRUE)
							SET_PED_CONFIG_FLAG(pedShooter[i], PCF_DontInfluenceWantedLevel, TRUE)
							SET_PED_ACCURACY(pedRider[i], 15)
							SET_PED_ACCURACY(pedShooter[i], 15)
						ENDREPEAT
						KILL_ANY_CONVERSATION()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DELETE_SURVIVING_LOST()
	IF NOT IS_PED_INJURED(pedRider[0])
		DELETE_PED(pedRider[0])
	ENDIF
	IF NOT IS_PED_INJURED(pedRider[1])
		DELETE_PED(pedRider[1])
	ENDIF
	IF NOT IS_PED_INJURED(pedShooter[0])
		DELETE_PED(pedShooter[0])
	ENDIF
	IF NOT IS_PED_INJURED(pedShooter[1])
		DELETE_PED(pedShooter[1])
	ENDIF
ENDPROC

PROC CREATE_BACKUP_SHOOTERS()
	INT i
	VECTOR vBackup[4]
	vBackup[0] = << 986.8661, -113.7463, 72.9153 >>
	vBackup[1] = << 964.0147, -144.0337, 73.4875 >>
	vBackup[2] = << 954.2817, -137.4290, 73.4781 >>
	vBackup[3] = << 976.5977, -149.0529, 73.2404 >>
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vGangHouse, <<10,10,10>>)
		REPEAT 4 i
			pedBackup[i] = CREATE_PED(PEDTYPE_CRIMINAL, modelKidnap, vBackup[i])
			GIVE_WEAPON_TO_PED(pedBackup[i], WEAPONTYPE_SAWNOFFSHOTGUN, 999999, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedBackup[i], relGroupBaddies)
//			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedBackup[i], 50)
			TASK_COMBAT_PED(pedBackup[i], PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(pedBackup[i], PCF_DontInfluenceWantedLevel, TRUE)
			blipBackup[i] = CREATE_AMBIENT_BLIP_FOR_PED(pedBackup[i], TRUE)
		ENDREPEAT
		bBackupCreated = TRUE
	ENDIF
ENDPROC

PROC CHECK_AND_REMOVE_BACKUP_BLIPS()
	INT i, iDeadCounter
	REPEAT 4 i
		IF IS_PED_INJURED(pedBackup[i])
			IF DOES_BLIP_EXIST(blipBackup[i])
				REMOVE_BLIP(blipBackup[i])
			ENDIF
			iDeadCounter++
		ENDIF
	ENDREPEAT
	IF iDeadCounter = 4
		bBackupDead = TRUE
	ENDIF
ENDPROC

PROC CLEANUP_BACKUP()
	INT i
	REPEAT 4 i
		IF NOT IS_PED_INJURED(pedBackup[i])
			DELETE_PED(pedBackup[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC CHECK_AND_REMOVE_KIDNAPPER_BLIPS()
	INT iDeadCounter
	IF IS_PED_INJURED(pedKidnap)
		IF DOES_BLIP_EXIST(blipKidnap)
			REMOVE_BLIP(blipKidnap)
		ENDIF
		iDeadCounter++
	ENDIF
	IF IS_PED_INJURED(pedDriver)
		IF DOES_BLIP_EXIST(blipDriver)
			REMOVE_BLIP(blipDriver)
		ENDIF
		iDeadCounter++
	ENDIF
	IF IS_PED_INJURED(pedPassenger)
		IF DOES_BLIP_EXIST(blipPassenger)
			REMOVE_BLIP(blipPassenger)
		ENDIF
		iDeadCounter++
	ENDIF
	IF NOT IS_PED_INJURED(pedVictim)
		IF GET_GAME_TIMER() > iScreamTimer+8000
			PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE)
			iScreamTimer = GET_GAME_TIMER() 
			PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_PED_AMBIENT_SPEECH - GENERIC_FRIGHTENED_HIGH")
		ENDIF
	ENDIF
	IF iDeadCounter = 3
		bKidnappersDead = TRUE
	ENDIF
ENDPROC

PROC RUN_ANIM_PLACEMENT()

	IF NOT IS_PED_INJURED(pedVictim)
	AND NOT IS_PED_INJURED(pedKidnap)
	AND IS_VEHICLE_DRIVEABLE(vehKidnap)
			
		#IF IS_DEBUG_BUILD
		IF bRestartScene
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
				TASK_SYNCHRONIZED_SCENE(pedKidnap, sceneIDsnatch, animDict, "guy_on_phone_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDsnatch, animDict, "girl_on_phone_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ELSE
				playFullScene = TRUE
			ENDIF
			bRestartScene = FALSE
		ENDIF
		
		IF bResetSceneOffset
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDsnatch, scenePosition, sceneRotation)
			ENDIF
			bResetSceneOffset = FALSE
		ENDIF
		#ENDIF
		
		IF playFullScene
			scenePosition = <<0.3,0,0>>
			sceneRotation = <<0,0,345>>
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.9
					sceneIDsnatch = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDsnatch, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
					TASK_SYNCHRONIZED_SCENE(pedKidnap, sceneIDsnatch, animDict, "GUY_DRAG_INTO_VAN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDsnatch, animDict, "GIRL_DRAG_INTO_VAN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDsnatch, FALSE)
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDsnatch, FALSE)
					playFullScene = FALSE
				ENDIF
			ELSE
				sceneIDsnatch = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDsnatch, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDsnatch, scenePosition, sceneRotation)
				TASK_SYNCHRONIZED_SCENE(pedKidnap, sceneIDsnatch, animDict, "guy_on_phone_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDsnatch, animDict, "girl_on_phone_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDsnatch, TRUE)
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDsnatch, TRUE)
			ENDIF
		ENDIF
		
		IF setOffsets
//			IF IS_ENTITY_ATTACHED_TO_ENTITY(pedVictim, vehKidnap)
//				DETACH_ENTITY(pedVictim)
//			ENDIF
//			IF IS_ENTITY_ATTACHED_TO_ENTITY(pedKidnap, vehKidnap)
//				DETACH_ENTITY(pedKidnap)
//			ENDIF
//			ATTACH_ENTITY_TO_ENTITY(pedVictim, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, "chassis"), vVictimOffset, <<0,0,wrap((fInput-fVictim), 0,360)>>)
//			ATTACH_ENTITY_TO_ENTITY(pedKidnap, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, "chassis"), vKidnapperOffset, <<0,0,wrap((fInput-fKidnapper), 0,360)>>)
			SET_ENTITY_COORDS(pedVictim, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, vVictimOffset))
			SET_ENTITY_HEADING(pedVictim, wrap((fInput-fVictim), 0,360))
			SET_ENTITY_COORDS(pedKidnap, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, vKidnapperOffset))
			SET_ENTITY_HEADING(pedKidnap, wrap((fInput-fKidnapper), 0,360))
			setOffsets = FALSE
		ENDIF
		
		IF toggleVanDoors
			IF IS_VEHICLE_DOOR_FULLY_OPEN(vehKidnap, SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_SHUT(vehKidnap, SC_DOOR_REAR_LEFT)
			ELSE
				SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_LEFT)
			ENDIF
			IF IS_VEHICLE_DOOR_FULLY_OPEN(vehKidnap, SC_DOOR_REAR_RIGHT)
				SET_VEHICLE_DOOR_SHUT(vehKidnap, SC_DOOR_REAR_RIGHT)
			ELSE
				SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_RIGHT)
			ENDIF
			toggleVanDoors = FALSE
		ENDIF
		
	ENDIF
ENDPROC

//
//PROC PLAY_FULL_EXIT_SCENE()
//
//	VEHICLE_INDEX tempCar
//	VECTOR vPlayersVeh, tempArea
//	FLOAT fPlayersVeh
//	
//	//PRINTLN("PLAY_FULL_EXIT_SCENE()")
//	//PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, kidnapperSceneCantRun = ", kidnapperSceneCantRun, ", bKidnappersDead = ", bKidnappersDead, ", bThankYouPlayed = ", bThankYouPlayed, ", bCowerWhenFinished = ", bCowerWhenFinished)
//	
//	IF bCowerWhenFinished
//	
//		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDexit)
//			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDexit) > 0.9
//				IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
//					TASK_COWER(pedVictim)
//					bExitSceneComplete = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ELSE
//	
//		IF IS_VEHICLE_DRIVEABLE(vehKidnap)
//			IF playFullExitScene
//				IF IS_VEHICLE_STOPPED(vehKidnap)
//					IF NOT IS_PED_INJURED(pedKidnap)
//						IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), vehKidnap, VS_BACK_RIGHT)
//						AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), vehKidnap, VS_BACK_LEFT)
//							SET_ENTITY_COORDS(pedKidnap, GET_ENTITY_COORDS(vehKidnap, FALSE))
//							scenePosition = <<0.3,0,0>>
//							sceneRotation = <<0,0,-5>>
//							sceneIDexit = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDexit, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
//							SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDexit, scenePosition, sceneRotation)
//							CLEAR_PED_TASKS_IMMEDIATELY(pedKidnap)
//							SET_PED_KEEP_TASK(pedKidnap, TRUE)
//							TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
//							playFullExitScene = FALSE
//							triggerFemaleExit = TRUE PRINTLN("triggerFemaleExit = TRUE 1")
//							kidnapperSceneCantRun = TRUE
//						ENDIF
//					ELSE
//						TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
//						kidnapperSceneCantRun = TRUE
//						playFullExitScene = FALSE
//						triggerFemaleExit = TRUE PRINTLN("triggerFemaleExit = TRUE 2")
//					ENDIF
//				ENDIF
//			ELIF triggerFemaleExit
//				IF (kidnapperSceneCantRun AND bKidnappersDead AND bThankYouPlayed)
//					IF CAN_PLAYER_START_CUTSCENE()
//						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<2.5,2.5,2.5>>, FALSE, TRUE, TM_ON_FOOT) 
//						
//							PRINTLN(" PLAYING FAST EXIT ANIM ")
//							
//							SET_SCRIPTS_SAFE_FOR_CUTSCENE()
//							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//							HIDE_HUD_AND_RADAR_THIS_FRAME()
//							exitVanCam.StartCamPos = <<-1.2232, -4.3354, 1.3385>>
//							exitVanCam.StartCamRot = <<-22.8598, -0.0000, GET_ENTITY_HEADING(vehKidnap)-60>>
//							exitVanCam.EndCamPos = <<-1.4334, -4.6656, 1.3733>>
//							exitVanCam.EndCamRot = <<-22.8598, -0.0000, -106.3162>>
//							exitVanCam.CamFov = 55
//							exitVanCam.ShotTime = 3000
//							exitVanCam.AttachToEntity = FALSE
//							Run_camera_Attach_Entity_Static(exitVanCam, vehKidnap)
//							HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//							RENDER_SCRIPT_CAMS(TRUE, FALSE)
//							
//							SET_ENTITY_COORDS(pedVictim, GET_ENTITY_COORDS(vehKidnap, FALSE))
//							scenePosition = <<0,0,0>>
//							sceneRotation = <<0,0,0>>
//							sceneIDexit = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//							
//							IF IS_ENTITY_ATTACHED(pedVictim)
//								DETACH_ENTITY(pedVictim)
//							ENDIF
//							
//							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDexit, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
//							SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDexit, scenePosition, sceneRotation)
//							TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
//							TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDexit, animDict, "IG_1_ALT1_EXIT_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDexit, FALSE)
//							SET_PED_KEEP_TASK(pedVictim, TRUE)
//							PLAY_ENTITY_ANIM(vehKidnap, "IG_1_ALT1_EXIT_VAN_BURR", animDict, INSTANT_BLEND_IN, FALSE, TRUE)
//							SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_LEFT)
//							SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_RIGHT)
//							scenePosition = GET_ENTITY_COORDS(vehKidnap, FALSE)
//							sceneRotation.z = GET_ENTITY_HEADING(vehKidnap)
//							
//							tempArea = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<0,-6,0>>)
//							
//							IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
//								PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())")
//								IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), tempArea, <<5,5,5>>)
//									PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), tempArea")
//									WHILE NOT GET_POSITION_FOR_PLAYERS_CAR(tempArea, vPlayersVeh, fPlayersVeh)
//										WAIT(0)
//									ENDWHILE
//									tempCar = GET_PLAYERS_LAST_VEHICLE()
//									SET_ENTITY_HEADING(tempCar, fPlayersVeh)
//									SET_ENTITY_COORDS(tempCar, vPlayersVeh)
//								ENDIF
//							ENDIF
//							
//							tempArea = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<0,-4.7,0>>)
//							
//							GET_GROUND_Z_FOR_3D_COORD(tempArea, tempArea.Z)
//							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(vehKidnap))
//							SET_ENTITY_COORDS(PLAYER_PED_ID(), tempArea)
//							
//							SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle)
//							triggerFemaleExit = FALSE
//							bExitSceneComplete = TRUE
//							PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, bExitSceneComplete 1")
//							
//						ENDIF
//					ENDIF
//					
//				ELSE
//					//PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, FAIL 1")
//					IF NOT IS_PED_INJURED(pedKidnap)
//						IF GET_SCRIPT_TASK_STATUS(pedKidnap, SCRIPT_TASK_COMBAT) = FINISHED_TASK
//						AND GET_SCRIPT_TASK_STATUS(pedKidnap, SCRIPT_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
//							TASK_COMBAT_PED(pedKidnap, PLAYER_PED_ID())
//						ENDIF
//					ENDIF
//					IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_LOOK_AT_ENTITY) = FINISHED_TASK
//						TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
//					ENDIF
//				ENDIF
//			ELSE
//				IF snatchedStage = driveHome
//					PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, bExitSceneComplete 2")
//					bExitSceneComplete = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC SET_UP_ANIMS()
	IF widgetsCreated
		//RUN_ANIM_PLACEMENT()
	ELSE
#IF IS_DEBUG_BUILD
	START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())	
	
		ADD_WIDGET_BOOL("Restart scene", bRestartScene)
		
		ADD_WIDGET_FLOAT_SLIDER("scenePosition.x", scenePosition.x, -5, 5, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("scenePosition.y", scenePosition.y, -5, 5, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("scenePosition.z", scenePosition.z, -5, 5, 0.1)
		
		ADD_WIDGET_FLOAT_SLIDER("sceneRotation.x", sceneRotation.x, -180, 180, 1)
		ADD_WIDGET_FLOAT_SLIDER("sceneRotation.y", sceneRotation.y, -180, 180, 1)
		ADD_WIDGET_FLOAT_SLIDER("sceneRotation.z", sceneRotation.z, 0, 360, 1)
		
		ADD_WIDGET_BOOL("Reset scene offset", bResetSceneOffset)
		ADD_WIDGET_BOOL("Play full scene", playFullScene)
		ADD_WIDGET_BOOL("Toggle van doors", toggleVanDoors)
		
		ADD_WIDGET_FLOAT_SLIDER("Offset X", vCamOffsetPos.x, -1, 1, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Offset Y", vCamOffsetPos.y, -1, 1, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Offset Z", vCamOffsetPos.z, -1, 1, 0.01)
		ADD_WIDGET_BOOL("Test cam", bTestCam)
		
	STOP_WIDGET_GROUP()
#ENDIF	//	IS_DEBUG_BUILD

		widgetsCreated = TRUE
	ENDIF
ENDPROC

PROC RUN_HINT_CAM()
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(vehKidnap)
        	CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehKidnap)
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
    ELSE
        KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
    ENDIF
ENDPROC

INT iDropOffStageProgress

FUNC BOOL HAS_VICTIM_BEEN_DROPPED_OFF()

	SEQUENCE_INDEX seqTemp
	
	IF NOT IS_PED_INJURED(pedVictim)	
	
		SWITCH iDropOffStageProgress
		
			CASE 0
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						WAIT(0)
					ENDWHILE
				ENDIF
				
				IF NOT IS_PED_INJURED(pedVictim)
					IF IS_PED_IN_GROUP(pedVictim)
						REMOVE_PED_FROM_GROUP(pedVictim)
					ENDIF
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION()
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)
				
				WHILE NOT CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_ARRV", CONV_PRIORITY_AMBIENT_HIGH)
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
					TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
				ENDIF
				
				TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 30000, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, 30000, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV)
				
				iDropOffStageProgress++
				
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OPEN_SEQUENCE_TASK(seqTemp)
					IF IS_PED_IN_ANY_VEHICLE(pedVictim)
						TASK_LEAVE_ANY_VEHICLE(NULL)
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-3069.7371, 455.0024, 8.6478>>, PEDMOVEBLENDRATIO_WALK, 30000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)
					TASK_ACHIEVE_HEADING(NULL, 73.0132)
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_SMOKING", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedVictim, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
					iDropOffStageProgress++
					SETTIMERA(0)
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_PED_INJURED(pedVictim)
					IF NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
						SETTIMERA(0)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
						iDropOffStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF TIMERA() > 1000
					RETURN TRUE
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_DROP_OFF_CUT_FINISHED()
	VECTOR vPlayer, vVictim, vEndPlayer 
	FLOAT fPlayer, fEndPlayer
	
	vStartPos   = << -3054.8135, 437.3873, 6.6462 >> 
    vStartRot   = << 3.4436, 0.0000, 89.0629 >>
	vVictim		= << -3055.6208, 436.4604, 5.3614 >>
	fVictim		= 69.8396
	vEndLocation = << -3053.7212, 440.9221, 5.3566 >>
	
	vPlayer 	= << -3052.7913, 436.0500, 5.3118 >>
	fPlayer		= 2.0996
	vEndPlayer 	= << -3052.0864, 434.8363, 5.2950 >>
	fEndPlayer	= 271.0279
	
    HIDE_HUD_AND_RADAR_THIS_FRAME()
	
    IF NOT IS_PED_INJURED(pedVictim)
    AND NOT IS_PED_INJURED(PLAYER_PED_ID())
        IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
            returnCutStage = cleanupCut
        ENDIF
        SWITCH returnCutStage
			CASE stopTheCar
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF CAN_PLAYER_START_CUTSCENE()
						IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
							SET_SCRIPTS_SAFE_FOR_CUTSCENE()
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 5)
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								WAIT(0)
								ENDWHILE
							ENDIF
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							returnCutStage = setTheScene
							SETTIMERA(0)
						ENDIF
					ENDIF
				ENDIF
			BREAK
            CASE setTheScene

                CLEAR_AREA_OF_PEDS(vVictim, 20)
				CLEAR_AREA(vVictim, 10, TRUE)
                //SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEANUP_BACKUP()
				DELETE_SURVIVING_LOST()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
                    SET_ENTITY_COORDS(pedVictim, vVictim)
					SET_ENTITY_HEADING(pedVictim, fVictim)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
                    SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vPlayer)
					SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fPlayer)
					
//					vStartPos = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0.3,0.45,0>>)
					vStartPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_WORLD_POSITION_OF_ENTITY_BONE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), "seat_dside_f")), fPlayer, vCamOffsetPos)
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//						vStartPos.z -= 0.075
//					ENDIF
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedVictim, -1, SLF_WIDEST_YAW_LIMIT)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayer)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fVictim)
					
					SET_ENTITY_COORDS(pedVictim, vVictim)
					SET_ENTITY_HEADING(pedVictim, fVictim)
					vStartPos = << -3051.6760, 436.5014, 6.9980 >>
					vStartRot = << -2.1870, -0.0000, 100.6721 >>
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, pedVictim, 5000, SLF_EXTEND_YAW_LIMIT)	
						TASK_PAUSE(NULL, 6000)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vEndPlayer, PEDMOVEBLENDRATIO_WALK, DEFAULT, fEndPlayer)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
                
                camStart = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vStartPos, vStartRot, 40.000000)
                camEnd = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vStartPos, vStartRot, 50.000000)
                SET_CAM_ACTIVE_WITH_INTERP(camEnd, camStart, 10000)
                RENDER_SCRIPT_CAMS(TRUE, FALSE)
				TASK_PLAY_ANIM(pedVictim, animDict, "Exit")
                returnCutStage = runTasks
            BREAK
            
            CASE runTasks
				SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(PLAYER_PED_ID(), FALSE)
                IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
				OR NOT IS_CAM_INTERPOLATING(camEnd)
				OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					
					returnCutStage = cleanupCut
				ELIF NOT bPlayedCutDialogue
					IF IS_ENTITY_PLAYING_ANIM(pedVictim, animDict, "Exit")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedVictim, animDict, "Exit") > 0.24
							CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_ARRV", CONV_PRIORITY_AMBIENT_HIGH)
							bPlayedCutDialogue = TRUE
						ENDIF
					ENDIF
                ENDIF

            BREAK
            
            CASE cleanupCut
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vEndPlayer)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fEndPlayer)
				ENDIF
                RENDER_SCRIPT_CAMS(FALSE, FALSE)
                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
                SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DELETE_PED(pedVictim)
                SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
                RETURN TRUE
            BREAK
        ENDSWITCH
        
    ENDIF
    RETURN FALSE
ENDFUNC

PROC BLIP_BAD_GUYS()
	IF NOT IS_PED_INJURED(pedDriver)
		IF NOT DOES_BLIP_EXIST(blipDriver)
			blipDriver = CREATE_AMBIENT_BLIP_FOR_PED(pedDriver, TRUE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedKidnap)
		IF IS_ENTITY_AT_ENTITY(pedKidnap, PLAYER_PED_ID(), <<100,100,20>>)
			IF NOT DOES_BLIP_EXIST(blipKidnap)
				blipKidnap = CREATE_AMBIENT_BLIP_FOR_PED(pedKidnap, TRUE)
			ENDIF
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedKidnap)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedPassenger)
		IF IS_ENTITY_AT_ENTITY(pedPassenger, PLAYER_PED_ID(), <<100,100,20>>)
			IF NOT DOES_BLIP_EXIST(blipPassenger)
				blipPassenger = CREATE_AMBIENT_BLIP_FOR_PED(pedPassenger, TRUE)
			ENDIF
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedPassenger)
		ENDIF
	ENDIF
ENDPROC

PROC DRIVER_ABANDONS_VAN()

    IF NOT IS_PED_INJURED(pedDriver)
		TASK_COMBAT_PED(pedDriver, PLAYER_PED_ID())
		SET_PED_KEEP_TASK(pedDriver, TRUE)
		CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_ATT", CONV_PRIORITY_AMBIENT_HIGH)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedKidnap)
		GIVE_WEAPON_TO_PED(pedKidnap, WEAPONTYPE_SMG, 68, TRUE)
		SET_CURRENT_PED_WEAPON(pedKidnap, WEAPONTYPE_SMG, TRUE)
		SET_PED_CAN_BE_TARGETTED(pedKidnap, TRUE)
		TASK_COMBAT_PED(pedKidnap, PLAYER_PED_ID())
		SET_PED_KEEP_TASK(pedKidnap, TRUE)
    ENDIF
	
	IF NOT IS_PED_INJURED(pedPassenger)
		GIVE_WEAPON_TO_PED(pedPassenger, WEAPONTYPE_PISTOL, 68, TRUE)
		SET_CURRENT_PED_WEAPON(pedPassenger, WEAPONTYPE_PISTOL, TRUE)
		TASK_COMBAT_PED(pedPassenger, PLAYER_PED_ID())
		SET_PED_KEEP_TASK(pedPassenger, TRUE)
	ENDIF
	
	BLIP_BAD_GUYS()
	CLEAR_SEQUENCE_TASK(seq)
	snatchedStage = fightKidnappers
	
ENDPROC

PROC ATTACK_PLAYER()
	DRIVER_ABANDONS_VAN()
	WAIT(0)
ENDPROC

FUNC BOOL IS_VAN_TYRE_BURST()
    IF IS_VEHICLE_DRIVEABLE(vehKidnap)
        IF IS_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_FRONT_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_FRONT_RIGHT)
        OR IS_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_REAR_LEFT)
        OR IS_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_REAR_RIGHT)
			PRINTSTRING("\nIS_VAN_TYRE_BURST()\n")
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE    
ENDFUNC

FUNC BOOL HAS_GUNSHOT_SCARED_DRIVER()
    IF NOT IS_PED_INJURED(pedDriver)
        IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedDriver, BONETAG_HEAD, <<0,0,0>>), 1)
			PRINTSTRING("\nHAS_GUNSHOT_SCARED_DRIVER()\n")
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_KILLED_A_KIDNAPPER()
	IF IS_PED_INJURED(pedDriver)
		RETURN TRUE
	ELSE
		IF IS_PED_ON_FOOT(pedDriver)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedKidnap)
		RETURN TRUE
    ENDIF
	
	IF IS_PED_INJURED(pedPassenger)
		RETURN TRUE
    ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_TAKEN_ENOUGH_DAMAGE()
    IF IS_VEHICLE_DRIVEABLE(vehKidnap)
        IF GET_VEHICLE_ENGINE_HEALTH(vehKidnap) < 700
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehKidnap) < 700
			PRINTSTRING("\nHAS_VEHICLE_TAKEN_ENOUGH_DAMAGE()\n")
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

INT iTimeFirstBump = -1

PROC SET_DRIVEBY_TASKS()
	IF NOT IS_PED_INJURED(pedDriver)
		IF IS_PED_IN_ANY_VEHICLE(pedDriver)
			IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedDriver) 
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> pedDriver given TASK_DRIVE_BY")
				IF NOT HAS_PED_GOT_WEAPON(pedDriver, WEAPONTYPE_MICROSMG)
				OR (HAS_PED_GOT_WEAPON(pedDriver, WEAPONTYPE_MICROSMG) AND GET_PED_AMMO_BY_TYPE(pedDriver, AMMOTYPE_SHOTGUN) = 0)
					GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_MICROSMG, 68, TRUE)
				ELSE
					SET_CURRENT_PED_WEAPON(pedDriver, WEAPONTYPE_MICROSMG, TRUE)
				ENDIF
				TASK_DRIVE_BY(pedDriver, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 100, TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF GET_GAME_TIMER() - iTimeFirstBump > 250
		IF NOT IS_PED_INJURED(pedKidnap)
			IF IS_PED_IN_ANY_VEHICLE(pedKidnap)
			AND GET_SCRIPT_TASK_STATUS(pedKidnap, SCRIPT_TASK_DRIVE_BY) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedKidnap, SCRIPT_TASK_DRIVE_BY) <> WAITING_TO_START_TASK
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> pedKidnap given TASK_DRIVE_BY")
				CLEAR_PED_TASKS(pedKidnap)
				GIVE_WEAPON_TO_PED(pedKidnap, WEAPONTYPE_MICROSMG, 68, TRUE)
				TASK_DRIVE_BY(pedKidnap, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 100)
				//SET_PED_KEEP_TASK(pedKidnap, TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedPassenger)
		IF IS_PED_IN_ANY_VEHICLE(pedPassenger)
		AND GET_SCRIPT_TASK_STATUS(pedPassenger, SCRIPT_TASK_DRIVE_BY) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedPassenger, SCRIPT_TASK_DRIVE_BY) <> WAITING_TO_START_TASK
			PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> pedPassenger given TASK_DRIVE_BY")
			CLEAR_PED_TASKS(pedPassenger)
			GIVE_WEAPON_TO_PED(pedPassenger, WEAPONTYPE_PISTOL, 68, TRUE)
			TASK_DRIVE_BY(pedPassenger, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 100)
			SET_PED_KEEP_TASK(pedPassenger, TRUE)
		ENDIF
	ENDIF
	IF NOT bSetDriveby
		bSetDriveby = TRUE
	ENDIF
ENDPROC

FUNC BOOL HAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()
    IF IS_VEHICLE_DRIVEABLE(vehKidnap)
		IF bumpedCounter > 0
			SET_DRIVEBY_TASKS()
		ENDIF
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehKidnap, <<20,20,10>>)
            IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehKidnap, PLAYER_PED_ID())
                IF iTimeFirstBump = -1
					iTimeFirstBump = GET_GAME_TIMER()
				ENDIF
				bumpedCounter++
                CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehKidnap)
				IF TIMERA() > 10000
					IF GET_RANDOM_INT_IN_RANGE(0,100) > 49
						CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_STRG", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						IF NOT IS_PED_INJURED(pedDriver)
							CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_FOFF", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
					SETTIMERA(0)
				ENDIF
            ENDIF
            IF bumpedCounter > 30
				PRINTSTRING("\nHAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()\n")
                RETURN TRUE
            ENDIF
        ELSE
            bumpedCounter = 0
        ENDIF
		IF IS_VEHICLE_STUCK_ON_ROOF(vehKidnap)
			RETURN TRUE
		ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

PROC ADD_ALTRUIST_CULT_BLIP()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF NOT DOES_BLIP_EXIST(blipCult)
			PRINT_CULT_HELP()
			blipCult = CREATE_AMBIENT_BLIP_FOR_COORD(vCult)
			SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
			REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
			PRINTLN("<re_snatched> - ADD_ALTRUIST_CULT_BLIP()")
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALTRUIST_CULT_BLIP()
	IF DOES_BLIP_EXIST(blipCult)
		REMOVE_BLIP(blipCult)
		PRINTLN("<re_snatched> - REMOVE_ALTRUIST_CULT_BLIP()")
	ENDIF
ENDPROC

PROC PLAY_BACKUP_DIALOGUE()
	VECTOR vMin = <<-20,-20,-20>>, vMax = <<20,20,20>>
	IF NOT bPlayedBackupDialogue
		vMin = GET_ENTITY_COORDS(pedVictim, FALSE)+vMin
		vMax = GET_ENTITY_COORDS(pedVictim, FALSE)+vMax
		//IF IS_ANY_PED_SHOOTING_IN_AREA(vMin, vMax, FALSE)
			IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_MOR", CONV_PRIORITY_AMBIENT_HIGH, SET_SUB_DISPLAY_BASED_ON_DISTANCE())
				IF NOT IS_PED_INJURED(pedVictim)
				AND NOT IS_PED_INJURED(pedShooter[0])
					TASK_LOOK_AT_ENTITY(pedVictim, pedRider[0], 20000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
				ENDIF
				bPlayedBackupDialogue = TRUE
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

//PROC DO_EXTRA_BIKER_WARPING()
//	IF GET_GAME_TIMER() - iTimeOfAttack > 4000
//	AND GET_GAME_TIMER() - iTimeOfAttack < 30000
//		IF NOT IS_PED_INJURED(pedShooter[iCurrentWarper])
//		AND NOT IS_PED_INJURED(pedRider[iCurrentWarper])
//		AND IS_VEHICLE_DRIVEABLE(vehBike[iCurrentWarper])
//		AND IS_PED_IN_VEHICLE(pedShooter[iCurrentWarper], vehBike[iCurrentWarper])
//		AND IS_PED_IN_VEHICLE(pedRider[iCurrentWarper], vehBike[iCurrentWarper])
//		AND IS_POINT_BEHIND_PLAYER(GET_ENTITY_COORDS(pedRider[iCurrentWarper]))
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX vehTemp
//				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				IF IS_VEHICLE_DRIVEABLE(vehTemp)
//				AND GET_ENTITY_SPEED(vehTemp) > 10
//					IF NOT IS_PED_INJURED(pedVictim)
//					AND IS_PED_IN_ANY_VEHICLE(pedVictim)
//					AND IS_PED_IN_VEHICLE(pedVictim, vehTemp)
//						PRINTLN("CHECKING FOR WARP FOR PED ", iCurrentWarper)
//						RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehBike[iCurrentWarper], vehTemp, vBikerWarpPosition[iCurrentWarper], fBikerWarpHeading[iCurrentWarper], vBikerLastTargetPosition[iCurrentWarper], fBikerLastTargetHeading[iCurrentWarper], iChaseWarpTimer, <<0,0,0>>, 1000, 30.0)
//						IF iCurrentWarper = 1
//							iCurrentWarper = 0
//						ELSE
//							iCurrentWarper = 1
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC CHECK_DISTANCE_TO_HOUSE_AND_CREATE_MORE_BAD_GUYS()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndLocation) < 1500
			OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vCult) < 1500
				IF LOADED_AND_CREATED_ATTACKING_BIKERS()
					bExtraAttackersLaunched = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_BIKER_BEEN_SPOTTED(PED_INDEX ped)
	BOOL bRet
	IF DOES_ENTITY_EXIST(ped)
	AND NOT IS_PED_INJURED(ped)
	AND NOT IS_ENTITY_DEAD(ped)
		IF NOT IS_ENTITY_OCCLUDED(ped) 
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) , GET_ENTITY_COORDS(ped, FALSE)) < 100*100
			bRet = TRUE
		ENDIF
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) , GET_ENTITY_COORDS(ped, FALSE)) < 50*50
			bRet = TRUE
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PLAYER_PED_ID(), ped)
			bRet = TRUE
		ENDIF
		IF NOT IS_PED_INJURED(pedVictim)
		AND IS_PED_IN_ANY_VEHICLE(pedVictim)
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(pedVictim))
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(pedVictim), ped)
					bRet = TRUE
				ENDIF
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != GET_VEHICLE_PED_IS_IN(pedVictim)	
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), ped)
						bRet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bRet
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_CLOSEST_BIKER()
	INT i
	FLOAT fClosestDist = -1
	REPEAT COUNT_OF(pedRider) i
		IF DOES_ENTITY_EXIST(pedRider[i])
		AND NOT IS_PED_INJURED(pedRider[i])
		AND NOT IS_ENTITY_DEAD(pedRider[i])
			IF fClosestDist = -1
				fClosestDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedRider[i], FALSE))
			ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedRider[i], FALSE)) < fClosestDist
				fClosestDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedRider[i], FALSE))
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(pedShooter) i
		IF DOES_ENTITY_EXIST(pedShooter[i])
		AND NOT IS_PED_INJURED(pedShooter[i])
		AND NOT IS_ENTITY_DEAD(pedShooter[i])
			IF fClosestDist = -1
				fClosestDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedShooter[i], FALSE))
			ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedShooter[i], FALSE)) < fClosestDist
				fClosestDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedShooter[i], FALSE))
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN fClosestDist	
ENDFUNC

FUNC BOOL ARE_EXTRA_ATTACKERS_LOST_OR_DEAD()
	
	FLOAT fDist = GET_DISTANCE_TO_CLOSEST_BIKER()
	
	IF fDist = -1
	OR fDist > 300
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
		
ENDFUNC

FUNC BOOL HAS_VEHICLE_REACHED_GANG_HOUSE()
	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
		IF IS_ENTITY_AT_COORD(vehKidnap, vGangHouse, <<10,10,10>>)
			bCreateBackupAtGangHouse = TRUE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_EVERYONE_IN_VAN()

	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
	AND NOT IS_PED_INJURED(pedVictim)
	AND NOT IS_PED_INJURED(pedKidnap)
	AND NOT IS_PED_INJURED(pedPassenger)
//	
//		SET_ENTITY_NO_COLLISION_ENTITY(vehKidnap, pedKidnap, TRUE)
//		SET_ENTITY_NO_COLLISION_ENTITY(vehKidnap, pedVictim, TRUE)
//		SET_ENTITY_NO_COLLISION_ENTITY(vehKidnap, pedPassenger, TRUE)
//		
//		SET_ENTITY_NO_COLLISION_ENTITY(pedKidnap, vehKidnap, TRUE)
//		SET_ENTITY_NO_COLLISION_ENTITY(pedVictim, vehKidnap, TRUE)
//		SET_ENTITY_NO_COLLISION_ENTITY(pedPassenger, vehKidnap, TRUE)
		
		SET_PED_INTO_VEHICLE(pedPassenger, vehKidnap, VS_BACK_RIGHT)
		SET_PED_INTO_VEHICLE(pedKidnap, vehKidnap, VS_BACK_LEFT)
		ATTACH_ENTITY_TO_ENTITY(pedVictim, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone), << 0.421191, -1.99042, 0.20134 >>, << -5.08056e-06, -6.5249e-06, 174.612 >>, TRUE, FALSE)
		SET_ENTITY_COORDS(pedVictim, GET_ENTITY_COORDS(vehKidnap, FALSE))
		
		CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
		TASK_PLAY_ANIM(pedVictim, animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
		SET_PED_KEEP_TASK(pedVictim, TRUE)
		
//		IF IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "DRAG_INTO_VAN_BURR")
//			STOP_ENTITY_ANIM(vehKidnap, "DRAG_INTO_VAN_BURR", animDict, INSTANT_BLEND_OUT)
//		ENDIF
		
		IF NOT DOES_BLIP_EXIST(blipDriver)
			blipDriver = CREATE_AMBIENT_BLIP_FOR_PED(pedDriver, TRUE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehKidnap)
			SET_VEHICLE_DOORS_SHUT(vehKidnap, FALSE)
		ENDIF
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		
		bSnatchOngoing = FALSE
		
	ENDIF
	
ENDPROC

PROC SET_VICTIM_IN_VAN()
	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
	AND NOT IS_PED_INJURED(pedVictim)
		ATTACH_ENTITY_TO_ENTITY(pedVictim, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone), << 0.421191, -1.99042, 0.20134 >>, << -5.08056e-06, -6.5249e-06, 174.612 >>, TRUE, FALSE)
		SET_ENTITY_COORDS(pedVictim, GET_ENTITY_COORDS(vehKidnap, FALSE))
		CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
		TASK_PLAY_ANIM(pedVictim, animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
		SET_PED_KEEP_TASK(pedVictim, TRUE)
		IF NOT DOES_BLIP_EXIST(blipDriver)
			blipDriver = CREATE_AMBIENT_BLIP_FOR_PED(pedDriver, TRUE)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehKidnap)
			SET_VEHICLE_DOORS_SHUT(vehKidnap)
		ENDIF
		bSnatchOngoing = FALSE	
	ENDIF
ENDPROC

PROC ALLOW_PASSENGER_TO_FINISH_CLOSING_DOOR()
	IF NOT IS_PED_INJURED(pedPassenger)
	AND IS_VEHICLE_DRIVEABLE(vehKidnap)
		IF NOT IS_PED_IN_VEHICLE(pedPassenger, vehKidnap)
			IF NOT IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "IG_1_ALT1_EXIT_VAN_BURR")
	//			SET_ENTITY_NO_COLLISION_ENTITY(vehKidnap, pedPassenger, TRUE)
	//			SET_ENTITY_NO_COLLISION_ENTITY(pedPassenger, vehKidnap, TRUE)
				SET_PED_INTO_VEHICLE(pedPassenger, vehKidnap, VS_BACK_LEFT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_SNATCHING()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSnatchScene)
		IF NOT IS_PED_INJURED(pedKidnap)
			CLEAR_PED_TASKS(pedKidnap)
		ENDIF
		IF NOT IS_PED_INJURED(pedPassenger)
			CLEAR_PED_TASKS(pedPassenger)
		ENDIF
		IF NOT IS_PED_INJURED(pedVictim)
			CLEAR_PED_TASKS(pedVictim)
		ENDIF
		STOP_SYNCHRONIZED_ENTITY_ANIM(vehKidnap, SLOW_BLEND_OUT, TRUE)
	ENDIF
ENDPROC

PROC missionCleanup()
	WAIT(0)
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	IF g_bAltruistCultFinalDialogueHasPlayed
		TRIGGER_MUSIC_EVENT("AC_STOP")
	ENDIF
	RESET_CULT_STATUS()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		SET_WANTED_LEVEL_MULTIPLIER(1)
		REMOVE_SCENARIO_BLOCKING_AREA(ganghouseBlocking)
	ELSE
		Mission_Over(g_iRECandidateID)
	ENDIF
	
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1)
	
	IF DOES_ENTITY_EXIST(pedVictim)
	AND NOT IS_PED_INJURED(pedVictim)
	AND IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
		REMOVE_PED_FROM_GROUP(pedVictim)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
		SET_VEHICLE_DOORS_LOCKED(vehKidnap, VEHICLELOCK_UNLOCKED)
	ENDIF
	SET_BLOCKING_AREAS(FALSE)
	RANDOM_EVENT_OVER()
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_PARK_PERPENDICULAR_NOSE_IN", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
	
    TERMINATE_THIS_THREAD()
	
ENDPROC

PROC missionPassed()

	IF NOT IS_PED_INJURED(pedVictim)
		SET_PED_CONFIG_FLAG(pedVictim, PCF_CanSayFollowedByPlayerAudio, TRUE)
	ENDIF
	
	RANDOM_EVENT_PASSED()
	LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
	
ENDPROC

PROC VICTIM_FLEES()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
		DETACH_SYNCHRONIZED_SCENE(sceneID)
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
		DETACH_SYNCHRONIZED_SCENE(sceneIDsnatch)
	ENDIF
	IF NOT IS_PED_INJURED(pedVictim)
		SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
		IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP")
			TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
			SET_PED_KEEP_TASK(pedVictim, TRUE)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTSTRING("\nre_snatched - pedVictim was aggroed by the player.\n")
	#ENDIF
	DRIVER_ABANDONS_VAN()
	PRINTLN("mission cleanup 3")
	missionCleanup()
ENDPROC

PROC CHECK_FOR_PLAYER_AGGROING_GIRL()
	IF NOT IS_PED_INJURED(pedVictim)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
		OR IS_ENTITY_ON_FIRE(pedVictim)
			PRINTSTRING(" - HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY\n") 
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSnatchScene)
				STOP_SNATCHING()
			ENDIF
			VICTIM_FLEES()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_SHOOTING_AT_GIRL()
	VECTOR vTempMax = <<10,10,3>>, vTempMin = <<-10,-10,-3>>
	IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 15)
		PRINTLN("<re_snatched> - IS_PLAYER_SHOOTING_AT_GIRL() - Player bullet near girl.")
		RETURN TRUE
	ENDIF
	
	vTempMax = vTempMax+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
	vTempMin = vTempMin+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
	IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
	OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_VEHICLE_WEAPON_TANK, TRUE)
		PRINTLN("<re_snatched> - IS_PLAYER_SHOOTING_AT_GIRL() - Player is using a projectile")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(pedVictim)
		PRINTLN("<re_snatched> - IS_PLAYER_SHOOTING_AT_GIRL() - Girl is on fire")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_PLAYER_AGGROING_VAN()

	BOOL bFlag = FALSE
	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehKidnap, PLAYER_PED_ID())
			bFlag = TRUE
		ENDIF
	ELSE
		bFlag = TRUE
	ENDIF
	
	IF IS_PED_INJURED(pedKidnap)
		bFlag = TRUE
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedKidnap, PLAYER_PED_ID())
		bFlag = TRUE
	ENDIF
	
	IF IS_PED_INJURED(pedDriver)
		bFlag = TRUE
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDriver, PLAYER_PED_ID())
		bFlag = TRUE
	ENDIF
	
	IF IS_PED_INJURED(pedPassenger)
		bFlag = TRUE
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPassenger, PLAYER_PED_ID())
		bFlag = TRUE
	ENDIF
	
	IF IS_PED_INJURED(pedVictim)
		bFlag = TRUE
	ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
		bFlag = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF playFullScene
			bFlag = TRUE
		ENDIF
	#ENDIF
	
	IF bFlag
	
		STOP_SNATCHING()
		
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> CHECK_FOR_PLAYER_AGGROING_VAN()")
		IF bSnatchOngoing
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) = ", GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch))
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.735
					PRINTLN("SET_EVERYONE_IN_VAN() 4")
					SET_EVERYONE_IN_VAN()
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.4
					SET_VICTIM_IN_VAN()
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedVictim)
					TASK_COWER(pedVictim)
				ENDIF
			ENDIF
			IF bVanFrozen
				FREEZE_ENTITY_POSITION(vehKidnap, FALSE)
			ENDIF
			REMOVE_BLIP(blipScene)
			IF NOT DOES_BLIP_EXIST(blipDriver)
            	blipDriver = CREATE_AMBIENT_BLIP_FOR_PED(pedDriver, TRUE)
			ENDIF
			SET_WANTED_LEVEL_MULTIPLIER(0.3)
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				SET_RANDOM_EVENT_ACTIVE()
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			DRIVER_ABANDONS_VAN()
		ELSE
			IF NOT IS_PED_INJURED(pedDriver)
				IF IS_VEHICLE_DRIVEABLE(vehKidnap)
					TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehKidnap, PLAYER_PED_ID(), MISSION_FLEE, 40, DRIVINGMODE_AVOIDCARS, -1, -1)
				ELSE
					TASK_SMART_FLEE_PED(pedDriver, PLAYER_PED_ID(), 150, -1)
					SET_PED_KEEP_TASK(pedDriver, TRUE)
				ENDIF
			ENDIF
			VICTIM_FLEES()
		ENDIF
	ENDIF
ENDPROC

ENUM DRAG_INTO_VAN_STAGE
	DIV_WAIT = 0,
	DIV_GRABBING_GIRL,
	DIV_WHEEL_SPIN,
	DIV_FINISHED
ENDENUM

DRAG_INTO_VAN_STAGE eSnatchSceneStage = DIV_WAIT


//doors closed at 	0.731
//at van at 		0.502

PROC RUN_DRAG_INTO_VAN_SCENE()

	IF DOES_ENTITY_EXIST(pedKidnap)
	AND DOES_ENTITY_EXIST(pedPassenger)
	AND DOES_ENTITY_EXIST(pedVictim)
	AND DOES_ENTITY_EXIST(vehKidnap)
	
		REQUEST_ANIM_DICT("random@kidnap_girl")
		IF HAS_ANIM_DICT_LOADED("random@kidnap_girl")
		
			FLOAT fSceneSpeed = 1.25
			
			SWITCH eSnatchSceneStage
			
				CASE DIV_WAIT
					
					IF IS_VEHICLE_DRIVEABLE(vehKidnap)
					AND NOT IS_PED_INJURED(pedVictim)
					AND NOT IS_PED_INJURED(pedKidnap)
					AND NOT IS_PED_INJURED(pedPassenger)
					
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iWaitScene)
						
							scenePosition = <<0,0,0>>
							sceneRotation = <<0,0,0>>
							iWaitScene = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
							
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iWaitScene, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
							SET_SYNCHRONIZED_SCENE_LOOPED (iWaitScene, TRUE)
							SET_SYNCHRONIZED_SCENE_ORIGIN(iWaitScene, scenePosition, sceneRotation)
							
							TASK_SYNCHRONIZED_SCENE(pedVictim, iWaitScene, animDict, "IG_1_GIRL_ON_PHONE_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							TASK_SYNCHRONIZED_SCENE(pedKidnap, iWaitScene, animDict, "IG_1_GUY1_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							TASK_SYNCHRONIZED_SCENE(pedPassenger, iWaitScene, animDict, "IG_1_GUY2_IN_VAN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							
							SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
							
						ELSE
						
							CHECK_FOR_PLAYER_AGGROING_VAN()
							CHECK_FOR_PLAYER_AGGROING_GIRL()
							
							IF IS_PLAYER_SHOOTING_AT_GIRL()
								VICTIM_FLEES()
							ENDIF
							
						ENDIF
						
					ENDIF
					
				BREAK
				
				CASE DIV_GRABBING_GIRL
				
					
					IF NOT IS_PED_INJURED(pedVictim)
					AND NOT IS_PED_INJURED(pedKidnap)
					AND NOT IS_PED_INJURED(pedPassenger)
					
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iWaitScene)
							CLEAR_PED_TASKS(pedKidnap)
							CLEAR_PED_TASKS(pedVictim)
							CLEAR_PED_TASKS(pedPassenger)
						ENDIF
						
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSnatchScene)
						
							IF IS_VEHICLE_DRIVEABLE(vehKidnap)
						
								scenePosition = <<0,0,0>>
								sceneRotation = GET_ENTITY_ROTATION(vehKidnap)
								iSnatchScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehKidnap, FALSE), <<sceneRotation.x, sceneRotation.y, GET_ENTITY_HEADING(vehKidnap)>>)
								
								IF bVanFrozen
									FREEZE_ENTITY_POSITION(vehKidnap, FALSE)
									bVanFrozen = FALSE
								ENDIF
								
								SET_SYNCHRONIZED_SCENE_RATE(iSnatchScene, fSceneSpeed)
								TASK_SYNCHRONIZED_SCENE(pedVictim, iSnatchScene, animDict, "IG_1_GIRL_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								TASK_SYNCHRONIZED_SCENE(pedKidnap, iSnatchScene, animDict, "IG_1_GUY1_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								TASK_SYNCHRONIZED_SCENE(pedPassenger, iSnatchScene, animDict, "IG_1_GUY2_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(vehKidnap, iSnatchScene, "DRAG_INTO_VAN_BURR", animDict, INSTANT_BLEND_IN, SLOW_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
		//							PLAY_ENTITY_ANIM(vehKidnap, "DRAG_INTO_VAN_BURR", animDict, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0)

							ENDIF
							
						ELSE
						
//							IF IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "DRAG_INTO_VAN_BURR")
//								SET_ENTITY_ANIM_SPEED(vehKidnap, animDict, "DRAG_INTO_VAN_BURR", fSceneSpeed)
//							ENDIF
							
//							PRINTLN("Synched scene phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iSnatchScene))
//							IF IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "DRAG_INTO_VAN_BURR")
//								SET_ENTITY_ANIM_SPEED(vehKidnap, animDict, "DRAG_INTO_VAN_BURR", fSceneSPeed)
//								PRINTLN("Van anim phase is ", GET_ENTITY_ANIM_CURRENT_TIME(vehKidnap, animDict, "DRAG_INTO_VAN_BURR"))
//							ENDIF
							
							IF NOT bVictimScreams
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iSnatchScene)
								AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehKidnap, FALSE)) < 30
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND GET_SYNCHRONIZED_SCENE_PHASE(iSnatchScene) > 0.1
								AND GET_SYNCHRONIZED_SCENE_PHASE(iSnatchScene) < 0.4
									IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_HELP", CONV_PRIORITY_AMBIENT_HIGH, SET_SUB_DISPLAY_BASED_ON_DISTANCE())
										bVictimScreams = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF GET_SYNCHRONIZED_SCENE_PHASE(iSnatchScene) > 0.502
							
								SET_ENTITY_INVINCIBLE(pedVictim, TRUE)
								SET_ENTITY_INVINCIBLE(pedKidnap, TRUE)
								SET_ENTITY_INVINCIBLE(pedPassenger, TRUE)
								
								IF GET_SYNCHRONIZED_SCENE_PHASE(iSnatchScene) < 0.732
								
									IF NOT IS_ENTITY_ATTACHED(vehKidnap)
										SET_PED_CAN_RAGDOLL(pedKidnap, FALSE)
										SET_PED_CAN_RAGDOLL(pedPassenger, FALSE)
										IF NOT IS_PED_INJURED(pedDriver) AND IS_VEHICLE_DRIVEABLE(vehKidnap)
											TASK_VEHICLE_TEMP_ACTION(pedDriver, vehKidnap, TEMPACT_BURNOUT, 6000)
										ENDIF
										SET_ENTITY_INVINCIBLE(pedVictim, TRUE)
										SET_PED_CONFIG_FLAG(pedVictim, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
										SET_PED_CONFIG_FLAG(pedKidnap, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
										SET_PED_CONFIG_FLAG(pedPassenger, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
										//ATTACH_ENTITY_TO_ENTITY(vehKidnap, objVanFixer, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objVanFixer, GET_ENTITY_COORDS(vehKidnap, FALSE)), <<0,0,0>>, TRUE, FALSE, TRUE)
									ENDIF
									
									IF IS_ENTITY_DEAD(vehKidnap)
										STOP_SNATCHING()
										IF DOES_ENTITY_EXIST(pedVictim)
											SET_ENTITY_HEALTH(pedVictim, 0)
										ENDIF
										IF DOES_ENTITY_EXIST(pedPassenger)
											SET_ENTITY_HEALTH(pedPassenger, 0)
										ENDIF
										IF DOES_ENTITY_EXIST(pedKidnap)
											SET_ENTITY_HEALTH(pedKidnap, 0)
										ENDIF
										IF DOES_ENTITY_EXIST(pedDriver)
											SET_ENTITY_HEALTH(pedDriver, 0)
										ENDIF
										DETACH_ENTITY(vehKidnap, TRUE, FALSE)
									ENDIF
									
								ELSE
									SET_PED_CAN_RAGDOLL(pedKidnap, TRUE)
									SET_PED_CAN_RAGDOLL(pedPassenger, TRUE)
									SET_PED_CONFIG_FLAG(pedVictim, PCF_DontActivateRagdollFromVehicleImpact, FALSE)
									SET_PED_CONFIG_FLAG(pedKidnap, PCF_DontActivateRagdollFromVehicleImpact, FALSE)
									SET_PED_CONFIG_FLAG(pedPassenger, PCF_DontActivateRagdollFromVehicleImpact, FALSE)
									//DETACH_ENTITY(vehKidnap)
									
//									SET_ENTITY_INVINCIBLE(pedVictim, FALSE)
									SET_ENTITY_INVINCIBLE(pedKidnap, FALSE)
									SET_ENTITY_INVINCIBLE(pedPassenger, FALSE)
									STOP_SNATCHING()
									SET_EVERYONE_IN_VAN()
//									STOP_SYNCHRONIZED_ENTITY_ANIM(vehKidnap, SLOW_BLEND_OUT, TRUE)
									
									eSnatchSceneStage = DIV_FINISHED
									
									SETTIMERB(0)
									
								ENDIF
								
							ELSE
							
								CHECK_FOR_PLAYER_AGGROING_VAN()
								CHECK_FOR_PLAYER_AGGROING_GIRL()
								
								IF IS_PLAYER_SHOOTING_AT_GIRL()
									VICTIM_FLEES()
								ENDIF
								
							ENDIF
							
						ENDIF
						
					ENDIF
					
				BREAK
				
				CASE DIV_FINISHED
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL HAS_SNATCH_FINISHED()

	bSnatchOngoing = TRUE
	FLOAT fSceneSpeed = 1.25
	IF NOT IS_PED_INJURED(pedKidnap)
	AND NOT IS_PED_INJURED(pedVictim)
	AND NOT IS_PED_INJURED(pedPassenger)
	AND IS_VEHICLE_DRIVEABLE(vehKidnap)
	
		IF IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "DRAG_INTO_VAN_BURR")
			SET_ENTITY_ANIM_SPEED(vehKidnap, animDict, "DRAG_INTO_VAN_BURR", fSceneSPeed)
		ENDIF
		
		SWITCH initialSnatchCut
			CASE startTheSnatch
				sceneIDsnatch = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehKidnap, FALSE), <<sceneRotation.x, sceneRotation.y, GET_ENTITY_HEADING(vehKidnap)>>) //
				SET_SYNCHRONIZED_SCENE_RATE(sceneIDsnatch, fSceneSpeed)
				TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDsnatch, animDict, "IG_1_GIRL_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				TASK_SYNCHRONIZED_SCENE(pedKidnap, sceneIDsnatch, animDict, "IG_1_GUY1_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				TASK_SYNCHRONIZED_SCENE(pedPassenger, sceneIDsnatch, animDict, "IG_1_GUY2_DRAG_INTO_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_ENTITY_ANIM(vehKidnap, "DRAG_INTO_VAN_BURR", animDict, INSTANT_BLEND_IN, FALSE, TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDsnatch, FALSE)
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDsnatch, FALSE)
				initialSnatchCut = waitForVisual
			BREAK
			CASE waitForVisual
				FLOAT fDistToScene
				FLOAT fStartPhase
				fDistToScene = VDIST(GET_ENTITY_COORDS(pedVictim, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) < 0.75
					AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedVictim, FALSE)+<<0,0,-1.5>>, 3)
					AND fDistToScene > 20.0
					AND fDistToScene < 120.0
						PRINTLN("Start ohase to skip to is ", fStartPhase)
						fDistToScene -= 20
						fStartPhase = CLAMP(1.0 - (fDistToScene/100), 0, 0.75)
					ELSE
						IF fStartPhase > GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch)
							SET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch, fStartPhase)
						ENDIF
						initialSnatchCut = waitForEnd
					ENDIF
				ELSE
					PRINTLN("SET_EVERYONE_IN_VAN() 1")
					SET_EVERYONE_IN_VAN()
					RETURN TRUE
				ENDIF
			BREAK
			CASE waitForEnd			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDsnatch)			
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.3
						IF bSnatchDialoguePlayed
							IF NOT bVictimScreams
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_HELP", CONV_PRIORITY_AMBIENT_HIGH, SET_SUB_DISPLAY_BASED_ON_DISTANCE())
									bVictimScreams = TRUE	
								ENDIF
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_SNCH", CONV_PRIORITY_AMBIENT_HIGH, SET_SUB_DISPLAY_BASED_ON_DISTANCE())
								bSnatchDialoguePlayed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bVanFrozen
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.4
							bVanFrozen = FALSE
							FREEZE_ENTITY_POSITION(vehKidnap, FALSE)
							SET_VEHICLE_TYRES_CAN_BURST(vehKidnap, TRUE)
							TASK_VEHICLE_TEMP_ACTION(pedDriver, vehKidnap, TEMPACT_BURNOUT, 10000)
						ENDIF
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDsnatch) > 0.735
						IF IS_VEHICLE_DRIVEABLE(vehKidnap)
							PRINTLN("SET_EVERYONE_IN_VAN() 2")
							SET_EVERYONE_IN_VAN()
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehKidnap)
						PRINTLN("SET_EVERYONE_IN_VAN() 3")
						SET_EVERYONE_IN_VAN()
						RETURN TRUE
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_EVERYONE()
	INT i
	REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)
	REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 2)
	REPEAT 4 i
		IF NOT IS_PED_INJURED(pedBackup[i])
			DELETE_PED(pedBackup[i])
		ENDIF
		IF DOES_BLIP_EXIST(blipBackup[i])
			REMOVE_BLIP(blipBackup[i])
		ENDIF
	ENDREPEAT
	IF NOT IS_PED_INJURED(pedKidnap)
		DELETE_PED(pedKidnap)
	ENDIF
	IF NOT IS_PED_INJURED(pedDriver)
		DELETE_PED(pedDriver)
	ENDIF
	IF NOT IS_PED_INJURED(pedPassenger)
		DELETE_PED(pedPassenger)
	ENDIF
	IF NOT IS_PED_INJURED(pedVictim)
		DELETE_PED(pedVictim)
	ENDIF
	IF DOES_ENTITY_EXIST(vehKidnap)
		DELETE_VEHICLE(vehKidnap)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike[0])
		DELETE_VEHICLE(vehBike[0])
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike[1])
		DELETE_VEHICLE(vehBike[1])
	ENDIF
	IF DOES_BLIP_EXIST(blipShooter[0])
		REMOVE_BLIP(blipShooter[0])
	ENDIF
	IF DOES_BLIP_EXIST(blipShooter[1])
		REMOVE_BLIP(blipShooter[1])
	ENDIF
	IF DOES_BLIP_EXIST(blipScene)
		REMOVE_BLIP(blipScene)
	ENDIF
	IF DOES_BLIP_EXIST(blipKidnap)
		REMOVE_BLIP(blipKidnap)
	ENDIF
	IF DOES_BLIP_EXIST(blipVictim)
		REMOVE_BLIP(blipVictim)
	ENDIF
	IF DOES_BLIP_EXIST(blipDriver)
		REMOVE_BLIP(blipDriver)
	ENDIF
	IF DOES_BLIP_EXIST(blipPassenger)
		REMOVE_BLIP(blipPassenger)
	ENDIF
	IF DOES_BLIP_EXIST(blipKidnap)
		REMOVE_BLIP(blipKidnap)
	ENDIF
	REMOVE_ALTRUIST_CULT_BLIP()
	DELETE_SURVIVING_LOST()
ENDPROC

PROC WARP_PLAYER_IN_CAR(VECTOR vWarp, FLOAT fWarp)
	VEHICLE_INDEX tempCar
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vWarp) 
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fWarp)
			ENDIF
		ELSE
			//GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, tempModel, iClass)
			REQUEST_MODEL(SENTINEL)
			WHILE NOT HAS_MODEL_LOADED(SENTINEL)
			WAIT(0)
			ENDWHILE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarp)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarp)
				tempCar = CREATE_VEHICLE(SENTINEL, vWarp, fWarp)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), tempCar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(tempCar)
				SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL)
			ENDIF
		ENDIF
	ENDIF
	LOAD_SCENE(vWarp)
ENDPROC

PROC SKIP_TO_DEBUG_STAGE()

ENDPROC

PROC RUN_DEBUG()
	#IF IS_DEBUG_BUILD 
	    // Event Passed
	    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
	        missionPassed()
	    ENDIF
	    // Jump to end cut
	    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(vehKidnap)
	    ENDIF
	    // Event Failed
	    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
	        missionCleanup()
	    ENDIF
		
		// Quick hack to kill driver and front passenger for code test of synched scene. PD
		IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iSkipToThisStage)
				SKIP_TO_DEBUG_STAGE()	
		    ENDIF
		ENDIF
		
		IF bTestCam
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				DESTROY_ALL_CAMS()
				vStartPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_WORLD_POSITION_OF_ENTITY_BONE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), "seat_dside_f")), GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), vCamOffsetPos)
				vStartRot.z = GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))-90
				IF vStartRot.z < 0 
					vStartRot.z += 360
				ENDIF
				camStart = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vStartPos, vStartRot, 40.000000)
	            camEnd = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vStartPos, vStartRot, 50.000000)
	            
	            SET_CAM_ACTIVE_WITH_INTERP(camEnd, camStart, 10000)
	            
	            RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				DESTROY_ALL_CAMS()
			ENDIF
			bTestCam = FALSE
		ENDIF
	#ENDIF
ENDPROC

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD
	sSkipMenu[Z_SKIP_CAUGHT_VAN].sTxtLabel 	= "Fight with Lost in van."     
	sSkipMenu[Z_SKIP_LOST_HQ].sTxtLabel 	= "Fight with Lost at HQ."  
	sSkipMenu[Z_SKIP_LOST_CHASE].sTxtLabel 	= "Chased by Lost bikes."     
	sSkipMenu[Z_SKIP_ALTRUIST].sTxtLabel 	= "Take girl to Altruists with Trevor."     
	sSkipMenu[Z_SKIP_ENDCUT].sTxtLabel 		= "End cutscene in vehicle."
	sSkipMenu[Z_SKIP_ENDCUT_FOOT].sTxtLabel = "End cutscene on foot."
	#ENDIF
ENDPROC

PROC GO_TO_CONV(SNATCHED_CONVERSATIONS_ENUM newConv)
	iLastConvFinish = GET_GAME_TIMER()
	eDriveHomeConv = newConv
	iConvSubStage = 0
ENDPROC

FUNC BOOL CAN_PLAYER_AND_VICTIM_CHAT()//BOOL bIgnoreAcitveConv = FALSE)
	IF NOT IS_PED_INJURED(pedVictim)
//	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		AND IS_PED_IN_VEHICLE(pedVictim, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			RETURN TRUE
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
		AND NOT IS_PED_RAGDOLL(pedVictim)
			IF VDIST2(GET_ENTITY_COORDS(pedVictim, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 400
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER(STRING sStub, STRING sSuffix = NULL)
	TEXT_LABEL_23 sTemp
	sTemp += sStub
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		sTemp += "M"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		sTemp += "F"
	ELSE
		sTemp += "T"
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sSuffix)
		sTemp += sSuffix
	ENDIF
	IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, sTemp, CONV_PRIORITY_HIGH)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MANAGE_CONVERSATION(STRING Label, BOOL bCanRun)

	IF bCanRun
	
		IF NOT bStarted
			IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, label, CONV_PRIORITY_HIGH)
				PRINTLN(label, " started")
				bStarted = TRUE
				bOnHold = FALSE
			ENDIF
		ELSE
			IF bOnHold
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(dialogueStruct, sConvBlock, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
					PRINTLN(label, " resumed")
					bOnHold = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bStarted
		AND NOT bOnHold
		AND bCanRun
			IF iConvTimer = -1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTLN(label, " set timer for finish")
					iConvTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF GET_GAME_TIMER() - iConvTimer > 500
					PRINTLN(label, " done")
					iConvTimer = -1
					bStarted = FALSE
					bOnHold = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	
		IF bStarted
		AND NOT bOnHold
			PRINTLN(label, " on hold")
			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			bOnHold = TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SHIFT_PLAYERS_CAR()

	VECTOR tempArea, vPlayersVeh
	FLOAT fPlayersVeh
	INT iNoOfLanes
	
	tempArea = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<0,-6,0>>)

	IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
		PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())")
		IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), tempArea, <<5,5,5>>)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " - PLAY_FULL_EXIT_SCENE, IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), tempArea")
//			WHILE NOT GET_POSITION_FOR_PLAYERS_CAR(tempArea, vPlayersVeh, fPlayersVeh)
//				WAIT(0)
//			ENDWHILE
			GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_ENTITY_COORDS(PLAYER_PED_ID()), iPlayerCarNodeNumber, vPlayersVeh, fPlayersVeh, iNoOfLanes, NF_INCLUDE_SWITCHED_OFF_NODES)
			PRINTLN("players car set to ", vPlayersVeh, fPlayersVeh)
			SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), fPlayersVeh)
			SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), vPlayersVeh)
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_DRIVE_HOME_DIALOGUE()

	IF NOT bJacked[0]
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedVictim, FALSE)) < 20*20
			IF IS_PED_JACKING(PLAYER_PED_ID())
				bJacked[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bUnsuitable[0]
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedVictim, FALSE)) < 20*20
		AND GET_GAME_TIMER() - iLastUnsuitable > 30000
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF NOT DOES_VEHICLE_HAVE_FREE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						bUnsuitable[0] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bUnsuitable[1]
			IF CAN_PLAYER_AND_VICTIM_CHAT()
				iLastUnsuitable = GET_GAME_TIMER()
				bUnsuitable[0] = FALSE
				bUnsuitable[1] = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bStopped[0]
		IF CAN_PLAYER_AND_VICTIM_CHAT()
			IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1
				fStoppedTime += GET_FRAME_TIME()
				IF fStoppedTime > 30.0
					bStopped[0] = TRUE
				ENDIF
			ELSE
				IF fStoppedTime > 0
					fStoppedTime -= GET_FRAME_TIME()*10
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bStopped[1]
			fStoppedTime = 0
			bStopped[0] = FALSE
			bStopped[1] = FALSE
		ENDIF
	ENDIF
	
	IF NOT bNoVeh[0]
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
			IF NOT DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
				fNoVehTimer += GET_FRAME_TIME()
				IF fNoVehTimer > 15.0
					bNoVeh[0] = TRUE
				ENDIF
			ENDIF
		ELSE
			IF fNoVehTimer > 0
				fNoVehTimer -= GET_FRAME_TIME()*10
			ENDIF
		ENDIF
	ENDIF
	
	IF 	(NOT bStopped[0] 	OR bStopped[0] 		AND bStopped[1]		)
	AND (NOT bJacked[0] 	OR bJacked[0] 		AND bJacked[1]		)
	AND (NOT bUnsuitable[0] OR bUnsuitable[0] 	AND bUnsuitable[1]	)
	AND (NOT bNoVeh[0] 		OR bNoVeh[0] 		AND bNoVeh[1]		)
	
		SWITCH eDriveHomeConv
		
			CASE RESNA_TAKE_ME_HOME
			
				SWITCH iConvSubStage
				
					CASE 0 
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iConvSubStage++
						ENDIF
					BREAK
					
					CASE 1
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_HOME", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								iConvSubStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_RESP")
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 3
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF GET_RANDOM_BOOL()
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_ADD", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
									iConvSubStage++
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_ADD2", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							GO_TO_CONV(RESNA_THANKS_FOR_STEPPING_IN)
						ENDIF
					BREAK
					
				ENDSWITCH
				
			BREAK
			
			CASE RESNA_THANKS_FOR_STEPPING_IN
			
				IF GET_GAME_TIMER() - iLastConvFinish > 15000
				
					SWITCH iConvSubStage
					
						CASE 0
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_BANT", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 2
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								IF MANAGE_CONVERSATION("RESNA_B1T", CAN_PLAYER_AND_VICTIM_CHAT())
									iConvSubStage++
								ENDIF
							ELSE
								IF CAN_PLAYER_AND_VICTIM_CHAT()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B1")
										iConvSubStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 3
							IF MANAGE_CONVERSATION("RESNA_B2", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						 
						CASE 4
						 	IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B2")
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 5
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								GO_TO_CONV(RESNA_WHY_GET_INVOLVED)
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
			BREAK
			
			CASE RESNA_WHY_GET_INVOLVED
			
				IF GET_GAME_TIMER() - iLastConvFinish > 15000
					
					SWITCH iConvSubStage
						
						CASE 0
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B4")
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF MANAGE_CONVERSATION("RESNA_B4", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 2
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B4", "b")
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 3
							IF MANAGE_CONVERSATION("RESNA_B5b", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 4
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								GO_TO_CONV(RESNA_CELLPHONE)
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
			BREAK
			
			CASE RESNA_HANDLED_YOURSELF
			
				IF GET_GAME_TIMER() - iLastConvFinish > 15000
					
					SWITCH iConvSubStage
					
						CASE 0
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_B5", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CAN_PLAYER_AND_VICTIM_CHAT()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B5")
										iConvSubStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF MANAGE_CONVERSATION("RESNA_B5b", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 3
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								GO_TO_CONV(RESNA_CELLPHONE)
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
			BREAK
			
			CASE RESNA_THATS_IT_IM_DONE
			
				IF GET_GAME_TIMER() - iLastConvFinish > 15000
				
					SWITCH iConvSubStage
					
						CASE 0
							IF MANAGE_CONVERSATION("RESNA_B6", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 1
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B6")
									iConvSubStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF MANAGE_CONVERSATION("RESNA_RUNOUT", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 3
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								GO_TO_CONV(RESNA_CELLPHONE)
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
			BREAK
			
			CASE RESNA_CELLPHONE
			
				IF iConvSubStage > 1
				AND iConvSubStage < 3
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
						TASK_USE_MOBILE_PHONE(pedVictim, TRUE)
					ENDIF
				ELSE
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(pedVictim)
						TASK_USE_MOBILE_PHONE(pedVictim, FALSE)
					ENDIF
				ENDIF
				
				SWITCH iConvSubStage
				
					CASE 0
						IF GET_GAME_TIMER() - iLastConvFinish > 10000
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_CELL1", CONV_PRIORITY_HIGH)
									SET_PED_CAN_PLAY_GESTURE_ANIMS(pedVictim, FALSE)
									SET_PED_CAN_PLAY_AMBIENT_ANIMS(pedVictim, FALSE)
									SET_PED_CAN_PLAY_VISEME_ANIMS(pedVictim, FALSE)
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
						TEXT_LABEL_23 sTemp
						sTemp = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						IF ARE_STRINGS_EQUAL( sTemp , "RESNA_CELL1_3")
							iConvSubStage++
						ENDIF
					BREAK
					
					CASE 2
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iLastConvFinish = GET_GAME_TIMER()
							iConvSubStage++
						ENDIF
					BREAK
					
					CASE 3
						IF GET_GAME_TIMER() - iLastConvFinish > 2000
							IF CAN_PLAYER_AND_VICTIM_CHAT()
							AND NOT IS_MOBILE_PHONE_TO_PED_EAR(pedVictim)
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_CELLR")
									SET_PED_CAN_PLAY_GESTURE_ANIMS(pedVictim, TRUE)
									SET_PED_CAN_PLAY_AMBIENT_ANIMS(pedVictim, TRUE)
									SET_PED_CAN_PLAY_VISEME_ANIMS(pedVictim, TRUE)
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_CELLD", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								iConvSubStage++
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
			BREAK
			
			CASE RESNA_LETS_GET_OUT_OF_HERE
			
				SWITCH iConvSubStage
				
					CASE 0
						IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_BANT2", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
							iConvSubStage++
						ENDIF
					BREAK
					
					CASE 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B3")
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_B3B", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								iConvSubStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 3
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B3B")
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_B3C", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								iConvSubStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 5
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CAN_PLAYER_AND_VICTIM_CHAT()
								IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B3C")
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 6
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_B3D", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								iConvSubStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 7
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iConvSubStage++
						ENDIF
					BREAK
					
					CASE 8
						IF CAN_PLAYER_AND_VICTIM_CHAT()
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PLAY_SINGLE_LINE_FOR_SPECIFIC_CHARACTER("RESNA_B3D")
										iConvSubStage++
									ENDIF
								ENDIF
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								IF MANAGE_CONVERSATION("RESNA_B3DM", CAN_PLAYER_AND_VICTIM_CHAT())
									iConvSubStage++
								ENDIF
							ELSE
								IF MANAGE_CONVERSATION("RESNA_B3DF", CAN_PLAYER_AND_VICTIM_CHAT())
									iConvSubStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 9
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							GO_TO_CONV(RESNA_CONVERSATION_CAP)
						ENDIF
					BREAK
					
				ENDSWITCH
				
			BREAK
			
			CASE RESNA_CONVERSATION_CAP
			
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				AND GET_GAME_TIMER() - iLastConvFinish > 15000
				
					SWITCH iConvSubStage
					
						CASE 0
							IF READY_FOR_CULT_DIALOGUE(vEndLocation)
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 1
							IF MANAGE_CONVERSATION("RESNA_TREV", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
								iLastConvFinish = GET_GAME_TIMER()
							ENDIF
						BREAK
					
						CASE 2
							IF READY_FOR_CULT_DIALOGUE(vEndLocation)
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 3
							IF MANAGE_CONVERSATION("RESNA_CULT", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
								iLastConvFinish = GET_GAME_TIMER()
							ENDIF
						BREAK
						
						CASE 4
							IF READY_FOR_CULT_DIALOGUE(vEndLocation)
							AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vCult) < 250
								iConvSubStage++
							ENDIF
						BREAK
						
						CASE 5
							IF MANAGE_CONVERSATION("RESNA_NEAR", CAN_PLAYER_AND_VICTIM_CHAT())
								iConvSubStage++
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ELSE
	
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
			IF bStarted
			AND NOT bOnHold
				sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
				KILL_FACE_TO_FACE_CONVERSATION()
				bOnHold = TRUE
				PRINTLN("Setting conversation on hold for special conversation at line ", sConvResumeLabel)
			ENDIF
			
		ELSE
			
			IF bJacked[0]
			AND NOT bJacked[1]
				PRINTLN("Waiting to play jacked line")
				IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_JACK", CONV_PRIORITY_HIGH)
					bJacked[1] = TRUE
				ENDIF
			ENDIF
			
			IF bNoVeh[0]
			AND NOT bNoVeh[1]
				PRINTLN("Waiting to play no vehicle line")
				IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_NOVEH", CONV_PRIORITY_HIGH)
					bNoVeh[1] = TRUE
				ENDIF
			ENDIF
			
			IF bStopped[0]
			AND NOT bStopped[1]
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 3
					PRINTLN("Waiting to play stopped line")
					IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_STOP", CONV_PRIORITY_HIGH)
						bStopped[1] = TRUE
					ENDIF
				ELSE
					bStopped[0] = FALSE
				ENDIF
			ENDIF
			
			IF bUnsuitable[0]
			AND NOT bUnsuitable[1]
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PED_IN_ANY_VEHICLE(pedVictim)
				AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					STRING sVehSpeak
					MODEL_NAMES vehModel 
					vehModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_THIS_MODEL_A_BIKE(vehModel)
					OR IS_THIS_MODEL_A_BICYCLE(vehModel)
					OR IS_THIS_MODEL_A_QUADBIKE(vehModel)
						sVehSpeak = "RESNA_VEH1"
					ELSE
						sVehSpeak = "RESNA_VEH2"
					ENDIF
					PRINTLN("Waiting to play unsuitable vehicle line")
					IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, sVehSpeak, CONV_PRIORITY_HIGH)
						bUnsuitable[1] = TRUE
					ENDIF
				ELSE
					bUnsuitable[0] = FALSE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

// Clean Up

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	fInput = in_coords.headings[0]

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedVictim)
			IF IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
				REMOVE_PED_FROM_GROUP(pedVictim)
				TASK_WANDER_STANDARD(pedVictim)			
				SET_PED_COMBAT_ATTRIBUTES(pedVictim, CA_ALWAYS_FLEE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_USE_VEHICLE, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
				SET_PED_KEEP_TASK(pedVictim, TRUE)
			ELSE
				IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, animDict, "IG_1_ALT1_GIRL_IN_VAN_LOOP")
					SET_PED_SUFFERS_CRITICAL_HITS(pedVictim, TRUE)
					TASK_SMART_FLEE_COORD(pedVictim, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 150, -1)
					SET_PED_KEEP_TASK(pedVictim, TRUE)
				ENDIF
			ENDIF
		ENDIF
		PRINTLN("mission cleanup 4") 
	    missionCleanup()
	ENDIF
	
	//----
	
	IF CAN_RANDOM_EVENT_LAUNCH(vInput)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_PARK_PERPENDICULAR_NOSE_IN", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
	
	SETUP_DEBUG()
	
	bShowHeightOnBlip = FALSE
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
	
		WAIT(0)
		
		IF NOT bAreasBlocked
			SET_BLOCKING_AREAS(TRUE)
			bAreasBlocked = TRUE
		ENDIF
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				PRINTLN("cleanup 1")
				missionCleanup()
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_SN")
	    RUN_DEBUG()
		SET_SUB_DISPLAY_BASED_ON_DISTANCE()
		RUN_DRAG_INTO_VAN_SCENE()
		
	    IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		
	        IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			
	            SWITCH ambStage
				
	                CASE ambCanRun
					
	                    IF LOADED_AND_CREATED_KIDNAP_SCENE()
							SET_MAX_WANTED_LEVEL(0)
	                        ambStage = (ambRunning)
	                    ELSE
							IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
								PRINTLN("cleanup 2")
								missionCleanup()
							ENDIF
						ENDIF
						
	                BREAK
					
	                CASE ambRunning
											
						SET_UP_ANIMS()
						
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	                    AND NOT IS_PED_INJURED(pedKidnap)
	                    AND NOT IS_PED_INJURED(pedVictim)
						
							IF NOT DOES_BLIP_EXIST(blipDriver)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedVictim, FALSE) - <<0,0,7>>, <<142.000000,83.750000,35.250000>>)
									IF DOES_BLIP_EXIST(blipScene)
			                           	REMOVE_BLIP(blipScene)
									ENDIF
									IF NOT DOES_BLIP_EXIST(blipDriver)
										FLASH_MINIMAP_DISPLAY()
	                                	blipDriver = CREATE_AMBIENT_BLIP_FOR_PED(pedDriver, TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							IF eSnatchSceneStage = DIV_WAIT
								// Check for player interfering in the scene
								IF NOT IS_ENTITY_OCCLUDED(vehKidnap)
								OR NOT IS_ENTITY_OCCLUDED(pedKidnap)
								OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<40, 40, 20>>)
									SETTIMERB(0)
									eSnatchSceneStage = DIV_GRABBING_GIRL
								ENDIF
							ENDIF
							
							IF eSnatchSceneStage = DIV_FINISHED
								SET_WANTED_LEVEL_MULTIPLIER(0)
								SET_VEHICLE_TYRES_CAN_BURST(vehKidnap, TRUE)
								SET_RANDOM_EVENT_ACTIVE(FALSE)
							ELSE
								IF NOT bTrevorLost
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vInput) < 40
										AND DOES_ENTITY_EXIST(vehKidnap)
										AND NOT IS_ENTITY_OCCLUDED(vehKidnap)
											IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_LOST", CONV_PRIORITY_HIGH)
												bTrevorLost = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
	                        ENDIF
							
						ELSE
						
							CHECK_FOR_PLAYER_AGGROING_GIRL()
							CHECK_FOR_PLAYER_AGGROING_VAN()
							
	                    ENDIF
						
	                BREAK
					
	            ENDSWITCH
				
	        ELSE
				PRINTLN("IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()")
				missionCleanup()
	        ENDIF
			
	    ELSE
		
			IF NOT IS_PED_INJURED(pedVictim)
			
	        	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<150,150,50>>)
				OR (IS_VEHICLE_DRIVEABLE(vehKidnap) AND NOT IS_ENTITY_OCCLUDED(vehKidnap))
				
	                SWITCH snatchedStage
					
	                    CASE waitingForSnatch
							REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	                    	IF IS_VEHICLE_DRIVEABLE(vehKidnap)
	                        AND NOT IS_PED_INJURED(pedDriver)
							
	                            IF IS_PED_IN_VEHICLE(pedDriver, vehKidnap)
								
									IF IS_ENTITY_ATTACHED(pedVictim)
									AND GET_SCRIPT_TASK_STATUS(pedDriver, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
									
										//SET_VEHICLE_CUSTOM_PATH_NODE_STREAMING_RADIUS(vehKidnap, VDIST(vInput, vGangHouse) + 200)
										
										TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
										OPEN_SEQUENCE_TASK(seq)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, vWaypoint, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<-859.6284, 382.7891, 86.4466>>, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<-703.2946, 287.2290, 82.6848>>, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<-496.6651, 246.5268, 82.0747>>, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<-166.6776, 250.8786, 92.4768>>, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<74.5106, 236.4861, 108.1344>> , 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<278.7988, 161.9029, 103.4460>>, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<425.6758, 108.9366, 99.4329>> , 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, <<729.2792, -19.6160, 81.9823>> , 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehKidnap, vGangHouse, 50, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 7, 15)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedDriver, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
	                                    snatchedStage = kidnapperIsGettingAway
										
									ENDIF
									
	                            ENDIF
								
							ELSE
							
								DRIVER_ABANDONS_VAN()
								snatchedStage = kidnapperIsGettingAway
								
	                        ENDIF
							
	                    BREAK
	                    
	                    CASE kidnapperIsGettingAway
							REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
							#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
									IF DOES_ENTITY_EXIST(vehKidnap)
										IF NOT IS_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_FRONT_LEFT)
											SET_VEHICLE_TYRE_BURST(vehKidnap, SC_WHEEL_CAR_FRONT_LEFT, TRUE)
										ENDIF
									ENDIF
								ENDIF
							#ENDIF
							
							ALLOW_PASSENGER_TO_FINISH_CLOSING_DOOR()
	                        RUN_HINT_CAM()
							UPDATE_CHASE_BLIP(blipDriver, pedKidnap, 150)
							
	                        IF IS_VAN_TYRE_BURST()
	                        OR HAS_VEHICLE_TAKEN_ENOUGH_DAMAGE()
	                        OR HAS_STOLEN_CAR_BEEN_BUMPED_ENOUGH()
							OR HAS_VEHICLE_REACHED_GANG_HOUSE()
							OR HAS_PLAYER_KILLED_A_KIDNAPPER()
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedVictim)
								IF DOES_BLIP_EXIST(blipDriver)
									IF GET_BLIP_COLOUR(blipDriver) != BLIP_COLOUR_RED
										SET_BLIP_AS_FRIENDLY(blipDriver, FALSE)
										SET_BLIP_COLOUR(blipDriver, BLIP_COLOUR_RED)
									ENDIF
								ENDIF
	//							IF NOT IS_PED_INJURED(pedVictim)
	//								SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
	//							ENDIF
	                            DRIVER_ABANDONS_VAN()
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	                            snatchedStage = fightKidnappers
							ELSE
								IF bSetDriveby
								OR bLostSpeedUp
									IF DOES_BLIP_EXIST(blipDriver)
										IF GET_BLIP_COLOUR(blipDriver) != BLIP_COLOUR_RED
											SET_BLIP_AS_FRIENDLY(blipDriver, FALSE)
											SET_BLIP_COLOUR(blipDriver, BLIP_COLOUR_RED)
										ENDIF
									ENDIF
								ELSE
									IF IS_VEHICLE_DRIVEABLE(vehKidnap)
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehKidnap, <<25,25,5>>)
										AND GET_ENTITY_SPEED(vehKidnap) > 10
											IF GET_GAME_TIMER() > iTimeInRange+5000
												PRINTLN(GET_THIS_SCRIPT_NAME(), " - bLostSpeedUp Player was within 25m of Thief for 5s.")
												bLostSpeedUp = TRUE
											ENDIF
										ELSE
											iTimeInRange = GET_GAME_TIMER()
										ENDIF
									ENDIF
//									REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vMinAreaForNodes.x, vMinAreaForNodes.y, vMaxAreaForNodes.x, vMaxAreaForNodes.y)
									//FLASH_RANDOM_EVENT_DECISION_BLIP(blipDriver, iFlashTimestamp)
								ENDIF
	                        ENDIF
							
	                    BREAK
						
						CASE fightKidnappers
							REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
							IF NOT bShowHeightOnBlip
								bShowHeightOnBlip = TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
									IF DOES_ENTITY_EXIST(pedDriver)
										EXPLODE_PED_HEAD(pedDriver)
									ENDIF
									IF DOES_ENTITY_EXIST(pedKidnap)
										EXPLODE_PED_HEAD(pedKidnap)
									ENDIF
									IF DOES_ENTITY_EXIST(pedPassenger)
										EXPLODE_PED_HEAD(pedPassenger)
									ENDIF
								ENDIF
							#ENDIF
							
							IF DOES_ENTITY_EXIST(vehKidnap)
							AND DOES_ENTITY_EXIST(pedVictim)
								IF IS_ENTITY_ATTACHED_TO_ENTITY(pedVictim, vehKidnap)
									IF IS_ENTITY_DEAD(vehKidnap)
										SET_ENTITY_INVINCIBLE(pedVictim, FALSE)
										DETACH_ENTITY(pedVictim, TRUE, FALSE)
									ENDIF
								ENDIF
							ENDIF
							
							IF bCreateBackupAtGangHouse
								IF bBackupCreated
									CHECK_AND_REMOVE_BACKUP_BLIPS()
								ELSE
									CREATE_BACKUP_SHOOTERS()
								ENDIF
							ELSE
								bBackupDead = TRUE
							ENDIF
							
							CHECK_FOR_PLAYER_AGGROING_GIRL()
							CHECK_AND_REMOVE_KIDNAPPER_BLIPS()
							
							IF bBackupDead
								IF bKidnappersDead
									CHECK_AND_REMOVE_KIDNAPPER_BLIPS()
									snatchedStage = rescueGirl
									//url:bugstar:1995954
									SET_ENTITY_INVINCIBLE(pedVictim, FALSE)
								ENDIF
							ENDIF
							
						BREAK
						
						CASE rescueGirl
						
							IF NOT DOES_BLIP_EXIST(blipVictim)
								blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
							ENDIF
							
							SWITCH iExitSceneStage
							
								CASE 0
								
									IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
										IF NOT DOES_ENTITY_EXIST(vehShift)
											vehShift = GET_PLAYERS_LAST_VEHICLE()
											SET_ENTITY_AS_MISSION_ENTITY(vehshift, TRUE, TRUE)
										ENDIF
									ELSE
										PRINTLN("CANNOT GET PLAYERS LAST VEH")
									ENDIF
									
									IF CAN_PLAYER_START_CUTSCENE()
									AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<2.5,2.5,2.5>>, FALSE, TRUE, TM_ON_FOOT)
										
										//url:bugstar:1995954
										CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50)
										SET_PED_CONFIG_FLAG(pedVictim, PCF_RunFromFiresAndExplosions, FALSE)
										
										KILL_ANY_CONVERSATION()
										
										PRINTLN(" PLAYING FAST EXIT ANIM ")
										
										SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										HIDE_HUD_AND_RADAR_THIS_FRAME()
										exitVanCam.StartCamPos = <<-1.2232, -4.3354, 1.3385>>
										IF NOT IS_ENTITY_DEAD(vehKidnap)		
											STOP_SYNCHRONIZED_ENTITY_ANIM(vehKidnap, NORMAL_BLEND_OUT, TRUE)
											FREEZE_ENTITY_POSITION(vehKidnap, FALSE)
											SET_VEHICLE_DOORS_LOCKED(vehKidnap, VEHICLELOCK_UNLOCKED)
										
											exitVanCam.StartCamRot = <<-22.8598, -0.0000, GET_ENTITY_HEADING(vehKidnap)-60>>
										ENDIF
										exitVanCam.EndCamPos = <<-1.4334, -4.6656, 1.3733>>
										exitVanCam.EndCamRot = <<-22.8598, -0.0000, -106.3162>>
										exitVanCam.CamFov = 55
										exitVanCam.ShotTime = 3000
										exitVanCam.AttachToEntity = FALSE
										Run_camera_Attach_Entity_Static(exitVanCam, vehKidnap)
										
										SET_ENTITY_COORDS(pedVictim, GET_ENTITY_COORDS(vehKidnap, FALSE))
										scenePosition = <<0,0,0>>
										sceneRotation = <<0,0,0>>
										sceneIDexit = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
										
										IF IS_ENTITY_ATTACHED(pedVictim)
											DETACH_ENTITY(pedVictim, TRUE, FALSE)
										ENDIF
										
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDexit, vehKidnap, GET_ENTITY_BONE_INDEX_BY_NAME(vehKidnap, chassisBone))
										SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDexit, scenePosition, sceneRotation)
										TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), -1)
										//url:bugstar:1995954 - Stopping interruption on synced scene.
										TASK_SYNCHRONIZED_SCENE(pedVictim, sceneIDexit, animDict, "IG_1_ALT1_EXIT_VAN", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDexit, FALSE)
										SET_PED_KEEP_TASK(pedVictim, TRUE)
										//PLAY_SYNCHRONIZED_ENTITY_ANIM(vehKidnap, sceneIDexit, "IG_1_ALT1_EXIT_VAN_BURR", animDict, SLOW_BLEND_IN)
										exitCam = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
										PLAY_SYNCHRONIZED_CAM_ANIM(exitCam, sceneIDexit, "IG_1_ALT1_EXIT_VAN_CAM", animDict)
										FREEZE_ENTITY_POSITION(vehKidnap, TRUE)
										SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_LEFT)
										SET_VEHICLE_DOOR_OPEN(vehKidnap, SC_DOOR_REAR_RIGHT)
										scenePosition = GET_ENTITY_COORDS(vehKidnap, FALSE)
										sceneRotation.z = GET_ENTITY_HEADING(vehKidnap)
										
										VECTOR tempArea
										tempArea = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehKidnap, <<0,-4.7,0>>)
										GET_GROUND_Z_FOR_3D_COORD(tempArea, tempArea.Z)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(vehKidnap))
										SET_ENTITY_COORDS(PLAYER_PED_ID(), tempArea)
										
										IF DOES_PLAYER_CAR_NEED_SHIFTING_FOR_RESCUE(vehShift)
											SHIFT_PLAYERS_CAR()
										ENDIF
										
										IF DOES_ENTITY_EXIST(pedKidnap)
											IF IS_ENTITY_DEAD(pedKidnap)
											AND VDIST(GET_ENTITY_COORDS(pedKidnap, FALSE), GET_ENTITY_COORDS(pedVictim, FALSE)) < 3
												DELETE_PED(pedKidnap)
											ENDIF
											IF IS_ENTITY_DEAD(pedPassenger)
											AND VDIST(GET_ENTITY_COORDS(pedPassenger, FALSE), GET_ENTITY_COORDS(pedPassenger, FALSE)) < 3
												DELETE_PED(pedPassenger)
											ENDIF
										ENDIF

										HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
										
										SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle)
										HIDE_HUD_AND_RADAR_THIS_FRAME()
										
										iExitSceneStage++
										
									ENDIF
									
								BREAK
								
								CASE 1
									IF DOES_ENTITY_EXIST(vehKidnap)
										iExitSceneStage++
									ENDIF
								BREAK	
								
								CASE 2
								
									STRING sTHXString
									
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										sTHXString = "RESNA_THX"
									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										sTHXString = "RESNA_THX1"
									ELSE
										sTHXString = "RESNA_THX2"
									ENDIF
									
									PRINTLN("THX LINE IS ", sTHXString)
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, sTHXString, CONV_PRIORITY_HIGH)
											iExitSceneStage++
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 3
								
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_THX3", CONV_PRIORITY_HIGH)
											iExitSceneStage++
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 4
								
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDexit)
										IF GET_SYNCHRONIZED_SCENE_PHASE (sceneIDexit) > 0.3746478801825015
										AND NOT bPushInToFirstPerson
											IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
												ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
												PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
												bPushInToFirstPerson = TRUE
											ENDIF
										ENDIF
										IF GET_SYNCHRONIZED_SCENE_PHASE (sceneIDexit) > 0.4
											iExitSceneStage++
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 5
								
									CLEAR_PED_TASKS(pedVictim)
									SET_ENTITY_INVINCIBLE(pedVictim, FALSE)
									//url:bugstar:1995954
									SET_PED_CONFIG_FLAG(pedVictim, PCF_RunFromFiresAndExplosions, TRUE)
									
									vEndPos = GET_ANIM_INITIAL_OFFSET_POSITION(animDict, "IG_1_ALT1_EXIT_VAN",  scenePosition, sceneRotation, 1)
									GET_GROUND_Z_FOR_3D_COORD(vEndPos, vEndPos.z)
					                SET_ENTITY_COORDS(pedVictim, vEndPos)
									
									vEndPos = GET_ANIM_INITIAL_OFFSET_ROTATION(animDict, "IG_1_ALT1_EXIT_VAN", scenePosition, sceneRotation, 1)
									SET_ENTITY_HEADING(pedVictim, vEndPos.z)
									
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDexit)
										SET_SYNCHRONIZED_SCENE_PHASE(sceneIDexit, 1)
									ENDIF
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
									HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
									
									IF IS_VEHICLE_DRIVEABLE(vehKidnap)
										FREEZE_ENTITY_POSITION(vehKidnap, FALSE)
										IF IS_ENTITY_PLAYING_ANIM(vehKidnap, animDict, "IG_1_ALT1_EXIT_VAN")
											STOP_ENTITY_ANIM(vehKidnap, "IG_1_ALT1_EXIT_VAN", animDict, NORMAL_BLEND_OUT)
										ENDIF
									ENDIF
									
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									
									IF DOES_CAM_EXIST(exitCam)
										DESTROY_CAM(exitCam)
									ENDIF
									
					                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					                SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					                SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
									
									SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
									READY_FOR_CULT_DIALOGUE(vEndLocation)
									
									IF IS_VEHICLE_DRIVEABLE(vehKidnap)
										SET_VEHICLE_DOORS_LOCKED(vehKidnap, VEHICLELOCK_UNLOCKED)
									ENDIF
									
									snatchedStage = driveHome
									
								BREAK
								
							ENDSWITCH
							
							IF iExitSceneStage > 0
							AND iExitSceneStage < 5
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDexit)
								OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDexit) AND GET_SYNCHRONIZED_SCENE_PHASE (sceneIDexit) > 0.4)
									iExitSceneStage = 5
								ENDIF
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(blipVictim)
								blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
							ENDIF
							
						BREAK
						
	                    CASE driveHome
						
							IF GET_MAX_WANTED_LEVEL() = 0
								SET_MAX_WANTED_LEVEL(5)
								SET_WANTED_LEVEL_MULTIPLIER(0)
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedDriver)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedDriver, FALSE)) > 10000
								AND IS_ENTITY_OCCLUDED(pedDriver)
									SET_PED_AS_NO_LONGER_NEEDED(pedDriver)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedPassenger)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedPassenger, FALSE)) > 10000
								AND IS_ENTITY_OCCLUDED(pedPassenger)
									SET_PED_AS_NO_LONGER_NEEDED(pedPassenger)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedKidnap)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedKidnap, FALSE)) > 10000
								AND IS_ENTITY_OCCLUDED(pedKidnap)
									SET_PED_AS_NO_LONGER_NEEDED(pedKidnap)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(vehKidnap)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehKidnap, FALSE)) > 10000
								AND IS_ENTITY_OCCLUDED(vehKidnap)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehKidnap)
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
									CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_HOME", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
								ENDIF
							#ENDIF
							
							MANAGE_DRIVE_HOME_DIALOGUE()
							
							IF DOES_BLIP_EXIST(blipVictim)
							
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<10,10,10>>)
									IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
										IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) != FINISHED_TASK
											CLEAR_PED_TASKS(pedVictim)
										ENDIF
										TASK_CLEAR_LOOK_AT(pedVictim)
										SET_PED_AS_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
										SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedVictim, VS_FRONT_RIGHT)
										REMOVE_BLIP(blipVictim)
										IF DOES_BLIP_EXIST(blipScene)
		                                	REMOVE_BLIP(blipScene)
										ENDIF
										ADD_ALTRUIST_CULT_BLIP()
										blipScene = CREATE_AMBIENT_BLIP_FOR_COORD(vEndLocation, TRUE)
									ENDIF
								ENDIF
								
							ELSE
							
								IF DOES_BLIP_EXIST(blipScene)
								
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vEndLocation, g_vAnyMeansLocate, TRUE)
									
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND IS_PED_IN_ANY_VEHICLE(pedVictim)
											IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
											AND IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
											AND IS_ENTITY_UPRIGHT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
												snatchedStage = carReturned
												REMOVE_ALTRUIST_CULT_BLIP()
												REMOVE_BLIP(blipScene)
											ENDIF
										ELSE
											snatchedStage = carReturned
											REMOVE_ALTRUIST_CULT_BLIP()
											REMOVE_BLIP(blipScene)
										ENDIF
										
									ELSE
									
										IF NOT IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
											REMOVE_ALTRUIST_CULT_BLIP()
											REMOVE_BLIP(blipScene)
											IF NOT DOES_BLIP_EXIST(blipVictim)
												blipVictim = CREATE_BLIP_FOR_PED(pedVictim)
											ENDIF
										ENDIF
										
										//throw way home dialogue in here
										IF NOT bExtraAttackersLaunched
										AND NOT bExtraAttackersDone
										
											CHECK_DISTANCE_TO_HOUSE_AND_CREATE_MORE_BAD_GUYS()
											iTimeOfAttack = GET_GAME_TIMER()
											
										ELSE
										
											IF NOT bExtraAttackersDone
										
												IF GET_GAME_TIMER() - iTimeOfAttack > 5000
													iTimeOfAttack = GET_GAME_TIMER() - 5000
												ENDIF
												
												MANAGE_CHASER_WARPING()
												
												INT i
												REPEAT COUNT_OF(vehBike) i
												
													IF HAS_BIKER_BEEN_SPOTTED(pedRider[i])
													OR HAS_BIKER_BEEN_SPOTTED(pedShooter[i])
													OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndLocation) < 400
													OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vCult) < 400
													
														IF DOES_BLIP_EXIST(blipScene)
															REMOVE_BLIP(blipScene)
														ENDIF
														
														REMOVE_ALTRUIST_CULT_BLIP()
														eSavedConv = eDriveHomeConv
														IF eSavedConv < RESNA_CONVERSATION_CAP
															IF iConvSubStage > 0
																eSavedConv = INT_TO_ENUM(SNATCHED_CONVERSATIONS_ENUM, ENUM_TO_INT(eSavedConv)+1)
															ENDIF
														ENDIF
														
														KILL_FACE_TO_FACE_CONVERSATION()
														bExtraAttackersDone = FALSE
														
														IF NOT IS_PED_INJURED(pedRider[i])
															SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedRider[i], KNOCKOFFVEHICLE_HARD)
														ENDIF
														
														IF NOT IS_PED_INJURED(pedShooter[i])
															SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedShooter[i], KNOCKOFFVEHICLE_HARD)
														ENDIF
														
														snatchedStage = attackedOnWayBack
														
													ENDIF
													
												ENDREPEAT
												
											ENDIF
											
										ENDIF
										
									ENDIF
									
								ENDIF
								
							ENDIF
							
							CHECK_FOR_PLAYER_AGGROING_GIRL()
							
							IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
								DELETE_PED(pedVictim)
								missionPassed()
							ENDIF
							
	                    BREAK
						
						CASE attackedOnWayBack
						
							IF GET_MAX_WANTED_LEVEL() != 0
								SET_MAX_WANTED_LEVEL(0)
								SET_WANTED_LEVEL_MULTIPLIER(0)
							ENDIF
							
							CHECK_FOR_PLAYER_AGGROING_GIRL()
							PLAY_BACKUP_DIALOGUE()
							MANAGE_CHASER_WARPING()
							
							IF NOT bPlayedBackupDialogue
								IF CAN_PLAYER_AND_VICTIM_CHAT()
									IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_MOR", CONV_PRIORITY_AMBIENT_HIGH, SET_SUB_DISPLAY_BASED_ON_DISTANCE())
										bPlayedBackupDialogue = TRUE
										iTimeOfMoreBikersDialogue = GET_GAME_TIMER()
									ENDIF
								ELSE
									bPlayedBackupDialogue = TRUE
								ENDIF
							ELSE
								IF GET_GAME_TIMER() - iTimeOfMoreBikersDialogue > 2000
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT bFinger
										IF CAN_PLAYER_AND_VICTIM_CHAT()
										AND GET_DISTANCE_TO_CLOSEST_BIKER() < 30
											IF NOT IS_PED_INJURED(pedVictim)
											AND NOT IS_PED_INJURED(pedRider[0])
												IF IS_PED_IN_ANY_VEHICLE(pedVictim)
													TASK_DRIVE_BY(pedVictim, pedRider[0], NULL, <<0,0,0>>, 50, 90, FALSE, FIRING_PATTERN_SINGLE_SHOT)
												ENDIF
												bFinger = TRUE
											ENDIF
										ENDIF
									ELSE
										IF NOT bFingerShout
											IF IS_PED_IN_ANY_VEHICLE(pedVictim)
												IF IS_PED_DOING_DRIVEBY(pedVictim)
													IF CREATE_CONVERSATION(dialogueStruct, sConvBlock, "RESNA_FINGER", CONV_PRIORITY_HIGH)
														bFingerShout = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							INT j
							REPEAT COUNT_OF(vehBike) j
							
								BOOL bBikeBlipNeeded
								bBikeBlipNeeded = FALSE
								
								IF DOES_ENTITY_EXIST(pedRider[j])
								AND NOT IS_PED_INJURED(pedRider[j])
								AND NOT IS_ENTITY_DEAD(pedRider[j])
								
									IF IS_VEHICLE_DRIVEABLE(vehBike[j])
									AND IS_PED_IN_VEHICLE(pedRider[j],  vehBike[j])
										IF DOES_BLIP_EXIST(blipRider[j])
											REMOVE_BLIP(blipRider[j])
										ENDIF
										bBikeBlipNeeded = TRUE
									ENDIF
									
									IF NOT DOES_BLIP_EXIST(blipRider[j])
									AND NOT IS_PED_IN_ANY_VEHICLE(pedRider[j])
										blipRider[j] = CREATE_BLIP_FOR_PED(pedRider[j], TRUE)
									ENDIF
									
								ELSE
								
									IF DOES_BLIP_EXIST(blipRider[j])
										REMOVE_BLIP(blipRider[j])
									ENDIF
									
								ENDIF
								
								IF DOES_ENTITY_EXIST(pedShooter[j])
								AND NOT IS_PED_INJURED(pedShooter[j])
								AND NOT IS_ENTITY_DEAD(pedShooter[j])
								
									IF IS_VEHICLE_DRIVEABLE(vehBike[j])
									AND IS_PED_IN_VEHICLE(pedShooter[j],  vehBike[j])
										IF DOES_BLIP_EXIST(blipShooter[j])
											REMOVE_BLIP(blipShooter[j])
										ENDIF
										bBikeBlipNeeded = TRUE
									ENDIF
									
									IF NOT DOES_BLIP_EXIST(blipShooter[j])
									AND NOT IS_PED_IN_ANY_VEHICLE(pedShooter[j])
										blipShooter[j] = CREATE_BLIP_FOR_PED(pedShooter[j], TRUE)
									ENDIF
									
								ELSE
								
									IF DOES_BLIP_EXIST(blipShooter[j])
										REMOVE_BLIP(blipShooter[j])
									ENDIF
									
								ENDIF
								
								MANAGE_CHASER_BLIPS(j, bBikeBlipNeeded AND IS_VEHICLE_DRIVEABLE(vehBike[j]))
								
							ENDREPEAT
							
							IF ARE_EXTRA_ATTACKERS_LOST_OR_DEAD()
							
								REPEAT 2 iTemp
									IF IS_VEHICLE_DRIVEABLE(vehBike[iTemp])
										SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBike[iTemp])
									ENDIF
									IF NOT IS_PED_INJURED(pedRider[iTemp])
										SET_PED_AS_NO_LONGER_NEEDED(pedRider[iTemp])
									ENDIF
									IF NOT IS_PED_INJURED(pedShooter[iTemp])
										SET_PED_AS_NO_LONGER_NEEDED(pedShooter[iTemp])
									ENDIF
									IF DOES_BLIP_EXIST(blipRider[iTemp])
										REMOVE_BLIP(blipRider[iTemp])
									ENDIF
									IF DOES_BLIP_EXIST(blipShooter[iTemp])
										REMOVE_BLIP(blipShooter[iTemp])
									ENDIF
									IF DOES_BLIP_EXIST(blipExtraBikes[iTemp])
										REMOVE_BLIP(blipExtraBikes[iTemp])
									ENDIF
									SET_MODEL_AS_NO_LONGER_NEEDED(modelBike)
									SET_MODEL_AS_NO_LONGER_NEEDED(modelKidnap)
									SET_MODEL_AS_NO_LONGER_NEEDED(modelDriver)
								ENDREPEAT
								
								ADD_ALTRUIST_CULT_BLIP()
								blipScene = CREATE_AMBIENT_BLIP_FOR_COORD(vEndLocation, TRUE)
								GO_TO_CONV(RESNA_LETS_GET_OUT_OF_HERE)
								bExtraAttackersDone = TRUE
								CLEAR_PED_TASKS(pedVictim)
								IF NOT IS_PED_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
									SET_PED_AS_GROUP_MEMBER(pedVictim, GET_PLAYER_GROUP(PLAYER_ID()))
								ENDIF								
								snatchedStage = driveHome
								
							ENDIF
							
						BREAK
						
	                    CASE carReturned
	                        IF HAS_VICTIM_BEEN_DROPPED_OFF()
								PRINTLN("passed here 2")
	                            missionPassed()
	                        ENDIF
	                    BREAK
						
	                ENDSWITCH
					
					IF DOES_BLIP_EXIST(blipScene)
						SHOW_HEIGHT_ON_BLIP(blipScene , bShowHeightOnBlip)
					ENDIF
					IF DOES_BLIP_EXIST(blipKidnap)
						SHOW_HEIGHT_ON_BLIP(blipKidnap , bShowHeightOnBlip)
					ENDIF
					IF DOES_BLIP_EXIST(blipVictim)
						SHOW_HEIGHT_ON_BLIP(blipVictim , bShowHeightOnBlip)
					ENDIF
					IF DOES_BLIP_EXIST(blipDriver)
						SHOW_HEIGHT_ON_BLIP(blipDriver , bShowHeightOnBlip)
					ENDIF
					IF DOES_BLIP_EXIST(blipPassenger)
						SHOW_HEIGHT_ON_BLIP(blipPassenger , bShowHeightOnBlip)
					ENDIF
					
	            ELSE
				
					IF IS_VEHICLE_DRIVEABLE(vehKidnap)
						DELETE_VEHICLE(vehKidnap)
					ENDIF
					PRINTLN("re_snatched - pedVictim is out of range.")
	               	missionCleanup()
					
	            ENDIF
				
	        ELSE
			
	            PRINTLN("re_snatched - pedVictim is dead.")
				ATTACK_PLAYER()
	            missionCleanup()
				
	        ENDIF
			
	    ENDIF
		
	ENDWHILE
	
ENDSCRIPT
