//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	CarTheft.sc													//
//		AUTHOR			:	Brenda Carey												//
//		DESCRIPTION		:	Someone's car has been stolen						 		//
//		DATE			:	26/07/2010											 		//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
///    
///    
///    
///    
///    

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES =====================================

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "commands_ped.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_path.sch"
USING "LineActivation.sch"
//USING "Minigame_private.sch"
USING "AggroSupport.sch"
USING "Commands_interiors.sch"
USING "Ambient_approach.sch"
USING "dialogue_public.sch"
USING "Shop_public.sch"
USING "Ambient_Speech.sch"
USING "script_blips.sch"
USING "cutscene_public.sch"
USING "ambient_common.sch"
USING "taxi_functions.sch"
USING "commands_event.sch"
USING "altruist_cult.sch"
USING "script_heist.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"
USING "load_queue_public.sch"

//#IF IS_DEBUG_BUILD
//	USING "SP_Debug_Menu.sch"
//	BOOL set_the_player_in_debug_car
//#ENDIF

STRUCT AMBIENTASSET
	MODEL_NAMES mnCriminal1
	MODEL_NAMES mnCriminal2
	MODEL_NAMES mnShopKeep
	MODEL_NAMES mnpedShoppers1
	MODEL_NAMES mnpedShoppers2
	MODEL_NAMES mnPolice
	MODEL_NAMES mnPoliceCar
	STRING sGestAnimDict
	STRING sShockedAnimDictA//, sShockedAnimDictB
ENDSTRUCT

BLIP_INDEX biDestinationBlip, biInitialActionBlip

structPedsForConversation GetawayDriverConversation

ENUM EUpdate
	EUpdate_Start,
	EUpdate_CreateCriminals,
	EUpdate_CreateCriminalsSecondStage,
	EUpdate_waitForPlayerApproach,
	EUpdate_RunToExit,
	EUpdate_CreepTowardsPlayer,
	EUpdate_GetInCar,
	EUpdate_KillOneCriminal,
//	EUpdate_WaitTillInCar,
	EUpdate_PrintGod,
	EUpdate_LosePolice,
	EUpdate_WorkoutEndDestination,
	EUpdate_WaitTillAtDestination,
	EUpdate_PoliceStation,
	Eupdate_RunCutscene,
	Eupdate_RunCutscene1,
	EUpdate_ControlBackOn,
	EUpdate_LostCriminals,
	EUpdate_CalledPolice,
	EUpdate_CriminalAttacked,
	EUpdate_WaitForPolice,
	EUpdate_Finish
ENDENUM

BOOL bIsGetawayPlayerControlOn = TRUE
BOOL bToldToStop
BOOL bToldToDriveHome
INT /*random_int, ExitDestination,*/ FinalDestination
INT iAttackTimer
INT /*IndexOfLonePedInCar,*/ iChatStages, iChatTimer, iChatPitchStages
INT iEndCutsceneIndex
INT iAimingTimer = -1
INT iStoppingTimer = -1
INT iShockingEvent
INT iTimerDialogue

GROUP_INDEX aGroup
//INTERPCAMS ExitCams
VECTOR vInput

VECTOR vCopSpawn
VECTOR vDiffVector
VECTOR vSafeCoord//, CameraRotation, vVehiclePosition

VECTOR vPoliceStation1 = << 370.257629,-1597.488281,35.949543 >>
VECTOR vPoliceStation2 = << 824.992004,-1289.266846,27.245644 >>
VECTOR vPoliceStation3 = << 450.188873,-981.675415,42.691738 >>
VECTOR vPoliceStation4 = << -1088.369751,-842.291077,30.275543 >>
VECTOR vPoliceStation5 = << 608.907593,0.806411,100.249680 >>
VECTOR vPoliceStation6 = << -562.015869,-130.811310,37.436901 >>
VECTOR vPoliceStation7 = << 1855.237061,3683.236084,33.291649 >>
VECTOR vPoliceStation8 = << -443.506348,6016.230957,30.717875 >>

VECTOR vCult = << -1034.6, 4918.6, 205.9 >>
VECTOR vPlayerVehiclePosition

BOOL bLeaveEarly

BOOL bCopsCanSpawn
BOOL bCopsHaveSpawned

BOOL bShopKeepToldToThank

BOOL bHasCriminalAskedForCar
BOOL bHasCriminalAskedForCarAgain

BOOL bHasCivilianBeenScared

BOOL bCriminalsAttacked
BOOL bCriminalsAimedAt

CONST_INT MAX_NUM_CRIMINALS 2

BLIP_INDEX blipCriminal[MAX_NUM_CRIMINALS] // ShopKeepBlip
BLIP_INDEX blipCult

PED_INDEX pedShopkeep
PED_INDEX pedCriminal[MAX_NUM_CRIMINALS]
PED_INDEX pedShoppers[5]
PED_INDEX pedCop[3]
PED_INDEX pedTempCop[2]

VEHICLE_INDEX vehPoliceCar
VEHICLE_INDEX vehTempCop
VEHICLE_INDEX vehImpound

SEQUENCE_INDEX seq

REL_GROUP_HASH rghCriminals
REL_GROUP_HASH rghShopeep
REL_GROUP_HASH rghCops

BOOL bDoFullCleanUp

LoadQueueMedium sLoadQueue


FUNC BOOL LoadOrUnloadFirstAssets(BOOL Load, AMBIENTASSET Asset)

	IF Load = TRUE
	
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnCriminal1)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnCriminal2)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnShopKeep)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnpedShoppers1)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnpedShoppers2)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnPolice)
		LOAD_QUEUE_MEDIUM_ADD_MODEL(sLoadQueue, Asset.mnPoliceCar)

		IF HAS_LOAD_QUEUE_MEDIUM_LOADED(sLoadQueue)
			RETURN TRUE
		ENDIF
	ELSE
		CLEANUP_LOAD_QUEUE_MEDIUM(sLoadQueue)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR ValidPositionInFront()

	VECTOR PlayerOffset
	VECTOR CarNode
	VECTOR SafeCoord
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		PlayerOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,100,0>>)
		IF GET_CLOSEST_VEHICLE_NODE(PlayerOffset, CarNode)
			GET_SAFE_COORD_FOR_PED(CarNode, TRUE, SafeCoord)
		ELSE
			GET_SAFE_COORD_FOR_PED(PlayerOffset, TRUE, SafeCoord)
		ENDIF
	ENDIF
	
	RETURN SafeCoord
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 2
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
			IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) != RHINO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_CRIMINALS_IN_VEHICLE()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX TempIndex
		TempIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(TempIndex)
			IF NOT IS_PED_INJURED(pedCriminal[0])
			AND NOT IS_PED_INJURED(pedCriminal[1])
				IF IS_PED_IN_VEHICLE(pedCriminal[0], TempIndex)
				AND IS_PED_IN_VEHICLE(pedCriminal[1], TempIndex)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CRIMINAL_CLOSE_TO_POLICE_STATION()

	IF ARE_ALL_CRIMINALS_IN_VEHICLE()
	  	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation1, << 50, 50, 50 >>)
        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation2, << 50, 50, 50 >>)
        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation3, << 50, 50, 50 >>)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation4, << 50, 50, 50 >>)
        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation5, << 50, 50, 50 >>)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation6, << 50, 50, 50 >>)
        OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation7, << 50, 50, 50 >>)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation8, << 50, 50, 50 >>)
			IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.2
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_CRIMINAL_BEEN_ATTACKED()
	INT index
	REPEAT COUNT_OF(pedCriminal) index
		IF DOES_ENTITY_EXIST(pedCriminal[index])
			IF NOT IS_PED_INJURED(pedCriminal[index])
				IF NOT IS_PED_IN_GROUP(pedCriminal[index])
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedCriminal[index], PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(pedCriminal[0])
							AND NOT IS_PED_INJURED(pedCriminal[1])
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedCriminal[0])
								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCriminal[0])
								OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedCriminal[1])
								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCriminal[1])
									IF iAimingTimer = -1
										iAimingTimer = GET_GAME_TIMER()
										PRINTLN("\n @@@@@@@@@@@@@@@@@@@@ iAimingTimer: ", iAimingTimer, " @@@@@@@@@@@@@@@@@@@@ \n")
									ENDIF
								ELSE
									PRINTLN("\n @@@@@@@@@@@@@@@@@@@@ iAimingTimer = -1 @@@@@@@@@@@@@@@@@@@@ \n")
									iAimingTimer = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bCriminalsAimedAt
						IF (iAimingTimer != -1 AND GET_GAME_TIMER() > iAimingTimer + 1000)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_AIM", CONV_PRIORITY_AMBIENT_HIGH)
		//						TASK_LOOK_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
								bCriminalsAimedAt = TRUE
//							ENDIF
						ENDIF
					ENDIF
				
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[index], GET_PLAYERS_LAST_VEHICLE())
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY - GET_PLAYERS_LAST_VEHICLE()) @@@@@@@@@@@@@@@@@@@@ \n")
				//		AND GET_ENTITY_HEALTH(pedCriminal[0]) < 120
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[index], PLAYER_PED_ID())
					AND GET_ENTITY_HEALTH(pedCriminal[index]) < 190
					OR (iAimingTimer != -1 AND GET_GAME_TIMER() > iAimingTimer + 5000)
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY - PLAYER_PED_ID() or iAimingTimer @@@@@@@@@@@@@@@@@@@@ \n")
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[index], GET_PLAYERS_LAST_VEHICLE())
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY - GET_PLAYERS_LAST_VEHICLE()) @@@@@@@@@@@@@@@@@@@@ \n")
			//		AND GET_ENTITY_HEALTH(pedCriminal[0]) < 120
						RETURN TRUE
					ENDIF
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[index], PLAYER_PED_ID())
					PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY - PLAYER_PED_ID() or iAimingTimer @@@@@@@@@@@@@@@@@@@@ \n")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CRIMINAL_IN_RANGE(PED_INDEX& thePeds[], VEHICLE_INDEX TheVehicle)

	INT I
	IF NOT bToldToStop
		FOR I = 0 TO COUNT_OF(thePeds)-1
			IF NOT IS_PED_INJURED(thePeds[I])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 121.8366, -1503.0064, 28.0999 >>, << 0.1367, -1649.7048, 48.1608 >>, 125.8125)
				AND NOT IS_ENTITY_OCCLUDED(thePeds[I])
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 75.3661, -1542.8114, 28.4601 >>, << 43.7035, -1580.2795, 33.4259 >>, 26.0000)
					IF NOT DOES_BLIP_EXIST(blipCriminal[I])
						IF NOT IS_PED_INJURED(thePeds[I])
							IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
								SET_RANDOM_EVENT_ACTIVE()
								DISABLE_TAXI_HAILING(TRUE)
								IF NOT IS_ENTITY_DEAD(vehImpound)
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpound)
									AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehImpound, << 50, 50, 50 >>)
										SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
									ENDIF
								ENDIF
							ENDIF
							blipCriminal[I] = CREATE_BLIP_FOR_PED(thePeds[I])
							SHOW_HEIGHT_ON_BLIP(blipCriminal[I], FALSE)
							RemoveBlip(biInitialActionBlip)
						ENDIF
					ENDIF
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF iTimerDialogue < GET_GAME_TIMER()
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_SHOUT", CONV_PRIORITY_AMBIENT_HIGH)
							iTimerDialogue = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 4000)
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 75.3661, -1542.8114, 28.4601 >>, << 43.7035, -1580.2795, 33.4259 >>, 26.0000)
						KILL_ANY_CONVERSATION()
						IF NOT IS_ENTITY_DEAD(TheVehicle)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								AND IS_VEHICLE_ON_ALL_WHEELS(GET_PLAYERS_LAST_VEHICLE())
			//						PRINT_NOW("Getaway_start", DEFAULT_GOD_TEXT_TIME, 1)
									CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_START", CONV_PRIORITY_AMBIENT_HIGH)
									bToldToStop = TRUE
								ELSE
									CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_START2", CONV_PRIORITY_AMBIENT_HIGH)
									bToldToStop = TRUE
								ENDIF
								IF DOES_BLIP_EXIST(blipCriminal[0])
									SHOW_HEIGHT_ON_BLIP(blipCriminal[0], TRUE)
								ENDIF
								IF DOES_BLIP_EXIST(blipCriminal[1])
									SHOW_HEIGHT_ON_BLIP(blipCriminal[1], TRUE)
								ENDIF
								IF NOT IS_PED_INJURED(thePeds[1])
								AND NOT IS_PED_INJURED(pedShopkeep)
									SET_PED_RESET_FLAG(thePeds[1], PRF_InstantBlendToAim, TRUE)
									OPEN_SEQUENCE_TASK(seq)
										TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "stretch_call_hurry_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
										TASK_PLAY_ANIM(NULL, "combat@gestures@pistol@halt", "180", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
										TASK_AIM_GUN_AT_ENTITY(NULL, pedShopkeep, -1)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(thePeds[1], seq)
									CLEAR_SEQUENCE_TASK(seq)
								ENDIF
								TASK_LOOK_AT_ENTITY(thePeds[I], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC AddblipCriminal(PED_INDEX& thePeds[], FLOAT Distance)

		INT I 
		FOR I = 0 TO COUNT_OF(thePeds)-1
		
			IF IS_PED_INJURED(thePeds[I])
				IF DOES_BLIP_EXIST(blipCriminal[I])
					REMOVE_BLIP(blipCriminal[I])
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF IS_PED_IN_VEHICLE(thePeds[I], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF DOES_BLIP_EXIST(blipCriminal[I])
								REMOVE_BLIP(blipCriminal[I])
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(blipCriminal[I])
								blipCriminal[I] = CREATE_BLIP_FOR_PED(thePeds[I])
							ENDIF
//							RemoveBlip(biDestinationBlip)
//							IF DOES_BLIP_EXIST(blipCult)
//								REMOVE_BLIP(blipCult)
//							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipCriminal[I])
						blipCriminal[I] = CREATE_BLIP_FOR_PED(thePeds[I])
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_AT_ENTITY(thePeds[I], PLAYER_PED_ID(), <<Distance, Distance, Distance>>) 
					IF DOES_BLIP_EXIST(blipCriminal[I])
						REMOVE_BLIP(blipCriminal[I])
					ENDIF
				ENDIF
			ENDIF
			
		ENDFOR
		
//		IF IS_PED_INJURED(pedShopkeep)
//			IF DOES_BLIP_EXIST(ShopKeepBlip)
//				REMOVE_BLIP(ShopKeepBlip)
//			ENDIF
//		ENDIF
		
ENDPROC

PROC AttackHatedPedsAround(PED_INDEX aPed)

	IF NOT IS_PED_INJURED(aPed)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(aPed, 50.0)
		SET_PED_KEEP_TASK(aPed, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(aPed, CA_CAN_CAPTURE_ENEMY_PEDS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(aPed, CA_AGGRESSIVE, TRUE)
		SET_PED_COMBAT_RANGE(aPed, CR_FAR)
		SET_PED_COMBAT_MOVEMENT(aPed, CM_WILLADVANCE)
		
		SET_PED_INFINITE_AMMO(aPed, TRUE)
	ENDIF

ENDPROC

PROC RunChatOnWayHome(PED_INDEX& TheCriminals[])

	IF NOT IS_PED_INJURED(TheCriminals[0])
	AND NOT IS_PED_INJURED(TheCriminals[1])
		IF IS_ENTITY_AT_ENTITY(TheCriminals[0], TheCriminals[1], << 15, 15, 15 >>)
		AND IS_ENTITY_AT_ENTITY(TheCriminals[0], PLAYER_PED_ID(), << 15, 15, 15 >>)
			SWITCH iChatStages
			
				CASE 0
					iChatTimer = GET_GAME_TIMER()
					iChatStages++	
				BREAK
				
				CASE 1
					IF DO_TIMER(iChatTimer, 2000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_HOM2", CONV_PRIORITY_AMBIENT_HIGH)
							iChatStages++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_HOM4M", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_HOM4F", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_HOM4T", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						iChatStages++
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

PROC RunChatOnWayHomePitch(PED_INDEX& TheCriminals[])

	IF NOT IS_PED_INJURED(TheCriminals[0])
	AND NOT IS_PED_INJURED(TheCriminals[1])
		IF IS_ENTITY_AT_ENTITY(TheCriminals[0], TheCriminals[1], << 15, 15, 15 >>)
		AND IS_ENTITY_AT_ENTITY(TheCriminals[0], PLAYER_PED_ID(), << 15, 15, 15 >>)
			SWITCH iChatPitchStages
			
				CASE 0
					iChatTimer = GET_GAME_TIMER()
					iChatPitchStages++	
				BREAK
				
				CASE 1
					IF DO_TIMER(iChatTimer, 2000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_PITCH", CONV_PRIORITY_AMBIENT_HIGH)
							iChatPitchStages++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_SUREM", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_SUREF", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_SURET", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						iChatPitchStages++
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

PROC followPlayerInWrongVehicle()
	
	INT index
	REPEAT COUNT_OF(pedCriminal) index
		IF NOT IS_PED_INJURED(pedCriminal[index])
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
					PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE DOESN'T FIT @@@@@@@@@@@@@@@@@@@@@@@@@@")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCriminal[index], TRUE)
					IF IS_PED_IN_GROUP(pedCriminal[index])
						REMOVE_PED_FROM_GROUP(pedCriminal[index])
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(pedCriminal[index], SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedCriminal[index], SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
						TASK_GO_TO_ENTITY(pedCriminal[index], PLAYER_PED_ID(), -1, 6)
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(pedCriminal[index])
						TASK_LEAVE_ANY_VEHICLE(pedCriminal[index])
					ENDIF
					IF NOT bHasCriminalAskedForCarAgain
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CAR", CONV_PRIORITY_AMBIENT_HIGH)
							bHasCriminalAskedForCarAgain = TRUE
						ENDIF
					ENDIF
				ELSE
					bHasCriminalAskedForCarAgain = FALSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCriminal[index], FALSE)
					IF GET_SCRIPT_TASK_STATUS(pedCriminal[index], SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedCriminal[index], SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
						CLEAR_PED_TASKS(pedCriminal[index])
					ENDIF
					IF NOT IS_PED_INJURED(pedCriminal[0])
					AND NOT IS_PED_INJURED(pedCriminal[1])
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[0], VS_BACK_LEFT)//|VS_BACK_RIGHT)
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[1], VS_FRONT_RIGHT)//|VS_BACK_LEFT)
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) = BUS
							IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
								PED_INDEX pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
								IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
									SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
									DELETE_PED(pedTemp)
								ENDIF
							ENDIF
							IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
								PED_INDEX pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
								IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
									SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
									DELETE_PED(pedTemp)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF NOT IS_PED_IN_GROUP(pedCriminal[index])
				CLEAR_PED_TASKS(pedCriminal[index])
				SET_PED_AS_GROUP_MEMBER(pedCriminal[index], PLAYER_GROUP_ID())
				SET_PED_NEVER_LEAVES_GROUP(pedCriminal[index], TRUE)
				IF NOT IS_PED_INJURED(pedCriminal[0])
				AND NOT IS_PED_INJURED(pedCriminal[1])
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[0], VS_BACK_LEFT)
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[1], VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SpawnCops()
	
	IF NOT bCopsHaveSpawned
		PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ bCopsHaveSpawned = FALSE @@@@@@@@@@@@@@@@@@@@ \n")
		
//		GET_SAFE_COORD_FOR_PED(GET_PLAYER_COORDS(PLAYER_ID()), FALSE, vSafeCoord)
//		GET_POSITION_BY_SIDE_OF_ROAD(GET_PLAYER_COORDS(PLAYER_ID()), -1, vSafeCoord)
		GET_NTH_CLOSEST_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()) - << 0, 10, 0 >>, GET_RANDOM_INT_IN_RANGE(5, 15), vSafeCoord)
		
		IF NOT IS_SPHERE_VISIBLE(vSafeCoord, 10)
//		IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord + << 0, 5, 0 >>)
//		AND WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord - << 0, 5, 0 >>)
//		AND WOULD_ENTITY_BE_OCCLUDED(POLICE, vSafeCoord /*+ << 0, 15, 0 >>*/ )
			
			PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ FIRST OCCLUDED @@@@@@@@@@@@@@@@@@@@ \n")
			vCopSpawn = vSafeCoord
			bCopsCanSpawn = TRUE
			
		ELSE GET_NTH_CLOSEST_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()) +  << 0, 10, 0 >>, GET_RANDOM_INT_IN_RANGE(5, 15), vSafeCoord)
		
			IF NOT IS_SPHERE_VISIBLE(vSafeCoord, 10)
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord - << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, vSafeCoord /*+ << 0, 15, 0 >>*/ )
				
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ SECOND OCCLUDED @@@@@@@@@@@@@@@@@@@@ \n")
				vCopSpawn = vSafeCoord
				bCopsCanSpawn = TRUE
			ENDIF
			
//		IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord - << 0, 15, 0 >> )
//		AND WOULD_ENTITY_BE_OCCLUDED(POLICE, vSafeCoord - << 0, 15, 0 >> )
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@OCCLUDED - << 0, 15, 0 >>@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			vCopSpawn = vSafeCoord - << 0, 15, 0 >>
//		ENDIF
//		IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord + << 15, 0, 0 >> )
//		AND WOULD_ENTITY_BE_OCCLUDED(POLICE, vSafeCoord + << 15, 0, 0 >> )
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@OCCLUDED + << 15, 0, 0 >>@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			vCopSpawn = vSafeCoord + << 15, 0, 0 >>
//		ENDIF
//		IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, vSafeCoord - << 15, 0, 0 >> )
//		AND WOULD_ENTITY_BE_OCCLUDED(POLICE, vSafeCoord - << 15, 0, 0 >> )
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@OCCLUDED - << 15, 0, 0 >>@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			vCopSpawn = vSafeCoord - << 15, 0, 0 >>
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation1, <<50, 50, 50>>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE STATION 1@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << -550.0526, -925.7214, 22.8613 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << -550.0526, -925.7214, 22.8613 >>)
//				vCopSpawn = << -550.0526, -925.7214, 22.8613 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01,<< -574.7938, -876.4594, 24.7011 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << -574.7938, -876.4594, 24.7011 >>)
//				vCopSpawn = << -574.7938, -876.4594, 24.7011 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << -619.0921, -919.3215, 22.4567 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << -619.0921, -919.3215, 22.4567 >>)
//				vCopSpawn = << -619.0921, -919.3215, 22.4567 >>
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation2, <<50, 50, 50>>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE STATION 2@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 402.4493, -1622.2377, 28.2918 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 402.4493, -1622.2377, 28.2918 >>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE CAN SPAWN AT FIRST LOCATION@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//				vCopSpawn = << 402.4493, -1622.2377, 28.2918 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 354.8831, -1693.8075, 31.5320 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 354.8831, -1693.8075, 31.5320 >>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE CAN SPAWN AT SECOND LOCATION@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//				vCopSpawn = << 354.8831, -1693.8075, 31.5320 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 400.3829, -1685.6654, 28.1794 >> + << 0, 5, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 400.3829, -1685.6654, 28.1794 >>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE CAN SPAW AT THIRD LOCATION@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//				vCopSpawn = << 400.3829, -1685.6654, 28.1794 >>
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPoliceStation3, <<50, 50, 50>>)
//			PRINTSTRING("@@@@@@@@@@@@@@@@@@@@POLICE STATION 3@@@@@@@@@@@@@@@@@@@@")
//			PRINTNL()
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 0, -15, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 0, -15, 0 >>)
//				vCopSpawn = << 0, -15, 0 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 0, -15, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 0, -15, 0 >>)
//				vCopSpawn = << 0, -15, 0 >>
//			ENDIF
//			IF WOULD_ENTITY_BE_OCCLUDED(S_M_Y_COP_01, << 0, -15, 0 >>)
//			AND WOULD_ENTITY_BE_OCCLUDED(POLICE, << 0, -15, 0 >>)
//				vCopSpawn = << 0, -15, 0 >>
//			ENDIF
//		ENDIF
			IF bCopsCanSpawn
			
				IF NOT DOES_ENTITY_EXIST(pedCop[0])
					pedCop[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, vCopSpawn + << 0, 5, 0 >>, 0.0)
					IF IS_CRIMINAL_CLOSE_TO_POLICE_STATION()
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ pedCop[0] = CREATE_PED @@@@@@@@@@@@@@@@@@@@ \n")
						GIVE_WEAPON_TO_PED(pedCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[0], rghCops)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[0], TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[0], 50.0)
//						TASK_COMBAT_PED(pedCop[0], pedCriminal[0])
						SET_PED_KEEP_TASK(pedCop[0], TRUE)
					ELSE
//						REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 1)
//						REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 2)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[0], TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_ENTITY/*_OFFSET_XY*/(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 6,/* -4, 1,*/ PEDMOVE_SPRINT)
							TASK_PLAY_ANIM(NULL, "random@getawaydriver", "gesture_nod_yes_hard", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							TASK_STAND_STILL(NULL, 3000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCop[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedCop[0], TRUE)
						ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 4, pedCop[0], "REGETCop")
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedCop[1])
					pedCop[1] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, vCopSpawn - << 0, 5, 0 >>, 0.0)
					IF IS_CRIMINAL_CLOSE_TO_POLICE_STATION()
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ pedCop[1] = CREATE_PED @@@@@@@@@@@@@@@@@@@@ \n")
						GIVE_WEAPON_TO_PED(pedCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[1], rghCops)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[1], TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[1], 50.0)
//						TASK_COMBAT_PED(pedCop[1], pedCriminal[1])
						SET_PED_KEEP_TASK(pedCop[1], TRUE)
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[1], TRUE)
						TASK_GO_TO_COORD_ANY_MEANS(pedCop[1], << 68.7217, -1569.8304, 28.6027 >>, PEDMOVE_SPRINT, NULL)
						SET_PED_KEEP_TASK(pedCop[1], TRUE)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehPoliceCar)
					vehPoliceCar = CREATE_VEHICLE(POLICE3, vCopSpawn)
					vDiffVector = GET_PLAYER_COORDS(PLAYER_ID()) - GET_ENTITY_COORDS(vehPoliceCar)
					SET_ENTITY_HEADING(vehPoliceCar, GET_HEADING_FROM_VECTOR_2D(vDiffVector.x, vDiffVector.y))
//					SET_VEHICLE_FORWARD_SPEED(vehPoliceCar, 5)
					SET_VEHICLE_SIREN(vehPoliceCar, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedCop[2])
					pedCop[2] = CREATE_PED_INSIDE_VEHICLE(vehPoliceCar, PEDTYPE_COP, S_M_Y_COP_01)
					IF IS_CRIMINAL_CLOSE_TO_POLICE_STATION()
						GIVE_WEAPON_TO_PED(pedCop[2], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[2], rghCops)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_USE_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_LEAVE_VEHICLES, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[2], TRUE)
						TASK_VEHICLE_MISSION_PED_TARGET(pedCop[2], vehPoliceCar, pedCriminal[1], MISSION_ATTACK, 15, DF_StopForPeds, -1, -1)
						SET_PED_KEEP_TASK(pedCop[2], TRUE)
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[2], TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPoliceCar, << 40.2328, -1555.7529, 28.2128 >>, 10, DRIVINGSTYLE_NORMAL, POLICE, DRIVINGMODE_AVOIDCARS, 1, -1)
							TASK_STAND_STILL(NULL, 5000)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCop[2], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedCop[2], TRUE)
					ENDIF
				ENDIF
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@ bCopsHaveSpawned = TRUE @@@@@@@@@@@@@@@@@@@@ \n")
				bCopsHaveSpawned = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UpdateEvent(EUpdate& curstate, AMBIENTASSET Asset, PED_INDEX& TheCriminals[], INT& waitTime, BOOL& bTaskGiven)

//	VECTOR vMin, vMax
	SEQUENCE_INDEX SeqNPC//, SeqShoppers
//	MODEL_NAMES VehModel 
	
	VECTOR vCriminalCreationPoint[MAX_NUM_CRIMINALS]
	FLOAT fCriminalCreationHeading[MAX_NUM_CRIMINALS]
	VECTOR vShopKeepCreationPoint
	FLOAT fShopKeepCreationHeading
	
//	VECTOR vPossibleExitForCriminals[3], vPossibleExitCriminal0[3], vPossibleExitCriminal1[3]
	
	VECTOR vPossibleDestinationPos[5]
	FLOAT ShortestDestination
	VECTOR vPossibleAlleyDestintation[5]
	
//	vPossibleExitForCriminals[0] = << 49.6959, -1559.2205, 28.3747 >>
//	vPossibleExitForCriminals[1] = << 44.2892, -1588.4633, 28.4601 >>
//	vPossibleExitForCriminals[2] = << 77.2422, -1550.0905, 28.4601 >>
	
//	vPossibleExitCriminal0[0] = << 46.5189, -1556.6342, 28.2868 >>
//	vPossibleExitCriminal1[0] = << 42.8682, -1556.3724, 28.2306 >>
//	vPossibleExitCriminal0[1] = << 38.4557, -1601.6094, 28.4158 >>
//	vPossibleExitCriminal1[1] = << 36.1769, -1600.4901, 28.3765 >>
//	vPossibleExitCriminal0[2] = << 87.3964, -1542.8710, 28.2913 >>
//	vPossibleExitCriminal1[2] = << 89.5791, -1543.4287, 28.2931 >>
	
	vPossibleDestinationPos[0] = << -56.7157, -1317.7660, 27.9845 >>
	vPossibleDestinationPos[1] = << 457.5345, -813.3361, 26.5110 >>
	vPossibleDestinationPos[2] = << 884.0470, -2239.4299, 29.4387 >>
	vPossibleDestinationPos[3] = << -1339.3282, -762.4225, 19.3140 >>
	vPossibleDestinationPos[4] = << -461.6898, 283.6650, 82.2492 >>
	
	vPossibleAlleyDestintation[0] = << -45.2654, -1290.1265, 28.1977 >>
	vPossibleAlleyDestintation[1] = << 470.1930, -731.1867, 26.4120 >>
	vPossibleAlleyDestintation[2] = << 889.9938, -2177.6382, 29.5195 >>
	vPossibleAlleyDestintation[3] = << -1334.8262, -792.1527, 18.8175 >> //<< -1315.3622, -777.9807, 20.0513 >>
	vPossibleAlleyDestintation[4] = << -424.5901, 288.3558, 82.2293 >>
	
	
	INT I//, J
	VECTOR PlayerPos
	VEHICLE_INDEX vehParkedCar[3]
	
//	IF IS_VEHICLE_DRIVEABLE(TheVehicle)
	
//		VehModel = GET_ENTITY_MODEL(TheVehicle)
//		GET_MODEL_DIMENSIONS(VehModel, vMin, vMax)
		
//		VECTOR vCarDoorOffset = <<(-vMax.x-0.3),0,0>>
//		VECTOR vCarDoorStandPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TheVehicle, vCarDoorOffset)		
		
		SWITCH curstate
		
			CASE EUpdate_Start
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_Start @@@@@@@@@@@@@@@@@@@@@@@@@@@@")
				IF IS_COORD_EVENT_ACTIVATED(vInput, 300.0, TRUE)
					curstate = EUpdate_CreateCriminals
				ENDIF
			BREAK
			
			CASE EUpdate_CreateCriminals
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_CreateCriminals @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				waitTime = 0
				
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
				SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
				
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TheVehicle)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 0, PLAYER_PED_ID(), "MICHAEL")
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 0, PLAYER_PED_ID(), "TREVOR")
					ENDIF
					
					IF VDIST(vInput, <<933.83, 3640.22, 32.36>> ) < 20.0
						vCriminalCreationPoint[0] = << 943.1853, 3647.2180, 31.3603 >>
						fCriminalCreationHeading[0] = 58.2967
						vCriminalCreationPoint[1] = << 941.3207, 3650.3005, 31.3111 >>
						fCriminalCreationHeading[1] = 92.7793
						vShopKeepCreationPoint = << 939.2422, 3649.7551, 31.3536 >>
						fShopKeepCreationHeading = 260.4475
						
					ELIF VDIST(vInput, <<57.86, -1562.40, 29.47>> ) < 20.0
						vCriminalCreationPoint[0] = << 66.0644, -1564.2178, 28.4602 >>
						fCriminalCreationHeading[0] = 198.4093
						vCriminalCreationPoint[1] =  << 63.5055, -1568.1784, 28.4602 >>
						fCriminalCreationHeading[1] = 254.1330
						vShopKeepCreationPoint = << 68.4259, -1569.0674, 28.6027 >>
						fShopKeepCreationHeading = 64.1114
					ENDIF
					
					aGroup = GET_PLAYER_GROUP(PLAYER_ID())
					
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>, FALSE)
					ADD_SCENARIO_BLOCKING_AREA(vInput - << 50, 50, 50 >>, vInput + << 50, 50, 50 >>)
					SET_PED_PATHS_IN_AREA(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>, FALSE)
					SET_ROADS_IN_AREA(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>, FALSE)
					
					REQUEST_MODEL(PICADOR)
					REQUEST_MODEL(STANIER)
					REQUEST_MODEL(PATRIOT)
					WHILE NOT HAS_MODEL_LOADED(PICADOR)
					OR NOT HAS_MODEL_LOADED(STANIER)
					OR NOT HAS_MODEL_LOADED(PATRIOT)
						WAIT(0)
					ENDWHILE
	 				vehParkedCar[0] = CREATE_VEHICLE(PICADOR, << 59.5209, -1547.2279, 28.4602 >>, 49.1762)
					vehParkedCar[1] = CREATE_VEHICLE(STANIER, << 33.3296, -1577.5375, 28.2826 >>, 51.8183)
					vehParkedCar[2] = CREATE_VEHICLE(PATRIOT, << 48.8282, -1582.7057, 28.4603 >>, 226.8345)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehParkedCar[0])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehParkedCar[1])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehParkedCar[2])
					
//					PRINTSTRING("\n@@@@@@@@@@@@@@@ iShockingEvent: ")PRINTINT(iShockingEvent)PRINTSTRING(" @@@@@@@@@@@@@@@\n")
					IF iShockingEvent = 0
//						iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_SEEN_CONFRONTATION, vShopKeepCreationPoint, 60000)
					ENDIF
					
					ADD_RELATIONSHIP_GROUP("reCriminals", rghCriminals)
					ADD_RELATIONSHIP_GROUP("reShopKeep", rghShopeep)
					ADD_RELATIONSHIP_GROUP("reCops", rghCops)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminals, RELGROUPHASH_COP)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCops, rghCriminals)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rghCriminals)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, rghCops)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCops, RELGROUPHASH_COP)
					
					TheCriminals[0] = CREATE_PED(PEDTYPE_CRIMINAL, Asset.mnCriminal1, vCriminalCreationPoint[0], fCriminalCreationHeading[0])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TheCriminals[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnCriminal1)
					SET_PED_RELATIONSHIP_GROUP_HASH(TheCriminals[0], rghCriminals)
					GIVE_WEAPON_TO_PED(TheCriminals[0], WEAPONTYPE_APPISTOL, INFINITE_AMMO, TRUE)
					SET_PED_MONEY(TheCriminals[0], GET_RANDOM_INT_IN_RANGE(800, 2000))
					SET_PED_CONFIG_FLAG(TheCriminals[0], PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[0], FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[0], FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[0], FA_FORCE_EXIT_VEHICLE, TRUE)
//						SET_ENTITY_HEALTH(TheCriminals[0], 450)
//						SET_PED_MAX_HEALTH(TheCriminals[0], 450)
					SET_PED_COMPONENT_VARIATION(TheCriminals[0], PED_COMP_SPECIAL2, 1, 0)
					ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 2, TheCriminals[0], "REGETRobber2")
					
					TheCriminals[1] = CREATE_PED(PEDTYPE_CRIMINAL, Asset.mnCriminal2, vCriminalCreationPoint[1], fCriminalCreationHeading[1])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TheCriminals[1], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnCriminal2)
					SET_PED_RELATIONSHIP_GROUP_HASH(TheCriminals[1], rghCriminals)
					GIVE_WEAPON_TO_PED(TheCriminals[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
					SET_PED_MONEY(TheCriminals[1], GET_RANDOM_INT_IN_RANGE(800, 2000))
					SET_PED_CONFIG_FLAG(TheCriminals[1], PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[1], FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[1], FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(TheCriminals[1], FA_FORCE_EXIT_VEHICLE, TRUE)
//						SET_ENTITY_HEALTH(TheCriminals[1], 450)
//						SET_PED_MAX_HEALTH(TheCriminals[1], 450)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_HEAD, 5, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_BERD, 1, 0, 0) //(berd)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_HAIR, 1, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_TORSO, 6, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_LEG, 6, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_FEET, 1, 0, 0) //(feet)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_SPECIAL, 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_SPECIAL2, 0, 0, 0) //(task)
					SET_PED_COMPONENT_VARIATION(TheCriminals[1], PED_COMP_DECL, 1, 0, 0) //(decl)
					SET_AMBIENT_VOICE_NAME(TheCriminals[1], "PACKIE_AI_Norm_Part1_Booth")
					ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 1, TheCriminals[1], "PACKIE")
					
					pedShopkeep = CREATE_PED(PEDTYPE_CIVMALE, Asset.mnShopKeep, vShopKeepCreationPoint, fShopKeepCreationHeading)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnShopKeep)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopkeep, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedShopkeep, rghShopeep)
					SET_AMBIENT_VOICE_NAME(pedShopkeep, "A_M_Y_Business_01_CHINESE_MINI_01")
					ADD_PED_FOR_DIALOGUE(GetawayDriverConversation, 3, pedShopkeep, "StoreOwner")
					
					pedShoppers[0] = CREATE_PED(PEDTYPE_CIVFEMALE, Asset.mnpedShoppers1, << 62.8929, -1575.5201, 28.6027 >>, 326.1814)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnpedShoppers1)
					SET_PED_COMBAT_ATTRIBUTES(pedShoppers[0], CA_ALWAYS_FLEE, TRUE)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShoppers[0], TRUE)
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOCKING_EVENT_REACT(NULL, iShockingEvent)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, TheCriminals[1])
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedShoppers[0], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					TASK_START_SCENARIO_IN_PLACE(pedShoppers[0], "WORLD_HUMAN_MOBILE_FILM_SHOCKING")
//					TASK_SHOCKING_EVENT_REACT(pedShoppers[0], iShockingEvent)
					TASK_COWER(pedShoppers[0], -1)
					
//					pedShoppers[1] = CREATE_PED(PEDTYPE_CIVMALE, Asset.mnpedShoppers2, << 61.0436, -1555.2566, 28.4602 >>, 200.7124)
//					SET_MODEL_AS_NO_LONGER_NEEDED (Asset.mnpedShoppers2)
//					SET_PED_COMBAT_ATTRIBUTES(pedShoppers[1], CA_ALWAYS_FLEE, TRUE)
////					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShoppers[1], TRUE)
////					OPEN_SEQUENCE_TASK(SeqShoppers)
////						TASK_TURN_PED_TO_FACE_ENTITY(NULL, TheCriminals[1])
////						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
////						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
////						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
////					SET_SEQUENCE_TO_REPEAT(SeqShoppers, REPEAT_FOREVER)
////					CLOSE_SEQUENCE_TASK(SeqShoppers)
////					TASK_PERFORM_SEQUENCE(pedShoppers[1], SeqShoppers)
//					CLEAR_SEQUENCE_TASK(SeqShoppers)
//					TASK_START_SCENARIO_IN_PLACE(pedShoppers[1], "WORLD_HUMAN_TOURIST_MOBILE")
////					TASK_SHOCKING_EVENT_REACT(pedShoppers[1], iShockingEvent)

					pedShoppers[2] = CREATE_PED(PEDTYPE_CIVFEMALE, Asset.mnpedShoppers1, << 61.6538, -1555.2559, 28.4601 >>, 206.7004)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnpedShoppers1)
					SET_PED_COMBAT_ATTRIBUTES(pedShoppers[2], CA_ALWAYS_FLEE, TRUE)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShoppers[2], TRUE)
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOCKING_EVENT_REACT(NULL, iShockingEvent)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, TheCriminals[0])
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedShoppers[2], seq)
//					CLEAR_SEQUENCE_TASK(seq)
					TASK_START_SCENARIO_IN_PLACE(pedShoppers[2], "WORLD_HUMAN_MOBILE_FILM_SHOCKING")
//					TASK_SHOCKING_EVENT_REACT(pedShoppers[2], iShockingEvent)
					
					pedShoppers[3] = CREATE_PED(PEDTYPE_CIVMALE, Asset.mnpedShoppers2, << 75.9343, -1556.3062, 28.6028 >>, 147.2908)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnpedShoppers2)
					SET_PED_COMBAT_ATTRIBUTES(pedShoppers[3], CA_ALWAYS_FLEE, TRUE)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShoppers[3], TRUE)
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_TURN_PED_TO_FACE_ENTITY(NULL, TheCriminals[0])
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//						TASK_PLAY_ANIM(NULL, "random@getawaydriver", "react_small_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY, 0)
//					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedShoppers[3], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//					TASK_SHOCKING_EVENT_REACT(pedShoppers[3], iShockingEvent)
//					TASK_START_SCENARIO_IN_PLACE(pedShoppers[3], "WORLD_HUMAN_MOBILE_FILM_SHOCKING")
//				ENDIF
				curstate = EUpdate_CreateCriminalsSecondStage
			BREAK
			
			CASE EUpdate_CreateCriminalsSecondStage
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_CreateCriminalsSecondStage @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF NOT IS_ENTITY_DEAD(pedShopkeep)
				AND NOT IS_ENTITY_DEAD(pedShoppers[0])
//				AND NOT IS_ENTITY_DEAD(pedShoppers[1])
				AND NOT IS_ENTITY_DEAD(pedShoppers[2])
				AND NOT IS_ENTITY_DEAD(pedShoppers[3])
				AND NOT IS_ENTITY_DEAD(TheCriminals[0])
				AND NOT IS_ENTITY_DEAD(TheCriminals[1])
//				AND NOT IS_ENTITY_DEAD(vehParkedCar[0])
//				AND NOT IS_ENTITY_DEAD(vehParkedCar[1])
//				AND NOT IS_ENTITY_DEAD(vehParkedCar[2])
					IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedShopkeep)
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedShoppers[0])
//					OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedShoppers[1])
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedShoppers[2])
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedShoppers[3])
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TheCriminals[0])
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TheCriminals[1])
						REQUEST_ANIM_DICT("random@getawaydriver@thugs")
						REQUEST_ANIM_DICT("combat@gestures@pistol@halt")
						REQUEST_ANIM_DICT("misslamar1ig_20")
						REQUEST_ANIM_DICT("random@getawaydriver")
						WHILE NOT HAS_ANIM_DICT_LOADED("random@getawaydriver@thugs")
						OR NOT HAS_ANIM_DICT_LOADED("combat@gestures@pistol@halt")
						OR NOT HAS_ANIM_DICT_LOADED("misslamar1ig_20")
						OR NOT HAS_ANIM_DICT_LOADED("random@getawaydriver")
							WAIT(0)
						ENDWHILE
						
						IF NOT IS_PED_INJURED(pedShopkeep)
							TASK_HANDS_UP(pedShopkeep, -1)
							
							IF NOT IS_PED_INJURED(TheCriminals[0])
								TASK_PLAY_ANIM(TheCriminals[0], "random@getawaydriver@thugs", "base_a", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
								TASK_LOOK_AT_ENTITY(TheCriminals[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
	//							TASK_AIM_GUN_AT_ENTITY(TheCriminals[0], pedShopkeep, -1)
							ENDIF
							
							IF NOT IS_PED_INJURED(TheCriminals[1])
								TASK_PLAY_ANIM(TheCriminals[1], "random@getawaydriver@thugs", "base_b", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
								TASK_LOOK_AT_ENTITY(TheCriminals[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
	//							TASK_AIM_GUN_AT_ENTITY(TheCriminals[1], pedShopkeep, -1)
							ENDIF
						ENDIF
						
	//					biInitialActionBlip = CREATE_AMBIENT_BLIP_INITIAL_COORD(vCriminalCreationPoint[0])
	//					SET_BLIP_PRIORITY(biInitialActionBlip, BLIPPRIORITY_MED)
	//					RemoveBlip(biInitialActionBlip)
	//					REPEAT COUNT_OF(TheCriminals) I
	//						blipCriminal[I] = CREATE_BLIP_FOR_PED(TheCriminals[I])
	//					ENDREPEAT
						SETTIMERA(0)
						curstate = EUpdate_waitForPlayerApproach
	//				ELSE
	//					PRINTSTRING("***********************************")
	//					PRINTNL()
	//					PRINTSTRING("Ending GatawayDriver: Not in the vehicle")
	//					PRINTNL()
	//					PRINTSTRING("***********************************")
	//					curstate = EUpdate_Finish
	//				ENDIF
				
					ENDIF
				ENDIF
			BREAK
			
			CASE EUpdate_waitForPlayerApproach
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_waitForPlayerApproach @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF TIMERA() > 0
					IF NOT IS_PED_INJURED(pedShoppers[0])
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedShoppers[0], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
						PLAY_PED_AMBIENT_SPEECH(pedShoppers[0], "GENERIC_SHOCKED_HIGH")
					ENDIF
				ENDIF
//				IF TIMERA() > 1000
//					IF NOT IS_PED_INJURED(pedShoppers[1])
//						PLAY_PED_AMBIENT_SPEECH(pedShoppers[1], "GENERIC_SHOCKED_HIGH")
////						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedShoppers[1], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
//					ENDIF
//				ENDIF
				IF TIMERA() > 1000
					IF NOT IS_PED_INJURED(pedShoppers[2])
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedShoppers[2], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
						PLAY_PED_AMBIENT_SPEECH(pedShoppers[2], "GENERIC_FRIGHTENED_HIGH")
					ENDIF
				ENDIF
				IF TIMERA() > 2000
					IF NOT IS_PED_INJURED(pedShoppers[3])
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedShoppers[3], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
						PLAY_PED_AMBIENT_SPEECH(pedShoppers[3], "GENERIC_FRIGHTENED_HIGH")
						SETTIMERA(0)
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<81.719765,-1530.834106,28.176506>>, <<26.785257,-1595.692749,58.294434>>, 32.875000)
					IF NOT bHasCivilianBeenScared
						IF NOT IS_PED_INJURED(pedShoppers[0])
							TASK_SMART_FLEE_COORD(pedShoppers[0], vInput, 200, -1)
						ENDIF
						IF NOT IS_PED_INJURED(pedShoppers[2])
							OPEN_SEQUENCE_TASK(seq)
								TASK_PAUSE(NULL, 250)
								TASK_SMART_FLEE_COORD(NULL, vInput, 200, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedShoppers[2], seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
						IF NOT IS_PED_INJURED(pedShoppers[3])
							OPEN_SEQUENCE_TASK(seq)
								TASK_PAUSE(NULL, 500)
								TASK_SMART_FLEE_COORD(NULL, vInput, 200, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedShoppers[3], seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
						bHasCivilianBeenScared = TRUE
					ENDIF
				ENDIF
				IF IS_CRIMINAL_IN_RANGE(TheCriminals, GET_PLAYERS_LAST_VEHICLE())
//					FOR I = 0 TO COUNT_OF(TheCriminals)-1
//						IF NOT IS_PED_INJURED(TheCriminals[I])
//							IF IS_VEHICLE_DRIVEABLE(TheVehicle)
//								IF NOT IS_PED_IN_VEHICLE(TheCriminals[I], TheVehicle)
//									FOR J = 0 TO COUNT_OF(TheCriminals)-1
//										IF NOT DOES_BLIP_EXIST(blipCriminal[J])
//											IF NOT IS_PED_INJURED(TheCriminals[J])
//												IF NOT this_script_activated
//													this_script_activated = TRUE
//												ENDIF
//												blipCriminal[J] = CREATE_BLIP_FOR_PED(TheCriminals[J])
//											ENDIF
//										ENDIF
//									ENDFOR
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDFOR
//					IF NOT DOES_BLIP_EXIST(ShopKeepBlip)
//						ShopKeepBlip = CREATE_BLIP_FOR_PED(pedShopkeep)
//					ENDIF
//					RemoveBlip(biInitialActionBlip)
					curstate = EUpdate_RunToExit
				ENDIF
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
				IF IS_COP_PED_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
				OR IS_COP_VEHICLE_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						TASK_SMART_FLEE_PED(TheCriminals[0], PLAYER_PED_ID(), 1000, -1)
						TASK_SMART_FLEE_PED(TheCriminals[1], PLAYER_PED_ID(), 1000, -1)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_POLICE", CONV_PRIORITY_AMBIENT_HIGH)
						curstate = EUpdate_Finish
					ENDIF
				ENDIF
			BREAK
			
			CASE EUpdate_RunToExit
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_RunToExit @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
					AND NOT IS_PED_INJURED(pedShopkeep)
						
//						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(TheCriminals[0], vPossibleExitCriminal0[ExitDestination], pedShopkeep, PEDMOVE_SPRINT, FALSE)
//						TASK_GO_TO_COORD_ANY_MEANS(TheCriminals[1], vPossibleExitCriminal1[ExitDestination], PEDMOVE_RUN, NULL)
						
//						PlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//						ShortestDestination = 9999
//						IF VDIST(PlayerPos, vPossibleExitForCriminals[0]) < ShortestDestination
//							ShortestDestination = VDIST(PlayerPos, vPossibleExitForCriminals[0])
//							ExitDestination = 0
//						ENDIF
//						IF VDIST(PlayerPos, vPossibleExitForCriminals[1]) < ShortestDestination
//							ShortestDestination = VDIST(PlayerPos, vPossibleExitForCriminals[1])
//							ExitDestination = 1
//						ENDIF
//						IF VDIST(PlayerPos, vPossibleExitForCriminals[2]) < ShortestDestination
//							ShortestDestination = VDIST(PlayerPos, vPossibleExitForCriminals[2])
//							ExitDestination = 2
//						ENDIF
//						
//						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(TheCriminals[0], vPossibleExitCriminal0[ExitDestination], pedShopkeep, PEDMOVE_SPRINT, FALSE)
//						TASK_GO_TO_COORD_ANY_MEANS(TheCriminals[1], vPossibleExitCriminal1[ExitDestination], PEDMOVE_RUN, NULL)
						
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedShopkeep)
						IF NOT IS_PED_INJURED(pedShopkeep)
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_DOWN", CONV_PRIORITY_AMBIENT_HIGH)
							TASK_LOOK_AT_ENTITY(TheCriminals[0], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
							
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(NULL, "random@getawaydriver", "idle_2_hands_up", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT, 0.250)
								TASK_PLAY_ANIM(NULL, "random@getawaydriver", "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedShopkeep, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
						curstate = EUpdate_CreepTowardsPlayer
					ENDIF
				ENDIF
				
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
				OR IS_COP_PED_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
				OR IS_COP_VEHICLE_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						TASK_SMART_FLEE_PED(TheCriminals[0], PLAYER_PED_ID(), 1000, -1)
						TASK_SMART_FLEE_PED(TheCriminals[1], PLAYER_PED_ID(), 1000, -1)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_POLICE", CONV_PRIORITY_AMBIENT_HIGH)
						curstate = EUpdate_Finish
					ENDIF
				ENDIF
			BREAK
			
			CASE EUpdate_CreepTowardsPlayer
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_CreepTowardsPlayer @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				
				IF IS_PED_INJURED(pedShopkeep)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						TASK_TURN_PED_TO_FACE_ENTITY(TheCriminals[0], PLAYER_PED_ID(), -1)
						TASK_TURN_PED_TO_FACE_ENTITY(TheCriminals[1], PLAYER_PED_ID(), -1)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedShopkeep, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedShopkeep, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(NULL, "random@getawaydriver", "idle_2_hands_up", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT, 0.250)
							TASK_PLAY_ANIM(NULL, "random@getawaydriver", "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedShopkeep, seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
				
//				IF IS_ENTITY_AT_COORD(TheCriminals[0], vPossibleExitCriminal0[ExitDestination], << 1.0, 1.0, 2.0 >>)
//					IF NOT IS_PED_INJURED(TheCriminals[0])
//					AND NOT IS_PED_INJURED(pedShopkeep)
//						TASK_AIM_GUN_AT_ENTITY(TheCriminals[0], pedShopkeep, -1)
//					ENDIF
//				ENDIF
//				IF IS_ENTITY_AT_COORD(TheCriminals[1], vPossibleExitCriminal1[ExitDestination], << 1.0, 1.0, 2.0 >>)
//					IF NOT IS_PED_INJURED(TheCriminals[1])
//						TASK_TURN_PED_TO_FACE_ENTITY(TheCriminals[1], PLAYER_PED_ID())
//					ENDIF
//				ENDIF
				IF NOT IS_PED_INJURED(pedCriminal[0])
				AND NOT IS_PED_INJURED(pedCriminal[1])
//					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), TheCriminals[1], << 12, 12, 12 >>, FALSE, TRUE, TM_IN_VEHICLE)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.1
								IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
								AND IS_VEHICLE_ON_ALL_WHEELS(GET_PLAYERS_LAST_VEHICLE())
									REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//									WHILE NOT HAS_VEHICLE_ASSET_LOADED(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//										WAIT(0)
//									ENDWHILE
//									IF NOT IS_PED_INJURED(pedCriminal[0])
//									AND NOT IS_PED_INJURED(pedCriminal[1])
										IF NOT IS_PED_INJURED(pedShopkeep)
		//									DOORFLAGS DoorPicked
											vPlayerVehiclePosition = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
											VECTOR DoorCoordsPed0, DoorCoordsPed1
											DoorCoordsPed0 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << -1.5, -1.5, 0 >>) //Back Left
											DoorCoordsPed1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << 1.5, 0, 0 >>) //Front Right
		//									GET_CLOSEST_VEHICLE_DOOR(pedCriminal[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 1, DoorPicked, DoorCoordsPed0)
		//									GET_CLOSEST_VEHICLE_DOOR(pedCriminal[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), ENUM_TO_INT(DoorPicked), DoorPicked, DoorCoordsPed1)
		//									SET_PED_ENTER_CLOSEST_VEHICLE_DOOR(TheCriminals[0], TheVehicle, 0, DoorPicked, PEDMOVE_SPRINT, FALSE, ECF_RESUME_IF_INTERRUPTED, pedShopkeep)
		//									SET_PED_ENTER_CLOSEST_VEHICLE_DOOR(TheCriminals[1], TheVehicle, 0, DoorPicked, PEDMOVE_SPRINT, FALSE, ECF_RESUME_IF_INTERRUPTED, pedShopkeep)
											OPEN_SEQUENCE_TASK(seq)
												TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
												TASK_AIM_GUN_AT_ENTITY(NULL, pedShopkeep, 1500)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, DoorCoordsPed0, pedShopkeep, PEDMOVE_SPRINT, FALSE)//, 2.5, 3)  //Back Left
												TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
		//										TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 3)
		//										TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 2)
		//										TASK_ENTER_VEHICLE(NULL, TheVehicle, -1, VS_ANY_PASSENGER, PEDMOVE_SPRINT, ECF_RESUME_IF_INTERRUPTED)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(pedCriminal[0], seq)
											CLEAR_SEQUENCE_TASK(seq)
											
											OPEN_SEQUENCE_TASK(seq)
												TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
												TASK_AIM_GUN_AT_ENTITY(NULL, pedShopkeep, 2500)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, DoorCoordsPed1, pedShopkeep, PEDMOVE_SPRINT, FALSE)//, 2.5, 3) //Front Right
												TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
		//										TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 3)
		//										TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 2)
		//										TASK_ENTER_VEHICLE(NULL, TheVehicle, -1, VS_ANY_PASSENGER, PEDMOVE_SPRINT, ECF_RESUME_IF_INTERRUPTED)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(pedCriminal[1], seq)
											CLEAR_SEQUENCE_TASK(seq)
		//									SET_PED_KEEP_TASK(pedCriminal[0], TRUE)
		//									SET_PED_KEEP_TASK(pedCriminal[1], TRUE)
										ELSE
		//									CLEAR_PED_TASKS(pedCriminal[0])
		//									CLEAR_PED_TASKS(pedCriminal[1])
											SET_PED_AS_GROUP_MEMBER(pedCriminal[0], PLAYER_GROUP_ID())
											SET_PED_AS_GROUP_MEMBER(pedCriminal[1], PLAYER_GROUP_ID())
											SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[0], VS_BACK_LEFT)//|VS_BACK_RIGHT)
											SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[1], VS_FRONT_RIGHT)//|VS_BACK_LEFT)
											PED_INDEX pedTemp
											IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) = BUS
												IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
													pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
													IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
														SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
														DELETE_PED(pedTemp)
													ENDIF
												ENDIF
												IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
													pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
													IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
														SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
														DELETE_PED(pedTemp)
													ENDIF
												ENDIF
											ENDIF
											SET_PED_NEVER_LEAVES_GROUP(pedCriminal[0], TRUE)
											SET_PED_NEVER_LEAVES_GROUP(pedCriminal[1], TRUE)
		//									TASK_GO_TO_ENTITY(pedCriminal[0], PLAYER_PED_ID())
		//									TASK_GO_TO_ENTITY(pedCriminal[1], PLAYER_PED_ID())
		//									TASK_ENTER_VEHICLE(NULL, TheVehicle, -1, VS_ANY_PASSENGER, PEDMOVE_SPRINT, ECF_RESUME_IF_INTERRUPTED)
										ENDIF
//									ENDIF
	//								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 2)
	//								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), pedShopkeep, PEDMOVE_SPRINT, FALSE, 2)
	//								SET_PED_ENTER_CLOSEST_VEHICLE_DOOR(TheCriminals[0], TheVehicle, 1+ENUM_TO_INT(DoorPicked), DoorPicked, PEDMOVE_SPRINT, TRUE, ECF_RESUME_IF_INTERRUPTED, pedShopkeep)
	//								PLAY_AMBIENT_DIALOGUE_LINE(GetawayDriverConversation, TheCriminals[0], "REGETAU", "REAWA_DOWN")
	//								IF DOES_ENTITY_EXIST(pedShopkeep)
	//									REQUEST_ANIM_DICT("random@getawaydriver")
	//									IF HAS_ANIM_DICT_LOADED("random@getawaydriver")
	//										IF NOT IS_PED_INJURED(pedShopkeep)
	//											TASK_PLAY_ANIM(pedShopkeep, "random@getawaydriver", "idle_2_hands_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
												curstate = EUpdate_GetInCar
	//										ENDIF
	//									ENDIF
	//								ENDIF
								ELIF NOT bHasCriminalAskedForCar
//									PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ GET A FUCKING CAR @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CAR", CONV_PRIORITY_AMBIENT_HIGH)
										TASK_LOOK_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
										bHasCriminalAskedForCar = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), TheCriminals[1], << 10, 10, 10 >>)
//						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ GET A FUCKING CAR @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
						IF NOT bHasCriminalAskedForCar
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_NDCAR", CONV_PRIORITY_AMBIENT_HIGH)
								TASK_LOOK_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
								bHasCriminalAskedForCar = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bHasCriminalAskedForCar
				OR bHasCriminalAskedForCarAgain
					IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
						IF iStoppingTimer = -1
							iStoppingTimer = GET_GAME_TIMER()
							
						ENDIF
					ELSE
						iStoppingTimer = -1
					ENDIF
					
					IF (iStoppingTimer != -1 AND GET_GAME_TIMER() > iStoppingTimer + 10000)
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ GAME_TIMER() > iStoppingTimer + 10000 @@@@@@@@@@@@@@@@@@@@ \n")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS", CONV_PRIORITY_AMBIENT_HIGH)
						IF NOT IS_PED_INJURED(pedCriminal[0])
							TASK_SMART_FLEE_PED(pedCriminal[0], PLAYER_PED_ID(), 200, -1)
							SET_PED_KEEP_TASK(pedCriminal[0], TRUE)
							WAIT(0)
							SET_PED_AS_NO_LONGER_NEEDED(pedCriminal[0])
						ENDIF
						IF NOT IS_PED_INJURED(pedCriminal[1])
							TASK_SMART_FLEE_PED(pedCriminal[1], PLAYER_PED_ID(), 200, -1)
							SET_PED_KEEP_TASK(pedCriminal[1], TRUE)
							WAIT(0)
							SET_PED_AS_NO_LONGER_NEEDED(pedCriminal[1])
						ENDIF
						waitTime = 0
						curstate = EUpdate_Finish
					ENDIF
				ENDIF
				
				AddblipCriminal(TheCriminals, 120.0)
				
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
				OR IS_COP_PED_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
				OR IS_COP_VEHICLE_IN_AREA_3D(vInput - << 40, 40, 40 >>, vInput + << 40, 40, 40 >>)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						TASK_SMART_FLEE_PED(TheCriminals[0], PLAYER_PED_ID(), 200, -1)
						TASK_SMART_FLEE_PED(TheCriminals[1], PLAYER_PED_ID(), 200, -1)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_POLICE", CONV_PRIORITY_AMBIENT_HIGH)
						curstate = EUpdate_Finish
					ENDIF
				ENDIF
				
			BREAK
			
			CASE EUpdate_GetInCar
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_GetInCar @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				curstate = EUpdate_KillOneCriminal
			BREAK
			
			CASE EUpdate_KillOneCriminal
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_KillOneCriminal @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ENDIF
				#ENDIF
				
				waitTime = 0
				
//				IF NOT IS_PED_INJURED(pedCriminal[0])
//				AND NOT IS_PED_INJURED(pedCriminal[1])
//					SET_PED_MIN_MOVE_BLEND_RATIO(pedCriminal[0], PEDMOVEBLENDRATIO_RUN)
//					SET_PED_MIN_MOVE_BLEND_RATIO(pedCriminal[1], PEDMOVEBLENDRATIO_RUN)
//				ENDIF
				
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF NOT ARE_PEDS_INJURED_OR_IN_VEHICLE(TheCriminals, GET_PLAYERS_LAST_VEHICLE(), 120.0)
					OR NOT bLeaveEarly
						IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPlayerVehiclePosition, << 2, 2, 2 >>)
							// OR JUST MAKE THE CRIMINALS GROUP MEMBERS RIGHT AWAY
							IF NOT IS_PED_INJURED(pedCriminal[0])
							AND NOT IS_PED_INJURED(pedCriminal[1])
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
									IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
									AND IS_VEHICLE_ON_ALL_WHEELS(GET_PLAYERS_LAST_VEHICLE())
										REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
										WHILE NOT HAS_VEHICLE_ASSET_LOADED(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
											WAIT(0)
										ENDWHILE
										IF NOT IS_PED_INJURED(pedCriminal[0])
										AND NOT IS_PED_INJURED(pedCriminal[1])
											IF NOT IS_PED_IN_GROUP(pedCriminal[0])
											AND NOT IS_PED_IN_GROUP(pedCriminal[1])
//												IF NOT IS_PED_INJURED(pedShopkeep)
//				//									DOORFLAGS DoorPicked
//													vPlayerVehiclePosition = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
//													VECTOR DoorCoordsPed0, DoorCoordsPed1
//													DoorCoordsPed0 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << -1.5, -1.5, 0 >>) //Back Left
//													DoorCoordsPed1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << 1.5, 1.5, 0 >>) //Front Right
//													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(pedCriminal[0], DoorCoordsPed0, pedShopkeep, PEDMOVE_SPRINT, FALSE, 2.5, 3) //Back Left
//													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(pedCriminal[1], DoorCoordsPed1, pedShopkeep, PEDMOVE_SPRINT, FALSE, 2.5, 3) //Front Right
//												ELSE
//													IF NOT IS_PED_IN_GROUP(pedCriminal[0])
//													AND NOT IS_PED_IN_GROUP(pedCriminal[1])
//														TASK_ENTER_VEHICLE(pedCriminal[0], GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT)
//														TASK_ENTER_VEHICLE(pedCriminal[1], GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
														CLEAR_PED_TASKS(pedCriminal[0])
														CLEAR_PED_TASKS(pedCriminal[1])
														TASK_LOOK_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
														SET_PED_AS_GROUP_MEMBER(pedCriminal[0], PLAYER_GROUP_ID())
														SET_PED_AS_GROUP_MEMBER(pedCriminal[1], PLAYER_GROUP_ID())
														SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[0], VS_BACK_LEFT)//|VS_BACK_RIGHT)
														SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedCriminal[1], VS_FRONT_RIGHT)//|VS_BACK_LEFT)
														PED_INDEX pedTemp
														IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) = BUS
															IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
																pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_BACK_LEFT)
																IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
																	SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
																	DELETE_PED(pedTemp)
																ENDIF
															ENDIF
															IF NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
																pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_FRONT_RIGHT)
																IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTemp)
																	SET_ENTITY_AS_MISSION_ENTITY(pedTemp)
																	DELETE_PED(pedTemp)
																ENDIF
															ENDIF
														ENDIF
														SET_PED_NEVER_LEAVES_GROUP(pedCriminal[0], TRUE)
														SET_PED_NEVER_LEAVES_GROUP(pedCriminal[1], TRUE)
//													ENDIF
//												ENDIF
											ENDIF
										ENDIF
									ELIF NOT bHasCriminalAskedForCar
//										PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ GET A FUCKING CAR @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CAR", CONV_PRIORITY_AMBIENT_HIGH)
											bHasCriminalAskedForCar = TRUE
										ENDIF
									ENDIF
								ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), TheCriminals[1], << 10, 10, 10 >>)
//									PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ GET A FUCKING CAR @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
									IF NOT bHasCriminalAskedForCar
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_NDCAR", CONV_PRIORITY_AMBIENT_HIGH)
											bHasCriminalAskedForCar = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedCriminal[0])
					IF NOT IS_PED_IN_GROUP(pedCriminal[0])
						IF NOT IS_PED_INJURED(pedShopkeep)
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
	//							IF NOT IS_ENTITY_AT_ENTITY(pedCriminal[0], pedShopkeep, << 10, 10, 10 >>)
	//							OR IS_ENTITY_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), << 3, 3, 3 >>)
								IF (GET_SCRIPT_TASK_STATUS(pedCriminal[0], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK AND GET_SCRIPT_TASK_STATUS(pedCriminal[0], SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK)
								OR GET_SCRIPT_TASK_STATUS(pedCriminal[0], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
	//								CLEAR_PED_TASKS(pedCriminal[0])
//									TASK_ENTER_VEHICLE(pedCriminal[0], GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT)
									SET_PED_AS_GROUP_MEMBER(pedCriminal[0], PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedCriminal[0], TRUE)
								ENDIF
							ENDIF
						ELSE
//							CLEAR_PED_TASKS(pedCriminal[0])
							SET_PED_AS_GROUP_MEMBER(pedCriminal[0], PLAYER_GROUP_ID())
							SET_PED_NEVER_LEAVES_GROUP(pedCriminal[0], TRUE)
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(pedCriminal[0])
							AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPlayerVehiclePosition, << 2, 2, 2 >>)
							AND NOT IS_ENTITY_AT_COORD(pedCriminal[0], GET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE()), << 2.5, 2.5, 2.5 >>)
								SET_PED_MIN_MOVE_BLEND_RATIO(pedCriminal[0], PEDMOVEBLENDRATIO_RUN)
							ENDIF
						ENDIF
						IF GET_SCRIPT_TASK_STATUS(pedCriminal[0], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = PERFORMING_TASK
						OR GET_SCRIPT_TASK_STATUS(pedCriminal[0], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = WAITING_TO_START_TASK
//							CLEAR_PED_TASKS(pedCriminal[0])
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedCriminal[1])
					IF NOT IS_PED_IN_GROUP(pedCriminal[1])
						IF NOT IS_PED_INJURED(pedShopkeep)
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
	//							IF IS_ENTITY_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), << 3, 3, 3 >>)
								IF (GET_SCRIPT_TASK_STATUS(pedCriminal[1], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK AND GET_SCRIPT_TASK_STATUS(pedCriminal[1], SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK)
								OR GET_SCRIPT_TASK_STATUS(pedCriminal[1], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
	//								CLEAR_PED_TASKS(pedCriminal[1])
//									TASK_ENTER_VEHICLE(pedCriminal[1], GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
									SET_PED_AS_GROUP_MEMBER(pedCriminal[1], PLAYER_GROUP_ID())
									SET_PED_NEVER_LEAVES_GROUP(pedCriminal[1], TRUE)
								ENDIF
							ENDIF
						ELSE
//							CLEAR_PED_TASKS(pedCriminal[1])
							SET_PED_AS_GROUP_MEMBER(pedCriminal[1], PLAYER_GROUP_ID())
							SET_PED_NEVER_LEAVES_GROUP(pedCriminal[1], TRUE)
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(pedCriminal[1])
							AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPlayerVehiclePosition, << 2, 2, 2 >>)
							AND NOT IS_ENTITY_AT_COORD(pedCriminal[1], GET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE()), << 2.5, 2.5, 2.5 >>)
								SET_PED_MIN_MOVE_BLEND_RATIO(pedCriminal[1], PEDMOVEBLENDRATIO_RUN)
							ENDIF
						ENDIF
						IF GET_SCRIPT_TASK_STATUS(pedCriminal[1], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = PERFORMING_TASK
						OR GET_SCRIPT_TASK_STATUS(pedCriminal[1], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = WAITING_TO_START_TASK
//							CLEAR_PED_TASKS(pedCriminal[1])
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bLeaveEarly
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND NOT IS_PED_INJURED(pedCriminal[0])
						AND NOT IS_PED_INJURED(pedCriminal[1])
							IF IS_PED_IN_VEHICLE(pedCriminal[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							AND NOT IS_PED_IN_VEHICLE(pedCriminal[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCriminal[1], << 20, 20, 20 >>)
								bLeaveEarly = TRUE
							ENDIF
							IF IS_PED_IN_VEHICLE(pedCriminal[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							AND NOT IS_PED_IN_VEHICLE(pedCriminal[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCriminal[0], << 20, 20, 20 >>)
								bLeaveEarly = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF ARE_PEDS_INJURED_OR_IN_VEHICLE(TheCriminals, GET_PLAYERS_LAST_VEHICLE(), 120.0)
					OR bLeaveEarly
	//					SET_PED_RELATIONSHIP_GROUP_HASH(TheCriminals[0], rghCriminals)
						
						IF NOT IS_PED_INJURED(pedShopkeep)
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(NULL, "random@getawaydriver", "hands_up_2_idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedShopkeep, seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedShopkeep, TRUE)
	//						REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 3)
							SET_PED_AS_NO_LONGER_NEEDED(pedShopkeep) //SET ENTITY AS NO LONGER NEEDED! TURN OFF AGGRO CHECKS FOR THIS GUY
						ENDIF
						
						REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
						
						IF NOT IS_SPHERE_VISIBLE(<< 7.3162, -1544.8378, 28.2558 >>, 3)
							vehTempCop = CREATE_VEHICLE(POLICE3, << 7.3162, -1544.8378, 28.2558 >>, 251.9956)
							pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER)
							pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
							GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							SET_PED_TARGET_LOSS_RESPONSE(pedTempCop[0], TLR_EXIT_TASK)
						ENDIF
						IF NOT IS_SPHERE_VISIBLE(<< 91.6602, -1541.8986, 28.3422 >>, 3)
							vehTempCop = CREATE_VEHICLE(POLICE3, << 91.6602, -1541.8986, 28.3422 >>, 127.3064)
							pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER)
							pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
							GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							SET_PED_TARGET_LOSS_RESPONSE(pedTempCop[0], TLR_EXIT_TASK)
						ENDIF
						IF NOT IS_SPHERE_VISIBLE(<< 32.8029, -1594.4001, 28.4262 >>, 3)
							vehTempCop = CREATE_VEHICLE(POLICE3, << 32.8029, -1594.4001, 28.4262 >>, 337.7429)
							pedTempCop[0] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER)
							pedTempCop[1] = CREATE_PED_INSIDE_VEHICLE(vehTempCop, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
							GIVE_WEAPON_TO_PED(pedTempCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							GIVE_WEAPON_TO_PED(pedTempCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
							SET_PED_TARGET_LOSS_RESPONSE(pedTempCop[0], TLR_EXIT_TASK)
						ENDIF
						
						PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_GETAWAY_01", 0.0) 
//						SET_DISPATCH_IDEAL_SPAWN_DISTANCE(50)
						SET_VEHICLE_IS_WANTED(GET_PLAYERS_LAST_VEHICLE(), TRUE)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rghCriminals)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminals, RELGROUPHASH_COP)
						WAIT(1000)
						IF NOT bLeaveEarly
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS2", CONV_PRIORITY_AMBIENT_HIGH)//REAWA_DRIVE
						ENDIF
						IF NOT IS_PED_INJURED(TheCriminals[I])
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(TheCriminals[I])
						ENDIF
//						WAIT(2500)
						
						iAttackTimer = GET_GAME_TIMER()
						curstate = EUpdate_PrintGod
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 95.0746, -1520.0424, 28.3593 >>, << -5.9698, -1641.8188, 38.4042 >>, 98.1875)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						TASK_SMART_FLEE_PED(TheCriminals[0], PLAYER_PED_ID(), 200, -1)
						TASK_SMART_FLEE_PED(TheCriminals[1], PLAYER_PED_ID(), 200, -1)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS", CONV_PRIORITY_AMBIENT_HIGH)
						curstate = EUpdate_Finish
					ENDIF
				ENDIF
				
				AddblipCriminal(TheCriminals, 120.0)
				followPlayerInWrongVehicle()
				
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
			BREAK
			
			CASE EUpdate_PrintGod
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_PrintGod @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ENDIF
				#ENDIF
				
				RemoveBlip(biDestinationBlip)
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
				
				IF DO_TIMER(iAttackTimer, 3000)
//					PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 1)
					curstate = EUpdate_LosePolice
				ENDIF
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
			BREAK
			
			CASE EUpdate_LosePolice
				RunChatOnWayHome(TheCriminals)
				
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_LosePolice @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ENDIF
				#ENDIF
				
//				SET_DISPATCH_IDEAL_SPAWN_DISTANCE(150)
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, rghCriminals)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCriminals, RELGROUPHASH_COP)
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						SET_VEHICLE_IS_WANTED(GET_PLAYERS_LAST_VEHICLE(), FALSE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(vehTempCop)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTempCop)
					ENDIF
					IF NOT IS_PED_INJURED(pedTempCop[0])
						SET_PED_AS_NO_LONGER_NEEDED(pedTempCop[0])
					ENDIF
					IF NOT IS_PED_INJURED(pedTempCop[1])
						SET_PED_AS_NO_LONGER_NEEDED(pedTempCop[1])
					ENDIF
					RemoveBlip(biDestinationBlip)
//					IF DOES_BLIP_EXIST(blipCult)
//						REMOVE_BLIP(blipCult)
//					ENDIF
					curstate = EUpdate_WorkoutEndDestination
				ENDIF
				
				REPEAT COUNT_OF(TheCriminals) I
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					AND NOT IS_PED_INJURED(TheCriminals[I])
						IF NOT IS_PED_SITTING_IN_VEHICLE(TheCriminals[I], GET_PLAYERS_LAST_VEHICLE())
							IF NOT DOES_BLIP_EXIST(blipCriminal[I])
								blipCriminal[I] = CREATE_BLIP_FOR_PED(TheCriminals[I])
								RemoveBlip(biDestinationBlip)
//								IF DOES_BLIP_EXIST(blipCult)
//									REMOVE_BLIP(blipCult)
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				AddblipCriminal(TheCriminals, 120.0)
				followPlayerInWrongVehicle()
				
				IF IS_CRIMINAL_CLOSE_TO_POLICE_STATION()
					curstate = EUpdate_PoliceStation
				ENDIF
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
			BREAK
			
			CASE EUpdate_WorkoutEndDestination
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_WorkoutEndDestination @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rghCriminals)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminals, RELGROUPHASH_COP)
					RemoveBlip(biDestinationBlip)
//					IF DOES_BLIP_EXIST(blipCult)
//						REMOVE_BLIP(blipCult)
//					ENDIF
					curstate = EUpdate_PrintGod
				ELSE
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, rghCriminals)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCriminals, RELGROUPHASH_COP)
					PlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					ShortestDestination = 9999
					INT index
					REPEAT COUNT_OF(vPossibleDestinationPos) index
						IF VDIST(PlayerPos, vPossibleDestinationPos[index]) < ShortestDestination
							ShortestDestination = VDIST(PlayerPos, vPossibleDestinationPos[index])
							FinalDestination = index
						ENDIF
					ENDREPEAT
					IF ARE_VECTORS_EQUAL(vPossibleDestinationPos[FinalDestination], <<0,0,0>>)
						FinalDestination = 0
					ENDIF
					biDestinationBlip = CREATE_BLIP_FOR_COORD(vPossibleDestinationPos[FinalDestination], TRUE)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					AND NOT IS_CULT_FINISHED()
						IF NOT DOES_BLIP_EXIST(blipCult)
							blipCult = CREATE_BLIP_FOR_COORD(vCult)
							SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
							PRINT_CULT_HELP()
						ENDIF
					ENDIF
					
					iAttackTimer = GET_GAME_TIMER()
					
					IF NOT bToldToDriveHome
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_HOM", CONV_PRIORITY_AMBIENT_HIGH) //DRIVE DRIVE DRIVE!
						bToldToDriveHome = TRUE
					ENDIF
//					PRINT_NOW("Getaway_end1", DEFAULT_GOD_TEXT_TIME, 1)
					curstate = EUpdate_WaitTillAtDestination
				ENDIF
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
			BREAK
			
			CASE EUpdate_WaitTillAtDestination
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ EUpdate_WaitTillAtDestination @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				waitTime = 0
				
				RunChatOnWayHomePitch(TheCriminals)
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					RemoveBlip(biDestinationBlip)
//					IF DOES_BLIP_EXIST(blipCult)
//						REMOVE_BLIP(blipCult)
//					ENDIF
					curstate = EUpdate_PrintGod
				ELSE
				
					#IF IS_DEBUG_BUILD
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vPossibleDestinationPos[FinalDestination])
						ENDIF
					#ENDIF
					
					IF READY_FOR_CULT_DIALOGUE(vPossibleDestinationPos[FinalDestination])
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CULT", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					IF READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF GET_RANDOM_BOOL()
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CULT2", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_CULT3", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
					
//					IF bLeaveEarly = FALSE
//						FOR I = 0 TO COUNT_OF(TheCriminals)-1	
//							IF NOT IS_PED_INJURED(TheCriminals[I])
//								IF NOT IS_PED_IN_VEHICLE(TheCriminals[I], TheVehicle)
//									PRINTSTRING("***********************************")
//									PRINTNL()
//									PRINTSTRING("Ending GatawayDriver: One of the two criminals is not in the car")
//									PRINTNL()
//									PRINTSTRING("***********************************")
//									curstate = EUpdate_Finish
//								ENDIF
//							ENDIF
//						ENDFOR
//					ELSE
//						IF NOT IS_PED_INJURED(TheCriminals[IndexOfLonePedInCar])
//							IF NOT IS_PED_IN_VEHICLE(TheCriminals[IndexOfLonePedInCar], TheVehicle)
//								PRINTSTRING("***********************************")
//								PRINTNL()
//								PRINTSTRING("Ending GatawayDriver: The One Remaining Criminal is not in the car")
//								PRINTNL()
//								PRINTSTRING("***********************************")
//								curstate = EUpdate_Finish
//							ENDIF
//						ENDIF
//					ENDIF
					
//					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF NOT IS_PED_INJURED(pedCriminal[0])
					AND NOT IS_PED_INJURED(pedCriminal[1])
//						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
//						AND IS_PED_SITTING_IN_VEHICLE(pedCriminal[0], GET_PLAYERS_LAST_VEHICLE())
//						AND IS_PED_SITTING_IN_VEHICLE(pedCriminal[1], GET_PLAYERS_LAST_VEHICLE())
//							IF NOT DOES_BLIP_EXIST(biDestinationBlip)
//								biDestinationBlip = CREATE_BLIP_FOR_COORD(vPossibleDestinationPos[FinalDestination], TRUE)
//							ENDIF
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//								IF NOT DOES_BLIP_EXIST(blipCult)
//									blipCult = CREATE_BLIP_FOR_COORD(vCult)
//									SET_BLIP_SPRITE(blipCult, RADAR_TRACE_ALTRUIST)
//									PRINT_CULT_HELP()
//								ENDIF
//							ENDIF
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPossibleDestinationPos[FinalDestination], g_vAnyMeansLocate, TRUE)
							AND IS_ENTITY_AT_COORD(pedCriminal[0], vPossibleDestinationPos[FinalDestination], << 20, 20, 20 >>)
							AND IS_ENTITY_AT_COORD(pedCriminal[1], vPossibleDestinationPos[FinalDestination], << 20, 20, 20 >>)
//							AND IS_VEHICLE_ON_ALL_WHEELS(GET_PLAYERS_LAST_VEHICLE())
								RemoveBlip(biDestinationBlip)
//								IF DOES_BLIP_EXIST(blipCult)
//									REMOVE_BLIP(blipCult)
//								ENDIF
								FOR I = 0 TO COUNT_OF(TheCriminals)-1
									IF bIsGetawayPlayerControlOn
										BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 2)
										WAIT(DEFAULT_CAR_STOPPING_TO_CUTSCENE)
										bIsGetawayPlayerControlOn = FALSE
									ENDIF
									IF NOT IS_PED_INJURED(TheCriminals[I])
										CLEAR_ENTITY_LAST_DAMAGE_ENTITY(TheCriminals[I])
										REMOVE_PED_FROM_GROUP(TheCriminals[I])
										REMOVE_GROUP(aGroup)
									ENDIF
								ENDFOR
								
//								ExitCams.CamFov = 37
//								ExitCams.ShotTime = 20000
								
	//							PRINT_NOW("Getaway_Stop", DEFAULT_GOD_TEXT_TIME, 1)
								curstate = Eupdate_RunCutscene
							ENDIF
//						ENDIF
					ENDIF
				ENDIF
				
//				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
//					IF NOT IS_PED_INJURED(pedCriminal[0])
//					AND NOT IS_PED_INJURED(pedCriminal[1])
//						IF IS_ENTITY_AT_COORD(pedCriminal[0], vPossibleDestinationPos[FinalDestination], g_vAnyMeansLocate, TRUE)
//						AND IS_ENTITY_AT_COORD(pedCriminal[1], vPossibleDestinationPos[FinalDestination], g_vAnyMeansLocate)
//							curstate = Eupdate_RunCutscene1
//						ENDIF
//					ENDIF
//				ENDIF
				
				REPEAT COUNT_OF(TheCriminals) I
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					AND NOT IS_PED_INJURED(TheCriminals[I])
						IF NOT IS_PED_SITTING_IN_VEHICLE(TheCriminals[I], GET_PLAYERS_LAST_VEHICLE())
							IF NOT DOES_BLIP_EXIST(blipCriminal[I])
								blipCriminal[I] = CREATE_BLIP_FOR_PED(TheCriminals[I])
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				AddblipCriminal(TheCriminals, 120.0)
				followPlayerInWrongVehicle()
				
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
					IF iStoppingTimer = -1
						iStoppingTimer = GET_GAME_TIMER()
					ENDIF
				ELSE
					iStoppingTimer = -1
				ENDIF
				
				IF (iStoppingTimer != -1 AND GET_GAME_TIMER() > iStoppingTimer + 60000)
					PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ GAME_TIMER() > iStoppingTimer + 60000 @@@@@@@@@@@@@@@@@@@@ \n")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS", CONV_PRIORITY_AMBIENT_HIGH)
					IF NOT IS_PED_INJURED(pedCriminal[0])
						TASK_SMART_FLEE_PED(pedCriminal[0], PLAYER_PED_ID(), 200, -1)
						SET_PED_KEEP_TASK(pedCriminal[0], TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(pedCriminal[1])
						TASK_SMART_FLEE_PED(pedCriminal[1], PLAYER_PED_ID(), 200, -1)
						SET_PED_KEEP_TASK(pedCriminal[1], TRUE)
					ENDIF
					waitTime = 0
					curstate = EUpdate_Finish
				ENDIF
				IF IS_CRIMINAL_CLOSE_TO_POLICE_STATION()
					curstate = EUpdate_PoliceStation
				ENDIF
				IF HAS_CRIMINAL_BEEN_ATTACKED()
					curstate = EUpdate_CriminalAttacked
				ENDIF
			BREAK
			
			CASE EUpdate_PoliceStation
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ EUpdate_PoliceStation @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				SpawnCops()
				IF bCopsHaveSpawned
					REMOVE_PED_FROM_GROUP(pedCriminal[0])
					REMOVE_PED_FROM_GROUP(pedCriminal[1])
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS3", CONV_PRIORITY_AMBIENT_HIGH)
					IF NOT IS_PED_INJURED(TheCriminals[0])
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(TheCriminals[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(TheCriminals[0], TRUE)
//						WAIT(0)
//						SET_PED_AS_NO_LONGER_NEEDED(TheCriminals[0])
					ENDIF
					IF NOT IS_PED_INJURED(TheCriminals[1])
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL, 500, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(TheCriminals[1], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(TheCriminals[1], TRUE)
//						WAIT(0)
//						SET_PED_AS_NO_LONGER_NEEDED(TheCriminals[1])
					ENDIF
//					WAIT(10000)
					curstate = EUpdate_Finish
				ENDIF
			BREAK
			
			CASE Eupdate_RunCutscene
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ Eupdate_RunCutscene @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 5.0
//					vVehiclePosition = GET_ENTITY_COORDS(TheVehicle)
//					
//					CameraRotation = vVehiclePosition
//					CameraRotation.x -= 0.75
//					CameraRotation.y += 0.1
//					CameraRotation.z += 0.8
//					
//					Run_camera_Attach_car(ExitCams, TheVehicle, << 1, 3, 1 >>,CameraRotation, FALSE)
//					SET_CUTSCENE_RUNNING(TRUE)
					
					FOR I = 0 TO COUNT_OF(TheCriminals)-1	
						IF NOT IS_PED_INJURED(TheCriminals[I])
							IF GET_SCRIPT_TASK_STATUS(TheCriminals[I], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							OR GET_SCRIPT_TASK_STATUS(TheCriminals[I], SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
								IF NOT IS_PED_INJURED(TheCriminals[I])
//									random_int += 800
									
//									OPEN_SEQUENCE_TASK(SeqNPC)
//										IF I = 0
//											TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(500, 900))
//										ENDIF
//										TASK_LEAVE_VEHICLE(TheCriminals[0], TheVehicle)
//										TASK_LEAVE_ANY_VEHICLE(TheCriminals[1], 1000)
//									CLOSE_SEQUENCE_TASK(SeqNPC)
//									TASK_PERFORM_SEQUENCE(TheCriminals[I], SeqNPC)
//									CLEAR_SEQUENCE_TASK(SeqNPC)
									SeqNPC = NULL
									iEndCutsceneIndex = I
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					curstate = Eupdate_RunCutscene1
				ENDIF
			BREAK
			
			CASE Eupdate_RunCutscene1
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ Eupdate_RunCutscene1 @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				
//				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					bTaskGiven = TRUE
//					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1000,FALSE,TRUE)
//					IF NOT IS_PED_INJURED(pedCriminal[0])
//					AND NOT IS_PED_INJURED(pedCriminal[1])
//						DELETE_PED(pedCriminal[0])
//						DELETE_PED(pedCriminal[1])
//					ENDIF
//					curstate = EUpdate_ControlBackOn
//				ENDIF
				
//				FOR I = 0 TO COUNT_OF(TheCriminals)-1
//					IF I <> iEndCutsceneIndex
//						IF NOT IS_PED_INJURED(TheCriminals[I])
//							IF NOT IS_PED_IN_VEHICLE(TheCriminals[I], TheVehicle)
//								OPEN_SEQUENCE_TASK(SeqNPC)
//									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPossibleAlleyDestintation[FinalDestination], PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
//									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
//								CLOSE_SEQUENCE_TASK(SeqNPC)
//								TASK_PERFORM_SEQUENCE(TheCriminals[I], SeqNPC)
//								CLEAR_SEQUENCE_TASK(SeqNPC)
//								SeqNPC = NULL
//								SET_PED_KEEP_TASK(TheCriminals[I], TRUE)
//								TASK_LOOK_AT_ENTITY(TheCriminals[I], PLAYER_PED_ID(), 4000)
//							ENDIF
//						ENDIF
//					ENDIF
					
					REPEAT COUNT_OF(TheCriminals) I
						IF NOT IS_PED_INJURED(TheCriminals[0])
	//					AND NOT IS_ENTITY_DEAD(TheVehicle)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), TheCriminals[I], 4000, SLF_WHILE_NOT_IN_FOV)
	//						IF NOT IS_PED_IN_VEHICLE(TheCriminals[iEndCutsceneIndex], TheVehicle)
								OPEN_SEQUENCE_TASK(SeqNPC)
//									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 500)
	//								TASK_PLAY_ANIM(NULL, Asset.sGestAnimDict, "its_done")
									TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_WARP_IF_DOOR_IS_BLOCKED|ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPossibleAlleyDestintation[FinalDestination], PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
								CLOSE_SEQUENCE_TASK(SeqNPC)
								TASK_PERFORM_SEQUENCE(TheCriminals[0], SeqNPC)
								CLEAR_SEQUENCE_TASK(SeqNPC)
								IF NOT IS_PED_INJURED(TheCriminals[1])
									OPEN_SEQUENCE_TASK(SeqNPC)
										TASK_LEAVE_ANY_VEHICLE(NULL, 2000, ECF_WARP_IF_DOOR_IS_BLOCKED|ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
//										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//										TASK_PLAY_ANIM(NULL, "random@getawaydriver", "gesture_nod_yes_hard", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_SECONDARY|AF_UPPERBODY)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPossibleAlleyDestintation[FinalDestination], PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
										TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
									CLOSE_SEQUENCE_TASK(SeqNPC)
									TASK_PERFORM_SEQUENCE(TheCriminals[1], SeqNPC)
									CLEAR_SEQUENCE_TASK(SeqNPC)
								ENDIF
								SeqNPC = NULL
								SET_PED_KEEP_TASK(TheCriminals[I], TRUE)
								TASK_LOOK_AT_ENTITY(TheCriminals[I], PLAYER_PED_ID(), 4000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								WAIT(0)
								IF iEndCutsceneIndex > 0
	//								CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REGETA_STOP2", CONV_PRIORITY_AMBIENT_HIGH)
									CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_STOP2", CONV_PRIORITY_AMBIENT_HIGH)
								ELSE
	//								CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REGETA_STOP", CONV_PRIORITY_AMBIENT_HIGH)
									CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_STOP", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								curstate = EUpdate_ControlBackOn
	//						ENDIF
						ENDIF
					ENDREPEAT
//				ENDFOR
				
//				IF ARE_PEDS_INJURED_OR_NOT_IN_VEHICLE(TheCriminals, TheVehicle)
//					curstate = EUpdate_ControlBackOn
//				ENDIF
			BREAK
			
			CASE EUpdate_ControlBackOn
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ EUpdate_ControlBackOn @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF NOT bTaskGiven
					IF NOT IS_PED_INJURED(TheCriminals[1])
	//					IF GET_SEQUENCE_PROGRESS(TheCriminals[iEndCutsceneIndex]) > 1
	//					IF GET_SCRIPT_TASK_STATUS(TheCriminals[iEndCutsceneIndex], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
	//						IF NOT IS_ENTITY_DEAD(TheVehicle)
								IF NOT bIsGetawayPlayerControlOn
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									bIsGetawayPlayerControlOn = TRUE
								ENDIF
								CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1000, FALSE, TRUE)
	//							OPEN_SEQUENCE_TASK(SeqNPC)
	//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPossibleAlleyDestintation[FinalDestination], PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
	//								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
	//							CLOSE_SEQUENCE_TASK(SeqNPC)
	//							TASK_PERFORM_SEQUENCE(TheCriminals[iEndCutsceneIndex], SeqNPC)
	//							CLEAR_SEQUENCE_TASK(SeqNPC)
	//							SeqNPC = NULL
	//							
								bTaskGiven = TRUE
	//							SET_PED_KEEP_TASK(TheCriminals[iEndCutsceneIndex], TRUE)
	//						ENDIF
	//					ENDIF
					ENDIF
				
				ELSE
//					SET_CUTSCENE_RUNNING(FALSE)
					IF NOT IS_PED_INJURED(TheCriminals[0])
					AND NOT IS_PED_INJURED(TheCriminals[1])
						IF NOT IS_PED_IN_ANY_VEHICLE(TheCriminals[0])
						AND NOT IS_PED_IN_ANY_VEHICLE(TheCriminals[1])
							CLEAR_PRINTS()
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
								WAIT(0)
							ENDWHILE
							SET_HEIST_CREW_MEMBER_UNLOCKED(CM_GUNMAN_G_PACKIE_UNLOCK)
							
							IF GET_MISSION_COMPLETE_STATE( SP_HEIST_FINALE_2_INTRO )
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE, FALSE )
							ENDIF
	
							SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_CRIMINALS)
							RANDOM_EVENT_PASSED()
							RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
							curstate = EUpdate_Finish
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE EUpdate_LostCriminals
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ EUpdate_LostCriminals @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				bTaskGiven = TRUE
				curstate = EUpdate_Finish
			BREAK
			
			CASE EUpdate_CalledPolice
				
			BREAK
			
			CASE EUpdate_CriminalAttacked
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@ EUpdate_CriminalAttacked @@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				bCriminalsAttacked = TRUE
//				REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 1)
//				REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 2)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminals, RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCriminals, rghCriminals)
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				SET_WANTED_LEVEL_MULTIPLIER(0)
				SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
				
				INT iCount
				REPEAT COUNT_OF(pedCriminal) iCount
					IF NOT IS_PED_INJURED(pedCriminal[iCount])
						IF DOES_BLIP_EXIST(blipCriminal[iCount])
							REMOVE_BLIP(blipCriminal[iCount])
							IF IS_PED_IN_ANY_VEHICLE(pedCriminal[iCount])
								TASK_LEAVE_ANY_VEHICLE(pedCriminal[iCount])
							ENDIF
							IF DOES_BLIP_EXIST(biDestinationBlip)
								REMOVE_BLIP(biDestinationBlip)
							ENDIF
//							IF DOES_BLIP_EXIST(blipCult)
//								REMOVE_BLIP(blipCult)
//							ENDIF
							REMOVE_PED_FROM_GROUP(pedCriminal[iCount])
							TASK_COMBAT_PED(pedCriminal[iCount], PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedCriminal[iCount], TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_PED_INJURED(pedCriminal[1])
					CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_ATTP", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(pedCriminal[0])
					CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_ATTB", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				
				IF NOT bShopKeepToldToThank
					IF NOT IS_PED_INJURED(pedShopkeep)
						SET_PED_CAN_BE_TARGETTED(pedShopkeep, FALSE)
						TASK_PLAY_ANIM(pedShopkeep, "random@getawaydriver", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ENDIF
				
				REPEAT COUNT_OF(pedShoppers) iCount
					IF NOT IS_PED_INJURED(pedShoppers[iCount])
//						REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 3)
						TASK_SMART_FLEE_PED(pedShoppers[iCount], PLAYER_PED_ID(), 150, -1)
//						TASK_SMART_FLEE_PED(pedShopkeep, PLAYER_PED_ID(), 150, -1)
						SET_PED_KEEP_TASK(pedShoppers[iCount], TRUE)
//						SET_PED_KEEP_TASK(pedShopkeep, TRUE)
					ENDIF
				ENDREPEAT
				
//				IF NOT IS_PED_INJURED(pedShopkeep)
//					TASK_SMART_FLEE_PED(pedShopkeep, PLAYER_PED_ID(), 150, -1)
//					SET_PED_KEEP_TASK(pedShopkeep, TRUE)
//				ENDIF
				
				curstate = EUpdate_WaitForPolice
			BREAK
			
			CASE EUpdate_WaitForPolice
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ TASK_COMBAT_PED - WaitForPolice @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminals, RELGROUPHASH_PLAYER)
				REPEAT COUNT_OF(pedCriminal) iCount
					IF IS_PED_INJURED(pedCriminal[iCount])
						IF DOES_BLIP_EXIST(blipCriminal[iCount])
							REMOVE_BLIP(blipCriminal[iCount])
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedCriminal[iCount])
						IF NOT DOES_BLIP_EXIST(blipCriminal[iCount])
							blipCriminal[iCount] = CREATE_BLIP_FOR_PED(pedCriminal[iCount], TRUE)
						ENDIF
						IF IS_PED_IN_GROUP(pedCriminal[iCount])
							REMOVE_PED_FROM_GROUP(pedCriminal[iCount])
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(pedCriminal[iCount])
							TASK_LEAVE_ANY_VEHICLE(pedCriminal[iCount])
							TASK_COMBAT_PED(pedCriminal[iCount], PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedCriminal[iCount], TRUE)
						ELSE
							TASK_COMBAT_PED(pedCriminal[iCount], PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedCriminal[iCount], TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				IF DOES_BLIP_EXIST(biDestinationBlip)
					REMOVE_BLIP(biDestinationBlip)
				ENDIF
//				IF DOES_BLIP_EXIST(blipCult)
//					REMOVE_BLIP(blipCult)
//				ENDIF
			BREAK
			
			CASE EUpdate_Finish
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EUpdate_Finish @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
			BREAK
			
		ENDSWITCH
//	ENDIF

RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID CarTheftWidget
#ENDIF

PROC DrawWidgets(BOOL DoDraw)

	IF DoDraw
		#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(CarTheftWidget)
				CarTheftWidget = START_WIDGET_GROUP("Beat: Car Theft")
				STOP_WIDGET_GROUP()
			ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(CarTheftWidget)
				DELETE_WIDGET_GROUP(CarTheftWidget)
			ENDIF
		#ENDIF
	ENDIF
	
ENDPROC

PROC Cleanup(AMBIENTASSET Asset, PED_INDEX& CleanupPeds[], BOOL bTaskGiven)
	PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ CLEANUP @@@@@@@@@@@@@@@@@@@@@@@ \n")
	
	IF bDoFullCleanUp
	
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		IF g_bAltruistCultFinalDialogueHasPlayed
			TRIGGER_MUSIC_EVENT("AC_STOP")
		ENDIF
		RESET_CULT_STATUS()
		
		DISABLE_TAXI_HAILING(FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>, TRUE)
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_PED_PATHS_BACK_TO_ORIGINAL(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>)
		SET_ROADS_BACK_TO_ORIGINAL(vInput - << 20, 20, 20 >>, vInput + << 20, 20, 20 >>)
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
//		SET_DISPATCH_IDEAL_SPAWN_DISTANCE(200)
		
		IF NOT IS_ENTITY_DEAD(vehImpound)
			SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
		ENDIF
		
		IF NOT IS_PED_INJURED(pedShopkeep)
			IF IS_ENTITY_PLAYING_ANIM(pedShopkeep, "random@getawaydriver", "idle_a")
			OR IS_ENTITY_PLAYING_ANIM(pedShopkeep, "random@getawaydriver", "idle_2_hands_up")
				OPEN_SEQUENCE_TASK(seq)
					TASK_PLAY_ANIM(NULL, "random@getawaydriver", "hands_up_2_idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
					IF NOT IS_PED_INJURED(CleanupPeds[0])
						TASK_SMART_FLEE_PED(NULL, CleanupPeds[0], 150, -1)
					ELIF NOT IS_PED_INJURED(CleanupPeds[1])
						TASK_SMART_FLEE_PED(NULL, CleanupPeds[1], 150, -1)
					ENDIF
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedShopkeep, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(pedShopkeep, TRUE)
			ENDIF
			CLEAR_PED_SECONDARY_TASK(pedShopkeep)
			SET_PED_COMBAT_ATTRIBUTES(pedShopkeep, CA_ALWAYS_FLEE, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopkeep, FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(CleanupPeds[0])
			SET_PED_CONFIG_FLAG(CleanupPeds[0], PCF_CanSayFollowedByPlayerAudio, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(CleanupPeds[0], CA_ALWAYS_FLEE, TRUE)
			REMOVE_PED_FROM_GROUP(CleanupPeds[0])
			SET_PED_CAN_BE_TARGETTED(CleanupPeds[0], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CleanupPeds[0], FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(CleanupPeds[1])
			SET_PED_CONFIG_FLAG(CleanupPeds[1], PCF_CanSayFollowedByPlayerAudio, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(CleanupPeds[1], CA_ALWAYS_FLEE, TRUE)
			REMOVE_PED_FROM_GROUP(CleanupPeds[1])
			SET_PED_CAN_BE_TARGETTED(CleanupPeds[1], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CleanupPeds[1], FALSE)
		ENDIF
		
		INT index
		REPEAT COUNT_OF(pedShoppers) index
			IF NOT IS_PED_INJURED(pedShoppers[index])
//				TASK_SMART_FLEE_PED(pedShoppers[index], PLAYER_PED_ID(), 150, -1)
//				SET_PED_KEEP_TASK(pedShoppers[index], TRUE)
//				iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, << 68.4259, -1569.0674, 28.6027 >>, 60000)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShoppers[index], FALSE)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(pedCop[0])
			GIVE_WEAPON_TO_PED(pedCop[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[0], FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(pedCop[1])
			GIVE_WEAPON_TO_PED(pedCop[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[1], FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(pedCop[2])
			GIVE_WEAPON_TO_PED(pedCop[2], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[2], FALSE)
		ENDIF
		
		IF NOT bTaskGiven
		
			IF NOT IS_PED_INJURED(CleanupPeds[0])
				TASK_SMART_FLEE_COORD(CleanupPeds[0], GET_PLAYER_COORDS(PLAYER_ID()), 200, -1)
			ENDIF
			
			IF NOT IS_PED_INJURED(CleanupPeds[1])
				TASK_SMART_FLEE_COORD(CleanupPeds[1], GET_PLAYER_COORDS(PLAYER_ID()), 200, -1)
			ENDIF
			
		ENDIF
		
		IF NOT bIsGetawayPlayerControlOn
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bIsGetawayPlayerControlOn = TRUE
		ENDIF
		
		RemoveBlip(biDestinationBlip)
		IF DOES_BLIP_EXIST(blipCult)
			REMOVE_BLIP(blipCult)
		ENDIF
		RemoveBlip(biInitialActionBlip)
		DrawWidgets(FALSE)
		
		LoadOrUnloadFirstAssets(FALSE, Asset)
	ENDIF
		
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

//=================================== START OF MAIN SCRIPT ================================

SCRIPT (coords_struct in_coords)

	AMBIENT_EVENT_STATE Event_state = EVENT_INIT
	AMBIENTASSET initialAsset
	EUpdate updateState = EUpdate_Start
	vInput = in_coords.vec_coord[0]

	INT iBitFieldDontCheck
	BOOL bTaskGiven
	//VEHICLE_INDEX viPlayerVeh
	//PED_INDEX piShopKeep
	VECTOR debugCarPos
	FLOAT debugCarHeading
	INT StartingTimer
	INT waitTime
		
	#IF IS_DEBUG_BUILD
		BOOL DebugCheck
	#ENDIF

	initialAsset.mnCriminal1 			= G_M_Y_MexGoon_02
	initialAsset.mnCriminal2 			= HC_GUNMAN
	InitialAsset.mnShopKeep				= MP_M_ShopKeep_01
	InitialAsset.mnpedShoppers1			= A_F_Y_SouCent_01
	InitialAsset.mnpedShoppers2			= A_M_M_SouCent_01 //A_M_M_SouCent_02
	InitialAsset.mnPolice				= S_M_Y_COP_01
	InitialAsset.mnPoliceCar			= POLICE3

//	initialAsset.sGestAnimDict = "random@getawaydriver@thugs"
	//initialAsset.sShockedAnimDict = "gestures@female"
//	initialAsset.sShockedAnimDictA = "random@getawaydriver"
	//initialAsset.sShockedAnimDictB = "random@getawaydriver"

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedCriminal[0])
			IF IS_PED_IN_GROUP(pedCriminal[0])
				REMOVE_PED_FROM_GROUP(pedCriminal[0])
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedCriminal[1])
			IF IS_PED_IN_GROUP(pedCriminal[1])
				REMOVE_PED_FROM_GROUP(pedCriminal[1])
			ENDIF
		ENDIF
		PRINTNL()
		PRINTSTRING("Ending GatawayDriver: Force Cleanup Has happened")
		PRINTNL()
		Cleanup(initialAsset, pedCriminal, bTaskGiven)	
	ENDIF
	
	//Leave 3 frames between each load request for this script.
	SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(sLoadQueue, 3) 

	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	WHILE Event_state <> EVENT_OVER
	
		waitTime = 0 //CONST_AmbEventUpdate
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				PRINTNL()
				PRINTSTRING("GetawayDriver: Press S to Skip")
				PRINTNL()
				WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
					WAIT(0)
				ENDWHILE
				SET_HEIST_CREW_MEMBER_UNLOCKED(CM_GUNMAN_G_PACKIE_UNLOCK)
				
				IF GET_MISSION_COMPLETE_STATE( SP_HEIST_FINALE_2_INTRO )
					SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE, FALSE )
				ENDIF
	
				RANDOM_EVENT_PASSED()
				RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, pedCriminal, bTaskGiven)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				PRINTNL()
				PRINTSTRING("GetawayDriver: Press F to Fail")
				PRINTNL()
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, pedCriminal, bTaskGiven)
			ENDIF
		#ENDIF
		
		UPDATE_LOAD_QUEUE_MEDIUM(sLoadQueue)
		
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		AND NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			waitTime = 0
			event_state = EVENT_OVER
			Cleanup(initialAsset, pedCriminal, bTaskGiven)
		ENDIF
		
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
			OR IS_PLAYER_IN_SHOP(CLOTHES_SHOP_L_01_SC)
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, pedCriminal, bTaskGiven)
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_GD")
		
		IF HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
			IF DOES_ENTITY_EXIST(pedCriminal[0])
				DELETE_PED(pedCriminal[0])
			ENDIF
			IF DOES_ENTITY_EXIST(pedCriminal[1])
				DELETE_PED(pedCriminal[1])
			ENDIF
			WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
				WAIT(0)
			ENDWHILE
			
			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE, FALSE )
	
			RANDOM_EVENT_PASSED()
			RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
			waitTime = 0
			event_state = EVENT_OVER
			Cleanup(initialAsset, pedCriminal, bTaskGiven)
		ENDIF
		
//		PRINTSTRING("\n@@@@@@@@@@@@@@@ i StoppingTimer ")PRINTINT(iStoppingTimer)PRINTSTRING(" @@@@@@@@@@@@@@@\n")
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
//		AND CAN_PED_SEE_HATED_PED(GET_CLOSEST_PED(GET_PLAYER_COORDS(PLAYER_ID()), 100, FALSE, FALSE, ClosestCop, FALSE, ), PLAYER_PED_ID())
			IF IS_COP_PED_IN_AREA_3D(GET_PLAYER_COORDS(PLAYER_ID()) - << 20, 20, 10 >>, GET_PLAYER_COORDS(PLAYER_ID()) + << 20, 20, 10 >>)
			OR IS_COP_VEHICLE_IN_AREA_3D(GET_PLAYER_COORDS(PLAYER_ID()) - << 20, 20, 10 >>, GET_PLAYER_COORDS(PLAYER_ID()) + << 20, 20, 10 >>)
				IF NOT IS_PED_INJURED(pedCriminal[0])
				AND NOT IS_PED_INJURED(pedCriminal[1])
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedCriminal[0], GET_PLAYERS_LAST_VEHICLE())
				AND IS_PED_IN_VEHICLE(pedCriminal[1], GET_PLAYERS_LAST_VEHICLE())
					IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.2
						IF iStoppingTimer = -1
							iStoppingTimer = GET_GAME_TIMER()
						ENDIF
					ELSE
						iStoppingTimer = -1
					ENDIF
					
					IF (iStoppingTimer != -1 AND GET_GAME_TIMER() > iStoppingTimer + 1000)
	//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ GAME_TIMER() > iStoppingTimer + 1000 @@@@@@@@@@@@@@@@@@@@ \n")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ CRIMINALS HATING ON PLAYER @@@@@@@@@@@@@@@@@@@@ \n")
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_LOSE", CONV_PRIORITY_AMBIENT_HIGH)//REAWA_COPS2
						ENDIF
						IF (iStoppingTimer != -1 AND GET_GAME_TIMER() > iStoppingTimer + 10000)
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ GAME_TIMER() > iStoppingTimer + 10000 @@@@@@@@@@@@@@@@@@@@ \n")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_COPS", CONV_PRIORITY_AMBIENT_HIGH)
							IF NOT IS_PED_INJURED(pedCriminal[0])	
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedCriminal[0], seq)
								CLEAR_SEQUENCE_TASK(seq)
								SET_PED_KEEP_TASK(pedCriminal[0], TRUE)
							ENDIF
							IF NOT IS_PED_INJURED(pedCriminal[1])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_ANY_VEHICLE(NULL, 500)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedCriminal[1], seq)
								CLEAR_SEQUENCE_TASK(seq)
								SET_PED_KEEP_TASK(pedCriminal[1], TRUE)
							ENDIF
							waitTime = 0
							event_state = EVENT_OVER
							Cleanup(initialAsset, pedCriminal, bTaskGiven)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF updateState > EUpdate_CreateCriminals
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghCriminals, RELGROUPHASH_AMBIENT_GANG_BALLAS)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rghCriminals, RELGROUPHASH_AMBIENT_GANG_FAMILY)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_BALLAS, rghCriminals)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_FAMILY, rghCriminals)
			ELSE
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCriminals, RELGROUPHASH_AMBIENT_GANG_BALLAS)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghCriminals, RELGROUPHASH_AMBIENT_GANG_FAMILY)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_BALLAS, rghCriminals)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_FAMILY, rghCriminals)
			ENDIF
		ENDIF
		
		IF (updateState > EUpdate_CreateCriminals AND updateState < EUpdate_KillOneCriminal)
		AND NOT bCriminalsAttacked
			IF DOES_ENTITY_EXIST(pedShopkeep)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedShopkeep, PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
//					CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_DEAD2", CONV_PRIORITY_AMBIENT_HIGH)
					IF NOT IS_PED_INJURED(pedCriminal[0])
						TASK_SMART_FLEE_PED(pedCriminal[0], PLAYER_PED_ID(), 200, -1)
					ENDIF
					IF NOT IS_PED_INJURED(pedCriminal[1])
						TASK_SMART_FLEE_PED(pedCriminal[1], PLAYER_PED_ID(), 200, -1)
					ENDIF
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
			ENDIF
		ENDIF
		
		IF (updateState > EUpdate_KillOneCriminal AND updateState < EUpdate_CriminalAttacked)
		AND NOT bCriminalsAttacked
			IF DOES_ENTITY_EXIST(pedCriminal[0])
			AND DOES_ENTITY_EXIST(pedCriminal[1])
				IF IS_PED_INJURED(pedCriminal[1])
				OR NOT IS_ENTITY_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), << 80, 80, 80 >>)
					IF NOT IS_PED_INJURED(pedCriminal[0])
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@ Criminal 1 dead, Criminal 0 should flee @@@@@@@@@@@@@@@@@ \n")
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCriminal[0], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedCriminal[0], TRUE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_FKDB", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
				IF IS_PED_INJURED(pedCriminal[0])
				OR NOT IS_ENTITY_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), << 80, 80, 80 >>)
					IF NOT IS_PED_INJURED(pedCriminal[1])
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@ Criminal 0 dead, Criminal 1 should flee @@@@@@@@@@@@@@@@@ \n")
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedCriminal[1], seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(pedCriminal[1], TRUE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_DEAD1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
			ENDIF
		ENDIF
		
		IF ARE_PEDS_INJURED_OR_NOT_IN_VEHICLE(pedCriminal, GET_PLAYERS_LAST_VEHICLE(), 180)
			PRINTSTRING("\n GetawayDriver: ARE_PEDS_INJURED_OR_NOT_IN_VEHICLE \n")
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[0], PLAYER_PED_ID())
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCriminal[0], PLAYER_PED_ID())
				bCriminalsAttacked = TRUE
			ENDIF
			
			IF bCriminalsAttacked
				IF NOT bCopsHaveSpawned
					SpawnCops()
					// And tell shop keep to approach player if he's still alive
				ENDIF
				IF NOT bShopKeepToldToThank
					IF NOT IS_PED_INJURED(pedShopkeep)
						OPEN_SEQUENCE_TASK(Seq)
							IF IS_ENTITY_PLAYING_ANIM(pedShopkeep, "random@getawaydriver", "idle_a")
							OR IS_ENTITY_PLAYING_ANIM(pedShopkeep, "random@getawaydriver", "idle_2_hands_up")
								TASK_PLAY_ANIM(NULL, "random@getawaydriver", "hands_up_2_idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							ENDIF
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
							TASK_GO_TO_ENTITY/*_OFFSET_XY*/(NULL, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 5,/*6, -4, 1,*/ PEDMOVE_SPRINT)
							TASK_PLAY_ANIM(NULL, "random@getawaydriver", "gesture_nod_yes_soft", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
							TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
//							TASK_PAUSE(NULL, 3000)
						CLOSE_SEQUENCE_TASK(Seq)
						TASK_PERFORM_SEQUENCE(pedShopkeep, Seq)
						CLEAR_SEQUENCE_TASK(Seq)
						SET_PED_KEEP_TASK(pedShopkeep, TRUE)
						bShopKeepToldToThank = TRUE
					ENDIF
				ENDIF
			ELSE
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, pedCriminal, bTaskGiven)
			ENDIF
			
			IF bCopsHaveSpawned
				SET_WANTED_LEVEL_MULTIPLIER(1)
				SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
				
				IF NOT IS_PED_INJURED(pedCop[0])
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						IF IS_ENTITY_AT_ENTITY(pedCop[0], PLAYER_PED_ID(), << 4, 4, 4 >>)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TASK_LOOK_AT_ENTITY(pedCop[0], PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
								PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ PLAY_THX_DIALOGUE @@@@@@@@@@@@@@@@@@@@@@@ \n")
								CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_THX", CONV_PRIORITY_AMBIENT_HIGH)
								SETTIMERB(0)
							ELSE
								IF NOT IS_PED_INJURED(pedCop[0])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[0], FALSE)
								ENDIF
								IF NOT IS_PED_INJURED(pedCop[1])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[1], FALSE)
								ENDIF
								IF TIMERB() > 3000
									WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
										WAIT(0)
									ENDWHILE
									
									SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE, FALSE )
										
									SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_COPS)
									RANDOM_EVENT_PASSED()
									RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
									waitTime = 0
									event_state = EVENT_OVER
									Cleanup(initialAsset, pedCriminal, bTaskGiven)
								ENDIF
							ENDIF
						ELIF NOT IS_ENTITY_AT_ENTITY(pedCop[0], PLAYER_PED_ID(), << 120, 120, 120 >>)
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ ELIF NOT IS_ENTITY_AT_ENTITY(pedCop[0] @@@@@@@@@@@@@@@@@@@@@@@ \n")
							waitTime = 0
							event_state = EVENT_OVER
							Cleanup(initialAsset, pedCriminal, bTaskGiven)
						ENDIF
					ELSE
						CLEAR_PED_TASKS(pedCop[0])
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ WANTED @@@@@@@@@@@@@@@@@@@@@@@ \n")
						waitTime = 0
						event_state = EVENT_OVER
						Cleanup(initialAsset, pedCriminal, bTaskGiven)
					ENDIF
				ELIF IS_PED_INJURED(pedShopkeep)
					PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ IS_PED_INJURED(TheCrim @@@@@@@@@@@@@@@@@@@@@@@ \n")
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
			ENDIF
			
			IF bShopKeepToldToThank
				IF NOT IS_PED_INJURED(pedShopkeep)
					IF IS_ENTITY_AT_ENTITY(pedShopkeep, PLAYER_PED_ID(), << 4, 4, 4 >>)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							TASK_LOOK_AT_ENTITY(pedShopkeep, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV)
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ PLAY_THANKS_DIALOGUE @@@@@@@@@@@@@@@@@@@@@@@ \n")
							CREATE_CONVERSATION(GetawayDriverConversation, "REGETAU", "REAWA_THANKS", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							IF NOT IS_PED_INJURED(pedShopkeep)
								SET_PED_CAN_BE_TARGETTED(pedShopkeep, TRUE)
							ENDIF
							WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								WAIT(0)
							ENDWHILE
							WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
								WAIT(0)
							ENDWHILE
							REMOVE_ALL_SHOCKING_EVENTS(TRUE)
							
							SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE, FALSE )

							SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_COPS)
							RANDOM_EVENT_PASSED()
							RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
							waitTime = 0
							event_state = EVENT_OVER
							Cleanup(initialAsset, pedCriminal, bTaskGiven)
						ENDIF
					ELIF NOT IS_ENTITY_AT_ENTITY(pedShopkeep, PLAYER_PED_ID(), << 120, 120, 120 >>)
						PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ ELIF NOT IS_ENTITY_AT_ENTITY(pedShopkeep @@@@@@@@@@@@@@@@@@@@@@@ \n")
						waitTime = 0
						event_state = EVENT_OVER
						Cleanup(initialAsset, pedCriminal, bTaskGiven)
					ENDIF
				ELIF IS_PED_INJURED(pedCop[0])
					PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@ IS_PED_INJURED(TheSho @@@@@@@@@@@@@@@@@@@@@@@ \n")
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
			ENDIF
		ENDIF
		
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			waitTime = 0
//			PRINTSTRING("\n Ending GatawayDriver: Wrong character, can't be played by Michael \n")
//			event_state = EVENT_OVER
//			Cleanup(initialAsset, pedCriminal, bTaskGiven)
//		ENDIF
		
		SWITCH event_state
			CASE EVENT_INIT
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_INIT @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					waitTime = 0
					event_state = EVENT_OVER
					Cleanup(initialAsset, pedCriminal, bTaskGiven)
				ENDIF
				IF LoadOrUnloadFirstAssets(TRUE, initialAsset)
					waitTime = 0
					Event_state = EVENT_WAIT_TO_START
				ENDIF
			BREAK
			
			CASE EVENT_WAIT_TO_START
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_WAIT_TO_START @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF IS_AMBIENT_SAFE_TO_RUN(RUN_ON_MISSION_NEVER)
					
					bDoFullCleanUp = TRUE
					DrawWidgets(TRUE)
					StartingTimer = GET_GAME_TIMER()
					event_state = EVENT_STARTING
				ELSE
					PRINTSTRING("\n Ending GatawayDriver: AmbientEvent Is not safe to run \n")
					event_state = EVENT_ENDING
				ENDIF
				waitTime = 0
			BREAK
			
			CASE EVENT_STARTING
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_STARTING @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				waitTime = 0
				
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_HeardShot))
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_ShotNear))
				SET_BIT(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					viPlayerVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						IF IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
							IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
								IF IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
								AND IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_BACK_RIGHT)
								AND IS_VEHICLE_SEAT_FREE(GET_PLAYERS_LAST_VEHICLE(), VS_BACK_LEFT)
									PRINTSTRING("\n @@@@@@@@@@@@@@@@Getaway Driver: Seats Free@@@@@@@@@@@@@@@@@@@@@@ \n")
									event_state = EVENT_RUNNING
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTSTRING("\n Ending GatawayDriver: Player is not in any vehicle \n")
					event_state = EVENT_ENDING
				ENDIF
				event_state = EVENT_RUNNING
				
				GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_ENTITY_COORDS(PLAYER_PED_ID()), debugCarPos, debugCarHeading)	
				
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						DebugCheck = TRUE
					ENDIF
					IF DebugCheck = TRUE
						IF CreateDebugCar(<<-1160.7942, -856.2255, 13.1581 >>, 35, DebugCheck)
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							DebugCheck = FALSE
						ENDIF
					ENDIF
				#ENDIF
				
				IF DO_TIMER(StartingTimer, 20000)
					PRINTSTRING("\n Ending GatawayDriver: Event timed out \n")
					event_state = EVENT_ENDING
				ENDIF
				
				IF event_state <> EVENT_RUNNING
					#IF IS_DEBUG_BUILD
//						PRINT("Getaway_begin", 100, 1)
					#ENDIF
				ENDIF
				
			BREAK
				
			CASE EVENT_RUNNING
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_RUNNING @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				IF updateState > EUpdate_CreateCriminals
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						
						//Add in all the player messing up stuff
						IF IS_PED_INJURED(PLAYER_PED_ID())
							waitTime = 0
							PRINTSTRING("\n Ending GatawayDriver: Player is injured \n")
							event_state = EVENT_ENDING
							BREAK
						ENDIF
						
	//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//						viPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	//					ELSE
	//						waitTime = 0
	//						PRINTNL()
	//						PRINTSTRING("Ending GatawayDriver: Player not in a car")
	//						PRINTNL()
	//						
	//						event_state = EVENT_ENDING
	//						BREAK
	//					ENDIF
						
	//					IF IS_ENTITY_DEAD(viPlayerVeh)
	//						waitTime = 0
	//						PRINTSTRING("\n Ending GatawayDriver: Player car is dead \n")
	//						event_state = EVENT_ENDING
	//						BREAK
	//					ENDIF
						
	//					IF NOT IS_PED_INJURED(pedCriminal[0])
	//					AND NOT IS_PED_INJURED(pedCriminal[1])
	//						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedCriminal[0])
	//						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCriminal[0])
	//						OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),pedCriminal[1])
	//						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCriminal[1])
	//							IF iAimingTimer = -1
	//								iAimingTimer = GET_GAME_TIMER()
	//							ENDIF
	//						ELSE
	//							iAimingTimer = -1
	//						ENDIF
	//						
	//						IF HAS_CRIMINAL_BEEN_ATTACKED() = FALSE
	//							IF (iAimingTimer != -1 AND GET_GAME_TIMER() > iAimingTimer + 1500)
	//								PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@@@@ PLAY_AMBIENT_DIALOGUE_LINE @@@@@@@@@@@@@@@@@@@@@@@")
	//								PRINTNL()
	////								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	////								WAIT(0)
	//								PLAY_AMBIENT_DIALOGUE_LINE(GetawayDriverConversation, pedCriminal[0], "REGETAU", "REAWA_AIM")
	//							ENDIF
	//						ENDIF
	//						
	//						IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedCriminal[0])
	//						OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedCriminal[1])
	//						OR GET_ENTITY_HEALTH(pedCriminal[0]) < 150
	//						OR GET_ENTITY_HEALTH(pedCriminal[1]) < 150
	//						OR (iAimingTimer != -1 AND GET_GAME_TIMER() > iAimingTimer + 5000)
	//							
	//							waitTime = 0
	//							PRINTNL()
	//							PRINTSTRING("Attacked one of the criminals")
	//							PRINTNL()
	//							HAS_CRIMINAL_BEEN_ATTACKED() = TRUE
	//							
	//							
	//							IF bCopsHaveSpawned = TRUE
	//								bTaskGiven = TRUE
	//								event_state = EVENT_ENDING
	//							ENDIF
	//							BREAK
	//						ENDIF
	//					ENDIF
	//					
	//					IF IS_PED_INJURED(pedCriminal[0])
	//					AND IS_PED_INJURED(pedCriminal[1])
	//						PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@ BOTH ENEMIES DEAD @@@@@@@@@@@@@@@@@@@@@")
	//						PRINTNL()
	//						SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
	//						SpawnCops()
	//					ENDIF
	//					
	//					IF bHasCivilianBeenScared = FALSE
	//						INT iCount
	//						REPEAT COUNT_OF(pedShoppers) iCount
	//							IF DOES_ENTITY_EXIST(pedShoppers[iCount])
	//							AND DOES_ENTITY_EXIST(pedShopkeep)
	//								IF NOT IS_PED_INJURED(pedShoppers[iCount])
	//								AND NOT IS_PED_INJURED(pedShopkeep)
	//									IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedShoppers[iCount])
	//									OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedShopkeep)
	//										PRINTSTRING("@@@@@@@@@@@@@@@@@@@@@@ bHasCivilianBeenScared @@@@@@@@@@@@@@@@@@@@@")
	//										PRINTNL()
	//										TASK_SMART_FLEE_PED(pedShoppers[iCount], PLAYER_PED_ID(), 150, -1)
	//										SET_PED_KEEP_TASK(pedShoppers[iCount], TRUE)
	//										TASK_SMART_FLEE_PED(pedShopkeep, PLAYER_PED_ID(), 150, -1)
	//										SET_PED_KEEP_TASK(pedShopkeep, TRUE)
	//									ENDIF
	//									bHasCivilianBeenScared = TRUE
	//								ENDIF
	//							ENDIF
	//						ENDREPEAT
	//					ENDIF
						IF NOT bHasCivilianBeenScared
							IF IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), << 61.5891, -1565.2933, 28.4601 >> - << 30, 30, 30 >>, << 61.5891, -1565.2933, 28.4601 >> + << 30, 30, 30 >>, FALSE)
								INT iCount
								REPEAT COUNT_OF(pedShoppers) iCount
									IF NOT IS_PED_INJURED(pedShoppers[iCount])
										PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@@ ")PRINTINT(iCount)PRINTSTRING(" CIVILIAN SCARED @@@@@@@@@@@@@@@@@@@@@\n")
	//									REMOVE_PED_FOR_DIALOGUE(GetawayDriverConversation, 3)
										TASK_SMART_FLEE_PED(pedShoppers[iCount], PLAYER_PED_ID(), 1000, -1)
										SET_PED_KEEP_TASK(pedShoppers[iCount], TRUE)
									ENDIF
								ENDREPEAT
								IF NOT IS_PED_INJURED(pedShopkeep)
									TASK_SMART_FLEE_PED(pedShopkeep, PLAYER_PED_ID(), 1000, -1)
									SET_PED_KEEP_TASK(pedShopkeep, TRUE)
								ENDIF
								bHasCivilianBeenScared = TRUE
	//							IF NOT IS_PED_INJURED(pedCriminal[0])
	//							AND NOT IS_PED_INJURED(pedCriminal[1])
	//								TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[0], PLAYER_PED_ID(), -1)
	//								TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[1], PLAYER_PED_ID(), -1)
	//							ENDIF
							ENDIF
						ENDIF
						
	//					IF NOT IS_PED_INJURED(pedCriminal[0])
	//						IF NOT IS_ENTITY_AT_ENTITY(pedCriminal[0], PLAYER_PED_ID(), << 120, 120, 120 >>)
	////							bTaskGiven = TRUE
	//							event_state = EVENT_ENDING
	//							BREAK
	//						ENDIF
	//					ENDIF
	//					
	//					IF NOT IS_PED_INJURED(pedCriminal[1])
	//						IF NOT IS_ENTITY_AT_ENTITY(pedCriminal[1], PLAYER_PED_ID(), << 120, 120, 120 >>)
	////							bTaskGiven = TRUE
	//							event_state = EVENT_ENDING
	//							BREAK
	//						ENDIF
	//					ENDIF
					ENDIF
				ELSE
					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
						waitTime = 0
						event_state = EVENT_OVER
						Cleanup(initialAsset, pedCriminal, bTaskGiven)
					ENDIF
				ENDIF
				
				IF event_state <> EVENT_ENDING
					IF UpdateEvent(updateState, initialAsset, pedCriminal, waitTime, bTaskGiven)
						PRINTSTRING("\n Ending GatawayDriver: EventUpdate returned true \n")
						event_state = EVENT_ENDING
						BREAK
					ENDIF
				ENDIF
			
			BREAK
			
			CASE EVENT_ENDING
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_ENDING @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				waitTime = 0
				event_state = EVENT_OVER
				Cleanup(initialAsset, pedCriminal, bTaskGiven)
				BREAK
			BREAK
			
			CASE EVENT_OVER
				PRINTSTRING("\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EVENT_OVER @@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n")
				waitTime = 0
			BREAK
		
		ENDSWITCH
		
		IF Event_state <> EVENT_OVER
			WAIT(waitTime)
		ENDIF
	ENDWHILE
	
	Cleanup(initialAsset, pedCriminal, bTaskGiven)
	
ENDSCRIPT

