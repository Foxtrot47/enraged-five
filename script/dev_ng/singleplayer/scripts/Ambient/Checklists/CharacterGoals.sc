

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_ped.sch"
USING "commands_path.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "finance_control_public.sch"

ENUM Trevor_life_goal
	Trevor_life_goal_SETUP,
	Trevor_life_goal_TRACKING,
	Trevor_life_goal_CLEAN
ENDENUM
Trevor_life_goal Trevor_life_goal_stage = Trevor_life_goal_SETUP

BOOL goallaunch[2], REACHED_HALFWAY_POINT
INT counter_current_time_goal, counter_time_started_goal



// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()

	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC DISPLAY_TASKLIST_PROGRESS(STRING TextToDisplay, INT current_progress_int)

	DRAW_RECT (0.852, 0.810, 0.141, 0.025, 0, 0, 0, 175)
	format_medium_ostext (255, 255, 255, 255) //White
	//DISPLAY_TEXT_WITH_LITERAL_STRING (0.81, 0.70, "STRING", "New Side Task.") //This will be replaced with proper text soon!
	DISPLAY_TEXT_WITH_NUMBER(0.81, 0.80, TextToDisplay, current_progress_int)

ENDPROC


// Mission Script -----------------------------------------//
SCRIPT

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		PRINTSTRING("...CharacterGoals.sc has been forced to cleanup (SP to MP)")
		PRINTNL()
		
		Script_Cleanup()
	ENDIF

#IF IS_DEBUG_BUILD
	
//	WIDGET_ID missionTab
//	WIDGET_ID checklistwidget
	//WIDGET_ID holdMissionsWidget
	
//	missionTab = 
	START_WIDGET_GROUP("CharacterGoals")			
	
//		checklistwidget = 
		START_WIDGET_GROUP("Trevor")
					
			ADD_WIDGET_BOOL("millionaire", goallaunch[0])			
			ADD_WIDGET_BOOL("Give cash - 500k", goallaunch[1])		
					
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()

#ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE

WAIT(250)
		
	SWITCH Trevor_life_goal_stage

		CASE Trevor_life_goal_SETUP
			IF goallaunch[0]		
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[0] = GET_TOTAL_CASH(CHAR_TREVOR) 
					DEBIT_BANK_ACCOUNT(CHAR_TREVOR, BAAC_LESTER, g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[0])
				ENDIF

				g_AmbientSideTask[Sidetask_3].SideTaskTitle = "PA_TREV1" //Earn one million dollars.
				g_AmbientSideTask[Sidetask_3].SideTaskSynopsis = "PA_TREV1"  //Earn one million dollars.
				g_AmbientSideTask[Sidetask_3].SideTaskFullyComplete = FALSE
			
				g_AmbientSideTask[Sidetask_3].NumberOfSideTaskPortions = 1
				
				g_AmbientSideTask[Sidetask_3].SideTaskPortionLabel[1] = "PA_TREV1A" //Current wealth $~1~.
				g_AmbientSideTask[Sidetask_3].HasThisSideTaskPortionBeenCompleted[1] = FALSE
//				g_AmbientSideTask[Sidetask_3].SideTaskPortionTargetInt[0] = -1 
				//g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[0] = 0 		
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[1] = GET_TOTAL_CASH(CHAR_TREVOR)
				ENDIF
				
				SET_SIDETASK_LIVE (CHAR_TREVOR, Sidetask_3)
				DISPLAY_TASKLIST_PROGRESS("PA_TREV1A", g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[1])
				
				//PRINT("PA_TREV1",2000, 1) //Brief
				//PRINT_WITH_NUMBER("PA_TREV1A", g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[0], 2000, 1)
				//CLEAR_THIS_PRINT("CL_C1B_BR")
				
//				PRINT_STRING_IN_STRING ("CL_ADDED", "CL_C1B", 3000, 1)
				Trevor_life_goal_stage = Trevor_life_goal_TRACKING
				#IF IS_DEBUG_BUILD
					goallaunch[0] = FALSE
				#ENDIF
			ENDIF
		BREAK
	
		CASE Trevor_life_goal_TRACKING
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[1] = GET_TOTAL_CASH(CHAR_TREVOR)
				//IS_ENTITY_IN_AREA(PLAYER_PED_ID(),  <<80, -1014, 0>>, <<311, -847, 80>>, FALSE)
				IF IS_TOTAL_CASH_GREATER(CHAR_TREVOR, 999999)
					PRINT_STRING_IN_STRING("PA_COMPLETE", "PA_TREV1", 2000, 1) //Brief
					g_AmbientSideTask[Sidetask_3].HasThisSideTaskPortionBeenCompleted[1] = TRUE
					g_AmbientSideTask[Sidetask_3].SideTaskFullyComplete = TRUE
//					DISPLAY_TASKLIST_PROGRESS("PA_TREV1A", g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[1])
					counter_time_started_goal = GET_GAME_TIMER()
					Trevor_life_goal_stage = Trevor_life_goal_CLEAN
				ELIF IS_TOTAL_CASH_GREATER(CHAR_TREVOR, 499999)
					IF NOT REACHED_HALFWAY_POINT
						PRINT("PA_TREV1",2000, 1)
						//PRINT_WITH_NUMBER("PA_TREV1A", g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[0], 2000, 1) //Halfway there
						REACHED_HALFWAY_POINT = TRUE
					ENDIF
				ENDIF
			
				IF goallaunch[1]
					CREDIT_BANK_ACCOUNT(CHAR_TREVOR, BAAC_LESTER, 500000)
					goallaunch[1] = FALSE
				ENDIF
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF goallaunch[0]
					Trevor_life_goal_stage = Trevor_life_goal_SETUP
					SET_SIDETASK_DEAD(Sidetask_3)
				ENDIF
			#ENDIF
			
		BREAK
		
		CASE Trevor_life_goal_CLEAN
						
			counter_current_time_goal = GET_GAME_TIMER()
			IF (counter_current_time_goal - counter_time_started_goal) < 5000
				DISPLAY_TASKLIST_PROGRESS("PA_TREV1A", g_AmbientSideTask[Sidetask_3].SideTaskPortionCurrentInt[1])
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF goallaunch[0]
					Trevor_life_goal_stage = Trevor_life_goal_SETUP
					SET_SIDETASK_DEAD(Sidetask_3)
				ENDIF
			#ENDIF
			
		BREAK
		
	ENDSWITCH

ENDWHILE
	
ENDSCRIPT
