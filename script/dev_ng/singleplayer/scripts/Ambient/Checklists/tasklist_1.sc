
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//World Point template

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_ped.sch"
USING "commands_path.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"

// Variables ----------------------------------------------//
//#IF IS_DEBUG_BUILD //WIDGETS
	BOOL checklistlaunch[3]
//#ENDIF

INT i
//FLOAT vehicles_speed
PED_INDEX temp_gang_guy[3]
//VEHICLE_INDEX players_vehicle
//STRUCT_STAT_INT receivingStruct

ENUM JOB_ONE
	JOB_ONE_SETUP,
	JOB_ONE_TRACKING,
	JOB_ONE_CLEAN
ENDENUM
JOB_ONE job_one_stage = JOB_ONE_SETUP

ENUM JOB_TWO
	JOB_TWO_SETUP,
	JOB_TWO_TRACKING,
	JOB_TWO_CLEAN
ENDENUM
JOB_TWO job_two_stage = JOB_TWO_SETUP

//ENUM JOB_THREE
//	JOB_THREE_SETUP,
//	JOB_THREE_TRACKING,
//	JOB_THREE_CLEAN
//ENDENUM
//JOB_THREE job_three_stage = JOB_THREE_SETUP

// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()

	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC


// Mission Script -----------------------------------------//
SCRIPT

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		PRINTSTRING("...tasklist_1.sc has been forced to cleanup (SP to MP)")
		PRINTNL()
		
		Script_Cleanup()
	ENDIF


#IF IS_DEBUG_BUILD
	
//	WIDGET_ID missionTab
//	WIDGET_ID contact1Widget
//	WIDGET_ID contact2Widget
	
//	missionTab = 
	START_WIDGET_GROUP("Checklist missions")			
	
//		contact1Widget = 
		START_WIDGET_GROUP("Alex Jobs")			
			
			ADD_WIDGET_BOOL("Killer Photo", checklistlaunch[0])			
			
			ADD_WIDGET_BOOL("Bike Pro", checklistlaunch[1])	

		STOP_WIDGET_GROUP()

//		contact2Widget = 
		START_WIDGET_GROUP("Bob Jobs")			
			
			
		STOP_WIDGET_GROUP()
		
	
	STOP_WIDGET_GROUP()

#ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(250)
		
	SWITCH job_one_stage
		
		CASE JOB_ONE_SETUP
			IF checklistlaunch[0]
			
				g_AmbientSideTask[Sidetask_1].SideTaskTitle = "CL_C1A" //~s~Killer photo
				g_AmbientSideTask[Sidetask_1].SideTaskSynopsis = "CL_C1A_BR" //kill Some gang members and take a photo of the corpses.
				g_AmbientSideTask[Sidetask_1].SideTaskFullyComplete = FALSE
			
				g_AmbientSideTask[Sidetask_1].NumberOfSideTaskPortions = 2
				
				g_AmbientSideTask[Sidetask_1].SideTaskPortionLabel[1] = "CL_C1A_J1" //~g~Gang members killed~s~ ~1~/~1~
				g_AmbientSideTask[Sidetask_1].HasThisSideTaskPortionBeenCompleted[1] = FALSE
				g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[1] = 3 //Number of gang members to kill
				g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[1] = 0 //reset current int progress for Number of gang members to kill
			
				g_AmbientSideTask[Sidetask_1].SideTaskPortionLabel[2] = "CL_C1A_J2" //~g~Photo's taken of dead gang members~s~ ~1~/~1~
				g_AmbientSideTask[Sidetask_1].HasThisSideTaskPortionBeenCompleted[2] = FALSE
				g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[2] = 3 //Number of photos to take
				g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[2] = 0 //reset current int progress for Number of photos to take
				
				REPEAT g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[1] i
					CHECKED_OFF_GANG_PHOTOS[i] = FALSE
					CHECKED_OFF_KILLS[i] = FALSE
				ENDREPEAT
				
				SET_SIDETASK_LIVE (CHAR_FRANKLIN, Sidetask_1)
				
				PRINT_STRING_IN_STRING ("CL_ADDED", "CL_C1A", 3000, 1)
				job_one_stage = JOB_ONE_TRACKING
				#IF IS_DEBUG_BUILD
					checklistlaunch[0] = FALSE
				#ENDIF
			ENDIF
		BREAK
	
		CASE JOB_ONE_TRACKING
			
			REPEAT g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[1] i
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(temp_gang_guy[i])
						IF IS_PED_INJURED(temp_gang_guy[i])
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(temp_gang_guy[i], PLAYER_PED_ID())
								IF NOT CHECKED_OFF_KILLS[i]
									g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[1] ++
//
									PRINT_WITH_2_NUMBERS("CL_C1A_J1", g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[1], g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[1], 2000, 1)
									CHECKED_OFF_KILLS[i] = TRUE
								ENDIF
								
								IF IS_ENTITY_DEAD(temp_gang_guy[i])
									IF NOT CHECKED_OFF_GANG_PHOTOS[i]
										//dead_ped_coords[i] = /*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(temp_gang_guy[i])
										IF CELL_CAM_IS_CHAR_VISIBLE_NO_FACE_CHECK(temp_gang_guy[i])
										AND HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
										//IF GET_IS_PED_PHOTOGRAPHING_COORD(PLAYER_PED_ID(), dead_ped_coords[i])
											g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[2] ++
											PRINT_WITH_2_NUMBERS("CL_C1A_J2", g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[2], g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[2], 2000, 1)
											CHECKED_OFF_GANG_PHOTOS[i] = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								IF g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[1] >= g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[1]
								AND g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[2] >= g_AmbientSideTask[Sidetask_1].SideTaskPortionTargetInt[2]
									PRINT_STRING_IN_STRING ("CL_COMPLETE", "CL_C1A", 3000, 1)
									g_AmbientSideTask[Sidetask_1].SideTaskFullyComplete = TRUE
									job_one_stage = JOB_ONE_CLEAN
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				IF checklistlaunch[0]
					job_one_stage = JOB_ONE_SETUP
				ENDIF
			#ENDIF
			
		BREAK
		
		CASE JOB_ONE_CLEAN

			#IF IS_DEBUG_BUILD
				IF checklistlaunch[0]
					job_one_stage = JOB_ONE_SETUP
				ENDIF
			#ENDIF
			
		BREAK
	
	ENDSWITCH
		
#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	AND IS_KEYBOARD_KEY_PRESSED(KEY_LSHIFT)
		REQUEST_MODEL(G_M_Y_StrPunk_01)
		IF HAS_MODEL_LOADED(G_M_Y_StrPunk_01)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR temp_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				REPEAT 3 i
					temp_coords.y = temp_coords.y + 1
					temp_gang_guy[i] = CREATE_RANDOM_MALE_PED(<<temp_coords.x, temp_coords.y, temp_coords.z>> )						
					SET_ENTITY_AS_MISSION_ENTITY(temp_gang_guy[i])
	//				SET_ENTITY_AS_NO_LONGER_NEEDED(temp_gang_guy[i])
				ENDREPEAT
			ENDIF
		ENDIF			
	ENDIF
#ENDIF	//	IS_DEBUG_BUILD
		
WAIT(250)	
		
	SWITCH Job_two_stage

		CASE JOB_TWO_SETUP
			IF checklistlaunch[1]
				g_AmbientSideTask[Sidetask_2].SideTaskTitle = "CL_C1B" //~s~Bike Pro
				g_AmbientSideTask[Sidetask_2].SideTaskSynopsis = "CL_C1B_BR"  //Do a stunt on a bike and kill ~1~ gang member while on a bike.
				g_AmbientSideTask[Sidetask_2].SideTaskFullyComplete = FALSE
			
				g_AmbientSideTask[Sidetask_2].NumberOfSideTaskPortions = 3
				
//				g_AmbientSideTask[Sidetask_2].SideTaskPortionLabel[1] = "CL_C1B_J1" //~g~Killed ~s~~1~/~1~ ~g~while on a bike~s~
//				g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1] = FALSE
//				g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[1] = 3 //Number of gang members to kill
//				g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[1] = 0 //reset current int progress
				
				g_AmbientSideTask[Sidetask_2].SideTaskPortionLabel[1] = "CL_C1B_J1" //~g~Successfully do a stunt on a bike ~a~
				g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1] = FALSE
				g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[1] = -1 //Number of gang members to kill
				g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[1] = -1 //reset current int progress

				g_AmbientSideTask[Sidetask_2].SideTaskPortionLabel[2] = "CL_C1B_J2" //~g~Successfully do a stunt on a bike ~a~
				g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2] = FALSE
				g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[2] = -1 //Number of gang members to kill
				g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[2] = -1 //reset current int progress
				
				g_AmbientSideTask[Sidetask_2].SideTaskPortionLabel[3] = "CL_C1B_J3" //~g~Successfully do a stunt on a bike ~a~
				g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[3] = FALSE
				g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[3] = -1 //Number of gang members to kill
				g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[3] = -1 //reset current int progress

//				g_AmbientSideTask[Sidetask_2].SideTaskPortionLabel[4] = "CL_C1B_J2" //~g~Successfully do a stunt on a bike ~a~
//				g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[4] = FALSE
//				g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[4] = -1 //Number of gang members to kill
//				g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[4] = -1 //reset current int progress
				
//				REPEAT g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[1] i
//					SHOOT_PEDS_FROM_BIKE[i] = FALSE
//				ENDREPEAT
				
				SET_SIDETASK_LIVE (CHAR_ARTHUR, Sidetask_2)
				
				PRINT_STRING_IN_STRING ("CL_ADDED", "CL_C1B", 3000, 1)
				Job_two_stage = JOB_TWO_TRACKING
				#IF IS_DEBUG_BUILD
					checklistlaunch[1] = FALSE
				#ENDIF
			ENDIF
		BREAK
	
		CASE JOB_TWO_TRACKING
		
			IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1]
//				STAT_GET_DATA(LONGEST_WHEELIE_TIME, receivingStruct, SIZE_OF(STRUCT_STAT_INT))
//				IF receivingStruct.value > 200
//					PRINTNL()
//					PRINTSTRING("********** LONGEST_WHEELIE_TIME > 200 **********")
//					PRINTNL()
//					PRINT("CL_C1B_J2", 2000, 1)
//					g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1] = TRUE
//				ENDIF
			ENDIF
			
			IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2]
//				STAT_GET_DATA(LONGEST_STOPPIE_TIME, receivingStruct, SIZE_OF(STRUCT_STAT_INT))
//				IF receivingStruct.value > 100
//					PRINTNL()
//					PRINTSTRING("********** LONGEST_STOPPIE_TIME > 100**********")
//					PRINTNL()
//					PRINT("CL_C1B_J2", 2000, 1)
//					g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2] = TRUE
//				ENDIF
			ENDIF
			
			IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[3]
//				STAT_GET_DATA(MOST_FLIPS_IN_ONE_JUMP, receivingStruct, SIZE_OF(STRUCT_STAT_INT))
//				IF receivingStruct.value > 1
//					PRINTNL()
//					PRINTSTRING("********** MOST_FLIPS_IN_ONE_JUMP > 1 **********")
//					PRINTNL()
//					PRINT("CL_C1B_J2", 2000, 1)
//					g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[3] = TRUE
//				ENDIF
			ENDIF
//
//			IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[4]
//				STAT_GET_DATA(TOP_SPEED_CAR, receivingStruct, SIZE_OF(STRUCT_STAT_INT))
//				IF receivingStruct.value > 10
//					PRINTNL()
//					PRINTSTRING("********** TOP_SPEED_CAR > 10 **********")
//					PRINTNL()
//					PRINT("CL_C1B_J2", 2000, 1)
//					g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[4] = TRUE
//				ENDIF
//			ENDIF
			
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
//					players_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//					IF IS_VEHICLE_DRIVEABLE(players_vehicle)
//						
//						IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1]
//							vehicles_speed = GET_ENTITY_SPEED(players_vehicle)
//							IF IS_ENTITY_IN_AIR(players_vehicle)
//							AND vehicles_speed >= 20
//								PRINT("CL_C1B_J2", 2000, 1)
//								g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1] = TRUE
//							ENDIF
//						ENDIF
//						
//						IF NOT g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2]
//							REPEAT g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[1] i
//								IF NOT SHOOT_PEDS_FROM_BIKE[i]
//									IF DOES_ENTITY_EXIST(temp_gang_guy[i])
//										IF IS_PED_INJURED(temp_gang_guy[i])
//											IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(temp_gang_guy[i], PLAYER_PED_ID())
//												g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[1] ++
//												PRINT_WITH_2_NUMBERS("CL_C1B_J1", g_AmbientSideTask[Sidetask_2].SideTaskPortionCurrentInt[1], g_AmbientSideTask[Sidetask_2].SideTaskPortionTargetInt[1], 2000, 1)
//	////										SET_ENTITY_AS_NO_LONGER_NEEDED(temp_gang_guy[i])
//												g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2] = TRUE
//												SHOOT_PEDS_FROM_BIKE[i] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF		
//							ENDREPEAT
//						ENDIF
						
						IF g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[1] = TRUE
						AND g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[2] = TRUE
						AND g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[3] = TRUE
//						AND g_AmbientSideTask[Sidetask_2].HasThisSideTaskPortionBeenCompleted[4] = TRUE
							PRINT_STRING_IN_STRING ("CL_COMPLETE", "CL_C1B", 3000, 1)
							g_AmbientSideTask[Sidetask_2].SideTaskFullyComplete = TRUE
							job_two_stage = JOB_TWO_CLEAN	
						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF checklistlaunch[1]
					job_two_stage = JOB_TWO_SETUP
				ENDIF
			#ENDIF
			
		BREAK
		
		CASE JOB_TWO_CLEAN

			#IF IS_DEBUG_BUILD
				IF checklistlaunch[1]
					job_two_stage = JOB_TWO_SETUP
				ENDIF
			#ENDIF
			
		BREAK
		
	ENDSWITCH

	
ENDWHILE


ENDSCRIPT


