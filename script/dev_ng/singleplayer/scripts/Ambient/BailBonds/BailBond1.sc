// ******************************************************************************************************************
// ******************************************************************************************************************
//
//		MISSION NAME	:	BailBond1.sc
//		AUTHOR			:	Ahron Mason / prev Ste Kerrigan
//		DESCRIPTION		:	Trevor apprehends bail jumpers at the quarry (vehicle chase)					
//
// ******************************************************************************************************************
// ******************************************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "ambience_run_checks.sch"
USING "BailBond_include.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "clearmissionarea.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "CompletionPercentage_public.sch"
using "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "flow_special_event_checks.sch"
using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "RC_Area_public.sch"
USING "RC_helper_functions.sch"
USING "RC_threat_public.sch" 
USING "replay_public.sch"
USING "script_blips.sch"
USING "script_ped.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
#ENDIF	

//----------------------------------------------------------------------------------------------------------------
//		ENUMS
//----------------------------------------------------------------------------------------------------------------

/// PURPOSE: the stages of the mission
ENUM MISSION_STAGE
	MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER,		
	MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION,	
	MISSION_STAGE_CHASE,				
	MISSION_STAGE_TAKE_TARGET_TO_MAUDE,
	MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET,
	MISSION_STAGE_MISSION_PASSED,
	//Additional stages
	MISSION_STAGE_LOSE_THE_COPS,
	MISSION_STAGE_TARGET_FLEES_AGAIN,
	MISSION_STAGE_MISSION_OVER_BAIL_JUMPER_KILLED,
	MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE						
ENDENUM

/// PURPOSE: each mission stage uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

/// PURPOSE: used to set the way the player first spots the bail jumper
ENUM BB_PLAYER_SPOTS_BAIL_JUMPER_TYPE_ENUM
	BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE = 0,
	BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER,
	BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE,
	BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_INTIMIDATED,
	
	BB_PSBJT_INVALID
ENDENUM

/// PURPOSE: assets requested during the mission
ENUM MISSION_REQ
	RQ_TEXT,
	RQ_ANIMS,
	RQ_PHONE_MODEL,
	RQ_MAUDE_ANIM
ENDENUM

/// PURPOSE: list different ways player can pass the mission
ENUM MISSION_PASSED_CONDITION
	BB_PASSED_DEFAULT,
	BB_PASSED_TARGET_CAPTURED,
	BB_PASSED_TARGET_KILLED
ENDENUM

/// PURPOSE: fail conditions
ENUM BAIL_BOND_FAILED_REASON_ENUM
	BB_FAILED_DEFAULT = 0,
	BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER,
	BB_FAILED_BAIL_JUMPER_ESCAPED,
	BB_FAILED_MAUDE_DIED,
	BB_FAILED_MAUDE_ATTACKED
ENDENUM

/// used to determine the specific scenario type the bail bond is
ENUM BAILBOND_TYPE
	BBT_SURRENDER,
	BBT_BRAWL,
	BBT_ONFOOT_FLEE,
	BBT_VEHICLE_FLEE,
	BBT_SHOOTOUT,
	BBT_BASE_JUMP
ENDENUM

/// PURPOSE: ai states for the buddy peds
ENUM BUDDY_STATE
	BS_WAITING,
	BS_WAIT_PICK_REACT,
	BS_COMBAT,
	BS_BREAKOUT_ANIM,
	BS_FLEE,
	BS_DEAD
ENDENUM

/// PURPOSE: used to update mission peds behaviour
ENUM BB_PED_AI
	//BB_PED_AI_SETUP_RELAXED,
	BB_PED_AI_STATE_RELAXED,
	BB_PED_AI_SETUP_ROUTINE,
	BB_PED_AI_STATE_ROUTINE,
	BB_PED_AI_SETUP_AWARE_OF_PLAYER,
	BB_PED_AI_STATE_AWARE_OF_PLAYER,
	BB_PED_AI_SETUP_MOVE_TOWARDS_PLAYER,
	BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER,
	BB_PED_AI_SETUP_MELEE_ATTACK,
	BB_PED_AI_STATE_MELEE_ATTACK,
	BB_PED_AI_SETUP_BEATEN_UP,
	BB_PED_AI_STATE_BEATEN_UP,
	BB_PED_AI_SETUP_PLAYER_OUT_OF_REACH,
	BB_PED_AI_STATE_PLAYER_OUT_OF_REACH,
	BB_PED_AI_SETUP_FLEE_ON_FOOT,
	BB_PED_AI_STATE_FLEE_ON_FOOT,
	BB_PED_AI_SETUP_FLEE_TO_VEHICLE,
	BB_PED_AI_STATE_FLEE_TO_VEHICLE,
	BB_PED_AI_SETUP_FLEE_IN_VEHICLE,
	BB_PED_AI_STATE_FLEE_IN_VEHICLE,
	BB_PED_AI_SETUP_FRIGHTENED,
	BB_PED_AI_STATE_FRIGHTENED,
	BB_PED_AI_SETUP_FOLLOW_PLAYER,
	BB_PED_AI_STATE_FOLLOW_PLAYER,
	BB_PED_AI_SETUP_SURRENDERED,
	BB_PED_AI_STATE_SURRENDERED,
	BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND,
	BB_PED_AI_STATE_PLAYER_LEFT_BEHIND,
	BB_PED_AI_SETUP_WAITING_IN_VEHICLE,
	BB_PED_AI_STATE_WAITING_IN_VEHICLE,
	BB_PED_AI_SETUP_AIM_GUN_AND_ADVANCE,
	BB_PED_AI_STATE_AIM_GUN_AND_ADVANCE,
	BB_PED_AI_SETUP_GUN_COMBAT,
	BB_PED_AI_STATE_GUN_COMBAT,
	BB_PED_AI_SETUP_TEMP_OVERRIDE_AI_CONTROL,	// used when we need to perform a task which has to be handled outside of BB_PED_AI 
	BB_PED_AI_STATE_TEMP_OVERRIDE_AI_CONTROL	// used when we need to perform a task which has to be handled outside of BB_PED_AI 
ENDENUM

/// PURPOSE: track which clipset bail jumper is playing
ENUM BB_JUMPER_MOVEMENT_CLIPSET_STATE
	BB_JUMPER_MOVEMENT_CLIPSET_UNSET,
	BB_JUMPER_MOVEMENT_CLIPSET_NORMAL,
	BB_JUMPER_MOVEMENT_CLIPSET_TAZERED
ENDENUM

//----------------------------------------------------------------------------------------------------------------
//		STRUCTS
//----------------------------------------------------------------------------------------------------------------

/// PURPOSE: used by scripted peds in the mission - bail jumper, buddies
///    AI uses BB_PED_AI to set behaviours
STRUCT STRUCT_MISSION_PED
	AI_BLIP_STRUCT 	blipAIStruct
	BLIP_INDEX		blipIndex
	PED_INDEX 		pedIndex
	BB_PED_AI		AI
	MODEL_NAMES		mnModel
	WEAPON_TYPE 	wtWeapon = WEAPONTYPE_INVALID
	VECTOR			vPosition
	FLOAT			fHeading
	INT				iTimer
	INT				iAiDelayTimer
	INT				iFrameCountLastSeenPlayer
ENDSTRUCT

STRUCT BUDDY
	PED_INDEX index
	VECTOR vPos
	FLOAT fDir	
	WEAPON_TYPE wtWeapon = WEAPONTYPE_INVALID
	AI_BLIP_STRUCT blipAIStruct
	BUDDY_STATE state
	INT iThinkTime //= GET_RANDOM_INT_IN_RANGE( 500, 1000 )
	INT iTimeActivated
	BOOL bActivated = FALSE
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT BAILBOND_PED
	PED_INDEX index
	VECTOR vPos
	FLOAT fDir
	WEAPON_TYPE wtWeapon = WEAPONTYPE_INVALID
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	INT iPedTimer
ENDSTRUCT

/// PURPOSE: used by scripted vehicles in the mission - musician's car
STRUCT STRUCT_MISSION_VEHICLE
	VEHICLE_INDEX 	vehIndex
	MODEL_NAMES		mnModel
	VECTOR			vPosition
	FLOAT			fHeading
ENDSTRUCT

STRUCT BAILBOND_VEHICLE
	VEHICLE_INDEX index
	VECTOR vPos
	FLOAT fDir
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT BAILBOND_OBJECT
	OBJECT_INDEX index
	VECTOR vPos
	FLOAT fDir	
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT BAILBOND_PROP
	OBJECT_INDEX index
	VECTOR vPos
	VECTOR vRot
	FLOAT fDir	
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT BAILBOND_PICKUP
	PICKUP_INDEX index
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vPos
	//VECTOR vRot
	//FLOAT fDir	
	PICKUP_TYPE type = PICKUP_CUSTOM_SCRIPT
	INT iPlacementFlags
ENDSTRUCT

//----------------------------------------------------------------------------------------------------------------
//		CONSTANTS
//----------------------------------------------------------------------------------------------------------------

CONST_FLOAT BAIL_BONDS_RANGE_TO_TARGET_BLIP				35.0
CONST_FLOAT BAIL_BONDS_RANGE_TO_TARGET_ACTIVE			25.0	//22.0
CONST_FLOAT BAIL_BONDS_INTIMIDATING_DETECTION_RANGE		25.0
CONST_FLOAT BAIL_BONDS_ESCAPED_DISTANCE 				150.0		// fail distance for target escaping during chase
CONST_FLOAT BAIL_BONDS_CLEANUP_NOT_ACTIVATED_DISTANCE 	200.0		// fail distance for target escaping during chase
CONST_FLOAT BAIL_BONDS_CHASE_BLIP_FLASH_PERCENTAGE		0.65		// targets blip flashes when he gets further than this percentage away to BAIL_BONDS_ESCAPED_DISTANCE
CONST_INT BAIL_BONDS_CP_AFTER_MOCAP             		0			// Checkpoint
CONST_INT BAIL_BONDS_MAX_OBJECTS 						8
CONST_INT BAIL_BONDS_MAX_PROPS 							4
CONST_INT BAIL_BONDS_MAX_PICKUPS						1
CONST_INT BAIL_BONDS_MAX_VEHICLES						6
CONST_INT BAIL_BONDS_MAX_CHASE_POINTS					10
CONST_INT BAIL_BONDS_MAX_COP_CONVS						3
CONST_INT BAIL_BONDS_MAX_DRIVE_CONVOS					3
CONST_INT BAIL_BONDS_MAX_BACKUP_PEDS					6
CONST_INT MAUDE_1_CAMERA_CUT_TREV						0
CONST_INT MAUDE_1_CAMERA_CUT_MAUDE						1
CONST_INT MAUDE_1_MAX_CAMERA_CUTS						2
CONST_INT TREVOR_SPEAKER_ID								2			// Conversation speaker ID
CONST_INT BAIL_JUMPER_SPEAKER_ID						3			// Conversation speaker ID
CONST_INT MAUDE_SPEAKER_ID								4			// Conversation speaker ID
CONST_INT CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD		10			// grace period in frame count for can ped see ped tests
CONST_INT LEAVE_PLAYER_GROUP_DISTANCE					25
CONST_INT REJOIN_PLAYER_GROUP_DISTANCE					8
CONST_INT DIST_LIMIT_PLAYER_TO_PED_FOR_DIALOGUE			30	
CONST_INT CP_BB1_TRIGGER_BAIL_JUMPER_REACTION			0			// Initial mission replay checkpoint Note: this doesn't get set by the mission, but is used to skip over the phonecall
CONST_INT CP_BB1_MISSION_PASSED							1			// 1st mission replay checkpoint - used by shitskip
CONST_INT Z_SKIP_LOCATE_BAIL_JUMPER						0			// z skip stage
CONST_INT Z_SKIP_TRIGGER_BAIL_JUMPER_REACTION			1			// z skip stage
CONST_INT Z_SKIP_CHASE_BAIL_JUMPER						2			// z skip stage
CONST_INT Z_SKIP_TAKE_JUMPER_TO_MAUDE					3			// z skip stage
CONST_INT Z_SKIP_OUTRO_CUTSCENE							4			// z skip stage
CONST_INT Z_SKIP_MISSION_PASSED							5			// z skip stage
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//		VARIABLES
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD	// debug stage skipping
	CONST_INT 					MAX_SKIP_MENU_LENGTH 6
	MissionStageMenuTextStruct 	mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

MISSION_STAGE 							eMissionStage 	= MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER		//track what MISSION stage we are at
MISSION_STAGE 							eMissionSkipTargetStage											//used in mission checkpoint setup and debug stage skipping
SUB_STAGE 								eSubStage 		= SS_SETUP										//Internal state tracking for mission stages
BB_PLAYER_SPOTS_BAIL_JUMPER_TYPE_ENUM	ePlayerSpotsBailJumperType
BAIL_BOND_FAILED_REASON_ENUM 			eBB_MissionFailedReason
BAIL_BOND_LAUNCH_DATA 					sBailBondLaunchData			// information set by the launcher which gets passed into the mission script
BAILBOND_TYPE 							eBailBondType				// used to pick specific mission behaviour
STRUCT_MISSION_PED 						sBailJumperPed				// the bail jumper
BB_JUMPER_MOVEMENT_CLIPSET_STATE		eBailJumperClipset			// track which clipset jumper is using
BUDDY 									sBackupPed[BAIL_BONDS_MAX_BACKUP_PEDS]				// the bail jumper's backup peds
BAILBOND_VEHICLE 						sMissionVehicle[BAIL_BONDS_MAX_VEHICLES]	// vehicles positioned for the mission
BAILBOND_OBJECT 						sObjectBailJumperPhone //sMissionObject[BAIL_BONDS_MAX_OBJECTS]		// objects positioned for the mission
BAILBOND_PROP 							sMissionProp[BAIL_BONDS_MAX_PROPS]			// props used by the mission
BAILBOND_PICKUP							sMissionPickup[BAIL_BONDS_MAX_PICKUPS]		// pickups used by the mission
BAILBOND_PED 							sMaude
BAILBOND_OBJECT 						sObjMaudeChair
BAILBOND_OBJECT							sObjMaudeLaptop
BAILBOND_OBJECT							sObjMaudeTable
BAILBOND_OBJECT							sObjMaudeRadio					// needed to prevent bug 1712317

BOOL bDoneDialogue_BailJumperBeginsToFlee
BOOL bDoneDialogue_BailJumperSurrendered
BOOL bDoneDialogue_TrevorResponsesToBailJumperSurrendering
BOOL bDoneDialogue_BailJumperNoticesCops
BOOL bDoneDialogue_DriveToMaudes[BAIL_BONDS_MAX_DRIVE_CONVOS] 
BOOL bDoneDialogue_PlayerSpotsBailJumperCloseDistance
BOOL bDoneDialogue_PlayerSpotsBailJumperFromDistance
bool bDoneDialogue_BailJumperRespondToPlayerOrders
BOOL bDoneDialogue_PlayerGetsInVehicle
BOOL bDoneDialogue_PlayerGetsOutVehicle
BOOL bDoneDialogue_BailJumperSpotsPlayerFirst
BOOL bDoneDialogue_PlayerJackingPed
BOOL bDoneDialogue_IdleInVehicle
BOOL bDoneDialogue_MissionFailed
BOOL bDoneDialogue_BailJumperKilled
BOOL bDoneObjective_TakeBailJumperToMaude
BOOL bDoneObjective_ApproachBailJumper
BOOL bDoneObjective_ApprehendBailJumper
BOOL bDoneObjective_LoseWantedLevel
BOOL bDoneObjective_ReturnToBailJumper
BOOL bDoneHelp_UnsuitableVehicle
BOOL bFinishedStageSkipping	= TRUE 		//used to determine if we are mission replay checkpoint skipping or debug skipping
BOOL bLoadedWorldForStageSkipping = FALSE	// flag to say if we haveloaded the world around the player when stage skipping
BOOL bSetMaudeFleeSyncSceneExit = FALSE
//BOOL bAppliedParachuteTask = FALSE
BOOL bCleanupBailJumperLocationDuringDropOff
BOOL bConvoCoolDownActive = FALSE
BOOL bHasBailJumperSurrendered
BOOL bHasPlayerLeftBailJumperBehind
BOOL bRequireMissionCleanup = FALSE	// used in the Script_Cleanup to determine if the mission has done it's setup and therefore needs a full cleanup
BOOL bSetupMaude
BOOL bRequestedOutroMocap
BOOL bIsJumperRevertingBackToFlee
BOOL bHasJumperBeenTazered

BLIP_INDEX blipIndexMaudesPlace

FLOAT fHeading_PlayerMissionStart					// heading player gets set to when skipping back to the start of the the mission
FLOAT fHeading_PlayerWarpGetNearTarget				// heading player gets set to when skipping the mission stage - MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
FLOAT fHeading_PlayerWarpApproachTarget				// heading player gets set to when skipping the mission stage - MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION

INT iChaseIndex = 0
INT iConvoCoolDown = 0
INT iDialogueTimer = 0
INT iDialogueTimerHandsUpDuringDropOff = 0
INT iDialogueTimer_MaudeAmbient
INT iFlashBlipTimer = 0
INT iFlashBlipGodTextTimer = 0
INT iNumMissionProps										// the total number of mission props
INT iNumMissionVehicles										// the total number of mission vehicles
INT iNumMissionObjects										// the total number of mission objects
INT iNumMissionPickups										// the total number of mission pickups
INT iNumChaseRoutePositions										// the total number of goto locations
INT iNumTargetBackupPeds									// the total number of bail jumper backup peds
INT iFrameCountPlayerLastSeenBailJumper
INT iDelay_SurrenderForPlayerCloseToWhileFleeing
INT iTime_BailJumperStartedFleeingOnFoot
INT iTimer_BailJumperVehicleStopped
INT iTimer_DelayMissionOverBailJumperKilled					// timer used to advance to mission over after killing bail jumper, if convo or post script isn't launched in time
INT iBailJumperHealthOnBeganFleeAgain
INT iDialogueTimer_IdleInVehicle
INT iSyncScene_MaudeReact
INT iTimerLowerHandsDelayForGetUp

SCENARIO_BLOCKING_INDEX 	scenarioBlockingArea_BailJumperLocation
SCENARIO_BLOCKING_INDEX 	ScenarioBlockArea_CutsceneMaude

SEQUENCE_INDEX sequenceAI

STRING sBailJumpers_VoiceID
STRING sDialogue_BailJumperBeginChase
STRING sDialogueRoot_BailJumperSurrenders
STRING sDialogueRoot_DriveToMaudesPlace[BAIL_BONDS_MAX_DRIVE_CONVOS]
STRING sMissionTextBlock = "BB1AUD"
STRING sCommonTextBlock = "BBCAUD"
STRING sSceneHandle_Trevor	= "Trevor"
STRING sSceneHandle_Maude	= "Maude"
STRING sSceneHandle_BailJumper = "maude_criminal_1"
STRING sSceneHandle_MaudeChair = "maude_chair"
STRING sSceneHandle_MaudeLaptop = "maude_laptop"

structPedsForConversation 	sDialogue

REL_GROUP_HASH 				HASH_BAILBOND_TARGET

TEXT_LABEL_23 tlDialogueRoot_MissionFailed = ""
TEXT_LABEL_23 tlOutroMocapName = "MAUDE_MCS_2"

VECTOR vMaudePlace
VECTOR vPlayerPos
VECTOR vChaseRoute[BAIL_BONDS_MAX_CHASE_POINTS]
VECTOR vTargetCover
VECTOR vPos_PlayerMissionStart						// pos player gets warped to when skipping back to the start of the the mission
VECTOR vPos_PlayerWarpGetNearTarget					// pos player gets warped to when skipping the mission stage - MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
VECTOR vPos_PlayerWarpApproachTarget				// pos player gets warped to when skipping the mission stage - MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION
VECTOR vPos_JumperBeganFleeAgain
VECTOR vPos_ProjectileFleeingFrom

VEHICLE_INDEX vehIndex_MissionReplayRestore

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:DEBUG FUNCS / PROCS / WIDGETS
//-------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_ForceFail 					= FALSE
	BOOL bDebug_ChangeTargetComponent 		= FALSE
	INT bDebug_PedVar						= 0
	INT bDebug_PedComponent 				= 0
	INT bDebug_PedTexture					= 0
	BOOL bDebug_DrawChaseRoutePositions			= FALSE
	BOOL bDebug_CallClearAreaOffObjects		= FALSE
	VECTOR vDebug_ClearAreaPos	= << 2727, 4146.5, 44.3 >>
	FLOAT fDebug_ClearAreaRadius = 15.0
	INT iDebug_ClearAreaFlag = 0
	
	/// PURPOSE:
	/// My debug mission widget groups, which get created in RAG->SCRIPT
	PROC SETUP_MISSION_WIDGET()
		widgetGroup = START_WIDGET_GROUP("BAIL BOND 1 - MISSION WIDGET")
			ADD_WIDGET_BOOL("TTY Toggle - Print Bail Bonds Debug Info", bDebug_PrintBailBondInfoToTTY)			
			ADD_WIDGET_BOOL("Draw Chase Route Positions", bDebug_DrawChaseRoutePositions)	
			ADD_WIDGET_BOOL("Force Fail", bDebug_ForceFail)		
			START_WIDGET_GROUP("Target Variations")
				ADD_WIDGET_BOOL("Process Variation change", bDebug_ChangeTargetComponent)
				ADD_WIDGET_INT_SLIDER("Var Number", bDebug_PedVar, 0, 5, 1)
				ADD_WIDGET_INT_SLIDER("Texture Number", bDebug_PedTexture, 0, 5, 1)
				ADD_WIDGET_INT_SLIDER("Component Number", bDebug_PedComponent, 0, 12, 1)				
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Maude's object clear area")
				ADD_WIDGET_BOOL("TTY Toggle - bDebug_CallClearAreaOffObjects : ", bDebug_CallClearAreaOffObjects)
				ADD_WIDGET_VECTOR_SLIDER("vDebug_ClearAreaPos", vDebug_ClearAreaPos, -9000.0, 9000.0, 0.1)		
				ADD_WIDGET_FLOAT_SLIDER("fDebug_ClearAreaRadius : ", fDebug_ClearAreaRadius, 0.0, 50.0, 0.01)
				ADD_WIDGET_INT_SLIDER("iDebug_ClearAreaFlag : ", iDebug_ClearAreaFlag, 0, 10, 1)
				
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()		
	ENDPROC
		
	/// PURPOSE:
	///    updates my mission widgets, based off RAG input
	PROC MAINTAIN_MISSION_WIDGETS()
		INT i
		
		IF bDebug_CallClearAreaOffObjects
			IF iDebug_ClearAreaFlag = 0
			OR iDebug_ClearAreaFlag = 2
			OR iDebug_ClearAreaFlag = 4
			OR iDebug_ClearAreaFlag = 8
				CLEAR_AREA_OF_OBJECTS(vDebug_ClearAreaPos, fDebug_ClearAreaRadius, INT_TO_ENUM(CLEAROBJECTS_FLAGS, iDebug_ClearAreaFlag))
				CPRINTLN(DEBUG_MISSION, "bDebug_CallClearAreaOffObjects done : iDebug_ClearAreaFlag = ", iDebug_ClearAreaFlag, "fDebug_ClearAreaRadius : ", fDebug_ClearAreaRadius, " vDebug_ClearAreaPos : ", vDebug_ClearAreaPos)
			ELSE
				CPRINTLN(DEBUG_MISSION, "bDebug_CallClearAreaOffObjects failed iDebug_ClearAreaFlag not valid : ", iDebug_ClearAreaFlag)
			ENDIF
			bDebug_CallClearAreaOffObjects = FALSE
		ENDIF
		
		IF bDebug_ForceFail
			eMissionStage = MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
			eSubStage = SS_SETUP
			bDebug_ForceFail = FALSE
		ENDIF			
		IF bDebug_ChangeTargetComponent
			IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
				SET_PED_COMPONENT_VARIATION(sBailJumperPed.pedIndex, INT_TO_ENUM(PED_COMPONENT, bDebug_PedComponent), bDebug_PedVar, bDebug_PedTexture)
			ENDIF
			bDebug_ChangeTargetComponent = FALSE
		ENDIF	
		IF bDebug_DrawChaseRoutePositions
			IF iNumChaseRoutePositions > 0
				SET_DEBUG_ACTIVE(TRUE)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				FOR i = 0 TO (iNumChaseRoutePositions - 1)
					DRAW_DEBUG_SPHERE(vChaseRoute[i], 5.0, 255, 0, 0, 150)
					DRAW_DEBUG_SPHERE(vChaseRoute[i], 1.0, 0, 225, 0, 150)
				ENDFOR
			ENDIF
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	/// 	removes my debug mission widget group
	PROC CLEANUP_MISSION_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	ENDPROC
#ENDIF

/// PURPOSE:
///    Performs the CAN_PED_SEE_PED check and sets the specified FrameCounter if it returned TRUE
///    counter is used as a grace period for checking the ped has seen the player, as CAN_PED_SEE_PED takes several frames to return
/// PARAMS:
///    pedIndex - the ped to test against the player
///    iFrameCountLastSeenPlayer - the ped's last seen player counter
///    fOverride_PedViewCone - the ped's viewing cone - 170 is the default value
///    eBodyPart - set specific body part to check can be seen, default picks part at random
PROC UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(PED_INDEX pedIndex, INT &iFrameCountLastSeenPlayer, FLOAT fOverride_PedViewCone = 170.0, THREAT_TEST_BODY_PART_ENUM eBodyPart = TT_BODY_PART_RANDOM)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF CAN_PED_SEE_PED(pedIndex, PLAYER_PED_ID(), fOverride_PedViewCone, TRUE, DEFAULT, eBodyPart)
			iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER - CAN_PED_SEE_PED returned TRUE so set frame count to : ", iFrameCountLastSeenPlayer) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the CAN_PED_SEE_PED check and sets the specified FrameCounter if it returned TRUE
///    counter is used as a grace period for checking the ped has seen the player, as CAN_PED_SEE_PED takes several frames to return
/// PARAMS:
///    pedIndex - the ped to test against the player
///    iFrameCountLastSeenPlayer - the ped's last seen player counter
///    fOverride_PedViewCone - the ped's viewing cone - 170 is the default value
///    eBodyPart - set specific body part to check can be seen, default picks part at random
PROC UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(PED_INDEX pedToSeeIndex, INT &iFrameCountLastSeenPed, FLOAT fOverride_PlayerViewCone = 170.0, THREAT_TEST_BODY_PART_ENUM eBodyPart = TT_BODY_PART_RANDOM)
	IF IS_ENTITY_ALIVE(pedToSeeIndex)
		IF CAN_PED_SEE_PED(PLAYER_PED_ID(), pedToSeeIndex, fOverride_PlayerViewCone, TRUE, DEFAULT, eBodyPart)
			iFrameCountLastSeenPed = GET_FRAME_COUNT()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER - CAN_PED_SEE_PED returned TRUE so set frame count to : ", iFrameCountLastSeenPed) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    check if a ped can see the player, based off their INT counter which records the last frame they
///    were classed as being able to see the player using - UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER
/// RETURNS:
///    TRUE if ped can see player this frame
FUNC BOOL BB_CAN_PED_SEE_PLAYER_THIS_FRAME(INT &iFrameCounter_PedLastSeePlayer)
	IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the player ped has a clear line of sight to a ped who is also visible on screen
/// PARAMS:
///    pedToSeeIndex - ped to check player can see
///    iFrameCounter_PlayerLastSeenPed - counter to track last frame player could see the ped
///    bDoRangeAndHeightChecks - if true results will return false if the player is out of range/height 
///    fPlayerDetectionDist - max dist for seeing range
///    fHeightDiffTolerance - max height diff for seeing range
/// RETURNS:
///    TRUE if player has clear line of sight and ped is visible on screen
FUNC BOOL BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN(PED_INDEX pedToSeeIndex, INT iFrameCounter_PlayerLastSeenPed, BOOL bDoRangeAndHeightChecks = TRUE, FLOAT fPlayerDetectionDist = BAIL_BONDS_RANGE_TO_TARGET_ACTIVE, FLOAT fHeightDiffTolerance = 6.0)
	IF IS_ENTITY_ALIVE(pedToSeeIndex)
		IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PlayerLastSeenPed, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test PLAYER has seen the ped in the last 10 frames
			IF bDoRangeAndHeightChecks	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, fPlayerDetectionDist)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN - return FALSE : out of detection range") ENDIF #ENDIF
					RETURN FALSE
				ENDIF
				IF NOT IS_PLAYER_AT_SAME_HEIGHT_AS_PED(pedToSeeIndex, vPlayerPos, fHeightDiffTolerance)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN - return FALSE : out of height diff tolerance") ENDIF #ENDIF
					RETURN FALSE
				ENDIF				
			ENDIF		
			IF IS_ENTITY_ON_SCREEN(pedToSeeIndex)
				IF NOT IS_ENTITY_OCCLUDED(pedToSeeIndex)
				//IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedToSeeIndex, 5)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN - return TRUE : framecount :", GET_FRAME_COUNT()) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is intimidating a ped enough that they should react
///    NOTE: doesn't take into account player weapon
/// PARAMS:
///    pedIndex - Ped who the player is intimidating
///    iFrameCounter_PedLastSeePlayer - the frame count for the last time the pedIndex could see the player
///    fPlayerDetectionDist - Player is automatically classed as intimidating when shooting or targetting within this range
///    bAimingCloseToPedCounts - if true player aiming close to the ped counts
///    fBulletRadiusCheck - Used for IS_BULLET_IN_AREA check around pedIndex
///    fProjectileRadiusCheck - Used for IS_PROJECTILE_IN_AREA check around pedIndex
///    bVisibleWeaponIsIntimidating - if true and player is seen with weapon drawn func will return true
/// RETURNS:
///    True if bullet is in area around ped, if player is shooting, if player is targetting or free aiming at ped in view or if damaged.
FUNC BOOL IS_PLAYER_INTIMIDATE_SPECIFIC_PED(PED_INDEX pedIndex, INT iFrameCounter_PedLastSeePlayer, FLOAT fPlayerDetectionDist = 20.0, BOOL bAimingCloseToPedCounts = FALSE, FLOAT fBulletRadiusCheck = 8.0, FLOAT fProjectileRadiusCheck = 15.0, BOOL bVisibleWeaponIsIntimidating = FALSE)
	IF IS_PED_UNINJURED(pedIndex)	
		VECTOR vTemp_PedPos = GET_ENTITY_COORDS(pedIndex)	
		IF IS_BULLET_IN_AREA(vTemp_PedPos, fBulletRadiusCheck, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - bullet in area around ped") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fPlayerDetectionDist)	//and is with in the range
			IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
				IF bVisibleWeaponIsIntimidating
					IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
						// B*1511108 - try to ensure weapon is visible
						IF IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
						OR IS_PED_RELOADING(PLAYER_PED_ID())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing has leathal weapon in view") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF				
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing targeting ped") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
						//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing free aiming at ped") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
					IF bAimingCloseToPedCounts
						IF IS_PLAYER_FREE_AIMING(PLAYER_ID())	
							IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedIndex, 45.0)
								//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing free aiming around ped") ENDIF #ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_SHOOTING(PLAYER_PED_ID())	
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player shooting with weapon") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
				OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player seen in combat ") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player seen performing stealth kill") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - ped damaged by player with weapon") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())
		OR HAS_PED_RECEIVED_EVENT(pedIndex, EVENT_PLAYER_COLLISION_WITH_PED)
			IF IS_PED_RAGDOLL(pedIndex)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED check - PLAYER touching PED and ped is ragdoll") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vTemp_PedPos, fProjectileRadiusCheck)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - explosion in area around ped") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		VECTOR vMin, vMax // is one of the player's projectiles nearby?
		vMin = vTemp_PedPos
		vMax = vMin
		vMin.x= vMin.x - fProjectileRadiusCheck
		vMin.y = vMin.y -fProjectileRadiusCheck
		vMin.z = vMin.z - fProjectileRadiusCheck
		vMax.x = vMax.x + fProjectileRadiusCheck
		vMax.y = vMax.y + fProjectileRadiusCheck
		vMax.z = vMax.z + fProjectileRadiusCheck
		IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - projectile in area around ped : ", "return TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    checks to see if Maude should flee
/// PARAMS:
///    bCheckPlayerWanted - if TRUE checks the player's wanted level and returns TRUE if he has one
/// RETURNS:
///    TRUE if reason for Maude to flee
FUNC BOOL SHOULD_MAUDE_FLEE(BOOL bCheckPlayerWanted = FALSE)

	IF IS_PED_UNINJURED(sMaude.index)
	
		// Check for Maude taking damage
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(sMaude.index)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sMaude.index)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(sMaude.index)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude took damage : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// Check for player pushing them with their car
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) 					// Ignore player knocking them with car door (in vehicle, but not sitting= must be exiting)
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sMaude.index)
			//OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(objMaudeTable)	
				CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude pushed by player vehicle : FC = ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
			IF DOES_ENTITY_EXIST(sObjMaudeChair.index)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sObjMaudeChair.index)	
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's chair pushed by player vehicle : FC = ", GET_FRAME_COUNT())
					RETURN TRUE
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(sObjMaudeTable.index)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sObjMaudeTable.index)	
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's table pushed by player vehicle : FC = ", GET_FRAME_COUNT())
					RETURN TRUE
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(sObjMaudeLaptop.index)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sObjMaudeLaptop.index)
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's laptop pushed by player vehicle : FC = ", GET_FRAME_COUNT())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// code event check for EVENT_POTENTIAL_GET_RUN_OVER
		IF HAS_PED_RECEIVED_EVENT(sMaude.index, EVENT_POTENTIAL_GET_RUN_OVER)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's received event EVENT_POTENTIAL_GET_RUN_OVER : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// check for ragdoll
		IF IS_PED_RAGDOLL(sMaude.index)
			IF IS_ENTITY_AT_ENTITY(sMaude.index, PLAYER_PED_ID(), <<3.0, 3.0, 3.0>>)				// Player close (so they bumped into them)
				CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's ragdolled with player close by : FC = ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
		ENDIF
		
		// code event check for EVENT_PED_COLLISION_WITH_PLAYER
		IF HAS_PED_RECEIVED_EVENT(sMaude.index, EVENT_PED_COLLISION_WITH_PLAYER)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's received event EVENT_PED_COLLISION_WITH_PLAYER : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// check for player aiming at RC character
		IF IS_PLAYER_VISIBLY_TARGETTING_PED(sMaude.index)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Player visibly targetting Maude : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
					
		// Check for player shooting nearby
		IF IS_PLAYER_SHOOTING_NEAR_PED(sMaude.index)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Player shooting near Maude : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// Check for explosions nearby
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sMaude.index), 15)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Explosion near Maude : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// code event check for EVENT_RESPONDED_TO_THREAT
		IF HAS_PED_RECEIVED_EVENT(sMaude.index, EVENT_RESPONDED_TO_THREAT)
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude's received event EVENT_RESPONDED_TO_THREAT : FC = ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF
		
		// flee if player is wanted
		IF bCheckPlayerWanted
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : player has wanted level : FC = ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SHOULD_MAUDE_FLEE() return TRUE : Maude injured : FC = ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   cleans up the bail jumper's buddy ped's combat blips
PROC CLEANUP_BUDDY_COMBAT_BLIPS(INT iNumBuddies)
	INT i
	FOR i = 0 TO (iNumBuddies - 1)
		CLEANUP_AI_PED_BLIP(sBackupPed[i].blipAIStruct)
	ENDFOR
ENDPROC

/// PURPOSE:
///    give a ped a flee from player task
/// PARAMS:
///    pedIndex - specific ped
///    bBlockTempEvents - if ped needs temp events blocking
PROC GIVE_PED_FLEE_ORDER(PED_INDEX pedIndex, BOOL bBlockTempEvents = TRUE)
	IF IS_PED_UNINJURED(pedIndex)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_ALWAYS_FLEE, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedIndex, FA_NEVER_FLEE, FALSE)
		IF NOT IsPedPerformingTask(pedIndex, SCRIPT_TASK_SMART_FLEE_PED) 
			CLEAR_PED_TASKS(pedIndex)
			CLEAR_PED_SECONDARY_TASK(pedIndex)
			TASK_LOOK_AT_ENTITY(pedIndex, PLAYER_PED_ID(), 3000)
			TASK_SMART_FLEE_PED(pedIndex, PLAYER_PED_ID(), 10000, -1)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GIVE_PED_FLEE_ORDER - done, bBlockTempEvents = ", bBlockTempEvents) ENDIF #ENDIF
		ENDIF
		IF bBlockTempEvents
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
		ENDIF			
		SET_PED_KEEP_TASK(pedIndex, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    set all remaining buddy peds to flee
/// PARAMS:
///    iNumBuddies - the total number of buddy peds
PROC BUDDY_SURRENDER(INT iNumBuddies)
	INT i
	FOR i = 0 TO (iNumBuddies-1)
		IF sBackupPed[i].state != BS_DEAD	
			GIVE_PED_FLEE_ORDER(sBackupPed[i].index)
			sBackupPed[i].state = BS_FLEE	
			CLEANUP_AI_PED_BLIP(sBackupPed[i].blipAIStruct)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "BUDDY_SURRENDER set ID = ", i) ENDIF #ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC TELL_TARGET_AIM_STRAFE_COVER_SHOOT(PED_INDEX TargetPed, VECTOR vPos)
	IF IS_PED_UNINJURED(TargetPed)
		SET_PED_ACCURACY(TargetPed, 50)
		OPEN_SEQUENCE_TASK(sequenceAI)
			TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 3000)
			TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vPos, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, TRUE)
			TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 1500, TRUE)
			TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vPos, 50)	// note: defensive area radius shouldn't be set less than 10m after initial get to area as per B*1173588
		CLOSE_SEQUENCE_TASK(sequenceAI)
		TASK_PERFORM_SEQUENCE(TargetPed, sequenceAI)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "TELL_TARGET_AIM_STRAFE_COVER_SHOOT - done, vPos : ", vPos) ENDIF #ENDIF
	ENDIF
ENDPROC

PROC TELL_BUDDY_STAND_SHOOT_COVER(PED_INDEX TargetBuddy)
	IF IS_PED_UNINJURED(TargetBuddy)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TargetBuddy, FALSE)
		OPEN_SEQUENCE_TASK(sequenceAI)
			IF IS_PED_IN_ANY_VEHICLE(TargetBuddy)
				TASK_LEAVE_ANY_VEHICLE(NULL)
			ENDIF
			TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE( 500, 3000 ))
			TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 3500, TRUE)
			TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vTargetCover, 50)	// note: defensive area radius shouldn't be set less than 10m after initial get to area as per B*1173588
			TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
		CLOSE_SEQUENCE_TASK(sequenceAI)
		TASK_PERFORM_SEQUENCE(TargetBuddy, sequenceAI)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "TELL_BUDDY_STAND_SHOOT_COVER - done") ENDIF #ENDIF
	ENDIF
ENDPROC

PROC TELL_BUDDY_STAND_SHOOT(PED_INDEX TargetBuddy)
	IF IS_PED_UNINJURED(TargetBuddy)
		OPEN_SEQUENCE_TASK(sequenceAI)
			IF IS_PED_IN_ANY_VEHICLE(TargetBuddy)
				TASK_LEAVE_ANY_VEHICLE(NULL)
			ENDIF
			TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1) //BS_STAND_SHOOT_INDEF
		CLOSE_SEQUENCE_TASK(sequenceAI)
		TASK_PERFORM_SEQUENCE(TargetBuddy, sequenceAI)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "TELL_BUDDY_STAND_SHOOT - done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    used to select to correct reaction the buddy ped should use
/// PARAMS:
///    TargetBuddy - the buddy ped's struct
PROC PICK_BUDDY_REACTION(INT iBuddyPedID)
	IF IS_PED_UNINJURED(sBackupPed[iBuddyPedID].index)		
		CLEAR_PED_SECONDARY_TASK(sBackupPed[iBuddyPedID].index)
		CLEAR_PED_TASKS(sBackupPed[iBuddyPedID].index)
		TASK_CLEAR_LOOK_AT(sBackupPed[iBuddyPedID].index)
		
		// don't have bums attack if player has a lethal weapon on show
		IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
			GIVE_PED_FLEE_ORDER(sBackupPed[iBuddyPedID].index)
			sBackupPed[iBuddyPedID].state = BS_FLEE	
			CLEANUP_AI_PED_BLIP(sBackupPed[iBuddyPedID].blipAIStruct)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PICK_BUDDY_REACTION set RELEASE  player lethal weapon : ID = ", iBuddyPedID) ENDIF #ENDIF
		ELSE
			// only bums setup to hate Trevor will attack
			IF GET_PED_RELATIONSHIP_GROUP_HASH(sBackupPed[iBuddyPedID].index) = HASH_BAILBOND_TARGET
				SET_PED_HEARING_RANGE(sBackupPed[iBuddyPedID].index, 30.0)
				SET_PED_SEEING_RANGE(sBackupPed[iBuddyPedID].index, 30.0)
				SET_PED_ID_RANGE(sBackupPed[iBuddyPedID].index, 30.0)
				SET_PED_COMBAT_RANGE(sBackupPed[iBuddyPedID].index, CR_NEAR)
				SET_PED_COMBAT_ATTRIBUTES(sBackupPed[iBuddyPedID].index, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(sBackupPed[iBuddyPedID].index, CA_LEAVE_VEHICLES, TRUE)							
				SET_PED_COMBAT_ATTRIBUTES(sBackupPed[iBuddyPedID].index, CA_ALWAYS_FIGHT, TRUE)	// attempt to stop security ped taking cover during brawl
				SET_PED_COMBAT_ATTRIBUTES(sBackupPed[iBuddyPedID].index, CA_USE_COVER, FALSE)	// attempt to stop security ped taking cover during brawl
				SET_PED_FLEE_ATTRIBUTES(sBackupPed[iBuddyPedID].index, FA_NEVER_FLEE, TRUE)
				TASK_LOOK_AT_ENTITY(sBackupPed[iBuddyPedID].index, PLAYER_PED_ID(), -1)
				TASK_TURN_PED_TO_FACE_ENTITY(sBackupPed[iBuddyPedID].index, PLAYER_PED_ID(), -1)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBackupPed[iBuddyPedID].index, TRUE)
				sBackupPed[iBuddyPedID].state = BS_COMBAT	
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PICK_BUDDY_REACTION set FIGHT player non lethal weapon : ID = ", iBuddyPedID) ENDIF #ENDIF
			ELSE
				// peds to watch
				SET_PED_COMBAT_ATTRIBUTES(sBackupPed[iBuddyPedID].index, CA_ALWAYS_FIGHT, FALSE)
				TASK_LOOK_AT_ENTITY(sBackupPed[iBuddyPedID].index, PLAYER_PED_ID(), -1)
				TASK_TURN_PED_TO_FACE_ENTITY(sBackupPed[iBuddyPedID].index, PLAYER_PED_ID(), -1)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBackupPed[iBuddyPedID].index, FALSE)
				sBackupPed[iBuddyPedID].state = BS_FLEE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PICK_BUDDY_REACTION set RELEASE player non lethal weapon : ID = ", iBuddyPedID) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    set all the bail jumper's buddy to react
/// PARAMS:
///    iNumBuddies - the num of buddy peds
PROC ACTIVATE_ALL_BUDDIES(INT iNumBuddies)
	INT i
	FOR i=0 TO (iNumBuddies-1)
		IF IS_PED_UNINJURED(sBackupPed[i].index)
			sBackupPed[i].state = BS_WAIT_PICK_REACT
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    
PROC SET_BAIL_JUMPER_SURRENDERED()
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
		IF sBailJumperPed.AI != BB_PED_AI_SETUP_SURRENDERED
		AND sBailJumperPed.AI != BB_PED_AI_STATE_SURRENDERED
		AND sBailJumperPed.AI != BB_PED_AI_SETUP_FOLLOW_PLAYER
		AND sBailJumperPed.AI != BB_PED_AI_STATE_FOLLOW_PLAYER
			CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, HASH_BAILBOND_TARGET)
			CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_BAILBOND_TARGET, RELGROUPHASH_PLAYER)
			IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
				REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SET_BAIL_JUMPER_SURRENDERED - sBailJumperPed.pedIndex removed from group") ENDIF #ENDIF
			ENDIF
			CLEAR_PED_SECONDARY_TASK(sBailJumperPed.pedIndex)
			CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
			SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, FALSE)	// shouldn't need this after this point - added to help with loading navmesh on change route
			SET_PED_DROPS_WEAPON(sBailJumperPed.pedIndex)
			REMOVE_ALL_PED_WEAPONS(sBailJumperPed.pedIndex)
			SET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, WEAPONTYPE_UNARMED, TRUE)		
			SET_PED_CAN_SWITCH_WEAPON(sBailJumperPed.pedIndex, FALSE)
			SET_PED_ARMOUR(sBailJumperPed.pedIndex, 0)	// clear any armour left when he's surrendered
			
			// cover stuff added for B*1266421
			SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_COVER, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FLEE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_USE_COVER, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
			
			IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex, TRUE)
				TASK_LEAVE_ANY_VEHICLE(sBailJumperPed.pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SET_BAIL_JUMPER_SURRENDERED - tasked leave vehicle FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			iTimerLowerHandsDelayForGetUp = GET_GAME_TIMER()
			sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SET_BAIL_JUMPER_SURRENDERED - done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    check to see if the surrendered bailjumper should delay using follow behaviour
/// RETURNS:
///    TRUE if he hasn't announced he's surrendered yet
FUNC BOOL SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()
	IF NOT bDoneDialogue_BailJumperSurrendered	// wait for the initial dialogue from bail jumper saying he surrenders
		RETURN TRUE
	ENDIF
	IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sDialogueRoot_BailJumperSurrenders, FALSE)	// surrender dialogue on going?
		RETURN TRUE
	ENDIF
	IF eMissionStage != MISSION_STAGE_TAKE_TARGET_TO_MAUDE
	AND eMissionStage != MISSION_STAGE_LOSE_THE_COPS
		RETURN TRUE
	ENDIF
	
	// B*1550554 - force the hands up if he's getting up from a fall
	IF IS_PED_GETTING_UP(sBailJumperPed.pedIndex)
	OR IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
		iTimerLowerHandsDelayForGetUp = GET_GAME_TIMER()
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR return TRUE ragdoll or getting up - done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	IF NOT HAS_TIME_PASSED(iTimerLowerHandsDelayForGetUp, 2000)	
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR return TRUE iTimerLowerHandsDelayForGetUp - done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    currently sets blocking of temp events, relationship group and stops player getting in their vehicle when jacking
/// PARAMS:
///    pedIndex - ped to allow changes to
PROC PED_COMMON_SETUP(PED_INDEX pedIndex)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedIndex, HASH_BAILBOND_TARGET)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_PedsJackingMeDontGetIn, TRUE)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_GetOutBurningVehicle, TRUE)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_RunFromFiresAndExplosions, TRUE)
	// request remove from Imran email 21/05/2013 - no explanation why. SET_PED_CONFIG_FLAG(pedIndex, PCF_DisableHurt, TRUE)	// Fix B*1204129 - stunned peds are dying with SET_PED_DIES_WHEN_INJURED flag
	//SET_PED_DIES_WHEN_INJURED(pedIndex, TRUE)
	SET_PED_TO_INFORM_RESPECTED_FRIENDS(pedIndex, 10.0, 5)
	SET_PED_AS_ENEMY(pedIndex, TRUE)		// B*1535622 - makes sure HUD elements are set for enemy ped
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
ENDPROC

PROC DO_SPECIFIC_SCENARIO_SETUP()

	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)	
		STOP_PED_SPEAKING(sBailJumperPed.pedIndex, TRUE)
		SET_PED_PATH_PREFER_TO_AVOID_WATER(sBailJumperPed.pedIndex, TRUE)
		SET_PED_ARMOUR(sBailJumperPed.pedIndex, 100)
		SET_ENTITY_HEALTH(sBailJumperPed.pedIndex, 200) //Stops peds dying to easily when falling of bikes/quads
		//SET_PED_SUFFERS_CRITICAL_HITS(sBailJumperPed.pedIndex, FALSE)
		SET_PED_CAN_BE_SHOT_IN_VEHICLE(sBailJumperPed.pedIndex, TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBailJumperPed.pedIndex, TRUE)
		SET_PED_DIES_IN_WATER(sBailJumperPed.pedIndex, TRUE)
		SET_PED_DIES_IN_SINKING_VEHICLE(sBailJumperPed.pedIndex, TRUE)
		SET_PED_MAX_TIME_IN_WATER(sBailJumperPed.pedIndex, 2.0)
		SET_PED_MAX_TIME_UNDERWATER(sBailJumperPed.pedIndex, 2.0)
		SET_PED_PATH_PREFER_TO_AVOID_WATER(sBailJumperPed.pedIndex, TRUE)
		SET_PED_PATH_MAY_ENTER_WATER(sBailJumperPed.pedIndex, FALSE)
		SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_RemoveDeadExtraFarAway, TRUE)	
		SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_DisableGoToWritheWhenInjured, TRUE)
		IF sBailJumperPed.wtWeapon != WEAPONTYPE_INVALID
			GIVE_WEAPON_TO_PED(sBailJumperPed.pedIndex, sBailJumperPed.wtWeapon, INFINITE_AMMO, TRUE)
		ENDIF
		PED_COMMON_SETUP(sBailJumperPed.pedIndex)
		SET_PED_DEBUG_NAME(sBailJumperPed.pedIndex, "TARGET_", 0)
			
		sBailJumperPed.AI = BB_PED_AI_STATE_RELAXED
		SET_PED_DEFAULT_COMPONENT_VARIATION(sBailJumperPed.pedIndex)
		
		TASK_PLAY_ANIM(sBailJumperPed.pedIndex, "ODDJOBS@BAILBOND_QUARRY", "prem_producer_argue_a", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
		
		IF NOT DOES_ENTITY_EXIST(sObjectBailJumperPhone.index)
			sObjectBailJumperPhone.index = CREATE_OBJECT(PROP_PHONE_ING, GET_ENTITY_COORDS(sBailJumperPed.pedIndex, FALSE))
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "DO_SPECIFIC_SCENARIO_SETUP - bail jumper phone created") ENDIF #ENDIF
		ENDIF
		IF IS_ENTITY_ALIVE(sObjectBailJumperPhone.index)
			ATTACH_ENTITY_TO_ENTITY(sObjectBailJumperPhone.index, sBailJumperPed.pedIndex, GET_PED_BONE_INDEX(sBailJumperPed.pedIndex, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "DO_SPECIFIC_SCENARIO_SETUP - bail jumper phone attached") ENDIF #ENDIF
		ENDIF
	ENDIF
	INT i		
	FOR i = 0 TO (BAIL_BONDS_MAX_BACKUP_PEDS - 1)
		IF IS_PED_UNINJURED(sBackupPed[i].index)
			
			PED_COMMON_SETUP(sBackupPed[i].index)
			IF sBackupPed[i].wtWeapon != WEAPONTYPE_INVALID
				GIVE_WEAPON_TO_PED(sBackupPed[i].index, sBackupPed[i].wtWeapon, INFINITE_AMMO, TRUE)
			ENDIF
			SET_PED_CONFIG_FLAG(sBackupPed[i].index, PCF_RemoveDeadExtraFarAway, TRUE)	// Bug fix attempt for B*1300191 - peds visibly deleted when sniping from hill top
			SET_PED_DEBUG_NAME(sBackupPed[i].index, "BACKUP_", i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (iNumMissionVehicles - 1)
		IF IS_VEHICLE_OK(sMissionVehicle[i].index)
			// B* 1514608 - bail jumper needs to drive off quicker
			SET_VEHICLE_ENGINE_ON(sMissionVehicle[i].index, TRUE, TRUE)
		ENDIF
	ENDFOR		
ENDPROC

/// PURPOSE:
///    creates all the entities at the bail jumper's location
/// RETURNS:
///    TRUE when all entities are created and setup
FUNC BOOL CREATE_BAIL_JUMPER_SCENE()	
	INT iSpawnedEntities = 0
	INT iSpawnedBuddyPeds = 0
	INT i	
	// spawn target ped
	IF SPAWN_PED(sBailJumperPed.pedIndex, sBailJumperPed.mnModel, sBailJumperPed.vPosition, sBailJumperPed.fHeading)
		IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - target ped spawned ") ENDIF #ENDIF
			iSpawnedEntities++
		ENDIF
	ENDIF	
	// spawn backup peds
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - num back up ped to spawn = ", iNumTargetBackupPeds) ENDIF #ENDIF
	IF iNumTargetBackupPeds > 0
		FOR i=0 TO (iNumTargetBackupPeds - 1)			
			IF SPAWN_PED(sBackupPed[i].index, sBackupPed[i].model, sBackupPed[i].vPos, sBackupPed[i].fDir, FALSE)
				IF IS_PED_UNINJURED(sBackupPed[i].index)

					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - back up ped spawned ID = ", i) ENDIF #ENDIF
					iSpawnedEntities++
					iSpawnedBuddyPeds++
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	// unload the buddy ped models afterwards
	IF iSpawnedBuddyPeds = iNumTargetBackupPeds
		FOR i=0 TO (iNumTargetBackupPeds - 1)
			SET_MODEL_AS_NO_LONGER_NEEDED(sBackupPed[i].model)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - ALL buddy peds spawned so unloading models now - SET_MODEL_AS_NO_LONGER_NEEDED for ped ID = ", i) ENDIF #ENDIF
		ENDFOR
	ENDIF
	// spawn mission vehicles
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - num mission vehicles to spawn = ", iNumMissionVehicles) ENDIF #ENDIF
	IF iNumMissionVehicles > 0
		FOR i=0 TO (iNumMissionVehicles - 1)
			IF SPAWN_VEHICLE(sMissionVehicle[i].index, sMissionVehicle[i].model,  sMissionVehicle[i].vPos, sMissionVehicle[i].fDir)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - mission vehicle spawned ID = ", i) ENDIF #ENDIF
				iSpawnedEntities++
			ENDIF
		ENDFOR
	ENDIF
	/*// spawn mission objects
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - num mission objects to spawn = ", iNumMissionObjects) ENDIF #ENDIF
	IF iNumMissionObjects > 0
		FOR i=0 TO (iNumMissionObjects - 1)
			IF SPAWN_OBJECT(sMissionObject[i].index, sMissionObject[i].model, sMissionObject[i].vPos, sMissionObject[i].fDir) 
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - mission object spawned ID = ", i) ENDIF #ENDIF
				iSpawnedEntities++
			ENDIF
		ENDFOR
	ENDIF	*/
	// spawn mission props
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - num mission props to spawn = ", iNumMissionProps) ENDIF #ENDIF
	IF iNumMissionProps > 0
		FOR i=0 TO (iNumMissionProps - 1)
			IF SPAWN_OBJECT(sMissionProp[i].index, sMissionProp[i].model, sMissionProp[i].vPos, sMissionProp[i].fDir) 
				FREEZE_ENTITY_POSITION(sMissionProp[i].index, TRUE)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - mission prop spawned ID = ", i) ENDIF #ENDIF
				iSpawnedEntities++
			ENDIF
		ENDFOR
	ENDIF
	// spawn mission pickups
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - num mission pickups to spawn = ", iNumMissionPickups) ENDIF #ENDIF
	IF iNumMissionPickups > 0
		FOR i=0 TO (iNumMissionPickups - 1)
			IF SPAWN_PICKUP( sMissionPickup[i].index, sMissionPickup[i].type, sMissionPickup[i].vPos, sMissionPickup[i].iPlacementFlags, sMissionPickup[i].model) 
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_SCENE - mission pickup spawned ID = ", i) ENDIF #ENDIF
				iSpawnedEntities++
			ENDIF
		ENDFOR
	ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - iSpawnedEntities = ", iSpawnedEntities) ENDIF #ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - (iNumTargetBackupPeds + iNumMissionVehicles + iNumMissionObjects + iNumMissionProps + iNumMissionPickups + 1) = ", 
	//(iNumTargetBackupPeds + iNumMissionVehicles + iNumMissionObjects + iNumMissionProps + iNumMissionPickups + 1)) ENDIF #ENDIF
	IF iSpawnedEntities = (iNumTargetBackupPeds + iNumMissionVehicles + iNumMissionObjects + iNumMissionProps + iNumMissionPickups + 1)	// +1 for target ped
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_BAIL_JUMPER_SCENE - scene loaded! frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq)

	SWITCH missionReq
		CASE RQ_TEXT		
			REQUEST_ADDITIONAL_TEXT("BBONDS", MISSION_TEXT_SLOT)	
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "return TRUE - ", "mission text loaded") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "mission text loading...") ENDIF #ENDIF
		BREAK
		
		CASE RQ_MAUDE_ANIM
			REQUEST_ANIM_DICT(GET_MAUDE_IDLE_ANIM_DICT())
			IF HAS_ANIM_DICT_LOADED(GET_MAUDE_IDLE_ANIM_DICT())
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "RQ_MAUDE_ANIM loaded") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "RQ_MAUDE_ANIM loading...") ENDIF #ENDIF
		BREAK
		
		CASE RQ_ANIMS
			REQUEST_ANIM_DICT("ODDJOBS@BAILBOND_QUARRY")			
			IF HAS_ANIM_DICT_LOADED("ODDJOBS@BAILBOND_QUARRY")
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "return TRUE - ", "RQ_ANIMS loaded") ENDIF #ENDIF
				RETURN TRUE
			ENDIF			
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "RQ_ANIMS loading...") ENDIF #ENDIF
		BREAK
		
		CASE RQ_PHONE_MODEL
			REQUEST_MODEL(sObjectBailJumperPhone.model)
			IF HAS_MODEL_LOADED(sObjectBailJumperPhone.model)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SETUP_STAGE_REQUIREMENTS : ", "return TRUE - ", "RQ_PHONE_MODEL skipped no anims") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    sets Maude and the chair into the sync scene
PROC SET_MAUDE_SYNC_SCENE()
	IF IS_PED_UNINJURED(sMaude.index)
		IF IS_ENTITY_ALIVE(sObjMaudeChair.index)
			IF SETUP_STAGE_REQUIREMENTS(RQ_MAUDE_ANIM)
				INT mSyncScene
				mSyncScene = CREATE_SYNCHRONIZED_SCENE(GET_MAUDE_SYNC_SCENE_POSITION(), GET_MAUDE_SYNC_SCENE_ORIENTATION())
				SET_SYNCHRONIZED_SCENE_LOOPED(mSyncScene, TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSyncScene, FALSE)
				TASK_SYNCHRONIZED_SCENE(sMaude.index, mSyncScene, GET_MAUDE_IDLE_ANIM_DICT(), GET_MAUDE_IDLE_ANIM(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
				INT iEntitySyncedSceneFlags = 0
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)	
				PLAY_SYNCHRONIZED_ENTITY_ANIM(sObjMaudeChair.index, mSyncScene, GET_MAUDE_CHAIR_IDLE_ANIM(), GET_MAUDE_IDLE_ANIM_DICT(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iEntitySyncedSceneFlags)	
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMaude.index, TRUE)	
				SET_PED_MONEY(sMaude.index, 0)
				SET_PED_CAN_BE_TARGETTED(sMaude.index, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sMaude.index, RELGROUPHASH_PLAYER)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(sMaude.index, FALSE)	// Maude has serious timber, not realistic for her to vault
				SET_PED_PATH_CAN_DROP_FROM_HEIGHT(sMaude.index, FALSE)	// Maude has serious timber, not realistic for her to vault
				SET_PED_KEEP_TASK(sMaude.index, TRUE)	
				SET_PED_CONFIG_FLAG(sMaude.index, PCF_RunFromFiresAndExplosions, FALSE)	// B*1560870 - stop pop during exit anim
				
				#IF IS_DEBUG_BUILD SET_PED_NAME_DEBUG(sMaude.index, "MISS_MAUDE") #ENDIF
				
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SET_MAUDE_SYNC_SCENE : ", "done") ENDIF #ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    creates and sets up Maude ready for the drop off
/// RETURNS:
///    TRUE is she is uninjured and setup
FUNC BOOL CREATE_AND_SETUP_MAUDE()

	IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, sMaude.vPos, 210.0)// 210.0 I think is the max dist for world brain stream dist
		IF NOT bSetupMaude
			IF SETUP_STAGE_REQUIREMENTS(RQ_MAUDE_ANIM)
			AND SPAWN_OBJECT(sObjMaudeChair.index, sObjMaudeChair.model, sObjMaudeChair.vPos, sObjMaudeChair.fDir)
			AND SPAWN_OBJECT(sObjMaudeLaptop.index, sObjMaudeLaptop.model, sObjMaudeLaptop.vPos, sObjMaudeLaptop.fDir)
				// only spawn Maude once the chair and laptop has been setup
				IF SPAWN_PED(sMaude.index, sMaude.model, sMaude.vPos, sMaude.fDir, TRUE, FALSE, TRUE)
					SET_MAUDE_SYNC_SCENE()
					bSetupMaude = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_AND_SETUP_MAUDE : ", "done") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
		// get handle to Maude's table (done set as mission entity since created by map instead uses para on DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS)
		IF NOT DOES_ENTITY_EXIST(sObjMaudeTable.index)
			IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, sObjMaudeTable.vPos, 150.0)
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(sObjMaudeTable.vPos, 10.0, GET_MAUDE_TABLE_MODEL())
					sObjMaudeTable.index = GET_CLOSEST_OBJECT_OF_TYPE(sObjMaudeTable.vPos, 10.0, GET_MAUDE_TABLE_MODEL(), TRUE)
					IF IS_ENTITY_ALIVE(sObjMaudeTable.index)
						SET_ENTITY_COORDS(sObjMaudeTable.index, GET_MAUDE_TABLE_POSITION())
						SET_ENTITY_HEADING(sObjMaudeTable.index, GET_MAUDE_TABLE_HEADING())
						FREEZE_ENTITY_POSITION(sObjMaudeTable.index, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_AND_SETUP_MAUDE : ", "grabbed handle to table") ENDIF #ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		// get handle to Maude's Radio (done set as mission entity since created by map instead uses para on DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS)
		IF NOT DOES_ENTITY_EXIST(sObjMaudeRadio.index)
			IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, sObjMaudeRadio.vPos, 150.0)
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(sObjMaudeRadio.vPos, 10.0, GET_MAUDE_RADIO_MODEL())
					sObjMaudeRadio.index = GET_CLOSEST_OBJECT_OF_TYPE(sObjMaudeRadio.vPos, 10.0, GET_MAUDE_RADIO_MODEL(), TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "CREATE_AND_SETUP_MAUDE : ", "grabbed handle to radio") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_SPHERE_VISIBLE(sMaude.vPos, 2.5)
			SAFE_REMOVE_PED(sMaude.index, TRUE)
			REMOVE_ANIM_DICT(GET_MAUDE_IDLE_ANIM_DICT())
			SAFE_REMOVE_OBJECT(sObjMaudeChair.index, TRUE)
			SAFE_REMOVE_OBJECT(sObjMaudeLaptop.index, TRUE)			
			SAFE_REMOVE_OBJECT(sObjMaudeTable.index, FALSE)
			SAFE_REMOVE_OBJECT(sObjMaudeRadio.index, FALSE)
			bSetupMaude = FALSE
		ENDIF
	ENDIF
	
	RETURN bSetupMaude
ENDFUNC

/// PURPOSE:
///    Updates the behaviour of the bail jumper ped
PROC UPDATE_BAIL_JUMPER_PED_AI()
	WEAPON_TYPE wtTempCheckForUnarmed
	VEHICLE_INDEX vehTempPlayer
	VEHICLE_SEAT vehSeat
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
	
		// Update bail jumpers movement clipsets when surrendered
		IF eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_UNSET
			UPDATE_PED_TAZERED_STATUS(sBailJumperPed.pedIndex, bHasJumperBeenTazered)
			IF HAS_BAIL_JUMPER_SURRENDER_MOVEMENT_CLIPSETS_LOADED()
				// only set once he's surrendered the first time
				IF bHasBailJumperSurrendered
				OR eMissionStage = MISSION_STAGE_TARGET_FLEES_AGAIN
					IF bHasJumperBeenTazered
						SET_PED_MOVEMENT_CLIPSET(sBailJumperPed.pedIndex, BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED())
						eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_TAZERED
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ^^^ set tazered movement clipset from unset") ENDIF #ENDIF
					ELSE
						SET_PED_MOVEMENT_CLIPSET(sBailJumperPed.pedIndex, BAIL_JUMPER_MOVEMENT_CLIPSET_NORMAL())
						eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_NORMAL
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ^^^ set normal movement clipset from unset") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_NORMAL
			// detect if he needs to switch to tazered version
			UPDATE_PED_TAZERED_STATUS(sBailJumperPed.pedIndex, bHasJumperBeenTazered)			
			IF bHasJumperBeenTazered
				IF HAS_BAIL_JUMPER_SURRENDER_MOVEMENT_CLIPSETS_LOADED()
					SET_PED_MOVEMENT_CLIPSET(sBailJumperPed.pedIndex, BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED())
					eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_TAZERED
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ^^^ set tazered movement clipset from normal clipset") ENDIF #ENDIF					
				ENDIF
			ENDIF			
		ENDIF
		
		IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	//having to test this because changes to tasks breaks the execution / takedown moves
			SWITCH sBailJumperPed.AI
				CASE BB_PED_AI_STATE_RELAXED
					// should be playing the one phone anim
					IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 30.0)
						IF IS_ENTITY_PLAYING_ANIM(sBailJumperPed.pedIndex, "ODDJOBS@BAILBOND_QUARRY", "prem_producer_argue_a")
							STOP_ENTITY_ANIM(sBailJumperPed.pedIndex, "prem_producer_argue_a", "ODDJOBS@BAILBOND_QUARRY", REALLY_SLOW_BLEND_OUT)
						ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_ROUTINE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI BB_PED_AI_STATE_RELAXED -> BB_PED_AI_SETUP_ROUTINE") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_ROUTINE
					IF NOT IS_ENTITY_PLAYING_ANIM(sBailJumperPed.pedIndex, "ODDJOBS@BAILBOND_QUARRY", "prem_producer_argue_a")
						SAFE_REMOVE_OBJECT(sObjectBailJumperPhone.index, TRUE)
						IF NOT IS_PED_ACTIVE_IN_SCENARIO(sBailJumperPed.pedIndex)
							TASK_START_SCENARIO_IN_PLACE(sBailJumperPed.pedIndex, "WORLD_HUMAN_SMOKING", 0, TRUE)
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sBailJumperPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_SETUP_ROUTINE - bail jumper TASK_START_SCENARIO_AT_POSITION ") ENDIF #ENDIF
						ENDIF
						sBailJumperPed.AI = BB_PED_AI_STATE_ROUTINE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_SETUP_ROUTINE -> bBB_PED_AI_STATE_ROUTINE") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_ROUTINE
					//currently an idle state - head track the player when he gets close
					IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 20.0)	// Trevor approach comment dist
						IF NOT IS_PED_HEADTRACKING_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ set look at player -	BB_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						ENDIF
					ELSE
						IF IS_PED_HEADTRACKING_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(sBailJumperPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ Clear look at player	 - BB_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_AWARE_OF_PLAYER
					IF HAS_TIME_PASSED(sBailJumperPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(250, 500))
						IF IS_PED_USING_ANY_SCENARIO(sBailJumperPed.pedIndex)
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sBailJumperPed.pedIndex)
						ENDIF
						TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
						TASK_TURN_PED_TO_FACE_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
						sBailJumperPed.AI = BB_PED_AI_STATE_AWARE_OF_PLAYER
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_SETUP_AWARE_OF_PLAYER - set") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_AWARE_OF_PLAYER						
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure a task reapply will go through first attempt
						sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_STATE_AWARE_OF_PLAYER - reapply") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_MOVE_TOWARDS_PLAYER
					IF NOT IS_PED_HEADTRACKING_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ set look at player -	BB_PED_AI_SETUP_MOVE_TOWARDS_PLAYER") ENDIF #ENDIF
					ENDIF
					TASK_GO_TO_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 2.5, PEDMOVEBLENDRATIO_WALK)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)	
					sBailJumperPed.AI = BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ done - BB_PED_AI_SETUP_MOVE_TOWARDS_PLAYER") ENDIF #ENDIF
				BREAK
				CASE BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 3.5)
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_GO_TO_ENTITY)
							sBailJumperPed.AI = BB_PED_AI_SETUP_MOVE_TOWARDS_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ reapply - BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER") ENDIF #ENDIF
						ELSE
							SET_PED_MOVE_RATE_OVERRIDE(sBailJumperPed.pedIndex, 1.1)	//1.15)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ovrride speed this frame - BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER") ENDIF #ENDIF
						ENDIF
					ELSE
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							TASK_TURN_PED_TO_FACE_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ applied turn to face task - BB_PED_AI_STATE_MOVE_TOWARDS_PLAYER") ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_MELEE_ATTACK
					IF HAS_TIME_PASSED(sBailJumperPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(250, 1000))	// small delay in his reaction
						IF IS_PED_USING_ANY_SCENARIO(sBailJumperPed.pedIndex)
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sBailJumperPed.pedIndex)
							//TRIGGER_PED_SCENARIO_PANICEXITTOCOMBAT(sBailJumperPed.pedIndex, PLAYER_PED_ID())
						//ELSE
						//	CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
						ENDIF
						SET_PED_COMBAT_RANGE(sBailJumperPed.pedIndex, CR_NEAR)				
						SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, TRUE)	// attempt to stop security ped taking cover during brawl
						SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_USE_COVER, FALSE)	// attempt to stop security ped taking cover during brawl
						TASK_COMBAT_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())			
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)						
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_SETUP_MELEE_ATTACK") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_STATE_MELEE_ATTACK
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_MELEE_ATTACK
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_COMBAT)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_STATE_MELEE_ATTACK - kicking back to BB_PED_AI_SETUP_MELEE_ATTACK") ENDIF #ENDIF
                    	sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure the task reapply has no delay
						sBailJumperPed.AI = BB_PED_AI_SETUP_MELEE_ATTACK
                  	ENDIF 
				BREAK
				CASE BB_PED_AI_SETUP_BEATEN_UP
					SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_CAN_RAGDOLL(sBailJumperPed.pedIndex, TRUE)					
					IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
						SET_PED_TO_RAGDOLL(sBailJumperPed.pedIndex, 1000, 1000, TASK_RELAX)
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_STATE_BEATEN_UP") ENDIF #ENDIF
					sBailJumperPed.AI = BB_PED_AI_STATE_BEATEN_UP
				BREAK
				CASE BB_PED_AI_STATE_BEATEN_UP
					IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_COWER)
						AND NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP)		
							IF NOT IS_ENTITY_IN_WATER(sBailJumperPed.pedIndex)
								SET_PED_COWER_HASH(sBailJumperPed.pedIndex, "CODE_HUMAN_STAND_COWER")
								TASK_COWER(sBailJumperPed.pedIndex, -1)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_STATE_BEATEN_UP - set up cower") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_AIM_GUN_AND_ADVANCE
					GET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, wtTempCheckForUnarmed)
					IF wtTempCheckForUnarmed = WEAPONTYPE_UNARMED
						GIVE_WEAPON_TO_PED(sBailJumperPed.pedIndex, WEAPONTYPE_PISTOL, -1, FALSE)
					ENDIF
					SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_USE_COVER, FALSE)
					SET_PED_COMBAT_RANGE(sBailJumperPed.pedIndex, CR_NEAR)
					SET_PED_ACCURACY(sBailJumperPed.pedIndex, 15)					
					CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)					
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_SETUP_AIM_GUN_AND_ADVANCE - going to BB_PED_AI_STATE_AIM_GUN_AND_ADVANCE") ENDIF #ENDIF
					sBailJumperPed.AI = BB_PED_AI_STATE_AIM_GUN_AND_ADVANCE
				BREAK
				CASE BB_PED_AI_STATE_AIM_GUN_AND_ADVANCE
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						FLOAT fRandomSeekRadius
						fRandomSeekRadius = GET_RANDOM_FLOAT_IN_RANGE(5.0, 8.0)
	                	OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_GOTO_ENTITY_AIMING(NULL, PLAYER_PED_ID(), fRandomSeekRadius, 30.0)
							TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000, FALSE)
						CLOSE_SEQUENCE_TASK(sequenceAI)									
						//CLEAR_PED_TASKS_IMMEDIATELY(sBailJumperPed.pedIndex)	//we do this because CLEAR_PED_TASKS doesn't always switch the task straight away, e.g. when brawling							
						TASK_PERFORM_SEQUENCE(sBailJumperPed.pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_STATE_AIM_GUN_AND_ADVANCE - reapplied tasks") ENDIF #ENDIF
	              	ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_GUN_COMBAT
					IF HAS_TIME_PASSED(sBailJumperPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(250, 750))	// small delay in his reaction
						SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_USE_COVER, TRUE)
						SET_PED_COMBAT_RANGE(sBailJumperPed.pedIndex, CR_NEAR)
						SET_PED_ACCURACY(sBailJumperPed.pedIndex, GET_RANDOM_INT_IN_RANGE(10, 22))
						SET_PED_SHOOT_RATE(sBailJumperPed.pedIndex, GET_RANDOM_INT_IN_RANGE(30, 60))
						SET_PED_FIRING_PATTERN(sBailJumperPed.pedIndex, FIRING_PATTERN_DELAY_FIRE_BY_ONE_SEC)
						GET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, wtTempCheckForUnarmed)
						IF wtTempCheckForUnarmed = WEAPONTYPE_UNARMED
							GIVE_WEAPON_TO_PED(sBailJumperPed.pedIndex, WEAPONTYPE_PISTOL, -1, FALSE)
							SET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, WEAPONTYPE_PISTOL, FALSE)
						ENDIF				
						TASK_COMBAT_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)				
						sBailJumperPed.AI = BB_PED_AI_STATE_GUN_COMBAT
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_SETUP_GUN_COMBAT - going to BB_PED_AI_STATE_GUN_COMBAT") ENDIF #ENDIF
					ENDIF
				BREAK				
				CASE BB_PED_AI_STATE_GUN_COMBAT				
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_COMBAT)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~	BB_PED_AI_STATE_GUN_COMBAT - going back to BB_PED_AI_SETUP_GUN_COMBAT") ENDIF #ENDIF
						sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure the task reapply has no delay
						sBailJumperPed.AI = BB_PED_AI_SETUP_GUN_COMBAT
					ENDIF					
					/*IF (VDIST(GET_ENTITY_COORDS(sBailJumperPed.pedIndex), sBailBondLaunchData.vStartPoint) >= BAIL_JUMPER_PERIMETER)
					//	SET_PED_COMBAT_MOVEMENT(sBailJumperPed.pedIndex, CM_STATIONARY)
					//ELIF IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(PLAYER_PED_ID())	
						SET_PED_COMBAT_MOVEMENT(sBailJumperPed.pedIndex, CM_DEFENSIVE)
					ELSE
						SET_PED_COMBAT_MOVEMENT(sBailJumperPed.pedIndex, CM_WILLADVANCE)
					ENDIF*/
				BREAK
				CASE BB_PED_AI_SETUP_FLEE_ON_FOOT	
					IF HAS_TIME_PASSED(sBailJumperPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(0, 500))
						IF IS_PED_USING_ANY_SCENARIO(sBailJumperPed.pedIndex)
							SET_PED_PANIC_EXIT_SCENARIO(sBailJumperPed.pedIndex, vPlayerPos)
						ENDIF
						GET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, wtTempCheckForUnarmed)
						IF wtTempCheckForUnarmed != WEAPONTYPE_UNARMED
							SET_PED_DROPS_WEAPON(sBailJumperPed.pedIndex)
							REMOVE_ALL_PED_WEAPONS(sBailJumperPed.pedIndex)
							SET_CURRENT_PED_WEAPON(sBailJumperPed.pedIndex, WEAPONTYPE_UNARMED, TRUE)		
							SET_PED_CAN_SWITCH_WEAPON(sBailJumperPed.pedIndex, FALSE)
						ENDIF
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_VEHICLE, FALSE)			
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_DISABLE_HANDS_UP, FALSE)						
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_PREFER_PAVEMENTS, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_COVER, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FLEE, TRUE)
						OPEN_SEQUENCE_TASK(sequenceAI)
							IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE - ped told to exit vehicle") ENDIF #ENDIF
							ENDIF
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200.0, -1)	//, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE - ped setup to flee on foot") ENDIF #ENDIF
						CLOSE_SEQUENCE_TASK(sequenceAI)
						TASK_PERFORM_SEQUENCE(sBailJumperPed.pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)		
						SET_PED_KEEP_TASK(sBailJumperPed.pedIndex, TRUE)
						sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_ON_FOOT
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_SETUP_FLEE_ON_FOOT - set") ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_SETUP_FLEE_ON_FOOT - unable to set this frame due to ai delay: iAiDelayTimer = ", sBailJumperPed.iAiDelayTimer, " game timer = ", GET_GAME_TIMER()) ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_FLEE_ON_FOOT	
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_SMART_FLEE_PED)
					//AND NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_COWER)
					//AND NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP)
					AND NOT IS_PED_FLEEING(sBailJumperPed.pedIndex)
						sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure a task reapply will go through first attempt
						sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI - BB_PED_AI_STATE_FLEE_ON_FOOT - reapply") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_FLEE_TO_VEHICLE					
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_VEHICLE, TRUE)		
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_DISABLE_HANDS_UP, FALSE)						
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_PREFER_PAVEMENTS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FLEE, TRUE)
					SET_PED_RESET_FLAG(sBailJumperPed.pedIndex, PRF_UseFastEnterExitVehicleRates, TRUE)
					
					// check ped should still try to reach his car
					IF IS_VEHICLE_OK(sMissionVehicle[0].index)																		//if Musician vehicle is ok to use
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMissionVehicle[0].index)
						AND IS_ENTITY_IN_RANGE_COORDS(sMissionVehicle[0].index, sMissionVehicle[0].vPos, 3.0)						//if Musician vehicle pos hasn't changed much
						AND IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(GET_ENTITY_HEADING(sMissionVehicle[0].index), sMissionVehicle[0].fDir, 20.0)	//if Musician vehicle heading hasn't changed much	//if Player isn't already in the vehicle
						AND NOT ARE_VEHICLE_TYRES_BURST(sMissionVehicle[0].index, 2)
							IF IS_PED_IN_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index)										//if Musician isn't already in the vehicle
							AND NOT IS_PED_BEING_JACKED(sBailJumperPed.pedIndex)	//if Musician isn't being jacked.
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_TO_VEHICLE going to BB_PED_AI_STATE_FLEE_TO_VEHICLE without task as in the vehicle") ENDIF #ENDIF
								sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_TO_VEHICLE
							ELSE
								IF HAS_TIME_PASSED(sBailJumperPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(250, 350))	 //slight delay
									SAFE_REMOVE_OBJECT(sObjectBailJumperPhone.index, FALSE)	// just drop the phone if it's still in hand
									IF IS_ENTITY_PLAYING_ANIM(sBailJumperPed.pedIndex, "ODDJOBS@BAILBOND_QUARRY", "prem_producer_argue_a")
										STOP_ENTITY_ANIM(sBailJumperPed.pedIndex, "prem_producer_argue_a", "ODDJOBS@BAILBOND_QUARRY", SLOW_BLEND_OUT)
									ELIF IS_PED_USING_ANY_SCENARIO(sBailJumperPed.pedIndex)
										SET_PED_PANIC_EXIT_SCENARIO(sBailJumperPed.pedIndex, vPlayerPos)
									ELSE
										CLEAR_PED_TASKS(sBailJumperPed.pedIndex)	
									ENDIF
									TASK_ENTER_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index, 25000, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT, ECF_DONT_JACK_ANYONE)	// increased DEFAULT_TIME_BEFORE_WARP since he has a fair way to run
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)						
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_TO_VEHICLE going to BB_PED_AI_STATE_FLEE_TO_VEHICLE") ENDIF #ENDIF
									sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_TO_VEHICLE
								ENDIF
							ENDIF
						ELSE
							sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER() - 5000
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_TO_VEHICLE going to BB_PED_AI_SETUP_FLEE_ON_FOOT") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT	
						ENDIF
					ELSE
						sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER() - 5000
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_TO_VEHICLE going to BB_PED_AI_SETUP_FLEE_ON_FOOT") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT	
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_FLEE_TO_VEHICLE
					SET_PED_RESET_FLAG(sBailJumperPed.pedIndex, PRF_UseFastEnterExitVehicleRates, TRUE)
					
					IF NOT IS_VEHICLE_OK(sMissionVehicle[0].index)
						sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER() - 5000
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FLEE_TO_VEHICLE - going to BB_PED_AI_SETUP_FLEE_ON_FOOT - car not ok") ENDIF #ENDIF
                    	sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
					ELIF (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMissionVehicle[0].index, TRUE)
					OR IS_PED_BEING_JACKED(sBailJumperPed.pedIndex)	
					OR NOT IS_ENTITY_IN_RANGE_COORDS(sMissionVehicle[0].index, sMissionVehicle[0].vPos, 3.0)
					OR NOT IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(GET_ENTITY_HEADING(sMissionVehicle[0].index), sMissionVehicle[0].fDir, 20.0))
					OR ARE_VEHICLE_TYRES_BURST(sMissionVehicle[0].index, 2)
						sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER() - 5000
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FLEE_TO_VEHICLE - going to BB_PED_AI_SETUP_FLEE_ON_FOOT - reason to abort") ENDIF #ENDIF
                    	sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
					ELIF IS_PED_SITTING_IN_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FLEE_TO_VEHICLE - going to BB_PED_AI_SETUP_TEMP_OVERRIDE_AI_CONTROL ") ENDIF #ENDIF
                    	sBailJumperPed.AI = BB_PED_AI_SETUP_TEMP_OVERRIDE_AI_CONTROL
					ELSE
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FLEE_TO_VEHICLE - going to BB_PED_AI_SETUP_FLEE_TO_VEHICLE") ENDIF #ENDIF
		                    sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_TO_VEHICLE
						ENDIF
					ENDIF					
				BREAK
				CASE BB_PED_AI_SETUP_FLEE_IN_VEHICLE	
					IF IS_VEHICLE_OK(sMissionVehicle[0].index)																		//if Musician vehicle is ok to use											//if Player isn't already in the vehicle
					AND IS_PED_SITTING_IN_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index)								//if Musician isn't already in the vehicle		
						TASK_VEHICLE_MISSION_PED_TARGET(sBailJumperPed.pedIndex, sMissionVehicle[0].index, PLAYER_PED_ID(), MISSION_FLEE, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 350.0, 30)//-1.0)
						SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_AllowPlayerToInterruptVehicleEntryExit, FALSE)	//reset this from getting in state
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)						
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_IN_VEHICLE going to BB_PED_AI_STATE_FLEE_IN_VEHICLE") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_IN_VEHICLE
					ELSE
						IF NOT IS_PED_GETTING_INTO_A_VEHICLE(sBailJumperPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FLEE_IN_VEHICLE going to BB_PED_AI_SETUP_FLEE_ON_FOOT") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_FLEE_IN_VEHICLE
					IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_VEHICLE_MISSION)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FLEE_IN_VEHICLE - going to BB_PED_AI_SETUP_FLEE_IN_VEHICLE") ENDIF #ENDIF
	                    sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_IN_VEHICLE
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_FRIGHTENED	
					CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
					SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COWER_HASH(sBailJumperPed.pedIndex, "CODE_HUMAN_STAND_COWER")
					TASK_COWER(sBailJumperPed.pedIndex, -1)	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FRIGHTENED done") ENDIF #ENDIF
					sBailJumperPed.AI = BB_PED_AI_STATE_FRIGHTENED
				BREAK
				CASE BB_PED_AI_STATE_FRIGHTENED
					IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_COWER)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_FRIGHTENED done") ENDIF #ENDIF
	                    	sBailJumperPed.AI = BB_PED_AI_SETUP_FRIGHTENED
	                  	ENDIF
					ENDIF
				BREAK	
				CASE BB_PED_AI_SETUP_FOLLOW_PLAYER
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
					OR SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FOLLOW_PLAYER -> BB_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
					ELSE
						IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())	
							// B*1534772 - can only be applied is task is active (so exclude normal WAITING_TO_START_TASK check)
							IF (GET_SCRIPT_TASK_STATUS(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP) = PERFORMING_TASK)
								UPDATE_TASK_HANDS_UP_DURATION(sBailJumperPed.pedIndex, 500)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", " BB_PED_AI_SETUP_FOLLOW_PLAYER updated hands up durationto 500 ###") ENDIF #ENDIF
							ELSE
								CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
								CLEAR_PED_SECONDARY_TASK(sBailJumperPed.pedIndex)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", " BB_PED_AI_SETUP_FOLLOW_PLAYER ped tasks cleared") ENDIF #ENDIF
							ENDIF
							IF NOT IS_PED_GROUP_MEMBER(sBailJumperPed.pedIndex, PLAYER_GROUP_ID())
								SET_PED_AS_GROUP_MEMBER(sBailJumperPed.pedIndex, PLAYER_GROUP_ID())
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", " BB_PED_AI_SETUP_FOLLOW_PLAYER already in group so removed to put back in") ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_FOLLOW_PLAYER already in group ****") ENDIF #ENDIF
							ENDIF
							SET_GROUP_FORMATION(PLAYER_GROUP_ID(), FORMATION_LOOSE)
							SET_GROUP_FORMATION_SPACING(PLAYER_GROUP_ID(), 1.5, 3.5)
							SET_PED_GROUP_MEMBER_PASSENGER_INDEX(sBailJumperPed.pedIndex, VS_FRONT_RIGHT)
							SET_PED_PATH_CAN_USE_CLIMBOVERS(sBailJumperPed.pedIndex, TRUE)
							SET_PED_PATH_CAN_USE_LADDERS(sBailJumperPed.pedIndex, TRUE)
							SET_PED_PATH_MAY_ENTER_WATER(sBailJumperPed.pedIndex, TRUE)
							SET_PED_PATH_PREFER_TO_AVOID_WATER(sBailJumperPed.pedIndex, TRUE)
							SET_PED_PATH_CAN_DROP_FROM_HEIGHT(sBailJumperPed.pedIndex, FALSE)
							SET_PED_PATH_AVOID_FIRE(sBailJumperPed.pedIndex, TRUE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_DontEnterVehiclesInPlayersGroup, TRUE)	//used to control which vehicles the ped will get into
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_RunFromFiresAndExplosions, TRUE)
							//SET_PED_NEVER_LEAVES_GROUP(ped, TRUE)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBailJumperPed.pedIndex, TRUE)	// whilst in the ped group
							
							// group specific stuff - 
							SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, TRUE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_DontInfluenceWantedLevel, TRUE)	
							SET_PED_DIES_IN_WATER(sBailJumperPed.pedIndex, TRUE)
							SET_PED_DIES_IN_SINKING_VEHICLE(sBailJumperPed.pedIndex, TRUE)
							SET_PED_MAX_TIME_IN_WATER(sBailJumperPed.pedIndex, 2.0)
							SET_PED_MAX_TIME_UNDERWATER(sBailJumperPed.pedIndex, 2.0)
							SET_PED_PATH_PREFER_TO_AVOID_WATER(sBailJumperPed.pedIndex, TRUE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_RunFromFiresAndExplosions, TRUE)
							// cover stuff added for B*1266421
							SET_PED_FLEE_ATTRIBUTES(sBailJumperPed.pedIndex, FA_USE_COVER, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_ALWAYS_FLEE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sBailJumperPed.pedIndex, CA_USE_COVER, FALSE)
							
							SET_PED_RESET_FLAG(sBailJumperPed.pedIndex, PRF_UseFastEnterExitVehicleRates, TRUE)	// to improve slowness getting in vehicles
		
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_SETUP_FOLLOW_PLAYER done") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_STATE_FOLLOW_PLAYER
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_SETUP_FOLLOW_PLAYER failed player group doesn't exist!") ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_FOLLOW_PLAYER
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
					OR SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER -> BB_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
					ELSE
						// player is in a vehicle
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER()
								vehTempPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF NOT IS_PED_IN_VEHICLE(sBailJumperPed.pedIndex, vehTempPlayer)
									FLOAT fVehSpeed
									fVehSpeed = GET_ENTITY_SPEED(vehTempPlayer)
									IF fVehSpeed < 1.75
									AND IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 8.0)	// can use player pos since it uses veh pos if in one
										IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
											vehSeat = VS_DRIVER
											IF ARE_ANY_VEHICLE_SEATS_FREE(vehTempPlayer)
												IF IS_VEHICLE_SEAT_FREE(vehTempPlayer, VS_FRONT_RIGHT)
												AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(sBailJumperPed.pedIndex, vehTempPlayer, VS_FRONT_RIGHT, TRUE)
													vehSeat = VS_FRONT_RIGHT
												ELSE
													vehSeat = VS_ANY_PASSENGER
												ENDIF
											ENDIF
											IF vehSeat != VS_DRIVER
												TASK_ENTER_VEHICLE(sBailJumperPed.pedIndex, vehTempPlayer, 25000, vehSeat, PEDMOVEBLENDRATIO_RUN, ECF_RESUME_IF_INTERRUPTED | ECF_BLOCK_SEAT_SHUFFLING)
												#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : tasked to enter player's veh, seat : ", vehSeat) ENDIF #ENDIF
											ELSE
												#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : unable to find passenger seat!") ENDIF #ENDIF
											ENDIF
										ELSE
											IF IS_PED_GETTING_INTO_A_VEHICLE(sBailJumperPed.pedIndex)
												IF (GET_VEHICLE_PED_IS_ENTERING(sBailJumperPed.pedIndex) != vehTempPlayer)
													CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
													#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : cleared enter veh task - wrong veh") ENDIF #ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)					
											IF NOT IS_PED_GETTING_INTO_A_VEHICLE(sBailJumperPed.pedIndex)
												CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
												#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : cleared enter vehicle task : veh speed was ", fVehSpeed) ENDIF #ENDIF	
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
									CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : cleared enter veh task - unsuitable veh") ENDIF #ENDIF		
								ENDIF
							ENDIF
						ELSE
							IF IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
								CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_STATE_FOLLOW_PLAYER : cleared enter veh task - player out of veh") ENDIF #ENDIF		
							ENDIF
						ENDIF
						
						// B*1424207 - don't look at player when in a vehicle - interfers with Vehicle conversational head lookat IK						
						IF IS_PED_HEADTRACKING_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID())
							IF IS_PED_GETTING_INTO_A_VEHICLE(sBailJumperPed.pedIndex)	// if you do this with in vehicle it will continuously clear the IK lookat
								TASK_CLEAR_LOOK_AT(sBailJumperPed.pedIndex)								
								//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, " * UPDATE_BAIL_JUMPER_PED_AI --- cleared look at player - BB_PED_AI_STATE_FOLLOW_PLAYER") ENDIF #ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex, TRUE)
								IF NOT IS_PED_GETTING_INTO_A_VEHICLE(sBailJumperPed.pedIndex)
									TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
									//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, " * UPDATE_BAIL_JUMPER_PED_AI --- set look at player	 - BB_PED_AI_STATE_FOLLOW_PLAYER") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						SET_PED_RESET_FLAG(sBailJumperPed.pedIndex, PRF_UseFastEnterExitVehicleRates, TRUE)	// to improve slowness getting in vehicles
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_SURRENDERED
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
					OR SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()						
						IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
							REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)	
							//CLEAR_PED_TASKS(sBailJumperPed.pedIndex)	
							// group specific stuff - 
							SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, FALSE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_DontInfluenceWantedLevel, FALSE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_RunFromFiresAndExplosions, TRUE)
						ENDIF
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBailJumperPed.pedIndex, FALSE)	// allow any type of damage if the player is threatening
						
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
								CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI : ", "BB_PED_AI_SETUP_SURRENDERED : cleared enter veh task - player not in veh") ENDIF #ENDIF		
							ENDIF
						ENDIF
						
						IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
						//AND NOT IS_PED_GETTING_UP(sBailJumperPed.pedIndex)	// B*1550554 - allow task hands up whilst getting up
						AND NOT IS_PED_BEING_STUNNED(sBailJumperPed.pedIndex)
						AND NOT IS_PED_BEING_JACKED(sBailJumperPed.pedIndex)
						AND NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
						
							TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
							
							// test if the bail jumper can see the player
							IF (NOT HAS_FRAME_COUNTER_PASSED(sBailJumperPed.iFrameCountLastSeenPlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	
							AND IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 50.0))
							OR NOT HAS_TIME_PASSED(iTimerLowerHandsDelayForGetUp, 2000)	// B*1550554 - force the hands up ifhe's getting up from a fall
								IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)							
									TASK_LEAVE_ANY_VEHICLE(sBailJumperPed.pedIndex)
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED - bail out vehicle") ENDIF #ENDIF
								ELSE
									// don't put hands up if player is unarmed / or bail jumper is in water
									IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE | WF_INCLUDE_GUN)
									AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sBailJumperPed.pedIndex)	// melee targeting
									AND NOT SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()	// ensure he keeps his hands up until Trevor says you are coming with me.
									OR IS_ENTITY_IN_WATER(sBailJumperPed.pedIndex)
										TASK_TURN_PED_TO_FACE_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED - just turn to face player") ENDIF #ENDIF
									ELSE
										// if he is already playing the hands up just reset the duration to -1 infinite
										// B*1534772 - can only be applied is task is active (so exclude normal WAITING_TO_START_TASK check)
										IF (GET_SCRIPT_TASK_STATUS(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP) = PERFORMING_TASK)
											UPDATE_TASK_HANDS_UP_DURATION(sBailJumperPed.pedIndex, -1)
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", " BB_PED_AI_SETUP_SURRENDERED updated hands up duration to -1 ###") ENDIF #ENDIF
										ELSE
											TASK_HANDS_UP(sBailJumperPed.pedIndex, -1, PLAYER_PED_ID(), -1)	
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED - task hands up") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								// bail jumper can't see the player so just turn to face player direction if on foot
								IF NOT IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)	
									TASK_TURN_PED_TO_FACE_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED - just turn to face player") ENDIF #ENDIF
								ENDIF
							ENDIF
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)
							sBailJumperPed.AI = BB_PED_AI_STATE_SURRENDERED
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED ->  BB_PED_AI_STATE_SURRENDERED: framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ELSE
						// switch back to different state if he's currently classed as left behind
						IF NOT bHasPlayerLeftBailJumperBehind
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED -> BB_PED_AI_SETUP_FOLLOW_PLAYER") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_SURRENDERED -> BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_SURRENDERED
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
					OR SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()		
						IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP)
							IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
									// test if the bail jumper can see the player
									IF (NOT HAS_FRAME_COUNTER_PASSED(sBailJumperPed.iFrameCountLastSeenPlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	
									AND IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 50.0))
										TASK_LEAVE_ANY_VEHICLE(sBailJumperPed.pedIndex)
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_SURRENDERED - bail out vehicle") ENDIF #ENDIF
										sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_SURRENDERED - done : framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			                		sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
								ENDIF
							ELSE
								IF (NOT HAS_FRAME_COUNTER_PASSED(sBailJumperPed.iFrameCountLastSeenPlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	
								AND IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 50.0))
									IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE | WF_INCLUDE_GUN)
									OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sBailJumperPed.pedIndex)	// melee targeting
									OR SHOULD_BAIL_JUMPER_DELAY_FOLLOW_BEHAVIOUR()	// ensure he keeps his hands up until Trevor says you are coming with me.
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_SURRENDERED - player now armed so head back to setup : framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				                		sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						// switch back to different state if he's currently classed as left behind
						IF NOT bHasPlayerLeftBailJumperBehind
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_SURRENDERED -> BB_PED_AI_SETUP_FOLLOW_PLAYER") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_SURRENDERED -> BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND") ENDIF #ENDIF
							sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
						ENDIF
					ENDIF
				BREAK	
				CASE BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND -> BB_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
					ELSE
						IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
							REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
							// group specific stuff - 
							SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, FALSE)
							SET_PED_CONFIG_FLAG(sBailJumperPed.pedIndex, PCF_DontInfluenceWantedLevel, FALSE)
						ENDIF
						CLEAR_PED_TASKS(sBailJumperPed.pedIndex)	// needed to stop enter veh task from follow behavioour
						TASK_LOOK_AT_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)						
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND done") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_STATE_PLAYER_LEFT_BEHIND
					ENDIF
				BREAK
				CASE BB_PED_AI_STATE_PLAYER_LEFT_BEHIND
					IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, 100, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_STATE_PLAYER_LEFT_BEHIND -> BB_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
						sBailJumperPed.AI = BB_PED_AI_SETUP_SURRENDERED
					ELSE
						IF IS_ENTITY_IN_WATER(sBailJumperPed.pedIndex)
							SET_PED_PATH_PREFER_TO_AVOID_WATER(sBailJumperPed.pedIndex, TRUE)
							SET_PED_PATH_MAY_ENTER_WATER(sBailJumperPed.pedIndex, FALSE)
						ELIF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
						AND NOT IS_PED_GETTING_UP(sBailJumperPed.pedIndex)
						AND IS_PED_ON_FOOT(sBailJumperPed.pedIndex)
							IF NOT IS_PED_FACING_PED(sBailJumperPed.pedIndex, PLAYER_PED_ID(), 45.0)
								IF NOT IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									TASK_TURN_PED_TO_FACE_ENTITY(sBailJumperPed.pedIndex, PLAYER_PED_ID(), -1)
								ENDIF
							ENDIF										
						ENDIF
					ENDIF
				BREAK
				CASE BB_PED_AI_SETUP_WAITING_IN_VEHICLE
				BREAK
				CASE BB_PED_AI_STATE_WAITING_IN_VEHICLE
				BREAK
				CASE BB_PED_AI_SETUP_TEMP_OVERRIDE_AI_CONTROL
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, TRUE)						
					iTimer_BailJumperVehicleStopped = GET_GAME_TIMER()	// set used to check for bail jumper vehicle stopping in flee in veh state
					SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, TRUE)	// help with loading navmesh on change route
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "BB_PED_AI_SETUP_TEMP_OVERRIDE_AI_CONTROL done") ENDIF #ENDIF
					sBailJumperPed.AI = BB_PED_AI_STATE_TEMP_OVERRIDE_AI_CONTROL
				BREAK
				CASE BB_PED_AI_STATE_TEMP_OVERRIDE_AI_CONTROL
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes all the assets loaded by the mission, used in mission cleanup and reset mission functions.
/// PARAMS:
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC UNLOAD_ALL_MISSION_ASSETS(BOOL bClearTextSlots = TRUE)	
	IF bClearTextSlots
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
	REMOVE_ANIM_DICT(GET_MAUDE_REACT_ANIM_DICT())
	REMOVE_ANIM_DICT("ODDJOBS@BAILBOND_QUARRY")
	RELEASE_BAIL_JUMPER_SURRENDER_MOVEMENT_CLIPSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjectBailJumperPhone.model)
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC DELETE_ALL_MISSION_BLIPS()
	CLEANUP_BUDDY_COMBAT_BLIPS(iNumTargetBackupPeds)
	SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)
	CLEANUP_AI_PED_BLIP(sBailJumperPed.blipAIStruct)
	SAFE_REMOVE_BLIP(blipIndexMaudesPlace)
ENDPROC

/// PURPOSE:
///    handles releasing the objects at the bail jumper's location to be used during the drop off stage
/// PARAMS:
///    vPlayersCoords - player's position
///    vBailJumperLocation - the bail jumper scenario's position
///    fRangeBeforeCleanup - dist to reach before stuff gets released
/// RETURNS:
///    TRUE if cleanup occurs
FUNC BOOL CLEANUP_BAIL_JUMPER_LOCATION_DURING_DROPOFF(VECTOR vPlayersCoords, VECTOR vBailJumperLocation, FLOAT fRangeBeforeCleanup)
	IF NOT IS_COORD_IN_RANGE_OF_COORD_2D(vPlayersCoords, vBailJumperLocation, fRangeBeforeCleanup)
		INT i
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)	
	
		IF iNumTargetBackupPeds > 0
			FOR i=0 TO (iNumTargetBackupPeds - 1) //Start at 1 as we dont want to delete the target
				SAFE_RELEASE_PED(sBackupPed[i].index)
			ENDFOR
		ENDIF				
		IF iNumMissionVehicles > 0
			FOR i=0 TO (iNumMissionVehicles - 1)
				SAFE_RELEASE_VEHICLE(sMissionVehicle[i].index)
			ENDFOR
		ENDIF				
		/*IF iNumMissionObjects > 0
			FOR i=0 TO (iNumMissionObjects - 1)
				SAFE_RELEASE_OBJECT(sMissionObject[i].index)
			ENDFOR
		ENDIF*/
		SAFE_RELEASE_OBJECT(sObjectBailJumperPhone.index)
		
		IF iNumMissionProps > 0
			FOR i=0 TO (iNumMissionProps - 1)
				SAFE_RELEASE_OBJECT(sMissionProp[i].index)
			ENDFOR
		ENDIF
		IF iNumMissionPickups > 0
			FOR i=0 TO (iNumMissionPickups - 1)
				SAFE_REMOVE_PICKUP(sMissionPickup[i].index)
			ENDFOR
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Deletes all of the mission entities checking if they exist
///    Used in Mission Failed, when faded out.
/// PARAMS:
///    bDelete - if true all entities will be deleted, else released
PROC CLEANUP_ALL_MISSION_ENTITIES(BOOL bDelete = FALSE)
	INT i
	/*IF iNumMissionObjects > 0
		FOR i=0 TO (iNumMissionObjects - 1)
			SAFE_REMOVE_OBJECT(sMissionObject[i].index, bDelete)
		ENDFOR
	ENDIF*/
	SAFE_REMOVE_OBJECT(sObjectBailJumperPhone.index)
	
	IF iNumMissionProps > 0
		FOR i=0 TO (iNumMissionProps - 1)
			SAFE_REMOVE_OBJECT(sMissionProp[i].index, bDelete)
		ENDFOR
	ENDIF	
	IF iNumMissionPickups > 0
		FOR i=0 TO (iNumMissionPickups - 1)
			SAFE_REMOVE_PICKUP(sMissionPickup[i].index)
		ENDFOR
	ENDIF	
	SAFE_REMOVE_OBJECT(sObjMaudeChair.index, bDelete)
	SAFE_REMOVE_OBJECT(sObjMaudeLaptop.index, bDelete)
	SAFE_REMOVE_OBJECT(sObjMaudeTable.index, FALSE)	// can't delete map objects
	SAFE_REMOVE_OBJECT(sObjMaudeRadio.index, FALSE)	// can't delete map objects
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex) 
		IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
	        IF IS_PED_GROUP_MEMBER(sBailJumperPed.pedIndex, GET_PLAYER_GROUP(PLAYER_ID())) 
				REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_BAIL_JUMPER_PED_AI ~~~ ", "CLEANUP_ALL_MISSION_ENTITIES() REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)") ENDIF #ENDIF
	        ENDIF
		ENDIF
		SET_ENTITY_LOAD_COLLISION_FLAG(sBailJumperPed.pedIndex, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBailJumperPed.pedIndex, FALSE)
		// B*1253394 - don't keep tasks if terminate reasion wasn't gameplay related
		IF eBailBondMissionOverState = BBMOS_CLEANUP
			SET_PED_KEEP_TASK(sBailJumperPed.pedIndex, FALSE)
		ELSE
			SET_PED_KEEP_TASK(sBailJumperPed.pedIndex, TRUE)
		ENDIF
    ENDIF
	SAFE_REMOVE_PED(sBailJumperPed.pedIndex, bDelete)
	IF IS_PED_UNINJURED(sMaude.index)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMaude.index, FALSE)
		SET_PED_KEEP_TASK(sMaude.index, TRUE)
	ENDIF
	SAFE_REMOVE_PED(sMaude.index, bDelete)	
	IF iNumTargetBackupPeds > 0
		FOR i = 0 TO (iNumTargetBackupPeds - 1)
			IF eBailBondType = BBT_SHOOTOUT
				IF IS_PED_UNINJURED(sBackupPed[i].index)
					SET_PED_RELATIONSHIP_GROUP_HASH(sBackupPed[i].index, RELGROUPHASH_HATES_PLAYER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBackupPed[i].index, FALSE)
					// B*1253394 - don't keep tasks if terminate reasion wasn't gameplay related
					IF eBailBondMissionOverState = BBMOS_CLEANUP
						SET_PED_KEEP_TASK(sBackupPed[i].index, FALSE)
					ELSE
						SET_PED_KEEP_TASK(sBackupPed[i].index, TRUE)
					ENDIF
				ENDIF				
			ENDIF
			SAFE_REMOVE_PED(sBackupPed[i].index, bDelete)
		ENDFOR
	ENDIF
	IF iNumMissionVehicles > 0
		FOR i=0 TO (iNumMissionVehicles - 1)
			SAFE_REMOVE_VEHICLE(sMissionVehicle[i].index, bDelete)
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE:
///    Mission cleanup
/// PARAMS:
///    bDeleteAll - if TRUE all entities are deleted, else released
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC MISSION_CLEANUP(BOOL bDeleteAll = FALSE, BOOL bClearTextSlots = TRUE)
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(TRUE)
	ENDIF
	IF eBailBondMissionOverState = BBMOS_CLEANUP
		KILL_ANY_CONVERSATION()	// only do this if the script terminate reasion wasn't gameplay related
	ENDIF
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)	
	SET_CREATE_RANDOM_COPS(TRUE)
	DISABLE_CELLPHONE(FALSE)
	HIDE_ACTIVE_PHONE(FALSE)	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	DISABLE_GANGS(FALSE)
	SET_AGGRESSIVE_HORNS(FALSE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
	// proc from initial scene so they match up
	IF eMissionStage > MISSION_STAGE_CHASE	// only revert this if it was setup in the SS_CLEANUP for MISSION_STAGE_CHASE
		SETUP_MAUDES_FOR_DROPOFF(FALSE, ScenarioBlockArea_CutsceneMaude)
	ENDIF
				
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingArea_BailJumperLocation)
	CLEAR_PED_NON_CREATION_AREA()	
	REMOVE_PED_FOR_DIALOGUE(sDialogue, TREVOR_SPEAKER_ID)					// "TREVOR"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, BAIL_JUMPER_SPEAKER_ID)				// bail jumper
	REMOVE_PED_FOR_DIALOGUE(sDialogue, MAUDE_SPEAKER_ID)					// "MAUDE"
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_TIME_SCALE(1)
	RC_END_CUTSCENE_MODE()
	STOP_SCRIPT_GLOBAL_SHAKING()
	WAIT_FOR_CUTSCENE_TO_STOP()	

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	//Reenable player flying through windscreens and taking crash damage.
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, TRUE)
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	DELETE_ALL_MISSION_BLIPS()
	CLEANUP_ALL_MISSION_ENTITIES(bDeleteAll)
	//REMOVE_RELATIONSHIP_GROUP(HASH_BAILBOND_TARGET)		// may not want to do this as target's buddies may stop fighting on mission passed
	UNLOAD_ALL_MISSION_ASSETS(bClearTextSlots)
	#IF IS_DEBUG_BUILD
		CLEANUP_MISSION_WIDGETS()
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF	
	CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : MISSION_CLEANUP : done - bDeleteAll = ", bDeleteAll, " bClearTextSlots = ", bClearTextSlots)
ENDPROC

/// PURPOSE:
///    Handles call to MISSION_CLEANUP and terminates the thread
///    Only performs MISSION_CLEANUP if bRequireMissionCleanup has been set true, this is to stop mission specific cleanup occuring before the mission has even setup
PROC Script_Cleanup()
	IF bRequireMissionCleanup
		MISSION_CLEANUP(FALSE)
	ENDIF
	CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : Script_Cleanup() : frame count : ", GET_FRAME_COUNT())
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Mission Passed
///    Pass function calls cleanup and termination 
PROC SCRIPT_PASSED(MISSION_PASSED_CONDITION eMissionPassType)
	CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SCRIPT_PASSED - condition : ", eMissionPassType)
	SAFE_FADE_SCREEN_IN_FROM_BLACK()
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY(sBailBondLaunchData.eBailBondID))
	
	// set the mission over state for the launcher to use
	IF eMissionPassType = BB_PASSED_TARGET_KILLED
		eBailBondMissionOverState = BBMOS_PASSED_KILLED
		CPRINTLN(DEBUG_BAIL_BOND, "SCRIPT_PASSED - set eBailBondMissionOverState = BBMOS_PASSED_KILLED")
	ELSE
		eBailBondMissionOverState = BBMOS_PASSED_DELIVERED
		CPRINTLN(DEBUG_BAIL_BOND, "SCRIPT_PASSED - set eBailBondMissionOverState = BBMOS_PASSED_DELIVERED")
	ENDIF
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    reset's the STRUCT_MISSION_PED's iTimer and iFrameCountLastSeenPlayer
PROC RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
	RESET_THREAT_SHAPETESTS()
	sBailJumperPed.iTimer = 0
	sBailJumperPed.iAiDelayTimer = 0
	sBailJumperPed.iFrameCountLastSeenPlayer = 0
ENDPROC

/// PURPOSE:
///    sets the initial values for all the variables used on the mission
PROC SET_MISSION_VARIABLES()
	INT i = 0

	//bail bond scenario
	eBailBondType = BBT_VEHICLE_FLEE
	
	// ped info
	sBailJumperPed.mnModel = S_M_M_Trucker_01
	sBailJumperPed.vPosition = <<2945.0356, 2796.2544, 39.6930>> 	// <<2956.3489, 2795.9553, 39.9217>>
	sBailJumperPed.fHeading = 38.6771								// 65.6672 
	sBailJumperPed.wtWeapon = WEAPONTYPE_UNARMED	
	sBailJumperPed.AI = BB_PED_AI_STATE_RELAXED
	sBailJumperPed.iTimer = 0
	sBailJumperPed.iAiDelayTimer = 0
	sBailJumperPed.iFrameCountLastSeenPlayer = 0
	
	RESET_THREAT_SHAPETESTS()
	
	iNumTargetBackupPeds = 0

	// vehicle info
	sMissionVehicle[0].model = landstalker
	sMissionVehicle[0].vPos = << 2950.07, 2796.39, 40.35 >>	// <<2945.2556, 2801.2214, 40.1939>>
	sMissionVehicle[0].fDir = 338.45	// 314.9999
	sMissionVehicle[1].model = BjXl
	sMissionVehicle[1].vPos = << 2945.97, 2790.77, 40.37 >>	// <<2930.1162, 2800.5247, 40.2321>>
	sMissionVehicle[1].fDir = 28.22	// 284.9999

	sObjectBailJumperPhone.model = PROP_PHONE_ING
	
	iNumMissionVehicles = 2

	// object info
	iNumMissionObjects = 0

	// prop info
	iNumMissionProps = 0
	
	//pickup info
	iNumMissionPickups = 0

	// Chase Route info
	vChaseRoute[0] = <<2962.58, 2817.19, 42.29>>
	vChaseRoute[1] = <<2942.4817, 2841.7075, 46.4443>>
	vChaseRoute[2] = <<2909.12, 2820.88, 52.90>>
	vChaseRoute[3] = <<2860.14, 2816.90, 52.56>>			
	vChaseRoute[4] = <<2821.6384, 2893.2593, 45.5300>>
	vChaseRoute[5] = <<2768.72, 2950.61, 39.78>>
	vChaseRoute[6] = <<2712.31, 2963.74, 35.79>>
	vChaseRoute[7] = <<2657.5732, 2981.7312, 38.9122>>		
	vChaseRoute[8] = <<2623.29492, 3077.35767, 46.24589>>

	iNumChaseRoutePositions = 9
	
	vPos_PlayerMissionStart 			= <<2883.0232, 2807.7195, 53.6941>>
	fHeading_PlayerMissionStart 		= 256.3262
	vPos_PlayerWarpGetNearTarget 		= <<2938.9597, 2783.2747, 38.5873>>
	fHeading_PlayerWarpGetNearTarget 	= 355.8176
	vPos_PlayerWarpApproachTarget 		= <<2947.2463, 2787.4058, 39.4516>>
	fHeading_PlayerWarpApproachTarget 	= 306.6250
	vPos_JumperBeganFleeAgain			= << 0.0, 0.0, 0.0 >>
	vPos_ProjectileFleeingFrom			= << 0.0, 0.0, 0.0 >>
	
	ePlayerSpotsBailJumperType 					= BB_PSBJT_INVALID

	bDoneDialogue_BailJumperBeginsToFlee		= FALSE
	bDoneDialogue_BailJumperSurrendered			= FALSE
	bDoneDialogue_TrevorResponsesToBailJumperSurrendering = FALSE
	bDoneDialogue_BailJumperRespondToPlayerOrders = TRUE	// initialised to TRUE and only set to false when we wat him to say a reply
	bDoneDialogue_BailJumperNoticesCops			= TRUE		// initialised to TRUE and only set to false when we wat him to say a reply
	FOR i = 0 TO (BAIL_BONDS_MAX_DRIVE_CONVOS - 1)
		bDoneDialogue_DriveToMaudes[i] 			= FALSE
	ENDFOR
	bDoneDialogue_PlayerSpotsBailJumperCloseDistance	= FALSE
	bDoneDialogue_PlayerSpotsBailJumperFromDistance		= FALSE
	bDoneDialogue_PlayerGetsInVehicle					= FALSE
	bDoneDialogue_PlayerGetsOutVehicle					= FALSE
	bDoneDialogue_BailJumperSpotsPlayerFirst			= TRUE	// only setup to false if needed
	bDoneDialogue_PlayerJackingPed						= FALSE
	bDoneDialogue_IdleInVehicle							= FALSE
	bDoneObjective_TakeBailJumperToMaude		= FALSE
	bDoneObjective_ApproachBailJumper			= FALSE
	bDoneObjective_ApprehendBailJumper			= FALSE
	bDoneObjective_LoseWantedLevel				= FALSE
	bDoneObjective_ReturnToBailJumper			= FALSE
	bDoneHelp_UnsuitableVehicle					= FALSE
	bSetMaudeFleeSyncSceneExit					= FALSE
	bCleanupBailJumperLocationDuringDropOff		= FALSE
	bConvoCoolDownActive 						= FALSE
	bHasBailJumperSurrendered					= FALSE
	bHasPlayerLeftBailJumperBehind				= FALSE
	bRequireMissionCleanup 						= TRUE	// used in the Script_Cleanup to determine if the mission has done it's setup and therefore needs a full cleanup
	bSetupMaude									= FALSE
	bRequestedOutroMocap						= FALSE
	bIsJumperRevertingBackToFlee				= FALSE
	bHasJumperBeenTazered						= FALSE
	bDoneDialogue_MissionFailed					= TRUE	//initialise to TRUE and only set to false when we have dialogue which will need to play
	bDoneDialogue_BailJumperKilled				= FALSE
		
	eBB_MissionFailedReason 					= BB_FAILED_DEFAULT
	
	//iDeadBuddies 				= 0
	iConvoCoolDown 				= 0
	iFlashBlipTimer				= 0
	iFlashBlipGodTextTimer		= 0
	iDialogueTimer 				= 0
	iChaseIndex 				= 0
	iDialogueTimer_IdleInVehicle = 0
	iDialogueTimer_MaudeAmbient = 0
	iFrameCountPlayerLastSeenBailJumper				= 0				// set low so it won't trigger anything when we first start
	iDelay_SurrenderForPlayerCloseToWhileFleeing 	= -1			// keep at -1 used to test initialised state -  used to delay player proximity to fleeing ped reason to surrender check
	iTime_BailJumperStartedFleeingOnFoot			= -1			// keep at -1 used to test initialised state -  used to modify the bail jumper flee speed over time
	iTimer_BailJumperVehicleStopped 				= 0
	iBailJumperHealthOnBeganFleeAgain 				= 0
	iSyncScene_MaudeReact 							= -1
	vMaudePlace 				= <<2722.6611, 4143.1055, 43.0617>>

	tlDialogueRoot_MissionFailed	= ""
	
	sMaude.model = GET_NPC_PED_MODEL(CHAR_MAUDE)
	sMaude.vPos = GET_MAUDE_SPAWN_POSITION()
	sMaude.fDir = GET_MAUDE_SPAWN_HEADING()
	
	sObjMaudeChair.model = GET_MAUDE_CHAIR_MODEL()
	sObjMaudeChair.vPos = GET_MAUDE_CHAIR_POSITION()
	sObjMaudeChair.fDir = GET_MAUDE_CHAIR_HEADING()
	
	sObjMaudeLaptop.model = GET_MAUDE_LAPTOP_MODEL()
	sObjMaudeLaptop.vPos = GET_MAUDE_LAPTOP_POSITION()
	sObjMaudeLaptop.fDir = GET_MAUDE_LAPTOP_HEADING()
	
	sObjMaudeTable.model = GET_MAUDE_TABLE_MODEL()
	sObjMaudeTable.vPos = GET_MAUDE_TABLE_POSITION()
	sObjMaudeTable.fDir = GET_MAUDE_TABLE_HEADING()	
	
	sObjMaudeRadio.model = GET_MAUDE_RADIO_MODEL()
	sObjMaudeRadio.vPos = GET_MAUDE_RADIO_POSITION()
	sObjMaudeRadio.fDir = GET_MAUDE_RADIO_HEADING()
	
	// Dialogue
	sBailJumpers_VoiceID = "BailBond1Jumper"
	sDialogue_BailJumperBeginChase  = "BB1_J1"
	// No I don't think so, pal!  
	// No, no, stay away from me!
	// Oh shit!
	
	sDialogueRoot_BailJumperSurrenders = "BB1_J3"	
	// Okay, Goddamnit! I give up.
	// I surrender, I surrender. Don't hurt me!
	// Okay, okay. You got me.  
	sDialogueRoot_DriveToMaudesPlace[0] = "BB1_VC1"
	sDialogueRoot_DriveToMaudesPlace[1] = "BB1_VC2"
	sDialogueRoot_DriveToMaudesPlace[2] = "BB1_VC3"
	
	FOR i=0 TO (BAIL_BONDS_MAX_DRIVE_CONVOS - 1)
		bDoneDialogue_DriveToMaudes[i] = FALSE
	ENDFOR
	
	eBailJumperClipset = BB_JUMPER_MOVEMENT_CLIPSET_UNSET
ENDPROC

///PURPOSE: 
///    Initiate the mission
///    set variables, load initial assets
///    spawn scene and setup the scenario
PROC INIT_MISSION()

	// set the variables initial values
	SET_MISSION_VARIABLES()
	
	#IF IS_DEBUG_BUILD
		SETUP_MISSION_WIDGET()
		// stage skipping	
		mSkipMenu[Z_SKIP_LOCATE_BAIL_JUMPER].sTxtLabel 				= "Find the bail jumper."
		mSkipMenu[Z_SKIP_TRIGGER_BAIL_JUMPER_REACTION].sTxtLabel 	= "Wait for bail jumper reaction"
		mSkipMenu[Z_SKIP_CHASE_BAIL_JUMPER].sTxtLabel				= "Chase the bail jumper"
		mSkipMenu[Z_SKIP_TAKE_JUMPER_TO_MAUDE].sTxtLabel			= "Take bail jumper to Maude"
		mSkipMenu[Z_SKIP_OUTRO_CUTSCENE].sTxtLabel					= "Mocap: MAUDE_MCS_2"
		mSkipMenu[Z_SKIP_MISSION_PASSED].sTxtLabel 					= "Mission Passed"
		
		//Used only for debug skipping should not be included when checking in mission 
		//	SAFE_TELEPORT_PED(PLAYER_PED_ID(), sBailBondLaunchData.vStartPoint, 218.4714)//<< -2228.9470, 2731.5837, 1.8044 >>, 206.2414)
	#ENDIF
	
	// load the intial assets required
	WHILE NOT SETUP_STAGE_REQUIREMENTS(RQ_TEXT)
	OR NOT SETUP_STAGE_REQUIREMENTS(RQ_ANIMS)
	OR NOT SETUP_STAGE_REQUIREMENTS(RQ_PHONE_MODEL)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "waiting for init specific mission assets and dialogue to load...") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	
	//Stop player fly through windscreens.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, FALSE)
	ENDIF
	
	//setup the relationship groups
	ADD_RELATIONSHIP_GROUP("ENEMIES", HASH_BAILBOND_TARGET)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, HASH_BAILBOND_TARGET)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_BAILBOND_TARGET, RELGROUPHASH_PLAYER)
	
	// diable everything which might interfer with the bail bond
	VECTOR vMin = GET_MISSION_CLEAR_AREA_MIN_VECTOR(sBailBondLaunchData)
	VECTOR vMax = GET_MISSION_CLEAR_AREA_MAX_VECTOR(sBailBondLaunchData)
	SET_PED_NON_CREATION_AREA(vMin, vMax)
	scenarioBlockingArea_BailJumperLocation = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
	CLEAR_AREA(sBailBondLaunchData.vStartPoint, 30.0, FALSE) // may need to change the radius value depending on the mission.
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)	
	SET_CREATE_RANDOM_COPS(FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.2)
	
	DISABLE_GANGS(TRUE)
	
	// spawn the scene
	WHILE NOT CREATE_BAIL_JUMPER_SCENE()
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "waiting for scene to load...") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	
	DO_SPECIFIC_SCENARIO_SETUP()
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sDialogue, TREVOR_SPEAKER_ID, PLAYER_PED_ID(), "TREVOR") //Its alway trevor
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
	ENDIF
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
		ADD_PED_FOR_DIALOGUE(sDialogue, BAIL_JUMPER_SPEAKER_ID, sBailJumperPed.pedIndex, sBailJumpers_VoiceID)//Different ped voice for target ped 
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sBailJumperPed.pedIndex, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    handle triggering the specific driving converstation
/// PARAMS:
///    i - 
PROC DO_DRIVE_CONV(INT i)
	IF NOT bDoneDialogue_DriveToMaudes[i]
		IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, sDialogueRoot_DriveToMaudesPlace[i], CONV_PRIORITY_AMBIENT_HIGH)
			bDoneDialogue_DriveToMaudes[i] = TRUE
			bConvoCoolDownActive = FALSE
			iConvoCoolDown = 0
			bDoneDialogue_BailJumperRespondToPlayerOrders = TRUE	// reset so Bail Jumper doesn't make response at inappropriate time
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    triggers all the relevant dialogue during the take bail jumper to Maude's state and lose wanted rating
PROC HANDLE_DIALOGUE_DURING_DROPOFF()

	// occasional response from bail jumper
	// player getting in an unsuitable vehicle
	VEHICLE_INDEX vehTempPlayer
	
	// some conversation shouldn't get delayed if an objective is currently being displayed
	// in this case they will play out without subtitles
	enumSubtitlesState eSubtitleState_ImportantDialogue = DISPLAY_SUBTITLES
	IF IS_MESSAGE_BEING_DISPLAYED()
	AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
		eSubtitleState_ImportantDialogue = DO_NOT_DISPLAY_SUBTITLES
	ENDIF
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
	
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueTimer = GET_GAME_TIMER()
		ENDIF
		
		// bail jumper will occasionally respond to players order
		IF NOT bDoneDialogue_BailJumperRespondToPlayerOrders
			IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J6", CONV_PRIORITY_AMBIENT_HIGH)
				// Random selected dialogue lines - 
				// I'm doing my best.  
				// Cut me some slack here, will you?
				// Jesus, give me a chance.
				bDoneDialogue_BailJumperRespondToPlayerOrders = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_BailJumperRespondToPlayerOrders - BB1_J6") ENDIF #ENDIF
			ENDIF		
		ENDIF
		
		// player is in a vehicle
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehTempPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			// check if bail jumper is in the vehicle too
			IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)			
			AND (GET_VEHICLE_PED_IS_IN(sBailJumperPed.pedIndex) = vehTempPlayer)
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT bConvoCoolDownActive
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iConvoCoolDown = GET_GAME_TIMER()
							bConvoCoolDownActive = TRUE
						ENDIF
					ELSE
						IF NOT bDoneDialogue_DriveToMaudes[0]
							IF (GET_GAME_TIMER() - iConvoCoolDown) > 7500
								DO_DRIVE_CONV(0)
							ENDIF

						ELIF NOT bDoneDialogue_DriveToMaudes[1]
							IF (GET_GAME_TIMER() - iConvoCoolDown) > 10000
								DO_DRIVE_CONV(1)
							ENDIF

						ELIF NOT bDoneDialogue_DriveToMaudes[2]

							IF (GET_GAME_TIMER() - iConvoCoolDown) > 10000
								DO_DRIVE_CONV(2)
							ENDIF
						ENDIF
					ENDIF
					// comments if the player is idling in a vehicle					
					IF DOES_ENTITY_EXIST(vehTempPlayer)						
						IF (GET_ENTITY_SPEED(vehTempPlayer) < 0.5)
							IF NOT bDoneDialogue_IdleInVehicle
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF HAS_TIME_PASSED(iDialogueTimer_IdleInVehicle, 3500)
										IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_STOP", CONV_PRIORITY_AMBIENT_HIGH)
											// Random selected dialogue lines - 
											// Why are we stopped? 
											// No rush. I can sit here all day.
											// You're really dragging this out aren't you?  
											bDoneDialogue_IdleInVehicle = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - triggered bDoneDialogue_IdleInVehicle", "BB1_STOP") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF bDoneDialogue_IdleInVehicle
								IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_STOP")
									iDialogueTimer_IdleInVehicle = GET_GAME_TIMER()
								ENDIF
							ELSE
								iDialogueTimer_IdleInVehicle = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bDoneDialogue_BailJumperNoticesCops
						IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J9", CONV_PRIORITY_AMBIENT_HIGH)
							// Random selected dialogue lines - 
							// Looks like we're both wanted men now.
							// Are those guys after you or me?
							bDoneDialogue_BailJumperNoticesCops = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_BailJumperNoticesCops - BB1_J9") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// help text for player using unsuitable vehicle to transport bail jumper e.g. bike
				IF NOT bDoneHelp_UnsuitableVehicle
					IF NOT IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER()
						PRINT_HELP("BBS_VH1")	// ~w~This vehicle isn't suitable to transport the bail jumper.~s~
						bDoneHelp_UnsuitableVehicle = TRUE
					ENDIF
				ENDIF
				iDialogueTimer_IdleInVehicle = GET_GAME_TIMER()
			ENDIF
			
			// reset the get in and out dialogue if the player is in a vehicle
			bDoneDialogue_PlayerGetsInVehicle	= FALSE
			bDoneDialogue_PlayerGetsOutVehicle	= FALSE
		ELSE
			iDialogueTimer_IdleInVehicle = GET_GAME_TIMER()
			bDoneDialogue_IdleInVehicle = FALSE	// reset dialogue
			
			// player getting into a vehicle
			IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				vehTempPlayer = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
				
				IF NOT bDoneDialogue_PlayerGetsInVehicle
					IF IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER()
						IF NOT IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
							// Trevor gets in a suitable vehicleBB1_JACK
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T6")
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T6", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Random selected dialogue lines - 
									// Get in. 
									// Sit your ass down in here.
									// Get the fuck in.
									// In, now.									
									bDoneDialogue_PlayerGetsInVehicle = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerGetsInVehicle - BBC_T6") ENDIF #ENDIF
								ENDIF
							ENDIF							
						ENDIF
						
						// clear the unsuitable vehicle help text
						CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("BBS_VH1", TRUE)	// ~w~This vehicle isn't suitable to transport the bail jumper.~s~
					ELSE
						// vehicle is unsuitable						
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T8")
						AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_WTF")
							IF (GET_RANDOM_INT_IN_RANGE(0, 11) < 5)
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T8", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Random selected dialogue lines - 
									// Stay there, I'm just taking a break.
									// Don't move.
									// I'm just testing this out.
									bDoneDialogue_PlayerGetsInVehicle = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerGetsInVehicle - BBC_T8") ENDIF #ENDIF
								ENDIF
							ELSE
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_WTF", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Good luck trying to take me in with that vehicle.  
									bDoneDialogue_PlayerGetsInVehicle = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerGetsInVehicle - BB1_WTF") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bDoneDialogue_PlayerJackingPed
						IF BB_IS_PLAYER_JACKING_DRIVER(vehTempPlayer)
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_JACK")
							AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T6")
							AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T8")
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_JACK", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Who's the criminal here again?  
									bDoneDialogue_PlayerJackingPed = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerJackingPed - BB1_JACK") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// reset the get in and out dialogue if the player is not in / getting in a vehicle
				bDoneDialogue_PlayerGetsInVehicle	= FALSE			
				
				// tell bail jumper to get out
				IF IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
					IF NOT bDoneDialogue_PlayerGetsOutVehicle
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T7")
						AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_OUT")
							IF (GET_RANDOM_INT_IN_RANGE(0, 11) < 5)
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T7", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Random selected dialogue lines - 
									// Get out.
									// Come on out.
									// Come on.
									// Get the fuck out.
									// Move it.
									bDoneDialogue_PlayerGetsOutVehicle = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerGetsOutVehicle - BBC_T7") ENDIF #ENDIF
								ENDIF
							ELSE
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_OUT", CONV_PRIORITY_AMBIENT_HIGH, eSubtitleState_ImportantDialogue)
									// Random selected dialogue lines - 
									// Where are you going now?  
									// What are you doing? 
									// Make up your mind!    
									bDoneDialogue_PlayerGetsOutVehicle = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_PlayerGetsOutVehicle - BB1_OUT") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// check for Trevo threatening Bail Jumper whilst in his command
					IF sBailJumperPed.AI = BB_PED_AI_STATE_SURRENDERED
					AND IsPedPerformingTask(sBailJumperPed.pedIndex, SCRIPT_TASK_HANDS_UP)
						IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, LEAVE_PLAYER_GROUP_DISTANCE)
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_J10")
								IF HAS_TIME_PASSED(iDialogueTimerHandsUpDuringDropOff, GET_RANDOM_INT_IN_RANGE(2500, 5000))
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF							
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J10", CONV_PRIORITY_AMBIENT_HIGH)
										// Random selected dialogue lines - 
										// Whoa, what are you doing?
										// Look, I'm coming with you, okay?
										// Please, I'll do exactly what you tell me to do.    
										// Don't do that. I won't run again, I promise.
										// Come on, please put the gun down now.  
										iDialogueTimerHandsUpDuringDropOff = GET_GAME_TIMER()
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger aiming at jumper BB1_J10") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELSE
								iDialogueTimerHandsUpDuringDropOff = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						iDialogueTimerHandsUpDuringDropOff = 0	//reset this timer to ensure it goes through straight away
						
						// both on foot
						IF NOT bDoneDialogue_BailJumperNoticesCops
							IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J9", CONV_PRIORITY_AMBIENT_HIGH)
								// Random selected dialogue lines - 
								// Looks like we're both wanted men now.
								// Are those guys after you or me?
								iDialogueTimer = GET_GAME_TIMER()
								bDoneDialogue_BailJumperNoticesCops = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger bDoneDialogue_BailJumperNoticesCops BB1_J9") ENDIF #ENDIF
							ENDIF
						ENDIF
						//  bail jumper is struggling to keep up
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF HAS_TIME_PASSED(iDialogueTimer, GET_RANDOM_INT_IN_RANGE(3000, 6000))
								IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, (LEAVE_PLAYER_GROUP_DISTANCE - 5))
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J4", CONV_PRIORITY_AMBIENT_HIGH)
										// Random selected dialogue lines - 
										// Are you going to wait for me or not?  
										// I'm doing my best to keep up, okay?
										// I'm a fifty-two year old man. You're going to have to slow it down.
										// I can't go that fast. I'm real unfit.  
										iDialogueTimer = GET_GAME_TIMER()
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HANDLE_DIALOGUE_DURING_DROPOFF - trigger struggle to keep up BB1_J4") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    rubberbanding for a fleeing ped in a vehicle for the chasing player
///    NOTE: doesn't test for the ped being alive, being in a vehicle or having a valid drive task
/// PARAMS:
///    pedIndex_Driver - the ped driving
///    vehIndex - the driver's vehicle
///    vPlayerPosition - the player's position
PROC UPDATE_VEHICLE_SPEED_FOR_CHASING_PLAYER(PED_INDEX pedIndex_Driver, VEHICLE_INDEX vehIndex, VECTOR vPlayerPosition)
	FLOAT fSetSpeed
	FLOAT fIncreaseSpeedRange = 64.0	// 8.0
	FLOAT fDecreaseSpeedRange = 1600.0	// 40.0
	FLOAT fUberDecreaseSpeedRange = 4900	// 70.0	
	FLOAT fCurrentChaseDistanceSquared = VDIST2(GET_ENTITY_COORDS(pedIndex_Driver), vPlayerPosition)	
	
	//IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
	IF fCurrentChaseDistanceSquared > fUberDecreaseSpeedRange
	AND NOT IS_ENTITY_ON_SCREEN(vehIndex)
		fSetSpeed = 12.0
	ELIF fCurrentChaseDistanceSquared > fDecreaseSpeedRange
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			fSetSpeed = 10.0
		ELSE
			fSetSpeed = 18.0
		ENDIF
	ELIF fCurrentChaseDistanceSquared < fIncreaseSpeedRange
		fSetSpeed = 28.0
	ELSE
		fSetSpeed = 22.0
	ENDIF
	
	IF (GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER) = pedIndex_Driver)	// B*1909317 - assert fix for ped needing to be the driver
		SET_DRIVE_TASK_CRUISE_SPEED(pedIndex_Driver, fSetSpeed)		
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_VEHICLE_SPEED_FOR_CHASING_PLAYER - cruise speed set to - ", fSetSpeed) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles ped driving along point route - uses TASK_VEHICLE_DRIVE_TO_COORD
/// PARAMS:
///    pedIndex_Driver 		- the ped driving
///    TargetVehicle 		- the vehicle they are using
///    TargetVehicleModel 	- the vehicle model
///    ChaseRoute - 
///    ChaseIndex - 
///    iTotalChasePoints - 
/// RETURNS:
///    TRUE if ped has reached final point chase route point
FUNC BOOL PROGRESS_VEHICLE_ROUTE(PED_INDEX pedIndex_Driver, VEHICLE_INDEX TargetVehicle, MODEL_NAMES TargetVehicleModel, VECTOR &ChaseRoute[], INT &ChaseIndex, INT iTotalChasePoints)
	IF iChaseIndex >= iTotalChasePoints
		RETURN TRUE
	ENDIF
	// info from jMart - just use 0 for target dist if you don't want them to stop at all
	IF NOT IS_ENTITY_IN_RANGE_COORDS(TargetVehicle, ChaseRoute[ChaseIndex], 15)
		IF NOT IsPedPerformingTask(pedIndex_Driver, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
		AND NOT IsPedPerformingTask(pedIndex_Driver, SCRIPT_TASK_VEHICLE_GOTO_NAVMESH)
			IF iChaseIndex < 7 // point at which the chase leaves the road network
				TASK_VEHICLE_DRIVE_TO_COORD(pedIndex_Driver, TargetVehicle, ChaseRoute[ChaseIndex], 35, DRIVINGSTYLE_NORMAL, TargetVehicleModel, DRIVINGMODE_AVOIDCARS_RECKLESS, 0.0, 2.0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PROGRESS_VEHICLE_ROUTE : ", "TASK_VEHICLE_DRIVE_TO_COORD failed so reapply - target coords : ", ChaseRoute[ChaseIndex]) ENDIF #ENDIF
			ELSE
				TASK_VEHICLE_GOTO_NAVMESH(pedIndex_Driver, TargetVehicle, ChaseRoute[ChaseIndex], 35, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_PreferNavmeshRoute, 0.0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PROGRESS_VEHICLE_ROUTE : ", "New checkpoint :", ChaseIndex, "TASK_VEHICLE_GOTO_NAVMESH failed so reapply - target coords : ", ChaseRoute[ChaseIndex]) ENDIF #ENDIF
			ENDIF
		ENDIF
	ELSE
		ChaseIndex++
		IF iChaseIndex >= iTotalChasePoints
			RETURN TRUE
		ELSE
			IF iChaseIndex < 7 // point at which the chase leaves the road network
				TASK_VEHICLE_DRIVE_TO_COORD(pedIndex_Driver, TargetVehicle, ChaseRoute[ChaseIndex], 35, DRIVINGSTYLE_NORMAL, TargetVehicleModel, DRIVINGMODE_AVOIDCARS_RECKLESS, 0.0, 2.0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PROGRESS_VEHICLE_ROUTE : ", "New checkpoint :", ChaseIndex, "TASK_VEHICLE_DRIVE_TO_COORD - target coords : ", ChaseRoute[ChaseIndex]) ENDIF #ENDIF
			ELSE
				TASK_VEHICLE_GOTO_NAVMESH(pedIndex_Driver, TargetVehicle, ChaseRoute[ChaseIndex], 35, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_PreferNavmeshRoute, 0.0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "PROGRESS_VEHICLE_ROUTE : ", "New checkpoint :", ChaseIndex, "TASK_VEHICLE_GOTO_NAVMESH - target coords : ", ChaseRoute[ChaseIndex]) ENDIF #ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE msStage)
	emissionStage = msStage
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///    requests the assets for the outro mocap cutscene entities so SET_CUTSCENE_PED_COMPONENT_VARIATION_... can be called
PROC SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()

	IF IS_ENTITY_ALIVE(sBailJumperPed.pedIndex)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_BailJumper, sBailJumperPed.pedIndex)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - Bail Jumper - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(sMaude.index)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_Maude, sMaude.index)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - Maude - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles requesting mocap on player approach and releasing outro mocap if player moves away
PROC MANAGE_OUTRO_MOCAP_LOADING()

	IF IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMaudePlace, DEFAULT_CUTSCENE_LOAD_DIST)
		IF NOT IS_PLAYER_CHANGING_CLOTHES()
			REQUEST_CUTSCENE(tlOutroMocapName)
			SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			bRequestedOutroMocap = TRUE
		ENDIF
	ELSE	
		IF bRequestedOutroMocap
			IF NOT IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMaudePlace, DEFAULT_CUTSCENE_UNLOAD_DIST)
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
					bRequestedOutroMocap = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ELSE
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			ENDIF
		ENDIF
	ENDIF
	// B*1579800 - have to bin off the mocap if player changes clothes
	IF bRequestedOutroMocap
		IF IS_PLAYER_CHANGING_CLOTHES()
			REMOVE_CUTSCENE()
			bRequestedOutroMocap = FALSE
			CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " player changing clothes! Frame Count : ", GET_FRAME_COUNT())		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles everything which needs to happen after END_REPLAY_SETUP() has been called
/// PARAMS:
///    eCurrentStage - which mission stage we are returning to
PROC REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(MISSION_STAGE eReturnToStage)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH eReturnToStage
			CASE MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET
				// preload the mocap do it kicks in straight away
				RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : ", "RC_REQUEST_MID_MISSION_CUTSCENE : MAUDE_MCS_2 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				
				WHILE NOT RC_IS_CUTSCENE_OK_TO_START(TRUE)
				OR NOT CREATE_AND_SETUP_MAUDE()
					RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
					SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : ", "waiting on RC_IS_CUTSCENE_OK_TO_START & CREATE_AND_SETUP_MAUDE : MAUDE_MCS_2 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					
					WAIT(0)
				ENDWHILE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles setup game to fulfil mission stage criteria so it can advance to the next stage
PROC SKIP_MISSION_STAGE()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		KILL_ANY_CONVERSATION()
		CLEAR_PRINTS()
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP(TRUE)
		ENDIF
		SWITCH eMissionStage		
			//	------------------------------------------
			CASE MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.						
					IF NOT IS_REPLAY_BEING_SET_UP()
						//WARP_PLAYER_TO_RAMDOM_SAFE_POINT_IN_RANGE_PED(sBailJumperPed.pedIndex, BAIL_BONDS_RANGE_TO_TARGET_BLIP-1)
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPos_PlayerWarpGetNearTarget, fHeading_PlayerWarpGetNearTarget)
						IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
							SET_ENTITY_FACING(PLAYER_PED_ID(), GET_ENTITY_COORDS(sBailJumperPed.pedIndex))
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION	// if the target stage is the next stage load the scene otherwise don't bother
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					bDoneDialogue_PlayerSpotsBailJumperFromDistance = TRUE
					ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					eSubStage = SS_CLEANUP							
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			//	------------------------------------------
			CASE MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION
				bDoneDialogue_PlayerSpotsBailJumperFromDistance = TRUE
				bDoneDialogue_PlayerSpotsBailJumperCloseDistance = TRUE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF NOT IS_REPLAY_BEING_SET_UP()
						//WARP_PLAYER_TO_RAMDOM_SAFE_POINT_IN_RANGE_PED(sBailJumperPed.pedIndex, BAIL_BONDS_RANGE_TO_TARGET_ACTIVE-1)
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPos_PlayerWarpApproachTarget, fHeading_PlayerWarpApproachTarget)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_CHASE	// if the target stage is the next stage load the scene otherwise don't bother
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					sBailJumperPed.iTimer = GET_GAME_TIMER() - 5000	// force wait to go ahead
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK	
			//	------------------------------------------
			CASE MISSION_STAGE_CHASE
				bDoneDialogue_BailJumperBeginsToFlee = TRUE	// set to skip over the convo's which loop in the SS_SETUP sub stage
				IF eBailBondType = BBT_VEHICLE_FLEE
					IF eSubStage = SS_SETUP
						IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
						AND IS_VEHICLE_OK(sMissionVehicle[0].index)
							IF NOT IS_PED_IN_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index)	
								SET_PED_INTO_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE	// skip the initial dialogue which triggers in MISSION_STAGE_TAKE_TARGET_TO_MAUDE
					IF NOT IS_REPLAY_BEING_SET_UP()
						WARP_PLAYER_TO_RAMDOM_SAFE_POINT_IN_RANGE_PED(sBailJumperPed.pedIndex, 2.5)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_TAKE_TARGET_TO_MAUDE	// if the target stage is the next stage load the scene otherwise don't bother
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_CHASE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					eSubStage = SS_CLEANUP	// B*1468078 - conditions aren't met on replay because player can't be warped
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_CHASE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			//	------------------------------------------
			CASE MISSION_STAGE_TAKE_TARGET_TO_MAUDE
				bDoneObjective_ApproachBailJumper = TRUE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					SAFE_TELEPORT_PED(sBailJumperPed.pedIndex, vMaudePlace, 0)
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), vMaudePlace, 254.9762)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)					
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET	// if the target stage is the next stage load the scene otherwise don't bother
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					eSubStage = SS_CLEANUP					
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			//	------------------------------------------
			CASE MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET	
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
					ENDIF	
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			//	------------------------------------------
			CASE MISSION_STAGE_LOSE_THE_COPS
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_LOSE_THE_COPS : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			//	------------------------------------------
			CASE MISSION_STAGE_TARGET_FLEES_AGAIN
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE	// skip the initial dialogue which triggers in MISSION_STAGE_TAKE_TARGET_TO_MAUDE
					IF NOT IS_REPLAY_BEING_SET_UP()
						WARP_PLAYER_TO_RAMDOM_SAFE_POINT_IN_RANGE_PED(sBailJumperPed.pedIndex, 2.5)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_TAKE_TARGET_TO_MAUDE	// if the target stage is the next stage load the scene otherwise don't bother
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_CHASE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					RESET_MISSION_PED_TIMERS_AND_THREAT_TESTS()
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SKIP_STAGE : ", "MISSION_STAGE_TARGET_FLEES_AGAIN : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
///    Also decides when we have finished skipping
/// PARAMS:
///    eNewStage - stage to jump to
PROC JUMP_TO_STAGE(MISSION_STAGE eNewStage)
	IF eMissionStage = eNewStage	// end skip stage
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)	// don't return control before REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY has finished RC_END_Z_SKIP to handle it
		ENDIF
		REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(eMissionStage)	// specific setup which must come after END_REPLAY_SETUP
		//B* 1468238 - don't fade back in here when skipping to mocap (let the mocap stage handle it)
		IF eMissionStage = MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET
			RC_END_Z_SKIP(DEFAULT, FALSE)
		ELSE
			RC_END_Z_SKIP()
		ENDIF
        bFinishedStageSkipping = TRUE
		bLoadedWorldForStageSkipping = FALSE
		// ensure we are fully faded in if we have skipped to the mission passed stage, since the mission passed GUI doesn't display if not (seems to need a frame wait too)
		IF eMissionStage = MISSION_STAGE_MISSION_PASSED
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
	ELSE
		SKIP_MISSION_STAGE()
	ENDIF	
ENDPROC

/// PURPOSE: 
///     Reset the mission, cleanups the current state and set's the mission up again
///     USED by the mission replay checkpoint setup and Debug skips
PROC RESET_MISSION()
	MISSION_CLEANUP(TRUE, FALSE)
	
	//teleport player back to start location, as he'll fall through the floor otherwise, because its not loaded.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_REPLAY_BEING_SET_UP()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		ENDIF
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	KILL_ANY_CONVERSATION()	// added here since i commented it out in mission_cleanup as fail dialogue can be playing when it's called
	REMOVE_RELATIONSHIP_GROUP(HASH_BAILBOND_TARGET)
	//re do mission initialization
	INIT_MISSION()
	SET_STAGE(MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER)
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "RESET_MISSION - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Perform a Z skip.  Used by the mission checkpoints and the debug Z skip function
/// PARAMS:
///    iNewStage - Mission stage we want to skip to
///    bResetMission - used when we go backwards in mission flow.  If false we also don't stop the active cutscene in RC_START_Z_SKIP, instead handled in SKIP_STAGE to fix bug 1006740
PROC DO_Z_SKIP(INT iNewStage, BOOL bResetMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "DO_Z_SKIP with parameters  - iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	RC_START_Z_SKIP(bResetMission)
	IF bResetMission
		RESET_MISSION()
	ENDIF
	eMissionSkipTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
	bFinishedStageSkipping = FALSE
	IF IS_REPLAY_BEING_SET_UP()
		bLoadedWorldForStageSkipping = TRUE
	ELSE
		bLoadedWorldForStageSkipping = FALSE
	ENDIF
	// load world for the mission start area if we are resetting the mission to the intro mocap.  Moved here from script skip stage to fix bug 1006740 - mocap exit states not getting set as game is waiting on world to load before getting to check				
	// basically if you press CROSS to confirm which stage in the z menu, it skipped the mocap but couldn't sent exit states as it was waiting for world to load first.					
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF eMissionSkipTargetStage = MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//setup player in spot for mission start
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				bLoadedWorldForStageSkipping = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "DO_Z_SKIP -  LOADED WORLD ready at mission start area framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "DO_Z_SKIP : iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	JUMP_TO_STAGE(eMissionSkipTargetStage)
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	DEBUG - J,P and Z skip stuff
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    check for debug S, F, P and J skips
	PROC DEBUG_Check_Debug_Keys()
		INT i
		INT iNewStage
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_ANY_CONVERSATION()
			FOR i=0 TO (iNumTargetBackupPeds - 1)
				IF IS_PED_UNINJURED(sBackupPed[i].index)
					CLEAR_PED_TASKS(sBackupPed[i].index)
				ENDIF
			ENDFOR
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			CLEAR_PRINTS()
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			//make target and buddies unhostile? or done in cleanup?
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "S SKIP") ENDIF #ENDIF
			SCRIPT_PASSED(BB_PASSED_TARGET_CAPTURED)
		ENDIF
		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_ANY_CONVERSATION()
			FOR i=0 TO (iNumTargetBackupPeds - 1)
				IF IS_PED_UNINJURED(sBackupPed[i].index)
					CLEAR_PED_TASKS(sBackupPed[i].index)
				ENDIF
			ENDFOR
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			CLEAR_PRINTS()			
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "F SKIP") ENDIF #ENDIF
			SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
		ENDIF
		// Check for Skip forward
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					FALLTHRU
				CASE MISSION_STAGE_TARGET_FLEES_AGAIN
					iNewStage = ENUM_TO_INT(MISSION_STAGE_TAKE_TARGET_TO_MAUDE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "J SKIP : ", "PRE STAGE, FROM LOSE COPS / FLEE AGAIN int now = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage) + 1
				BREAK
			ENDSWITCH	
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "J SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
			DO_Z_SKIP(iNewStage, FALSE)	//perform a Z skip to the next stage, without the mission reset				
		ENDIF	
		// Check for Skip backwards
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))    
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					FALLTHRU
				CASE MISSION_STAGE_TARGET_FLEES_AGAIN
					iNewStage = ENUM_TO_INT(MISSION_STAGE_TAKE_TARGET_TO_MAUDE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "P SKIP : ", "PRE STAGE, FROM LOSE COPS / FLEE AGAIN int now = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage)-1
				BREAK
			ENDSWITCH		
		    IF iNewStage > -1
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "P SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
		    ENDIF	
		ENDIF
		// z skip menu
	    IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage)
			// if we are skipping forward in the mission stages, just J skip rather than a full mission reset
			// make sure additional stages are handled here!  These sit at the end of the enum so need to be dealt with differently.
			IF (eMissionStage = MISSION_STAGE_LOSE_THE_COPS)		// additional stages dealt seperately since they sit at the end of the MISSION_STAGE enum
			OR (eMissionStage = MISSION_STAGE_TARGET_FLEES_AGAIN)	// additional stages dealt seperately since they sit at the end of the MISSION_STAGE enum
				IF iNewStage = ENUM_TO_INT(MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET)	// trying to z skip past lose cops so don't reset mission
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, FALSE)
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, TRUE)
				ENDIF
			ELIF (iNewStage <= ENUM_TO_INT(eMissionStage))
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, FALSE)
			ENDIF
	    ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    updates the reason for mission failed
///    in order of most important fail reason so if multiple fails conditions have been set, we use the most important
PROC UPDATE_FAIL_REASON()

	// check for player attacking or killing Maude
	IF DOES_ENTITY_EXIST(sMaude.index)
		IF IS_ENTITY_DEAD(sMaude.index)
		OR IS_PED_INJURED(sMaude.index)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Mission Failed Checks - BB_FAILED_MAUDE_DIED") ENDIF #ENDIF
			eBB_MissionFailedReason = BB_FAILED_MAUDE_DIED
			EXIT
		ENDIF
		//if we have already failed for attacking Maude no need to recheck
		IF eBB_MissionFailedReason = BB_FAILED_MAUDE_ATTACKED	
			EXIT	
		ENDIF		
		IF SHOULD_MAUDE_FLEE()
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Mission Failed Checks - BB_FAILED_MAUDE_ATTACKED") ENDIF #ENDIF
			eBB_MissionFailedReason = BB_FAILED_MAUDE_ATTACKED
			EXIT
		ENDIF
	ENDIF
	//if we have already failed for jumper escaping don't do the rest of the checks
	IF eBB_MissionFailedReason = BB_FAILED_BAIL_JUMPER_ESCAPED	
	OR eBB_MissionFailedReason = BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER
		EXIT	
	ENDIF
	// check for player not activating the encounter
	IF eMissionStage = MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
		// dist check needs to exceed the mission launch distance so adding another 25 meters
		IF NOT (VDIST2(vPlayerPos, sBailBondLaunchData.vStartPoint) <= ((sBailBondLaunchData.fStartRange + 25.0)* (sBailBondLaunchData.fStartRange + 25.0)))
			IF IS_ENTITY_OCCLUDED(sBailJumperPed.pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Mission Failed Checks - BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER") ENDIF #ENDIF
				eBB_MissionFailedReason = BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(sBailJumperPed.pedIndex)
			IF NOT IS_ENTITY_DEAD(sBailJumperPed.pedIndex)
			AND NOT IS_PED_INJURED(sBailJumperPed.pedIndex)
				// Check for Bail jumper escaped
				IF eMissionStage = MISSION_STAGE_CHASE
				OR eMissionStage = MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION
				OR eMissionStage = MISSION_STAGE_TARGET_FLEES_AGAIN
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, BAIL_BONDS_ESCAPED_DISTANCE)
						IF IS_ENTITY_OCCLUDED(sBailJumperPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Mission Failed Checks - BB_FAILED_BAIL_JUMPER_ESCAPED") ENDIF #ENDIF
							eBB_MissionFailedReason = BB_FAILED_BAIL_JUMPER_ESCAPED
							EXIT
						ENDIF
					ENDIF
					// Check for Player leaving surrendered bail jumper behind
				ELIF eMissionStage = MISSION_STAGE_TAKE_TARGET_TO_MAUDE
				OR eMissionStage = MISSION_STAGE_LOSE_THE_COPS	
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 80.0)
						IF IS_ENTITY_OCCLUDED(sBailJumperPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "Mission Failed Checks - BB_FAILED_BAIL_JUMPER_ESCAPED - left behind") ENDIF #ENDIF
							eBB_MissionFailedReason = BB_FAILED_BAIL_JUMPER_ESCAPED
							EXIT
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF			
	ENDIF
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has failed
PROC MISSION_FAILED_CHECKS()
	
	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET		// B*1477983 - spook somehow occured during cutscene
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED					// don't do the checks if we are already in mission passed
			UPDATE_FAIL_REASON()
			IF eBB_MissionFailedReason <> BB_FAILED_DEFAULT
				IF eBB_MissionFailedReason = BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER
					// set the mission over state for the launcher to use
					eBailBondMissionOverState = BBMOS_CLEANUP
			        Script_Cleanup()
				ELSE
					SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has passed the mission early
PROC CHECK_BAIL_JUMPER_KILLED()
	
	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET		// B*1477983 - spook somehow occured during cutscene
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED					// don't do the checks if we are already in mission passed
		AND eMissionStage != MISSION_STAGE_MISSION_OVER_BAIL_JUMPER_KILLED	// don't do the checks if we are already in mission over for killing bail jumper stage
			IF DOES_ENTITY_EXIST(sBailJumperPed.pedIndex)
				IF IS_ENTITY_DEAD(sBailJumperPed.pedIndex)
				OR IS_PED_INJURED(sBailJumperPed.pedIndex)
					SET_STAGE(MISSION_STAGE_MISSION_OVER_BAIL_JUMPER_KILLED)
					CPRINTLN(DEBUG_BAIL_BOND, "CHECK_BAIL_JUMPER_KILLED -> MISSION_STAGE_MISSION_OVER_BAIL_JUMPER_KILLED set")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		Mission Stages			
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    waiting for the player to get close enough to interact
///    Player might have triggered the mission script but not be aware of the bail jumper
///    So if we cleaned up at this state, it's as if the mission didn't get triggered
PROC STAGE_PLAYER_LOCATE_BAIL_JUMPER()
	
	SWITCH eSubStage
		CASE SS_SETUP
			IF bFinishedStageSkipping
				IF IS_REPEAT_PLAY_ACTIVE()
				AND NOT g_bPlayerIsInTaxi	// don't fade up whilst in a taxi
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, NEEDED for repeat play purposes.
					CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER - SS_SETUP fade in for repeat play")
				ENDIF
			ENDIF
			// B*1598235 - if the player has skipped a taxi ride to a bail bond - don't start the mission flow until he gets out the cab
			IF NOT g_bPlayerIsInTaxi
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER - SS_SETUP done") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			ELSE
				CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER - SS_SETUP waiting for player to leave the taxi")
			ENDIF
		BREAK		
		CASE SS_UPDATE
			HANDLE_TREVOR_DIALOGUE_FOR_READING_TRIGGER_EMAIL(sBailBondLaunchData, sDialogue)
			IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
				UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper, DEFAULT, TT_BODY_PART_HEAD)
				UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
				UPDATE_BAIL_JUMPER_PED_AI()
				
				// check to see when the player detects the bail jumper
				IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, BAIL_BONDS_RANGE_TO_TARGET_ACTIVE)
				AND IS_PLAYER_AT_SAME_HEIGHT_AS_PED(sBailJumperPed.pedIndex, vPlayerPos, 10.0)
					// don't shout out to the bail jumper if in stealth mode
					IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
					OR IS_PED_IN_COVER(PLAYER_PED_ID())
						// if bail jumper spots him first he shouts out
						IF BB_CAN_PED_SEE_PLAYER_THIS_FRAME(sBailJumperPed.iFrameCountLastSeenPlayer)
							ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player in stealth / cover - bail jumper spotted") ENDIF #ENDIF
							eSubStage = SS_CLEANUP
						// if he bumps into him spot him
						ELIF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
						AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sBailJumperPed.pedIndex)
							ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player in stealth / cover - bail jumper bumped into") ENDIF #ENDIF
							eSubStage = SS_CLEANUP
						ENDIF
					ELSE
						// if player can see bail jumper shout out
						IF BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper, FALSE)	
							ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player spotted the bail jumper on screen") ENDIF #ENDIF
							eSubStage = SS_CLEANUP
						// if player gets really close just shout anyway 
						ELIF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 5.0)
							ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player spotted the bail jumper close up") ENDIF #ENDIF
							eSubStage = SS_CLEANUP						
						// if bail jumper spots him first he shouts out
						ELIF BB_CAN_PED_SEE_PLAYER_THIS_FRAME(sBailJumperPed.iFrameCountLastSeenPlayer)
							// Better chance for player comment to trigger first
							IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, (BAIL_BONDS_RANGE_TO_TARGET_ACTIVE - 1.5))
								ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : bail jumper spotted player") ENDIF #ENDIF
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF		
					ENDIF
				ELSE
					// targetting him from a distance trigger different dialogue
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sBailJumperPed.pedIndex)	
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sBailJumperPed.pedIndex)
						ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player spotted the bail jumper from distance") ENDIF #ENDIF
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
				IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, BAIL_BONDS_RANGE_TO_TARGET_ACTIVE)	// BAIL_BONDS_INTIMIDATING_DETECTION_RANGE)			
					ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_INTIMIDATED
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : SS_UPDATE done : player spotted the bail jumper via intimidated checks") ENDIF #ENDIF
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		CASE SS_CLEANUP
			IF ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_INTIMIDATED
				INIT_FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, TRUE, FALSE)	// skip over the bail jumper text in this instance (or maybe switch it out for some alternative)
				SET_STAGE(MISSION_STAGE_CHASE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER - SS_CLEANUP done -> MISSION_STAGE_CHASE") ENDIF #ENDIF
			ELSE
				// if bail jumper spotted the player first kick off his dialogue before player response
				IF ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
					bDoneDialogue_BailJumperSpotsPlayerFirst = FALSE
					IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
					AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
						sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 50000) //set high to ensure it goes through straight away
						sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER						
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER : ", "SS_CLEANUP - sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER") ENDIF #ENDIF
				ENDIF
				SET_STAGE(MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_PLAYER_LOCATE_BAIL_JUMPER - SS_CLEANUP done -> MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION") ENDIF #ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    player closer to the target, waiting for him to interact
PROC STAGE_TRIGGER_BAIL_JUMPER_REACTION()
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
		UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper, DEFAULT, TT_BODY_PART_HEAD)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
		UPDATE_BAIL_JUMPER_PED_AI()
		
		SWITCH eSubStage
			CASE SS_SETUP			
				IF NOT DOES_BLIP_EXIST(sBailJumperPed.blipIndex)
					sBailJumperPed.blipIndex = CREATE_PED_BLIP(sBailJumperPed.pedIndex, TRUE, FALSE)
				ENDIF
				INIT_FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, TRUE, FALSE)	// skip over the bail jumper text in this instance (or maybe switch it out for some alternative)
				eSubStage = SS_UPDATE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_SETUP - done") ENDIF #ENDIF
			BREAK		
			CASE SS_UPDATE
				SWITCH ePlayerSpotsBailJumperType
				
					CASE BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE
					
						IF NOT bDoneDialogue_PlayerSpotsBailJumperFromDistance
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T14", CONV_PRIORITY_AMBIENT_HIGH)
								// RANDOM SELECTED LINE
								// Well look who it is.
								// There's the fucker.
								// That looks like the bastard.
								sBailJumperPed.iTimer = GET_GAME_TIMER()
								bDoneDialogue_PlayerSpotsBailJumperFromDistance = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_UPDATE - bDoneDialogue_PlayerSpotsBailJumperFromDistance") ENDIF #ENDIF
							ENDIF
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
						ELIF NOT bDoneObjective_ApprehendBailJumper
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T14")	// bDoneDialogue_PlayerSpotsBailJumperFromDistance
								INIT_FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APHEND", "BBS_APHEND2", iFlashBlipTimer, iFlashBlipGodTextTimer, TRUE, TRUE)	// Apprehend the ~r~bail jumper. // Apprehend the ~b~bail jumper.
								bDoneObjective_ApprehendBailJumper = TRUE
							ENDIF
						ELSE
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APHEND", "BBS_APHEND2", iFlashBlipTimer, iFlashBlipGodTextTimer)
							
							// check to see when the player interacts with the bail jumper
							IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, BAIL_BONDS_RANGE_TO_TARGET_ACTIVE)
							AND IS_PLAYER_AT_SAME_HEIGHT_AS_PED(sBailJumperPed.pedIndex, vPlayerPos, 10.0)
								// don't shout out to the bail jumper if in stealth mode
								IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								OR IS_PED_IN_COVER(PLAYER_PED_ID())
									// if bail jumper spots him first he shouts out
									IF BB_CAN_PED_SEE_PLAYER_THIS_FRAME(sBailJumperPed.iFrameCountLastSeenPlayer)
										ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
										bDoneDialogue_BailJumperSpotsPlayerFirst = FALSE
											IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
											AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
												sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 50000) //set high to ensure it goes through straight away
												sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER						
											ENDIF
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE done : player in stealth / cover - bail jumper spotted") ENDIF #ENDIF
									// if he bumps into him spot him
									ELIF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
									AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sBailJumperPed.pedIndex)
										ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
										bDoneDialogue_BailJumperSpotsPlayerFirst = FALSE
										IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
										AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
											sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 50000) //set high to ensure it goes through straight away
											sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER						
										ENDIF
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE done : player in stealth / cover - bail jumper bumped into") ENDIF #ENDIF
									ENDIF
								ELSE
									// if player can see bail jumper shout out
									IF BB_CAN_PLAYER_SEE_PED_WHO_IS_VISIBLE_ON_SCREEN(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper, FALSE)	
										ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE done : player spotted the bail jumper on screen") ENDIF #ENDIF
									// if player gets really close just shout anyway 
									ELIF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 5.0)
										ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE done : player spotted the bail jumper close up") ENDIF #ENDIF	
									// if bail jumper spots him first he shouts out
									ELIF BB_CAN_PED_SEE_PLAYER_THIS_FRAME(sBailJumperPed.iFrameCountLastSeenPlayer)
										// Better chance for player comment to trigger first
										IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, (BAIL_BONDS_RANGE_TO_TARGET_ACTIVE - 1.5))
											ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
											bDoneDialogue_BailJumperSpotsPlayerFirst = FALSE
											IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
											AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
												sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 50000) //set high to ensure it goes through straight away
												sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER						
											ENDIF
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE done : bail jumper spotted player") ENDIF #ENDIF
										ENDIF
									ENDIF		
								ENDIF
							ENDIF
						ENDIF					
						IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, BAIL_BONDS_INTIMIDATING_DETECTION_RANGE)
							eSubStage = SS_CLEANUP
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE ", "BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE done") ENDIF #ENDIF
						ENDIF
					BREAK
					
					CASE BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE
						FALLTHRU
					CASE BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER
					
						IF NOT bDoneDialogue_BailJumperSpotsPlayerFirst
							// do dialogue then objective
							IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND")	// Apprehend the ~r~bail jumper.
							OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND2") // Apprehend the ~b~bail jumper.
								CLEAR_PRINTS()
							ENDIF
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J2", CONV_PRIORITY_AMBIENT_HIGH)
								// RANDOM SELECTED LINE
								// Who the hell are you?
								// Can I help you?
								// Do you work here?  
								// What are you doing here?  
								sBailJumperPed.iTimer = GET_GAME_TIMER()
								iDialogueTimer = GET_GAME_TIMER()
								bDoneDialogue_BailJumperSpotsPlayerFirst = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_UPDATE - bDoneDialogue_BailJumperSpotsPlayerFirst, FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
						ELIF NOT bDoneDialogue_PlayerSpotsBailJumperCloseDistance
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BBC_T14")	// bDoneDialogue_PlayerSpotsBailJumperFromDistance
							AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("BB1_J2")	// bDoneDialogue_BailJumperSpotsPlayerFirst
								// do dialogue then objective
								IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND")	// Apprehend the ~r~bail jumper.
								OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND2") // Apprehend the ~b~bail jumper.
									CLEAR_PRINTS()
								ENDIF
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T1", CONV_PRIORITY_AMBIENT_HIGH)
									// RANDOM SELECTED LINE
									// Hey punk, you're coming with me!
									// There's a bounty on your head, and it's mine!
									// Let's do this the easy way, the hard ends with you on my boot!
									// Be a good little convict and come with me!
									// Hands where I can see them!
									// Dead or alive you're coming with me!
									sBailJumperPed.iTimer = GET_GAME_TIMER()
									bDoneDialogue_PlayerSpotsBailJumperCloseDistance = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_UPDATE - bDoneDialogue_PlayerSpotsBailJumperCloseDistance, FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ELSE
									// B*1462370 - took Trevor ages to reply because his veh was flipped over
									IF ePlayerSpotsBailJumperType = BB_PSBJT_BAIL_JUMPER_SPOTS_PLAYER	// only do if bail jumper spoke first otherwise it could get skipped over!
										IF HAS_TIME_PASSED(iDialogueTimer, 1250)
											sBailJumperPed.iTimer = (GET_GAME_TIMER() - 3000) // ensure task goes straight through in this instance
											bDoneDialogue_PlayerSpotsBailJumperCloseDistance = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_UPDATE - bDoneDialogue_PlayerSpotsBailJumperCloseDistance skipped time out, FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iDialogueTimer = GET_GAME_TIMER()
							ENDIF
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
						ELSE							
							IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
							AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
								IF HAS_TIME_PASSED(sBailJumperPed.iTimer, 500)
									sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
									sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER
									eSubStage = SS_CLEANUP
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_UPDATE - sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER") ENDIF #ENDIF
								ENDIF
							ELSE
								eSubStage = SS_CLEANUP
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE ", "BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_CLOSE_DISTANCE done") ENDIF #ENDIF
							ENDIF
						ENDIF
						IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer, BAIL_BONDS_INTIMIDATING_DETECTION_RANGE)
							eSubStage = SS_CLEANUP
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "BAIL_JUMPER_SHOULD_REACT_EARLY returned true so heading the SS_CLEANUP") ENDIF #ENDIF
						ENDIF
					BREAK
					
					CASE BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_INTIMIDATED
					
						FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
						eSubStage = SS_CLEANUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : SS_UPDATE ", "BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_INTIMIDATED done") ENDIF #ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SS_CLEANUP
				IF sBailJumperPed.AI != BB_PED_AI_SETUP_AWARE_OF_PLAYER
				AND sBailJumperPed.AI != BB_PED_AI_STATE_AWARE_OF_PLAYER
					sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 50000) //set high to ensure it goes through straight away
					sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_CLEANUP - sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER") ENDIF #ENDIF
				ENDIF
				sBailJumperPed.iTimer = GET_GAME_TIMER()
				IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND")	// Apprehend the ~r~bail jumper. // Apprehend the ~b~bail jumper. 
				OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APHEND2") // Apprehend the ~b~bail jumper.
					CLEAR_PRINTS()
				ENDIF				
				FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
				iFlashBlipTimer = GET_GAME_TIMER()
				iFlashBlipGodTextTimer = GET_GAME_TIMER()
				SET_STAGE(MISSION_STAGE_CHASE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TRIGGER_BAIL_JUMPER_REACTION : ", "SS_CLEANUP - done") ENDIF #ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC STAGE_CHASE()

	BOOL bStopFleeingIfPlayerClose = FALSE
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
		
		UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
		UPDATE_BAIL_JUMPER_PED_AI()
		
		SWITCH eSubStage
			CASE SS_SETUP
				IF sBailJumperPed.AI != BB_PED_AI_SETUP_FLEE_TO_VEHICLE
				AND sBailJumperPed.AI != BB_PED_AI_STATE_FLEE_TO_VEHICLE
					sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
					sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_TO_VEHICLE
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_AWARE_OF_PLAYER") ENDIF #ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
					IF NOT DOES_BLIP_EXIST(sBailJumperPed.blipIndex)
						sBailJumperPed.blipIndex = CREATE_PED_BLIP(sBailJumperPed.pedIndex, TRUE, FALSE)
					ENDIF
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sBailJumperPed.pedIndex)
				ENDIF
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_SETUP - done") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			BREAK		
			CASE SS_UPDATE
				IF NOT bHasBailJumperSurrendered
					FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sBailJumperPed.pedIndex)
					
					// Flee on foot it the player gets in the escape vehicle or it gets wrecked
					IF sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
					OR sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_ON_FOOT
						IF iTime_BailJumperStartedFleeingOnFoot = -1	// if this hasn't been set yet, start the timer
							iTime_BailJumperStartedFleeingOnFoot = GET_GAME_TIMER()
						ENDIF
						IF iDelay_SurrenderForPlayerCloseToWhileFleeing = -1	// if this hasn't been set yet, start the timer
							iDelay_SurrenderForPlayerCloseToWhileFleeing = GET_GAME_TIMER()
						ELIF HAS_TIME_PASSED(iDelay_SurrenderForPlayerCloseToWhileFleeing, 5000)	// don't surrender if player is close by initially (give him chance to run)
							bStopFleeingIfPlayerClose = TRUE
						ENDIF
						IF SHOULD_PED_SURENDER_ONFOOT(sBailJumperPed.pedIndex, vPlayerPos, bStopFleeingIfPlayerClose)
							SET_BAIL_JUMPER_SURRENDERED()
							sBailJumperPed.iTimer = GET_GAME_TIMER()
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)							
							bHasBailJumperSurrendered = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_UPDATE - surrender on foot") ENDIF #ENDIF
						ELSE
							// additional dialogue during on foot chase
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
								IF HAS_TIME_PASSED(iDialogueTimer, GET_RANDOM_INT_IN_RANGE(5000, 12000))								
									IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, DIST_LIMIT_PLAYER_TO_PED_FOR_DIALOGUE)
										IF IS_PED_FLEEING(sBailJumperPed.pedIndex)
											IF (GET_RANDOM_INT_IN_RANGE(0, 101) < 30)
												IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T13", CONV_PRIORITY_AMBIENT_HIGH)
													// RANDOM SELECTED LINE
													// I'm going to catch you, fucker!
													// You can't out run a mad man!
													// Trevor's going to hunt you down!
													iDialogueTimer = GET_GAME_TIMER()
													#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_UPDATE - trigger on foot flee dialogue : BBC_T13") ENDIF #ENDIF
												ENDIF
											ELSE
												IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
													// RANDOM SELECTED LINE
													// I'm not going down!
													// I'm innocent!
													// You've got the wrong guy! 
													// Leave me alone!
													// I want to see some credentials!  
													iDialogueTimer = GET_GAME_TIMER()
													#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_UPDATE - trigger on foot flee dialogue : BB1_FLEE") ENDIF #ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iDialogueTimer = GET_GAME_TIMER()
							ENDIF
							UPDATE_PED_ON_FOOT_SPEED_FOR_CHASING_PLAYER(sBailJumperPed.pedIndex, vPlayerPos, iTime_BailJumperStartedFleeingOnFoot)
						ENDIF
					ELSE
						// heading to the vehicle
						IF sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_TO_VEHICLE
						OR sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_TO_VEHICLE
							IF NOT bDoneDialogue_BailJumperBeginsToFlee								
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, DIST_LIMIT_PLAYER_TO_PED_FOR_DIALOGUE)
										IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, sDialogue_BailJumperBeginChase, CONV_PRIORITY_AMBIENT_HIGH)
											sBailJumperPed.iTimer = GET_GAME_TIMER()
											iDialogueTimer = GET_GAME_TIMER()
											bDoneDialogue_BailJumperBeginsToFlee = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "bDoneDialogue_BailJumperBeginsToFlee") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							// B*1316512 - allow surrender on foot checks for running to the car
							IF SHOULD_PED_SURENDER_ONFOOT(sBailJumperPed.pedIndex, vPlayerPos, bStopFleeingIfPlayerClose)
								SET_BAIL_JUMPER_SURRENDERED()
								sBailJumperPed.iTimer = GET_GAME_TIMER()
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								bHasBailJumperSurrendered = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_UPDATE - surrender on foot - whilst getting to his car") ENDIF #ENDIF
							ENDIF
						ENDIF
						
						// fleeing in the vehicle
						IF IS_VEHICLE_OK(sMissionVehicle[0].index)
							IF sBailJumperPed.AI = BB_PED_AI_STATE_TEMP_OVERRIDE_AI_CONTROL
							OR sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_IN_VEHICLE
							OR sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_IN_VEHICLE									
								IF SHOULD_PED_SURRENDER_VEHICLE(sBailJumperPed.pedIndex, sMissionVehicle[0].index, iTimer_BailJumperVehicleStopped)
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", " ENEMY VEHICLE HEALTH = ", GET_ENTITY_HEALTH(sMissionVehicle[0].index)) ENDIF #ENDIF
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										SET_BAIL_JUMPER_SURRENDERED()
										sBailJumperPed.iTimer = GET_GAME_TIMER()
										KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
										bHasBailJumperSurrendered = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "surrender in vehicle") ENDIF #ENDIF
									ENDIF
								ELSE
									IF PROGRESS_VEHICLE_ROUTE(sBailJumperPed.pedIndex, sMissionVehicle[0].index, sMissionVehicle[0].model, vChaseRoute, iChaseIndex, iNumChaseRoutePositions)
										IF sBailJumperPed.AI = BB_PED_AI_STATE_TEMP_OVERRIDE_AI_CONTROL
											sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_IN_VEHICLE
										ENDIF
									ENDIF
									UPDATE_CHASE_BLIP(sBailJumperPed.blipIndex, sBailJumperPed.pedIndex, BAIL_BONDS_ESCAPED_DISTANCE, BAIL_BONDS_CHASE_BLIP_FLASH_PERCENTAGE)
									UPDATE_VEHICLE_SPEED_FOR_CHASING_PLAYER(sBailJumperPed.pedIndex, sMissionVehicle[0].index, vPlayerPos)
								ENDIF
							ENDIF
						ELSE
							SET_BAIL_JUMPER_SURRENDERED()
							sBailJumperPed.iTimer = GET_GAME_TIMER()
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)	
							bHasBailJumperSurrendered = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_UPDATE - early exit vehicle not ok") ENDIF #ENDIF
						ENDIF
					ENDIF				
				ELSE
					SET_BAIL_JUMPER_SURRENDERED()
					
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 10.0)
						IF NOT bDoneObjective_ApproachBailJumper
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sDialogueRoot_BailJumperSurrenders)
								INIT_FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APPSUS", "BBS_APPSUS2", iFlashBlipTimer, iFlashBlipGodTextTimer, TRUE, TRUE)	// Approach the ~r~bail jumper. // Approach the ~b~bail jumper.
								bDoneObjective_ApproachBailJumper = TRUE
							ENDIF
						ELSE
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APPSUS", "BBS_APPSUS2", iFlashBlipTimer, iFlashBlipGodTextTimer)		// Approach the ~r~bail jumper. // Approach the ~b~bail jumper.
						ENDIF
					ELSE
						IF NOT bDoneDialogue_BailJumperSurrendered
							IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
							AND NOT IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
								IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS")	// Approach the ~r~bail jumper.
								OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS2") // Approach the ~b~bail jumper.
									CLEAR_PRINTS()
								ENDIF
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, sDialogueRoot_BailJumperSurrenders, CONV_PRIORITY_AMBIENT_HIGH) 
									// God damn it!
									// Don't shoot, am sorry for running!
									// Oh for fuck sake!
									bDoneDialogue_BailJumperSurrendered = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "bDoneDialogue_BailJumperSurrendered - done") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
						FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
					ENDIF
					IF SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE(sBailJumperPed.pedIndex)
						bIsJumperRevertingBackToFlee = TRUE
						eSubStage = SS_CLEANUP				
					ENDIF
				ENDIF
			BREAK
			CASE SS_CLEANUP
				IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS")	// Approach the ~r~bail jumper.
				OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS2") // Approach the ~b~bail jumper.
					CLEAR_PRINTS()
				ENDIF
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
				bDoneDialogue_BailJumperSurrendered = TRUE // mark this up as completed as it is used to stop Bail jumper switching to follow behaviour
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, HASH_BAILBOND_TARGET)
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_BAILBOND_TARGET, RELGROUPHASH_PLAYER)
				SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)
				SET_BAIL_JUMPER_SURRENDERED()
				sBailJumperPed.iTimer = GET_GAME_TIMER()
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				// proc from initial scene so they match up
				SETUP_MAUDES_FOR_DROPOFF(TRUE, ScenarioBlockArea_CutsceneMaude)
				
				IF bIsJumperRevertingBackToFlee
					SET_STAGE(MISSION_STAGE_TARGET_FLEES_AGAIN)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_CLEANUP -> MISSION_STAGE_TARGET_FLEES_AGAIN") ENDIF #ENDIF
				ELSE
					SET_STAGE(MISSION_STAGE_TAKE_TARGET_TO_MAUDE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_CHASE : ", "SS_CLEANUP - done") ENDIF #ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC STAGE_TAKE_TARGET_TO_MAUDE()

	CREATE_AND_SETUP_MAUDE()
	MANAGE_OUTRO_MOCAP_LOADING()
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
	
		UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
		UPDATE_BAIL_JUMPER_PED_AI()
		
		SWITCH eSubStage
			CASE SS_SETUP
				eSubStage = SS_UPDATE
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_SETUP - done") ENDIF #ENDIF
			BREAK		
			CASE SS_UPDATE
				IF NOT bHasPlayerLeftBailJumperBehind
					IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")
						CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
					ENDIF
					SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)						

					// check if the player has left the bail jumper behind
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, LEAVE_PLAYER_GROUP_DISTANCE)
						// only switch states if he's following you at the time
						IF sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
						OR sBailJumperPed.AI = BB_PED_AI_STATE_FOLLOW_PLAYER
							sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
							sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND") ENDIF #ENDIF
						ENDIF
						// Comment when the bail jumper is out of range of Trevor
						IF HAS_FRAME_COUNTER_PASSED(iFrameCountPlayerLastSeenBailJumper, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCountPlayerLastSeenBailJumper, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// only comment if Trev can't see him
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T9", CONV_PRIORITY_AMBIENT_HIGH)
										// Shit, where did he go?
										// What the fuck, where is he?
										// Fuck, did I just lose him?
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE// skip this dialogue if we are going straight to left bail jumper behind state
						bHasPlayerLeftBailJumperBehind = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE - Bail Jumper removed from Player group") ENDIF #ENDIF
					ELSE		
						// player must lose their wanted level before handing bail jumper over to Maude
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
							//Don't allow the wanted level to drop this frame, as we need it to test in the cleanup
							SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
							bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE// skip this dialogue if we are going straight to left bail jumper behind state
							eSubStage = SS_CLEANUP
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_UPDATE - player has a wanted level") ENDIF #ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(blipIndexMaudesPlace)
								IF bDoneObjective_TakeBailJumperToMaude
									blipIndexMaudesPlace = CREATE_COORD_BLIP(vMaudePlace)
								ENDIF
							ENDIF
							IF NOT bDoneDialogue_TrevorResponsesToBailJumperSurrendering
								IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sDialogueRoot_BailJumperSurrenders)
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T5", CONV_PRIORITY_AMBIENT_HIGH)
										// Random dialogue line from - 
										// Correct decision, you're coming with me now.
										// Damn right, come with me before I take your legs off.
										// You're my bitch, now heel.
										bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE
									ENDIF
								ENDIF
							ELIF NOT bDoneObjective_TakeBailJumperToMaude
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT_NOW("BBS_DROP", DEFAULT_GOD_TEXT_TIME, 0)	// Take the bail jumper to ~y~Maude's place.
									iDialogueTimer = GET_GAME_TIMER()
									bDoneObjective_TakeBailJumperToMaude = TRUE
								ENDIF
							ELSE
								HANDLE_DIALOGUE_DURING_DROPOFF()
								TRIGGER_AMBIENT_MAUDE_DIALOGUE_FOR_PLAYER_APPROACH_WITH_JUMPER(sMaude.index, vPlayerPos, iDialogueTimer_MaudeAmbient)
							ENDIF
							
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMaudePlace, << 4.0, 4.0, LOCATE_SIZE_HEIGHT >>, TRUE)	// used to display locate for dropoff -  LOCATE_SIZE_ANY_MEANS for x and y but Les Bug * 1078339
							ENDIF
							// inner area covering the area where Maude is
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2732.815186,4143.618164,42.028221>>, <<2724.603027,4144.802734,46.330837>>, 9.000000)
								eSubStage = SS_CLEANUP
							ENDIF
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMaudePlace, 6.5) // Reduced activation range for B*1163584
									eSubStage = SS_CLEANUP
								ENDIF
							ELSE
								IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMaudePlace, 2.5) 
									eSubStage = SS_CLEANUP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipIndexMaudesPlace)
					IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_DROP")	
						CLEAR_THIS_PRINT("BBS_DROP")	// Take the bail jumper to ~y~Maude's place.
					ENDIF

					// check to see if the player has returned to the bail jumper
					IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, REJOIN_PLAYER_GROUP_DISTANCE)
						// only make him rejoin if he was in left behind state
						IF sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
						OR sBailJumperPed.AI = BB_PED_AI_STATE_PLAYER_LEFT_BEHIND
							sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
							sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER") ENDIF #ENDIF
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")	
							CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
						ENDIF
						// Comment when the bail jumper is back in range of Trevor and he's returning to follow behaviour
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
							OR sBailJumperPed.AI = BB_PED_AI_STATE_FOLLOW_PLAYER
								IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T10", CONV_PRIORITY_AMBIENT_HIGH)
										// Come back here bitch.
										// You sneaky fuck, get back here.
										// You trying to escape fucker?
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J5", CONV_PRIORITY_AMBIENT_HIGH)
										// I thought you'd had a change of heart?  
										// Are you serious? I'd assumed I was free to go.
										// Oh crap, you're back. I thought you'd let me go.  
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						bHasPlayerLeftBailJumperBehind = FALSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TAKE_TARGET_TO_MAUDE - Bail Jumper added back into Player group") ENDIF #ENDIF
					ELSE
						IF NOT DOES_BLIP_EXIST(sBailJumperPed.blipIndex)
							sBailJumperPed.blipIndex = CREATE_PED_BLIP(sBailJumperPed.pedIndex, TRUE, TRUE)
						ENDIF
						IF NOT bDoneObjective_ReturnToBailJumper
							PRINT_NOW("BBS_05", DEFAULT_GOD_TEXT_TIME, 1)	// Return to the ~b~bail jumper.~s~
							bDoneObjective_ReturnToBailJumper = TRUE
						ENDIF
						// play any suitable dialogue here
					ENDIF
				ENDIF
				IF NOT bCleanupBailJumperLocationDuringDropOff
					bCleanupBailJumperLocationDuringDropOff = CLEANUP_BAIL_JUMPER_LOCATION_DURING_DROPOFF(vPlayerPos, sBailBondLaunchData.vStartPoint, 180.0)
				ENDIF
				IF SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE(sBailJumperPed.pedIndex)
					bIsJumperRevertingBackToFlee = TRUE
					eSubStage = SS_CLEANUP				
				ENDIF
			BREAK		
			CASE SS_CLEANUP
				SAFE_REMOVE_BLIP(blipIndexMaudesPlace)
				IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_DROP")	
					CLEAR_THIS_PRINT("BBS_DROP")	// Take the bail jumper to ~y~Maude's place.
				ENDIF
				// jumper is fleeing again
				IF bIsJumperRevertingBackToFlee
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_CLEANUP -> MISSION_STAGE_TARGET_FLEES_AGAIN") ENDIF #ENDIF
					SET_STAGE(MISSION_STAGE_TARGET_FLEES_AGAIN)
				// player must lose their wanted level before handing bail jumper over to Maude
				ELIF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)					
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_CLEANUP - player had wanted level") ENDIF #ENDIF
					SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
				ELSE					
					SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)
					IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")	
						CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
					ENDIF
					IF NOT bCleanupBailJumperLocationDuringDropOff
						bCleanupBailJumperLocationDuringDropOff = CLEANUP_BAIL_JUMPER_LOCATION_DURING_DROPOFF(vPlayerPos, sBailBondLaunchData.vStartPoint, 0.0)
					ENDIF

					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "MISSION_STAGE_TAKE_TARGET_TO_MAUDE : ", "SS_CLEANUP - done") ENDIF #ENDIF
					SET_STAGE(MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Additional stage which monitors the player losing his wanted level
PROC STAGE_LOSE_THE_COPS()

	CREATE_AND_SETUP_MAUDE()
	MANAGE_OUTRO_MOCAP_LOADING()
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
	
		UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
		UPDATE_BAIL_JUMPER_PED_AI()
		
		SWITCH eSubStage		
			//	------------------------------------------
			CASE SS_SETUP				
				KILL_ANY_CONVERSATION()
				bDoneDialogue_BailJumperNoticesCops = FALSE
				bDoneObjective_LoseWantedLevel = FALSE
				eSubStage = SS_UPDATE	
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS - SS_SETUP done") ENDIF #ENDIF
			BREAK		
			//	------------------------------------------
			CASE SS_UPDATE
				IF NOT bHasPlayerLeftBailJumperBehind
					IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")
						CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
					ENDIF
					SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)					

					// check if the player has left the bail jumper behind
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, LEAVE_PLAYER_GROUP_DISTANCE)
						// only switch states if he's following you at the time
						IF sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
						OR sBailJumperPed.AI = BB_PED_AI_STATE_FOLLOW_PLAYER
							sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
							sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND") ENDIF #ENDIF
						ENDIF
						// Comment when the bail jumper is out of range of Trevor
						IF HAS_FRAME_COUNTER_PASSED(iFrameCountPlayerLastSeenBailJumper, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCountPlayerLastSeenBailJumper, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// only comment if Trev can't see him
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T9", CONV_PRIORITY_AMBIENT_HIGH)
										// Shit, where did he go?
										// What the fuck, where is he?
										// Fuck, did I just lose him?
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						bHasPlayerLeftBailJumperBehind = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS - Bail Jumper removed from Player group") ENDIF #ENDIF
					ELSE
						IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
							IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_COPS")
								CLEAR_THIS_PRINT("BBS_COPS")	//Lose the cops.
							ENDIF
							bDoneDialogue_BailJumperNoticesCops = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	// wait for current dialogue to finish first
								eSubStage = SS_CLEANUP
							ENDIF
						ELSE
							IF NOT bDoneDialogue_TrevorResponsesToBailJumperSurrendering
								IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sDialogueRoot_BailJumperSurrenders)
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T5", CONV_PRIORITY_AMBIENT_HIGH)
										// Random dialogue line from - 
										// Correct decision, you're coming with me now.
										// Damn right, come with me before I take your legs off.
										// You're my bitch, now heel.
										bDoneDialogue_TrevorResponsesToBailJumperSurrendering = TRUE
									ENDIF
								ENDIF
							ELIF NOT bDoneObjective_LoseWantedLevel						
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("BBS_COPS")
									PRINT_NOW("BBS_COPS", DEFAULT_GOD_TEXT_TIME, 0)	// Lose the cops.
									bDoneObjective_LoseWantedLevel = TRUE
								ELSE
									HANDLE_DIALOGUE_DURING_DROPOFF()
								ENDIF
							ELSE
								HANDLE_DIALOGUE_DURING_DROPOFF()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// check to see if the player has returned to the bail jumper
					IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, REJOIN_PLAYER_GROUP_DISTANCE)
						// only make him rejoin if he was in left behind state
						IF sBailJumperPed.AI = BB_PED_AI_SETUP_PLAYER_LEFT_BEHIND
						OR sBailJumperPed.AI = BB_PED_AI_STATE_PLAYER_LEFT_BEHIND
							sBailJumperPed.iAiDelayTimer = GET_GAME_TIMER()
							sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER") ENDIF #ENDIF
						ENDIF
						bDoneObjective_LoseWantedLevel = FALSE	// reset the lose cops objective
						IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")	
							CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
						ENDIF
						// Comment when the bail jumper is back in range of Trevor and he's returning to follow behaviour
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF sBailJumperPed.AI = BB_PED_AI_SETUP_FOLLOW_PLAYER
							OR sBailJumperPed.AI = BB_PED_AI_STATE_FOLLOW_PLAYER
								IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T10", CONV_PRIORITY_AMBIENT_HIGH)
										// Come back here bitch.
										// You sneaky fuck, get back here.
										// You trying to escape fucker?
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J5", CONV_PRIORITY_AMBIENT_HIGH)
										// I thought you'd had a change of heart?  
										// Are you serious? I'd assumed I was free to go.
										// Oh crap, you're back. I thought you'd let me go.  
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						bHasPlayerLeftBailJumperBehind = FALSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS - Bail Jumper added back into Player group") ENDIF #ENDIF
					ELSE
						IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_COPS")
							CLEAR_THIS_PRINT("BBS_COPS")	//Lose the cops.
						ENDIF
						IF NOT DOES_BLIP_EXIST(sBailJumperPed.blipIndex)
							sBailJumperPed.blipIndex = CREATE_PED_BLIP(sBailJumperPed.pedIndex, TRUE, TRUE)
						ENDIF
						IF NOT bDoneObjective_ReturnToBailJumper
							PRINT_NOW("BBS_05", DEFAULT_GOD_TEXT_TIME, 1)	// Return to the ~b~bail jumper.~s~
							bDoneObjective_ReturnToBailJumper = TRUE
						ENDIF
						// play any suitable dialogue here
					ENDIF
				ENDIF
				IF SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE(sBailJumperPed.pedIndex)
					bIsJumperRevertingBackToFlee = TRUE
					eSubStage = SS_CLEANUP				
				ENDIF
			BREAK		
			//	------------------------------------------
			CASE SS_CLEANUP
				IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_05")
					CLEAR_THIS_PRINT("BBS_05")	// Return to the ~b~bail jumper.~s~
				ENDIF
				bDoneDialogue_BailJumperNoticesCops = TRUE
				SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)
				
				// jumper is fleeing again
				IF bIsJumperRevertingBackToFlee
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS : ", "SS_CLEANUP -> MISSION_STAGE_TARGET_FLEES_AGAIN") ENDIF #ENDIF
					SET_STAGE(MISSION_STAGE_TARGET_FLEES_AGAIN)
				ELSE				
					IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_COPS")
						CLEAR_THIS_PRINT("BBS_COPS")	// Lose the cops.
					ENDIF					
					SET_STAGE(MISSION_STAGE_TAKE_TARGET_TO_MAUDE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_LOSE_THE_COPS - SS_CLEANUP done") ENDIF #ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    stage to handle target fleeing again (after he's already surrendered once)
PROC STAGE_TARGET_FLEES_AGAIN()
	
	BOOL bStopFleeingIfPlayerClose = FALSE
	
	CREATE_AND_SETUP_MAUDE()
	MANAGE_OUTRO_MOCAP_LOADING()
	
	IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
	
		VECTOR vBailJumperPos = GET_ENTITY_COORDS(sBailJumperPed.pedIndex, FALSE)
		
		UPDATE_PLAYER_CAN_SEE_PED_FRAME_COUNTER(sBailJumperPed.pedIndex, iFrameCountPlayerLastSeenBailJumper)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBailJumperPed.pedIndex, sBailJumperPed.iFrameCountLastSeenPlayer)
		UPDATE_BAIL_JUMPER_PED_AI()
		
		SWITCH eSubStage
			CASE SS_SETUP
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, HASH_BAILBOND_TARGET)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_BAILBOND_TARGET, RELGROUPHASH_PLAYER)
				
				iBailJumperHealthOnBeganFleeAgain = GET_ENTITY_HEALTH(sBailJumperPed.pedIndex)
				vPos_JumperBeganFleeAgain = GET_ENTITY_COORDS(sBailJumperPed.pedIndex, FALSE)				
				UPDATE_PROJECTICLE_FLEE_FROM_POSITION(vPos_ProjectileFleeingFrom, vPos_JumperBeganFleeAgain)
				
				PED_COMMON_SETUP(sBailJumperPed.pedIndex)	// set relationship group back up and other common stuff for flee				
				IF sBailJumperPed.AI != BB_PED_AI_SETUP_FLEE_ON_FOOT
				AND sBailJumperPed.AI != BB_PED_AI_STATE_FLEE_ON_FOOT
					CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
					sBailJumperPed.iAiDelayTimer = (GET_GAME_TIMER() - 3000)
					sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_SETUP - sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT") ENDIF #ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
					IF NOT DOES_BLIP_EXIST(sBailJumperPed.blipIndex)
						sBailJumperPed.blipIndex = CREATE_PED_BLIP(sBailJumperPed.pedIndex, TRUE, FALSE)
					ENDIF
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sBailJumperPed.pedIndex)
				ENDIF
				iDelay_SurrenderForPlayerCloseToWhileFleeing = -1	// reset
				iTime_BailJumperStartedFleeingOnFoot = -1 			// reset
				bHasBailJumperSurrendered = FALSE					// reset since this is also used in CHASE_TARGET stage
				bDoneDialogue_BailJumperSurrendered = FALSE			// reset since this is also used in CHASE_TARGET stage
				bDoneDialogue_BailJumperBeginsToFlee = FALSE		// reset since this is also used in CHASE_TARGET stage
				sDialogueRoot_BailJumperSurrenders = "BB1_J8"		// different dialogue for surrendering a second time
				iDialogueTimer = GET_GAME_TIMER()
				//SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_SETUP - done") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			BREAK		
			CASE SS_UPDATE
				IF NOT bHasBailJumperSurrendered
					FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sBailJumperPed.pedIndex)
					
					// Flee on foot it the player gets in the escape vehicle or it gets wrecked
					IF sBailJumperPed.AI = BB_PED_AI_SETUP_FLEE_ON_FOOT
					OR sBailJumperPed.AI = BB_PED_AI_STATE_FLEE_ON_FOOT
						IF iTime_BailJumperStartedFleeingOnFoot = -1	// if this hasn't been set yet, start the timer
							iTime_BailJumperStartedFleeingOnFoot = GET_GAME_TIMER()
						ENDIF
						IF iDelay_SurrenderForPlayerCloseToWhileFleeing = -1	// if this hasn't been set yet, start the timer
							iDelay_SurrenderForPlayerCloseToWhileFleeing = GET_GAME_TIMER()
						ELIF HAS_TIME_PASSED(iDelay_SurrenderForPlayerCloseToWhileFleeing, 8000)	// don't surrender if player is close by initially (give him chance to run)
							bStopFleeingIfPlayerClose = TRUE
						ENDIF
						IF SHOULD_PED_SURENDER_ONFOOT(sBailJumperPed.pedIndex, vPlayerPos, bStopFleeingIfPlayerClose, FALSE, TRUE, iBailJumperHealthOnBeganFleeAgain)
							// make sure he's left the area he's fleeing from first to ensure he's clear of projectiles
							IF HAS_TIME_PASSED(iTime_BailJumperStartedFleeingOnFoot, 5000)
							OR ARE_VECTORS_EQUAL(vPos_ProjectileFleeingFrom, << 0.0, 0.0, 0.0 >>)
							OR NOT IS_COORD_IN_RANGE_OF_COORD(vBailJumperPos, vPos_ProjectileFleeingFrom, 8.5)
							AND NOT IS_COORD_IN_RANGE_OF_COORD(vBailJumperPos, vPos_JumperBeganFleeAgain, 8.5)
								SET_BAIL_JUMPER_SURRENDERED()
								sBailJumperPed.iTimer = GET_GAME_TIMER()
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								bHasBailJumperSurrendered = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_UPDATE - surrender on foot") ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_UPDATE - detected reason to surrender but failed projectile clearance checks") ENDIF #ENDIF
							ENDIF
						ELSE
							IF NOT bDoneDialogue_BailJumperBeginsToFlee
								IF NOT HAS_TIME_PASSED(iDialogueTimer, 2500)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
										IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, DIST_LIMIT_PLAYER_TO_PED_FOR_DIALOGUE)
											IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_J7", CONV_PRIORITY_AMBIENT_HIGH)
												// RANDOM SELECTED LINE
												// Screw this, I'm out of here.
												// That's me gone, asshole!
												iDialogueTimer = GET_GAME_TIMER()
												bDoneDialogue_BailJumperBeginsToFlee = TRUE
												#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", " trigger bDoneDialogue_BailJumperBeginsToFlee : BB1_J7") ENDIF #ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									bDoneDialogue_BailJumperBeginsToFlee = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "bDoneDialogue_BailJumperBeginsToFlee skipped time out") ENDIF #ENDIF
								ENDIF
							ELSE
								// additional dialogue during on foot chase
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
									IF HAS_TIME_PASSED(iDialogueTimer, GET_RANDOM_INT_IN_RANGE(5000, 12000))								
										IF IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, DIST_LIMIT_PLAYER_TO_PED_FOR_DIALOGUE)
											IF IS_PED_FLEEING(sBailJumperPed.pedIndex)
												IF (GET_RANDOM_INT_IN_RANGE(0, 101) < 30)
													IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_T13", CONV_PRIORITY_AMBIENT_HIGH)
														// RANDOM SELECTED LINE
														// I'm going to catch you, fucker!
														// You can't out run a mad man!
														// Trevor's going to hunt you down!
														iDialogueTimer = GET_GAME_TIMER()
														#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_UPDATE - trigger on foot flee dialogue : BBC_T13") ENDIF #ENDIF
													ENDIF
												ELSE
													IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, "BB1_FLEE", CONV_PRIORITY_AMBIENT_HIGH)
														// RANDOM SELECTED LINE
														// I'm not going down!
														// I'm innocent!
														// You've got the wrong guy! 
														// Leave me alone!
														// I want to see some credentials!  
														iDialogueTimer = GET_GAME_TIMER()
														#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_UPDATE - trigger on foot flee dialogue : BB1_FLEE") ENDIF #ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									iDialogueTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
							UPDATE_PED_ON_FOOT_SPEED_FOR_CHASING_PLAYER(sBailJumperPed.pedIndex, vPlayerPos, iTime_BailJumperStartedFleeingOnFoot)
						ENDIF
					ELSE
						// no longer in flee behaviour
						SET_BAIL_JUMPER_SURRENDERED()
						sBailJumperPed.iTimer = GET_GAME_TIMER()
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						bHasBailJumperSurrendered = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_UPDATE - switch to surrender for not being in flee on foot AI") ENDIF #ENDIF
					ENDIF				
				ELSE
					SET_BAIL_JUMPER_SURRENDERED()
					
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sBailJumperPed.pedIndex, vPlayerPos, 10.0)
						IF NOT bDoneObjective_ApproachBailJumper
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sDialogueRoot_BailJumperSurrenders)
								INIT_FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APPSUS", "BBS_APPSUS2", iFlashBlipTimer, iFlashBlipGodTextTimer, TRUE, TRUE)	// Approach the ~r~bail jumper. // Approach the ~b~bail jumper.
								bDoneObjective_ApproachBailJumper = TRUE
							ENDIF
						ELSE
							FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "BBS_APPSUS", "BBS_APPSUS2", iFlashBlipTimer, iFlashBlipGodTextTimer)		// Approach the ~r~bail jumper. // Approach the ~b~bail jumper.
						ENDIF
					ELSE
						IF NOT bDoneDialogue_BailJumperSurrendered
							IF NOT IS_PED_RAGDOLL(sBailJumperPed.pedIndex)
							AND NOT IS_PED_IN_ANY_VEHICLE(sBailJumperPed.pedIndex)
								IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS")	// Approach the ~r~bail jumper.
								OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS2") // Approach the ~b~bail jumper.
									CLEAR_PRINTS()
								ENDIF
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sMissionTextBlock, sDialogueRoot_BailJumperSurrenders, CONV_PRIORITY_AMBIENT_HIGH) 
									// Okay, Okay I'm done.
									// Fuck, you got me.
									// Shit, I'm sorry that was stupid.
									bDoneDialogue_BailJumperSurrendered = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "bDoneDialogue_BailJumperSurrendered - done") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
						FLASH_BLIP_AND_TEXT(sBailJumperPed.blipIndex, "", "", iFlashBlipTimer, iFlashBlipGodTextTimer, FALSE)
					ENDIF
					IF SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE(sBailJumperPed.pedIndex)
						bIsJumperRevertingBackToFlee = TRUE
						eSubStage = SS_SETUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", " SS_UPDATE -> SS_SETUP reason to revert to flee yet again!") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE SS_CLEANUP
				IF IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS")	// Approach the ~r~bail jumper.
				OR IS_THIS_PRINT_BEING_DISPLAYED("BBS_APPSUS2") // Approach the ~b~bail jumper.
					CLEAR_PRINTS()
				ENDIF
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
				bDoneDialogue_BailJumperSurrendered = TRUE // mark this up as completed as it is used to stop Bail jumper switching to follow behaviour
				SET_BAIL_JUMPER_SURRENDERED()
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, HASH_BAILBOND_TARGET)
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_BAILBOND_TARGET, RELGROUPHASH_PLAYER)
				SAFE_REMOVE_BLIP(sBailJumperPed.blipIndex)
				bDoneDialogue_BailJumperSurrendered = TRUE // mark this up as completed as it is used to stop Bail jumper switching to follow behaviour
				sBailJumperPed.iTimer = GET_GAME_TIMER()
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)	
				bHasPlayerLeftBailJumperBehind = FALSE	// reset
				bIsJumperRevertingBackToFlee = FALSE	// reset
				SET_STAGE(MISSION_STAGE_TAKE_TARGET_TO_MAUDE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_TARGET_FLEES_AGAIN : ", "SS_CLEANUP - done") ENDIF #ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Trevor delivers the bail jumper to Maude
PROC STAGE_CUTSCENE_MAUDE_TAKES_TARGET()

	SWITCH eSubStage
		CASE SS_SETUP
			RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
			SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			
			IF CREATE_AND_SETUP_MAUDE()
				
				IF RC_IS_CUTSCENE_OK_TO_START(TRUE, DEFAULT_VEH_STOPPING_DISTANCE, TRUE)
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Trevor, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					IF IS_ENTITY_ALIVE(sMaude.index)
						REGISTER_ENTITY_FOR_CUTSCENE(sMaude.index, sSceneHandle_Maude, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_ENTITY_ALIVE(sBailJumperPed.pedIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(sBailJumperPed.pedIndex, sSceneHandle_BailJumper, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF					
					IF IS_ENTITY_ALIVE(sObjMaudeChair.index)
						REGISTER_ENTITY_FOR_CUTSCENE(sObjMaudeChair.index, sSceneHandle_MaudeChair, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_ENTITY_ALIVE(sObjMaudeLaptop.index)
						REGISTER_ENTITY_FOR_CUTSCENE(sObjMaudeLaptop.index, sSceneHandle_MaudeLaptop, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					START_CUTSCENE()                     
					WAIT(0)    //needed because cutscene doesn't start straight away, causing player to see vehicles getting removed.
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// //B* 1468238 - when skipping to mocap (let the mocap stage handle fade in) tried just before START_CUTSCENE but issue still occured
					RC_START_CUTSCENE_MODE(<< 2727.58, 4144.19, 43.95 >>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE) //has to be after start_cutscene and wait(0)
					
					VECTOR vResolveArea_Pos1, vResolveArea_Pos2, vVehRespotPosSizeLimit
					FLOAT fResolveArea_Width
					GET_RESOLVE_VEHICLES_AT_MAUDE_ANGLED_AREA_VALUES(vResolveArea_Pos1, vResolveArea_Pos2, fResolveArea_Width)
					vVehRespotPosSizeLimit = GET_DEFAULT_VEHICLE_SIZE_LIMIT_AT_MAUDES()	 //GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()

					// set the vehicle the player triggered the mission in to regenerate and clear the area - use alternative location for bigger vehicles
					IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(vVehRespotPosSizeLimit, FALSE)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vResolveArea_Pos1, vResolveArea_Pos2, fResolveArea_Width, << 2721.02, 4140.34, 43.66 >>, 257.67)
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<< 2721.02, 4140.34, 43.66 >>, 257.67, FALSE)
						CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - resolved vehicles in area and setup standard vehicle gen - framecount : ", GET_FRAME_COUNT())
					ELSE
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vResolveArea_Pos1, vResolveArea_Pos2, fResolveArea_Width, <<2710.6646, 4149.0752, 42.7026>>, 180.9488)
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<2710.6646, 4149.0752, 42.7026>>, 180.9488, FALSE)
						CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - resolved vehicles in area and setup large vehicle gen -  framecount : ", GET_FRAME_COUNT())
					ENDIF
					
					// temp until bail jumper is added to the cutscene
					IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
						IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
							REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
						ENDIF
						CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
						SET_ENTITY_COORDS(sBailJumperPed.pedIndex, <<2724.02, 4145.08, 43.29>>)
						SET_ENTITY_HEADING(sBailJumperPed.pedIndex, -84.20)	
						CLEAR_PED_TASKS(sBailJumperPed.pedIndex)
					ENDIF
					
					eSubStage = SS_UPDATE
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - CUTSCENE_STAGE_SETUP done framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		BREAK		
		CASE SS_UPDATE
		
			// set exit state for Maude on her chair
			IF IS_ENTITY_ALIVE(sObjMaudeChair.index)
				IF IS_PED_UNINJURED(sMaude.index)
					// Set exit state for Maude
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Maude)
					AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_MaudeChair)
						SET_MAUDE_SYNC_SCENE()
						CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : STAGE_INTRO_MOCAP_SCENE - CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY - sMaude.index and sSceneHandle_MaudeChair - FC = ", GET_FRAME_COUNT())
					ENDIF
				ENDIF
			ENDIF
			
			// Set exit state for Trevor
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Trevor)
				// B*1478470 - mocap got shortened caused blend out issue so respoting Trev to safe coords
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2724.1929, 4145.3057, 42.8324>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 76.4896)
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 2000)				
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - SS_UPDATE done - set exit state for Trevor framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - SS_UPDATE - Camera - set camera exit state in mocap Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				
				eSubStage = SS_CLEANUP
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - SS_UPDATE done framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK		
		CASE SS_CLEANUP
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ELSE
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ENDIF
				IF bFinishedStageSkipping	//only do this if we aren't stage skipping
					RC_END_CUTSCENE_MODE()					
				ENDIF

				IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
					IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
						REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
					ENDIF
				ENDIF
				SAFE_REMOVE_PED(sBailJumperPed.pedIndex, TRUE)
				
				IF IS_PED_UNINJURED(sMaude.index)
					TASK_LOOK_AT_ENTITY(sMaude.index, PLAYER_PED_ID(), -1)
					SET_PED_KEEP_TASK(sMaude.index, TRUE)	
				ENDIF
				SAFE_RELEASE_PED(sMaude.index)
			
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_CUTSCENE_MAUDE_TAKES_TARGET - SS_CLEANUP done framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				
				SET_STAGE(MISSION_STAGE_MISSION_PASSED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    mini stage to handle player killing bail jumper, trigger dialogue and setting post mission script running
PROC STAGE_MISSION_OVER_BAIL_JUMPER_KILLED()
			
	SWITCH eSubStage
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF			
			DELETE_ALL_MISSION_BLIPS()

			SAFE_REMOVE_PED(sMaude.index, TRUE)	// ensure Maude isn't in the world if we are passing early (if she sees player kill jumper it will fail)
			
			INT i
			FOR i=0 TO (iNumTargetBackupPeds - 1)
				//GIVE_PED_FLEE_ORDER(sBackupPed[i].index)
				PICK_BUDDY_REACTION(i)
			ENDFOR
			
			iTimer_DelayMissionOverBailJumperKilled = GET_GAME_TIMER()	//set timer to advance to mission over if dialogue doesn't trigger or post script doesn't launch quick enough
			
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_MISSION_OVER_BAIL_JUMPER_KILLED : SS_SETUP done framecount : ", GET_FRAME_COUNT())
		BREAK		
		CASE SS_UPDATE

			IF HAS_TIME_PASSED(iTimer_DelayMissionOverBailJumperKilled, 500)
			
				// trigger bail jumper killed dialogue
				IF NOT bDoneDialogue_BailJumperKilled				
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF	
					IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, "BBC_PK1", CONV_PRIORITY_AMBIENT_HIGH)
						// Fucker is down. Maude will understand.
						// Man down! Maude won't be getting this guy.
						// I don't think Maude would appreciate him in a body bag.
						// That got the bastard.  I won't be dropping him off at Maude's then.
						bDoneDialogue_BailJumperKilled = TRUE
						CPRINTLN(DEBUG_BAIL_BOND, "STAGE_MISSION_OVER_BAIL_JUMPER_KILLED - dialogue triggered BBC_PK1")
					ELSE
						CPRINTLN(DEBUG_BAIL_BOND, "STAGE_MISSION_OVER_BAIL_JUMPER_KILLED - dialogue failed to trigger BBC_PK1 framecount : ", GET_FRAME_COUNT())
					ENDIF
				ENDIF
			
				// check to see if we can head to mission over
				IF bDoneDialogue_BailJumperKilled
					SCRIPT_PASSED(BB_PASSED_TARGET_KILLED)
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_MISSION_OVER_BAIL_JUMPER_KILLED : SS_UPDATE done SCRIPT_PASSED called framecount : ", GET_FRAME_COUNT())
				ENDIF
				
				// fail safe if conversation or script launch takes too long
				IF HAS_TIME_PASSED(iTimer_DelayMissionOverBailJumperKilled, 1500)
					SCRIPT_PASSED(BB_PASSED_TARGET_KILLED)
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_MISSION_OVER_BAIL_JUMPER_KILLED : SS_UPDATE done SCRIPT_PASSED called iTimer_DelayMissionOverBailJumperKilled timed out framecount : ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC STAGE_FAILED_WAIT_FOR_FADE()
	STRING sFailReason = NULL	
	INT i
	SWITCH eSubStage
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF			
			DELETE_ALL_MISSION_BLIPS()
			
			eBailBondMissionOverState = BBMOS_FAILED
			
			//setup mission failed dialogue if needed			
			IF eBB_MissionFailedReason = BB_FAILED_MAUDE_ATTACKED				
				// flee comment from Maude
				IF IS_PED_UNINJURED(sMaude.index)
					REMOVE_PED_FOR_DIALOGUE(sDialogue, MAUDE_SPEAKER_ID)	// leave area comments - remove Maude
					MAKE_PED_SCREAM(sMaude.index, FALSE)
					IF NOT IS_AMBIENT_SPEECH_PLAYING(sMaude.index)
						IF IS_ENTITY_IN_RANGE_COORDS(sMaude.index, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 35.0)
							STOP_PED_SPEAKING(sMaude.index, FALSE)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(sMaude.index, "GENERIC_FRIGHTENED_HIGH", "MAUDE", SPEECH_PARAMS_FORCE) 	// GENERIC_SHOCKED_HIGH
							CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : TRIGGER_MAUDE_FLEE_DIALOGUE_LINE - requested to trigger ambient speech line GENERIC_FRIGHTENED_HIGH")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : TRIGGER_MAUDE_FLEE_DIALOGUE_LINE - failed already ambient Maude speech playing")
					ENDIF
				ENDIF
				// ensure these aren't frozen for flee
				IF DOES_ENTITY_EXIST(sObjMaudeLaptop.index)
					FREEZE_ENTITY_POSITION(sObjMaudeLaptop.index, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sObjMaudeTable.index)
					FREEZE_ENTITY_POSITION(sObjMaudeTable.index, FALSE)
				ENDIF
				REQUEST_ANIM_DICT(GET_MAUDE_REACT_ANIM_DICT())	// begin request for flee anim
				IF SET_MAUDE_PLAY_FLEE_EXIT_ANIM(sMaude.index, sObjMaudeChair.index, iSyncScene_MaudeReact)
					bSetMaudeFleeSyncSceneExit = TRUE
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_FAILED_WAIT_FOR_FADE : maude exit sync scene done")
				ENDIF
				
			ELIF eBB_MissionFailedReason = BB_FAILED_BAIL_JUMPER_ESCAPED
				bDoneDialogue_MissionFailed = FALSE
				// Fuck, I lost him.
				// Shit, I lost him.
				tlDialogueRoot_MissionFailed = "BBC_T12"
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE - set fail dialogue for Maude attacked root: BBC_T12") ENDIF #ENDIF
			ENDIF
			
			IF IS_PED_UNINJURED(sBailJumperPed.pedIndex)
				IF IS_PED_IN_GROUP(sBailJumperPed.pedIndex)
					REMOVE_PED_FROM_GROUP(sBailJumperPed.pedIndex)
				ENDIF
				GIVE_PED_FLEE_ORDER(sBailJumperPed.pedIndex)	
			ENDIF
			FOR i = 0 TO (iNumTargetBackupPeds - 1)
				GIVE_PED_FLEE_ORDER(sBackupPed[i].index)
			ENDFOR
			
			// set the fail reason
			SWITCH eBB_MissionFailedReason
				// shouldn't come in here if this was the fail reason
				CASE BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER
					SCRIPT_ASSERT("BAILBOND1 : invalid fail reason BB_FAILED_DIST_CLEANUP_BEFORE_ENCOUNTER in STAGE_FAILED_WAIT_FOR_FADE")
				BREAK
				CASE BB_FAILED_BAIL_JUMPER_ESCAPED
					sFailReason = "BBS_F1"		// ~w~The bail jumper escaped.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE string set : BB_FAILED_BAIL_JUMPER_ESCAPED") ENDIF #ENDIF
				BREAK
				CASE BB_FAILED_MAUDE_DIED	
					sFailReason = "BBS_F3"		// ~w~Maude died.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE string set : BB_FAILED_MAUDE_DIED") ENDIF #ENDIF
				BREAK
				CASE BB_FAILED_MAUDE_ATTACKED		
					sFailReason = "BBS_F2"		// ~w~Maude was spooked.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE string set : BB_FAILED_MAUDE_ATTACKED") ENDIF #ENDIF
				BREAK
				CASE BB_FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE string set : BB_FAILED_DEFAULT") ENDIF #ENDIF
				BREAK
			ENDSWITCH

			//Check if fail reason needs to be displayed
			IF eBB_MissionFailedReason != BB_FAILED_DEFAULT
				MISSION_FLOW_SET_FAIL_REASON(sFailReason)
			ENDIF
			
			BAILBOND_FAILED(0)

			eSubStage = SS_UPDATE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE - SS_SETUP done") ENDIF #ENDIF
		BREAK

		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//---- Do any specific cleanup here----
				CLEANUP_ALL_MISSION_ENTITIES(TRUE)	
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE - SS_UPDATE done") ENDIF #ENDIF
				Script_Cleanup()	
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
				IF NOT bDoneDialogue_MissionFailed
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF eBB_MissionFailedReason = BB_FAILED_MAUDE_ATTACKED
						IF IS_PED_UNINJURED(sMaude.index)
							IF IS_ENTITY_IN_RANGE_COORDS(sMaude.index, vPlayerPos, 35.0)
								ADD_PED_FOR_DIALOGUE(sDialogue, MAUDE_SPEAKER_ID, sMaude.index, "MAUDE")
								IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, tlDialogueRoot_MissionFailed, CONV_PRIORITY_AMBIENT_HIGH)
									bDoneDialogue_MissionFailed = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE - bDoneDialogue_MissionFailed - true") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sCommonTextBlock, tlDialogueRoot_MissionFailed, CONV_PRIORITY_AMBIENT_HIGH)
							bDoneDialogue_MissionFailed = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "STAGE_FAILED_WAIT_FOR_FADE - bDoneDialogue_MissionFailed - true") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				// Handle Maude's flee exit
				IF eBB_MissionFailedReason = BB_FAILED_MAUDE_ATTACKED
					IF NOT bSetMaudeFleeSyncSceneExit
						REQUEST_ANIM_DICT(GET_MAUDE_REACT_ANIM_DICT())	// begin request for flee anim
						IF SET_MAUDE_PLAY_FLEE_EXIT_ANIM(sMaude.index, sObjMaudeChair.index, iSyncScene_MaudeReact)
							bSetMaudeFleeSyncSceneExit = TRUE
							CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_FAILED_WAIT_FOR_FADE : maude exit sync scene done")
						ENDIF
					ELSE
						IF IS_PED_UNINJURED(sMaude.index)	
							IF NOT IsPedPerformingTask(sMaude.index, SCRIPT_TASK_SMART_FLEE_PED)
								IF NOT IS_PED_FLEEING(sMaude.index)
									IF NOT IS_ENTITY_PLAYING_ANIM(sMaude.index, GET_MAUDE_REACT_ANIM_DICT(), GET_MAUDE_REACT_ANIM())												
										SET_MAUDE_FLEE_ATTRIBUTES(sMaude.index)										
										TASK_SMART_FLEE_PED(sMaude.index, PLAYER_PED_ID(), 50, -1)	// keep small to inprove latency path finding issue (should be faded by dist reached)
										SET_PED_KEEP_TASK(sMaude.index, TRUE)			// Keep scripted task
										CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : STAGE_FAILED_WAIT_FOR_FADE : maude flee task applied")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

SCRIPT(BAIL_BOND_LAUNCH_DATA launcherBailBondData)

	//Grab handle to the mission data set by the launcher
	sBailBondLaunchData = launcherBailBondData
	
	// Setup callback when player is killed, arrested , goes to multiplayer or random events force cleanup
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		
		// cleanup silently without fade if player was unaware of the bail jumper mission
		IF eMissionStage = MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : FORCE CLEANUP : mission stage not detected bail jumpr. frame count : ", GET_FRAME_COUNT())
			// set the mission over state for the launcher to use
			eBailBondMissionOverState = BBMOS_CLEANUP
	        Script_Cleanup()		
		ELSE
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : FORCE CLEANUP : frame count : ", GET_FRAME_COUNT())
			// set the mission over state for the launcher to use
			eBailBondMissionOverState = BBMOS_FAILED
			BAILBOND_FAILED(0)
	        Script_Cleanup()
		ENDIF
	ENDIF
	
	// Check the random event is allowed to run?
	IF NOT IS_BAIL_BOND_SAFE_TO_LAUNCH(TRUE)
		CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : CLEANUP - random event not allowed to run : frame count : ", GET_FRAME_COUNT())
		// set the mission over state for the launcher to use
		eBailBondMissionOverState = BBMOS_CLEANUP
        Script_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	INIT_MISSION()
	
	IF IS_REPLAY_IN_PROGRESS()	// handle replay checkpoints
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()

		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF
		SWITCH iReplayStage		
			CASE CP_BB1_TRIGGER_BAIL_JUMPER_REACTION
				// B*1526934 - heli needs different pos plus allow heli flag
				IF IS_THIS_MODEL_A_HELI(g_stageSnapshot.mVehicleStruct.mVehicle.eModel)
				OR IS_THIS_MODEL_A_PLANE(g_stageSnapshot.mVehicleStruct.mVehicle.eModel)
					CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, <<2898.2944, 2796.3049, 53.8490>>, 162.6353, FALSE, FALSE, TRUE, FALSE, FALSE)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, <<2891.4587, 2798.1245, 53.7482>>, 206.3925, FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
				START_REPLAY_SETUP(vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//setup player in spot for mission start
				ePlayerSpotsBailJumperType = BB_PSBJT_PLAYER_SPOTS_BAIL_JUMPER_LONG_DISTANCE
				DO_Z_SKIP(Z_SKIP_TRIGGER_BAIL_JUMPER_REACTION)	 // skip the mocap intro
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "start chase in progress")
			BREAK	
			CASE CP_BB1_MISSION_PASSED
				CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, <<2704.0776, 4152.2397, 42.2514>>, 175.1930, FALSE, FALSE, FALSE, FALSE, FALSE)	// Across from Maude's
				START_REPLAY_SETUP(vMaudePlace, 254.9762, FALSE)	// match position for kicking off outro mocap
				DO_Z_SKIP(Z_SKIP_OUTRO_CUTSCENE)	 
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "mission passed in progress (play outro mocap first")
			BREAK
			DEFAULT
				SCRIPT_ASSERT("BAILBOND1 :  Replay checkpoint * - starting mission from invalid checkpoint")
			BREAK
		ENDSWITCH
	ELIF IS_REPEAT_PLAY_ACTIVE()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//setup player in spot which matches start of the mission
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			CPRINTLN(DEBUG_BAIL_BOND, "IS_REPEAT_PLAY_ACTIVE -  set player coords for repeat play ", vPos_PlayerMissionStart, " framecount : ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())

			// Fix B*1121745 - swap over to call everyframe commands
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
			MISSION_FAILED_CHECKS()
			CHECK_BAIL_JUMPER_KILLED()
			
			SWITCH eMissionStage			
				CASE MISSION_STAGE_PLAYER_LOCATE_BAIL_JUMPER
					STAGE_PLAYER_LOCATE_BAIL_JUMPER()
				BREAK	
				
				CASE MISSION_STAGE_TRIGGER_BAIL_JUMPER_REACTION	
					STAGE_TRIGGER_BAIL_JUMPER_REACTION()
				BREAK	
				
				CASE MISSION_STAGE_CHASE						
					STAGE_CHASE()
				BREAK	
				
				CASE MISSION_STAGE_TAKE_TARGET_TO_MAUDE
					STAGE_TAKE_TARGET_TO_MAUDE()
				BREAK
				
				CASE MISSION_STAGE_LOSE_THE_COPS
					STAGE_LOSE_THE_COPS()
				BREAK
				
				CASE MISSION_STAGE_TARGET_FLEES_AGAIN
					STAGE_TARGET_FLEES_AGAIN()
				BREAK
				
				CASE MISSION_STAGE_CUTSCENE_MAUDE_TAKES_TARGET
					STAGE_CUTSCENE_MAUDE_TAKES_TARGET()
				BREAK
				
				CASE MISSION_STAGE_MISSION_PASSED
					SCRIPT_PASSED(BB_PASSED_TARGET_CAPTURED)
				BREAK
				
				CASE MISSION_STAGE_MISSION_OVER_BAIL_JUMPER_KILLED
					STAGE_MISSION_OVER_BAIL_JUMPER_KILLED()
				BREAK
				
				CASE MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
					STAGE_FAILED_WAIT_FOR_FADE()
				BREAK

			ENDSWITCH		
			
			// if we are skipping through the mission stages, for a checkpoint / debug skip		
			IF bFinishedStageSkipping = FALSE		
				JUMP_TO_STAGE(eMissionSkipTargetStage)
			ENDIF
		
			#IF IS_DEBUG_BUILD
				MAINTAIN_MISSION_WIDGETS()
				IF bFinishedStageSkipping = TRUE		// not skipping stages, check for debug keys as long as we aren't in fail state	
					IF eMissionStage <> MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
						DEBUG_Check_Debug_Keys()	// Check debug completion/failure
					ENDIF
				ENDIF
			#ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
