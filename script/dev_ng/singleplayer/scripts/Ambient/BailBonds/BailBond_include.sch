USING "rage_builtins.sch"
USING "globals.sch"


USING "BailBond_common.sch"
USING "CompletionPercentage_public.sch"

USING "RC_helper_functions.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
	BOOL bDebug_PrintBailBondInfoToTTY		= TRUE
#ENDIF

/// PURPOSE: used to update a ped's movement state - for use with detection system / travel
ENUM BB_PED_MOVEMENT_STATE
	BB_PEDMOVEMENTSTATE_COVER,
	BB_PEDMOVEMENTSTATE_STEALTH,
	BB_PEDMOVEMENTSTATE_RUNNING,
	BB_PEDMOVEMENTSTATE_SPRINTING,
	BB_PEDMOVEMENTSTATE_WALKING,
	BB_PEDMOVEMENTSTATE_IN_VEHICLE,
	BB_PEDMOVEMENTSTATE_DEFAULT
ENDENUM

/// PURPOSE:
///    remove object from the game
/// PARAMS:
///    objectIndex - object to remove
///    bDelete - if true delete it, else set as no longer needed
PROC SAFE_REMOVE_OBJECT(OBJECT_INDEX &objectIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_OBJECT(objectIndex)
	ELSE
		SAFE_RELEASE_OBJECT(objectIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes vehicle from the world
/// PARAMS:
///    vehIndex - vehicle to be removed
///    bDelete - if true AND player isn't inside it delete it, else set as no longer needed
PROC SAFE_REMOVE_VEHICLE(VEHICLE_INDEX &vehIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_VEHICLE(vehIndex)
	ELSE
		SAFE_RELEASE_VEHICLE(vehIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes ped from the game
/// PARAMS:
///    ped - ped to be removed
///    bDelete - if true delete it, else set as no longer needed
///    bDetach - if true ped will be detached if attached, and no in a vehicle or getting into a vehicle
PROC SAFE_REMOVE_PED(PED_INDEX &ped, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_PED(ped)
	ELSE
		SAFE_RELEASE_PED(ped)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a ped to a new location
/// PARAMS:
///    pedIndex - ped to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the ped at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
///    bForcePedAiAndAnimUpdate - if true FORCE_PED_AI_AND_ANIMATION_UPDATE is applied on the ped
PROC SAFE_TELEPORT_PED(PED_INDEX pedIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bForcePedAiAndAnimUpdate = FALSE)
	SAFE_TELEPORT_ENTITY(pedIndex, vPosition, fHeading, bSnapToGround)
	IF bForcePedAiAndAnimUpdate
		IF IS_ENTITY_ALIVE(pedIndex)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    get the default vehicle size limit for respotting the player's vehicle to the
///    large vehicle position
/// RETURNS:
///    VECTOR
FUNC VECTOR GET_DEFAULT_VEHICLE_SIZE_LIMIT_AT_MAUDES()
	RETURN << 3.01, 11.01, 6.01 >>
ENDFUNC

/// PURPOSE:
///    the normal clipset for the bail jumper to use when surrendered
/// RETURNS:
///    STRING value
FUNC STRING BAIL_JUMPER_MOVEMENT_CLIPSET_NORMAL()
	RETURN "move_m@bail_bond_not_tazered"
ENDFUNC

/// PURPOSE:
///    the normal clipset for the bail jumper to use when surrendered after being tazered
/// RETURNS:
///    STRING value
FUNC STRING BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED()
	RETURN "move_m@bail_bond_tazered"
ENDFUNC

/// PURPOSE:
///    requests and checks if the bail jumper's movements clipsets are loaded
/// RETURNS:
///    TRUE if clipsets are loaded
FUNC BOOL HAS_BAIL_JUMPER_SURRENDER_MOVEMENT_CLIPSETS_LOADED()

	REQUEST_ANIM_SET(BAIL_JUMPER_MOVEMENT_CLIPSET_NORMAL())
	REQUEST_ANIM_SET(BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED())
	
	IF HAS_CLIP_SET_LOADED(BAIL_JUMPER_MOVEMENT_CLIPSET_NORMAL())
	AND HAS_CLIP_SET_LOADED(BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Releases the clips sets
PROC RELEASE_BAIL_JUMPER_SURRENDER_MOVEMENT_CLIPSETS()

	REMOVE_ANIM_SET(BAIL_JUMPER_MOVEMENT_CLIPSET_NORMAL())
	REMOVE_ANIM_SET(BAIL_JUMPER_MOVEMENT_CLIPSET_TAZERED())		
ENDPROC

/// PURPOSE:
///    just monitors the ped for IS_PED_BEING_STUNNED and will set bReturnHasJumperBeenTazered 
///    NOTE: doesn't safe check the ped
/// PARAMS:
///   pedIndex -  ped to test
///   bReturnHasJumperBeenTazered - returned status
PROC UPDATE_PED_TAZERED_STATUS(PED_INDEX &pedIndex, BOOL &bReturnHasJumperBeenTazered)

	IF IS_PED_BEING_STUNNED(pedIndex)
		bReturnHasJumperBeenTazered = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PED_TAZERED_STATUS - tazered ped detected") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a vehicle to a new location
/// PARAMS:
///    vehIndex - vehicle to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the vehicle at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
PROC SAFE_TELEPORT_VEHICLE(VEHICLE_INDEX &vehIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bKeepTasks = FALSE)
	IF IS_ENTITY_ALIVE(vehIndex)
		IF bSnapToGround = TRUE
			BOOL groundCheckOk = FALSE
			FLOAT fNewZ = 0.0
			groundCheckOk = GET_GROUND_Z_FOR_3D_COORD(vPosition, fNewZ)			
			IF (groundCheckOk)
				vPosition.z = fNewZ
			ENDIF
		ENDIF
		SET_ENTITY_COORDS(vehIndex, vPosition, FALSE, bKeepTasks)
		SET_ENTITY_HEADING(vehIndex, fHeading)
		IF bSnapToGround
			SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleports a ped out of their vehicle.
///    NOTE: proc waits until GET_SAFE_COORD_FOR_PED returns true
/// PARAMS:
///    pedIndex - ped to Teleport
PROC SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PED_INDEX pedIndex)
	VECTOR vRespotCoords = << 0.0, 0.0, 0.0 >>
	FLOAT fRespotHeading = 0.0	
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_IN_ANY_VEHICLE(pedIndex)				
			vRespotCoords = GET_ENTITY_COORDS(pedIndex)			
			WHILE NOT GET_SAFE_COORD_FOR_PED(vRespotCoords, FALSE, vRespotCoords)
				WAIT(0)
				vRespotCoords.x += 2.0
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE - looking for safe coords") ENDIF #ENDIF
			ENDWHILE			
			fRespotHeading = GET_ENTITY_HEADING(pedIndex)			
			SAFE_TELEPORT_PED(pedIndex, vRespotCoords, fRespotHeading, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Compares two headings
/// PARAMS:
///    f1 - first heading
///    f2 - second heading
///    fTolerance - max difference allowed in the headings
/// RETURNS:
///    TRUE if the difference between the headings is less than or equal to the tolerance
FUNC BOOL IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(FLOAT f1, FLOAT f2, FLOAT fTolerance)
	IF (ABSF(f1 - f2) <= fTolerance)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE - test returned true")
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
///    and are within the z threshold
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
///    fThresholdZ - max z different between the two positions
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D_WITH_DIFFERENT_Z_THRESHOLD(VECTOR v1, VECTOR v2, FLOAT fRange, FLOAT fThresholdZ)
	VECTOR vDiff = v2 - v1
	IF ((vDiff.z * vDiff.z) <= (fThresholdZ * fThresholdZ))
		IF ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Test for if two coords are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN VDIST2(v1, v2) <= (fRange*fRange)
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    e1 - 
///    e2 - 
///    tolerance - 
/// RETURNS:
///    
FUNC BOOL ARE_ENTITYS_AT_SAME_HEIGHT(ENTITY_INDEX e1, ENTITY_INDEX e2, FLOAT tolerance = 6.0)
	VECTOR vE1 = GET_ENTITY_COORDS(e1, FALSE)
	VECTOR vE2 = GET_ENTITY_COORDS(e2, FALSE)
	FLOAT fDiff = ABSF(vE1.z - vE2.z)
	IF fDiff <= tolerance
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    e1 - 
///    e2 - 
///    tolerance - 
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_AT_SAME_HEIGHT_AS_PED(PED_INDEX pedIndex, VECTOR vPlayerPosition, FLOAT tolerance = 6.0)
	VECTOR vPedPos = GET_ENTITY_COORDS(pedIndex, FALSE)
	FLOAT fDiff = ABSF(vPedPos.z - vPlayerPosition.z)
	IF fDiff <= tolerance
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    get the min vector for the mission's clear area
///    all element (X, Y, Z) must be smaller than the max vector defined in GET_MISSION_CLEAR_AREA_MAX_VECTOR
/// PARAMS:
///    sLaunchData - the data grabbed from the launch, specifying which bail bond it is
/// RETURNS:
///    the min vector
FUNC VECTOR GET_MISSION_CLEAR_AREA_MIN_VECTOR(BAIL_BOND_LAUNCH_DATA sLaunchData)
	SWITCH sLaunchData.eBailBondID	
		CASE BBI_QUARRY
			RETURN <<2929.6653, 2782.2190, 34.8822>>		
		CASE BBI_FARM
			RETURN <<145.6927, 2266.8862, 70.4983>>
		CASE BBI_MOUNTAIN
			RETURN <<491.7797, 5485.7061, 736.4918>>
		CASE BBI_HOBO
			RETURN <<1414.2113, 6326.6069, 20.8036>>
	ENDSWITCH
	CPRINTLN(DEBUG_BAIL_BOND, "GET_MISSION_CLEAR_AREA_MIN_VECTOR - error! invalid bail bond ID")
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

/// PURPOSE:
///    get the max vector for the mission's clear area
///    all element (X, Y, Z) must be larger than the min vector defined in GET_MISSION_CLEAR_AREA_MIN_VECTOR
/// PARAMS:
///    sLaunchData - the data grabbed from the launch, specifying which bail bond it is
/// RETURNS:
///    the max vector
FUNC VECTOR GET_MISSION_CLEAR_AREA_MAX_VECTOR(BAIL_BOND_LAUNCH_DATA sLaunchData)
	SWITCH sLaunchData.eBailBondID		
		CASE BBI_QUARRY
			RETURN <<2956.7854, 2824.5942, 45.1931>>		
		CASE BBI_FARM
			RETURN <<197.6706, 2333.7725, 105.6470>>
		CASE BBI_MOUNTAIN
			RETURN <<535.9079, 5518.3228, 772.5947>>
		CASE BBI_HOBO
			RETURN <<1446.7683, 6361.9297, 26.9007>>	// <<1469.02942, 6376.20801, 31.0>>	if i have to clear all the camp but don't want to do that!
	ENDSWITCH
	CPRINTLN(DEBUG_BAIL_BOND, "GET_MISSION_CLEAR_AREA_MAX_VECTOR - error! invalid bail bond ID")
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

/// PURPOSE:
///    get resolve vehicles at Maudes angled area
/// PARAMS:
///    vPos 1 - 
///    vPos 2 - 
///    fWidth
/// RETURNS:
///    TRUE once values have been set
FUNC BOOL GET_RESOLVE_VEHICLES_AT_MAUDE_ANGLED_AREA_VALUES(VECTOR &vPos1, VECTOR &vPos2, FLOAT &fWidth)
	vPos1 = <<2728.995117,4143.165039,41.029861>>
	vPos2 = <<2719.125977,4144.691406,47.885429>>
	fWidth = 13.0
	CPRINTLN(DEBUG_BAIL_BOND, "GET_RESOLVE_VEHICLES_AT_MAUDE_ANGLED_AREA_VALUES - done")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    get the position to play Maude's sync scene at
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_SYNC_SCENE_POSITION()
	RETURN << 2727.40, 4145.56, 43.68 >>
ENDFUNC

/// PURPOSE:
///    get the orientation to play Maude's sync scene at
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_SYNC_SCENE_ORIENTATION()
	RETURN << 0.0, 0.0, -92.17 >>
ENDFUNC

/// PURPOSE:
///    get the position to spawn Maude
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_SPAWN_POSITION()
	RETURN << 2728.33, 4145.60, 43.89 >>
ENDFUNC

/// PURPOSE:
///    get the heading to spawn Maude
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    FLOAT the heading
FUNC FLOAT GET_MAUDE_SPAWN_HEADING()
	RETURN 89.19
ENDFUNC

/// PURPOSE:
///    get the position to spawn Maude's laptop
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_LAPTOP_POSITION()
	RETURN <<2727.686035,4145.714844,44.080002>>
ENDFUNC

/// PURPOSE:
///    get the heading to spawn Maude's laptop
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    FLOAT the heading
FUNC FLOAT GET_MAUDE_LAPTOP_HEADING()
	RETURN 71.000000
ENDFUNC

/// PURPOSE:
///    get the model to spawn Maude's laptop
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_MAUDE_LAPTOP_MODEL()
	RETURN Prop_Laptop_01a
ENDFUNC

/// PURPOSE:
///    get the position to spawn Maude's chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_CHAIR_POSITION()
	RETURN << 2728.35, 4145.59, 43.30 >>
ENDFUNC

/// PURPOSE:
///    get the heading to spawn Maude's chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    FLOAT the heading
FUNC FLOAT GET_MAUDE_CHAIR_HEADING()
	RETURN -91.28
ENDFUNC

/// PURPOSE:
///    get the model to spawn Maude's chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_MAUDE_CHAIR_MODEL()
	RETURN prop_table_03_chr
ENDFUNC

/// PURPOSE:
///    get the position to spawn Maude's table
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_TABLE_POSITION()
	RETURN << 2727.40, 4145.56, 43.68 >>
ENDFUNC

/// PURPOSE:
///    get the heading to spawn Maude's table
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    FLOAT the heading
FUNC FLOAT GET_MAUDE_TABLE_HEADING()
	RETURN -92.17
ENDFUNC

/// PURPOSE:
///    get the model to spawn Maude's table
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_MAUDE_TABLE_MODEL()
	RETURN prop_table_03b
ENDFUNC

/// PURPOSE:
///    get the position to spawn Maude's radio
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    VECTOR the positon
FUNC VECTOR GET_MAUDE_RADIO_POSITION()
	RETURN << 2727.29, 4145.90, 44.16 >>
ENDFUNC

/// PURPOSE:
///    get the heading to spawn Maude's radio
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    FLOAT the heading
FUNC FLOAT GET_MAUDE_RADIO_HEADING()
	RETURN 75.15
ENDFUNC

/// PURPOSE:
///    get the model to spawn Maude's radio
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    MODEL_NAMES
FUNC MODEL_NAMES GET_MAUDE_RADIO_MODEL()
	RETURN PROP_RADIO_01
ENDFUNC

/// PURPOSE:
///    get the animation dictionary for Maude's idle
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_IDLE_ANIM_DICT()
	RETURN "special_ped@maude@base"
ENDFUNC

/// PURPOSE:
///    get the animation name for Maude's idle
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_IDLE_ANIM()
	RETURN "base"
ENDFUNC

/// PURPOSE:
///    get the animation name for Maude's chair's idle
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_CHAIR_IDLE_ANIM()
	RETURN "base_chair"
ENDFUNC

/// PURPOSE:
///    get the animation dictionary for Maude's reaction to exit the table and chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_REACT_ANIM_DICT()
	RETURN "special_ped@maude@exit_flee"
ENDFUNC

/// PURPOSE:
///    get the animation name for Maude's reaction to exit the table and chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_REACT_ANIM()
	RETURN "female_Flee_Table_Left_Maude"
ENDFUNC


/// PURPOSE:
///    get the animation name for Maude's chair's reaction to exit the table and chair
///    stored in include file so it's consistent across all bail bonds
/// RETURNS:
///    STRING
FUNC STRING GET_MAUDE_CHAIR_REACT_ANIM()
	RETURN "Female_Flee_Table_Left_Maude_Chair"
ENDFUNC

/// PURPOSE:
///    generates valid spawn positions around a point
/// PARAMS:
///    in - position to create spawn positions around
///    out - the returning vector
///    range - range from the in position
///    pavecheck - specific if returning positin needs to be on pavement or not
/// RETURNS:
///    true if safe coord for ped is found
FUNC BOOL GENERATE_POINT_AROUND_POSITION(VECTOR in, VECTOR &out, FLOAT range, BOOL pavecheck = FALSE)  
	FLOAT zrot 
	VECTOR pos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(in, GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0), <<0, range, 0>>)

	//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "TRYING TO FIND POINT TO WARP TO ") ENDIF #ENDIF
	IF GET_GROUND_Z_FOR_3D_COORD(pos, zrot) 
		pos.z = zrot 
	ENDIF
	out = pos
	RETURN GET_SAFE_COORD_FOR_PED(pos, pavecheck, out) 
ENDFUNC

FUNC BOOL WARP_PLAYER_TO_RAMDOM_SAFE_POINT_IN_RANGE_PED(PED_INDEX ped, FLOAT range)
	VECTOR vWarpPoint
	IF IS_PED_UNINJURED(ped)
		IF GENERATE_POINT_AROUND_POSITION(GET_ENTITY_COORDS(ped), vWarpPoint, range)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpPoint)
			SET_ENTITY_FACING(PLAYER_PED_ID(), GET_ENTITY_COORDS(ped))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Check is the specified time has passed using specified timer.
/// PARAMS:
///    iTimeAmount - Time to check.
///    iTimer - Timer to use.
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimer, INT iTimeAmount)
	RETURN (GET_GAME_TIMER() - iTimer) > iTimeAmount
ENDFUNC

/// PURPOSE:
///    Check is the specified frame count has passed using specified timer.
/// PARAMS:
///    iFrameCounterTimer - Frame Count to check.
///    iFrameAmount - Frame Count to check against.
/// RETURNS:
///    True is the specified amount of iFrameAmount has passed for the specified iFrameCounterTimer.
FUNC BOOL HAS_FRAME_COUNTER_PASSED(INT iFrameCounterTimer, INT iFrameAmount)
	RETURN (GET_FRAME_COUNT() - iFrameCounterTimer) > iFrameAmount
ENDFUNC

/// PURPOSE:
///    clear's the specified help text if it is displaying
/// PARAMS:
///    sStringToClear - string to test
PROC CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING(STRING sStringToClear, BOOL bClearNow = TRUE)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sStringToClear)
		CLEAR_HELP(bClearNow)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
/// PARAMS:
///    sConversationRoot - the conversation root to test
///    bReturnTrueIfStringIsNull - checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT can return "NULL" the first few frames after CREATE_CONVERSATION has returned true
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a root matching the passed in string or "NULL" if bReturnTrueIfNULL
FUNC BOOL IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(STRING sConversationRoot, BOOL bReturnTrueIfStringIsNull = TRUE)
	TEXT_LABEL_23 tlTempRoot		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
		//if current root matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempRoot, sConversationRoot)
			RETURN TRUE
		ENDIF		
		IF bReturnTrueIfStringIsNull
			//if current root matches null, return true
			//because if the correct root isn't returned the first frame or so after CREATE_CONVERSATION has return true.
			IF ARE_STRINGS_EQUAL(tlTempRoot, "NULL")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
///    cloneConversation - ?
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL BB_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel,
								enumConversationPriority PassedConversationPriority = CONV_PRIORITY_AMBIENT_HIGH, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES,
								enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - Which line of dialogue to start from
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL BB_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel,
												STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority = CONV_PRIORITY_AMBIENT_HIGH,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using PLAY_SINGLE_LINE_FROM_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - the specific line we want to play
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen 
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL BB_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel,
													STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority = CONV_PRIORITY_AMBIENT_HIGH,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    will return the enumSubtitlesState needed to allow a dialogue line to play
///    without subtitles if a message is being displayed
/// RETURNS:
///    enumSubtitlesState
FUNC enumSubtitlesState GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()

	IF IS_MESSAGE_BEING_DISPLAYED()
		RETURN DO_NOT_DISPLAY_SUBTITLES
	ENDIF
	
	RETURN DISPLAY_SUBTITLES
ENDFUNC

/// PURPOSE:
///    trigger ambient speech using PLAY_PED_AMBIENT_SPEECH_WITH_VOICE
///    handles toggling STOP_PED_SPEAKING
/// PARAMS:
///    pedIndex - 
///    Context - 
///    VoiceName - 
///    Params - 
PROC BB_PLAY_TRIGGER_AMBIENT_SPEECH(PED_INDEX &pedIndex ,STRING Context, STRING VoiceName, SPEECH_PARAMS Params = SPEECH_PARAMS_ADD_BLIP)
	IF IS_PED_UNINJURED(pedIndex)
		BOOL bTemp = IS_AMBIENT_SPEECH_DISABLED(pedIndex)
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, FALSE)
		ENDIF
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedIndex, Context, VoiceName, Params)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "BB_PLAY_TRIGGER_AMBIENT_SPEECH - Context : ", Context, " VoiceName : ", VoiceName) ENDIF #ENDIF
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check the specified ped is in the current conversation
/// PARAMS:
///    pedIndex - ped to test
/// RETURNS:
///    True if ped is alive and IS_PED_IN_CURRENT_CONVERSATION
FUNC BOOL BB_IS_PED_INVOLVED_IN_CURRENT_CONVERSATION(PED_INDEX pedIndex)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_IN_CURRENT_CONVERSATION(pedIndex)
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "NIG1A_IS_PED_INVOLVED_IN_CURRENT_CONVERSATION - return true, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player is currently in an unsuitable vehicle to transport the bail jumper
///    e.g. doesn;t makes sense for jumper to be hugging trev on a low rider
/// RETURNS:
///    TRUE if vehicle is ok for transport
FUNC BOOL IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER()
	VEHICLE_INDEX vehTempPlayer
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehTempPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ELIF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
		vehTempPlayer = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_VEHICLE_OK(vehTempPlayer)	// add suitable vehicle checks here!
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER() -> RETURN FALSE : vehicle undriveable") ENDIF #ENDIF
		RETURN FALSE
	ELSE
		IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehTempPlayer) = 0
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER() -> RETURN FALSE : GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehTempPlayer) = 0") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
		MODEL_NAMES mnTempVehModel = GET_ENTITY_MODEL(vehTempPlayer)
		IF IS_THIS_MODEL_A_BIKE(mnTempVehModel)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER() -> RETURN FALSE : player on bike") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
		IF IS_THIS_MODEL_A_BICYCLE(mnTempVehModel)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER() -> RETURN FALSE : player on bicycle") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
		IF IS_THIS_MODEL_A_QUADBIKE(mnTempVehModel)	// already one seater , but in case it changes in future
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "IS_PLAYER_VEHICLE_SUITABLE_TO_TRANSPORT_BAIL_JUMPER() -> RETURN FALSE : player on quadbike") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
		//IF IS_THIS_MODEL_A_TRAIN(mnTempVehModel)
		//	RETURN FALSE
		//ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    checks if the player's current weapon is lethal
/// PARAMS:
///    bIncludeProjectilesCheck - if true, checks for grenades,stickybombs etc
///    bIncludeLeathalMeleeWeapons - if true check includes knife, hammer, crowbar etc
///    bIncludeStunGunAsLethal - added parameter as we now want pool peds to surrender to stun gun attack
/// RETURNS:
///    true - if lethal
FUNC BOOL IS_PLAYER_CURRENT_WEAPON_LETHAL(BOOL bIncludeProjectilesCheck = TRUE, BOOL bIncludeLeathalMeleeWeapons = FALSE, BOOL bIncludeStunGunAsLethal = TRUE)	
	//different dialogue based on player weapon
	WEAPON_TYPE currentPlayerWeapon
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
	SWITCH currentPlayerWeapon
		CASE WEAPONTYPE_PISTOL
			FALLTHRU
		CASE WEAPONTYPE_COMBATPISTOL
			FALLTHRU
		CASE WEAPONTYPE_APPISTOL
			FALLTHRU
		CASE WEAPONTYPE_MICROSMG
			FALLTHRU
		CASE WEAPONTYPE_SMG
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTRIFLE
			FALLTHRU
		CASE WEAPONTYPE_CARBINERIFLE
			FALLTHRU
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			FALLTHRU
		CASE WEAPONTYPE_ADVANCEDRIFLE
			FALLTHRU
		CASE WEAPONTYPE_MG
			FALLTHRU
		CASE WEAPONTYPE_COMBATMG
			FALLTHRU
		CASE WEAPONTYPE_PUMPSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SNIPERRIFLE
			FALLTHRU
		CASE WEAPONTYPE_HEAVYSNIPER
			FALLTHRU
		CASE WEAPONTYPE_REMOTESNIPER
			FALLTHRU
		CASE WEAPONTYPE_GRENADELAUNCHER
			FALLTHRU
		CASE WEAPONTYPE_RPG
			FALLTHRU
		CASE WEAPONTYPE_DLC_SNSPISTOL
			FALLTHRU
		CASE WEAPONTYPE_MINIGUN
			RETURN TRUE
	ENDSWITCH	
	IF bIncludeProjectilesCheck
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_GRENADE
				FALLTHRU
			CASE WEAPONTYPE_STICKYBOMB
				FALLTHRU
			CASE WEAPONTYPE_MOLOTOV
				RETURN TRUE
		ENDSWITCH
	ENDIF
	IF bIncludeLeathalMeleeWeapons
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_KNIFE
				FALLTHRU
			CASE WEAPONTYPE_HAMMER
				FALLTHRU
			CASE WEAPONTYPE_CROWBAR
				FALLTHRU
			CASE WEAPONTYPE_DLC_BOTTLE
				FALLTHRU
		ENDSWITCH
	ENDIF
	IF bIncludeStunGunAsLethal
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    get the given ped's current movement state
///    NOTE: for use with the ped detection system
/// PARAMS:
///    pedIndex - ped to get update for 
///    bDoDeadCheck - check ped is alive first
/// RETURNS:
///    BB_PED_MOVEMENT_STATE for the ped
FUNC BB_PED_MOVEMENT_STATE GET_UPDATED_PED_MOVEMENT_STATE(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	BB_PED_MOVEMENT_STATE eCurrent_PedMovementState
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_DEFAULT
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() return BB_PEDMOVEMENTSTATE_DEFAULT ped not alive") ENDIF #ENDIF
			RETURN eCurrent_PedMovementState
		ENDIF
	ENDIF	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_IN_VEHICLE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_IN_VEHICLE") ENDIF #ENDIF
	ELIF GET_PED_STEALTH_MOVEMENT(pedIndex)
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_STEALTH
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_STEALTH") ENDIF #ENDIF
	ELIF IS_PED_IN_COVER(pedIndex)
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_COVER
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_COVER") ENDIF #ENDIF
	ELIF IS_PED_WALKING(pedIndex)
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_WALKING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_WALKING") ENDIF #ENDIF
	ELIF IS_PED_RUNNING(PLAYER_PED_ID())
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_RUNNING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_RUNNING") ENDIF #ENDIF
	ELIF IS_PED_SPRINTING(PLAYER_PED_ID())
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_SPRINTING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_SPRINTING") ENDIF #ENDIF
	ELSE
		eCurrent_PedMovementState = BB_PEDMOVEMENTSTATE_DEFAULT
		//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "GET_UPDATED_PED_MOVEMENT_STATE() -> BB_PEDMOVEMENTSTATE_DEFAULT") ENDIF #ENDIF		
	ENDIF
	RETURN eCurrent_PedMovementState
ENDFUNC

/// PURPOSE:
///    rubberbanding for a fleeing ped on foot for a chasing player
///    makes the ped slow down the longer he's been running
///    NOTE: doesn't test for the ped being alive, or having a valid drive task
/// PARAMS:
///    pedIndex_Driver - the ped fleeing on foot
///    vPlayerPosition - the player's position
///    iTimeStartedFleeingOnFoot - the time in game time the ped started fleeing on foor (used to modify this speed to slow up)
///    fTimeFleeingModifier - the modifer used with iTimeFleeingOnFoot
PROC UPDATE_PED_ON_FOOT_SPEED_FOR_CHASING_PLAYER(PED_INDEX pedIndex, VECTOR vPlayerPosition, INT iTimeStartedFleeingOnFoot, FLOAT fTimeFleeingModifier = 0.00001)
	FLOAT fDesiredSpeed
	FLOAT fIncreaseSpeedRange = 64.0	// 8.0
	FLOAT fDecreaseSpeedRange = 625.0	// 25.0
	FLOAT fUberDecreaseSpeedRange = 16000.0	// 40.0	
	FLOAT fCurrentChaseDistanceSquared = VDIST2(GET_ENTITY_COORDS(pedIndex), vPlayerPosition)	
	INT iTotalTimeFleeing = GET_GAME_TIMER() - iTimeStartedFleeingOnFoot
	fTimeFleeingModifier *= iTotalTimeFleeing
	
	IF fCurrentChaseDistanceSquared > fUberDecreaseSpeedRange	
	AND IS_ENTITY_OCCLUDED(pedIndex)
		fDesiredSpeed = PEDMOVEBLENDRATIO_WALK
	ELIF fCurrentChaseDistanceSquared > fDecreaseSpeedRange
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			fDesiredSpeed = 1.7	// PEDMOVEBLENDRATIO_RUN
			fDesiredSpeed -= fTimeFleeingModifier
			IF fDesiredSpeed < 1.5// cap the min speed after the modifier
				fDesiredSpeed = 1.5
			ENDIF
		ELSE
			fDesiredSpeed = PEDMOVEBLENDRATIO_RUN
		ENDIF
	ELIF fCurrentChaseDistanceSquared < fIncreaseSpeedRange
		fDesiredSpeed = 2.4	// PEDMOVEBLENDRATIO_SPRINT
		fDesiredSpeed -= fTimeFleeingModifier
		IF fDesiredSpeed < 1.5// cap the min speed after the modifier
			fDesiredSpeed = 1.5
		ENDIF
	ELSE
		fDesiredSpeed = PEDMOVEBLENDRATIO_RUN
		fDesiredSpeed -= fTimeFleeingModifier
		IF fDesiredSpeed < 1.5// cap the min speed after the modifier
			fDesiredSpeed = 1.5
		ENDIF
	ENDIF
	SET_PED_MAX_MOVE_BLEND_RATIO(pedIndex, fDesiredSpeed)
	//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PED_ON_FOOT_SPEED_FOR_CHASING_PLAYER - max move speed blend ratio set = ", fDesiredSpeed) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    eBailBoundID - 
/// RETURNS:
///    
FUNC enumCompletionPercentageEntries GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY(BAILBOND_ID eBailBoundID)
	enumCompletionPercentageEntries eReturningEntry
	SWITCH eBailBoundID		
		CASE BBI_QUARRY
			eReturningEntry = CP_OJ_BB3
			CPRINTLN(DEBUG_BAIL_BOND, "Bail Bonds: GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY done for BBI_QUARRY")
		BREAK
		CASE BBI_FARM
			eReturningEntry = CP_OJ_BB5
			CPRINTLN(DEBUG_BAIL_BOND, "Bail Bonds: GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY done for BBI_FARM")
		BREAK
		CASE BBI_MOUNTAIN
			eReturningEntry = CP_OJ_BB7
			CPRINTLN(DEBUG_BAIL_BOND, "Bail Bonds: GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY done for BBI_MOUNTAIN")
		BREAK
		CASE BBI_HOBO
			eReturningEntry = CP_OJ_BB6
			CPRINTLN(DEBUG_BAIL_BOND, "Bail Bonds: GET_SPECIFIC_BAIL_BOND_COMPLETION_PERCENTAGE_ENTRY done for BBI_HOBO")
		BREAK
	ENDSWITCH
	RETURN eReturningEntry
ENDFUNC

/// PURPOSE:
///    Requests a model and checks for it loading
/// PARAMS:
///    _modname - The name of the model to load
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname)

	IF _modname = DUMMY_MODEL_FOR_SCRIPT
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "REQUEST_AND_CHECK_MODEL - DUMMY_MODEL_FOR_SCRIPT passed in, so just return TRUE") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(_modname)
	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "REQUEST_AND_CHECK_MODEL - requested model : ", GET_MODEL_NAME_FOR_DEBUG(_modname)) ENDIF #ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "REQUEST_AND_CHECK_MODEL - model loaded : ", GET_MODEL_NAME_FOR_DEBUG(_modname)) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)

	IF _modname <> DUMMY_MODEL_FOR_SCRIPT
		IF bCheckLoaded
			IF HAS_MODEL_LOADED(_modname)
				SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
			ENDIF
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "UNLOAD_MODEL - DUMMY_MODEL_FOR_SCRIPT passed in, so just return TRUE") ENDIF #ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn
///    bFreeze
///    bBlockNonTemporaryEvents - 
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bBlockNonTemporaryEvents = FALSE)

	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model)
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)

			IF DOES_ENTITY_EXIST(pedindex)
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bBlockNonTemporaryEvents
					IF NOT IS_PED_INJURED(pedindex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, TRUE)
					ENDIF
				ENDIF

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_PED - model loading...") ENDIF #ENDIF
		ENDIF
	ELSE
		IF bUnloadAfterSpawn
			UNLOAD_MODEL(model, FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)

	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model)
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				IF IS_VEHICLE_OK(vehicleindex)
					SET_ENTITY_COORDS(vehicleindex, pos)
					SET_ENTITY_HEADING(vehicleindex, dir)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_VEHICLE - model loading...") ENDIF #ENDIF
		ENDIF
	ELSE
		IF bUnloadAfterSpawn
			UNLOAD_MODEL(model, FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC
 
//// PURPOSE:
///    Spawns a ped in a vehicle and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    Car - the vehicle to spawn inside
///    modelName - The ped's model to load and use to create the ped
///    seat - which seat in the vehicle to spawn in
///    bUnloadAfterSpawn - sets the model as no longer needed after spawning successfully
/// RETURNS:
///    TRUE if the ped was created in the vehicle seat
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF IS_VEHICLE_OK(Car)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		IF bUnloadAfterSpawn
			UNLOAD_MODEL(modelName, FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0, BOOL bUnloadAfterSpawn = TRUE)

	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model)
			objectindex = CREATE_OBJECT(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
//				FLOAT fRealZ
//				GET_GROUND_Z_FOR_3D_COORD(pos, fRealZ)
//				pos.z = fRealZ
//				
//				SET_ENTITY_COORDS(objectindex, pos)
				SET_ENTITY_HEADING(objectindex, dir)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(objectindex, TRUE)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_OBJECT - model loading...") ENDIF #ENDIF
		ENDIF
	ELSE
		IF bUnloadAfterSpawn
			UNLOAD_MODEL(model, FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Spawns a pickup. And activates its physics
/// PARAMS:
///    pickupIndex - The OBJECT_INDEX to write the newly create object to 
///    type - The type of picup to load and use to create the pickup
///    iPlacementFlags - pickup placement flags to indicate how to spawn the pickup
///    model - the pickup model, to be used when creating a custom script pickup type
///    bUnloadAfterSpawn - if true models will be unloaded once spawned
/// RETURNS:
///    TRUE if the pickup was created
FUNC BOOL SPAWN_PICKUP(PICKUP_INDEX &pickupIndex, PICKUP_TYPE type, VECTOR pos, INT iPlacementFlags = 0, MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT, BOOL bUnloadAfterSpawn = TRUE)

	IF NOT DOES_PICKUP_EXIST(pickupIndex)
		IF REQUEST_AND_CHECK_MODEL(model)
			pickupIndex = CREATE_PICKUP(type, pos, iPlacementFlags, -1, TRUE, model)			
			IF DOES_PICKUP_EXIST(pickupIndex)
				IF DOES_PICKUP_OBJECT_EXIST(pickupIndex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(model)
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_PICKUP - pickup and pickup object created") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_PICKUP - model loading...") ENDIF #ENDIF
		ENDIF
	ELSE
		IF DOES_PICKUP_OBJECT_EXIST(pickupIndex)		
			IF bUnloadAfterSpawn
				UNLOAD_MODEL(model, FALSE)
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_PICKUP - pickup and pickup object do exist") ENDIF #ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SPAWN_PICKUP - waiting for pickup object to exist") ENDIF #ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Test if given vehicle has x amount of tyres burst
/// PARAMS:
///    vehIndex - vehicle to check
///    iNumTyresBurstToReturnTrue - num tyres to be burst inorder to return true
/// RETURNS:
///    TRUE - if num of burst tyres >= iNumTyresBurstToReturnTrue
FUNC BOOL ARE_VEHICLE_TYRES_BURST(VEHICLE_INDEX vehIndex, INT iNumTyresBurstToReturnTrue = 1)
	IF IS_VEHICLE_OK(vehIndex)
		INT iNumBurstTyres = 0
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_LEFT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_RIGHT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_LEFT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_RIGHT)
			iNumBurstTyres++
		ENDIF
		IF iNumBurstTyres >= iNumTyresBurstToReturnTrue
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    tests to see if the player is shooting near the ped.
/// PARAMS:
///    ped - ped to test 
///    vPlayerPosition - player coords
///    fPlayerMinDistFromPed - player must be at least this distance from the ped
///    fCloseBulletReactDist - dist bullets wizzing by the ped must be
/// RETURNS:
///    TRUE if player is with in fPlayerMinDistFromPed range and shooting bullets within fCloseBulletReactDist to the ped
FUNC BOOL HAS_PLAYER_FIRED_NEAR_PED(PED_INDEX ped, VECTOR vPlayerPosition, FLOAT fPlayerMinDistFromPed = 50.0, FLOAT fCloseBulletReactDist = 5.0)
	IF IS_ENTITY_IN_RANGE_COORDS(ped, vPlayerPosition, fPlayerMinDistFromPed)		
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped), fCloseBulletReactDist)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the player in a vehicle has potentially just ran over the specified ped
/// PARAMS:
///    ped - ped to check
///    vPlayerPosition - player coords
/// RETURNS:
///    TRUE if he's may have ran over him recently
FUNC BOOL HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED(PED_INDEX pedIndex, VECTOR vPlayerPosition)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
		AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPosition, 3.5)
			OR IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED - returning true") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the ped should surrender whilst on foot
///    checks damage, firing close by and stunned
/// PARAMS:
///    pedIndex - ped to test
///    bTestPlayerProximity - will surrender if player gets really close
///    bTestShootingWeapon - will surender for bullets close by
///    bTestStunning - will surrender if stunned
///    iSurrenderHealthThreshold - if health drops before this ped should surrender
/// RETURNS:
///    TRUE if one of the conditions are met
FUNC BOOL SHOULD_PED_SURENDER_ONFOOT(PED_INDEX pedIndex, VECTOR vPlayerPosition, BOOL bTestPlayerProximity = TRUE, BOOL bTestShootingWeapon = FALSE, BOOl bTestStunning = TRUE, INT iSurrenderHealthThreshold = 170)
	IF IS_PED_UNINJURED(pedIndex)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
			CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - player damaged ped , FC = ", GET_FRAME_COUNT())
			
			// make him surrender if shot in the legs
			IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
			
				CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - ped damaged by weapon anytype , FC = ", GET_FRAME_COUNT())
				
				// don't worry about stunned, get handled below
				IF NOT HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedIndex, WEAPONTYPE_STUNGUN)
					
					CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - ped not damaged by stun gun , FC = ", GET_FRAME_COUNT())
					
					CLEAR_PED_LAST_WEAPON_DAMAGE(pedIndex)
					PED_BONETAG tempBoneTag
					
					IF GET_PED_LAST_DAMAGE_BONE(pedIndex, tempBoneTag)
					
						CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - ped last bone damaged grabbed , FC = ", GET_FRAME_COUNT())
						CLEAR_PED_LAST_DAMAGE_BONE(pedIndex)
						SWITCH tempBoneTag
							CASE BONETAG_PELVIS
							FALLTHRU
							CASE BONETAG_SPINE
							FALLTHRU
							CASE BONETAG_SPINE1
							FALLTHRU
							CASE BONETAG_SPINE2
							FALLTHRU
							CASE BONETAG_SPINE3
							FALLTHRU
							CASE BONETAG_NECK
							FALLTHRU
							CASE BONETAG_L_THIGH
							FALLTHRU
							CASE BONETAG_L_CALF
							FALLTHRU
							CASE BONETAG_R_THIGH
							FALLTHRU
							CASE BONETAG_R_CALF
								CLEAR_PED_LAST_DAMAGE_BONE(pedIndex)
								CLEAR_PED_LAST_WEAPON_DAMAGE(pedIndex)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
								#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true damage from player weapon on ped bone") ENDIF #ENDIF
								RETURN TRUE
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
			
			CLEAR_PED_LAST_DAMAGE_BONE(pedIndex)
			CLEAR_PED_LAST_WEAPON_DAMAGE(pedIndex)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
			
			IF (GET_ENTITY_HEALTH(pedIndex) < iSurrenderHealthThreshold)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true damaged and health") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			IF IS_PED_RAGDOLL(pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true damaged and ragdoll") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			IF HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED(pedIndex, vPlayerPosition)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true damaged and  ran over") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
		
		IF bTestShootingWeapon
			IF HAS_PLAYER_FIRED_NEAR_PED(pedIndex, vPlayerPosition)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true player shot close by") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		IF bTestStunning
			IF IS_PED_BEING_STUNNED(pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true pedIndex being stunned") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		IF bTestPlayerProximity
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPosition, 5.0)
				IF IS_PED_RAGDOLL(pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true pedIndex has ragdolled with player close by") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPosition, 1.0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true player proimity") ENDIF #ENDIF
					RETURN TRUE			
				ENDIF
			ENDIF			
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURENDER_ONFOOT - returning true player touching pedIndex") ENDIF #ENDIF
					RETURN TRUE	
				ENDIF
				//IF HAS_PED_RECEIVED_EVENT(pedIndex, EVENT_PED_COLLISION_WITH_PLAYER)
				
				//ENDIF
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the specified ped is in the same veh as the player
///    NOTE: doesn't check peds for exist / alive
/// PARAMS:
///    pedIndex - the ped to test
/// RETURNS:
///    TRUE if in the same vehicle
FUNC BOOL IS_PED_IN_SAME_VEHILC_AS_PLAYER(PED_INDEX &pedIndex)

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(pedIndex)
			IF (GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = GET_VEHICLE_PED_IS_IN(pedIndex))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    checks to decide if the bail jumper should change his mind and being to flee again
/// PARAMS:
///    pedIndex - the ped to test
/// RETURNS:
///    TRUE if reason to revert back to flee is met
FUNC BOOL SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE(PED_INDEX &pedIndex)
	
	IF IS_PED_UNINJURED(pedIndex)		
		VECTOR vPos = GET_ENTITY_COORDS(pedIndex, FALSE)
		
		// B*1465300 - if sat in same veh as player don't flee for projectile checks
		IF NOT IS_PED_IN_SAME_VEHILC_AS_PLAYER(pedIndex)
			// B*1460048 - After you apprehend them, Bail jumpers don't react to having a grenade thrown near them.
			IF IS_PROJECTILE_IN_AREA((vPos - << 7.0, 7.0, 7.0 >>), (vPos + << 7.0, 7.0, 7.0 >>))
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE - return TRUE  projectile in area") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			/*IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, (vPos - << 7.0, 7.0, 7.0 >>), (vPos + << 7.0, 7.0, 7.0 >>))
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_BAIL_JUMPER_REVERT_BACK_TO_FLEE - return TRUE  explosion in area") ENDIF #ENDIF
				RETURN TRUE
			ENDIF*/
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the ped should surrender whilst in a vehicle
///    checkd ped damge, vehicle damage, ped being out the vehicle, ped getting stunned, burst tyres
/// PARAMS:
///    ped - ped to test 
///    veh - vehicle to test
///    iTimer_Stopped - timer for return TRUE if the vehicle has been stopped for a period of time
///    iNumTyresBurstToWreckVehicle - num of tyres burst to class the ped as should surrender in vehicle
/// RETURNS:
///    TRUE if one of the conditions are met
FUNC BOOL SHOULD_PED_SURRENDER_VEHICLE(PED_INDEX ped, VEHICLE_INDEX veh, INT &iTimer_Stopped, INT iNumTyresBurstToWreckVehicle = 2)
	IF IS_PED_UNINJURED(ped)
		IF IS_VEHICLE_OK(veh)
			IF ARE_VEHICLE_TYRES_BURST(veh, iNumTyresBurstToWreckVehicle)
				TASK_VEHICLE_TEMP_ACTION(ped, veh, TEMPACT_HANDBRAKETURNLEFT, 2000)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - burst tyres") ENDIF #ENDIF	
				RETURN TRUE
			//ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, PLAYER_PED_ID())
			OR (GET_ENTITY_HEALTH(veh) < 150)
				SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE)
				IF (GET_VEHICLE_ENGINE_HEALTH(veh) > 100)
					SET_VEHICLE_ENGINE_HEALTH(veh, 100)
				ENDIF
				SET_VEHICLE_UNDRIVEABLE(veh, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - set wrecked") ENDIF #ENDIF
				RETURN TRUE
			ELIF NOT IS_PED_IN_VEHICLE(ped, veh)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - ped not in vehicle") ENDIF #ENDIF
				RETURN TRUE
			ELIF IS_PED_BEING_STUNNED(ped)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - ped being stunned") ENDIF #ENDIF
				RETURN TRUE
			//ELIF IS_PED_BEING_JACKED(ped)
			//	#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - ped being jacked") ENDIF #ENDIF
			//	RETURN TRUE
			ELIF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, 10000)
			OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_JAMMED, 20000)
			OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, 10000)
			OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, 10000)
				#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - vehicle stuck") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			IF IS_VEHICLE_STOPPED(veh)
				IF HAS_TIME_PASSED(iTimer_Stopped, 3500)
					#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - vehicle stopped") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				iTimer_Stopped = GET_GAME_TIMER()
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_BAIL_BOND, "SHOULD_PED_SURRENDER_VEHICLE - vehicle wrecked") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PED_DEBUG_NAME(PED_INDEX index, STRING name, INT id)
	TEXT_LABEL tDebugName = name
	tDebugName += id
	SET_PED_NAME_DEBUG(index, tDebugName)
ENDPROC

/// PURPOSE:
///    toggle enabling / disabling gangs attacking the player
/// PARAMS:
///    bOn - if true gangs gets disabled, FALSE they get reset
PROC DISABLE_GANGS(BOOL bOn)
	IF bOn
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_FAMILY)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MARABUNTE)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_CULT)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_SALVA)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_WEICHENG)
		SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), FALSE)
	ELSE
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_FAMILY)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MARABUNTE)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_CULT)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_SALVA)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_WEICHENG)
		SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    triggers ambient dialogue from Maude for player's approach
/// PARAMS:
///    pedIndexMaude - Maude
///    vPlayerCoords - Player's position used in proximity check
///    iTimer_Dialogue - timer to set next line going
///    iTimeDelay - delay for next dialogue line
///    fTriggerRange - dist player has to be from Maude
/// RETURNS:
///    TRUE if dialogue was added to the non critical standard buffer
FUNC BOOL TRIGGER_AMBIENT_MAUDE_DIALOGUE_FOR_PLAYER_APPROACH_WITH_JUMPER(PED_INDEX &pedIndexMaude, VECTOR vPlayerCoords,
													INT &iTimer_Dialogue, INT iTimeDelay = 5000, FLOAT fTriggerRange = 20.0)
	
	IF IS_PED_UNINJURED(pedIndexMaude)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			VECTOR vMaudeCoords = GET_ENTITY_COORDS(pedIndexMaude)
			
			IF (VDIST2(vPlayerCoords, vMaudeCoords) < (fTriggerRange * fTriggerRange))
				IF (GET_GAME_TIMER() - iTimer_Dialogue) > (iTimeDelay + GET_RANDOM_INT_IN_RANGE(0, 2000))
					structPedsForConversation sTempDialogueStruct
					ADD_PED_FOR_DIALOGUE(sTempDialogueStruct, 4, pedIndexMaude, "MAUDE")
					STRING sDialogueBlock = "BBCAUD"
					STRING sDialogueRoot = "BBC_loiter"
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sTempDialogueStruct, sDialogueBlock, sDialogueRoot, CONV_PRIORITY_AMBIENT_HIGH)
					iTimer_Dialogue = GET_GAME_TIMER()
					
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : TRIGGER_AMBIENT_MAUDE_DIALOGUE_FOR_PLAYER_APPROACH_WITH_JUMPER - return TRUE : sDialogueBlock = ", sDialogueBlock, " sDialogueRoot = ", sDialogueRoot)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    test if player is jacking a ped out of a driver seat
/// PARAMS:
///    vehIndex - vehicle the player is getting in
/// RETURNS:
///    TRUE if player is jacking a ped
FUNC BOOL BB_IS_PLAYER_JACKING_DRIVER(VEHICLE_INDEX &vehIndex)
	
	IF IS_VEHICLE_OK(vehIndex)
		IF IS_PED_JACKING(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
		PED_INDEX pedDriver = GET_JACK_TARGET(PLAYER_PED_ID())
		IF IS_ENTITY_ALIVE(pedDriver)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    updates vReturnProjectilePos value to store the position of the projectile in the area around vPositionToTest
/// PARAMS:
///    vReturnProjectilePos - ref updated to store projectile in area around vPositionToTest, will return << 0.0, 0.0, 0.0, >> if none found
///    vPositionToTest - position to check from
PROC UPDATE_PROJECTICLE_FLEE_FROM_POSITION(VECTOR &vReturnProjectilePos, VECTOR vPositionToTest)

	VECTOR vTempMin, vTempMax
	vTempMin = vPositionToTest - << 7.0, 7.0, 7.0 >>
	vTempMax = vPositionToTest + << 7.0, 7.0, 7.0 >>
	IF IS_PROJECTILE_IN_AREA(vTempMin, vTempMax)
		IF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_GRENADE vReturnProjectilePos = ", vReturnProjectilePos)
		ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_MOLOTOV, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_MOLOTOV vReturnProjectilePos = ", vReturnProjectilePos)
		ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_SMOKEGRENADE vReturnProjectilePos = ", vReturnProjectilePos)
		ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_BZGAS, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_BZGAS vReturnProjectilePos = ", vReturnProjectilePos)
		ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_STICKYBOMB vReturnProjectilePos = ", vReturnProjectilePos)
		ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_FLARE, vReturnProjectilePos)
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : set WEAPONTYPE_FLARE vReturnProjectilePos = ", vReturnProjectilePos)
		ELSE
			vReturnProjectilePos = << 0.0, 0.0, 0.0 >>
			CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : unable to locate the projectile near the jumper vReturnProjectilePos set << 0.0, 0.0, 0.0 >>")
		ENDIF
	ELSE
		vReturnProjectilePos = << 0.0, 0.0, 0.0 >>
		CPRINTLN(DEBUG_BAIL_BOND, "UPDATE_PROJECTICLE_FLEE_FROM_POSITION : no projectile in area to store vReturnProjectilePos set << 0.0, 0.0, 0.0 >>")
	ENDIF
ENDPROC

/// PURPOSE:
///    set Maude's attributes ready to flee
PROC SET_MAUDE_FLEE_ATTRIBUTES(PED_INDEX &pedMaudeIndex)

	IF IS_PED_UNINJURED(pedMaudeIndex)	
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_USE_VEHICLE, FALSE)			
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_DISABLE_HANDS_UP, FALSE)						
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_PREFER_PAVEMENTS, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_USE_COVER, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedMaudeIndex, FA_LOOK_FOR_CROWDS, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedMaudeIndex, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedMaudeIndex, CA_ALWAYS_FLEE, TRUE)
		SET_PED_CONFIG_FLAG(pedMaudeIndex, PCF_RunFromFiresAndExplosions, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    task Maude with the exit from sync scene anim
/// PARAMS:
///    pedMaudeIndex - ref to Maude
///    objMaudeChair - ref to Maude's chair
///    iMaudeReact_SceneSyncIndex - handle to sync scene
/// RETURNS:
///    TRUE if anim was applied or if Maude isn't playing the sync scene to apply it
FUNC BOOL SET_MAUDE_PLAY_FLEE_EXIT_ANIM(PED_INDEX &pedMaudeIndex, OBJECT_INDEX &objMaudeChair, INT &iMaudeReact_SceneSyncIndex)

	IF IS_PED_UNINJURED(pedMaudeIndex)		
		IF IS_ENTITY_PLAYING_ANIM(pedMaudeIndex, GET_MAUDE_IDLE_ANIM_DICT(), GET_MAUDE_IDLE_ANIM())
		//OR IsPedPerformingTask(pedMaudeIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)	// removed since this still returned true when she was ragdolling
			IF NOT IS_PED_RAGDOLL(pedMaudeIndex)
			OR IS_PED_GETTING_UP(pedMaudeIndex)
				REQUEST_ANIM_DICT(GET_MAUDE_REACT_ANIM_DICT())
				IF HAS_ANIM_DICT_LOADED(GET_MAUDE_REACT_ANIM_DICT())
					iMaudeReact_SceneSyncIndex = CREATE_SYNCHRONIZED_SCENE(<< 2727.40, 4145.56, 43.68 >>, << 0.0, 0.0, -92.17 >>)
					SET_SYNCHRONIZED_SCENE_LOOPED(iMaudeReact_SceneSyncIndex, FALSE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iMaudeReact_SceneSyncIndex, FALSE)
					
					IF IsPedPerformingTask(pedMaudeIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
						STOP_SYNCHRONIZED_ENTITY_ANIM(pedMaudeIndex, INSTANT_BLEND_OUT, TRUE)
					ENDIF
					TASK_SYNCHRONIZED_SCENE(pedMaudeIndex, iMaudeReact_SceneSyncIndex, GET_MAUDE_REACT_ANIM_DICT(), GET_MAUDE_REACT_ANIM(), INSTANT_BLEND_IN, SLOW_BLEND_OUT,
												SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT | SYNCED_SCENE_TAG_SYNC_OUT)
												//SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT | SYNCED_SCENE_TAG_SYNC_OUT)	// SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)	
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedMaudeIndex)
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MAUDE_PLAY_FLEE_EXIT_ANIM() Maude given exit sync scene this frame FC = ", GET_FRAME_COUNT())
					
					IF IS_ENTITY_ALIVE(objMaudeChair)
						IF IS_ENTITY_PLAYING_ANIM(objMaudeChair, GET_MAUDE_IDLE_ANIM_DICT(), GET_MAUDE_CHAIR_IDLE_ANIM())
							STOP_SYNCHRONIZED_ENTITY_ANIM(objMaudeChair, FAST_BLEND_OUT, FALSE)
						ENDIF
						INT iEntitySyncedSceneFlags = 0
						iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
						iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE)
						iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
						iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)		
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objMaudeChair, iMaudeReact_SceneSyncIndex, GET_MAUDE_CHAIR_REACT_ANIM(), GET_MAUDE_REACT_ANIM_DICT(), FAST_BLEND_IN, SLOW_BLEND_OUT, iEntitySyncedSceneFlags)
					ENDIF
					
					RETURN TRUE	// needs to wait a frame for anim to register
				ELSE
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MAUDE_PLAY_FLEE_EXIT_ANIM() return FALSE loading anim dict...")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MAUDE_PLAY_FLEE_EXIT_ANIM() return TRUE Maude ragdoll / getting up so skipping exit anim")
				RETURN TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : SET_MAUDE_PLAY_FLEE_EXIT_ANIM() return TRUE Maude not playing sync scene to exit from so skipping exit anim")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
