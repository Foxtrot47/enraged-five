// Includes
USING "mission_control_public.sch"
USING "flow_public_core.sch"
USING "replay_public.sch"

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    temp proc to help determine what's blocking bail bond hobocamp from launching
	PROC DEBUG_PRINT_ON_MISSION_STATE_THIS_FRAME()
		TEXT_LABEL_63 tlMissionState = "invalid"
		SWITCH g_OnMissionState 
			CASE MISSION_TYPE_STORY
				tlMissionState = "MISSION_TYPE_STORY"
			BREAK
			CASE MISSION_TYPE_STORY_PREP
				tlMissionState = "MISSION_TYPE_STORY_PREP"
			BREAK
			CASE MISSION_TYPE_STORY_FRIENDS
				tlMissionState = "MISSION_TYPE_STORY_FRIENDS"
			BREAK
			CASE MISSION_TYPE_RANDOM_CHAR
				tlMissionState = "MISSION_TYPE_RANDOM_CHAR"
			BREAK
			CASE MISSION_TYPE_RANDOM_EVENT
				tlMissionState = "MISSION_TYPE_RANDOM_EVENT"
			BREAK
			CASE MISSION_TYPE_FRIEND_ACTIVITY
				tlMissionState = "MISSION_TYPE_FRIEND_ACTIVITY"
			BREAK
			CASE MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG
				tlMissionState = "MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG"
			BREAK
			CASE MISSION_TYPE_FRIEND_SQUAD
				tlMissionState = "MISSION_TYPE_FRIEND_SQUAD"
			BREAK
			CASE MISSION_TYPE_MINIGAME
				tlMissionState = "MISSION_TYPE_MINIGAME"
			BREAK
			CASE MISSION_TYPE_MINIGAME_FRIENDS
				tlMissionState = "MISSION_TYPE_MINIGAME_FRIENDS"
			BREAK
			CASE MISSION_TYPE_RAMPAGE
				tlMissionState = "MISSION_TYPE_RAMPAGE"
			BREAK
			CASE MISSION_TYPE_EXILE
				tlMissionState = "MISSION_TYPE_EXILE"
			BREAK
			CASE MISSION_TYPE_OFF_MISSION
				tlMissionState = "MISSION_TYPE_OFF_MISSION"
			BREAK
		ENDSWITCH
		CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : DEBUG_PRINT_ON_MISSION_STATE_THIS_FRAME() - ", tlMissionState)
	ENDPROC
#ENDIF

/// PURPOSE:
///    get the current email message enum for the specific bail bond
/// PARAMS:
///    eBailBondID - the specific bailbond
/// RETURNS:
///    EMAIL_MESSAGE_ENUMS - for the specfic bail bond
FUNC EMAIL_MESSAGE_ENUMS GET_CURRENT_EMAIL(BAILBOND_ID eBailBondID)
	SWITCH eBailBondID
		CASE BBI_QUARRY
			RETURN EMAIL_BAIL_BONDS_INFO1
		CASE BBI_FARM
			RETURN EMAIL_BAIL_BONDS_INFO2
		CASE BBI_MOUNTAIN
			RETURN EMAIL_BAIL_BONDS_INFO3
		CASE BBI_HOBO
			RETURN EMAIL_BAIL_BONDS_INFO4
	ENDSWITCH
	SCRIPT_ASSERT("BailBond_Launcher.sc : GET_CURRENT_EMAIL invalid parameter eBailBondID")
	RETURN EMAIL_BAIL_BONDS_FINAL
ENDFUNC

/// PURPOSE:
///    Get the specific bail bond's dialogue text block
/// PARAMS:
///    eBailBondID - the specific bail bond
/// RETURNS:
///    string value
FUNC STRING GET_BAIL_BOND_DIALOGUE_TEXT_BLOCK(BAILBOND_ID eBailBondID)
	SWITCH eBailBondID
		CASE BBI_QUARRY
			RETURN "BB1AUD"
		CASE BBI_FARM
			RETURN "BB2AUD"
		CASE BBI_MOUNTAIN
			RETURN "BB3AUD"
		CASE BBI_HOBO
			RETURN "BB4AUD"
	ENDSWITCH
	SCRIPT_ASSERT("BailBond_common.sch : GET_BAIL_BOND_DIALOGUE_TEXT_BLOCK invalid parameter eBailBondID")
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Get the specific bail bond's dialogue root for the Trevor comment
/// PARAMS:
///    eBailBondID - the specific bail bond
/// RETURNS:
///    string value
FUNC STRING GET_TREVOR_DIALOGUE_ROOT_CHECK_EMAIL(BAILBOND_ID eBailBondID)
	SWITCH eBailBondID
		CASE BBI_QUARRY
			RETURN "BB1_TE"		// I'll have to keep an eye out for you.  
		CASE BBI_FARM
			RETURN "BB2_TE"		// Look who it is. Larry Tupper.  
		CASE BBI_MOUNTAIN
			RETURN "BB3_TE"		// Finance guy, my favorite.  
		CASE BBI_HOBO
			RETURN "BB4_TE"		// Looks like Father Christmas has been a bad boy.
	ENDSWITCH
	SCRIPT_ASSERT("BailBond_common.sch : GET_TREVOR_DIALOGUE_ROOT_CHECK_EMAIL invalid parameter eBailBondID")
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
// handles triggering comment from Trevor once he has read the email from Maude
///    Moved to common since they could be trigger either off or on mission
/// PARAMS:
///    eLaunchData - the launch data for the specific bail bond
///    sDialogueStruct - the convo struct to use for adding Trevor for dialogue and CREATE_CONVERSATION
PROC HANDLE_TREVOR_DIALOGUE_FOR_READING_TRIGGER_EMAIL(BAIL_BOND_LAUNCH_DATA &eLaunchData, structPedsForConversation &sDialogueStruct)
	IF NOT eLaunchData.bDoneTrevorReadsEmailDialogue
		IF HAS_DYNAMIC_EMAIL_BEEN_READ_BY_PRIMARY_TARGET(DYNAMIC_THREAD_BAIL_BONDS, GET_CURRENT_EMAIL(eLaunchData.eBailBondID))	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					ADD_PED_FOR_DIALOGUE(sDialogueStruct, 2, PLAYER_PED_ID(), "TREVOR")
					IF CREATE_CONVERSATION(sDialogueStruct, GET_BAIL_BOND_DIALOGUE_TEXT_BLOCK(eLaunchData.eBailBondID),
										GET_TREVOR_DIALOGUE_ROOT_CHECK_EMAIL(eLaunchData.eBailBondID), CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
						eLaunchData.bDoneTrevorReadsEmailDialogue = TRUE
						CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : HANDLE_TREVOR_DIALOGUE_FOR_READING_TRIGGER_EMAIL : Triggered Trevor comment after reading the email from Maude")
					ENDIF
				ELSE
					eLaunchData.bDoneTrevorReadsEmailDialogue = TRUE
					CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : HANDLE_TREVOR_DIALOGUE_FOR_READING_TRIGGER_EMAIL : Skipped Trevor comment after reading the email from Maude - ongoing convo or objective")
				ENDIF
			ELSE
				eLaunchData.bDoneTrevorReadsEmailDialogue = TRUE
				CPRINTLN(DEBUG_BAIL_BOND, GET_THIS_SCRIPT_NAME(), " : HANDLE_TREVOR_DIALOGUE_FOR_READING_TRIGGER_EMAIL : Skipped Trevor comment after reading the email from Maude - player ped injutede")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    called by the mission script when the mission has failed
PROC BAILBOND_FAILED(INT iBailbondIndex)
	
	SET_FORCE_CLEANUP_FAIL_REASON()
	
	// this check is needed as it is possible to fail a mission and then get killed 
	// during the fade (after mission already marked as failed, and replay is being processed)
	IF NOT IS_REPLAY_BEING_PROCESSED() 
		// set up replay
		CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), "Setting up for a replay for bailbond.")
		
		// shitskips....
		//Increment the shitskip counter but only if the retry value is the same as last time
		IF g_iLastObservedRetryStatusOnFail = g_replayMissionStage
			//replace this with bailbond version
			g_savedGlobals.sBailBondData.iFailsNoProgress[iBailbondIndex]++
			CPRINTLN(DEBUG_REPLAY, "Mission fail without progress counter increased to ", g_savedGlobals.sBailBondData.iFailsNoProgress[iBailbondIndex])
		ENDIF
		// update the checkpoint we reached
		g_iLastObservedRetryStatusOnFail = g_replayMissionStage
		
		
		Setup_Minigame_Replay(GET_THIS_SCRIPT_NAME())
	ENDIF
ENDPROC

/// PURPOSE:
///    performs checks to make sure the bail bond ambient event is allowed to launch
/// PARAMS:
///    bIsMissionScriptRunning - we don't need to perform some checks if the mission script is running
/// RETURNS:
///    TRUE if the bail bond is allowed to launch
FUNC BOOL IS_BAIL_BOND_SAFE_TO_LAUNCH(BOOL bIsMissionScriptRunning = FALSE)
	IF GET_MISSION_FLAG()
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as mission flag is already set.")
		RETURN FALSE
	ENDIF	
	IF g_bIsOnRampage
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as g_bIsOnRampage is set.")
		RETURN FALSE
	ENDIF
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as currently on friend activity is TRUE.")	
		RETURN FALSE
	ENDIF
	/* B*965604 - commented out for now since Bail Bonds are not to be trigger in flow for now.
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_RANDOM_EVENTS)
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied FLOWFLAG_ALLOW_RANDOM_EVENTS not set.")	
		RETURN FALSE
	ENDIF*/
	IF (GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR)
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied player character isn't Trevor.")	
		RETURN FALSE
	ENDIF
	IF IS_CUTSCENE_PLAYING()
		CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied IS_CUTSCENE_PLAYING.")
		RETURN FALSE
	ENDIF
	IF NOT bIsMissionScriptRunning
		IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_RANDOM_CHAR)
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_RANDOM_CHAR) return FALSE")
			#IF IS_DEBUG_BUILD DEBUG_PRINT_ON_MISSION_STATE_THIS_FRAME() #ENDIF
			RETURN FALSE
		ENDIF
		IF GET_RANDOM_EVENT_FLAG()
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as random event flag is already set.")	
			RETURN FALSE
		ENDIF
		IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied IS_PLAYER_PLAYING returned false.")	
			RETURN FALSE
		ENDIF
		IF NOT IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_EVENT)
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_EVENT) returned false.")	
			RETURN FALSE
		ENDIF
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied player has wanted level is TRUE.")	
				RETURN FALSE
			ENDIF
		ENDIF
		
		// follow checks taken from old check IF NOT PRIVATE_Is_Safe_To_Start_Communication(BIT_TREVOR, CHAR_MAUDE, CPR_MEDIUM)
		
		IF IS_CUTSCENE_PLAYING()
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied IS_CUTSCENE_PLAYING() return TRUE")
			RETURN FALSE
		ENDIF
		//Is the browser screen visible?
		IF g_bBrowserVisible
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied g_bBrowserVisible = TRUE")
			RETURN FALSE
		ENDIF
		//Are we switching?
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		OR Is_Player_Timetable_Scene_In_Progress()
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied player switch in progress")
			RETURN FALSE
		ENDIF
		IF IS_RESULT_SCREEN_DISPLAYING()
			CPRINTLN(DEBUG_BAIL_BOND, "IS_BAIL_BOND_SAFE_TO_LAUNCH: ", "Script denied as IS_RESULT_SCREEN_DISPLAYING() return TRUE")	
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    toggle setup of Maude's dropoff area, such as clear areas, scenario blocking and turning off road nodes.
///    NOTE keep proc in sync with SETUP_AREA_FOR_SUBMISSION(RC_MAUDE_1)
/// PARAMS:
///    bEnable - if we are enabling the setup or turning it all back off
PROC SETUP_MAUDES_FOR_DROPOFF(BOOL bEnable, SCENARIO_BLOCKING_INDEX &scenarioBlockingIndex)
	
	VECTOR vClearZone_DropOffPoint_Min = << 2711.19775, 4134.42529, 32.90168 >>
	VECTOR vClearZone_DropOffPoint_Max = << 2739.98145, 4155.22070, 50.28859 >>
	
	IF bEnable
		
		// Scenarios
		scenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)
		
		// Peds
		SET_PED_NON_CREATION_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)

		// vehicles								
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)
		SET_ROADS_IN_AREA(<< 2697.22241, 4119.89355, 42.79107 >>, << 2746.03955, 4162.97363, 43.62732 >>, FALSE)	// bigger area for turning off the road nodes
		
		CLEAR_AREA(<< 2728.33276, 4144.77783, 43.29292 >>, 7.5, TRUE)
		CPRINTLN(DEBUG_BAIL_BOND, "SETUP_MAUDES_FOR_DROPOFF - Enabled")
	ELSE
		// Restore everything to normal
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingIndex)
		
		CLEAR_PED_NON_CREATION_AREA()
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, TRUE)
		SET_ROADS_BACK_TO_ORIGINAL(<< 2697.22241, 4119.89355, 42.79107 >>, << 2746.03955, 4162.97363, 43.62732 >>)
		CPRINTLN(DEBUG_BAIL_BOND, "SETUP_MAUDES_FOR_DROPOFF - Disbled")
	ENDIF
ENDPROC
