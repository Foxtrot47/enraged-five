// Dave Watson

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"



// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun


// Clean Up
PROC missionCleanup()
	printDebugString("ob_abattoir Terminated >>>>>>>>>>>>>>>>>\n")
	REMOVE_ANIM_DICT("MISSMIC2")
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oAbattoirCut)

IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
	missionCleanup()
ENDIF

IF DOES_ENTITY_EXIST(oAbattoirCut)
	
	FREEZE_ENTITY_POSITION(oAbattoirCut, TRUE)
	
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oAbattoirCut)
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oAbattoirCut)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael2")) > 0
			SWITCH ambStage
				CASE ambCanRun
					IF DOES_ENTITY_HAVE_DRAWABLE(oAbattoirCut)
						REQUEST_ANIM_DICT("MISSMIC2")
						IF HAS_ANIM_DICT_LOADED("MISSMIC2")
							//-- Model is P_beefsplitter_S
							
							PLAY_ENTITY_ANIM(oAbattoirCut, "beefsplitter_loop", "MISSMIC2", 1.0, TRUE, FALSE)
							ambStage = ambRunning
							
						ENDIF
					ENDIF
				BREAK
				CASE ambRunning
				
				BREAK
				CASE ambEnd
				
				BREAK
			ENDSWITCH 
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

