// Includes
USING "ob_safehouse_common.sch"
USING "freemode_header.sch"
USING "context_control_public.sch"
USING "net_realty_mess_include.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_bong.sc
//		DESCRIPTION		:	Franklin and Michael's bong safehouse activity
//
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE,
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_TRIGGER_EXIT_ANIM,
	TS_BREAKOUT_EARLY,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

ENUM MP_TRIGGER_STATE
	MP_TS_PLAYER_OUT_OF_RANGE,
	MP_TS_WAIT_FOR_PLAYER,
	MP_TS_TRIGGER_SMOKE_ANIM,
	MP_TS_CHECK_SMOKE_ANIM,
	MP_TS_RUN_SMOKE_ANIM,
	MP_TS_TRIGGER_EXIT_ANIM,
	MP_TS_CHECK_EXIT_ANIM,
	MP_TS_BREAKOUT_EARLY,
	MP_TS_RESET
ENDENUM
MP_TRIGGER_STATE eMPTriggerState = MP_TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT STONED_PHASE		0.5

BOOL			bBongSmoke    		= FALSE
BOOL 			bBreakoutEarly		= FALSE
BOOL			bLighterFlame  		= FALSE	
BOOL			bLighterSparks 		= FALSE
BOOL			bSetupAnimDict 		= FALSE
BOOL			bSetupPTFX 			= FALSE
//BOOL 			bTriggeredCam2		= FALSE
BOOL 			bTriggeredExitCam 	= FALSE
BOOL 			bTakenHit			= FALSE
BOOL 			bAppliedAnim		= FALSE
BOOL			bConvoKilled		= FALSE
BOOL			bInLowApt			= FALSE

//FLOAT	 		fCam2PhaseTime		= 0.1
//FLOAT 			fExitCamPhaseTime	= 0.7

INT 			iCurProperty
INT				mSynchedScene
INT 			iUsageStat
INT 			iContextIntention 	= NEW_CONTEXT_INTENTION

CAMERA_INDEX	CamIndex

OBJECT_INDEX 	mLighter

PTFX_ID			ptfxLighter
STRING			sClipSet 		= "move_m@drunk@slightlydrunk"  
STRING 			sAnimDict
STRING 			sPedSmokeAnim
STRING 			sBongSmokeAnim
STRING			sPedExitAnim
STRING 			sBongExitAnim
STRING 			sLighterSmokeAnim
STRING			sLighterExitAnim

VECTOR 			vTriggerPos
FLOAT 			fTriggerHead

VECTOR			vScenePos
VECTOR 			vSceneHead

//VECTOR			vCamPos
//VECTOR			vCamHead
//
//VECTOR			vCam2Pos
//VECTOR			vCam2Head

//VECTOR			vExitCamPos
//VECTOR			vExitCamHead

WEAPON_TYPE		storedWeapon

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_BONG_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Cleaning up...")
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clear help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BONG2")
		CLEAR_HELP(TRUE)
	ENDIF
			
	// Remove particle fx
	IF bSetupPTFX
		REMOVE_PTFX_ASSET()
	ENDIF
	
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
		
	// Player is no longer stoned
	CLEANUP_STONED_EFFECT()
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
		SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
	ENDIF
	
	// B*1918442
	IF bAppliedAnim = TRUE		// if we have applied the drunk clipset
		IF bTakenHit = FALSE	// but we haven't made the player drunk, reset them
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(PLAYER_PED_ID(), AAT_IDLE)
				RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), 0.0)		
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Cleaning up - reset movement clipset changes")
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bInMultiplayer
		
		RELEASE_CONTEXT_INTENTION(iContextIntention)
		
		IF bSafehouseSetControlOff
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		// End script
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	ELSE
					
		// Clean up the audio scenes
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
			STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
			STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
		ENDIF

		// Remove monalogues
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		REMOVE_PED_FOR_DIALOGUE(mWeedConv, 0)
		REMOVE_PED_FOR_DIALOGUE(mWeedConv, 1)
		
		// B*1895222 - Reactivate the chop script only if the player started the activity
		IF bIsFranklin
			IF bSafehouseSetControlOff
				REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
			ENDIF
		ENDIF
	
		// Restore control and remove motion blur
		SAFE_RESTORE_PLAYER_CONTROL()
		
		// End script
		TERMINATE_THIS_THREAD()
	
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary.
FUNC BOOL HAS_BONG_ANIM_DICT_LOADED()
	
	IF bIsFranklin
		sPedSmokeAnim 		= "bong_FRA"
		sPedExitAnim		= "exit_FRA"
		sBongSmokeAnim 		= "bong_bong"
		sLighterSmokeAnim 	= "bong_lighter"
		//sCamAnim 			= "bong_cam"
	ELSE
		sPedSmokeAnim 		= "michael_short"
		sPedExitAnim		= "michael_exit"
		sBongSmokeAnim 		= "bong_short"
		sLighterSmokeAnim 	= "lighter_short"
		//sCamAnim 			= "bong_cam"
	ENDIF
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	
	IF HAS_ANIM_DICT_LOADED(sAnimDict)
		bSetupAnimDict = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests and loads particle effects for this activity
FUNC BOOL HAS_BONG_PTFX_ASSET_LOADED()
	
	// Load PTFX
	REQUEST_PTFX_ASSET()
	IF HAS_PTFX_ASSET_LOADED()
		bSetupPTFX = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


FUNC BOOL HAS_BONG_AUDIO_LOADED()

	IF bIsFranklin
		REQUEST_AMBIENT_AUDIO_BANK("SAFEHOUSE_FRANKLIN_USE_BONG")
	ELSE
		REQUEST_AMBIENT_AUDIO_BANK("SAFEHOUSE_MICHAEL_USE_BONG")
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Have particle effects and animations loaded for this activity
FUNC BOOL HAVE_ASSETS_LOADED()

	IF HAS_BONG_ANIM_DICT_LOADED()
		IF HAS_BONG_PTFX_ASSET_LOADED()
			IF HAS_BONG_AUDIO_LOADED()
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Assets have loaded...")
				RETURN TRUE
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Audio loading...")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: PTFX loading...")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Anims loading...")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_LOW_APT_SOFA_IN_USE()

	IF bInLowApt
	AND IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Returns anim phase for bong smoke FX
FUNC FLOAT GET_BREAKOUT_PHASE()
	IF bIsFranklin
		RETURN 0.350
	ELSE
		RETURN 0.550
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns anim phase for bong smoke FX
FUNC FLOAT GET_PHASE_TIME_FOR_BONG_SMOKE()
	IF bIsFranklin
		RETURN 0.440
	ELSE
		RETURN 0.495
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns anim phase for lighter flame FX 
FUNC FLOAT GET_PHASE_TIME_FOR_LIGHTER_FLAME(BOOL bStartFX=TRUE)
	IF bIsFranklin
		IF bStartFX
			RETURN 0.194
		ELSE
			RETURN 0.438
		ENDIF
	ELSE
		IF bStartFX
			RETURN 0.246
		ELSE
			RETURN 0.470
		ENDIF
	ENDIF
ENDFUNC

FUNC VECTOR GET_LIGHTER_FLAME_POS()

	IF bIsFranklin
		RETURN <<-0.005,0.0,0.05>>
	ELSE
		RETURN <<0.0,0.0,0.06>>
	ENDIF

ENDFUNC

/// PURPOSE:
///    Returns anim phase for lighter sparks FX 
FUNC FLOAT GET_PHASE_TIME_FOR_LIGHTER_SPARKS()
	IF bIsFranklin
		RETURN 0.185
	ELSE
		RETURN 0.244
	ENDIF
ENDFUNC

// Detect breakout of animation
PROC CHECK_FOR_BREAKOUT()
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: BREAKOUT DETECTED")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_EXIT_PHASE_TIME()
	
	IF bIsFranklin
		RETURN 0.783
	ELSE
		RETURN 0.899
	ENDIF
ENDFUNC

FUNC FLOAT GET_HEADING()
	IF bIsFranklin
		RETURN -47.5447
	ELSE
		RETURN 7.9319
	ENDIF
ENDFUNC

FUNC FLOAT GET_PITCH
	IF bIsFranklin
		RETURN -7.5031
	ELSE
		RETURN 0.7148
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Updates SINGLE PLAYER bong smoking activity
PROC UPDATE_BONG_ACTIVITY()
	
	vTriggerPos  = GET_TRIGGER_VEC_FOR_THIS_OBJECT()
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0
	FLOAT fReturnStartPhase, fReturnEndPhase
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
				
			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(mTrigger), TRIGGER_ANGLE)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM_WEAPDIS")	
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Player close to trigger...")
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(mTrigger), TRIGGER_ANGLE)
				
					IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						
						//vStartPos = GET_ENTITY_COORDS(mTrigger)
						//fStartHead = GET_ENTITY_HEADING(mTrigger)
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Activity triggered...")
						SHOW_DEBUG_FOR_THIS_OBJECT()
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF bIsFranklin
							Request_Drunk_Audio_Scene("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
							//START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
						ELSE
							Request_Drunk_Audio_Scene("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
							//START_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						
						IF DOES_ENTITY_EXIST(mTrigger)
							IF NOT IS_ENTITY_ATTACHED(mTrigger)	
								FREEZE_ENTITY_POSITION(mTrigger, FALSE)
							ENDIF
						ENDIF
	
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())

			// Apply the anim to the player and all props he uses.
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_ENTER_ANIM_FOR_SECOND_ENTITY(), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
									
			// Setup camera
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				
				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, GET_CAMERA_FOR_THIS_ACTIVITY(), sAnimDict)
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TS_RUN_ANIM")
			eTriggerState = TS_RUN_ANIM
			
		BREAK
				
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			CHECK_FOR_BREAKOUT()
			
			IF NOT bAppliedAnim
				
				REQUEST_CLIP_SET(sClipSet)
		
				IF HAS_CLIP_SET_LOADED(sClipSet)				
					
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
					SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), sClipSet)
					SET_PED_ALTERNATE_MOVEMENT_ANIM(PLAYER_PED_ID(), AAT_IDLE, sClipSet, "idle")
					bAppliedAnim = TRUE
				ENDIF
			ENDIF
			
			// Monitor for particle effects/early breakout
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				IF NOT bConvoKilled
					IF fScenePhase > 0.15
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Killing any previous convo")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Convo check done...")
						bConvoKilled = TRUE
					ENDIF
				ENDIF
				
				// Lighter flame
				IF bLighterFlame = FALSE
					IF fScenePhase > GET_PHASE_TIME_FOR_LIGHTER_FLAME()
						ptfxLighter = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_lighter_flame", GET_SECONDARY_ENTITY(), GET_LIGHTER_FLAME_POS(), <<0.0,0.0,0.0>>)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxLighter, "inhale", 1)
						bLighterFlame = TRUE
					ENDIF
				ELSE
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLighter)
						IF fScenePhase > GET_PHASE_TIME_FOR_LIGHTER_FLAME(FALSE)
							STOP_PARTICLE_FX_LOOPED(ptfxLighter)
						ENDIF
					ENDIF
				ENDIF
				
				// Lighter sparks
				IF bLighterSparks = FALSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > GET_PHASE_TIME_FOR_LIGHTER_SPARKS()
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sh_lighter_sparks", GET_SECONDARY_ENTITY(), << 0.0, 0.0, 0.06 >>, << 0.0, 0.0, 0.0 >>)
						bLighterSparks = TRUE
					ENDIF
				ENDIF
				
				// Bong smoke fx
				IF bBongSmoke = FALSE
					IF fScenePhase > GET_PHASE_TIME_FOR_BONG_SMOKE()
						START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
						bBongSmoke = TRUE
					ENDIF
				ENDIF
				
				IF fScenePhase > STONED_PHASE
					IF NOT bTakenHit
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
						ELSE
							Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL)
						ENDIF
						iTimesUsed++
						bTakenHit = TRUE
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Player_Takes_Weed_Hit called ")
					ENDIF
				ENDIF			
				
				IF NOT bTriggeredExitCam
					IF fScenePhase > GET_EXIT_PHASE_TIME()
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TRIGGERING EXIT CAM")
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						// Normal gameplay camera
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							DESTROY_CAM(CamIndex)
																		
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING())
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_PITCH())
							
							SHAKE_GAMEPLAY_CAM("HAND_SHAKE", DEFAULT_CAM_SHAKE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						
						bTriggeredExitCam = TRUE
						
					ENDIF
				ENDIF

						
				// Handle breakout of animation
				IF bBreakoutEarly		
										
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), "ScriptEvent", fReturnStartPhase, fReturnEndPhase)
						IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: Found ScriptEvent at ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Breakout Early - TS_RUN_ANIM -> TS_TRIGGER_EXIT_ANIM")
							eTriggerState = TS_TRIGGER_EXIT_ANIM
						ENDIF
					ENDIF
					
//					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
//						IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
//							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: Found WalkInterruptible at ", fScenePhase)
//							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Breakout Early - TS_RUN_ANIM -> TS_TRIGGER_EXIT_ANIM")
//							eTriggerState = TS_TRIGGER_EXIT_ANIM
//						ENDIF
//					ENDIF
				ENDIF
				
			// Synchronised scene has finished
			ELSE	
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TS_RUN_ANIM -> TS_RESET")
				eTriggerState = TS_RESET
			ENDIF
		BREAK
		
		CASE TS_TRIGGER_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
										
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			
			// Apply the exit anim to the player and all props he uses.
			IF bEntityHasExitAnim
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_EXIT_ANIM_FOR_SECOND_ENTITY(), sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					IF NOT bIsFranklin
						SET_CAM_ANIM_CURRENT_PHASE(CamIndex, 1.0)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
				
			bBreakoutEarly = FALSE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TS_BREAKOUT_EARLY")
			eTriggerState = TS_BREAKOUT_EARLY
			
		BREAK
		
		CASE TS_BREAKOUT_EARLY
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			CHECK_FOR_BREAKOUT()
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TS_RESET due to scene ending")
				eTriggerState = TS_RESET
			ELSE
				
				IF NOT bTakenHit
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
					ELSE
						Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL)
					ENDIF
					iTimesUsed++
					bTakenHit = TRUE
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TS_BREAKOUT_EARLY Player_Takes_Weed_Hit called ")
				ENDIF
				
				IF bBreakoutEarly
					
					//CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: Player wants to break out of the exit anim")
					
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)				
							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: Found WalkInterruptible at ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: TS_BREAKOUT_EARLY -> TS_RESET (EXIT BREAKOUT!)")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
					
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "ScriptEvent", fReturnStartPhase, fReturnEndPhase)	
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)				
							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: Found ScriptEvent at ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: TS_BREAKOUT_EARLY -> TS_RESET (EXIT BREAKOUT!)")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
				
				// Frankin has an exit cam
				IF bIsFranklin
					IF NOT bTriggeredExitCam
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TRIGGERING EXIT CAM")
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						// Normal gameplay camera
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							DESTROY_CAM(CamIndex)						
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING())
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_PITCH())
							
							SHAKE_GAMEPLAY_CAM("HAND_SHAKE", DEFAULT_CAM_SHAKE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bTriggeredExitCam = TRUE					
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE TS_RESET
			
			// Clear tasks
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
						
			// Normal gameplay camera
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
				STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
				STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
			ENDIF
			
			// Increment tracking stat
			IF STAT_GET_INT(NUM_SH_BONG_SMOKED, iUsageStat)
				STAT_SET_INT(NUM_SH_BONG_SMOKED, iUsageStat+1)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mTrigger)
				IF NOT IS_ENTITY_ATTACHED(mTrigger)
					FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				ENDIF
			ENDIF
						
			// Reset Flags
			bBongSmoke     		= FALSE
			bLighterFlame  		= FALSE
			bLighterSparks 		= FALSE
			bBreakoutEarly 		= FALSE
			bTriggeredExitCam 	= FALSE
			bTakenHit 			= FALSE
			bConvoKilled		= FALSE
						
			// Restore control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_SOMEONE_KNOCKED_BONG_OVER()
	
	VECTOR vBongRot = GET_ENTITY_ROTATION(GET_CLOSEST_OBJECT_OF_TYPE(vTriggerPos, 2.0, PROP_BONG_01, FALSE))
	
	IF (vBongRot.x < 0.1) AND (vBongRot.x > -0.1)
	AND (vBongRot.y < 0.1) AND( vBongRot.y > -0.1)  
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: SOMEONE KNOCKED THE BONG OVER")
	RETURN TRUE
	
ENDFUNC

PROC MP_UPDATE_BONG_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0
	FLOAT fReturnStartPhase, fReturnEndPhase
	
	SWITCH eMPTriggerState
		
		CASE MP_TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = ci_APT_ACT_BONG
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BONG )	
			AND NOT HAS_SOMEONE_KNOCKED_BONG_OVER()
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(mTrigger)									
				IF iContextIntention = NEW_CONTEXT_INTENTION
					REGISTER_CONTEXT_INTENTION(iContextIntention, CP_MEDIUM_PRIORITY, "SA_BONG2")
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_WAIT_FOR_PLAYER")		
				eMPTriggerState = MP_TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK
		
		CASE MP_TS_WAIT_FOR_PLAYER
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BONG )
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
				AND NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
				AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
				AND NOT HAS_SOMEONE_KNOCKED_BONG_OVER()
				
					IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntention)
						
						iPersonalAptActivity = ci_APT_ACT_BONG
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BONG2")
							CLEAR_HELP(TRUE)
						ENDIF
						
						RELEASE_CONTEXT_INTENTION(iContextIntention)
						DISABLE_INTERACTION_MENU()							
				
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						
						IF DOES_ENTITY_EXIST(mTrigger)
							IF NOT IS_ENTITY_ATTACHED(mTrigger)
								FREEZE_ENTITY_POSITION(mTrigger, FALSE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(mLighter)
							IF NOT IS_ENTITY_ATTACHED(mLighter)
								FREEZE_ENTITY_POSITION(mLighter, FALSE)
							ENDIF
						ENDIF
						
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 5000, fTriggerHead, DEFAULT_NAVMESH_RADIUS)

						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_TRIGGER_SMOKE_ANIM")	
						eMPTriggerState = MP_TS_TRIGGER_SMOKE_ANIM
					ENDIF
					
				ELSE
					RELEASE_CONTEXT_INTENTION(iContextIntention)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BONG2")
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_PLAYER_OUT_OF_RANGE")
					eMPTriggerState = MP_TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_TS_TRIGGER_SMOKE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_ALL_MP_HUD_THIS_FRAME()
		
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
				
				REMOVE_PLAYER_MASK()
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
								
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, sPedSmokeAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				IF NOT PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sBongSmokeAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: CANNOT PLAY BONG ANIM.")
				ENDIF
				IF NOT PLAY_SYNCHRONIZED_ENTITY_ANIM(mLighter, mSynchedScene, sLighterSmokeAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)	
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: CANNOT PLAY LIGHTER ANIM.")
				ENDIF
 			
//				Initialise camera
//				CamIndex = CREATE_CAMERA()
//				
//				IF DOES_CAM_EXIST(CamIndex)
//					SET_CAM_ACTIVE(CamIndex, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_CAM_PARAMS(CamIndex, vCamPos, vCamHead, 35.0)
//					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
//
//				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: USING ANIMATED CAMERA!")
//				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
//					
//				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, GET_CAMERA_FOR_THIS_ACTIVITY(), sAnimDict)
//				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, "bong_cam", sAnimDict)		
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_CHECK_SMOKE_ANIM")
				eMPTriggerState = MP_TS_CHECK_SMOKE_ANIM
			ENDIF
			
		BREAK
		
		CASE MP_TS_CHECK_SMOKE_ANIM
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_RUN_SMOKE_ANIM")
				eMPTriggerState = MP_TS_RUN_SMOKE_ANIM
			ENDIF	
			
		BREAK
		
		CASE MP_TS_RUN_SMOKE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			//CHECK_FOR_BREAKOUT()
			
			// Monitor for particle effects/early breakout
			//IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.95
			
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
						
				IF bSetupPTFX
					// Lighter flame
					IF bLighterFlame = FALSE
						IF fScenePhase > GET_PHASE_TIME_FOR_LIGHTER_FLAME()
							ptfxLighter = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_lighter_flame", mLighter, GET_LIGHTER_FLAME_POS(), <<0.0,0.0,0.0>>)
							bLighterFlame = TRUE
						ENDIF
					ELSE
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLighter)
							IF fScenePhase > GET_PHASE_TIME_FOR_LIGHTER_FLAME(FALSE)
								STOP_PARTICLE_FX_LOOPED(ptfxLighter)
							ENDIF
						ENDIF
					ENDIF
					
					// Lighter sparks
					IF bLighterSparks = FALSE
						IF fScenePhase > GET_PHASE_TIME_FOR_LIGHTER_SPARKS()
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sh_lighter_sparks", mLighter, << 0.0, 0.0, 0.06 >>, << 0.0, 0.0, 0.0 >>)
							bLighterSparks = TRUE
						ENDIF
					ENDIF
					
					// Bong smoke fx
					IF bBongSmoke = FALSE
						IF fScenePhase > GET_PHASE_TIME_FOR_BONG_SMOKE()
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
							bBongSmoke = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF fScenePhase > STONED_PHASE
					IF NOT bTakenHit
						Player_Takes_Weed_Hit(PLAYER_PED_ID())
						//iTimesUsed++
						bTakenHit = TRUE
					ENDIF
				ENDIF			
				
//				IF NOT bTriggeredCam2
//					IF fScenePhase > fCam2PhaseTime
//					
//						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TRIGGERING CAM 2")
//						
//						IF DOES_CAM_EXIST(CamIndex)
//							SET_CAM_PARAMS(CamIndex, vCam2Pos, vCam2Head, 40.0)
//							SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//						ENDIF
//									
//						bTriggeredCam2 = TRUE
//						
//					ENDIF
//				ENDIF
				
//				IF NOT bTriggeredExitCam
//					IF fScenePhase > fExitCamPhaseTime
//						
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					
//						// Normal gameplay camera
//						IF DOES_CAM_EXIST(CamIndex)
//							DESTROY_CAM(CamIndex)
//						ENDIF
//													
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING())
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_PITCH())
//						
//						SHAKE_GAMEPLAY_CAM("HAND_SHAKE", DEFAULT_CAM_SHAKE)
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
//						
//						bTriggeredExitCam = TRUE	
//						
//					ENDIF
//				ENDIF
				
			// Synchronised scene has finished
			ELSE						
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_RESET")
				iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
				eMPTriggerState = MP_TS_RESET
			ENDIF
		BREAK
				
		CASE MP_TS_TRIGGER_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: APPLY BREAKOUT ANIMATION")
								
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, sBongExitAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			// Apply the exit anim to the player and all props he uses.
			IF bEntityHasExitAnim
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sBongExitAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mLighter, mSynchedScene, sLighterExitAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				IF NOT bIsFranklin
					//SET_CAM_ANIM_CURRENT_PHASE(CamIndex, 1.0)
				ENDIF
			ENDIF
			
			bBreakoutEarly = FALSE
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_BREAKOUT_EARLY")
			eMPTriggerState = MP_TS_BREAKOUT_EARLY
			
		BREAK
		
		CASE MP_TS_BREAKOUT_EARLY
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_BREAKOUT()
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_RESET")
				eMPTriggerState = MP_TS_RESET
			ELSE
				
				IF NOT bTakenHit
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
					ELSE
						Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL)
					ENDIF
					//iTimesUsed++
					bTakenHit = TRUE
				ENDIF
					
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Looking for WalkInteruptable")
				IF FIND_ANIM_EVENT_PHASE(sAnimDict, sPedExitAnim, "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Found WalkInterruptible")
					IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)				
						// Clear tasks
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CPRINTLN(DEBUG_AMBIENT, "[SA] Bong: MP_TS_RESET (EXIT BREAKOUT!)")
						eMPTriggerState = MP_TS_RESET
					ENDIF
				ENDIF
				
				// Frankin has an exit cam
//				IF NOT bTriggeredExitCam
//					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: TRIGGERING EXIT CAM")
//					
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					
//					// Normal gameplay camera
//					IF DOES_CAM_EXIST(CamIndex)
//						DESTROY_CAM(CamIndex)
//					ENDIF
//												
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING())
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_PITCH())
//					SHAKE_GAMEPLAY_CAM("HAND_SHAKE", DEFAULT_CAM_SHAKE)
//					
//					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
//					
//					bTriggeredExitCam = TRUE					
//				ENDIF
				
			ENDIF
		BREAK
		
		CASE MP_TS_RESET
													
//			// Normal gameplay camera
//			IF DOES_CAM_EXIST(CamIndex)
//				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
//			ENDIF
			
			RESTORE_PLAYER_MASK()
			
			// Clear tasks
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			INCREMENT_MP_APT_SMOKE_COUNTER()
			
			IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
				STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
				STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
			ENDIF
			
//			// Increment tracking stat
//			IF STAT_GET_INT(NUM_SH_BONG_SMOKED, iUsageStat)
//				STAT_SET_INT(NUM_SH_BONG_SMOKED, iUsageStat+1)
//			ENDIF
			
			IF DOES_ENTITY_EXIST(mTrigger)
				IF NOT IS_ENTITY_ATTACHED(mTrigger)
					FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				ENDIF
			ENDIF
			
			// Reset Flags
			bBongSmoke     		= FALSE
			bLighterFlame  		= FALSE
			bLighterSparks 		= FALSE
			bBreakoutEarly 		= FALSE
			bTriggeredExitCam 	= FALSE
			bTakenHit 			= FALSE
			
			ENABLE_INTERACTION_MENU()
			
			// Restore control				// cntrl vsble clrtk colsn fze ps taget invinc damage allow cam
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_PLAYER_OUT_OF_RANGE")
			eMPTriggerState = MP_TS_PLAYER_OUT_OF_RANGE
			
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		OR Is_Player_On_Or_Triggering_Any_Mission()
		OR MPGlobalsAmbience.bRunningFmIntroCut
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Default Cleanup")
			CLEANUP_BONG_ACTIVITY()
		ENDIF
	
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
				
		IF DOES_ENTITY_EXIST(mObjectIn)
				
			mTrigger = mObjectIn
			mTriggerModel = GET_ENTITY_MODEL(mTrigger)
			
//			IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(mTrigger)
//				NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
//			ENDIF
			
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			INT iCurProp = GET_PROPERTY_SIZE_TYPE(iCurProperty)
			
			SWITCH iCurProp
				CASE PROP_SIZE_TYPE_SMALL_APT		
					bInLowApt = TRUE
				BREAK
				DEFAULT
					bInLowApt = FALSE
				BREAK
			ENDSWITCH
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Bong: Current property is ", iCurProperty)
			
			sAnimDict = "mp_safehousebong@"
			
			GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
			
			vTriggerPos = tempPropertyStruct.house.activity[SAFEACT_BONG].vTriggerVec 
			fTriggerHead = tempPropertyStruct.house.activity[SAFEACT_BONG].fTriggerRot
			vScenePos = tempPropertyStruct.house.activity[SAFEACT_BONG].vSceneVec
			vSceneHead = tempPropertyStruct.house.activity[SAFEACT_BONG].vSceneRot
//			vCamPos = tempPropertyStruct.house.activity[SAFEACT_BONG].vCamVec
//			vCamHead = tempPropertyStruct.house.activity[SAFEACT_BONG].vCamRot
//			vCam2Pos = tempPropertyStruct.house.activity[SAFEACT_BONG].vCam2Vec
//			vCam2Head = tempPropertyStruct.house.activity[SAFEACT_BONG].vCam2Rot
//			vExitCamPos = tempPropertyStruct.house.activity[SAFEACT_BONG].vExitCamVec
//			vExitCamHead = tempPropertyStruct.house.activity[SAFEACT_BONG].vExitCamRot
			bIsFranklin = tempPropertyStruct.house.activity[SAFEACT_BONG].bFranklinBong
			
			IF fTriggerHead < 0.0
				fTriggerHead+= 360.0
			ENDIF
			
			IF CAN_REGISTER_MISSION_OBJECTS(2)
				// Get a handle to the  lighter
				mLighter = GET_CLOSEST_OBJECT_OF_TYPE(vTriggerPos, 2.0, P_CS_LIGHTER_01, FALSE)
//				IF DOES_ENTITY_EXIST(mLighter) 
//					IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(mLighter)
//						NETWORK_REGISTER_ENTITY_AS_NETWORKED(mLighter)
//						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Bong: Registering Lighter as Network object.")
//					ENDIF
//				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mTrigger)
				IF NOT IS_ENTITY_ATTACHED(mTrigger)
					FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mLighter)
				IF NOT IS_ENTITY_ATTACHED(mLighter)
					FREEZE_ENTITY_POSITION(mLighter, TRUE)
				ENDIF
			ENDIF
		ENDIF
	
	ELSE
		
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DIRECTOR) 
		OR IS_PED_INJURED(PLAYER_PED_ID())		//#1686982
		OR IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
		OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())
		OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Default Cleanup")
			CLEANUP_BONG_ACTIVITY()
		ENDIF
		
		IF DOES_ENTITY_EXIST(mObjectIn)
			mTrigger = mObjectIn		
			mTriggerModel = GET_ENTITY_MODEL(mTrigger)
			
			IF CAN_PLAYER_USE_THIS_OBJECT()
			
				sAnimDict = GET_ANIM_DICT_FOR_THIS_ACTIVITY()
				
				IF bIsFranklin
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Franklins bong anims loaded")
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Michaels bong anims loaded")
				ENDIF
				
				IF DOES_ENTITY_EXIST(mTrigger)
					IF NOT IS_ENTITY_ATTACHED(mTrigger)
						FREEZE_ENTITY_POSITION(mTrigger, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: Ready for launch...")
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
			
		IF DOES_ENTITY_EXIST(mTrigger)
		AND IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
		AND NOT IS_ENTITY_DEAD(mTrigger)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND CAN_PLAYER_USE_THIS_OBJECT()
		AND IS_INTERIOR_CORRECT(mTrigger)
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
											
			SWITCH eState
									
				CASE AS_LOAD_ASSETS
					IF HAVE_ASSETS_LOADED()
						eState = AS_RUN_ACTIVITY
					ELSE
						IF g_bInMultiplayer AND SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
							CLEANUP_BONG_ACTIVITY()
						ENDIF
					ENDIF
				BREAK
				
				CASE AS_RUN_ACTIVITY
					//IF g_OldSafehouseActivites = TRUE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF g_bInMultiplayer
								IF NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
									IF NOT IS_LOW_APT_SOFA_IN_USE()
										MP_UPDATE_BONG_ACTIVITY()
										APPLY_USAGE_EFFECTS()
									ENDIF
								ELSE
									CLEANUP_BONG_ACTIVITY()
								ENDIF
							ELSE
								UPDATE_BONG_ACTIVITY()
								APPLY_USAGE_EFFECTS()
							ENDIF
						ENDIF
					//ENDIF
				BREAK
				
				CASE AS_END
				BREAK
			ENDSWITCH
		ELSE					
			CLEANUP_BONG_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
