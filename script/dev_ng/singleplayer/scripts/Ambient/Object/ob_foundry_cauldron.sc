// Rob Bray - ptfx on cauldron in foundry

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"



// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun

PTFX_ID steamPTFX
OBJECT_INDEX bucketObject

// Clean Up
PROC missionCleanup()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(steamPTFX)
		STOP_PARTICLE_FX_LOOPED(steamPTFX)
	ENDIF
	IF DOES_ENTITY_EXIST(bucketObject)
		SET_OBJECT_AS_NO_LONGER_NEEDED(bucketObject)
	ENDIF

	printDebugString("ob_foundry_cauldron Terminated >>>>>>>>>>>>>>>>>\n")
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oFoundryCauldron)

IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
	missionCleanup()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oFoundryCauldron)
		INTERIOR_INSTANCE_INDEX playerInterior
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oFoundryCauldron)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finaleC1")) = 0
			SWITCH ambStage
				CASE ambCanRun
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						playerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						IF IS_VALID_INTERIOR(playerInterior)
							IF IS_INTERIOR_READY(playerInterior)
								IF IS_INTERIOR_SCENE()
									REQUEST_PTFX_ASSET()
									ambStage = ambRunning
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ambRunning
					IF HAS_PTFX_ASSET_LOADED()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT DOES_ENTITY_EXIST(bucketObject)
								bucketObject = GET_CLOSEST_OBJECT_OF_TYPE(<<1090,-1996,39>>, 100, V_ILEV_FOUND_CRANEBUCKET)
							ENDIF
						
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(steamPTFX)
								IF DOES_ENTITY_EXIST(bucketObject)
									playerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
									IF IS_VALID_INTERIOR(playerInterior)							
										IF IS_INTERIOR_READY(playerInterior)
											IF IS_INTERIOR_SCENE()
												steamPTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_obfoundry_cauldron_steam", bucketObject, <<0,0,0>>, <<0,0,0>>)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					//PRINTSTRING("running foundry cauldron script") PRINTNL()
				BREAK
				CASE ambEnd
				
				BREAK
			ENDSWITCH 
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

