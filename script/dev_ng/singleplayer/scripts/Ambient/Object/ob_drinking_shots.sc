// Includes
USING "ob_safehouse_common.sch"
USING "context_control_public.sch"
USING "rgeneral_include.sch"
USING "net_realty_mess_include.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_drinking_shots.sc
//		DESCRIPTION		:	Whiskey drinking and beers at Trevor's trailer
//
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE,
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_WAIT_FOR_ANIM_TO_RUN,
	TS_WAIT_FOR_MORE_INPUT,
	TS_TRIGGER_EXIT_ANIM,
	TS_RUN_EXIT_ANIM,
	TS_RESET,
	TS_TEMP_BEER_STATE
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT 	WHISKEY_AREA_WIDTH 	2.0
CONST_INT 		MAX_DRINKS 			6

STRUCT BEER_BOTTLE

	OBJECT_INDEX mObj
	VECTOR vPos
	VECTOR vRot
	BOOL bCreated

ENDSTRUCT

BEER_BOTTLE mBottle[MAX_DRINKS]

// Variables
BOOL bAnimsLoaded 		= FALSE
BOOL bBreakoutEarly		= FALSE
BOOL bDoneGlassAnim		= FALSE
BOOL bEnterAnimRunning 	= FALSE
BOOL bIdleAnimRunning 	= FALSE
BOOL bTakenHit			= FALSE
BOOL bInMidApt			= FALSE

BOOL bPCCOntrolsSetup	= FALSE

CAMERA_INDEX CamIndex


FLOAT fTriggerHead

INT iFarCam
INT iCurProperty
INT mSynchedScene
INT iExitAnimDuration
INT iUsageStat

INTERIOR_INSTANCE_INDEX iInterior 

OBJECT_INDEX mTriggerBottle
OBJECT_INDEX mWhiskeyBottle 

// Script Cameras
VECTOR	vCamPos[MAX_CAMS]
VECTOR	vCamHead[MAX_CAMS]
FLOAT	fCamFov[MAX_CAMS]
FLOAT	fRelHead[MAX_CAMS]
FLOAT 	fRelPitch[MAX_CAMS]

// Use a different model for the dummy bottles
MODEL_NAMES mBottleModel = PROP_CS_BEER_BOT_01

STRING sAnimDict		

STRING sPedEnterAnim	= "enter"
STRING sGlassEnterAnim	= "enter_glass"
STRING sBottleEnterAnim = "enter_bot"

STRING sHelpText 		= "SA_WHSKY"
STRING sInstructions	= "SA_SHOT2"

VECTOR vTriggerPos
VECTOR vScenePos
VECTOR vSceneHead

VECTOR vAreaAPos
VECTOR vAreaBPos

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------

// PC control scheme
PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC INIT_CAMERAS()
	
	CPRINTLN(DEBUG_AMBIENT, "INIT_CAMERAS")
	
	INT i = 0
	REPEAT MAX_CAMS i 
		vCamPos[i] = <<0.0, 0.0, 0.0>>
		vCamHead[i] = <<0.0, 0.0, 0.0>>
		fCamFov[i] = 0.0
	ENDREPEAT
	
//	vExitCamPos = <<0.0, 0.0, 0.0>>
//	vExitCamRot = <<0.0, 0.0, 0.0>>
//	fExitCamFov = 0.0

	SWITCH mTriggerModel

		// Beers
		CASE PROP_RADIO_01
			IF bIsTrevor
				
				vCamPos[0] = <<1978.0763, 3820.8691, 33.2061>>	// To the left of Trevor
				vCamHead[0] = <<15.6025, -0.1642, 86.2794>> 	
				fCamFov[0] = 33.0
				fRelHead[0] = -100.4062
				fRelPitch[0] = -0.0806
				vCamPos[1] = <<1974.1635, 3820.4739, 33.5729>> 	// To the right of Trevor
				vCamHead[1] = <<5.8330, -0.4169, -78.1203>>	
				fCamFov[1] = 36.4
				fRelHead[1] = 126.7381
				fRelPitch[1] = 12.4420
				vCamPos[2] = <<1976.0, 3818.9, 33.7>>
				vCamHead[2] = <<-0.9, 0.0, -17.6>>
				fCamFov[2] = 35.9
				fRelHead[2] = -174.5658
				fRelPitch[2] = -9.0580
				
				// TODO: Find out the length of Trevor's exit anim
				iExitAnimDuration = 4333
				
			ELSE
				vCamPos[0] = <<1976.3, 3822.3, 33.5>> 	// Behind Michael to right // Doesnt need interp
				vCamHead[0] = <<8.5, 0.0, -165.3>>
				fCamFov[0] = 37.4
				fRelHead[0] = 110.6507
				fRelPitch[0] = 6.9751
				vCamPos[1] = <<1977.3, 3822.3, 34.0>>	// Behind michaels to left - needs interp
				vCamHead[1] = <<-5.5, 0.0, 175.3>>
				fRelHead[1] = 17.1708
				fRelPitch[1] = -18.3493
				fCamFov[1] = 40.0
				vCamPos[2] = <<1978.7, 3819.2, 33.5>>	// In front of Micheal, to his left // Doesn't need interp.
				vCamHead[2] = <<4.7, 0.0, 46.8>>
				fCamFov[2] = 40.0
				fRelHead[2] = -121.4617
				fRelPitch[2] = 6.6625
				vCamPos[3] = <<1975.5, 3820.1, 33.6>>	// in front, to michael's right - needs interp
				vCamHead[3] = <<5.5, 0.0, -65.5>>
				fCamFov[3] = 35.0	
				fRelHead[3] = 108.5816
				fRelPitch[3] = 12.9751
				
				iExitAnimDuration = 4333
			ENDIF
		BREAK		
		
		// Whiskey
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF NOT g_bInMultiplayer
				IF bIsTrevor
					// Fix for B*1885245 - camera clipping through windowframe.
					vCamPos[0] = <<-1152.7435, -1522.6039, 10.7997>>	// <<-1152.7, -1522.7, 10.8>>
					vCamHead[0] = <<3.2390, -0.0758, 117.0767>>			// <<4.4, 0.0, 117.4 >>
					fCamFov[0] = 38.9
					fRelHead[0] = -98.0916
					fRelPitch[0] = -5.6388
	//				vExitCamPos = <<-1154.5, -1521.8, 11.2>>
	//				vExitCamRot = <<-1.8, 0.0, -158.5>>
	//				fExitCamFov = 50.0

					iExitAnimDuration = 4333

				ELSE
					// Move camera so pizza box doesn't obscure..
					IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_3_CALLS_DONE)
						vCamPos[0] = <<-800.8, 185.8, 72.7>>
						vCamHead[0] = <<6.5, 0.0, -171.3>>
						fCamFov[0] = 34.1
						fRelHead[0] = -175.1866
						fRelPitch[0] = 2.5942
					ELSE
						vCamPos[0] = <<-801.5, 185.3, 72.7>>
						vCamHead[0] = <<7.2, 0.0, -138.6>>
						fCamFov[0] = 34.1
						fRelHead[0] = -175.1866
						fRelPitch[0] = 2.5942
					ENDIF
					vCamPos[1] = <<-801.7059, 183.0696, 72.7336>>
					vCamHead[1] = <<7.1986, -0.2811, -47.3614>>
					fCamFov[1] = 34.1
					fRelHead[1] = -86.9298
					fRelPitch[1] = 1.2308
					vCamPos[2] = <<-799.2, 185.2, 72.8>>
					vCamHead[2] = <<5.0, 0.0, 124.5>>
					fCamFov[2] = 33.7
					fRelHead[2] = 110.6507
					fRelPitch[2] = 6.9751
					
					iExitAnimDuration = 4333
					
				ENDIF
			ELSE
			
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

// PURPOSE
//	Set the position and rotation of the discarded beer bottles
// 	Same for both Trev and Michael.
PROC INIT_BOTTLE_POSITIONS()
			
	INT i = 0
	REPEAT MAX_DRINKS i 
		mBottle[i].bCreated = FALSE
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Creates a dummy beer bottle on the floor of Trevors trailer
PROC CREATE_DUMMY_BEER_BOTTLE()
	
	CPRINTLN(DEBUG_AMBIENT, "Creating dummy beer bottle  no offset : ", iTimesUsed, " vPos : ", mBottle[iTimesUsed].vPos, " vRot : ", mBottle[iTimesUsed].vRot)
	
	mBottle[iTimesUsed].mObj = CREATE_OBJECT_NO_OFFSET(mBottleModel, mBottle[iTimesUsed].vPos, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(mBottle[iTimesUsed].mObj)
		SET_ENTITY_ROTATION(mBottle[iTimesUsed].mObj, mBottle[iTimesUsed].vRot)
		ACTIVATE_PHYSICS(mBottle[iTimesUsed].mObj)
		mBottle[iTimesUsed].bCreated = TRUE
		SET_OBJECT_AS_NO_LONGER_NEEDED(mBottle[iTimesUsed].mObj)
	ENDIF
	
ENDPROC

/// PURPOSE
///    Gets the furthest camera from the current gameplay cams position (to reduce the visible jump in the players pos).
FUNC INT GET_FURTHEST_CAM()
	
	//CPRINTLN(DEBUG_AMBIENT, "GET_FURTHEST_CAM")
	
	FLOAT fDiff
	INT i = 0
	INT iFurthestCam = 0
	FLOAT fFurthestDist = 0.0

	VECTOR vGameplayCamPos = GET_GAMEPLAY_CAM_COORD()
	
	REPEAT MAX_CAMS i
		IF NOT ARE_VECTORS_ALMOST_EQUAL(vCamPos[i], <<0.0, 0.0, 0.0>>)
						
			fDiff = GET_DISTANCE_BETWEEN_COORDS(vGameplayCamPos, vCamPos[i])
			 
			IF fDiff > fFurthestDist
				fFurthestDist = fDiff
				iFurthestCam = i // Hack this to test specific cam positions. (Michael cam 1 lets you see in box)
			ENDIF
		ENDIF
	ENDREPEAT
	
//	IF mTriggerModel = PROP_RADIO_01
//		IF NOT bIsTrevor
//			IF iFurthestCam = 1
//				vExitCamPos = <<1977.4473, 3821.9509, 34.0488>>
//				vExitCamRot = <<-6.1802, -0.0876, 168.0278>>
//				fExitCamFov = 40.0
//				bNeedInterp = TRUE
//			ENDIF
//			
//			IF iFurthestCam = 3
//				vExitCamPos = <<1974.9725, 3819.8628, 33.5419>> 
//				vExitCamRot = <<5.4447, 0.0, -65.6456>>
//				fExitCamFov = 35.0
//				bNeedInterp = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, " RETURNING ", iFurthestCam)
	RETURN iFurthestCam
		
ENDFUNC

/// PURPOSE:
///    Requests and loads the correct animation dictionary.
FUNC BOOL HAS_SHOT_DRINKING_DICT_LOADED()
	
	// Request animations
	REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
	
	WHILE NOT HAS_ANIM_DICT_LOADED(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
		WAIT(0)
	ENDWHILE
	bAnimsLoaded = TRUE
	//CPRINTLN(DEBUG_AMBIENT, "SHOT DRINKING DICT LOADED")
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Get the correct idle anim based on how many shots the player has had.
FUNC STRING GET_IDLE_ANIM_FOR_THIS_SHOT()

	STRING sAnim

	SWITCH (mTriggerModel)
		
		CASE PROP_RADIO_01
			IF bIsTrevor
				sAnim = "base"
			ELSE
				sAnim = "base_michael"
			ENDIF
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
			AND NOT g_bInMultiplayer
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "base_2_trevor" BREAK
					CASE 1  sAnim = "base_2_trevor" BREAK
					CASE 2  sAnim = "base_3_trevor" BREAK
					CASE 3  sAnim = "base_4_trevor" BREAK
					DEFAULT sAnim = "base_5_trevor" BREAK
				ENDSWITCH
			ELSE
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "first_shot_base"	BREAK
					CASE 1  sAnim = "first_shot_base" 	BREAK
					CASE 2  sAnim = "second_shot_base"  BREAK 
					CASE 3  sAnim = "third_shot_base" 	BREAK 
					CASE 4  sAnim = "fourth_shot_base" 	BREAK 
					DEFAULT sAnim = "fifth_shot_base"  	BREAK
				ENDSWITCH
			ENDIF
		BREAK
				
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Get the correct idle anim based on how many shots the player has had.
///    Played on the prop used as the trigger, normally the glass.
FUNC STRING GET_IDLE_ANIM_FOR_THIS_ENTITY()

	STRING sAnim

	SWITCH (mTriggerModel)
		
		CASE PROP_RADIO_01
			IF bIsTrevor
				sAnim = "base_beer"
			ELSE
				sAnim = "base_bottle"
			ENDIF
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
			AND NOT g_bInMultiplayer
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "base_shotglass"			BREAK
					CASE 1  sAnim = "base_2_shotglass"			BREAK
					CASE 2  sAnim = "base_3_shotglass" 			BREAK
					CASE 3  sAnim = "base_4_shotglass" 			BREAK
					DEFAULT sAnim = "base_5_shotglass" 			BREAK
				ENDSWITCH
			ELSE
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "first_shot_base_glass"  	BREAK
					CASE 1  sAnim = "second_shot_base_glass" 	BREAK
					CASE 2  sAnim = "third_shot_base_glass"  	BREAK 
					CASE 3  sAnim = "fourth_shot_base_glass" 	BREAK 
					DEFAULT sAnim = "fifth_shot_base_glass"  	BREAK
				ENDSWITCH
			ENDIF
		BREAK
						
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Get the correct idle anim based on how many shots the player has had.
///    Played on the secondary prop, either the whiskey bottle or the beer cap.
FUNC STRING GET_IDLE_ANIM_FOR_SECOND_ENTITY()

	STRING sAnim

	SWITCH (mTriggerModel)
		
		CASE PROP_RADIO_01
			IF NOT bIsTrevor
				sAnim = "base_cap"
//			ELSE
//				sAnim = "base_cam"
			ENDIF
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
			AND NOT g_bInMultiplayer
			
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "base_bottle"		BREAK
					CASE 1  sAnim = "base_2_bottle" 	BREAK
					CASE 2  sAnim = "base_3_bottle" 	BREAK
					CASE 3  sAnim = "base_4_bottle" 	BREAK
					DEFAULT sAnim = "base_5_bottle" 	BREAK
				ENDSWITCH
			ELSE
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "first_shot_base_bot"	BREAK
					CASE 1  sAnim = "second_shot_base_bot" 	BREAK
					CASE 2  sAnim = "third_shot_base_bot"  	BREAK 
					CASE 3  sAnim = "fourth_shot_base_bot" 	BREAK 
					DEFAULT sAnim = "fifth_shot_base_bot"	BREAK
				ENDSWITCH
			ENDIF
		BREAK
					
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Is there an animated camera for this activity
FUNC BOOL DOES_IDLE_CAMERA_EXIST_FOR_THIS_ACTIVITY()

	SWITCH mTriggerModel
		
		CASE PROP_RADIO_01
			//IF bIsTrevor
				RETURN TRUE
			//ENDIF
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF NOT bIsTrevor
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Set a flag if the player wants to break out the drink anim early
PROC CHECK_FOR_BREAKOUT()

	INT iLeftX, iLeftY, iRightX, iRightY
	
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
	
	IF NOT bBreakoutEarly
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			
			CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drinking: BREAKOUT DETECTED")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vAreaAPos, vAreaBPos, WHISKEY_AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_DRINKING_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "Cleaning up...")
	SHOW_DEBUG_FOR_THIS_OBJECT()
	
	IF NOT g_bInMultiplayer
		SET_PLAYER_CLOTH_PACKAGE_INDEX( ENUM_TO_INT(CLOTH_PACKAGE_DEFAULT) )
	ENDIF
	
	// Clean up anims on all shot glass props.
	IF DOES_ENTITY_EXIST(GET_SYNCHED_SCENE_OBJECT())
	AND bAnimsLoaded
	AND DOES_ENTITY_HAVE_DRAWABLE(GET_SYNCHED_SCENE_OBJECT())
//		IF mTriggerModel = P_TUMBLER_02_S1
//		OR mTriggerModel = P_TUMBLER_CS2_S 
			PLAY_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), GET_IDLE_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, FALSE, TRUE)
//		ENDIF
	ENDIF
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	IF bNeedSecondProp
	AND bAnimsLoaded
		IF mTriggerModel = P_TUMBLER_02_S1
		OR mTriggerModel = P_TUMBLER_CS2_S 
		OR mTriggerModel = P_TUMBLER_CS2_S_TREV
		OR NOT bIsTrevor
			IF DOES_ENTITY_EXIST(GET_SECONDARY_ENTITY())
				IF DOES_ENTITY_HAVE_DRAWABLE(GET_SECONDARY_ENTITY())
					PLAY_ENTITY_ANIM(GET_SECONDARY_ENTITY(), GET_IDLE_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
							
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
		CLEAR_HELP(TRUE)
	ENDIF
								
	IF bAnimsLoaded
		REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
		bAnimsLoaded = FALSE
	ENDIF
	
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	CLEANUP_PC_CONTROLS()

	IF g_bInMultiplayer
		
		// Restore control
		IF bSafehouseSetControlOff 
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		// End script
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	ELSE
		
		// Restore control and remove motion blur
		SAFE_RESTORE_PLAYER_CONTROL()
			
		// End script
		TERMINATE_THIS_THREAD()
	ENDIF
	
ENDPROC

FUNC STRING GET_FACIAL_DRINK_ANIM()

	STRING sAnim
	
	IF bIsTrevor
	AND NOT g_bInMultiplayer
	
		SWITCH mTriggerModel
			
			// IG 6
			CASE P_TUMBLER_02_S1
			CASE P_TUMBLER_CS2_S
			CASE P_TUMBLER_CS2_S_TREV
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "enter_trevor_facial"	BREAK
					CASE 1  sAnim = "drink_2_trevor_facial"	BREAK
					CASE 2  sAnim = "drink_3_trevor_facial"	BREAK
					CASE 3  sAnim = "drink_4_trevor_facial"	BREAK
					DEFAULT sAnim = "drink_5_trevor_facial"	BREAK
				ENDSWITCH
			BREAK
			
			CASE PROP_RADIO_01
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "enter_facial"		BREAK
					CASE 1  sAnim = "drink_1_facial"	BREAK
					CASE 2  sAnim = "drink_2_facial"	BREAK
					CASE 3  sAnim = "drink_3_facial"	BREAK
					CASE 4	sAnim = "drink_4_facial"	BREAK
					DEFAULT sAnim = "drink_5_facial"	BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
	ELSE
		SWITCH mTriggerModel
			
			CASE P_TUMBLER_02_S1
			CASE P_TUMBLER_CS2_S
			CASE P_TUMBLER_CS2_S_TREV
				SWITCH (iTimesUsed)		
					CASE 0	sAnim = "enter_facial"			BREAK
					CASE 1  sAnim = "first_shot_facial"		BREAK
					CASE 2  sAnim = "second_shot_facial"	BREAK
					CASE 3  sAnim = "third_shot_facial"		BREAK
					CASE 4	sAnim = "fourth_shot_facial"	BREAK
					DEFAULT sAnim = "fifth_shot_facial"		BREAK
				ENDSWITCH
			BREAK
			
			CASE PROP_RADIO_01
				SWITCH (iTimesUsed)		
					CASE 0  sAnim = "enter_michael_facial"		BREAK
					CASE 1  sAnim = "drink_1_michael_facial"	BREAK
					CASE 2  sAnim = "drink_2_michael_facial"	BREAK
					CASE 3  sAnim = "drink_3_michael_facial"	BREAK
					DEFAULT sAnim = "drink_3_michael_facial"	BREAK // drink_4_michael_facial is missing
				ENDSWITCH
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN sAnim

ENDFUNC

FUNC STRING GET_FACIAL_EXIT_ANIM()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	IF bIsTrevor
	AND NOT g_bInMultiplayer
	
		SWITCH mTriggerModel
			
			CASE P_TUMBLER_02_S1
			CASE P_TUMBLER_CS2_S
			CASE P_TUMBLER_CS2_S_TREV
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober_trevor_facial"				BREAK
					CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk_trevor_facial"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk_trevor_facial"		BREAK
					CASE DL_verydrunk		sAnim = "exit_very_drunk_trevor_facial"			BREAK
				ENDSWITCH
			BREAK
			
			CASE PROP_RADIO_01
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober_facial"				BREAK
					CASE DL_moderatedrunk	sAnim = "exit_1_facial"					BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk_facial"	BREAK
					CASE DL_verydrunk		sAnim = "exit_very_drunk_facial"		BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
	ELSE
		SWITCH mTriggerModel
			
			CASE P_TUMBLER_02_S1
			CASE P_TUMBLER_CS2_S
			CASE P_TUMBLER_CS2_S_TREV
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober_facial"				BREAK
					CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk_facial"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk_facial"	BREAK
					CASE DL_verydrunk		sAnim = "exit_drunk_facial"				BREAK
				ENDSWITCH
			BREAK
			
			CASE PROP_RADIO_01
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_1_michael_facial"		BREAK
					CASE DL_moderatedrunk	sAnim = "exit_2_michael_facial"		BREAK
					CASE DL_slightlydrunk	sAnim = "exit_3_michael_facial"		BREAK
					CASE DL_verydrunk		sAnim = "exit_drunk_michael_facial"	BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN sAnim

ENDFUNC

PROC MAKE_BOTTLE_VISIBLE(BOOL bVisible)

	IF mTriggerModel = PROP_RADIO_01
	
		IF bVisible
			IF NOT IS_ENTITY_VISIBLE(mTriggerBottle)
				CPRINTLN(DEBUG_AMBIENT, "-------- BOTTLE VISIBLE ---------")
				SET_ENTITY_VISIBLE(mTriggerBottle, TRUE)
			ENDIF
						
			IF bNeedSecondProp
			AND NOT IS_ENTITY_VISIBLE(GET_SECONDARY_ENTITY())
				CPRINTLN(DEBUG_AMBIENT, "-------- CAP VISIBLE ---------")
				SET_ENTITY_VISIBLE(GET_SECONDARY_ENTITY(), TRUE)
			ENDIF
		ELSE
			IF IS_ENTITY_VISIBLE(mTriggerBottle)
				CPRINTLN(DEBUG_AMBIENT, "-------- BOTTLE INVISIBLE ---------")
				mBottle[iTimesUsed].vPos = GET_ENTITY_COORDS(mTriggerBottle)
				mBottle[iTimesUsed].vRot = GET_ENTITY_ROTATION(mTriggerBottle)
				CPRINTLN(DEBUG_AMBIENT, "-------- POS AND ROT GRABBED ---------")
				SET_ENTITY_VISIBLE(mTriggerBottle, FALSE)
			ENDIF
			
			IF bNeedSecondProp
			AND IS_ENTITY_VISIBLE(GET_SECONDARY_ENTITY())
				CPRINTLN(DEBUG_AMBIENT, "-------- CAP INVISIBLE ---------")
				SET_ENTITY_VISIBLE(GET_SECONDARY_ENTITY(), FALSE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MID_APT_RADIO_IN_USE()
	
	IF bInMidApt
 	AND IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Updates drinking activity
PROC UPDATE_DRINKING_ACTIVITY()
	
	vTriggerPos  = GET_TRIGGER_VEC_FOR_THIS_OBJECT()
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT fScenePhase, fReturnStartPhase, fReturnEndPhase
	
	SWITCH eTriggerState
		CASE TS_PLAYER_OUT_OF_RANGE
		
			// Forces the tumbler into an empty state at the start of the activity.
			IF mTriggerModel = P_TUMBLER_CS2_S OR mTriggerModel = P_TUMBLER_02_S1 OR mTriggerModel = P_TUMBLER_CS2_S_TREV
				IF DOES_ENTITY_EXIST(GET_SYNCHED_SCENE_OBJECT())
					IF NOT bDoneGlassAnim
					AND DOES_ENTITY_HAVE_DRAWABLE(GET_SYNCHED_SCENE_OBJECT()) // Fix for B*1718503. Check the entity has a drawable before animating.
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, TRUE)
						bDoneGlassAnim  = TRUE
					ENDIF
				ENDIF
			ENDIF
						
			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND DO_REQUIRED_OBJECTS_EXIST()
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM_WEAPDIS") // Stop the player from triggering the activity when this message is on-screen.
			AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
			AND NOT IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "PLAYER CLOSE TO TRIGGER")
				INIT_CAMERAS()
				SHOW_DEBUG_FOR_THIS_OBJECT()
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER			
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND DO_REQUIRED_OBJECTS_EXIST()
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				AND NOT IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
					IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
											
						DISABLE_SELECTOR_THIS_FRAME()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
						CPRINTLN(DEBUG_AMBIENT, "ACTIVITY TRIGGERED...")
						SHOW_DEBUG_FOR_THIS_OBJECT()
						
						IF bIsTrevor
							START_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
						ELSE
							START_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
						ENDIF
	
						// Set global for timetable events
						IF mTriggerModel = PROP_RADIO_01
							IF bIsTrevor
								g_eCurrentSafehouseActivity = SA_TREVOR_BEER
							ELSE
								g_eCurrentSafehouseActivity = SA_MICHAEL_BEER
							ENDIF
						ELSE
							IF bIsTrevor
								g_eCurrentSafehouseActivity = SA_TREVOR_SHOTS
							ELSE
								g_eCurrentSafehouseActivity = SA_MICHAEL_SHOTS
							ENDIF
						ENDIF					
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
			
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE

						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
						DISABLE_CELLPHONE(TRUE)
						
						SET_PLAYER_CLOTH_PACKAGE_INDEX( ENUM_TO_INT(CLOTH_PACKAGE_TOP_SKINNED)  )
									
						CPRINTLN(DEBUG_AMBIENT, "TS_GRAB_PLAYER")
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
			
			CPRINTLN(DEBUG_AMBIENT, "START_SYNC_SCENE")
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Temp: Reset the anims to match up again...
			iTimesUsed = 0
			
			IF mTriggerModel = PROP_RADIO_01
			AND NOT bIsTrevor
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
			ENDIF
						
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			
			// Apply drinking anims to player and glass
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			IF mTriggerModel = PROP_RADIO_01
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTriggerBottle, mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			ELSE
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), GET_FACIAL_DRINK_ANIM(), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
						
			// Secondary prop - bottle (or bottle prop)
			IF bNeedSecondProp
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_ENTER_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			 
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON	
				CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drinking: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAMERA()
				
				IF mTriggerModel = PROP_RADIO_01
					iFarCam = GET_FURTHEST_CAM()			
				ELSE
					IF bIsTrevor
						iFarCam = 0
					ELSE
						iFarCam = GET_FURTHEST_CAM()
					ENDIF
				ENDIF
				
				SET_CAM_PARAMS(CamIndex, vCamPos[iFarCam], vCamHead[iFarCam], fCamFov[iFarCam])	
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drinking: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
						
			// Initialise camera
			IF DOES_CAM_EXIST(CamIndex)
				SET_CAM_ACTIVE(CamIndex, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drinking: initialised CamIndex")
			ENDIF
			
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			bEnterAnimRunning = TRUE
			
			SETUP_PC_CONTROLS()
			
			CPRINTLN(DEBUG_AMBIENT, "TS_WAIT_FOR_MORE_INPUT")		
			eTriggerState = TS_WAIT_FOR_MORE_INPUT
			
		BREAK
				
		CASE TS_WAIT_FOR_MORE_INPUT
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])	
			ENDIF
			CHECK_FOR_BREAKOUT()
			
			// Apply the idle if we're not already playing it...
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				CPRINTLN(DEBUG_AMBIENT, "ANIM FINISHED, APPLY THE IDLE AND WAIT FOR MORE INPUT")				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					
				// Apply looped idle anims to player and props
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_IDLE_ANIM_FOR_THIS_SHOT(), SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
				
				MAKE_BOTTLE_VISIBLE(FALSE)
				
				IF mTriggerModel = PROP_RADIO_01 AND NOT mBottle[iTimesUsed].bCreated
					CREATE_DUMMY_BEER_BOTTLE()
				ENDIF				
				
				IF mTriggerModel = PROP_RADIO_01
					PLAY_SYNCHRONIZED_ENTITY_ANIM(mTriggerBottle, mSynchedScene, GET_IDLE_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				ELSE
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_IDLE_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				ENDIF
				
				IF bNeedSecondProp
					IF mTriggerModel = P_TUMBLER_02_S1
					OR mTriggerModel = P_TUMBLER_CS2_S 
					OR mTriggerModel = P_TUMBLER_CS2_S_TREV
					OR NOT bIsTrevor
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_IDLE_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
							
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
								
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
					PRINT_HELP_FOREVER(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
				ENDIF
								
				bEnterAnimRunning = FALSE
				bIdleAnimRunning = TRUE
			
			ELSE
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.75
				
					IF NOT bTakenHit
									
						CPRINTLN(DEBUG_AMBIENT, "Player_Takes_Alcohol_Hit(PLAYER_PED_ID())")
						
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
						ELSE
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
						ENDIF
						
						iTimesUsed++
						CPRINTLN(DEBUG_AMBIENT, "iTimesUsed is now ", iTimesUsed)
						
						// Increase Trevor's rage when he's drinking beer
						IF bIsTrevor AND mTriggerModel = PROP_RADIO_01 AND IS_SPECIAL_ABILITY_UNLOCKED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.05, TRUE)
						ENDIF		
						
						bTakenHit = TRUE
					
					ENDIF
				
				ENDIF
			ENDIF
			
			// Help is on-screen... - Can't rely on this being there when returning from pause/online.
			//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
			IF bIdleAnimRunning
			
				// Drink again?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					
					MAKE_BOTTLE_VISIBLE(TRUE)
					
					// Remove the help text.
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
					
					// Stop the scene from looping, so the blend to the next drink is smoother.
					IF IS_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, FALSE)
					ENDIF
					
					bTakenHit = FALSE
					
					CPRINTLN(DEBUG_AMBIENT, "TS_RUN_ANIM")
					eTriggerState = TS_RUN_ANIM
					
				// Quit?
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						// Stop the scene from looping so we can tell when it's finished.
						IF IS_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, FALSE)
						ENDIF
					ENDIF
					
					bEnterAnimRunning = FALSE
					bIdleAnimRunning = FALSE
					
					MAKE_BOTTLE_VISIBLE(TRUE)
					
					CPRINTLN(DEBUG_AMBIENT, "TS_TRIGGER_EXIT_ANIM")
					eTriggerState =	TS_TRIGGER_EXIT_ANIM
				
				ENDIF		
			ENDIF
						
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			ENDIF
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_SHOT_ANIM_FOR_THIS_SHOT(), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			
			IF mTriggerModel = PROP_RADIO_01
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTriggerBottle, mSynchedScene, GET_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, SLOW_BLEND_OUT)
			ELSE
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, SLOW_BLEND_OUT)
			ENDIF
			
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), GET_FACIAL_DRINK_ANIM(), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
									
			IF bNeedSecondProp
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, SLOW_BLEND_OUT)
			ENDIF
														
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
						
			bIdleAnimRunning = FALSE
			bBreakoutEarly = FALSE
			
			CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drink: TS_WAIT_FOR_ANIM_TO_RUN")
			eTriggerState = TS_WAIT_FOR_ANIM_TO_RUN
			
		BREAK
		
		CASE TS_WAIT_FOR_ANIM_TO_RUN
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			ENDIF
			CHECK_FOR_BREAKOUT()
			
			fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
			
			IF bBreakoutEarly
									
				// Check the entry anim for breakouts...
				IF bEnterAnimRunning
				AND FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
					IF fScenePhase >= fReturnStartPhase //AND fScenePhase <= fReturnEndPhase
						CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drink: TS_TRIGGER_EXIT_ANIM during enter")
						eTriggerState = TS_TRIGGER_EXIT_ANIM
					ENDIF
				ENDIF
				
				IF bIdleAnimRunning
				AND FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_IDLE_ANIM_FOR_THIS_SHOT(), GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
					IF fScenePhase >= fReturnStartPhase //AND fScenePhase <= fReturnEndPhase
						CPRINTLN(DEBUG_AMBIENT, "[SH] Shot Drink: TS_TRIGGER_EXIT_ANIM during idle")
						eTriggerState = TS_TRIGGER_EXIT_ANIM
					ENDIF
				ENDIF	
			ENDIF
					
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
				IF NOT bTakenHit
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.75
							
						CPRINTLN(DEBUG_AMBIENT, "Player_Takes_Alcohol_Hit(PLAYER_PED_ID())")
						
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
						ELSE
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
						ENDIF
						
						// Create the dummy bottle before incrementing the counter.
						IF mTriggerModel = PROP_RADIO_01 AND NOT mBottle[iTimesUsed].bCreated
							CREATE_DUMMY_BEER_BOTTLE()
						ENDIF
			
						iTimesUsed++
						CPRINTLN(DEBUG_AMBIENT, "iTimesUsed is now ", iTimesUsed)
						
						// Increase Trevor's rage when he's drinking beer
						IF bIsTrevor AND mTriggerModel = PROP_RADIO_01 AND IS_SPECIAL_ABILITY_UNLOCKED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.05, TRUE)
						ENDIF		
						
						bTakenHit = TRUE
					ENDIF
				
				ENDIF
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
					
					IF iTimesUsed < MAX_DRINKS
			
						// Set state to monitor next input..
						CPRINTLN(DEBUG_AMBIENT, "TS_WAIT_FOR_MORE_INPUT")
						eTriggerState = TS_WAIT_FOR_MORE_INPUT
					ELSE
						// OR run the exit anim if we've drunk 5 max shots...
						CPRINTLN(DEBUG_AMBIENT, "Bailing out - we've drank enough")
						eTriggerState = TS_TRIGGER_EXIT_ANIM
					ENDIF
				ENDIF			
			ENDIF
			
		BREAK
		
		CASE TS_TRIGGER_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Remove the help text.
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
				CLEAR_HELP(TRUE)
			ENDIF
			
			// This causes B*2044678
			//IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON
				//SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			//ENDIF
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			
			IF mTriggerModel = PROP_RADIO_01
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTriggerBottle, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			ELSE
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			ENDIF
			
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), GET_FACIAL_EXIT_ANIM(), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
						
			// Don't play an exit anim on the beer bottle, because it pops into to the beer box.
			IF bNeedSecondProp AND mTriggerModel <> PROP_RADIO_01
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, GET_EXIT_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
								
			bBreakoutEarly = FALSE
			
			CPRINTLN(DEBUG_AMBIENT, "TS_RUN_EXIT_ANIM")
			eTriggerState = TS_RUN_EXIT_ANIM
			
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelHead[iFarCam])
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelPitch[iFarCam])
					//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					SET_CAM_ACTIVE(CamIndex, FALSE)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, iExitAnimDuration) //FLOOR(GET_ENTITY_ANIM_TOTAL_TIME(PLAYER_PED_ID(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_EXIT_ANIM_FOR_THIS_ACTIVITY())))
					//DESTROY_CAM(CamIndex)
				ENDIF
			
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				IF fScenePhase > SAFE_SKIP_PHASE
				
					CHECK_FOR_BREAKOUT()
					
					IF bBreakoutEarly
					
						IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
													
							IF (fScenePhase >= fReturnStartPhase) //AND (fScenePhase <= fReturnEndPhase)
								
								CPRINTLN(DEBUG_AMBIENT, "[SG] Shots: WalkInterruptible tag found after phase", fScenePhase)
								
								// Stop anim and clear tasks.
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									CLEAR_PED_TASKS(PLAYER_PED_ID())
								ENDIF
															
								CPRINTLN(DEBUG_AMBIENT, "[SG] Shots: TS_RUN_EXIT_ANIM -> TS_RESET (EXIT BREAKOUT!)")
								eTriggerState = TS_RESET
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				eTriggerState = TS_RESET
			ENDIF
			
		BREAK 
								
		CASE TS_RESET
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)				
			OR bBreakoutEarly
			
				// Reset global for timetable events
				g_eCurrentSafehouseActivity = SA_NONE
				
				IF mTriggerModel <> PROP_RADIO_01			
					// Increment tracking stat
					IF STAT_GET_INT(NUM_SH_WHISKEY, iUsageStat)
						STAT_SET_INT(NUM_SH_WHISKEY, iUsageStat+1)
					ENDIF
				ELSE
					IF STAT_GET_INT(NUM_SH_BEER_DRUNK, iUsageStat)
						STAT_SET_INT(NUM_SH_BEER_DRUNK, iUsageStat+1)
					ENDIF
				ENDIF
				
				bBreakoutEarly = FALSE
				bEnterAnimRunning = FALSE
				bDoneGlassAnim = FALSE
				bIdleAnimRunning = FALSE
				bTakenHit = FALSE
											
				// Can accept phone calls again..
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
				
				IF IS_CELLPHONE_DISABLED()
					DISABLE_CELLPHONE(FALSE)
				ENDIF
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bSafehouseSetControlOff = FALSE
				
				CPRINTLN(DEBUG_AMBIENT, "TS_PLAYER_OUT_OF_RANGE")
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MP_UPDATE_DRINKING_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT fScenePhase, fReturnStartPhase, fReturnEndPhase
	
	//DRAW_DEBUG_AREA(vAreaAPos, vAreaBPos, WHISKEY_AREA_WIDTH)
	
	SWITCH eTriggerState
		CASE TS_PLAYER_OUT_OF_RANGE
		
			IF iPersonalAptActivity = ci_APT_ACT_SHOTS
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
		
//			// Forces the tumbler into an empty state at the start of the activity.
			IF DOES_ENTITY_EXIST(mTrigger)
				IF NOT bDoneGlassAnim AND bAnimsLoaded AND DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] Glass anim started...")
					PLAY_ENTITY_ANIM(mTrigger, GET_IDLE_ANIM_FOR_THIS_ENTITY(), sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE)
					bDoneGlassAnim  = TRUE
				ENDIF
			ENDIF
			
			IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_SHOTS)				
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaAPos, vAreaBPos, WHISKEY_AREA_WIDTH)
				AND NOT Is_Ped_Drunk(PLAYER_PED_ID())

					PRINT_HELP_FOREVER(sHelpText)
					
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_WAIT_FOR_PLAYER")
					eTriggerState = TS_WAIT_FOR_PLAYER			
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_SHOTS)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaAPos, vAreaBPos, WHISKEY_AREA_WIDTH)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
											
						DISABLE_SELECTOR_THIS_FRAME()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
						CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] ACTIVITY TRIGGERED...")
										
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							CLEAR_HELP(TRUE)
						ENDIF
				
						iPersonalAptActivity = ci_APT_ACT_SHOTS
			
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, TRIGGER_WIDTH)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE

						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
									
						SET_PLAYER_CLOTH_PACKAGE_INDEX( ENUM_TO_INT(CLOTH_PACKAGE_TOP_SKINNED)  )
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
						
						CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_GRAB_PLAYER")
						
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				
				REMOVE_PLAYER_MASK()
				
				// Temp: Reset the anims to match up again...
				iTimesUsed = 0
							
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				
				// Apply drinking anims to player and glass
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, sPedEnterAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN, AIK_DISABLE_HEAD_IK)
				SET_ENTITY_COLLISION(mTrigger, FALSE)
				SET_ENTITY_COLLISION(mWhiskeyBottle, FALSE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sGlassEnterAnim, sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mWhiskeyBottle, mSynchedScene, sBottleEnterAnim, sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
				 						
				//CamIndex = CREATE_CAMERA()
						
				// Initialise camera
//				IF DOES_CAM_EXIST(CamIndex)
//					SET_CAM_PARAMS(CamIndex, vCamPos[0], vCamHead[0], 50.0)
//					SET_CAM_ACTIVE(CamIndex, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
				
				KILL_ANY_CONVERSATION()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				bEnterAnimRunning = TRUE
				
				CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_WAIT_FOR_MORE_INPUT")		
				eTriggerState = TS_WAIT_FOR_MORE_INPUT
				
			ENDIF
			
		BREAK
				
		CASE TS_WAIT_FOR_MORE_INPUT
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			//DO_CAM_ADJUST(CamIndex, vCamHead)
			CHECK_FOR_BREAKOUT()
			
			// Apply the idle if we're not already playing it...
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
				
				CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] ANIM FINISHED, APPLY THE IDLE AND WAIT FOR MORE INPUT")				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
					
				// Apply looped idle anims to player and props
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_IDLE_ANIM_FOR_THIS_SHOT() , SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_IDLE_ANIM_FOR_THIS_ENTITY(), sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mWhiskeyBottle, mSynchedScene, GET_IDLE_ANIM_FOR_SECOND_ENTITY(), sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
								
//				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
//					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
//				ENDIF
							
//				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
								
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sInstructions)
					PRINT_HELP_FOREVER(sInstructions)
				ENDIF
				
				bEnterAnimRunning = FALSE
				bIdleAnimRunning = TRUE
			
			ELSE
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.75
				
					IF NOT bTakenHit
									
						CPRINTLN(DEBUG_AMBIENT, "Player_Takes_Alcohol_Hit(PLAYER_PED_ID())")
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
						ELSE
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
						ENDIF
						
						iTimesUsed++
						CPRINTLN(DEBUG_AMBIENT, "iTimesUsed is now ", iTimesUsed)
												
						bTakenHit = TRUE
					
					ENDIF
				
				ENDIF
			ENDIF
			
			// Help is on-screen... - Can't rely on this being there when returning from pause/online.
			//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_INSTRUCTIONS_FOR_THIS_ACTIVITY())
			IF bIdleAnimRunning
			
				// Drink again?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					
					// Remove the help text.
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sInstructions)
						CLEAR_HELP(TRUE)
					ENDIF
					iPersonalAptActivityState = ci_APT_ACT_STATE_REPEAT					
					bTakenHit = FALSE
					
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_RUN_ANIM")
					eTriggerState = TS_RUN_ANIM
					
				// Quit?
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
					
					bEnterAnimRunning = FALSE
					bIdleAnimRunning = FALSE
					iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
					
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_TRIGGER_EXIT_ANIM")
					eTriggerState =	TS_TRIGGER_EXIT_ANIM
				
				ENDIF		
			ENDIF
						
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			//DO_CAM_ADJUST(CamIndex, vCamHead)
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_SHOT_ANIM_FOR_THIS_SHOT(), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_ANIM_FOR_THIS_ENTITY(), sAnimDict, SLOW_BLEND_IN, SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mWhiskeyBottle, mSynchedScene, GET_ANIM_FOR_SECOND_ENTITY(), sAnimDict, SLOW_BLEND_IN, SLOW_BLEND_OUT)
						
			bIdleAnimRunning = FALSE
			bBreakoutEarly = FALSE
			
			CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_WAIT_FOR_ANIM_TO_RUN")
			eTriggerState = TS_WAIT_FOR_ANIM_TO_RUN
				
		BREAK
		
		CASE TS_WAIT_FOR_ANIM_TO_RUN
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			//DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			CHECK_FOR_BREAKOUT()
			
			fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
			
			IF bBreakoutEarly
									
				// Check the entry anim for breakouts...
				IF bEnterAnimRunning
				AND FIND_ANIM_EVENT_PHASE(sAnimDict, sPedEnterAnim, "ScriptEvent", fReturnStartPhase, fReturnEndPhase)
					IF fScenePhase >= fReturnStartPhase //AND fScenePhase <= fReturnEndPhase
						CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_TRIGGER_EXIT_ANIM during enter")
						eTriggerState = TS_TRIGGER_EXIT_ANIM
						iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
					ENDIF
				ENDIF
				
				IF bIdleAnimRunning
				AND FIND_ANIM_EVENT_PHASE(sAnimDict, GET_IDLE_ANIM_FOR_THIS_SHOT(), "ScriptEvent", fReturnStartPhase, fReturnEndPhase)
					IF fScenePhase >= fReturnStartPhase //AND fScenePhase <= fReturnEndPhase
						CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_TRIGGER_EXIT_ANIM during idle")
						eTriggerState = TS_TRIGGER_EXIT_ANIM
						iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
					ENDIF
				ENDIF	
			ENDIF
									
			IF NOT bTakenHit
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.75
						
					CPRINTLN(DEBUG_AMBIENT, "Player_Takes_Alcohol_Hit(PLAYER_PED_ID())")
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
					ELSE
						Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
					ENDIF
	
					iTimesUsed++
					CPRINTLN(DEBUG_AMBIENT, "iTimesUsed is now ", iTimesUsed)
					
					bTakenHit = TRUE
				ENDIF
			
			ENDIF
		
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
				
				IF iTimesUsed < MAX_DRINKS
					// Set state to monitor next input..
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_WAIT_FOR_MORE_INPUT")
					eTriggerState = TS_WAIT_FOR_MORE_INPUT
				ELSE
					// OR run the exit anim if we've drunk 5 max shots...
					CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_TRIGGER_EXIT_ANIM")
					eTriggerState = TS_TRIGGER_EXIT_ANIM
					iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
				ENDIF
			ENDIF			
						
		BREAK
		
		CASE TS_TRIGGER_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Remove the help text.
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sInstructions)
				CLEAR_HELP(TRUE)
			ENDIF
			
//			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON
//				SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
//			ENDIF
									
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mWhiskeyBottle, mSynchedScene, GET_EXIT_ANIM_FOR_SECOND_ENTITY(), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
											
			CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_RUN_EXIT_ANIM")
					
			bBreakoutEarly = FALSE
							
			eTriggerState = TS_RUN_EXIT_ANIM
			
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
				
//				IF DOES_CAM_EXIST(CamIndex)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelHead[0])
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelPitch[0])
//					SET_CAM_ACTIVE(CamIndex, FALSE)
//					RENDER_SCRIPT_CAMS(FALSE, TRUE, iExitAnimDuration) //FLOOR(GET_ENTITY_ANIM_TOTAL_TIME(PLAYER_PED_ID(), sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY())))
//				ENDIF
			
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				IF fScenePhase > SAFE_SKIP_PHASE
				
					CHECK_FOR_BREAKOUT()
					
					IF bBreakoutEarly
					
						IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
													
							IF (fScenePhase >= fReturnStartPhase) //AND (fScenePhase <= fReturnEndPhase)
								
								CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] WalkInterruptible tag found after phase", fScenePhase)
																						
								CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_RUN_EXIT_ANIM -> TS_RESET (EXIT BREAKOUT!)")
								//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, 50.0)
								eTriggerState = TS_RESET
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, 50.0)
				eTriggerState = TS_RESET
			ENDIF
			
		BREAK 
								
		CASE TS_RESET
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99			
			OR bBreakoutEarly
				
				//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 1.0)
				
				RESTORE_PLAYER_MASK()
				
				// Stop anim and clear tasks.
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
								
				// Reset global for timetable events
				g_eCurrentSafehouseActivity = SA_NONE
				
				INCREMENT_MP_APT_DRINK_COUNTER()

				iPersonalAptActivity = ci_APT_ACT_IDLE
				
//				IF STAT_GET_INT(NUM_SH_BEER_DRUNK, iUsageStat)
//					STAT_SET_INT(NUM_SH_BEER_DRUNK, iUsageStat+1)
//				ENDIF
				
				bBreakoutEarly = FALSE
				bEnterAnimRunning = FALSE
				bDoneGlassAnim = FALSE
				bIdleAnimRunning = FALSE
				bTakenHit = FALSE
											
				SET_ENTITY_COLLISION(mTrigger, TRUE)
				SET_ENTITY_COLLISION(mWhiskeyBottle, TRUE)
				
				// Can accept phone calls again..
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
								
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bSafehouseSetControlOff = FALSE
				
				CLEANUP_PC_CONTROLS()
				
				CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] TS_PLAYER_OUT_OF_RANGE")
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)

	IF NETWORK_IS_GAME_IN_PROGRESS()
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		OR Is_Player_On_Or_Triggering_Any_Mission()
		OR MPGlobalsAmbience.bRunningFmIntroCut
			CPRINTLN(DEBUG_AMBIENT, "SHOTS DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ELSE
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP) 
		OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
		OR IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
			CPRINTLN(DEBUG_AMBIENT, "SHOTS DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				CPRINTLN(DEBUG_AMBIENT, "SHOTS DEFAULT CLEANUP - player is wearing a helmet")
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
		ENDIF
	ENDIF
		
	
	IF DOES_ENTITY_EXIST(mObjectIn)
	
		mTrigger = mObjectIn
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)
		
		bPCControlsSetup = FALSE
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
				
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
						
			// This makes sure the net script is active, waits until it is.
			HANDLE_NET_SCRIPT_INITIALISATION()
			SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
			
//			IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(mTrigger)
//				NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
//			ENDIF
		
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			INT iCurProp = GET_PROPERTY_SIZE_TYPE(iCurProperty)
			
			SWITCH iCurProp
				CASE PROP_SIZE_TYPE_MED_APT		
					bInMidApt = TRUE
				BREAK
				DEFAULT
					bInMidApt = FALSE
				BREAK
			ENDSWITCH
			
			CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] Current property is ", iCurProperty)
			
			GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
			
			vTriggerPos 	= tempPropertyStruct.house.activity[SAFEACT_WHISKEY].vTriggerVec 
			fTriggerHead 	= tempPropertyStruct.house.activity[SAFEACT_WHISKEY].fTriggerRot
						
			vScenePos 		= tempPropertyStruct.house.activity[SAFEACT_WHISKEY].vSceneVec
			vSceneHead 		= tempPropertyStruct.house.activity[SAFEACT_WHISKEY].vSceneRot
							
			vAreaAPos 		=  tempPropertyStruct.house.activity[SAFEACT_WHISKEY].vAreaAVec
			vAreaBPos 		=  tempPropertyStruct.house.activity[SAFEACT_WHISKEY].vAreaBVec
			
			IF fTriggerHead < 0.0
				fTriggerHead+= 360.0
			ENDIF
			
			sAnimDict = "MP_SAFEHOUSEWHISKEY@"
			
			mWhiskeyBottle = GET_CLOSEST_OBJECT_OF_TYPE(vTriggerPos, 2.0, P_WHISKEY_BOTTLE_S, FALSE)
			
			IF ARE_VECTORS_ALMOST_EQUAL(vTriggerPos, <<342.6041, -1001.5839, -100.1962>>)
				iInterior = GET_INTERIOR_AT_COORDS(vTriggerPos)
				IF DOES_ENTITY_EXIST(mTrigger)
					FORCE_ROOM_FOR_ENTITY(mTrigger, iInterior, GET_HASH_KEY("rm_mid_lounge"))
				ENDIF
				
				IF DOES_ENTITY_EXIST(mWhiskeyBottle)
					FORCE_ROOM_FOR_ENTITY(mWhiskeyBottle, iInterior, GET_HASH_KEY("rm_mid_lounge"))
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] Ready for network launch...")
			
		ELSE
			
			mTrigger = mObjectIn
					
			IF mTriggerModel = P_TUMBLER_02_S1
			OR mTriggerModel = P_TUMBLER_CS2_S
			OR mTriggerModel = P_TUMBLER_CS2_S_TREV
				FREEZE_ENTITY_POSITION(mTrigger, TRUE)
			ELIF mTriggerModel = PROP_RADIO_01
				REQUEST_MODEL(mBottleModel)
				IF DOES_ENTITY_EXIST(GET_SYNCHED_SCENE_OBJECT())
					FREEZE_ENTITY_POSITION(GET_SYNCHED_SCENE_OBJECT(), TRUE)
					mTriggerBottle = GET_SYNCHED_SCENE_OBJECT()
				ENDIF
			ENDIF
			
			IF bNeedSecondProp AND DOES_ENTITY_EXIST(GET_SECONDARY_ENTITY())
				FREEZE_ENTITY_POSITION(GET_SECONDARY_ENTITY(), TRUE)
			ENDIF
	
				// If the flow is in the process of being configured, we wait for it to finish 
			#IF IS_DEBUG_BUILD 
				WHILE g_flowUnsaved.bUpdatingGameflow 
					CPRINTLN(DEBUG_AMBIENT, "Waiting for gameflow to stop updating...")
					WAIT(0)
				ENDWHILE 
			#ENDIF	
	
		ENDIF
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF DOES_ENTITY_EXIST(mTrigger)
		AND IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND NOT IS_ENTITY_DEAD(mTrigger)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND CAN_PLAYER_USE_THIS_OBJECT() 
		AND IS_INTERIOR_CORRECT(mTrigger)
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			SWITCH eState
									
				CASE AS_LOAD_ASSETS
					IF HAS_SHOT_DRINKING_DICT_LOADED()
					AND HAS_AUDIO_LOADED()
						IF mTriggerModel = PROP_RADIO_01
							IF NOT bIsTrevor
								// Fix michaels jacket
								SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
							ENDIF
							
							INIT_BOTTLE_POSITIONS()
						ENDIF		
						CPRINTLN(DEBUG_AMBIENT, "[MP SHOTS] AS_RUN_ACTIVITY")	
						eState = AS_RUN_ACTIVITY
					ENDIF
				BREAK
				
				CASE AS_RUN_ACTIVITY
					//IF g_OldSafehouseActivites = TRUE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF g_bInMultiplayer
								IF NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
								AND NOT IS_MID_APT_RADIO_IN_USE()
									//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, 2.0)
									MP_UPDATE_DRINKING_ACTIVITY()
								ELSE
									CLEANUP_DRINKING_ACTIVITY()
								ENDIF
							ELSE
								UPDATE_DRINKING_ACTIVITY()
							ENDIF
						ENDIF
					//ENDIF
				BREAK
				
				CASE AS_END
				BREAK
			ENDSWITCH			
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[SHOTS] Insta-clean")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
