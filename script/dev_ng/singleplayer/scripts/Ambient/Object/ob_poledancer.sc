// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_ped.sch"
USING "commands_vehicle.sch"
USING "commands_object.sch"
USING "commands_hud.sch"
USING "commands_player.sch"
USING "commands_task.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "commands_path.sch"
USING "types.sch"
USING "commands_camera.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_interiors.sch"
USING "script_player.sch"


// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambSpecial,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun

PED_INDEX pedPoleDancer
VECTOR vPoleDancer
FLOAT fPoleDancer
STRING sDance = "Poledance_01"

MODEL_NAMES modelStripper = A_F_Y_BEACH_01

STRING sAnimDict = "MISSSTRIP_CLUB"
BOOL bAnimTimeSet = FALSE
	   
// Functions ----------------------------------------------//
FUNC BOOL assetsAreLoaded()
	// Do some loading in here...
	REQUEST_MODEL(modelStripper)
	REQUEST_ANIM_DICT(sAnimDict)
	

	IF HAS_MODEL_LOADED	(modelStripper)
	AND HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN TRUE
	ELSE
		REQUEST_MODEL(modelStripper)
		REQUEST_ANIM_DICT(sAnimDict)
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL isCharInMainRoom(PED_INDEX ped)
//	INT i_key
//	GET_KEY_FOR_ENTITY_IN_ROOM(ped, i_key)
//	SWITCH currentClub
//		CASE clubClamPalace
//			IF i_key=GET_HASH_KEY("clammainroom")
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE clubBadabings
//			IF i_key=GET_HASH_KEY("Bada_room1")
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH
//	RETURN FALSE
//ENDFUNC


//FUNC BOOL isCharInAnyRoom(PED_INDEX ped)
//	INT i_key
//	IF NOT IS_CHAR_DEAD(ped)
//		GET_KEY_FOR_CHAR_IN_ROOM(ped, i_key)
//		SWITCH currentClub
//			CASE clubClamPalace
//				IF i_key=GET_HASH_KEY("clampalbkrm")		// 3 rooms in the interior
//				OR i_key=GET_HASH_KEY("clammainroom")
//				OR i_key=GET_HASH_KEY("clamfronrm")
//				OR i_key=GET_HASH_KEY("clamchangeroom")
//					RETURN TRUE
//				ENDIF
//			BREAK
//			CASE clubBadabings
//				IF i_key=GET_HASH_KEY("Bada_room2")		// 3 rooms in the interior
//				OR i_key=GET_HASH_KEY("Bada_room1")
//					RETURN TRUE
//				ENDIF
//			BREAK
//			CASE clubLostClubhouse
//				IF i_key=GET_HASH_KEY("E1_Lost_grndfront")		// 3 rooms in the interior
//				OR i_key=GET_HASH_KEY("E1_Lost_grndbar")
//				OR i_key=GET_HASH_KEY("E1_Lost_grndsave")
//					RETURN TRUE
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ENDIF	
//	RETURN FALSE
//ENDFUNC

PROC createPoleDancer()
	IF IS_ENTITY_DEAD(pedPoledancer)
		pedPoleDancer = CREATE_PED(PEDTYPE_CIVFEMALE, modelStripper, <<vPoledancer.x, vPoledancer.y, vPoledancer.z>>, fPoleDancer)
		SET_PED_RANDOM_COMPONENT_VARIATION(pedPoleDancer)
		SET_PED_CAN_BE_TARGETTED(pedPoleDancer, FALSE)
		//RETAIN_ENTITY_IN_INTERIOR(pedPoleDancer, GET_INTERIOR_AT_COORDS(vPoledancer))
		//SET_ROOM_FOR_PED_BY_KEY(pedPoleDancer, GET_ROOM_KEY_FROM_ENTITY(poleObject))
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPoleDancer, TRUE)
	ENDIF
ENDPROC

PROC runPoledancer()
	IF NOT IS_PED_INJURED(pedPoleDancer)
		IF GET_SCRIPT_TASK_STATUS(pedPoleDancer, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
			TASK_PLAY_ANIM(pedPoleDancer, sAnimDict, sDance)
		ELSE
			IF HAS_ENTITY_ANIM_FINISHED(pedPoleDancer, sAnimDict, sDance)
				TASK_PLAY_ANIM(pedPoleDancer, sAnimDict, sDance)
			ELSE
				IF NOT bAnimTimeSet
					IF IS_ENTITY_PLAYING_ANIM(pedPoleDancer, sAnimDict, sDance)
						SET_ENTITY_ANIM_CURRENT_TIME(pedPoleDancer, sAnimDict, sDance, GET_RANDOM_FLOAT_IN_RANGE())
						bAnimTimeSet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL isPlayerCausingRuckus()
	VECTOR ruckusLoc = <<2,2,0.5>>
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(pedPoleDancer)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedPoleDancer, ruckusLoc)
				RETURN TRUE
			ENDIF	
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPoleDancer, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedPoleDancer)
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// Clean Up
PROC missionCleanup()
	IF NOT IS_ENTITY_DEAD(pedPoleDancer)
		IF NOT IS_ENTITY_ON_SCREEN(pedPoleDancer)
			DELETE_PED(pedPoleDancer)
		ELSE
			SET_PED_KEEP_TASK(pedPoleDancer, TRUE)
		ENDIF
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oPole)

//SWAP_NEAREST_BUILDING_MODEL(-1716.52, 362.84, 27.48, 75.0, NJ03ac3200, LostBurnt01)

IF DOES_ENTITY_EXIST(oPole)
	FREEZE_ENTITY_POSITION(oPole, TRUE)
	vPoleDancer = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oPole, <<0.0, 0.0, 0.0>>)
	fPoleDancer = GET_ENTITY_HEADING(oPole)
	SET_ENTITY_COLLISION(oPole, FALSE)
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oPole)
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oPole)
			SWITCH ambStage
				CASE ambCanRun
					IF assetsAreLoaded()
						createPoleDancer()
						ambStage = (ambRunning)
					ENDIF
				BREAK
				CASE ambRunning
					runPoledancer()
					IF isPlayerCausingRuckus()
						SET_ENTITY_COLLISION(oPole, TRUE)
						ambStage = ambEnd
					ENDIF
				BREAK
				CASE ambEnd
					IF NOT IS_PED_INJURED(pedPoleDancer)
						TASK_COWER(pedPoleDancer)
						SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
						SET_PED_KEEP_TASK (pedPoleDancer, TRUE)
						TERMINATE_THIS_THREAD()
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			// Need check for fleeing char deletion/multiple char creation.
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

