// Includes
USING "ob_safehouse_common.sch"
USING "commands_ped.sch"
USING "freemode_header.sch"
USING "net_realty_mess_include.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_stripper.sc
//		DESCRIPTION		:	Handles strippers in the multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_STRIP_ANIM,
	TS_CHECK_STRIP_ANIM,
	TS_START_LOOPED_IDLE,
	TS_WAIT_FOR_INPUT,
	TS_RUN_ACCEPT_ANIM,
	TS_CHECK_ACCEPT_ANIM,
	TS_RUN_DECLINE_ANIM,
	TS_CHECK_DECLINE_ANIM
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

// Variables
BOOL 			bSetUpAnimDict			= FALSE
BOOL			bStripperModelInMem		= FALSE

CAMERA_INDEX  	mCam
//CAMERA_INDEX  	mCam2

INT 			iCurProperty
INT 			iScene, iLocalScene
INT 			iCurStrip				= 0

MODEL_NAMES		mStripperModel 			= S_F_Y_STRIPPER_01
MODEL_NAMES		mStripper2Model 		= S_F_Y_STRIPPER_02

NETWORK_INDEX	mStripper
NETWORK_INDEX	mStripper2

// Stripping anims
// Enter
STRING 			sEnterAnimDict 			= "mini@strip_club@lap_dance_2g@ld_2g_intro" 
STRING 			sPlayerEnterAnim 		= "ld_2g_intro_m"
STRING 			sStripperEnterAnim		= "ld_2g_intro_s1"
STRING 			sStripper2EnterAnim		= "ld_2g_intro_s2"
STRING 			sStripperEnterFacial	= "ld_2g_intro_s1_face"
STRING 			sStripper2EnterFacial	= "ld_2g_intro_s2_face"

// Strip 1
STRING 			sStripOneAnimDict		= "mini@strip_club@lap_dance_2g@ld_2g_p1"
STRING			sPlayerStripOne			= "ld_2g_p1_m"
STRING			sStripperStripOne		= "ld_2g_p1_s1"
STRING			sStripper2StripOne		= "ld_2g_p1_s2"
STRING			sStripperFacialOne		= "ld_2g_p1_s1_face"
STRING			sStripper2FacialOne		= "ld_2g_p1_s2_face"

// Strip 2
STRING 			sStripTwoAnimDict		= "mini@strip_club@lap_dance_2g@ld_2g_p2" 
STRING			sPlayerStripTwo			= "ld_2g_p2_m"
STRING			sStripperStripTwo		= "ld_2g_p2_s1"
STRING			sStripper2StripTwo		= "ld_2g_p2_s2"
STRING			sStripperFacialTwo		= "ld_2g_p2_s1_face"
STRING			sStripper2FacialTwo		= "ld_2g_p2_s2_face"

// Strip 3
STRING 			sStripThreeAnimDict		= "mini@strip_club@lap_dance_2g@ld_2g_p3" 
STRING			sPlayerStripThree		= "ld_2g_p3_m"
STRING			sStripperStripThree		= "ld_2g_p3_s1"
STRING			sStripper2StripThree	= "ld_2g_p3_s2"
STRING			sStripperFacialThree	= "ld_2g_p3_s1_face"
STRING			sStripper2FacialThree	= "ld_2g_p3_s2_face"

// Exit to idle
STRING 			sExitToIdleAnimDict 	= "mini@strip_club@lap_dance_2g@ld_2g_exit"
STRING 			sPlayerExitToIdle		= "ld_2g_exit_m"
STRING 			sStripperExitToIdle		= "ld_2g_exit_s1"
STRING 			sStripper2ExitToIdle	= "ld_2g_exit_s2"
STRING 			sStripperExitFacial		= "ld_2g_exit_s1_face"
STRING 			sStripper2ExitFacial	= "ld_2g_exit_s2_face"

// Waiting
STRING			sIdleAnimDict			= "mini@strip_club@lap_dance_2g@ld_2g_wait"
STRING			sPlayerIdleAnim			= "ld_2g_wait_m"
STRING			sStripperIdleAnim		= "ld_2g_wait_s1"
STRING			sStripper2IdleAnim		= "ld_2g_wait_s2"
STRING			sStripperIdleFacial		= "ld_2g_wait_s1_face"
STRING			sStripper2IdleFacial	= "ld_2g_wait_s2_face"

// Accept Another
STRING			sAcceptAnimDict			= "mini@strip_club@lap_dance_2g@ld_2g_accept"
STRING			sPlayerAcceptAnim		= "ld_2g_accept_m"
STRING			sStripperAcceptAnim		= "ld_2g_accept_s1"
STRING			sStripper2AcceptAnim	= "ld_2g_accept_s2"
STRING			sStripperAcceptFacial	= "ld_2g_accept_s1_face"
STRING			sStripper2AcceptFacial	= "ld_2g_accept_s2_face"

// Decline Another
STRING			sDeclineAnimDict		= "mini@strip_club@lap_dance_2g@ld_2g_decline"
STRING			sPlayerDeclineAnim		= "ld_2g_decline_m"
STRING			sStripperDeclineAnim	= "ld_2g_decline_s1"
STRING			sStripper2DeclineAnim	= "ld_2g_decline_s2"
STRING			sStripperDeclineFacial	= "ld_2g_decline_s1_face"
STRING			sStripper2DeclineFacial	= "ld_2g_decline_s2_face"

STRING 			sChosenAnimDict
STRING 			sChosenPlayerAnim
STRING 			sChosenStripperAnim
STRING 			sChosenStripper2Anim
STRING 			sChosenStripperFacial
STRING 			sChosenStripper2Facial

VECTOR 			vTriggerPos
FLOAT			fTriggerHead	

VECTOR	 		vScenePos				
VECTOR 			vSceneHead			

VECTOR			vStripperSpawn
FLOAT 			fStripperHead

//VECTOR 			vCamPos					
//VECTOR 			vCamHead				
//FLOAT 			fCamFov				= 45.0	
//
//VECTOR 			vExitCamPos				
//VECTOR 			vExitCamHead		
//FLOAT 			fExitCamFov			= 50.0

// PC CONTROLS
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_STRIPPER_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Cleaning up...")
	
	// Clear the help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_OUT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN")
		CLEAR_HELP()
	ENDIF
	
	IF bStripperModelInMem
		SET_MODEL_AS_NO_LONGER_NEEDED(mStripperModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(mStripper2Model)
	ENDIF
		
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sEnterAnimDict)
		REMOVE_ANIM_DICT(sStripOneAnimDict)
	 	REMOVE_ANIM_DICT(sStripTwoAnimDict)
	 	REMOVE_ANIM_DICT(sStripThreeAnimDict)
	 	REMOVE_ANIM_DICT(sExitToIdleAnimDict)
	 	REMOVE_ANIM_DICT(sIdleAnimDict)
	 	REMOVE_ANIM_DICT(sAcceptAnimDict)
	 	REMOVE_ANIM_DICT(sDeclineAnimDict)
	ENDIF
		
	// Remove camera
	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	
	CLEANUP_PC_CONTROLS()
	
	IF bSafehouseSetControlOff
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF						
		
	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_STRIPPER()
		
	// Load animations
	REQUEST_ANIM_DICT(sEnterAnimDict)
	REQUEST_ANIM_DICT(sStripOneAnimDict)
	REQUEST_ANIM_DICT(sStripTwoAnimDict)
	REQUEST_ANIM_DICT(sStripThreeAnimDict)
	REQUEST_ANIM_DICT(sExitToIdleAnimDict)
	REQUEST_ANIM_DICT(sIdleAnimDict)
	REQUEST_ANIM_DICT(sAcceptAnimDict)
	REQUEST_ANIM_DICT(sDeclineAnimDict)

	IF NOT HAS_ANIM_DICT_LOADED(sEnterAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sStripOneAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sStripTwoAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sStripThreeAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sExitToIdleAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sIdleAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sAcceptAnimDict)
	OR NOT HAS_ANIM_DICT_LOADED(sDeclineAnimDict)
	
		RETURN FALSE
		
	ELSE
		IF NOT bSetUpAnimDict
			CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: STRIPPER ANIMS LOADED")
			bSetupAnimDict = TRUE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_STRIPPER_MODEL_LOADED()
	
	CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: REQUESTING AUDIO")
	REQUEST_MODEL(mStripperModel)
	REQUEST_MODEL(mStripper2Model)
	
	IF HAS_MODEL_LOADED(mStripperModel)
	AND HAS_MODEL_LOADED(mStripper2Model)
		bStripperModelInMem = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CREATE_STRIPPER()

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(mStripper)
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(2, FALSE, TRUE)
			IF CAN_REGISTER_MISSION_PEDS(2)			
				IF CREATE_NET_PED(mStripper, PEDTYPE_CIVFEMALE, mStripperModel, vStripperSpawn, fStripperHead)
				AND CREATE_NET_PED(mStripper2, PEDTYPE_CIVFEMALE, mStripper2Model, vStripperSpawn, fStripperHead)
					SET_MODEL_AS_NO_LONGER_NEEDED(mStripperModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(mStripper2Model)
					bStripperModelInMem = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_STRIPPER_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.5, 1.5, 2.5 >>
//	FLOAT  fScenePhase  = 0.0
		
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE

			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > fTriggerHead-45
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < fTriggerHead+45
				
				IF iPersonalAptActivity = ci_APT_ACT_STRIPPERS
					iPersonalAptActivity = ci_APT_ACT_IDLE
				ENDIF
			
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_CALL")
					PRINT_HELP_FOREVER("SA_STRIP_CALL")
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_WAIT_FOR_PLAYER")				
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
						
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > fTriggerHead-45
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < fTriggerHead+45
			
					IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					
						SETUP_PC_CONTROLS()
						
						iPersonalAptActivity = ci_APT_ACT_STRIPPERS
						
						CREATE_STRIPPER()
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_CALL")
							CLEAR_HELP()
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, TRIGGER_WIDTH)
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						TASK_PED_SLIDE_TO_COORD(PLAYER_PED_ID(), vTriggerPos, fTriggerHead)
												
						CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_RUN_ENTRY_ANIM")
						eTriggerState = TS_RUN_ENTRY_ANIM
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_CALL")
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PED_SLIDE_TO_COORD) = FINISHED_TASK
				
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_CALL_STRIPPER_HOUSE, TRUE)
				
//				// Initialise camera
//				mCam = CREATE_CAMERA()
//				
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_ACTIVE(mCam, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_CAM_PARAMS(mCam, vCamPos, vCamHead, fCamFov)
//					SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
							
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sEnterAnimDict, sPlayerEnterAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				
				IF NOT IS_NET_PED_INJURED(mStripper)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sEnterAnimDict, sStripperEnterAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sStripperEnterFacial, sEnterAnimDict)
				ENDIF
				
				IF NOT IS_NET_PED_INJURED(mStripper2)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sEnterAnimDict, sStripper2EnterAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sStripper2EnterFacial, sEnterAnimDict)
				ENDIF
				
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_CHECK_ENTRY_ANIM")
				eTriggerState = TS_CHECK_ENTRY_ANIM
			
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.99
				
				sChosenAnimDict 		= sStripOneAnimDict
				sChosenPlayerAnim 		= sPlayerStripOne
				sChosenStripperAnim		= sStripperStripOne
				sChosenStripper2Anim	= sStripper2StripOne
				sChosenStripperFacial	= sStripperFacialOne
				sChosenStripper2Facial	= sStripper2FacialOne
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_RUN_STRIP_ANIM")
				eTriggerState = TS_RUN_STRIP_ANIM				
			ENDIF
		
		BREAK
			
		CASE TS_RUN_STRIP_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()	
								
			iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sChosenAnimDict, sChosenPlayerAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			
			IF NOT IS_NET_PED_INJURED(mStripper)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sChosenAnimDict, sChosenStripperAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sChosenStripperFacial, sChosenAnimDict)
			ENDIF
			
			IF NOT IS_NET_PED_INJURED(mStripper2)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sChosenAnimDict, sChosenStripper2Anim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sChosenStripper2Facial, sChosenAnimDict)
			ENDIF
			
			NETWORK_START_SYNCHRONISED_SCENE(iScene)
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_CAM")
				PRINT_HELP_FOREVER("SA_STRIP_CAM")
			ENDIF
				
			CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_CHECK_STRIP_ANIM")
			eTriggerState = TS_CHECK_STRIP_ANIM
		
		BREAK
		
		CASE TS_CHECK_STRIP_ANIM
	
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.99
							
				// Exit to idle
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sExitToIdleAnimDict, sPlayerExitToIdle, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS) 
				
				IF NOT IS_NET_PED_INJURED(mStripper)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sExitToIdleAnimDict, sStripperExitToIdle, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sStripperExitFacial, sExitToIdleAnimDict)
				ENDIF
				
				IF NOT IS_NET_PED_INJURED(mStripper2)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sExitToIdleAnimDict, sStripper2ExitToIdle, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS) 
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sStripper2ExitFacial, sExitToIdleAnimDict)
				ENDIF
				
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_CAM")
					CLEAR_HELP()
				ENDIF
			
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_START_LOOPED_IDLE")
				eTriggerState = TS_START_LOOPED_IDLE
				
			ENDIF
			
		BREAK
			
		CASE TS_START_LOOPED_IDLE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			
				// Idle loop
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sIdleAnimDict, sPlayerIdleAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS) 
				
				IF NOT IS_NET_PED_INJURED(mStripper)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sIdleAnimDict, sStripperIdleAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS) 
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sStripperIdleFacial, sIdleAnimDict)
				ENDIF
							
				IF NOT IS_NET_PED_INJURED(mStripper2)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sIdleAnimDict, sStripper2IdleAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sStripper2IdleFacial, sIdleAnimDict)
				ENDIF
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				// Increment the counter
				iCurStrip++
				
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_MENU")
					PRINT_HELP_FOREVER("SA_STRIP_MENU")
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_WAIT_FOR_INPUT")
				eTriggerState = TS_WAIT_FOR_INPUT
			ENDIF
					
		BREAK
				
		CASE TS_WAIT_FOR_INPUT	
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
				
				// Has the player accepted?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
									
					// Clear the help message
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_MENU")
						CLEAR_HELP()
					ENDIF
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_RUN_ACCEPT_ANIM")
					eTriggerState = TS_RUN_ACCEPT_ANIM
				ENDIF
				
				// Has the player declined?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
					
					// Change the camera angle
//					IF DOES_CAM_EXIST(mCam)
//						SET_CAM_PARAMS(mCam, vExitCamPos, vExitCamHead, fExitCamFov)
//					ENDIF
			
					// Clear the help message
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_STRIP_MENU")
						CLEAR_HELP()
					ENDIF
					
					CLEANUP_PC_CONTROLS()
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_RUN_DECLINE_ANIM")
					eTriggerState = TS_RUN_DECLINE_ANIM
				ENDIF
								
			ENDIF

		BREAK
			
		CASE TS_RUN_ACCEPT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
						
			iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAcceptAnimDict, sPlayerAcceptAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			
			IF NOT IS_NET_PED_INJURED(mStripper)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sAcceptAnimDict, sStripperAcceptAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sStripperAcceptFacial, sAcceptAnimDict)
			ENDIF
			
			IF NOT IS_NET_PED_INJURED(mStripper2)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sAcceptAnimDict, sStripper2AcceptAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sStripper2AcceptFacial, sAcceptAnimDict)
			ENDIF
						
			NETWORK_START_SYNCHRONISED_SCENE(iScene)
						
			CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_CHECK_ACCEPT_ANIM")	
			eTriggerState = TS_CHECK_ACCEPT_ANIM
			
		BREAK
			
		CASE TS_CHECK_ACCEPT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.99
				
				IF iCurStrip > 2
					iCurStrip = 0
				ENDIF
				
				SWITCH iCurStrip
				
					CASE 0
						CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Again! - Strip one")
						sChosenAnimDict 		= sStripOneAnimDict
						sChosenPlayerAnim 		= sPlayerStripOne
						sChosenStripperAnim		= sStripperStripOne
						sChosenStripper2Anim	= sStripper2StripOne
						sChosenStripperFacial	= sStripperFacialOne
						sChosenStripper2Facial	= sStripper2FacialOne
					BREAK
					
					CASE 1
						CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Again! - Strip two")
						sChosenAnimDict 		= sStripTwoAnimDict
						sChosenPlayerAnim 		= sPlayerStripTwo
						sChosenStripperAnim		= sStripperStripTwo
						sChosenStripper2Anim	= sStripper2StripTwo
						sChosenStripperFacial	= sStripperFacialTwo
						sChosenStripper2Facial	= sStripper2FacialTwo
					BREAK
					
					CASE 2
						CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Again! - Strip three")
						sChosenAnimDict 		= sStripThreeAnimDict
						sChosenPlayerAnim 		= sPlayerStripThree
						sChosenStripperAnim		= sStripperStripThree
						sChosenStripper2Anim	= sStripper2StripThree
						sChosenStripperFacial	= sStripperFacialThree
						sChosenStripper2Facial	= sStripper2FacialThree
					BREAK
					
				ENDSWITCH
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_RUN_STRIP_ANIM")		
				eTriggerState = TS_RUN_STRIP_ANIM
			ENDIF
			
		BREAK
			
		CASE TS_RUN_DECLINE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
						
			iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sDeclineAnimDict, sPlayerDeclineAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			
			IF NOT IS_NET_PED_INJURED(mStripper)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper), iScene, sDeclineAnimDict, sStripperDeclineAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper), sStripperDeclineFacial, sDeclineAnimDict)
			ENDIF
			
			IF NOT IS_NET_PED_INJURED(mStripper2)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(mStripper2), iScene, sDeclineAnimDict, sStripper2DeclineAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_FACIAL_ANIM(NET_TO_PED(mStripper2), sStripper2DeclineFacial, sDeclineAnimDict)
			ENDIF
			
			NETWORK_START_SYNCHRONISED_SCENE(iScene)
			
			CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_CHECK_DECLINE_ANIM")
			eTriggerState = TS_CHECK_DECLINE_ANIM
			
		BREAK
		
		CASE TS_CHECK_DECLINE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
						 
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.5
				
				// Clean up the strippers somehow!
				IF NOT IS_NET_PED_INJURED(mStripper)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(mStripper)
						CLEANUP_NET_ID(mStripper)
					ENDIF
				ENDIF
			
				IF NOT IS_NET_PED_INJURED(mStripper2)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(mStripper2)
						CLEANUP_NET_ID(mStripper2)
					ENDIF
				ENDIF
				
				INCREMENT_MP_APT_STRIPPER_COUNTER()
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 1.0)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bSafehouseSetControlOff = FALSE
								
				CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: TS_PLAYER_OUT_OF_RANGE")
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
			ENDIF
		
		BREAK
							
	ENDSWITCH
	
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
		
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
		CLEANUP_STRIPPER_ACTIVITY()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()

		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Ready for network launch...")
	ENDIF
	
	IF DOES_ENTITY_EXIST(mObjectIn)
				
		mTrigger = mObjectIn
				
		iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
		
		CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Current property is ", iCurProperty)
		
		GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
		
		vTriggerPos = tempPropertyStruct.house.activity[SAFEACT_STRIP].vTriggerVec 
		fTriggerHead = tempPropertyStruct.house.activity[SAFEACT_STRIP].fTriggerRot
		
		IF fTriggerHead < 0.0
			fTriggerHead+= 360.0
		ENDIF
		
		vStripperSpawn = tempPropertyStruct.house.activity[SAFEACT_STRIP].vAreaAVec
		fStripperHead = tempPropertyStruct.house.activity[SAFEACT_STRIP].vAreaBVec.z
		
		vScenePos = tempPropertyStruct.house.activity[SAFEACT_STRIP].vSceneVec
		vSceneHead = tempPropertyStruct.house.activity[SAFEACT_STRIP].vSceneRot
		
//		vCamPos = tempPropertyStruct.house.activity[SAFEACT_STRIP].vCamVec
//		vCamHead = tempPropertyStruct.house.activity[SAFEACT_STRIP].vCamRot
//		
//		vExitCamPos = tempPropertyStruct.house.activity[SAFEACT_STRIP].vExitCamVec
//		vExitCamHead = tempPropertyStruct.house.activity[SAFEACT_STRIP].vExitCamRot
		
	ENDIF
			
	CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Script triggered")
	
	RESERVE_NETWORK_MISSION_PEDS(2)
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
			
		WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()	
		
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)) < SOFA_TRIGGER_DIST
						
				SWITCH eState
										
					CASE AS_LOAD_ASSETS			
						IF HAS_ANIM_DICT_LOADED_FOR_STRIPPER()
						AND HAS_STRIPPER_MODEL_LOADED()
							CPRINTLN(DEBUG_AMBIENT, "[SH] MP Stripper: Assets loaded for activity...")
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
							UPDATE_STRIPPER_ACTIVITY()
						ELSE
							CLEANUP_STRIPPER_ACTIVITY()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
					
				ENDSWITCH
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+2
					CPRINTLN(DEBUG_AMBIENT, "[MP safehouse] Mid level shower: Player too far away - clean up...")
					CLEANUP_STRIPPER_ACTIVITY()
				ENDIF
			ENDIF
			
		ENDIF
			
	ENDWHILE
	
ENDSCRIPT
