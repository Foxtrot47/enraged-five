// Includes
USING "ob_safehouse_common.sch"
USING "commands_ped.sch"
USING "context_control_public.sch"
USING "freemode_header.sch"
//USING "rgeneral_include.sch"
USING "net_spawn_activities.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_bed_med.sc
//		DESCRIPTION		:	Handles bed activity in mid-level multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************
CONST_INT BS_ANIMATION_RUNNING   0
INT iValidityCheckerBS

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_IDLE_ANIM,
	TS_CHECK_IDLE_ANIM,
	TS_WAIT_FOR_INPUT,
	TS_RUN_EXIT_ANIM,
	TS_CHECK_EXIT_ANIM,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT 	AREA_WIDTH			2.0

// Variables
BOOL 			bSetUpAnimDict		= FALSE
//BOOL 			bSleeping			= FALSE
BOOL 			bInTransition	 	= FALSE
BOOL 			bInFirstPersonMode  = FALSE
//CAMERA_INDEX  	mCam

INT 			iScene
INT 			iLocalScene
INT 			iContextIntention 	= NEW_CONTEXT_INTENTION

//NAVDATA 		mNavStruct

STRING 			sAnimDict 			= "mp_bedmid"
STRING 			sEnterAnim			= "f_getin_l_bighouse"
STRING 			sIdleAnim 			= "f_sleep_l_loop_bighouse"
STRING			sExitAnim			= "f_getout_l_bighouse"

//VECTOR 			vBedPos				= <<350.0688, -996.2967, -99.6341>>

VECTOR 			vTriggerPos			// This need to get initailised AFTER we've grabbed the vector from the world point.
FLOAT			fTriggerHead		= 43.8287		

VECTOR			vTriggerAreaA		= <<349.0000, -997.3587, -100.5000>>
VECTOR			vTriggerAreaB		= <<351.7400, -997.3587, -97.0000>>

VECTOR	 		vScenePos			= << 349.660, -996.183, -99.764 >>
VECTOR 			vSceneHead			= << 0.0, 0.0, -3.960>>

//VECTOR 			vCamPos				= <<352.6264, -992.7764, -99.1287>> 
//VECTOR 			vCamRot				= <<-3.4257, -0.0441, 144.4589>>
//FLOAT 			fCamFOV				= 30.0033
//
//VECTOR 			vExitCamPos			= <<352.0019, -998.7175, -98.6698>> 
//VECTOR 			vExitCamRot			= <<-7.6837, 0.0327, 58.7848>>
//FLOAT 			fExitCamFov			= 36.9097

VECTOR 			vPlayerPos

// PC CONTROL FOR GETTING OUT OF BED
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_BED_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "MP Bed Script Cleaning up...")

	// Clear the context intention
	IF iContextIntention > -1
		RELEASE_CONTEXT_INTENTION(iContextIntention)
	ENDIF
	
	// Clear any text
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
		CLEAR_HELP()
	ENDIF
	
	CLEANUP_PC_CONTROLS()
	
	// If the script has ended with the player sleeping (going into PSN store etc), get the ped out of bed.
//	IF bSleeping
//		IF NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_STREET)
//		AND NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//			
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: TASK_PLAY_ANIM_ADVANCED")
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: vScenePos: ", vScenePos, ", vSceneHead: ", vSceneHead)
//			TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDict, sExitAnim, vScenePos, vSceneHead, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
//		ELSE
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: CLEARING EXIT BITS")
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_STREET)
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//		ENDIF
//		
////		iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
////		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sExitAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)                
////		NETWORK_START_SYNCHRONISED_SCENE(iScene)
//	ENDIF
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
		
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
		
	// Remove camera
//	IF DOES_CAM_EXIST(mCam)
//		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 1.0)
//		DESTROY_CAM(mCam)
//	ENDIF
	
	ENABLE_INTERACTION_MENU()
	
//	IF bSafehouseSetControlOff
//	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		PRINTLN("POD: CLEANUP_BED_ACTIVITY: NET_SET_PLAYER_CONTROL TRUE")
//	ENDIF

	IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
	AND IS_SKYSWOOP_AT_GROUND()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ELSE
		PRINTLN("POD: CLEANUP_BED_ACTIVITY: Didn't set player control because either player already had player control: bSafehouseSetControlOff: ", bSafehouseSetControlOff, ", or player was spectating as well: IS_PLAYER_SPECTATING: ", IS_PLAYER_SPECTATING(PLAYER_ID()), " or IS_NEW_LOAD_SCENE_ACTIVE: ", IS_NEW_LOAD_SCENE_ACTIVE(), " ")
	ENDIF						

	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Check the player is alone in the trigger area
FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_BED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN FALSE
	ENDIF
	bSetupAnimDict = TRUE
	RETURN TRUE
ENDFUNC

PROC SYNC_SCENE_VALIDITY_CHECK()
	iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
		BREAK
		CASE TS_WAIT_FOR_PLAYER
		BREAK
		CASE TS_RUN_ENTRY_ANIM
//			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
//				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_RUN_ENTRY_ANIM")
//				CLEANUP_BED_ACTIVITY()
//			ENDIF
		BREAK
		CASE TS_CHECK_ENTRY_ANIM
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_CHECK_ENTRY_ANIM")
				CLEANUP_BED_ACTIVITY()
			ENDIF
		BREAK
		CASE TS_RUN_IDLE_ANIM
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_RUN_IDLE_ANIM")
				CLEANUP_BED_ACTIVITY()
			ENDIF
		BREAK
		CASE TS_CHECK_IDLE_ANIM
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_CHECK_IDLE_ANIM")
				CLEANUP_BED_ACTIVITY()
			ENDIF
		BREAK
		CASE TS_WAIT_FOR_INPUT
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_WAIT_FOR_INPUT")
				CLEANUP_BED_ACTIVITY()
			ENDIF
		BREAK
		CASE TS_RUN_EXIT_ANIM             
//			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
//				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_RUN_EXIT_ANIM")
//				CLEANUP_BED_ACTIVITY()
//			ENDIF
		BREAK
		CASE TS_CHECK_EXIT_ANIM
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				PRINTLN("POD: SYNC_SCENE_VALIDITY_CHECK: TS_CHECK_EXIT_ANIM")
				CLEANUP_BED_ACTIVITY()
			ENDIF
		BREAK
		CASE TS_RESET
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_BED_ACTIVITY()
	#IF IS_DEBUG_BUILD
		PRINTLN("POD: BED: UPDATE_BED_ACTIVITY: CASE: ", eTriggerState)
	#ENDIF
	//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
	
	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
//	FLOAT  fScenePhase  = 0.0
		
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = ci_APT_ACT_BED_MED
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			// Is this script running a transition scene?
			IF (GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)		
				
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					bInFirstPersonMode = TRUE
				ENDIF
				
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sIdleAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON) 
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				eTriggerState = TS_RUN_EXIT_ANIM	
			ELSE
			
				IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BED_MED)	
					IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS(vTriggerPos)
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
					AND IS_PLAYER_ALONE_IN_AREA()	
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					#IF FEATURE_GTAO_MEMBERSHIP
					AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
					#ENDIF
						IF iContextIntention = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(iContextIntention, CP_MEDIUM_PRIORITY, "SA_BED_IN")
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - PLAYER CLOSE TO TRIGGER")
														
						eTriggerState = TS_WAIT_FOR_PLAYER					
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
			
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)		
				AND vPlayerPos.z < vTriggerPos.z + 1.0
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BED_MED)
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS(vTriggerPos)
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
				#IF FEATURE_GTAO_MEMBERSHIP				
				AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
				#ENDIF
					IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntention)
									
						CPRINTLN(DEBUG_AMBIENT, "[SH] - mid level bed TRIGGERED...")
						
						RELEASE_CONTEXT_INTENTION(iContextIntention)
						DISABLE_INTERACTION_MENU()				
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
							CLEAR_HELP()
						ENDIF
						
						SETUP_PC_CONTROLS()
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
					
						iPersonalAptActivity = ci_APT_ACT_BED_MED						
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
						vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDict, sEnterAnim, vScenePos, vSceneHead)  
						VECTOR vTempRotation 
						vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sEnterAnim, vScenePos, vSceneHead)  
						fTriggerHead = vTempRotation.Z
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 5000, fTriggerHead, 0.05)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
						AND DOES_ENTITY_EXIST(mTrigger)
							SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
							PRINTLN("POD: SET_PED_DESIRED_HEADING: ", GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_SEAT_PLAYER")
						eTriggerState = TS_RUN_ENTRY_ANIM
					ENDIF
				ELSE
					RELEASE_CONTEXT_INTENTION(iContextIntention)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
						CLEAR_HELP()
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
						
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			AND DOES_ENTITY_EXIST(mTrigger)
				SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
			
//				// Initialise camera
//				mCam = CREATE_CAMERA()
//				
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_ACTIVE(mCam, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_CAM_PARAMS(mCam, vCamPos, vCamRot, fCamFov)
//					SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)											
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sEnterAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
				                 
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					bInFirstPersonMode = TRUE
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_CHECK_ENTRY_ANIM")
				eTriggerState = TS_CHECK_ENTRY_ANIM
			
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				SET_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)
			
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_RUN_IDLE_ANIM")		
				eTriggerState = TS_RUN_IDLE_ANIM
			ENDIF
		
		BREAK
		
		CASE TS_RUN_IDLE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.9
				
//				bSleeping = TRUE
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)		
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sIdleAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON) 
			
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
								
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_CHECK_IDLE_ANIM")
				eTriggerState = TS_CHECK_IDLE_ANIM
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_IDLE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				SET_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iLocalScene, TRUE)				
			
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_WAIT_FOR_INPUT")		
				eTriggerState = TS_WAIT_FOR_INPUT
			ENDIF
			
		BREAK
				
		CASE TS_WAIT_FOR_INPUT	
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			IF NOT IS_SELECTOR_ONSCREEN()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
			ENDIF
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
			#IF FEATURE_GTAO_MEMBERSHIP
			AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
			#ENDIF
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
								
				// Clear the help message
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
				
				CLEANUP_PC_CONTROLS()
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)				
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sExitAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
//				bSleeping = FALSE
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_RUN_EXIT_ANIM")
								
				eTriggerState = TS_CHECK_EXIT_ANIM
			ENDIF
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
			IF NOT IS_SKYSWOOP_IN_SKY()
			AND IS_INTERIOR_READY( GET_INTERIOR_AT_COORDS(vTriggerPos))
			AND bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			IF IS_INTERIOR_READY( GET_INTERIOR_AT_COORDS(vTriggerPos))
			AND NOT IS_SKYSWOOP_IN_SKY()
			AND NOT IS_SKYSWOOP_MOVING()
			AND g_TransitionData.bEnteringPropertyCam = FALSE
//				SCRIPT_ASSERT("POD: bEnteringPropertyCam = FALSE")
				
				bInTransition = TRUE
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)		
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sExitAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				eTriggerState = TS_CHECK_EXIT_ANIM	
			ENDIF
		BREAK
		
		CASE TS_CHECK_EXIT_ANIM
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			IF NOT bInTransition
				DISABLE_SELECTOR_THIS_FRAME()
				DISABLE_ALL_MP_HUD_THIS_FRAME()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				SET_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)
				
				SET_SPAWN_ACTIVITY_OBJECT_SCRIPT_AS_READY(SPAWN_ACTIVITY_BED)
				
//				Change the camera angle - non-shake
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_PARAMS(mCam, vExitCamPos, vExitCamRot, fExitCamFov)
//				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mid level Bed - TS_RUN_IDLE_ANIM")		
				eTriggerState = TS_RESET
			ENDIF
			
		BREAK
		
		CASE TS_RESET
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF bInFirstPersonMode = TRUE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				CLEAR_BIT(iValidityCheckerBS, BS_ANIMATION_RUNNING)
				ENABLE_INTERACTION_MENU()
				
				IF bSafehouseSetControlOff
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bSafehouseSetControlOff = FALSE
				ENDIF
				
				SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
				
				iPersonalAptActivity = ci_APT_ACT_IDLE
				bInFirstPersonMode = FALSE
			ENDIF
			
		BREAK
					
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(coords_struct in_coords)

	vTriggerPos = in_coords.vec_coord[0]
	vTriggerPos = <<349.9853, -997.8344, -99.1952>>
	
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP) // re-added the SP_TO_MP check for B*1644593.
	OR Is_Player_On_Or_Triggering_Any_Mission()
	OR MPGlobalsAmbience.bRunningFmIntroCut
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] High Bed: Default Cleanup")
		CLEANUP_BED_ACTIVITY()
	ENDIF
			
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
		CLEANUP_BED_ACTIVITY()
	ENDIF
			
	IF NETWORK_IS_GAME_IN_PROGRESS()
			
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		CPRINTLN(DEBUG_AMBIENT, "[MP Safehouse] Mid level bed: Ready for network launch...")
		
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
			
		WAIT(0)
//		PRINTLN("POD: BED: While looop")
				
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)	
		AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < SOFA_TRIGGER_DIST
//			PRINTLN("POD: BED: MAIN CASE: ", eState)
				SWITCH eState
										
					CASE AS_LOAD_ASSETS			
						IF HAS_ANIM_DICT_LOADED_FOR_BED()
							CPRINTLN(DEBUG_AMBIENT, "[MP safehouse] mid-level bed: Assets loaded for activity...")
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							UPDATE_BED_ACTIVITY()
//							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
//								NETWORK_STOP_SYNCHRONISED_SCENE(iScene)
//							ENDIF
							IF IS_BIT_SET(iValidityCheckerBS, BS_ANIMATION_RUNNING)		
								SYNC_SCENE_VALIDITY_CHECK()
							ENDIF
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
					
				ENDSWITCH
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 6.0
					CLEANUP_BED_ACTIVITY()
				ENDIF
			ENDIF
		ELSE
			CLEANUP_BED_ACTIVITY()
		ENDIF
			
	ENDWHILE
ENDSCRIPT
