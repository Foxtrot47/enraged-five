// Includes
USING "ob_safehouse_common.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_drinking.sc
//		DESCRIPTION		:	Franklin's beer and wine drinking, Michael's wheatgrass.
//
// *****************************************************************************************
// *****************************************************************************************
// Constants
CONST_INT WHEATGRASS_REGEN_DIST(25)

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE, 
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_RUN_EXIT_ANIM,
	TS_BREAKOUT_EARLY,
	TS_WAIT_TO_LEAVE_AREA,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

// Variables
BOOL	bBreakoutEarly 		= FALSE
BOOL	bAnimLoaded 		= FALSE

CAMERA_INDEX  	CamIndex

INT 	iCurProperty
INT	  	mSynchedScene
INT 	iUsageStat

g_eDrunkLevel eDrunkLevel

STRING 	sAnimDict
STRING 	sPedEnterAnim		= "ig_2_wheatgrassdrink_michael"
//STRING 	sPedExitAnim		= "exit_michael"
STRING 	sGlassEnterAnim		= "ig_2_wheatgrassdrink_glass"
//STRING 	sGlassExitAnim		= "exit_glass"
STRING 	sHelpText			= "SA_WHEAT"
VECTOR 	vCamPos					
VECTOR 	vCamHead				
FLOAT 	fCamFov					

VECTOR 	vExitCamPos				
VECTOR 	vExitCamHead		
FLOAT 	fExitCamFov	

VECTOR 	vScenePos
VECTOR 	vSceneHead

VECTOR 	vTriggerPos
FLOAT 	fTriggerHead

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_DRINKING_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: Cleaning up")
	
	// url:bugstar:1645538
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
	
	// Clear help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
		CLEAR_HELP(TRUE)
	ENDIF
					
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	IF bAnimLoaded
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
		
	IF g_bInMultiplayer
	
		IF bSafehouseSetControlOff
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		// End script
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
		
	ELSE
			
		// Restore control and remove motion blur
		SAFE_RESTORE_PLAYER_CONTROL()
	
		// End script
		TERMINATE_THIS_THREAD()
		
	ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and checks that the anim dict for this activity has loaded
/// RETURNS:
///    True if the anim dict has loaded.
FUNC BOOL HAS_DRINKING_ANIM_DICT_LOADED()

	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	
	WHILE NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		WAIT(0)
	ENDWHILE
	//CPRINTLN(DEBUG_AMBIENT, "ANIM DICT LOADED")
	bAnimLoaded = TRUE
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()
	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: PLAYER WANTS TO BREAK OUT")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates drinking activity
PROC UPDATE_DRINKING_ACTIVITY()
	
	vTriggerPos = (GET_TRIGGER_VEC_FOR_THIS_OBJECT())
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0

	SWITCH eTriggerState
			
		CASE TS_PLAYER_OUT_OF_RANGE

			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
			AND DO_REQUIRED_OBJECTS_EXIST()
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM_WEAPDIS")
				IF NOT IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF	
					
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: PLAYER CLOSE TO TRIGGER...")
				SHOW_DEBUG_FOR_THIS_OBJECT()	
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				AND DO_REQUIRED_OBJECTS_EXIST()
				
					IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: ACTIVITY TRIGGERED...")
						SHOW_DEBUG_FOR_THIS_OBJECT()
						
						SET_PED_CLOTH_PIN_FRAMES(PLAYER_PED_ID(), 180)
						
						DISABLE_SELECTOR_THIS_FRAME()		
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION()
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						CLEAR_HELP(TRUE)
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
						
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
	
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), sAnimDict, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), "ig_2_wheatgrassdrink_michael_facial", sAnimDict)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(GET_SYNCHED_SCENE_OBJECT()) //Stop the glitch on the glass.
			
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
						
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAMERA()
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
						
			// Initialise camera
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				SET_CAM_ACTIVE(CamIndex, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_PARAMS(CamIndex, vCamPos, vCamHead, fCamFov)
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: initialised CamIndex")
			ENDIF
					
			// Play scene
			eTriggerState = TS_RUN_ANIM
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: TS_GRAB_PLAYER -> TS_RUN_ANIM")
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					DO_CAM_ADJUST(CamIndex, vCamHead)
				ENDIF
				
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				CHECK_FOR_BREAKOUT()
				
				IF NOT bDoneWheatGrassDialogue
					IF fScenePhase > 0.6
						START_WHEATGRASS_DIALOGUE()
					ENDIF
				ENDIF
				
				IF bBreakoutEarly
					
					FLOAT fReturnStartPhase, fReturnEndPhase

					// There is no ScriptEvent tag present in the drinking wheatgrass anim.
					// The script looks for a WalkInteruptible and if it's found, we exit out to the early breakout anim
	
					// Can we trigger the seperate exit anim early?
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)

						IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
						
							// If we have seperate exit anims, play them here.
							mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
														
							IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
							ENDIF
							
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
							ENDIF
							
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: TS_RUN_ANIM -> TS_RESET WalkInterruptible detected at ScenePhase ", fScenePhase)						
							eTriggerState = TS_RESET
							
						ENDIF
					ENDIF
									
				ENDIF
			
			ELSE // The drinking anim has finished
								
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: TS_RUN_ANIM -> TS_RESET")
				eTriggerState  = TS_RESET
								
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: TS_RUN_EXIT_ANIM -> TS_RESET")
				eTriggerState = TS_RESET
			ENDIF
		BREAK
				
		CASE TS_RESET	
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: Resetting")
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Increment TV usage stat	
			IF STAT_GET_INT(NUM_SH_WHEATGRASS, iUsageStat)
				STAT_SET_INT(NUM_SH_WHEATGRASS, iUsageStat+1)
			ENDIF
					
			// Normal gameplay cam
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
			
			// Restore control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
			bSafehouseSetControlOff = FALSE
	
			bBreakoutEarly = FALSE
									
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
						
			// Also can only trigger once, need to walk away for activity to reset
			FREEZE_ENTITY_POSITION(mTrigger, TRUE)
			PLAY_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), GET_ENTER_ANIM_FOR_THIS_ENTITY(), sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 1.0)
			
			// Give Michael a health boost if he's just drunk wheatgrass 
			IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) < (GET_PED_MAX_HEALTH(PLAYER_PED_ID())- HEALTH_BOOST_VALUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) + HEALTH_BOOST_VALUE))
			ENDIF
			
			// Fill Michael's special ability by 25%
			SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.25, TRUE)
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wheatgrass: TS_RESET -> TS_WAIT_TO_LEAVE_AREA")
			
			eTriggerState = TS_WAIT_TO_LEAVE_AREA
			
		BREAK
				
	ENDSWITCH
	
ENDPROC

PROC MP_UPDATE_DRINKING_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
//	FLOAT  fScenePhase  = 0.0

	SWITCH eTriggerState
			
		CASE TS_PLAYER_OUT_OF_RANGE

			IF iPersonalAptActivity = ci_APT_ACT_WHEATGRASS
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
			
			IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_WHEATGRASS)	AND NOT IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_WHEATGRASS_USED)	
				PRINTSTRING("PAul - addituional") PRINTNL()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
//				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
//				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
				AND eDrunkLevel = DL_NO_LEVEL
			
					PRINT_HELP_FOREVER("SA_WHEAT")
					eTriggerState = TS_WAIT_FOR_PLAYER					
				ENDIF
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
//				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
//				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_WHEATGRASS)	
				AND NOT IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_WHEATGRASS_USED)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						ENDIF
				
						iPersonalAptActivity = ci_APT_ACT_WHEATGRASS
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass ACTIVITY TRIGGERED...")
											
						DISABLE_SELECTOR_THIS_FRAME()	
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						CLEAR_HELP(TRUE)
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
						
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_WHEAT")
						CLEAR_HELP(TRUE)
					ENDIF
	
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				
				REMOVE_PLAYER_MASK()
				
//				CamIndex = CREATE_CAMERA()
							
//				 Initialise camera
//				IF DOES_CAM_EXIST(CamIndex)
//					SET_CAM_ACTIVE(CamIndex, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_CAM_PARAMS(CamIndex, vCamPos, vCamHead, 35.0)
//					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, sPedEnterAnim, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, REALLY_SLOW_BLEND_IN, AIK_DISABLE_HEAD_IK)
				SET_ENTITY_COLLISION(mTrigger, FALSE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sGlassEnterAnim, sAnimDict, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
				
				// Play scene
				eTriggerState = TS_RUN_ANIM
				CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_GRAB_PLAYER -> TS_RUN_ANIM")
			
			ENDIF
			
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
																									
				CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_RUN_ANIM -> TS_RESET")
				//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, 50.0)
				eTriggerState  = TS_RESET
								
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
				CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_RUN_EXIT_ANIM -> TS_RESET")
				//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, 50.0)
				eTriggerState = TS_RESET
			ENDIF
		BREAK
				
		CASE TS_RESET	
			
			CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass Resetting")
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			RESTORE_PLAYER_MASK()
			
//			// Increment TV usage stat	
//			IF STAT_GET_INT(NUM_SH_WHEATGRASS, iUsageStat)
//				STAT_SET_INT(NUM_SH_WHEATGRASS, iUsageStat+1)
//			ENDIF
						
//			 Normal gameplay cam
//			IF DOES_CAM_EXIST(CamIndex)
//				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
//			ENDIF
			
			// Restore control
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
	
			bBreakoutEarly = FALSE
			iPersonalAptActivity = ci_APT_ACT_IDLE
									
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
						
			// Also can only trigger once, need to walk away for activity to reset
			FREEZE_ENTITY_POSITION(mTrigger, TRUE)
			PLAY_ENTITY_ANIM(mTrigger, sGlassEnterAnim, sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 1.0)
			
			// Give player a health boost if he's just drunk wheatgrass 
			IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) < (GET_PED_MAX_HEALTH(PLAYER_PED_ID())- HEALTH_BOOST_VALUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) + HEALTH_BOOST_VALUE))
			ELSE
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
			ENDIF
			
//			// Fill Michael's special ability by 25%
//			SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.25, TRUE)
			SET_ENTITY_COLLISION(mTrigger, TRUE)
			CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_RESET -> TS_WAIT_TO_LEAVE_AREA")
			
			eTriggerState = TS_WAIT_TO_LEAVE_AREA
			
		BREAK
				
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_WITHIN_BRAIN_ACTIVATION_RANGE()

	IF eTriggerState = TS_WAIT_TO_LEAVE_AREA
		// This will be handled in TS_WAIT_TO_LEAVE_AREA 
		// Needed to reset wheatgrass glass state
		RETURN TRUE
	ELSE
		// Standard world brain check
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
			//CPRINTLN(DEBUG_AMBIENT, "Object is within brain activation range...")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU) 
		OR Is_Player_On_Or_Triggering_Any_Mission()
		OR MPGlobalsAmbience.bRunningFmIntroCut
			CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ELSE
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)	
		OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
		OR Is_Player_On_Or_Triggering_Any_Mission()
			CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mObjectIn)
		
		mTrigger = mObjectIn		
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)
		
		IF NOT IS_ENTITY_ATTACHED(mTrigger)
			FREEZE_ENTITY_POSITION(mTrigger, TRUE)
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
//			IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(mTrigger)
//				NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
//			ENDIF
		
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			CPRINTLN(DEBUG_AMBIENT, "[SH MP] Wheatgrass: Current property is ", iCurProperty)
			
			GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
			
			vTriggerPos = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vTriggerVec 
			fTriggerHead = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].fTriggerRot
			
			IF fTriggerHead < 0.0
				fTriggerHead+= 360.0
			ENDIF
			
			vScenePos = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vSceneVec
			vSceneHead = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vSceneRot
					
//			vCamPos = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vCamVec
//			vCamHead = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vCamRot
//			
//			vExitCamPos = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vExitCamVec
//			vExitCamHead = tempPropertyStruct.house.activity[SAFEACT_WHEATGRS].vExitCamRot
//			fExitCamFov = 38.2
			
			sAnimDict = "mp_safehousewheatgrass@"
			
	//		vDrawAreaA = tempPropertyStruct.house.activity[SAFEACT_SHOWER].vAreaAVec
	//		vDrawAreaB = tempPropertyStruct.house.activity[SAFEACT_SHOWER].vAreaBVec
		
		ELSE
			
			sAnimDict			= "safe@michael@ig_2"
			
			vCamPos				= <<-803.7434, 183.1330, 72.8617>>
			vCamHead			= <<6.6195, -0.0696, -1.4962>>
			fCamFov				= 37.3

			vExitCamPos			= <<-802.6, 184.6, 73.2>>
			vExitCamHead		= <<-6.4, 0.0, 89.1>>
			fExitCamFov			= 40.0

		ENDIF
		
	ENDIF

	CPRINTLN(DEBUG_AMBIENT, "... WHEAT GRASS IS TRIGGERING ...")
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF DOES_ENTITY_EXIST(mTrigger)
			
			IF IS_WITHIN_BRAIN_ACTIVATION_RANGE()
			AND NOT IS_ENTITY_DEAD(mTrigger)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND CAN_PLAYER_USE_THIS_OBJECT()
			AND IS_INTERIOR_CORRECT()
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_COVER(PLAYER_PED_ID())
			
				SWITCH eState
										
					CASE AS_LOAD_ASSETS
						IF HAS_DRINKING_ANIM_DICT_LOADED()
						AND HAS_AUDIO_LOADED()
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
		
					CASE AS_RUN_ACTIVITY 
						//IF g_OldSafehouseActivites = TRUE
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF g_bInMultiplayer
									IF NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
										MP_UPDATE_DRINKING_ACTIVITY()
									ELSE
										UPDATE_DRINKING_ACTIVITY()
									ENDIF
								ELSE
									UPDATE_DRINKING_ACTIVITY()
								ENDIF
							ENDIF
						//ENDIF
					BREAK
					
					CASE AS_END
					BREAK
				ENDSWITCH
			ELSE
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
		ELSE
			
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
