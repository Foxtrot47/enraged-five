USING "net_activity_creator_activities.sch"

STRUCT ServerBroadcastData
	INT iBS
	SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
	PED_VAR_ID_STRUCT pedVariationStruct[iMaxCreatorActivities]
ENDSTRUCT

#IF FEATURE_FREEMODE_PED_ANIM_CREATOR_PROTOTYPE

ServerBroadcastData serverBD

ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]
ACTIVITY_INTERIOR_STRUCT interiorStruct

#ENDIF

PROC RUN_ACTIVITY_LOGIC()
	
	
	#IF FEATURE_FREEMODE_PED_ANIM_CREATOR_PROTOTYPE
		CHECK_TO_KILL_PROTOTYPE_ACTIVITY_SCRIPT()
	// Runs activity creator logic
	
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_WITH_AI_PEDS(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds, serverBD.pedVariationStruct, pedLocalVariationsStruct, interiorStruct)
		
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds)

	#ENDIF
	#IF IS_DEBUG_BUILD
	CHECK_TO_LAUNCH_ACTIVITY_CREATOR_UI_SCRIPT(interiorStruct)
	#ENDIF
ENDPROC



// Philip O'Duffy

// Mission Script -----------------------------------------//
SCRIPT	
	#IF IS_DEBUG_BUILD 
//	CREATE_LUXE_ACT_WIDGETS(serverBD, luxeActState)
	
	
	CREATE_WIDGETS_FOR_ACTIVITY()
	#ENDIF
	PROCESS_PRE_GAME_PROTOTYPE_ACT_CREATOR()
	
	WHILE TRUE
		WAIT(0)
		RUN_ACTIVITY_LOGIC()
	ENDWHILE
ENDSCRIPT
