// Includes
USING "familySchedule_private.sch"
USING "ob_safehouse_common.sch"
USING "tv_control_public.sch"
USING "streamed_scripts.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_franklin_tv.sc
//		DESCRIPTION		:	Script that handles Franklin's Tv interaction in Vinewood safehouse
//
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM 
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_SEAT_PLAYER,
	TS_WAIT_FOR_SEATING,
	TS_SHOW_SOFA_INSTRUCTIONS,
	TS_IDLE_WITH_REMOTE,
	TS_CHANGE_CHANNEL,
	TS_QUITTING,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

VECTOR 			vTriggerVec
VECTOR 			vSofaPos			= <<1.3376, 525.3682, 174.5992>> // Position character must face before being able to trigger activity.

BOOL 			bBreakoutEarly		= FALSE
BOOL			bNeedChangeChannel	= FALSE
BOOL			bStoodInPlace		= FALSE
BOOL 			bSetupAnimDict		= FALSE

INT 			mSynchedScene
INT				iTVUsageStat

CAMERA_INDEX  	CamIndex
STRING 			sAnimDict 			= "safe@franklin@ig_14"

VECTOR 			vSceneOrigin		= <<2.58, 527.82, 173.65>>
VECTOR 			vSceneRot			= <<0.0, 0.0, 13.000 >> // -29.96>>

//VECTOR 			vTvOnCamPos			= <<1.2066, 524.0279, 175.0116>>
//VECTOR 			vTvOnCamRot			= <<0.0377, 0.0990, -6.5240>>
//FLOAT 			fTvOnCamFov			= 37.7681

//VECTOR 			vExitCamPos			= <<3.6100, 526.1229, 174.9386>>
//VECTOR			vExitCamRot			= <<-10.4356, 0.0990, 81.9451>>
//FLOAT			fExitCamFov			= 50.0

// -----------------------------------------------------------------------------------------------------------
//		Debug widgets
// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID widgetGroup

	BOOL bDebugAttach = TRUE
	BOOL bReAttachRemote = FALSE
	//BOOL bPrintOutValues = FALSE
		
	PROC SETUP_REMOTE_CONTROL_WIDGET()
		
		ADD_WIDGET_BOOL("Show Attach Offsets",	bDebugAttach)
		ADD_WIDGET_BOOL("Re-Attach Remote", bReAttachRemote)
		
//		START_WIDGET_GROUP("ATTACH OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X OFFSET", vOffset, -50.0, 50.0, 0.01)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("ROTATION OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X ROT", vRot, -360.0, 360.0, 0.5)
//		STOP_WIDGET_GROUP()
					
	ENDPROC
	
	PROC INIT_RAG_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("Remote Control Widget")
			SETUP_REMOTE_CONTROL_WIDGET()
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	PROC UPDATE_RAG_WIDGETS()
		
		IF bReAttachRemote
//			//IF IS_ENTITY_ATTACHED(GET_REMOTE())
//				CPRINTLN(DEBUG_AMBIENT, "detatched")
//				//DETACH_ENTITY(GET_REMOTE(), FALSE)
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(GET_REMOTE())
//				CPRINTLN(DEBUG_AMBIENT, "re-attached")
////				ATTACH_ENTITY_TO_ENTITY(GET_REMOTE(), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<vOffset.x, vOffset.y, vOffset.z>>, <<vRot.x, vRot.y, vRot.z>>)
//				bReAttachRemote = FALSE
//			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// PC CONTROL FOR SOFA/TV
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SOFA ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for Michael's sofa script
PROC CLEANUP_FRANKLIN_TV_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Franklin Sofa: Cleaning up...")
	
//	PLAYER_DETACH_VIRTUAL_BOUND()
//	SET_PLAYER_CLOTH_PACKAGE_INDEX(0)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP7")
		CLEAR_HELP(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CLEANUP_OBJECT_WIDGETS()
	#ENDIF
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	ENABLE_MOVIE_SUBTITLES(FALSE)
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_VINEWOOD")
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (cleanup)")
		STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_VINEWOOD")
	ENDIF

	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
	
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		SET_CAM_ACTIVE(CamIndex, FALSE)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	// Shutdown the PC custom control scheme
	CLEANUP_PC_CONTROLS()
	
	// B*1895222 - Reactivate the chop script only if the player started the activity
	IF bSafehouseSetControlOff
		REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
	ENDIF
		
	// Restore control
	SAFE_RESTORE_PLAYER_CONTROL()
	
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary.
FUNC BOOL HAS_SOFA_ANIM_DICT_LOADED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF HAS_ANIM_DICT_LOADED(sAnimDict)
		bSetupAnimDict = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Obtains the heading and position of the remote control
FUNC BOOL HAS_REMOTE_BEEN_FOUND()

	OBJECT_INDEX mRemote = GET_CLOSEST_OBJECT_OF_TYPE(vTriggerVec, 10.0, PROP_CS_REMOTE_01, FALSE)
	IF DOES_ENTITY_EXIST(mRemote)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Have particle effects and animations loaded for this activity
FUNC BOOL HAVE_ASSETS_LOADED()

	IF HAS_SOFA_ANIM_DICT_LOADED()
	AND HAS_REMOTE_BEEN_FOUND()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Assets have loaded...")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC OBJECT_INDEX GET_REMOTE()
	OBJECT_INDEX mObj 
	mObj = GET_CLOSEST_OBJECT_OF_TYPE(vTriggerVec, 3.0, PROP_CS_REMOTE_01, FALSE)
	RETURN mObj
ENDFUNC

/// PURPOSE:
///    Plays the correct exit anim on the player, depending on
///    if he's holding the remote control or not.
PROC PLAY_QUIT_ANIM()
	
	CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - PLAY_QUIT_ANIM")
	
	IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)
	TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
	IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
		PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, "exit_cam", sAnimDict)
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
	ENDIF
								
ENDPROC


/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()
	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - PLAYER WANTS TO BREAK OUT")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check for Michael getting up off the sofa
FUNC BOOL HAS_PLAYER_CANCELLED()

	// Cancel?
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		
		// Clear the text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP7")
				CLEAR_HELP(TRUE)
		ENDIF
		
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		CLEANUP_PC_CONTROLS()
		
		PLAY_QUIT_ANIM()

//		PLAYER_DETACH_VIRTUAL_BOUND()
//		SET_PLAYER_CLOTH_PACKAGE_INDEX(0)
		
		bBreakoutEarly = FALSE
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC




/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_FRANKLIN_TV_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
	FLOAT  fScenePhase  = 0.0
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
						
			IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
//			AND DO_REQUIRED_OBJECTS_EXIST()
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM_WEAPDIS")
				IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), vSofaPos, TRIGGER_ANGLE)
				OR bStoodInPlace
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_WAIT_FOR_PLAYER")
					PRINT_HELP_FOREVER("TV_HLP0")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
						START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
					ENDIF
				
					eTriggerState = TS_WAIT_FOR_PLAYER	
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
	
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
				AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vTriggerVec, 3.0, PROP_CS_REMOTE_01)
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
						AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
						AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
																						
							CPRINTLN(DEBUG_AMBIENT, "[SH] - SOFA TRIGGERED...")
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
								CPRINTLN(DEBUG_AMBIENT, "[SH] - SOFA - Clear Help...")
								CLEAR_HELP(TRUE)
							ENDIF
							
							ENABLE_MOVIE_SUBTITLES(TRUE)
							
							CLEAR_AREA_OF_PROJECTILES(vSofaPos, 3.0)
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							ELSE
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
							ENDIF
							bSafehouseSetControlOff = TRUE
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							SETUP_PC_CONTROLS()
						
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_SEAT_PLAYER")
							eTriggerState = TS_SEAT_PLAYER
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
						CLEAR_HELP(TRUE)
					ENDIF
					bStoodInPlace = FALSE
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_SEAT_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
									
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)
			
			IF g_TVStruct[TV_LOC_FRANKLIN_VINEWOOD].bIsTVOn
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Play the anim, DONT turn the tv on")
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "enter_no_remote", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "enter_no_remote_remote", sAnimDict, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Play the anim, turn the tv on")
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "enter", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "enter_remote", sAnimDict, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
			ENDIF
				
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				IF g_TVStruct[TV_LOC_FRANKLIN_VINEWOOD].bIsTVOn
					PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, "enter_no_remote_cam", sAnimDict)
				ELSE
					PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, "enter_cam", sAnimDict)
				ENDIF
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
					
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_WAIT_FOR_SEATING")
			eTriggerState = TS_WAIT_FOR_SEATING
		BREAK
		
		CASE TS_WAIT_FOR_SEATING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT g_TVStruct[TV_LOC_FRANKLIN_VINEWOOD].bIsTVOn
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.691
					RESTORE_STANDARD_CHANNELS()
					START_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_VINEWOOD)
				ENDIF
			
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.9
			
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)									
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
								
				// Show help text about operating TV.
				PRINT_HELP_FOREVER("TV_HLP7")	
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_VINEWOOD")
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene started TS_WAIT_FOR_SEATING")
					START_AUDIO_SCENE("TV_FRANKLINS_HOUSE_VINEWOOD")
				ENDIF
					
				CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState = TS_IDLE_WITH_REMOTE	

			ENDIF	
		BREAK
				
		CASE TS_IDLE_WITH_REMOTE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT HAS_PLAYER_CANCELLED()
				
				// Check the prompt is displaying - sometimes doesn't re-display when exiting a paused state...
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
					// Show help text about operating TV or smoking cigars.
					PRINT_HELP_FOREVER("TV_HLP7")
				ENDIF
				
				//DO_CAM_ADJUST(CamIndex, vTvOnCamRot)
				RUN_TV_TRANSPORT_CONTROLS(FALSE, TRUE)		
							
				// Has player turned the TV off?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
									
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, "exit_cam", sAnimDict)
					ENDIF
								
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")
					eTriggerState = TS_QUITTING
				
				ELIF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL()
					
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "idle_a_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					
					bNeedChangeChannel = TRUE
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_CHANGE_CHANNEL")
					eTriggerState = TS_CHANGE_CHANNEL
					STOP_CONTROL_SHAKE(FRONTEND_CONTROL)
					CLEAR_CONTROL_SHAKE_SUPPRESSED_ID(FRONTEND_CONTROL)
					
				ENDIF	
			
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING from TS_CHANGE_CHANNEL")
				eTriggerState = TS_QUITTING
			ENDIF
			
		BREAK 
		
		CASE TS_CHANGE_CHANNEL
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			//DO_CAM_ADJUST(CamIndex, vTvOnCamRot)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				IF bNeedChangeChannel
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					IF fScenePhase > 0.3
						IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_1
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
						ELSE
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)	
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Channel changed")
						bNeedChangeChannel = FALSE
						
					ENDIF
				ENDIF
			ELSE
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSceneOrigin, vSceneRot)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
								
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_WITH_REMOTE")
				eTriggerState = TS_IDLE_WITH_REMOTE
			ENDIF
		BREAK
						
		CASE TS_QUITTING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_VINEWOOD")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped TS_QUITTING ")
				STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_VINEWOOD")
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET")
				eTriggerState = TS_RESET
			ELSE
			
				CHECK_FOR_BREAKOUT()
				
				IF bBreakoutEarly
										
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
			
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, "exit", "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
				
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
														
							// Clear tasks as we are breaking out of the anim early...
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET - EXIT BREAKOUT!")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RESET
						
			// Increment TV usage stat
			IF STAT_GET_INT(NUM_SH_TV_WATCHED, iTVUsageStat)
				STAT_SET_INT(NUM_SH_TV_WATCHED, iTVUsageStat+1)
			ENDIF
			
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
			
			// Normal gameplay camera
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
						
			// Return control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
			bSafehouseSetControlOff = FALSE
			bBreakoutEarly = FALSE
			bStoodInPlace = TRUE
			
			IF g_TVStruct[TV_LOC_FRANKLIN_VINEWOOD].bIsTVOn
				RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOC_FRANKLIN_VINEWOOD)
			ENDIF			
			
			IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_VINEWOOD")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped - TS RESET")
				STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_VINEWOOD")
			ENDIF
			
			// Reset global for timetable events
			g_eCurrentSafehouseActivity = SA_NONE
					
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_FINALE_BEEN_TRIGGERED()
	
	//IF IS_MISSION_AVAILABLE(SP_MISSION_FINALE_INTRO)
	//IF NOT ARE_STRINGS_EQUAL(g_txtIntroMocapToLoad, "CHOICE_INT") 
	
	// B*1897092 - bail if the final intro mocap is up next.
	IF HAS_THIS_CUTSCENE_LOADED("CHOICE_INT")
		IF IS_CUTSCENE_PLAYING()
	
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] << FINALE HAS BEEN TRIGGERED >>")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(coords_struct in_coords)

	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU) 
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Franklin Sofa: Default Cleanup")
		CLEANUP_FRANKLIN_TV_ACTIVITY()
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		bIsFranklin = TRUE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
	OR IS_MOBILE_PHONE_CALL_ONGOING()
		CLEANUP_FRANKLIN_TV_ACTIVITY()
	ENDIF
	
	vTriggerVec = in_coords.vec_coord[0]
	vTriggerVec = <<2.1801, 526.4401, 173.6278>>
		
	#IF IS_DEBUG_BUILD
		INIT_RAG_WIDGETS()
	#ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Franklin TV: Ready for launch...")
		
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
		
		IF bIsFranklin
		AND NOT HAS_FINALE_BEEN_TRIGGERED()
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> NULL
		
		//AND g_TVStruct[TV_LOC_FRANKLIN_VINEWOOD].bAvailableForUse	
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerVec, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < SOFA_TRIGGER_DIST
				
				SWITCH eState
										
					CASE AS_LOAD_ASSETS			
						IF HAVE_ASSETS_LOADED()
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Franklin Sofa: Assets loaded for activity...")
							eState = AS_RUN_ACTIVITY
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Franklin Sofa: Loading assets...")
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							//UPDATE_RAG_WIDGETS()
							UPDATE_FRANKLIN_TV_ACTIVITY()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
					
				ENDSWITCH
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
				CLEANUP_OBJECT_WIDGETS()
			#ENDIF
			CLEANUP_FRANKLIN_TV_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
