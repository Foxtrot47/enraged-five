// Includes
USING "ob_safehouse_common.sch"
USING "commands_ped.sch"
USING "context_control_public.sch"
USING "freemode_header.sch"
//USING "rgeneral_include.sch"
USING "net_spawn_activities.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_bed_low.sc
//		DESCRIPTION		:	Handles bed activity in low-level multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_IDLE_ANIM,
	TS_CHECK_IDLE_ANIM,
	TS_WAIT_FOR_INPUT,
	TS_RUN_EXIT_ANIM,
	TS_CHECK_EXIT_ANIM,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT 	AREA_WIDTH			2.0
// Variables
BOOL 			bSetUpAnimDict		= FALSE
//BOOL 			bSleeping 			= FALSE
BOOL 			bInTransition		= FALSE

CAMERA_INDEX  	mCam

INT 			iScene
INT 			iLocalScene
INT 			iContextIntention 	= NEW_CONTEXT_INTENTION

//NAVDATA			mNavStruct

STRING 			sAnimDict 			= "mp_bedmid"
STRING 			sEnterAnim			= "f_getin_l_bighouse"
STRING 			sIdleAnim 			= "f_sleep_l_loop_bighouse"
STRING			sExitAnim			= "f_getout_l_bighouse"

VECTOR			vTriggerAreaA		= <<262.9207, -1002.9797, -100.0086>>
VECTOR			vTriggerAreaB		= <<261.0173, -1002.9797, -98.0086>>

//VECTOR 			vBedPos				= <<263.0075, -1004.7825, -100.0086>>

VECTOR 			vTriggerPos			// This can only be initialised AFTER we've grabbed the vector from the world pos
FLOAT			fTriggerHead		= 230.5943	

VECTOR	 		vScenePos			= <<262.740, -1004.344, -99.575>>
VECTOR 			vSceneHead			= <<0.0, 0.0, -162.360>>

//VECTOR 			vCamPos				= <<263.4871, -1002.1442, -99.1129>> 
//VECTOR 			vCamRot				= <<0.1187, 0.0500, 162.9949>>
//FLOAT 			fCamFov				= 34.3727
//
//VECTOR 			vExitCamPos			= <<263.4190, -1004.7426, -98.8425>> 
//VECTOR 			vExitCamRot			= <<-1.0877, -0.0549, 32.6409>>
//FLOAT 			fExitCamFov			= 50.0

// PC CONTROL FOR GETTING OUT OF BED
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_BED_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "MP Bed Script Cleaning up...")
	
	// Clear the context intention
	IF iContextIntention > -1
		RELEASE_CONTEXT_INTENTION(iContextIntention)
	ENDIF
	
	// Clean up the text
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
		CLEAR_HELP()
	ENDIF
//	// If the script has ended with the player sleeping (going into PSN store etc), get the ped out of bed.
//	IF bSleeping
//		IF NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_STREET)
//		AND NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//			
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: TASK_PLAY_ANIM_ADVANCED")
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: vScenePos: ", vScenePos, ", vSceneHead: ", vSceneHead)
//			TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDict, sExitAnim, vScenePos, vSceneHead, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
//		ELSE
//			CPRINTLN(DEBUG_AMBIENT, "CLEANUP_BED_ACTIVITY: CLEARING EXIT BITS")
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_STREET)
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//		ENDIF
//
//	ENDIF
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
		
	// Remove camera
	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
	
	IF bSafehouseSetControlOff
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEANUP_PC_CONTROLS()

	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Check the player is alone in the trigger area
FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_BED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN FALSE
	ENDIF
	bSetupAnimDict = TRUE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_BED_ACTIVITY()
	
	// DEBUG
	//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
					
	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
//	FLOAT  fScenePhase  = 0.0
		
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = ci_APT_ACT_BED_LOW
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			// Is this script running a transition scene?
			IF (GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)
			
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sIdleAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON) 
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
			
				eTriggerState = TS_RUN_EXIT_ANIM	
			ELSE
			
				IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BED_LOW)	
					IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS(vTriggerPos)	
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
					AND IS_PLAYER_ALONE_IN_AREA()
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					#IF FEATURE_GTAO_MEMBERSHIP					
					AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
					#ENDIF
						IF iContextIntention = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(iContextIntention, CP_MEDIUM_PRIORITY, "SA_BED_IN")
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - PLAYER CLOSE TO TRIGGER")
										
						eTriggerState = TS_WAIT_FOR_PLAYER	
					
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
					
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)		
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BED_LOW)
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS(vTriggerPos)
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
				
					IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntention)
									
						CPRINTLN(DEBUG_AMBIENT, "[SH] - Low level bed TRIGGERED...")
						
						RELEASE_CONTEXT_INTENTION(iContextIntention)
						DISABLE_INTERACTION_MENU()	
				
						iPersonalAptActivity = ci_APT_ACT_BED_LOW				
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
							CLEAR_HELP()
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
												
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						
						SETUP_PC_CONTROLS()
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDict, sEnterAnim, vScenePos, vSceneHead)  
						VECTOR vTempRotation 
						vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sEnterAnim, vScenePos, vSceneHead)  
						fTriggerHead = vTempRotation.Z
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 5000, fTriggerHead, 0.05)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
						AND DOES_ENTITY_EXIST(mTrigger)
							SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
							PRINTLN("POD: SET_PED_DESIRED_HEADING: ", GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_SEAT_PLAYER")
						eTriggerState = TS_RUN_ENTRY_ANIM
					ENDIF
				ELSE
					RELEASE_CONTEXT_INTENTION(iContextIntention)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
						CLEAR_HELP()
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
						
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			AND DOES_ENTITY_EXIST(mTrigger)
				SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
			
				// Initialise camera
//				mCam = CREATE_CAMERA()
//				
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_ACTIVE(mCam, TRUE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_CAM_PARAMS(mCam, vCamPos, vCamRot, fCamFov)
//					SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//				ENDIF
				
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sEnterAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
				                 
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_CHECK_ENTRY_ANIM")
				eTriggerState = TS_CHECK_ENTRY_ANIM
			
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_RUN_IDLE_ANIM")		
				eTriggerState = TS_RUN_IDLE_ANIM
			ENDIF
		
		BREAK
		
		CASE TS_RUN_IDLE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.9

				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sIdleAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON) 
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
								
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_CHECK_IDLE_ANIM")
				eTriggerState = TS_CHECK_IDLE_ANIM
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_IDLE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iLocalScene, TRUE)				
			
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_WAIT_FOR_INPUT")		
				eTriggerState = TS_WAIT_FOR_INPUT
			ENDIF
			
		BREAK
				
		CASE TS_WAIT_FOR_INPUT	
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF NOT IS_SELECTOR_ONSCREEN()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
			ENDIF
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
			#IF FEATURE_GTAO_MEMBERSHIP			
			AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
			#ENDIF
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
								
				// Clear the help message
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
				
				CLEANUP_PC_CONTROLS()
								
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sExitAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
			
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_RUN_EXIT_ANIM")
								
				eTriggerState = TS_CHECK_EXIT_ANIM
			ENDIF
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
			#IF IS_DEBUG_BUILD
			PRINTLN("POD: TS_PLAYER_OUT_OF_RANGE: IS_SKYSWOOP_IN_SKY: ", IS_SKYSWOOP_IN_SKY())		
			PRINTLN("POD: TS_PLAYER_OUT_OF_RANGE: IS_SKYSWOOP_MOVING: ", IS_SKYSWOOP_MOVING())		
			PRINTLN("POD: TS_PLAYER_OUT_OF_RANGE: bEnteringPropertyCam: ", g_TransitionData.bEnteringPropertyCam)		
			#ENDIF
			
			IF IS_INTERIOR_READY( GET_INTERIOR_AT_COORDS(vTriggerPos))
			AND NOT IS_SKYSWOOP_IN_SKY()
			AND NOT IS_SKYSWOOP_MOVING()
			AND g_TransitionData.bEnteringPropertyCam = FALSE
			
				bInTransition = TRUE
						
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sExitAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				eTriggerState = TS_CHECK_EXIT_ANIM	
			ENDIF
		BREAK
		
		CASE TS_CHECK_EXIT_ANIM
			
			IF NOT bInTransition
				DISABLE_SELECTOR_THIS_FRAME()
				DISABLE_ALL_MP_HUD_THIS_FRAME()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SPAWN_ACTIVITY_OBJECT_SCRIPT_AS_READY(SPAWN_ACTIVITY_BED)
			
//				// Change the camera angle - non-shake
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_PARAMS(mCam, vExitCamPos, vExitCamRot, fExitCamFov)
//				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Low level bed - TS_RUN_IDLE_ANIM")		
				eTriggerState = TS_RESET
			ENDIF
			
		BREAK
		
		CASE TS_RESET
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				ENABLE_INTERACTION_MENU()
				
				IF bSafehouseSetControlOff
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bSafehouseSetControlOff = FALSE
				ENDIF

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
				
				iPersonalAptActivity = ci_APT_ACT_IDLE
				
			ENDIF
			
		BREAK
					
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(coords_struct in_coords)
	
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU) 
	OR Is_Player_On_Or_Triggering_Any_Mission()
	OR MPGlobalsAmbience.bRunningFmIntroCut
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Low Bed: Default Cleanup")
		CLEANUP_BED_ACTIVITY()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
		CLEANUP_BED_ACTIVITY()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
				
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
			
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		CPRINTLN(DEBUG_AMBIENT, "[MP Safehouse] Low level bed: Ready for network launch...")

	ENDIF
	
	vTriggerPos = in_coords.vec_coord[0]
	//vTriggerPos = <<262.0073, -1002.6263, -100.0086>>
	vTriggerPos = <<261.8297, -1002.9283, -99.0062>>
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
			
		WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)	
		AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < SOFA_TRIGGER_DIST
						
				SWITCH eState
										
					CASE AS_LOAD_ASSETS			
						IF HAS_ANIM_DICT_LOADED_FOR_BED()
							CPRINTLN(DEBUG_AMBIENT, "[MP safehouse] mid-level bed: Assets loaded for activity...")
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							UPDATE_BED_ACTIVITY()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
					
				ENDSWITCH
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+2
					CLEANUP_BED_ACTIVITY()
				ENDIF
			ENDIF
		ELSE
			CLEANUP_BED_ACTIVITY()
		ENDIF
			
	ENDWHILE
ENDSCRIPT
