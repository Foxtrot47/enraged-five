// Includes
USING "ob_safehouse_common.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_huffing_gas.sc
//		DESCRIPTION		:	Trevor huffs the gas rag, passes out and ends up at a random
//                          location on the map.
//
// *****************************************************************************************
// *****************************************************************************************
// Constants
CONST_INT NUM_OF_HOURS_PASSED_OUT(6)

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE,
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ENTER_ANIM,
	TS_FADE_OUT_AND_WARP,
	TS_FADE_IN_AND_WAKE_UP,
	TS_RUN_EXIT_ANIM,
	TS_FADE_OFF_EFFECT,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

// ---------
// Variables
// ---------

BOOL		  	bActivityTriggered	= FALSE
//BOOL 			bAppliedAnim 		= FALSE
BOOL			bDoneHitEffect 		= FALSE

CAMERA_INDEX  	CamIndex

INT           	mSyncScene
INT			  	iRandomWarp
INT 			iUsageStat

STRING 		  	sAnimPlayer
STRING 		  	sAnimCamera
//STRING 			sClipSet		= "move_m@drunk@verydrunk"

VECTOR		  	vWarpPos
FLOAT		  	fWarpHeading

// -----------------------------------------------------------------------------------------------------------
//		Debug widgets
// -----------------------------------------------------------------------------------------------------------
//#IF IS_DEBUG_BUILD
//
//	WIDGET_GROUP_ID widgetGroup
//
//	BOOL bDebugAttach = TRUE
//	BOOL bReAttachRemote = FALSE
//	//BOOL bPrintOutValues = FALSE
//		
//	PROC SETUP_REMOTE_CONTROL_WIDGET()
//		
//		ADD_WIDGET_BOOL("Show Attach Offsets",	bDebugAttach)
//		ADD_WIDGET_BOOL("Re-Attach Remote", bReAttachRemote)
//		
//		START_WIDGET_GROUP("ATTACH OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X OFFSET", vOffset, -50.0, 50.0, 0.01)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("ROTATION OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X ROT", vRot, -360.0, 360.0, 0.5)
//		STOP_WIDGET_GROUP()
//					
//	ENDPROC
//	
//	PROC INIT_RAG_WIDGETS()
//		widgetGroup = START_WIDGET_GROUP("Remote Control Widget")
//			SETUP_REMOTE_CONTROL_WIDGET()
//		STOP_WIDGET_GROUP()
//	ENDPROC
//	
//	PROC CLEANUP_OBJECT_WIDGETS()
//		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
//			DELETE_WIDGET_GROUP(widgetGroup)
//		ENDIF
//	ENDPROC 
//	
//	PROC UPDATE_RAG_WIDGETS()
//		
//		IF bReAttachRemote
//			IF IS_ENTITY_ATTACHED(GET_CLOSEST_REMOTE())
//				CPRINTLN(DEBUG_AMBIENT, "detatched")
//				DETACH_ENTITY(GET_CLOSEST_RAG(), FALSE)
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(GET_CLOSEST_REMOTE())
//				CPRINTLN(DEBUG_AMBIENT, "re-attached")
//				ATTACH_ENTITY_TO_ENTITY(GET_CLOSEST_RAG(), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), mBoneTag), <<vOffset.x, vOffset.y, vOffset.z>>, <<vRot.x, vRot.y, vRot.z>>)
//				bReAttachRemote = FALSE
//			ENDIF
//			
//		ENDIF
//	ENDPROC
//#ENDIF

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Cleaning up...")
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
		CLEAR_HELP(TRUE)
	ENDIF
	
	SET_TIME_SCALE(1.0)
	
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
					
	// Unload animations
	REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())

	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		SET_CAM_MOTION_BLUR_STRENGTH(CamIndex, 0.0)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	// Restore control
	SAFE_RESTORE_PLAYER_CONTROL()

	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Waits for the world to load in around a given coordinate.
///    Call this before fading back in if you teleport the player for a checkpoint restart
/// PARAMS:
///    vLocation - the location, will usually be the player's location
///    fRadius - the radius around the location you need to be loaded, increase this if the world still doesn't properly load
///    satDataToLoad - the type of data that needs to be loaded: FLAG_COLLISIONS_MOVER, FLAG_COLLISIONS_WEAPON or FLAG_MAPDATA, can be OR'd together
PROC WAIT_FOR_WORLD_TO_LOAD(VECTOR vLocation, FLOAT fRadius = 50.0, STREAMVOL_ASSET_TYPES satDataToLoad = FLAG_MAPDATA)
	
	STREAMVOL_ID streamVolID   // ID for camera's stream vol ID
	streamVolID = STREAMVOL_CREATE_SPHERE(vLocation, fRadius, satDataToLoad)
	
	IF STREAMVOL_IS_VALID(streamVolID)
		INT iWaitTime = GET_GAME_TIMER() + 20000
		WHILE NOT STREAMVOL_HAS_LOADED(streamVolID) AND GET_GAME_TIMER() < iWaitTime
			WAIT(0)
		ENDWHILE
		IF GET_GAME_TIMER() < iWaitTime
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: World loaded")
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: World loading timed out")
		ENDIF
		STREAMVOL_DELETE(streamVolID)
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Unable to create the streaming volume, cannot test for the world loading!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures that the screen fades to black
PROC SAFE_FADE_OUT_TO_BLACK(INT iFadeTime = 250, BOOL bWaitForFade = TRUE)
	
	IF IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(iFadeTime)
			IF bWaitForFade
				WHILE NOT IS_SCREEN_FADED_OUT()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures that the screen fades in from black
PROC SAFE_FADE_IN_FROM_BLACK(INT iFadeTime = 250, BOOL bWaitForFade = TRUE)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(iFadeTime)
		ENDIF
	ENDIF
	IF bWaitForFade
		WHILE NOT IS_SCREEN_FADED_IN()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC
					


CONST_FLOAT TIME_SLOW_LIMIT 8.0  

CONST_INT HUFF_INTERVAL	50

BOOL bCanRestoreHuffEffect = FALSE
BOOL bCanStartHuffEffect = FALSE
BOOL bGotHuffTimer = FALSE

INT iHuffTime

FLOAT fCurTimeScale = 1.0
FLOAT fCurMotionBlur = 0.0
FLOAT fCurTimeCycle	= 0.0

PROC PROCESS_HUFF_EFFECT()
	
	FLOAT fTargetMotionBlur 	= 5.0
	FLOAT fTargetTimeScale 		= -0.5
	FLOAT fTargetTimeCycle		= 1.0
	
	IF bCanStartHuffEffect
		
		IF NOT bGotHuffTimer
			iHuffTime = GET_GAME_TIMER() + HUFF_INTERVAL
			bGotHuffTimer = TRUE
		ELSE
		
			IF GET_GAME_TIMER() > iHuffTime
			
				IF fCurTimeScale > fTargetTimeScale
					fCurTimeScale -= 0.004
					SET_TIME_SCALE(fCurTimeScale)
					CPRINTLN(DEBUG_AMBIENT, "TIME SCALE IS ", fCurTimeScale)
				ENDIF
				
				IF fCurMotionBlur < fTargetMotionBlur
					fCurMotionBlur += 0.075
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						SET_CAM_MOTION_BLUR_STRENGTH(CamIndex, fCurMotionBlur)
					ENDIF					
					CPRINTLN(DEBUG_AMBIENT, "MOTION BLUR IS ", fCurMotionBlur)
				ENDIF
				
				IF fCurTimeCycle < fTargetTimeCycle
					fCurTimeCycle += 0.004
					SET_TIMECYCLE_MODIFIER_STRENGTH(fCurTimeCycle)
					CPRINTLN(DEBUG_AMBIENT, "TIME SCALE IS ", fCurTimeCycle)
				ENDIF
						
				bGotHuffTimer = FALSE
				
			ENDIF
		ENDIF
	ENDIF	
		
ENDPROC

BOOL bTimeScaleRestored		= FALSE
BOOL bMotionBlurRestored	= FALSE
BOOL bTimeCycleRestored		= FALSE

PROC RESTORE_HUFF_EFFECT()

	FLOAT fTargetMotionBlur 	= 0.0
	FLOAT fTargetTimeScale 		= 1.0
	FLOAT fTargetTimeCycle		= 0.0
	
	IF bCanRestoreHuffEffect
						
		IF NOT bGotHuffTimer
			iHuffTime = GET_GAME_TIMER() + HUFF_INTERVAL
			bGotHuffTimer = TRUE
		ELSE
		
			IF GET_GAME_TIMER() > iHuffTime
			
				IF fCurTimeScale < fTargetTimeScale
					fCurTimeScale += 0.003
					
					IF fCurTimeScale > 1.0
						fCurTimeScale = 1.0
						bTimeScaleRestored = TRUE
					ENDIF
					
					SET_TIME_SCALE(fCurTimeScale)
					CPRINTLN(DEBUG_AMBIENT, "TIME SCALE IS ", fCurTimeScale)
				ENDIF
				
				IF fCurMotionBlur > fTargetMotionBlur
					fCurMotionBlur -= 0.02
					
					IF fCurMotionBlur < 0.0
						fCurMotionBlur = 0.0
						bMotionBlurRestored = TRUE
					ENDIF
					
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						SET_CAM_MOTION_BLUR_STRENGTH(CamIndex, fCurMotionBlur)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "MOTION BLUR IS ", fCurMotionBlur)
				ENDIF
				
				IF fCurTimeCycle > fTargetTimeCycle
					fCurTimeCycle -= 0.004
					
					IF fCurTimeCycle < 0.0
						fCurTimeCycle = 0.0
						bTimeCycleRestored = TRUE
					ENDIF
					
					SET_TIMECYCLE_MODIFIER_STRENGTH(fCurTimeCycle)
					CPRINTLN(DEBUG_AMBIENT, "TIME CYCLE MODIFIER ", fCurTimeCycle)
				ENDIF
				
				bGotHuffTimer = FALSE
				
				IF bTimeScaleRestored
				AND bMotionBlurRestored 
				AND bTimeCycleRestored
					bCanRestoreHuffEffect = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC


/// PURPOSE:
///    Updates the huffing gas activity
PROC UPDATE_HUFFING_GAS()
		
	VECTOR vTriggerPos = GET_TRIGGER_VEC_FOR_THIS_OBJECT()
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	VECTOR vPlayerPos
	
	SWITCH eTriggerState
		CASE TS_PLAYER_OUT_OF_RANGE

			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			//AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(mTrigger), TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Player close to trigger...")
				SHOW_DEBUG_FOR_THIS_OBJECT()
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF
	
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER		
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
			
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
			
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				AND vPlayerPos.z > 33.0 AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
				
					IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
																
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Activity triggered...")
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
				
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						IF DOES_ENTITY_EXIST(mTrigger)
							FREEZE_ENTITY_POSITION(mTrigger, FALSE)
						ENDIF
															
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						bActivityTriggered = TRUE
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
	
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	
			mSyncScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSyncScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSyncScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			
			IF NOT IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSyncScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSyncScene, TRUE)
			ENDIF

			// Setup camera		
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSyncScene, GET_CAMERA_FOR_THIS_ACTIVITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
						
			eTriggerState = TS_RUN_ENTER_ANIM
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: TS_RUN_ENTER_ANIM")
		BREAK
		
		CASE TS_RUN_ENTER_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSyncScene) > 0.95
				eTriggerState = TS_FADE_OUT_AND_WARP
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: TS_FADE_OUT_AND_WARP")
			ELIF GET_SYNCHRONIZED_SCENE_PHASE(mSyncScene) > 0.35
				IF NOT bDoneHitEffect
					SET_ENTITY_MOTION_BLUR(PLAYER_PED_ID(), TRUE)
					//Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
					SET_TIMECYCLE_MODIFIER("DRUG_gas_huffin")
					SET_TIMECYCLE_MODIFIER_STRENGTH(0.0)
					bCanStartHuffEffect = TRUE
					bDoneHitEffect = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_FADE_OUT_AND_WARP
					
			SAFE_FADE_OUT_TO_BLACK(DEFAULT_FADE_TIME_LONG)
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_SCREEN_FADED_OUT()
				
				bCanStartHuffEffect = FALSE
				
				// Clear the timecycle modifier
				CLEAR_TIMECYCLE_MODIFIER()
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(mSyncScene)
					SET_SYNCHRONIZED_SCENE_PHASE(mSyncScene, 1.0)
				ENDIF
					
				IF DOES_CAM_EXIST(CamIndex)
					DESTROY_CAM(CamIndex)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF

				// Get warp location	
				iRandomWarp = GET_RANDOM_INT_IN_RANGE(0,6)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Warp locate ", iRandomWarp)
				
				// Set target coords
				SWITCH iRandomWarp
					CASE 0 // Bottom of swimming pool
						vWarpPos = <<1546.0688, 3821.2224, 29.9139>>
						fWarpHeading = 235.0020
					BREAK
					CASE 1 // Hippy comume/alien grafitti
						vWarpPos = <<2515.7485, 3783.2017, 52.0043>>
						fWarpHeading = 0.0
					BREAK				
					CASE 2 // Under tarp / In Barn
						vWarpPos = <<1902.8933, 4921.0947, 47.0>>
						fWarpHeading = 69.8293
					BREAK
					CASE 3
						vWarpPos = <<-194.0443, 3638.5381, 63.4521>>
						fWarpHeading = 156.1723
					BREAK
					CASE 4
						vWarpPos = <<560.8397, 2738.1025, 41.2029>> 
						fWarpHeading = 91.5686 
					BREAK				
					CASE 5
						//vWarpPos = <<-425.1890, 1587.8060, 355.3480>>
						vWarpPos = <<-455.5521, 1598.6919, 358.1478>>
						fWarpHeading = 270.0
					BREAK
				ENDSWITCH		
					
				// Warp Trevor
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				
				// Increase time
				ADD_TO_CLOCK_TIME(NUM_OF_HOURS_PASSED_OUT, 0, 0)
				ADVANCE_FRIEND_TIMERS(TO_FLOAT(NUM_OF_HOURS_PASSED_OUT))
						
				WAIT_FOR_WORLD_TO_LOAD(vWarpPos, 10.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
				eTriggerState = TS_FADE_IN_AND_WAKE_UP
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: TS_FADE_IN_AND_WAKE_UP")
			ENDIF
			
		BREAK
		
		CASE TS_FADE_IN_AND_WAKE_UP	
				
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSyncScene)
				
				SET_TIMECYCLE_MODIFIER("drug_wobbly")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				fCurTimeCycle = 1.0
				
				// Get anim variant
				IF GET_RANDOM_BOOL()
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Front Anim")
					sAnimPlayer = "ig_8_wake_up_front_player"
					sAnimCamera = "ig_8_wake_up_front_cam"
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Right Anim")
					sAnimPlayer = "ig_8_wake_up_right_player"
					sAnimCamera = "ig_8_wake_up_right_cam"
				ENDIF
								
				VECTOR vPos
				vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							
				// Set target coords
				SWITCH iRandomWarp
					CASE 0	vPos.z -= 0.20	BREAK	// Area is sloped, cannot seem to get it spot on.
					CASE 1	vPos.z -= 0.15	BREAK	// Moved to a flatter area.
					CASE 2	vPos.z -= 0.05	BREAK	// Done
					CASE 3	vPos.z -= 0.15	BREAK	// Done
					CASE 4	vPos.z -= 0.15	BREAK 	// Done				
					CASE 5	vPos.z -= 0.10	BREAK	
				ENDSWITCH	
			
				// Trigger get-up animation
				mSyncScene = CREATE_SYNCHRONIZED_SCENE(<<vPos.x, vPos.y, vPos.z>>, <<0.0, 0.0, fWarpHeading>>)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSyncScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), sAnimPlayer, INSTANT_BLEND_IN, SLOW_BLEND_OUT)

				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSyncScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSyncScene, FALSE)
				ENDIF
				
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: not using 1st person so creating CamIndex")
					CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
					PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSyncScene, sAnimCamera, GET_ANIM_DICT_FOR_THIS_ACTIVITY())
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: using 1st person so not creating CamIndex")
					DESTROY_ALL_CAMS() // Ensure there are no script cams
				ENDIF
			ELSE
				
				bCanRestoreHuffEffect = TRUE
				//Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
				
				// Fade back in
				SAFE_FADE_IN_FROM_BLACK(DEFAULT_FADE_TIME)
				START_STONED_DIALOGUE()
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: TS_RUN_EXIT_ANIM")
				eTriggerState = TS_RUN_EXIT_ANIM
				
			ENDIF
			
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
							
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Synchronised scene has finished
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSyncScene)
								
				// Normal gameplay camera
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ENDIF

				// Unload audio
				RELEASE_AMBIENT_AUDIO_BANK()
				
				// Restore control
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
				bSafehouseSetControlOff = FALSE
								
				eTriggerState = TS_RESET
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: TS_RESET")
			ENDIF
			
//			IF NOT bAppliedAnim
//				
//				REQUEST_CLIP_SET(sClipSet)
//		
//				IF HAS_CLIP_SET_LOADED(sClipSet)				
//										
//					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
//					SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), sClipSet)
//					SET_PED_ALTERNATE_MOVEMENT_ANIM(PLAYER_PED_ID(), AAT_IDLE, sClipSet, "idle")
//					bAppliedAnim = TRUE
//				ENDIF
//			ENDIF
			
		BREAK
				
		CASE TS_RESET
		
			// Have all the effects of the gas worn off?
			IF bTimeScaleRestored AND bMotionBlurRestored AND bTimeCycleRestored
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				IF DOES_ENTITY_EXIST(mTrigger)
					FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				ENDIF
				
				IF STAT_GET_INT(NUM_SH_GAS_HUFFED, iUsageStat)
					STAT_SET_INT(NUM_SH_GAS_HUFFED, iUsageStat+1)
				ENDIF
				
				bDoneHitEffect = FALSE
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				bActivityTriggered = FALSE
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Can the safehouse activity trigger?
FUNC BOOL IS_ACTIVITY_OBJECT_ALIVE()

	IF DOES_ENTITY_EXIST(mTrigger)
		VECTOR vPos = GET_ENTITY_COORDS(mTrigger)
		IF NOT IS_ENTITY_DEAD(mTrigger)
		AND vPos.z > 33.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Can the safehouse activity trigger?
FUNC BOOL CAN_ACTIVITY_LAUNCH()

	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_CURRENTLY_ON_MISSION_TO_TYPE")
		RETURN FALSE
	ENDIF
		
	IF IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_PLAYER_USE_THIS_OBJECT()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: CAN_PLAYER_USE_THIS_OBJECT")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ACTIVITY_OBJECT_ALIVE()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_ACTIVITY_OBJECT_ALIVE")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_INTERIOR_CORRECT()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_INTERIOR_CORRECT")
		RETURN FALSE
	ENDIF
	
	IF g_eCurrentFamilyEvent[FM_TREVOR_0_RON] = FE_T0_RON_drinks_moonshine_from_a_jar
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Ron is drinking moonshine.")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Trevor is wearing a mask.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Should the activity shut down?
FUNC BOOL CAN_ACTIVITY_PROCEED()

	// Check for player switch and cancel activity
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_PLAYER_SWITCH_IN_PROGRESS()")
		RETURN FALSE
	ENDIF
		
	IF NOT bActivityTriggered
		
		IF NOT IS_ACTIVITY_OBJECT_ALIVE()
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_ACTIVITY_OBJECT_ALIVE()")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE()")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)

	// Default callback
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
	
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: HAS_FORCE_CLEANUP_OCCURRED")
		CLEANUP_ACTIVITY()
	ENDIF
	
	// Setup object
	IF DOES_ENTITY_EXIST(mObjectIn)
		mTrigger = mObjectIn		
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)
	ENDIF
		
	// Activity checks
	IF NOT CAN_ACTIVITY_LAUNCH()
		CLEANUP_ACTIVITY()
	ENDIF
	
	// Preload animations
	REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Huffing Gas: Ready to launch")
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
		
		IF NOT CAN_ACTIVITY_PROCEED()
			CLEANUP_ACTIVITY()
		ENDIF
		
		// Main state handler	
		SWITCH eState
								
			CASE AS_LOAD_ASSETS				
				IF HAS_THIS_ANIM_DICT_LOADED()
				AND HAS_AUDIO_LOADED()
					eState = AS_RUN_ACTIVITY
				ENDIF
			BREAK

			CASE AS_RUN_ACTIVITY
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					UPDATE_HUFFING_GAS()
					PROCESS_HUFF_EFFECT()
					RESTORE_HUFF_EFFECT()
					#IF IS_DEBUG_BUILD
						//UPDATE_RAG_WIDGETS()
					#ENDIF
				ENDIF
			BREAK
			
			CASE AS_END
			BREAK
		ENDSWITCH
	ENDWHILE
ENDSCRIPT
