// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"
USING "script_player.sch"
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "cutscene_public.sch"
USING "context_control_public.sch"
USING "freemode_header.sch"
USING "rgeneral_include.sch"
USING "net_cash_transactions.sch"

// Variables ----------------------------------------------//
PED_COMP_NAME_ENUM eStoredHelmet = DUMMY_PED_COMP, eStoredMask = DUMMY_PED_COMP
BOOL bSwappedChemSuit = FALSE
ENUM ambStageFlag
	ambCanRun,
	ambRunning, 
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM scopeStageFlag
	playerOutOfRange,
	waitForPlayer,
	grabPlayer,
	playEnter,
	runScope,
	resetScope
ENDENUM
OBJECT_INDEX oBlinds	
scopeStageFlag scopeStage = waitForPlayer

WEAPON_TYPE storedWeapon
//BOOL bTurnedPlayerControlOff
INT iContextButtonIntention = NEW_CONTEXT_INTENTION, iTelescopeStartTime = -1
INT iSoundCoinDrop = -1, iSoundCountdown = -1, iSoundTimer = -1, iSoundZoom = -1, iSoundZoomLimit = -1, iSoundTurnVert = -1, iSoundTurnHori = -1, iSoundTurnLimit = -1/*, iSoundEndUse = -1*/
BOOL bPublicTelescope, bRespottedPlayer, bHitVertLimit = FALSE, bHitHorizLimit = FALSE, bHitZoomLimit = FALSE

STRING sTelescopeSoundBank
SCRIPT_TIMER telescopeMouseTimer
FLOAT fMouseX 
FLOAT fStoredMouseX
FLOAT fMouseY 
FLOAT fStoredMouseY

OBJECT_INDEX oScope
CAMERA_INDEX cameraTelescope
SCALEFORM_INDEX sfMovie
VECTOR vLookCamPos
VECTOR vLookCamRot
FLOAT fMinYChange	= -40
FLOAT fMaxYChange = 40
FLOAT fMinXChange	= -70
FLOAT fMaxXChange = 70
FLOAT fMaxFOV = 10
FLOAT fMinFOV = 65
//FLOAT vScopeHeading 

FLOAT fZoomChange = fMinFOV
FLOAT fZoomSpeed = 1
FLOAT fSpeedMultiplier = 50
FLOAT fCamSpeed = fZoomChange/fSpeedMultiplier
FLOAT fCamChangeY
FLOAT fCamChangeX
FLOAT fHomeTelescopeOffset = 0.34
CONST_INT STICK_DEAD_ZONE 28
CONST_FLOAT MOUSE_CAMERA_SPEED_ADJUST 0.1//0.0004
STRING animDictEnter = "MINI@TELESCOPE"
STRING animDictExit = "MINI@TELESCOPE"
INT scopeCost = 1
STRING sScopeContextHelp
//SEQUENCE_INDEX seq
BOOL scopeFallen = FALSE

CONST_INT TELESCOPE_TIME 60000

CONST_INT iBS_STORE_ACTIVE					0
INT iBoolsBitSet

STRING animEnter
STRING animLoop
STRING animExit

BOOL bSetupPCControls = FALSE


PROC CLEAN_UP_CAMERA()
	IF DOES_ENTITY_EXIST(oBlinds)
		SET_ENTITY_VISIBLE(oBlinds, TRUE)
	ENDIF
	CLEAR_HELP(TRUE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	IF IS_CAM_ACTIVE(cameraTelescope)
		SET_CAM_ACTIVE(cameraTelescope,FALSE)
	ENDIF
	DESTROY_CAM(cameraTelescope)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	IF DOES_ENTITY_EXIST(oScope)
	AND GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
		//don't clear timecycles for arena
	ELSE
		//think we still need to hit this regardless of existance of prop at this point
		CLEAR_TIMECYCLE_MODIFIER()
		POP_TIMECYCLE_MODIFIER()
		IF NOT Is_Ped_Drunk(PLAYER_PED_ID())
			CLEAR_TIMECYCLE_MODIFIER()
		ENDIF
	ENDIF
	
	PRINTLN("Ob_telescope called - CLEAN_UP_CAMERA()")
ENDPROC

// Clean Up
PROC missionCleanup(BOOL bAllowDelayForDeadPed)
	IF NOT IS_PLAYER_USING_RC_TANK(PLAYER_ID())
	AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sScopeContextHelp)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sScopeContextHelp)
			CLEAR_HELP()
		ENDIF
	ENDIF
	IF iContextButtonIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
	ENDIF
	
	IF bSwappedChemSuit = TRUE
		IF bAllowDelayForDeadPed
		AND IS_PED_INJURED(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - delay mission cleanup to allow injured ped to swap hazmat hood")
			EXIT
		ENDIF
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
		bSwappedChemSuit = FALSE
	ENDIF
	IF eStoredHelmet != DUMMY_PED_COMP
	AND eStoredHelmet != PROPS_HEAD_NONE
		IF bAllowDelayForDeadPed
		AND IS_PED_INJURED(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - delay mission cleanup to allow injured ped to swap stored Helmet")
			EXIT
		ENDIF
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, eStoredHelmet, FALSE)
		eStoredHelmet = DUMMY_PED_COMP
	ENDIF
	IF eStoredMask != DUMMY_PED_COMP
		IF bAllowDelayForDeadPed
		AND IS_PED_INJURED(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - delay mission cleanup to allow injured ped to swap stored mask")
			EXIT
		ENDIF
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eStoredMask, FALSE)
		eStoredMask = DUMMY_PED_COMP
	ENDIF
	
	IF DOES_CAM_EXIST(cameraTelescope)
		CLEAN_UP_CAMERA()
	ENDIF
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMovie)
	
	PRINTLN("POD: telescope: scaleform no longer needed")
	
	MPGlobalsAmbience.bUsingTelescope = FALSE
	g_bCurrentlyUsingTelescope = FALSE // Prevent Chop running off when using a telescope
	
	IF bSetupPCControls
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SET_UP_CAMERA()
	IF DOES_ENTITY_EXIST(oBlinds)
		SET_ENTITY_VISIBLE(oBlinds, FALSE)
	ENDIF
	cameraTelescope = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vLookCamPos, vLookCamRot, fMinFOV, TRUE)
	//cameraTelescope = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vLookCamPos, fZoomChange, fMinFOV, TRUE)
	
	//vCamRot
	//fZoomChange
	SET_CAM_NEAR_CLIP(cameraTelescope, 0.1)
	SET_CAM_CONTROLS_MINI_MAP_HEADING(cameraTelescope, TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	IF NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		PUSH_TIMECYCLE_MODIFIER()
	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfMovie, 0,0,0,255)
	
	PRINTLN("POD: telescope: drawing scaleform - 2")
	
	IF NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		SET_TIMECYCLE_MODIFIER("telescope")
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

PROC PLAY_SQUEAK(FLOAT squeakAmount)
	IF bPublicTelescope
		IF NOT HAS_SOUND_FINISHED(iSoundTurnVert)
			//STOP_SOUND(iSoundSqueak)
//		ENDIF
		//PLAY_SOUND_FROM_ENTITY(iSoundSqueak, "TELESCOPE_SQUEAK_MASTER", oScope)
			IF squeakAmount < 0
				squeakAmount += -squeakAmount*2
			ENDIF
			SET_VARIABLE_ON_SOUND(iSoundTurnVert, "velocity", squeakAmount)
		ENDIF
		IF NOT HAS_SOUND_FINISHED(iSoundTurnHori)
			//STOP_SOUND(iSoundSqueak)
//		ENDIF
		//PLAY_SOUND_FROM_ENTITY(iSoundSqueak, "TELESCOPE_SQUEAK_MASTER", oScope)
			IF squeakAmount < 0
				squeakAmount += -squeakAmount*2
			ENDIF
			SET_VARIABLE_ON_SOUND(iSoundTurnHori, "velocity", squeakAmount)
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL HAS_MOUSE_JUST_MOVED()
	
//	GET_MOUSE_POSITION(fMouseX, fMouseY)
	fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )

	IF fMouseX != fStoredMouseX
	OR fMouseY != fStoredMouseY
		CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: fStoredMouseX: ", fStoredMouseX, " != fMouseX: ", fMouseX)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: fStoredMouseY: ", fStoredMouseY, " != fMouseY: ", fMouseY)
		fStoredMouseX = fMouseX 
		fStoredMouseY = fMouseY  

//		IF HAS_NET_TIMER_STARTED(telescopeMouseTimer)
//			RESET_NET_TIMER(telescopeMouseTimer)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: mouse still moving ")
//		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(telescopeMouseTimer)
			START_NET_TIMER(telescopeMouseTimer,TRUE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: START_NET_TIMER telescopeMouseTimer")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(telescopeMouseTimer)
		IF HAS_NET_TIMER_EXPIRED(telescopeMouseTimer, 200, TRUE)
			RESET_NET_TIMER(telescopeMouseTimer)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: HAS_NET_TIMER_EXPIRED = TRUE")
			RETURN FALSE
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: HAS_NET_TIMER_EXPIRED = FALSE")
			RETURN TRUE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_MOUSE_JUST_MOVED: HAS_NET_TIMER_STARTED = FALSE")
		RETURN FALSE
	ENDIF
	
ENDFUNC
PROC RUN_FIRST_PERSON_CAMERA()
	FLOAT fTemp//, fHyp, fSideX, fSideY
	VECTOR vCamRot
	//INT iSqueakCounter
	INT right_stick_x, right_stick_y, iIgnore, left_stick_y
	BOOL bPlayZoom = TRUE, bPlayTurn = TRUE
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF IS_CAM_ACTIVE(cameraTelescope)
			 
			//REMAP_LODSCALE_RANGE_THIS_FRAME(1.7,4.7,1.0,1.8)
			USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, FALSE)
			SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(8.0) //this is a multiplier.. 50 is crazy I've changed this down to 2.0 if there are issues talk with James Broad.
			
	//		PRINTLN("GET_CAM_FOV(cameraTelescope)= ", GET_CAM_FOV(cameraTelescope))
	//		PRINTLN("fZoomChange = ", fZoomChange)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfMovie, 0,0,0,255)
			
			PRINTLN("POD: telescope: drawing scaleform - 1")
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
			
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND( iIgnore, left_stick_y, right_stick_x, right_stick_y)
						
				IF NOT IS_LOOK_INVERTED()
					right_stick_y *= -1
				ENDIF
				
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					right_stick_x = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_LR) * 127.0)
					right_stick_y = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_UD) * 127.0)
				ENDIF

				// invert the vertical
				IF (right_stick_y > STICK_DEAD_ZONE)
				OR (right_stick_y < (STICK_DEAD_ZONE * -1))
				OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				
					fTemp = TO_FLOAT(right_stick_y)
					
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						fTemp *= -MOUSE_CAMERA_SPEED_ADJUST
					ELSE
						fTemp *= fTemp
						fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
						IF (right_stick_y < 0)
							fTemp *= -1.0
						ENDIF 
					ENDIF
					
					fTemp *= fCamSpeed
					
					fCamChangeY += fTemp
					//fSideY = fTemp
					IF HAS_SOUND_FINISHED(iSoundTurnVert)
					AND NOT bHitVertLimit
					AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						CDEBUG1LN(DEBUG_SAFEHOUSE, " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						PLAY_SOUND_FROM_ENTITY(iSoundTurnVert, "Turn", oScope, sTelescopeSoundBank)
					ENDIF
					
					
					IF HAS_SOUND_FINISHED(iSoundTurnVert)
					AND NOT bHitVertLimit
					AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					AND HAS_MOUSE_JUST_MOVED()
						CDEBUG1LN(DEBUG_SAFEHOUSE, " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						PLAY_SOUND_FROM_ENTITY(iSoundTurnVert, "Turn", oScope, sTelescopeSoundBank)
					ENDIF
					
					IF NOT HAS_SOUND_FINISHED(iSoundTurnVert)
					AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					AND NOT HAS_MOUSE_JUST_MOVED()
						STOP_SOUND(iSoundTurnVert)
					ENDIF
					
					bPlayTurn = TRUE
					
					IF (fCamChangeY < fMinYChange)
						fCamChangeY = fMinYChange	
						bPlayTurn = FALSE
					ENDIF
					IF (fCamChangeY > fMaxYChange)
						fCamChangeY = fMaxYChange	
						bPlayTurn = FALSE
					ENDIF
					IF fCamChangeY != fMinYChange	
					AND fCamChangeY != fMaxYChange
						bHitVertLimit = FALSE
					ENDIF
				ELSE
					IF NOT HAS_SOUND_FINISHED(iSoundTurnVert)
						STOP_SOUND(iSoundTurnVert)
					ENDIF
					IF fCamChangeY != fMinYChange	
					AND fCamChangeY != fMaxYChange
						bHitVertLimit = FALSE
					ENDIF
				ENDIF
				
				IF bPlayTurn
					PLAY_SQUEAK(fTemp)
				ELSE
					IF NOT HAS_SOUND_FINISHED(iSoundTurnVert)
						IF NOT bHitVertLimit
							STOP_SOUND(iSoundTurnVert)
							PLAY_SOUND_FROM_ENTITY(iSoundTurnLimit, "Turn_Limit", oScope, sTelescopeSoundBank)
							CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - PLAY_SOUND_FROM_ENTITY iSoundTurnLimit")
							bHitVertLimit = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF (right_stick_x > STICK_DEAD_ZONE)
				OR (right_stick_x < (STICK_DEAD_ZONE * -1))
				OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				
					fTemp = TO_FLOAT(right_stick_x)
					
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						fTemp *= -MOUSE_CAMERA_SPEED_ADJUST
					ELSE
						fTemp *= fTemp
						fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
						IF (right_stick_x > 0)
							fTemp *= -1.0
						ENDIF
					ENDIF
					
					fTemp *= fCamSpeed
					
					fCamChangeX += fTemp
					//fSideX = fTemp
					
					
					IF HAS_SOUND_FINISHED(iSoundTurnHori)
					AND NOT bHitHorizLimit
					AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
//						CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						CDEBUG1LN(DEBUG_SAFEHOUSE, " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						PLAY_SOUND_FROM_ENTITY(iSoundTurnHori, "Turn", oScope, sTelescopeSoundBank)
					ENDIF
					
					
					IF HAS_SOUND_FINISHED(iSoundTurnHori)
					AND NOT bHitHorizLimit
					AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					AND HAS_MOUSE_JUST_MOVED()
//						CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						CDEBUG1LN(DEBUG_SAFEHOUSE,  " - PLAY_SOUND_FROM_ENTITY iSoundTurn")
						PLAY_SOUND_FROM_ENTITY(iSoundTurnHori, "Turn", oScope, sTelescopeSoundBank)
					ENDIF
					
					IF HAS_SOUND_FINISHED(iSoundTurnHori)
					AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					AND NOT HAS_MOUSE_JUST_MOVED()
						STOP_SOUND(iSoundTurnHori)
					ENDIF
					
					bPlayTurn = TRUE
					
					
					IF (fCamChangeX < fMinXChange)
						fCamChangeX = fMinXChange	
						bPlayTurn = FALSE
					ENDIF
					IF (fCamChangeX > fMaxXChange)
						fCamChangeX = fMaxXChange	
						bPlayTurn = FALSE
					ENDIF
					IF fCamChangeX != fMinXChange	
					AND fCamChangeX != fMaxXChange	
						bHitHorizLimit = FALSE
					ENDIF
				ELSE	
					IF NOT HAS_SOUND_FINISHED(iSoundTurnHori)
						STOP_SOUND(iSoundTurnHori)
					ENDIF
					IF fCamChangeX != fMinXChange	
					AND fCamChangeX != fMaxXChange	
						bHitHorizLimit = FALSE
					ENDIF
					bPlayTurn = FALSE
				ENDIF
				
				IF bPlayTurn
					PLAY_SQUEAK(fTemp)
				ELSE
					IF NOT HAS_SOUND_FINISHED(iSoundTurnHori)
						IF NOT bHitHorizLimit
							STOP_SOUND(iSoundTurnHori)
							PLAY_SOUND_FROM_ENTITY(iSoundTurnLimit, "Turn_Limit", oScope, sTelescopeSoundBank)
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, GET_THIS_SCRIPT_NAME(), " - PLAY_SOUND_FROM_ENTITY iSoundTurnLimit")
							bHitHorizLimit = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				
				// Zoom in/out
				IF (left_stick_y > STICK_DEAD_ZONE)
				OR (left_stick_y < (STICK_DEAD_ZONE * -1))
				
					fTemp = TO_FLOAT(left_stick_y)
					fTemp *= fTemp
					fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
					fTemp *= fZoomSpeed
					
					IF (left_stick_y < 0)
						fTemp *= -1.0
					ENDIF 
					IF HAS_SOUND_FINISHED(iSoundZoom)
					AND NOT bHitZoomLimit
						PLAY_SOUND_FROM_ENTITY(iSoundZoom, "Zoom", oScope, sTelescopeSoundBank)
					ENDIF
					bPlayZoom = TRUE
					fZoomChange += fTemp
					
					
					IF (fZoomChange < fMaxFOV)
						fZoomChange = fMaxFOV
						bPlayZoom = FALSE
						PRINTLN("(fZoomChange < fMaxFOV)", GET_CAM_FOV(cameraTelescope))
					ENDIF
					IF (fZoomChange > fMinFOV)
						fZoomChange = fMinFOV	
						bPlayZoom = FALSE
						PRINTLN("(fZoomChange > fMinFOV)", GET_CAM_FOV(cameraTelescope))
					ENDIF
					IF fZoomChange != fMaxFOV
					AND fZoomChange != fMinFOV
						bHitZoomLimit = FALSE	
					ENDIF
				ELSE
					IF fZoomChange != fMaxFOV
					AND fZoomChange != fMinFOV
						bHitZoomLimit = FALSE	
					ENDIF
					bPlayZoom = FALSE
				ENDIF
				
				IF NOT bPlayZoom
					IF NOT HAS_SOUND_FINISHED(iSoundZoom)
						IF NOT bHitZoomLimit
							STOP_SOUND(iSoundZoom)
							PLAY_SOUND_FROM_ENTITY(iSoundZoomLimit, "Zoom_Limit", oScope, sTelescopeSoundBank)
							bHitZoomLimit = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
					fCamChangeX = 0
					fCamChangeY = 0
					fZoomChange = 0
				ENDIF
									
				vCamRot = vLookCamRot
				vCamRot += <<fCamChangeY, 0, fCamChangeX>>
				SET_CAM_ROT(cameraTelescope, vCamRot)
				
				fCamSpeed = fZoomChange/(fSpeedMultiplier)
				
				IF fZoomChange > 0
					SET_CAM_FOV(cameraTelescope, fZoomChange)
				ELSE
					SET_CAM_FOV(cameraTelescope, fMinFOV)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_TELESCOPE_CLEAR_OF_PEDS(VECTOR vPos)
	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_AT_COORD(mPed[i], vPos, <<0.5, 0.5, 1.0>>)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_AREA_CLEAR_OF_OBSTRUCTION(VECTOR vPos)
	BOOL bReturn = TRUE
	VEHICLE_INDEX vehTemp
//	PED_INDEX pedTemp
	
	vehTemp = GET_RANDOM_VEHICLE_IN_SPHERE(vPos, 3, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF DOES_ENTITY_EXIST(vehTemp)
		bReturn = FALSE
	ENDIF
	
	vehTemp = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehTemp)
		IF IS_ENTITY_AT_COORD(vehTemp, vPos, <<3,3,3>>)
			bReturn = FALSE
		ENDIF
	ENDIF
	
//	pedTemp = GET_RANDOM_PED_AT_COORD(vPos, <<0.5,0.5,0.5>>, PEDTYPE_PLAYER_NETWORK)
//	IF DOES_ENTITY_EXIST(pedTemp)
//		bReturn = FALSE
//	ENDIF
	
	//Don't allow if another ped is at the telescope.
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//IF IS_ANY_PED_IN_SPHERE(vPos+<<0, 0, 0.2>>, 0.25, PLAYER_PED_ID())
		IF NOT IS_TELESCOPE_CLEAR_OF_PEDS(vPos)
			bReturn = FALSE
		ENDIF
	ENDIF
	
//	PRINTLN("@@@@@@@@@ IS_AREA_CLEAR_OF_OBSTRUCTION - NET = ", bReturn, " @@@@@@@@@")
	RETURN bReturn
ENDFUNC

PROC INITIALISE_SOUNDS()
	iSoundCoinDrop = GET_SOUND_ID()
	iSoundCountdown = GET_SOUND_ID()
	iSoundTimer = GET_SOUND_ID()
	iSoundTurnVert = GET_SOUND_ID()
	iSoundTurnHori = GET_SOUND_ID()
	iSoundTurnLimit = GET_SOUND_ID() 
	iSoundZoom = GET_SOUND_ID() 
	iSoundZoomLimit = GET_SOUND_ID() 
//	iSoundEndUse = GET_SOUND_ID()
	
	IF bPublicTelescope
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			sTelescopeSoundBank = "TELESCOPE_High_End"
		ELSE
			sTelescopeSoundBank = "TELESCOPE_COIN_OP"
		ENDIF
	ELSE
		sTelescopeSoundBank = "TELESCOPE_DOMESTIC"
	ENDIF
ENDPROC

PROC PLAY_COIN_DROP()
	PRINTSTRING("PLAY_COIN_DROP") PRINTNL()
	IF bPublicTelescope
		PRINTSTRING("PLAY_COIN_DROP bPublicTelescope") PRINTNL()
		
		IF NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			PLAY_SOUND_FROM_ENTITY(iSoundCoinDrop, "Insert_Coin", oScope, sTelescopeSoundBank)
			
			PLAY_SOUND_FROM_ENTITY(iSoundTimer, "Timer", oScope, sTelescopeSoundBank)
		ENDIF
		
		IF g_bInMultiplayer
			IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_ARENA_SPECTATOR_BOX, 2, iTransactionID)
					
					g_cashTransactionData[iTransactionID].cashInfo.iItemHash = 0
				ELSE
					NETWORK_SPEND_ARENA_SPECTATOR_BOX(2, 0, FALSE, FALSE)
				ENDIF
			ELSE
				GIVE_LOCAL_PLAYER_FM_CASH(-1,1,TRUE,0)
				
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_TELESCOPE, 1, iTransactionID)
				ELSE
					NETWORK_SPENT_TELESCOPE(1)
				ENDIF
			ENDIF
			
			
		ELSE
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, scopeCost)
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_COUNTDOWN()
	IF bPublicTelescope
		STOP_SOUND(iSoundTimer)
		PLAY_SOUND_FROM_ENTITY(iSoundCountdown, "10_Seconds", oScope, sTelescopeSoundBank)
	ENDIF
ENDPROC

PROC GET_CAM_POS_AND_ROTATION_FOR_SCOPE()
	
	VECTOR vMin, vMax, vOffset
	
	IF GET_ENTITY_MODEL(oScope) = PROP_TELESCOPE
	OR GET_ENTITY_MODEL(oScope) = PROP_TELESCOPE_01
	OR GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
		bPublicTelescope = TRUE
	ENDIF
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(oScope), vMin, vMax)
	vLookCamRot = GET_ENTITY_ROTATION(oScope)
	
	IF bPublicTelescope
		
		sScopeContextHelp = "TELEHLP"
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			sScopeContextHelp = "TELEHLP2"
		ENDIF
		
		IF GET_ENTITY_MODEL(oScope) = PROP_TELESCOPE_01
		OR GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
			vLookCamRot.z -= 180
			vOffset = <<0, -vMax.y-0.5, vMax.z+0.5>>
		ELSE
			vOffset = <<0, vMax.y, vMax.z>>
		ENDIF
	ELSE
		vLookCamRot.z -= 270
		sScopeContextHelp = "TELEHOME"
		vOffset = <<-fHomeTelescopeOffset, 0, (vMax.z+0.2)>>
	ENDIF
	
	IF vLookCamRot.z < 0
		vLookCamRot.z += 360
	ENDIF
	
	vLookCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, vOffset)
ENDPROC

FUNC BOOL IS_PLAYER_TAKING_DAMAGE()
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(PLAYER_PED_ID())
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(PLAYER_PED_ID())
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(PLAYER_PED_ID())
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_OK(PLAYER_INDEX thePlayer)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_NET_PLAYER_OK(thePlayer)
		OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		OR IS_PLAYER_TAKING_DAMAGE()
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_ENOUGH_MONEY()
	IF g_bInMultiplayer
		IF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
			IF NETWORK_CAN_SPEND_MONEY(2, FALSE, FALSE, FALSE)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF GET_PLAYER_FM_CASH(PLAYER_ID()) > 0
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_CLEAR_LOS_TO_OBSERVATORY_SCOPE()
	IF DOES_ENTITY_EXIST(oScope)
		IF GET_ENTITY_MODEL(oScope) = PROP_TELESCOPE_01
		OR GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
			IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), oScope)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_TELESCOPE()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//MP checks.
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
		OR IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
			RETURN FALSE
		ENDIF
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		AND NOT IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
			CDEBUG2LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_USE_TELESCOPE: IS_PLAYER_CRITICAL_TO_ANY_EVENT = TRUE, returning FALSE")
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
			PRINTLN ("CAN_PLAYER_USE_TELESCOPE: IS_PLAYER_USING_BALLISTIC_EQUIPMENT = TRUE, returning FALSE")
			RETURN FALSE
		
		ENDIF
		
		IF NOT HAS_PLAYER_FOUND_DOUBLE_ACTION_REVOLVER()

			IF g_treasurehunt_is_player_close_to_first_clue
				PRINTLN ("CAN_PLAYER_USE_TELESCOPE: HAS_PLAYER_FOUND_DOUBLE_ACTION_REVOLVER g_treasurehunt_is_player_close_to_first_clue , returning FALSE")
				RETURN FALSE
			ENDIF
			
		ENDIF
		
		IF ARE_TELESCOPES_DISABLED()
			PRINTLN ("CAN_PLAYER_USE_TELESCOPE: ARE_TELESCOPES_DISABLED = TRUE , returning FALSE")
			RETURN FALSE
		ENDIF
		
		IF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
			IF NOT IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 0.0, 50.0)
				PRINTLN ("CAN_PLAYER_USE_TELESCOPE: IS_HEADING_ACCEPTABLE_CORRECTED , returning FALSE")
				RETURN FALSE
			ENDIF
			
			IF g_bCelebrationScreenIsActive
				PRINTLN ("CAN_PLAYER_USE_TELESCOPE: g_bCelebrationScreenIsActive , returning FALSE")
				RETURN FALSE
			ENDIF
			
			IF g_sMPTunables.bAW_DISABLE_TELESCOPE
				PRINTLN ("CAN_PLAYER_USE_TELESCOPE: bAW_DISABLE_TELESCOPE , returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		//SP checks.
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check the player is alone in the trigger area
FUNC BOOL IS_PLAYER_ALONE_IN_AREA(VECTOR VecCoords, VECTOR locateSize)

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_AT_COORD(mPed[i], VecCoords, <<locateSize.x,locateSize.y,locateSize.z>>)
//				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerArea1, vTriggerArea2, AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC runTelescope()
	//BOOL y, yy, yyy, yyyy
	
	VECTOR buyPos//, triggerPos = GET_ENTITY_COORDS(oScope)
	
	IF bPublicTelescope
		IF GET_ENTITY_MODEL(oScope) = PROP_TELESCOPE
			// Mountain
			buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<0.02, -0.78, 0.0>>)
			animEnter = "UPRIGHT_ENTER_FRONT"
			animLoop = "UPRIGHT_IDLE"
			animExit = "UPRIGHT_EXIT_FRONT"
//			vScopeHeading = 0
		ELIF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
			buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<-0.03, 0.96, 0.0>>)
			
			animEnter = "PUBLIC_ENTER_FRONT"
			animLoop = "PUBLIC_IDLE"
			animExit = "PUBLIC_EXIT_FRONT"
		ELSE 
			// Observatory
			IF g_bInMultiplayer
				buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<-0.03, 0.96, 0.0>>)
			ELSE
	//			SCRIPT_ASSERT("POD: NOT in multiplayer")
				buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<0.2, 0.96, 0.0>>)
			ENDIF
			
			animEnter = "PUBLIC_ENTER_FRONT"
			animLoop = "PUBLIC_IDLE"
			animExit = "PUBLIC_EXIT_FRONT"
//			vScopeHeading = -180
		ENDIF
//#IF NOT USE_CLF_DLC 
//#IF NOT USE_NRM_DLC
	ELSE
		PRINTLN("POD: TELESCOPES: private telescope")
		// Franklin's apartment
//		buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<1.14, -0.35, 0.0>>)
		IF g_bInMultiplayer
//			SCRIPT_ASSERT("POD: in multiplayer")
			buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<1.14, 0.02, 0.0>>)
		ELSE
			#IF NOT USE_SP_DLC
//				SCRIPT_ASSERT("POD: NOT in multiplayer")
				buyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oScope, <<1.0, -0.4, 0.0>>)				
			#ENDIF
		ENDIF

		animEnter = "ENTER_FRONT"
		animLoop = "IDLE"
		animExit = "EXIT_FRONT"
//		vScopeHeading = 90
//		buyPos = GET_ANIM_INITIAL_OFFSET_POSITION(animDictEnter, "ENTER_FRONT", GET_ENTITY_COORDS(oScope), <<0,0,vScopeHeading>>) 

		#IF NOT USE_SP_DLC
			FLOW_BITSET_IDS eFlowBitsetToQuery
			INT iOffsetBitIndex
			IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) <= 31 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) 
			ELSE 
				eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
				iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) - 31 
			ENDIF 
			
			IF IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
				PRINTLN("POD: TELESCOPES: iOffsetBitIndex exiting")
				EXIT
			ENDIF
		#endif
	ENDIF
	
	VECTOR locateSize = <<1.2,1.2,1.2>>
	IF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		locateSize = <<0.8,0.8,0.8>>
	ENDIF
	
	IF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
		locateSize = <<0.8,0.7, 1.0>>
	ENDIF
	
	//PRINTSTRING("Telescop coords:") PRINTVECTOR(GET_ENTITY_COORDS(oScope)) PRINTNL()
	IF bPublicTelescope
		IF iTelescopeStartTime > 0
			IF GET_GAME_TIMER() > (iTelescopeStartTime+(TELESCOPE_TIME-9500))
				IF HAS_SOUND_FINISHED(iSoundCountdown)
					IF GET_GAME_TIMER() < (iTelescopeStartTime+TELESCOPE_TIME)
						PLAY_COUNTDOWN()
					ELSE
						IF scopeStage <> grabPlayer
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PED_SLIDE_TO_COORD) = FINISHED_TASK
								iTelescopeStartTime = -1
							ENDIF
						ENDIF
						STOP_SOUND(iSoundTimer)
						STOP_SOUND(iSoundTurnVert)
						STOP_SOUND(iSoundTurnHori)
						STOP_SOUND(iSoundZoom)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
		IF NOT DOES_ENTITY_EXIST(oBlinds)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "grabbing new blinds")
			oBlinds = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(oScope), 20, INT_TO_ENUM(MODEL_NAMES, HASH("EX_prop_office_louvres")), FALSE)		
		ENDIF		
	ENDIF
	PRINTLN("POD: TELESCOPES: scopeStage", scopeStage)
	
	SWITCH scopeStage
		
		CASE playerOutOfRange
		
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, <<locateSize.x*2,locateSize.y*2,locateSize.z>>)
					IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(oScope)
//						REQUEST_AMBIENT_AUDIO_BANK("TELESCOPES")
						IF bPublicTelescope
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TELECSH")
								INT iCost
								
								iCost = 0
								
								IF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
									iCost = 1
								ENDIF
								
								IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) > iCost
								OR GET_GAME_TIMER() < (iTelescopeStartTime+TELESCOPE_TIME)
									scopeStage = waitForPlayer
								ENDIF
							ENDIF
						ELSE
							scopeStage = waitForPlayer
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE waitForPlayer
		
			IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
				IF NOT g_sShopSettings.bProcessStoreAlert
					CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
					PRINTLN("ob_telescope: NET_SET_PLAYER_CONTROL- A")
					//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					PRINTLN("@@@@@@@@@@  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
				ENDIF
			ENDIF
			
//			IF NOT MPGlobalsAmbience.bUsingTelescope
//			OR NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF CAN_PLAYER_START_CUTSCENE()
				AND IS_PLAYER_OK(PLAYER_ID())
				
				//y = IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, <<locateSize.x,locateSize.y,locateSize.z>>)
				//yy =  IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//yyy = IS_AREA_CLEAR_OF_OBSTRUCTION(buyPos)
				//yyyy =GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(oScope)
				
				//PRINTSTRING("Paul - bools ") PRINTBOOL(y)PRINTBOOL(yy)PRINTBOOL(yyy)PRINTBOOL(yyyy) PRINTNL()
					IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, <<0.5, 0.5, 1.0>>)
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, <<locateSize.x,locateSize.y,locateSize.z>>)
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND IS_AREA_CLEAR_OF_OBSTRUCTION(buyPos)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(oScope)
					AND HAS_PLAYER_CLEAR_LOS_TO_OBSERVATORY_SCOPE()
					AND CAN_PLAYER_USE_TELESCOPE()
					AND NOT scopeFallen
					AND IS_PLAYER_ALONE_IN_AREA(buyPos, locateSize)
					 
						IF DOES_PLAYER_HAVE_ENOUGH_MONEY()
						OR GET_GAME_TIMER() < (iTelescopeStartTime+TELESCOPE_TIME)
						OR NOT bPublicTelescope
						
							IF IS_PLAYER_PLAYING(PLAYER_ID())
							AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
							
	//							IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
	//							AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
									
									IF bPublicTelescope
										IF GET_GAME_TIMER() < (iTelescopeStartTime+TELESCOPE_TIME)
											sScopeContextHelp = "TELEHOME"
										ELSE
											IF ARE_STRINGS_EQUAL(sScopeContextHelp, "TELEHOME")
											//	PRINTSTRING("RELEASE CONTEXT 1") PRINTNL()
												RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
											ENDIF
											sScopeContextHelp = "TELEHLP"
											
											IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
												sScopeContextHelp = "TELEHLP2"
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, locateSize)
									AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									AND NOT IS_PAUSE_MENU_ACTIVE_EX()
									AND ((GET_GAME_TIMER() < (iTelescopeStartTime+TELESCOPE_TIME)) OR (iTelescopeStartTime = -1))
									AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), animDictExit, animExit)
										IF iContextButtonIntention = NEW_CONTEXT_INTENTION
											REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, sScopeContextHelp)
										ENDIF
										
										BOOL bDontRequestAudio
										
										IF GET_ENTITY_MODEL(oScope) = INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
											bDontRequestAudio = TRUE
										ENDIF
										
										IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
										AND (bDontRequestAudio OR REQUEST_AMBIENT_AUDIO_BANK("TELESCOPES"))
											eStoredHelmet = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
											eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
											IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
												PRINTLN("POD: TELESCOPE: IN FIRST PERSON")
											ELSE	
												PRINTLN("POD: TELESCOPE: NOT IN FIRST PERSON")
											ENDIF
											CLEAR_HELP()
											GET_CAM_POS_AND_ROTATION_FOR_SCOPE()
											IF NOT g_bInMultiplayer
												IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
													VECTOR tempHeadingVector 
													tempHeadingVector = <<0, 0, GET_ENTITY_HEADING(oScope)>>
													
													VECTOR vTempPos 
													vTempPos = GET_ANIM_INITIAL_OFFSET_POSITION( animDictEnter, animEnter, GET_ENTITY_COORDS(oScope), tempHeadingVector )  
													VECTOR vTempRotation 
													vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION( animDictEnter, animEnter, GET_ENTITY_COORDS(oScope), tempHeadingVector )
													TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTempPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, vTempRotation.Z, 0.05)
													PRINTLN("POD: TELESCOPE: TASK_GO_STRAIGHT_TO_COORD")
												ELSE
													SET_MULTIHEAD_SAFE(TRUE,TRUE)	//B* 2223476
													SET_UP_CAMERA()
													RUN_FIRST_PERSON_CAMERA()
													PRINTLN("POD: TELESCOPE: SET_UP_CAMERA 1")
												ENDIF
												STOP_FIRE_IN_RANGE(buyPos, 10)
												SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
												g_bCurrentlyUsingTelescope = TRUE // Prevent Chop running off when using a telescope
												SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
											ELSE
												VECTOR tempHeadingVector 
												tempHeadingVector = <<0, 0, GET_ENTITY_HEADING(oScope)>>
												
												VECTOR vTempPos 
												vTempPos = GET_ANIM_INITIAL_OFFSET_POSITION( animDictEnter, animEnter, GET_ENTITY_COORDS(oScope), tempHeadingVector )  
												VECTOR vTempRotation 
												vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION( animDictEnter, animEnter, GET_ENTITY_COORDS(oScope), tempHeadingVector )
												SET_MULTIHEAD_SAFE(TRUE,TRUE)
												TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTempPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, vTempRotation.Z, 0.05)
//												SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), oScope, TRUE)
//												TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), buyPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, GET_ENTITY_HEADING(oScope) + vScopeHeading, 0.1)
												DISABLE_INTERACTION_MENU()
												NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
												MPGlobalsAmbience.bUsingTelescope = TRUE
											ENDIF
											
											IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
												SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(oScope)))
												PRINTLN("POD: SET_PED_DESIRED_HEADING: ", GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(oScope)))
												FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
												SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
											ENDIF
											
											GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
											/*OPEN_SEQUENCE_TASK(seq)
												TASK_PED_SLIDE_TO_COORD_HDG_RATE(NULL, buyPos, vLookCamRot.z, 100.0, 270.0)
												TASK_PLAY_ANIM(NULL, animDictEnter, animEnter, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
												//TASK_PLAY_ANIM(NULL, animDictEnter, "IDLE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
											PRINTSTRING("PAUL -  FORCE_PED_AI_AND_ANIMATION_UPDATE waitForPlayer") PRINTNL()
											FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
											CLEAR_SEQUENCE_TASK(seq)*/
											
											RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
											SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
											SETTIMERA(0)
											scopeStage = grabPlayer
											
											INIT_PC_SCRIPTED_CONTROLS("Ob_Telescope")
											bSetupPCControls = TRUE
											//WAIT(100000)
										ENDIF
									ELSE
										PRINTSTRING("RELEASE CONTEXT 4") PRINTNL()
										RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
									ENDIF
	//							ELSE
	//								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
	//							ENDIF
							ELSE
								//PRINTSTRING("RELEASE CONTEXT 5") PRINTNL()
								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
							ENDIF
						ELSE
							IF NOT NETWORK_IS_GAME_IN_PROGRESS()
							//	PRINTSTRING("RELEASE CONTEXT 6") PRINTNL()
								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
								PRINT_HELP("TELECSH")
							ELSE
								IF iContextButtonIntention = NEW_CONTEXT_INTENTION
									IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
										REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "TELECSH")
									ELSE
										REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "TELEHLP")
									ENDIF
								ENDIF
								
								IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
								AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
									IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
										LAUNCH_STORE_CASH_ALERT()
										SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
										//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
										PRINTLN("@@@@@@@@@@  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CALLED")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT IS_PLAYER_ALONE_IN_AREA(buyPos, locateSize)	
							CDEBUG1LN(DEBUG_SAFEHOUSE, "waitForPlayer: IS_PLAYER_ALONE_IN_AREA = FALSE")
						ENDIF
						#ENDIF
						//PRINTSTRING("RELEASE CONTEXT 7") PRINTNL()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
					ENDIF
				ELSE
					//PRINTSTRING("RELEASE CONTEXT 8") PRINTNL()
					RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
				ENDIF
//			ELSE
//				//PRINTSTRING("RELEASE CONTEXT 9") PRINTNL()
//				RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
//			ENDIF
		BREAK
		
		CASE grabPlayer
			IF IS_PLAYER_OK(PLAYER_ID()) // CDM- Fix for focus not clearing when dead
				IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					sfMovie = REQUEST_SCALEFORM_MOVIE("observatory_scope")
					
					PRINTLN("POD: telescope: requesting scaleform")
					
					WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(sfMovie))
						WAIT(0)
					ENDWHILE
					
					PRINTLN("POD: telescope: scaleform loaded")
				ENDIF
				
				IF NOT g_bInMultiplayer
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
						PRINTLN("POD: TELESCOPE: SET_UP_CAMERA 2")
						RUN_FIRST_PERSON_CAMERA()
					ENDIF
				ENDIF
				
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(oScope)))
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
				ENDIF
				
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
				//OR NOT g_bInMultiplayer
				//AND TIMERA() > 2500
					DISABLE_SELECTOR_THIS_FRAME()
					PRINTLN("POD: TELESCOPE: FINISHED APPRAOCH SUCCESSFULLY")
					
					//SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), buyPos)//, vLookCamRot.z, 100.0, 270.0)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(), vLookCamRot.z)
//					IF NOT g_bInMultiplayer
//						TASK_PLAY_ANIM(PLAYER_PED_ID(), animDictEnter, animLoop, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_LOOPING)
//						scopeStage = playEnter
//					ELSE
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
						TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), animDictEnter, animEnter, GET_ENTITY_COORDS(oScope), GET_ENTITY_ROTATION(oScope),REALLY_SLOW_BLEND_IN, 0.1, -1, AF_HIDE_WEAPON|AF_EXTRACT_INITIAL_OFFSET|AF_USE_MOVER_EXTRACTION)//|AF_TURN_OFF_COLLISION)
					ENDIF
						scopeStage = playEnter
//					ENDIF
					PRINTSTRING("PAUL -  FORCE_PED_AI_AND_ANIMATION_UPDATE grabPlayer") PRINTNL()
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					
					IF g_bInMultiplayer
						PRINTLN("ob_telescope: removing masks")
						
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
						IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
							PRINTLN("ob_telescope: removing helmet")
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
						ENDIF 
						REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
						IF IS_PED_WEARING_PILOT_SUIT(PLAYER_PED_ID(), PED_COMP_TEETH)
							PRINTLN("ob_telescope: removing pilot suit")
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
						ENDIF
						IF IS_PED_WEARING_HAZ_HOOD_UP(PLAYER_PED_ID())
							SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
							bSwappedChemSuit = TRUE
						ENDIF
					ENDIF

					
				ELIF TIMERA() > 2500
				AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					IF NOT g_bInMultiplayer
						PRINTLN("POD: TELESCOPE: FAILED APPRAOCH ")
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						g_bCurrentlyUsingTelescope = FALSE
					ELSE
						ENABLE_INTERACTION_MENU()
						PRINTLN("ob_telescope: NET_SET_PLAYER_CONTROL- B")
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						IF bSetupPCControls
							SHUTDOWN_PC_SCRIPTED_CONTROLS()
							bSetupPCControls = FALSE
						ENDIF
						SET_MULTIHEAD_SAFE(FALSE) 
						MPGlobalsAmbience.bUsingTelescope = FALSE
					ENDIF
					SET_MULTIHEAD_SAFE(FALSE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, TRUE)
					scopeStage = waitForPlayer
				ENDIF
			ELSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				IF NOT g_bInMultiplayer
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					g_bCurrentlyUsingTelescope = FALSE
				ELSE
					ENABLE_INTERACTION_MENU()
					PRINTLN("ob_telescope: NET_SET_PLAYER_CONTROL- C")
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					SET_MULTIHEAD_SAFE(FALSE)
					MPGlobalsAmbience.bUsingTelescope = FALSE
				ENDIF
				IF bSetupPCControls
					SHUTDOWN_PC_SCRIPTED_CONTROLS()
					bSetupPCControls = FALSE
				ENDIF
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, TRUE)
				scopeStage = waitForPlayer
			ENDIF
		BREAK
		
		CASE playEnter
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
			
				//IF g_bInMultiplayer
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
					TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), animDictEnter, animLoop, GET_ENTITY_COORDS(oScope), GET_ENTITY_ROTATION(oScope),REALLY_SLOW_BLEND_IN, 0.1, -1, AF_HIDE_WEAPON|AF_LOOPING|AF_EXTRACT_INITIAL_OFFSET|AF_USE_MOVER_EXTRACTION)//|AF_TURN_OFF_COLLISION)
				ENDIF
				
				//ENDIF
					
				
				IF bPublicTelescope
					IF iTelescopeStartTime = -1
						IF NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
							iTelescopeStartTime = GET_GAME_TIMER()
						ENDIF
						
						PLAY_COIN_DROP()
					ENDIF
				ENDIF
				
				//IF NOT g_bInMultiplayer
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
						PRINTLN("POD: TELESCOPE: SET_UP_CAMERA 2")
						SET_UP_CAMERA()
					ENDIF
				//ENDIF
				
				IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_THROUGH_A_LENS)
					SET_LOCAL_PLAYER_ARENA_GARAGE_TROPHY_LENS_DATA()
				ENDIF
				
				RUN_FIRST_PERSON_CAMERA()
				IF NOT g_bInMultiplayer
					CLEAR_AREA_OF_PROJECTILES(buyPos, 10)
				ENDIF
				scopeStage = runScope
			ENDIF
		
		BREAK
		
		CASE runScope
			IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			AND g_bCelebrationScreenIsActive
				PRINTLN("ob_telescope: scopeStage = resetScope")
				
				scopeStage = resetScope
				
				EXIT
			ENDIF
			
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			
			//IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			IF IS_PLAYER_OK(PLAYER_ID()) // CDM- Fix for focus not clearing when dead
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), buyPos, << 1.0, 1.0, 1.5 >>)
				DISABLE_SELECTOR_THIS_FRAME()
				RUN_FIRST_PERSON_CAMERA()
				
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TELEUSE")
					IF IS_APARTMENT_OWNER_IN_PREVIEW_MODE()
						PRINT_HELP_NO_SOUND("TELEUSE")
					ELSE 
						PRINT_HELP_FOREVER("TELEUSE")
					ENDIF	
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				OR scopeFallen
					scopeStage = resetScope
				ELIF bPublicTelescope
				AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					IF (GET_GAME_TIMER() > (iTelescopeStartTime+TELESCOPE_TIME))
						scopeStage = resetScope
					ENDIF
				ENDIF
				IF NOT bRespottedPlayer
					//Removed, as we see the character move.
					//GET_GROUND_Z_FOR_3D_COORD(buyPos, buyPos.z)
					//SET_ENTITY_COORDS(PLAYER_PED_ID(), buyPos)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(), vLookCamRot.z)
					//TASK_PLAY_ANIM(PLAYER_PED_ID(), animDictEnter, "IDLE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_LOOPING)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					bRespottedPlayer = TRUE
				ENDIF
			ELSE
				CLEAN_UP_CAMERA()
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				IF NOT g_bInMultiplayer
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					g_bCurrentlyUsingTelescope = FALSE
				ELSE
					ENABLE_INTERACTION_MENU()
					PRINTLN("ob_telescope: NET_SET_PLAYER_CONTROL- D")
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					SET_MULTIHEAD_SAFE(FALSE)
					MPGlobalsAmbience.bUsingTelescope = FALSE
				ENDIF
				IF bSetupPCControls
					SHUTDOWN_PC_SCRIPTED_CONTROLS()
					bSetupPCControls = FALSE
				ENDIF
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, TRUE)
				scopeStage = waitForPlayer
			ENDIF
		BREAK
		
		CASE resetScope
			IF bSwappedChemSuit = TRUE
				SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
				bSwappedChemSuit = FALSE
			ENDIF
			IF eStoredHelmet != DUMMY_PED_COMP
			AND eStoredHelmet != PROPS_HEAD_NONE
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, eStoredHelmet, FALSE)
				eStoredHelmet = DUMMY_PED_COMP
			ENDIF
			IF eStoredMask != DUMMY_PED_COMP
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eStoredMask, FALSE)
				eStoredMask = DUMMY_PED_COMP
			ENDIF
			
			PRINTLN("POD: TELESCOPE: REAPPLYING MASK")
			STOP_SOUND(iSoundTurnVert)
			STOP_SOUND(iSoundTurnHori)
			STOP_SOUND(iSoundZoom)
			
			IF GET_ENTITY_MODEL(oScope) != INT_TO_ENUM(MODEL_NAMES, HASH("XS_PROP_ARENA_TELESCOPE_01"))
				IF REQUEST_AMBIENT_AUDIO_BANK("TELESCOPES")
					RELEASE_AMBIENT_AUDIO_BANK()
				ENDIF
			ENDIF
			
			bRespottedPlayer = FALSE
			CLEAN_UP_CAMERA()
			
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.536638,529.315369,173.628204>>, <<12.656813,528.802307,175.878204>>, 2)
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN("POD: TELESCOPE: TASK_PLAY_ANIM")
					//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					TASK_PLAY_ANIM(PLAYER_PED_ID(), animDictExit, animExit, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT|AF_HIDE_WEAPON, 0.0)
	//				TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), animDictExit, animExit, GET_ENTITY_COORDS(oScope), GET_ENTITY_ROTATION(oScope),REALLY_SLOW_BLEND_IN, 0.1, -1, AF_HIDE_WEAPON|AF_USE_KINEMATIC_PHYSICS|AF_EXTRACT_INITIAL_OFFSET|AF_USE_MOVER_EXTRACTION|AF_TURN_OFF_COLLISION)
				ELSE
					PRINTLN("POD: TELESCOPE: STOPPING ANIM PLAYBACK")
					//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	//				TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), animDictExit, animExit, GET_ENTITY_COORDS(oScope), GET_ENTITY_ROTATION(oScope),REALLY_SLOW_BLEND_IN, 0.1, -1, AF_HIDE_WEAPON|AF_EXTRACT_INITIAL_OFFSET|AF_USE_MOVER_EXTRACTION|AF_TURN_OFF_COLLISION)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), animDictExit, animExit, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT|AF_HIDE_WEAPON, 0.0)
	//				STOP_ANIM_TASK(PLAYER_PED_ID(),animDictEnter,animEnter)
	//				STOP_ANIM_PLAYBACK(PLAYER_PED_ID(),AF_PRIORITY_HIGH)
				ENDIF
			ENDIF
			
			PRINTSTRING("PAUL -  FORCE_PED_AI_AND_ANIMATION_UPDATE resetScope") PRINTNL()
				
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			IF NOT g_bInMultiplayer
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				g_bCurrentlyUsingTelescope = FALSE
			ELSE
				ENABLE_INTERACTION_MENU()
				PRINTLN("ob_telescope: NET_SET_PLAYER_CONTROL- E")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_MULTIHEAD_SAFE(FALSE)
				MPGlobalsAmbience.bUsingTelescope = FALSE
			ENDIF
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, TRUE)
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TELEUSE")
				CLEAR_HELP()
			ENDIF
			
			IF bSetupPCControls
				SHUTDOWN_PC_SCRIPTED_CONTROLS()
				bSetupPCControls = FALSE
			ENDIF
		
			scopeStage = waitForPlayer
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_DEBUG()
	
	#IF IS_DEBUG_BUILD
 	START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())			
		ADD_WIDGET_FLOAT_SLIDER("fSpeedMultiplier", fSpeedMultiplier, 2, 80, 1)
	STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC

FUNC BOOL GET_IS_OBJECT_FALLEN(OBJECT_INDEX ob)
	VECTOR rot = GET_ENTITY_ROTATION(ob)
	IF rot.x < -5.0
	OR rot.x > 5.0
	OR rot.y < -5.0
	OR rot.y > 5.0
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oTelescope)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP) 
	//OR IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
		missionCleanup(FALSE)
	ENDIF
	
	oScope = oTelescope
	IF DOES_ENTITY_EXIST(oScope)
		//FREEZE_ENTITY_POSITION(oScope, TRUE)
		GET_CAM_POS_AND_ROTATION_FOR_SCOPE()
	ENDIF
	
//	GET_MOUSE_POSITION(fStoredMouseX, fStoredMouseY)
	fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
	
	SETUP_DEBUG()
		
	// Mission Loop -------------------------------------------//
	WHILE TRUE
		
		WAIT(0)
		
		IF DOES_ENTITY_EXIST(oScope)
			IF GET_IS_OBJECT_FALLEN(oScope)
				PRINTLN("ob_telescope: GET_IS_OBJECT_FALLEN TRUE")
				DRAW_DEBUG_TEXT_2D("SCOPE FALLEN", <<0.02, 0.1, 0>>)
				scopeFallen = TRUE
			ELSE
				DRAW_DEBUG_TEXT_2D("SCOPE UPRIGHT", <<0.02, 0.1, 0>>)
				scopeFallen = FALSE
			ENDIF
			
			IF NOT bPublicTelescope
				PRINTLN("ob_telescope: FREEZE_ENTITY_POSITION oScope")
				FREEZE_ENTITY_POSITION(oScope, TRUE)
			ENDIF
//			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//				FREEZE_ENTITY_POSITION(oScope, TRUE)
//				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT bPublicTelescope
//					FREEZE_ENTITY_POSITION(oScope, FALSE)
//				ELSE
//					FREEZE_ENTITY_POSITION(oScope, TRUE)
//				ENDIF
//			ENDIF
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oScope)
				//IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
				
					IF g_bStripperHideHUDCutScene
						CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_telescope: mission cleanup called for g_bStripperHideHUDCutScene")
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
						missionCleanup(TRUE)
					ENDIF
					
					PRINTLN("POD: telescope: ambStage: ", ambStage)
					SWITCH ambStage
						
						CASE ambCanRun
							IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
								PRINTLN("POD: telescope: requesting anims")
								
								REQUEST_ANIM_DICT(animDictEnter)
								REQUEST_ANIM_DICT(animDictExit)
								
								WHILE (NOT HAS_ANIM_DICT_LOADED(animDictEnter))
								OR NOT HAS_ANIM_DICT_LOADED(animDictExit)
									WAIT(0)
								ENDWHILE
								
								INITIALISE_SOUNDS()
								
								PRINTLN("POD: telescope: anims loaded")
								
								ambStage = ambRunning
							ELSE
								sfMovie = REQUEST_SCALEFORM_MOVIE("observatory_scope")
								
								PRINTLN("POD: telescope: requesting scaleform")
								
								REQUEST_ANIM_DICT(animDictEnter)
								REQUEST_ANIM_DICT(animDictExit)
								
								WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(sfMovie))
								OR NOT HAS_ANIM_DICT_LOADED(animDictEnter)
								OR NOT HAS_ANIM_DICT_LOADED(animDictExit)
									HINT_AMBIENT_AUDIO_BANK("TELESCOPES")
									WAIT(0)
								ENDWHILE
								
								INITIALISE_SOUNDS()
								
								PRINTLN("POD: telescope: scaleform loaded")
								
								ambStage = ambRunning
							ENDIF
						BREAK
						
						CASE ambRunning
							IF IS_PLAYER_PLAYING(PLAYER_ID())
							AND IS_SKYSWOOP_AT_GROUND() 
								runTelescope()
							ELSE
								IF DOES_CAM_EXIST(cameraTelescope)
									CLEAN_UP_CAMERA()
								ENDIF
							ENDIF
						BREAK
						
						CASE ambEnd
						BREAK
					ENDSWITCH 
				//ELSE
				//	missionCleanup(TRUE)
				//ENDIF
			ELSE
				missionCleanup(TRUE)
			ENDIF
		ELSE
			missionCleanup(TRUE)
		ENDIF
	ENDWHILE
ENDSCRIPT
