// Includes
USING "familySchedule_private.sch"
USING "ob_safehouse_common.sch"
USING "tv_control_public.sch"
USING "commands_ped.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_sofa_franklin.sc
//		DESCRIPTION		:	Script that handles Franklin's sofa interaction
//
// *****************************************************************************************
// *****************************************************************************************

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_SEAT_PLAYER,
	TS_WAIT_FOR_SEATING,
	TS_SHOW_SOFA_INSTRUCTIONS,
	TS_IDLE_NO_REMOTE,
	TS_PICK_UP_REMOTE,
	TS_IDLE_WITH_REMOTE,
	TS_CHANGE_CHANNEL,
	TS_PUT_REMOTE_DOWN,
	TS_RUN_SMOKE,
	TS_DROP_REMOTE_THEN_QUIT,
	TS_QUITTING,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

ENUM SMOKE_STATE
	SS_DROP_REMOTE = 0,
	SS_ENTER,
	SS_IDLE, 
	SS_EXIT, 
	SS_PICK_UP_REMOTE,
	SS_START_SEATING_IDLE,
	SS_FLAG_FINISHED
ENDENUM
SMOKE_STATE eSmokeState = SS_ENTER

ENUM PARTICLE_STATE
	PT_ENTRY = 0,
	PT_IDLE_A,
	PT_IDLE_B, 
	PT_EXIT
ENDENUM

// Variables
BOOL		  	bLighterFlame 		= FALSE	
BOOL		  	bLighterSparks 		= FALSE
BOOL		  	bExhaleMouth 		= FALSE
BOOL		  	bExhaleNose 		= FALSE
BOOL 			bForceEnterSmoke	= FALSE
BOOL 			bBreakoutEarly		= FALSE
BOOL			bSetBreakoutAnim	= FALSE
BOOL 			bTriggerExitCam 	= FALSE
BOOL 			bNeedTvOff 			= FALSE
BOOL			bNeedChangeChannel	= FALSE
BOOL 			bRandom
BOOL		  	bSetupAnimDict 		= FALSE
BOOL		  	bSetupPTFX 			= FALSE
BOOL			bTakenHit			= FALSE
BOOL			bTVStatincreased 
BOOL		  	bCigSmoke 			= FALSE
BOOL			bPCControlsSetup	= FALSE

CAMERA_INDEX  	CamIndex

PTFX_ID		  	ptfxCigarette
PTFX_ID		  	ptfxLighter

INT 			mSynchedScene
INT				iSmokeVariant 		= 0
INT 			iSmokeUsageStat
INT				iTVUsageStat
INT 			iSofaUsageStat

STRING 			sAnimDict 			= "safe@franklin@ig_13"

VECTOR 			vTriggerVec
VECTOR 			vSofaPos 			= <<-11.3259, -1441.4713, 30.5788>>  // Position character must face before being able to trigger activity.

// Particle effect offsets
VECTOR 			vMouthExhalePos		= << -0.025, 0.13, 0.0 >>
VECTOR 			vNoseExhalePos		= << -0.025, 0.12, 0.0 >>
VECTOR 			vNoseExhaleRot		= <<  0.000, 90.0, 0.0 >>

// Camera positions
VECTOR 			vTvOffCamPos		= <<-10.1, -1440.7, 31.1>>
VECTOR 			vTvOffCamRot		= <<-4.0, -0.0, 121.2>>
FLOAT			fTvOffCamFov		= 31.0

VECTOR 			vTvOnCamPos			= <<-12.1, -1440.7, 31.3>>
VECTOR 			vTvOnCamRot			= <<-5.3, 0.0, -117.8>>
FLOAT 			fTvOnCamFov			= 40.0

VECTOR			vExitCamPos			= <<-10.1, -1438.5, 31.5 >> 
VECTOR 			vExitCamRot			= << 0.3, 0.0, 159.0 >>
FLOAT			fExitCamFov			= 50.0

VECTOR 			vSmokeCamPos		= <<-9.4, -1439.6, 30.9>>
VECTOR			vSmokeCamRot		= <<2.3, -0.0, 142.8>>
FLOAT			fSmokeCamFov		= 25.9


// -----------------------------------------------------------------------------------------------------------
//		Debug widgets
// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID widgetGroup

	BOOL bDebugAttach = TRUE
	BOOL bReAttachRemote = FALSE
	//BOOL bPrintOutValues = FALSE
		
	PROC SETUP_REMOTE_CONTROL_WIDGET()
		
		ADD_WIDGET_BOOL("Show Attach Offsets",	bDebugAttach)
		ADD_WIDGET_BOOL("Re-Attach Remote", bReAttachRemote)
		
//		START_WIDGET_GROUP("ATTACH OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X OFFSET", vOffset, -50.0, 50.0, 0.01)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("ROTATION OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X ROT", vRot, -360.0, 360.0, 0.5)
//		STOP_WIDGET_GROUP()
					
	ENDPROC
	
	PROC INIT_RAG_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("Remote Control Widget")
			SETUP_REMOTE_CONTROL_WIDGET()
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	PROC UPDATE_RAG_WIDGETS()
		
		IF bReAttachRemote
			IF IS_ENTITY_ATTACHED(GET_CLOSEST_REMOTE())
				CPRINTLN(DEBUG_AMBIENT, "detatched")
				DETACH_ENTITY(GET_CLOSEST_REMOTE(), FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_CLOSEST_REMOTE())
				CPRINTLN(DEBUG_AMBIENT, "re-attached")
//				ATTACH_ENTITY_TO_ENTITY(GET_CLOSEST_REMOTE(), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), mLeftHand), <<vOffset.x, vOffset.y, vOffset.z>>, <<vRot.x, vRot.y, vRot.z>>)
				bReAttachRemote = FALSE
			ENDIF
			
		ENDIF
	ENDPROC
#ENDIF

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for Franklin's sofa script
PROC CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "Sofa Cleaning up...")
	SHOW_DEBUG_FOR_THIS_OBJECT()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
		CPRINTLN(DEBUG_AMBIENT, "Cleaning help text up in MissionCleanup")
		CLEAR_HELP(TRUE)
	ENDIF
		
	#IF IS_DEBUG_BUILD
		CLEANUP_OBJECT_WIDGETS()
	#ENDIF
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (cleanup)")
		STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
	ENDIF
	
	ENABLE_MOVIE_SUBTITLES(FALSE)
		
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
	ENDIF
	
	// Remove particle fx
	IF bSetupPTFX
		REMOVE_PTFX_ASSET()
	ENDIF
	
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	CLEANUP_STONED_EFFECT()
	REMOVE_PED_FOR_DIALOGUE(mWeedConv, 0)
	REMOVE_PED_FOR_DIALOGUE(mWeedConv, 1)

	// B*1895222 - Reactivate the chop script only if the player started the activity
	IF bSafehouseSetControlOff
		REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
	ENDIF
	
	// Restore control
	SAFE_RESTORE_PLAYER_CONTROL()
	
	IF bPCControlsSetup
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
	ENDIF
		
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_SOFA()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN FALSE
	ENDIF
	bSetupAnimDict = TRUE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests and checks the PTFX asset
/// RETURNS:
///    True if the asset has loaded.
FUNC BOOL HAS_SOFA_PTFX_ASSET_LOADED()

	REQUEST_PTFX_ASSET()
	IF HAS_PTFX_ASSET_LOADED()
		bSetupPTFX = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Obtains the heading and position of the remote control
FUNC BOOL HAS_REMOTE_BEEN_FOUND()

	OBJECT_INDEX mRemote = GET_CLOSEST_REMOTE()
	IF DOES_ENTITY_EXIST(mRemote)
//		vRemotePos = GET_ENTITY_COORDS(mRemote, FALSE)
//		fRemoteHead = GET_ENTITY_HEADING(mRemote)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests and loads the correct animation dictionary
///    and any required particle effects
FUNC BOOL HAVE_ASSETS_LOADED()

	IF HAS_ANIM_DICT_LOADED_FOR_SOFA()
	//AND HAS_SOFA_PTFX_ASSET_LOADED()
	AND HAS_REMOTE_BEEN_FOUND()
	AND HAS_AUDIO_LOADED()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Assets have loaded...")
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED_FOR_SOFA()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - CANNOT LOAD ANIMS")
	ENDIF
	
	IF NOT HAS_SOFA_PTFX_ASSET_LOADED()
		//CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - CANNOT LOAD PTFX")
	ENDIF
	
	IF NOT HAS_REMOTE_BEEN_FOUND()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - CANNOT LOAD REMOTE")
	ENDIF
	
	IF NOT HAS_AUDIO_LOADED()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - CANNOT LOAD AUDIO")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays the correct exit anim on the player, depending on
///    if he's holding the remote control or not.
PROC PLAY_QUIT_ANIM()
	
	mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
	TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								
	IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
		PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
	ENDIF
								
ENDPROC

/// PURPOSE:
///    Check for Franklin smoking a spliff
PROC CHECK_FOR_SMOKE()
	
	// Do we want to smoke
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		BAWSAQ_INCREMENT_MODIFIER(BSMF_BSM_SMOKED_FOR_DEB)		//#1514495 - stockmarket
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
			CLEAR_HELP(TRUE)
		ENDIF
		
		// Play the smoking anim.
		IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
			CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vTvOn")
			//SET_CAM_PARAMS(CamIndex, vTvOnCamPos, vTvOnCamRot, fTvOnCamFov)
			eSmokeState = SS_DROP_REMOTE
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vSmokeCam")
			//SET_CAM_PARAMS(CamIndex, vSmokeCamPos, vSmokeCamRot, fSmokeCamFov)
			bForceEnterSmoke = TRUE
			eSmokeState = SS_ENTER
		ENDIF
				
		IF STAT_GET_INT(NUM_SH_SOFA_SMOKED, iSmokeUsageStat)
			STAT_SET_INT(NUM_SH_SOFA_SMOKED, iSmokeUsageStat+1)
		ENDIF
						
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RUN_SMOKE")
		eTriggerState = TS_RUN_SMOKE
	ENDIF	
ENDPROC

/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()
	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - PLAYER WANTS TO BREAK OUT")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check for Franklin getting up off the sofa
PROC DO_PRE_CANCEL_STUFF()
	
	// Put camera in exit position.
	CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vExitCam")
	IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
		SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamRot, fExitCamFov)
	ENDIF
	
	// Clear the text
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
		CLEAR_HELP(TRUE)
	ENDIF
	
	IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	ENDIF
				
ENDPROC

/// PURPOSE:
///    Handles the lighter sparks, flames and smoke produced by the smoking events
PROC HANDLE_PARTICLES(PARTICLE_STATE ePartState)
		
	SWITCH (ePartState)
	
		CASE PT_ENTRY
			IF NOT bLighterSparks AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.25
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Lighter Sparks")
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sh_lighter_sparks", GET_SECONDARY_ENTITY(), <<0,0,0.05>>, <<0,0,0>>)
				bLighterSparks = TRUE
			ENDIF
			
			IF NOT bLighterFlame AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.255
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Lighter Flame")
				ptfxLighter = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_lighter_flame", GET_SECONDARY_ENTITY(), <<0,0,0.05>>, <<0,0,0>>)
				bLighterFlame = TRUE
			ENDIF
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLighter) AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.45
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Stop Lighter Flame")
				STOP_PARTICLE_FX_LOOPED(ptfxLighter)
			ENDIF
		
			IF NOT bCigSmoke AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.346
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Cig Smoke")
				ptfxCigarette = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_cig_smoke", GET_SYNCHED_SCENE_OBJECT(), << -0.09, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
				bCigSmoke = TRUE
			ENDIF
							
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.82
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Mouth Exhale")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
						
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.876
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Exhale Nose")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
		BREAK
		
		CASE PT_IDLE_A
															
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.42
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_A - Start Exhale Nose")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
			
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.7
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale mouth")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
		
		BREAK
					
		CASE PT_IDLE_B
			
			// Do a nose exhale when he coughs...
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.48
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale Nose")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
		BREAK
		
		CASE PT_EXIT
			
			// Stop the joint from smoking...
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette) AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.27
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_EXIT - End Cig Smoke")
				STOP_PARTICLE_FX_LOOPED(ptfxCigarette)
			ENDIF
			
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.45
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale mouth")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
			
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.45
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale Nose")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Resets the particle effect bools.
PROC RESET_PARTICLE_BOOLS()

	// Reset particle bools
	bLighterFlame = FALSE
	bLighterSparks = FALSE
	bCigSmoke = FALSE
	bExhaleMouth = FALSE
	bExhaleNose = FALSE
	
ENDPROC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_FRANKLIN_SOFA_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
	FLOAT  fScenePhase  = 0.0
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
						
			IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), vSofaPos, TRIGGER_ANGLE)
			AND DO_REQUIRED_OBJECTS_EXIST()
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - PLAYER CLOSE TO TRIGGER")
				PRINT_HELP_FOREVER("TV_HLP0")
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF
								
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
						
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
				
					IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					AND DO_REQUIRED_OBJECTS_EXIST()		
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] - SOFA TRIGGERED...")							
												
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
							CLEAR_HELP(TRUE)
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vSofaPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						ENABLE_MOVIE_SUBTITLES(TRUE)
								
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_SEAT_PLAYER")
						
						INIT_PC_SCRIPTED_CONTROLS( "SOFA ACTIVITY")
						bPCControlsSetup = TRUE
						
						eTriggerState = TS_SEAT_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
						CLEAR_HELP(TRUE)
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_SEAT_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAMERA()			
				
				IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
					SET_CAM_PARAMS(CamIndex, vTvOnCamPos, vTvOnCamRot, fTvOnCamFov)	
					IF NOT IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene started (wait for seating)")
						START_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
					ENDIF
				ELSE
					SET_CAM_PARAMS(CamIndex, vTvOffCamPos, vTvOffCamRot, fTvOffCamFov)
				ENDIF
			
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
		
			// Initialise camera
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				SET_CAM_ACTIVE(CamIndex, TRUE)
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - initialised CamIndex")
			ENDIF
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "enter", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 0.278)
			ELSE
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 0.0) // B*2045376
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_WAIT_FOR_SEATING")
			eTriggerState = TS_WAIT_FOR_SEATING
		BREAK
		
		CASE TS_WAIT_FOR_SEATING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
					DO_CAM_ADJUST(CamIndex, vTvOnCamRot)	
				ELSE
					DO_CAM_ADJUST(CamIndex, vTvOffCamRot)
				ENDIF
			ENDIF
			
			// Put the player into his idle sitting anim
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene) 
			// GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.6
			
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				
				// Loop the base idle if the TV is off...
				IF NOT g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				
					RESTORE_STANDARD_CHANNELS()
							
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
					SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				
				// Pick the remote up if the TV is on
				ELSE
				
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)

					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					
				ENDIF		
				
				g_eCurrentSafehouseActivity = SA_FRANKLIN_SOFA	// Set global for timetable events
				
				CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState = TS_SHOW_SOFA_INSTRUCTIONS			
				
			ENDIF	
		BREAK
		
		CASE TS_SHOW_SOFA_INSTRUCTIONS
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
										
			// Television is switched off...
			IF NOT g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				
				// Show help text about turning TV on or smoking weed.
				PRINT_HELP_FOREVER("TV_HLP3")
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_NO_REMOTE")
				eTriggerState = TS_IDLE_NO_REMOTE
			
			// TV is switched on...
			ELSE
								
				// Clear previous help
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
					CPRINTLN(DEBUG_AMBIENT, "Clearing the TV Off help..")
					CLEAR_HELP(TRUE)
				ENDIF
				
				// This is checking the "pick up remote" anim.
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.2
									
					// Show help text about turning TV off or smoking.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
						PRINT_HELP_FOREVER("TV_HLP4")
					ENDIF
					
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
					ENDIF
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PICK_UP_REMOTE")
					eTriggerState = TS_PICK_UP_REMOTE
				
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_IDLE_NO_REMOTE
				
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			CHECK_FOR_SMOKE()
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vTvOffCamRot)
			ENDIF
			
			// Show help text about turning TV on or smoking weed.
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
				CPRINTLN(DEBUG_AMBIENT, "Forcing help text...")
				PRINT_HELP_FOREVER("TV_HLP3")
			ENDIF
			
			// Has player turned the TV on?
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				
				IF NOT bTVStatincreased
					IF STAT_GET_INT(NUM_SH_TV_WATCHED, iTVUsageStat)
						STAT_SET_INT(NUM_SH_TV_WATCHED, iTVUsageStat+1)
						bTVStatincreased = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene started (wait for seating)")
					START_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
				ENDIF
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
												
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PICK_UP_REMOTE")			
				eTriggerState = TS_PICK_UP_REMOTE
			
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
				
				DO_PRE_CANCEL_STUFF()
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				
//				IF Is_Ped_Drunk(PLAYER_PED_ID())
//					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - quitting drunk")
//					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit_drunk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//				ELSE
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - quitting sober")
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - don't hold the last frame!")
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
						
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")			
				eTriggerState = TS_QUITTING
			ENDIF
		BREAK
		
		CASE TS_PICK_UP_REMOTE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
									
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.6	
			
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vTvOn")
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vTvOnCamPos, vTvOnCamRot, fTvOnCamFov)
					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_WITH_REMOTE")
				eTriggerState = TS_IDLE_WITH_REMOTE
			ELSE
								
				IF NOT g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						DO_CAM_ADJUST(CamIndex, vTvOffCamRot)
					ENDIF
					
					// Only turn the TV on when the anim is part-way through.
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
						
						// Turn the TV on
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - START_AMBIENT_TV_PLAYBACK")
						START_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_LIVING)
						//WAIT(0)
						//HIDE_HUD_AND_RADAR_THIS_FRAME()
						// Show help text about operating TV or smoking.
						PRINT_HELP_FOREVER("TV_HLP4")
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene started (pick up remote)")
							START_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
						ENDIF
					ENDIF						
				ENDIF
			ENDIF
			
		BREAK
		
		CASE TS_IDLE_WITH_REMOTE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			CHECK_FOR_SMOKE()
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vTvOnCamRot)
			ENDIF
			RUN_TV_TRANSPORT_CONTROLS(FALSE, TRUE)
			
			SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ENABLE_MOVIE_SUBTITLES(TRUE)
			ENDIF
			
			// Has player turned the TV off?
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
								
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
				
				bNeedTvOff = TRUE
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PUT_REMOTE_DOWN")
				eTriggerState = TS_PUT_REMOTE_DOWN
			
			// Does the player want to quit the sofa
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
				
				DO_PRE_CANCEL_STUFF()
				
				// Play the put down remote anim on the player and the remote
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_DROP_REMOTE_THEN_QUIT")
				eTriggerState = TS_DROP_REMOTE_THEN_QUIT
			
			// Does the player want the change the channel
			ELIF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL()
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_idle_a_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
				  
				bNeedChangeChannel = TRUE
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_CHANGE_CHANNEL")
				eTriggerState = TS_CHANGE_CHANNEL
				
			ENDIF			
		BREAK 
		
		CASE TS_CHANGE_CHANNEL
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vTvOnCamRot)
			ENDIF
			
			SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)				
				IF bNeedChangeChannel
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					IF fScenePhase > 0.3
						IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_1
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
						ELSE
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)	
						ENDIF								
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - bNeedChangeChannel = FALSE")
						bNeedChangeChannel = FALSE
					ENDIF
				ENDIF

			ELSE
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
							
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_WITH_REMOTE")
				eTriggerState = TS_IDLE_WITH_REMOTE
				
			ENDIF

		BREAK
		
		CASE TS_PUT_REMOTE_DOWN
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.9
				IF bNeedTvOff
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Turn the tv off")
						STOP_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_LIVING)
						IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (put remote down)")
							STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
						ENDIF
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
							CLEAR_HELP()
						ENDIF
						bNeedTvOff = FALSE
					ENDIF
				ENDIF
			ELSE
				// Play the idle anims
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vTvOffCamPos")
					SET_CAM_PARAMS(CamIndex, vTvOffCamPos, vTvOffCamRot, fTvOffCamFov)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState = TS_SHOW_SOFA_INSTRUCTIONS
			ENDIF
			
		BREAK
		
		// Put remote down, smoke a spliff, get stoned and pick remote back up again if appropriate.
		CASE TS_RUN_SMOKE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					DO_CAM_ADJUST(CamIndex, vTvOnCamRot)
				ENDIF
				SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			ELSE
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					DO_CAM_ADJUST(CamIndex, vSmokeCamRot)
				ENDIF
			ENDIF
				
			SWITCH (eSmokeState)
				
				CASE SS_DROP_REMOTE		
				
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
						CLEAR_HELP(TRUE)
					ENDIF
					
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_ENTER")
					eSmokeState = SS_ENTER
				BREAK
				
				CASE SS_ENTER
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
						CLEAR_HELP(TRUE)
					ENDIF
					
//					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.9
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
					OR bForceEnterSmoke			
										
						CPRINTLN(DEBUG_AMBIENT, "[SH] ANIM blunt_enter_joint")
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
													
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "blunt_enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "blunt_enter_joint", sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT) //NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "blunt_enter_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						
						// Only use the smoke cam if the tv is off.
						IF NOT g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								CPRINTLN(DEBUG_AMBIENT, "[SH] CAM changed to vSmokeCamPos")
								SET_CAM_PARAMS(CamIndex, vSmokeCamPos, vSmokeCamRot, fSmokeCamFov)
							ENDIF
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						bForceEnterSmoke = FALSE
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_IDLE")
						eSmokeState = SS_IDLE
											
					ENDIF
				BREAK
				
				CASE SS_IDLE
										
					// Handle particle effects when lighting up
					IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						// Stop any stoned dialogue from a previous smoke
						IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.55
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Kill dialogue from previous joint")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
						
						HANDLE_PARTICLES(PT_ENTRY)
					ELSE	
																						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						bRandom = GET_RANDOM_BOOL()
												
						IF bRandom 	
							CPRINTLN(DEBUG_AMBIENT, "[SH] ANIM blunt_idle_a_joint - was there a pop?")
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "blunt_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "blunt_idle_a_joint", sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT) // NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "blunt_idle_a_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "[SH] ANIM blunt_idle_b_joint")
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "blunt_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "blunt_idle_b_joint", sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT) // NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "blunt_idle_b_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
																	
						RESET_PARTICLE_BOOLS()
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_EXIT")
						eSmokeState  = SS_EXIT
					ENDIF
				BREAK
				
				CASE SS_EXIT
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						CHECK_FOR_BREAKOUT()
		
						IF bRandom
							HANDLE_PARTICLES(PT_IDLE_A)
						ELSE
							HANDLE_PARTICLES(PT_IDLE_B)
						ENDIF
						
						// Does the player want to quit this?
						IF bBreakoutEarly
							IF NOT bSetBreakoutAnim
								
								fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
								FLOAT fReturnStartPhase, fReturnEndPhase
																
								IF iSmokeVariant = 0
									IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "blunt_idle_a", GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
										CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - FOUND ANIM EVENT PHASE for idle a")
										IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
											bSetBreakoutAnim = TRUE
										ENDIF
									ENDIF
								ELSE
									IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "blunt_idle_b", GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
										CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - FOUND ANIM EVENT PHASE for idle b")
										IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
											bSetBreakoutAnim = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
							
								mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
								
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "blunt_interrupt", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "blunt_interrupt_joint", sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT) // NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "blunt_interrupt_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)																		
								
								IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
								ENDIF
								
								IF NOT bTakenHit
									IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
										Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
									ELSE
										Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL)
									ENDIF
									iTimesUsed++ // Need this to trigger stoned dialogue
									bTakenHit = TRUE
								ENDIF
								
								CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")
								bBreakoutEarly = FALSE
								eTriggerState  = TS_QUITTING
					
							ENDIF
							
						ENDIF
																		
					ELSE
						
						RESET_PARTICLE_BOOLS()
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] ANIM blunt_exit_joint - pop from idle b?")
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "blunt_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "blunt_exit_joint", sAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT) // NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "blunt_exit_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)					
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						IF NOT bTakenHit
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								Player_Takes_Weed_Hit(PLAYER_PED_ID(), CamIndex)
							ELSE
								Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL)
							ENDIF
							iTimesUsed++
							bTakenHit = TRUE
						ENDIF
						
						IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_PICK_UP_REMOTE")							
							eSmokeState = SS_PICK_UP_REMOTE
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_START_SEATING_IDLE")
							eSmokeState = SS_START_SEATING_IDLE
						ENDIF
					ENDIF
				BREAK
				
				CASE SS_PICK_UP_REMOTE
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
					//IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.9
					
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_START_SEATING_IDLE")
						eSmokeState = SS_START_SEATING_IDLE	
					ELSE
						HANDLE_PARTICLES(PT_EXIT)
					ENDIF
				BREAK
				
				CASE SS_START_SEATING_IDLE
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
					//IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.9
						
						RESET_PARTICLE_BOOLS()
						
						bTakenHit = FALSE
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						
						IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
							SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
							
							// Show help text about turning TV on or smoking.
							PRINT_HELP_FOREVER("TV_HLP4")
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_IDLE_WITH_REMOTE")
							eTriggerState = TS_IDLE_WITH_REMOTE
						
						ELSE
												
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: CAM pos changed to vTvOffCamPos")
								SET_CAM_PARAMS(CamIndex, vTvOffCamPos, vTvOffCamRot, fTvOffCamFov)
							ENDIF
																	
							SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
							
							// Show help text about operating TV or smoking weed.
							PRINT_HELP_FOREVER("TV_HLP3")
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_NO_REMOTE")
							eTriggerState = TS_IDLE_NO_REMOTE
							
						ENDIF
					ELSE
						HANDLE_PARTICLES(PT_EXIT)
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK 
		
		CASE TS_DROP_REMOTE_THEN_QUIT
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Have we dropped the remote yet?
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				DO_PRE_CANCEL_STUFF()
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
//				IF Is_Ped_Drunk(PLAYER_PED_ID())
//					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Franklin is quitting drunk")	
//					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit_drunk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//				ELSE
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Franklin is quitting sober")	
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")			
				eTriggerState = TS_QUITTING
			ENDIF
			
		BREAK
			
		CASE TS_QUITTING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_AUDIO_SCENE_ACTIVE("TV_FRANKLINS_HOUSE_SOCEN")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (put remote down)")
				STOP_AUDIO_SCENE("TV_FRANKLINS_HOUSE_SOCEN")
			ENDIF
									
			//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.95
							
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET")
				eTriggerState = TS_RESET
			ELSE
			
				HANDLE_PARTICLES(PT_EXIT)			
				CHECK_FOR_BREAKOUT()
				
				IF NOT bTriggerExitCam
				
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Sofa: TRIGGERING EXIT CAM")
									
					//SHAKE_GAMEPLAY_CAM("HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
					
					//RENDER_SCRIPT_CAMS(FALSE, FALSE)
					// Normal gameplay camera
					IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					ENDIF
											
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING())
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_PITCH())
											
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					bTriggerExitCam = TRUE
											
				ENDIF
				
				IF bBreakoutEarly	
					
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase	
					
					IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "exit", "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
					OR FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "exit_drunk", "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
					
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
																					
							// Clear tasks and restore control
							CLEAR_PED_TASKS(PLAYER_PED_ID())
														
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET - EXIT BREAKOUT!")
							eTriggerState = TS_RESET
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RESET	
			
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
			
			// Normal gameplay camera
			IF NOT bTriggerExitCam
			AND IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
			
			IF STAT_GET_INT(NUM_SH_SOFA_USED, iSofaUsageStat)
				STAT_SET_INT(NUM_SH_SOFA_USED, iSofaUsageStat+1)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
			
			IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bIsTVOn
				RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOC_FRANKLIN_LIVING)
			ENDIF
					
			IF bPCControlsSetup
				SHUTDOWN_PC_SCRIPTED_CONTROLS()
				bPCControlsSetup = FALSE
			ENDIF
			
			// Reset global for timetable events
			g_eCurrentSafehouseActivity = SA_NONE
			bBreakoutEarly = FALSE
			bSetBreakoutAnim = FALSE
			bTakenHit = FALSE
			bTVStatIncreased = FALSE
			bTriggerExitCam = FALSE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)							
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(coords_struct in_coords)
	
	CPRINTLN(DEBUG_AMBIENT, "FRA Sofa triggered..")
	
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU) 
		CPRINTLN(DEBUG_AMBIENT, "FRA Sofa DEFAULT CLEANUP")
		CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		CPRINTLN(DEBUG_AMBIENT, "FRA Sofa CURRENTLY ON MISSION")
		CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	ENDIF
	
	IF IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC()
		CPRINTLN(DEBUG_AMBIENT, "FRA Sofa IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE")
		CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	ENDIF
	
	IF IS_MOBILE_PHONE_CALL_ONGOING()
		CPRINTLN(DEBUG_AMBIENT, "FRA Sofa IS_MOBILE_PHONE_CALL_ONGOING")
		CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		CPRINTLN(DEBUG_AMBIENT, "FRA Sofa Player has a wanted level")
		CLEANUP_FRANKLIN_SOFA_ACTIVITY()
	ENDIF
	
	vTriggerVec   = in_coords.vec_coord[0]
	vTriggerVec   = <<-11.3888, -1441.3252, 30.1>>
	mTriggerModel = P_CS_JOINT_01
		
	#IF IS_DEBUG_BUILD
		INIT_RAG_WIDGETS()
	#ENDIF
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> NULL
			//IF g_TVStruct[TV_LOC_FRANKLIN_LIVING].bAvailableForUse
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerVec, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < SOFA_TRIGGER_DIST
			
				IF bSafehouseSetControlOff
					SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				ENDIF
				
				SWITCH eState
										
					CASE AS_LOAD_ASSETS		
						IF HAVE_ASSETS_LOADED()
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							UPDATE_FRANKLIN_SOFA_ACTIVITY()
							//HANDLE_STONED_CAMERA()
							APPLY_USAGE_EFFECTS()
							//UPDATE_RAG_WIDGETS()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
			
				ENDSWITCH
			ENDIF
		ELSE
			CLEANUP_FRANKLIN_SOFA_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
