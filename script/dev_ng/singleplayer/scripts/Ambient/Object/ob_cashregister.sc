// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"

USING "net_synced_ambient_pickups.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun
//PICKUP_INDEX pickupCash
//BLIP_INDEX blipPickup
VECTOR vRegister
BOOL bRunningNetworkVersion

// Clean Up
PROC missionCleanup()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC ADD_MONEY_BAG()
	MODEL_NAMES modelBag = Prop_Money_Bag_01
	PICKUP_TYPE pickupType = PICKUP_MONEY_MED_BAG
	INT iPlacementFlags
	INT stolenCash = GET_RANDOM_INT_IN_RANGE(70, 121)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		modelBag = PROP_CASH_PILE_01
		stolenCash = GET_RANDOM_INT_IN_RANGE(50, 101)
		stolenCash = MULTIPLY_CASH_BY_TUNABLE(stolenCash)
	ENDIF
	
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	REQUEST_MODEL(modelBag)
	WHILE NOT HAS_MODEL_LOADED(modelBag)
	WAIT(0)
	ENDWHILE
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		#IF FEATURE_COPS_N_CROOKS
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS
			BROADCAST_EVENT_COPSCROOKS_GIVE_CROOK_HELD_CASH(GET_RANDOM_INT_IN_RANGE(500, 1001))
			PRINTLN("[COPS_CROOKS] [CASH] BROADCAST_EVENT_COPSCROOKS_GIVE_CROOK_HELD_CASH - Shot open till.")
		ELSE
		#ENDIF
		
			CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(pickupType, GET_SAFE_PICKUP_COORDS(vRegister), iPlacementFlags, stolenCash, modelBag,FALSE,FALSE)
			
		#IF FEATURE_COPS_N_CROOKS
		ENDIF
		#ENDIF
		
	ELSE
		CREATE_PICKUP(pickupType, GET_SAFE_PICKUP_COORDS(vRegister), iPlacementFlags, stolenCash, FALSE, modelBag)
	ENDIF
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - ADD_MONEY_BAG() added with value of $", stolenCash)
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oCashRegister)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//For 1684955, kill cash register scripts before they can be net registered if we are in the net script intensive area
		//located at the downtown gunshop with shooting range
		IF IS_BIT_SET(g_MPAMData.iBitset, MPAM_BS_IN_NET_SCRIPT_INTENSIVE_AREA)
			CPRINTLN(DEBUG_MP_AMBIENT_MANAGER,  "=== 1684955 === Killing Cash Register Script instance due to MPAM_BS_IN_NET_SCRIPT_INTENSIVE_AREA")
			missionCleanup()
		ELSE
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, -1)
			HANDLE_NET_SCRIPT_INITIALISATION()
			SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
			PRINTLN(GET_THIS_SCRIPT_NAME(), "  - Network Version")
			bRunningNetworkVersion = TRUE
		ENDIF
	ELSE
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
			missionCleanup()
		ENDIF
	ENDIF



	// Mission Loop -------------------------------------------//
	WHILE TRUE
		WAIT(0)
		
		IF bRunningNetworkVersion = TRUE
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE ", vRegister)
				missionCleanup()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(oCashRegister)
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oCashRegister)
				SWITCH ambStage
					CASE ambCanRun
						IF DOES_ENTITY_HAVE_DRAWABLE(oCashRegister)
							vRegister = GET_ENTITY_COORDS(oCashRegister)
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - has launched at ", vRegister)
							ambStage = ambRunning
						ENDIF
					BREAK
					CASE ambRunning
						IF HAS_OBJECT_BEEN_BROKEN(oCashRegister)
						AND IS_ENTITY_VISIBLE(oCashRegister)
						AND NOT IS_ENTITY_A_MISSION_ENTITY(oCashRegister)
							ADD_MONEY_BAG()
							ambStage = ambEnd
						ELSE
						
						ENDIF
					BREAK
					CASE ambEnd
					
					BREAK
				ENDSWITCH
			ELSE
				missionCleanup()
			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT

