// Includes
USING "ob_safehouse_common.sch"
USING "rgeneral_include.sch"
USING "net_realty_mess_include.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_drinking.sc
//		DESCRIPTION		:	Franklin's wine
//
// *****************************************************************************************
// *****************************************************************************************

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE, 
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_RUN_EXIT_ANIM,
	TS_BREAKOUT_EARLY,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT	WINE_AREA_WIDTH	1.5

// Variables
BOOL	bBreakoutEarly 		= FALSE
BOOL	bDoneGlassAnim		= FALSE
BOOL	bAnimLoaded 		= FALSE
BOOL 	bAnimRunning		= FALSE
BOOL 	bTakenHit			= FALSE

CAMERA_INDEX  	CamIndex
CAMERA_INDEX	ExitCam

FLOAT fTriggerHead

g_eDrunkLevel eDrunkLevel

INT iCurProperty
INT	mSynchedScene
INT iUsageStat

OBJECT_INDEX mWineBottle

STRING sAnimDict 		
STRING sPedEnterAnim	= "drinking_wine"
STRING sGlassEnterAnim	= "drinking_wine_glass"
STRING sGlassExitAnim	= "drinking_wine_exit_glass"
STRING sBottleEnterAnim	= "drinking_wine_bottle"
STRING sBottleExitAnim	= "drinking_wine_exit_bottle"

STRING sHelpText 		= "SA_WINE"

VECTOR vTriggerPos
VECTOR vScenePos
VECTOR vSceneHead
//VECTOR vCamPos
//VECTOR vCamHead
//VECTOR vExitCamPos
//VECTOR vExitCamHead
VECTOR vTriggerAreaA
VECTOR vTriggerAreaB

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_DRINKING_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wine: CLEANUP_DRINKING_ACTIVITY")
		
	// Clear help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
		CLEAR_HELP(TRUE)
	ENDIF
				
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	IF bAnimLoaded
	
		IF bAnimRunning
			IF DOES_ENTITY_HAVE_DRAWABLE(mTrigger) AND DOES_ENTITY_HAVE_DRAWABLE(mWineBottle)
				PLAY_ENTITY_ANIM(mTrigger, sGlassExitAnim, sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE)
				PLAY_ENTITY_ANIM(mWineBottle, sBottleExitAnim, sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE)
			ENDIF
		ENDIF
		
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
	
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		DESTROY_CAM(CamIndex)
	ENDIF
	
	IF DOES_CAM_EXIST(ExitCam)
		DESTROY_CAM(ExitCam)
	ENDIF
	
	IF g_bInMultiplayer
	
		IF bSafehouseSetControlOff
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
		ENDIF
		
		// End script
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	ELSE
		
		// B*1895222 - Reactivate the chop script only if the player started the activity
		IF bSafehouseSetControlOff
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
		ENDIF
	
		// Restore control and remove motion blur
		SAFE_RESTORE_PLAYER_CONTROL()
		
		// End script
		TERMINATE_THIS_THREAD()
	
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and checks that the anim dict for this activity has loaded
/// RETURNS:
///    True if the anim dict has loaded.
FUNC BOOL HAS_DRINKING_ANIM_DICT_LOADED()

	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	
	IF HAS_ANIM_DICT_LOADED(sAnimDict)
		CPRINTLN(DEBUG_AMBIENT, "WINE ANIM DICT LOADED")
		bAnimLoaded = TRUE
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()
	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: PLAYER WANTS TO BREAK OUT")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerAreaA, vTriggerAreaB, WINE_AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Updates drinking activity
PROC UPDATE_WINE_ACTIVITY()
	
	vTriggerPos = (GET_TRIGGER_VEC_FOR_THIS_OBJECT())
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0

	SWITCH eTriggerState
			
		CASE TS_PLAYER_OUT_OF_RANGE
			
			// Forces the tumbler into an empty state at the start of the activity.
			IF NOT bDoneGlassAnim
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				
				IF DOES_ENTITY_HAVE_DRAWABLE(GET_SYNCHED_SCENE_OBJECT())
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, TRUE)
					bDoneGlassAnim  = TRUE
				ENDIF
			ENDIF
			
			eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
			
			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
			AND DO_REQUIRED_OBJECTS_EXIST()
			AND eDrunkLevel <> DL_slightlydrunk
			AND eDrunkLevel <> DL_verydrunk
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF
					
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_WAIT_FOR_PLAYER")	
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				AND DO_REQUIRED_OBJECTS_EXIST()
				
					IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: ACTIVITY TRIGGERED...")
						SHOW_DEBUG_FOR_THIS_OBJECT()
						
						DISABLE_SELECTOR_THIS_FRAME()	
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_GRAB_PLAYER")
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
	
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			
			PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
			
			// Wine bottle
			PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(vTriggerPos, 2.0, PROP_WINE_BOT_01, mSynchedScene, GET_ANIM_FOR_SECOND_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, GET_BEST_WINE_CAM(), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
				SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: using 1st person so not creating CamIndex")
			ENDIF
						
			// Play scene
			eTriggerState = TS_RUN_ANIM
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_GRAB_PLAYER -> TS_RUN_ANIM")
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
				
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				IF NOT bTakenHit
					IF fScenePhase > 0.5
						Player_Takes_Alcohol_Hit(PLAYER_PED_ID())
						bTakenHit = TRUE
					ENDIF
				ENDIF
			
			ELSE // The drinking anim has finished
				
				bBreakoutEarly = FALSE				
				
				// If we have seperate exit anims, play them here.
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mTrigger) //Stop the glitch on the wine glass.
				
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF				
				
				IF NOT bTakenHit
					CPRINTLN(DEBUG_AMBIENT, "Player has had a drink")
					Player_Takes_Alcohol_Hit(PLAYER_PED_ID())
					bTakenHit = TRUE
				ENDIF
											
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: Enter anim finished - run exit anim")
				eTriggerState  = TS_RUN_EXIT_ANIM
												
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, TRUE)
			
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_RUN_EXIT_ANIM -> TS_RESET")
				eTriggerState = TS_RESET
			ELSE
				CHECK_FOR_BREAKOUT()
			
				IF bBreakoutEarly
										
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
			
					IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
				
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
							
							mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, TRUE)
			
							// Clear tasks
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_RUN_EXIT_ANIM -> TS_RESET (EXIT BREAKOUT!)")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
						
		CASE TS_RESET	
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: Resetting")
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF STAT_GET_INT(NUM_SH_WINE_DRANK, iUsageStat)
				STAT_SET_INT(NUM_SH_WINE_DRANK, iUsageStat+1)
			ENDIF
			
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
			
			// Normal gameplay cam
			IF DOES_CAM_EXIST(CamIndex)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: Destroy wine cam")
				DESTROY_CAM(CamIndex)				
			ENDIF
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: not using 1st person so creating ExitCam")
				ExitCam = CREATE_CAMERA()
				IF DOES_CAM_EXIST(ExitCam)
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: Create exit cam")
					SET_CAM_PARAMS(ExitCam, GET_EXIT_CAM_POS(), GET_EXIT_CAM_ROT(), 50.0)
					SET_CAM_ACTIVE(ExitCam, TRUE)
					WAIT(0)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: using 1st person so not creating ExitCam")
			ENDIF
										
			// Restore control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
	
			bBreakoutEarly = FALSE
			bTakenHit = FALSE
						
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
						
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: TS_RESET -> TS_PLAYER_OUT_OF_RANGE")

		BREAK
		
	ENDSWITCH
ENDPROC

PROC MP_UPDATE_WINE_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0
	
	//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, WINE_AREA_WIDTH)
	
	SWITCH eTriggerState
			
		CASE TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = ci_APT_ACT_WINE
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			// Forces the wine glass into an empty state at the start of the activity.
			IF NOT bDoneGlassAnim AND bAnimLoaded AND DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
				PLAY_ENTITY_ANIM(mTrigger, sGlassExitAnim, sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE)	
				bDoneGlassAnim  = TRUE
			ENDIF

			eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
						
			IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_WINE)
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, WINE_AREA_WIDTH)
				AND eDrunkLevel <> DL_moderatedrunk
				AND eDrunkLevel <> DL_slightlydrunk
				AND eDrunkLevel <> DL_verydrunk
					
	//				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	//					START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	//				ENDIF
						
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: TS_WAIT_FOR_PLAYER")	
					PRINT_HELP_FOREVER(sHelpText)
					eTriggerState = TS_WAIT_FOR_PLAYER	
				ENDIF
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, WINE_AREA_WIDTH)
				AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_WINE)
										
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
												
						DISABLE_SELECTOR_THIS_FRAME()			
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						iPersonalAptActivity = ci_APT_ACT_WINE	
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
																		
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
											
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: TS_GRAB_PLAYER")
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
			
				REMOVE_PLAYER_MASK()
				
				bAnimRunning = TRUE
				
				SET_ENTITY_COLLISION(mTrigger, FALSE)
				SET_ENTITY_COLLISION(mWineBottle, FALSE)
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, sPedEnterAnim, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, REALLY_SLOW_BLEND_IN, AIK_DISABLE_HEAD_IK)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sGlassEnterAnim, sAnimDict, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mWineBottle, mSynchedScene, sBottleEnterAnim, sAnimDict, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT)
														
				// Play scene
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: TS_RUN_ANIM")
				eTriggerState = TS_RUN_ANIM
				
			ENDIF
			
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
				
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
				
				IF NOT bTakenHit
					IF fScenePhase > 0.8
						Player_Takes_Alcohol_Hit(PLAYER_PED_ID())
						bTakenHit = TRUE
					ENDIF
				ENDIF
			
			ELSE // The drinking anim has finished
				
				bBreakoutEarly = FALSE				
				
				// Exit anim depends on peds drunk level...
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, sGlassExitAnim, sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)	
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mWineBottle, mSynchedScene, sBottleExitAnim, sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)		
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mTrigger) //Stop the glitch on the wine glass.
				
				IF NOT bTakenHit
					CPRINTLN(DEBUG_AMBIENT, "Player has had a drink")
					Player_Takes_Alcohol_Hit(PLAYER_PED_ID())
					bTakenHit = TRUE
				ENDIF
											
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse]MP Wine: Enter anim finished - run exit anim")
				eTriggerState  = TS_RUN_EXIT_ANIM
												
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
				
				bAnimRunning = FALSE
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 1.0)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mTrigger) //Stop the glitch on the wine glass.
				
				// Change the camera angle - non-shake
//				IF DOES_CAM_EXIST(CamIndex)
//					SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, 50.0)
//				ENDIF
							
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP wine: TS_RUN_EXIT_ANIM -> TS_RESET")
				eTriggerState = TS_RESET
				
			ENDIF
		BREAK
						
		CASE TS_RESET	
								
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
//			IF STAT_GET_INT(NUM_SH_WINE_DRANK, iUsageStat)
//				STAT_SET_INT(NUM_SH_WINE_DRANK, iUsageStat+1)
//			ENDIF
			
			RESTORE_PLAYER_MASK()
			
			// Clear tasks
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			INCREMENT_MP_APT_DRINK_COUNTER()
							
			// Normal gameplay cam
//			IF DOES_CAM_EXIST(CamIndex)
//				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wine: Destroy wine cam")
//				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 1.0)		
//			ENDIF
													
			// Restore control
			IF bSafehouseSetControlOff
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bSafehouseSetControlOff = FALSE
			ENDIF
			
			//Set the collision back. 
			SET_ENTITY_COLLISION(mTrigger, TRUE)
			SET_ENTITY_COLLISION(mWineBottle, TRUE)
				
			bBreakoutEarly = FALSE
			bTakenHit = FALSE
			
			iPersonalAptActivity = ci_APT_ACT_IDLE
						
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
						
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: TS_PLAYER_OUT_OF_RANGE")
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		OR Is_Player_On_Or_Triggering_Any_Mission()
		OR MPGlobalsAmbience.bRunningFmIntroCut
			CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	
	ELSE
			
		// Default cleanup
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP) 
		OR IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
		OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())
		OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)	
		OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
			CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
		
	ENDIF
		
	IF DOES_ENTITY_EXIST(mObjectIn)
			
		// Wine glass is the trigger
		mTrigger = mObjectIn
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)
	
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
			
			// This makes sure the net script is active, waits until it is.
			HANDLE_NET_SCRIPT_INITIALISATION()
			SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
						
//			NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
			
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: Current property is ", iCurProperty)
			
			GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
			
			vTriggerPos = tempPropertyStruct.house.activity[SAFEACT_WINE].vTriggerVec 
			fTriggerHead = tempPropertyStruct.house.activity[SAFEACT_WINE].fTriggerRot
			vScenePos = tempPropertyStruct.house.activity[SAFEACT_WINE].vSceneVec
			vSceneHead = tempPropertyStruct.house.activity[SAFEACT_WINE].vSceneRot
//			vCamPos = tempPropertyStruct.house.activity[SAFEACT_WINE].vCamVec
//			vCamHead = tempPropertyStruct.house.activity[SAFEACT_WINE].vCamRot
//			vExitCamPos = tempPropertyStruct.house.activity[SAFEACT_WINE].vExitCamVec
//			vExitCamHead = tempPropertyStruct.house.activity[SAFEACT_WINE].vExitCamRot
			vTriggerAreaA = tempPropertyStruct.house.activity[SAFEACT_WINE].vAreaAVec
			vTriggerAreaB = tempPropertyStruct.house.activity[SAFEACT_WINE].vAreaBVec
			
			IF fTriggerHead < 0.0
				fTriggerHead+= 360.0
			ENDIF
			
			IF CAN_REGISTER_MISSION_OBJECTS(2)
				// Get a handle to the wine bottle
				mWineBottle = GET_CLOSEST_OBJECT_OF_TYPE(vTriggerPos, 2.0, PROP_WINE_BOT_01, FALSE)
//				NETWORK_REGISTER_ENTITY_AS_NETWORKED(mWineBottle)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: Registering Wine Bottle as Network object.")
			ENDIF
			
			IF DOES_ENTITY_EXIST(mTrigger) 
				IF NOT IS_ENTITY_ATTACHED(mTrigger)
					FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mWineBottle)
				IF NOT IS_ENTITY_ATTACHED(mWineBottle)
					FREEZE_ENTITY_POSITION(mWineBottle, TRUE)
				ENDIF
			ENDIF
			
			sAnimDict = "MP_SAFEHOUSEWINE@"
			
		ELSE
		
			IF DOES_ENTITY_EXIST(mObjectIn)
				mTrigger = mObjectIn		
				mTriggerModel = GET_ENTITY_MODEL(mTrigger)
				FREEZE_ENTITY_POSITION(mTrigger, TRUE)		
			ENDIF
			
			sAnimDict = "safe@franklin@ig_11"
		ENDIF
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	CPRINTLN(DEBUG_AMBIENT, "...WINE IS TRIGGERING ...")
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
		
		WAIT(0)
				
		IF DOES_ENTITY_EXIST(mTrigger)
			
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
			AND NOT IS_ENTITY_DEAD(mTrigger)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND CAN_PLAYER_USE_THIS_OBJECT()
			AND IS_INTERIOR_CORRECT(mTrigger)
			//AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() AND NOT g_bInMultiplayer) // Don't check transtion or switches if we're in MP.
			
				SWITCH eState
										
					CASE AS_LOAD_ASSETS
						IF HAS_DRINKING_ANIM_DICT_LOADED()
						AND HAS_AUDIO_LOADED()
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
		
					CASE AS_RUN_ACTIVITY
						//IF g_OldSafehouseActivites = TRUE
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF g_bInMultiplayer
									//CPRINTLN(DEBUG_AMBIENT, "[Safehouse] MP Wine: MP UPDATE IS BEING CALLED")
									IF NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() 
										MP_UPDATE_WINE_ACTIVITY()
										APPLY_USAGE_EFFECTS()
									ELSE
										CLEANUP_DRINKING_ACTIVITY()
									ENDIF
								ELSE
									//CPRINTLN(DEBUG_AMBIENT, "[Safehouse] SP Wine: UPDATE IS BEING CALLED")
									UPDATE_WINE_ACTIVITY()
									APPLY_USAGE_EFFECTS()
								ENDIF
							ENDIF
						//ENDIF
					BREAK
					
					CASE AS_END
					BREAK
				ENDSWITCH
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "Wine cleanup 1")
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "Wine cleanup 2")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	
	ENDWHILE
ENDSCRIPT
