// Philip O'Duffy
USING "net_luxe_veh_activities.sch"

SERVER_BROADCAST_DATA serverBD
PLAYER_BROADCAST_DATA playerBD[NUM_NETWORK_PLAYERS]
LUXE_ACT_SERVER_STRUCT luxeActServerStruct
// Mission Script -----------------------------------------//
SCRIPT(LUXE_ACT_STRUCT luxeActState)
	serverBD.luxeServerActScriptState = SR_LUXE_INIT
	
	#IF IS_DEBUG_BUILD
	serverBD.iSeatWidget = -1
	#ENDIF
	
	#IF IS_DEBUG_BUILD 
	CREATE_LUXE_ACT_WIDGETS(serverBD, luxeActState)
	#ENDIF
	
	PROCESS_PRE_GAME_LUXE(serverBD, playerBD, luxeActState)
	
	WHILE TRUE
		WAIT(0)
		HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY(serverBD, playerBD, luxeActState, luxeActServerStruct)
	ENDWHILE
ENDSCRIPT
