// Includes
USING "familySchedule_private.sch"
USING "ob_safehouse_common.sch"
USING "tv_control_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_sofa_michael.sc
//		DESCRIPTION		:	Script that handles Michael's sofa interaction
//
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_SEAT_PLAYER,
	TS_WAIT_FOR_SEATING,
	TS_SHOW_SOFA_INSTRUCTIONS,
	TS_IDLE_NO_REMOTE,
	TS_PICK_UP_REMOTE,
	TS_IDLE_WITH_REMOTE,
	TS_CHANGE_CHANNEL,
	TS_PUT_REMOTE_DOWN,
	TS_RUN_SMOKE,
	TS_QUITTING,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

ENUM SMOKE_STATE
	SS_DROP_REMOTE = 0,
	SS_ENTER,
	SS_IDLE, 
	SS_EXIT, 
	SS_PICK_UP_REMOTE,
	SS_START_SEATING_IDLE,
	SS_FLAG_FINISHED
ENDENUM
SMOKE_STATE 	eSmokeState = SS_ENTER

ENUM PARTICLE_STATE
	PT_ENTRY = 0,
	PT_IDLE_A,
	PT_IDLE_B, 
	PT_EXIT
ENDENUM

ENUM CS_CAM_STATE

	CS_NULL = 0,
	CS_CREATE,
	CS_TV_ON,
	CS_TV_OFF,
	CS_ADJUST_CAMERA,
	CS_EXIT
	
ENDENUM
CS_CAM_STATE eCamState = CS_NULL

// Variables
BOOL 			bForceEnterSmoke	= FALSE
BOOL 			bBreakoutEarly		= FALSE
BOOL			bSetBreakoutAnim	= FALSE
BOOL 			bNeedTvOff 			= FALSE
BOOL			bNeedChangeChannel	= FALSE
BOOL 			bRandom 			= FALSE
BOOL			bStoodInPlace		= FALSE
BOOL 			bIncreasedTVStat	= FALSE
BOOL		  	bCigSmoke 			= FALSE
BOOL		  	bLighterFlame 		= FALSE	
BOOL		  	bLighterSparks 		= FALSE
BOOL		  	bExhaleMouth 		= FALSE
BOOL		  	bExhaleNose 		= FALSE
BOOL		  	bExhaleMouth2 		= FALSE
BOOL		  	bExhaleNose2 		= FALSE
BOOL		  	bSetupAnimDict 		= FALSE
BOOL		  	bSetupPTFX 			= FALSE
BOOL			bPCControlsSetup	= FALSE

CAMERA_INDEX  	TvOnCam
CAMERA_INDEX	TvOffCam

INT 			mSynchedScene
INT 			iSofaUsageStat
INT				iTVUsageStat
INT 			iSmokeUsageStat
INT 			iBlockArea			= -1

PTFX_ID		  	ptfxCigarette
PTFX_ID		  	ptfxLighter

STRING 			sAnimDict 			= "safe@michael@ig_3"

VECTOR 			vTvOffCamPos		= <<-804.374, 176.864, 73.015>>  // [AG] MOVED FROM CAMERA <<-804.6, 175.1, 72.9>> TO FIX B*1941817
VECTOR 			vTvOffCamRot		= <<-3.7, 0.0, 172.7 >>
FLOAT			fTvOffCamFov		= 34.0

VECTOR 			vTvOnCamPos			= <<-806.6, 173.4, 72.9>>
VECTOR 			vTvOnCamRot			= <<6.0, 0.0, -86.8>>
FLOAT 			fTvOnCamFov			= 40.0

VECTOR 			vExitCamPos			= <<-802.4001, 175.8452, 73.3742>>
VECTOR			vExitCamRot			= <<-2.0225, 0.0000, 137.6933>>
FLOAT			fExitCamFov			= 50.0

// Particle effect positions
VECTOR 			vCigSmokePos 		= <<0.11, 0.0, 0.0 >>

VECTOR 			vMouthExhalePos		= << -0.025, 0.13, 0.0 >>
VECTOR 			vNoseExhalePos		= << -0.015, 0.13 , 0.0 >>
VECTOR 			vNoseExhaleRot		= <<  0.000, 90.0, 0.0 >>

VECTOR 			vTriggerVec
VECTOR 			vSofaPos			= <<-805.6204, 172.3675, 71.8347>> // Position character must face before being able to trigger activity.

VECTOR 			vCamRot				= <<0.0, 0.0, 0.0>>
// -----------------------------------------------------------------------------------------------------------
//		Debug widgets
// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID widgetGroup

	BOOL bDebugAttach = TRUE
	BOOL bReAttachRemote = FALSE
	//BOOL bPrintOutValues = FALSE
		
	PROC SETUP_REMOTE_CONTROL_WIDGET()
		
		ADD_WIDGET_BOOL("Show Attach Offsets",	bDebugAttach)
		ADD_WIDGET_BOOL("Re-Attach Remote", bReAttachRemote)
		
//		START_WIDGET_GROUP("ATTACH OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X OFFSET", vOffset, -50.0, 50.0, 0.01)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("ROTATION OFFSET")
//			ADD_WIDGET_VECTOR_SLIDER("X ROT", vRot, -360.0, 360.0, 0.5)
//		STOP_WIDGET_GROUP()
					
	ENDPROC
	
	PROC INIT_RAG_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("Remote Control Widget")
			SETUP_REMOTE_CONTROL_WIDGET()
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	PROC UPDATE_RAG_WIDGETS()
		
		IF bReAttachRemote
			IF IS_ENTITY_ATTACHED(GET_CLOSEST_REMOTE())
				CPRINTLN(DEBUG_AMBIENT, "detatched")
				DETACH_ENTITY(GET_CLOSEST_REMOTE(), FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_CLOSEST_REMOTE())
				CPRINTLN(DEBUG_AMBIENT, "re-attached")
//				ATTACH_ENTITY_TO_ENTITY(GET_CLOSEST_REMOTE(), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<vOffset.x, vOffset.y, vOffset.z>>, <<vRot.x, vRot.y, vRot.z>>)
				bReAttachRemote = FALSE
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for Michael's sofa script
PROC CLEANUP_MICHAEL_SOFA_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Cleaning up...")
	
	PLAYER_DETACH_VIRTUAL_BOUND()
	SET_PLAYER_CLOTH_PACKAGE_INDEX(0)
		
	// Stop the anim playing on the remote control.
	IF IS_ENTITY_PLAYING_ANIM(GET_CLOSEST_REMOTE(), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "tv_base_remote", ANIM_SYNCED_SCENE)
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Cleaning up tv_base_remote anim...")
		STOP_ENTITY_ANIM(GET_CLOSEST_REMOTE(), "tv_base_remote", GET_ANIM_DICT_FOR_THIS_ACTIVITY(), INSTANT_BLEND_OUT)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
		CLEAR_HELP(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CLEANUP_OBJECT_WIDGETS()
	#ENDIF
	
	ENABLE_MOVIE_SUBTITLES(FALSE)
	
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	IF IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (cleanup)")
		STOP_AUDIO_SCENE("TV_MICHAELS_HOUSE")
	ENDIF
	
	// Remove the block area
	IF iBlockArea != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iBlockArea)
	ENDIF
	
	// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
	ENDIF
	
	// Remove particle fx
	IF bSetupPTFX
		REMOVE_PTFX_ASSET()
	ENDIF
	
	// Remove cameras
	IF DOES_CAM_EXIST(TvOnCam)
		SET_CAM_ACTIVE(TvOnCam, FALSE)
		DESTROY_CAM(TvOnCam)
	ENDIF
	
	IF DOES_CAM_EXIST(TvOffCam)
		SET_CAM_ACTIVE(TvOffCam, FALSE)
		DESTROY_CAM(TvOffCam)
	ENDIF
	
	// Restore control
	SAFE_RESTORE_PLAYER_CONTROL()
	
	IF bPCControlsSetup
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
	ENDIF
	
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary.
FUNC BOOL HAS_SOFA_ANIM_DICT_LOADED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF HAS_ANIM_DICT_LOADED(sAnimDict)
		bSetupAnimDict = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests and loads particle effects for this activity
FUNC BOOL HAS_SOFA_PTFX_ASSET_LOADED()
	
	// Load PTFX
	REQUEST_PTFX_ASSET()
	IF HAS_PTFX_ASSET_LOADED()
		bSetupPTFX = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Obtains the heading and position of the remote control
FUNC BOOL HAS_REMOTE_BEEN_FOUND()

	OBJECT_INDEX mRemote = GET_CLOSEST_REMOTE()
	IF DOES_ENTITY_EXIST(mRemote)
//		vRemotePos = GET_ENTITY_COORDS(mRemote, FALSE)
//		fRemoteHead = GET_ENTITY_HEADING(mRemote)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Have particle effects and animations loaded for this activity
FUNC BOOL HAVE_ASSETS_LOADED()

	IF HAS_SOFA_ANIM_DICT_LOADED()
	AND HAS_SOFA_PTFX_ASSET_LOADED()
	AND HAS_REMOTE_BEEN_FOUND()
	AND HAS_AUDIO_LOADED()
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Assets have loaded...")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays the correct exit anim on the player, depending on
///    if he's holding the remote control or not.
PROC PLAY_QUIT_ANIM()
	
	IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	ENDIF
		
	mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
	TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "exit_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)							
		
	IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
	ENDIF
								
ENDPROC


/// PURPOSE:
///    Check for Michael smoking a cigar
PROC CHECK_FOR_SMOKE()
	
	// Do we want to smoke
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		BAWSAQ_INCREMENT_MODIFIER(BSMF_BSM_SMOKED_FOR_RWC)		//#1514495 - stockmarket
		
		IF STAT_GET_INT(NUM_SH_SOFA_SMOKED, iSmokeUsageStat)
			STAT_SET_INT(NUM_SH_SOFA_SMOKED, iSmokeUsageStat+1)
		ENDIF
		
		// Play the smoking anims.
		IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
			eSmokeState = SS_DROP_REMOTE
		ELSE
			bForceEnterSmoke = TRUE
			eSmokeState = SS_ENTER
		ENDIF
				
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RUN_SMOKE")
		eTriggerState = TS_RUN_SMOKE
	ENDIF		
ENDPROC

/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()
	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - PLAYER WANTS TO BREAK OUT")
			bBreakoutEarly = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the lighter sparks, flames and smoke produced by the smoking events
PROC HANDLE_PARTICLES(PARTICLE_STATE ePartState)
		
	SWITCH (ePartState)
	
		CASE PT_ENTRY
			IF NOT bLighterSparks AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.27
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Lighter Sparks")
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sh_lighter_sparks", GET_SECONDARY_ENTITY(), <<0,0,0.05>>, <<0,0,0>>)
				bLighterSparks = TRUE
			ENDIF
			
			IF NOT bLighterFlame AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.275
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Lighter Flame")
				ptfxLighter = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_lighter_flame", GET_SECONDARY_ENTITY(), <<0,0,0.05>>, <<0,0,0>>)
				bLighterFlame = TRUE
			ENDIF
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLighter) AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.45
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Stop Lighter Flame")
				STOP_PARTICLE_FX_LOOPED(ptfxLighter)
			ENDIF
		
			IF NOT bCigSmoke AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.346
				//CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Cig Smoke")
				ptfxCigarette = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_cig_smoke", GET_SYNCHED_SCENE_OBJECT(), vCigSmokePos, <<0,0,0>>)
				bCigSmoke = TRUE
			ENDIF
				
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Mouth Exhale 1")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
			
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Exhale Nose 1")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
			
			IF NOT bExhaleMouth2 AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.95
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Mouth Exhale 1")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth2 = TRUE
			ENDIF
			
			IF NOT bExhaleNose2 AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.95
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - ENTRY - Start Exhale Nose 1")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose2 = TRUE
			ENDIF
			
		BREAK
		
		CASE PT_IDLE_A
				
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.01
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_A - Start Exhale Mouth 2")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
						
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.01
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_A - Start Exhale Nose 2")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF
						
		BREAK
		
		CASE PT_IDLE_B
					
			IF NOT bExhaleMouth AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.01
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale mouth 2")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_mouth", PLAYER_PED_ID(), vMouthExhalePos, <<0,0,0>>, BONETAG_HEAD)
				bExhaleMouth = TRUE
			ENDIF
			
			IF NOT bExhaleNose AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.01
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_IDLE_B - Start Exhale Nose 2")
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_cig_exhale_nose", PLAYER_PED_ID(), vNoseExhalePos, vNoseExhaleRot, BONETAG_HEAD)
				bExhaleNose = TRUE
			ENDIF	
			
		BREAK
		
		CASE PT_EXIT

			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette) AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.35
				CPRINTLN(DEBUG_AMBIENT, "[SH] PTFX - PT_EXIT - End Cig Smoke")
				STOP_PARTICLE_FX_LOOPED(ptfxCigarette)
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC
			
/// PURPOSE:
///    Resets the particle effect bools.
PROC RESET_PARTICLE_BOOLS()

	// Reset particle bools
	bLighterFlame 	= FALSE
	bLighterSparks 	= FALSE
	bCigSmoke 		= FALSE
	bExhaleMouth 	= FALSE
	bExhaleNose 	= FALSE
	bExhaleMouth2 	= FALSE
	bExhaleNose2 	= FALSE
	
ENDPROC



PROC HANDLE_CAMERA()

	SWITCH eCamState
	
		CASE CS_CREATE
			
			CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CREATE")
			
			IF NOT DOES_CAM_EXIST(TvOnCam)
				TvOnCam = CREATE_CAMERA()
			ENDIF
			
			IF NOT DOES_CAM_EXIST(TvOffCam)
				TvOffCam = CREATE_CAMERA()
			ENDIF
			
			// Initialise camera
			IF DOES_CAM_EXIST(TvOnCam)
			AND DOES_CAM_EXIST(TvOffCam)	
				
				SET_CAM_PARAMS(TvOnCam, vTvOnCamPos, vTvOnCamRot, fTvOnCamFov)
				SET_CAM_PARAMS(TvOffCam, vTvOffCamPos, vTvOffCamRot, fTvOffCamFov)
				
				IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
					SET_CAM_ACTIVE(TvOnCam, TRUE)
					vCamRot = vTvOnCamRot // GET_CAM_ROT(TvOnCam)
					SHAKE_CAM(TvOnCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				ELSE
					SET_CAM_ACTIVE(TvOffCam, TRUE)
					vCamRot = vTvOffCamRot // GET_CAM_ROT(TvOffCam)
					SHAKE_CAM(TvOffCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
											
				CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_CREATE -> CS_ADJUST_CAMERA")
				eCamState = CS_ADJUST_CAMERA
			ENDIF
							
		BREAK
		
		CASE CS_TV_ON
			
			CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_TV_ON")
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene started (1)")
				START_AUDIO_SCENE("TV_MICHAELS_HOUSE")
			ENDIF
			
			IF DOES_CAM_EXIST(TvOffCam)
				IF IS_CAM_ACTIVE(TvOffCam)
					SET_CAM_ACTIVE(TvOnCam, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(TvOnCam)
				IF NOT IS_CAM_ACTIVE(TvOnCam)
				
					vCamRot = vTvOnCamRot
					SHAKE_CAM(TvOnCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
					SET_CAM_ACTIVE(TvOnCam, TRUE)
					
					CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_TV_ON -> CS_ADJUST_CAMERA")
					eCamState = CS_ADJUST_CAMERA
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_TV_OFF
			
			CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_TV_OFF")
			
			IF DOES_CAM_EXIST(TvOnCam)
				IF IS_CAM_ACTIVE(TvOnCam)
					SET_CAM_ACTIVE(TvOnCam, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(TvOffCam)
				IF NOT IS_CAM_ACTIVE(TvOffCam)
				
					vCamRot = vTvOffCamRot
					SHAKE_CAM(TvOffCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
					SET_CAM_ACTIVE(TvOffCam, TRUE)

					CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_TV_OFF -> CS_ADJUST_CAMERA")
					eCamState = CS_ADJUST_CAMERA
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_ADJUST_CAMERA
			IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
				DO_CAM_ADJUST(TvOnCam, vCamRot)
			ELSE
				DO_CAM_ADJUST(TvOffCam, vCamRot)
			ENDIF
		BREAK
		
		CASE CS_EXIT
			
			CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_EXIT")
			
			IF IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped")
				STOP_AUDIO_SCENE("TV_MICHAELS_HOUSE")
			ENDIF
			
			IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
				
				vExitCamPos = <<-806.2905, 173.1870, 73.3211>>
				vExitCamRot = <<-1.0904, -0.0034, -94.3685>>
				fExitCamFov = 50.0
				
				IF DOES_CAM_EXIST(TvOnCam)
					SET_CAM_PARAMS(TvOnCam, vTvOnCamPos, vTvOnCamRot, fTvOnCamFov, 0)
					SET_CAM_PARAMS(TvOnCam, vExitCamPos, vExitCamRot, fExitCamFov, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ENDIF
				
			ELSE
			
				vExitCamPos = <<-803.3088, 175.5172, 73.1840>>
				vExitCamRot = <<2.4800, -0.0034, 149.8074>>
				fExitCamFov = 50.0
				
				IF DOES_CAM_EXIST(TvOffCam)
					SET_CAM_PARAMS(TvOffCam, vTvOffCamPos, vTvOffCamRot, fTvOffCamFov, 0)
					SET_CAM_PARAMS(TvOffCam, vExitCamPos, vExitCamRot, fExitCamFov, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ENDIF
				
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "[SOFA CAM] CS_EXIT -> CS_NULL")				
			eCamState = CS_NULL
			
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL HAS_PLAYER_CANCELLED()

	// Cancel?
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		
		// Clear the text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
			CLEAR_HELP(TRUE)
		ENDIF
		
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) 
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		PLAY_QUIT_ANIM()

		PLAYER_DETACH_VIRTUAL_BOUND()
		SET_PLAYER_CLOTH_PACKAGE_INDEX(0)
		
		CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")
		bBreakoutEarly = FALSE
		
		IF bPCControlsSetup
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCControlsSetup = FALSE
		ENDIF
		
		eCamState = CS_EXIT
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_MICHAEL_SOFA_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
	FLOAT  fScenePhase  = 0.0
	
	IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
		HANDLE_CAMERA()
	ENDIF
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
						
			IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
			AND DO_REQUIRED_OBJECTS_EXIST()
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM_WEAPDIS")
				IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), vSofaPos, TRIGGER_ANGLE)
				OR bStoodInPlace
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_WAIT_FOR_PLAYER")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
						START_AUDIO_SCENE("MICHAEL_SAFEHOUSE_ACTIVITIES_SCENE")
					ENDIF						
										
					PRINT_HELP_FOREVER("TV_HLP0")
					eTriggerState = TS_WAIT_FOR_PLAYER	
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
	
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerVec, vTriggerSize)
				AND DO_REQUIRED_OBJECTS_EXIST()
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
						AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
						AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
																														
							CPRINTLN(DEBUG_AMBIENT, "[SH] - SOFA TRIGGERED...")
							g_eCurrentSafehouseActivity = SA_MICHAEL_SOFA	// Set global for timetable events
							
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								eCamState = CS_CREATE
							ENDIF
							
							// Block that annoying pole...
							IF iBlockArea = -1
								iBlockArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-804.05, 173.54, 72.170>>, <<1.02, 1.0, 1.0>>, DEG_TO_RAD(-52.4))
							ENDIF
					
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
								CLEAR_HELP(TRUE)
							ENDIF
							
							//ENABLE_MOVIE_SUBTITLES(TRUE)
							
							CLEAR_AREA_OF_PROJECTILES(vSofaPos, 3.0)
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							ELSE
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
							ENDIF
							bSafehouseSetControlOff = TRUE
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
							
							INIT_PC_SCRIPTED_CONTROLS( "SOFA ACTIVITY")
							bPCControlsSetup = TRUE
														
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_SEAT_PLAYER")
							eTriggerState = TS_SEAT_PLAYER
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP0")
						CLEAR_HELP(TRUE)
					ENDIF
					bStoodInPlace = FALSE
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_SEAT_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "enter_michael", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 0.0)
			ELSE
				SET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene, 0.163)
			ENDIF

			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF		
						
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			ENDIF
			
			CLEAR_AREA_OF_VEHICLES(GET_SYNCHED_SCENE_VECTOR(), 5.0)
			
			PLAYER_ATTACH_VIRTUAL_BOUND( << -804.592834, 173.180145, 71.684357 >>, << 0.0, 0.0, 0.590625 >>, 1.0, 0.7 )
			SET_PLAYER_CLOTH_PACKAGE_INDEX(2)
			
			CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_WAIT_FOR_SEATING")
			eTriggerState = TS_WAIT_FOR_SEATING
		BREAK
		
		CASE TS_WAIT_FOR_SEATING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Put the player into his idle sitting anim
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.8
			
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				
				// Loop the plain seating idle if the TV is off
				IF NOT g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
				
					RESTORE_STANDARD_CHANNELS()
					
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base_michael", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
					SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				
				// Don't loop if the TV is on, because we need to pick the remote up.
				ELSE
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV Switched on, TV audio scene started")
						START_AUDIO_SCENE("TV_MICHAELS_HOUSE")
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base_michael", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
				ENDIF
																
				CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState = TS_SHOW_SOFA_INSTRUCTIONS	
			
			ENDIF	
		BREAK
		
		CASE TS_SHOW_SOFA_INSTRUCTIONS
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT HAS_PLAYER_CANCELLED()
									
				IF NOT g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
										
					// Show help text about turning on TV or smoking cigars.
					PRINT_HELP_FOREVER("TV_HLP3")				
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_NO_REMOTE")
					eTriggerState = TS_IDLE_NO_REMOTE
					
				ELSE
					
					// Pick the remote control up once we're sat down
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.8
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter_michael", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						// Clear previous help
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
							CLEAR_HELP(TRUE)
						ENDIF
						
						
						ENABLE_MOVIE_SUBTITLES(TRUE)
						
						// Show help text about turning TV on or smoking.
						PRINT_HELP_FOREVER("TV_HLP4")
									
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PICK_UP_REMOTE")
						eTriggerState = TS_PICK_UP_REMOTE
					
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING from TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState  = TS_QUITTING
			ENDIF
			
		BREAK 
		
		CASE TS_IDLE_NO_REMOTE
				
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			
			IF NOT HAS_PLAYER_CANCELLED()
				
				CHECK_FOR_SMOKE()
								
				// Check the prompt is displaying - sometimes doesn't re-display when exiting a paused state...
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
					// Show help text about operating TV or smoking cigars.
					PRINT_HELP_FOREVER("TV_HLP3")
				ENDIF
				
				// Has player turned the TV on?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
														
					// Increment TV usage stat
					IF NOT bIncreasedTVStat
						IF STAT_GET_INT(NUM_SH_SOFA_USED, iTVUsageStat)
							STAT_SET_INT(NUM_SH_SOFA_USED, iTVUsageStat+1)
							bIncreasedTVStat = TRUE
						ENDIF
					ENDIF
			
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PICK_UP_REMOTE")			
					eTriggerState = TS_PICK_UP_REMOTE
				
				ENDIF	
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING from TS_IDLE_NO_REMOTE")
				eTriggerState = TS_QUITTING
			ENDIF
		BREAK
		
		CASE TS_PICK_UP_REMOTE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.8
				
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base_michael", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
									
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_WITH_REMOTE")
				eTriggerState = TS_IDLE_WITH_REMOTE
			ELSE
									
				IF NOT g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn	
					// Only turn the TV on when the anim is part-way through.
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
					
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Setting TvOnCam active")
							eCamState = CS_TV_ON
						ENDIF
						
						// Turn the TV on
						START_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV Switched on, TV audio scene started")
							START_AUDIO_SCENE("TV_MICHAELS_HOUSE")
						ENDIF
						
						ENABLE_MOVIE_SUBTITLES(TRUE)	
						// Show help text about operating TV or smoking.
						PRINT_HELP_FOREVER("TV_HLP4")
					ENDIF	
				ENDIF
			ENDIF
			
		BREAK
		
		CASE TS_IDLE_WITH_REMOTE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			
			IF NOT HAS_PLAYER_CANCELLED()
				
				// Check the prompt is displaying - sometimes doesn't re-display when exiting a paused state...
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
					// Show help text about operating TV or smoking cigars.
					PRINT_HELP_FOREVER("TV_HLP4")
				ENDIF
				
				CHECK_FOR_SMOKE()
				RUN_TV_TRANSPORT_CONTROLS(FALSE, TRUE)		
							
				// Has player turned the TV off?
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
									
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_exit_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
										
					bNeedTvOff = TRUE
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_PUT_REMOTE_DOWN")
					eTriggerState = TS_PUT_REMOTE_DOWN
				
				ELIF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL()
					
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_idle_a_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_idle_a_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					
					bNeedChangeChannel = TRUE
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_CHANGE_CHANNEL")
					eTriggerState = TS_CHANGE_CHANNEL
					
				ENDIF	
			
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING from TS_CHANGE_CHANNEL")
				eTriggerState = TS_QUITTING
			ENDIF
			
		BREAK 
		
		CASE TS_CHANGE_CHANNEL
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				IF bNeedChangeChannel
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					IF fScenePhase > 0.3
						IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_1
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
						ELSE
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)	
						ENDIF
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Channel changed")
						bNeedChangeChannel = FALSE
						
					ENDIF
				ENDIF
			ELSE
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
				
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_WITH_REMOTE")
				eTriggerState = TS_IDLE_WITH_REMOTE
			ENDIF
		BREAK
		
		CASE TS_PUT_REMOTE_DOWN
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
						
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
							
				IF bNeedTvOff
					IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.5
					
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Setting TvOffCam active")
							eCamState = CS_TV_OFF
						ENDIF
						
						STOP_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
						ENABLE_MOVIE_SUBTITLES(FALSE)
						
						IF IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV switched off by player - TV audio scene stopped (reset)")
							STOP_AUDIO_SCENE("TV_MICHAELS_HOUSE")
						ENDIF
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
							CLEAR_HELP()
						ENDIF
						bNeedTvOff = FALSE
					ENDIF
				ENDIF
			ELSE		
				
				// Play the idle anims
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))		
				SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)				
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_SHOW_SOFA_INSTRUCTIONS")
				eTriggerState = TS_SHOW_SOFA_INSTRUCTIONS
			ENDIF
			
		BREAK
		
		CASE TS_RUN_SMOKE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
				SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
			ENDIF
					
			SWITCH (eSmokeState)
				
				CASE SS_DROP_REMOTE	
				
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
						CLEAR_HELP(TRUE)
					ENDIF
		
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_exit_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_exit_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					
					IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_ENTER")
					eSmokeState = SS_ENTER
				BREAK
				
				CASE SS_ENTER
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP4")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP3")
						CLEAR_HELP(TRUE)
					ENDIF
		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
					OR bForceEnterSmoke
						
						RESET_PARTICLE_BOOLS()
															
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
													
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "cigar_enter_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "cigar_enter_cigar", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "cigar_enter_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						bForceEnterSmoke = FALSE
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_IDLE")
						eSmokeState = SS_IDLE
					ENDIF
				BREAK
				
				CASE SS_IDLE
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						//CHECK_FOR_BREAKOUT()
						HANDLE_PARTICLES(PT_ENTRY)						
						
					ELSE
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						bRandom = GET_RANDOM_BOOL()
												
						IF bRandom
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Using idle A anims")
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "cigar_idle_a_michael", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "cigar_idle_a_cigar", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "cigar_idle_a_lighter", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - Using idle B anims")
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "cigar_idle_b_michael", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "cigar_idle_b_cigar", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "cigar_idle_b_lighter", sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						RESET_PARTICLE_BOOLS()						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_EXIT")
						eSmokeState = SS_EXIT
					
					ENDIF
						
				BREAK
				
				CASE SS_EXIT
					
					// Is the idle still running?
					IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
												
						IF bRandom
							HANDLE_PARTICLES(PT_IDLE_A)
						ELSE
							HANDLE_PARTICLES(PT_IDLE_B)
						ENDIF
						
						fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
						FLOAT fReturnStartPhase, fReturnEndPhase
						
						IF fScenePhase < 0.5
							CHECK_FOR_BREAKOUT()
						ENDIF
						
						// Does the player want to quit early?
						IF bBreakoutEarly
							IF NOT bSetBreakoutAnim
														
								IF bRandom
									IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "cigar_idle_a_michael", GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
										CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - FOUND ANIM EVENT PHASE for idle a")
										IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
											bSetBreakoutAnim = TRUE
										ENDIF
									ENDIF
								ELSE
									IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "cigar_idle_b_michael", GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)
										CPRINTLN(DEBUG_AMBIENT, "[SA] Sofa - FOUND ANIM EVENT PHASE for idle b")
										IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
											bSetBreakoutAnim = TRUE
										ENDIF
									ENDIF
								ENDIF
							
							ELSE
														
								mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
								
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "cigar_interrupt_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "cigar_interrupt_cigar", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "cigar_interrupt_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)					
								
								IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
								ENDIF
								
								CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_QUITTING")
								bBreakoutEarly = FALSE
								eTriggerState  = TS_QUITTING
							
							ENDIF
						ENDIF
					
					// If the smoking idle has finished...
					ELSE
						
						// Play the exit anim
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "cigar_exit_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, "cigar_exit_cigar", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SECONDARY_ENTITY(), mSynchedScene, "cigar_exit_lighter", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						RESET_PARTICLE_BOOLS()
						
						IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn	
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_PICK_UP_REMOTE")
							eSmokeState = SS_PICK_UP_REMOTE
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_START_SEATING_IDLE")
							eSmokeState = SS_START_SEATING_IDLE
						ENDIF
					ENDIF
				BREAK
				
				CASE SS_PICK_UP_REMOTE
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_enter_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_enter_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
												
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - SS_START_SEATING_IDLE")
						eSmokeState = SS_START_SEATING_IDLE	
					ELSE
						HANDLE_PARTICLES(PT_EXIT)
					ENDIF
			
				BREAK
				
				CASE SS_START_SEATING_IDLE
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
						
						RESET_PARTICLE_BOOLS()
						
						mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
						
						IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn	
				
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "tv_base_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "tv_base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
							SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
							
							// Show help text about turning TV on or smoking.
							PRINT_HELP_FOREVER("TV_HLP4")
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa: TS_IDLE_WITH_REMOTE")
							eTriggerState = TS_IDLE_WITH_REMOTE
						
						ELSE
												
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, "base_michael", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)	
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_REMOTE(), mSynchedScene, "base_remote", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
							SET_SYNCHRONIZED_SCENE_LOOPED(mSynchedScene, TRUE)
							
							// Show help text about operating TV or smoking weed.
							PRINT_HELP_FOREVER("TV_HLP3")
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_IDLE_NO_REMOTE")
							eTriggerState = TS_IDLE_NO_REMOTE
							
						ENDIF
					ELSE
						HANDLE_PARTICLES(PT_EXIT)
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK 
				
		CASE TS_QUITTING
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)	
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(TvOnCam)
				AND IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(TvOffCam)
					IF NOT IS_CAM_INTERPOLATING(TvOnCam)
					AND NOT IS_CAM_INTERPOLATING(TvOffCam)
						CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET")
						eTriggerState = TS_RESET
					ENDIF
				ELSE
					eTriggerState = TS_RESET
				ENDIF
			ELSE
			
				CHECK_FOR_BREAKOUT()
				HANDLE_PARTICLES(PT_EXIT)
				
				IF bBreakoutEarly
										
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
			
					IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), "exit_michael", "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
				
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
														
							// Clear tasks as we are breaking out of the anim early...
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TS_RESET - EXIT BREAKOUT!")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RESET
			
			ENABLE_MOVIE_SUBTITLES(FALSE)
			
			// Increment sofa usage stat
			IF STAT_GET_INT(NUM_SH_SOFA_USED, iSofaUsageStat)
				STAT_SET_INT(NUM_SH_SOFA_USED, iSofaUsageStat+1)
			ENDIF
			
			// Normal gameplay camera
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("TV_MICHAELS_HOUSE")
				CPRINTLN(DEBUG_AMBIENT, "[SH] Sofa - TV audio scene stopped (reset)")
				STOP_AUDIO_SCENE("TV_MICHAELS_HOUSE")
			ENDIF
				
			// Return control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
			bSafehouseSetControlOff = FALSE
			bBreakoutEarly = FALSE
			bSetBreakoutAnim = FALSE
			bStoodInPlace = TRUE
			
			IF g_TVStruct[TV_LOC_MICHAEL_PROJECTOR].bIsTVOn
				RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOC_MICHAEL_PROJECTOR)
			ENDIF			
			
			// Reset global for timetable events
			g_eCurrentSafehouseActivity = SA_NONE
					
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(coords_struct in_coords)

	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU) 
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Default Cleanup")
		CLEANUP_MICHAEL_SOFA_ACTIVITY()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
	OR IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
//	OR PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_DAUGHTER)
//	OR PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_SON)
//	OR PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_WIFE)
//	OR PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_MEXMAID)
	OR IS_MOBILE_PHONE_CALL_ONGOING()
		CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Cleanup due to some shit happening close by")
		CLEANUP_MICHAEL_SOFA_ACTIVITY()
	ENDIF
	
	vTriggerVec = in_coords.vec_coord[0]
	vTriggerVec = <<-803.6420, 173.2916, 71.8347>>
	mTriggerModel = PROP_CIGAR_03	
	
	#IF IS_DEBUG_BUILD
		INIT_RAG_WIDGETS()
	#ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Ready for launch...")
		
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> NULL
		AND IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)	
			IF GET_DISTANCE_BETWEEN_COORDS(vTriggerVec, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < SOFA_TRIGGER_DIST
			
				SWITCH eState
										
					CASE AS_LOAD_ASSETS			
						IF HAVE_ASSETS_LOADED()
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Assets loaded for activity...")
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							//UPDATE_RAG_WIDGETS()
							UPDATE_MICHAEL_SOFA_ACTIVITY()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
					
				ENDSWITCH
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(vTriggerVec, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 6.0
					CLEANUP_MICHAEL_SOFA_ACTIVITY()
				ENDIF
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
				CLEANUP_OBJECT_WIDGETS()
			#ENDIF
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Michael Sofa: Cleaning up from inside script loop...")
			CLEANUP_MICHAEL_SOFA_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
