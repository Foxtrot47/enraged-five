// Includes
USING "ob_safehouse_common.sch"
USING "rgeneral_include.sch"
USING "net_realty_mess_include.sch"
USING "net_mission_trigger_public.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_drinking.sc
//		DESCRIPTION		:	Franklin's beer
//
// *****************************************************************************************
// *****************************************************************************************
// Constants
CONST_INT WHEATGRASS_REGEN_DIST(25)

// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE, 
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_RUN_EXIT_ANIM,
	TS_BREAKOUT_EARLY,
	TS_WAIT_TO_LEAVE_AREA,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

CONST_FLOAT BEER_AREA_WIDTH 2.0

// Variables
BOOL	bBreakoutEarly 		= FALSE
BOOL	bAnimLoaded 		= FALSE
BOOL 	bTakenHit			= FALSE

CAMERA_INDEX CamIndex

FLOAT fTriggerHead

INT 	iCurProperty
INT		mSynchedScene
INT 	iUsageStat
INT 	iFarCam

VECTOR 	vCamPos[MAX_CAMS]			
VECTOR	vCamHead[MAX_CAMS]
//FLOAT 	fCamFov[MAX_CAMS]

VECTOR 	vExitCamPos	
VECTOR 	vExitCamHead
FLOAT 	fExitCamFov		= 50.0

STRING	sClipSet	

STRING sAnimDict		

//STRING sPedEnterAnim	= "enter"
//STRING sBottleEnterAnim = "enter_bot"

STRING sHelpText 		= "SA_BEER"

VECTOR vTriggerPos
VECTOR vScenePos
VECTOR vSceneHead
VECTOR vTriggerAreaA
VECTOR vTriggerAreaB

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_DRINKING_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: Cleaning up...")
	
	// Clear help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
		CLEAR_HELP(TRUE)
	ENDIF
					
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
		
	// Remove camera
	IF DOES_CAM_EXIST(CamIndex)
		DESTROY_CAM(CamIndex)
	ENDIF
		
	IF g_bInMultiplayer
		
		// Clean up anims on beer bottle prop.
		IF DOES_ENTITY_EXIST(mTrigger)
		AND bAnimLoaded
		AND DOES_ENTITY_HAVE_DRAWABLE(mTrigger) // Fix for B*1647028 - Check for drawable to prevent assert.
			PLAY_ENTITY_ANIM(mTrigger, "base_drink_bottle", sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE)
		ENDIF
		
		IF bAnimLoaded
			REMOVE_ANIM_DICT(sAnimDict)
		ENDIF
	
		// Restore control
		IF bSafehouseSetControlOff 
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		// End script
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	ELSE
		
		IF bAnimLoaded
			REMOVE_ANIM_DICT(sAnimDict)
		ENDIF
		
		// B*1895222 - Reactivate the chop script only if the player started the activity
		IF bSafehouseSetControlOff
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
		ENDIF
		
		// Restore control and remove motion blur
		SAFE_RESTORE_PLAYER_CONTROL()
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_PED_MOTION_BLUR(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// End script
		TERMINATE_THIS_THREAD()
	
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE
///    Gets the furthest camera from the current gameplay cams position (to reduce the visible jump in the players pos).
FUNC INT GET_FURTHEST_CAM()
	
	FLOAT fDiff
	FLOAT fFurthestDist = 0.0
	INT i = 0
	INT iFurthestCam = 0
	
	VECTOR vGameplayCamPos = GET_GAMEPLAY_CAM_COORD()
	
	REPEAT MAX_CAMS i
		IF NOT ARE_VECTORS_ALMOST_EQUAL(vCamPos[i], <<0.0, 0.0, 0.0>>)
			
			fDiff = GET_DISTANCE_BETWEEN_COORDS(vGameplayCamPos, vCamPos[i])
			
			IF fDiff > fFurthestDist
				fFurthestDist = fDiff
				iFurthestCam = i 
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iFurthestCam

ENDFUNC

PROC INIT_CAM_POSITIONS()
	
	INT i = 0
	REPEAT MAX_CAMS i 
		vCamPos[i] = <<0.0, 0.0, 0.0>>
		vCamHead[i] = <<0.0, 0.0, 0.0>>
		//fCamFov[i] = 0.0
	ENDREPEAT
		
	vCamPos[0] = <<-9.5, -1428.4, 31.5>>
	vCamHead[0] = <<0.6, 0.0, 148.7>>
	//fCamFov[0] = 40.0

	vCamPos[1] = <<-9.1, -1429.6, 31.4>>
	vCamHead[1] = <<11.8, 0.0, 71.9>>
	//fCamFov[1] = 34.6
	
ENDPROC

/// PURPOSE:
///    Requests and checks that the anim dict for this activity has loaded
/// RETURNS:
///    True if the anim dict has loaded.
FUNC BOOL HAS_DRINKING_ANIM_DICT_LOADED()

	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	
	WHILE NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		WAIT(0)
	ENDWHILE
	//CPRINTLN(DEBUG_AMBIENT, "ANIM DICT LOADED")
	bAnimLoaded = TRUE
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Monitor sticks for movement and set a flag if the player wants to
///    quit the activity early.
PROC CHECK_FOR_BREAKOUT()

	// Does the player want to break out?
	IF NOT bBreakoutEarly
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			
			sClipSet = Get_Drunk_Level_Moveclip(DL_verydrunk)
			REQUEST_CLIP_SET(sClipSet)
	
			IF HAS_CLIP_SET_LOADED(sClipSet)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: PLAYER WANTS TO BREAK OUT")
				bBreakoutEarly = TRUE
				//SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), sClipSet)
				//SET_PED_ALTERNATE_MOVEMENT_ANIM(PLAYER_PED_ID(), AAT_IDLE, sClipSet, "idle")
			ENDIF			
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerAreaA, vTriggerAreaB, BEER_AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Updates drinking activity
PROC UPDATE_DRINKING_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0

	SWITCH eTriggerState
			
		CASE TS_PLAYER_OUT_OF_RANGE

			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
			AND DO_REQUIRED_OBJECTS_EXIST()
			
			//AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
				
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: PLAYER CLOSE TO TRIGGER...")
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (GET_HEADING_FOR_THIS_OBJ() - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (GET_HEADING_FOR_THIS_OBJ() + TRIGGER_ANGLE)
				AND DO_REQUIRED_OBJECTS_EXIST()
				
					IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
												
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: ACTIVITY TRIGGERED...")
						
						DISABLE_SELECTOR_THIS_FRAME()		
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						bSafehouseSetControlOff = TRUE
						
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT,FALSE)	
						
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
	
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			// Fix for crappy shadows
			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.3) 
			CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)

			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), sAnimDict, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON 
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAMERA()
							
				// Initialise camera
				IF DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vCamPos[0], vCamHead[0], 45.0)
					SET_CAM_ACTIVE(CamIndex, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SHAKE_CAM(CamIndex, "HAND_SHAKE", DEFAULT_CAM_SHAKE/DEFAULT_CAM_DAMPER)
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
			
			// Play scene
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: TS_GRAB_PLAYER -> TS_RUN_ANIM")
			eTriggerState = TS_RUN_ANIM
			
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
				
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
								
				IF NOT bTakenHit
					IF fScenePhase > 0.7
						IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
						ELSE
							Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
						ENDIF
						bTakenHit = TRUE
					ENDIF
				ENDIF
							
				CHECK_FOR_BREAKOUT()
				
				IF bBreakoutEarly
					
					FLOAT fReturnStartPhase, fReturnEndPhase
						
					// Can we trigger the seperate exit anim early?
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), GET_INTERUPT_STRING(), fReturnStartPhase, fReturnEndPhase)

						IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
							
							CPRINTLN(DEBUG_AMBIENT, "Player has had a drink")
							IF NOT bTakenHit
								IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
									Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
								ELSE
									Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
								ENDIF
								bTakenHit = TRUE
							ENDIF
							
							// If we have seperate exit anims, play them here.
							mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT)
														
							IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
							ENDIF
							
							bBreakoutEarly = FALSE
							
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer hit ScriptEvent at frame ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: TS_RUN_ANIM -> TS_BREAKOUT_EARLY")
							eTriggerState = TS_BREAKOUT_EARLY
							
						ENDIF
					ENDIF
					
					// Can we break out completely here?
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)

						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT, "Player has had a drink")
							IF NOT bTakenHit
								IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
									Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
								ELSE
									Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
								ENDIF
								bTakenHit = TRUE
							ENDIF
							
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer hit walkinterruptible at frame ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer - TS_RESET")
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
							ENDIF
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
					
				ENDIF
			
			ELSE // The drinking anim has finished
				CPRINTLN(DEBUG_AMBIENT, "The drinking enter anim has finished")
								
				bBreakoutEarly = FALSE
									
				// Play the seperate exit anim
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_SYNCHED_SCENE_OBJECT(), mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
				ENDIF
									
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer - TS_RUN_EXIT_ANIM")
				eTriggerState  = TS_RUN_EXIT_ANIM
												
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA BEER - Scene has finished TS_RESET")
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
				ENDIF
				eTriggerState = TS_RESET
			ELSE
				CHECK_FOR_BREAKOUT()
			
				IF bBreakoutEarly
										
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
			
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
				
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
														
							// Clear tasks
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] In TS_RUN_EXIT_ANIM FRA Beer hit walkinterruptible at frame ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer TS_RESET")
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
							ENDIF
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_BREAKOUT_EARLY
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				
				CHECK_FOR_BREAKOUT()
								
				IF bBreakoutEarly
											
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
				
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
					
						IF (fScenePhase >= fReturnStartPhase) 
						AND (fScenePhase <= fReturnEndPhase)
															
							// Clear tasks
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] In TS_BREAKOUT_EARLY FRA Beer hit walkinterruptible at frame ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer - TS_RESET")
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
							ENDIF
							eTriggerState = TS_RESET
						ENDIF

					ENDIF
				
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] In TS_BREAKOUT_EARLY , Player allowed breakout exit anim to run to completion.")
				IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
					SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
				ENDIF
				eTriggerState = TS_RESET
			ENDIF
		BREAK
				
		CASE TS_RESET	
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: Resetting")
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")	
			
			// Turn off the shadows fix.
			CASCADE_SHADOWS_INIT_SESSION()
			
			IF STAT_GET_INT(NUM_SH_BEER_DRUNK, iUsageStat)
				STAT_SET_INT(NUM_SH_BEER_DRUNK, iUsageStat+1)
			ENDIF
					
			// Normal gameplay cam
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)	
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
						
			// Restore control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
			bSafehouseSetControlOff = FALSE
			bBreakoutEarly = FALSE
			bTakenHit = FALSE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT,FALSE)
				
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: TS_RESET -> TS_PLAYER_OUT_OF_RANGE")

		BREAK
		
	ENDSWITCH
ENDPROC

PROC MP_UPDATE_DRINKING_ACTIVITY()
	
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0
	
	//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, BEER_AREA_WIDTH)
	
	SWITCH eTriggerState
				
		CASE TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = ci_APT_ACT_BEER
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BEER)
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, BEER_AREA_WIDTH)
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
						START_AUDIO_SCENE("FRANKLIN_SAFEHOUSE_ACTIVITIES_SCENE")
					ENDIF
					
					PRINT_HELP_FOREVER(sHelpText)
					
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: TS_WAIT_FOR_PLAYER")
					eTriggerState = TS_WAIT_FOR_PLAYER					
				ENDIF
			ENDIF
		BREAK

		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
			AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(ci_APT_ACT_BEER)
				IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) >= (fTriggerHead - TRIGGER_ANGLE)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) <= (fTriggerHead + TRIGGER_ANGLE)
				AND IS_PLAYER_ALONE_IN_AREA()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, BEER_AREA_WIDTH)
				
					IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)			
						
						iPersonalAptActivity = ci_APT_ACT_BEER
												
						DISABLE_SELECTOR_THIS_FRAME()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
							CLEAR_HELP(TRUE)
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						bSafehouseSetControlOff = TRUE
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)	
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
						
						CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: TS_GRAB_PLAYER")
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
						CLEAR_HELP(TRUE)
					ENDIF
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				
				// Remove mask
				REMOVE_PLAYER_MASK()
				
				// Fix for crappy shadows
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE) 
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.3) 
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)

				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_ENTER_ANIM_FOR_THIS_ACTIVITY(), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN, AIK_DISABLE_HEAD_IK)
				SET_ENTITY_COLLISION(mTrigger, FALSE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(), sAnimDict, SLOW_BLEND_IN, SLOW_BLEND_OUT)
								
				// Play scene
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: TS_RUN_ANIM")
				eTriggerState = TS_RUN_ANIM
			
			ENDIF
						
		BREAK
		
		CASE TS_RUN_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			//DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
					
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
									
					IF NOT bTakenHit
						IF fScenePhase > 0.7
							IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
								Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), CamIndex)
							ELSE
								Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), NULL)
							ENDIF
							bTakenHit = TRUE
						ENDIF
					ENDIF
										
				ELSE // The drinking anim has finished
													
					bBreakoutEarly = FALSE
									
					// Play the seperate exit anim
					mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, NORMAL_BLEND_IN, AIK_DISABLE_HEAD_IK)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
																			
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA Beer - TS_RUN_EXIT_ANIM")
					eTriggerState  = TS_RUN_EXIT_ANIM
													
				ENDIF
			ENDIF
		BREAK 
		
		CASE TS_RUN_EXIT_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			//DO_CAM_ADJUST(CamIndex, vCamHead[iFarCam])
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) > 0.99
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] FRA BEER - Scene has finished TS_RESET")
					//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
					eTriggerState = TS_RESET

				ENDIF
			ENDIF
		BREAK
		
		CASE TS_BREAKOUT_EARLY
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene) < 0.99
				
				CHECK_FOR_BREAKOUT()
								
				IF bBreakoutEarly
											
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
				
					IF FIND_ANIM_EVENT_PHASE(sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
					
						IF (fScenePhase >= fReturnStartPhase) AND (fScenePhase <= fReturnEndPhase)
															
							// Clear tasks
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] In TS_BREAKOUT_EARLY FRA Beer hit walkinterruptible at frame ", fScenePhase)
							CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer - TS_RESET")
							//SET_CAM_PARAMS(CamIndex, vExitCamPos, vExitCamHead, fExitCamFov)
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
			
			ELSE // The drinking anim has finished
				CPRINTLN(DEBUG_AMBIENT, "The drinking enter anim has finished")
								
				bBreakoutEarly = FALSE
									
				// Play the seperate exit anim
				mSynchedScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneHead)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, sAnimDict, GET_EXIT_ANIM_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_EXIT_ANIM_FOR_THIS_ENTITY(), sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
																	
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer - TS_RUN_EXIT_ANIM")
				eTriggerState  = TS_RUN_EXIT_ANIM
												
			ENDIF
		BREAK 
					
		CASE TS_RESET	
									
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			RESTORE_PLAYER_MASK()
			
			INCREMENT_MP_APT_DRINK_COUNTER()
			
			// Turn off the shadows fix.
			CASCADE_SHADOWS_INIT_SESSION()
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			iPersonalAptActivity = ci_APT_ACT_IDLE
			
//			IF STAT_GET_INT(NUM_SH_BEER_DRUNK, iUsageStat)
//				STAT_SET_INT(NUM_SH_BEER_DRUNK, iUsageStat+1)
//			ENDIF

		
			// Restore control
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
			bBreakoutEarly = FALSE
			bTakenHit = FALSE
			
			SET_ENTITY_COLLISION(mTrigger, TRUE)
						
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
			
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: TS_PLAYER_OUT_OF_RANGE")
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
			
		BREAK
		
	ENDSWITCH
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
			
	IF DOES_ENTITY_EXIST(mObjectIn)
	
		mTrigger = mObjectIn		
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)
		FREEZE_ENTITY_POSITION(mTrigger, TRUE)
		CPRINTLN(DEBUG_AMBIENT, "Beer - Trigger Object Frozen")
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_DIRECTOR)
			OR Is_Player_On_Or_Triggering_Any_Mission()
			OR MPGlobalsAmbience.bRunningFmIntroCut
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
			
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
			
			// This makes sure the net script is active, waits until it is.
			HANDLE_NET_SCRIPT_INITIALISATION()
			SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
						
			//NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
			
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			CPRINTLN(DEBUG_AMBIENT, "[MP] Beer - Current property is ", iCurProperty)
			
			GET_HOUSE_INTERIOR_DETAILS(tempPropertyStruct, iCurProperty)
			
			vTriggerPos = tempPropertyStruct.house.activity[SAFEACT_BEER].vTriggerVec 
			fTriggerHead = tempPropertyStruct.house.activity[SAFEACT_BEER].fTriggerRot
			vScenePos = tempPropertyStruct.house.activity[SAFEACT_BEER].vSceneVec
			vSceneHead = tempPropertyStruct.house.activity[SAFEACT_BEER].vSceneRot
			
//			vCamPos[0] = tempPropertyStruct.house.activity[SAFEACT_BEER].vCamVec
//			vCamHead[0] = tempPropertyStruct.house.activity[SAFEACT_BEER].vCamRot
//			vCamPos[1] = tempPropertyStruct.house.activity[SAFEACT_BEER].vCam2Vec
//			vCamHead[1] = tempPropertyStruct.house.activity[SAFEACT_BEER].vCam2Rot
//			
//			fCamFov[0] = 37.1757
//			vExitCamPos = tempPropertyStruct.house.activity[SAFEACT_BEER].vExitCamVec
//			vExitCamHead = tempPropertyStruct.house.activity[SAFEACT_BEER].vExitCamRot
			vTriggerAreaA = tempPropertyStruct.house.activity[SAFEACT_BEER].vAreaAVec
			vTriggerAreaB = tempPropertyStruct.house.activity[SAFEACT_BEER].vAreaBVec
			
			IF fTriggerHead < 0.0
				fTriggerHead+= 360.0
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "[MP] Beer Ready for network launch...")
			
			sAnimDict = "MP_SAFEHOUSEBEER@"
		ELSE
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// Default cleanup
				IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
				OR IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
				OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)	
				OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
					CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
					CLEANUP_DRINKING_ACTIVITY()
				ENDIF
			ELIF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
				OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)	
				OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC()))
				CPRINTLN(DEBUG_AMBIENT, "DEFAULT CLEANUP")
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
			
			INIT_CAM_POSITIONS()
			
			vTriggerPos = GET_TRIGGER_VEC_FOR_THIS_OBJECT()
			vExitCamPos = << -11.2, -1428.9, 32.0 >>
			vExitCamHead = << -23.1, -0.0000, -121.3 >>
			fExitCamFov	= 50.0
			
			sAnimDict = "safe@franklin@ig_9"
			
			CPRINTLN(DEBUG_AMBIENT, "[SAFEHOUSE] BEER IS TRIGGERING ...")
				
		ENDIF
		
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF DOES_ENTITY_EXIST(mTrigger)
									
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
			AND NOT IS_ENTITY_DEAD(mTrigger)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND CAN_PLAYER_USE_THIS_OBJECT()
			AND IS_INTERIOR_CORRECT(mTrigger)
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
				SWITCH eState
										
					CASE AS_LOAD_ASSETS
						IF HAS_DRINKING_ANIM_DICT_LOADED()
						AND HAS_AUDIO_LOADED()
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
		
					CASE AS_RUN_ACTIVITY
						//IF g_OldSafehouseActivites = TRUE
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF g_bInMultiplayer
									IF NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
										MP_UPDATE_DRINKING_ACTIVITY()
									ELSE
										CLEANUP_DRINKING_ACTIVITY()
									ENDIF
								ELSE
									UPDATE_DRINKING_ACTIVITY()
								ENDIF							
							ENDIF
						//ENDIF
					BREAK
					
					CASE AS_END
					BREAK
				ENDSWITCH
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: CLEANUP_DRINKING_ACTIVITY 1")
				CLEANUP_DRINKING_ACTIVITY()
			ENDIF
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Beer: CLEANUP_DRINKING_ACTIVITY 2")
			CLEANUP_DRINKING_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
