// Philip O'Duffy
USING "net_activity_creator_activities.sch"

//CONST_INT ACTIVITY_STRUCT_VARIABLE_SIZE 0

USING "net_activity_creator_components.sch"
// Mission Script -----------------------------------------//
SCRIPT(CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)	
	#IF IS_DEBUG_BUILD 
//	CREATE_LUXE_ACT_WIDGETS(serverBD, luxeActState)
	#ENDIF
	ACTIVITY_MAIN activityCreatorMain
	
	PROCESS_PRE_GAME_ACT_CREATOR(activityCreatorMain, activityCheck)
	
	WHILE TRUE
		WAIT(0)
		RUN_ACTIVITY_UPDATE(activityCreatorMain)
	ENDWHILE
ENDSCRIPT