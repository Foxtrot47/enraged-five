// Includes
USING "rage_builtins.sch" 
USING "globals.sch" 
USING "ob_safehouse_common.sch"
USING "commands_ped.sch"
USING "freemode_header.sch"
USING "net_spawn_activities.sch"
USING "net_mission_trigger_public.sch"
USING "rgeneral_include.sch"
USING "net_realty_new.sch"
USING "net_gang_boss.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_shower_med.sc
//		DESCRIPTION		:	Handles bed activity in low-level multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************

//***********APT-Content variables***********
MP_DOOR_DETAILS bathroomDoor
MP_PROP_OFFSET_STRUCT bathroomDoorLoc

VECTOR 		vBathroomAreaCPos
VECTOR 		vBathroomAreaCPosB
VECTOR 		vShowerObject

INT 		iActivityID 		= ci_APT_ACT_SHOWER		//Needed for multiple yacht showers
INT 		iShowerID 			= SAFEACT_SHOWER
INT 		iBaseProperty
INT 		iBathroomPedsLoop
INT 		iBathroomRoomKey
INT			iNumPedsNearBathroom						// Number of peds currently in aPedsNearBathroom
CONST_INT 	PEDS_NEAR_BATHROOM_SIZE	8					// Used for array aPedsNearBathroom

BOOL 		bPlayerAloneInBathroom
BOOL 		bBathroomDoorLocked
BOOL 		bAssetLoadingStuck
BOOL 		bHasSwitchedToExitCam 	= FALSE 
BOOL 		bClothedPlayerYacht 	= FALSE
BOOL 		BnetworkInitDone 		= FALSE

SCRIPT_TIMER 			showerExitCam
SCRIPT_TIMER 			stAssetLoadingTimer
PED_INDEX				aPedsNearBathroom[PEDS_NEAR_BATHROOM_SIZE]	// Array of peds from GET_PEDS_NEARBY_PED
INTERIOR_INSTANCE_INDEX apartmentInterior

//***********ENUMS***********
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_ENTER_TO_IDLE_ANIM,
	TS_CHECK_ENTER_TO_IDLE_ANIM,
	TS_CHOOSE_RANDOM_IDLE,
	TS_RUN_IDLE_ANIM,
	TS_CHECK_IDLE_ANIM,
	TS_WAIT_FOR_INPUT,
	SHOW_HEAD,
	START_SHOWER,
	TS_RUN_EXIT_SHOWER_ANIM,
	TS_CHECK_EXIT_SHOWER_ANIM,
	TS_RUN_EXIT_TO_GAME_ANIM,
	TS_CHECK_EXIT_TO_GAME_ANIM
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

ENUM CAM_STATE 
	
	CS_WAIT_TO_START = 0,
	CS_ENTER_WIDE, 
	CS_LOOK_SHOWER,
	CS_ABOVE_SHOWER,
	CS_TURN_OFF_WIDE,
	CS_EXIT_FRAME_WATCH_STEAM,
	CS_EXIT_SHOWER,
	CS_NULL
	
ENDENUM
CAM_STATE eCamState = CS_NULL

//***********CONSTANTS***********
CONST_INT		MAX_SING_LEVEL		300
CONST_INT		RP_AWARD			20
CONST_INT		iMaxShower  		(1000*60*2)
CONST_INT		ci_ABORT	        0

//***********VARIABLES***********
INT 			iIdleAnim
INT 			iShowerSceneTwo
INT 			startTimeShowering
INT 			camTime

PTFX_ID			mWaterEffect
PTFX_ID			mSteamEffect

CAMERA_INDEX  	mCam

STRING 			sAnimDict
STRING 			sEnterAnim
STRING 			sEnterToIdle
STRING 			sIdleAnimA
STRING			sIdleAnimB
STRING 			sIdleAnimC
STRING 			sIdleAnimD
STRING			sIdleToExit
STRING 			sEnterAnimDoor
STRING 			sExitAnimDoor
STRING 			sChosenIdle
STRING 			sSingPrompt
STRING 			sSoundBank01		= "dlc_EXEC1/MP_APARTMENT_SHOWER_01"

SCRIPT_TIMER 	showerStripTimer
SCRIPT_TIMER 	showerVFXTimer

BOOL bInitOffsets
BOOL bFirstTimeShower 	= TRUE
BOOL bClothesStream 	= FALSE
BOOL bDoorAudio 		= FALSE

// PC CONTROL FOR SHOWER
BOOL bPCCOntrolsSetup 		= FALSE
BOOL bForceCamChange	 	= FALSE
BOOL bGMemberBeforeShower 	= FALSE

//***********PROCEDURES & FUNCTIONS***********
PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//Purpose: Checks for player gang priviliges
//To be run before and after the shower to prevent: url:bugstar:2684443
PROC CHECK_GANG_STATUS_FOR_OUTFIT(BOOL bBeforeShower)

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)	
	AND bBeforeShower
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED][GB_SHOWER] Player is a gang member or boss before entering shower")
		bGMemberBeforeShower = TRUE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Dresses the player
PROC DRESS_PLAYER()

	IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)	
	AND bGMemberBeforeShower
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED][GB_SHOWER] SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE")
		SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DRESS_PLAYER CALLED ")
		
		SET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
			
		IF HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
			FINALIZE_HEAD_BLEND(PLAYER_PED_ID()) 
		ENDIF
		
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED][Clothes] UPDATE_TATOOS_MP, block torso decals = FALSE")
		UPDATE_TATOOS_MP(PLAYER_PED_ID())
	ENDIF
	
ENDPROC

PROC LOCK_BATHROOM_DOOR()
	DOOR_SYSTEM_SET_OPEN_RATIO(MP_MED_APT_BATH_DOOR, 0.0, FALSE, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(MP_MED_APT_BATH_DOOR, DOORSTATE_LOCKED, FALSE, TRUE)
	g_showerGlobals.bDoorLocked = TRUE					
ENDPROC

PROC UNLOCK_BATHROOM_DOOR()
	DOOR_SYSTEM_SET_DOOR_STATE(MP_MED_APT_BATH_DOOR, DOORSTATE_UNLOCKED, FALSE, TRUE)
	g_showerGlobals.bDoorLocked = FALSE
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_SHOWER_ACTIVITY(BOOL bCleanupFromSpawn = FALSE)
	IF NOT bCleanupFromSpawn
		RESET_NET_TIMER(showerStripTimer)
		RESET_NET_TIMER(showerVFXTimer)	
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script Cleaning up...")
		bDoorAudio = FALSE
		g_showerGlobals.bPauseSkyCamForSpawn 	= FALSE
		// Clear the help message
		IF NOT IS_STRING_NULL_OR_EMPTY(sSingPrompt)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingPrompt)	
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF g_showerGlobals.bDoorLocked
			UNLOCK_BATHROOM_DOOR()
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN2")
			CLEAR_HELP()
		ENDIF
		
		//Clear the peds tasks in case we are in the shower at this point. 
		IF g_showerGlobals.bShowering = TRUE
			g_showerGlobals.bShowering = FALSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script - we were showering when cleanup was called")
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			IF g_showerGlobals.bSetUpAnimDict
				
				g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
				
				IF DOES_ENTITY_EXIST(mTrigger)
					IF DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, g_showerGlobals.iScene, sExitAnimDoor, sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
					ENDIF
					SET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene, 1.0)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] g_showerGlobals.bTakenClothesOff: ", g_showerGlobals.bTakenClothesOff)
			#ENDIF
			IF NOT g_showerGlobals.bRestoredClothes
			AND g_showerGlobals.bTakenClothesOff
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DRESS_PLAYER: ")
				#ENDIF	
				DRESS_PLAYER()
			ENDIF
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		ENDIF
		
		// Unload audio
		IF g_showerGlobals.bShowerAudioLoaded
			RELEASE_AMBIENT_AUDIO_BANK()
			IF g_showerGlobals.iWaterSoundID != -1
				RELEASE_SOUND_ID(g_showerGlobals.iWaterSoundID)
			ENDIF
			PRINTLN("[SH_MED]obShowerMed: Releasing Audio")
			g_showerGlobals.bShowerAudioLoaded = FALSE
		ENDIF
			
		// Remove animation dictionary
		IF g_showerGlobals.bSetUpAnimDict
			REMOVE_ANIM_DICT(sAnimDict)
		ENDIF
			
		// Remove camera
		IF DOES_CAM_EXIST(mCam)
			DESTROY_CAM(mCam)
		ENDIF
		
		CLEANUP_PC_CONTROLS()
		
		IF bSafehouseSetControlOff
		AND IS_SKYSWOOP_AT_GROUND()
		AND g_iSimpleInteriorState != SIMPLE_INT_STATE_LIMO_SERVICE_EXIT
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF					
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
		ENDIF
			
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
		ENDIF
		
		g_showerGlobals.bInteriorForceCleanup 		= FALSE
		g_showerGlobals.bInteriorForceCleanupToGar 	= FALSE
		g_showerGlobals.bInteriorForceCleanupNoFade = FALSE
		
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_CASINO_APT
		AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			SET_OBJECT_AS_NO_LONGER_NEEDED(mTrigger)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script Cleaning up from multiple shower scripts running...")
	ENDIF
	
	//Let the interior script know that we're cleaning up
	g_showerGlobals.bShowerScriptJustRunCleanup = TRUE
	
	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Remove the players clothes
PROC STRIP_PLAYER()
	
	CHECK_GANG_STATUS_FOR_OUTFIT(TRUE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] shower - taking clothes off")
	REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
	
	// Remember what clothes ped is wearing. (Removing this call here for yacht apartments to prevent issues when dressing the player)
	IF iBaseProperty != PROPERTY_YACHT_APT_1_BASE
		GET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
	ENDIF
				
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 15, 0)
	
	IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO) != 15
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 15, 0)
	ENDIF
	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)		// Store the mask
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)		// Remove crew emblem
	
	IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET) != 5
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 5, 0)
	ENDIF
	
	// After removing mask, restore the hair...
	IF IS_SAFE_TO_RESTORE_SAVED_HAIR_MP(PLAYER_PED_ID(), eReturnItem) 
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, eReturnItem, FALSE) 
	ENDIF
		
	CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
	
	IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					
		IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG) != 14	
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 14, 0)
		ENDIF						

	ELSE
		
		IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG) != 15
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 15, 0)
		ENDIF
		
	ENDIF
	
	IF HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
		FINALIZE_HEAD_BLEND(PLAYER_PED_ID()) 
	ENDIF
	
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED][Clothes] UPDATE_TATOOS_MP, block torso decals = TRUE")
	UPDATE_TATOOS_MP(PLAYER_PED_ID(),TRUE)
	
	g_showerGlobals.bTakenClothesOff = TRUE
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED][Clothes] Just stripped: g_showerGlobals.bTakenClothesOff:", g_showerGlobals.bTakenClothesOff)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Starts the shower PTFX - water and steam
PROC START_SHOWER_PTFX()
	
	mWaterEffect = START_PARTICLE_FX_LOOPED_AT_COORD("ent_amb_shower", g_showerGlobals.vShowerHeadPos, g_showerGlobals.vShowerHeadHead, 1, FALSE, FALSE, FALSE, TRUE)
	mSteamEffect = START_PARTICLE_FX_LOOPED_AT_COORD("ent_amb_shower_steam", g_showerGlobals.vSteamPos, g_showerGlobals.vSteamHead, 1, FALSE, FALSE, FALSE, TRUE)
	
	IF  GET_PROPERTY_SIZE_TYPE(g_showerGlobals.iCurProperty) = PROP_SIZE_TYPE_SMALL_APT
	OR  GET_PROPERTY_SIZE_TYPE(g_showerGlobals.iCurProperty) = PROP_SIZE_TYPE_MED_APT
		PLAY_SOUND_FROM_ENTITY(g_showerGlobals.iWaterSoundID, "GTAO_MP_APARTMENT_SHOWER_PLASTIC_MASTER", PLAYER_PED_ID())
	ELSE
		PLAY_SOUND_FROM_ENTITY(g_showerGlobals.iWaterSoundID, "MP_APARTMENT_SHOWER_MASTER", PLAYER_PED_ID())
	ENDIF
	
	g_showerGlobals.bStartedPtfx = TRUE
	
ENDPROC

/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_SHOWER()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN FALSE
	ELSE
		IF NOT g_showerGlobals.bSetUpAnimDict
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER ANIMS LOADED")
			g_showerGlobals.bSetUpAnimDict = TRUE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_SHOWER_AUDIO_LOADED()
	
	IF g_showerGlobals.bShowerAudioLoaded
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER MID - AUDIO IS READY")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER MID - REQUESTING AUDIO")
	
	IF REQUEST_AMBIENT_AUDIO_BANK(sSoundBank01)
		g_showerGlobals.iWaterSoundID 		= GET_SOUND_ID()
		g_showerGlobals.bShowerAudioLoaded 	= TRUE
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER MID - AUDIO NOT READY")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_SHOWER_PTFX_ASSET_LOADED()
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] PTFX Loading..")
	REQUEST_PTFX_ASSET()
	
	IF HAS_PTFX_ASSET_LOADED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_YACHT_ANIM_DICTS()
	IF NOT IS_PLAYER_FEMALE()
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script - init male shower anims")
		
		sAnimDict 			= "ANIM@MP_YACHT@SHOWER@MALE@"
		sEnterAnim			= "male_shower_undress_&_turn_on_water"
		sEnterToIdle		= "male_shower_enter_into_idle"
		sIdleAnimA			= "male_shower_idle_a"
		sIdleAnimB			= "male_shower_idle_b"
		sIdleAnimC			= "male_shower_idle_c"
		sIdleAnimD			= "male_shower_idle_d"
		sIdleToExit			= "Male_Shower_Exit_To_Idle"
		
		sEnterAnimDoor		= "Male_Shower_Undress_&_Turn_On_Water_PROP_DOOR"
		sExitAnimDoor		= "Male_Shower_Exit_To_Idle_PROP_DOOR"
		
		g_showerGlobals.fNakedPhase			= 0.5
		g_showerGlobals.fNonNakedPhase		= 0.95
		g_showerGlobals.fPtfxOnPhase		= 0.99
		g_showerGlobals.fPtfxOffPhase		= 0.075
		
		g_showerGlobals.fLookShowerPhase	= 0.260
		g_showerGlobals.fAboveShowerPhase	= 0.5
		//g_showerGlobals.fTurnOffWidePhase	= 0.5
		g_showerGlobals.fExitFramePhase		= 0.3
		g_showerGlobals.fExitShowerPhase	= 0.79
				
	ELSE
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script - init male shower anims")
		
		sAnimDict 			= "ANIM@MP_YACHT@SHOWER@FEMALE@"
		sEnterAnim			= "shower_undress_&_turn_on_water"
		sEnterToIdle		= "shower_enter_into_idle"
		sIdleAnimA			= "shower_idle_a"
		sIdleAnimB			= "shower_idle_b"
		sIdleAnimC			= "shower_idle_b"
		sIdleAnimD			= "shower_idle_a"
		sIdleToExit			= "shower_Exit_To_Idle"
		
		sEnterAnimDoor		= "Shower_Undress_&_Turn_On_Water_PROP_DOOR"
		sExitAnimDoor		= "Shower_Exit_To_Idle_PROP_DOOR"
		
		g_showerGlobals.fNakedPhase			= 0.5
		g_showerGlobals.fNonNakedPhase		= 0.95
		g_showerGlobals.fPtfxOnPhase		= 0.384
		g_showerGlobals.fPtfxOffPhase		= 0.133
		
		g_showerGlobals.fLookShowerPhase	= 0.260
		g_showerGlobals.fAboveShowerPhase	= 0.9
		g_showerGlobals.fExitFramePhase		= 0.3
		g_showerGlobals.fExitShowerPhase	= 0.75
		
	ENDIF
ENDPROC

PROC INIT_GENDER_SPECIFICS()
	
	IF NOT IS_PLAYER_FEMALE()
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script - init male shower anims")
		
		sAnimDict 			= "mp_safehouseshower@male@"
		sEnterAnim			= "male_shower_undress_&_turn_on_water"
		sEnterToIdle		= "male_shower_enter_into_idle"
		sIdleAnimA			= "male_shower_idle_a"
		sIdleAnimB			= "male_shower_idle_b"
		sIdleAnimC			= "male_shower_idle_c"
		sIdleAnimD			= "male_shower_idle_d"
		sIdleToExit			= "Male_Shower_Exit_To_Idle"
		
		sEnterAnimDoor		= "male_shower_undress_&_turn_on_water_door"
		sExitAnimDoor		= "Male_Shower_Exit_To_Idle_Door"
		
		g_showerGlobals.fNakedPhase			= 0.5
		g_showerGlobals.fNonNakedPhase		= 0.55
		g_showerGlobals.fPtfxOnPhase		= 0.833
		g_showerGlobals.fPtfxOffPhase		= 0.25
		
		g_showerGlobals.fLookShowerPhase	= 0.260
		g_showerGlobals.fAboveShowerPhase	= 0.9
		g_showerGlobals.fExitFramePhase		= 0.3
		g_showerGlobals.fExitShowerPhase	= 0.79
		
		IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
		OR iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
			g_showerGlobals.fNonNakedPhase		= 0.89
		ENDIF	
				
	ELSE
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower Script - init male shower anims")
		
		sAnimDict 			= "mp_safehouseshower@female@"
		sEnterAnim			= "shower_undress_&_turn_on_water"
		sEnterToIdle		= "shower_enter_into_idle"
		sIdleAnimA			= "shower_idle_a"
		sIdleAnimB			= "shower_idle_b"
		sIdleAnimC			= "shower_idle_b"
		sIdleAnimD			= "shower_idle_a"
		sIdleToExit			= "shower_Exit_To_Idle"
		
		sEnterAnimDoor		= "shower_undress_&_turn_on_water_door"
		sExitAnimDoor		= "shower_Exit_To_Idle_Door"
		
		g_showerGlobals.fNakedPhase			= 0.5
		g_showerGlobals.fNonNakedPhase		= 0.5
		g_showerGlobals.fPtfxOnPhase		= 0.384
		g_showerGlobals.fPtfxOffPhase		= 0.166
		
		g_showerGlobals.fLookShowerPhase	= 0.260
		g_showerGlobals.fAboveShowerPhase	= 0.9
		g_showerGlobals.fExitFramePhase		= 0.3
		g_showerGlobals.fExitShowerPhase	= 0.75
		
		IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A 
		OR iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
			g_showerGlobals.fNonNakedPhase		= 0.89
		ENDIF
		
	ENDIF
	
ENDPROC

PROC HANDLE_SHOWER_CAM()
	
	SWITCH eCamState
		
		CASE CS_WAIT_TO_START
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sEnterAnim, ANIM_SYNCED_SCENE) 
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_WAIT_TO_START")
				eCamState = CS_LOOK_SHOWER
			ENDIF
		BREAK
		
		CASE CS_LOOK_SHOWER
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sEnterAnim, ANIM_SYNCED_SCENE) 
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fLookShowerPhase
						SET_CAM_PARAMS(mCam, g_showerGlobals.vLookShowerCamPos, g_showerGlobals.vLookShowerCamHead, g_showerGlobals.fLookShowerCamFov)
						SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_ABOVE_SHOWER")
						eCamState = CS_ABOVE_SHOWER
					ENDIF
				ENDIF
			ENDIF
				
		BREAK
		
		CASE CS_ABOVE_SHOWER
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sEnterAnim, ANIM_SYNCED_SCENE) 
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fAboveShowerPhase
						SET_CAM_PARAMS(mCam, g_showerGlobals.vAboveShowerCamPos, g_showerGlobals.vAboveShowerCamHead, g_showerGlobals.fAboveShowerCamFov)
						SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_TURN_OFF_WIDE")
						eCamState = CS_TURN_OFF_WIDE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_TURN_OFF_WIDE
		
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sIdleToExit, ANIM_SYNCED_SCENE) 
				SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_EXIT_FRAME_WATCH_STEAM")
				eCamState = CS_EXIT_FRAME_WATCH_STEAM			
			ENDIF
			
		BREAK 
		
		CASE CS_EXIT_FRAME_WATCH_STEAM
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sIdleToExit, ANIM_SYNCED_SCENE) 
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fExitFramePhase
						SET_CAM_PARAMS(mCam, g_showerGlobals.vExitFrameCamPos, g_showerGlobals.vExitFrameCamHead, g_showerGlobals.fExitFrameCamFov)
						SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_EXIT_SHOWER")
						eCamState = CS_EXIT_SHOWER
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_EXIT_SHOWER
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sIdleToExit, ANIM_SYNCED_SCENE) 
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fExitShowerPhase
						SET_CAM_PARAMS(mCam, g_showerGlobals.vExitCamPos, g_showerGlobals.vExitCamHead, g_showerGlobals.fExitCamFov)
						SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - exit cam")
						eCamState = CS_NULL
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_NULL
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC CHECK_FOR_SINGING()
	
	IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
	AND NOT bEarnedEnoughRPSinging
		DRAW_GENERIC_METER(g_showerGlobals.iSingingDuration, MAX_SING_LEVEL, "SING_LABEL")
		
		FLOAT fVolume = NETWORK_GET_PLAYER_LOUDNESS(PLAYER_ID())
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] NETWORK_GET_PLAYER_LOUDNESS is ", fVolume)
		
		fVolume -= 0.35
		IF fVolume < 0
		      fVolume = 0
		ENDIF
		fVolume = (fVolume*4.0)

		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Player volume is ", fVolume)
		
		IF fVolume > 0.0

			g_showerGlobals.iSingingDuration++
			
			IF g_showerGlobals.iSingingDuration >= MAX_SING_LEVEL
				// Award RP
				SET_MP_INT_CHARACTER_STAT(MP_STAT_SING_IN_SHOWER, GET_MP_INT_CHARACTER_STAT(MP_STAT_SING_IN_SHOWER)+1)
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_SING_REWARD", XPTYPE_SKILL, XPCATEGORY_COMPLETED_SHOWER, g_sMPTunables.iSING_IN_SHOWER_RP)
				g_showerGlobals.iSingingDuration = 0
				bEarnedEnoughRPSinging = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if the local player is alone in the bathroom and the door is nearly closed
/// RETURNS:
///    True if alone in the bathroom
FUNC BOOL IS_PLAYER_ALONE_IN_AREA()

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], g_showerGlobals.vAreaAPos, g_showerGlobals.vAreaBPos, g_showerGlobals.fBathroomWidth)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	INT iCurProp = GET_PROPERTY_SIZE_TYPE(g_showerGlobals.iCurProperty)
	FLOAT door_open = 0.0
	
	SWITCH iCurProp
		CASE PROP_SIZE_TYPE_SMALL_APT
			door_open = DOOR_SYSTEM_GET_OPEN_RATIO(MP_LOW_APT_BATH_DOOR)
		BREAK
		CASE PROP_SIZE_TYPE_MED_APT
			door_open = DOOR_SYSTEM_GET_OPEN_RATIO(MP_MED_APT_BATH_DOOR)
		BREAK
			
		CASE PROP_SIZE_TYPE_LARGE_APT
			door_open = DOOR_SYSTEM_GET_OPEN_RATIO(MP_HIGH_APT_BATH_DOOR)
		BREAK
	ENDSWITCH
	
	IF (ABSF(door_open) > 0.06)
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC

PROC SET_DAY_TO_CURRENT_TIME()
	currentDayShowering 	= GET_TIMEOFDAY_DAY(GET_CURRENT_TIMEOFDAY()) 
	currentMonthShowering 	= GET_TIMEOFDAY_MONTH(GET_CURRENT_TIMEOFDAY())
	currentYearShowering 	= GET_TIMEOFDAY_YEAR(GET_CURRENT_TIMEOFDAY())
	
	PRINTLN("[SH_MED] Current day ", currentDayShowering)
	PRINTLN("[SH_MED] Current month ", ENUM_TO_INT(currentMonthShowering))
	PRINTLN("[SH_MED] Current year ", currentYearShowering)
ENDPROC

FUNC BOOL IS_DAY_THE_SAME()
	INT newDay, newYear
	MONTH_OF_YEAR newMonth
	
	newDay 		= GET_TIMEOFDAY_DAY(GET_CURRENT_TIMEOFDAY())
	newMonth 	= GET_TIMEOFDAY_MONTH(GET_CURRENT_TIMEOFDAY())
	newYear 	= GET_TIMEOFDAY_YEAR(GET_CURRENT_TIMEOFDAY())
	
	IF newDay = currentDayShowering
	AND newMonth = currentMonthShowering
	AND newYear = currentYearShowering
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SHOWER_VARIABLES_AFTER_SHOWER()
	// Final anim has finished
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	
	STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 1.0)
	
	PRINTLN("[SH_MED] CLEANUP_SHOWER_VARIABLES_AFTER_SHOWER: NET_SET_PLAYER_CONTROL 6 (TS_CHECK_EXIT_SHOWER_ANIM)")
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	bDoorAudio 								= FALSE
	bSafehouseSetControlOff 				= FALSE
	g_showerGlobals.bRestoredClothes		= FALSE
	g_showerGlobals.bWashedBloodOff			= FALSE
	g_showerGlobals.bTakenClothesOff		= FALSE
	g_showerGlobals.bStartedPtfx			= FALSE
	g_showerGlobals.bStoppedPtFX			= FALSE
	g_showerGlobals.bShowering 				= FALSE
	g_showerGlobals.bInTransition			= FALSE
	g_showerGlobals.bPauseSkyCamForSpawn 	= FALSE
	iPersonalAptActivity 					= ci_APT_ACT_IDLE	
	g_showerGlobals.iSingingDuration 		= 0	
	
	RESET_NET_TIMER(showerStripTimer)
	RESET_NET_TIMER(showerVFXTimer)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_PLAYER_OUT_OF_RANGE")
	
	eTriggerState = TS_PLAYER_OUT_OF_RANGE
ENDPROC

/// PURPOSE:
///    Runs the alternate logic used when spawning into an apartment/office/yacht
PROC RUN_SHOWER_SPAWN()
	IF NOT bAssetLoadingStuck
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] RUN_SHOWER_SPAWN: Asset loading check: FALSE. Update stage: TS_PLAYER_OUT_OF_RANGE")
	ENDIF
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF g_showerGlobals.bShowering = FALSE
		//Get the players outfit here if were on the yacht to prevent problems dressing the player after showering
		IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
			GET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
		ENDIF
		
		IF NOT g_showerGlobals.bTakenClothesOff
			STRIP_PLAYER()
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(showerStripTimer)
			START_NET_TIMER(showerStripTimer,TRUE)
		ENDIF
		
		g_showerGlobals.bShowering 		= TRUE
		g_showerGlobals.bInTransition 	= TRUE
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] RUN_SHOWER_SPAWN STRIPPING PLAYER")
		
	ELIF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
	AND bClothesStream = FALSE
	OR HAS_NET_TIMER_EXPIRED(showerStripTimer, 10000, TRUE)
	AND g_showerGlobals.bShowering = TRUE
	
		bClothesStream = TRUE
		g_showerGlobals.bPauseSkyCamForSpawn = FALSE		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] RUN_SHOWER_SPAWN: HAVE_ALL_STREAMING_REQUESTS_COMPLETED = TRUE & Player stripped")
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SH_MED] RUN_SHOWER_SPAWN TS_RUN_EXIT_ANIM: IS_SKYSWOOP_IN_SKY: ", IS_SKYSWOOP_IN_SKY(), " IS_PLAYER_SWITCH_IN_PROGRESS: ", IS_PLAYER_SWITCH_IN_PROGRESS(), " IS_TRANSITION_ACTIVE: ", IS_TRANSITION_ACTIVE())		
	#ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(showerVFXTimer)
		START_NET_TIMER(showerVFXTimer,TRUE)
	ENDIF
	
	IF bClothesStream = TRUE
	AND NOT IS_SKYSWOOP_IN_SKY()
	AND NOT IS_TRANSITION_ACTIVE()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	OR HAS_NET_TIMER_EXPIRED(showerVFXTimer, 17500, TRUE)
	
		#IF IS_DEBUG_BUILD
		IF HAS_NET_TIMER_EXPIRED(showerVFXTimer, 17500, TRUE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] RUN_SHOWER_SPAWN: TS_PLAYER_OUT_OF_RANGE: SKIPPING INTERIOR CHECK waiting too long for skycam swoop ")
		ENDIF
		#ENDIF
	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] RUN_SHOWER_SPAWN: TS_PLAYER_OUT_OF_RANGE: START_SHOWER_PTFX")
		START_SHOWER_PTFX()
		IF NOT DOES_CAM_EXIST(mCam)
			mCam = CREATE_CAMERA()
		ENDIF		

		IF DOES_CAM_EXIST(mCam)	
			SET_CAM_ACTIVE(mCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_CAM_PARAMS(mCam, g_showerGlobals.vAboveShowerCamPos, g_showerGlobals.vAboveShowerCamHead, g_showerGlobals.fAboveShowerCamFov)			
			SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
			
			PRINTLN("[SH_MED] RUN_SHOWER_SPAWN: NET_SET_PLAYER_CONTROL 1 (SPAWNING - TS_PLAYER_OUT_OF_RANGE) Cam: - CS_ABOVE_SHOWER")
			
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
			ELSE
				PRINTLN("[SH_MED] RUN_SHOWER_SPAWN: NET_SET_PLAYER_CONTROL 1 - FAILED SKYWOOP NOT AT GROUND, NOT SETTING PLAYER CNTROL TO FALSE")
			ENDIF
			
			bSafehouseSetControlOff = TRUE
			iPersonalAptActivity 	= iActivityID
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())

			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sIdleAnimC, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			
			IF (g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iShowersTakenWithHeadset < 3) 
			AND NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
			AND NOT bEarnedEnoughRPSinging			
				sSingPrompt = "SA_SHWR_OUT2"
			ELSE
				sSingPrompt = "SA_SHWR_OUT"
			ENDIF
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingPrompt)
				PRINT_HELP_FOREVER(sSingPrompt)
			ENDIF
			
			startTimeShowering = GET_GAME_TIMER() 	//Save our game time. 
			SET_DAY_TO_CURRENT_TIME() 				//Set the day.
	
			eTriggerState = TS_WAIT_FOR_INPUT
			RESET_SPAWNING_INTERACTION_GLOBALS()
			
			g_TransitionSpawnData.bSpawnActivityReady = TRUE
			g_TransitionSpawnData.bSpawnActivtyObjectScriptReady = TRUE
			
			PRINTLN("[SH_MED] RUN_SHOWER_SPAWN setting eTriggerState = TS_WAIT_FOR_INPUT")		
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_STRING_LITERAL_SHOWER_ACTIVITY_STAGE(TRIGGER_STATE TriggerState)
	SWITCH TriggerState
		CASE TS_PLAYER_OUT_OF_RANGE			RETURN "TS_PLAYER_OUT_OF_RANGE"
		CASE TS_WAIT_FOR_PLAYER				RETURN "TS_WAIT_FOR_PLAYER"
		CASE TS_RUN_ENTRY_ANIM				RETURN "TS_RUN_ENTRY_ANIM"
		CASE TS_CHECK_ENTRY_ANIM			RETURN "TS_CHECK_ENTRY_ANIM"
		CASE TS_RUN_ENTER_TO_IDLE_ANIM		RETURN "TS_RUN_ENTER_TO_IDLE_ANIM"
		CASE TS_CHECK_ENTER_TO_IDLE_ANIM	RETURN "TS_CHECK_ENTER_TO_IDLE_ANIM"
		CASE TS_CHOOSE_RANDOM_IDLE			RETURN "TS_CHOOSE_RANDOM_IDLE"
		CASE TS_RUN_IDLE_ANIM				RETURN "TS_RUN_IDLE_ANIM"
		CASE TS_CHECK_IDLE_ANIM				RETURN "TS_CHECK_IDLE_ANIM"
		CASE TS_WAIT_FOR_INPUT				RETURN "TS_WAIT_FOR_INPUT"
		CASE SHOW_HEAD						RETURN "SHOW_HEAD"
		CASE START_SHOWER					RETURN "START_SHOWER"
		CASE TS_RUN_EXIT_SHOWER_ANIM		RETURN "TS_RUN_EXIT_SHOWER_ANIM"
		CASE TS_CHECK_EXIT_SHOWER_ANIM		RETURN "TS_CHECK_EXIT_SHOWER_ANIM"
		CASE TS_RUN_EXIT_TO_GAME_ANIM		RETURN "TS_RUN_EXIT_TO_GAME_ANIM"
		CASE TS_CHECK_EXIT_TO_GAME_ANIM		RETURN "TS_CHECK_EXIT_TO_GAME_ANIM"
	ENDSWITCH
	
	RETURN "STage Unknown!"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Updates shower activity
///    
PROC UPDATE_SHOWER_ACTIVITY()
	
	Bool quitShower = FALSE
	BOOL canIShower = TRUE
	INT iChosenIdle

	IF eTriggerState != TS_PLAYER_OUT_OF_RANGE
	AND eTriggerState != TS_WAIT_FOR_PLAYER
		SET_TRANSITION_SESSIONS_ON_CALL_DISABLED_THIS_FRAME_AFTER_RESTART()
	ENDIF

	#IF IS_DEBUG_BUILD
	IF ENUM_TO_INT(eTriggerState) > 0
		IF (GET_FRAME_COUNT() % 120) = 0  
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: eTriggerState: ", DEBUG_GET_STRING_LITERAL_SHOWER_ACTIVITY_STAGE(eTriggerState))
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: stage: TS_PLAYER_OUT_OF_RANGE bPauseSkyCamForSpawn: ", g_showerGlobals.bPauseSkyCamForSpawn)
	ENDIF
	#ENDIF
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
			
			IF iPersonalAptActivity = iActivityID
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_SHOWER
				RUN_SHOWER_SPAWN()
			ELSE
			
				IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID)	AND (IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_BATHROOM_LOCKED) OR IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_BATHROOM_LOCKED_2))
				OR IS_PROPERTY_YACHT_APARTMENT(g_showerGlobals.iCurProperty) AND MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID)/*(ci_APT_ACT_SHOWER)*/ AND bBathroomDoorLocked = TRUE
					
					IF NOT IS_DAY_THE_SAME()
						PRINTSTRING("[SH_MED][Paul] - day change!") PRINTNL()
						totalTimeShowering = 0
						bEarnedEnoughRPSinging = FALSE
						PRINTLN("[SH_MED] bEarnedEnoughRPSinging = ", bEarnedEnoughRPSinging)
					ENDIF
					
					IF bInPropertyOwnerNotPaidLastUtilityBill
						IF IS_TRIGGER_AREA_OK(g_showerGlobals.vTriggerPos, g_showerGlobals.vTriggerSize)
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPSH_BILL")
								PRINT_HELP_FOREVER("MPSH_BILL")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPSH_BILL")
								CLEAR_HELP(TRUE)
							ENDIF
						ENDIF
						
						CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED] Can't shower bInPropertyOwnerNotPaidLastUtilityBill is true")
					ELIF totalTimeShowering  > iMaxShower
						
						//Showered too much today.
						IF IS_TRIGGER_AREA_OK(g_showerGlobals.vTriggerPos, g_showerGlobals.vTriggerSize)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER: totalTimeShowering: ", totalTimeShowering, ", vTriggerPos = ", g_showerGlobals.vTriggerPos)
							
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN2")
								PRINT_HELP_FOREVER("SA_SHWR_IN2")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN2")
								CLEAR_HELP(TRUE)
							ENDIF
						ENDIF
						
					ELIF IS_TRIGGER_AREA_OK(g_showerGlobals.vTriggerPos, g_showerGlobals.vTriggerSize)
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
						
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN")
						AND NOT IS_BROWSER_OPEN()
							PRINT_HELP_FOREVER("SA_SHWR_IN")
						ENDIF
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] POD: Shower: - TS_WAIT_FOR_PLAYER")				
						eTriggerState = TS_WAIT_FOR_PLAYER					
					ELSE
						IF NOT IS_TRIGGER_AREA_OK(g_showerGlobals.vTriggerPos, g_showerGlobals.vTriggerSize)
							CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED] IS_TRIGGER_AREA_OK = false, vTriggerPos = ", g_showerGlobals.vTriggerPos, ", vTriggerSize = ", g_showerGlobals.vTriggerSize)
						ENDIF
					ENDIF
				ELSE
					IF NOT MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] ob_mp_shower_med: NOT MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLEcan't trigger shower.")
					ENDIF
					IF NOT IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_BATHROOM_LOCKED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] ob_mp_shower_med: NOT IS_BIT_SET ci_APT_ACT_BATHROOM_LOCKED")
					ENDIF
					IF bBathroomDoorLocked = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TS_WAIT_FOR_PLAYER: bBathroomDoorLocked = FALSE")
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] ob_mp_shower_med: Player can't trigger shower.")
				ENDIF
				
			ENDIF
			
			bFirstTimeShower 			= TRUE
			g_showerGlobals.bLastCam 	= FALSE
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
									
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(g_showerGlobals.vTriggerPos, g_showerGlobals.vTriggerSize)
					IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID)
					AND (IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_BATHROOM_LOCKED)
					OR IS_BIT_SET(iBS_AptActivityInUse, ci_APT_ACT_BATHROOM_LOCKED_2))
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					#IF FEATURE_GTAO_MEMBERSHIP
					AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
					#ENDIF
					AND canIShower = TRUE 											//Make sure we have not run out of time.
					OR IS_PROPERTY_YACHT_APARTMENT(g_showerGlobals.iCurProperty)
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					AND canIShower = TRUE 											//Make sure we have not run out of time.
					AND bBathroomDoorLocked = TRUE
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							// Call this once for on call
							SET_TRANSITION_SESSIONS_RESTART_ON_CALL()
							SET_TRANSITION_SESSIONS_ON_CALL_DISABLED_THIS_FRAME_AFTER_RESTART()
	
							//Get the players outfit here if were on the yacht to prevent problems dressing the player after showering
							IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
								GET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
							ENDIF
													
							IF (g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iShowersTakenWithHeadset < 3) 
							AND NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
							AND NOT bEarnedEnoughRPSinging
								sSingPrompt = "SA_SHWR_OUT2"
								g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iShowersTakenWithHeadset++
							ELSE
								sSingPrompt = "SA_SHWR_OUT"
							ENDIF
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN")
								CLEAR_HELP()
							ENDIF
							
							eCamState = CS_WAIT_TO_START
							
							CLEAR_AREA_OF_PROJECTILES(g_showerGlobals.vTriggerPos, 3.0)
							PRINTLN("[SH_MED] SHOWER: NET_SET_PLAYER_CONTROL 2 (TS_WAIT_FOR_PLAYER)")
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
							bSafehouseSetControlOff = TRUE
					
							iPersonalAptActivity = iActivityID
							
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							SETUP_PC_CONTROLS()
						
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] POD: Shower: - TS_RUN_ENTRY_ANIM")
							
							IF NOT DOES_CAM_EXIST(mCam)
								mCam = CREATE_CAMERA()
							ENDIF
				
							IF DOES_CAM_EXIST(mCam)								
								SET_CAM_ACTIVE(mCam, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
									SET_CAM_PARAMS(mCam, g_showerGlobals.vEnterWideCamPos, g_showerGlobals.vEnterWideCamHead, g_showerGlobals.fEnterWideCamFov)
								ELSE
									SET_CAM_PARAMS(mCam, g_showerGlobals.vLookShowerCamPos, g_showerGlobals.vLookShowerCamHead, g_showerGlobals.fLookShowerCamFov)							
								ENDIF
								CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - LOOK CAM")
								SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
							ENDIF
							
							g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sEnterAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
							
							IF DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, g_showerGlobals.iScene, sEnterAnimDoor, sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
							ENDIF
							
							eTriggerState = TS_CHOOSE_RANDOM_IDLE
						ENDIF
					ELSE
						IF NOT MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID)//(ci_APT_ACT_SHOWER)	
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TS_WAIT_FOR_PLAYER: MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE =  FALSE")
						ENDIF
						
						IF IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TS_WAIT_FOR_PLAYER: IS_PLAYER_ON_PAUSE_MENU = TRUE")
						ENDIF
						
						IF canIShower = FALSE //Make sure we have not run out of time.
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TS_WAIT_FOR_PLAYER: canIShower = FALSE")
						ENDIF
						
						IF bBathroomDoorLocked = FALSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TS_WAIT_FOR_PLAYER: bBathroomDoorLocked = FALSE")
						ENDIF
					ENDIF
						
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_SHWR_IN")
						CLEAR_HELP(TRUE)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_PLAYER_OUT_OF_RANGE")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - vTriggerSize: ", g_showerGlobals.vTriggerSize, ", vTriggerPos: ", g_showerGlobals.vTriggerPos)
					
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_PLAYER_OUT_OF_RANGE: CAN_PLAYER_START_CUTSCENE = FALSE")
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
						
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sEnterAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
			IF DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, g_showerGlobals.iScene, sEnterAnimDoor, sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			// Create camera 
			IF NOT DOES_CAM_EXIST(mCam)
				mCam = CREATE_CAMERA()
			ENDIF
			
			IF DOES_CAM_EXIST(mCam)
				SET_CAM_ACTIVE(mCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_PARAMS(mCam, g_showerGlobals.vEnterWideCamPos, g_showerGlobals.vEnterWideCamHead, g_showerGlobals.fEnterWideCamFov)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - ENTER CAM")
				SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
			ENDIF
			
			g_showerGlobals.bShowering 	= TRUE
			bForceCamChange 			= FALSE			
			startTimeShowering 			= GET_GAME_TIMER() //Save our game time. 
			
			SET_DAY_TO_CURRENT_TIME() //Set the day.
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_CHECK_ENTRY_ANIM")
			eTriggerState = TS_CHECK_ENTRY_ANIM

		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			IF g_showerGlobals.iScene < 0
				eTriggerState = TS_RUN_EXIT_SHOWER_ANIM
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_RUN_EXIT_SHOWER_ANIM - synced scene ID not valid! - A")
			ELSE
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
				OR GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > 0.99
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_RUN_ENTER_TO_IDLE_ANIM")
					eTriggerState = TS_RUN_ENTER_TO_IDLE_ANIM
				ELSE
					
					IF NOT g_showerGlobals.bStartedPtfx
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fPtfxOnPhase
							START_SHOWER_PTFX()
						ENDIF
					ENDIF
					
					IF NOT g_showerGlobals.bTakenClothesOff
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fNakedPhase
							STRIP_PLAYER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTER_TO_IDLE_ANIM
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()
				
			g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sEnterToIdle, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			
			IF NOT g_showerGlobals.bWashedBloodOff
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())	
				g_showerGlobals.bWashedBloodOff = TRUE
			ENDIF
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingPrompt)
				PRINT_HELP_FOREVER(sSingPrompt)
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] - TS_CHECK_ENTER_TO_IDLE_ANIM")
			eTriggerState = TS_CHOOSE_RANDOM_IDLE
			
		BREAK
				
		CASE TS_CHOOSE_RANDOM_IDLE
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()
			
			iChosenIdle = iIdleAnim
			
			IF iChosenIdle = 0
				sChosenIdle = sIdleAnimA
			ELIF iChosenIdle = 1
				sChosenIdle = sIdleAnimB
			ELIF iChosenIdle = 2
				sChosenIdle = sIdleAnimC
			ELSE
				sChosenIdle = sIdleAnimD
			ENDIF
			
			iIdleAnim++
			IF iIdleAnim > 4
				iIdleAnim = 0
			ENDIF
			
			g_showerGlobals.bShowering = TRUE
			
			startTimeShowering = GET_GAME_TIMER() 	//Save our game time. 
			SET_DAY_TO_CURRENT_TIME() 				//Set the day.
				
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_RUN_IDLE_ANIM: iIdleAnim: ", iIdleAnim)
			eTriggerState = TS_RUN_IDLE_ANIM
			
		BREAK
		
		CASE TS_RUN_IDLE_ANIM
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()
								
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_CHECK_IDLE_ANIM")
			PLAY_SOUND_FROM_ENTITY(-1, "MP_APARTMENT_SHOWER_GET_UNDRESSED_MASTER", PLAYER_PED_ID())
		
			camTime 		= GET_GAME_TIMER()
			eTriggerState 	= SHOW_HEAD
			
		BREAK
		
		//Stage to show the player whilst hiding them when they are stripped of clothing during the entry animation
		CASE SHOW_HEAD
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			INT iCamTimer
			iCamTimer = 2000
			
			IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
				iCamTimer = 3750
				SET_CAM_PARAMS(mCam, g_showerGlobals.vEnterWideCamPos, g_showerGlobals.vEnterWideCamHead, g_showerGlobals.fEnterWideCamFov)
			ENDIF
			
			IF ((GET_GAME_TIMER() - camTime) > iCamTimer)
			
				IF NOT IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
					IF iBaseProperty != PROPERTY_STILT_APT_5_BASE_A
					AND iBaseProperty != PROPERTY_STILT_APT_1_BASE_B
						START_SHOWER_PTFX()
					ENDIF
					IF NOT g_showerGlobals.bTakenClothesOff
						STRIP_PLAYER()
					ENDIF
				ENDIF
				
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				
				camTime 		= GET_GAME_TIMER()
				eTriggerState 	= START_SHOWER
			ENDIF
			
		BREAK
		
		CASE START_SHOWER
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			CHECK_FOR_SINGING()			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			IF ((GET_GAME_TIMER() - camTime) > 1500)
				eTriggerState = TS_WAIT_FOR_INPUT
				SET_CAM_PARAMS(mCam, g_showerGlobals.vAboveShowerCamPos, g_showerGlobals.vAboveShowerCamHead, g_showerGlobals.fAboveShowerCamFov)
				
				IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
				OR iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
				OR iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
					START_SHOWER_PTFX()
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - CS_ABOVE_SHOWER")
				
				IF (bFirstTimeShower)
					MP_PROP_OFFSET_STRUCT SStilt_APT_Offset
					IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
						SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << 122.499, 551.150, 179.706 >>, << 0.000, -0.000, -143.500 >>)
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Offset.vLoc, SStilt_APT_Offset.vRot)
					ELIF iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
						SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << -167.322, 490.492, 133.015 >>, << 0.000, -0.000, -107.000 >>)
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Offset.vLoc, SStilt_APT_Offset.vRot)
					ELSE
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
					ENDIF
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sEnterToIdle, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					
					//Play the animation from a different starting point for female peds in the new stilt apartments
					IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
					OR iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
					OR iBaseProperty = PROPERTY_YACHT_APT_1_BASE
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01 
							SET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene, 0.3)
						ENDIF
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
				bFirstTimeShower = FALSE
				
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingPrompt)
					PRINT_HELP_FOREVER(sSingPrompt)
				ENDIF
			
			//If we're on the yacht strip the player now
			ELIF ((GET_GAME_TIMER() - camTime) > 1250)
			AND iBaseProperty = PROPERTY_YACHT_APT_1_BASE
			AND NOT g_showerGlobals.bTakenClothesOff
				STRIP_PLAYER()
			ENDIF
		BREAK
		
				
		CASE TS_WAIT_FOR_INPUT	
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			RESET_NET_TIMER(showerVFXTimer)
			RESET_NET_TIMER(showerStripTimer)
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			IF IS_SKYSWOOP_AT_GROUND()
			AND NOT IS_SKYSWOOP_MOVING()
				SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			ENDIF
					
			IF IS_SKYSWOOP_AT_GROUND()
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				PRINTLN("[SH_MED] SHOWER: NET_SET_PLAYER_CONTROL 3 (TS_WAIT_FOR_INPUT)")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
			ENDIF
			IF g_showerGlobals.iScene < 0
				eTriggerState = TS_RUN_EXIT_SHOWER_ANIM
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_RUN_EXIT_SHOWER_ANIM - synced scene ID not valid! - B")
			ELSE
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
				OR GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > 0.99
				AND quitShower = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_CHOOSE_RANDOM_IDLE")
					
					iChosenIdle = iIdleAnim
			
					IF iChosenIdle = 0
						sChosenIdle = sIdleAnimA
					ELIF iChosenIdle = 1
						sChosenIdle = sIdleAnimB
					ELIF iChosenIdle = 2
						sChosenIdle = sIdleAnimC
					ELSE
						sChosenIdle = sIdleAnimD
					ENDIF
					
					iIdleAnim++
					IF iIdleAnim > 4
						iIdleAnim = 0
					ENDIF
					
					MP_PROP_OFFSET_STRUCT SStilt_APT_Idle_Offset
					IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
						SStilt_APT_Idle_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << 122.499, 551.150, 179.706 >>, << 0.000, -0.000, -143.500 >>)
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Idle_Offset.vLoc, SStilt_APT_Idle_Offset.vRot)
					ELIF iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
						SStilt_APT_Idle_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << -167.322, 490.492, 133.015 >>, << 0.000, -0.000, -107.000 >>)
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Idle_Offset.vLoc, SStilt_APT_Idle_Offset.vRot)
					ELSE
						g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
					ENDIF
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sChosenIdle, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ELIF quitShower = TRUE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingprompt)
						CLEAR_HELP()
					ENDIF
					
					//Save our total time.
					totalTimeShowering += (GET_GAME_TIMER() - startTimeShowering)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] - TS_RUN_EXIT_SHOWER_ANIM")
					eTriggerState = TS_RUN_EXIT_SHOWER_ANIM
				ENDIF	
			ENDIF

			IF (NOT IS_DAY_THE_SAME())
				PRINTLN("[SH_MED] - day change!")
				SET_DAY_TO_CURRENT_TIME()
				startTimeShowering = GET_GAME_TIMER() //Reset the timer as we have started a new day.
				totalTimeShowering = 0
				bEarnedEnoughRPSinging = FALSE
			ENDIF
			
			IF ((totalTimeShowering + (GET_GAME_TIMER() - startTimeShowering)) > (iMaxShower)) //10 minutes
				//Quit out.
				PRINTLN("[SH_MED] quitting time.")
				quitShower = TRUE
			ENDIF
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingPrompt)
				PRINT_HELP_FOREVER(sSingPrompt)
			ENDIF
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
			#IF FEATURE_GTAO_MEMBERSHIP
			AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
			#ENDIF
			
				CHECK_FOR_SINGING()
						
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
									
					// Clear the help message
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSingprompt)
						CLEAR_HELP()
					ENDIF
					
					//Save our total time.
					totalTimeShowering += (GET_GAME_TIMER() - startTimeShowering)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_RUN_EXIT_SHOWER_ANIM")
					eTriggerState = TS_RUN_EXIT_SHOWER_ANIM
				ENDIF
			ENDIF

		BREAK
		
		CASE TS_RUN_EXIT_SHOWER_ANIM
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			PRINTLN("[SH_MED] SHOWER: NET_SET_PLAYER_CONTROL 4 (TS_RUN_EXIT_SHOWER_ANIM)")
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			SET_SPAWN_ACTIVITY_OBJECT_SCRIPT_AS_READY(SPAWN_ACTIVITY_SHOWER)

			iPersonalAptActivityState = ci_APT_ACT_STATE_FINISHING
			
			//For stilt apartments play the door anim at a different position to the ped anim
			MP_PROP_OFFSET_STRUCT SStilt_APT_Exit_Offset
			IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
				SStilt_APT_Exit_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << 121.346, 551.242, 179.769 >>, << 0.000, -0.000, -109.500 >>)
				g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Exit_Offset.vLoc, SStilt_APT_Exit_Offset.vRot)
				iShowerSceneTwo = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
			ELIF iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
				SStilt_APT_Exit_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << -168.235, 490.142, 133.040 >>, << 0.000, -0.000, -107.000 >>)
				g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(SStilt_APT_Exit_Offset.vLoc, SStilt_APT_Exit_Offset.vRot)
				iShowerSceneTwo = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
			ELSE
				g_showerGlobals.iScene = CREATE_SYNCHRONIZED_SCENE(g_showerGlobals.vScenePos, g_showerGlobals.vSceneHead)
				iShowerSceneTwo = g_showerGlobals.iScene
			ENDIF
			
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_showerGlobals.iScene, sAnimDict, sIdleToExit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			IF DOES_ENTITY_HAVE_DRAWABLE(mTrigger)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, iShowerSceneTwo, sExitAnimDoor, sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			IF DOES_CAM_EXIST(mCam)
				SET_CAM_PARAMS(mCam, g_showerGlobals.vTurnOffWideCamPos, g_showerGlobals.vTurnOffWideCamHead, g_showerGlobals.fTurnOffWideCamFov)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - TURN OFF")
			ELSE			
				// If this is a transition, create the camera.
				mCam = CREATE_CAMERA()
						
				IF DOES_CAM_EXIST(mCam)
					SET_CAM_ACTIVE(mCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_CAM_PARAMS(mCam, g_showerGlobals.vTurnOffWideCamPos, g_showerGlobals.vTurnOffWideCamHead, g_showerGlobals.fTurnOffWideCamFov)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - TURN OFF")
					SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: - TS_CHECK_EXIT_SHOWER_ANIM")	
			eTriggerState = TS_CHECK_EXIT_SHOWER_ANIM
			
		BREAK
		
		CASE TS_CHECK_EXIT_SHOWER_ANIM
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			
			g_showerGlobals.bShowering = TRUE
			
			IF g_showerGlobals.iScene >= 0
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_showerGlobals.iScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) < 0.99			
			AND NOT bForceCamChange
				
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 120) = 0  
					PRINTLN("[SH_MED] SHOWER: NET_SET_PLAYER_CONTROL 5 (TS_CHECK_EXIT_SHOWER_ANIM)")
				ENDIF
				#ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
			
				IF IS_PED_FEMALE(PLAYER_PED_ID())
					FLOAT fFemaleShowerStopSoundTime
					fFemaleShowerStopSoundTime = 0.129
						IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							fFemaleShowerStopSoundTime = 0.089
						ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) >= fFemaleShowerStopSoundTime
						STOP_SOUND(g_showerGlobals.iWaterSoundID)
					ENDIF
				ELSE
					FLOAT fMaleShowerStopSoundTime
					fMaleShowerStopSoundTime = 0.176
						IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							fMaleShowerStopSoundTime = 0.05
						ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) >= fMaleShowerStopSoundTime
						STOP_SOUND(g_showerGlobals.iWaterSoundID)
					ENDIF
				ENDIF
				
				IF bDoorAudio = FALSE
				AND iBaseProperty != PROPERTY_YACHT_APT_1_BASE
					IF IS_PED_FEMALE(PLAYER_PED_ID())
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) >= 0.76
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] - PLAY_SOUND_FROM_ENTITY: MP_APARTMENT_SHOWER_DOOR_OPEN_MASTER: female")
							PLAY_SOUND_FROM_ENTITY(-1, "MP_APARTMENT_SHOWER_DOOR_OPEN_MASTER", PLAYER_PED_ID())
							bDoorAudio = TRUE
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) >= 0.8
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] - PLAY_SOUND_FROM_ENTITY: MP_APARTMENT_SHOWER_DOOR_OPEN_MASTER: male")
							PLAY_SOUND_FROM_ENTITY(-1, "MP_APARTMENT_SHOWER_DOOR_OPEN_MASTER", PLAYER_PED_ID())
							bDoorAudio = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT g_showerGlobals.bStoppedPtFX
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fPtfxOffPhase
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] TURNING OFF PARTICLE EFFECTS")
						IF DOES_PARTICLE_FX_LOOPED_EXIST(mWaterEffect)
							STOP_PARTICLE_FX_LOOPED(mWaterEffect)
						ENDIF
						
						IF DOES_PARTICLE_FX_LOOPED_EXIST(mSteamEffect)
							STOP_PARTICLE_FX_LOOPED(mSteamEffect)
						ENDIF
							
						g_showerGlobals.bStoppedPtFX = TRUE
					ELSE
						CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED] NOT READY TO TURN OFF PARTICLE EFFECTS")
					ENDIF
				ENDIF
				
				// Put clothes back on
				IF NOT g_showerGlobals.bRestoredClothes
				OR bClothedPlayerYacht
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > g_showerGlobals.fNonNakedPhase
						IF iBaseProperty != PROPERTY_YACHT_APT_1_BASE
							DRESS_PLAYER()
						ELSE
							bClothedPlayerYacht = FALSE
						ENDIF
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), g_showerGlobals.vExitShowerPos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), g_showerGlobals.fExitShowerHeading)
						PLAY_SOUND_FROM_ENTITY(-1, "MP_APARTMENT_SHOWER_GET_DRESSED_MASTER", PLAYER_PED_ID())
						g_showerGlobals.bRestoredClothes = TRUE
						
						//Ading force exit of this IF block to update the camera in yacht
						IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
							bForceCamChange = TRUE							
						ENDIF
						
					//Adding custom camera cut due to stilt apartment showers
					ELIF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > 0.6
						IF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
						OR iBaseProperty = PROPERTY_STILT_APT_1_BASE_B						
							SET_CAM_PARAMS(mCam, g_showerGlobals.vExitFrameCamPos, g_showerGlobals.vExitFrameCamHead, g_showerGlobals.fExitCamFov)
						ENDIF
					ENDIF
				ENDIF
				
				//Adding slightly different PED dress time for yachts to better show off the door anims
				IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
				AND NOT g_showerGlobals.bRestoredClothes
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > 0.252				
						DRESS_PLAYER()
						bClothedPlayerYacht = TRUE
					ENDIF
				ENDIF
								
				IF NOT g_showerGlobals.bLastCam
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_showerGlobals.iScene) > 0.148
						IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
							SET_CAM_PARAMS(mCam, g_showerGlobals.vExitCamPos, g_showerGlobals.vExitCamHead, g_showerGlobals.fExitCamFov)
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Cam:  - EXIT CAM")
						g_showerGlobals.bLastCam = TRUE
					ENDIF
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)						
			ELSE
				
				INT iTimerOne
				INT iTimerTwo
				
				IF iBaseProperty = PROPERTY_YACHT_APT_1_BASE
					iTimerOne = 100
					iTimerTwo = 110
				ELSE
					iTimerOne = 1000
					iTimerTwo = 1500
				ENDIF
				
				IF iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
				OR iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
				OR iBaseProperty = PROPERTY_YACHT_APT_1_BASE
				OR iBaseProperty = PROPERTY_HIGH_APT_1
				OR iBaseProperty = PROPERTY_MEDIUM_APT_1
				OR iBaseProperty = PROPERTY_LOW_APT_1
				OR iBaseProperty = PROPERTY_CUSTOM_APT_1_BASE
				OR iBaseProperty = PROPERTY_OFFICE_2_BASE
				OR iBaseProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
				OR iBaseProperty = PROPERTY_CASINO_GARAGE
				OR iBaseProperty = PROPERTY_SECURITY_OFFICE_GARAGE
					IF NOT HAS_NET_TIMER_STARTED(showerExitCam)
						START_NET_TIMER(showerExitCam)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER: TIMER STARTED")
					ELSE 
						IF HAS_NET_TIMER_EXPIRED(showerExitCam,iTimerOne)
						AND NOT bHasSwitchedToExitCam
								CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER: CAM_PARAMS_SET")
								IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
									SET_CAM_PARAMS(mCam, g_showerGlobals.vExitFrameCamPos, g_showerGlobals.vExitFrameCamHead, g_showerGlobals.fExitCamFov)
								ELSE
									MP_PROP_OFFSET_STRUCT SStilt_APT_Offset
									IF iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-166.5047, 495.4067, 134.3412>>, <<-0.8419, -0.0000, 152.6515>>)
									ELIF iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<123.8405, 555.7748, 181.1385>>, <<-7.5952, -0.0000, 142.9223>>)
									ELIF iBaseProperty = PROPERTY_CUSTOM_APT_1_BASE
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-806.3640, 332.1782, 191.4045>>, <<-5.8699, -0.0000, -34.4544>>)
									ELIF iBaseProperty = PROPERTY_HIGH_APT_1
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-785.8720, 333.6375, 201.9363>>, <<-5.6634, 0.0138, 137.4976>>)
									ELIF iBaseProperty = PROPERTY_MEDIUM_APT_1
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<346.7920, -993.2576, -97.8206>>, <<-32.4215, 0.0966, -149.9743>>)
									ELIF iBaseProperty = PROPERTY_LOW_APT_1
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<257.0706, -999.8622, -98.1861>>, <<-12.4860, -0.0615, 118.1023>>)
									ELIF iBaseProperty = PROPERTY_OFFICE_2_BASE
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1385.1969, -470.0034, 73.7174>>, <<-52.8011, -0.0058, 91.9435>>)
									ELIF iBaseProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
										SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<203.4809, 5161.8564, -87.6065>>, <<-28.9625, -0.0000, -1.5162>>)
									ELIF iBaseProperty = PROPERTY_CASINO_GARAGE
										IF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_1_KEY
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<978.6433, 75.8965, 117.3245>>, <<-15.7838, -0.0000, 140.3427>>)
										ELIF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_2_KEY
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<982.9430, 71.0726, 117.6769>>, <<-27.9204, -0.0000, -22.7347>>)
										ENDIF
									ELIF iBaseProperty = PROPERTY_SECURITY_OFFICE_GARAGE
										IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_HAWICK
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << 389.849, -68.069, 112.852 >>, <<-14.5993, -0.0000, 168.0363>>)
										ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, << -1011.485, -428.634, 73.350 >>, <<-19.0944, -0.0092, -145.0433>>)
										ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-596.7040, -711.3295, 122.4940>>, <<-19.3047, 0.0336, 8.5442>>)
										ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
											SStilt_APT_Offset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-998.734, -752.362, 71.383>>, <<-18.4503, -0.0377, -81.4201>>)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER: Couldn't find correct Fixer HQ simple interior for exit cam coords!")
										ENDIF
									ENDIF
									SET_CAM_PARAMS(mCam,SStilt_APT_Offset.vLoc,SStilt_APT_Offset.vRot , g_showerGlobals.fExitCamFov)
								ENDIF
								bHasSwitchedToExitCam = TRUE
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER: TIMING SHOWER EXIT CAM")
						ELIF HAS_NET_TIMER_EXPIRED(showerExitCam,iTimerTwo)
							
							bHasSwitchedToExitCam 	= FALSE 
							bForceCamChange 		= FALSE
							RESET_NET_TIMER(showerExitCam)
							CLEANUP_SHOWER_VARIABLES_AFTER_SHOWER()
							
						ELSE
							DRESS_PLAYER()
						ENDIF
					ENDIF
				ELSE 
					CLEANUP_SHOWER_VARIABLES_AFTER_SHOWER()
				ENDIF
				
			ENDIF
		BREAK					
	ENDSWITCH
ENDPROC

PROC INIT_BATHROOM_DOOR(INT iBathroomID, INT iYachtID)
		
	IF GET_YACHT_BATHROOM_DOORS(bathroomDoorLoc, bathroomDoor, iYachtID, iBathroomID)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR successfull")
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR FAILED to get door details")
	ENDIF
	
	vBathroomAreaCPos = mpYachts[iYachtID].yachtPropertyDetails.house.activity[iBathroomID].vBathroomDoorArea
	vBathroomAreaCPosB = mpYachts[iYachtID].yachtPropertyDetails.house.activity[iBathroomID].vBathroomDoorAreaB
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR: Yacht ID: ", iYachtID)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR: door hash ", bathroomDoor.iDoorHash)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR: door at :", bathroomDoor.vCoords)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR: vBathroomAreaCPos :", vBathroomAreaCPos)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] INIT_BATHROOM_DOOR: vBathroomAreaCPosB :", vBathroomAreaCPosB)	
ENDPROC

// Check if player is alone in bathroom
// Updated for url:bugstar:2089550 & url:bugstar:1796703
PROC IS_PLAYER_ALONE_IN_BATHROOM()  

	// Access nearby peds every time we restart the staggered loop
	IF iBathroomPedsLoop = 0
		iNumPedsNearBathroom = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), aPedsNearBathroom) 
	ENDIF
	
	// Flag used to determine if we've reset the loop
	BOOL bReset

	IF DOES_ENTITY_EXIST(aPedsNearBathroom[iBathroomPedsLoop])
	AND NOT IS_PED_INJURED(aPedsNearBathroom[iBathroomPedsLoop]) 
		// Verify ped is in the bathroom
		IF GET_INTERIOR_FROM_ENTITY(aPedsNearBathroom[iBathroomPedsLoop]) = apartmentInterior
		AND GET_ROOM_KEY_FROM_ENTITY(aPedsNearBathroom[iBathroomPedsLoop]) = iBathroomRoomKey
			// Update bool
			bPlayerAloneInBathroom =  FALSE
			
			// Reset the loop
			iBathroomPedsLoop = 0
			bReset = TRUE
		ENDIF 
	ENDIF
	
	// Increment array index
	IF NOT bReset
		iBathroomPedsLoop++
		
		// Reset array index if we've looped through all nearby peds
		// OR we reach end of array.
		IF iBathroomPedsLoop >= iNumPedsNearBathroom
		OR iBathroomPedsLoop >= PEDS_NEAR_BATHROOM_SIZE
			iBathroomPedsLoop 		= 0 // Reset loop
			bPlayerAloneInBathroom 	=  TRUE
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_BATHROOM()   
	
	// Check if player is in bathroom
	IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != iBathroomRoomKey
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBathroomAreaCPos, vBathroomAreaCPosB, 1.0)
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != iBathroomRoomKey
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] IS_PLAYER_IN_BATHROOM: Player not in bathroom, roomkey = ", GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()), ", iBathroomRoomKey = ", iBathroomRoomKey)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBathroomAreaCPos, vBathroomAreaCPosB, 1.0)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] IS_PLAYER_IN_BATHROOM: IS_ENTITY_IN_ANGLED_AREA: TRUE, BathroomAreaCPos = ", vBathroomAreaCPos, ", vBathroomAreaCPosB = ", vBathroomAreaCPosB)
		ENDIF
		
		//Fix for: 2743529 - Player room key is 0 causing an infinite hang
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = 0
		AND IS_TRANSITION_ACTIVE()
		AND GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_SHOWER
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] IS_PLAYER_IN_BATHROOM: SPAWN_ACTIVITY_SHOWER: IS_TRANS_ACTIVE: ", IS_TRANSITION_ACTIVE(), " IS_PLAYER_SWITCH_IN_PROGRESS: ", IS_PLAYER_SWITCH_IN_PROGRESS() )
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//purpose: Runs the bathroom door bit set check to see if any of the participants have requested that the yacht door be locked
FUNC BOOL CHECK_FOR_BATHROOM_DOOR_LOCK_REQUEST()
	INT i	
	REPEAT NUM_NETWORK_PLAYERS i
		IF iShowerID = SAFEACT_SHOWER
			IF IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
				RETURN TRUE
			ENDIF
		ELIF iShowerID = SAFEACT_SHOWER_2
			IF IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
				RETURN TRUE
			ENDIF
		ELIF iShowerID = SAFEACT_SHOWER_3
			IF IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

//purpose: Sets or clears the lock request bit for the local participant
PROC SET_YACHT_BATHROOM_DOOR_STATE(BOOL bLockMeIn)
	IF iShowerID = SAFEACT_SHOWER
		IF bLockMeIn
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom  Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK")
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom NOT Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_ONE_LOCK")
			ENDIF
		ENDIF
	ELIF iShowerID = SAFEACT_SHOWER_2
		IF bLockMeIn
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom  Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK")
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom NOT Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_TWO_LOCK")
			ENDIF
		ENDIF
	ELIF iShowerID = SAFEACT_SHOWER_3
		IF bLockMeIn
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom  Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK")
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] OB_MP_SHOWER_MED: bathroom NOT Requesting lock door: PROPERTY_BROADCAST_BS_PLAYER_REQUEST_YACHT_SHOWER_THREE_LOCK")
			ENDIF
		ENDIF
	ELSE
	ENDIF
ENDPROC

PROC DEAL_WITH_BATHROOM_DOOR()

	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(bathroomDoor.iDoorHash)
		EXIT
	ENDIF		

	IS_PLAYER_ALONE_IN_BATHROOM()
	
	BOOL bTryToLockMeIn = FALSE
	
	IF IS_PLAYER_IN_BATHROOM()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: IS_PLAYER_IN_BATHROOM = TRUE")
		IF bPlayerAloneInBathroom		
			bTryToLockMeIn = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: bPlayerAloneInBathroom = TRUE")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: bPlayerAloneInBathroom = FALSE")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: IS_PLAYER_IN_BATHROOM = FALSE")
	ENDIF
	
	IF NOT bPlayerAloneInBathroom												
		bTryToLockMeIn = FALSE
	ENDIF
	
	//Set the request lock bit for the relevant door
	SET_YACHT_BATHROOM_DOOR_STATE(bTryToLockMeIn)
	
	// If server says someone is alone in bathroom, lock door...
	IF CHECK_FOR_BATHROOM_DOOR_LOCK_REQUEST()
		//If the player is not in the door area check to see if it is locked
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBathroomAreaCPos, vBathroomAreaCPosB, 1.0)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(bathroomDoor.iDoorHash)
			AND DOOR_SYSTEM_GET_DOOR_STATE(bathroomDoor.iDoorHash) = DOORSTATE_LOCKED
			AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(bathroomDoor.iDoorHash)) < 0.02
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: door locked by interior script enabling shower activity")
				#ENDIF
				//The door has been locked by the AM_PROPERTY_INT SCRIPT and the door is sufficiently closed, allow the player to shower
				IF NOT bBathroomDoorLocked
					bBathroomDoorLocked = TRUE
				ENDIF
			ELSE
				IF bBathroomDoorLocked
					bBathroomDoorLocked = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: Door not closed or locked setting bBathroomDoorLocked to false")
				ENDIF
			ENDIF
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBathroomAreaCPos, vBathroomAreaCPosB, 1.0)
		OR NOT bPlayerAloneInBathroom
		OR NOT IS_PLAYER_IN_BATHROOM()
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBathroomAreaCPos, vBathroomAreaCPosB, 1.0)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: IS_ENTITY_IN_ANGLED_AREA: FALSE, BathroomAreaCPos = ", vBathroomAreaCPos, ", vBathroomAreaCPosB = ", vBathroomAreaCPosB)
			ENDIF
			
			IF NOT bTryToLockMeIn
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEAL_WITH_BATHROOM_DOOR: bTryToLockMeIn: FALSE")
			ENDIF
			
			IF bBathroomDoorLocked
				bBathroomDoorLocked = FALSE
			ENDIF
			
		ENDIF
	ELSE
		//No one is in the bathroom or the doors aren't locked disable shower activity
		IF bBathroomDoorLocked
			bBathroomDoorLocked = FALSE
		ENDIF
	ENDIF
	
	IF NOT bPlayerAloneInBathroom
		SET_YACHT_BATHROOM_DOOR_STATE(bPlayerAloneInBathroom)
	ENDIF
ENDPROC

FUNC STRING GET_YACHT_BATHROOM_ROOMKEY(INT iTempShowerID)
	SWITCH iTempShowerID
		CASE SAFEACT_SHOWER
			RETURN "YachtRm_Bath1"
		BREAK
		
		CASE SAFEACT_SHOWER_2
			RETURN "YachtRm_Bath2"
		BREAK
		
		CASE SAFEACT_SHOWER_3
			RETURN "YachtRm_weebathroom"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC RUN_NETWORK_SCRIPT_INIT()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT BnetworkInitDone
		
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		BnetworkInitDone = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Mid level shower: Ready for network launch...")
	ENDIF
ENDPROC

PROC CLEAR_SHOWER_GLOBAL_VARS()
	//VARIABLES ALL CHANGED TO GLOBALS FOR 1676619 AS CHANGING TO OVERFLOW FUNCTION ITSELF IS PROBLEMATIC!	
	// Variables
	g_showerGlobals.bSetUpAnimDict		= FALSE
	g_showerGlobals.bRestoredClothes	= FALSE
	g_showerGlobals.bWashedBloodOff		= FALSE
	g_showerGlobals.bTakenClothesOff	= FALSE
	g_showerGlobals.bStartedPtfx		= FALSE
	g_showerGlobals.bShowerAudioLoaded 	= FALSE
	g_showerGlobals.bStoppedPtFX		= FALSE
	g_showerGlobals.bShowering 			= FALSE
	g_showerGlobals.bDoorLocked			= FALSE
	g_showerGlobals.bInTransition		= FALSE
	g_showerGlobals.bScreenFadedByForcedClosedShower = FALSE

	// Camera timings
	g_showerGlobals.fLookShowerPhase 	= 0.0
	g_showerGlobals.fAboveShowerPhase 	= 0.0
	g_showerGlobals.fExitFramePhase 	= 0.0
	g_showerGlobals.fExitShowerPhase 	= 0.0
	g_showerGlobals.fBathroomWidth 		= 0.0
	
	// PTFX timings
	g_showerGlobals.fNakedPhase 		= 0.0
	g_showerGlobals.fNonNakedPhase 		= 0.0
	g_showerGlobals.fPtfxOnPhase 		= 0 
	g_showerGlobals.fPtfxOffPhase 		= 0
	g_showerGlobals.iScene 				= 0			//, iLocalScene
	g_showerGlobals.iWaterSoundID		= -1
	g_showerGlobals.iCurProperty 		= 0
	g_showerGlobals.iSingingDuration 	= 0		
	g_showerGlobals.vTriggerPos 		= <<0,0,0>>
	g_showerGlobals.fTriggerHead 		= 0
	g_showerGlobals.vTriggerSize 		= <<0,0,0>>
	
	//Sync scene position
	g_showerGlobals.vScenePos 			= <<0,0,0>>				
	g_showerGlobals.vSceneHead 			= <<0,0,0>>			
	
	//Camarea positions
	g_showerGlobals.vEnterWideCamPos 	= <<0,0,0>>				
	g_showerGlobals.vEnterWideCamHead 	= <<0,0,0>>			
	g_showerGlobals.fEnterWideCamFov	= 64.9935	

	g_showerGlobals.vLookShowerCamPos 	= <<0,0,0>>					
	g_showerGlobals.vLookShowerCamHead 	= <<0,0,0>>		
	g_showerGlobals.fLookShowerCamFov 	= 40.0

	g_showerGlobals.vAboveShowerCamPos 	= <<0,0,0>>					
	g_showerGlobals.vAboveShowerCamHead	= <<0,0,0>>			
	g_showerGlobals.fAboveShowerCamFov	= 32.0

	g_showerGlobals.vTurnOffWideCamPos 	= <<0,0,0>>				
	g_showerGlobals.vTurnOffWideCamHead = <<0,0,0>>				
	g_showerGlobals.fTurnOffWideCamFov	= 48.0

	g_showerGlobals.vExitFrameCamPos 	= <<0,0,0>>					
	g_showerGlobals.vExitFrameCamHead 	= <<0,0,0>>				
	g_showerGlobals.fExitFrameCamFov	= 48.0

	g_showerGlobals.vExitCamPos	 		= <<0,0,0>>			
	g_showerGlobals.vExitCamHead 		= <<0,0,0>>		
	g_showerGlobals.fExitCamFov			= 50.0

	g_showerGlobals.vShowerHeadPos 		= <<0,0,0>>	
	g_showerGlobals.vShowerHeadHead	 	= <<0,0,0>>	

	g_showerGlobals.vSteamPos 			= <<0,0,0>>
	g_showerGlobals.vSteamHead 			= <<0,0,0>>

	g_showerGlobals.vAreaAPos 			= <<0,0,0>>
	g_showerGlobals.vAreaBPos 			= <<0,0,0>>
	
	g_showerGlobals.vExitShowerPos 		= <<0,0,0>>
	g_showerGlobals.fExitShowerHeading 	= 0

	g_showerGlobals.bLastCam = FALSE
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] ob_mp_shower_med.sc started")
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_ARENA_GARAGE_1
		g_showerGlobals.iCurProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
	ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_CASINO_APT
		g_showerGlobals.iCurProperty = PROPERTY_CASINO_GARAGE
	ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_HAWICK
	OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
	OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
	OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
		g_showerGlobals.iCurProperty = PROPERTY_SECURITY_OFFICE_GARAGE
	ELSE
		g_showerGlobals.iCurProperty 				= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	ENDIF
	
	g_showerGlobals.bShowerScriptJustRunCleanup = FALSE
	
	//Check for more than one shower script running & grab the shower boject if possible
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("ob_mp_shower_med")) > 1
	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower script already running for spawn! cleaning up ")
		CLEANUP_SHOWER_ACTIVITY(TRUE)
	ELIF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Not running shower during Yacht mission when network game is not in progress")
		CLEANUP_SHOWER_ACTIVITY(TRUE)
	ELIF mObjectIn = NULL
	AND g_showerGlobals.iCurProperty != -1
	AND g_showerGlobals.iCurProperty <= MAX_MP_PROPERTIES
	
		RUN_NETWORK_SCRIPT_INIT()
		
		//Find the trigger point for this property
		VECTOR vecShower = mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vTriggerVec			
		
		//Currently there are three different shower door models. Grab the first one available
		IF IS_PROPERTY_OFFICE(g_showerGlobals.iCurProperty)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for office shower object with hash: ex_p_mp_h_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(vecShower, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_p_mp_h_showerdoor_s")), TRUE, FALSE, FALSE)
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_ARENA_GARAGE_1
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for arena garage shower object with hash: xs_prop_arena_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<202.8190, 5162.1733, -88.8851>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), TRUE, FALSE, FALSE)
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_CASINO_APT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for casino apartment shower object with hash: xs_prop_arena_showerdoor_s")
			
			IF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_1_KEY
				mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<979.1015, 72.9345, 115.1641>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
			ELIF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_2_KEY
				mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<981.7758, 70.6806, 115.1641>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
			ENDIF
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_HAWICK
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object in Hawick Fixer HQ with hash: xs_prop_arena_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<389.44, -69.02, 110.96>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object in Rockford Heights Fixer HQ with hash: xs_prop_arena_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<-1011.08, -429.52, 71.46>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object in Little Seoul Fixer HQ with hash: xs_prop_arena_showerdoor_sm")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<-596.73, -710.30, 120.61>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object in Vespucci Fixer HQ with hash: xs_prop_arena_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(<<-998.01, -752.18, 70.94>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_showerdoor_s")), FALSE, FALSE, FALSE)
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object with hash: p_mp_showerdoor_s")
			mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(vecShower, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("p_mp_showerdoor_s")), TRUE, FALSE, FALSE)
		
			IF mObjectIn = NULL
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Looking for shower object with hash: apa_p_mp_h_showerdoor_s")
				mObjectIn = GET_CLOSEST_OBJECT_OF_TYPE(vecShower, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("apa_p_mp_h_showerdoor_s")), TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	//If we still don't have an object kill the sctipt
	IF mObjectIn = NULL
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower script couldn't find shower object! cleaning up")
		CLEANUP_SHOWER_ACTIVITY(TRUE)
	ENDIF
	
		vShowerObject = GET_ENTITY_COORDS(mObjectIn)
	
	RUN_NETWORK_SCRIPT_INIT()	
	CLEAR_SHOWER_GLOBAL_VARS()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] ob_mp_shower_med.sc started, object coords: ", vShowerObject)
	
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
	OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
	OR Is_Player_On_Or_Triggering_Any_Mission()
	OR MPGlobalsAmbience.bRunningFmIntroCut
	OR NOT DOES_ENTITY_EXIST(mObjectIn)
		IF NOT DOES_ENTITY_EXIST(mObjectIn)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SECOND CLEANUP: SHOWER OBJECT DOESN'T EXIST")
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] DEFAULT CLEANUP")
		CLEANUP_SHOWER_ACTIVITY()
	ENDIF
	
	IF DOES_ENTITY_EXIST(mObjectIn)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Shower: trigger object found, running setup")
		mTrigger = mObjectIn
		
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_ARENA_GARAGE_1
			g_showerGlobals.iCurProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
			iBaseProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_CASINO_APT
			g_showerGlobals.iCurProperty = PROPERTY_CASINO_GARAGE
			iBaseProperty = PROPERTY_CASINO_GARAGE
		ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		OR GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			g_showerGlobals.iCurProperty = PROPERTY_SECURITY_OFFICE_GARAGE
			iBaseProperty = PROPERTY_SECURITY_OFFICE_GARAGE
		ELSE
			g_showerGlobals.iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			iBaseProperty = GET_BASE_PROPERTY_FROM_PROPERTY(g_showerGlobals.iCurProperty)
		ENDIF
		
		IF NOT IS_ENTITY_ATTACHED(mTrigger)
			FREEZE_ENTITY_POSITION(mTrigger, TRUE)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Shower: Could not find shower object within the world")
	ENDIF
		
	INIT_GENDER_SPECIFICS()
	
	apartmentInterior 				= GET_INTERIOR_FROM_ENTITY(mObjectIn) 
	iBathroomRoomKey 				= GET_ROOM_KEY_FROM_ENTITY(mObjectIn)	
	g_showerGlobals.bInTransition 	= g_showerGlobals.bInTransition	
	BOOL bObjectExists 				= (DOES_ENTITY_EXIST(mObjectIn) OR mObjectIn != NULL)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Mid level shower: Script triggered! apartmentInterior: ", NATIVE_TO_INT(apartmentInterior), " iBathroomRoomKey: ", iBathroomRoomKey)
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
			
		WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NETWORK_IS_GAME_IN_PROGRESS()
		AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		AND bObjectExists
		AND NOT(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_IN_A_GARAGE) AND Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
		AND NOT g_showerGlobals.bInteriorForceCleanup
		AND NOT g_showerGlobals.bInteriorForceCleanupToGar
		AND NOT g_showerGlobals.bInteriorForceCleanupNoFade
		
			IF IS_PROPERTY_CUSTOMISABLE(g_showerGlobals.iCurProperty)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW)
			AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as the interior was changed")
				CLEANUP_SHOWER_ACTIVITY()
			ENDIF
				
			IF g_showerGlobals.iCurProperty <= 0
				IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_ARENA_GARAGE_1
					g_showerGlobals.iCurProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower H: Current property is ", g_showerGlobals.iCurProperty)
				ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_CASINO_APT
					g_showerGlobals.iCurProperty = PROPERTY_CASINO_GARAGE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower H: Current property is ", g_showerGlobals.iCurProperty)
				ELSE
					g_showerGlobals.iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower H: Current property is ", g_showerGlobals.iCurProperty)
				ENDIF
			ELSE
				IF NOT bInitOffsets
					
					IF IS_PROPERTY_YACHT_APARTMENT(g_showerGlobals.iCurProperty)
						INT iYachtID = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmOn
						IF iYachtID  = -1
							iYachtID = 22
						ENDIF 
						IF g_showerGlobals.iCurProperty > 0
							IS_SHOWER_IN_SPECIFIC_YACHT(vShowerObject, iShowerID, iYachtID) // sets the iShowerID
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] MP Shower H: iShowerID = ", iShowerID)
						ENDIF
						
						iBathroomRoomKey 	= GET_HASH_KEY(GET_YACHT_BATHROOM_ROOMKEY(iShowerID))
						
						IF iYachtID = 36
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "ac_mpapa_yacht")
						ELIF iYachtID = 37
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "h4_islandx_yacht_01_int")
						ELIF iYachtID = 38
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "h4_islandx_yacht_02_int")
						ELIF iYachtID = 39
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "h4_islandx_yacht_03_int")
						#IF FEATURE_FIXER
						ELIF iYachtID = 40
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "sf_yacht_01_int")
						ELIF iYachtID = 41
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "sf_yacht_02_int")
						#ENDIF
						ELSE
							apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(bathroomDoorLoc.vLoc, "apa_mpapa_yacht")
						ENDIF
						
						//setup for the new multiple showers 
						iActivityID = ci_APT_ACT_SHOWER
						IF iShowerID = SAFEACT_SHOWER_2
							iActivityID += 1
						ELIF iShowerID = SAFEACT_SHOWER_3
							iActivityID += 2
						ENDIF
						
						SET_YACHT_ANIM_DICTS()
						
						FLOAT fTempHeading
						
						IF DOES_ENTITY_EXIST(mObjectIn)
							g_showerGlobals.vTriggerPos 	= GET_ENTITY_COORDS(mObjectIn) 							
							g_showerGlobals.fTriggerHead 	= GET_ENTITY_HEADING(mObjectIn)						
							g_showerGlobals.vScenePos 		= GET_ENTITY_COORDS(mTrigger)
							fTempHeading					= GET_ENTITY_HEADING(mTrigger)
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower script couldn't find shower object! bInitOffsets")
							CLEANUP_SHOWER_ACTIVITY(FALSE)
						ENDIF
						
						g_showerGlobals.vTriggerSize = <<1.0, 1.0, 1.0>>
							
						IF g_showerGlobals.fTriggerHead < 0.0
							g_showerGlobals.fTriggerHead+= 360.0
						ENDIF
						
						g_showerGlobals.vSceneHead 			= <<0, 0, fTempHeading>>
						
						g_showerGlobals.vShowerHeadPos 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vShowerHeadVec
						g_showerGlobals.vShowerHeadHead 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vShowerHeadRot
						
						g_showerGlobals.vSteamPos 			= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vSteamVec
						g_showerGlobals.vSteamHead 			= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vSteamRot
						
						g_showerGlobals.vEnterWideCamPos 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vCamVec					
						g_showerGlobals.vEnterWideCamHead 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vCamRot					
						
						g_showerGlobals.vExitCamPos 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vExitCamVec	// shot of player out of the shower				
						g_showerGlobals.vExitCamHead 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vExitCamRot
						
						g_showerGlobals.vLookShowerCamPos 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vLookCamVec		
						g_showerGlobals.vLookShowerCamHead 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vLookCamRot	
						
						g_showerGlobals.vAboveShowerCamPos 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vAboveCamVec			
						g_showerGlobals.vAboveShowerCamHead	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vAboveCamRot 
						
						g_showerGlobals.vTurnOffWideCamPos 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vTurnOffCamVec
						g_showerGlobals.vTurnOffWideCamHead	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vTurnOffCamRot 
						
						g_showerGlobals.vExitFrameCamPos 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vExitFrameCamVec					
						g_showerGlobals.vExitFrameCamHead	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vExitFrameCamRot		
						
						g_showerGlobals.vAreaAPos 			= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vAreaAVec				
						g_showerGlobals.vAreaBPos 			= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vAreaBVec	
						
						g_showerGlobals.fBathroomWidth		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].fBathAreaWidth
						
						g_showerGlobals.vExitShowerPos 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[iShowerID].vExitShowerPos
						
						
						VECTOR vTempTrigger 		= g_showerGlobals.vAreaAPos + g_showerGlobals.vAreaBPos 
						g_showerGlobals.vTriggerPos = vTempTrigger*0.5
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] g_showerGlobals.vAreaAPos = ", g_showerGlobals.vAreaAPos )
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] g_showerGlobals.vAreaBPos = ", g_showerGlobals.vAreaBPos )
						
						MP_PROP_OFFSET_STRUCT tempOffset
						
						IF iShowerID = SAFEACT_SHOWER_3
							tempOffset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1465.9028, -3756.6323, 6.3920>>, <<2.3104, -1.0247, 125.1869>>)
							g_showerGlobals.vExitFrameCamPos	= tempOffset.vLoc
							g_showerGlobals.vExitFrameCamHead	= tempOffset.vRot
							g_showerGlobals.fExitFrameCamFov	= 50
							tempOffset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1466.8800, -3757.3828, 4.8731>>, <<0.0000, 0.0000, 311.9359>>)
							g_showerGlobals.fExitShowerHeading	= tempOffset.vRot.Z
						ELIF iShowerID = SAFEACT_SHOWER_2
							tempOffset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1477.3781, -3753.4993, 6.3990>>, <<-6.5528, 0.2213, 126.3170>>)
							g_showerGlobals.vExitFrameCamPos	= tempOffset.vLoc
							g_showerGlobals.vExitFrameCamHead	= tempOffset.vRot
							g_showerGlobals.fExitFrameCamFov	= 49.5
							tempOffset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1478.2604, -3754.2109, 4.8889>>, <<0.0000, 0.0000, 312.4691>>)
							g_showerGlobals.fExitShowerHeading	= tempOffset.vRot.Z
						ELSE iShowerID = SAFEACT_SHOWER
							tempOffset = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(g_showerGlobals.iCurProperty, iBaseProperty, <<-1487.3311, -3745.0483, 4.8842>>, <<0.0000, 0.0000, 293.8913>>)
							g_showerGlobals.fExitFrameCamFov	= 50.0
							g_showerGlobals.fExitShowerHeading	= tempOffset.vRot.Z
						ENDIF
						
						g_showerGlobals.fAboveShowerCamFov	= 44.0515
						g_showerGlobals.fLookShowerCamFov 	= g_showerGlobals.fEnterWideCamFov
						
						INIT_BATHROOM_DOOR(iShowerID, iYachtID)
						
						bInitOffsets = TRUE
					ELIF g_showerGlobals.iCurProperty = PROPERTY_ARENAWARS_GARAGE_LVL1
						g_showerGlobals.vTriggerPos = <<201.9919, 5162.1450, -90.1981>>
						g_showerGlobals.fTriggerHead = 0.0
						g_showerGlobals.vTriggerSize = <<1.0, 1.0, 1.0>>
						
						g_showerGlobals.vScenePos = <<202.288, 5162.176, -89.949>>
						g_showerGlobals.vSceneHead = <<0.0, 0.0, 180.0>>
						
						g_showerGlobals.vShowerHeadPos = <<201.6622, 5162.5479, -87.8831>>
						g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 0.0>>
						
						g_showerGlobals.vSteamPos = <<201.6941, 5161.8130, -90.0635>>
						g_showerGlobals.vSteamHead = <<0.0, 0.0, 180.0>>
						
						g_showerGlobals.vEnterWideCamPos = <<202.1388, 5161.2617, -88.2460>>
						g_showerGlobals.vEnterWideCamHead = <<-19.0634, -0.0000, 30.3881>>
						
						g_showerGlobals.vLookShowerCamPos = <<201.7434, 5161.1338, -88.2383>>
						g_showerGlobals.vLookShowerCamHead = <<-12.5763, -0.0130, 23.0038>>
						
						g_showerGlobals.vAboveShowerCamPos = <<201.8755, 5161.6890, -87.9364>>
						g_showerGlobals.vAboveShowerCamHead	= <<-23.5302, -0.0130, 10.3934>>
						
						g_showerGlobals.vTurnOffWideCamPos = <<201.9632, 5161.5781, -87.7381>>
						g_showerGlobals.vTurnOffWideCamHead	= <<-25.2330, -0.0313, 22.2460>>
						
						g_showerGlobals.vExitFrameCamPos = <<202.1001, 5161.5820, -88.0490>>
						g_showerGlobals.vExitFrameCamHead = <<-13.3643, -0.0000, 31.2322>>
						
						g_showerGlobals.vExitCamPos = <<202.1001, 5161.5820, -88.0490>>
						g_showerGlobals.vExitCamHead = <<-13.3643, -0.0000, 31.2322>>
						
						g_showerGlobals.vAreaAPos = <<202.482574,5161.075195,-90.198479>>
						g_showerGlobals.vAreaBPos = <<202.468918,5164.763184,-87.698479>>
						
						g_showerGlobals.fBathroomWidth = 4.0
						
						g_showerGlobals.vExitShowerPos = <<203.2565, 5163.6968, -90.1985>>
						g_showerGlobals.fExitShowerHeading = 176.5010
						
						bInitOffsets = TRUE
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] POD: Shower: g_showerGlobals.vTriggerPos =  ", g_showerGlobals.vTriggerPos)
					ELIF g_showerGlobals.iCurProperty = PROPERTY_CASINO_GARAGE
						IF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_1_KEY
							g_showerGlobals.vTriggerPos = <<979.5013, 72.1432, 116.1641>>
							g_showerGlobals.fTriggerHead = 0.0
							g_showerGlobals.vTriggerSize = <<1.5, 1.5, 1.5>>
							
							g_showerGlobals.vScenePos = <<978.931, 72.050, 115.401>>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, 39.75>>
							
							g_showerGlobals.vShowerHeadPos = <<979.5851, 72.1230, 117.6544>>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 0.0>>
							
							g_showerGlobals.vSteamPos = <<979.2773, 72.3523, 115.2539>>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 180.0>>
							
							g_showerGlobals.vEnterWideCamPos = <<979.3251, 72.9845, 117.4352>>
							g_showerGlobals.vEnterWideCamHead = <<1.0976, -0.0000, -159.3846>>
							
							g_showerGlobals.vLookShowerCamPos = <<979.3251, 72.9845, 117.4352>>
							g_showerGlobals.vLookShowerCamHead = <<1.0976, -0.0000, -159.3846>>
							
							g_showerGlobals.vAboveShowerCamPos = <<979.0707, 73.0831, 117.1748>>
							g_showerGlobals.vAboveShowerCamHead	= <<-20.3202, -0.0000, -146.6535>>
							
							g_showerGlobals.vTurnOffWideCamPos = <<979.0707, 73.0831, 117.1748>>
							g_showerGlobals.vTurnOffWideCamHead	= <<-20.3202, -0.0000, -146.6535>>
							
							g_showerGlobals.vExitFrameCamPos = <<979.0707, 73.0831, 117.1748>>
							g_showerGlobals.vExitFrameCamHead = <<-20.3202, -0.0000, -146.6535>>
							
							g_showerGlobals.vExitCamPos = <<979.0707, 73.0831, 117.1748>>
							g_showerGlobals.vExitCamHead = <<-20.3202, -0.0000, -146.6535>>
							
							g_showerGlobals.vAreaAPos = <<981.144897,73.755341,115.164139>>
							g_showerGlobals.vAreaBPos = <<977.301941,76.177605,117.664139>>
							
							g_showerGlobals.fBathroomWidth = 7.0
							
							g_showerGlobals.vExitShowerPos = <<977.2527, 74.2392, 115.1641>>
							g_showerGlobals.fExitShowerHeading = 326.1824
						ELIF g_sShopSettings.playerRoom = CASINO_APARTMENT_BATHROOM_2_KEY
							g_showerGlobals.vTriggerPos = <<980.6657, 71.4493, 116.1641>>
							g_showerGlobals.fTriggerHead = 0.0
							g_showerGlobals.vTriggerSize = <<1.5, 1.5, 1.5>>
							
							g_showerGlobals.vScenePos = <<981.268, 71.438, 115.401>>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, -140.25>>
							
							g_showerGlobals.vShowerHeadPos = <<980.5698, 71.4066, 117.5516>>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 180.0>>
							
							g_showerGlobals.vSteamPos = <<980.8514, 71.2960, 115.2477>>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 0.0>>
							
							g_showerGlobals.vEnterWideCamPos = <<981.3823, 71.6328, 117.3809>>
							g_showerGlobals.vEnterWideCamHead = <<8.4651, -0.0000, 102.3239>>
							
							g_showerGlobals.vLookShowerCamPos = <<981.3823, 71.6328, 117.3809>>
							g_showerGlobals.vLookShowerCamHead = <<8.4651, -0.0000, 102.3239>>
							
							g_showerGlobals.vAboveShowerCamPos = <<982.2231, 71.5305, 117.2554>>
							g_showerGlobals.vAboveShowerCamHead	= <<-9.0685, -0.0216, 94.2441>>
							
							g_showerGlobals.vTurnOffWideCamPos = <<982.2231, 71.5305, 117.2554>>
							g_showerGlobals.vTurnOffWideCamHead	= <<-9.0685, -0.0216, 94.2441>>
							
							g_showerGlobals.vExitFrameCamPos = <<982.2231, 71.5305, 117.2554>>
							g_showerGlobals.vExitFrameCamHead = <<-9.0685, -0.0216, 94.2441>>
							
							g_showerGlobals.vExitCamPos = <<982.2231, 71.5305, 117.2554>>
							g_showerGlobals.vExitCamHead = <<-9.0685, -0.0216, 94.2441>>
							
							g_showerGlobals.vAreaAPos = <<983.572693,74.219955,115.164139>>
							g_showerGlobals.vAreaBPos = <<980.920410,69.909042,118.164139>>
							
							g_showerGlobals.fBathroomWidth = 3.25
							
							g_showerGlobals.vExitShowerPos = <<983.3156, 72.9934, 115.1641>>
							g_showerGlobals.fExitShowerHeading = 143.2963
						ENDIF
						
						bInitOffsets = TRUE
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] POD: Shower: g_showerGlobals.vTriggerPos =  ", g_showerGlobals.vTriggerPos)
					ELIF g_showerGlobals.iCurProperty = PROPERTY_SECURITY_OFFICE_GARAGE // Fixer Agency
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							g_showerGlobals.vTriggerPos = << 390.543, -68.489, 111.258 >>
							g_showerGlobals.fTriggerHead = 250.0000
							g_showerGlobals.vTriggerSize = <<1.0, 1.4, 2.0>>
							
							g_showerGlobals.vScenePos = << 390.7025, -69.30652, 111.183 >>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, 69.840>>
							
							g_showerGlobals.vShowerHeadPos = << 391.315, -68.841, 113.349 >>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 180.0>>
							
							g_showerGlobals.vSteamPos = << 391.233, -68.832, 111.009 >>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 0.0>>
							
							g_showerGlobals.vEnterWideCamPos = << 390.465, -68.308, 113.004 >>
							g_showerGlobals.vEnterWideCamHead = <<14.2462, 0.0155, -127.1540>>
							
							g_showerGlobals.vLookShowerCamPos = << 390.465, -68.308, 113.004 >>
							g_showerGlobals.vLookShowerCamHead = <<14.2462, 0.0155, -127.1540>>
							
							g_showerGlobals.vAboveShowerCamPos = << 390.111, -68.393, 112.591 >>
							g_showerGlobals.vAboveShowerCamHead	= <<-2.4953, -0.0000, -111.9079>>
							
							g_showerGlobals.vTurnOffWideCamPos = << 390.010, -68.405, 113.492 >>
							g_showerGlobals.vTurnOffWideCamHead	= <<-17.1280, 0.0255, -111.5817>>
							
							g_showerGlobals.vExitFrameCamPos = << 390.010, -68.405, 113.492 >>
							g_showerGlobals.vExitFrameCamHead = <<-17.1280, 0.0255, -111.5817>>
							
							g_showerGlobals.vExitCamPos = << 390.010, -68.405, 113.492 >>
							g_showerGlobals.vExitCamHead = <<-17.1280, 0.0255, -111.5817>>
							
							g_showerGlobals.vAreaAPos = << 389.478, -71.520, 110.963 >>
							g_showerGlobals.vAreaBPos = << 390.699, -68.133, 113.021 >>
							
							g_showerGlobals.fBathroomWidth = 2.55
							
							g_showerGlobals.vExitShowerPos = << 389.592, -70.228, 110.963 >>
							g_showerGlobals.fExitShowerHeading = 161.596

						ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
							g_showerGlobals.vTriggerPos = << -1010.703, -428.414, 71.756 >>
							g_showerGlobals.fTriggerHead = 296.950
							g_showerGlobals.vTriggerSize = <<1.0, 1.4, 2.0>>
							
							g_showerGlobals.vScenePos = << -1010.0, -428.865, 71.690 >>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, 116.720>>
							
							g_showerGlobals.vShowerHeadPos = << -1009.919, -428.090, 73.847 >>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 180.0>>
							
							g_showerGlobals.vSteamPos = << -1009.982, -428.144, 71.507 >>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 0.0>>
							
							g_showerGlobals.vEnterWideCamPos = << -1010.889, -428.347, 73.502 >>
							g_showerGlobals.vEnterWideCamHead = <<11.5057, 0.0275, -80.1633>>
							
							g_showerGlobals.vLookShowerCamPos = << -1010.889, -428.347, 73.502 >>
							g_showerGlobals.vLookShowerCamHead = <<11.5057, 0.0275, -80.1633>>
							
							g_showerGlobals.vAboveShowerCamPos = << -1011.069, -428.664, 73.089 >>
							g_showerGlobals.vAboveShowerCamHead	= <<-3.7327, -0.0297, -67.3337>>
							
							g_showerGlobals.vTurnOffWideCamPos = << -1011.128, -428.746, 73.990 >>
							g_showerGlobals.vTurnOffWideCamHead	= <<-24.1055, -0.0024, -65.0728>>
							
							g_showerGlobals.vExitFrameCamPos = << -1011.128, -428.746, 73.990 >>
							g_showerGlobals.vExitFrameCamHead = <<-24.1055, -0.0024, -65.0728>>
							
							g_showerGlobals.vExitCamPos = << -1011.128, -428.746, 73.990 >>
							g_showerGlobals.vExitCamHead = <<-24.1055, -0.0024, -65.0728>>
							
							g_showerGlobals.vAreaAPos = << -1009.216, -431.261, 71.461 >>
							g_showerGlobals.vAreaBPos = << -1010.857, -428.057, 73.519 >>
							
							g_showerGlobals.fBathroomWidth = 2.55
							
							g_showerGlobals.vExitShowerPos = << -1010.082, -430.296, 71.461 >>
							g_showerGlobals.fExitShowerHeading = 208.546
						ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							g_showerGlobals.vTriggerPos = << -597.50, -711.17, 120.90 >>
							g_showerGlobals.fTriggerHead = 89.850
							g_showerGlobals.vTriggerSize = <<1.0, 1.4, 2.0>>
							
							g_showerGlobals.vScenePos = << -597.9343, -710.4116, 120.8051 >>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, -90.0>>
							
							g_showerGlobals.vShowerHeadPos = << -598.346, -711.102, 122.991 >>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 180.0>>
							
							g_showerGlobals.vSteamPos = << -598.266, -711.082, 120.651 >>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 0.0>>
							
							g_showerGlobals.vEnterWideCamPos = << -597.365, -711.314, 122.646 >>
							g_showerGlobals.vEnterWideCamHead = <<8.8696, 0.0135, 72.8438>>
							
							g_showerGlobals.vLookShowerCamPos = << -597.365, -711.314, 122.646 >>
							g_showerGlobals.vLookShowerCamHead = <<8.8696, 0.0135, 72.8438>>
							
							g_showerGlobals.vAboveShowerCamPos = << -597.061, -711.114, 122.233 >>
							g_showerGlobals.vAboveShowerCamHead	= <<-3.7572, -0.0405, 85.8827>>
							
							g_showerGlobals.vTurnOffWideCamPos = << -596.970, -711.068, 123.134 >>
							g_showerGlobals.vTurnOffWideCamHead	= <<-23.7941, 0.0314, 88.9784>>
							
							g_showerGlobals.vExitFrameCamPos = << -596.970, -711.068, 123.134 >>
							g_showerGlobals.vExitFrameCamHead = <<-23.7941, 0.0314, 88.9784>>
							
							g_showerGlobals.vExitCamPos = << -596.970, -711.068, 123.134 >>
							g_showerGlobals.vExitCamHead = <<-23.7941, 0.0314, 88.9784>>
							
							g_showerGlobals.vAreaAPos = << -597.527, -707.958, 120.605 >>
							g_showerGlobals.vAreaBPos = << -597.526, -711.558, 122.663 >>
							
							g_showerGlobals.fBathroomWidth = 2.55
							
							g_showerGlobals.vExitShowerPos = << -597.196, -709.212, 120.605 >>
							g_showerGlobals.fExitShowerHeading = 1.446
						ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
							g_showerGlobals.vTriggerPos = << -998.576, -751.566, 69.789 >>
							g_showerGlobals.fTriggerHead = 90.0
							g_showerGlobals.vTriggerSize = <<1.0, 1.4, 2.0>>
							
							g_showerGlobals.vScenePos = <<-997.8353, -751.140, 69.69422>>
							g_showerGlobals.vSceneHead = <<0.0, 0.0, 180.0>>
							
							g_showerGlobals.vShowerHeadPos = <<-998.51, -750.72, 71.88>>
							g_showerGlobals.vShowerHeadHead = <<-45.0, 0.0, 180.0>>
							
							g_showerGlobals.vSteamPos = <<-998.49, -750.80, 69.54>>
							g_showerGlobals.vSteamHead = <<0.0, 0.0, 0.0>>
							
							g_showerGlobals.vEnterWideCamPos = <<-998.7197, -751.7010, 71.5347>>
							g_showerGlobals.vEnterWideCamHead = <<11.6813, -0.0000, -15.8878>>
							
							g_showerGlobals.vLookShowerCamPos = <<-998.7197, -751.7010, 71.5347>>
							g_showerGlobals.vLookShowerCamHead = <<11.6813, -0.0000, -15.8878>>
							
							g_showerGlobals.vAboveShowerCamPos = <<-998.5189, -752.0052, 71.1220>>
							g_showerGlobals.vAboveShowerCamHead	= <<-0.3037, -0.0000, -1.8903>>
							
							g_showerGlobals.vTurnOffWideCamPos = <<-998.4734, -752.0953, 72.0231>>
							g_showerGlobals.vTurnOffWideCamHead	= <<-26.6351, -0.0000, -2.5264>>
							
							g_showerGlobals.vExitFrameCamPos = <<-998.4734, -752.0953, 72.0231>>
							g_showerGlobals.vExitFrameCamHead = <<-26.6351, -0.0000, -2.5264>>
							
							g_showerGlobals.vExitCamPos = <<-998.4734, -752.0953, 72.0231>>
							g_showerGlobals.vExitCamHead = <<-26.6351, -0.0000, -2.5264>>
							
							g_showerGlobals.vAreaAPos = <<-995.36377, -751.53027, 69.49422>>
							g_showerGlobals.vAreaBPos = <<-998.96393, -751.54065, 71.55220>>
							
							g_showerGlobals.fBathroomWidth = 2.55
							
							g_showerGlobals.vExitShowerPos = <<-996.6172, -751.8649, 69.4942>>
							g_showerGlobals.fExitShowerHeading = 271.5963
						ELSE
							#IF IS_DEBUG_BUILD
							CERRORLN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [AM_MP_FIXER_HQ] Player is in a simple interior that does not match any known Fixer HQ!! Can't set shower globals!!")
							#ENDIF
						ENDIF
				
						bInitOffsets = TRUE
					ELSE	
					
						g_showerGlobals.vTriggerPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vTriggerVec 
						g_showerGlobals.fTriggerHead 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].fTriggerRot
						g_showerGlobals.vTriggerSize 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vTriggerSizeVec
						
						IF g_showerGlobals.fTriggerHead < 0.0
							g_showerGlobals.fTriggerHead+= 360.0
						ENDIF						
						
						g_showerGlobals.vScenePos 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vSceneVec
						g_showerGlobals.vSceneHead 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vSceneRot
												
						g_showerGlobals.vShowerHeadPos 		= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vShowerHeadVec
						g_showerGlobals.vShowerHeadHead 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vShowerHeadRot
						
						g_showerGlobals.vSteamPos 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vSteamVec
						g_showerGlobals.vSteamHead 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vSteamRot
						
						g_showerGlobals.vEnterWideCamPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vCamVec					
						g_showerGlobals.vEnterWideCamHead 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vCamRot				
						
						g_showerGlobals.vLookShowerCamPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vLookCamVec // shot of the shower head					
						g_showerGlobals.vLookShowerCamHead 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vLookCamRot				
						
						g_showerGlobals.vAboveShowerCamPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vAboveCamVec // shot of players shoulders				
						g_showerGlobals.vAboveShowerCamHead	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vAboveCamRot			
						
						g_showerGlobals.vTurnOffWideCamPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vTurnOffCamVec  // angled shot of player turning off shower					
						g_showerGlobals.vTurnOffWideCamHead	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vTurnOffCamRot			
						
						g_showerGlobals.vExitFrameCamPos 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vExitFrameCamVec					
						g_showerGlobals.vExitFrameCamHead	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vExitFrameCamRot				
						
						g_showerGlobals.vExitCamPos 		= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vExitCamVec	// shot of player out of the shower				
						g_showerGlobals.vExitCamHead 		= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vExitCamRot	
						
						g_showerGlobals.vAreaAPos 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vAreaAVec				
						g_showerGlobals.vAreaBPos 			= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vAreaBVec	
						
						g_showerGlobals.fBathroomWidth		= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].fBathAreaWidth
						
						g_showerGlobals.vExitShowerPos 		= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].vExitShowerPos
						g_showerGlobals.fExitShowerHeading 	= mpProperties[g_showerGlobals.iCurProperty].house.activity[SAFEACT_SHOWER].fExitShowerHeading
						
						g_showerGlobals.vExitShowerPos		= g_showerGlobals.vExitShowerPos
						g_showerGlobals.fExitShowerHeading 	= g_showerGlobals.fExitShowerHeading
						
						VECTOR vTempTrigger 
						vTempTrigger = g_showerGlobals.vAreaAPos + g_showerGlobals.vAreaBPos
						g_showerGlobals.vTriggerPos = vTempTrigger*0.5
						
						bInitOffsets = TRUE
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] POD: Shower: g_showerGlobals.vTriggerPos =  ", g_showerGlobals.vTriggerPos)
						
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: g_showerGlobals.vTriggerPos =  ", g_showerGlobals.vTriggerPos)
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(g_showerGlobals.vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)) < SOFA_TRIGGER_DIST + 2					
							
						IF IS_PROPERTY_YACHT_APARTMENT(g_showerGlobals.iCurProperty)
							DEAL_WITH_BATHROOM_DOOR()
						ENDIF
						
						SWITCH eState
												
							CASE AS_LOAD_ASSETS			
								IF IS_PLAYER_IN_BATHROOM()			
									IF HAS_ANIM_DICT_LOADED_FOR_SHOWER()
									AND HAS_SHOWER_AUDIO_LOADED()
									AND HAS_SHOWER_PTFX_ASSET_LOADED()
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Mid level shower: Assets loaded for activity...!!!")
										#ENDIF

										eState = AS_RUN_ACTIVITY
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: HAS_ANIM_DICT_LOADED_FOR_SHOWER OR HAS_SHOWER_AUDIO_LOADED OR HAS_SHOWER_PTFX_ASSET_LOADED = FALSE. bPauseSkyCamForSpawn: ", g_showerGlobals.bPauseSkyCamForSpawn)
										
										IF NOT bAssetLoadingStuck
										AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stAssetLoadingTimer, 10000)
											bAssetLoadingStuck = TRUE										
										ENDIF
										
										IF INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) = SPAWN_ACTIVITY_SHOWER
											IF NOT bAssetLoadingStuck
												CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: Timing shower asset loading")
											ELSE
												CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Loading audio and PTFX assets for spawning in shower activity is taking longer than 10 seconds")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									CDEBUG2LN(DEBUG_SAFEHOUSE, "[SH_MED] AS_LOAD_ASSETS: not loading assets as player isn't in bathroom")
								ENDIF
							BREAK
							
							CASE AS_RUN_ACTIVITY
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									UPDATE_SHOWER_ACTIVITY()
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: CLEANUP - AS_RUN_ACTIVITY: PED IS INJURED OR DEAD")
									CLEANUP_SHOWER_ACTIVITY()
								ENDIF
							BREAK
							
							CASE AS_END
							BREAK
							
						ENDSWITCH
					ELSE
						IF GET_DISTANCE_BETWEEN_COORDS(g_showerGlobals.vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+3
							#IF IS_DEBUG_BUILD
							VECTOR vTempPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
							FLOAT fTempDistance = GET_DISTANCE_BETWEEN_COORDS(g_showerGlobals.vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Mid level shower: Player too far away - clean up..., g_showerGlobals.vTriggerPos: ", g_showerGlobals.vTriggerPos, ", Player position = ", vTempPlayerPos, ", distance = ", fTempDistance)
							#ENDIF
							CLEANUP_SHOWER_ACTIVITY()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_INJURED(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed because ped is injured")
			ENDIF
			
			IF (IS_PLAYER_SWITCH_IN_PROGRESS() AND GET_SPAWN_ACTIVITY() != SPAWN_ACTIVITY_SHOWER)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed because player switch is in progress and spawn activity isn't set to the shower, GET_SPAWN_ACTIVITY() = ", ENUM_TO_INT(GET_SPAWN_ACTIVITY()), ", IS_PLAYER_SWITCH_IN_PROGRESS = ", IS_PLAYER_SWITCH_IN_PROGRESS())				
			ENDIF
			
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed because SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE) 
				AND Is_Player_Currently_On_MP_Heist(PLAYER_ID()) = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as we're in the garage on a heist mission")
			ENDIF
				
			IF NOT DOES_ENTITY_EXIST(mObjectIn)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER OBJECT DOESN'T EXIST")
			ENDIF 
			
			IF g_showerGlobals.bInteriorForceCleanup
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as g_showerGlobals.bInteriorForceCleanup = TRUE. Fading screen to black")
			ENDIF
			
			IF g_showerGlobals.bInteriorForceCleanupToGar
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as g_showerGlobals.bInteriorForceCleanupToGar = TRUE")
			ENDIF
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as NETWORK_IS_GAME_IN_PROGRESS is FALSE")
			ENDIF
			
			IF g_showerGlobals.bInteriorForceCleanupNoFade
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] SHOWER script is being killed as bInteriorForceCleanupNoFade is TRUE")
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH_MED] Shower: [MP safehouse] Mid level shower: Clean up 2...")
			
			IF g_showerGlobals.bInteriorForceCleanup
				IF IS_SCREEN_FADED_OUT()
					g_showerGlobals.bScreenFadedByForcedClosedShower = TRUE
					CLEANUP_SHOWER_ACTIVITY()
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ENDIF
			ELSE
				CLEANUP_SHOWER_ACTIVITY()
			ENDIF
		ENDIF
			
	ENDWHILE
ENDSCRIPT
