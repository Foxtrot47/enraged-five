// Includes
USING "ob_safehouse_common.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mr_raspberry_jam.sc
//		DESCRIPTION		:	Trevor picks up Mr Raspberry Jam and plays a line of dialogue
//
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE,
	TS_WAIT_FOR_PLAYER,
	TS_GRAB_PLAYER,
	TS_RUN_ANIM,
	TS_TRIGGER_EXIT_ANIM,
	TS_RESET
ENDENUM
TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE

// Variables
INT				mSynchedScene
CAMERA_INDEX	CamIndex
//WEAPON_TYPE		storedWeapon
INT				iCurrentAnim   	= GET_RANDOM_INT_IN_RANGE(0,4)
BOOL			bBreakoutAnim	= FALSE
BOOL			bAnimsLoaded 	= FALSE
BOOL			bConvStarted
INT				iConvTimer
INT 			iUsageStat

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC CLEANUP_RASPBERRY_JAM_ACTIVITY()
	
	CPRINTLN(DEBUG_AMBIENT, "Cleaning up...")
	SHOW_DEBUG_FOR_THIS_OBJECT()
	
	// Release audio
	RELEASE_AMBIENT_AUDIO_BANK()
	
	// Clean up the audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
		STOP_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
	ENDIF
	
	// Clear help message
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
		CLEAR_HELP(TRUE)
	ENDIF
	
	IF bAnimsLoaded
		REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_THIS_ACTIVITY())
	ENDIF
	
	// Restore control
	SAFE_RESTORE_PLAYER_CONTROL()
	
	SHOW_DEBUG_FOR_THIS_OBJECT()
	CPRINTLN(DEBUG_AMBIENT, "...is terminated")
	
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Checks if the anim dict has loaded and flags it up if so
/// RETURNS:
///    True if the anim dict has loaded
FUNC BOOL HAS_MR_JAM_ANIM_DICT_LOADED()

	IF HAS_THIS_ANIM_DICT_LOADED()
		bAnimsLoaded = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC
	

FUNC STRING GET_LINE_FOR_THIS_ANIM()

	STRING sString
	
	SWITCH iCurrentAnim
		CASE 0	sString = "SHRJ_0"	BREAK
		CASE 1	sString = "SHRJ_1"	BREAK
		CASE 2	sString = "SHRJ_2"	BREAK
		CASE 3	sString = "SHRJ_3"	BREAK
	ENDSWITCH
	
	RETURN sString
	
ENDFUNC

/// PURPOSE:
///    How long should we wait before playing Trevor's RJ comment?
/// RETURNS:
///    Delay required in milliseconds
FUNC INT GET_RASPBERRY_DIALOGUE_DELAY()
	// Return the correct dialogue delay
	SWITCH iCurrentAnim
		CASE 0	// anim ig_7_howcouldisayno_cam
			RETURN 3600
		BREAK
		CASE 1	// anim ig_7_ifuwanttodothat_cam
			RETURN 4000
		BREAK
		CASE 2	// anim ig_7_lookatu_cam
			RETURN 3000
		BREAK
		CASE 3	// anim ig_7_smelllikeasea_cam
			RETURN 5500
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Requesting a Mr Raspberry Jam dialogue delay with an invalid index!")
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    
PROC CHECK_FOR_BREAKOUT()

	IF bBreakoutAnim = FALSE
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
		IF iLeftX < -64 OR iLeftX > 64 // Left/Right
		OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
			bBreakoutAnim = TRUE
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Updates the Mr Raspberry Jam interaction
PROC UPDATE_RASPBERRY_JAM_ACTIVITY()
	
	VECTOR vTriggerPos = (GET_TRIGGER_VEC_FOR_THIS_OBJECT())
	VECTOR vTriggerSize = << 1.2, 1.2, 1.2 >>
	FLOAT  fScenePhase  = 0.0
	
	SWITCH eTriggerState
		CASE TS_PLAYER_OUT_OF_RANGE

			IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
			AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(mTrigger), TRIGGER_ANGLE)
				CPRINTLN(DEBUG_AMBIENT, "PLAYER CLOSE TO TRIGGER...")
				SHOW_DEBUG_FOR_THIS_OBJECT()
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
					START_AUDIO_SCENE("TREVOR_SAFEHOUSE_ACTIVITIES_SCENE")
				ENDIF
				
				PRINT_HELP_FOREVER(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
				eTriggerState = TS_WAIT_FOR_PLAYER					
			ENDIF
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
								
					IF NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
																
						CPRINTLN(DEBUG_MISSION, "ACTIVITY TRIGGERED...")
						SHOW_DEBUG_FOR_THIS_OBJECT()
							
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
							CLEAR_HELP(TRUE)
						ENDIF
				
						CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						bSafehouseSetControlOff = TRUE
						//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)	
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						FREEZE_ENTITY_POSITION(mTrigger, FALSE)	
						CPRINTLN(DEBUG_AMBIENT, iCurrentAnim)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						eTriggerState = TS_GRAB_PLAYER
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_TEXT_FOR_THIS_ACTIVITY())
						CLEAR_HELP(TRUE)
					ENDIF
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
			ENDIF
		BREAK
		
		CASE TS_GRAB_PLAYER
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
			
			mSynchedScene = CREATE_SYNCHRONIZED_SCENE(GET_SYNCHED_SCENE_VECTOR(), GET_SYNCHED_SCENE_ROT())
			
			// Apply the anim to the player and all props he uses.
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), mSynchedScene, GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(iCurrentAnim), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(mTrigger, mSynchedScene, GET_ENTER_ANIM_FOR_THIS_ENTITY(iCurrentAnim), GET_ANIM_DICT_FOR_THIS_ACTIVITY(), NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	
			IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(mSynchedScene, FALSE)
			ENDIF
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mr Jam: not using 1st person so creating CamIndex")
				CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, mSynchedScene, GET_CAMERA_FOR_THIS_ACTIVITY(iCurrentAnim), GET_ANIM_DICT_FOR_THIS_ACTIVITY())
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[SH] Mr Jam: using 1st person so not creating CamIndex")
				DESTROY_ALL_CAMS() // Ensure there are no script cams
			ENDIF
						
			// Reset conversation flag
			bConvStarted = FALSE
			iConvTimer = GET_GAME_TIMER() + GET_RASPBERRY_DIALOGUE_DELAY()
			
			eTriggerState = TS_RUN_ANIM
			CPRINTLN(DEBUG_AMBIENT, "MR RASP JAM ANIM STARTING")
		BREAK
		
		CASE TS_RUN_ANIM
		
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
			
				// Handle breakout of animation
				CHECK_FOR_BREAKOUT()
				
				IF bBreakoutAnim
				
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(mSynchedScene)
					FLOAT fReturnStartPhase, fReturnEndPhase
					
					IF FIND_ANIM_EVENT_PHASE(GET_ANIM_DICT_FOR_THIS_ACTIVITY(), GET_ENTER_ANIM_FOR_THIS_ACTIVITY(iCurrentAnim), "WalkInterruptible", fReturnStartPhase, fReturnEndPhase)
						IF fScenePhase >= fReturnStartPhase AND fScenePhase <= fReturnEndPhase
							CPRINTLN(DEBUG_AMBIENT, "[SH] Mr Jam: TS_TRIGGER_EXIT_ANIM during TS_RUN_ANIM")
							eTriggerState = TS_RESET
						ENDIF
					ENDIF
				ENDIF
				
				// See if conversation has started yet
				IF NOT bConvStarted
				AND GET_GAME_TIMER() > iConvTimer
					IF CREATE_CONVERSATION(mWeedConv, "RASPAUD", GET_LINE_FOR_THIS_ANIM(), CONV_PRIORITY_MEDIUM)
						bConvStarted = TRUE
					ENDIF
				ENDIF
	
			ELSE
				eTriggerState = TS_RESET
			ENDIF
		BREAK 
		
		CASE TS_TRIGGER_EXIT_ANIM
			
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(mSynchedScene)
				eTriggerState = TS_RESET
			ENDIF
		BREAK
		
		CASE TS_RESET
			
			IF IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			ENDIF
							
			// Unload audio
			RELEASE_AMBIENT_AUDIO_BANK()
						
			IF STAT_GET_INT(NUM_SH_MR_JAM_USED, iUsageStat)
				STAT_SET_INT(NUM_SH_MR_JAM_USED, iUsageStat+1)
			ENDIF
			
			// Restore control
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bSafehouseSetControlOff = FALSE
			
			// Update current animation
			iCurrentAnim++
			IF iCurrentAnim > 3
				iCurrentAnim = 0
			ENDIF
			
			bBreakoutAnim = FALSE
				
			IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) < (GET_PED_MAX_HEALTH(PLAYER_PED_ID())- HEALTH_BOOST_VALUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) + HEALTH_BOOST_VALUE))
			ENDIF
				
			//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, TRUE)	
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			FREEZE_ENTITY_POSITION(mTrigger, TRUE)
			eTriggerState = TS_PLAYER_OUT_OF_RANGE
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)
	
	CPRINTLN(DEBUG_AMBIENT, "Safehouse Activity Triggered..")
	SHOW_DEBUG_FOR_THIS_OBJECT()
	
	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU) 
		CPRINTLN(DEBUG_AMBIENT, "RASP JAM FORCE CLEANUP OCCURRED")
		CLEANUP_RASPBERRY_JAM_ACTIVITY()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
		CPRINTLN(DEBUG_AMBIENT, "RASP JAM CLEANING UP - on misssion")
		CLEANUP_RASPBERRY_JAM_ACTIVITY()
	ENDIF
	
	IF IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC()
		CPRINTLN(DEBUG_AMBIENT, "RASP JAM CLEANING UP - IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE")
		CLEANUP_RASPBERRY_JAM_ACTIVITY()
	ENDIF
	
	IF DOES_ENTITY_EXIST(mObjectIn)
		mTrigger = mObjectIn	
		FREEZE_ENTITY_POSITION(mTrigger, TRUE)
		mTriggerModel = GET_ENTITY_MODEL(mTrigger)	
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(mWeedConv, 2, PLAYER_PED_ID(), "TREVOR")
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE

		WAIT(0)
				
		IF DOES_ENTITY_EXIST(mTrigger)
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(mTrigger)
			AND NOT IS_ENTITY_DEAD(mTrigger)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND CAN_PLAYER_USE_THIS_OBJECT()
			AND IS_INTERIOR_CORRECT()
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
				SWITCH eState
										
					CASE AS_LOAD_ASSETS						
						IF HAS_MR_JAM_ANIM_DICT_LOADED()
						AND HAS_AUDIO_LOADED()
							eState = AS_RUN_ACTIVITY
						ENDIF
					BREAK
					
					CASE AS_RUN_ACTIVITY
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							UPDATE_RASPBERRY_JAM_ACTIVITY()
							//APPLY_USAGE_EFFECTS()
						ENDIF
					BREAK
					
					CASE AS_END
					BREAK
				ENDSWITCH
			ELSE
				CLEANUP_RASPBERRY_JAM_ACTIVITY()
			ENDIF
		ELSE
			CLEANUP_RASPBERRY_JAM_ACTIVITY()
		ENDIF
	ENDWHILE
ENDSCRIPT
