// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"



// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun


// Clean Up
PROC missionCleanup()
	printDebugString("ob_airdancer Terminated >>>>>>>>>>>>>>>>>\n")
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oAirDancer)

IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP) 
	missionCleanup()
ENDIF


IF DOES_ENTITY_EXIST(oAirDancer)
	 
	FREEZE_ENTITY_POSITION(oAirDancer, TRUE)
	
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oAirDancer)
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oAirDancer)
			SWITCH ambStage
				CASE ambCanRun
					IF DOES_ENTITY_HAVE_DRAWABLE(oAirDancer)
						REQUEST_ANIM_DICT("map_objects")
						IF HAS_ANIM_DICT_LOADED("map_objects")
						
							//IF HAS_MODEL_LOADED(p_airdancer_01_s)
								PLAY_ENTITY_ANIM(oAirDancer, "airdancer_test", "map_objects", 1.0, TRUE, FALSE)
								ambStage = ambRunning
							//ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ambRunning
				
				BREAK
				CASE ambEnd
				
				BREAK
			ENDSWITCH 
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

