// Includes
USING "ob_safehouse_common.sch"
USING "commands_ped.sch"
USING "context_control_public.sch"
USING "freemode_header.sch"
//USING "rgeneral_include.sch"
USING "net_spawn_activities.sch"
USING "net_mission_trigger_public.sch"


// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ob_mp_bed_med.sc
//		DESCRIPTION		:	Handles bed activity in mid-level multiplayer safehouses
//
// *****************************************************************************************
// *****************************************************************************************#




// Enums
ENUM ACTIVITY_STATE
	AS_LOAD_ASSETS = 0,
	AS_RUN_ACTIVITY,
	AS_END
ENDENUM
ACTIVITY_STATE eState = AS_LOAD_ASSETS

ENUM TRIGGER_STATE
	TS_PLAYER_OUT_OF_RANGE = 0,
	TS_WAIT_FOR_PLAYER,
	TS_RUN_ENTRY_ANIM,
	TS_CHECK_ENTRY_ANIM,
	TS_RUN_IDLE_ANIM,
	TS_CHECK_IDLE_ANIM,
	TS_WAIT_FOR_INPUT,
	TS_RUN_EXIT_ANIM,
	TS_CHECK_EXIT_ANIM,
	TS_RESET
ENDENUM

STRUCT PlayerBroadcastData
	INT iBedID = -1
	INT iAnimVar = -1
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

TRIGGER_STATE eTriggerState = TS_PLAYER_OUT_OF_RANGE
INT iPlayerStateBitSet
CONST_INT BIT_INDEX_WEARING_HELMET   0
PED_COMP_NAME_ENUM eStoredMask 
BOOL bSwappedChemSuit = FALSE
INT iStoredHatTex
INT iStoredHat

FLOAT	AREA_WIDTH

// Variables
BOOL 			bSetUpAnimDict		= FALSE
BOOL			bSleeping			= FALSE
BOOL 			bInTransition		= FALSE

CAMERA_INDEX  	mCam

FLOAT 			fTriggerHead
FLOAT 			fTriggerHead1


INT 			iCurProperty		
INT 			iContextIntention 	= NEW_CONTEXT_INTENTION
INT 			iContextIntentionRight = NEW_CONTEXT_INTENTION
INT				iContextIntentionCH = NEW_CONTEXT_INTENTION
INT 			iContextIntentionWH = NEW_CONTEXT_INTENTION
INT				iLocalScene
INT 			iScene
INT				iActivityID			= ci_APT_ACT_BED_HI

//NAVDATA			mNavStruct
BOOL bInitOffsets

STRING 			sAnimDict 				= "mp_bedmid"
//STRING 			sEnterAnim			= "f_getin_l_bighouse"
//STRING 			sIdleAnim 			= "f_sleep_l_loop_bighouse"
//STRING			sExitAnim			= "f_getout_l_bighouse"

//VECTOR 			vBedPos

//VECTOR			vCamPos, vCamHead
//FLOAT			fCamFov				= 45.0
//
//VECTOR			vExitCamPos, vExitCamHead
//FLOAT			fExitCamFov			= 50.0

//VECTOR 			vPlayerPos

VECTOR			vTriggerAreaA, vTriggerAreaB
//VECTOR			vTriggerAreaA1, vTriggerAreaB1
VECTOR			vTriggerAreaA2, vTriggerAreaB2
VECTOR			vTriggerAreaA3, vTriggerAreaB3
VECTOR 			vTriggerAreaA4, vTriggerAreaB4

VECTOR 			vTriggerPos
VECTOR 			vTriggerPos1


INT iBedID = -1
//FLOAT			fTriggerHead
BOOL bRightBedSide

VECTOR 			vScenePos
VECTOR 			vSceneHead
VECTOR 			vScenePos1
VECTOR 			vSceneHead1

VECTOR 			vScenePos2
VECTOR 			vSceneHead2
FLOAT 			fTriggerHead2
VECTOR 			vTriggerPos2

VECTOR vScenePos3
VECTOR vSceneHead3
VECTOR vTriggerPos3
FLOAT fTriggerHead3
BOOL bDrunkWakeUp = FALSE


#IF IS_DEBUG_BUILD
VECTOR vTempDebugVector
#ENDIF

//VECTOR vYachtBed1 = <<-1565.0925, -4089.3171, 5.7800>>
//VECTOR vYachtBed2 = <<-1872.8811, -4088.8394, 5.7800>>
//VECTOR vYachtBed3 = <<-1593.2943, -4080.7271, 5.7800>>
// PC CONTROL FOR GETTING OUT OF BED
BOOL bPCCOntrolsSetup = FALSE



FUNC STRING GET_ENTER_ANIM( BOOL bRightSide = FALSE )
	STRING sReturnString = "f_getin_l_bighouse"

	IF bRightSide = FALSE
		sReturnString = "f_getin_l_bighouse"
	ELSE
		sReturnString = "f_getin_r_bighouse"
	ENDIF
		
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTER_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

FUNC STRING GET_EXIT_ANIM( BOOL bRightSide = FALSE  )
	STRING sReturnString = "f_getout_l_bighouse"

	IF bRightSide = FALSE
		sReturnString = "f_getout_l_bighouse"
	ELSE
		sReturnString = "f_getout_r_bighouse"
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_EXIT_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

FUNC STRING GET_IDLE_ANIM( BOOL bRightSide = FALSE  )
	STRING sReturnString = "f_sleep_l_loop_bighouse"

	IF bRightSide = FALSE
		sReturnString = "f_sleep_l_loop_bighouse"
	ELSE
		sReturnString = "f_sleep_r_loop_bighouse"
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_IDLE_ANIM: ", sReturnString)
	RETURN sReturnString
	
ENDFUNC

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("SAFEHOUSE ACTIVITY")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_NEW_BUSINESS_INTERIOR()
	SWITCH iCurProperty
		CASE PROPERTY_BUS_HIGH_APT_1
		CASE PROPERTY_BUS_HIGH_APT_2
		CASE PROPERTY_BUS_HIGH_APT_3
		CASE PROPERTY_BUS_HIGH_APT_4
		CASE PROPERTY_BUS_HIGH_APT_5
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up assets for bed script script
PROC CLEANUP_BED_ACTIVITY()
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY")
//	SET_LOCAL_PLAYER_INVISIBLE_LOCALLY  
//	SET_PLAYER_INVISIBLE_LOCALLY
	g_showerGlobals.bPauseSkyCamForSpawn = FALSE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MP Bed Script Cleaning up...")
	
	// Clear the context intention
	IF iContextIntention > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntention)")
		RELEASE_CONTEXT_INTENTION(iContextIntention)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntention: ", iContextIntention)
	ENDIF
	
	IF iContextIntentionRight > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntentionRight)")
		RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntentionRight: ", iContextIntentionRight)
	ENDIF
	
	IF iContextIntentionCH > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntentionCH)")
		RELEASE_CONTEXT_INTENTION(iContextIntentionCH)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntentionCH: ", iContextIntentionCH)
	ENDIF
	
	IF iContextIntentionWH > -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: RELEASE_CONTEXT_INTENTION(iContextIntentionWH)")
		RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: iContextIntentionWH: ", iContextIntentionWH)
	ENDIF
	
	// Clear any text
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: CLEAR_HELP")
		CLEAR_HELP()
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: Help text not detected")
	ENDIF
	
	CLEANUP_PC_CONTROLS()
	
	// If the script has ended with the player sleeping (going into PSN store etc), get the ped out of bed.
	IF bSleeping
//	//AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//		IF NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_STREET)
//		AND NOT IS_BIT_SET(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//			
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: TASK_PLAY_ANIM_ADVANCED")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: vScenePos: ", vScenePos, ", vSceneHead: ", vSceneHead)
//			TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDict, sExitAnim, vScenePos, vSceneHead, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT)
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BED_ACTIVITY: CLEARING EXIT BITS")
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_STREET)
//			CLEAR_BIT(iPropertyAllExitBitset, ALL_EXIT_GARAGE)
//		ENDIF
	ENDIF
		
	// Unload audio
	RELEASE_AMBIENT_AUDIO_BANK()
		
	// Remove camera
	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	
		// Remove animation dictionary
	IF bSetupAnimDict
		REMOVE_ANIM_DICT(sAnimDict)
	ENDIF
	g_showerGlobals.bPauseSkyCamForSpawn = FALSE
	IF bSafehouseSetControlOff
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
	AND IS_SKYSWOOP_AT_GROUND()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF						

	// End script
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Requests and loads the correct animation dictionary
FUNC BOOL HAS_ANIM_DICT_LOADED_FOR_BED()
		
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		RETURN FALSE
	ENDIF
	bSetupAnimDict = TRUE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check the player is alone in the trigger area
FUNC BOOL IS_PLAYER_ALONE_IN_AREA(VECTOR vTriggerArea1, VECTOR vTriggerArea2)

	PED_INDEX mPed[8]
	INT iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
	
	IF iPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(mPed[i])
				IF IS_ENTITY_IN_ANGLED_AREA(mPed[i], vTriggerArea1, vTriggerArea2, AREA_WIDTH)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


FUNC INT GENERATE_ANIM_VAR_INT(INT iExculdeInt = -1) 
	INT iRandInt 
	iRandInt = GET_RANDOM_INT_IN_RANGE(1, 5)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_ANIM_VAR_INT: iExculdeInt = ", iExculdeInt)
	
	WHILE (iRandInt = iExculdeInt)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "iRandInt was unsuitable: ", iRandInt, ", so regenerating")
		iRandInt = GET_RANDOM_INT_IN_RANGE(1, 5)
	ENDWHILE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GENERATE_ANIM_VAR_INT returning AnimVar: ", iRandInt)
	RETURN iRandInt
ENDFUNC

FUNC STRING GET_RIGHT_BEDSIDE_ANIM_DICT(INT iVar)
	
	SWITCH iVar
		CASE 1
			RETURN "anim@mp_bedmid@right_var_01"
		BREAK
		CASE 2
			RETURN "anim@mp_bedmid@right_var_02"
		BREAK
		CASE 3
			RETURN "anim@mp_bedmid@right_var_03"
		BREAK
		CASE 4
			RETURN "anim@mp_bedmid@right_var_04"
		BREAK
	ENDSWITCH
	RETURN "anim@mp_bedmid@right_var_01"
ENDFUNC

FUNC STRING GET_LEFT_BEDSIDE_ANIM_DICT(INT iVar)
	
	SWITCH iVar
		CASE 1
			RETURN "anim@mp_bedmid@left_var_01"
		BREAK
		CASE 2
			RETURN "anim@mp_bedmid@left_var_02"
		BREAK
		CASE 3
			RETURN "anim@mp_bedmid@left_var_03"
		BREAK
		CASE 4
			RETURN "anim@mp_bedmid@left_var_04"
		BREAK
	ENDSWITCH
	RETURN "anim@mp_bedmid@left_var_01"
ENDFUNC


FUNC BOOL IS_PROPERTY_A_VEHICLE_WAREHOUSE()
	
	SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()

	IF eSimpleInterior > SIMPLE_INTERIOR_INVALID
	AND eSimpleInterior < SIMPLE_INTERIOR_END
		// Check if the local player is in a vehicle warehouse.
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInterior) = SIMPLE_INTERIOR_TYPE_IE_GARAGE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


PROC ASSIGN_ANIM_VARIATION()
	PLAYER_INDEX playerToCheck
	INT i
	BOOL bFoundPlayerInBed = FALSE
	IF NOT bFoundPlayerInBed
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				playerToCheck = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(playerToCheck, FALSE)
				AND playerToCheck != PLAYER_ID()
					IF NOT IS_PLAYER_IN_YACHT_PROPERTY(PLAYER_ID())
						IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), playerToCheck, TRUE)
						AND NOT IS_PLAYER_ENTERING_PROPERTY(playerToCheck)
							IF playerBD[NATIVE_TO_INT(playerToCheck)].iBedID = playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID
							AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID != -1
								bFoundPlayerInBed = TRUE
								playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: Other player: ", GET_PLAYER_NAME(playerToCheck)," is playering " , playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar, " so setting player ", GET_PLAYER_NAME(PLAYER_ID()), " has been assigned var ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same bed as local player, is in bed = ", playerBD[NATIVE_TO_INT(playerToCheck)].iBedID)
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_YACHT_PLAYER_IS_ON(playerToCheck) > -1
							IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(playerToCheck)
								IF playerBD[NATIVE_TO_INT(playerToCheck)].iBedID = playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID
								AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID != -1
									bFoundPlayerInBed = TRUE
									playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar)
									#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: Other player: ", GET_PLAYER_NAME(playerToCheck)," is playering " , playerBD[NATIVE_TO_INT(playerToCheck)].iAnimVar, " so setting player ", GET_PLAYER_NAME(PLAYER_ID()), " has been assigned var ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
									#ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same bed as local player, is in bed = ", playerBD[NATIVE_TO_INT(playerToCheck)].iBedID)
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CDEBUG2LN(DEBUG_SAFEHOUSE, "ASSIGN_ANIM_VARIATION: ", GET_PLAYER_NAME(playerToCheck), " is not in the same yacht as ", GET_PLAYER_NAME(playerToCheck))
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDREPEAT
	ENDIF
	
	// No one was found in the same bed
	IF NOT bFoundPlayerInBed
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT()
		IF IS_PROPERTY_A_VEHICLE_WAREHOUSE()
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = GENERATE_ANIM_VAR_INT(1) // Var 1 will task the ped to walk into a position in the wall
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Updates sofa activity
PROC UPDATE_BED_ACTIVITY()
	#IF IS_DEBUG_BUILD
//		PRINTLN("POD: BED: UPDATE_BED_ACTIVITY: CASE: ", eTriggerState)
	#ENDIF
//	VECTOR vTriggerSize = << 1.5, 1.5, 1.5 >>
//	FLOAT  fScenePhase  = 0.0
	
	//DRAW_DEBUG_AREA(vTriggerAreaA, vTriggerAreaB, 3.0)
	
	BOOL bUseLeftBedSide = TRUE
	STRING sAnimName
	
	IF IS_PROPERTY_A_VEHICLE_WAREHOUSE()
	OR IS_PROPERTY_A_BUNKER()
	OR IS_PROPERTY_A_NIGHTCLUB()
		IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_RUNNING_SPAWN_ACTIVITY)
			// Dont show prompts when spawn activity is handled by warehouse script
			EXIT
		ENDIF
	ENDIF
	
	SWITCH eTriggerState
		
		CASE TS_PLAYER_OUT_OF_RANGE
						
			IF iPersonalAptActivity = iActivityID //ci_APT_ACT_BED_HI
				iPersonalAptActivity = ci_APT_ACT_IDLE
			ENDIF
			
			// Is this script running a transition scene?
			#IF IS_DEBUG_BUILD
//				PRINTLN("POD: BED: GET_SPAWN_ACTIVITY() = ", GET_SPAWN_ACTIVITY())
//				PRINTLN("POD: BED: IS_PLAYER_SWITCH_IN_PROGRESS = ", IS_PLAYER_SWITCH_IN_PROGRESS())
			#ENDIF
			
			CDEBUG3LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: g_SpawnData.iDrunkRespawnYachtState = ", g_SpawnData.iDrunkRespawnYachtState )
			CDEBUG3LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: g_SpawnData.bPassedOutDrunk  = ", g_SpawnData.bPassedOutDrunk )
			
			IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
			AND NOT IS_PROPERTY_A_VEHICLE_WAREHOUSE()
				IF (g_SpawnData.bPassedOutDrunk and g_SpawnData.iDrunkRespawnYachtState = 2)
				or !g_SpawnData.bPassedOutDrunk
					IF g_SpawnData.iDrunkRespawnYachtState = 2
						bDrunkWakeUp = TRUE
					ENDIF
					
					vSceneHead = vSceneHead1
					vScenePos = vScenePos1
					vTriggerPos = vTriggerPos1
					fTriggerHead = fTriggerHead1
					bRightBedSide = FALSE
					
					STRING sSpawnAnimDict
					sSpawnAnimDict = sAnimDict					
					
					IF IS_PROPERTY_OFFICE(iCurProperty)
					vSceneHead = vSceneHead2
					vScenePos = vScenePos2
					vTriggerPos = vTriggerPos2
					fTriggerHead = fTriggerHead2
					bRightBedSide = TRUE
					sSpawnAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(1)
					ENDIF
					
					IF IS_PROPERTY_CLUBHOUSE(iCurProperty)
					vSceneHead = vSceneHead3
					vScenePos = vScenePos3
					vTriggerPos = vTriggerPos3
					fTriggerHead = fTriggerHead3
					bRightBedSide = TRUE
					sSpawnAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(1)
					ENDIF
					

					eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: FINALIZE_HEAD_BLEND")
					FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
					iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
					
					sAnimName = GET_IDLE_ANIM()

					IF IS_PROPERTY_OFFICE(iCurProperty)
						sAnimName = GET_IDLE_ANIM(IS_PROPERTY_OFFICE(iCurProperty))
					ELIF IS_PROPERTY_CLUBHOUSE(iCurProperty) 
						sAnimName = GET_IDLE_ANIM(IS_PROPERTY_CLUBHOUSE(iCurProperty))
					ENDIF
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sSpawnAnimDict, sAnimName, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)				
					CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: running sync scene")
					NETWORK_START_SYNCHRONISED_SCENE(iScene)
					
					//g_showerGlobals.bPauseSkyCamForSpawn = FALSE
					//CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_PLAYER_OUT_OF_RANGE: g_showerGlobals.bPauseSkyCamForSpawn = FALSE")
					
					eTriggerState = TS_RUN_EXIT_ANIM	

				ENDIF
			ELSE
//				IF MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(iActivityID) //(ci_APT_ACT_BED_HI)	
//					IF IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
					IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
					AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
					#IF FEATURE_GTAO_MEMBERSHIP
					AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
					#ENDIF
					AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
					AND NOT IS_PED_SPRINTING(PLAYER_PED_ID())

						// Restrict players from playing left bed side anims when in a biker clubhouse.
							IF IS_PROPERTY_CLUBHOUSE(iCurProperty)
								bUseLeftBedSide = FALSE
							ELSE 
								bUseLeftBedSide = TRUE
							ENDIF

						IF bUseLeftBedSide
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaB, vTriggerAreaA, AREA_WIDTH)
							AND IS_PLAYER_ALONE_IN_AREA(vTriggerAreaB, vTriggerAreaA)	
							
								IF iContextIntention = NEW_CONTEXT_INTENTION
									REGISTER_CONTEXT_INTENTION(iContextIntention, CP_MEDIUM_PRIORITY, "SA_BED_IN")
								ENDIF
								
								IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntention)
									playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = iBedID
									ASSIGN_ANIM_VARIATION()
									sAnimDict = GET_LEFT_BEDSIDE_ANIM_DICT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
									
									
									vSceneHead = vSceneHead1
									vScenePos = vScenePos1
									vTriggerPos = vTriggerPos1
									fTriggerHead = fTriggerHead1
									bRightBedSide = FALSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: CONTEXT_BUTTON_TRIGGERED: bRightBedSide = FALSE")
									eTriggerState = TS_WAIT_FOR_PLAYER
								ENDIF
							ELSE
								RELEASE_CONTEXT_INTENTION(iContextIntention)
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA2, vTriggerAreaB2, AREA_WIDTH)
						AND IS_PLAYER_ALONE_IN_AREA(vTriggerAreaA2, vTriggerAreaB2)
						
							IF iContextIntentionRight = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(iContextIntentionRight, CP_MEDIUM_PRIORITY, "SA_BED_IN")
							ENDIF
							
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntentionRight)
								playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = iBedID
								ASSIGN_ANIM_VARIATION()
								sAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
								
								bRightBedSide = TRUE
								vSceneHead = vSceneHead2
								vScenePos = vScenePos2
								vTriggerPos = vTriggerPos2
								fTriggerHead = fTriggerHead2
								CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: CONTEXT_BUTTON_TRIGGERED: bRightBedSide = True")
								eTriggerState = TS_WAIT_FOR_PLAYER
							ENDIF
						ELSE
							RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA3, vTriggerAreaB3, AREA_WIDTH)
						AND IS_PLAYER_ALONE_IN_AREA(vTriggerAreaA3, vTriggerAreaB3)
							IF iContextIntentionCH = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(iContextIntentionCH, CP_MEDIUM_PRIORITY, "SA_BED_IN")
							ENDIF
							
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntentionCH)
								playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = iBedID
								ASSIGN_ANIM_VARIATION()
								sAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
								
								bRightBedSide = TRUE
								vSceneHead = vSceneHead3
								vScenePos = vScenePos3
								vTriggerPos = vTriggerPos3
								fTriggerHead = fTriggerHead3
								CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: CONTEXT_BUTTON_TRIGGERED: bRightBedSide = True")
								eTriggerState = TS_WAIT_FOR_PLAYER
							ENDIF
						ELSE
							RELEASE_CONTEXT_INTENTION(iContextIntentionCH)
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA4, vTriggerAreaB4, AREA_WIDTH)
						AND IS_PLAYER_ALONE_IN_AREA(vTriggerAreaA4, vTriggerAreaB4)
							IF iContextIntentionWH = NEW_CONTEXT_INTENTION
								CDEBUG1LN(DEBUG_SAFEHOUSE, "registering context intention")
								REGISTER_CONTEXT_INTENTION(iContextIntentionWH, CP_MEDIUM_PRIORITY, "SA_BED_IN")
							ENDIF
							
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextIntentionWH)
								playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = iBedID
								ASSIGN_ANIM_VARIATION()
								sAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar)
								
								bRightBedSide = TRUE
//								vSceneHead = vSceneHead4
//								vScenePos = vScenePos4
								vTriggerPos = vTriggerPos
								fTriggerHead = fTriggerHead
								CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: CONTEXT_BUTTON_TRIGGERED: bRightBedSide = True")
								eTriggerState = TS_WAIT_FOR_PLAYER
							ENDIF
						ELSE
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "issue getting into bed")
							RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
						ENDIF
					ELSE
						RELEASE_CONTEXT_INTENTION(iContextIntention)
						RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
						RELEASE_CONTEXT_INTENTION(iContextIntentionCH)
						RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
						
						#IF IS_DEBUG_BUILD
						
//						IF NOT IS_TRIGGER_AREA_OK(vTriggerPos, vTriggerSize)
//							CDEBUG2LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - trigger area not ok, Trigger area = ", (vTriggerPos))
//						ENDIF
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA, vTriggerAreaB, AREA_WIDTH)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - player not in trigger area. vTriggerAreaA.", vTriggerAreaA, "vTriggerAreaB", vTriggerAreaB)
						ENDIF
						IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(mTrigger))
							CDEBUG2LN(DEBUG_SAFEHOUSE, "CDM: ob_mp_bed_high - interior does not match, Player = ", NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())), ", Brain Object = ", NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(mTrigger)))
						ENDIF
						#ENDIF
					ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: bed - ", iActivityID ," not free!")
//					#ENDIF
//				ENDIF
			ENDIF
			
		BREAK
		
		CASE TS_WAIT_FOR_PLAYER
		
			IF CAN_PLAYER_START_CUTSCENE()	
			AND HAS_ANIM_DICT_LOADED_FOR_BED()
			AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaB, vTriggerAreaA, AREA_WIDTH)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA2, vTriggerAreaB2, AREA_WIDTH)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA3, vTriggerAreaB3, AREA_WIDTH)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA4, vTriggerAreaB4, AREA_WIDTH))
	
				eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
				
				IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
					iStoredHat = GET_PED_PROP_INDEX( PLAYER_PED_ID(), ANCHOR_HEAD)
					iStoredHatTex = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
					SET_BIT(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
				ENDIF			
				
				IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())			
					// If the player is already wearing a helmet whilst getting 
					// into bed, then keep it on.
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, TRUE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER - Setting PED_CONFIG_FLAG: PCF_DontTakeOffHelmet - TRUE")
					#ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] - mid level bed TRIGGERED...")
				
				RELEASE_CONTEXT_INTENTION(iContextIntention)
				RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
				RELEASE_CONTEXT_INTENTION(iContextIntentionCH)
				RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
				
				DISABLE_INTERACTION_MENU()				
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_IN")
					CLEAR_HELP()
				ENDIF
				
				iPersonalAptActivity = iActivityID// ci_APT_ACT_BED_HI
				
				CLEAR_AREA_OF_PROJECTILES(vTriggerPos, 3.0)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
				bSafehouseSetControlOff = TRUE
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

				SETUP_PC_CONTROLS()
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: fTriggerHead = ", fTriggerHead)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerAreaA = ", vTriggerAreaA)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerAreaB = ", vTriggerAreaB)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: High Bed: bInitOffsets: vTriggerPos = ", vTriggerPos)
				
				//mNavStruct.m_fSlideToCoordHeading = fTriggerHead		
				vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDict, GET_ENTER_ANIM( bRightBedSide ), vScenePos, vSceneHead)  
				VECTOR vTempRotation 
				vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, GET_ENTER_ANIM( bRightBedSide ), vScenePos, vSceneHead)  
				fTriggerHead = vTempRotation.Z
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: vTriggerPos = ", vTriggerPos)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "fTriggerHead: ", fTriggerHead)
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "vScenePos: ", vScenePos)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "vSceneHead: ", vSceneHead)
				
//				VECTOR vTempPos 
//				vTempPos = vTriggerPos - vScenePos
//				FLOAT fTempFloat 
//				fTempFloat = fTriggerHead - vTempRotation.Z
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "vTriggerPos - vScenePos: ", vTempPos)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "fTriggerHead - vSceneHead: ", fTempFloat)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: vTriggerPos = ", vTriggerPos)
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 5000, fTriggerHead, 0.05)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, fTriggerHead)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_SEAT_PLAYER")
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
					PRINTLN("POD: SET_PED_DESIRED_HEADING: ", GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
				ENDIF
				
				eTriggerState = TS_RUN_ENTRY_ANIM
			ELSE		
				#IF IS_DEBUG_BUILD
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaB, vTriggerAreaA, AREA_WIDTH)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA2, vTriggerAreaB2, AREA_WIDTH)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerAreaA4, vTriggerAreaB4, AREA_WIDTH)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_WAIT_FOR_PLAYER: player isn't in either angled area anymore, therefore going to TS_PLAYER_OUT_OF_RANGE")
					eTriggerState = TS_PLAYER_OUT_OF_RANGE
				ENDIF
				#ENDIF
			ENDIF
		BREAK
		
		CASE TS_RUN_ENTRY_ANIM
						
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			
			
			IF IS_PED_WEARING_HAZ_HOOD_UP(PLAYER_PED_ID())
				SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
				bSwappedChemSuit = TRUE
			ENDIF
			
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)

			REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
			IF IS_PED_WEARING_PILOT_SUIT(PLAYER_PED_ID(), PED_COMP_TEETH)
				PRINTLN("bed: removing pilot suit")
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
			ENDIF
//			// Initialise camera
//			mCam = CREATE_CAMERA()
//			
//			IF DOES_CAM_EXIST(mCam)
//				SET_CAM_ACTIVE(mCam, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				SET_CAM_PARAMS(mCam, vCamPos, vCamHead, fCamFov)
//				SHAKE_CAM(mCam, "HAND_SHAKE", DEFAULT_CAM_SHAKE)
//			ENDIF
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mTrigger)))
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePedToStrafe, TRUE)
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, GET_ENTER_ANIM( bRightBedSide ), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
				 
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
							
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_CHECK_ENTRY_ANIM")
				eTriggerState = TS_CHECK_ENTRY_ANIM
				
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_ENTRY_ANIM	
			
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RUN_IDLE_ANIM")	
				bSleeping = TRUE
				eTriggerState = TS_RUN_IDLE_ANIM
			ENDIF
		
		BREAK
		
		CASE TS_RUN_IDLE_ANIM
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_RUN_IDLE_ANIM	")
			#ENDIF
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) > 0.9
				
				bSleeping = TRUE
				
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead, EULER_YXZ, FALSE, TRUE)
				sAnimName = GET_IDLE_ANIM( bRightBedSide )
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sAnimName, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON) 
								
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
							
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_CHECK_IDLE_ANIM")
				eTriggerState = TS_CHECK_IDLE_ANIM
			ENDIF
			
		BREAK
		
		CASE TS_CHECK_IDLE_ANIM
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_CHECK_IDLE_ANIM	")
			#ENDIF
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iLocalScene, TRUE)				
			
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_WAIT_FOR_INPUT")		
				eTriggerState = TS_WAIT_FOR_INPUT
			ENDIF
			
		BREAK
				
		CASE TS_WAIT_FOR_INPUT
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_WAIT_FOR_INPUT	")
			#ENDIF
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF NOT IS_SELECTOR_ONSCREEN()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					PRINT_HELP_FOREVER("SA_BED_OUT")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
			ENDIF
					
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
			#IF FEATURE_GTAO_MEMBERSHIP
			AND NOT SHOULD_LAUNCH_MEMBERSHIP_PAGE_BROWSER()
			#ENDIF
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				THEFEED_HIDE_THIS_FRAME()
				
				bSleeping = FALSE
				
				// Clear the help message
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SA_BED_OUT")
					CLEAR_HELP()
				ENDIF
							
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				sAnimName = GET_EXIT_ANIM( bRightBedSide )
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sAnimDict, sAnimName, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			                  
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
			
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RUN_EXIT_ANIM")
				
				CLEANUP_PC_CONTROLS()
								
				eTriggerState = TS_CHECK_EXIT_ANIM
			ENDIF
		BREAK
		
		CASE TS_RUN_EXIT_ANIM
		
			#IF IS_DEBUG_BUILD
			PRINTLN("POD: BED: TS_RUN_EXIT_ANIM: IS_SKYSWOOP_IN_SKY: ", IS_SKYSWOOP_IN_SKY())		
			#ENDIF
			IF NOT IS_SKYSWOOP_IN_SKY()
			AND IS_SKYSWOOP_AT_GROUND() 
			
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_IN()
				#IF FEATURE_SHORTEN_SKY_HANG
				AND NOT (g_bSkySwoopDownDidFadeOut)
				#ENDIF
					DO_SCREEN_FADE_IN(1000)
				ENDIF					
	
				bInTransition = TRUE
				
				STRING sSpawnAnimDict
				sSpawnAnimDict = sAnimDict
				
				IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
				AND (IS_PROPERTY_OFFICE(iCurProperty) OR IS_PROPERTY_CLUBHOUSE(iCurProperty) OR IS_PROPERTY_A_VEHICLE_WAREHOUSE()) 
					sSpawnAnimDict = GET_RIGHT_BEDSIDE_ANIM_DICT(1)
				ENDIF
						
				iScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneHead)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iScene, sSpawnAnimDict, GET_EXIT_ANIM( bRightBedSide ), INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)

				PRINTLN("POD: BED: TS_RUN_EXIT_ANIM: Dict: ", sAnimDict, " ANIM: ", GET_EXIT_ANIM(bRightBedSide))
				
				NETWORK_START_SYNCHRONISED_SCENE(iScene)
				
				eTriggerState = TS_CHECK_EXIT_ANIM	
			ENDIF
		BREAK
		
		CASE TS_CHECK_EXIT_ANIM
		
			#IF IS_DEBUG_BUILD
				PRINTLN("POD: BED: in TS_CHECK_EXIT_ANIM")
			#ENDIF
			
			IF NOT bInTransition
				DISABLE_SELECTOR_THIS_FRAME()
				DISABLE_ALL_MP_HUD_THIS_FRAME()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				THEFEED_HIDE_THIS_FRAME()
			ENDIF
			
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iScene)
			                  
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				
				SET_SPAWN_ACTIVITY_OBJECT_SCRIPT_AS_READY(SPAWN_ACTIVITY_BED)
				
				IF bDrunkWakeUp
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				g_showerGlobals.bPauseSkyCamForSpawn = FALSE
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "UPDATE_BED_ACTIVITY: TS_CHECK_EXIT_ANIM: g_showerGlobals.bPauseSkyCamForSpawn = FALSE")
				
//				 Change the camera angle - non-shake
//				IF DOES_CAM_EXIST(mCam)
//					SET_CAM_PARAMS(mCam, vExitCamPos, vExitCamHead, fExitCamFov)
//				ENDIF

				CDEBUG1LN(DEBUG_SAFEHOUSE, "[SH] High level Bed - TS_RESET")		
				eTriggerState = TS_RESET
			ENDIF
			
		BREAK
		
		CASE TS_RESET
			
			DISABLE_SELECTOR_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
			OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
						
			
				IF bDrunkWakeUp
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bDrunkWakeUp = FALSE
				ENDIF
				
				IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: Finishing anim early as BREAK_OUT anim event fired")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: Finishing anim finished at phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						NETWORK_STOP_SYNCHRONISED_SCENE(iLocalScene)
					ENDIF
					
				ENDIF
				
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iAnimVar = -1
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iBedID = -1
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: reseting iAnimVar to -1")
				
				IF bSwappedChemSuit = TRUE
					SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
					bSwappedChemSuit = FALSE					
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eStoredMask, FALSE)
				
				IF IS_BIT_SET(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, iStoredHat, iStoredHatTex)
					CLEAR_BIT(iPlayerStateBitSet, BIT_INDEX_WEARING_HELMET)
				ENDIF
				PRINTLN("POD: BED: REAPPLYING MASK")
				
				ENABLE_INTERACTION_MENU()
				
				iPersonalAptActivity = ci_APT_ACT_IDLE
				
				IF bSafehouseSetControlOff
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bSafehouseSetControlOff = FALSE
				ENDIF
				FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				RELEASE_CONTEXT_INTENTION(iContextIntention)
				RELEASE_CONTEXT_INTENTION(iContextIntentionRight)
				RELEASE_CONTEXT_INTENTION(iContextIntentionCH)
				RELEASE_CONTEXT_INTENTION(iContextIntentionWH)
				
				eTriggerState = TS_PLAYER_OUT_OF_RANGE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "TS_RESET: going to TS_PLAYER_OUT_OF_RANGE")
			ENDIF
			
		BREAK
					
	ENDSWITCH
ENDPROC



// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(OBJECT_INDEX mObjectIn)

	// Default cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP) 
	OR Is_Player_On_Or_Triggering_Any_Mission()
	OR MPGlobalsAmbience.bRunningFmIntroCut
	OR SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Default Cleanup")
		CLEANUP_BED_ACTIVITY()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
	OR (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
	OR NOT DOES_ENTITY_EXIST(mObjectIn)
		IF NOT DOES_ENTITY_EXIST(mObjectIn)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "BED OBJECT DOESN'T EXIST")
		ENDIF 
		CLEANUP_BED_ACTIVITY()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Ready for network launch...")
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(mObjectIn)
				
		mTrigger = mObjectIn
		
//		NETWORK_REGISTER_ENTITY_AS_NETWORKED(mTrigger)
		
//		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR(GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
//			eSimpleInterior = GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[OB_MP_BED_HIGH] IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR returning TRUE.")
//		ELSE
			iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[OB_MP_BED_HIGH] IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR returning FALSE.")
//		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Current property is ", iCurProperty, ", name: ", GET_PROPERTY_ENUM_NAME(iCurProperty))
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: NO TRIGGER OBJECT")
	ENDIF
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Ready for launch...")
	
	IF iBedID >0 
	ENDIF
	
//	ASSIGN_CURRENT_PROPERTY_ID(iCurProperty)
	MP_PROP_OFFSET_STRUCT tempOffset
	MP_PROP_OFFSET_STRUCT offset
	MP_PROP_OFFSET_STRUCT tempOffset1
	MP_PROP_OFFSET_STRUCT tempOffset2
	MP_PROP_OFFSET_STRUCT tempOffset3
	//MP_PROP_OFFSET_STRUCT offset

	// Mission Loop -------------------------------------------//
	WHILE TRUE
			
		WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED)
		AND NOT SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		AND DOES_ENTITY_EXIST(mObjectIn)

			IF iCurProperty <= 0
			AND NOT IS_PROPERTY_A_VEHICLE_WAREHOUSE()
				iCurProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//				iCurProperty = PROPERTY_YACHT_APT_1_BASE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: Current property is ", iCurProperty, ", name: ", GET_PROPERTY_ENUM_NAME(iCurProperty))
				
			
			ELIF iCurProperty > 0
			OR IS_PROPERTY_A_VEHICLE_WAREHOUSE()
				IF IS_PROPERTY_A_VEHICLE_WAREHOUSE()
					
					
						// Bed position within world: <<959.113159, -3007.748047, -40.106758>>
						vTriggerPos 	= GET_ENTITY_COORDS(mObjectIn, FALSE)
						fTriggerHead 	= GET_ENTITY_HEADING(mObjectIn)
						
//						vTriggerAreaA 	= <<960.632629,-3006.805908,-40.634918>>
//						vTriggerAreaB 	= <<957.861206,-3006.800781,-38.634918>> 
						vTriggerAreaA4 	= <<959.281433,-3009.064209,-40.634876>>
						vTriggerAreaB4	= <<959.310120,-3006.321289,-38.634876>> 
						
						vScenePos 		= <<960.363, -3006.056, -39.615>>
						vSceneHead		= <<0.000, -0.000, 136.800>>
						AREA_WIDTH = 2.875
						iBedID = 1
						bInitOffsets = TRUE
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PROPERTY_A_VEHICLE_WAREHOUSE = TRUE")
				ENDIF
				IF NOT bInitOffsets
				
					vTriggerPos1 	= mpProperties[iCurProperty].house.activity[SAFEACT_BED].vTriggerVec 
					fTriggerHead1	= mpProperties[iCurProperty].house.activity[SAFEACT_BED].fTriggerRot
					
					vTriggerAreaA  	= mpProperties[iCurProperty].house.activity[SAFEACT_BED].vAreaAVec
					vTriggerAreaB  	= mpProperties[iCurProperty].house.activity[SAFEACT_BED].vAreaBVec
					
					vScenePos1		= mpProperties[iCurProperty].house.activity[SAFEACT_BED].vSceneVec
					vSceneHead1		= mpProperties[iCurProperty].house.activity[SAFEACT_BED].vSceneRot
					iBedID = 1
					
					IF IS_PROPERTY_OFFICE(iCurProperty)
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vTriggerAreaA		= tempOffset.vLoc
						
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vTriggerAreaB		= tempOffset.vLoc
						
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_SCENE, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vScenePos1 			= tempOffset.vLoc
						vSceneHead1 		= tempOffset.vRot
					ENDIF
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A_R, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vTriggerAreaA2		= tempOffset.vLoc
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B_R, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vTriggerAreaB2		= tempOffset.vLoc
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_SCENE_R, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vScenePos2 			= tempOffset.vLoc
					vSceneHead2 		= tempOffset.vRot
					
					IF IS_PROPERTY_CLUBHOUSE(iCurProperty)
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vTriggerAreaA = offset.vLoc
						
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vTriggerAreaB = offset.vLoc
						
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_SCENE, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
						vScenePos3 = offset.vLoc
						vSceneHead3 = offset.vRot
					ENDIF
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A_R, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vTriggerAreaA3 = offset.vLoc
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B_R, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vTriggerAreaB3 = offset.vLoc
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurProperty, MP_PROP_ELEMENT_BED_SCENE_R, offset, GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty))
					vScenePos3 = offset.vLoc
					vSceneHead3 = offset.vRot

					IF g_iCurrentPropertyVariation 		= 3
					OR g_iCurrentPropertyVariation 		= 7
					OR g_iCurrentPropertyVariation 		= 4
						vScenePos.Z += 0.039
					ELIF g_iCurrentPropertyVariation 	= 2
					OR g_iCurrentPropertyVariation 		= 6
					OR g_iCurrentPropertyVariation 		= 8
						vScenePos.Z += 0.01
					ENDIF
					
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						AREA_WIDTH = 1.5	
					ELSE
						AREA_WIDTH = 2
					ENDIF
					
					IF IS_THIS_NEW_BUSINESS_INTERIOR()
						AREA_WIDTH = 2.75
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurProperty) = PROPERTY_STILT_APT_5_BASE_A
						AREA_WIDTH = 1.75
					ELIF IS_PROPERTY_CUSTOM_APARTMENT(iCurProperty)
						AREA_WIDTH = 2
					ELIF IS_PROPERTY_YACHT_APARTMENT(iCurProperty)
						AREA_WIDTH = 1.75
						INT iYachtID = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmOn//GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
						IF iYachtID  = -1
							iYachtID = 22
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "BED activity: iYachtID = ", iYachtID )
						GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_OBJECT, tempOffset1)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_OBJECT_2, tempOffset2)
						GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_OBJECT_3, tempOffset3)
						IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mObjectIn), tempOffset1.vLoc, 2.0)
														
							fTriggerHead1	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].fTriggerRot
							
							vTriggerAreaA 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].vAreaAVec
							vTriggerAreaB  	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].vAreaBVec
							
							vScenePos1 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].vSceneVec
							vSceneHead1 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].vSceneRot
							
							AREA_WIDTH = 1.75
							
							fTriggerHead2	= 0
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A_R, tempOffset1)
							vTriggerAreaA2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B_R, tempOffset1)
							vTriggerAreaB2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_SCENE_R, tempOffset1)
							
							vScenePos2 			= tempOffset1.vLoc
							vSceneHead2 		= tempOffset1.vRot
							
//							vTriggerAreaA2  	= <<-1483.289795,-3747.465576,4.911377>> 
//							vTriggerAreaB2  	= <<-1484.427246,-3746.913818,6.411377>>
//							
//							vScenePos2 		= << -1483.299, -3747.215, 5.915 >>
//							vSceneHead2 		= << 0.000, -0.000, 110.520 >>

							iBedID = 1
							
						ELIF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mObjectIn), tempOffset2.vLoc, 2.0)
							
							
							fTriggerHead1	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_2].fTriggerRot
							
							vTriggerAreaA 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_2].vAreaAVec
							vTriggerAreaB  	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_2].vAreaBVec
							
							vScenePos1 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_2].vSceneVec
							vSceneHead1 	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_2].vSceneRot
							
							fTriggerHead2	= 0
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A_2_R, tempOffset1)
							vTriggerAreaA2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B_2_R, tempOffset1)
							vTriggerAreaB2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_SCENE_2_R, tempOffset1)
							vScenePos2 			= tempOffset1.vLoc
							vSceneHead2 		= tempOffset1.vRot
							AREA_WIDTH = 1.75
							
//							vTriggerAreaA2  = <<-1476.852417,-3747.311035,4.884243>>
//							vTriggerAreaB2 	= <<-1475.615723,-3747.936523,6.384243>>
//											
//							vScenePos2 		= << -1475.450, -3747.539, 5.915 >>
//							vSceneHead2 	= << 0.000, -0.000, 110.520 >>	

							iBedID = 2
							
						ELIF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mObjectIn), tempOffset3.vLoc, 2.0)
						
							
							fTriggerHead1	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_3].fTriggerRot
							
							vTriggerAreaA  	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_3].vAreaAVec
							vTriggerAreaB  	= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_3].vAreaBVec
							
							vScenePos1 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_3].vSceneVec
							vSceneHead1 		= mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED_3].vSceneRot
							
							fTriggerHead2	= 0
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_A_3_R, tempOffset1)
							vTriggerAreaA2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_TRIGGER_AREA_B_3_R, tempOffset1)
							vTriggerAreaB2		= tempOffset1.vLoc
							
							GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtID, MP_PROP_ELEMENT_BED_SCENE_3_R, tempOffset1)
							vScenePos2 			= tempOffset1.vLoc
							vSceneHead2 		= tempOffset1.vRot
//							vTriggerAreaA2  	= <<-1460.709717,-3761.144287,4.884149>>
//							vTriggerAreaB2  	= <<-1461.607178,-3760.514648,6.384149>> 
//							
//							vScenePos2 		= << -1461.202, -3761.051, 5.915 >>
//							vSceneHead2 	= << 0.000, 0.000, -65.520 >>
							
							AREA_WIDTH = 1.75
							
							iBedID = 3
						ELSE
							VECTOR vTempObjectLoc = GET_ENTITY_COORDS(mObjectIn)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "bInitOffsets: iBedID not found,  GET_ENTITY_COORDS(mObjectIn) = ", vTempObjectLoc )
							CDEBUG1LN(DEBUG_SAFEHOUSE, "bInitOffsets: iBedID not found,  Bed1 = ", tempOffset1.vLoc)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "bInitOffsets: iBedID not found,  Bed2 = ", tempOffset2.vLoc)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "bInitOffsets: iBedID not found,  Bed3 = ", tempOffset3.vLoc)
						ENDIF
						
						VECTOR vTempTrigger 
						vTempTrigger = vTriggerAreaA + vTriggerAreaB
						vTriggerPos = vTempTrigger*0.5						
						
						//If we're on a yacht we need to account for the other beds Independently 
						IF iBedID = 2
							iActivityID = ci_APT_ACT_BED_HI_YACHT_2
						ELIF iBedID = 3
							iActivityID = ci_APT_ACT_BED_HI_YACHT_3
						ENDIF						
						
					ENDIF
					
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: fTriggerHead = ", fTriggerHead)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaA = ", vTriggerAreaA)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaB = ", vTriggerAreaB)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerPos = ", vTriggerPos)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: fTriggerHead2 = ", fTriggerHead2)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaA2 = ", vTriggerAreaA2)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaB2 = ", vTriggerAreaB2)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerPos2 = ", vTriggerPos2)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vScenePos2 ", vScenePos2)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vSceneHead2 ", vSceneHead2)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: fTriggerHead3 = ", fTriggerHead3)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaA3 = ", vTriggerAreaA3)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaB3 = ", vTriggerAreaB3)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerPos3 = ", vTriggerPos3)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vScenePos3 ", vScenePos3)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vSceneHead3 ", vSceneHead3)
					
					/*
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: fTriggerHead4 = ", fTriggerHead4)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaA4 = ", vTriggerAreaA4)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerAreaB4 = ", vTriggerAreaB4)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: vTriggerPos4 = ", vTriggerPos4)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vScenePos4 ", vScenePos4)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vSceneHead4 ", vSceneHead4)
					*/
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "High Bed: bInitOffsets: iBedID = ", iBedID)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vScenePos ", vScenePos)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: bInitOffsets: vSceneHead ", vSceneHead)
					
					
					IF fTriggerHead < 0.0
						fTriggerHead+= 360.0
					ENDIF
						
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: vTriggerPos = ", vTriggerPos)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: fTriggerHead = ", fTriggerHead)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: vTriggerAreaA = ", vTriggerAreaA)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: vTriggerAreaB = ", vTriggerAreaB)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: vScenePos = ", vScenePos)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: vSceneHead = ", vSceneHead)
					
					IF NOT ARE_VECTORS_EQUAL(vTriggerAreaA, <<0, 0, 0>>)
						bInitOffsets = TRUE	
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Bed activity retrieved origin coords for bed coords, so waiting till bedPropertyStruct is referencing the correct data structure")
					ENDIF
					
					
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mObjectIn), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 5//SOFA_TRIGGER_DIST					
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: < SOFA_TRIGGER_DIST: eState: ", eState)
						SWITCH eState
												
							CASE AS_LOAD_ASSETS			
								IF HAS_ANIM_DICT_LOADED_FOR_BED()
									CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Assets loaded for activity...")
									eState = AS_RUN_ACTIVITY
								ENDIF
							BREAK
							
							CASE AS_RUN_ACTIVITY
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								
									UPDATE_BED_ACTIVITY()
								ENDIF
							BREAK
							
							CASE AS_END
							BREAK
							
						ENDSWITCH
					ELSE
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "[Safehouse] High Bed: > SOFA_TRIGGER_DIST: ", GET_DISTANCE_BETWEEN_COORDS(vTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)))
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mObjectIn), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+3
							#IF IS_DEBUG_BUILD
							vTempDebugVector = GET_ENTITY_COORDS(PLAYER_PED_ID())
							VECTOR vTempDebugVectorObjectCoords = GET_ENTITY_COORDS(mObjectIn)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, 1, object: ",vTempDebugVectorObjectCoords, " player:", vTempDebugVector)
							FLOAT fTempFloat  = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mObjectIn), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) 
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, distance from object: ",fTempFloat)
							#ENDIF
							
							CLEANUP_BED_ACTIVITY()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(mObjectIn), 6, 0, 255, 0, 128)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mObjectIn), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > SOFA_TRIGGER_DIST+3
				#IF IS_DEBUG_BUILD
				vTempDebugVector = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vTempDebugVectorObjectCoords = GET_ENTITY_COORDS(mObjectIn)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, 2, object: ",vTempDebugVectorObjectCoords, " player:", vTempDebugVector)
				FLOAT fTempFloat  = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mObjectIn), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) 
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[MP safehouse] High Bed: Player has moved far enough away, distance from object: ",fTempFloat)
				#ENDIF
				
				CLEANUP_BED_ACTIVITY()
			ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(mObjectIn)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "BED OBJECT DOESN'T EXIST")
			ENDIF
			CLEANUP_BED_ACTIVITY()
		ENDIF
			
	ENDWHILE
ENDSCRIPT
