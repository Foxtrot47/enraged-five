// SteveR 8.4.13 - Rewrite to support multiple TVs.
// ChrisM 19.8.8
// Adapted from Dave Bruce's original TV script

USING "rage_builtins.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "types.sch"
USING "commands_vehicle.sch"
USING "commands_ped.sch"
USING "commands_streaming.sch"
USING "commands_hud.sch"
USING "commands_task.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_debug.sch"
USING "commands_player.sch"
USING "commands_pad.sch"
USING "commands_camera.sch"
USING "commands_graphics.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_clock.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "commands_entity.sch"
USING "context_control_public.sch"

USING "cellphone_public.sch"

USING "script_player.sch"
USING "LineActivation.sch"
USING "cutscene_public.sch"

USING "tv_control_public.sch"

USING "finance_modifiers_public.sch"

structTimer tmrContextButtonDelay

CONST_FLOAT DEFAULT_TV_VOLUME -4.0

ENUM MAIN_STAGES
	TV_STATE_INITIALISE = 0,
	TV_STATE_OFF,
	TV_STATE_SETUP,
	TV_STATE_ON,
	TV_STATE_ON_CONTROLS,
	TV_STATE_SUSPEND,
	TV_STATE_CLEANUP
ENDENUM
MAIN_STAGES main_TV_stages = TV_STATE_INITIALISE

CONST_FLOAT TRIGGER_ANGLE 90.0

FLOAT fCurrentVolume
VECTOR TV_position
FLOAT fHeading
INT iRenderTarget = -1
BOOL bTVScriptTurnedPlayerControlOff = FALSE
//INT iChannelForThisTV = 0

OBJECT_INDEX objectTV
OBJECT_INDEX objectOverlay
OBJECT_INDEX objectProjector
OBJECT_INDEX objectOrigTV

RAYFIRE_INDEX iTVObject // For setting Jimmy's TV destroyed.

//VIEWPORT_INDEX GameViewport
INT iContextButtonIntention = NEW_CONTEXT_INTENTION

TV_LOCATION eTVLocation = TV_LOC_NONE
TVCHANNELTYPE eTVChannelType = TVCHANNELTYPE_CHANNEL_NONE
TV_CHANNEL_PLAYLIST eTVChannelPlaylist = TV_PLAYLIST_NONE
//BOOL bMovieStarted = FALSE

TEXT_LABEL_63 radioEmitter = "NULL"
TEXT_LABEL_63 audioScene = "NULL"


#IF IS_DEBUG_BUILD
TEXT_LABEL_63 locationName= "NONE"
#ENDIF

CAMERA_INDEX cameraTV
VECTOR vCamPos
VECTOR vCamRot

BOOL bCamerasAreActive = FALSE

// TURNS ON LOTS OF TV DEBUG SPEW.
#IF IS_DEBUG_BUILD
BOOL bShowDebug = FALSE
#ENDIF

BOOL bIsTVCurrentlyIndestructible = FALSE

BOOL bIsCustomControlSchemeActive = FALSE

// =======================================================================


PROC ACTIVATE_CUSTOM_CONTROLS()

	IF IS_PC_VERSION()
	
		IF bIsCustomControlSchemeActive = FALSE
			INIT_PC_SCRIPTED_CONTROLS("TV_Controls")
			bIsCustomControlSchemeActive = TRUE
		ENDIF
	
	ENDIF

ENDPROC

PROC SHUTDOWN_CUSTOM_CONTROLS()

	IF IS_PC_VERSION()
	
		IF bIsCustomControlSchemeActive = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bIsCustomControlSchemeActive = FALSE
		ENDIF
	
	ENDIF

ENDPROC


/// PURPOSE:
/// 
///    
///    
PROC RELEASE_TV_RENDER_TARGET()
	
	WAIT(0)
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		RELEASE_NAMED_RENDERTARGET("tvscreen")
		iRenderTarget = -1
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - RELEASE_TV_RENDER_TARGET() - RENDER TARGET RELEASED" )
	ENDIF
	
ENDPROC

PROC START_TV_CAMERA() 
	FLOAT FOV = 50
	IF bCamerasAreActive = FALSE
		
		cameraTV = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, FOV)
		SET_CAM_FAR_CLIP(cameraTV, 100)
		SET_CAM_ACTIVE(cameraTV, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		IF DOES_ENTITY_EXIST( objectProjector )
			SET_ENTITY_VISIBLE(objectProjector,FALSE)
		ENDIF
		
		// Freeze the player
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
						
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				bTVScriptTurnedPlayerControlOff = TRUE
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - START_TV_CAMERA() -TURNED OFF PLAYER CONTROL" )
			ENDIF
			
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), TV_position, -1)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		IF NOT ARE_STRINGS_EQUAL(audioScene, "NULL")
			IF NOT IS_AUDIO_SCENE_ACTIVE( audioScene )
				START_AUDIO_SCENE( audioScene )
			ENDIF
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
		
		ENABLE_MOVIE_SUBTITLES(TRUE)
		
		bCamerasAreActive = TRUE
	ENDIF
ENDPROC


PROC STOP_TV_CAMERA()
	
	IF bCamerasAreActive
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)

		IF IS_CAM_ACTIVE(cameraTV)
			SET_CAM_ACTIVE(cameraTV,FALSE)
		ENDIF

		DESTROY_CAM(cameraTV)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		
		// Unfreeze the player
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), TV_position, 1)
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF bTVScriptTurnedPlayerControlOff = TRUE
					bTVScriptTurnedPlayerControlOff = FALSE
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - START_TV_CAMERA() -TURNED ON PLAYER CONTROL" )
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - START_TV_CAMERA() -PLAYER CONTROL WEAS TURNED OFF BY ANOTHER SCRIPT, NOT TURNING IT ON." )
				ENDIF
			ENDIF
			
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		IF NOT ARE_STRINGS_EQUAL(audioScene,"NULL")
			IF IS_AUDIO_SCENE_ACTIVE( audioScene )
				STOP_AUDIO_SCENE( audioScene )
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST( objectProjector )
			SET_ENTITY_VISIBLE(objectProjector,TRUE)
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
		
		ENABLE_MOVIE_SUBTITLES(FALSE)
		
		bCamerasAreActive = FALSE
		
	ENDIF
	
ENDPROC


PROC HANDLE_TV_CONTROLS()

	IF bCamerasAreActive = FALSE
	
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_SCRIPT_RUP )
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
			RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
			REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "TV_HLP6")
			START_TV_CAMERA()
		ENDIF
	
	ELSE
	
		DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE) // Disable alternate pause so Esc quits.
	
		// Force the player invisible if for some reason he gets made visible again.
		IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// Turn back off Player control if another script turns it on, but only if we have already turned if off.
		IF bTVScriptTurnedPlayerControlOff
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HANDLE_TV_CONTROLS() - WARNING! Another script turned player control on! Turning it back off." )
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			ENDIF
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		
		SET_TV_PLAYER_WATCHING_THIS_FRAME(PLAYER_PED_ID())
	
		RUN_TV_TRANSPORT_CONTROLS()
		
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_SCRIPT_RUP )
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE))
		
			RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
			
			STOP_TV_CAMERA()
			
			// Re-print help message
			//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP5")
			//	CLEAR_HELP(TRUE)
			//	PRINT_HELP("TV_HLP5")
			//ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC


PROC CLEAR_SCREEN()

	#IF IS_DEBUG_BUILD
		IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - RENDER TARGET IS REGISTERED" )
		ENDIF
	
		IF DOES_ENTITY_EXIST(objectTV)
			IF IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(objectTV))
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - RENDER TARGET IS LINKED!")
			ENDIF
		ENDIF
		
	#ENDIF
	
	IF NOT (iRenderTarget = -1)
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - RENDER TARGET VALID" )
		SET_TEXT_RENDER_ID(iRenderTarget)
		IF DOES_ENTITY_EXIST(objectTV)
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - ENTITY_EXISTS" )
			IF GET_ENTITY_MODEL(objectTV) = V_ILev_MM_Screen2
			OR GET_ENTITY_MODEL(objectTV) = V_ILev_MM_Scre_Off
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - ENTITY IS MICHAEL PROJECTOR" )
				SET_ENTITY_VISIBLE(objectTV, FALSE)
				
				IF GET_ENTITY_MODEL(objectTV) = V_ILev_MM_Scre_Off
					DRAW_RECT(0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)  // Clears the projector render target to white.
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CLEAR_SCREEN() - ENTITY IS GENERIC TV" )
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)  // Clears the render target to black.
			ENDIF
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(objectProjector)
		SET_ENTITY_VISIBLE(objectProjector, FALSE)
	ENDIF

ENDPROC


PROC DELETE_SCREEN()
					
		RELEASE_TV_RENDER_TARGET()
		
		IF DOES_ENTITY_EXIST(objectTV)
			IF GET_ENTITY_MODEL(objectTV) = V_ILEV_MM_SCREEN2
				DELETE_OBJECT(objectTV)
				SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_MM_SCREEN2)
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - DELETE_SCREEN() - PROJECTOR SCREEN OBJECT DELETED AND MODEL RELEASED" )
			ENDIF
		ENDIF
				
		IF DOES_ENTITY_EXIST( objectProjector )
			DELETE_OBJECT(objectProjector)
			SET_MODEL_AS_NO_LONGER_NEEDED(V_ILev_MM_Screen2_VL)
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - DELETE_SCREEN() - PROJECTOR LIGHT OBJECT DELETED AND MODEL RELEASED" )
		ENDIF
		
		IF DOES_ENTITY_EXIST(objectOverlay)
			DELETE_OBJECT(objectOverlay)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_TT_screenstatic)
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - DELETE_SCREEN() - TV STATIC OBJECT DELETED AND MODEL RELEASED" )
		ENDIF
		
ENDPROC

PROC NORMALISE_TV_SCALEFORM_DIMENSIONS(FLOAT &width)
	FLOAT fAspectRatio 		= GET_ASPECT_RATIO( FALSE )
	IF fAspectRatio <= ( 16.0 / 9.0 )
		FLOAT fAspectMultiplier = fAspectRatio / ( 16.0 / 9.0 )
		FLOAT tempW = width 
		width 		= tempW * fAspectMultiplier
	ENDIF
ENDPROC

PROC DRAW_TV_TEXTURE_TO_RENDERTARGET()
	FLOAT fWidth	= 1.0
	NORMALISE_TV_SCALEFORM_DIMENSIONS( fWidth )
	
    SET_TEXT_RENDER_ID(iRenderTarget)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	DRAW_TV_CHANNEL( 0.5, 0.5, fWidth, 1.0, 0.0, 255, 255, 255, 255 )
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
ENDPROC

VECTOR triggerPos

//BOOL first_damage_done = FALSE

PROC CREATE_NEW_SCREEN()

	IF DOES_ENTITY_EXIST(objectTV)
		IF GET_ENTITY_MODEL(objectTV) = V_ILev_MM_Screen2
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CREATE_NEW_SCREEN() - PROJECTOR ALREADY EXITS... EXITING" )
			EXIT
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CREATE_NEW_SCREEN() - objectTV DOESN'T EXIST!" )
	ENDIF
	
	
	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - CREATE_NEW_SCREEN() - CREATING PROJECTOR SCREEN" )
	
	// Projector
	objectTV = NULL
	objectTV = CREATE_OBJECT_NO_OFFSET(V_ILev_MM_Screen2, TV_position)//V_ILev_MM_Screen
	SET_ENTITY_HEADING(objectTV, fHeading)
	FREEZE_ENTITY_POSITION(objectTV, TRUE)
	SET_ENTITY_VISIBLE(objectTV, FALSE)
		
	// Volume light
	objectProjector = NULL
	objectProjector = CREATE_OBJECT_NO_OFFSET(V_ILev_MM_Screen2_VL, TV_position)
	SET_ENTITY_HEADING(objectProjector, fHeading)
	FREEZE_ENTITY_POSITION(objectProjector, TRUE)
	SET_ENTITY_VISIBLE(objectProjector, FALSE)
		
ENDPROC

PROC CREATE_FUZZ_OVERLAY()
	objectOverlay = CREATE_OBJECT_NO_OFFSET(Prop_TT_screenstatic, TV_position)
	SET_ENTITY_HEADING(objectOverlay, fHeading)
	SET_ENTITY_VISIBLE(objectOverlay, TRUE)
	FREEZE_ENTITY_POSITION(objectOverlay, TRUE)
ENDPROC

PROC GET_TV_INFO()
	
	PRINTNL()
	PRINTSTRING("TV script launched at ")
	TV_position = GET_ENTITY_COORDS(objectTV)
	fHeading = GET_ENTITY_HEADING(objectTV)
	
	RESTORE_STANDARD_CHANNELS()
	PRINTVECTOR(TV_position)
	
	// TV_LOC_FRANKLIN_LIVING
	
	IF GET_ENTITY_MODEL(objectTV) = PROP_TV_03
		PRINTSTRING(" using model PROP_TV_03")
		IF VDIST(TV_position, <<-9.541955,-1440.916626,31.346916>>) < 3
			PRINTSTRING(" in location TV_LOC_FRANKLIN_LIVING.")
			eTVLocation = TV_LOC_FRANKLIN_LIVING
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_FRANKLIN_LIVING"
			#ENDIF
			
			vCamPos 			=  <<-9.8135, -1440.9128, 31.3654>> 
			vCamRot 			=  <<-0.0000, 0.0000, -134.3211>>
						
			triggerPos = << -9.3078, -1440.9310, 30.1015 >>
			
			radioEmitter = "SE_FRANKLIN_AUNT_HOUSE_RADIO_01"
			
			audioScene = "TV_FRANKLINS_HOUSE_SOCEN"
						
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, <<-9.541955,-1440.916626,31.346916>>))
			PRINTSTRING(" from a chosen location.")
		ENDIF
		
	// TV_LOC_TREVOR_TRAILER
		
	ELIF GET_ENTITY_MODEL(objectTV) = prop_trev_tv_01
		PRINTSTRING(" using model prop_trev_tv_01")
		IF VDIST(TV_position, <<1978.425049,3819.657227,34.267632>>) < 3
			PRINTSTRING(" in location TV_LOC_TREVOR_TRAILER.")
			
			eTVLocation = TV_LOC_TREVOR_TRAILER
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_TREVOR_TRAILER"
			#ENDIF
			
			vCamPos 			= <<1978.2303, 3819.6504, 34.2724>> 
			vCamRot 			= <<-0.0000, 0.0000, -105.1500>>

			triggerPos = << 1978.3303, 3819.7170, 32.4501 >>
			
			CREATE_FUZZ_OVERLAY()
			
			radioEmitter = "SE_TREVOR_TRAILER_RADIO_01"
			
			audioScene = "TV_TREVORS_TRAILER"
									
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, <<1978.425049,3819.657227,34.267632>>))
			PRINTSTRING(" from a chosen location.")	
		ENDIF
		
	// TV_LOC_FRANKLIN_VINEWOOD
	
	ELIF GET_ENTITY_MODEL(objectTV) = PROP_TV_FLAT_01
		PRINTSTRING(" using model PROP_TV_FLAT_01")
		IF VDIST(TV_position, << 3.6654, 529.8486, 173.6281 >>) < 3
			PRINTSTRING(" in location TV_LOC_FRANKLIN_VINEWOOD.")
			eTVLocation = TV_LOC_FRANKLIN_VINEWOOD
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_FRANKLIN_VINEWOOD"
			#ENDIF
			
			vCamPos 			= <<2.5724, 527.9989, 176.1619>>
			vCamRot 			= <<-0.0000, 0.0000, -29.9488>>

			triggerPos = << 3.6654, 529.8486, 173.6281 >>
			radioEmitter = "SE_FRANKLIN_HILLS_HOUSE_RADIO_01"
			audioScene = "TV_FRANKLINS_HOUSE_VINEWOOD"
							
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, << 3.6654, 529.8486, 173.6281 >>))
			PRINTSTRING(" from a chosen location.")	
		ENDIF
		
	// TV_LOC_TREVOR_VENICE
	
	ELIF GET_ENTITY_MODEL(objectTV) = PROP_TV_FLAT_02
		PRINTSTRING(" using model PROP_TV_FLAT_02")
		IF VDIST(TV_position, <<-1160.694702,-1520.744751,10.491680>>) < 3
			PRINTSTRING(" in location TV_LOC_TREVOR_VENICE.")
			eTVLocation = TV_LOC_TREVOR_VENICE
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_TREVOR_VENICE"
			#ENDIF
			
			vCamPos 			= <<-1160.5024, -1520.7598, 10.7393>> 
			vCamRot 			= <<0.0000, 0.0000, 60.0610>>

			triggerPos =  <<-1160.1429, -1520.4946, 9.6555>>
			radioEmitter = "TREVOR_APARTMENT_RADIO"
			audioScene = "TV_FLOYDS_APARTMENT"
						
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, <<-1160.694702,-1520.744751,10.491680>>))
			PRINTSTRING(" from a chosen location.")	
		ENDIF
		
	// TV_LOC_MICHAEL_PROJECTOR
	
	ELIF (GET_ENTITY_MODEL(objectTV) = V_ILEV_MM_SCREEN2 OR GET_ENTITY_MODEL(objectTV) = V_ILev_MM_Scre_Off)
	
		PRINTSTRING(" using model V_ILEV_MM_SCREEN2")
		IF VDIST(TV_position, <<-802.252747,173.037430,74.357079>>) < 3
			PRINTSTRING(" in location TV_LOC_MICHAEL_PROJECTOR.")
			eTVLocation = TV_LOC_MICHAEL_PROJECTOR
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_MICHAEL_PROJECTOR"
			#ENDIF
			
			vCamPos 			= <<-802.8972, 172.5370, 74.5801>>
			vCamRot 			= <<-0.0000, 0.0000, -69.0273>>
			
			triggerPos = << -800.7292, 173.2194, 71.8348 >>
			radioEmitter = "SE_MICHAELS_HOUSE_RADIO"
			audioScene = "TV_MICHAELS_HOUSE"
			
					
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, <<-802.252747,173.037430,74.357079>>))
			PRINTSTRING(" from a chosen location.")	
		ENDIF
		
	// 	DES_TVSMASH_START (Jimmy's)
	
	ELIF (GET_ENTITY_MODEL(objectTV) = DES_TVSMASH_START)
		PRINTSTRING(" using model DES_TVSMASH_START")
		IF VDIST (TV_position, <<-809.9620, 170.9190, 75.7407>>) < 3
			PRINTSTRING(" in location TV_LOC_JIMMY_BEDROOM.")
			eTVLocation = TV_LOC_JIMMY_BEDROOM
			
			#IF IS_DEBUG_BUILD
			locationName = "TV_LOC_JIMMY_BEDROOM"
			#ENDIF
			
			vCamPos 			= <<-808.3051, 171.2623, 77.2822>> 
			vCamRot 			= <<1.8886, 0.0000, 110.9232>>
			
			triggerPos = <<-809.9620, 170.9190, 75.7407>>
			radioEmitter = "SE_MICHAELS_HOUSE_RADIO"
			audioScene = "TV_MICHAELS_HOUSE"
						
		ELSE
			PRINTSTRING(" but prop is ")
			PRINTFLOAT(VDIST(TV_position, <<-802.252747,173.037430,74.357079>>))
			PRINTSTRING(" from a chosen location.")
		ENDIF
		
	ENDIF

	PRINTNL()
ENDPROC


FUNC BOOL HAS_THE_TV_BEEN_SWITCHED_OFF_BY_PLAYER()

		#IF IS_DEBUG_BUILD
			IF bShowDebug
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_OFF_BY_PLAYER()" )
			ENDIF
		#ENDIF
		
		IF NOT IS_THIS_TV_FORCED_ON(eTVLocation)
		
			#IF IS_DEBUG_BUILD
				IF bShowDebug
					CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_OFF_BY_PLAYER() - NOT FORCED ON" )
				ENDIF
			#ENDIF
	
			IF GET_TIMER_IN_SECONDS(tmrContextButtonDelay) >= 1.0 // Slight delay to prevent the tv turning off when you turn it on.
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), triggerPos, <<1.0, 1.0, 1.5>>, FALSE)
			AND GET_INTERIOR_FROM_ENTITY(objectTV) = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), TV_position, TRIGGER_ANGLE)
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			

			//AND g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = TRUE
			
				IF iContextButtonIntention = NEW_CONTEXT_INTENTION
					ACTIVATE_CUSTOM_CONTROLS()
					REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "TV_HLP5")
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						RESTART_TIMER_NOW(tmrContextButtonDelay)
						g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
						IF REQUEST_AMBIENT_AUDIO_BANK( "SAFEHOUSE_MICHAEL_SIT_SOFA" )
							PLAY_SOUND_FRONTEND(-1, "MICHAEL_SOFA_TV_ON_MASTER" ) 
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
				
				HANDLE_TV_CONTROLS()
				
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP5")
					CLEAR_HELP()
				ENDIF
				SHUTDOWN_CUSTOM_CONTROLS()
				RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
			ENDIF
			
		ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_THE_TV_BEEN_SWITCHED_ON_BY_PLAYER()
		
	IF GET_TIMER_IN_SECONDS(tmrContextButtonDelay) < 1.0 // Slight delay to prevent the tv turning off when you turn it on.
	
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - TIMER DELAY" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
		RETURN FALSE
		
	ENDIF
	

	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), triggerPos, <<1.0, 1.0, 1.5>>, FALSE)
	OR NOT IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), TV_position, TRIGGER_ANGLE)
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())

		
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - PLAYER NOT AT COORD, OR WRONG ANGLE" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
			
		RETURN FALSE

	ENDIF
	
	IF GET_INTERIOR_FROM_ENTITY(objectTV) != GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - PLAYER NOT IN TV INTERIOR" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
		RETURN FALSE

	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - TRANSITION HUD DISPLAYING" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)

		RETURN FALSE

	ENDIF
	

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
	#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - PED IS IN A VEHICLE" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
		RETURN FALSE

	ENDIF
	
	
	IF g_bTriggerSceneActive
	
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - TRIGGER SCENE IS ACTIVE" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
		RETURN FALSE

	ENDIF
	
	
	IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled
	
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - TV CONTROLS DISABLED" )
		ENDIF
		#ENDIF
		
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
		RETURN FALSE
		
	ENDIF
			
	IF iContextButtonIntention = NEW_CONTEXT_INTENTION
		
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - NEW CONTEXT INTENTION" )
		ENDIF
		#ENDIF
	
		REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "TV_HLP1")
				
		
		RETURN FALSE
		
	ENDIF
		
	#IF IS_DEBUG_BUILD
	IF bShowDebug
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - HAS_TV_BEEN_SWITCHED_ON_BY_PLAYER() - CHECKING CONTEXT BUTTON" )
	ENDIF
	#ENDIF
	
	IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		RESTART_TIMER_NOW(tmrContextButtonDelay)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = TRUE
		
		IF REQUEST_AMBIENT_AUDIO_BANK( "SAFEHOUSE_MICHAEL_SIT_SOFA" )
			PLAY_SOUND_FRONTEND(-1, "MICHAEL_SOFA_TV_ON_MASTER" ) 
		ENDIF
		
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_WTCHTV)
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC


PROC END_WATCHING_TV()
	

	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - END_WATCHING_TV()" )
	
	IF eTVLocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(GET_TV_CHANNEL())
		//g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(TV_PLAYLIST_NONE)
	ENDIF
	
	fCurrentVolume = GET_TV_VOLUME()
	
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)

	STOP_TV_CAMERA()
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family5")) = 0
		IF NOT ARE_STRINGS_EQUAL(radioEmitter, "NULL")
			SET_STATIC_EMITTER_ENABLED(radioEmitter, TRUE)
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - END_WATCHING_TV() TURNED ON RADIO EMITTER ",radioEmitter )
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("family5")) > 0
			CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - END_WATCHING_TV() FAMILY 5 RUNNING - RADIO EMITTER NOT ENABLED ",radioEmitter )
		ENDIF
	#ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP6")
		CLEAR_HELP()
	ENDIF
	
	CLEAR_SCREEN()
	
	RELEASE_TV_RENDER_TARGET()
	
	IF DOES_ENTITY_EXIST(objectOverlay)
		IF NOT IS_ENTITY_VISIBLE(objectOverlay)
			SET_ENTITY_VISIBLE(objectOverlay, TRUE)
		ENDIF
	ENDIF
	
	ENABLE_MOVIE_SUBTITLES(FALSE)
	SHUTDOWN_CUSTOM_CONTROLS()

ENDPROC


PROC SUSPEND_TV()
	

	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SUSPEND_TV()" )

	g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
	
	g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(GET_TV_CHANNEL())
	
	fCurrentVolume = GET_TV_VOLUME()
	
	//IF NOT ARE_STRINGS_EQUAL(radioEmitter, "NULL")
	//	SET_STATIC_EMITTER_ENABLED(radioEmitter, TRUE)
	//ENDIF
	
	
	IF IS_AUDIO_SCENE_ACTIVE( audioScene )
		STOP_AUDIO_SCENE( audioScene )
	ENDIF

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP6")
		CLEAR_HELP()
	ENDIF
	
	RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
	
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	
	WAIT (0)
	
	CLEAR_SCREEN()
	
	RELEASE_TV_RENDER_TARGET()
		
ENDPROC


FUNC BOOL IS_PLAYER_IN_TV_LOCATION( TV_LOCATION eTVloc )

	//INTERIOR_INSTANCE_INDEX playerInteriorIndex = GET_INTERIOR_FROM_ENTITY( PLAYER_PED_ID())
	
	VECTOR vPlayerCoords = GET_PLAYER_COORDS( PLAYER_ID())
	
	
	
	SWITCH eTVloc
	
		CASE TV_LOC_FRANKLIN_LIVING
		CASE TV_LOC_FRANKLIN_VINEWOOD
		CASE TV_LOC_TREVOR_TRAILER
		CASE TV_LOC_TREVOR_VENICE
		CASE TV_LOC_GUN_CLUB
			RETURN TRUE
		BREAK
	
		
		CASE  TV_LOC_MICHAEL_PROJECTOR
		
			IF vPlayerCoords.z < 74.0
			
			//IF playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-813.4175, 179.7046, 71.1592>>) // Front Hallway
			//IF playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-807.0659, 175.2585, 71.8447>>) // Lounge	
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-797.3823, 178.7603, 71.8453>>) // Dining Room
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-801.8658, 181.6975, 71.6055>>) // Kitchen
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-805.0380, 181.5118, 71.3578>>) // Back Hall
				RETURN TRUE
			ENDIF
			
		BREAK
		
		
		CASE TV_LOC_JIMMY_BEDROOM
	
			IF vPlayerCoords.z > 75.0
			//IF playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-807.7032, 171.5911, 75.7504>>) // Son's bedroom
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-801.6771, 173.7775, 75.7407>>) // Daughter's bedroom
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-812.8871, 179.7412, 75.7504>>) // Master bedroom 
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-811.3488, 175.4640, 75.7504>>) // Wardrobe
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-804.5829, 177.8812, 75.7483>>) // Upper Hall
			//OR playerInteriorIndex = GET_INTERIOR_AT_COORDS(<<-802.9606, 168.0437, 75.7407>>) // Bathroom
				RETURN TRUE
			ENDIF

		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC SCRIPT_CLEANUP()
	
	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SCRIPT CLEANUP()" )
	
	IF eTVLocation = TV_LOC_NONE
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SCRIPT CLEANUP() - NO CLEANUP NEEDED AS TV NOT SUPPORTED" )
		TERMINATE_THIS_THREAD()
	ENDIF
	
	RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
		
	IF eTVLocation != TV_LOC_NONE
	
		END_WATCHING_TV()
		
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bAvailableForUse = FALSE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled = FALSE
		
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = FALSE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV = FALSE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn = FALSE
			
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType  = ENUM_TO_INT(TVCHANNELTYPE_CHANNEL_NONE)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(TV_PLAYLIST_NONE)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsIndestructible = FALSE
				
	ENDIF
		
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP6")
		CLEAR_HELP()
	ENDIF
	
	WAIT (0) // This is necessary to allow the TV screen to be cleared properly before the screen is deleted and the script terminated.
	
	DELETE_SCREEN()
	
	IF IS_AUDIO_SCENE_ACTIVE( audioScene )
		STOP_AUDIO_SCENE( audioScene )
	ENDIF
	
	UNHINT_AMBIENT_AUDIO_BANK()
	
	SHUTDOWN_CUSTOM_CONTROLS()

	TERMINATE_THIS_THREAD()
ENDPROC


PROC SETUP_SCREEN()

	MODEL_NAMES TVModel
	
	IF eTVLocation = TV_LOC_MICHAEL_PROJECTOR
		CREATE_NEW_SCREEN()
	ENDIF
	
	// Release the rendertarget if already registered.
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - RENDER TARGET ALREADY REGISTERED - RELEASING" )		
		RELEASE_NAMED_RENDERTARGET("tvscreen")
	ENDIF
	
	WAIT(0)

	IF NOT DOES_ENTITY_EXIST(objectTV)
		CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - objectTV DOES NOT EXIST!" )		
		SCRIPT_CLEANUP()	
	ENDIF
	
	TVModel = GET_ENTITY_MODEL(objectTV)
	
	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - REGISTERING RENDER TARGET" )

	REGISTER_NAMED_RENDERTARGET("tvscreen")
	LINK_NAMED_RENDERTARGET(TVModel)
	
	WAIT(0)
	
	IF eTVLocation <> TV_LOC_MICHAEL_PROJECTOR
	
		// New loop to continuously try to get and link a render target if the first attempt fails.
		WHILE NOT IS_NAMED_RENDERTARGET_LINKED(TVModel)
			
			// Cleanup if the TV object isn't valid any more.
			IF NOT DOES_ENTITY_EXIST(objectTV)
				SCRIPT_CLEANUP()
			ENDIF
		
			// Cleanup if the player is out of range.
			IF NOT IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(objectTV)
				SCRIPT_CLEANUP()
			ENDIF
			
			// Try to get a new rendertarget and link it.
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - RENDER TARGET NOT REGISTERED - RETRYING" )
				REGISTER_NAMED_RENDERTARGET("tvscreen")
			ENDIF
			
			IF NOT IS_NAMED_RENDERTARGET_LINKED(TVModel)
				CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - RENDER TARGET NOT LINKED - RETRYING" )
				LINK_NAMED_RENDERTARGET(TVModel)
			ENDIF
			
			WAIT(0)
		
		ENDWHILE

	ENDIF

	
	CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - SETUP_SCREEN() - RENDER TARGET LINKED - " )
	
	iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID("tvscreen")
		
	CLEAR_SCREEN()
	
ENDPROC

PROC SETUP_DEBUG()
	#IF IS_DEBUG_BUILD	
	TEXT_LABEL_63 TVDebugString = ""
	TVDebugString += "ob_TV debug "
	TVDebugString += locationName
	START_WIDGET_GROUP( TVDebugString )		
	ADD_WIDGET_BOOL("Turn on TV debug spew", bShowDebug)
	STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC

PROC RUN_DEBUG()
	//debug shit for testing purposes
	#IF IS_DEBUG_BUILD
//		IF NOT g_bForceStartAmbientTV
//			IF bPlayInvader
//			PLAY_TV_CHANNEL_WITH_PLAYLIST(TVCHANNELTYPE_CHANNEL_1, TV_PLAYLIST_SPECIAL_INVADER, TRUE)
//				bPlayInvader = FALSE
//			ENDIF
//			IF bPlayInvaderExp
//				PLAY_TV_CHANNEL_WITH_PLAYLIST(TVCHANNELTYPE_CHANNEL_SCRIPT, TV_PLAYLIST_SPECIAL_INVADER_EXP, TRUE)
//				bPlayInvaderExp = FALSE
//			ENDIF
//		ENDIF
	#ENDIF
ENDPROC


PROC HANDLE_TV_INDESTRUCTIBILITY()

	IF eTVLocation = TV_LOC_NONE
		EXIT
	ENDIF
	

	IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsIndestructible = FALSE 
	
		IF bIsTVCurrentlyIndestructible = TRUE
		
			PRINTLN("OB_TV: ",locationName," IS NOW DESTRUCTIBLE")
		
			
			IF DOES_ENTITY_EXIST(objectOrigTV)
				SET_ENTITY_INVINCIBLE(objectOrigTV, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objectTV)
				SET_ENTITY_INVINCIBLE(objectTV, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objectProjector)
				SET_ENTITY_INVINCIBLE(objectProjector, FALSE)
			ENDIF
								
			IF DOES_ENTITY_EXIST(objectOverlay)
				SET_ENTITY_INVINCIBLE(objectOverlay, FALSE)
			ENDIF
				
			bIsTVCurrentlyIndestructible = FALSE
			
		ENDIF
	
	ELSE
	
		IF  bIsTVCurrentlyIndestructible = FALSE
		
			PRINTLN("OB_TV: ",locationName," IS NOW INDESTRUCTIBLE")
		
			IF DOES_ENTITY_EXIST(objectOrigTV)
				SET_ENTITY_INVINCIBLE(objectOrigTV, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objectTV)
				SET_ENTITY_INVINCIBLE(objectTV, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objectProjector)
				SET_ENTITY_INVINCIBLE(objectProjector, TRUE)
			ENDIF
								
			IF DOES_ENTITY_EXIST(objectOverlay)
				SET_ENTITY_INVINCIBLE(objectOverlay, TRUE)
			ENDIF
				
			bIsTVCurrentlyIndestructible = TRUE
			
		ENDIF
	
	ENDIF

ENDPROC


SCRIPT (OBJECT_INDEX TV)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN( DEBUG_AMBIENT, "OB_TV: FORCED CLEANUP! ", locationName  )
		SCRIPT_CLEANUP()
	ENDIF
	
	IF DOES_ENTITY_EXIST(TV)
		objectTV = TV
		objectOrigTV = TV
	ENDIF
	
	//CPRINTLN( DEBUG_AMBIENT, "OB_TV: TEMPORARILY DISABLED!", locationName  )
	
	//TERMINATE_THIS_THREAD()
	
//=================================
	WHILE TRUE
		WAIT(0)
		RUN_DEBUG()
		
		
					
		IF DOES_ENTITY_EXIST(TV)
		
			// Fix for B* 1335636 - cleanup script if TV is lead-in is active.
		
			IF eTVLocation != TV_LOC_NONE
			AND (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
			AND NOT IS_THIS_TV_FORCED_ON(eTVLocation)
				CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV FORCE SHUT DOWN DUE TO MISSION LEADIN BEING ACTIVE ", locationName  )
				SCRIPT_CLEANUP()
			ENDIF
			
			// Fix for B* 1766471 - cleanup script if switch is in progress.
		
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
			AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT
				CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV FORCE SHUT DOWN DUE TO PLAYER SWITCH BEING IN PROGRESS ", locationName  )
				SCRIPT_CLEANUP()
			ENDIF
			
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV FORCE SHUT DOWN DUE TO PLAYER IN ANIMAL FORM ", locationName  )
				SCRIPT_CLEANUP()
			ENDIF
			
			//CPRINTLN( DEBUG_AMBIENT, "OB_TV: ", locationName, "HEALTH: ", GET_ENTITY_HEALTH(TV)  )
			
			IF HAS_OBJECT_BEEN_BROKEN(TV)
				CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV FORCE SHUT DOWN DUE TO BROKEN TV ", locationName  )
				SCRIPT_CLEANUP()
			ENDIF
		
			IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE (TV)
					
				IF IS_PLAYER_PLAYING(PLAYER_ID())
				
					HANDLE_TV_INDESTRUCTIBILITY()
				
					IF GET_ENTITY_HEALTH(TV) < 950
						CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV BROKEN! ", locationName  )
						SCRIPT_CLEANUP()
					ENDIF
					
					SWITCH main_TV_stages
					
						CASE TV_STATE_INITIALISE
							
							// Initialise some basic vars
						
							GET_TV_INFO()
							
							// Smash TV after Family 2, and mend after Family 4
							IF eTVLocation = TV_LOC_JIMMY_BEDROOM
							
								IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2) = TRUE
								AND GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4) = FALSE
							
									iTVObject = GET_RAYFIRE_MAP_OBJECT( <<-809.9620, 170.9190, 75.7407>>, 3, "des_tvsmash" )
									IF DOES_RAYFIRE_MAP_OBJECT_EXIST(iTVObject)
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(iTVObject, RFMO_STATE_ENDING) 
									ENDIF
									
									CPRINTLN( DEBUG_AMBIENT, "OB_TV: JIMMY'S TV SMASHED, SO SHUTTING DOWN.")
								
									TERMINATE_THIS_THREAD()
									
								ENDIF
							
							ENDIF
												
							
							
							IF eTVLocation = TV_LOC_NONE
							
								CPRINTLN( DEBUG_AMBIENT, "OB_TV: NO LOCATION! MODEL: ", GET_MODEL_NAME_FOR_DEBUG( GET_ENTITY_MODEL(TV) ))
								
								SCRIPT_CLEANUP()
									
							ELSE
							
								SETUP_DEBUG()
								
								HINT_AMBIENT_AUDIO_BANK( "SAFEHOUSE_MICHAEL_SIT_SOFA" )
								REGISTER_SCRIPT_WITH_AUDIO()
							
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn = FALSE
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bAvailableForUse = TRUE
								g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = GET_RANDOM_INT_IN_RANGE(0, 2)
								g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(TV_PLAYLIST_NONE)
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = FALSE
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled = FALSE
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsIndestructible = FALSE
							
								fCurrentVolume = DEFAULT_TV_VOLUME
								
								RESTART_TIMER_NOW(tmrContextButtonDelay)
															
								SETUP_SCREEN()
								
								// This allows the screen to be cleared, but prevents anything from
								// rendering to the TV when it's not in use.
								RELEASE_TV_RENDER_TARGET()

								WAIT(0)
				
								main_TV_stages = TV_STATE_OFF
							ENDIF
						BREAK
						
						CASE TV_STATE_OFF
						
					
							#IF IS_DEBUG_BUILD
								IF bShowDebug
									CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_OFF - ForceStartAmbient = ",g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV  )
								ENDIF
							#ENDIF
													
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								#IF IS_DEBUG_BUILD
									IF bShowDebug
										CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_OFF - WANTED LEVEL DETECTED!"  )
									ENDIF
								#ENDIF
								WAIT(0)
							ELSE
								
								IF IS_ENTITY_STATIC(objectTV)	
								AND IS_ENTITY_UPRIGHT(objectTV)
								
									IF HAS_THE_TV_BEEN_SWITCHED_ON_BY_PLAYER()
									OR g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV
										main_TV_stages = TV_STATE_SETUP
									ENDIF

								ELSE
								
									CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_OFF - TV IS NOT STATIC OR NOT UPRIGHT" )
									RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
								ENDIF
								
							ENDIF
							
							
							
						BREAK
						
												
						CASE TV_STATE_SETUP
						
							CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_SETUP - Stored channel = ", g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType )
			
							SETUP_SCREEN()
							
							IF NOT IS_THIS_TV_FORCED_ON(eTVLocation)
								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TV_HLP1")
									CLEAR_HELP()
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(objectTV)
								IF NOT IS_ENTITY_VISIBLE(objectTV)
									SET_ENTITY_VISIBLE(objectTV, TRUE)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(objectProjector)
								IF NOT IS_ENTITY_VISIBLE(objectProjector)
									SET_ENTITY_VISIBLE(objectProjector, TRUE)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(objectOverlay)
								IF IS_ENTITY_VISIBLE(objectOverlay)
									SET_ENTITY_VISIBLE(objectOverlay, FALSE)
								ENDIF
							ENDIF
	
							IF NOT ARE_STRINGS_EQUAL(radioEmitter, "NULL")
								SET_STATIC_EMITTER_ENABLED(radioEmitter, FALSE)
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_SETUP() TURNED OFF RADIO EMITTER ",radioEmitter )
							ENDIF
							
							SET_TV_AUDIO_FRONTEND(FALSE)
							
							WAIT(0)
							
							// Attach to original object, not the new object (e.g. the projector) to prevent audio attaching issues.
							IF DOES_ENTITY_EXIST(objectOrigTV)
								ATTACH_TV_AUDIO_TO_ENTITY(objectOrigTV)
							ENDIF
											
							// Player controlled TV
							IF NOT IS_THIS_TV_FORCED_ON(eTVLocation)
							//OR g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT( TV_PLAYLIST_NONE )
								 
								//RESTORE_STANDARD_CHANNELS()
								
								// If we're still on the script or special channel, then randomise the starting channel.
								IF g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(TVCHANNELTYPE_CHANNEL_SCRIPT)
								OR  g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(TVCHANNELTYPE_CHANNEL_SPECIAL)
								OR  g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(TVCHANNELTYPE_CHANNEL_NONE)
									g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = GET_RANDOM_INT_IN_RANGE(0, 2)
								ENDIF
								
								SET_TV_CHANNEL(INT_TO_ENUM(TVCHANNELTYPE,g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType))
								SET_TV_VOLUME(fCurrentVolume)
							
							ELSE
							
							// TV forced on by a script
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_SETUP - FORCED ON " )
								
								//g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartTV = FALSE
								
								eTVChannelType = INT_TO_ENUM( TVCHANNELTYPE, g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType)
								eTVChannelPlaylist = INT_TO_ENUM( TV_CHANNEL_PLAYLIST, g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist)
								
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " PLAYLIST ", g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist )
								
								//IF eTVChannelType = GET_TV_CHANNEL()
								SET_TV_CHANNEL_PLAYLIST( eTVChannelType, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(eTVChannelPlaylist), g_TVStruct[ENUM_TO_INT(eTVLocation)].bStartPlaylistFromBeginning )				 
								SET_TV_CHANNEL(eTVChannelType)
								//ENDIF
								
								// If the TV has been forced on, but also set as player controlled, then turn off the FORCED ON flag.
								// This allows the player to turn off the tv, and manipulate its controls, even if a script has forced it on.
								IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled 
									g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV = FALSE
									//g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartTV = FALSE
									g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
								ENDIF
							
							ENDIF
																
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn = TRUE
							
							BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_TVTICK_FOR_RIM)
							BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_TVTICK_FOR_WAP)
							BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_TVTICK_FOR_WIW)
							main_TV_stages = TV_STATE_ON
							
						BREAK
												
						CASE TV_STATE_ON
						
							#IF IS_DEBUG_BUILD
								IF bShowDebug
									CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_ON, CHANNEL = ", GET_TV_CHANNEL() )
								ENDIF
							#ENDIF
							
	
							IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " TV_STATE_ON - FORCED STOPPED" )
								main_TV_stages = TV_STATE_CLEANUP
							ENDIF
							
							// Restart the TV if a script wants to make use of it whilst the player is using it.
							IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled AND IS_THIS_TV_FORCED_ON(eTVLocation)
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " TV_STATE_ON - FORCED START AMBIENT" )
								g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
								main_TV_stages = TV_STATE_CLEANUP
							ENDIF
									
							
							IF NOT IS_PLAYER_IN_TV_LOCATION( eTVLocation )
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: PLAYER OUT OF RANGE OF ", locationName )
								SUSPEND_TV()
								main_TV_stages = TV_STATE_SUSPEND
							ELSE
								
								DRAW_TV_TEXTURE_TO_RENDERTARGET()
								
								// Restore the channel if it gets turned off. This can happen if a nearby TV suspends. 
								IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_NONE
									CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV_STATE_ON - RESTORING TV CHANNEL"  )
									SET_TV_CHANNEL(INT_TO_ENUM(TVCHANNELTYPE,g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType))
								ENDIF
																	
								IF HAS_THE_TV_BEEN_SWITCHED_OFF_BY_PLAYER()
									CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV HAS BEEN TOLD TO STOP! ", locationName  )
									main_TV_stages = TV_STATE_CLEANUP
								ENDIF
								
							ENDIF
							
						BREAK
						
						CASE TV_STATE_SUSPEND
						
							#IF IS_DEBUG_BUILD
								IF bShowDebug
									CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_SUSPEND - Stored channel = ",  g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType )
								ENDIF
							#ENDIF
						
							IF IS_PLAYER_IN_TV_LOCATION( eTVLocation )
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: PLAYER BACK IN RANGE OF ", locationName )
								main_TV_stages = TV_STATE_SETUP
							ENDIF
							
							IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV
								CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_SUSPEND FORCED STOPPED" )
								main_TV_stages = TV_STATE_CLEANUP
							ENDIF
	
						
						BREAK
												
						CASE TV_STATE_CLEANUP
						
							CPRINTLN(DEBUG_AMBIENT, "OB_TV: ", locationName, " - TV_STATE_CLEANUP" )
													
							END_WATCHING_TV()
							
							IF iContextButtonIntention != NEW_CONTEXT_INTENTION
								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
							ENDIF
							
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV = FALSE
							//g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartTV = FALSE
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = FALSE
							g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(TV_PLAYLIST_NONE)
							
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn = FALSE
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled = FALSE
							g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsIndestructible = FALSE
							
							main_TV_stages = TV_STATE_OFF

						BREAK
					ENDSWITCH	
				ENDIF
			ENDIF
		ELSE
			CPRINTLN( DEBUG_AMBIENT, "OB_TV: TV DOESN'T EXIST! ", locationName  )
			SCRIPT_CLEANUP()
			TERMINATE_THIS_THREAD()
		ENDIF
		
	ENDWHILE
	CPRINTLN( DEBUG_AMBIENT, "OB_TV: END OF SCRIPT! ", locationName  )
	SCRIPT_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDSCRIPT	

