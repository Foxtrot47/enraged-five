// Include file for Safehouse activities and TV watching.
USING "commands_debug.sch"
USING "commands_path.sch"
USING "commands_camera.sch"
USING "commands_brains.sch"

USING "brains.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "drunk_public.sch"
USING "friends_public.sch"
USING "flow_mission_data_public.sch"
USING "flow_public_core.sch"
USING "model_enums.sch"
USING "player_ped_public.sch"
USING "script_player.sch"
USING "types.sch"
USING "selector_public.sch"

// =========
// Constants
// =========

CONST_INT	HEALTH_BOOST_VALUE		10
CONST_INT 	iEffectDuration 		30000
CONST_INT 	CAM_DRUNK_AMT			2 			// How many shots before the camera starts wobbling
CONST_INT 	PED_DRUNK_AMT			4	 		// How many shots before the ped starts wobbling
CONST_INT 	MAX_CAMS				4
CONST_INT 	MAX_WINE_CAMS 			4
CONST_INT 	STONED_DIALOGUE_DELAY 	5000

CONST_INT	DEFAULT_CAM_DAMPER		3

CONST_FLOAT TRIGGER_WIDTH 			2.5
CONST_FLOAT	TRIGGER_ANGLE 			90.0
CONST_FLOAT DEFAULT_CAM_SHAKE		0.2

CONST_FLOAT	SAFE_SKIP_PHASE			0.15

// =========
// Variables
// =========

BOOL bDoneWheatgrassDialogue	= FALSE
BOOL bStartStonedDialogue 		= FALSE
BOOL bHasExitCam 				= FALSE

BOOL bNeedSecondProp			= FALSE
BOOL bSeparateExitAnim			= FALSE
BOOL bEntityHasExitAnim			= FALSE
BOOL bIsFranklin 				= FALSE
BOOL bIsTrevor 					= FALSE
BOOL bSafehouseSetControlOff 	= FALSE
BOOL bUseCounter				= TRUE // Play anims based off how many drinks the plays has had, or how drunk he is.

INT iDialogueStartTime
INT iTimesUsed 					= 0

MODEL_NAMES mTriggerModel

OBJECT_INDEX mTrigger

PED_VARIATION_STRUCT sVariationStruct
PED_COMP_NAME_ENUM eReturnItem

structPedsForConversation mWeedConv

VECTOR vWineCamPos[MAX_WINE_CAMS]
VECTOR vWineExitCamPos = <<0.0, 0.0, 0.0>>
VECTOR vWineExitCamRot = <<0.0, 0.0, 0.0>>

// =========
// FUNCTIONS 
// =========

/// PURPOSE:
///    SAve the player's headgear - helmet or mask
PROC REMOVE_PLAYER_MASK()
	
	// Remember what clothes ped is wearing.
	GET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
						
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)		// Store the mask
				
	IF IS_SAFE_TO_RESTORE_SAVED_HAIR_MP(PLAYER_PED_ID(), eReturnItem) 
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, eReturnItem, FALSE) 
	ENDIF
	
	FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
						
ENDPROC

/// PURPOSE:
///    Restores the players headgear after a drinking or smoking activity
PROC RESTORE_PLAYER_MASK()
	SET_PED_VARIATIONS(PLAYER_PED_ID(), sVariationStruct)
	FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
ENDPROC

/// PURPOSE:
///    When the script starts, shows the correct debug for the object passed in
PROC SHOW_DEBUG_FOR_THIS_OBJECT()
	
	SWITCH mTriggerModel
		
		CASE PROP_CIGAR_03			CPRINTLN(DEBUG_AMBIENT, "...CIGAR")				BREAK	// Cigar - Michael
		CASE PROP_BONG_01			CPRINTLN(DEBUG_AMBIENT, "...BONG")				BREAK 	// Bong	- Franklin/Michael
		CASE PROP_CS_BEER_BOT_01	CPRINTLN(DEBUG_AMBIENT, "...BEER")				BREAK	// Beer - Franklin
		CASE P_TUMBLER_02_S1		CPRINTLN(DEBUG_AMBIENT, "...WHISKEY - MICHAEL")	BREAK	// Whiskey - Michael
		CASE P_TUMBLER_CS2_S		CPRINTLN(DEBUG_AMBIENT, "...WHISKEY")			BREAK	// Whiskey - Trevor													
		CASE P_TUMBLER_CS2_S_TREV	CPRINTLN(DEBUG_AMBIENT, "...WHISKEY - MICHAEL")	BREAK 	// Whiskey - Michael
		CASE PROP_ROLLED_SOCK_02	CPRINTLN(DEBUG_AMBIENT, "...HUFFING GAS")		BREAK	// Huffing Gas
		CASE PROP_MR_RASPBERRY_01	CPRINTLN(DEBUG_AMBIENT, "...MR RASPBERRY JAM")	BREAK	// Mr Raspberry Jam - Trevor
		
		CASE PROP_RADIO_01			
			IF bIsTrevor
									CPRINTLN(DEBUG_AMBIENT, "...TREVOR BEER")				// Beer - Trevor	
			ELSE																						
									CPRINTLN(DEBUG_AMBIENT, "...MICHAEL BEER")				// Beer - Michael
			ENDIF
		BREAK
		
		CASE P_W_GRASS_GLS_S		CPRINTLN(DEBUG_AMBIENT, "...WHEATGRASS")		BREAK 	// Wheatgrass
		CASE P_WINE_GLASS_S			CPRINTLN(DEBUG_AMBIENT, "...WINE")				BREAK	// Wine	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Get the vector where the player
///    has to stand in order to trigger the activity
/// RETURNS:
///    The vector
FUNC VECTOR GET_TRIGGER_VEC_FOR_THIS_OBJECT()
	
	VECTOR vTriggerVec
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01	
		
			IF bIsFranklin
				vTriggerVec = << 9.95, 528.9, 173.6282 >>										// Bong - Franklin	
			ELSE
				vTriggerVec = <<-807.5781, 171.3570, 75.7407 >>									// Bong - Michael
			ENDIF
			
		BREAK
		
		CASE PROP_CIGAR_03			vTriggerVec = << -805.8810, 173.8548, 71.8347 >>	BREAK	// Cigar - Michael
		
		// Beer - Franklin
		CASE PROP_CS_BEER_BOT_01	vTriggerVec = << -9.66, -1429.48, 31.21 >>			BREAK	
		CASE P_CS_JOINT_01			vTriggerVec = << -10.6378, -1441.3145, 30.1015 >>	BREAK	// Joint - Franklin
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S		// Whiskey - Michael/Trevor	
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				vTriggerVec = <<-1154.3430, -1522.5997, 9.6327>>
			ELSE																				
				vTriggerVec = << -800.4056, 183.4856, 71.6055 >>	
			ENDIF
			
		BREAK
															
		CASE PROP_ROLLED_SOCK_02	vTriggerVec = << 1972.1417, 3813.2429, 32.4271>>	BREAK	// Huffing gas - Trevor	
		CASE PROP_MR_RASPBERRY_01	vTriggerVec = <<-1145.9100, -1514.9445, 9.6327>>	BREAK	// Mr Raspberry Jam - Trevor
		CASE PROP_RADIO_01			vTriggerVec = << 1976.9399, 3821.2418, 33.3266 >>	BREAK	// Beer - Michael/Trevor
		
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S		vTriggerVec = <<-804.2254, 184.3325, 72.6042 >>		BREAK
		
		// Wine - Franklin
		CASE P_WINE_GLASS_S			vTriggerVec = <<-8.8011, 515.7225, 173.6282>>		BREAK
		
	ENDSWITCH
	
	RETURN vTriggerVec
	
ENDFUNC

/// PURPOSE:
///    Gets the heading which the player must face in order to trigger the activity.
///    Also used to set the players heading once the activity has been triggered.
/// RETURNS:
///    The heading for the specific object.
FUNC FLOAT GET_HEADING_FOR_THIS_OBJ()

	FLOAT fHeading
	
	SWITCH mTriggerModel

		CASE PROP_BONG_01			
			IF bIsFranklin
				fHeading = -100.0								// Bong - Franklin
			ELSE
				fHeading = 192.2462								// Bong - Michael
			ENDIF
		BREAK
		
		CASE PROP_CIGAR_03			fHeading = 299.353	BREAK	// Cigar - Michael
		CASE PROP_CS_BEER_BOT_01	fHeading = 273.2771	BREAK	// Beer - Franklin	
		CASE P_CS_JOINT_01			fHeading = 256.8294	BREAK	// Joint - Franklin	
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S								// Whiskey		
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				fHeading = 213.5805	
			ELSE												
				fHeading = 15.7529	
			ENDIF
		BREAK
										
		CASE PROP_ROLLED_SOCK_02	fHeading = 206.0063	BREAK 	// Huffing gas - Trevor
		CASE PROP_MR_RASPBERRY_01	fHeading = 355.0925	BREAK	// Mr Raspberry Jam - Trevor
		CASE PROP_RADIO_01			fHeading = 332.7139	BREAK	// Beer - Michael/Trevor
		CASE P_W_GRASS_GLS_S		fHeading = 118.6500	BREAK 	// Wheatgrass - Michael
		CASE P_WINE_GLASS_S			fHeading = 331.4005	BREAK	// Wine	- Franklin
	ENDSWITCH
	
	RETURN fHeading

ENDFUNC

/// PURPOSE:
///		Get the vector of the prop which has been set as the origin of the synched scene.
/// RETURNS
/// 	The origin of the synched scene
FUNC VECTOR GET_SYNCHED_SCENE_VECTOR()

	VECTOR vPos = <<0.0, 0.0, 0.0>>
	
	SWITCH mTriggerModel
	
		CASE PROP_BONG_01
			IF NOT bIsFranklin
				vPos = <<-806.82, 170.03, 75.74>>									// Bong - Michael
			ELSE
				vPos = <<10.22, 527.73, 174.11>>									// Bong - Franklin
			ENDIF
		BREAK
		
		CASE PROP_CIGAR_03			vPos = << -805.17, 173.99, 72.69  >>	BREAK	// Cigar - Michael
		CASE PROP_CS_BEER_BOT_01	vPos = << -9.66, -1429.48, 31.21 >>		BREAK	// Beer - Franklin - PROP_CS_BEER_BOT_03		
		CASE P_CS_JOINT_01			vPos = << -10.07, -1440.64, 30.36 >>	BREAK	// Joint - Franklin														
		CASE PROP_ROLLED_SOCK_02	vPos = <<  1972.8521, 3812.42, 33.28 >>	BREAK	// Gas rag - Trevor	
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S	
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor 
				vPos = << -1153.829,-1523.314,10.560 >>								// Whiskey - Trevor	
			ELSE
				vPos = << -800.68, 184.18, 72.55 >>									// Whiskey - Michael	
			ENDIF
		BREAK

		//CASE PROP_MR_RASPBERRY_01	vPos = << -1145.95, -1514.08, 10.91>>	BREAK	// Mr Raspberry Jam - Trevor (Bear's pos)
		CASE PROP_MR_RASPBERRY_01	vPos = << -1146.2841, -1514.1305, 10.8468>>	BREAK	// Mr Raspberry Jam - Trevor (Irons pos)
		CASE PROP_RADIO_01			vPos = <<  1976.75, 3822.76, 33.28>>	BREAK	// Beer - Michael/Trevor
		CASE P_W_GRASS_GLS_S		vPos = << -804.87, 185.69, 72.75 >>		BREAK	// Wheatgrass - Michael
		CASE P_WINE_GLASS_S			vPos = << -9.10, 516.83, 173.62>>		BREAK	// Wine - Franklin
	ENDSWITCH
	
	RETURN vPos

ENDFUNC

/// PURPOSE:
///    Get the rotation of the object that the synced scene is based on.
/// RETURNS:
///    vVec
FUNC VECTOR GET_SYNCHED_SCENE_ROT()

	VECTOR vRot = <<0.0, 0.0, 0.0>>
	
	SWITCH mTriggerModel
	
		CASE PROP_BONG_01
			IF NOT bIsFranklin
				vRot = <<0, 0, -68.7549354 >>								// Bong - Michael
			ELSE
				vRot = <<0, 0, -179.908748 >>								// Bong - Franklin
			ENDIF
		BREAK

		CASE PROP_CIGAR_03			vRot = << 0.0, 0.0, 21.1994 >>	BREAK	// Cigar - Michael
		CASE PROP_CS_BEER_BOT_01	vRot = << 0.0, 0.0, -128.34 >>	BREAK	// Beer - Franklin - PROP_CS_BEER_BOT_03			
		CASE P_CS_JOINT_01			vRot = << 0.0, 0.0, 0.5729 >>	BREAK 	// Joint - Franklin													
		CASE PROP_ROLLED_SOCK_02	vRot = << 0.0, 0.0, 2.8647 >>	BREAK 	// Gas rag - Trevor	
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S												// Whiskey - Michael & Trevor	
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				vRot = << 0.0, 0.0, 34.9504 >>	
			ELSE
				vRot = << 0.0, 0.0, 21.1994 >>	
			ENDIF
		BREAK
		
		CASE PROP_MR_RASPBERRY_01	vRot = << 0.0, 0.0, 72.76564 >>	BREAK	// Raspberry Jam - Trevor - (Iron's rotation)
		CASE PROP_RADIO_01			vRot = << 0.0, 0.0, -9.1673 >>	BREAK	// Beer - Michael/Trevor
		CASE P_W_GRASS_GLS_S		vRot = << 0.0, 0.0, 21.1994 >>	BREAK	// Wheatgrass - Michael
		CASE P_WINE_GLASS_S			vRot = << 0.0, 0.0, -29.793 >>	BREAK	// Wine - Franklin 
	ENDSWITCH
	
	RETURN vRot
ENDFUNC

FUNC BOOL IS_PROPERTY_SEAT_ACTIVITY_ALREADY_IN_USE(INT iActivity)
	UNUSED_PARAMETER(iActivity)

	// Check if the local player is currently using or assigned to a seat.
	IF IS_PLAYER_USING_OFFICE_SEATID_VALID()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE,"IS_PROPERTY_SEAT_ACTIVITY_ALREADY_IN_USE = TRUE")
		#ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL MP_IS_APARTMENT_ACTIVITY_FREE_AND_PLAYER_IS_AVAILABLE(INT iActivity)
	IF IS_BIT_SET(iBS_AptActivityInUse, iActivity)
	OR iPersonalAptActivity != ci_APT_ACT_IDLE
	OR IS_ANY_INTERACTION_ANIM_PLAYING()					
	OR IS_PLAYER_FREE_AIMING(PLAYER_ID())
	OR IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR IS_PROPERTY_SEAT_ACTIVITY_ALREADY_IN_USE(iActivity)
		RETURN FALSE
	ENDIF	
	
	#IF FEATURE_TUNER
	IF GB_GET_TUNER_ROBBERY_PREP_PLAYER_IS_ON(PLAYER_ID()) = TRV_COMPUTER_VIRUS
		RETURN FALSE
	ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF GB_GET_CLUB_MANAGEMENT_MISSION_PLAYER_IS_ON(PLAYER_ID()) = CMV_PAPARAZZI
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Get the entity of type closest to the trigger vector.
/// RETURNS:
///    The object.
FUNC OBJECT_INDEX GET_SYNCHED_SCENE_OBJECT()

	OBJECT_INDEX mObj
	
	SWITCH mTriggerModel	

		// Michael's cigar
		CASE PROP_CIGAR_03
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, PROP_CIGAR_03, FALSE)
		BREAK
				
		// Franklin's beer
		CASE PROP_CS_BEER_BOT_01
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 2.5, PROP_CS_BEER_BOT_01, FALSE)
		BREAK
		
		// Michael and Trevor's whiskey glass
		CASE P_TUMBLER_02_S1
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_TUMBLER_02_S1, FALSE)
		BREAK
		
		CASE P_TUMBLER_CS2_S
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_TUMBLER_CS2_S, FALSE)
		BREAK
		
		CASE P_TUMBLER_CS2_S_TREV
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_TUMBLER_CS2_S_TREV, FALSE)
		BREAK
		
		// Franklin's sofa joint
		CASE P_CS_JOINT_01
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_CS_JOINT_01, FALSE)
		BREAK
	
		// Michael and Trevor's beer
		CASE PROP_RADIO_01
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 3.0, PROP_CS_BEER_BOT_01, FALSE)
		BREAK

		// Michael's wheatgrass
		CASE P_W_GRASS_GLS_S
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_W_GRASS_GLS_S, FALSE)
		BREAK
		
		// Franklin's wine glass
		CASE P_WINE_GLASS_S
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_WINE_GLASS_S, FALSE)
		BREAK
	ENDSWITCH
	
	RETURN mObj
ENDFUNC

/// PURPOSE:
///    Get the entity of type closest to the trigger vector.
/// RETURNS:
///    The secondary object used in the anim (a lighter, glass or bottle).
FUNC OBJECT_INDEX GET_SECONDARY_ENTITY()

	OBJECT_INDEX mObj
	
	SWITCH mTriggerModel
	
		CASE PROP_BONG_01
		CASE PROP_CIGAR_03
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_CS_LIGHTER_01, FALSE)
		BREAK
		
		CASE P_CS_JOINT_01
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 2.0, P_CS_LIGHTER_01, FALSE)
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, P_WHISKEY_BOTTLE_S, FALSE)
		BREAK
				
		CASE P_WINE_GLASS_S
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, PROP_WINE_BOT_01, FALSE)
		BREAK
		
		CASE PROP_RADIO_01
			mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0, PROP_BOTTLE_CAP_01, FALSE)
		BREAK
		
	ENDSWITCH
	
	RETURN mObj
ENDFUNC

/// PURPOSE:
///    Gets the closest remote control object to the player
/// RETURNS:
///    The remote control object
FUNC OBJECT_INDEX GET_CLOSEST_REMOTE()

	OBJECT_INDEX mObj 
	mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 10.0, PROP_CS_REMOTE_01, FALSE)
	RETURN mObj
ENDFUNC

FUNC OBJECT_INDEX GET_CLOSEST_RAG(MODEL_NAMES mModel=PROP_ROLLED_SOCK_02)

	OBJECT_INDEX mObj
	mObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 10.0, mModel, FALSE)
	RETURN mObj
	
ENDFUNC

/// PURPOSE:
///    Get the correct interupt tag for the activity
/// RETURNS:
///    The name of the interupt tag
FUNC STRING GET_INTERUPT_STRING()
				
	STRING sString = "ScriptEvent"
	RETURN sString
	
ENDFUNC

/// PURPOSE:
///    Checks that the trigger prop is inside the correct interior
/// RETURNS:
///    TRUE if the prop is inside the correct interior.
FUNC BOOL IS_INTERIOR_CORRECT(OBJECT_INDEX mTriggerObject = NULL)
	
	IF g_bInMultiplayer
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())

		//INTERIOR_INSTANCE_INDEX mInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
				
		IF mTriggerModel = PROP_ROLLED_SOCK_02
			IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mTrigger, FALSE), GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0)
				RETURN TRUE
			ENDIF
		ELIF mTriggerModel = P_W_GRASS_GLS_S
			IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mTrigger, FALSE), GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0)
				RETURN TRUE
			ENDIF
		ELIF mTriggerModel = PROP_MR_RASPBERRY_01
			IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(mTrigger, FALSE), GET_TRIGGER_VEC_FOR_THIS_OBJECT(), 5.0)
				RETURN TRUE
			ENDIF
		ELSE
			//mInterior = GET_INTERIOR_AT_COORDS(GET_TRIGGER_VEC_FOR_THIS_OBJECT())
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(mTriggerObject)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///		Checks if the currently active player can interact with the trigger
/// RETURNS:
/// 	TRUE if the player can use the trigger
FUNC BOOL CAN_PLAYER_USE_THIS_OBJECT()
			
	SWITCH mTriggerModel
	
		CASE PROP_CIGAR_03										// Cigar - Michael
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				//CPRINTLN(DEBUG_AMBIENT, "...yes")
				RETURN TRUE
			ENDIF
		BREAK

		CASE PROP_BONG_01										// Bong - Franklin/Michael
			
			IF NETWORK_IS_GAME_IN_PROGRESS() //  Multiplayer - no specific character required.
				RETURN TRUE
			ENDIF
			
			// Franklin can always use it
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				bIsFranklin = TRUE
				RETURN TRUE
			ENDIF
			
			// Michael smoking Jimmy's bong. Not available if Jimmy is in m room.
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				
				IF NOT bEntityHasExitAnim
					bEntityHasExitAnim = TRUE
				ENDIF
				
				// Don't allow Jimmy's bong to trigger if Family 3 is running
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Family5")) > 0
					CPRINTLN(DEBUG_AMBIENT, "...Mission Family 5 is running, so no bong available")
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
					RETURN FALSE
				ENDIF
				
				IF g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M2_SON_gaming_loop
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M7_SON_gaming
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M7_SON_jumping_jacks
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_smoking_weed_in_a_bong
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_sleeping
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_in_room_asks_for_munchies 
					CPRINTLN(DEBUG_AMBIENT, "...Jimmy in room, no bong available")
					RETURN FALSE
				ENDIF
				
				IF g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_rapping_in_the_shower
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_Borrows_sisters_car
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_on_ecstasy_AND_friendly
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_raids_fridge_for_food
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FAMILY_MEMBER_BUSY
				OR g_eCurrentFamilyEvent[FM_MICHAEL_SON] = NO_FAMILY_EVENTS
					RETURN TRUE
				ELSE
					//CPRINTLN(DEBUG_AMBIENT, "...Jimmy is in room, so no bong available")
				ENDIF

				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PROP_CS_BEER_BOT_01	// Beer	- Franklin
			
			IF g_bInMultiplayer
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF NOT bSeparateExitAnim
					bSeparateExitAnim = TRUE
				ENDIF
				//CPRINTLN(DEBUG_AMBIENT, "...Franklin can use this object...")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE P_CS_JOINT_01										// Joint - Franklin
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S				// Whiskey - Michael/Trevor
		CASE P_TUMBLER_CS2_S_TREV
		
			IF g_bInMultiplayer
				bNeedSecondProp = TRUE
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				bNeedSecondProp = TRUE
				RETURN TRUE
			ENDIF
			
			// Trev can only drink in city safehouse after Fame or Shame has been completed.
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			AND GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)	
				
				IF NOT bHasExitCam
					bHasExitCam = TRUE
				ENDIF
				
				bNeedSecondProp = TRUE
				bIsTrevor = TRUE
				RETURN TRUE
			ENDIF
		BREAK
					
		CASE PROP_ROLLED_SOCK_02	
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR		// Huffing gas - Trevor
			OR g_bInMultiplayer
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PROP_MR_RASPBERRY_01								// Mr Raspberry Jam - Trevor	
			
			// Make sure the interior has Mr Jam in the right spot on the chest of drawers.
			IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM] = BUILDINGSTATE_DESTROYED
			
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					
					// Can't run this if Floyd is in the room...
					IF g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD] <> FE_T1_FLOYD_cries_in_foetal_position
					AND g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD] <> FE_T1_FLOYD_is_sleeping
					AND g_eSceneBuddyEvents <> FE_T1_FLOYD_cries_in_foetal_position
					AND g_eSceneBuddyEvents <> FE_T1_FLOYD_is_sleeping
					AND NOT Is_Player_Timetable_Scene_In_Progress()
					AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						//CPRINTLN(DEBUG_AMBIENT, "...yes")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PROP_RADIO_01										// Beer - Michael/Trevor
			
			bNeedSecondProp = TRUE // For the bottle cap
			
			// Check that the trailer isn't trashed.
			IF g_eCurrentBuildingState[BUILDINGNAME_IPL_TREVORS_TRAILER] <> BUILDINGSTATE_DESTROYED
			AND g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR] <> FE_T0_TREVOR_and_kidnapped_wife_stare
			AND g_eCurrentFamilyEvent[FM_TREVOR_0_WIFE] <> FE_T0_TREVOR_and_kidnapped_wife_stare					
				
				// Trevor can always drink beer
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR	
					bIsTrevor = TRUE					
					RETURN TRUE
				ENDIF
							
				// Michael can drink Trevor's beers when he is in exile.
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
				AND GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
				AND NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)			
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

		CASE P_W_GRASS_GLS_S									// Wheatgrass - Michael
		
			IF g_bInMultiplayer
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF g_eCurrentFamilyEvent[FM_MICHAEL_SON] <> FE_M_SON_raids_fridge_for_food
				AND g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] <> FE_M7_WIFE_Making_juice
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE P_WINE_GLASS_S										// Wine - Franklin
		
			IF g_bInMultiplayer	
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				bIsFranklin = TRUE
				bSeparateExitAnim = TRUE
				IF NOT bNeedSecondProp
					bNeedSecondProp = TRUE
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks that all the entities required for the activity exist
/// RETURNS:
///    TRUE if the entities exist.
FUNC BOOL DO_REQUIRED_OBJECTS_EXIST()
		
	SWITCH mTriggerModel
		
		// michael's sofa
		CASE PROP_CIGAR_03
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-805.17, 173.98, 72.69>>, 0.1, PROP_CIGAR_03, FALSE) 																								
				FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-805.17, 173.98, 72.69>>, 0.1, PROP_CIGAR_03, FALSE), TRUE)					// Cigar
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-805.13, 173.86, 72.68>>, 0.1, P_CS_LIGHTER_01, FALSE) 						
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-805.13, 173.86, 72.68>>, 0.1, P_CS_LIGHTER_01, FALSE), TRUE)			// Lighter
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-805.09, 173.83, 72.68>>, 0.1, PROP_ASHTRAY_01, FALSE) 					
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-805.13, 173.86, 72.68>>, 0.1, PROP_ASHTRAY_01, FALSE), TRUE)		// Ashtray
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-804.45, 172.78, 72.33>>, 0.1, PROP_CS_REMOTE_01, FALSE)				
							FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-804.45, 172.78, 72.33>>, 0.1, PROP_CS_REMOTE_01, FALSE), TRUE)	// Remote control
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PROP_CS_BEER_BOT_01
			IF DOES_ENTITY_EXIST(mTrigger)
				CPRINTLN(DEBUG_AMBIENT, "...Frankin's beer bottle exists")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE P_WINE_GLASS_S
			IF DOES_ENTITY_EXIST(mTrigger) // Wine Glass
			AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-8.9, 517.01, 174.0>>, 1.0, PROP_WINE_BOT_01, FALSE) 	// Wine bottle
			//AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-8.9, 517.01, 174.0>>, 1.0, P_Pour_wine_S, FALSE)		// Liquid for wine pouring effect
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE P_CS_JOINT_01
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-10.3, -1440.94, 30.62>>, 0.5, P_CS_JOINT_01, FALSE) // Spliff
				FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-10.3, -1440.94, 30.62>>, 0.5, P_CS_JOINT_01, FALSE), TRUE)
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-10.3, -1440.94, 30.62>>, 0.5, P_CS_LIGHTER_01, FALSE) // Lighter
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-10.3, -1440.94, 30.62>>, 0.5, P_CS_LIGHTER_01, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-10.3, -1440.94, 30.62>>, 0.5, PROP_ASHTRAY_01, FALSE) // Ashtray
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-10.3, -1440.94, 30.62>>, 0.5, PROP_ASHTRAY_01, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE P_TUMBLER_02_S1
				
			IF bIsTrevor
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_02_S1, FALSE) // Shot Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_02_S1, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.57>>, 0.5, P_TUMBLER_02_S1, FALSE) // Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.57>>, 0.5, P_TUMBLER_02_S1, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE P_TUMBLER_CS2_S
			
			IF bIsTrevor
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_CS2_S, FALSE) // Shot Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_CS2_S, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
				
			ELSE
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.55>>, 0.5, P_TUMBLER_CS2_S, FALSE) // Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.55>>, 0.5, P_TUMBLER_CS2_S, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
					
			ENDIF
			
		BREAK
		
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_CS2_S_TREV, FALSE) // Shot Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_TUMBLER_CS2_S_TREV, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-1153.82, -1523.31, 10.51>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
				
			ELSE
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.55>>, 0.5, P_TUMBLER_CS2_S, FALSE) // Glass
					FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.55>>, 0.5, P_TUMBLER_CS2_S, FALSE), TRUE)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE) // Bottle
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-800.68, 184.18, 72.55>>, 0.5, P_WHISKEY_BOTTLE_S, FALSE), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
					
			ENDIF
			
		BREAK
		
		CASE PROP_RADIO_01
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<1976.92, 3821.21, 33.32>>, 0.5, PROP_CS_BEER_BOT_01, FALSE) // Beer bottle 
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE P_W_GRASS_GLS_S
			IF DOES_ENTITY_EXIST(mTrigger) // Wheatgrass Glass
				FREEZE_ENTITY_POSITION(mTrigger, TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	CPRINTLN(DEBUG_AMBIENT, "...Required objects do not exist...")
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Sets the correct initial help text string for this activity based on the trigger object
///    that was passed in.
/// RETURNS:
///    The help text string for the specific activity
FUNC STRING GET_HELP_TEXT_FOR_THIS_ACTIVITY()
	
	//CPRINTLN(DEBUG_AMBIENT, "Getting text for this object...")
	//SHOW_DEBUG_FOR_THIS_OBJECT()
	
	STRING sHelpString
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01			
			IF bIsFranklin
				sHelpString = "SA_BONG2"								// Bong - Franklin
			ELSE 														
				sHelpString = "SA_BONG"									// Bong - Michael
			ENDIF
		BREAK
		
		CASE PROP_CIGAR_03			sHelpString = "SA_CIGAR"	BREAK	// Cigar
		CASE PROP_CS_BEER_BOT_01	sHelpString = "SA_BEER"		BREAK	// Franklins Beer
		CASE P_CS_JOINT_01			sHelpString = "SA_SPLFF"	BREAK	// Joint
		CASE P_TUMBLER_02_S1		sHelpString = "SA_WHSKY"	BREAK	// Whiskey
		CASE P_TUMBLER_CS2_S		sHelpString = "SA_WHSKY"	BREAK	// Whiskey	
		CASE P_TUMBLER_CS2_S_TREV	sHelpString = "SA_WHSKY"	BREAK	// Whiskey
		CASE PROP_ROLLED_SOCK_02	sHelpString = "SA_GAS"		BREAK	// Huffing gas	
		CASE PROP_MR_RASPBERRY_01	sHelpString = "SA_MRJAM"	BREAK	// Mr Raspberry Jam
		CASE PROP_RADIO_01			sHelpString = "SA_BEER"		BREAK	// Trevor/Michael Beer
		CASE P_W_GRASS_GLS_S		sHelpString = "SA_WHEAT"	BREAK	// Wheatgrass
		CASE P_WINE_GLASS_S			sHelpString = "SA_WINE"		BREAK	// Wine	
		DEFAULT 					sHelpString = ""			BREAK	// Nothing
	ENDSWITCH
		
	RETURN sHelpString
ENDFUNC


/// PURPOSE:
///    Sets the correct initial help text string for this activity based on the trigger object
///    that was passed in.
/// RETURNS:
///    The help text string for the specific activity
FUNC STRING GET_INSTRUCTIONS_FOR_THIS_ACTIVITY()
	
	STRING sHelpString
	
	SWITCH mTriggerModel
		CASE PROP_RADIO_01			sHelpString = "SA_BEER2"	BREAK	// Beer - Michael/Trevor
		CASE P_TUMBLER_02_S1		sHelpString = "SA_SHOT2"	BREAK	// Whiskey
		CASE P_TUMBLER_CS2_S		sHelpString = "SA_SHOT2"	BREAK	// Whiskey - Michael/Trevor	
		CASE P_TUMBLER_CS2_S_TREV 	sHelpString = "SA_SHOT2"	BREAK	// Whiskey - Michael/Trevor
		DEFAULT 					sHelpString = "SA_MRJAM"	BREAK	// Nothing
	ENDSWITCH
		
	RETURN sHelpString
ENDFUNC

/// PURPOSE:
///    Gets the correct animation dictionary for the current trigger object.
/// PARAMS:
///    The model name of the object passed in.
/// RETURNS:
///    The STRING name of the animation dictionary
FUNC STRING GET_ANIM_DICT_FOR_THIS_ACTIVITY()
	
	STRING sAnimDict
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01			
			IF bIsFranklin
				sAnimDict = "safe@franklin@ig_10"										// Bong	- Franklin
			ELSE
				sAnimDict = "safe@michael@ig_4"											// Bong - Michael									
			ENDIF																
		BREAK 	

		CASE PROP_CIGAR_03			sAnimDict = "safe@michael@ig_3"				BREAK	// Cigar - Michael
		CASE PROP_CS_BEER_BOT_01	sAnimDict = "safe@franklin@ig_9"			BREAK	// Beer - Franklin		
		CASE P_CS_JOINT_01			sAnimDict = "safe@franklin@ig_13"			BREAK	// Joint - Franklin		
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF NOT g_bInMultiplayer
				IF bIsTrevor 
					sAnimDict = "safe@trevor@ig_6"											// Whiskey - Trevor		
				ELSE																		
					sAnimDict = "safe@michael@ig_1"											// Whiskey - Michael
				ENDIF
			ELSE
				sAnimDict = "MP_SAFEHOUSEWHISKEY@"
			ENDIF
			
		BREAK

		CASE PROP_ROLLED_SOCK_02	sAnimDict = "safe@trevor@ig_8"				BREAK 	// Gas Rag		
		CASE PROP_MR_RASPBERRY_01	sAnimDict = "safe@trevor@ig_7"				BREAK 	// Mr Raspberry Jam
		
		CASE PROP_RADIO_01									
			IF bIsTrevor
				sAnimDict = "safe@trevor@ig_5"											// Beer - Trevor				
			ELSE																			
				sAnimDict = "safe@michael@ig_5"											// Beer - Michael
			ENDIF
		BREAK 

		CASE P_W_GRASS_GLS_S		sAnimDict = "safe@michael@ig_2"				BREAK 	// Wheatgrass
		CASE P_WINE_GLASS_S			sAnimDict = "safe@franklin@ig_11"			BREAK	// Wine =- Franklin
	ENDSWITCH
	
	RETURN sAnimDict
ENDFUNC

/// PURPOSE:
///    Requests and loads the correct animation dictionary.
FUNC BOOL HAS_THIS_ANIM_DICT_LOADED()
	
	STRING sAnimDict = GET_ANIM_DICT_FOR_THIS_ACTIVITY()
	
	// Load animations
	REQUEST_ANIM_DICT(sAnimDict)
	
	WHILE NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		WAIT(0)
	ENDWHILE
	//CPRINTLN(DEBUG_AMBIENT, "ANIM DICT LOADED")
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Requests and loads particle effects for this activity
FUNC BOOL HAS_THIS_PTFX_ASSET_LOADED()
	REQUEST_PTFX_ASSET()
	IF NOT HAS_PTFX_ASSET_LOADED()
		CPRINTLN(DEBUG_AMBIENT, "PTFX ASSET LOADED")
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

ENUM WINE_CAM_NAME

	WINE_CAM_LEFT = 0,
	WINE_CAM_RIGHT,
	WINE_CAM_FRONT,
	WINE_CAM_BACK

ENDENUM

PROC INIT_WINE_CAMS()

	vWineCamPos[WINE_CAM_LEFT] 	= <<-10.1749, 517.8317, 175.1920>>
	vWineCamPos[WINE_CAM_RIGHT] = <<-7.6940, 516.4619, 175.2034>>
	vWineCamPos[WINE_CAM_FRONT] = <<-8.0344, 518.4184, 175.1866>>
	vWineCamPos[WINE_CAM_BACK] 	= <<-9.7451, 515.6803, 175.1711>>
	
ENDPROC

FUNC VECTOR GET_EXIT_CAM_POS()
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vWineExitCamPos, <<0.0, 0.0, 0.0>>)
		RETURN vWineExitCamPos
	ENDIF
	RETURN vWineExitCamPos
ENDFUNC

FUNC VECTOR GET_EXIT_CAM_ROT()
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vWineExitCamRot, <<0.0, 0.0, 0.0>>)
		RETURN vWineExitCamRot
	ENDIF
	RETURN vWineExitCamRot
ENDFUNC

FUNC STRING GET_BEST_WINE_CAM()
	
	INIT_WINE_CAMS()
	
	STRING sBestCam = ""
		
	VECTOR vGamePlayCamPos = GET_GAMEPLAY_CAM_COORD()
	
	// is the gameplay camera on the left or right of the player?
	FLOAT fDiffLeft = GET_DISTANCE_BETWEEN_COORDS(vGamePlayCamPos, vWineCamPos[WINE_CAM_LEFT])
	FLOAT fDiffRight = GET_DISTANCE_BETWEEN_COORDS(vGamePlayCamPos, vWineCamPos[WINE_CAM_RIGHT])
	
	// If we're on the left, chose between the two left-sided cameras
	IF fDiffLeft < fDiffRight
		
		CPRINTLN(DEBUG_AMBIENT, "WINE CAM - wide shot camera left")
		sBestCam = "drinking_wine_cam2"
			
//		IF GET_RANDOM_BOOL()
//			CPRINTLN(DEBUG_AMBIENT, "WINE CAM - closer up camera left")
//			sBestCam = "drinking_wine_cam1"
//		ELSE
//			CPRINTLN(DEBUG_AMBIENT, "WINE CAM - wide shot camera left")
//			sBestCam = "drinking_wine_cam2"
//		ENDIF
	
		vWineExitCamPos	= <<-8.9, 515.6, 175.4>>
		vWineExitCamRot = <<-17.1, 0.5, 6.5>>
		
	// If we're on the right, choose between the two right sided cameras
	ELSE
		
		CPRINTLN(DEBUG_AMBIENT, "WINE CAM - wide shot camera right")
		sBestCam = "drinking_wine_cam3"
			
//		IF GET_RANDOM_BOOL()
//			CPRINTLN(DEBUG_AMBIENT, "WINE CAM - closer up camera right")
//			sBestCam = "drinking_wine_cam"
//		ELSE
//			CPRINTLN(DEBUG_AMBIENT, "WINE CAM - wide shot camera right")
//			sBestCam = "drinking_wine_cam3"
//		ENDIF
		
		vWineExitCamPos	= <<-10.1, 516.0, 175.3>>
		vWineExitCamRot = <<-13.5, 0.5, -56.0>>	
				
	ENDIF
		
	RETURN sBestCam
	
ENDFUNC

/// PURPOSE:
///    Gets camera name for this activity
FUNC STRING GET_CAMERA_FOR_THIS_ACTIVITY(INT iAnim=0)
	
	STRING sAnim = ""
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01			
			IF bIsFranklin
				sAnim = "bong_cam"
			ELSE
				sAnim = "short_cam"
			ENDIF
		BREAK
		
		// Beer - Franklin
		CASE PROP_CS_BEER_BOT_01
			sAnim = "enter_cam"
		BREAK
				
		CASE PROP_ROLLED_SOCK_02
			sAnim = "ig_8_huff_gas_cam"
		BREAK

		CASE PROP_MR_RASPBERRY_01
			SWITCH(iAnim)
				CASE 0	sAnim = "ig_7_howcouldisayno_cam"	BREAK
				CASE 1	sAnim = "ig_7_ifuwanttodothat_cam"	BREAK
				CASE 2	sAnim = "ig_7_lookatu_cam"			BREAK
				CASE 3	sAnim = "ig_7_smelllikeasea_cam"	BREAK
			ENDSWITCH
		BREAK

		CASE P_W_GRASS_GLS_S		
			sAnim = "ig_2_wheatgrassdrink_cam"	
		BREAK 
		
		CASE P_WINE_GLASS_S
			sAnim = GET_BEST_WINE_CAM()
		BREAK
		
	ENDSWITCH

	RETURN sAnim
ENDFUNC



/// PURPOSE:
///    Gets the correct animation for the object
/// RETURNS:
///    The STRING name of the anim dict.
FUNC STRING GET_ENTER_ANIM_FOR_THIS_ACTIVITY(INT iAnim=0)
	
//	CPRINTLN(DEBUG_AMBIENT, "Get entry anim")
	
	STRING sAnim
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01
			IF bIsFranklin
				sAnim = "bong_FRA"													// Bong - Franklin				
			ELSE
				sAnim = "michael_short"												// Bong - Michael
			ENDIF															
		BREAK
			
		CASE PROP_CIGAR_03			sAnim = "cigar_enter_michael"			BREAK	// Cigar - Michael
		CASE PROP_CS_BEER_BOT_01	sAnim = "enter"							BREAK	// Beer	- Franklin		
		
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S		
			IF bIsTrevor
				sAnim = "enter_trevor"												// Whiskey - Trevor							
			ELSE																	
				sAnim = "enter"														// Whiskey - Michael							
			ENDIF					
		BREAK
			
		CASE PROP_ROLLED_SOCK_02	sAnim = "ig_8_huff_gas_player"			BREAK	// Huffing gas - Trevor										
		
		CASE PROP_MR_RASPBERRY_01
			SWITCH(iAnim)
				CASE 0				sAnim = "ig_7_howcouldisayno"			BREAK	// Mr Raspberry Jam
				CASE 1				sAnim = "ig_7_ifuwanttodothat"			BREAK	
				CASE 2				sAnim = "ig_7_lookatu"					BREAK
				CASE 3				sAnim = "ig_7_smelllikeasea"			BREAK
			ENDSWITCH
		BREAK
		
		CASE PROP_RADIO_01			
			IF bIsTrevor
				sAnim = "enter"														// Beer - Trevor						
			ELSE																	
				sAnim = "enter_michael"												// Beer - Michael
			ENDIF
		BREAK

		CASE P_W_GRASS_GLS_S		sAnim = "ig_2_wheatgrassdrink_michael"	BREAK 	// Wheatgrass - Michael
		CASE P_WINE_GLASS_S			sAnim = "drinking_wine"					BREAK	// Wine	- Franklin
	ENDSWITCH

	RETURN sAnim
ENDFUNC



/// PURPOSE:
///    Gets the ENTRY animation to play on the entity (beer bottle, whiskey glass) 
///    depending on the activity and how many times it's been used.
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_ENTER_ANIM_FOR_THIS_ENTITY(INT iAnim=0)

//	CPRINTLN(DEBUG_AMBIENT, "Get anim for this entity")
	
	STRING sAnim
	
	SWITCH mTriggerModel
		
		// Bong	- Franklin/Michael
		CASE PROP_BONG_01
			IF bIsFranklin
				sAnim = "bong_bong"				
			ELSE
				sAnim = "bong_short"
			ENDIF															
		BREAK 
		
		// Beer	- Franklin	
		CASE PROP_CS_BEER_BOT_01
			sAnim = "enter_bottle"
		BREAK
		
		// Mr Raspberry Jam - Trevor
		CASE PROP_MR_RASPBERRY_01								
			SWITCH(iAnim)
				CASE 0	sAnim = "ig_7_howcouldisayno_raspjam"	BREAK
				CASE 1	sAnim = "ig_7_ifuwanttodothat_raspjam"	BREAK	
				CASE 2	sAnim = "ig_7_lookatu_raspjam"			BREAK
				CASE 3	sAnim = "ig_7_smelllikeasea_raspjam"	BREAK
			ENDSWITCH
		BREAK
		
		// Whiskey - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S	
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				sAnim = "enter_shotglass"
			ELSE
				sAnim = "enter_glass"
			ENDIF
		BREAK	
		
		// Gas Rag - Trevor
		CASE PROP_ROLLED_SOCK_02
			sAnim = "ig_8_huff_gas_rag"
		BREAK
		
		// Beer - Michael/Trevor
		CASE PROP_RADIO_01	
			IF bIsTrevor
				sAnim = "enter_beer"
			ELSE
				sAnim = "enter_bottle"	
			ENDIF
		BREAK
		
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S
			sAnim = "ig_2_wheatgrassdrink_glass"
		BREAK

		// Wine - Franklin
		CASE P_WINE_GLASS_S						
			sAnim = "drinking_wine_glass"
		BREAK
		
	ENDSWITCH

	RETURN sAnim
	
ENDFUNC

/// PURPOSE:
///    Gets the ENTRY animation to play on the second entity (beer bottle, whiskey glass) 
///    depending on the activity and how many times it's been used.
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_ENTER_ANIM_FOR_SECOND_ENTITY()

//	CPRINTLN(DEBUG_AMBIENT, "Get anim for this entity")
	
	STRING sAnim
	
	SWITCH mTriggerModel
		
		// Bong - Franklin/Michael - second entity is the lighter..
		CASE PROP_BONG_01			
			IF bIsFranklin
				sAnim = "bong_lighter"				
			ELSE
				sAnim = "lighter_short"										
			ENDIF														
		BREAK 
		
		// Whiskey - Michael/Trevor (second entity is the bottle)
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				sAnim = "enter_bottle"
			ELSE
				sAnim = "enter_bot"
			ENDIF
		BREAK	
				
		// Beer - Michael/Trevor - bottle cap
		CASE PROP_RADIO_01	
			IF bIsTrevor
				sAnim = "enter_cap"
			ELSE
				sAnim = "enter_cap"	
			ENDIF
		BREAK
		
		// Wine - Franklin - second entity is the wine bottle
		CASE P_WINE_GLASS_S						
			sAnim = "drinking_wine_bottle"
		BREAK
		
	ENDSWITCH

	RETURN sAnim
	
ENDFUNC

/// PURPOSE:
///    Gets the animation to play on the entity (beer bottle, whiskey glass) 
///    depending on the activity and how many times it's been used.
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_ANIM_FOR_THIS_ENTITY()

//	CPRINTLN(DEBUG_AMBIENT, "Get anim for this entity")
	
	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH mTriggerModel
				
		// Beer	- Franklin	
		CASE PROP_CS_BEER_BOT_01
			sAnim = "enter_bottle"
		BREAK
		
		// Whiskey - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "drink_2_shotglass"	BREAK
						CASE 1	sAnim = "drink_2_shotglass"	BREAK
						CASE 2	sAnim = "drink_3_shotglass"	BREAK
						CASE 3	sAnim = "drink_4_shotglass"	BREAK
						CASE 4	sAnim = "drink_5_shotglass"	BREAK
						CASE 5	sAnim = "drink_4_shotglass"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_2_shotglass"		BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3_shotglass"		BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4_shotglass"		BREAK
						CASE DL_verydrunk		sAnim = "drink_5_shotglass"		BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "first_shot_glass"	BREAK
						CASE 1	sAnim = "first_shot_glass"	BREAK
						CASE 2	sAnim = "second_shot_glass"	BREAK
						CASE 3	sAnim = "third_shot_glass"	BREAK
						CASE 4	sAnim = "fourth_shot_glass"	BREAK
						CASE 5	sAnim = "fifth_shot_glass"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "first_shot_glass"	BREAK
						CASE DL_moderatedrunk	sAnim = "third_shot_glass"	BREAK
						CASE DL_slightlydrunk	sAnim = "fourth_shot_glass"	BREAK
						CASE DL_verydrunk		sAnim = "fifth_shot_glass"	BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
		BREAK	
		
		// Gas Rag - Trevor
		CASE PROP_ROLLED_SOCK_02
			sAnim = "ig_8_huff_gas_rag"
		BREAK
				
		// Beer - Michael/Trevor
		CASE PROP_RADIO_01	
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "drink_1_beer"	BREAK
						CASE 1	sAnim = "drink_1_beer"	BREAK
						CASE 2	sAnim = "drink_2_beer"	BREAK
						CASE 3	sAnim = "drink_3_beer"	BREAK
						CASE 4	sAnim = "drink_4_beer"	BREAK
						CASE 5	sAnim = "drink_5_beer"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1_beer"		BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3_beer"		BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4_beer"		BREAK
						CASE DL_verydrunk		sAnim = "drink_5_beer"		BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "drink_1_bottle"	BREAK
						CASE 1	sAnim = "drink_1_bottle"	BREAK
						CASE 2	sAnim = "drink_2_bottle"	BREAK
						CASE 3	sAnim = "drink_3_bottle"	BREAK
						CASE 4	sAnim = "drink_4_bottle"	BREAK
						CASE 5	sAnim = "drink_3_bottle"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1_bottle"	BREAK
						CASE DL_moderatedrunk	sAnim = "drink_2_bottle"	BREAK
						CASE DL_slightlydrunk	sAnim = "drink_3_bottle"	BREAK
						CASE DL_verydrunk		sAnim = "drink_4_bottle"	BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S
			sAnim = "ig_2_wheatgrassdrink_glass"
		BREAK

		// Wine - Franklin
		CASE P_WINE_GLASS_S						
			sAnim = "drinking_wine_glass"
		BREAK
		
	ENDSWITCH

	RETURN sAnim
	
ENDFUNC

/// PURPOSE:
///    Gets the animation to play on the secondary prop in a scene (lighters, bottles, etc.)
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_ANIM_FOR_SECOND_ENTITY()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH mTriggerModel		
		
		// Whiskey - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
		
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "drink_2_bottle"	BREAK
						CASE 1	sAnim = "drink_2_bottle"	BREAK
						CASE 2	sAnim = "drink_3_bottle"	BREAK
						CASE 3	sAnim = "drink_4_bottle"	BREAK
						CASE 4	sAnim = "drink_5_bottle"	BREAK
						CASE 5	sAnim = "drink_4_bottle"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_2_bottle"	BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3_bottle"	BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4_bottle"	BREAK
						CASE DL_verydrunk		sAnim = "drink_5_bottle"	BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "first_shot_bot"	BREAK
						CASE 1	sAnim = "first_shot_bot"	BREAK
						CASE 2	sAnim = "second_shot_bot"	BREAK
						CASE 3	sAnim = "third_shot_bot"	BREAK
						CASE 4	sAnim = "fourth_shot_bot"	BREAK
						CASE 5	sAnim = "fifth_shot_bot"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "first_shot_bot"	BREAK
						CASE DL_moderatedrunk	sAnim = "third_shot_bot"	BREAK
						CASE DL_slightlydrunk	sAnim = "fourth_shot_bot"	BREAK
						CASE DL_verydrunk		sAnim = "fifth_shot_bot"	BREAK
					ENDSWITCH
				ENDIF				
			ENDIF
		BREAK
			
		// Wine - Franklin
		CASE P_WINE_GLASS_S
			sAnim = "drinking_wine_bottle"
		BREAK
		
		// Beer - bottle cap
		CASE PROP_RADIO_01
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "drink_1_cap"	BREAK
						CASE 1	sAnim = "drink_1_cap"	BREAK
						CASE 2	sAnim = "drink_2_cap"	BREAK
						CASE 3	sAnim = "drink_3_cap"	BREAK
						CASE 4	sAnim = "drink_4_cap"	BREAK
						CASE 5	sAnim = "drink_5_cap"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1_cap"		BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3_cap"		BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4_cap"		BREAK
						CASE DL_verydrunk		sAnim = "drink_5_cap"		BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed
						CASE 0	sAnim = "drink_1_cap"	BREAK
						CASE 1	sAnim = "drink_1_cap"	BREAK
						CASE 2	sAnim = "drink_2_cap"	BREAK
						CASE 3	sAnim = "drink_3_cap"	BREAK
						CASE 4	sAnim = "drink_4_cap"	BREAK
						CASE 5	sAnim = "drink_3_cap"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1_cap"	BREAK
						CASE DL_moderatedrunk	sAnim = "drink_2_cap"	BREAK
						CASE DL_slightlydrunk	sAnim = "drink_3_cap"	BREAK
						CASE DL_verydrunk		sAnim = "drink_4_cap"	BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Get the correct shot animation based on how many shots the player has had.
FUNC STRING GET_SHOT_ANIM_FOR_THIS_SHOT()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH (mTriggerModel)
		
//		// Beer - Franklin
		CASE PROP_CS_BEER_BOT_01
			sAnim = "enter"
		BREAK
		
		// Whiskey - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "drink_2_trevor"	BREAK
						CASE 1	sAnim = "drink_2_trevor"	BREAK
						CASE 2	sAnim = "drink_3_trevor"	BREAK
						CASE 3	sAnim = "drink_4_trevor"	BREAK
						CASE 4	sAnim = "drink_5_trevor"	BREAK
						CASE 5	sAnim = "drink_4_trevor"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_2_trevor"			BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3_trevor"			BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4_trevor"			BREAK
						CASE DL_verydrunk		sAnim = "drink_5_trevor"			BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "first_shot"	BREAK
						CASE 1	sAnim = "first_shot"	BREAK
						CASE 2	sAnim = "second_shot"	BREAK
						CASE 3	sAnim = "third_shot"	BREAK
						CASE 4	sAnim = "fourth_shot"	BREAK
						CASE 5	sAnim = "fifth_shot"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "first_shot"	BREAK
						CASE DL_moderatedrunk	sAnim = "third_shot"	BREAK
						CASE DL_slightlydrunk	sAnim = "fourth_shot"	BREAK
						CASE DL_verydrunk		sAnim = "fifth_shot"	BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		
		// Beer - Michael/Trevor
		CASE PROP_RADIO_01
			
			IF bIsTrevor
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "drink_1"	BREAK
						CASE 1	sAnim = "drink_1"	BREAK
						CASE 2	sAnim = "drink_2"	BREAK
						CASE 3	sAnim = "drink_3"	BREAK
						CASE 4	sAnim = "drink_4"	BREAK
						CASE 5	sAnim = "drink_5"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1"			BREAK
						CASE DL_moderatedrunk	sAnim = "drink_3"			BREAK
						CASE DL_slightlydrunk	sAnim = "drink_4"			BREAK
						CASE DL_verydrunk		sAnim = "drink_5"			BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bUseCounter
					SWITCH iTimesUsed 
						CASE 0	sAnim = "drink_1_michael"	BREAK
						CASE 1	sAnim = "drink_1_michael"	BREAK
						CASE 2	sAnim = "drink_2_michael"	BREAK
						CASE 3	sAnim = "drink_3_michael"	BREAK
						CASE 4	sAnim = "drink_4_michael"	BREAK
						CASE 5	sAnim = "drink_3_michael"	BREAK
					ENDSWITCH
				ELSE
					SWITCH eDrunkLevel 
						CASE DL_NO_LEVEL		sAnim = "drink_1_michael"	BREAK
						CASE DL_moderatedrunk	sAnim = "drink_2_michael"	BREAK
						CASE DL_slightlydrunk	sAnim = "drink_3_michael"	BREAK
						CASE DL_verydrunk		sAnim = "drink_4_michael"	BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
				
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S
			sAnim = "ig_2_wheatgrass_drink_michael"
		BREAK
	ENDSWITCH
					
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Gets the exit animation to play on a character during a drinking activity
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_EXIT_ANIM_FOR_THIS_ACTIVITY()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH mTriggerModel
		
		//  Beer - Franklin
		CASE PROP_CS_BEER_BOT_01					
			SWITCH eDrunkLevel 
				CASE DL_NO_LEVEL		sAnim = "exit_1"	BREAK
				CASE DL_moderatedrunk	sAnim = "exit_2"	BREAK
				CASE DL_slightlydrunk	sAnim = "exit_3"	BREAK
				CASE DL_verydrunk		sAnim = "exit_4"	BREAK
			ENDSWITCH
		BREAK
		
		// Beer - Michael/Trevor
		CASE PROP_RADIO_01
			IF bIsTrevor
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober"			BREAK
					CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk"	BREAK
					CASE DL_verydrunk		sAnim = "exit_very_drunk"		BREAK
				ENDSWITCH
			ELSE
				SWITCH eDrunkLevel // TODO: Update these when Tristan's new exits have been posed.
					CASE DL_NO_LEVEL		sAnim = "exit_1_michael"		BREAK
					CASE DL_moderatedrunk	sAnim = "exit_2_michael"		BREAK
					CASE DL_slightlydrunk	sAnim = "exit_3_michael"		BREAK
					CASE DL_verydrunk		sAnim = "exit_4_michael"		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		// Whiskey - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
		
			IF bIsTrevor				
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "EXIT_SOBER_TREVOR"				BREAK
					CASE DL_moderatedrunk	sAnim = "EXIT_MODERATELY_DRUNK_TREVOR"	BREAK
					CASE DL_slightlydrunk	sAnim = "EXIT_SLIGHTLY_DRUNK_TREVOR"	BREAK
					CASE DL_verydrunk		sAnim = "EXIT_VERY_DRUNK_TREVOR"		BREAK
				ENDSWITCH	
			ELSE
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober"					BREAK
					CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk"			BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk"			BREAK
					CASE DL_verydrunk		sAnim = "exit_drunk"					BREAK
				ENDSWITCH
			ENDIF
		BREAK
				
		// Wine - Frankin
		CASE P_WINE_GLASS_S
			SWITCH eDrunkLevel 
				CASE DL_NO_LEVEL		sAnim = "drinking_wine_exit"					BREAK
				CASE DL_moderatedrunk	sAnim = "drinking_wine_exit_moderately_drunk"	BREAK
				CASE DL_slightlydrunk	sAnim = "drinking_wine_exit_slightly_drunk"		BREAK
				CASE DL_verydrunk		sAnim = "drinking_wine_exit_drunk"				BREAK
			ENDSWITCH			
		BREAK
		
		// Bong - Michael
		CASE PROP_BONG_01
			IF bIsFranklin
				sAnim = "exit_fra"
			ELSE
				sAnim = "michael_exit"
			ENDIF
		BREAK
		
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S
			sAnim = "exit_michael"
		BREAK
		
	ENDSWITCH
	
	
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Gets the exit animation to play on an entity during a drinking activity
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_EXIT_ANIM_FOR_THIS_ENTITY()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH mTriggerModel
		
		//  Beer - Franklin
		CASE PROP_CS_BEER_BOT_01
			SWITCH eDrunkLevel 
				CASE DL_NO_LEVEL		sAnim = "exit_1_bottle"		BREAK
				CASE DL_moderatedrunk	sAnim = "exit_2_bottle"		BREAK
				CASE DL_slightlydrunk	sAnim = "exit_3_bottle"		BREAK
				CASE DL_verydrunk		sAnim = "exit_4_bottle"		BREAK
			ENDSWITCH			
		BREAK		
		
		// Beer - Trev/Michael
		CASE PROP_RADIO_01 
			IF bIsTrevor
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_2_beer"	BREAK
					CASE DL_moderatedrunk	sAnim = "exit_2_beer"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_1_beer"	BREAK
					CASE DL_verydrunk		sAnim = "exit_1_beer"	BREAK
				ENDSWITCH
			ELSE
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_2_bottle"	BREAK
					CASE DL_moderatedrunk	sAnim = "exit_2_bottle"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_1_bottle"	BREAK
					CASE DL_verydrunk		sAnim = "exit_1_bottle"	BREAK
				ENDSWITCH	
			ENDIF
		BREAK
		
		// Bong - Michael
		CASE PROP_BONG_01
			IF bIsFranklin
				sAnim = "exit_fra"
			ELSE
				sAnim = "bong_exit"
			ENDIF
		BREAK
		
		// Whiskey Glass - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				sAnim = "exit_shotglass"
			ELSE
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober_glass"	BREAK
					CASE DL_moderatedrunk	sAnim = "exit_sober_glass"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_drunk_glass"	BREAK
					CASE DL_verydrunk		sAnim = "exit_drunk_glass"	BREAK
				ENDSWITCH			
			ENDIF
		BREAK
		
		// Wheatgrass - Michael
		CASE P_W_GRASS_GLS_S
			sAnim = "exit_glass"
		BREAK
		
		// Wine bottle - Frankin
		CASE P_WINE_GLASS_S
			sAnim = "drinking_wine_exit_glass"				
		BREAK
		
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

/// PURPOSE:
///    Gets the exit animation to play on an entity during a drinking activity
/// RETURNS:
///    The name of the anim to play on the prop.
FUNC STRING GET_EXIT_ANIM_FOR_SECOND_ENTITY()

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(PLAYER_PED_ID())
	
	SWITCH mTriggerModel
				
		// Bong Lighter- Michael
		CASE PROP_BONG_01
			sAnim = "lighter_exit"
		BREAK
		
		// Whiskey Bottle - Michael/Trevor
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				sAnim = "exit_bottle"
			ELSE
				SWITCH eDrunkLevel 
					CASE DL_NO_LEVEL		sAnim = "exit_sober_bot"			BREAK
					CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk_bot"	BREAK
					CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk_bot"	BREAK
					CASE DL_verydrunk		sAnim = "exit_drunk_bot"			BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		// Wine bottle - Frankin
		CASE P_WINE_GLASS_S
			sAnim = "drinking_wine_exit_bottle"				
		BREAK
		
		CASE PROP_RADIO_01
			sAnim = "enter_cap"
		BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

FUNC FLOAT WRAP_FLOAT(FLOAT v, FLOAT low, FLOAT hi)
	FLOAT dist
	FLOAT times
	
	IF (low = hi)
		RETURN low
	ENDIF
	
	dist = hi - low
	times = TO_FLOAT(FLOOR((v - low) / dist))
	RETURN v - (times * dist)
ENDFUNC

FUNC FLOAT LIMIT_ANGLE(FLOAT fAngle)

	// Put given angle into the range [-180, 180]
    IF fAngle < -180.0
    	fAngle += 360.0
	    IF fAngle < -180.0
	    	fAngle += 360.0
		ENDIF
    ELIF fAngle > 180.0
    	fAngle -= 360.0
	    IF fAngle > 180.0
	    	fAngle -= 360.0
		ENDIF
    ENDIF

    RETURN fAngle

ENDFUNC

//FUNC FLOAT CAMERA_TO_MOUSE_LIMIT_CUSHION(FLOAT fCurrent, FLOAT fLimit)
//	
//ENDFUNC

/// PURPOSE:
///    Allows the passed-in camera to be adjusted from its original heading 
PROC DO_CAM_ADJUST(CAMERA_INDEX mCam, VECTOR vDefault)
	
	VECTOR vCurrent, vIntended, vUpdated
	
	CONST_FLOAT MOUSE_X_LIMIT 6.5
	CONST_FLOAT MOUSE_Z_LIMIT 12.5
	CONST_FLOAT MOUSE_SPEED -5.0
	
	IF DOES_CAM_EXIST(mCam)
		
		vCurrent = GET_CAM_ROT(mCam)
				
		vIntended = vDefault
		
		// PC Mouse look
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			
			FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			FLOAT fMouseZ = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)

			FLOAT fCurrentMouseAngleX
			FLOAT fCurrentMouseAngleZ
			
			fCurrentMouseAngleX = (fMouseX - 0.5)*(-MOUSE_X_LIMIT*2)
			fCurrentMouseAngleZ = (fMouseZ - 0.5)*(-MOUSE_Z_LIMIT*2)
			
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.2, "NUMBER", fMouseX, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.3, "NUMBER", fMouseZ, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.6, "NUMBER", fCurrentMouseAngleX, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.7, "NUMBER", fCurrentMouseAngleZ, 2)
			
			vIntended.x += fCurrentMouseAngleX
			vIntended.z += fCurrentMouseAngleZ
			
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.1, "NUMBER", vDefault.z, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.2, "NUMBER", fRelativeMinZ, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.3, "NUMBER", fRelativeMaxZ, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.4, "NUMBER", vCurrent.z - vDefault.z, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.5, "NUMBER", vIntended.z, 2)
//			
			WHILE vIntended.z < -180.0
				vIntended.z += 360.0
			ENDWHILE
			
			WHILE vIntended.z > 180
				vIntended.z -= 360.0
			ENDWHILE
			
			WHILE vIntended.x < -180
				vIntended.x += 360.0
			ENDWHILE
			
			WHILE vIntended.x > 180
				vIntended.x -= 360.0
			ENDWHILE
			
			// work out shortest angle between angle 1 and 2
			FLOAT angdiff = vIntended.z - vCurrent.z
			FLOAT shortang = WRAP_FLOAT(angdiff, -180.0, 180.0)
			
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.2, "NUMBER", vIntended.z, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.3, "NUMBER", vCurrent.z, 2)
			
			vUpdated = (vCurrent + ((vIntended - vCurrent) * 0.25))
			vUpdated.z = vCurrent.z + (shortang * 0.25)			

			
		ELSE
			
			// Gamepad look		
					
			// Add right stick offset
			INT iLeftX, iLeftY, iRightX, iRightY
			
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)

			
			// Add a deadlock but keep values from 0.
			IF ((iRightX  < 32) AND (iRightX > -32))
			AND ((iRightY  < 32) AND (iRightY > -32))
			
				IF ((iRightX  < 32) AND (iRightX > -32))
					iRightX = 0
				ELSE
					IF (iRightX  < 0)
						iRightX -= 32
					ELSE
						iRightX += 32
					ENDIF
				ENDIF
				
				IF ((iRightY  < 32) AND (iRightY > -32))
					iRightY = 0
				ELSE
					IF (iRightY  < 0)
						iRightY -= 32
					ELSE
						iRightY += 32
					ENDIF
				ENDIF
			ENDIF
			
			iRightX *= -1
			iRightY *= -1
			IF IS_LOOK_INVERTED()
				iRightY	*= -1
			ENDIF
			
			vIntended.x += (iRightY * 0.05)
			vIntended.z += (iRightX * 0.10)
			
			WHILE vIntended.z < -180.0
				vIntended.z += 360.0
			ENDWHILE
			
			WHILE vIntended.z > 180
				vIntended.z -= 360.0
			ENDWHILE
			
			WHILE vCurrent.z < -180
				vCurrent.z += 360.0
			ENDWHILE
			
			WHILE vCurrent.z > 180
				vCurrent.z -= 360.0
			ENDWHILE
			
			// work out shortest angle between angle 1 and 2
			FLOAT angdiff = vIntended.z - vCurrent.z
			FLOAT shortang = WRAP_FLOAT(angdiff, -180.0, 180.0)
			
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.2, "NUMBER", vIntended.z, 2)
//			DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.3, "NUMBER", vCurrent.z, 2)
//			
			vUpdated = (vCurrent + ((vIntended - vCurrent) * 0.25))
			vUpdated.z = vCurrent.z + (shortang * 0.25)
			
		//	SET_TEXT_SCALE(0.2, 0.2) DISPLAY_TEXT_WITH_FLOAT(0.4, 0.4, "NUMBER", vCurrent.z, 4)
		//	SET_TEXT_SCALE(0.2, 0.2) DISPLAY_TEXT_WITH_FLOAT(0.4, 0.5, "NUMBER", vIntended.z, 4)
		//	SET_TEXT_SCALE(0.2, 0.2) DISPLAY_TEXT_WITH_FLOAT(0.4, 0.6, "NUMBER", vUpdated.z, 4)
		//	SET_TEXT_SCALE(0.2, 0.2) DISPLAY_TEXT_WITH_FLOAT(0.4, 0.7, "NUMBER", shortang, 4)
		//	SET_TEXT_SCALE(0.2, 0.2) DISPLAY_TEXT_WITH_FLOAT(0.4, 0.8, "NUMBER", angdiff, 4)
			
		ENDIF
		
		// wrap this between 0 and 180
		WHILE vUpdated.z < -180
			vUpdated.z += 360.0
		ENDWHILE
		
		WHILE vUpdated.z > 180
			vUpdated.z -= 360.0
		ENDWHILE
		
		//DISPLAY_TEXT_WITH_FLOAT( 0.3, 0.1, "NUMBER", RDIFF(vUpdated.z, vDefault.z), 4 )
		//DISPLAY_TEXT_WITH_FLOAT( 0.3, 0.2, "NUMBER", RDIFF(vUpdated.x, vDefault.x), 4 )
		
//		DISPLAY_TEXT_WITH_FLOAT( 0.3, 0.1, "NUMBER", vUpdated.z, 4 )
//		DISPLAY_TEXT_WITH_FLOAT( 0.3, 0.2, "NUMBER", vUpdated.x, 4 )
		
		SET_CAM_ROT(mCam, vUpdated)
		
	ENDIF
			
ENDPROC

PROC START_WHEATGRASS_DIALOGUE()
		
	ADD_PED_FOR_DIALOGUE(mWeedConv, 0, PLAYER_PED_ID(), "MICHAEL")
	
	IF GET_RANDOM_BOOL()
		IF CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WG1", 	CONV_PRIORITY_AMBIENT_HIGH)
			IF NOT bDoneWheatgrassDialogue
				bDoneWheatgrassDialogue = TRUE
			ENDIF
		ENDIF
	ELSE
		IF CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WG2", 	CONV_PRIORITY_AMBIENT_HIGH)
			IF NOT bDoneWheatgrassDialogue
				bDoneWheatgrassDialogue = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Starts stoned monologues
PROC START_STONED_DIALOGUE()
	
	CPRINTLN(DEBUG_AMBIENT, "START_STONED_DIALOGUE()")
	
	INT iRand 
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
		ADD_PED_FOR_DIALOGUE(mWeedConv, 0, PLAYER_PED_ID(), "MICHAEL")
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("SAFEHOUSE_STONED_MICHAEL")
			START_AUDIO_SCENE("SAFEHOUSE_STONED_MICHAEL")
		ENDIF
		
		SWITCH(iRand)
			CASE 0 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD1", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 1 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD2", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 2 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD3", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 3 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD4", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 4 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD5", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 5 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD6", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 6 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD7", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 7 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD8", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 8 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD9", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
			CASE 9 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "M_WD10", CONV_PRIORITY_AMBIENT_HIGH) 	BREAK
		ENDSWITCH
				
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
	
		// Make sure the TV subtitles don't clash with the stoned dialogue.
		ENABLE_MOVIE_SUBTITLES(FALSE)
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
		ADD_PED_FOR_DIALOGUE(mWeedConv, 1, PLAYER_PED_ID(), "FRANKLIN")
		SWITCH(iRand)
			CASE 0 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD1", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 1 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD2",	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 2 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD3", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 3 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD4", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 4 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD5", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 5 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD6", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 6 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD7", 	CONV_PRIORITY_AMBIENT_MEDIUM)	BREAK
			CASE 7 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD8", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 8 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD9", 	CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 9 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD10", CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
			CASE 10 CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "F_WD11", CONV_PRIORITY_AMBIENT_MEDIUM) 	BREAK
		ENDSWITCH
	
	ELSE
		iRand = GET_RANDOM_INT_IN_RANGE(0, 8)
		ADD_PED_FOR_DIALOGUE(mWeedConv, 2, PLAYER_PED_ID(), "TREVOR")
		SWITCH(iRand)
			CASE 0 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD1", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// This stuff does nothing for me... 
//			CASE 1 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD2",	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK 	// Man, reefer is pointless...
			CASE 1 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD3", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK 	// Man, that stuff is pointless...
			CASE 2 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD4", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// Oh man, I'm not tough at all.
			CASE 3 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD5", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// Whoo! I fucking love it!
			CASE 4 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD6", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// Why does everything hurt so much all the time?
//			CASE 6 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD7", 	CONV_PRIORITY_AMBIENT_HIGH)		BREAK	// I'm not stoned...
			CASE 5 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD8", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// Fine and suicidal...
			CASE 6 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD9", 	CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// I'm as sober as can be
			CASE 7 	CREATE_CONVERSATION(mWeedConv, "WEEDAUD", "T_WD10", CONV_PRIORITY_AMBIENT_HIGH) 	BREAK	// Mother!!
		ENDSWITCH
			
		// In case we get separate effects for Franklin...
//		IF NOT IS_AUDIO_SCENE_ACTIVE("SAFEHOUSE_STONED_FRANKLIN")
//			START_AUDIO_SCENE("SAFEHOUSE_STONED_FRANKLIN")
//		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "CREATE_CONVERSATION = ", iRand)
		
ENDPROC

/// PURPOSE:
///    Used to remove the effects of being stoned if the script terminates or is ended earlier.
PROC CLEANUP_STONED_EFFECT()

	CPRINTLN(DEBUG_AMBIENT, "CLEANED UP STONED EFFECT")
	
	iTimesUsed = 0
//	SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
//	STOP_AUDIO_SCENE("SAFEHOUSE_STONED_MICHAEL")

ENDPROC


/// PURPOSE:
///    Handles the weed smoking effect
PROC HANDLE_STONED_EFFECT()	//MODEL_NAMES mModel

	IF iTimesUsed > 0
					
		IF NOT bStartStonedDialogue
			CPRINTLN(DEBUG_AMBIENT, " STARTING STONED DIALOGUE TIMER.... ")
			iDialogueStartTime = GET_GAME_TIMER() + STONED_DIALOGUE_DELAY
			bStartStonedDialogue = TRUE
		ELSE
			IF GET_GAME_TIMER() > iDialogueStartTime
				CPRINTLN(DEBUG_AMBIENT, " STARTING STONED DIALOGUE ")
				START_STONED_DIALOGUE()
				bStartStonedDialogue = FALSE
				iTimesUsed = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Monitors how many times the player has interacted with the item
///    and applies the desired effect.
PROC APPLY_USAGE_EFFECTS()
		
	// Should be a percentage of max health for consistency - awaiting GET_PED_MAX_HEALTH command
	INT addedHealth = 10 
	
	//CPRINTLN(DEBUG_AMBIENT, "ITEM USES = ", iTimesUsed)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SWITCH(mTriggerModel)
		
			// Make player high
			CASE PROP_BONG_01
			CASE P_CS_JOINT_01
				HANDLE_STONED_EFFECT()		//mTriggerModel		
			BREAK
					
			// Give player a health boost
			CASE PROP_MR_RASPBERRY_01
			CASE P_W_GRASS_GLS_S
				IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) < (GET_PED_MAX_HEALTH(PLAYER_PED_ID())-addedHealth)
					SET_ENTITY_HEALTH(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) +addedHealth))
				ELSE
					SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF		
ENDPROC

FUNC BOOL IS_ANOTHER_CHAR_NEARBY()
	
	INT iCount 
	INT i 
	PED_INDEX tmpArray[32]
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		iCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), tmpArray)
	ENDIF
	
	REPEAT iCount i
		IF NOT IS_PED_INJURED(tmpArray[i])
		
			// Is Michael in the way of Trevor?
			IF bIsTrevor
			
				IF GET_ENTITY_MODEL(tmpArray[i]) = PLAYER_ZERO
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(tmpArray[i])) < 3.0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Is Trevor in the way of Michael?
			IF NOT bIsTrevor
			AND NOT bIsFranklin
				IF GET_ENTITY_MODEL(tmpArray[i]) = PLAYER_ONE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(tmpArray[i])) < 3.0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///     Checks whether we can trigger this activity
FUNC BOOL IS_TRIGGER_AREA_OK(VECTOR vTrigPos, VECTOR vTrigSize)
		
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		// Player checks
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		
			// Safety checks
			IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vTrigPos, 2.0)
				
				// Gameplay checks
				IF IS_GAMEPLAY_CAM_RENDERING()
				AND NOT IS_CUTSCENE_PLAYING()		
				AND NOT IS_CINEMATIC_CAM_RENDERING()	// Stop the help text displaying when the ToD screen is active.					
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT IS_ANOTHER_CHAR_NEARBY()
				
					if ( g_bTriggerSceneActive // Needed to stop context help from displaying when about to enter mission trigger				
					and g_bTriggerSceneBlockHelp)
					or IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() 
						CPRINTLN(DEBUG_FLOW_HELP, "<IS_TRIGGER_AREA_OK>  = FALSE 7.5m of mission trigger blip.")
						//CPRINTLN(DEBUG_AMBIENT, "Area not ok 1.")
						Return FALSE					
					endif
					
					// Player is at trigger point
					IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTrigPos, vTrigSize.x)
					//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTrigPos, vTrigSize)
						RETURN TRUE
					ELSE
						//CPRINTLN(DEBUG_AMBIENT, "Area not ok 2 - not in right area")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, "Area not ok 3.")
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///     Loads required audio banks for safehouse activity
FUNC STRING GET_AUDIO_BANK_FOR_ACTIVITY()

	STRING sAudioBank
	
	SWITCH mTriggerModel
		
		CASE PROP_BONG_01
			IF bIsFranklin
				sAudioBank = "SAFEHOUSE_FRANKLIN_USE_BONG"
			ELSE
				sAudioBank = "SAFEHOUSE_MICHAEL_USE_BONG"
			ENDIF
		BREAK
				
		// Whiskey
		CASE P_TUMBLER_02_S1
		CASE P_TUMBLER_CS2_S
		CASE P_TUMBLER_CS2_S_TREV
			IF bIsTrevor
				sAudioBank = "SAFEHOUSE_TREVOR_DRINK_WHISKEY"	
			ELSE
				sAudioBank = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
			ENDIF
		BREAK
				
		// Beers at Trevor's trailer
		CASE PROP_RADIO_01
			IF bIsTrevor
				sAudioBank = "SAFEHOUSE_TREVOR_DRINK_BEER"
			ELSE
				sAudioBank = "SAFEHOUSE_MICHAEL_DRINK_BEER"
			ENDIF
		BREAK
		
		CASE PROP_CS_BEER_BOT_01	sAudioBank = "SAFEHOUSE_FRANKLIN_DRINK_BEER"		BREAK
		CASE PROP_ROLLED_SOCK_02	sAudioBank = "SAFEHOUSE_TREVOR_GAS"					BREAK
		CASE PROP_MR_RASPBERRY_01	sAudioBank = "SAFEHOUSE_TREVOR_RASBERRY"			BREAK
		CASE P_W_GRASS_GLS_S		sAudioBank = "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"	BREAK
		CASE P_WINE_GLASS_S			sAudioBank = "SAFEHOUSE_FRANKLIN_DRINK_WINE"		BREAK
		CASE PROP_CIGAR_03			sAudioBank = "SAFEHOUSE_MICHAEL_SIT_SOFA"			BREAK
		CASE P_CS_JOINT_01			sAudioBank = "SAFEHOUSE_FRANKLIN_SOFA"				BREAK
		
	ENDSWITCH
	
	RETURN sAudioBank
	
ENDFUNC

/// PURPOSE:
///    Reqests the appropriate audio bank for the trigger object
/// RETURNS:
///    True if the audio bank has loaded.
FUNC BOOL HAS_AUDIO_LOADED()
		
	IF REQUEST_AMBIENT_AUDIO_BANK(GET_AUDIO_BANK_FOR_ACTIVITY())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///     Ensure player control is restored as long as we're not in a cutscene
PROC SAFE_RESTORE_PLAYER_CONTROL()

	CPRINTLN(DEBUG_AMBIENT, " Calling SAFE_RESTORE_PLAYER_CONTROL ")
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND CAN_PLAYER_START_CUTSCENE()
		AND bSafehouseSetControlOff				// Need this to stop this function restoring player control when disabled elsewhere.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF 
			CPRINTLN(DEBUG_AMBIENT, " PLAYER CONTROL RESTORED ")
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Returns true if we're not in 1st person mode and the camera index exists
FUNC BOOL IS_NOT_1ST_PERSON_AND_DOES_CAM_EXIST(CAMERA_INDEX CamIndex)

	IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
	AND DOES_CAM_EXIST(CamIndex)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


