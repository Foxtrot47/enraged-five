// Dave Watson
// Controls the opening / closing of the "roller" door at the rear of the abattoir
// 
// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"
USING "script_maths.sch"



// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambCheckForAnim,
	ambRunning,
	ambOpenDoor,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun

OBJECT_INDEX oRollerDoorCollision	//-- Collision for door
VECTOR vRollerDoorClosedPos			//-- Door closed position
VECTOR vRollerDoorRot				//-- Door rotation

// Clean Up
PROC missionCleanup()
	//--Delete door collision
	IF DOES_ENTITY_EXIST(oRollerDoorCollision)
		DELETE_OBJECT(oRollerDoorCollision)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(P_Abat_roller_1_col)
	printDebugString("ob_abatdoor Terminated >>>>>>>>>>>>>>>>>\n")
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oAbatDoor)

IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
	missionCleanup()
ENDIF

IF DOES_ENTITY_EXIST(oAbatDoor)
	 
	FREEZE_ENTITY_POSITION(oAbatDoor, TRUE)
	//--Store position of closed door.
	vRollerDoorClosedPos 	= GET_ENTITY_COORDS(oAbatDoor)
	vRollerDoorRot		 	= GET_ENTITY_ROTATION(oAbatDoor)
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oAbatDoor)
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oAbatDoor)
			SWITCH ambStage
				CASE ambCanRun
					IF DOES_ENTITY_HAVE_DRAWABLE(oAbatDoor)
						//-- Door model is P_Abat_roller_1 
						REQUEST_MODEL(P_Abat_roller_1_col)
						IF HAS_MODEL_LOADED(P_Abat_roller_1_col)
							IF NOT ARE_VECTORS_EQUAL(vRollerDoorClosedPos, <<0.0, 0.0, 0.0>>)
								//-- Create the door coollision
								oRollerDoorCollision = CREATE_OBJECT(P_Abat_roller_1_col,vRollerDoorClosedPos  ) //<<962.4963, -2105.9209, 30.5274 >>
								SET_ENTITY_ROTATION(oRollerDoorCollision, vRollerDoorRot) // <<0.0, 0.0, 90.0>>
								ambStage = ambCheckForAnim
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ambCheckForAnim
					IF DOES_ENTITY_HAVE_DRAWABLE(oAbatDoor)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael2")) > 0
							REQUEST_ANIM_DICT("map_objects")
							IF HAS_ANIM_DICT_LOADED("map_objects")
								ambStage = ambRunning
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ambRunning
					IF DOES_ENTITY_HAVE_DRAWABLE(oAbatDoor)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael2")) > 0
							IF g_bOpenAbattoirRollerDoor
						//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
								//-- Door open anim
								IF HAS_ANIM_DICT_LOADED("map_objects")
									PLAY_ENTITY_ANIM(oAbatDoor, "P_Abat_roller_1_open", "map_objects", 1.0, FALSE, TRUE)
									ambStage = ambOpenDoor
								ENDIF
							ENDIF
						//	ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ambOpenDoor
					VECTOR vRollerDoorOpenPos 
					VECTOR vCurrentRollerDoorPos
					VECTOR vRollerDistanceStillToGo
					FLOAT fRollerDoorMoveMult
					
					fRollerDoorMoveMult = 0.35
					vRollerDoorOpenPos = vRollerDoorClosedPos + <<0.0, 0.0, 3.45>>
					
					IF DOES_ENTITY_EXIST(oRollerDoorCollision)
						vCurrentRollerDoorPos = GET_ENTITY_COORDS(oRollerDoorCollision)
						
						IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentRollerDoorPos, vRollerDoorOpenPos,0.1)// <<962.4963, -2105.9209, 33.9774 >>
							vRollerDistanceStillToGo = vRollerDoorOpenPos  - vCurrentRollerDoorPos
							SET_ENTITY_COORDS(oRollerDoorCollision, (vCurrentRollerDoorPos + (NORMALISE_VECTOR(vRollerDistanceStillToGo) * GET_FRAME_TIME() * fRollerDoorMoveMult)))
						ELSE
							g_bAbattoirRollerDoorHasOpened = TRUE
							ambStage = ambEnd
						ENDIF
					ENDIF
				BREAK
				CASE ambEnd
				
				BREAK
			ENDSWITCH 
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

