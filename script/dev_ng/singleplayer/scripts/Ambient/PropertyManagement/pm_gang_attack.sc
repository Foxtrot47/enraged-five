//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//      SCRIPT NAME	:	Property Management - Gang Attack								//
//      AUTHOR		:	Michael Wadelin													//
//      DESCRIPTION	:	Recover Stolen missions for property management.				//
// 																						//
//////////////////////////////////////////////////////////////////////////////////////////
 
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

 
// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "properties_public.sch"
USING "shared_hud_displays.sch"
USING "script_ped.sch"
USING "asset_management_public.sch"
USING "shared_hud_displays.sch"
 
CONST_FLOAT							FAIL_DISTANCE								300.0
CONST_FLOAT							WARN_DISTANCE								50.0
CONST_FLOAT							TALK_DISTANCE								30.0

STRUCT PROPERTY_GANG_ATTACK_PARAMS
	PROPERTY_GANG_ATTACK_PED		sPedData[NUM_PROPERTY_GANG_ENEMIES]
	TEXT_LABEL_15					strDiaAppendix
	TEXT_LABEL_15					strVoiceID
ENDSTRUCT


INT 								iMissionStage
PROPERTY_GANG_ATTACK_PARAMS 		sStartParams
GANG_ATTACK_MISSION_VARIATION_ENUM  eVariation
structPedsForConversation			sConvo
INT									iDialogueFlag								= -1
INT									iDialogueLastConvoTime						= -1
TEXT_LABEL_7						strDialogue									= "PMGAUD"
INT									iFleeLeader									= -1
INT									iFleeLeaderPrev								= -1
BOOL								bLoseCopsShown								= FALSE
BOOL 								bNoEscapeWarn								= FALSE
BOOL 								bAlerted									= FALSE

VEHICLE_INDEX 						vehGang[NUM_PROPERTY_GANG_VEHS]
PED_INDEX 							pedGang[NUM_PROPERTY_GANG_ENEMIES]
INT									iAIStage[NUM_PROPERTY_GANG_ENEMIES]
BLIP_INDEX							blipEnemies[NUM_PROPERTY_GANG_ENEMIES]
REL_GROUP_HASH						relGang
MODEL_NAMES							modVeh
MODEL_NAMES							modPed

#IF IS_DEBUG_BUILD
	BOOL 							debug_bDrawDebug
#ENDIF


// cleanup the mission
PROC MISSION_CLEANUP()

	INT i
	
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
		IF DOES_ENTITY_EXIST(pedGang[i])
			IF NOT IS_PED_INJURED(pedGang[i])
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_SMART_FLEE_PED(pedGang[i], PLAYER_PED_ID(), 1000, -1, FALSE, TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedGang[i])
		ENDIF	
		
		IF DOES_BLIP_EXIST(blipEnemies[i])
			REMOVE_BLIP(blipEnemies[i])
		ENDIF
	
	ENDFOR
	
	FOR i = 0 TO NUM_PROPERTY_GANG_VEHS-1
		IF DOES_ENTITY_EXIST(vehGang[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGang[i])
		ENDIF	
	ENDFOR

	SET_PED_MODEL_IS_SUPPRESSED(modVeh, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modPed, FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	//REMOVE_RELATIONSHIP_GROUP(relGang)

	TERMINATE_THIS_THREAD() 
ENDPROC


// pass the mission
PROC MISSION_PASSED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_SUCCESS)
	MISSION_CLEANUP()
ENDPROC


// fail the mission
PROC MISSION_FAILED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	MISSION_CLEANUP()
ENDPROC

/// PURPOSE:
///    Appends the players first name initial to the end of the text label
/// PARAMS:
///    eChar - the player character name to append for
///    rootLabel - the root label to append to
/// RETURNS:
///    Returns the new root label with the appended initial
FUNC TEXT_LABEL_15 APPEND_PLAYER_ID_TO_CONV_ROOT(STRING rootLabel)

	TEXT_LABEL_15 result = rootLabel

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	// GUNMEN
		CASE CHAR_MICHAEL
			result += "_M"
		BREAK
		CASE CHAR_FRANKLIN
			result += "_F"
		BREAK
		CASE CHAR_TREVOR
			result += "_T"
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_MISSION, "APPEND_PLAYER_ID_TO_CONV_ROOT() conveted passed rootLabel from \"",  rootLabel, "\" to \"", result, "\".")
	RETURN result

ENDFUNC

FUNC STRING GET_GANG_VOICE_ID(PROPERTY_GANG_ENUM eGang)
	SWITCH eGang
		CASE PROPERTYGANG_BALLAS			RETURN "BALLA1"				BREAK
		CASE PROPERTYGANG_TRIAD				RETURN "KOREANGUY"			BREAK
		CASE PROPERTYGANG_VAGOS				RETURN "VAGOS"				BREAK
		DEFAULT								RETURN ""					BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GANG_ID_LABEL(PROPERTY_GANG_ENUM eGang)
	SWITCH eGang
		CASE PROPERTYGANG_BALLAS			RETURN "BAL"		BREAK
		CASE PROPERTYGANG_TRIAD				RETURN "KOR"		BREAK
		CASE PROPERTYGANG_VAGOS				RETURN "VAG"		BREAK
		DEFAULT								RETURN ""			BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Sets up the general mission stuff
PROC MISSION_SETUP()

	#IF IS_DEBUG_BUILD
		START_WIDGET_GROUP("AMB: Property - Recover Stolen")
		STOP_WIDGET_GROUP()
	#ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
		BREAK
		CASE CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(sConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
		BREAK
		CASE CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(sConvo, 2, PLAYER_PED_ID(), "TREVOR")
		BREAK
	ENDSWITCH
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	SET_CREATE_RANDOM_COPS(FALSE)
	
	eVariation = GET_CURRENT_GANG_ATTACK_VARIATION()
	
// Intialise the mission params
	PROPERTY_GANG_ENUM	eGang

	SWITCH GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY()
		CASE PROPERTY_BAR_TEQUILALA			eGang = PROPERTYGANG_VAGOS			BREAK
		CASE PROPERTY_BAR_PITCHERS			eGang = PROPERTYGANG_BALLAS			BREAK
		CASE PROPERTY_BAR_HEN_HOUSE			eGang = PROPERTYGANG_TRIAD			BREAK
		CASE PROPERTY_BAR_HOOKIES			eGang = PROPERTYGANG_VAGOS			BREAK
	ENDSWITCH
	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[0], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 0)	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[1], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 1)	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[2], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 2)	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[3], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 3)	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[4], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 4)	
	GET_GANG_ATTACK_PED_DATA(sStartParams.sPedData[5], GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), 5)	

	sStartParams.strDiaAppendix	= GET_GANG_ID_LABEL(eGang)
	sStartParams.strVoiceID		= GET_GANG_VOICE_ID(eGang)
	
	ADD_RELATIONSHIP_GROUP("relGang", relGang) 
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGang)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGang, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGang, RELGROUPHASH_COP)
	
	SWITCH eGang
		CASE PROPERTYGANG_VAGOS			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGang, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_MEXICAN, relGang)
		BREAK
		CASE PROPERTYGANG_BALLAS		
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_BALLAS)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGang, RELGROUPHASH_AMBIENT_GANG_BALLAS)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_BALLAS, relGang)
		BREAK
		CASE PROPERTYGANG_TRIAD			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_WEICHENG)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGang, RELGROUPHASH_AMBIENT_GANG_WEICHENG)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_WEICHENG, relGang)
		BREAK
	ENDSWITCH

ENDPROC

FUNC INT GET_NEAREST_GANG_MEMBER_TO_SPEAK()

	INT iNearest = -1
	INT i
	FLOAT fDist = 999999
	
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
		IF DOES_ENTITY_EXIST(pedGang[i])
		AND NOT IS_PED_INJURED(pedGang[i])
			FLOAT fDistTemp = GET_DISTANCE_BETWEEN_ENTITIES(pedGang[i], PLAYER_PED_ID())
			
			IF fDistTemp < TALK_DISTANCE
			AND fDistTemp < fDist
				iNearest 	= i
				fDist		= fDistTemp
			ENDIF
			
		ENDIF
	ENDFOR
	
	RETURN iNearest
	
ENDFUNC

FUNC BOOL AI_COMMON_DECISION_MAKER(INT iPedIndex)

	INT j
	FOR j = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
		IF DOES_ENTITY_EXIST(pedGang[j])
		AND NOT IS_PED_INJURED(pedGang[j])
		AND iAIStage[j] > 0
			bAlerted = TRUE
		ENDIF
	ENDFOR
	
	CONST_FLOAT DETECT_DIST 15.0

	IF IS_BITMASK_AS_ENUM_SET(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_FORCED_MISSION_START)
	OR CAN_PED_SEE_HATED_PED(pedGang[iPedIndex], PLAYER_PED_ID())
	OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedGang[iPedIndex])
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedGang[iPedIndex])
	OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST, TRUE) 
	OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST, TRUE)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS, GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE, GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR, GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(pedGang[iPedIndex]), DETECT_DIST)
	OR bAlerted
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC AI_RUN_STAY_AND_FIGHT_LOGIC()
	
	INT i
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
	
		INT j
	
		IF DOES_ENTITY_EXIST(pedGang[i])
		AND NOT IS_PED_INJURED(pedGang[i])
			
			// Update state
			SWITCH iAIStage[i]
				CASE 0
					IF AI_COMMON_DECISION_MAKER(i)
						iDialogueFlag = 0
						iAIStage[i]++
					ENDIF
				BREAK
				CASE 1
					// If only 2 peds left, jump to flee state
					INT iEnemyCount
					iEnemyCount = 0
					FOR j = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
						IF DOES_ENTITY_EXIST(pedGang[j])
						AND NOT IS_PED_INJURED(pedGang[j])
							iEnemyCount++
						ENDIF
					ENDFOR
					IF iEnemyCount <= 2
						iAIStage[i]++
					ENDIF
				BREAK
				CASE 2
					
				BREAK
			ENDSWITCH
			
			IF iAIStage[i] > 0
				WEAPON_TYPE eWeap
				eWeap = WEAPONTYPE_INVALID
				GET_CURRENT_PED_WEAPON(pedGang[i], eWeap)
				IF eWeap = WEAPONTYPE_MOLOTOV
					IF HAS_PED_GOT_WEAPON(pedGang[i], WEAPONTYPE_MICROSMG)
						SET_CURRENT_PED_WEAPON(pedGang[i], WEAPONTYPE_MICROSMG, TRUE)
					ELSE
						GIVE_WEAPON_TO_PED(pedGang[i], WEAPONTYPE_MICROSMG, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Update tasks
			SWITCH iAIStage[i]
				CASE 0
					PROPERTY_GANG_ATTACK_COMMON_AI_START(pedGang[i], sStartParams.sPedData[i].vAimCoord)
				BREAK
				CASE 1
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_COMBAT)
						TASK_COMBAT_PED(pedGang[i], PLAYER_PED_ID())
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_SMART_FLEE_PED)
						TASK_SMART_FLEE_PED(pedGang[i], PLAYER_PED_ID(), 1000.0, -1, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		
		ENDIF
		
	ENDFOR
	
// Dialogue
//-------------------------------------------------------------------------------------------
	INT iSpeaker
	TEXT_LABEL_15 strReuse

	SWITCH iDialogueFlag
		CASE 0
			IF IS_SAFE_TO_START_CONVERSATION()
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
					PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SUSPECT_SPOTTED", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()	
				strReuse = APPEND_PLAYER_ID_TO_CONV_ROOT("START")
				IF CREATE_CONVERSATION(sConvo, strDialogue, strReuse, CONV_PRIORITY_HIGH)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
					PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SHOUT_THREATEN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - iDialogueLastConvoTime > 3000
			AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "BLIND_RAGE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				iDialogueFlag++
			ENDIF
		BREAK
		CASE 4
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - iDialogueLastConvoTime > 1000
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
				
					// fleeing dialogue
					IF iAIStage[iSpeaker] = 2
						PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "GENERIC_INSULT", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						strReuse = ""
						iDialogueFlag--
					// combat dialogue
					ELSE
						PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SHOUT_THREATEN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						strReuse = ""
						iDialogueFlag--
					ENDIF
					
				ENDIF

			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC AI_RUN_FLEE_IN_VEHICLES_LOGIC()
	
	INT i
	INT iPedsGettingToVeh[NUM_PROPERTY_GANG_VEHS]

	FOR i = 0 TO NUM_PROPERTY_GANG_VEHS-1
		IF DOES_ENTITY_EXIST(vehGang[i])
		AND IS_VEHICLE_DRIVEABLE(vehGang[i])
			INT j
			FOR j = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
				IF DOES_ENTITY_EXIST(pedGang[j])
				AND NOT IS_PED_INJURED(pedGang[j])
					IF sStartParams.sPedData[j].iVehToGetTo != -1
					AND sStartParams.sPedData[j].iVehToGetTo = i
						IF NOT IS_PED_IN_VEHICLE(pedGang[j], vehGang[i])
							iPedsGettingToVeh[i]++
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		TEXT_LABEL_23 strDebug
		strDebug = "iPedsGettingToVeh[" 
		strDebug += i
		strDebug += "]: "
		strDebug += iPedsGettingToVeh[i]
		DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.25+(i*0.025), 0.0>>, 255, 255, 255, 255)
	ENDFOR

	// Main updated
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
	
		INT j
	
		IF DOES_ENTITY_EXIST(pedGang[i])
		AND NOT IS_PED_INJURED(pedGang[i])

			SWITCH iAIStage[i]
				CASE 0
					IF AI_COMMON_DECISION_MAKER(i)
						iDialogueFlag = 0
						iAIStage[i]++
					ENDIF
				BREAK
				CASE 1
					
					IF sStartParams.sPedData[i].iVehToGetTo = -1
					
						IF NOT GET_PED_CONFIG_FLAG(pedGang[i], PCF_ShouldChargeNow)
							SET_PED_CONFIG_FLAG(pedGang[i], PCF_ShouldChargeNow, TRUE)
						ENDIF
					
					ELSE
	
						BOOL bReadyToDriveOff
						bReadyToDriveOff = FALSE
						VEHICLE_INDEX vehTemp
						
						IF IS_PED_IN_ANY_VEHICLE(pedGang[i])
							
							vehTemp = GET_VEHICLE_PED_IS_IN(pedGang[i])
							FOR j = 0 TO NUM_PROPERTY_GANG_VEHS-1
								IF NOT bReadyToDriveOff
									IF vehGang[j] = vehTemp
										IF iPedsGettingToVeh[j] = 0
											bReadyToDriveOff = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDFOR

							IF bReadyToDriveOff
								IF iFleeLeader = -1
								AND GET_SEAT_PED_IS_IN(pedGang[i]) = VS_DRIVER
									iFleeLeader = i
								ENDIF
								iAIStage[i]++
							ENDIF
							
						ENDIF
					
					ENDIF
				BREAK
				CASE 2
					VEHICLE_INDEX vehLeader 
					IF iFleeLeader != -1
						iFleeLeaderPrev = iFleeLeader
						IF DOES_ENTITY_EXIST(pedGang[iFleeLeader])
							IF IS_PED_IN_ANY_VEHICLE(pedGang[iFleeLeader])
								vehLeader = GET_VEHICLE_PED_IS_IN(pedGang[iFleeLeader])
							ENDIF
						ENDIF
					ENDIF

					// Current leader is dead, find a new one
					// OR if vehicle leader is in is fucked
					IF iFleeLeader = -1
					OR NOT DOES_ENTITY_EXIST(pedGang[iFleeLeader]) OR IS_PED_INJURED(pedGang[iFleeLeader])
					OR NOT DOES_ENTITY_EXIST(vehLeader) OR NOT IS_VEHICLE_DRIVEABLE(vehLeader) 
					OR IS_VEHICLE_TYRE_BURST(vehLeader, SC_WHEEL_CAR_FRONT_LEFT, 	TRUE)
					OR IS_VEHICLE_TYRE_BURST(vehLeader, SC_WHEEL_CAR_FRONT_RIGHT, 	TRUE)
					OR IS_VEHICLE_TYRE_BURST(vehLeader, SC_WHEEL_CAR_REAR_LEFT, 	TRUE)
					OR IS_VEHICLE_TYRE_BURST(vehLeader, SC_WHEEL_CAR_REAR_RIGHT, 	TRUE)
						
						INT iNewLeader	
						iNewLeader = -1
						BOOL bIsDriver	
						bIsDriver = FALSE
						
						FOR j = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
							IF DOES_ENTITY_EXIST(pedGang[j])
							AND NOT IS_PED_INJURED(pedGang[j])
							
								IF GET_SEAT_PED_IS_IN(pedGang[j]) = VS_DRIVER
									IF iNewLeader = -1
									OR NOT bIsDriver
										bIsDriver 	= TRUE
										iNewLeader 	= j
									ENDIF
								ELSE
									IF NOT bIsDriver
										iNewLeader 	= j
									ENDIF
								ENDIF
								
							ENDIF
						ENDFOR
						
						IF iNewLeader != -1
							iFleeLeader 		= iNewLeader
						ENDIF
						
					ENDIF
					
				BREAK
			ENDSWITCH
			
			IF iAIStage[i] > 0
				WEAPON_TYPE eWeap
				eWeap = WEAPONTYPE_INVALID
				GET_CURRENT_PED_WEAPON(pedGang[i], eWeap)
				IF eWeap = WEAPONTYPE_MOLOTOV
					IF HAS_PED_GOT_WEAPON(pedGang[i], WEAPONTYPE_MICROSMG)
						SET_CURRENT_PED_WEAPON(pedGang[i], WEAPONTYPE_MICROSMG, TRUE)
					ELSE
						GIVE_WEAPON_TO_PED(pedGang[i], WEAPONTYPE_MICROSMG, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH iAIStage[i]
				CASE 0
					PROPERTY_GANG_ATTACK_COMMON_AI_START(pedGang[i], sStartParams.sPedData[i].vAimCoord)
				BREAK
				CASE 1
					IF sStartParams.sPedData[i].iVehToGetTo = -1
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_COMBAT)
							TASK_COMBAT_PED(pedGang[i], PLAYER_PED_ID())
						ENDIF
					
					ELSE
												
						// Get ped to combat to the vehicle and get in
						IF vehGang[sStartParams.sPedData[i].iVehToGetTo] 	= NULL
						OR sStartParams.sPedData[i].eVehSeatToGetTo			= VS_ANY_PASSENGER
						
						ELIF NOT IS_PED_IN_VEHICLE(pedGang[i], vehGang[sStartParams.sPedData[i].iVehToGetTo])
						
							IF GET_DISTANCE_BETWEEN_ENTITIES(pedGang[i], vehGang[sStartParams.sPedData[i].iVehToGetTo]) > 4.0
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_COMBAT)
									SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(pedGang[i], vehGang[sStartParams.sPedData[i].iVehToGetTo], <<0,0,0>>, 4.0)
									TASK_COMBAT_PED(pedGang[i], PLAYER_PED_ID())
								ENDIF
							ELSE
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_ENTER_VEHICLE)
									TASK_ENTER_VEHICLE(pedGang[i], vehGang[sStartParams.sPedData[i].iVehToGetTo], DEFAULT_TIME_NEVER_WARP, sStartParams.sPedData[i].eVehSeatToGetTo, PEDMOVE_RUN)
								ENDIF
							ENDIF
							
							// If the vehicle has been destroyed, need to handle it
							IF NOT IS_VEHICLE_DRIVEABLE(vehGang[sStartParams.sPedData[i].iVehToGetTo])
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_COMBAT)
									TASK_COMBAT_PED(pedGang[i], PLAYER_PED_ID())
								ENDIF
							ENDIF
							
						// Combat from the vehicle while waiting for all the gang to get in their cars
						ELSE
							
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_VEHICLE_SHOOT_AT_ENTITY)
								TASK_VEHICLE_SHOOT_AT_PED(pedGang[i], PLAYER_PED_ID())
							ENDIF
						
						ENDIF
					ENDIF
					
				BREAK
				CASE 2	
				
					IF IS_PED_IN_ANY_VEHICLE(pedGang[i])

						// Task the leader
						IF iFleeLeader = i
						
							IF GET_SEAT_PED_IS_IN(pedGang[i]) != VS_DRIVER
							
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
									TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(pedGang[i], GET_VEHICLE_PED_IS_IN(pedGang[i]))
								ENDIF
							
							ELSE
						
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_VEHICLE_MISSION)
								OR iFleeLeader != iFleeLeaderPrev
								
									TASK_VEHICLE_MISSION_PED_TARGET(pedGang[i], GET_VEHICLE_PED_IS_IN(pedGang[i]), PLAYER_PED_ID(), MISSION_FLEE, 40.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 0.0, TRUE)
									
								ELIF GET_SCRIPT_TASK_STATUS(pedGang[i], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
								
									IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedGang[i])
										TASK_DRIVE_BY(pedGang[i], PLAYER_PED_ID(), null, <<0,0,0>>, 1000, 100, TRUE)
									ENDIF
									
								ENDIF
							
							ENDIF
						
						// Task the follow driver
						ELIF GET_SEAT_PED_IS_IN(pedGang[i]) = VS_DRIVER
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_VEHICLE_MISSION)
							OR iFleeLeader != iFleeLeaderPrev
							
								TASK_VEHICLE_ESCORT(pedGang[i], GET_VEHICLE_PED_IS_IN(pedGang[i]), GET_VEHICLE_PED_IS_IN(pedGang[iFleeLeader]), VEHICLE_ESCORT_REAR, 40.0, DRIVINGMODE_AVOIDCARS_RECKLESS)
								
							ELIF GET_SCRIPT_TASK_STATUS(pedGang[i], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
							
								IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedGang[i])
									TASK_DRIVE_BY(pedGang[i], PLAYER_PED_ID(), null, <<0,0,0>>, 1000, 100, TRUE)
								ENDIF
							
							ENDIF
						
						// Task the passengers
						ELSE
							// Driver of this passenger's vehicle is dead, shuffle him over
							PED_INDEX pedDriver 
							pedDriver = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(pedGang[i]), VS_DRIVER)
							IF NOT DOES_ENTITY_EXIST(pedDriver)
							OR IS_PED_INJURED(pedDriver)
							
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
									TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(pedGang[i], GET_VEHICLE_PED_IS_IN(pedGang[i]))
								ENDIF
								
							// Still alive, continue driveby
							ELSE
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGang[i], SCRIPT_TASK_DRIVE_BY)
								OR iFleeLeader != iFleeLeaderPrev
									TASK_DRIVE_BY(pedGang[i], PLAYER_PED_ID(), null, <<0,0,0>>, 1000, 100, TRUE)
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF

				BREAK
			ENDSWITCH
			
		ENDIF
		
	ENDFOR

// Dialogue
//-------------------------------------------------------------------------------------------
	INT iSpeaker
	TEXT_LABEL_15 strReuse

	SWITCH iDialogueFlag
		CASE 0
			IF IS_SAFE_TO_START_CONVERSATION()
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
					PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SUSPECT_SPOTTED", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()	
				strReuse = APPEND_PLAYER_ID_TO_CONV_ROOT("START")
				IF CREATE_CONVERSATION(sConvo, strDialogue, strReuse, CONV_PRIORITY_HIGH)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
					PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SHOUT_THREATEN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					strReuse = ""
					iDialogueFlag++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - iDialogueLastConvoTime > 3000
				AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "BLIND_RAGE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				iDialogueFlag++
			ENDIF
		BREAK
		CASE 4
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - iDialogueLastConvoTime > 1000
				iSpeaker = GET_NEAREST_GANG_MEMBER_TO_SPEAK()
				IF iSpeaker != -1
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedGang[iSpeaker])
				
					// fleeing dialogue
					IF IS_PED_IN_ANY_VEHICLE(pedGang[iSpeaker])
						PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "GENERIC_INSULT", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						strReuse = ""
						iDialogueFlag--
					// combat dialogue
					ELSE
						PLAY_PED_AMBIENT_SPEECH(pedGang[iSpeaker], "SHOUT_THREATEN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						strReuse = ""
						iDialogueFlag--
					ENDIF
					
				ENDIF

			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Decides which variation of the mission to run
PROC MISSION_FLOW()

	TEXT_LABEL_15 strReuse
	INT i
	
// Mission flow
//---------------------------------------------------------------------------------
	SWITCH iMissionStage
		CASE 0
			REQUEST_ADDITIONAL_TEXT("PGANG", MISSION_TEXT_SLOT)
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			
			// Grab gang peds from trigger
				FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
					IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[i])
						
						pedGang[i] = g_PropertySystemData.managementEventPed[i]
						g_PropertySystemData.managementEventPed[i] = NULL
						SET_ENTITY_AS_MISSION_ENTITY(pedGang[i], TRUE, TRUE)
						
						IF modPed = DUMMY_MODEL_FOR_SCRIPT
							modPed = GET_ENTITY_MODEL(pedGang[i])
						ENDIF
						
						IF NOT IS_PED_INJURED(pedGang[i])
							
							SET_PED_RELATIONSHIP_GROUP_HASH(pedGang[i], relGang)					
							SET_PED_COMBAT_MOVEMENT(pedGang[i], CM_DEFENSIVE)
							SET_PED_COMBAT_RANGE(pedGang[i], CR_NEAR)
							SET_PED_ACCURACY(pedGang[i], 5)
							SET_PED_TARGET_LOSS_RESPONSE(pedGang[i], TLR_NEVER_LOSE_TARGET)
							SET_PED_VISUAL_FIELD_PROPERTIES(pedGang[i], 20, 5.0, 90, -80, 80)
							IF eVariation = GANG_ATTACK_MISSION_VARIATION_STAYS_AND_FIGHTS
								SET_PED_COMBAT_MOVEMENT(pedGang[i], CM_WILLADVANCE)
							ENDIF
							
							blipEnemies[i] = CREATE_BLIP_FOR_PED(pedGang[i], TRUE)
						ENDIF
					ENDIF
				ENDFOR
				
			// Grab vehicles from trigger
				FOR i = 0 TO NUM_PROPERTY_GANG_VEHS-1
					IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[i])
						vehGang[i] = g_PropertySystemData.managementEventVehicle[i]
						g_PropertySystemData.managementEventVehicle[i] = NULL
						SET_ENTITY_AS_MISSION_ENTITY(vehGang[i], TRUE, TRUE)
						
						IF modVeh = DUMMY_MODEL_FOR_SCRIPT
							modVeh = GET_ENTITY_MODEL(vehGang[i])
						ENDIF
					ENDIF
				ENDFOR
				
				strReuse = "START_" 
				strReuse += sStartParams.strDiaAppendix
				PRINT_NOW(strReuse, DEFAULT_GOD_TEXT_TIME, 1)
			
				iMissionStage++
			
			ENDIF
		BREAK
		CASE 1
			
			BOOL bHasKickedOff
			BOOL bAllDead
			bAllDead = TRUE
			FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
			
				IF NOT bHasKickedOff
					IF iAIStage[i] > 0
						bHasKickedOff = TRUE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedGang[i]) AND NOT IS_PED_INJURED(pedGang[i])
					bAllDead = FALSE
				ENDIF
			ENDFOR
			
			IF bHasKickedOff
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			ENDIF
			
			IF bAllDead
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT bLoseCopsShown
						PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 1)
						bLoseCopsShown = TRUE
					ENDIF
				ELSE
					MISSION_PASSED()
				ENDIF
			ENDIF

		BREAK
	ENDSWITCH
	
// AI
//-----------------------------------------------------------------------------------------
	SWITCH eVariation
		CASE GANG_ATTACK_MISSION_VARIATION_FLEES_ON_SIGHT		AI_RUN_FLEE_IN_VEHICLES_LOGIC()			BREAK
		CASE GANG_ATTACK_MISSION_VARIATION_STAYS_AND_FIGHTS		AI_RUN_STAY_AND_FIGHT_LOGIC() 			BREAK
	ENDSWITCH

	
// Clean up & checks
//---------------------------------------------------------------------------------------------
	FOR i = 0 TO NUM_PROPERTY_GANG_ENEMIES-1
		
		IF DOES_ENTITY_EXIST(pedGang[i])
		
			// Cleanup dead enemies
			IF IS_PED_INJURED(pedGang[i])
				SET_PED_AS_NO_LONGER_NEEDED(pedGang[i])
			ELSE
				// Distance check for fail
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedGang[i], GET_PROPERTY_PURCHASE_COORDS(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY())) > 100.0
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedGang[i]) > FAIL_DISTANCE
					MISSION_FAILED()
					
				ELIF bAlerted 
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedGang[i], GET_PROPERTY_PURCHASE_COORDS(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY())) > WARN_DISTANCE
					IF IS_SAFE_TO_DISPLAY_GODTEXT()
						IF NOT bNoEscapeWarn
							strReuse = "ESC_"
							strReuse += sStartParams.strDiaAppendix
							PRINT_NOW(strReuse, DEFAULT_GOD_TEXT_TIME, 1)
							bNoEscapeWarn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		// cleanup blips as ped die
		IF DOES_BLIP_EXIST(blipEnemies[i])
			IF NOT DOES_ENTITY_EXIST(pedGang[i]) OR IS_PED_INJURED(pedGang[i])
				REMOVE_BLIP(blipEnemies[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		MISSION_CLEANUP()
	ENDIF
	
	MISSION_SETUP()
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueLastConvoTime = -1
		ELSE
			IF iDialogueLastConvoTime = -1
				iDialogueLastConvoTime = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		MISSION_FLOW()
		
		#IF IS_DEBUG_BUILD
		
			IF debug_bDrawDebug
				TEXT_LABEL_31 strDebug = "MissionStage: "
				strDebug += iMissionStage
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.05, 0.0>>, 255, 255, 255, 255)
				strDebug = "FleeLeader: "
				strDebug += iFleeLeader
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.075, 0.0>>, 255, 255, 255, 255)
				strDebug = "DialogueStage: "
				strDebug += iDialogueFlag
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.1, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[0]: "
				strDebug += iAIStage[0]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.125, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[1]: "
				strDebug += iAIStage[1]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.15, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[2]: "
				strDebug += iAIStage[2]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.175, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[3]: "
				strDebug += iAIStage[3]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.2, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[4]: "
				strDebug += iAIStage[4]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.225, 0.0>>, 255, 255, 255, 255)
				strDebug = "AIStage[5]: "
				strDebug += iAIStage[5]
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.25, 0.0>>, 255, 255, 255, 255)
			ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
				debug_bDrawDebug = !debug_bDrawDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(debug_bDrawDebug)
			ENDIF
		#ENDIF
    ENDWHILE
ENDSCRIPT
