//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//      SCRIPT NAME	:	Property Management - Delivery									//
//      AUTHOR		:	Tor Sigurdson													//
//      DESCRIPTION	:	Delivery missions for property management.						//
// 																						//
//////////////////////////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "properties_public.sch"
USING "shared_hud_displays.sch"
USING "Locates_public.sch"
USING "chase_hint_cam.sch"

DELIVERY_MISSION_VARIATION_ENUM deliveryVariation

ENUM propertyStageFlag
	propertyCanRun,
	propertyRunning
ENDENUM
propertyStageFlag ambStage = propertyCanRun

//ENUM eventVariations
//	TARGET_NONE,
//	WEED_SHOP,
//	LIQUOR_STORE,
//	TARGET_END
//ENDENUM
//eventVariations thisEvent = TARGET_NONE

ENUM RUNNING_STAGE_ENUM
	SETUP_STAGE, //0
	GET_DELIVERY, //1
	RETURN_DELIVERY, //2
	TOO_LATE, //3
	TOO_DAMAGED //4
ENDENUM
RUNNING_STAGE_ENUM deliveryStage = SETUP_STAGE

LOCATES_HEADER_DATA	sLocatesData

VECTOR vDeliveryOrigin
VECTOR vDeliveryDestination
VECTOR vVehCop

FLOAT fVehDelivery
FLOAT fVehCop

BOOL bAssetsLoaded
BOOL bVariablesInitialised
BOOL bDoFullCleanUp
BOOL bPlayerWanted
BOOL bDeliveryFleeing

INT iTimeLimit
INT icountdownSound = 0

//INT iTimeLimitHour
//INT iTimeLimitMin
INT iTimeLimitHourModifier
INT iTimeLimitMinModifier
INT iWarningTime

INT iVehicleExtra = -1 //needed to set the booze truck default -1 to not set any

CONST_INT NUM_DELIVERY_PEDS 2
CONST_INT NUM_COP_PEDS 2
CONST_INT CLOCK_WARNING_MINUTES 30

MODEL_NAMES modelPedDelivery
MODEL_NAMES modelPedCop
MODEL_NAMES modelVehDelivery
MODEL_NAMES modelVehCop

PED_INDEX pedDelivery[NUM_DELIVERY_PEDS]
PED_INDEX pedCop[NUM_COP_PEDS]
VEHICLE_INDEX vehDelivery
VEHICLE_INDEX vehCop
VEHICLE_INDEX vehImpound
BLIP_INDEX blipPed[NUM_DELIVERY_PEDS]
BLIP_INDEX blipVehicle

String sGetIn		= "PMDL_OBJ"	
String sObjective	= "PMDL_LOC"
string sGetback		= "PMDL_BCK"

CHASE_HINT_CAM_STRUCT structHintCam

REL_GROUP_HASH rghCop
REL_GROUP_HASH rghCriminal

SEQUENCE_INDEX Seq

#if IS_DEBUG_BUILD
	WIDGET_GROUP_ID widget_debug
#endif

// cleanup the mission
PROC MISSION_CLEANUP()
	PRINTLN("\n@@@@@@@@@@@@@@@@@@@@ MISSION_CLEANUP @@@@@@@@@@@@@@@@@@@@\n")
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BENSON, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PONY2, FALSE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	IF bDoFullCleanUp
		IF NOT IS_ENTITY_DEAD(vehImpound)
			SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
		ENDIF
		
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		KILL_CHASE_HINT_CAM(structHintCam)
	ENDIF
	
	TERMINATE_THIS_THREAD() 
ENDPROC

// pass the mission
PROC MISSION_PASSED()
	PRINTLN("\n@@@@@@@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@@@@@@\n")
	
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_SUCCESS)
	MISSION_CLEANUP()
ENDPROC

// fail the mission
PROC MISSION_FAILED()
	PRINTLN("\n@@@@@@@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@@@@@@\n")
	
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	MISSION_CLEANUP()
ENDPROC

PROC initialisePropertyMissionVariables()
	float fDistance
	vDeliveryOrigin = GET_DELIVERY_EVENT_INIT_COORDS(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), deliveryVariation)
	fVehDelivery = GET_DELIVERY_EVENT_INIT_HEADING(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), deliveryVariation)
	
	if deliveryVariation > DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
		sGetIn				= "PMDL_TRUCK"	
		sObjective			= "PMDL_BTIM"
		sGetback			= "PMDL_BCKT"
		modelVehDelivery 	= BENSON	
		iVehicleExtra 		= 2
		
	endif
	
	IF GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() = PROPERTY_WEED_SHOP
		vDeliveryDestination = << -1161.213257, -1567.067749, 3.4234 >>
		modelVehDelivery = PONY2
		
		IF deliveryVariation = DELIVERY_MISSION_VARIATION_TIMED_1
			iTimeLimitHourModifier = 2
			iTimeLimitMinModifier = 15
			sObjective = "PMDL_TIM"
			
		ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_TIMED_2
			iTimeLimitHourModifier = 1
			iTimeLimitMinModifier = 30
			sObjective = "PMDL_TIM"
			
		ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_TIMED_3
			iTimeLimitHourModifier = 1
			iTimeLimitMinModifier = 30
			sObjective = "PMDL_TIM"
			
		ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_LOSE_COPS
			vVehCop = << -3121.2615, 1152.9200, 19.4047 >>
			fVehCop = 176.4887
			modelVehCop = POLICE4
			modelPedCop = S_M_Y_COP_01 //MP_M_GenWit_01
			
		ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_LOSE_COPS_2
			vVehCop = << 1543.1453, 2184.3706, 77.8114 >>
			fVehCop = 45.3499
			modelVehCop = POLICE4
			modelPedCop = S_M_Y_COP_01 //MP_M_GenWit_01
			
		ELIF deliveryVariation >= DELIVERY_MISSION_VARIATION_RECOVER_VAN
			modelPedDelivery = G_M_Y_SALVAGOON_02
			
		ENDIF
	//******************** BOOZE DELIVERY *********************	
	
	elif GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() = PROPERTY_BAR_HOOKIES
		vDeliveryDestination = <<-2169.8284, 4277.3652, 47.9568>>
		fDistance = GET_DISTANCE_BETWEEN_COORDS(vDeliveryOrigin,vDeliveryDestination)		
		iTimeLimitMinModifier 	= round((( fDistance / 16.5 ) + 40)/2) // 15 mins grace to distance. Tested distance works out at 2000m can be done in 2 mins
	elif GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() = PROPERTY_BAR_HEN_HOUSE
		vDeliveryDestination = <<-323.3560, 6264.4307, 30.4463>>
		fDistance = GET_DISTANCE_BETWEEN_COORDS(vDeliveryOrigin,vDeliveryDestination)
		iTimeLimitMinModifier 	= round((( fDistance / 16.5 ) + 40)/2)
	elif GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() = PROPERTY_BAR_PITCHERS
		vDeliveryDestination = <<198.5282, 342.2399, 104.9566>>
		fDistance = GET_DISTANCE_BETWEEN_COORDS(vDeliveryOrigin,vDeliveryDestination)
		iTimeLimitMinModifier 	= round((( fDistance / 16.5 ) + 15)/2)
	elif GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() = PROPERTY_BAR_TEQUILALA
		vDeliveryDestination = <<-560.0195, 301.1481, 82.1436>>		
		fDistance = GET_DISTANCE_BETWEEN_COORDS(vDeliveryOrigin,vDeliveryDestination)
		iTimeLimitMinModifier 	= round((( fDistance / 16.5 ) + 15)/2)
	ENDIF
	icountdownSound = 0
	
	bVariablesInitialised = TRUE
ENDPROC

PROC loadAssets()

	REQUEST_MODEL(modelVehDelivery)
	REQUEST_ADDITIONAL_TEXT("PMDL", MISSION_TEXT_SLOT)
//	REQUEST_ANIM_DICT("")
	IF (deliveryVariation >= DELIVERY_MISSION_VARIATION_LOSE_COPS AND deliveryVariation <= DELIVERY_MISSION_VARIATION_LOSE_COPS_2)
		REQUEST_MODEL(modelPedCop)
		REQUEST_MODEL(modelVehCop)
	ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN or deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN_2	
		REQUEST_MODEL(modelPedDelivery)
		
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BENSON, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PONY2, TRUE)	
	
	IF HAS_MODEL_LOADED(modelVehDelivery)
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
//	AND HAS_ANIM_DICT_LOADED("")
		IF (deliveryVariation >= DELIVERY_MISSION_VARIATION_LOSE_COPS AND deliveryVariation <= DELIVERY_MISSION_VARIATION_LOSE_COPS_2)
			IF HAS_MODEL_LOADED(modelPedCop)
			AND HAS_MODEL_LOADED(modelVehCop)
				bAssetsLoaded = TRUE
			ENDIF
		ELIF deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN or deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
			IF HAS_MODEL_LOADED(modelPedDelivery)
				bAssetsLoaded = TRUE
			ENDIF
		ELIF deliveryVariation > DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
			if REQUEST_SCRIPT_AUDIO_BANK("Deliveries")
				bAssetsLoaded = TRUE
			ENDIF
		ELSE
			bAssetsLoaded = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC createScene()
	vehDelivery = GET_PROPERTY_MANAGEMENT_EVENT_VEHICLE(PROPERTY_MANAGEMENT_VEHICLE_DELIVERY_VEHICLE)
	IF NOT IS_VEHICLE_DRIVEABLE(vehDelivery)
		vehDelivery = CREATE_VEHICLE(modelVehDelivery, vDeliveryOrigin, fVehDelivery)
		INT i
		FOR i=1 TO 8
			SET_VEHICLE_EXTRA(vehDelivery, i, TRUE)
		ENDFOR		
		if iVehicleExtra != -1
			SET_VEHICLE_EXTRA(vehDelivery,iVehicleExtra,false)
		endif
	ENDIF

	SET_VEHICLE_IS_WANTED(vehDelivery, TRUE)
	
	blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehDelivery)
	
	IF  deliveryVariation != DELIVERY_MISSION_VARIATION_RECOVER_VAN
	and deliveryVariation != DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
		IF  deliveryVariation > DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
			SET_ENTITY_PROOFS(vehDelivery,false,false,false,false,true)//melee proof
//			HINT_SCRIPT_AUDIO_BANK("BAR_DELIVER_BOOZE_RATTLE_MASTER")
			PLAY_SOUND_FROM_ENTITY(-1, "BAR_DELIVER_BOOZE_RATTLE_MASTER", vehDelivery)
		endif
		PRINT_NOW(sGetIn, DEFAULT_GOD_TEXT_TIME, 1)
	ELSE
		pedDelivery[0] = CREATE_PED_INSIDE_VEHICLE(vehDelivery, PEDTYPE_MISSION, modelPedDelivery)
		pedDelivery[1] = CREATE_PED_INSIDE_VEHICLE(vehDelivery, PEDTYPE_MISSION, modelPedDelivery, VS_FRONT_RIGHT)
		ADD_RELATIONSHIP_GROUP("rghCriminal", rghCriminal)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rghCriminal, RELGROUPHASH_PLAYER)
		INT index
		REPEAT COUNT_OF(pedDelivery) index
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDelivery[index], TRUE)
			GIVE_WEAPON_TO_PED(pedDelivery[index], WEAPONTYPE_SAWNOFFSHOTGUN, -1)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedDelivery[index], rghCriminal)
		ENDREPEAT
		TASK_VEHICLE_DRIVE_WANDER(pedDelivery[0], vehDelivery, 25, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
		PRINT_NOW("PMDL_REC", DEFAULT_GOD_TEXT_TIME, 1)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
		vehImpound = GET_PLAYERS_LAST_VEHICLE()
	ENDIF
	
	deliveryStage = GET_DELIVERY
	
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	
	IF DOES_ENTITY_EXIST(vehDelivery)
		IF IS_VEHICLE_DRIVEABLE(vehDelivery)
			IF NOT IS_ENTITY_AT_ENTITY(vehDelivery, PLAYER_PED_ID(), << 300, 300, 300 >>)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(pedDelivery[0])
		IF DOES_BLIP_EXIST(blipPed[0])
			REMOVE_BLIP(blipPed[0])
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_HINT_CAM()
	IF NOT IS_PED_INJURED(pedDelivery[0])
	AND NOT IS_ENTITY_DEAD(vehDelivery)
		IF IS_PED_IN_VEHICLE(pedDelivery[0], vehDelivery)
			CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(structHintCam, vehDelivery)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_DELIVERY_VEHICLE_TAKEN_SUFFICIENT_DAMAGE_TO_PULL_OVER()

	IF DOES_ENTITY_EXIST(vehDelivery)
		IF NOT IS_ENTITY_DEAD(vehDelivery)
			IF GET_ENTITY_HEALTH(vehDelivery) < 300
			OR GET_VEHICLE_ENGINE_HEALTH(vehDelivery) < 200
			OR IS_ENTITY_ON_FIRE(vehDelivery)
			OR (IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_FRONT_RIGHT))
			OR (IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_REAR_LEFT) AND IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_REAR_RIGHT))
			OR (IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_REAR_LEFT))
			OR (IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_FRONT_RIGHT) AND IS_VEHICLE_TYRE_BURST(vehDelivery, SC_WHEEL_CAR_REAR_RIGHT))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC pickupDelivery()

	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDelivery)
	
		if DOES_BLIP_EXIST(blipVehicle)
			REMOVE_BLIP(blipVehicle)
		endif
		
		IF deliveryVariation <= DELIVERY_MISSION_VARIATION_TIMED_3
			iTimeLimit = GET_MILLISECONDS_PER_GAME_MINUTE()*((60*iTimeLimitHourModifier) + iTimeLimitMinModifier)
			iTimeLimit += GET_GAME_TIMER()
			iWarningTime = iTimeLimit - (GET_MILLISECONDS_PER_GAME_MINUTE()*CLOCK_WARNING_MINUTES)
//			iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + iTimeLimitHourModifier
//			iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + iTimeLimitMinModifier
			// Return delivery to the shop before the time runs out.
//			PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
		elif deliveryVariation > DELIVERY_MISSION_VARIATION_RECOVER_VAN_2
			iTimeLimit = GET_MILLISECONDS_PER_GAME_MINUTE()*((60*iTimeLimitHourModifier) + iTimeLimitMinModifier)
			iTimeLimit += GET_GAME_TIMER()
			iWarningTime = iTimeLimit - (GET_MILLISECONDS_PER_GAME_MINUTE()*CLOCK_WARNING_MINUTES)
//			iTimeLimitHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + iTimeLimitHourModifier
//			iTimeLimitMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + iTimeLimitMinModifier
		ELIF (deliveryVariation >= DELIVERY_MISSION_VARIATION_LOSE_COPS AND deliveryVariation <= DELIVERY_MISSION_VARIATION_LOSE_COPS_2)
			vehCop = CREATE_VEHICLE(modelVehCop, vVehCop, fVehCop)
			pedCop[0] = CREATE_PED_INSIDE_VEHICLE(vehCop, PEDTYPE_COP, modelPedCop)
			pedCop[1] = CREATE_PED_INSIDE_VEHICLE(vehCop, PEDTYPE_COP, modelPedCop, VS_FRONT_RIGHT)
			ADD_RELATIONSHIP_GROUP("rghCop", rghCop)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_WANTED, rghCop, RELGROUPHASH_PLAYER)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[0], CA_CAN_BUST, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[1], CA_CAN_BUST, TRUE)
			INT index
			REPEAT COUNT_OF(pedCop) index
				GIVE_WEAPON_TO_PED(pedCop[index], WEAPONTYPE_PISTOL, -1)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[index], TRUE)
				SET_PED_SEEING_RANGE(pedCop[index], 100)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[index], rghCop)
			ENDREPEAT
//			PRINT_NOW(sObjective, DEFAULT_GOD_TEXT_TIME, 1)
		ENDIF
		
		deliveryStage = RETURN_DELIVERY
	ENDIF
	
ENDPROC

PROC recoverDelivery()

	IF NOT IS_ENTITY_DEAD(vehDelivery)
		IF HAS_DELIVERY_VEHICLE_TAKEN_SUFFICIENT_DAMAGE_TO_PULL_OVER()
		OR IS_PED_INJURED(pedDelivery[0])
			INT index
			REPEAT COUNT_OF(pedDelivery) index
				IF NOT IS_PED_INJURED(pedDelivery[index])
					IF NOT DOES_BLIP_EXIST(blipPed[index])
						blipPed[index] = CREATE_BLIP_FOR_PED(pedDelivery[index], TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedDelivery[index], CA_USE_VEHICLE, FALSE)
						OPEN_SEQUENCE_TASK(Seq)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(Seq)
						TASK_PERFORM_SEQUENCE(pedDelivery[index], Seq)
						CLEAR_SEQUENCE_TASK(Seq)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipPed[index])
						REMOVE_BLIP(blipPed[index])
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			IF NOT bDeliveryFleeing
				IF NOT IS_PED_INJURED(pedDelivery[0])
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedDelivery[0], << 50, 50, 50 >>)
						TASK_VEHICLE_MISSION_PED_TARGET(pedDelivery[0], vehDelivery, PLAYER_PED_ID(), MISSION_FLEE, 25, DRIVINGMODE_AVOIDCARS, -1, -1)
						bDeliveryFleeing = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDelivery)
			IF DOES_BLIP_EXIST(blipVehicle)
				REMOVE_BLIP(blipVehicle)
			ENDIF
			KILL_CHASE_HINT_CAM(structHintCam)
			deliveryStage = RETURN_DELIVERY
		ELSE
			IF IS_VEHICLE_SEAT_FREE(vehDelivery)
			OR IS_PED_INJURED(pedDelivery[0])
				KILL_CHASE_HINT_CAM(structHintCam)
			ELSE
				RUN_HINT_CAM()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC returnDelivery()

	IF (deliveryVariation >= DELIVERY_MISSION_VARIATION_LOSE_COPS AND deliveryVariation <= DELIVERY_MISSION_VARIATION_LOSE_COPS_2)
		INT index
		REPEAT COUNT_OF(pedCop) index
			IF NOT bPlayerWanted
				IF NOT IS_PED_INJURED(pedCop[index])
//					IF CAN_PED_SEE_HATED_PED(pedCop[index], PLAYER_PED_ID())
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDeliveryOrigin, << 50, 50, 50 >>)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[index], FALSE)
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
						bPlayerWanted = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT IS_PED_INJURED(pedCop[index])
						TASK_SMART_FLEE_PED(pedCop[index], PLAYER_PED_ID(), 1000, -1)
						SET_PED_KEEP_TASK(pedCop[index], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(pedCop[index])
					ENDIF
					IF DOES_ENTITY_EXIST(vehCop)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCop)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	if IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vDeliveryDestination, g_vOnFootLocate, true, vehDelivery, sObjective, "", sGetback, true)
		println("DELIVERY HERE")
		//Changing above since I had a bug about the locate being too big, we might want to do different checks depending on property.
		BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehDelivery,DEFAULT_VEH_STOPPING_DISTANCE, 2)
		TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),2000)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDelivery,false)
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		MISSION_PASSED()
	endif		
	
	INT index
	REPEAT COUNT_OF(pedDelivery) index
		IF NOT IS_PED_INJURED(pedDelivery[index])
			IF NOT DOES_BLIP_EXIST(blipPed[index])
				blipPed[index] = CREATE_BLIP_FOR_PED(pedDelivery[index], TRUE)
				OPEN_SEQUENCE_TASK(Seq)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(pedDelivery[index], Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				SET_PED_KEEP_TASK(pedDelivery[index], TRUE)
			ENDIF
			IF NOT IS_ENTITY_AT_ENTITY(pedDelivery[index], PLAYER_PED_ID(), << 100, 100, 100 >>)
				IF DOES_BLIP_EXIST(blipPed[index])
					REMOVE_BLIP(blipPed[index])
				ENDIF
				TASK_SMART_FLEE_PED(pedDelivery[index], PLAYER_PED_ID(), 1000, -1)
				SET_PED_KEEP_TASK(pedDelivery[index], TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(pedDelivery[index])
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipPed[index])
				REMOVE_BLIP(blipPed[index])
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
Func bool Is_clock_time_less_than_or_equal(int h,int m)
	if GET_CLOCK_HOURS() < h
		return true
	elif GET_CLOCK_HOURS() = h
		if GET_CLOCK_MINUTES() <= m
			return true
		else		
			return false 
		endif
	elif GET_CLOCK_HOURS() > h
		return false
	endif
	
	return false
endfunc
PROC timeLimitCountdown()
	
	INT iDisplayTime
			
	// Current time remaining converted to milliseconds so generic timer shows it properly
	iDisplayTime = iTimeLimit - GET_GAME_TIMER()
	IF iDisplayTime < 0
		iDisplayTime = 0
	ENDIF
	
	IF GET_GAME_TIMER() >= iWarningTime
		DRAW_GENERIC_TIMER(iDisplayTime, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
	ELSE
		DRAW_GENERIC_TIMER(iDisplayTime, "TIMER_TIME")
	ENDIF
	
	switch icountdownSound
		case 0
			IF GET_GAME_TIMER() >= (iTimeLimit - 10000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 1
			IF GET_GAME_TIMER() >= (iTimeLimit - 9000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 2
			IF GET_GAME_TIMER() >= (iTimeLimit - 8000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 3
			IF GET_GAME_TIMER() >= (iTimeLimit - 7000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 4
			IF GET_GAME_TIMER() >= (iTimeLimit - 6000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 5
			IF GET_GAME_TIMER() >= (iTimeLimit - 5000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 6
			IF GET_GAME_TIMER() >= (iTimeLimit - 4500)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 7
			IF GET_GAME_TIMER() >= (iTimeLimit - 4000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 8
			IF GET_GAME_TIMER() >= (iTimeLimit - 3500)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 9
			IF GET_GAME_TIMER() >= (iTimeLimit - 3000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 10
			IF GET_GAME_TIMER() >= (iTimeLimit - 2500)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 11
			IF GET_GAME_TIMER() >= (iTimeLimit - 2000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 12
			IF GET_GAME_TIMER() >= (iTimeLimit - 1500)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 13
			IF GET_GAME_TIMER() >= (iTimeLimit - 1000)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 14
			IF GET_GAME_TIMER() >= (iTimeLimit - 500)	
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 15
			IF GET_GAME_TIMER() >= (iTimeLimit)	
				PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
				icountdownSound++
			endif	
		break
		case 16
			
		break
	endswitch
	IF GET_GAME_TIMER() >= iTimeLimit
//	if not Is_clock_time_less_than_or_equal(iTimeLimitHour,iTimeLimitMin)	
		deliveryStage = TOO_LATE
	ENDIF
	
ENDPROC
PROC DamageCheck()
	
	if DOES_ENTITY_EXIST(vehDelivery)
	
		if IS_VEHICLE_DRIVEABLE(vehDelivery)
			
			
			int maxHealth = 200
			int currentHealth = GET_ENTITY_HEALTH(vehDelivery) - 800
			
			if IS_ENTITY_UPSIDEDOWN(vehDelivery)
				set_entity_health(vehDelivery,GET_ENTITY_HEALTH(vehDelivery) - 5)
			endif
			
			if 	currentHealth  <= 0
				deliveryStage = TOO_DAMAGED				
			elif currentHealth  < 75
				draw_timer_hud(currentHealth ,maxHealth,"PMDL_DMG")
			else
				draw_timer_hud(currentHealth,maxHealth,"PMDL_DMG",HUD_COLOUR_WHITE)
			endif
		else
			deliveryStage = TOO_DAMAGED
		endif
	else
		deliveryStage = TOO_DAMAGED
	endif
	
endPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		MISSION_CLEANUP()
	ENDIF
	
	deliveryVariation = GET_CURRENT_DELIVERY_VARIATION()
	
	//IF GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY() <> PROPERTY_WEED_SHOP
	//	#IF IS_DEBUG_BUILD
	//		PRINT_STRING_WITH_LITERAL_STRING("STRTNM1", "Placeholder delivery mission - S to pass F to fail", 10000, 0)
	//	#ENDIF
	//ENDIF	
	
	#if IS_DEBUG_BUILD
		widget_debug = START_WIDGET_GROUP("DELIVERY SCRIPT")
			SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
		STOP_WIDGET_GROUP()		
	#endif
	
    WHILE TRUE
	
		SWITCH ambStage
		
			CASE propertyCanRun
				IF NOT bVariablesInitialised
					initialisePropertyMissionVariables()
				ELSE
					loadAssets()
				ENDIF
				
				IF bAssetsLoaded 
					bDoFullCleanUp = TRUE
					ambStage = (propertyRunning)
				ENDIF
			BREAK
			
			CASE propertyRunning
			
				IF NOT IS_PLAYER_INTERFERING_WITH_EVENT()
				
					SWITCH deliveryStage
					
						CASE SETUP_STAGE
							createScene()
						BREAK
						
						CASE GET_DELIVERY
							IF (deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN OR deliveryVariation = DELIVERY_MISSION_VARIATION_RECOVER_VAN_2)
								recoverDelivery()
							ELSE
								pickupDelivery()
							ENDIF
						BREAK
						
						CASE RETURN_DELIVERY
							returnDelivery()
							IF deliveryVariation < DELIVERY_MISSION_VARIATION_LOSE_COPS
								timeLimitCountdown()
							elif deliveryVariation > DELIVERY_MISSION_VARIATION_RECOVER_VAN_2 // all booze delivery
								timeLimitCountdown()
								DamageCheck()
							ENDIF
						BREAK
						
						CASE TOO_LATE
						CASE TOO_DAMAGED
							MISSION_FAILED()
						BREAK
						
					ENDSWITCH
					
				ELSE
					PRINTln("\n@@@@@@@@@@@@@@@@@@@@ IS_PLAYER_INTERFERING_WITH_EVENT @@@@@@@@@@@@@@@@@@@@\n")
					MISSION_FAILED()
				ENDIF				
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				IF deliveryStage = GET_DELIVERY
					IF NOT IS_ENTITY_DEAD(vehDelivery)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDelivery)
					ENDIF
				ELIF deliveryStage = RETURN_DELIVERY
					IF NOT IS_ENTITY_DEAD(vehDelivery)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDelivery)
						SET_ENTITY_COORDS(vehDelivery, vDeliveryDestination)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
		#ENDIF
		
			// main loop	
       		WAIT(0)
		
    ENDWHILE
ENDSCRIPT
