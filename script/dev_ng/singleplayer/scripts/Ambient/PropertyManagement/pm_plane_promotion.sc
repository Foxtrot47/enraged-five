//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//      SCRIPT NAME	:	Property Management - Plane Promotion							//
//      AUTHOR		:	Michael Wadelin													//
//      DESCRIPTION	:	Hose Down Protestors missions for property management.			//
// 																						//
//////////////////////////////////////////////////////////////////////////////////////////
 
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

 
// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "properties_public.sch"
USING "shared_hud_displays.sch"
USING "locates_public.sch"
USING "script_ped.sch"
USING "asset_management_public.sch"

CONST_INT							I_NUM_DROP_ZONES						10

CONST_INT							I_LEAFLET_DROP_COOLDOWN					2000
CONST_INT 							I_LEAFLET_DROP_RATE						100
CONST_FLOAT							F_LEAFLET_DROP_RATE_MAX					1.0

CONST_FLOAT							F_GATE_SIZE_CURRENT						20.0
CONST_FLOAT 						F_OBJECTIVE_UPDATE_RADIUS				400.0
CONST_FLOAT 						F_HELP_TEXT_RADIUS						300.0
CONST_FLOAT							F_DROP_ZONE_RADIUS						200.0
CONST_FLOAT							F_DROP_ZONE_MODIFIER					0.5 // 2 seconds





VECTOR								vFinalHangarBlip						= <<-1078.86, -2943.56, 12.95>>//<<-1085.2794, -2970.5725, 12.9457>>
TEXT_LABEL_15						strDialogue								= "PMPLANE"
TEXT_LABEL_15						strAppendix								= ""

STRUCT DROP_ZONE_STRUCT
	VECTOR 							vCoord
	FLOAT 							fRadius									= 0.0
	BLIP_INDEX						blip									= NULL
	BLIP_INDEX						blipRadius								= NULL
	BOOL							bHasBeenLittered						= FALSE
	INT								iTimeHit								= -1
ENDSTRUCT

INT 								iMissionStage							= 0
INT									iDialogueStage							= 0
INT									iDialogueLastConvoTime					= -1
BLIP_INDEX							blipOjbective
LOCATES_HEADER_DATA					sLocatesData
structPedsForConversation			sConvo

VEHICLE_INDEX						vehPlane
DROP_ZONE_STRUCT					sDropZones[I_NUM_DROP_ZONES]
INT									iDropZonesHit
INT									iLeafletDropTimer
BOOL								bLeafletsDropEnabled
BOOL								bLeafletsDropping
BOOL								bDisplayedLeafletObjective
BOOL								bDisplayedDrophelp
BOOL								bIsRangeOfAZone
FLOAT 								fLeafletCurrentEvo
INT									iLoudHailerBroadcasts					= 0

PTFX_ID								ptfxLeafletDrop

#IF IS_DEBUG_BUILD
	BOOL 						debug_bDrawDebug
	WIDGET_GROUP_ID				widget_debug
#ENDIF


FUNC BOOL UPDATE_DROP_ZONE(DROP_ZONE_STRUCT &sDropZone)
	
	IF NOT sDropZone.bHasBeenLittered
	
		// 2D distance check
		FLOAT fDistFromZone2D = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), sDropZone.vCoord, FALSE)
		
		// Update objective once near one of the leaflet drops
		IF NOT bDisplayedLeafletObjective
			IF fDistFromZone2D < F_OBJECTIVE_UPDATE_RADIUS
				CLEAR_PRINTS()
				PRINT_NOW("FLY_DROP", DEFAULT_GOD_TEXT_TIME, 1)
				REQUEST_SCRIPT_AUDIO_BANK("PLANES")
				bDisplayedLeafletObjective = TRUE
			ENDIF
		ENDIF
		
		// Drop leaflet help text
		IF NOT bDisplayedDrophelp
			IF fDistFromZone2D < F_HELP_TEXT_RADIUS
				CLEAR_HELP()
				PRINT_HELP("LEAFLET")
				bDisplayedDrophelp		= TRUE
				bLeafletsDropEnabled 	= TRUE
			ENDIF
		ENDIF
		
		// Leaflet drop detection
		IF fDistFromZone2D < sDropZone.fRadius //F_DROP_ZONE_RADIUS
			bIsRangeOfAZone = TRUE
		
			IF bLeafletsDropping
				IF sDropZone.iTimeHit = -1
					sDropZone.iTimeHit = GET_GAME_TIMER()
				ELIF GET_GAME_TIMER() - sDropZone.iTimeHit > 1000
					sDropZone.bHasBeenLittered = TRUE
					IF DOES_BLIP_EXIST(sDropZone.blip)
						REMOVE_BLIP(sDropZone.blip)
					ENDIF
					IF DOES_BLIP_EXIST(sDropZone.blipRadius)
						REMOVE_BLIP(sDropZone.blipRadius)
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)	
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT sDropZone.bHasBeenLittered
		
			IF NOT DOES_BLIP_EXIST(sDropZone.blip)
				sDropZone.blip = CREATE_BLIP_FOR_COORD(sDropZone.vCoord, FALSE)
				SET_BLIP_SCALE(sDropZone.blip, 0.75)
				SHOW_HEIGHT_ON_BLIP(sDropZone.blip, FALSE)
			ENDIF
			IF NOT DOES_BLIP_EXIST(sDropZone.blipRadius)
				sDropZone.blipRadius = CREATE_BLIP_FOR_RADIUS(sDropZone.vCoord, sDropZone.fRadius, FALSE)
			ENDIF
			
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

// cleanup the mission
PROC MISSION_CLEANUP()

	IF DOES_ENTITY_EXIST(vehPlane)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlane)
	ENDIF
	
	IF DOES_BLIP_EXIST(sDropZones[0].blip)
		REMOVE_BLIP(sDropZones[0].blip)
	ENDIF
	IF DOES_BLIP_EXIST(sDropZones[0].blipRadius)
		REMOVE_BLIP(sDropZones[0].blipRadius)
	ENDIF
	IF DOES_BLIP_EXIST(sDropZones[1].blip)
		REMOVE_BLIP(sDropZones[1].blip)
	ENDIF
	IF DOES_BLIP_EXIST(sDropZones[1].blipRadius)
		REMOVE_BLIP(sDropZones[1].blipRadius)
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	TERMINATE_THIS_THREAD() 
ENDPROC

// pass the mission
PROC MISSION_PASSED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_SUCCESS)
	MISSION_CLEANUP()
ENDPROC

// fail the mission
PROC MISSION_FAILED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	MISSION_CLEANUP()
ENDPROC

PROC MISSION_SETUP()

	#IF IS_DEBUG_BUILD
		widget_debug = START_WIDGET_GROUP("AMB: Property - Cinema Promotion")
			ADD_WIDGET_FLOAT_READ_ONLY("fLeafletCurrentEvo", fLeafletCurrentEvo)
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	#ENDIF
	
	SET_IGNORE_NO_GPS_FLAG(TRUE)
	
	REQUEST_ADDITIONAL_TEXT("PPROM", MISSION_TEXT_SLOT)
	
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
	
	ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
	
	// Initialise the route
	sDropZones[0].vCoord					= <<-1039.5560, 	-1071.2635, 	150.0>>
	sDropZones[0].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[1].vCoord					= <<-1014.9763, 	450.1280, 		150.0>>
	sDropZones[1].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[2].vCoord					= <<-598.1714, 		-310.6700,		150.0>>
	sDropZones[2].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[3].vCoord					= <<-392.5704, 		514.8138,		150.0>>
	sDropZones[3].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[4].vCoord					= <<127.7632, 		-135.7758, 		150.0>>
	sDropZones[4].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[5].vCoord					= <<1033.4686, 		-570.8278, 		150.0>>
	sDropZones[5].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[6].vCoord					= <<246.5894, 		-890.8110, 		150.0>>
	sDropZones[6].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[7].vCoord					= <<1231.3933, 		-1694.3479, 	150.0>>
	sDropZones[7].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[8].vCoord					= <<424.9531, 		-1904.3250, 	150.0>>
	sDropZones[8].fRadius					= F_DROP_ZONE_RADIUS
	sDropZones[9].vCoord					= <<-22.4342, 		-1414.2750, 	160.0>>
	sDropZones[9].fRadius					= F_DROP_ZONE_RADIUS
	
	INT i
	FOR i = 0 TO I_NUM_DROP_ZONES-1
		IF NOT IS_VECTOR_ZERO(sDropZones[i].vCoord)
			FLOAT fCoordZ  = 0
			GET_GROUND_Z_FOR_3D_COORD(sDropZones[i].vCoord, fCoordZ)
			sDropZones[i].vCoord.z = fCoordZ
		ENDIF
	ENDFOR
	
	SWITCH GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY()
		CASE PROPERTY_CINEMA_DOWNTOWN
			strAppendix		= "DT"
		BREAK
		CASE PROPERTY_CINEMA_VINEWOOD
			strAppendix		= "VW"
		BREAK
		CASE PROPERTY_CINEMA_MORNINGWOOD
			strAppendix		= "MW"
		BREAK
	ENDSWITCH
	
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
ENDPROC

PROC MISSION_FLOW()

	VECTOR 					vPlaneCoords			= <<-1085.2794, -2970.5725, 12.9457>>
	CONST_FLOAT				fPlaneHeading			121.2244
	TEXT_LABEL_15			strReuse	
	
	bIsRangeOfAZone = FALSE	// reset each frame
	
	// Fail checks
	//-------------------------------------------------------------------------------
	IF DOES_ENTITY_EXIST(vehPlane)
		IF NOT IS_VEHICLE_DRIVEABLE(vehPlane)
			MISSION_FAILED()
		ELSE
			IF iMissionStage = 2
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlane)
					
					IF NOT DOES_BLIP_EXIST(blipOjbective)
						PRINT_NOW("BK_PLANE", DEFAULT_GOD_TEXT_TIME, 1)
						blipOjbective = CREATE_BLIP_FOR_VEHICLE(vehPlane, FALSE)
					ENDIF
				
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehPlane) > 300
						MISSION_FAILED()
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipOjbective)
						CLEAR_PRINTS()
						REMOVE_BLIP(blipOjbective)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
	// Manage Leaflet dropping mechanic
	//---------------------------------------------------------------------------------
	
	// Must be in the plane and the plane must be in the air to start the drop
	IF DOES_ENTITY_EXIST(vehPlane)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlane, FALSE)
	AND IS_ENTITY_IN_AIR(vehPlane)
	AND bLeafletsDropEnabled
	AND REQUEST_SCRIPT_AUDIO_BANK("PLANES")

		IF NOT bLeafletsDropping
		
			// Start drop
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK) // Changed to make it consistent with the package drop missions - Steve R 10/JUN/14
		
				// Cool down before you can use it again
				IF iLeafletDropTimer = -1
				OR GET_GAME_TIMER() - iLeafletDropTimer > I_LEAFLET_DROP_COOLDOWN
	
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
						ptfxLeafletDrop		= START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_property_leaflet_drop", vehPlane, <<0.0, -2.0, -0.65>>, <<0,0,0>>, 1.0)
						fLeafletCurrentEvo = 0.0
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEAFLET")
						CLEAR_HELP()
					ENDIF
					
					PLAY_SOUND_FROM_ENTITY(-1, "PROPERTIES_PLANE_PROMO_MASTER", vehPlane)
					//PLAY_SOUND_FRONTEND(-1, "PROPERTIES_PLANE_PROMO_MASTER")
					
					iLeafletDropTimer = GET_GAME_TIMER()
					bLeafletsDropping = TRUE
					
				ENDIF
			ENDIF
		
		// stop drop after 3 seoncs
		ELSE
			IF GET_GAME_TIMER() - iLeafletDropTimer > 3000
				bLeafletsDropping = FALSE
			ENDIF
		ENDIF
	
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
			STOP_PARTICLE_FX_LOOPED(ptfxLeafletDrop)
			fLeafletCurrentEvo = 0.0
		ENDIF
	ENDIF
	
	// Manage leaflet ptfx evolution
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeafletDrop)
	
		FLOAT fIncrement
		IF bLeafletsDropping
			fIncrement = 1
		ELSE
			fIncrement = -1
		ENDIF
	
		fLeafletCurrentEvo += (fIncrement * TIMESTEP() * I_LEAFLET_DROP_RATE)
		fLeafletCurrentEvo = CLAMP(fLeafletCurrentEvo, 0.0, F_LEAFLET_DROP_RATE_MAX)
		SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxLeafletDrop, "spawn", fLeafletCurrentEvo)
	
	ENDIF

	// Main mission flow logic
	//---------------------------------------------------------------------------------
	SWITCH iMissionStage
		CASE 0	
			
			REQUEST_MODEL(STUNT)
			IF HAS_MODEL_LOADED(STUNT)
				vehPlane = g_PropertySystemData.managementEventVehicle[PROPERTY_MANAGEMENT_VEHICLE_PLANE_PROMOTION_VEHICLE]
				IF NOT DOES_ENTITY_EXIST(vehPlane)
					vehPlane = CREATE_VEHICLE(STUNT, vPlaneCoords, fPlaneHeading)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlane)
				ELSE
					SET_ENTITY_AS_MISSION_ENTITY(vehPlane, TRUE, TRUE)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(STUNT)
				
				CLEAR_PRINTS()
				PRINT_NOW("GET_PLANE", DEFAULT_GOD_TEXT_TIME, 1)
				blipOjbective =  CREATE_BLIP_FOR_VEHICLE(vehPlane, FALSE)
				
				REQUEST_PTFX_ASSET()
				
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
				iMissionStage++
			ENDIF			
		BREAK
		CASE 1
			IF DOES_ENTITY_EXIST(vehPlane)
			AND IS_VEHICLE_DRIVEABLE(vehPlane)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlane)
				
					CLEAR_PRINTS()
					PRINT_NOW("TAKEOFF", DEFAULT_GOD_TEXT_TIME, 1)
					IF DOES_BLIP_EXIST(blipOjbective)
						REMOVE_BLIP(blipOjbective)
					ENDIF
					
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1021.5223, -2986.1421, 12.9519>>, 341.6539)
				
					iMissionStage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			INT i
			REPEAT COUNT_OF(sDropZones) i
				IF UPDATE_DROP_ZONE(sDropZones[i])
					iDropZonesHit++
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_T )
				iDropZonesHit = I_NUM_DROP_ZONES
			ENDIF
			#ENDIF
			
			IF I_NUM_DROP_ZONES - iDropZonesHit <= 0
				iMissionStage++
			ENDIF
		BREAK
		CASE 3
		// Park plane back outside the hangar		
			IF IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(sLocatesData, vFinalHangarBlip, <<-1067.083008,-2922.600830,12.824183>>, <<-1091.853516,-2965.403320,23.945641>>, 31.562500, 
													TRUE, vehPlane, "LAND", "", "BK_PLANE", TRUE)
			AND NOT IS_ENTITY_IN_AIR(vehPlane)
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
				iMissionStage++
			ENDIF
		BREAK
		CASE 4
			IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlane, DEFAULT, DEFAULT, 0.0)
				iMissionStage++
			ENDIF
		BREAK
		CASE 5
			//IF IS_VEHICLE_STOPPED(vehPlane)
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlane, FALSE)
				MISSION_PASSED()
			//ENDIF
		BREAK
	ENDSWITCH
	
	// Dialogue
	//-------------------------------------------------------------------------------
	SWITCH iDialogueStage
		CASE 0
			IF I_NUM_DROP_ZONES - iDropZonesHit <= 0
				iDialogueStage++
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - iDialogueLastConvoTime > 5000
				AND bIsRangeOfAZone
					strReuse = "RAND_"
					strReuse += strAppendix
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
					AND GET_RANDOM_INT_IN_RANGE(0, 3) = 0 // 1 in 3 chance of doing the meltdown line
						strReuse += "M"
					ELSE
						IF iLoudHailerBroadcasts > 0
							strReuse += iLoudHailerBroadcasts + 1
						ENDIF
					ENDIF
					IF CREATE_CONVERSATION(sConvo, strDialogue, strReuse, CONV_PRIORITY_HIGH)
						iLoudHailerBroadcasts++
						IF iLoudHailerBroadcasts >= 6
							iLoudHailerBroadcasts = 0
						ENDIF
						strReuse = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - iDialogueLastConvoTime > 1000
				IF CREATE_CONVERSATION(sConvo, strDialogue, "DONE", CONV_PRIORITY_HIGH)
					strReuse = ""
					iDialogueStage++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	

ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		MISSION_CLEANUP()
	ENDIF
	
	MISSION_SETUP()
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueLastConvoTime = -1
		ELSE
			IF iDialogueLastConvoTime = -1
				iDialogueLastConvoTime = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		MISSION_FLOW()
		
		#IF IS_DEBUG_BUILD
			
			IF debug_bDrawDebug
				TEXT_LABEL_31 strDebug = "MissionStage: "
				strDebug += iMissionStage
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.05, 0.0>>, 255, 255, 255, 255)
				strDebug = "DialogueStage: "
				strDebug += iDialogueStage
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.075, 0.0>>, 255, 255, 255, 255)
			ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
				debug_bDrawDebug = !debug_bDrawDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(debug_bDrawDebug)
			ENDIF
		#ENDIF
    ENDWHILE
ENDSCRIPT
