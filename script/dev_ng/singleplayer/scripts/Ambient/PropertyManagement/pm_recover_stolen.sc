//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//      SCRIPT NAME	:	Property Management - Recover Stolen							//
//      AUTHOR		:	Michael Wadelin													//
//      DESCRIPTION	:	Recover Stolen missions for property management.				//
// 																						//
//////////////////////////////////////////////////////////////////////////////////////////
 
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

 
// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "properties_public.sch"
USING "shared_hud_displays.sch"
USING "script_ped.sch"
USING "asset_management_public.sch"
USING "shared_hud_displays.sch"
USING "chase_hint_cam.sch"
 
STRUCT PROPERTY_RECOVER_STOLEN_PARAMS
	VECTOR							vThiefDriveOffTriggerV1
	VECTOR							vThiefDriveOffTriggerV2
	FLOAT							fThiefDriveOffTriggerW
	MODEL_NAMES 					modVeh										= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES 					modPed										= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES						modPed2										= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES						modStolenItem								= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES						modStolenItem2								= DUMMY_MODEL_FOR_SCRIPT
	TEXT_LABEL_15					strDiaRoot
	DRIVINGMODE						eDrivingMode								= DRIVINGMODE_AVOIDCARS_RECKLESS
	BOOL							bDropWhenKnockedOff
	BOOL							bDropInFear									
	INT								iUploadTimer								= -1
	BOOL							bShouldDriveBy
	BOOL							bFailOnThiefDead
	TEXT_LABEL_15					strStartHelp
	BOOL							bGiveHelment
	BOOL							bHardToKillOnKnockOff
	INT								iPlacementFlags								= 0
	INT								iPlacementFlags2							= 0
ENDSTRUCT

INT 								iMissionSubstage
PROPERTY_RECOVER_STOLEN_PARAMS 		sStartParams
INT									iChaseTimer									= -1
BOOL 								bGivenInitialFleeOnVehicleTask
BOOL								bEarlyDriveOff
BOOL								bStartAttackingPlayer
BOOL								bLoseCopsShown
BOOL								bPlayedTyreSqueal

structPedsForConversation			sConvo
INT									iDialogueFlag								= -1
INT									iDialogueLastConvoTime						= -1
TEXT_LABEL_7						strDialogue									= "PMRAUD"
BOOL								bDoGodText

SEQUENCE_INDEX						seq
VEHICLE_INDEX 						vehThief
PED_INDEX 							pedThief
PED_INDEX 							pedThief2
PICKUP_INDEX						pkupStolenItem
PICKUP_INDEX						pkupStolenItem2
BLIP_INDEX							blipObjective
BLIP_INDEX							blipObjective2
REL_GROUP_HASH						relThief
INT									iProofTimerTheif							= -1
INT									iProofTimerTheif2							= -1
CHASE_HINT_CAM_STRUCT				sHintCam

CONST_FLOAT							FAIL_DISTANCE								500.0

#IF IS_DEBUG_BUILD
	BOOL 							debug_bDrawDebug
#ENDIF


// cleanup the mission
PROC MISSION_CLEANUP()

	IF DOES_ENTITY_EXIST(pedThief)
		IF NOT IS_PED_INJURED(pedThief)
			TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 1000, -1, FALSE, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedThief)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedThief2)
		IF NOT IS_PED_INJURED(pedThief2)
			TASK_SMART_FLEE_PED(pedThief2, PLAYER_PED_ID(), 1000, -1, FALSE, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedThief2)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehThief)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehThief)
	ENDIF
	
	IF DOES_PICKUP_EXIST(pkupStolenItem)
		REMOVE_PICKUP(pkupStolenItem)
	ENDIF
	
	IF DOES_PICKUP_EXIST(pkupStolenItem2)
		REMOVE_PICKUP(pkupStolenItem2)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipObjective)
		REMOVE_BLIP(blipObjective)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipObjective2)
		REMOVE_BLIP(blipObjective2)
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sStartParams.modVeh, FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	REMOVE_RELATIONSHIP_GROUP(relThief)

	TERMINATE_THIS_THREAD() 
ENDPROC


// pass the mission
PROC MISSION_PASSED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_SUCCESS)
	MISSION_CLEANUP()
ENDPROC


// fail the mission
PROC MISSION_FAILED()
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	MISSION_CLEANUP()
ENDPROC


/// PURPOSE:
///    Initialises the mission struct with all the data for the particular variation of the mission
/// PARAMS:
///    sParams - struct that holds all the information
PROC INITIALISE_PROPERTY_RECOVER_STOLEN_MISISON_PARAMS(PROPERTY_RECOVER_STOLEN_PARAMS &sParams)

	VECTOR vTriggeredCoord 	= GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	vTriggeredCoord.z 		= 0.0
	
	INT	iPlacementFlagsNormal 	= 0
	INT	iPlacementFlagsAlt		= 0
	
	SET_BIT(iPlacementFlagsNormal, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlagsNormal, enum_to_int(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	
	SET_BIT(iPlacementFlagsAlt, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlagsAlt, enum_to_int(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	SET_BIT(iPlacementFlagsAlt, enum_to_int(PLACEMENT_FLAG_UPRIGHT))
	
	SWITCH GET_CURRENT_RECOVER_STOLEN_VARIATION()
		CASE RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
			sParams.modPed 					= A_M_M_EASTSA_02
			sParams.modStolenItem			= PROP_CS_DUFFEL_01B
			sParams.eDrivingMode			= DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_DriveIntoOncomingTraffic
			sParams.strDiaRoot				= "MON"
			sParams.bShouldDriveBy			= TRUE
			sParams.iPlacementFlags			= iPlacementFlagsNormal
		BREAK
		CASE RECOVER_STOLEN_MISSION_VARIATION_PAP
			sParams.modPed 					= A_M_M_PAPARAZZI_01
			sParams.modPed2					= A_M_M_PAPARAZZI_01
			sParams.modStolenItem			= PROP_PAP_CAMERA_01
			sParams.modStolenItem2			= PROP_NPC_PHONE
			sParams.eDrivingMode			= DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundPeds | DF_SteerAroundObjects
			sParams.bDropWhenKnockedOff		= TRUE
			sParams.bDropInFear				= TRUE
			sParams.strDiaRoot				= "PAP"
			sParams.iUploadTimer			= 60000
			sParams.bFailOnThiefDead		= TRUE
			sParams.strStartHelp			= "PAP_HLP"
			sParams.bGiveHelment			= FALSE
			sParams.bHardToKillOnKnockOff	= TRUE
			sParams.iPlacementFlags			= iPlacementFlagsAlt
			sParams.iPlacementFlags2		= iPlacementFlagsNormal
		BREAK
	ENDSWITCH
	
	// Spawn coords/heading, early trigger, etc
	SWITCH GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY()
	
	// CINEMA'S
		CASE PROPERTY_CINEMA_VINEWOOD
			sParams.vThiefDriveOffTriggerV1		= <<178.011719,202.350693,102.903671>> //<<263.034119,174.042191,101.784645>>
			sParams.vThiefDriveOffTriggerV2		= <<514.583862,78.788109,111.186409>> //<<369.218689,135.791321,111.851036>>
			sParams.fThiefDriveOffTriggerW		= 131.062500 //46.500000
		BREAK
		CASE PROPERTY_CINEMA_DOWNTOWN
			sParams.vThiefDriveOffTriggerV1		= <<418.579620,-636.289917,25.437576>> //<<408.123810,-667.587158,28.039864>>
			sParams.vThiefDriveOffTriggerV2		= <<400.246826,-826.161133,44.104679>> //<<408.559784,-761.593079,35.026428>>
			sParams.fThiefDriveOffTriggerW		= 95.375000 //29.125000
		BREAK
		CASE PROPERTY_CINEMA_MORNINGWOOD
			sParams.vThiefDriveOffTriggerV1		= <<-1368.457764,-238.454315,40.362411>> //<<-1383.417725,-234.078186,40.464439>>
			sParams.vThiefDriveOffTriggerV2		= <<-1475.669067,-127.621017,65.654663>> //<<-1455.533813,-148.215164,58.482277>>
			sParams.fThiefDriveOffTriggerW		= 158.250000 //67.687500
		BREAK
	
	// BARS
		CASE PROPERTY_BAR_TEQUILALA
			IF IS_COORD_LEFT_OF_PROPERTY(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), vTriggeredCoord)
				sParams.vThiefDriveOffTriggerV1		= <<-602.378540,258.274384,79.029305>> //<<-491.544159,243.241547,89.042717>>
				sParams.vThiefDriveOffTriggerV2		= <<-469.064026,244.873505,97.885765>> //<<-590.514221,250.685928,81.181282>>
				sParams.fThiefDriveOffTriggerW		= 70.125000 //59.375000
			ELSE
				sParams.vThiefDriveOffTriggerV1		= <<-621.588928,262.091492,78.674049>>  //<<-523.673340,252.373138,89.070892>>
				sParams.vThiefDriveOffTriggerV2		= <<-494.766571,248.219589,97.885635>> //<<-599.128052,261.026642,80.940002>>
				sParams.fThiefDriveOffTriggerW		= 70.125000 //38.500000
			ENDIF
		BREAK
		CASE PROPERTY_BAR_PITCHERS
			IF IS_COORD_LEFT_OF_PROPERTY(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), vTriggeredCoord)
				sParams.vThiefDriveOffTriggerV1		= <<137.207306,392.748199,110.944267>> //<<165.874680,375.516174,103.822090>>
				sParams.vThiefDriveOffTriggerV2		= <<297.210419,336.026917,120.352478>> //<<273.427826,340.939117,111.885216>>
				sParams.fThiefDriveOffTriggerW		= 95.375000 //56.375000
			ELSE
				sParams.vThiefDriveOffTriggerV1		= <<191.322540,364.994904,104.515266>> //<<210.276932,358.108643,104.169983>>
				sParams.vThiefDriveOffTriggerV2		= <<320.368805,325.287720,120.268921>> //<<294.528687,328.947205,112.148323>>
				sParams.fThiefDriveOffTriggerW		= 70.125000 //43.687500
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HEN_HOUSE
			IF IS_COORD_LEFT_OF_PROPERTY(GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY(), vTriggeredCoord)
				sParams.vThiefDriveOffTriggerV1		= <<-221.658585,6310.131348,28.405548>> //<<-297.491394,6232.303223,22.514248>>
				sParams.vThiefDriveOffTriggerV2		= <<-319.828674,6211.664551,46.195206>> //<<-234.892136,6293.300293,40.350250>>
				sParams.fThiefDriveOffTriggerW		= 95.375000 //73.062500	
			ELSE
				sParams.vThiefDriveOffTriggerV1		= <<-260.280914,6273.981445,28.428225>> //<<-346.792999,6187.302246,22.471390>>
				sParams.vThiefDriveOffTriggerV2		= <<-371.565674,6164.585938,46.189396>> //<<-263.595184,6270.673828,40.490345>>
				sParams.fThiefDriveOffTriggerW		= 95.375000 //65.062500
			ENDIF
		BREAK
		CASE PROPERTY_BAR_HOOKIES
			sParams.vThiefDriveOffTriggerV1		= <<-2313.765137,4159.021973,29.590107>>
			sParams.vThiefDriveOffTriggerV2		= <<-2140.886719,4437.056641,72.618996>>
			sParams.fThiefDriveOffTriggerW		= 113.500000
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Appends the players first name initial to the end of the text label
/// PARAMS:
///    eChar - the player character name to append for
///    rootLabel - the root label to append to
/// RETURNS:
///    Returns the new root label with the appended initial
FUNC TEXT_LABEL_15 APPEND_PLAYER_ID_TO_CONV_ROOT(STRING rootLabel)

	TEXT_LABEL_15 result = rootLabel

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			result += "_M"
		BREAK
		CASE CHAR_FRANKLIN
			result += "_F"
		BREAK
		CASE CHAR_TREVOR
			result += "_T"
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_MISSION, "APPEND_PLAYER_ID_TO_CONV_ROOT() conveted passed rootLabel from \"",  rootLabel, "\" to \"", result, "\".")
	RETURN result

ENDFUNC


/// PURPOSE:
///    
/// PARAMS:
///    ped - 
/// RETURNS:
///    
FUNC BOOL HAS_PED_BEEN_INTIMIDATED(PED_INDEX ped)
	
	IF HAS_PED_RECEIVED_EVENT(ped, EVENT_SHOT_FIRED) 
	OR HAS_PED_RECEIVED_EVENT(ped, EVENT_SHOT_FIRED_BULLET_IMPACT) 
	OR HAS_PED_RECEIVED_EVENT(ped, EVENT_SHOT_FIRED_WHIZZED_BY) 
	OR HAS_PED_RECEIVED_EVENT(ped, EVENT_MELEE_ACTION)
	OR (CAN_PED_SEE_HATED_PED(ped, PLAYER_PED_ID()) AND HAS_PED_RECEIVED_EVENT(ped, EVENT_GUN_AIMED_AT))
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, PLAYER_PED_ID(), TRUE)
		RETURN TRUE
	ELSE
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped)
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Sets up the general mission stuff
PROC MISSION_SETUP()

	#IF IS_DEBUG_BUILD
		START_WIDGET_GROUP("AMB: Property - Recover Stolen")
			ADD_WIDGET_BOOL("GivenInitialTask", bGivenInitialFleeOnVehicleTask)
			ADD_WIDGET_BOOL("EarlyDriveOffTriggered", bEarlyDriveOff)
		STOP_WIDGET_GROUP()
	#ENDIF

	INITIALISE_PROPERTY_RECOVER_STOLEN_MISISON_PARAMS(sStartParams)
	
	ADD_RELATIONSHIP_GROUP("relThief", relThief)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relThief)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relThief, RELGROUPHASH_PLAYER)
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
		BREAK
		CASE CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(sConvo, 1, PLAYER_PED_ID(), "FRANKLIN")
		BREAK
		CASE CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(sConvo, 2, PLAYER_PED_ID(), "TREVOR")
		BREAK
	ENDSWITCH
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)

ENDPROC

/// PURPOSE:
///    Decides which variation of the mission to run
PROC MISSION_FLOW()

	TEXT_LABEL_31 strDiaRoot
	
	SWITCH iMissionSubstage
		CASE 0
			REQUEST_MODEL(sStartParams.modStolenItem)
			IF sStartParams.modStolenItem2 != DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(sStartParams.modStolenItem2)
			ENDIF
			REQUEST_ADDITIONAL_TEXT("PRECOV", MISSION_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				
			// Grab the vehicle from the trigger scene
				IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[0])
				AND IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[0])
					vehThief = g_PropertySystemData.managementEventVehicle[0]
					g_PropertySystemData.managementEventVehicle[0] = NULL
					SET_ENTITY_AS_MISSION_ENTITY(vehThief, TRUE, TRUE)	
					sStartParams.modVeh = GET_ENTITY_MODEL(vehThief)
					SET_MODEL_AS_NO_LONGER_NEEDED(sStartParams.modVeh)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(sStartParams.modVeh, TRUE)
				ENDIF
				
			// Grab the thief from the trigger scene
				IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[0])
				//AND NOT IS_PED_INJURED(g_PropertySystemData.managementEventPed[0])
					pedThief = g_PropertySystemData.managementEventPed[0]
					g_PropertySystemData.managementEventPed[0] = NULL
					SET_ENTITY_AS_MISSION_ENTITY(pedThief, TRUE, TRUE)
				
					sStartParams.modPed = GET_ENTITY_MODEL(pedThief)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedThief, relThief)
					SET_PED_MONEY(pedThief, 0)
					SET_PED_ACCURACY(pedthief,5)
					SET_PED_KEEP_TASK(pedThief, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedThief, CA_REQUIRES_LOS_TO_SHOOT, TRUE)
					IF IS_THIS_MODEL_A_BIKE(sStartParams.modVeh)
						IF sStartParams.bGiveHelment
							SET_PED_HELMET(pedThief, TRUE)
							GIVE_PED_HELMET(pedThief, TRUE)
						ELSE
							SET_PED_HELMET(pedThief, FALSE)
						ENDIF
					ENDIF
					IF sStartParams.bShouldDriveBy
						GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_PISTOL, 120, FALSE, FALSE)
						GIVE_WEAPON_TO_PED(pedThief, WEAPONTYPE_MICROSMG, 120, TRUE, TRUE)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(sStartParams.modPed)
					SET_ENTITY_PROOFS(pedThief, FALSE, FALSE, FALSE, sStartParams.bHardToKillOnKnockOff, FALSE)
					ADD_PED_FOR_DIALOGUE(sConvo, 3, pedThief, "ABIGAIL")
				ENDIF
				
			// Grab the additional thief from the trigger scene
				IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[1])
				//AND NOT IS_PED_INJURED(g_PropertySystemData.managementEventPed[1])
					pedThief2 = g_PropertySystemData.managementEventPed[1]
					g_PropertySystemData.managementEventPed[1] = NULL
					SET_ENTITY_AS_MISSION_ENTITY(pedThief2, TRUE, TRUE)
					
					sStartParams.modPed2 = GET_ENTITY_MODEL(pedThief2)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedThief2, relThief)
					SET_PED_MONEY(pedThief2, 0)
					SET_PED_ACCURACY(pedthief2,5)
					SET_PED_KEEP_TASK(pedThief2, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedThief2, CA_REQUIRES_LOS_TO_SHOOT, TRUE)
					IF IS_THIS_MODEL_A_BIKE(sStartParams.modVeh)
						IF sStartParams.bGiveHelment
							SET_PED_HELMET(pedThief2, TRUE)
							GIVE_PED_HELMET(pedThief2, TRUE)
						ELSE
							SET_PED_HELMET(pedThief2, FALSE)
						ENDIF
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(sStartParams.modPed2)
					SET_ENTITY_PROOFS(pedThief2, FALSE, FALSE, FALSE, sStartParams.bHardToKillOnKnockOff, FALSE)
				ENDIF
				
				IF (NOT DOES_ENTITY_EXIST(pedThief) OR NOT IS_PED_INJURED(pedThief))
				AND (NOT DOES_ENTITY_EXIST(pedThief2) OR NOT IS_PED_INJURED(pedThief2))
				
					blipObjective = CREATE_BLIP_FOR_VEHICLE(vehThief, TRUE)

					strDiaRoot = "STOP_THIEF_"
					strDiaRoot += sStartParams.strDiaRoot
					PRINT_NOW(strDiaRoot, DEFAULT_GOD_TEXT_TIME, 1)
					
					PRINT_HELP(sStartParams.strStartHelp, DEFAULT_HELP_TEXT_TIME)
					
					iDialogueFlag 					= 0
					bGivenInitialFleeOnVehicleTask 	= FALSE
					bEarlyDriveOff					= FALSE
					IF sStartParams.iUploadTimer != -1
						iChaseTimer					= GET_GAME_TIMER() + sStartParams.iUploadTimer
					ENDIF
				
				ENDIF
				
				iMissionSubstage++
				
			ENDIF
			
		BREAK
		CASE 1	

		// HAS PED BEEN CAUSED TO DROP STOLEN ITEM
		//-----------------------------------------------------------------------------------------------------
			IF (DOES_ENTITY_EXIST(pedThief) AND IS_PED_INJURED(pedThief))
			OR (DOES_ENTITY_EXIST(pedThief2) AND IS_PED_INJURED(pedThief2))
			OR (sStartParams.bDropWhenKnockedOff 	AND IS_THIS_MODEL_A_BIKE(sStartParams.modVeh) AND NOT IS_PED_IN_VEHICLE(pedThief, vehThief))
			OR (sStartParams.bDropWhenKnockedOff 	AND IS_THIS_MODEL_A_BIKE(sStartParams.modVeh) AND NOT IS_PED_IN_VEHICLE(pedThief2, vehThief))
			OR (sStartParams.bDropInFear			AND NOT IS_PED_IN_VEHICLE(pedThief, vehThief) AND HAS_PED_BEEN_INTIMIDATED(pedThief))
			#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T)
			#ENDIF
				
				// Knock peds off bike
				IF sStartParams.bDropWhenKnockedOff AND IS_THIS_MODEL_A_BIKE(sStartParams.modVeh)
					IF NOT IS_PED_INJURED(pedThief) AND IS_PED_IN_VEHICLE(pedThief, vehThief)
						KNOCK_PED_OFF_VEHICLE(pedThief)
					ENDIF
					IF NOT IS_PED_INJURED(pedThief2) AND IS_PED_IN_VEHICLE(pedThief2, vehThief)
						KNOCK_PED_OFF_VEHICLE(pedThief2)
					ENDIF
				ENDIF
				
				// Clear existing god text, help and stop dialogue
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				iDialogueFlag = -1

				IF HAS_MODEL_LOADED(sStartParams.modStolenItem)
				AND (sStartParams.modStolenItem2 = DUMMY_MODEL_FOR_SCRIPT OR HAS_MODEL_LOADED(sStartParams.modStolenItem2))
				
					VECTOR vPickupCoord

					// Create pickup and blip item
					IF DOES_ENTITY_EXIST(pedThief)
						
						vPickupCoord = GET_DEAD_PED_PICKUP_COORDS(pedThief)
						PRINTLN("Created pickup at coord ", vPickupCoord)
						pkupStolenItem = CREATE_PICKUP(PICKUP_MONEY_MED_BAG, vPickupCoord, sStartParams.iPlacementFlags, -1, DEFAULT, sStartParams.modStolenItem)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(sStartParams.modStolenItem)
						
						IF DOES_BLIP_EXIST(blipObjective)
							REMOVE_BLIP(blipObjective)
						ENDIF
						blipObjective = CREATE_BLIP_FOR_PICKUP(pkupStolenItem)
					ENDIF

					// create a second pickup and blip it if there is a second ped with a second item set
					IF DOES_ENTITY_EXIST(pedThief2)
					AND sStartParams.modStolenItem2 != DUMMY_MODEL_FOR_SCRIPT
					
						vPickupCoord = GET_DEAD_PED_PICKUP_COORDS(pedThief2)
						PRINTLN("Created pickup_extra at coord ", vPickupCoord)
						pkupStolenItem2 = CREATE_PICKUP(PICKUP_MONEY_MED_BAG, vPickupCoord, sStartParams.iPlacementFlags2, -1, DEFAULT, sStartParams.modStolenItem2)
						SET_MODEL_AS_NO_LONGER_NEEDED(sStartParams.modStolenItem2)
						
						IF DOES_BLIP_EXIST(blipObjective2)
							REMOVE_BLIP(blipObjective2)
						ENDIF
						blipObjective2 = CREATE_BLIP_FOR_PICKUP(pkupStolenItem2)
					ENDIF

					KILL_CHASE_HINT_CAM(sHintCam)
					bDoGodText		= TRUE
					iMissionSubstage++
				
				ENDIF
				
			ELSE
			
				IF IS_PED_IN_ANY_VEHICLE(pedThief)
					IF DOES_BLIP_EXIST(blipObjective2)
						REMOVE_BLIP(blipObjective2)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipObjective)
						blipObjective = CREATE_BLIP_FOR_VEHICLE(GET_VEHICLE_PED_IS_IN(pedThief), TRUE)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipObjective)
						REMOVE_BLIP(blipObjective)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipObjective2)
						blipObjective2 = CREATE_BLIP_FOR_PED(pedThief, TRUE)
					ENDIF
				ENDIF
			
				CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS(sHintCam, pedThief)
			
		// FAIL CASES
		//-----------------------------------------------------------------------------

				// Upload timer fail and update
				IF iChaseTimer != -1
					IF GET_GAME_TIMER() > iChaseTimer
						MISSION_FAILED()
					ELSE
						INT iTimeLeft
						iTimeLeft = CLAMP_INT(ABSI(iChaseTimer - GET_GAME_TIMER()), 0, sStartParams.iUploadTimer)
						IF iTimeLeft <= 10000
							//DRAW_GENERIC_TIMER(iTimeLeft, "UPLOAD", 0, TIMER_STYLE_USEMILLISECONDS, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDFLASHING_FLASHRED, 10000)
							DRAW_TIMER_HUD(sStartParams.iUploadTimer - iTimeLeft, sStartParams.iUploadTimer, "UPLOAD", HUD_COLOUR_BLUE, 10000)
						ELSE
							//DRAW_GENERIC_TIMER(iTimeLeft, "UPLOAD", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDFLASHING_NONE, 0)
							DRAW_TIMER_HUD(sStartParams.iUploadTimer - iTimeLeft, sStartParams.iUploadTimer, "UPLOAD", HUD_COLOUR_BLUE, 0)
						ENDIF
						CPRINTLN(DEBUG_MISSION, "Drawing upload timer - ", GET_GAME_TIMER())
					ENDIF
				ENDIF
				
				// Thief got away fail and blip update
				IF DOES_ENTITY_EXIST(pedThief)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedThief) > FAIL_DISTANCE
						MISSION_FAILED()	
					ELSE
						IF DOES_BLIP_EXIST(blipObjective)
							UPDATE_CHASE_BLIP(blipObjective, pedThief, FAIL_DISTANCE, 0.8, FALSE)
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(pedThief2)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedThief2) > FAIL_DISTANCE
						MISSION_FAILED()	
					ELSE
						IF DOES_BLIP_EXIST(blipObjective2)
							UPDATE_CHASE_BLIP(blipObjective2, pedThief2, FAIL_DISTANCE, 0.8, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		CASE 2
			// unblip the main stolen item
			IF NOT DOES_PICKUP_EXIST(pkupStolenItem) OR HAS_PICKUP_BEEN_COLLECTED(pkupStolenItem)
				IF DOES_PICKUP_EXIST(pkupStolenItem)
					REMOVE_PICKUP(pkupStolenItem)
				ENDIF
				IF DOES_BLIP_EXIST(blipObjective)
					REMOVE_BLIP(blipObjective)
				ENDIF
			ENDIF
			// Unblip the 2nd optional item
			IF NOT DOES_PICKUP_EXIST(pkupStolenItem2) OR HAS_PICKUP_BEEN_COLLECTED(pkupStolenItem2)
				IF DOES_PICKUP_EXIST(pkupStolenItem2)
					REMOVE_PICKUP(pkupStolenItem2)
				ENDIF
				IF DOES_BLIP_EXIST(blipObjective2)
					REMOVE_BLIP(blipObjective2)
				ENDIF
			ENDIF
			
			// Passes the mission once the player retrieves all the stolen goods
			IF NOT DOES_BLIP_EXIST(blipObjective)
			AND NOT DOES_BLIP_EXIST(blipObjective2)
			
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT bLoseCopsShown
						PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 1)
						bLoseCopsShown = TRUE
					ENDIF
				ELSE
					CLEAR_PRINTS()
					MISSION_PASSED()
				ENDIF
				
			ELSE
				// fail for leaving the stolen good behind
				IF (DOES_PICKUP_EXIST(pkupStolenItem) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_PICKUP_COORDS(pkupStolenItem)) > FAIL_DISTANCE)
				OR (DOES_PICKUP_EXIST(pkupStolenItem2) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_PICKUP_COORDS(pkupStolenItem2)) > FAIL_DISTANCE)
					MISSION_FAILED()	
				ELSE
					IF bDoGodText
					AND IS_SAFE_TO_DISPLAY_GODTEXT()
						CLEAR_PRINTS()
						strDiaRoot = "PKUP_"
						strDiaRoot += sStartParams.strDiaRoot
						PRINT_NOW(strDiaRoot, DEFAULT_GOD_TEXT_TIME, 1)
						bDoGodText = FALSE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
// PED AI : THIEF1(MAIN)
//--------------------------------------------------------------------------------------------------
	IF DOES_ENTITY_EXIST(pedThief) AND NOT IS_PED_INJURED(pedThief)
		
		// vehicle is still drivable and Thief has not used it yet
		IF DOES_ENTITY_EXIST(vehThief) AND IS_VEHICLE_DRIVEABLE(vehThief)
		AND NOT bGivenInitialFleeOnVehicleTask
			
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_PERFORM_SEQUENCE)

				OPEN_SEQUENCE_TASK(seq)
					TASK_VEHICLE_TEMP_ACTION(NULL, vehThief, TEMPACT_REV_ENGINE, 10000)
					TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehThief, PLAYER_PED_ID(), MISSION_FLEE, 30.0, sStartParams.eDrivingMode, 1000, 0, TRUE)	
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedThief, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				bGivenInitialFleeOnVehicleTask = TRUE
			ENDIF
			
		ELSE
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_PERFORM_SEQUENCE)
			AND GET_SEQUENCE_PROGRESS(pedThief) < 1
			AND NOT bEarlyDriveOff
			AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sStartParams.vThiefDriveOffTriggerV1, sStartParams.vThiefDriveOffTriggerV2, sStartParams.fThiefDriveOffTriggerW)
			OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED)
			OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED_BULLET_IMPACT)
			OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED_WHIZZED_BY)
			OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_EXPLOSION_HEARD)
			OR IS_BITMASK_AS_ENUM_SET(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_FORCED_MISSION_START))
	
				TASK_VEHICLE_MISSION_PED_TARGET(pedThief, vehThief, PLAYER_PED_ID(), MISSION_FLEE, 30.0, sStartParams.eDrivingMode, 1000, 0, TRUE)	
				
				bEarlyDriveOff = TRUE
				
			// just flee normally
			ELIF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_PERFORM_SEQUENCE)
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_VEHICLE_MISSION))
			OR NOT IS_PED_IN_ANY_VEHICLE(pedThief)
			
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_SMART_FLEE_PED)
					TASK_SMART_FLEE_PED(pedThief, PLAYER_PED_ID(), 1000, -1, FALSE, TRUE)
				ENDIF
			
			ELIF IS_PED_IN_VEHICLE(pedThief, vehThief, TRUE)
			AND (IS_VEHICLE_STUCK_TIMER_UP(vehThief, VEH_STUCK_ON_ROOF, 3000)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehThief, VEH_STUCK_ON_SIDE, 3000)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehThief, VEH_STUCK_HUNG_UP, 8000)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehThief, VEH_STUCK_JAMMED, 8000))
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_LEAVE_VEHICLE)
					TASK_LEAVE_VEHICLE(pedThief, vehThief)
				ENDIF
			ENDIF
		ENDIF
		
		IF sStartParams.bShouldDriveBy
		
			IF NOT bStartAttackingPlayer
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedThief, PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehThief, PLAYER_PED_ID())
				OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED)
				OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED_BULLET_IMPACT)
				OR HAS_PED_RECEIVED_EVENT(pedThief, EVENT_SHOT_FIRED_WHIZZED_BY)
					bStartAttackingPlayer = TRUE
				ENDIF
			ENDIF
			
			IF bStartAttackingPlayer
				IF (GET_SCRIPT_TASK_STATUS(pedThief, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK AND GET_SEQUENCE_PROGRESS(pedThief) = 1)
				OR GET_SCRIPT_TASK_STATUS(pedThief, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
					
					IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(pedThief)
						SET_CURRENT_PED_VEHICLE_WEAPON(pedThief, WEAPONTYPE_MICROSMG)
						TASK_DRIVE_BY(pedThief, PLAYER_PED_ID(), null, <<0,0,0>>, 1000, 100, TRUE)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehThief)
		AND IS_VEHICLE_DRIVEABLE(vehThief)
			IF NOT bPlayedTyreSqueal
				IF (IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_PERFORM_SEQUENCE) AND GET_SEQUENCE_PROGRESS(pedThief) = 1)
				OR IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief, SCRIPT_TASK_VEHICLE_MISSION)
					IF REQUEST_SCRIPT_AUDIO_BANK("TAKINGS")
						PLAY_SOUND_FROM_ENTITY(-1, "TAKINGS_TIRES_PEELAWAY_master", vehThief)
						bPlayedTyreSqueal = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

// PED AI : THIEF2(OPTIONAL EXTRA)
//--------------------------------------------------------------------------------------------------
	IF DOES_ENTITY_EXIST(pedThief2) AND NOT IS_PED_INJURED(pedThief2)
		IF NOT IS_PED_IN_ANY_VEHICLE(pedThief2)
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedThief2, SCRIPT_TASK_SMART_FLEE_PED)
			
			TASK_SMART_FLEE_PED(pedThief2, PLAYER_PED_ID(), 1000, -1, FALSE, TRUE)
			
		ENDIF
	ENDIF

	
	// Run dialogue
	IF DOES_ENTITY_EXIST(pedThief)
	AND NOT IS_PED_INJURED(pedThief)
		IF IS_ENTITY_IN_AIR(pedThief) AND NOT IS_PED_IN_ANY_VEHICLE(pedThief)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedThief) < 15.0
				SWITCH iDialogueFlag
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
							strDiaRoot = sStartParams.strDiaRoot
							strDiaRoot += "_1"
							strDiaRoot = APPEND_PLAYER_ID_TO_CONV_ROOT(strDiaRoot)
							IF CREATE_CONVERSATION(sConvo, strDialogue, strDiaRoot, CONV_PRIORITY_HIGH)	
								strDiaRoot = ""
								iDialogueFlag++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedThief)
								IF GET_CURRENT_RECOVER_STOLEN_VARIATION() = RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
									strDiaRoot = "GENERIC_INSULT_HIGH"
								ELIF GET_CURRENT_RECOVER_STOLEN_VARIATION() = RECOVER_STOLEN_MISSION_VARIATION_PAP
									strDiaRoot = "GENERIC_INSULT_MED"
								ENDIF
								PLAY_PED_AMBIENT_SPEECH(pedThief, strDiaRoot, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								iDialogueLastConvoTime = -1
								strDiaRoot = ""
								iDialogueFlag++
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - iDialogueLastConvoTime > 3500
							strDiaRoot = sStartParams.strDiaRoot
							strDiaRoot += "_3"
							strDiaRoot = APPEND_PLAYER_ID_TO_CONV_ROOT(strDiaRoot)
							IF CREATE_CONVERSATION(sConvo, strDialogue, strDiaRoot, CONV_PRIORITY_HIGH)	
								strDiaRoot = ""
								iDialogueFlag++
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - iDialogueLastConvoTime > 1500
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedThief)
								IF GET_CURRENT_RECOVER_STOLEN_VARIATION() = RECOVER_STOLEN_MISSION_VARIATION_TAKINGS
									strDiaRoot = "GENERIC_INSULT_HIGH"
								ELIF GET_CURRENT_RECOVER_STOLEN_VARIATION() = RECOVER_STOLEN_MISSION_VARIATION_PAP
									strDiaRoot = "GENERIC_INSULT_MED"
								ENDIF
								PLAY_PED_AMBIENT_SPEECH(pedThief, strDiaRoot, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								iDialogueLastConvoTime = -1
								strDiaRoot = ""
								iDialogueFlag--
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	 
	
	
	IF DOES_ENTITY_EXIST(pedThief) 
		IF IS_PED_INJURED(pedThief)
			// Fail for dead thief
			IF sStartParams.bFailOnThiefDead	
				OVERRIDE_PROPERTY_MISSION_FAIL_REASON(PROPERTY_MISSION_FAIL_REASON_OVERRIDE_PAPS_KILLED)
				MISSION_FAILED()
			ENDIF
		ELSE
			IF sStartParams.bHardToKillOnKnockOff
				IF iProofTimerTheif = -1
					IF NOT IS_ENTITY_IN_AIR(pedThief)	
						iProofTimerTheif = GET_GAME_TIMER()
					ENDIF
				ELIF GET_GAME_TIMER() - iProofTimerTheif > 2000
					SET_ENTITY_PROOFS(pedThief, FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedThief2) 
		IF IS_PED_INJURED(pedThief2)
			// Fail for dead thief
			IF sStartParams.bFailOnThiefDead	
				OVERRIDE_PROPERTY_MISSION_FAIL_REASON(PROPERTY_MISSION_FAIL_REASON_OVERRIDE_PAPS_KILLED)
				MISSION_FAILED()
			ENDIF
		ELSE
			IF sStartParams.bHardToKillOnKnockOff
				IF iProofTimerTheif2 = -1
					IF NOT IS_ENTITY_IN_AIR(pedThief2)	
						iProofTimerTheif2 = GET_GAME_TIMER()
					ENDIF
				ELIF GET_GAME_TIMER() - iProofTimerTheif2 > 2000
					SET_ENTITY_PROOFS(pedThief2, FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		MISSION_CLEANUP()
	ENDIF
	
	MISSION_SETUP()
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueLastConvoTime = -1
		ELSE
			IF iDialogueLastConvoTime = -1
				iDialogueLastConvoTime = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		MISSION_FLOW()
		
		#IF IS_DEBUG_BUILD
		
			IF debug_bDrawDebug
				TEXT_LABEL_31 strDebug = "Substage: "
				strDebug += iMissionSubstage
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.1, 0.0>>, 255, 255, 255, 255)
			ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
				debug_bDrawDebug = !debug_bDrawDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(debug_bDrawDebug)
			ENDIF
		#ENDIF
    ENDWHILE
ENDSCRIPT
