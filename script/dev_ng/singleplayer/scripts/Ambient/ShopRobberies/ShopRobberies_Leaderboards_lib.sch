USING "net_leaderboards.sch"

FUNC TEXT_LABEL_31 GET_SHOP_ROB_LOC_TEXT_LABEL(SHOP_ROBBERIES_SHOP_INDEX & ShopRobIndex)
	TEXT_LABEL_31 sLocation
	
	SWITCH ShopRobIndex
	
		CASE SHOP_ROBBERIES_SHOP_CONV_10
		CASE SHOP_ROBBERIES_SHOP_NONE 		
			
			sLocation = "InvalidEnum"
			
		BREAK
		
			// Gas station stores
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			sLocation = "SHR_GAS"	// LTD Gasoline
			
		BREAK
		
			 // Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1	
			
			sLocation = "SHR_LIQ1"	// Liquor Barn
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			sLocation = "SHR_LIQ2"	// Rob's Liquor
			
		BREAK
		
			// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			sLocation = "SHR_CONV"	// 24-7
			
		BREAK
		
		DEFAULT	sLocation = "InvalidEnum"	BREAK
		
	ENDSWITCH
	
	RETURN sLocation
ENDFUNC

PROC WRITE_SHOP_ROBBERIES_SCLB_DATA(SHOP_ROBBERIES_SHOP_INDEX & ShopRobIndex, BOOL & bRobberyHappened, INT & iCashAmount)
	
	/*
	Name: MINI_GAMES_SHOP_ROBBERIES
	ID: 276 - LEADERBOARD_MINI_GAMES_SHOP_ROBBERIES
	Inputs: 2
		LB_INPUT_COL_SCORE (long)
		LB_INPUT_COL_NUM_ROBBERIES (long)
	Columns: 2
		SCORE_COLUMN ( AGG_Sum ) - InputId: LB_INPUT_COL_SCORE
		NUM_ROBBERIES ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_ROBBERIES
	Instances: 2
		GameType,Location
		GameType,Location,Challenge
	*/
	
	INT iNumRobberies
	iNumRobberies = PICK_INT(bRobberyHappened, 1, 0)
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "-!!- WRITE_SHOP_ROBBERIES_SCLB_DATA")
	TEXT_LABEL_23 sIdentifier[3]
	TEXT_LABEL_31 categoryName[2]
	categoryName[0] = "GameType"
	categoryName[1] = "Location"
	sIdentifier[0] = "SP"
	sIdentifier[1] = GET_SHOP_ROB_LOC_TEXT_LABEL(ShopRobIndex)
	//sIdentifier[2] = "challenge"  //what is challenge for??
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, " -!!- Location: ", sIdentifier[1])
	//categoryName = categoryName
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_SHOP_ROBBERIES,sIdentifier,categoryName,2)
		
		// score is money earned
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, iCashAmount, TO_FLOAT(iCashAmount))
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, " -!!- [SCORE_COLUMN]: ", iCashAmount)
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_ROBBERIES, iNumRobberies, TO_FLOAT(iNumRobberies))
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, " -!!- [NUM_ROBBERIES]: ", iNumRobberies)
	ENDIF
ENDPROC

// EOF
