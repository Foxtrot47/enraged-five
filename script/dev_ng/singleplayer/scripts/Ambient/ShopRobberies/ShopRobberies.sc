// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	ShopRobberies.sc
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	Main single player Shop Robberies script.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// =====================================
// 	FILE INCLUDES
// =====================================


// -------------------------------------
// GENERAL INCLUDES
// -------------------------------------
USING "globals.sch"
USING "mission_control_public.sch"
USING "blip_control_public.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "commands_xml.sch"
USING "commands_object.sch"

USING "lineactivation.sch"
USING "building_control_public.sch"
USING "shared_hud_displays.sch"
USING "brains.sch"					// Includes COORDS_STRUCT, used as an argument in SCRIPT
USING "oddjob_aggro.sch"
//USING "z_volumes.sch"
USING "select_mission_stage.sch"
USING "shrink_stat_tracking.sch"	// Includes the command for collecting Shrink data.
USING "script_oddjob_funcs.sch"
USING "context_control_public.sch"
USING "load_queue_public.sch"

USING "ShopRobberies_Leaderboards_lib.sch"


// =====================================
// 	E N D  FILE INCLUDES
// =====================================





// ===================================
// 	ROBBERIES VARIABLES
// ===================================

// Enums for controlling print helps.
ENUM PRINT_HELP_ENUM
	PRINT_HELP_WAIT_FOR_PLAYER_TO_ENTER_STORE,
	PRINT_HELP_BUY_SNACKS,
	PRINT_HELP_HOLDUP_STORE,
	PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT,
	PRINT_HELP_DONE
ENDENUM

PRINT_HELP_ENUM eCurrentPrintEnum

// Enums for controlling dialog flow.
ENUM DIALOG_STATE_ENUM
	DIALOG_STATE_WAIT_FOR_PLAYER_TO_ENTER_STORE,
	DIALOG_STATE_CLERK_GREET_WHEN_PLAYER_ENTERS,
	DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP,
	DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP,
	DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIP,
	DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS,
	DIALOG_STATE_CLERK_SHOCKED_WHEN_PLAYER_ENTERS,
	DIALOG_STATE_CLERK_COUNTER_WARNING,
	DIALOG_STATE_CLERK_BACKROOM_WARNING,
	DIALOG_STATE_PLAYER_BUMPS_CLERK_BEFORE_HOLDUP,
	DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_SCARED,
	DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_AGGRO,
	DIALOG_STATE_HOLDUP_STARTS,
	DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION,
	DIALOG_STATE_WAIT_FOR_HOLDUP,
	DIALOG_STATE_CLERK_REFUSES_TO_HOLDUP,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS_BAZOOKA,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS_KNIFE,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS_TOOL_OR_BAT,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS_THROW,
	DIALOG_STATE_HOLDUP_CLERK_OBEYS_STICKY,
	DIALOG_STATE_HOLDUP_CLERK_ATTACKS,
	DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP,
	DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER,
	DIALOG_STATE_PLAYER_SEES_COPS,
	DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN,
	DIALOG_STATE_CLERK_FLEES,
	DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED,
	DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO,
	DIALOG_STATE_CLERK_REACTS_PLAYER_ALREADY_WANTED,
	DIALOG_STATE_PATRON_REACTS_TO_HOLDUP,
	DIALOG_STATE_CLERK_MID_HOLDUP,
	DIALOG_STATE_PLAYER_HURRY_UP,
	DIALOG_STATE_CLERK_BUY_SNACK_REACTION,
	DIALOG_STATE_CLERK_STEAL_SNACK_REACTION,
	DIALOG_STATE_CLERK_THREATENED,
	DIALOG_STATE_DONE
ENDENUM

DIALOG_STATE_ENUM eCurrentDialogState

// Enums for controlling the patron.
ENUM SHOP_PATRON_STATE_ENUM
	SHOP_PATRON_STATE_WAIT,
	SHOP_PATRON_STATE_CHECK_AVAILABILITY,
	SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM,
	SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT,
	SHOP_PATRON_STATE_FLEES,
	SHOP_PATRON_STATE_DONE,
	SHOP_PATRON_STATE_UNAVAILABLE
ENDENUM

SHOP_PATRON_STATE_ENUM eCurrentShopPatronState


// Enums to track at what phase the synched scene is in.
ENUM CASH_REGISTER_SCENE_PHASE
	CASH_REGISTER_SCENE_PHASE_INVALID,				
	CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS,				// 0.000 to 0.117
	CASH_REGISTER_SCENE_PHASE_GOING_FOR_REGISTER,			// 0.118 to 0.154
	CASH_REGISTER_SCENE_PHASE_PREPARING_BAG_AND_REGISTER,	// 0.155 to 0.477
	CASH_REGISTER_SCENE_PHASE_GRABBING_BAG,					// 0.478 to 0.487
	CASH_REGISTER_SCENE_PHASE_TAKING_CASH_INTO_BAG,			// 0.488 to 0.809
	CASH_REGISTER_SCENE_PHASE_LIFTING_BAG,					// 0.810 to 0.871
	CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG,			// 0.872 to 0.894
	CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS_WHEN_DONE,	// 0.894 to 0.999
	CASH_REGISTER_SCENE_PHASE_FINISHED						// 1.000
ENDENUM

CASH_REGISTER_SCENE_PHASE eCurrentCashRegisterScenePhase


// Enums for controlling the clerk animation sequence.
ENUM CASH_REGISTER_SYNCHED_SCENE_ENUM
	CASH_REGISTER_SYNCHED_SCENE_CREATE_AND_START,
	CASH_REGISTER_WAIT_FOR_CLERK_TO_USE_BAG,
	CASH_REGISTER_SYNCHED_SCENE_WAIT,
	CASH_REGISTER_SYNCHED_SCENE_DONE,
	CASH_REGISTER_SYNCHED_SCENE_CLEANUP
ENDENUM

CASH_REGISTER_SYNCHED_SCENE_ENUM eCurrentCashRegisterSynchedSceneState


// Enums for controlling the cash bag
ENUM CASH_BAG_STATE_ENUM
	CASH_BAG_STATE_WAIT,
	CASH_BAG_STATE_WAIT_FOR_BAG_TO_BE_PAST_COUNTER,
	CASH_BAG_STATE_WAIT_TO_CREATE_PICKUP,
	CASH_BAG_STATE_CREATE_PICKUP,
	CASH_BAG_STATE_CREATE_EARLY_PICKUP,
	CASH_BAG_STATE_PICKUP_ACTIVE,
	CASH_BAG_STATE_LOOP_END,
	CASH_BAG_STATE_DONE
ENDENUM

CASH_BAG_STATE_ENUM eCurrentCashBagState


// Enums for controlling when the shop clerk looks at something.
ENUM SHOP_CLERK_ANIM_STATE_ENUM
	SHOP_CLERK_ANIM_STATE_WAIT,
	SHOP_CLERK_ANIM_STATE_GREET,
	SHOP_CLERK_ANIM_STATE_FIRE,
	SHOP_CLERK_ANIM_STATE_WORRIED,
	SHOP_CLERK_ANIM_STATE_SHOCKED,
	SHOP_CLERK_ANIM_STATE_STUBBORN,
	SHOP_CLERK_ANIM_STATE_COWER,
	SHOP_CLERK_ANIM_STATE_PUSHED,
	SHOP_CLERK_ANIM_STATE_DONE
ENDENUM

SHOP_CLERK_ANIM_STATE_ENUM eCurrentShopClerkAnimState


// Enums for controlling when the shop clerk looks at something.
ENUM SHOP_CLERK_LOOK_STATE_ENUM
	SHOP_CLERK_LOOK_STATE_WAIT,
	SHOP_CLERK_LOOK_STATE_AT_PLAYER_AFTER_ENTERING_SHOP,
	SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_GETTING_CASH_IN_HOLDUP,
	SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_HOLDUP_ENDS,
	SHOP_CLERK_LOOK_STATE_DONE
ENDENUM

SHOP_CLERK_LOOK_STATE_ENUM eCurrentShopClerkLookState


// Enums for controlling the shop clerk.
ENUM SHOP_CLERK_STATE_ENUM
	SHOP_CLERK_STATE_WAIT,
	SHOP_CLERK_STATE_NORMAL,
	SHOP_CLERK_STATE_STUBBORN,
	SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH,
	SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN,
	SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE,
	SHOP_CLERK_STATE_KICKED_OUT_OF_SYNC_SCENE,
	SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH,
	SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED,
	SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER,
	SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN,
	SHOP_CLERK_STATE_DEFAULT_AI,
	SHOP_CLERK_STATE_DONE
ENDENUM

SHOP_CLERK_STATE_ENUM eCurrentShopClerkState


// Enums for tracking player state.
ENUM COPS_STATE_ENUM
	COPS_STATE_WAIT,
	COPS_STATE_UNAWARE,
	COPS_STATE_ALERTED,
	COPS_STATE_SIREN_DELAY,
	COPS_STATE_WAIT_FOR_PLAYER_TO_SEE_COPS,
	COPS_STATE_WARN_PLAYER_TO_SURRENDER,
	COPS_STATE_WAIT_FOR_PLAYER_TO_AGGRO_COPS,
	COPS_STATE_AGGRO,
	COPS_STATE_DONE
ENDENUM

COPS_STATE_ENUM eCurrentCopsState


// Enums for tracking player state.
ENUM PLAYER_STATE_ENUM
	PLAYER_STATE_WAIT,
	PLAYER_STATE_ENTER_STORE_WITHOUT_WEAPON,
	PLAYER_STATE_ENTER_STORE_WITH_BAT,
	PLAYER_STATE_ENTER_STORE_WITH_TOOL,
	PLAYER_STATE_ENTER_STORE_WITH_STICKY,
	PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON,
	PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING,
	PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP,
	PLAYER_STATE_EQUIPS_BAT_IN_SHOP,
	PLAYER_STATE_EQUIPS_TOOL_IN_SHOP,
	PLAYER_STATE_EQUIPS_STICKY_IN_SHOP,
	PLAYER_STATE_WAIT_FOR_FIRING_CLOSE_TO_CLERK,
	PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP,
	PLAYER_STATE_WAIT_FOR_PLAYER_TO_STOP_AFTER_LEAVING_SHOP,
	PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST,
	PLAYER_STATE_DONE
ENDENUM

PLAYER_STATE_ENUM eCurrentPlayerState


// Enums for controlling the flow of setting up Shop Robberies.
ENUM SETUP_SHOP_ROBBERIES_ENUM
	SETUP_SHOP_ROBBERIES_WAIT_UNTIL_PLAYER_IS_CLOSE,
	SETUP_SHOP_ROBBERIES_INIT,
	SETUP_SHOP_ROBBERIES_REQUEST_ASSETS,
	SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS,
	SETUP_SHOP_ROBBERIES_CREATE_ENTITIES
ENDENUM

SETUP_SHOP_ROBBERIES_ENUM eCurrentShopRobberiesSetupState


// Enums for controlling the flow of updating Shop Robberies.
ENUM UPDATE_SHOP_ROBBERIES_ENUM
	UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP,
	UPDATE_SHOP_ROBBERIES_CLERK_GREET,
	UPDATE_SHOP_ROBBERIES_CLERK_WORRIED,
	UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED,
	UPDATE_SHOP_ROBBERIES_CLERK_SCARED,
	UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP,
	UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG,
	UPDATE_SHOP_ROBBERIES_CLERK_AGGRO,
	UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START,
	UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL,
	UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED,
	UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED,
	UPDATE_SHOP_ROBBERIES_PLAYER_ALREADY_WANTED,
	UPDATE_SHOP_ROBBERIES_DONE
ENDENUM

UPDATE_SHOP_ROBBERIES_ENUM eCurrentShopRobberiesUpdateState


// Track whether or not the hold up has occured.
ENUM SHOP_ROBBERIES_HOLDUP_STATE_ENUM
	SHOP_ROBBERIES_HOLDUP_STATE_BEFORE,
	SHOP_ROBBERIES_HOLDUP_STATE_ON,
	SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON,
	SHOP_ROBBERIES_HOLDUP_STATE_DONE
ENDENUM

SHOP_ROBBERIES_HOLDUP_STATE_ENUM eCurrentShopRobberiesHoldUpState

ENUM SHOP_SNACKS_STATE
	SHOPSNACKS_INIT,
	SHOPSNACKS_WAIT_ACTIVATE_MENU,
	SHOPSNACKS_SETUP_MENU,
	SHOPSNACKS_UPDATE_MENU,
	SHOPSNACKS_BUY,
	SHOPSNACKS_SHOPLIFT,
	SHOPSNACKS_SHOPLIFT2,
	SHOPSNACKS_RESET,
	SHOPSNACKS_CLEANUP,
	SHOPSNACKS_DONE
ENDENUM

SHOP_SNACKS_STATE eShopSnacksState = SHOPSNACKS_INIT

// Enums for controlling the main flow of Shop Robberies.
ENUM SHOP_ROBBERIES_ENUM
	SHOP_ROBBERIES_SETUP,
	SHOP_ROBBERIES_UPDATE,
	SHOP_ROBBERIES_CLOSED,
	SHOP_ROBBERIES_CLEANUP
ENDENUM

SHOP_ROBBERIES_ENUM eCurrentShopRobberiesState

// Local Bool Bits
ENUM SHOP_ROB_LOCAL_BOOLS
	SHOPROB_IN_SHOP_ARMED		= 	BIT0,
	SHOPROB_NON_AGG_ENTRY		= 	BIT1,
	SHOPROB_DYN_REQUESTED		= 	BIT2,
	SHOPROB_OBJ_DESTROYED		= 	BIT3,
	SHOPROB_HLD_REQUESTED		= 	BIT4,
	SHOPROB_DESTROY_REACT		= 	BIT5,
	SHOPROB_DOOR_CHECK			= 	BIT6,
	SHOPROB_CLERK_RUN_COWER		= 	BIT7,
	SHOPROB_CLERK_AGGRO_SAID	= 	BIT8,
	SHOPROB_CLERK_LOCK_DELAY	= 	BIT9,
	SHOPROB_ITEMS_CREATED		= 	BIT10,
	SHOPROB_CONTROL_TAKEN		= 	BIT11
ENDENUM


// Contain all data for the clerk and cash bag synchronized scene.
STRUCT SHOP_DIALOG_STRUCT
	INT iClerkGetsCashSceneID
	
	// Dialog conversation handle.
	structPedsForConversation convoStruct
	
	// Dialog labels.
	STRING szBlockOfText
	STRING szRootLabel_Greet
	STRING szRootLabel_BatGreet
	STRING szRootLabel_ToolGreet
	STRING szRootLabel_StickyGreet
	STRING szRootLabel_Worry
	STRING szRootLabel_Shock
	STRING szRootLabel_Stubborn
	STRING szRootLabel_Counter
	STRING szRootLabel_BackRoom
	STRING szRootLabel_Bump
	STRING szRootLabel_BumpAimScared
	STRING szRootLabel_BumpAimAggro
	STRING szRootLabel_HoldUp
	STRING szRootLabel_Scared
	STRING szRootLabel_KnifeReaction
	STRING szRootLabel_ToolReaction
	STRING szRootLabel_ThrowReaction
	STRING szRootLabel_StickyReaction
	STRING szRootLabel_Brave
	STRING szRootLabel_HeldupAgain
	STRING szRootLabel_Surrender
	STRING szRootLabel_Cops
	STRING szRootLabel_JerryCan
	STRING szRootLabel_Flee
	STRING szRootLabel_RecognizeScared
	STRING szRootLabel_RecognizeAggro
	STRING szRootLabel_Patron
	STRING szRootLabel_MidHoldup
	STRING szRootLabel_Hurry
	STRING szRootLabel_BuySnack
	STRING szRootLabel_StealSnack
	
	// Dialog line lables
	STRING szSpecificLabel_ClerkGreetsPlayer
	STRING szSpecificLabel_ClerkReactsToBat
	STRING szSpecificLabel_ClerkReactsToTool
	STRING szSpecificLabel_ClerkReactsToSticky
	STRING szSpecificLabel_ClerkWorriesAtPlayer
	STRING szSpecificLabel_ClerkShockedAtPlayer
	STRING szSpecificLabel_ClerkStubbornAtPlayer
	STRING szSpecificLabel_ClerkCounterWarning
	STRING szSpecificLabel_ClerkBackroomWarning
	STRING szSpecificLabel_PlayerBumpsClerk
	STRING szSpecificLabel_ClerkScaredFromBump
	STRING szSpecificLabel_ClerkAggroFromBump
	STRING szSpecificLabel_PlayerHoldup
	STRING szSpecificLabel_ClerkTakesMoney
	STRING szSpecificLabel_ClerkPullsGun
	STRING szSpecificLabel_ClerkReactsToBazooka
	STRING szSpecificLabel_ClerkReactsToKnifeAim
	STRING szSpecificLabel_ClerkReactsToToolAim
	STRING szSpecificLabel_ClerkReactsToThrowAim
	STRING szSpecificLabel_ClerkReactsToStickyAim
	STRING szSpecificLabel_ClerkHeldupAgain
	STRING szSpecificLabel_CopsSurrenderWarning
	STRING szSpecificLabel_PlayerSeesCops
	STRING szSpecificLabel_ClerkReactsToJerryCan
	STRING szSpecificLabel_ClerkFlees
	STRING szSpecificLabel_ClerkRecognizesScared
	STRING szSpecificLabel_ClerkRecognizesAggro
	STRING szSpecificLabel_PatronReactsToHoldup
	STRING szSpecificLabel_ClerkMidHoldup
	STRING szSpecificLabel_PlayerHurryUp
	STRING szSpecificLabel_ClerkBuySnackReact
	STRING szSpecificLabel_ClerkStealSnackReact
ENDSTRUCT


// Contains all data for the taxi patron.
STRUCT SHOP_PATRON
	PED_INDEX pedIndex
	MODEL_NAMES modelName
	
	// Data of where the patron will be in LIQ3.
	VECTOR vPosition
	FLOAT fRadiusFromPosition
	
	// Task sequence for reacting to holdup.
	SEQUENCE_INDEX seqReactToHoldup
	
	// Timer before patron flees.
	structTimer timerBeforeFlee
ENDSTRUCT

STRUCT SHOP_TILL
	MODEL_NAMES modelName
	OBJECT_INDEX objIndex
	VECTOR vPos
	FLOAT fHeading
ENDSTRUCT

// Contain all data for the clerk and cash bag synchronized scene.
STRUCT SHOP_SYNCHED_SCENE
	INT iClerkGetsCashSceneID
	
	// Animations.
	STRING szHoldUpAnimDict
		STRING szClerkHandsUpAnimClip
		STRING szClerkCashRegisterAnimClip
		STRING szBagCashRegisterAnimClip
		STRING szTillAnimClip
	
	VECTOR vPos
	VECTOR vRotation
ENDSTRUCT

// Track the Shop Robberies bag.
STRUCT SHOP_CASH_BAG
	OBJECT_INDEX objIndex
	
	PICKUP_INDEX pickupIndex
	BLIP_INDEX blipIndex
	
	INT iPickupPlacementFlags
	
	INT iMinCash
	INT iMaxCash
	
	INT iCashSoundID
	
	VECTOR vPos
	VECTOR vRot
	//FLOAT fHeading
	MODEL_NAMES modelName
	
	BOOL bBagWithCashCreated
	
	structTimer timerBagSound
ENDSTRUCT

STRUCT SHOP_SNACKS
	VECTOR vCamPos
	VECTOR vCamRot
	
	VECTOR vPos
	FLOAT fRadius
	
	//locate info
	VECTOR 	vEntryPos[2]		// The entry points for the locate
	VECTOR 	vLocateCenter		// Coord used to check heading
	FLOAT	fEntrySize			// The size used for line activation
	INT		iNumLocates			// used for the for loop check in any given shop
	INT		iCurrentLocate
	
	//menu info
	INT		iCurrentSelection
	INT		iMenuLength
	INT		iUseContext
	
	//buying snack locate info
	VECTOR 	vBuyEntryPos[2]		// The entry points for the locate
	VECTOR 	vBuyLocateCenter	// Coord used to check heading
	FLOAT	fBuyEntrySize		// The size used for line activation
	
	BOOL bSnackBought
	BOOL bSnackStole
	BOOL bSnackBuyDialogueSaid
	
	BOOL bSnackAfford
	BOOL bCheckMoney
	BOOL bCheckHealth
	BOOL bSnackNeeded
	BOOL bSnackPurchased
	
	INT iHoldTime
	
	STRING szHeaderTxd
	
	STRING szLookEnterDict
	STRING szLookBaseDict
	STRING szLookExitDict
	
	STRING  szLookEnterClip
	STRING  szLookBaseClip
	STRING  szLookExitClip
	
	// Timer to allow for multiple snack buys
	structTimer timerSnackReset
ENDSTRUCT

// Track the Shop Robberies clerk.
STRUCT SHOP_CLERK
	PED_INDEX pedIndex
	VECTOR vPos
	FLOAT fHeading
	MODEL_NAMES modelName
	
	// Backroom cower data.
	VECTOR vBackRoomCowerPos
	FLOAT fBackRoomCowerHead
	
	INT iLookTime
	
	BOOL bIsStubborn
	BOOL bIsGoingToAttack
	BOOL bIsPayingAttention
	BOOL bLeftShop
	
	// Animations.
	STRING szReactionAnimDict
	STRING szGreetAnimClip
	STRING szFireAnimClip
	STRING szWorriedAnimClip
	STRING szShockedAnimClip
	STRING szStubbornAnimClip
	STRING szPushedAnimClip
	
	SEQUENCE_INDEX seqAimAndAttackPlayer
	SEQUENCE_INDEX seqHandUpsAndCower
	SEQUENCE_INDEX seqLookAtPlayer
	SEQUENCE_INDEX seqJustFlee
ENDSTRUCT


// Number of scripted cops in Shop Robberies.
CONST_INT iNumOfShopRobberiesCops	3

// Track the Shop Robberies cops.
STRUCT SHOP_COPS
	PED_INDEX pedCops[iNumOfShopRobberiesCops]
	VEHICLE_INDEX vehCars[iNumOfShopRobberiesCops]
	
	VECTOR vPositions[iNumOfShopRobberiesCops]
	FLOAT fHeadings[iNumOfShopRobberiesCops]
	
	VECTOR vDestinationPos
	FLOAT fDestinationRadius
	
	MODEL_NAMES modelCopName
	MODEL_NAMES modelCarName
	
	// Determines the area where the player won't be attacked by cops.
	// Currently set around the entrance to the shop.
	VECTOR vSafeZoneArea_Min
	VECTOR vSafeZoneArea_Max
	FLOAT fSafeZoneArea_Width
	
	SEQUENCE_INDEX seqDriveToShopAndWait[iNumOfShopRobberiesCops]
	
	INT iSirenDelay
ENDSTRUCT

STRUCT WANTED_MONITOR
	BOOL bShopWantedLevelGiven
	
	structTimer timerOutOfShop
ENDSTRUCT

CONST_INT BUDDY_INIT 0
CONST_INT BUDDY_HIDE 1

// functionally the same as the struct found in regular shops
STRUCT BUDDY_MONITOR
	PED_INDEX buddyPeds[4]
	VECTOR buddyCoords[4]
	INT iFlags
ENDSTRUCT

// Tracks all the shared elements of every Shop Robberies shop.
STRUCT SHOP_ROBBERIES_STRUCT
	PED_INDEX 		pedPlayer
	INT				iPlayerCharacterIndex
	
	SHOP_CLERK 		shopClerk
	SHOP_CASH_BAG 	shopCashBag
	SHOP_COPS		shopCops
	SHOP_PATRON		shopPatron
	SHOP_TILL		shopTill
	SHOP_SNACKS		shopSnacks
	WANTED_MONITOR 	wantedMonitor
	BUDDY_MONITOR	buddyMonitor
	
	// Shop variables.
	VECTOR vShopPos
	FLOAT fCleanupShopAtThisRadius
	INTERIOR_INSTANCE_INDEX shopInterior
	
	// Angled area covering main interior of shop.
	VECTOR vInteriorMainArea_Min
	VECTOR vInteriorMainArea_Max
	FLOAT fInteriorMainArea_Width
	
	// Angled area covering hallway (if shop has one).
	VECTOR vInteriorHallArea_Min
	VECTOR vInteriorHallArea_Max
	FLOAT fInteriorHallArea_Width
	
	// Angled area covering backroom.
	VECTOR vInteriorBackArea_Min
	VECTOR vInteriorBackArea_Max
	FLOAT fInteriorBackArea_Width
	
	// Angled area for backroom doorway.
	VECTOR vInteriorBackDoorArea_Min
	VECTOR vInteriorBackDoorArea_Max
	FLOAT fInteriorBackDoorArea_Width
	
	// Angled area for clerk counter.
	VECTOR vInteriorCounterArea_Min
	VECTOR vInteriorCounterArea_Max
	FLOAT fInteriorCounterArea_Width
	
	// Dialog struct.
	SHOP_DIALOG_STRUCT dialog
	
	// Player aggro state.
	AGGRO_ARGS aggroArgs
	
	// Keep track of what weapon the player is holding at holdup.
	BOOL bIsPlayerHoldingRocketLauncherAtHoldUp
	BOOL bIsPlayerHoldingKnifeAtHoldUp
	BOOL bPlayerIsHoldingToolAtHoldUp
	BOOL bPlayerIsHoldingThrowAtHoldUp
	BOOL bPlayerIsHoldingStickyAtHoldUp
	BOOL bFakeGlowRendered
	
	// Keep track of whether or not cops will ambush.
	BOOL bAreCopsGoingToAmbush
	
	// Keep track of whether or not player has used Jerrycan in front of clerk.
	BOOL bHasPlayerPouredJerryCan
	
	// Keep track if anims have been requested
	BOOL bAnimsRequested
	
	// Say midholdup line once
	BOOL bMidHoldupLineSaid
	
	BOOL bWriteToLeaderboard
	
	INT iUseContext
	INT iFlags
	
	structTimer timerReactDelay
	
	// How long the player pours petrol before player notices.
	structTimer timerPlayerPouringPetrol
	
	// Timer to delay the wanted level happening when the cashier recognizes the player
	structTimer timerWantedDelay
	
	structTimer fleeDelay
	
	structTimer warningTimer
	
	SHOP_SYNCHED_SCENE cashRegisterSynchedScene
ENDSTRUCT

SHOP_ROBBERIES_STRUCT Shop_Robberies

SEQUENCE_INDEX siTemp
SCENARIO_BLOCKING_INDEX sbiShopCLosed

// The current Shop Robberies shop.
SHOP_ROBBERIES_SHOP_INDEX eCurrentShop

INT iShopRobCashAmount

// Total number of times player has robbed a shop.
INT iTotalNumTimesPlayerHasRobbedShops

// Current max wanted level, used if on mission
INT iMissionMaxWantedLevel

// Check against this value to determine if we can launch the current shop.
//FLOAT fHoursBeforeShopCanBeRobbedAgain

INT iHoursBeforeShopCanBeRobbedAgain

// Check against this value to determine if the clerk will recognize the player.
//FLOAT fHoursBeforeShopClerkForgetsLastPlayerRobber

INT iDaysBeforeShopClerkForgetsLastPlayerRobber

// Track to see if we're on another mission
BOOL bOnAnyMission

// Track if the total number of times has been robbed has already updated.
BOOL bHasNumOfTimesShopRobbedUpdated

BOOL bPlayerAimingAtClerk
BOOL bDoHeadAdditive

STRING sHeadAdditiveAnim

// Track if certain optional dialog has played.
BOOL bHasCounterWarningDialogPlayed
BOOL bHasBackroomWarningDialogPlayed
BOOL bHasShopClerkBeenPushed
BOOL bIsClerk247

BOOL bIsPulseTriggered

// Store maximum possible wanted level in the game (Couldn't find this value in code).
CONST_INT iMaxDefaultWantedLevel	5

LoadQueueLight sLoadQueue


// ===================================
// 	E N D  ROBBERIES VARIABLES
// ===================================










// ==============================================================
// 	ROBBERIES PROCEDURES AND FUNCTIONS
// ==============================================================

// --------------------------------------------------------
// 	ROBBERIES HELPER PROCEDURES AND FUNCTIONS
// --------------------------------------------------------

/// Purpose:
///    Find the midpoint position vector between two points.
FUNC VECTOR FIND_MIDPOINT_OF_TWO_POINTS(VECTOR vPointA, VECTOR vPointB)
	VECTOR vMidPoint
	
	vMidPoint.x = 	(vPointA.x + vPointB.x) / 2
	vMidPoint.y =	(vPointA.y + vPointB.y) / 2
	vMidPoint.z =	(vPointA.z + vPointB.z) / 2
	
	RETURN vMidPoint
ENDFUNC

/// PURPOSE:
///    Convert real-world milliseconds into game hours.
///    
///    NOTES:
///    	- 	Use with caution.  This is an estimate from my own 
///    		calculations, which indicate  30 game minutes = 1 real-world minute.
FUNC FLOAT CONVERT_MILLISECONDS_TO_GAME_HOURS(INT iCurrentTimeInMilliseconds)
	FLOAT fCurrentTimeInRealSeconds = TO_FLOAT(iCurrentTimeInMilliseconds) / 1000.0
	FLOAT fCurrentTimeInRealMinutes = fCurrentTimeInRealSeconds / 60.0
	FLOAT fCurrentTimeInRealHours 	= fCurrentTimeInRealMinutes / 60.0

	FLOAT fGameHourInRealMinutes	= 2.0		// 1 real minute = 30 game minutes
	FLOAT fGameHourInRealHours		= fGameHourInRealMinutes / 60.0
	
	RETURN fCurrentTimeInRealHours / fGameHourInRealHours
ENDFUNC

// --------------------------------------------------------
// 	E N D  ROBBERIES HELPER PROCEDURES AND FUNCTIONS
// --------------------------------------------------------





// --------------------------------------------------------
// 	ROBBERIES GENERAL DATA PROCEDURES AND FUNCTIONS
// --------------------------------------------------------

/// PURPOSE:
///    Get the index of the shop interior.
PROC GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX] Procedure started.")
	
	VECTOR vShopInteriorCoords = FIND_MIDPOINT_OF_TWO_POINTS(Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max)

	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			Shop_Robberies.shopInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopInteriorCoords, "v_gasstation")
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX] Getting interior using the following coords: ", vShopInteriorCoords,
						", and with shop type: v_gasstation")
		BREAK	
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			Shop_Robberies.shopInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopInteriorCoords, "v_gen_liquor")	
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX] Getting interior using the following coords: ", vShopInteriorCoords,
						", and with shop type: v_gen_liquor")			
		BREAK

		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			Shop_Robberies.shopInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopInteriorCoords, "v_shop_247")
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX] Getting interior using the following coords: ", vShopInteriorCoords,
						", and with shop type: v_shop_247")				
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Get the index of the player character that last robbed a shop.
FUNC INT GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	// Ensure the shop index is valid.
	IF NOT IS_SHOP_ROBBERY_INDEX_VALID(eShopIndex)
		RETURN -1.0
	ENDIF
	
	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Shop index is 1.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_GAS_1]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Shop index is 2.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_GAS_2]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Shop index is 3.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_GAS_3]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Shop index is 4.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_GAS_4]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_GAS_5]
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_LIQ_1]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Shop index is 6.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_LIQ_2]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Shop index is 7.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_LIQ_3]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Shop index is 8.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_LIQ_4]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Shop index is 9.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_LIQ_5]

		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Shop index is 17.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_1]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Shop index is 18.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_2]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Shop index is 19.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_3]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Shop index is 20.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_4]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Shop index is 21.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_5]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Shop index is 22.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_6]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Shop index is 23.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_7]

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Shop index is 24.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_8]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_PLAYER_CHARACTER_INDEX_TO_LAST_ROB_SHOP_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Shop index is 25.")
			RETURN g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[SHOP_ROBBERIES_SHOP_CONV_9]

		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Get the time, in milliseconds, passed since a shop's last robbery by the last player character that robbed the shop by its index.
FUNC INT GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	// Ensure the shop index is valid.
	IF NOT IS_SHOP_ROBBERY_INDEX_VALID(eShopIndex)
		RETURN -1.0
	ENDIF
	
	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Shop index is 1.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_GAS_1]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Shop index is 2.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_GAS_2]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Shop index is 3.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_GAS_3]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Shop index is 4.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_GAS_4]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_GAS_5]
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_LIQ_1]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Shop index is 6.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_LIQ_2]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Shop index is 7.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_LIQ_3]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Shop index is 8.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_LIQ_4]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Shop index is 9.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_LIQ_5]

		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Shop index is 17.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_1]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Shop index is 18.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_2]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Shop index is 19.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_3]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Shop index is 20.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_4]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Shop index is 21.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_5]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Shop index is 22.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_6]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Shop index is 23.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_7]

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Shop index is 24.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_8]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_LAST_PLAYER_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Shop index is 25.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[SHOP_ROBBERIES_SHOP_CONV_9]

		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Get the time, in milliseconds, passed since a shop's last robbery by its index.
FUNC INT GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	// Ensure the shop index is valid.
	IF NOT IS_SHOP_ROBBERY_INDEX_VALID(eShopIndex)
		RETURN -1.0
	ENDIF
	
	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Shop index is 1.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_GAS_1]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Shop index is 2.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_GAS_2]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Shop index is 3.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_GAS_3]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Shop index is 4.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_GAS_4]
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_GAS_5]
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Shop index is 5.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_LIQ_1]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Shop index is 6.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_LIQ_2]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Shop index is 7.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_LIQ_3]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Shop index is 8.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_LIQ_4]
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Shop index is 9.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_LIQ_5]

		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Shop index is 17.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_1]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Shop index is 18.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_2]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Shop index is 19.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_3]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Shop index is 20.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_4]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Shop index is 21.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_5]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Shop index is 22.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_6]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Shop index is 23.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_7]

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Shop index is 24.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_8]
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_MILLISECS_SINCE_LAST_ROBBERY_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Shop index is 25.")
			RETURN g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[SHOP_ROBBERIES_SHOP_CONV_9]

		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC


/// PURPOSE:
///    Get the cops data based on the current shop index.
PROC GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR& vPositions[], FLOAT& fHeadings[], 
											 VECTOR& vDestinationPos, FLOAT& fDestinationRadius,
											 MODEL_NAMES& modelCopName, MODEL_NAMES& modelCarName,
											 VECTOR& vSafeZoneArea_Min, VECTOR& vSafeZoneArea_Max, FLOAT& fSafeZoneArea_Width)
	// Ensure the shop index is valid.
	IF NOT IS_SHOP_ROBBERY_INDEX_VALID(eShopIndex)
		EXIT
	ENDIF
	
	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Shop index is 1.")
			vPositions[0] 		= << -677.0792, -952.9478, 20.4712 >>
			fHeadings[0]		= 90.5427
			
			vPositions[1] 		= << -668.8872, -961.7076, 20.6313 >>
			fHeadings[1]		= 90.5427
			
			vPositions[2] 		= << -749.7003, -901.2264, 19.6691 >>
			fHeadings[2]		= 170.7498
			
			vDestinationPos 	= << -715.81, -930.27, 18.04 >>
			fDestinationRadius 	= 8.81
			
			vSafeZoneArea_Min 	= << -714.079, -918.088, 18.196 >>
			vSafeZoneArea_Max 	= << -709.079, -918.166, 22.196 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Shop index is 2.")
			vPositions[0] 		= << -133.86, -1764.29, 29.39 >>
			fHeadings[0]		= 297.87
			
			vPositions[1] 		= << -63.16, -1793.14, 27.23 >>
			fHeadings[1]		= 50.66
			
			vPositions[2] 		= << -56.50, -1810.22, 26.60 >>
			fHeadings[2]		= 50.67
			
			vDestinationPos 	= << -68.92, -1764.16, 28.23 >>
			fDestinationRadius 	= 9.00
			
			vSafeZoneArea_Min 	= << -55.922, -1756.021, 28.196 >>
			vSafeZoneArea_Max 	= << -52.122, -1759.271, 32.196 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Shop index is 3.")
			vPositions[0] 		= << 1103.0040, -378.3616, 66.6258 >>
			fHeadings[0]		= 310.6741
			
			vPositions[1] 		= << 1186.7031, -289.3757, 68.5913 >>
			fHeadings[1]		= 214.1061
			
			vPositions[2] 		= << 1251.6385, -371.6591, 68.7078 >>
			fHeadings[2]		= 164.5987
			
			vDestinationPos 	= << 1161.84, -339.25, 67.29 >>
			fDestinationRadius 	= 9.17
			
			vSafeZoneArea_Min 	= << 1157.417, -328.616, 68.048 >>
			vSafeZoneArea_Max 	= << 1162.351, -327.810, 72.048 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Shop index is 4.")
			vPositions[0] 		= << 1667.82, 4916.04, 41.71 >>
			fHeadings[0]		= 346.32
			
			vPositions[1] 		= << 1710.43, 4963.03, 43.92 >>
			fHeadings[1]		= 112.11
			
			vPositions[2] 		= << 1727.59, 4986.13, 46.40 >>
			fHeadings[2]		= 132.27
			
			vDestinationPos 	= << 1692.99, 4939.56, 41.16 >>
			fDestinationRadius 	= 10.0
			
			vSafeZoneArea_Min 	= << 1699.338, 4932.104, 41.083 >> 
			vSafeZoneArea_Max 	= << 1696.313, 4928.124, 45.083 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK			
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Shop index is 5.")
			vPositions[0] 		= << -1862.4363, 732.8416, 130.8709 >>
			fHeadings[0]		= 301.6958
			
			vPositions[1] 		= << -1870.9546, 722.3616, 129.6428 >>
			fHeadings[1]		= 300.6013
			
			vPositions[2] 		= << -1711.1029, 873.6471, 145.9411 >>
			fHeadings[2]		= 146.5694
			
			vDestinationPos 	= << -1814.22, 782.55, 136.57 >>
			fDestinationRadius 	= 7.24
			
			vSafeZoneArea_Min 	= << -1823.281, 785.687, 137.027 >>
			vSafeZoneArea_Max 	= << -1819.465, 788.918, 141.027 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF			
		BREAK
		
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Shop index is 5.")
			vPositions[0] 		= << 1127.43, 2680.61, 38.02 >>
			fHeadings[0]		= 268.18
			
			vPositions[1] 		= << 1205.02, 2687.10, 37.34 >>
			fHeadings[1]		= 91.37
			
			vPositions[2] 		= << 1218.17, 2677.40, 37.27 >>
			fHeadings[2]		= 88.15
			
			vDestinationPos 	= << 1164.39, 2683.20, 37.06 >>
			fDestinationRadius 	= 8.49
			
			vSafeZoneArea_Min 	= << 1164.771, 2702.528, 38.162 >>
			vSafeZoneArea_Max 	= << 1167.771, 2702.548, 41.162  >>
			fSafeZoneArea_Width = 3.0
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF		
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Shop index is 6.")
			vPositions[0] 		= << -3021.4045, 236.3884, 15.6982 >>
			fHeadings[0]		= 354.8025
			
			vPositions[1] 		= << -3021.8831, 207.4436, 15.7511 >>
			fHeadings[1]		= 2.3146
			
			vPositions[2] 		= << -3015.0122, 640.8910, 21.0514 >>
			fHeadings[2]		= 193.5162
			
			vDestinationPos 	= << -2990.03, 391.19, 13.83 >>
			fDestinationRadius 	= 9.41
			
			vSafeZoneArea_Min 	= << -2974.550, 393.338, 14.037 >>
			vSafeZoneArea_Max 	= << -2975.084, 388.366, 18.037 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Shop index is 7.")
			vPositions[0] 		= << -1270.8063, -944.2620, 10.8025 >>
			fHeadings[0]		= 17.2662
			
			vPositions[1] 		= << -1327.7673, -855.9691, 16.4331 >>
			fHeadings[1]		= 216.1689
			
			vPositions[2] 		= << -1155.7985, -862.4445, 13.4857 >>
			fHeadings[2]		= 36.3470
			
			vDestinationPos 	= << -1233.36, -891.39, 11.35 >>
			fDestinationRadius 	= 5.70
			
			vSafeZoneArea_Min 	= << -1225.200, -899.881, 11.275 >>
			vSafeZoneArea_Max 	= << -1229.293, -902.753, 15.275 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Shop index is 8.")
			vPositions[0] 		= << 1132.0165, -956.2646, 47.2548 >>
			fHeadings[0]		= 278.2335
			
			vPositions[1] 		= << 1121.2181, -959.5676, 46.7880 >>
			fHeadings[1]		= 289.4738
			
			vPositions[2] 		= << 1236.9592, -1153.5865, 37.1736 >>
			fHeadings[2]		= 27.5191
			
			vDestinationPos 	= << 1154.17, -979.23, 45.36 >>
			fDestinationRadius 	= 6.02
			
			vSafeZoneArea_Min 	= << 1142.794, -983.315, 45.205 >>
			vSafeZoneArea_Max 	= << 1142.519, -978.322, 49.205 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK

		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Shop index is 9.")
			vPositions[0] 		= << -1525.77, -389.17, 41.39 >>
			fHeadings[0]		= 229.73
			
			vPositions[1] 		= << -1537.28, -378.84, 43.02 >>
			fHeadings[1]		= 226.22
			
			vPositions[2] 		= << -1475.06, -418.35, 35.71 >>
			fHeadings[2]		= 45.71
			
			vDestinationPos 	= << -1502.71, -400.39, 38.41 >>
			fDestinationRadius 	= 7.86
			
			vSafeZoneArea_Min 	= << -1493.544, -382.617, 38.994 >>
			vSafeZoneArea_Max 	= << -1489.947, -386.090, 42.994 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK


		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Shop index is 17.")
			vPositions[0] 		= << -3218.9985, 1105.4094, 9.9489 >>
			fHeadings[0]		= 175.7402
			
			vPositions[1] 		= << -3210.8462, 1114.8745, 9.8453 >>
			fHeadings[1]		= 152.4543
			
			vPositions[2] 		= << -3201.5183, 920.3387, 13.8887 >>
			fHeadings[2]		= 54.6774
			
			vDestinationPos 	= << -3230.27, 1003.54, 11.31 >>
			fDestinationRadius 	= 5.42
			
			vSafeZoneArea_Min 	= << -3238.442, 1001.727, 11.161 >>
			vSafeZoneArea_Max 	= << -3238.268, 1006.724, 15.161 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Shop index is 18.")
			vPositions[0] 		= << -3061.2180, 658.6545, 9.6541 >>
			fHeadings[0]		= 216.9221
			
			vPositions[1] 		= << -3017.3911, 509.7967, 6.7435 >>
			fHeadings[1]		= 348.3592
			
			vPositions[2] 		= << -3029.2324, 521.1291, 6.9750 >>
			fHeadings[2]		= 337.5117
			
			vDestinationPos 	= << -3027.51, 594.25, 6.87 >>
			fDestinationRadius 	= 5.89
			
			vSafeZoneArea_Min 	= << -3036.615, 587.625, 6.818 >>
			vSafeZoneArea_Max 	= << -3038.087, 592.404, 10.818 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF		
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Shop index is 19.")
			vPositions[0] 		= << 523.8614, 2658.0571, 42.0680 >>
			fHeadings[0]		= 2.8999
			
			vPositions[1] 		= << 471.6328, 2657.8347, 42.9164 >>
			fHeadings[1]		= 329.1264
			
			vPositions[2] 		= << 656.9346, 2731.9272, 41.5306 >>
			fHeadings[2]		= 183.2800
			
			vDestinationPos 	= << 545.05, 2684.96, 41.30 >>
			fDestinationRadius 	= 6.77
			
			vSafeZoneArea_Min 	= << 546.505, 2674.393, 41.152 >>
			vSafeZoneArea_Max 	= << 541.547, 2673.750, 45.152 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF			
		BREAK

		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Shop index is 20.")
			vPositions[0] 		= << 2593.8406, 273.2788, 104.9260 >>
			fHeadings[0]		= 345.1121
			
			vPositions[1] 		= << 2595.6899, 262.9971, 104.2577 >>
			fHeadings[1]		= 350.3949
			
			vPositions[2] 		= << 2591.5449, 256.5517, 103.7544 >>
			fHeadings[2]		= 343.9362
			
			vDestinationPos 	= << 2575.75, 385.11, 107.46 >>
			fDestinationRadius 	= 11.61
			
			vSafeZoneArea_Min 	= << 2560.435, 382.891, 107.626 >>
			vSafeZoneArea_Max 	= << 2560.670, 387.886, 111.626 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF			
		BREAK
	
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Shop index is 21.")
			vPositions[0] 		= << 2672.64, 3259.44, 54.87 >>
			fHeadings[0]		= 324.30
			
			vPositions[1] 		= << 2703.37, 3294.47, 55.31 >>
			fHeadings[1]		= 152.87
			
			vPositions[2] 		= << 2699.45, 3299.76, 55.35 >>
			fHeadings[2]		= 152.55
			
			vDestinationPos 	= << 2691.41, 3275.22, 54.24  >>
			fDestinationRadius 	= 7.56
			
			vSafeZoneArea_Min 	= << 2681.770, 3279.805, 54.245 >>
			vSafeZoneArea_Max 	= << 2684.297, 3284.120, 58.245 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Shop index is 22.")
			vPositions[0] 		= << 1740.0348, 6378.2017, 35.0341 >>
			fHeadings[0]		= 81.6255
			
			vPositions[1] 		= << 1755.7858, 6375.0464, 36.2501 >>
			fHeadings[1]		= 76.0570 
			
			vPositions[2] 		= << 1752.8389, 6367.3438, 36.1350 >>
			fHeadings[2]		= 75.9713
			
			vDestinationPos 	= << 1725.56, 6400.85, 33.42 >>
			fDestinationRadius 	= 7.09
			
			vSafeZoneArea_Min 	= << 1728.266, 6411.254, 34.006 >>
			vSafeZoneArea_Max 	= << 1732.739, 6409.022, 38.006 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= SHERIFF			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Shop index is 23.")
			vPositions[0] 		= << 1942.1416, 3738.8970, 31.9693 >>
			fHeadings[0]		= 210.3384
			
			vPositions[1] 		= << 2024.8911, 3780.5884, 31.9156 >>
			fHeadings[1]		= 210.1552
			
			vPositions[2] 		= << 1879.6309, 3673.2915, 33.1966 >>
			fHeadings[2]		= 302.2257
			
			vDestinationPos 	= << 1968.55, 3731.03, 31.36 >>
			fDestinationRadius 	= 6.12
			
			vSafeZoneArea_Min 	= << 1963.491, 3738.337, 31.324 >>
			vSafeZoneArea_Max 	= << 1967.792, 3740.886, 35.324 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Shop index is 24.")
			vPositions[0] 		= << 73.57, -1359.63, 28.96 >>
			fHeadings[0]		= 93.85
			
			vPositions[1] 		= << 77.26, -1364.69, 28.97 >>
			fHeadings[1]		= 88.25
			
			vPositions[2] 		= << -17.95, -1366.03, 28.97 >>
			fHeadings[2]		= 267.29
			
			vDestinationPos 	= << 30.98, -1362.09, 28.33 >>
			fDestinationRadius 	= 6.00
			
			vSafeZoneArea_Min 	= << 27.296, -1350.255, 28.332325 >>
			vSafeZoneArea_Max 	= << 30.795, -1350.308, 30.820192 >>
			fSafeZoneArea_Width = 3.0
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Shop index is 25.")
			vPositions[0] 		= << 424.9115, 314.1133, 102.6220 >>
			fHeadings[0]		= 155.4277
			
			vPositions[1] 		= << 439.6049, 292.7892, 102.5935 >>
			fHeadings[1]		= 74.6758
			
			vPositions[2] 		= << 275.9610, 331.2577, 105.1467 >>
			fHeadings[2]		= 250.7508
			
			vDestinationPos 	= << 372.79, 313.00, 102.47 >>
			fDestinationRadius 	= 3.94
			
			vSafeZoneArea_Min 	= << 373.907, 322.739, 102.439 >>
			vSafeZoneArea_Max 	= << 378.778, 321.610, 106.439 >>
			fSafeZoneArea_Width = 3.7
			
			modelCopName 		= S_M_Y_COP_01
			modelCarName 		= POLICE			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Get the total number of shops robbed by player.
FUNC INT GET_NUM_OF_TIMES_PLAYER_HAS_ROBBED_A_SHOP()
	INT iTempNumShopsRobbed = 0
	INT iLoopCounter
	REPEAT NUM_SHOP_ROBBERIES_SHOPS iLoopCounter
		iTempNumShopsRobbed += g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed[iLoopCounter]
	ENDREPEAT
	RETURN  iTempNumShopsRobbed
ENDFUNC

// --------------------------------------------------------
// 	E N D  ROBBERIES GENERAL DATA PROCEDURES AND FUNCTIONS
// --------------------------------------------------------





// -------------------------------------------------
// 	PED STATUS PROCEDURES AND FUNCTIONS
// -------------------------------------------------

/// PURPOSE:
///    Return whether or not the player is inside the counter area.
FUNC BOOL IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
	//AND NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorCounterArea_Min, Shop_Robberies.vInteriorCounterArea_Max, Shop_Robberies.fInteriorCounterArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_COUNTER_AREA_OF_SHOP] Player is inside counter area of shop.")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return whether or not the player is inside the backroom area.
FUNC BOOL IS_PLAYER_INSIDE_BACKROOM_AREA_OF_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_BACKROOM_AREA_OF_SHOP] Player is inside back area of shop.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return whether or not the player vehicle is inside the shop.
FUNC BOOL IS_PLAYER_VEHICLE_INSIDE_SHOP()
	BOOL bCheckIfVehicleIsInShop = FALSE
	VEHICLE_INDEX vehVehicleToCheck

	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_PED_IN_ANY_VEHICLE(Shop_Robberies.pedPlayer)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player is inside a vehicle.  Get vehicle and check if vehicle is in the shop")
			vehVehicleToCheck = GET_VEHICLE_PED_IS_IN(Shop_Robberies.pedPlayer, FALSE)
			bCheckIfVehicleIsInShop = TRUE
		ELIF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player is not inside vehicle, but his last vehicle is well.  Get vehicle and check if vehicle is in the shop")
			vehVehicleToCheck = GET_PLAYERS_LAST_VEHICLE()
			bCheckIfVehicleIsInShop = TRUE
		ENDIF 	
	ENDIF
	
	IF bCheckIfVehicleIsInShop
		IF NOT IS_ENTITY_DEAD(vehVehicleToCheck)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Checking vehicle in shop now.")
			IF IS_ENTITY_IN_ANGLED_AREA(vehVehicleToCheck, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player vehicle is inside main area of shop.")
				RETURN TRUE
			ELIF IS_ENTITY_IN_ANGLED_AREA(vehVehicleToCheck, Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player vehicle is inside back area of shop.")
				RETURN TRUE
			ELIF IS_ENTITY_IN_ANGLED_AREA(vehVehicleToCheck, Shop_Robberies.vInteriorBackDoorArea_Min, Shop_Robberies.vInteriorBackDoorArea_Max, Shop_Robberies.fInteriorBackDoorArea_Width)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player vehicle is inside backdoor area of shop.")
				RETURN TRUE
			ELIF Shop_Robberies.fInteriorHallArea_Width <> -1
				IF IS_ENTITY_IN_ANGLED_AREA(vehVehicleToCheck, Shop_Robberies.vInteriorHallArea_Min, Shop_Robberies.vInteriorHallArea_Max, Shop_Robberies.fInteriorHallArea_Width)
					//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player vehicle is inside hallway area of shop.")
					RETURN TRUE
				ELSE
					//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player vehicle IS NOT inside hallway area of shop.")
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player vehicle is not inside shop.")
			ENDIF
		ELSE
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player vehicle is DEAD.  Can't perform check.")
		ENDIF
	ELSE
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Not checking if vehicle is inside shop.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return whether or not an explosion is in the shop.
FUNC BOOL IS_EXPLOSION_INSIDE_MAIN_AREA_OF_SHOP()
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside main area of shop.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return whether or not a bullet is in the shop.
FUNC BOOL IS_BULLET_INSIDE_MAIN_AREA_OF_SHOP()
	IF IS_BULLET_IN_ANGLED_AREA(Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside main area of shop.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return whether or not the player is inside the counter area.
FUNC BOOL IS_PLAYER_INSIDE_MAIN_AREA_OF_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside main area of shop.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return whether or not the player is inside the shop.
FUNC BOOL IS_PLAYER_INSIDE_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside main area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside back area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorBackDoorArea_Min, Shop_Robberies.vInteriorBackDoorArea_Max, Shop_Robberies.fInteriorBackDoorArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player IS inside backdoor area of shop.")
			RETURN TRUE
		ELIF Shop_Robberies.fInteriorHallArea_Width <> -1
			IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.vInteriorHallArea_Min, Shop_Robberies.vInteriorHallArea_Max, Shop_Robberies.fInteriorHallArea_Width)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is inside hallway area of shop.")
				RETURN TRUE
			ELSE
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player IS NOT inside hallway area of shop.")
			ENDIF
		ENDIF
		
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Player is NOT ANYWHERE inside the shop.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return whether or not an entity is inside the shop.  Mainly used for the event catcher
FUNC BOOL IS_ENTITY_INSIDE_SHOP(ENTITY_INDEX thisEntity)
	IF NOT IS_ENTITY_DEAD(thisEntity)
		IF GET_INTERIOR_FROM_ENTITY(thisEntity) = Shop_Robberies.shopInterior
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity is inside correct interior.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(thisEntity, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity is inside main area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(thisEntity, Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity is inside back area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(thisEntity, Shop_Robberies.vInteriorBackDoorArea_Min, Shop_Robberies.vInteriorBackDoorArea_Max, Shop_Robberies.fInteriorBackDoorArea_Width)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity IS inside backdoor area of shop.")
			RETURN TRUE
		ELIF Shop_Robberies.fInteriorHallArea_Width <> -1
			IF IS_ENTITY_IN_ANGLED_AREA(thisEntity, Shop_Robberies.vInteriorHallArea_Min, Shop_Robberies.vInteriorHallArea_Max, Shop_Robberies.fInteriorHallArea_Width)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity is inside hallway area of shop.")
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity IS NOT inside hallway area of shop.")
			ENDIF
		ENDIF
		
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] thisEntity is NOT ANYWHERE inside the shop.")
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return whether or not the clerk is inside the shop.
FUNC BOOL IS_CLERK_INSIDE_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		IF GET_INTERIOR_FROM_ENTITY(Shop_Robberies.shopClerk.pedIndex) = Shop_Robberies.shopInterior
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk is inside correct interior.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk is inside main area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk is inside back area of shop.")
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.vInteriorBackDoorArea_Min, Shop_Robberies.vInteriorBackDoorArea_Max, Shop_Robberies.fInteriorBackDoorArea_Width)
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk IS inside backdoor area of shop.")
			RETURN TRUE
		ELIF Shop_Robberies.fInteriorHallArea_Width <> -1
			IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.vInteriorHallArea_Min, Shop_Robberies.vInteriorHallArea_Max, Shop_Robberies.fInteriorHallArea_Width)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk is inside hallway area of shop.")
				RETURN TRUE
			ELSE
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk IS NOT inside hallway area of shop.")
			ENDIF
		ENDIF
		
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_INSIDE_SHOP] Clerk is NOT ANYWHERE inside the shop.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return whether or not the player is inside the shop.
FUNC BOOL HAS_PLAYER_LEFT_COP_SAFE_ZONE()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_ENTITY_IN_ANGLED_AREA(Shop_Robberies.pedPlayer, Shop_Robberies.shopCops.vSafeZoneArea_Min, Shop_Robberies.shopCops.vSafeZoneArea_Max, Shop_Robberies.shopCops.fSafeZoneArea_Width)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Returns whether the player has left the shop's active area.
FUNC BOOL HAS_PLAYER_LEFT_SHOP_ACTIVE_AREA()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(Shop_Robberies.pedPlayer)
		FLOAT fDistanceBetweenPlayerAndShop = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, Shop_Robberies.vShopPos)
	
		IF fDistanceBetweenPlayerAndShop > Shop_Robberies.fCleanupShopAtThisRadius
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns whether the player is close to the shop.
FUNC BOOL IS_PLAYER_CLOSE_TO_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		VECTOR vCurrentPlayerPos = GET_ENTITY_COORDS(Shop_Robberies.pedPlayer)
		IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPlayerPos, Shop_Robberies.vShopPos) < 50.0 //35.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the scripted cop peds or cars are in the player's view.
FUNC BOOL ARE_COPS_IN_PLAYER_VIEW()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		INT iCopCounter
		REPEAT iNumOfShopRobberiesCops iCopCounter
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter])
				IF NOT IS_ENTITY_OCCLUDED(Shop_Robberies.shopCops.pedCops[iCopCounter])
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->ARE_COPS_IN_PLAYER_VIEW] Cop ped ", iCopCounter," is in player view.  Return TRUE.")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.vehCars[iCopCounter])
				IF NOT IS_ENTITY_OCCLUDED(Shop_Robberies.shopCops.vehCars[iCopCounter])
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->ARE_COPS_IN_PLAYER_VIEW] Cop vehicle ", iCopCounter," is in player view.  Return TRUE.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->ARE_COPS_IN_PLAYER_VIEW] No cop ped or vehicle in view.")
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns if the scripted cop peds or cars are in the player's view.
FUNC BOOL IS_ANY_COP_PED_OUT_OF_HIS_CAR()
	INT iCopCounter
	REPEAT iNumOfShopRobberiesCops iCopCounter
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter])
			IF NOT IS_PED_IN_ANY_VEHICLE(Shop_Robberies.shopCops.pedCops[iCopCounter])
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_ANY_COP_PED_OUT_OF_HIS_CARS] Cop ped ", iCopCounter," is NOT in a vehicle.  Return TRUE.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Give him a weapon if he's going to attack.
FUNC BOOL HAS_SHOP_CLERK_BEEN_PUSHED()
	WEAPON_TYPE currentWeapon
	IF DOES_ENTITY_EXIST(Shop_Robberies.pedPlayer)
		GET_CURRENT_PED_WEAPON(Shop_Robberies.pedPlayer, currentWeapon)
	ELSE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_CLERK_RUN_COWER)
		IF IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
		OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.pedPlayer) AND currentWeapon = WEAPONTYPE_UNARMED)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.vPos) > 1.15
		//OR NOT IS_ENTITY_AT_COORD(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.vPos,<<0.250000,0.250000,1.000000>>)
			IF IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk is ragdolling.")
			ENDIF
			IF (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.pedPlayer) AND currentWeapon = WEAPONTYPE_UNARMED)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk has been damaged.")
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.vPos) > 1.15
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk dist = ",  GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.vPos))
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk is far from spawn point.")
			ENDIF
			IF NOT IS_ENTITY_AT_COORD(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.vPos,<<0.350000,0.350000,1.000000>>)
				VECTOR vTemp
				vTemp = GET_ENTITY_COORDS(Shop_Robberies.shopClerk.pedIndex)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk pos = ", vTemp)
				vTemp = Shop_Robberies.shopClerk.vPos
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] vpos = ", vTemp)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_BEEN_PUSHED] clerk is not in position.")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns whether the player is close to the shop.
FUNC BOOL IS_CLERK_SEEING_PLAYER_USE_PETROL_CAN_IN_SHOP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
		IF IS_PLAYER_INSIDE_MAIN_AREA_OF_SHOP()
			WEAPON_TYPE currentPlayerWeapon
			GET_CURRENT_PED_WEAPON(Shop_Robberies.pedPlayer, currentPlayerWeapon)
		
			IF currentPlayerWeapon = WEAPONTYPE_PETROLCAN
				IF IS_PED_SHOOTING(Shop_Robberies.pedPlayer)
					IF IS_PED_FACING_PED(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.pedPlayer,  180.0)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the Taxi Take It Easy passenger
///    is in LIQ3.  This command is only called
///    if the current shop is LIQ3.
FUNC BOOL IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP()
	GET_CLOSEST_PED(Shop_Robberies.shopPatron.vPosition, Shop_Robberies.shopPatron.fRadiusFromPosition, TRUE, FALSE, 
						Shop_Robberies.shopPatron.pedIndex, FALSE, TRUE)
	
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopPatron.pedIndex)
		IF GET_ENTITY_MODEL(Shop_Robberies.shopPatron.pedIndex) = Shop_Robberies.shopPatron.modelName
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CHECK_FOR_SPECIAL_CASE_BETWEEN_TAXI_AND_LIQ3] Patron is in LIQ3!  Setting as mission entity, and adding patron dialog.")
			SET_ENTITY_AS_MISSION_ENTITY(Shop_Robberies.shopPatron.pedIndex)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopPatron.pedIndex, TRUE)
			//ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 5, Shop_Robberies.shopPatron.pedIndex, "TaxiOtis")
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP] Patron was not found.  Returning TRUE.")
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP] A different ped is in LIQ3.")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP] Patron is dead.")
	ENDIF
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP] Patron was not found.  Returning FALSE.")
	RETURN FALSE
ENDFUNC

PROC SHOP_ROB_PROCESS_ENTITY_DAMAGED_EVENTS()
	EVENT_NAMES eventType
	STRUCT_ENTITY_ID sEntID
	//PED_INDEX piEntity
	
	INT iIndex
	
	#IF IS_DEBUG_BUILD
	STRING sModelName
	MODEL_NAMES eModel
	#ENDIF
	
	IF IS_PLAYER_INSIDE_SHOP()
	AND eShopSnacksState != SHOPSNACKS_UPDATE_MENU
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iIndex
			eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iIndex)
			
			SWITCH eventType
				CASE EVENT_ENTITY_DESTROYED
					CDEBUG1LN( DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught an entity destroyed event, parsing data." )
					
					GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_AI, iIndex, sEntID, SIZE_OF(STRUCT_ENTITY_ID) )
					
					IF IS_ENTITY_INSIDE_SHOP(sEntID.EntityId)
						IF IS_ENTITY_A_PED( sEntID.EntityId )
							IF (sEntID.EntityId != GET_ENTITY_FROM_PED_OR_VEHICLE(Shop_Robberies.shopClerk.pedIndex))
							AND (sEntID.EntityId != GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID()))
								CDEBUG1LN( DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught a destroyed ped that isn't the clerk or player" )
								SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_PED( sEntID.EntityId ) AND NOT IS_ENTITY_A_VEHICLE( sEntID.EntityId )
							CDEBUG1LN( DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught a destroyed object, setting the store peds to run or aggro" )
							SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
						ENDIF
					ENDIF
				BREAK
				CASE EVENT_ENTITY_DAMAGED
					CPRINTLN( DEBUG_SHOP_ROBBERIES, "[SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught an entity damaged event, parsing..." )
					
					GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_AI, iIndex, sEntID, SIZE_OF(STRUCT_ENTITY_ID) )
					
					IF IS_ENTITY_INSIDE_SHOP(sEntID.EntityId)
						IF IS_ENTITY_A_PED( sEntID.EntityId )
							IF (sEntID.EntityId != GET_ENTITY_FROM_PED_OR_VEHICLE(Shop_Robberies.shopClerk.pedIndex))
							AND (sEntID.EntityId != GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID()))
								CDEBUG1LN( DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught a damaged ped that isn't the clerk or player" )
								SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_A_PED( sEntID.EntityId ) AND NOT IS_ENTITY_A_VEHICLE( sEntID.EntityId )
							#IF IS_DEBUG_BUILD
							eModel = GET_ENTITY_MODEL(sEntID.EntityId)
							sModelName = GET_MODEL_NAME_STRING(eModel)
							CPRINTLN( DEBUG_SHOP_ROBBERIES, "[SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] model = ",  sModelName)
							#ENDIF
							CPRINTLN( DEBUG_SHOP_ROBBERIES, "[SHOP_ROB_PROCESS_ENTITY_DAMAGE_EVENTS] Caught a damaged object, setting clerk to flee" )
							SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
ENDPROC

PROC UPDATE_BUDDIES_FOR_SHOP_ROB()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iGroupPed
		
		IF IS_PLAYER_INSIDE_SHOP()
			
			IF NOT IS_BIT_SET(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "buddy update - init")
				
				PED_INDEX nearbyPeds[10]
				INT iNearbyPedCount = 0
				INT iNearbyPed = 0
				iNearbyPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
				
				REPEAT COUNT_OF(Shop_Robberies.buddyMonitor.buddyPeds) iGroupPed
					Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed] = NULL
				ENDREPEAT
				
				iGroupPed = 0
				
				REPEAT iNearbyPedCount iNearbyPed
					IF iGroupPed < COUNT_OF(Shop_Robberies.buddyMonitor.buddyPeds)
						IF DOES_ENTITY_EXIST(nearbyPeds[iNearbyPed])
						AND NOT IS_PED_INJURED(nearbyPeds[iNearbyPed])
						AND NOT IS_PED_IN_ANY_VEHICLE(nearbyPeds[iNearbyPed])
							IF IS_PED_GROUP_MEMBER(nearbyPeds[iNearbyPed], GET_PLAYER_GROUP(PLAYER_ID()))
								Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed] = nearbyPeds[iNearbyPed]
								Shop_Robberies.buddyMonitor.buddyCoords[iGroupPed] = GET_ENTITY_COORDS(nearbyPeds[iNearbyPed])
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "ped added to buddy struct, #", iGroupPed)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "new buddy coord = ", Shop_Robberies.buddyMonitor.buddyCoords[iGroupPed])
								iGroupPed++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				SET_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
			ENDIF
			
			REPEAT COUNT_OF(Shop_Robberies.buddyMonitor.buddyPeds) iGroupPed
				IF DOES_ENTITY_EXIST(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
				AND NOT IS_PED_INJURED(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
				AND NOT IS_PED_IN_ANY_VEHICLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
					IF IS_BIT_SET(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
						IF (GET_GAME_TIMER() % 1500) < 50
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "setting buddies invisible")
						ENDIF
						SET_ENTITY_VISIBLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], FALSE)
						SET_ENTITY_COLLISION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], FALSE)
						FREEZE_ENTITY_POSITION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], Shop_Robberies.buddyMonitor.buddyCoords[iGroupPed], TRUE, TRUE)
					ELSE
						IF NOT IS_ENTITY_VISIBLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "setting buddies visible")
							SET_ENTITY_VISIBLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], TRUE)
							SET_ENTITY_COLLISION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], TRUE)
							FREEZE_ENTITY_POSITION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], FALSE)
							SET_ENTITY_COORDS_NO_OFFSET(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], Shop_Robberies.buddyMonitor.buddyCoords[iGroupPed], TRUE, TRUE)
						ENDIF
					ENDIF
				ELSE
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "iGroupPed = ", iGroupPed)
//					IF NOT DOES_ENTITY_EXIST(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "DOES_ENTITY_EXIST(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])")
//					ELIF IS_PED_INJURED(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "IS_PED_INJURED(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])")
//					ELIF IS_PED_IN_ANY_VEHICLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "IS_PED_IN_ANY_VEHICLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])")
//					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			IF IS_BIT_SET(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "player is outside of shop, clearing buddy init")
				CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
			ENDIF
			REPEAT COUNT_OF(Shop_Robberies.buddyMonitor.buddyPeds) iGroupPed
				IF DOES_ENTITY_EXIST(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
				AND NOT IS_PED_INJURED(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
				AND NOT IS_PED_IN_ANY_VEHICLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
					IF NOT IS_ENTITY_VISIBLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed])
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "player is outside of shop, setting buddies to visible")
						SET_ENTITY_VISIBLE(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], TRUE)
						SET_ENTITY_COLLISION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], TRUE)
						FREEZE_ENTITY_POSITION(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(Shop_Robberies.buddyMonitor.buddyPeds[iGroupPed], Shop_Robberies.buddyMonitor.buddyCoords[iGroupPed], TRUE, TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

// -------------------------------------------------
// 	E N D  PED STATUS PROCEDURES AND FUNCTIONS
// -------------------------------------------------





// ----------------------------------------------------
// 	ROBBERIES AI PROCEDURES AND FUNCTIONS
// ----------------------------------------------------

/// PURPOSE:
///    Set the player wanted level, only if it's greater
///    than the current wanted level.
///    
///    bIsSetToMaxWanted - Sets iWantedLevel to be the max wanted level possible if TRUE.
///    						If FALSE, resets the max wanted level to its default value.
PROC SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(INT iWantedLevel, BOOL bIsThisMaxWanted)
	IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < iWantedLevel
	AND ((NOT bOnAnyMission) OR (bOnAnyMission AND (iWantedLevel < iMissionMaxWantedLevel)))
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER] Raising player Wanted Level from ", 
					GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())," to ", iWantedLevel," stars.")
		SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), iWantedLevel)
		SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
		
		Shop_Robberies.wantedMonitor.bShopWantedLevelGiven = TRUE
		
		IF bIsThisMaxWanted
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER] Set MAX wanted level at ", iWantedLevel)
			SET_MAX_WANTED_LEVEL(iWantedLevel)
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER] Reset MAX wanted level back to ", iMaxDefaultWantedLevel)
			SET_MAX_WANTED_LEVEL(iMaxDefaultWantedLevel)
		ENDIF
	ELSE
		IF bOnAnyMission 
		AND iWantedLevel > iMissionMaxWantedLevel
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER] Not raising wanted level, desired iwanted level ", 
					iWantedLevel, " is greater that max mission wanted level of ", iMissionMaxWantedLevel)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_AIMING_SHOP_ROB(PED_INDEX target, AGGRO_ARGS& aggroArgs)
	IF NOT IS_ENTITY_DEAD(target)
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN|WF_INCLUDE_MELEE)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), target)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), target)
				IF IS_PED_FACING_PED(target, PLAYER_PED_ID(), 90)
					IF GET_PLAYER_DISTANCE_FROM_ENTITY(target) <  aggroArgs.fAimRange
						IF aggroArgs.aimTimer = 0
							aggroArgs.aimTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - aggroArgs.aimTimer > aggroArgs.fAimTime
							PRINTLN("CHECK_AIMING_SHOP_ROB returns TRUE")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Give him a weapon if he's going to attack.
PROC GIVE_SHOP_ROBBERIES_CLERK_AGGRO_WEAPON()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		//GIVE_WEAPON_TO_PED(Shop_Robberies.shopClerk.pedIndex, WEAPONTYPE_PUMPSHOTGUN, 12)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the clerk to take out a gun and attack player.
PROC TASK_SHOP_ROBBERIES_CLERK_TO_AGGRO_PLAYER()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		// Equip clerk with gun.
		GIVE_WEAPON_TO_PED(Shop_Robberies.shopClerk.pedIndex, WEAPONTYPE_PUMPSHOTGUN, 12)
		SET_CURRENT_PED_WEAPON(Shop_Robberies.shopClerk.pedIndex, WEAPONTYPE_PUMPSHOTGUN)
		
		SET_PED_RELATIONSHIP_GROUP_HASH(Shop_Robberies.shopClerk.pedIndex, RELGROUPHASH_HATES_PLAYER)
		
		// Tell clerk to aim gun at player, and then shoot it. 
		OPEN_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqAimAndAttackPlayer)																				
			TASK_AIM_GUN_AT_ENTITY(NULL, Shop_Robberies.pedPlayer, 2000)
			TASK_SHOOT_AT_ENTITY(NULL, Shop_Robberies.pedPlayer, 4000, FIRING_TYPE_CONTINUOUS)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 25)
		CLOSE_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqAimAndAttackPlayer)
		
		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.seqAimAndAttackPlayer)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set clerk to turn his head towards the player.
PROC TASK_CLERK_TO_LOOK_AT_PLAYER()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		TASK_LOOK_AT_ENTITY(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.pedPlayer, -1)
		
	ENDIF
ENDPROC

PROC TASK_CLERK_TO_LOOK_AT_PLAYER_ALWAYS()
	IF (GET_GAME_TIMER() - Shop_Robberies.shopClerk.iLookTime) > 5000
		CLEAR_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqLookAtPlayer)
		OPEN_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqLookAtPlayer)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, Shop_Robberies.pedPlayer)
			TASK_LOOK_AT_ENTITY(NULL, Shop_Robberies.pedPlayer, -1)
		CLOSE_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqLookAtPlayer)
		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.seqLookAtPlayer)
		
		Shop_Robberies.shopClerk.iLookTime = GET_GAME_TIMER()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc] Clerk re-tasked to look at player")
	ENDIF
ENDPROC

/// PURPOSE:
///    Set clerk to his default AI behavior
PROC RESET_CLERK_TO_DEFAULT_AI()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		//TASK_HANDS_UP(Shop_Robberies.shopClerk.pedIndex, -1) 
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopClerk.pedIndex, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Task clerk to stop turning his head towards the player.
PROC TASK_CLERK_TO_STOP_LOOKING_AT_PLAYER()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		TASK_CLEAR_LOOK_AT(Shop_Robberies.shopClerk.pedIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    Task clerk to cower.
PROC TASK_CLERK_TO_COWER()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		
		CLEAR_SEQUENCE_TASK(siTemp)
		OPEN_SEQUENCE_TASK(siTemp)
			TASK_COWER(NULL, 5000)
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 100, -1)
		CLOSE_SEQUENCE_TASK(siTemp)
		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopClerk.pedIndex, siTemp)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Task clerk to run to the backroom and cower.
PROC TASK_CLERK_TO_COWER_IN_BACKROOM()
	IF IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		EXIT
	ENDIF
	
	SEQUENCE_INDEX siBackroomCower

	OPEN_SEQUENCE_TASK(siBackroomCower)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, Shop_Robberies.shopClerk.vBackRoomCowerPos, PEDMOVEBLENDRATIO_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, Shop_Robberies.shopClerk.fBackRoomCowerHead)
		TASK_COWER(NULL, -1)
	CLOSE_SEQUENCE_TASK(siBackroomCower)
	
	TASK_PERFORM_SEQUENCE(Shop_Robberies.shopClerk.pedIndex, siBackroomCower)
	CLEAR_SEQUENCE_TASK(siBackroomCower)
	
	SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_CLERK_RUN_COWER )
ENDPROC

/// PURPOSE:
///    Task clerk to raise hands and then cower.
PROC TASK_CLERK_TO_RAISE_HANDS_AND_COWER()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex) 
		CLEAR_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqHandUpsAndCower)
		OPEN_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqHandUpsAndCower)
			//TASK_HANDS_UP(NULL, 10000)
			TASK_COWER(NULL, 5000)
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 100, -1)
		CLOSE_SEQUENCE_TASK(Shop_Robberies.shopClerk.seqHandUpsAndCower)
		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopClerk.seqHandUpsAndCower)
	ENDIF
ENDPROC

/// PURPOSE:
///    Grabs any peds in the shop and tasks them to cower
PROC TASK_NEARBY_PEDS_TO_COWER()
	PED_INDEX piNearPeds[2]
	INT iPedCount, iIndex
	IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
	AND IS_PLAYER_INSIDE_SHOP()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc] tasking nearby peds to cower")
		
		iPedCount = GET_PED_NEARBY_PEDS( PLAYER_PED_ID(), piNearPeds )
		
		REPEAT iPedCount iIndex
			IF piNearPeds[iIndex] <> PLAYER_PED_ID()
			AND piNearPeds[iIndex] <> Shop_Robberies.shopClerk.pedIndex
			AND NOT IS_ENTITY_A_MISSION_ENTITY(piNearPeds[iIndex])
				SET_ENTITY_AS_MISSION_ENTITY( piNearPeds[iIndex], TRUE, TRUE )
				
				CLEAR_SEQUENCE_TASK(siTemp)
				OPEN_SEQUENCE_TASK(siTemp)
					TASK_COWER( NULL, 5000 )
					TASK_SMART_FLEE_PED( NULL, PLAYER_PED_ID(), 100.0, -1 )
				CLOSE_SEQUENCE_TASK(siTemp)
				TASK_PERFORM_SEQUENCE(piNearPeds[iIndex], siTemp)
			ENDIF
		ENDREPEAT	
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc] not doing ped cower task, player is not in shop")
	ENDIF
ENDPROC

/// PURPOSE:
///    Grabs any peds in the shop and tasks them to flee
PROC TASK_NEARBY_PEDS_TO_FLEE()
	PED_INDEX piNearPeds[2]
	INT iPedCount, iIndex
	
	IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
	AND IS_PLAYER_INSIDE_SHOP()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc] tasking nearby peds to flee")
		
		iPedCount = GET_PED_NEARBY_PEDS( PLAYER_PED_ID(), piNearPeds )
		
		REPEAT iPedCount iIndex
			IF piNearPeds[iIndex] <> PLAYER_PED_ID()
			AND piNearPeds[iIndex] <> Shop_Robberies.shopClerk.pedIndex
			AND NOT IS_PED_INJURED(piNearPeds[iIndex])
			AND NOT IS_ENTITY_A_MISSION_ENTITY(piNearPeds[iIndex])
			AND NOT IS_PED_IN_COMBAT(piNearPeds[iIndex])
				SET_ENTITY_AS_MISSION_ENTITY( piNearPeds[iIndex], TRUE, TRUE )
				
				TASK_SMART_FLEE_PED( piNearPeds[iIndex], PLAYER_PED_ID(), 100.0, -1 )
			ENDIF
		ENDREPEAT
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc] not doing ped flee task, player is not in shop")
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the cops to attack the player.
PROC SET_SHOP_ROBBERIES_COPS_TO_AGGRO_PLAYER_WHEN_WANTED()
	INT iCopCounter
	REPEAT iNumOfShopRobberiesCops iCopCounter
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter])
			/* // Used to set aggro manually.
			IF IS_PED_IN_ANY_VEHICLE(Shop_Robberies.pedPlayer)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEAR_SHOP_ROBBERIES_COPS_SEQUENCE_TASKS] Cleared ped tasks of cop #", iCopCounter)
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEAR_SHOP_ROBBERIES_COPS_SEQUENCE_TASKS] Set cop #", iCopCounter, " to shoot player.")
				TASK_SHOOT_AT_ENTITY(Shop_Robberies.shopCops.pedCops[iCopCounter], Shop_Robberies.pedPlayer, 1000, FIRING_TYPE_DEFAULT)
				
				// NOTE: It doesn't seem I can OR these, or set them all to an int.
				SET_PED_COMBAT_ATTRIBUTES(Shop_Robberies.shopCops.pedCops[iCopCounter], CA_USE_COVER,								TRUE)
				SET_PED_COMBAT_ATTRIBUTES(Shop_Robberies.shopCops.pedCops[iCopCounter], CA_LEAVE_VEHICLES, 							TRUE)
				SET_PED_COMBAT_ATTRIBUTES(Shop_Robberies.shopCops.pedCops[iCopCounter], CA_CAN_CAPTURE_ENEMY_PEDS, 					TRUE)
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEAR_SHOP_ROBBERIES_COPS_SEQUENCE_TASKS] Set cop #", iCopCounter, " to hate player.")
			SET_PED_RELATIONSHIP_GROUP_HASH(Shop_Robberies.shopCops.pedCops[iCopCounter], RELGROUPHASH_HATES_PLAYER)
			*/
			
			// By releasing them from script constraints, cops will aggro player as appropriate based on wanted levet.
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopCops.pedCops[iCopCounter], FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    NOTE: Can't get this to work.  AI does nothing.
///    Set the cops to attack the player.
PROC SET_SHOP_ROBBERIES_COPS_TO_ARREST_PLAYER()
	INT iCopCounter
	REPEAT iNumOfShopRobberiesCops iCopCounter
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter])
			
			IF NOT IS_PED_RUNNING_ARREST_TASK(Shop_Robberies.shopCops.pedCops[iCopCounter])
				CLEAR_PED_TASKS(Shop_Robberies.shopCops.pedCops[iCopCounter])
				TASK_ARREST_PED(Shop_Robberies.shopCops.pedCops[iCopCounter], Shop_Robberies.pedPlayer)
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SET_SHOP_ROBBERIES_COPS_TO_ARREST_PLAYER] Ped cop ", iCopCounter, " tasked with arresting player.")
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE
///    Task patron to react to the player holding up the shop.
PROC TASK_PATRON_TO_REACT_TO_HOLDUP()
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopPatron.pedIndex)
		OPEN_SEQUENCE_TASK(Shop_Robberies.shopPatron.seqReactToHoldup)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, Shop_Robberies.pedPlayer)//, 2000)
			TASK_LOOK_AT_ENTITY(NULL, Shop_Robberies.pedPlayer, 2500)
			TASK_HANDS_UP(NULL, 2500)
		CLOSE_SEQUENCE_TASK(Shop_Robberies.shopPatron.seqReactToHoldup)

		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopPatron.pedIndex, Shop_Robberies.shopPatron.seqReactToHoldup)
	ENDIF
ENDPROC

// ----------------------------------------------------
// 	E N D  ROBBERIES AI PROCEDURES AND FUNCTIONS
// ----------------------------------------------------





// -------------------------------------------------------------
// 	ROBBERIES SYNCHED SCENE PROCEDURES AND FUNCTIONS
// -------------------------------------------------------------

/// PURPOSE:
///    Return whether or not the synched scene has finished.
FUNC BOOL HAS_SYNCHED_SCENE_FINISHED()
	IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
		IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) >= 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return which phase the synched scene is in.
PROC GET_CURRENT_CASH_REGISTER_SCENE_PHASE()
	IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
		IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) >= 0 
				IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.117
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.154
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_GOING_FOR_REGISTER
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.477
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_PREPARING_BAG_AND_REGISTER
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.487
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_GRABBING_BAG
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.809
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_TAKING_CASH_INTO_BAG
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.871
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_LIFTING_BAG
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.894
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.999
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS_WHEN_DONE
				ELIF
					eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_FINISHED
				ENDIF
			ELSE
				eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_INVALID
			ENDIF
		ELIF
			eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_INVALID
		ENDIF
	ELSE
		eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_INVALID
	ENDIF
ENDPROC


/// PURPOSE:
///    Setup synced scene of clerk taking money from register.
FUNC BOOL IS_CASH_REGISTER_SYNCHED_SCENE_DONE()

	// Update the current phase of the cash register synched scene.
	GET_CURRENT_CASH_REGISTER_SCENE_PHASE()
	
	SWITCH(eCurrentCashRegisterSynchedSceneState)
		
		// Create and start playing the synchronized scene.
		CASE CASH_REGISTER_SYNCHED_SCENE_CREATE_AND_START
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] CASE CASH_REGISTER_SYNCHED_SCENE_CREATE_AND_START: Case started.")
			
			Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID = CREATE_SYNCHRONIZED_SCENE(Shop_Robberies.cashRegisterSynchedScene.vPos, 
																										Shop_Robberies.cashRegisterSynchedScene.vRotation)
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID = ", 
						Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
			
			IF NOT IS_PED_DEAD_OR_DYING(Shop_Robberies.shopClerk.pedIndex)
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
					IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				
						SET_ENTITY_COLLISION(Shop_Robberies.shopCashBag.objIndex, TRUE)
						SET_ENTITY_DYNAMIC(Shop_Robberies.shopCashBag.objIndex, TRUE)
						
						
						TASK_SYNCHRONIZED_SCENE(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, 
												Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict, Shop_Robberies.cashRegisterSynchedScene.szClerkCashRegisterAnimClip,
													NORMAL_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE )
													
						PLAY_SYNCHRONIZED_ENTITY_ANIM(Shop_Robberies.shopCashBag.objIndex, Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, 
									Shop_Robberies.cashRegisterSynchedScene.szBagCashRegisterAnimClip, Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict, 
									INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY))
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(Shop_Robberies.shopTill.objIndex, Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, 
									Shop_Robberies.cashRegisterSynchedScene.szTillAnimClip, Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict, 
									INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY))
						
					ELSE
						RETURN TRUE
					ENDIF 
												
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Cash: Jumping to CASH_BAG_STATE_WAIT_FOR_BAG_TO_BE_PAST_COUNTER")
					eCurrentCashBagState = CASH_BAG_STATE_WAIT_FOR_BAG_TO_BE_PAST_COUNTER
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Clerk: Jumping to SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE")
					eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Scene: Jumping from CASH_REGISTER_SYNCHED_SCENE_CREATE_AND_START to CASH_REGISTER_SYNCHED_SCENE_WAIT")
					eCurrentCashRegisterSynchedSceneState = CASH_REGISTER_SYNCHED_SCENE_WAIT
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		// Wait until the clerk grabs the cash bag.
		CASE CASH_REGISTER_WAIT_FOR_CLERK_TO_USE_BAG
			IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.478
						IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
							//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, Shop_Robberies.shopCashBag.objIndex, -1)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(Shop_Robberies.shopCashBag.objIndex, Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, 
															Shop_Robberies.cashRegisterSynchedScene.szBagCashRegisterAnimClip, Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict, 
															INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY))
							
							Shop_Robberies.shopCashBag.vPos = << Shop_Robberies.shopCashBag.vPos.x, Shop_Robberies.shopCashBag.vPos.y, Shop_Robberies.shopCashBag.vPos.z+0.20 >>
						ENDIF
					
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Scene: Jumping from CASH_REGISTER_WAIT_FOR_CLERK_TO_USE_BAG to CASH_REGISTER_SYNCHED_SCENE_WAIT")
						eCurrentCashRegisterSynchedSceneState = CASH_REGISTER_SYNCHED_SCENE_WAIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Wait for synched scene to finish.
		CASE CASH_REGISTER_SYNCHED_SCENE_WAIT
			IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.447
						IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
							IF NOT IS_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CASH_REGISTER_SYNCHED_SCENE_WAIT] Scene: Setting cash bag to be visible.")
								SET_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF Shop_Robberies.bMidHoldupLineSaid
						IF GET_SYNCHRONIZED_SCENE_RATE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) <> 1.45
							SET_SYNCHRONIZED_SCENE_RATE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID, 1.45)
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Sync scene rate is now ", GET_SYNCHRONIZED_SCENE_RATE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID))
						ENDIF
					ENDIF
										
					IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.894
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Scene: Jumping from CASH_REGISTER_SYNCHED_SCENE_WAIT to CASH_REGISTER_SYNCHED_SCENE_DONE")
						eCurrentCashRegisterSynchedSceneState = CASH_REGISTER_SYNCHED_SCENE_DONE	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Clean up synched scene.
		CASE CASH_REGISTER_SYNCHED_SCENE_CLEANUP
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
				IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
					IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
						IF eCurrentCashRegisterScenePhase >= CASH_REGISTER_SCENE_PHASE_GRABBING_BAG
							IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) < 0.871
								eCurrentCashBagState = CASH_BAG_STATE_CREATE_EARLY_PICKUP
							ENDIF
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Stopping sync scene on cash bag.")
							STOP_SYNCHRONIZED_ENTITY_ANIM(Shop_Robberies.shopCashBag.objIndex, FAST_BLEND_OUT, TRUE)
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Activating physics on cash bag")
							ACTIVATE_PHYSICS(Shop_Robberies.shopCashBag.objIndex)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			//DETACH_SYNCHRONIZED_SCENE(shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->IS_CASH_REGISTER_SYNCHED_SCENE_DONE] Scene: Jumping from CASH_REGISTER_SYNCHED_SCENE_CLEANUP to CASH_REGISTER_SYNCHED_SCENE_DONE")
			eCurrentCashRegisterSynchedSceneState = CASH_REGISTER_SYNCHED_SCENE_DONE
		BREAK
		
		// Clean up synched scene.
		CASE CASH_REGISTER_SYNCHED_SCENE_DONE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// -------------------------------------------------------------
// 	E N D  ROBBERIES AI SYNCHED SCENE PROCEDURES AND FUNCTIONS
// -------------------------------------------------------------





// -------------------------------------------------
// 	ROBBERIES SETUP PROCEDURES AND FUNCTIONS
// -------------------------------------------------

/// PURPOSE:
///    Initialize dialog system.
PROC CREATE_SHOP_ROBBERIES_DIALOG()
	INT iRandomClerkGreetLine  						= GET_RANDOM_INT_IN_RANGE(1, 11)
	INT iRandomClerkReactsToBatLine					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkReactsToToolLine				= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkReactsToStickyLine				= GET_RANDOM_INT_IN_RANGE(1, 3)
	INT iRandomClerkWorryLine  						= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomClerkShockedLine  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkStubbornLine  					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkCounterLine  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkBackroomLine					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkBumpLine						= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkBumpAimScaredLine				= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkBumpAimAggroLine				= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomPlayerHoldupLine 					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkScaredLine  					= GET_RANDOM_INT_IN_RANGE(1, 9)
	INT iRandomClerkScaredBazookaLine  				= GET_RANDOM_INT_IN_RANGE(1, 3)
//	INT iRandomClerkKnifeAimLine  					= GET_RANDOM_INT_IN_RANGE(1, 5)
//	INT iRandomClerkToolAimLine  					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkThrowAimLine  					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomClerkStickyAimLine  					= GET_RANDOM_INT_IN_RANGE(1, 3)
	INT iRandomClerkBraveLine  						= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomClerkHeldupAgainLine					= GET_RANDOM_INT_IN_RANGE(1, 5)
	INT iRandomCopsSurrenderLine  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomPlayerCopsLine  						= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkJerrycanLine					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkFleesLine  						= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomClerkReconScaredLine  				= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomClerkReconAggroLine  				= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomPatronHoldupLine  					= GET_RANDOM_INT_IN_RANGE(1, 6)
	INT iRandomClerkMidHoldup	  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomPlayerHurryUp	  					= GET_RANDOM_INT_IN_RANGE(1, 3)
	INT iRandomClerkBuySnack	  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomClerkSneakSnack	  					= GET_RANDOM_INT_IN_RANGE(1, 4)
	
	// Fill out the conversation struct.
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerHoldupLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_1"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_2"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_3"
			BREAK
			
			CASE 4
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_4"
			BREAK
		ENDSWITCH
			
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerCopsLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_1"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_2"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_3"
			BREAK
//			
//			CASE 4
//				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_4"
//			BREAK
		ENDSWITCH
		
		// Determine which hurry up line the player will say.
		SWITCH iRandomPlayerHurryUp
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_1"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_2"
			BREAK
		ENDSWITCH
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
		
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerHoldupLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_5"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_6"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_7"
			BREAK
			
			CASE 4
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_8"
			BREAK
		ENDSWITCH
		
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerCopsLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_5"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_6"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_7"
			BREAK
//			
//			CASE 4
//				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_8"
//			BREAK
		ENDSWITCH
		
		// Determine which hurry up line the player will say.
		SWITCH iRandomPlayerHurryUp
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_3"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_4"
			BREAK
		ENDSWITCH
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 0, PLAYER_PED_ID(), "TREVOR")
		
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerHoldupLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_9"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_10"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_11"
			BREAK
			
			CASE 4
				Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup = "OJSR_HOLDUP_12"
			BREAK
		ENDSWITCH
		
		// Determine which hold up line the player will say.
		SWITCH iRandomPlayerCopsLine
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_9"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_10"
			BREAK
			
			CASE 3
				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_11"
			BREAK
//			
//			CASE 4
//				Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops = "OJSR_COPS_12"
//			BREAK
		ENDSWITCH
		
		// Determine which hurry up line the player will say.
		SWITCH iRandomPlayerHurryUp
			CASE 1
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_5"
			BREAK
			
			CASE 2
				Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp = "OJSR_PLRHUR_6"
			BREAK
		ENDSWITCH
		
	ENDIF
	
	
	// Determine which greeting line the clerk will say.
	SWITCH iRandomClerkGreetLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_5"
		BREAK
		
		CASE 6
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_6"
		BREAK
		
		CASE 7
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_7"
		BREAK
		
		CASE 8
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_8"
		BREAK
		
		CASE 9
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_9"
		BREAK
		
		CASE 10
			Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer = "OJSR_GREET_10"
		BREAK
	ENDSWITCH
	
	// Determine which line the clerk will play for the player entering to the store with a bat.
	SWITCH iRandomClerkReactsToBatLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat = "OJSR_BATEQ_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat = "OJSR_BATEQ_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat = "OJSR_BATEQ_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat = "OJSR_BATEQ_4"
		BREAK		
	ENDSWITCH
	
	
	// Determine which line the clerk will play for the player entering to the store with a tool weapon (wrench, crowbar, hammer).
	SWITCH iRandomClerkReactsToToolLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool = "OJSR_TOOLEQ_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool = "OJSR_TOOLEQ_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool = "OJSR_TOOLEQ_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool = "OJSR_TOOLEQ_4"
		BREAK		
	ENDSWITCH
	
	
	// Determine which line the clerk will play for the player entering to the store with a sticky grenade.
	SWITCH iRandomClerkReactsToStickyLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky = "OJSR_STICKEQ_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky = "OJSR_STICKEQ_2"
		BREAK
		
//		CASE 3
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky = "OJSR_STICKEQ_3"
//		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky = "OJSR_STICKEQ_4"
//		BREAK		
	ENDSWITCH	
	

	// Determine which worried line the clerk will say when player enters the store holding a weapon.
	SWITCH iRandomClerkWorryLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_5"
		BREAK
//		
//		CASE 6
//			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_6"
//		BREAK
//		
//		CASE 7
//			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_7"
//		BREAK
//		
//		CASE 8
//			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_8"
//		BREAK
//		
//		CASE 9
//			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_9"
//		BREAK
//		
//		CASE 10
//			Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer = "OJSR_WORRY_10"
//		BREAK
	ENDSWITCH
	
	
	// Determine which shocked line the clerk will say after player crashes into the shop entrance.
	SWITCH iRandomClerkShockedLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer = "OJSR_SHOCK_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer = "OJSR_SHOCK_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer = "OJSR_SHOCK_3"
		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer = "OJSR_SHOCK_4"
//		BREAK
	ENDSWITCH
	
	
	// Determine which shocked line the clerk will say after player crashes into the shop entrance.
	SWITCH iRandomClerkStubbornLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_4"
		BREAK
//		
//		CASE 5
//			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_5"
//		BREAK
//		
//		CASE 6
//			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_6"
//		BREAK
//		
//		CASE 7
//			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_7"
//		BREAK
//		
//		CASE 8
//			Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer = "OJSR_STUBBRN_8"
//		BREAK
	ENDSWITCH
	
	
	// Determine which counter warning line the clerk will say after player enters the counter before holdup.
	SWITCH iRandomClerkCounterLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning = "OJSR_COUNTER_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning = "OJSR_COUNTER_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning = "OJSR_COUNTER_3"
		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning = "OJSR_COUNTER_4"
//		BREAK
//		
//		CASE 5
//			Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning = "OJSR_COUNTER_5"
//		BREAK
	ENDSWITCH
	
	
	// Determine which counter warning line the clerk will say after player enters the counter before holdup.
	SWITCH iRandomClerkBackroomLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_3"
		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_4"
//		BREAK
//		
//		CASE 5
//			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_5"
//		BREAK
//		
//		CASE 6
//			Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning = "OJSR_BACKRM_6"
//		BREAK
	ENDSWITCH
	
	
	// Determine which bump line the clerk will say.
	SWITCH iRandomClerkBumpLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk = "OJSR_BUMP_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk = "OJSR_BUMP_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk = "OJSR_BUMP_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk = "OJSR_BUMP_4"
		BREAK	
	ENDSWITCH
	
	
	// Determine which bumb aim scared line the clerk will say.   iRandomClerkBumpAimAggroLine
	SWITCH iRandomClerkBumpAimScaredLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump = "OJSR_BPAIM_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump = "OJSR_BPAIM_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump = "OJSR_BPAIM_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump = "OJSR_BPAIM_4"
		BREAK	
	ENDSWITCH
	
	
	// Determine which bumb aim scared line the clerk will say.   iRandomClerkBumpAimAggroLine
	SWITCH iRandomClerkBumpAimAggroLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump = "OJSR_BPAIMAG_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump = "OJSR_BPAIMAG_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump = "OJSR_BPAIMAG_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump = "OJSR_BPAIMAG_4"
		BREAK	
	ENDSWITCH
	
	// Determine which scare line the clerk will say.
	SWITCH iRandomClerkScaredLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_5"
		BREAK
		
		CASE 6
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_6"
		BREAK
		
		CASE 7
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_7"
		BREAK
		
		CASE 8
			Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney = "OJSR_SCARED_8"
		BREAK
	ENDSWITCH
	
	
	// Determine which line the clerk will say when being aimed at with a rocket launcher.
	SWITCH iRandomClerkScaredBazookaLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka = "OJSR_SCARED_9"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka = "OJSR_SCARED_10"
		BREAK
//		
//		CASE 3
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka = "OJSR_SCARED_11"
//		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka = "OJSR_SCARED_12"
//		BREAK
	ENDSWITCH
	
	
	// Determine which line the clerk will say when being aimed at with a knife.
//	SWITCH iRandomClerkKnifeAimLine
//		CASE 1
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim = "OJSR_KNIFAIM_1"
//		BREAK
//		
//		CASE 2
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim = "OJSR_KNIFAIM_2"
//		BREAK
//		
//		CASE 3
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim = "OJSR_KNIFAIM_3"
//		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim = "OJSR_KNIFAIM_4"
//		BREAK
//	ENDSWITCH
	
	
	// Determine which line the clerk will say when being aimed at with a melee weapon.
//	SWITCH iRandomClerkToolAimLine
//		CASE 1
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim = "OJSR_TOOLAIM_1"
//		BREAK
//		
//		CASE 2
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim = "OJSR_TOOLAIM_2"
//		BREAK
//		
//		CASE 3
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim = "OJSR_TOOLAIM_3"
//		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim = "OJSR_TOOLAIM_4"
//		BREAK
//	ENDSWITCH
	
	
	// Determine which line the clerk will say when being aimed at with a grenade.
	SWITCH iRandomClerkThrowAimLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim = "OJSR_GRANAIM_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim = "OJSR_GRANAIM_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim = "OJSR_GRANAIM_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim = "OJSR_GRANAIM_4"
		BREAK
	ENDSWITCH
	
	
	// Determine which line the clerk will say when being aimed at with a sticky grenade.
	SWITCH iRandomClerkStickyAimLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim = "OJSR_STICAIM_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim = "OJSR_STICAIM_2"
		BREAK
		
//		CASE 3
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim = "OJSR_STICAIM_3"
//		BREAK
//		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim = "OJSR_STICAIM_4"
//		BREAK
	ENDSWITCH	
	
	
	// Determine which brave line the clerk will say.
	SWITCH iRandomClerkBraveLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_5"
		BREAK
		
//		CASE 6
//			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_6"
//		BREAK
//		
//		CASE 7
//			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_7"
//		BREAK
//		
//		CASE 8
//			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_8"
//		BREAK
//		
//		CASE 9
//			Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun = "OJSR_BRAVE_9"
//		BREAK
	ENDSWITCH
	
	
	// Determine which hold up again line the clerk will say.
	SWITCH iRandomClerkHeldupAgainLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkHeldupAgain = "OJSR_MOREAIM_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkHeldupAgain = "OJSR_MOREAIM_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkHeldupAgain = "OJSR_MOREAIM_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkHeldupAgain = "OJSR_MOREAIM_4"
		BREAK		
	ENDSWITCH
	
	
	// Determine which brave line the clerk will say.
	SWITCH iRandomCopsSurrenderLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_CopsSurrenderWarning = "OJSR_SURNDER_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_CopsSurrenderWarning = "OJSR_SURNDER_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_CopsSurrenderWarning = "OJSR_SURNDER_3"
		BREAK
	ENDSWITCH
	
	
	// Determine which fleeing line the clerk will say.
	SWITCH iRandomClerkJerrycanLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerryCan = "OJSR_POURCAN_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerryCan = "OJSR_POURCAN_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerryCan = "OJSR_POURCAN_3"
		BREAK
		
//		CASE 4
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerryCan = "OJSR_POURCAN_4"
//		BREAK
//		
//		CASE 5
//			Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerryCan = "OJSR_POURCAN_5"
//		BREAK
	ENDSWITCH
	
	
	// Determine which fleeing line the clerk will say.
	SWITCH iRandomClerkFleesLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkFlees = "OJSR_FLEE_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkFlees = "OJSR_FLEE_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkFlees = "OJSR_FLEE_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkFlees = "OJSR_FLEE_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkFlees = "OJSR_FLEE_5"
		BREAK
	ENDSWITCH
	

	// Determine which recognizing scared line the clerk will say when player re-enters shop he robbed last.
	SWITCH iRandomClerkReconScaredLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared = "OJSR_RECSCAR_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared = "OJSR_RECSCAR_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared = "OJSR_RECSCAR_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared = "OJSR_RECSCAR_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared = "OJSR_RECSCAR_5"
		BREAK
	ENDSWITCH
	
	
	// Determine which recognizing scared line the clerk will say when player re-enters shop he robbed last.
	SWITCH iRandomClerkReconAggroLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro = "OJSR_RECAGGR_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro = "OJSR_RECAGGR_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro = "OJSR_RECAGGR_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro = "OJSR_RECAGGR_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro = "OJSR_RECAGGR_5"
		BREAK
	ENDSWITCH		
	
	
	// Determine which fleeing line the clerk will say.
	SWITCH iRandomPatronHoldupLine
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup = "OJSR_CSTMER_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup = "OJSR_CSTMER_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup = "OJSR_CSTMER_3"
		BREAK
		
		CASE 4
			Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup = "OJSR_CSTMER_4"
		BREAK
		
		CASE 5
			Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup = "OJSR_CSTMER_5"
		BREAK
	ENDSWITCH
	
	// Determine which mid-holdup line the clerk will say.
	SWITCH iRandomClerkMidHoldup
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkMidHoldup = "OJSR_MOSC_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkMidHoldup = "OJSR_MOSC_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkMidHoldup = "OJSR_MOSC_3"
		BREAK
		
	ENDSWITCH
	
	SWITCH iRandomClerkBuySnack
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkBuySnackReact = "OJSR_BUY_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkBuySnackReact = "OJSR_BUY_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkBuySnackReact = "OJSR_BUY_3"
		BREAK
	ENDSWITCH
	
	SWITCH iRandomClerkSneakSnack
		CASE 1
			Shop_Robberies.dialog.szSpecificLabel_ClerkStealSnackReact = "OJSR_STEAL_1"
		BREAK
		
		CASE 2
			Shop_Robberies.dialog.szSpecificLabel_ClerkStealSnackReact = "OJSR_STEAL_2"
		BREAK
		
		CASE 3
			Shop_Robberies.dialog.szSpecificLabel_ClerkStealSnackReact = "OJSR_STEAL_3"
		BREAK
	ENDSWITCH
	
	
	// Set the dialog labels.
	Shop_Robberies.dialog.szBlockOfText 				= "OJSRAUD"
	Shop_Robberies.dialog.szRootLabel_Greet				= "OJSR_GREET"
	Shop_Robberies.dialog.szRootLabel_BatGreet			= "OJSR_BATEQ"
	Shop_Robberies.dialog.szRootLabel_ToolGreet			= "OJSR_TOOLEQ"
	Shop_Robberies.dialog.szRootLabel_StickyGreet		= "OJSR_STICKEQ"
	Shop_Robberies.dialog.szRootLabel_Worry				= "OJSR_WORRY"
	Shop_Robberies.dialog.szRootLabel_Shock				= "OJSR_SHOCK"
	Shop_Robberies.dialog.szRootLabel_Stubborn			= "OJSR_STUBBRN"
	Shop_Robberies.dialog.szRootLabel_Counter			= "OJSR_COUNTER"
	Shop_Robberies.dialog.szRootLabel_BackRoom			= "OJSR_BACKRM"
	Shop_Robberies.dialog.szRootLabel_Bump				= "OJSR_BUMP"
	Shop_Robberies.dialog.szRootLabel_BumpAimScared		= "OJSR_BPAIM"
	Shop_Robberies.dialog.szRootLabel_BumpAimAggro		= "OJSR_BPAIMAG"
	Shop_Robberies.dialog.szRootLabel_HoldUp			= "OJSR_HOLDUP"
	Shop_Robberies.dialog.szRootLabel_Scared			= "OJSR_SCARED"
	Shop_Robberies.dialog.szRootLabel_KnifeReaction		= "OJSR_KNIFAIM"
	Shop_Robberies.dialog.szRootLabel_ToolReaction		= "OJSR_TOOLAIM"
	Shop_Robberies.dialog.szRootLabel_ThrowReaction		= "OJSR_GRANAIM"
	Shop_Robberies.dialog.szRootLabel_StickyReaction 	= "OJSR_STICAIM"
	Shop_Robberies.dialog.szRootLabel_Brave				= "OJSR_BRAVE"
	Shop_Robberies.dialog.szRootLabel_HeldupAgain		= "OJSR_MOREAIM"
	Shop_Robberies.dialog.szRootLabel_Surrender			= "OJSR_SURNDER"
	Shop_Robberies.dialog.szRootLabel_Cops				= "OJSR_COPS"
	Shop_Robberies.dialog.szRootLabel_JerryCan			= "OJSR_POURCAN"
	Shop_Robberies.dialog.szRootLabel_Flee				= "OJSR_FLEE"
	Shop_Robberies.dialog.szRootLabel_RecognizeScared	= "OJSR_RECSCAR"
	Shop_Robberies.dialog.szRootLabel_RecognizeAggro	= "OJSR_RECAGGR"
	Shop_Robberies.dialog.szRootLabel_Patron			= "OJSR_CSTMER"
	Shop_Robberies.dialog.szRootLabel_MidHoldup			= "OJSR_MOSC"
	Shop_Robberies.dialog.szRootLabel_Hurry				= "OJSR_PLRHUR"
	Shop_Robberies.dialog.szRootLabel_BuySnack			= "OJSR_BUY"
	Shop_Robberies.dialog.szRootLabel_StealSnack		= "OJSR_STEAL"
	
	// Add the clerk for dialog.  Cop will be added to dialog once he's created.
	//ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 3, Shop_Robberies.shopClerk.pedIndex, "SHOPCLERK")
	
	bHasCounterWarningDialogPlayed	= FALSE
	bHasBackroomWarningDialogPlayed	= FALSE
	bHasShopClerkBeenPushed			= FALSE
ENDPROC


/// PURPOSE:
///    Init the LIQ3 patron.  Ped will already be created from a different script.
PROC INIT_SHOP_ROBBERIES_PATRON
	Shop_Robberies.shopPatron.modelName = A_M_Y_SOUCENT_04
	
	// Data of where the patron will be in LIQ3.
	Shop_Robberies.shopPatron.vPosition	 			= << -1224.15, -907.20, 12.33 >>
	Shop_Robberies.shopPatron.fRadiusFromPosition	= 2.00
ENDPROC
	


/// PURPOSE:
///    Create cops and cop cars, and task each one.
PROC CREATE_AND_TASK_SHOP_ROBBERIES_COPS()
	INT iCopCounter
	REPEAT iNumOfShopRobberiesCops iCopCounter
		// Create cop.
		Shop_Robberies.shopCops.pedCops[iCopCounter] = CREATE_PED(PEDTYPE_COP, Shop_Robberies.shopCops.modelCopName, Shop_Robberies.shopCops.vPositions[iCopCounter], 
														Shop_Robberies.shopCops.fHeadings[iCopCounter])
		SET_ENTITY_AS_MISSION_ENTITY(Shop_Robberies.shopCops.pedCops[iCopCounter])
		
		// Immediately set the clerk to do only what this script tells him to.
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopCops.pedCops[iCopCounter], TRUE)
		
		// Create cop car.
		Shop_Robberies.shopCops.vehCars[iCopCounter] = CREATE_VEHICLE(Shop_Robberies.shopCops.modelCarName, Shop_Robberies.shopCops.vPositions[iCopCounter],
														Shop_Robberies.shopCops.fHeadings[iCopCounter])
		SET_ENTITY_AS_MISSION_ENTITY(Shop_Robberies.shopCops.vehCars[iCopCounter])
														
		// Place cop in cop car, and give him a handgun.
		IF NOT (IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter]) AND IS_ENTITY_DEAD(Shop_Robberies.shopCops.vehCars[iCopCounter]))
			SET_PED_INTO_VEHICLE(Shop_Robberies.shopCops.pedCops[iCopCounter], Shop_Robberies.shopCops.vehCars[iCopCounter])
			GIVE_WEAPON_TO_PED(Shop_Robberies.shopCops.pedCops[iCopCounter], WEAPONTYPE_PISTOL, 99)
		ENDIF

		// Task cop to go shop.
		OPEN_SEQUENCE_TASK(Shop_Robberies.shopCops.seqDriveToShopAndWait[iCopCounter])																				
			TASK_VEHICLE_DRIVE_TO_COORD(NULL, Shop_Robberies.shopCops.vehCars[iCopCounter], Shop_Robberies.shopCops.vDestinationPos, 
									GET_VEHICLE_ESTIMATED_MAX_SPEED(Shop_Robberies.shopCops.vehCars[iCopCounter]) - 8.0,
									DRIVINGSTYLE_RACING, Shop_Robberies.shopCops.modelCarName, 
									DRIVINGMODE_AVOIDCARS | DF_ChangeLanesAroundObstructions | DF_DriveIntoOncomingTraffic, 
									Shop_Robberies.shopCops.fDestinationRadius, -1.0 )
			TASK_LEAVE_ANY_VEHICLE(NULL)
			TASK_AIM_GUN_AT_ENTITY(NULL, Shop_Robberies.pedPlayer, -1)
		CLOSE_SEQUENCE_TASK(Shop_Robberies.shopCops.seqDriveToShopAndWait[iCopCounter])
		
		TASK_PERFORM_SEQUENCE(Shop_Robberies.shopCops.pedCops[iCopCounter], Shop_Robberies.shopCops.seqDriveToShopAndWait[iCopCounter])
		
		
		
		// Set cop for dialog.
		//ADD_PED_FOR_DIALOGUE(Shop_Robberies.dialog.convoStruct, 4, Shop_Robberies.shopCops.pedCops[0], "SHOPROBCOP1")
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Create the cash bag pick up on the floor.
PROC CREATE_CASH_BAG_PICKUP(BOOL bEarly = FALSE)
	Shop_Robberies.shopCashBag.iPickupPlacementFlags = 0
	
	SET_BIT(Shop_Robberies.shopCashBag.iPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(Shop_Robberies.shopCashBag.iPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	
	IF NOT bEarly
		iShopRobCashAmount = GET_RANDOM_INT_IN_RANGE(Shop_Robberies.shopCashBag.iMinCash, Shop_Robberies.shopCashBag.iMaxCash)
		
		IF NOT DOES_BLIP_EXIST(Shop_Robberies.shopCashBag.blipIndex)
	       	Shop_Robberies.shopCashBag.blipIndex = CREATE_BLIP_FOR_OBJECT(Shop_Robberies.shopCashBag.objIndex)
			SET_BLIP_COLOUR(Shop_Robberies.shopCashBag.blipIndex, BLIP_COLOUR_GREEN)
		ENDIF
		
		SET_PICKUP_OBJECT_GLOW_OFFSET(Shop_Robberies.shopCashBag.objIndex, -0.2)
	ELSE
		iShopRobCashAmount = GET_RANDOM_INT_IN_RANGE(50, Shop_Robberies.shopCashBag.iMinCash)
		Shop_Robberies.shopCashBag.pickupIndex = CREATE_PICKUP(PICKUP_MONEY_VARIABLE, GET_ENTITY_COORDS(Shop_Robberies.shopCashBag.objIndex), 
												Shop_Robberies.shopCashBag.iPickupPlacementFlags, iShopRobCashAmount)
		
		IF NOT DOES_BLIP_EXIST(Shop_Robberies.shopCashBag.blipIndex)
	       	Shop_Robberies.shopCashBag.blipIndex = CREATE_BLIP_FOR_PICKUP(Shop_Robberies.shopCashBag.pickupIndex)
		ENDIF
	ENDIF
	
	Shop_Robberies.shopCashBag.bBagWithCashCreated = TRUE
ENDPROC


/// PURPOSE:
///    Create the shop cash bag.
PROC CREATE_SHOP_ROBBERIES_CASH_BAG()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_CASH_BAG] Procedure started.")

//	Shop_Robberies.shopCashBag.objIndex = CREATE_OBJECT(Shop_Robberies.shopCashBag.modelName, Shop_Robberies.shopCashBag.vPos)
	Shop_Robberies.shopCashBag.objIndex = CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED, Shop_Robberies.shopCashBag.vPos, FALSE, Shop_Robberies.shopCashBag.modelName)
	SET_ENTITY_ROTATION(Shop_Robberies.shopCashBag.objIndex, Shop_Robberies.shopCashBag.vRot)
	//SET_ENTITY_HEADING(Shop_Robberies.shopCashBag.objIndex, Shop_Robberies.shopCashBag.fHeading)
	SET_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex, FALSE)
	
	PREVENT_COLLECTION_OF_PORTABLE_PICKUP(Shop_Robberies.shopCashBag.objIndex, TRUE)
	
	Shop_Robberies.shopCashBag.bBagWithCashCreated = FALSE
ENDPROC


/// PURPOSE:
///    Create the shop clerk.
PROC CREATE_SHOP_ROBBERIES_CLERK()
	INT iTexture, iDrawable
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_CLERK] Procedure started.")

	Shop_Robberies.shopClerk.pedIndex = CREATE_PED(PEDTYPE_CIVMALE, Shop_Robberies.shopClerk.modelName, Shop_Robberies.shopClerk.vPos, 
													Shop_Robberies.shopClerk.fHeading)
													
	//SET_ENTITY_HEALTH(Shop_Robberies.shopClerk.pedIndex, 100)
	GET_SHOP_CLERK_HEAD(eCurrentShop, iDrawable, iTexture)
	//GET_SHOP_CLERK_HEAD( eCurrentShop )
	SET_PED_COMPONENT_VARIATION(Shop_Robberies.shopClerk.pedIndex, INT_TO_ENUM(PED_COMPONENT,0), iDrawable, iTexture, 0)
	GET_SHOP_CLERK_HAIR(iDrawable, iTexture)
	SET_PED_COMPONENT_VARIATION(Shop_Robberies.shopClerk.pedIndex, PED_COMP_HAIR, iDrawable, iTexture)
	GET_SHOP_CLERK_TORSO(iDrawable, iTexture)
	SET_PED_COMPONENT_VARIATION(Shop_Robberies.shopClerk.pedIndex, PED_COMP_TORSO, iDrawable, iTexture)
	GET_SHOP_CLERK_LEG(iDrawable, iTexture)
	SET_PED_COMPONENT_VARIATION(Shop_Robberies.shopClerk.pedIndex, PED_COMP_LEG, iDrawable, iTexture)
	GET_SHOP_CLERK_SPECIAL(iDrawable, iTexture)
	SET_PED_COMPONENT_VARIATION(Shop_Robberies.shopClerk.pedIndex, PED_COMP_SPECIAL, iDrawable, iTexture)
	
	// Immediately set the clerk to do only what this script tells him to.
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopClerk.pedIndex, TRUE)
	
	// Give him a weapon if he's going to attack.
	IF Shop_Robberies.shopClerk.bIsGoingToAttack
		GIVE_SHOP_ROBBERIES_CLERK_AGGRO_WEAPON()
	ENDIF
ENDPROC

PROC CREATE_SHOP_TILL()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_TILL] Procedure started.")
	
	CREATE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_01, TRUE)
	CREATE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_02, TRUE)
	CREATE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_03, TRUE)
	
	Shop_Robberies.shopTill.objIndex = CREATE_OBJECT(Shop_Robberies.shopTill.modelName, Shop_Robberies.cashRegisterSynchedScene.vPos)
	SET_ENTITY_HEADING(Shop_Robberies.shopTill.objIndex, Shop_Robberies.shopTill.fHeading)
	
	FREEZE_ENTITY_POSITION(Shop_Robberies.shopTill.objIndex, TRUE)
ENDPROC

/// PURPOSE:
///    Create Shop Robberies entities.
PROC CREATE_SHOP_ROBBERIES_ENTITIES()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_ENTITIES] Procedure started.")
	
	// Create clerk.
	CREATE_SHOP_ROBBERIES_CLERK()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_ENTITIES] Shop clerk created.")
	
	// Create cash bags.
	CREATE_SHOP_ROBBERIES_CASH_BAG()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_ENTITIES] Shop bag created.")
	
	CREATE_SHOP_TILL()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_ENTITIES] Shop till created.")
	
	// Now that we've created our entities, let's retain them in the shop interior.
	IF IS_VALID_INTERIOR(Shop_Robberies.shopInterior)
		RETAIN_ENTITY_IN_INTERIOR(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.shopInterior)
		RETAIN_ENTITY_IN_INTERIOR(Shop_Robberies.shopCashBag.objIndex, Shop_Robberies.shopInterior)
		IF NOT bOnAnyMission
			RETAIN_ENTITY_IN_INTERIOR(Shop_Robberies.shopTill.objIndex, Shop_Robberies.shopInterior)
		ENDIF
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->INIT_SHOP_ROBBERIES] Successfully retained entities in interior for shop #", ENUM_TO_INT(eCurrentShop))
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->INIT_SHOP_ROBBERIES] ERROR: Interior index is invalid for shop #", ENUM_TO_INT(eCurrentShop))
	ENDIF
	
	
	// Create dialog for entities.
	CREATE_SHOP_ROBBERIES_DIALOG()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->CREATE_SHOP_ROBBERIES_ENTITIES] Shop dialog created.")
ENDPROC


/// PURPOSE:
///    Check if requested assets have loaded.
FUNC BOOL HAVE_SHOP_ROBBERIES_ASSETS_LOADED()

	IF NOT HAS_LOAD_QUEUE_LIGHT_LOADED(sLoadQueue)
		RETURN FALSE
	ENDIF
	
	IF NOT bOnAnyMission
		IF NOT HAS_ANIM_DICT_LOADED(Shop_Robberies.shopClerk.szReactionAnimDict)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->HAVE_SHOP_ROBBERIES_ASSETS_LOADED] Anim dictionary ", Shop_Robberies.shopClerk.szReactionAnimDict," is not ready.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_INTERIOR_READY(Shop_Robberies.shopInterior)
		//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->HAVE_SHOP_ROBBERIES_ASSETS_LOADED] Shop interior is not ready.")
		RETURN FALSE
	ENDIF
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(FALSE)
	
	// If we made it here, all assets have loaded.
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Request needed assets, like clerk model and animations, and money bag.
PROC REQUEST_SHOP_ROBBERIES_ASSETS()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->REQUEST_SHOP_ROBBERIES_ASSETS] Procedure started.")
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	IF NOT bOnAnyMission
		REQUEST_ANIM_DICT(Shop_Robberies.shopClerk.szReactionAnimDict)
		sLoadQueue.iLastFrame = GET_FRAME_COUNT()
	ENDIF
	
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, Shop_Robberies.shopClerk.modelName)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, Shop_Robberies.shopCashBag.modelName)
	LOAD_QUEUE_LIGHT_ADD_MODEL(sLoadQueue, Shop_Robberies.shopTill.modelName)
	
	Shop_Robberies.bAnimsRequested = TRUE
ENDPROC		

/// PURPOSE:
///    Initialize Shop Robberies.  Set the player ped index, the shop index, and the clerk.
///    Set the distance check to disable shop robberies.
PROC INIT_SHOP_ROBBERIES()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->INIT_SHOP_ROBBERIES] Procedure started.")
	
	GET_SHOP_ROBBERIES_COP_DATA_BY_INDEX(eCurrentShop, Shop_Robberies.shopCops.vPositions, Shop_Robberies.shopCops.fHeadings, 
											Shop_Robberies.shopCops.vDestinationPos, Shop_Robberies.shopCops.fDestinationRadius,
											Shop_Robberies.shopCops.modelCopName, Shop_Robberies.shopCops.modelCarName,
											Shop_Robberies.shopCops.vSafeZoneArea_Min, Shop_Robberies.shopCops.vSafeZoneArea_Max, Shop_Robberies.shopCops.fSafeZoneArea_Width)
	
	GET_SHOP_ROBBERIES_SHOP_INTERIOR_BY_INDEX(eCurrentShop)
	
	//DEBUG VECTOR TWERK
	VECTOR vOrigin = <<-3244.573486,1000.657776,12.83>>
	VECTOR vPos1
	VECTOR vResult
	FLOAT fRot = 175.074
	
	vPos1 = <<-3242.007813,1001.202332,11.830711>>
	vResult = vPos1 - vOrigin
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "1st counter vpos 1 = ", vResult)
	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
	
	vPos1 = <<-3245.088379,1001.468079,13.650711>>
	vResult = vPos1 - vOrigin
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "1st counter vpos 2 = ", vResult)
	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
	
	vPos1 = << -3243.37, 1000.37, 12.83 >>
	vResult = vPos1 - vOrigin
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "1st counter  = ", vResult)
	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
	
//	vPos1 = <<-3245.442871,1003.958191,11.830711>>
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "2nd counter vpos 1 = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
//	
//	vPos1 = <<-3242.558105,1003.682922,13.650711>> 
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "2nd counter vpos 2 = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
//	
//	vPos1 = << -3243.95, 1003.88, 12.88 >>
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "2nd counter = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
//	
//	vPos1 = <<-3246.530029,1005.944763,11.830711>>
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "3rd counter vpos 1 = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
//	
//	vPos1 = <<-3246.730713,1003.896790,13.650711>> 
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "3rd counter vpos 2 = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
//	
//	vPos1 = << -3247.14, 1005.05, 12.94 >>
//	vResult = vPos1 - vOrigin
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "3rd counter = ", vResult)
//	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
//	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
	
	//TODO quick hack
	Shop_Robberies.iUseContext = NEW_CONTEXT_INTENTION
	Shop_Robberies.shopSnacks.iCurrentLocate = -1
	Shop_Robberies.shopTill.modelName = P_TILL_01_S
	Shop_Robberies.shopTill.vPos = Shop_Robberies.cashRegisterSynchedScene.vPos
	Shop_Robberies.shopTill.fHeading = Shop_Robberies.cashRegisterSynchedScene.vRotation.z + 180.0
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "&&&&&&[ShopRobberies.sch->INIT_SHOP_ROBBERIES] Shop_Robberies.shopTill.vPos     = ", Shop_Robberies.shopTill.vPos)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "&&&&&&[ShopRobberies.sch->INIT_SHOP_ROBBERIES] Shop_Robberies.shopTill.fHeading = ", Shop_Robberies.shopTill.fHeading)
	
	// Set up player aiming values.
	Shop_Robberies.aggroArgs.fAimTime = 0
	
	// Set what weapon the player is holding.
	Shop_Robberies.bIsPlayerHoldingRocketLauncherAtHoldUp 	= FALSE
	Shop_Robberies.bIsPlayerHoldingKnifeAtHoldUp 			= FALSE
	Shop_Robberies.bPlayerIsHoldingToolAtHoldUp 			= FALSE
	Shop_Robberies.bPlayerIsHoldingThrowAtHoldUp 			= FALSE
	Shop_Robberies.bPlayerIsHoldingStickyAtHoldUp 			= FALSE
	
	// The player hasn't used the Jerrycan in front of the clerk yet.
	Shop_Robberies.bHasPlayerPouredJerryCan					= FALSE
	
	// Initialize where the clerk will cower after being threatened.
	GET_SHOP_CLERK_BACKROOM_COWER_DATA(eCurrentShop, Shop_Robberies.shopClerk.vBackRoomCowerPos, Shop_Robberies.shopClerk.fBackRoomCowerHead)
	
	// Init shop patron if necessary.
	INIT_SHOP_ROBBERIES_PATRON()
ENDPROC


/// PURPOSE:
///    Loop for setting up Shop Robberies.
PROC SETUP_SHOP_ROBBERIES()
	#IF IS_DEBUG_BUILD
//	VECTOR vShelfPos = << -713.74, -913.31, 18.94 >>
//	VECTOR vPos1 = <<-714.125671,-914.352295,18.215599>>
//	VECTOR vPos2 = <<-712.702576,-912.938660,20.215599>> 
//	VECTOR vOffsetPos1, vOffsetPos2
	#ENDIF
	
	SWITCH(eCurrentShopRobberiesSetupState)
		// Before initializing anything, wait until player is closer to the shop.
		CASE SETUP_SHOP_ROBBERIES_WAIT_UNTIL_PLAYER_IS_CLOSE
			IF IS_PLAYER_CLOSE_TO_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] Setup: Jumping from SETUP_SHOP_ROBBERIES_WAIT_UNTIL_PLAYER_IS_CLOSE to SETUP_SHOP_ROBBERIES_INIT.")
				eCurrentShopRobberiesSetupState = SETUP_SHOP_ROBBERIES_INIT
			ENDIF
		BREAK
	
		// Initialize Shop Robberies.
		CASE SETUP_SHOP_ROBBERIES_INIT
			INIT_SHOP_ROBBERIES()
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] Setup: Jumping from SETUP_SHOP_ROBBERIES_INIT to SETUP_SHOP_ROBBERIES_REQUEST_ASSETS.")
			eCurrentShopRobberiesSetupState = SETUP_SHOP_ROBBERIES_REQUEST_ASSETS
			
			// DEBUG AREA
			#IF IS_DEBUG_BUILD
//			
//			vOffsetPos1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS() GET_OFFSET_FROM_COORD_IN_WORLD_COORDS()
			#ENDIF
			
		BREAK
		
		
		// Request needed assets.
		CASE SETUP_SHOP_ROBBERIES_REQUEST_ASSETS
			REQUEST_SHOP_ROBBERIES_ASSETS()
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] Setup: Jumping from SETUP_SHOP_ROBBERIES_REQUEST_ASSETS to SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS.")
			eCurrentShopRobberiesSetupState = SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS
		BREAK
		
		
		// Wait for assets to load.
		CASE SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS
			IF HAVE_SHOP_ROBBERIES_ASSETS_LOADED()
			
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] CASE SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS: Assets finished loading.")
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] Setup: Jumping from SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS to SETUP_SHOP_ROBBERIES_CREATE_ENTITIES")
				eCurrentShopRobberiesSetupState = SETUP_SHOP_ROBBERIES_CREATE_ENTITIES				
			ENDIF
		BREAK
		
		// Create entities.
		CASE SETUP_SHOP_ROBBERIES_CREATE_ENTITIES
			CREATE_SHOP_ROBBERIES_ENTITIES()
			
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SETUP_SHOP_ROBBERIES] Jumping from Setup: SETUP_SHOP_ROBBERIES_WAIT_FOR_ASSETS to Main: SHOP_ROBBERIES_UPDATE")
			eCurrentShopRobberiesState = SHOP_ROBBERIES_UPDATE
		BREAK
	ENDSWITCH
ENDPROC

// -------------------------------------------------
// 	E N D  ROBBERIES SETUP PROCEDURES AND FUNCTIONS
// -------------------------------------------------





// -------------------------------------------------
// 	ROBBERIES UPDATE PROCEDURES AND FUNCTIONS
// -------------------------------------------------

/// PURPOSE:
///    Update number of times this shopped has been robbed.
///    This can be updated only once per script run.
PROC UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED
	IF NOT bHasNumOfTimesShopRobbedUpdated
		// The player is holding up the shop.  Save the number of times the player has done this.
		g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed[eCurrentShop]++
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED] Updating number of times this shops has been robbed to ", 
					g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed[eCurrentShop])
					
		// Update the last player character that has robbed the shop.
		g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[eCurrentShop] = Shop_Robberies.iPlayerCharacterIndex
		
		// Collecting data for Shrink.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_SHOP_ROBBERY)
		ENDIF
		
		bHasNumOfTimesShopRobbedUpdated = TRUE
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED] Trying to update number of times this shop has been robbed, but this has already been updated in this script run to ", 
			g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed[eCurrentShop])
	ENDIF
ENDPROC

/// PURPOSE:
///    Update dialog state.
PROC UPDATE_PRINT_HELPS()
	SWITCH(eCurrentPrintEnum)
		CASE PRINT_HELP_WAIT_FOR_PLAYER_TO_ENTER_STORE
		BREAK
		
		CASE PRINT_HELP_HOLDUP_STORE
			IF NOT IS_SELECTOR_CAM_ACTIVE()
				
				// New help check
				IF IS_PLAYER_INSIDE_SHOP()
					IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_ROB)
						SWITCH GET_FLOW_HELP_MESSAGE_STATUS("SHR_HOLDUP_1")

							CASE FHS_EXPIRED
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : adding help to queue ")
								ADD_HELP_TO_FLOW_QUEUE("SHR_HOLDUP_1", FHP_HIGH)
								BREAK
							CASE FHS_DISPLAYED
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : queue was displayed ")
								SET_BITMASK_AS_ENUM(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_ROB)
								eCurrentPrintEnum = PRINT_HELP_BUY_SNACKS
								BREAK
						ENDSWITCH
						
						IF eCurrentShopRobberiesHoldUpState > SHOP_ROBBERIES_HOLDUP_STATE_BEFORE
							IF IS_FLOW_HELP_MESSAGE_QUEUED("SHR_HOLDUP_1")
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : player started holdup before help displayed, clearing from the queue ")
								REMOVE_HELP_FROM_FLOW_QUEUE("SHR_HOLDUP_1")
								eCurrentPrintEnum = PRINT_HELP_DONE
							ENDIF
						ENDIF
						
						IF (GET_GAME_TIMER() % 1000) < 50
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : help queue status : ", GET_FLOW_HELP_MESSAGE_STATUS("SHR_HOLDUP_1"))
						ENDIF
						
					ELSE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : help was already shown before, going to help_done ")
						eCurrentPrintEnum = PRINT_HELP_BUY_SNACKS
					ENDIF
				ELSE
					IF IS_FLOW_HELP_MESSAGE_QUEUED("SHR_HOLDUP_1")
						REMOVE_HELP_FROM_FLOW_QUEUE("SHR_HOLDUP_1")
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE PRINT_HELP_BUY_SNACKS
			IF NOT IS_SELECTOR_CAM_ACTIVE()
				
				// New help check
				IF IS_PLAYER_INSIDE_SHOP()
					IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_SNACK)
						SWITCH GET_FLOW_HELP_MESSAGE_STATUS("SHR_SNK_TUT")

							CASE FHS_EXPIRED
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : adding help to queue ")
								ADD_HELP_TO_FLOW_QUEUE("SHR_SNK_TUT", FHP_HIGH)
								BREAK
							CASE FHS_DISPLAYED
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : queue was displayed ")
								SET_BITMASK_AS_ENUM(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_SNACK)
								eCurrentPrintEnum = PRINT_HELP_DONE
								BREAK
						ENDSWITCH
					ELSE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : help was already shown before, going to help_done ")
						eCurrentPrintEnum = PRINT_HELP_DONE
					ENDIF
				ELSE
					IF IS_FLOW_HELP_MESSAGE_QUEUED("SHR_SNK_TUT")
						REMOVE_HELP_FROM_FLOW_QUEUE("SHR_SNK_TUT")
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_HOLDUP_1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_SNK_TUT")
				CLEAR_HELP(TRUE)
			ENDIF
			
			IF IS_FLOW_HELP_MESSAGE_QUEUED("SHR_SNK_TUT")
			AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_SNACK)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : clearing snk tut from the queue because of store clear print ")
				REMOVE_HELP_FROM_FLOW_QUEUE("SHR_SNK_TUT")
			ENDIF
			
			IF IS_FLOW_HELP_MESSAGE_QUEUED("SHR_HOLDUP_1")
			AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_ROB)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->update helps] : clearing hold up 1 from the queue because of store clear print ")
				REMOVE_HELP_FROM_FLOW_QUEUE("SHR_HOLDUP_1")
			ENDIF
			
			eCurrentPrintEnum = PRINT_HELP_DONE
		BREAK
		
		CASE PRINT_HELP_DONE
			IF NOT IS_PLAYER_INSIDE_SHOP()
			AND IS_FLOW_HELP_MESSAGE_QUEUED("SHR_HOLDUP_1")
			AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_ROB)
				REMOVE_HELP_FROM_FLOW_QUEUE("SHR_HOLDUP_1")
				eCurrentPrintEnum = PRINT_HELP_HOLDUP_STORE
			ENDIF
			
			IF NOT IS_PLAYER_INSIDE_SHOP()
			AND IS_FLOW_HELP_MESSAGE_QUEUED("SHR_SNK_TUT")
			AND NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, SHOPROB_BOOL_FIRST_SHOP_SNACK)
				REMOVE_HELP_FROM_FLOW_QUEUE("SHR_SNK_TUT")
				eCurrentPrintEnum = PRINT_HELP_HOLDUP_STORE
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Update dialog state.
PROC UPDATE_DIALOG()
	// Stop the clerk and player from playing default dialog inside store.
	IF IS_PLAYER_INSIDE_SHOP()
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
			//STOP_CURRENT_PLAYING_AMBIENT_SPEECH(Shop_Robberies.pedPlayer)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		//STOP_CURRENT_PLAYING_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex)
	ENDIF


	SWITCH(eCurrentDialogState)
		CASE DIALOG_STATE_CLERK_GREET_WHEN_PLAYER_ENTERS
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
//				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(Shop_Robberies.shopClerk.pedIndex, "SHOP_GREET", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE")
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_GREET", SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Greet, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_GREET_WHEN_PLAYER_ENTERS: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkGreetsPlayer, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, PICK_STRING(bIsClerk247, "SHOP_THREATENED", PICK_STRING(GET_RANDOM_BOOL(), "SHOP_NO_COPS_START", "SHOP_NO_COPS_END")), SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_BatGreet, 
//													Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBat, " dialog line.")		
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, PICK_STRING(bIsClerk247, "SHOP_THREATENED", PICK_STRING(GET_RANDOM_BOOL(), "SHOP_NO_COPS_START", "SHOP_NO_COPS_END")), SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_ToolGreet, 
//													Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToTool, " dialog line.")		
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIP
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, PICK_STRING(bIsClerk247, "SHOP_THREATENED", PICK_STRING(GET_RANDOM_BOOL(), "SHOP_NO_COPS_START", "SHOP_NO_COPS_END")), SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_StickyGreet, 
//													Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIPs: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToSticky, " dialog line.")		
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		
		CASE DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, PICK_STRING(bIsClerk247, "SHOP_THREATENED", PICK_STRING(GET_RANDOM_BOOL(), "SHOP_NO_COPS_START", "SHOP_NO_COPS_END")), SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Worry, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_SHOCKED_WHEN_PLAYER_ENTERS
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, PICK_STRING(bIsClerk247, "SHOP_THREATENED", PICK_STRING(GET_RANDOM_BOOL(), "SHOP_NO_COPS_START", "SHOP_NO_COPS_END")), SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Shock, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_SHOCKED_WHEN_PLAYER_ENTERS: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkShockedAtPlayer, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_REFUSES_TO_HOLDUP
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_STUBBORN", SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Stubborn, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REFUSES_TO_HOLDUP: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkStubbornAtPlayer, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_COUNTER_WARNING
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_NO_ENTRY", SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Counter, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_COUNTER_WARNING: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkCounterWarning, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_BACKROOM_WARNING
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_NO_ENTRY", SPEECH_PARAMS_FORCE)
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_BackRoom, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_BACKROOM_WARNING: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkBackroomWarning, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_WAIT_FOR_HOLDUP
		BREAK
		
		CASE DIALOG_STATE_CLERK_THREATENED
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_THREATENED", SPEECH_PARAMS_FORCE) 
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_THREATENED: Playing SHOP_THREATENED dialog line.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF		
		BREAK
		
		CASE DIALOG_STATE_PLAYER_BUMPS_CLERK_BEFORE_HOLDUP
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "BUMP", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Bump, 
//													Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
													
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_PLAYER_BUMPS_CLERK_BEFORE_HOLDUP: Playing ", Shop_Robberies.dialog.szSpecificLabel_PlayerBumpsClerk, " dialog line.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ELSE
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_SCARED
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_BumpAimScared, 
//													Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
													
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_SCARED: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkScaredFromBump, " dialog line.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF			
		BREAK
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_AGGRO
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_BRAVE", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_BumpAimAggro, 
//													Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
													
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_AGGRO: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkAggroFromBump, " dialog line.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF				
		BREAK
		
		CASE DIALOG_STATE_WAIT_FOR_HOLDUP
			IF IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()
			OR IS_PLAYER_INSIDE_BACKROOM_AREA_OF_SHOP()
				IF IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()
					IF NOT bHasCounterWarningDialogPlayed
						
						eCurrentDialogState = DIALOG_STATE_CLERK_COUNTER_WARNING
						bHasCounterWarningDialogPlayed = TRUE
					ENDIF
				ELIF IS_PLAYER_INSIDE_BACKROOM_AREA_OF_SHOP()
					IF NOT bHasBackroomWarningDialogPlayed
						
						eCurrentDialogState = DIALOG_STATE_CLERK_BACKROOM_WARNING
						bHasBackroomWarningDialogPlayed = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_TIMER_STARTED(Shop_Robberies.warningTimer)
					START_TIMER_NOW(Shop_Robberies.warningTimer)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] starting warning timer.")
				ELIF IS_TIMER_PAUSED(Shop_Robberies.warningTimer)
					UNPAUSE_TIMER(Shop_Robberies.warningTimer)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] unpausing warning timer.")
				ELIF GET_TIMER_IN_SECONDS(Shop_Robberies.warningTimer) > 10
					IF NOT IS_BITMASK_AS_ENUM_SET( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
						IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
						AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
							TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex, "misscommon@response", "numbnuts")
						ENDIF
						
						eCurrentDialogState = DIALOG_STATE_CLERK_THREATENED
						
						SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
						
						SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
					ENDIF
				ENDIF
			ELSE
				
				IF IS_TIMER_STARTED(Shop_Robberies.warningTimer)
				AND NOT IS_TIMER_PAUSED(Shop_Robberies.warningTimer)
					PAUSE_TIMER(Shop_Robberies.warningTimer)
				ENDIF
				
			ENDIF
				
//			IF IS_PLAYER_INSIDE_BACKROOM_AREA_OF_SHOP()
//				IF NOT bHasBackroomWarningDialogPlayed
//					
//					eCurrentDialogState = DIALOG_STATE_CLERK_BACKROOM_WARNING
//					bHasBackroomWarningDialogPlayed = TRUE
//				ENDIF
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_STARTS
//			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//				CLEAR_PRINTS()
//				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
//			ENDIF
			
			IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "SHOP_HOLDUP", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] player's holdup line called")
				ENDIF
				
				RESTART_TIMER_NOW(Shop_Robberies.timerReactDelay)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_STARTS: Playing ", Shop_Robberies.dialog.szSpecificLabel_PlayerHoldup, " dialog line.")
				IF Shop_Robberies.shopClerk.bIsStubborn
					eCurrentDialogState = DIALOG_STATE_DONE // stubborn dialogue is triggered elsewhere, just go to done here
				ELSE
					eCurrentDialogState = DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION
				ENDIF
			ELSE
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
			ENDIF
			
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_TIMER_IN_SECONDS_SAFE(Shop_Robberies.timerReactDelay) > 3.0
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_STARTS: Player is no longer playing dialog.  Move on to playing clerk dialog.")
				IF Shop_Robberies.shopClerk.bIsGoingToAttack
					eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_ATTACKS
				ELSE
					IF Shop_Robberies.bIsPlayerHoldingRocketLauncherAtHoldUp
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS_BAZOOKA
					ELIF Shop_Robberies.bIsPlayerHoldingKnifeAtHoldUp
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS_KNIFE
					ELIF Shop_Robberies.bPlayerIsHoldingToolAtHoldUp
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS_TOOL_OR_BAT
					ELIF Shop_Robberies.bPlayerIsHoldingThrowAtHoldUp
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS_THROW
					ELIF Shop_Robberies.bPlayerIsHoldingStickyAtHoldUp
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS_STICKY
					ELSE
						eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS
					ENDIF
				ENDIF
				
				CANCEL_TIMER(Shop_Robberies.timerReactDelay)
			ENDIF
//			ENDIF
		BREAK

		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) 
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Scared, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkTakesMoney, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS_BAZOOKA
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) // ** not playing SHOP_SCARED_HIGH
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Scared, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS_BAZOOKA: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToBazooka, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS_KNIFE
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) 
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_KnifeReaction, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS_KNIFE: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToKnifeAim, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS_TOOL_OR_BAT
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) 
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_ToolReaction, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS_TOOL_OR_BAT: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToToolAim, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS_THROW
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) // ** not playing SHOP_SCARED_HIGH
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_ThrowReaction, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS_THROW: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToThrowAim, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK
		
		CASE DIALOG_STATE_HOLDUP_CLERK_OBEYS_STICKY
			IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SCARED", SPEECH_PARAMS_FORCE) // ** not playing SHOP_SCARED_HIGH
			ENDIF
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_StickyReaction, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_OBEYS_STICKY: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToStickyAim, " dialog line.")
			eCurrentDialogState = DIALOG_STATE_DONE
		BREAK			
		
		CASE DIALOG_STATE_HOLDUP_CLERK_ATTACKS
			IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_BRAVE", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL) 
				ENDIF
				SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CLERK_AGGRO_SAID)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_HOLDUP_CLERK_ATTACKS: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkPullsGun, " dialog line.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP
			IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				IF Shop_Robberies.shopCashBag.bBagWithCashCreated
					IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
						PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_GAVE_YOU_EVERYTHING", SPEECH_PARAMS_FORCE) 
					ENDIF
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkHeldupAgain, " dialog line.")
				ENDIF
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ELSE
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF IS_VEHICLE_DRIVEABLE(Shop_Robberies.shopCops.vehCars[0])
					SET_VEHICLE_SIREN(Shop_Robberies.shopCops.vehCars[0], FALSE)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[0])
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopCops.pedCops[0], "SURROUNDED", SPEECH_PARAMS_FORCE_MEGAPHONE) 
				ENDIF
				
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Surrender, 
//									Shop_Robberies.dialog.szSpecificLabel_CopsSurrenderWarning, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER: Playing ", Shop_Robberies.dialog.szSpecificLabel_CopsSurrenderWarning, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER to DIALOG_STATE_PLAYER_SEES_COPS.")
				eCurrentDialogState = DIALOG_STATE_PLAYER_SEES_COPS
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_PLAYER_SEES_COPS
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "SPOT_POLICE", SPEECH_PARAMS_FORCE)  // ** NOT PLAYING
				ENDIF
				
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Cops, 
//									Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_PLAYER_SEES_COPS: Playing ", Shop_Robberies.dialog.szSpecificLabel_PlayerSeesCops, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] Jumping from DIALOG_STATE_PLAYER_SEES_COPS to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_POUR_CAN", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_JerryCan, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerrycan, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkReactsToJerrycan, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] Jumping from DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
//			ENDIF			
		BREAK
		
		CASE DIALOG_STATE_CLERK_FLEES
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SCREAM_PANIC", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Flee, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkFlees, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_FLEES: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkFlees, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_FLEES to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_RECOGNISE", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_RecognizeScared, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesScared, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
//			ELSE
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_RECOGNISE", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL) 
				ENDIF
				
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_RecognizeAggro, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro, CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
//			ELSE
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			ENDIF
		BREAK			
		
		CASE DIALOG_STATE_CLERK_REACTS_PLAYER_ALREADY_WANTED
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				 
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					//PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_WORRY", SPEECH_PARAMS_FORCE)  //*** NOT PLAYING
				PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_NO_COPS", SPEECH_PARAMS_FORCE)
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Worry, 
//												Shop_Robberies.dialog.szSpecificLabel_ClerkWorriesAtPlayer, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_REACTS_PLAYER_ALREADY_WANTED: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkRecognizesAggro, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_REACTS_PLAYER_ALREADY_WANTED to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_PATRON_REACTS_TO_HOLDUP
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Patron, 
//									Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_PATRON_REACTS_TO_HOLDUP: Playing ", Shop_Robberies.dialog.szSpecificLabel_PatronReactsToHoldup, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_PATRON_REACTS_TO_HOLDUP to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_PLAYER_HURRY_UP
			IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
			AND NOT IS_AMBIENT_SPEECH_PLAYING(Shop_Robberies.shopClerk.pedIndex)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "SHOP_HURRY", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL) // ** NOT PLAYING
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_Hurry,
//									Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_PLAYER_HURRY_UP: Playing ", Shop_Robberies.dialog.szSpecificLabel_PlayerHurryUp, " dialog line.")
				
				RESTART_TIMER_NOW(Shop_Robberies.timerReactDelay)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_PLAYER_HURRY_UP to DIALOG_STATE_CLERK_MID_HOLDUP.")
				eCurrentDialogState = DIALOG_STATE_CLERK_MID_HOLDUP
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] Can't say hurry up line yet, someone is saying an ambient speech line")
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_MID_HOLDUP
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_TIMER_IN_SECONDS_SAFE(Shop_Robberies.timerReactDelay) > 1.0
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_HURRYING", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_MidHoldup, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkMidHoldup, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_MID_HOLDUP: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkMidHoldup, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_MID_HOLDUP to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF
//			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_BUY_SNACK_REACTION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_SELL", SPEECH_PARAMS_FORCE)
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_BuySnack, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkBuySnackReact, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_BUY_SNACK_REACTION: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkBuySnackReact, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_BUY_SNACK_REACTION to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF
		BREAK
		
		CASE DIALOG_STATE_CLERK_STEAL_SNACK_REACTION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
					PLAY_PED_AMBIENT_SPEECH(Shop_Robberies.shopClerk.pedIndex, "SHOP_STEAL", SPEECH_PARAMS_FORCE) 
				ENDIF
//				PLAY_SINGLE_LINE_FROM_CONVERSATION(Shop_Robberies.dialog.convoStruct, Shop_Robberies.dialog.szBlockOfText, Shop_Robberies.dialog.szRootLabel_StealSnack, 
//									Shop_Robberies.dialog.szSpecificLabel_ClerkStealSnackReact, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] DIALOG_STATE_CLERK_STEAL_SNACK_REACTION: Playing ", Shop_Robberies.dialog.szSpecificLabel_ClerkStealSnackReact, " dialog line.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_DIALOG] From DIALOG_STATE_CLERK_STEAL_SNACK_REACTION to DIALOG_STATE_DONE.")
				eCurrentDialogState = DIALOG_STATE_DONE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

OBJECT_INDEX oiBoughtItem[3]

FUNC MODEL_NAMES GET_SNACK_MODEL(INT iWhichSnack)
	SWITCH iWhichSnack
		CASE 0		RETURN PROP_CHOC_PQ
		CASE 1		RETURN PROP_CHOC_EGO
		CASE 2		RETURN PROP_CHOC_METO
		DEFAULT 	RETURN PROP_CHOC_PQ
	ENDSWITCH
ENDFUNC

FUNC VECTOR GET_SNACK_INIT_POS(INT iWhichSnack)
	SWITCH iWhichSnack
		CASE 0		RETURN Shop_Robberies.cashRegisterSynchedScene.vPos - << 0.1, 0, 1.0>>
		CASE 1		RETURN Shop_Robberies.cashRegisterSynchedScene.vPos - << 0, 0, 1.0>>
		CASE 2		RETURN Shop_Robberies.cashRegisterSynchedScene.vPos - << -0.1, 0, 1.0>>
		DEFAULT 	RETURN Shop_Robberies.cashRegisterSynchedScene.vPos - << -0.1, 0, 1.0>>
	ENDSWITCH
ENDFUNC

ENUM DYNAMIC_REQUESTS_STATE
	DR_ENTER_SHOP,
	DR_REQ_LOADED,
	DR_EXIT_SHOP,
	DR_HARD_RELEASE
ENDENUM
DYNAMIC_REQUESTS_STATE eDynamicRequestState = DR_ENTER_SHOP
DYNAMIC_REQUESTS_STATE eDynamicHoldUpRequestState = DR_ENTER_SHOP


PROC CLEANUP_FOR_ON_MISSION()
	CPRINTLN(DEBUG_SHOP_ROBBERIES, "Running on-mission cleanup. We've gone on-mission while a shop robbery is running.")

	IF Shop_Robberies.shopCops.modelCopName != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(Shop_Robberies.shopCops.modelCopName)
	ENDIF
	IF Shop_Robberies.shopCops.modelCarName != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(Shop_Robberies.shopCops.modelCarName)
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(Shop_Robberies.shopClerk.szReactionAnimDict)
		REMOVE_ANIM_DICT(Shop_Robberies.shopClerk.szReactionAnimDict)
	ENDIF
ENDPROC



PROC CLEANUP_DYNAMIC_SHOP_ROB_REQUESTS()

	REMOVE_ANIM_DICT("oddjobs@shop_robbery@rob_till")
	REMOVE_ANIM_DICT("misscommon@response")
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\MARKET_CASH_REGISTER")
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_SHOP_HEADER_TEXTURE_STRING(eCurrentShop))
	
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CHOC_EGO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CHOC_METO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CHOC_PQ)
	
	CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(eCurrentShop))
	
	IF NOT bOnAnyMission
		SET_MODEL_AS_NO_LONGER_NEEDED(Shop_Robberies.shopCops.modelCopName)
		SET_MODEL_AS_NO_LONGER_NEEDED(Shop_Robberies.shopCops.modelCarName)
	ENDIF
ENDPROC



PROC UPDATE_DYNAMIC_HOLDUP_ANIM_REQUEST()
	SWITCH eDynamicHoldUpRequestState
		CASE DR_ENTER_SHOP
			IF IS_PLAYER_INSIDE_SHOP()
				
				REQUEST_ANIM_DICT(Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic hold up requests] requests made.")
				eDynamicHoldUpRequestState = DR_REQ_LOADED
			ENDIF
		BREAK
		
		CASE DR_REQ_LOADED
			
			IF NOT HAS_ANIM_DICT_LOADED(Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic hold up requests] Anim dictionary ", Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict," is not ready.")
				EXIT
				//RETURN FALSE
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
				SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic hold up requests] requests done, going to _exit_shop")
				eDynamicHoldUpRequestState = DR_EXIT_SHOP
			ENDIF
		BREAK
		
		CASE DR_EXIT_SHOP
			IF NOT IS_PLAYER_INSIDE_SHOP()
			AND eCurrentShopRobberiesHoldUpState != SHOP_ROBBERIES_HOLDUP_STATE_ON
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic hold up requests] cleaning up requests, player is out of shop")
				
				REMOVE_ANIM_DICT(Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict)
				
				CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
				
				eDynamicHoldUpRequestState = DR_ENTER_SHOP
			ENDIF
		BREAK
		
		CASE DR_HARD_RELEASE
			IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic hold up requests] assets were hard released!  they won't be requested again")
				
				REMOVE_ANIM_DICT(Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict)
				
				CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_DYNAMIC_SHOP_ROB_REQUESTS()
	
	SWITCH eDynamicRequestState
		CASE DR_ENTER_SHOP
			IF IS_PLAYER_INSIDE_SHOP()
				
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				
				REQUEST_ANIM_DICT("oddjobs@shop_robbery@rob_till")
				REQUEST_ANIM_DICT("misscommon@response")
				
				REQUEST_STREAMED_TEXTURE_DICT(GET_SHOP_HEADER_TEXTURE_STRING(eCurrentShop))
				
				REQUEST_MODEL(PROP_CHOC_EGO)
				REQUEST_MODEL(PROP_CHOC_METO)
				REQUEST_MODEL(PROP_CHOC_PQ)
				
				IF NOT bOnAnyMission
					REQUEST_MODEL(Shop_Robberies.shopCops.modelCopName)
					REQUEST_MODEL(Shop_Robberies.shopCops.modelCarName)
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] requests made.")
				eDynamicRequestState = DR_REQ_LOADED
			ENDIF
		BREAK
		
		CASE DR_REQ_LOADED
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Procedure started.")
			
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\MARKET_CASH_REGISTER")	
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "MARKET_CASH_REGISTER Audio loading")
				EXIT
				//RETURN FALSE
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED("oddjobs@shop_robbery@rob_till")
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Anim dictionary oddjobs@shop_robbery@rob_till is not ready.")
				EXIT
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED("misscommon@response")
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Anim dictionary misscommon@response is not ready.")
				EXIT
			ENDIF
			
			IF NOT HAS_MODEL_LOADED(PROP_CHOC_EGO)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Model PROP_CHOC_EGO is not ready.")
				EXIT
			ENDIF
			
			IF NOT HAS_MODEL_LOADED(PROP_CHOC_METO)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Model PROP_CHOC_METO is not ready.")
				EXIT
			ENDIF
			
			IF NOT HAS_MODEL_LOADED(PROP_CHOC_PQ)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] Model PROP_CHOC_PQ is not ready.")
				EXIT
			ENDIF
			
			IF NOT bOnAnyMission
				IF NOT HAS_MODEL_LOADED(Shop_Robberies.shopCops.modelCopName)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->dynamic requests] Cop ped model has not loaded.")
					EXIT
				ENDIF
				
				IF NOT HAS_MODEL_LOADED(Shop_Robberies.shopCops.modelCarName)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->dynamic requests] Cop car model has not loaded.")
					EXIT
				ENDIF
			ENDIF
			
			IF NOT LOAD_MENU_ASSETS("SNK_MNU", ENUM_TO_INT(eCurrentShop))
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch->dynamic requests] menu assets has not loaded.")
				EXIT
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
				IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_ITEMS_CREATED)
					oiBoughtItem[0] = CREATE_OBJECT(GET_SNACK_MODEL(0), GET_SNACK_INIT_POS(0))
					oiBoughtItem[1] = CREATE_OBJECT(GET_SNACK_MODEL(1), GET_SNACK_INIT_POS(1))
					oiBoughtItem[2] = CREATE_OBJECT(GET_SNACK_MODEL(2), GET_SNACK_INIT_POS(2))
					
					SET_ENTITY_VISIBLE(oiBoughtItem[0], FALSE)
					SET_ENTITY_VISIBLE(oiBoughtItem[1], FALSE)
					SET_ENTITY_VISIBLE(oiBoughtItem[2], FALSE)
					SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_ITEMS_CREATED)
				ENDIF
				
				SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] requests done, going to _exit_shop")
				eDynamicRequestState = DR_EXIT_SHOP
			ENDIF
		BREAK
		
		CASE DR_EXIT_SHOP
			IF NOT IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] cleaning up requests, player is out of shop")
				
				CLEANUP_DYNAMIC_SHOP_ROB_REQUESTS()
				
				CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
				
				eDynamicRequestState = DR_ENTER_SHOP
			ENDIF
		BREAK
		
		CASE DR_HARD_RELEASE
			IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sch-> dynamic requests] assets were hard released!  they won't be requested again")
				
				CLEANUP_DYNAMIC_SHOP_ROB_REQUESTS()
				
				CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


ENUM PLAYER_ROB_TILL_STATE
	PLAYERROBTILL_INIT,
	PLAYERROBTILL_WAIT_ACTIVATE,
	PLAYERROBTILL_QUICKCUT,
	PLAYERROBTILL_QUICKCUT_INTERP,
	PLAYERROBTILL_INTERP_FINISH,
	PLAYERROBTILL_CLEANUP,
	PLAYERROBTILL_SKIP,
	PLAYERROBTILL_DONE
ENDENUM
PLAYER_ROB_TILL_STATE ePlayerRobTillState = PLAYERROBTILL_INIT

CAMERA_INDEX ciTill
CAMERA_INDEX ciTillInterp
BOOL bMoneyFlag
WEAPON_TYPE eCurrentWeapon
INT iTillCamTimer

PROC UPDATE_PLAYER_ROBBING_TILL()
	
	FLOAT fAnimPhase
	INT iCashGrabbed
	BOOL bHasWeapon
	
	VEHICLE_INDEX vehVehicleToCheck
		
	IF ePlayerRobTillState > PLAYERROBTILL_WAIT_ACTIVATE
	AND ePlayerRobTillState < PLAYERROBTILL_CLEANUP
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			DO_SCREEN_FADE_OUT(500)
			
			ePlayerRobTillState = PLAYERROBTILL_SKIP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES,"CUTSCENE PLAYERROBTILL_SKIP")
		ENDIF
	ENDIF
	
	SWITCH ePlayerRobTillState
		CASE PLAYERROBTILL_INIT
			// let's have a check if the clerk is incapacitated and the player has not collected any money here
			IF (IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex) OR NOT IS_CLERK_INSIDE_SHOP() OR IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_CLERK_RUN_COWER))  //Shop_Robberies.shopClerk.bLeftShop)
			AND (eCurrentCashBagState < CASH_BAG_STATE_CREATE_PICKUP)
			AND GET_NUMBER_OF_FIRES_IN_RANGE(Shop_Robberies.shopClerk.vPos, 1.5) = 0
				IF IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PHONE_ONSCREEN_AND_RINGING()	
					
					PRINT_HELP("SHR_ROBTILL")
					
					Shop_Robberies.iUseContext = NEW_CONTEXT_INTENTION
					REGISTER_CONTEXT_INTENTION(Shop_Robberies.iUseContext, CP_HIGH_PRIORITY, "SHR_ROBTILL")
					
					CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
					CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
					
					ePlayerRobTillState = PLAYERROBTILL_WAIT_ACTIVATE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping to PLAYERROBTILL_WAIT_ACTIVATE")
				ENDIF
			ELIF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Clerk not injured yet")
			
			ELIF (eCurrentCashBagState = CASH_BAG_STATE_DONE)
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] eCurrentCashBagState = CASH_BAG_STATE_DONE")
			
			ELIF GET_NUMBER_OF_FIRES_IN_RANGE(Shop_Robberies.shopClerk.vPos, 1.5) > 0
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] fires in range = ", GET_NUMBER_OF_FIRES_IN_RANGE(Shop_Robberies.shopClerk.vPos, 1.5))
			
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_WAIT_ACTIVATE
			IF IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()
//					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
//					AND NOT IS_PHONE_ONSCREEN_AND_RINGING()
				IF HAS_CONTEXT_BUTTON_TRIGGERED(Shop_Robberies.iUseContext)
					
					
					IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->IS_PLAYER_VEHICLE_INSIDE_SHOP] Player is not inside vehicle, but his last vehicle is well.  Get vehicle and check if vehicle is in the shop")
							vehVehicleToCheck = GET_PLAYERS_LAST_VEHICLE()
							IF IS_ENTITY_IN_ANGLED_AREA(vehVehicleToCheck, Shop_Robberies.vInteriorCounterArea_Min, Shop_Robberies.vInteriorCounterArea_Max, Shop_Robberies.fInteriorCounterArea_Width)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc-> player's vehicle is behind the counter")
								SET_ENTITY_COORDS(vehVehicleToCheck, 
													GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(Shop_Robberies.cashRegisterSynchedScene.vPos, Shop_Robberies.cashRegisterSynchedScene.vRotation.z, <<-0.7961, -1.5620, -0.4215>>), FALSE)
								
							ENDIF
						ENDIF 	
					ENDIF
					
					
					SET_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "set buddy hide bit in rob till")
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_ROBTILL")
						CLEAR_HELP()
					ENDIF
					
					RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
					
					bHasWeapon = GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
					
					IF bHasWeapon
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Player has weapon, will swap it out")
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
					
					// award money
					//iCashGrabbed = GET_RANDOM_INT_IN_RANGE(100, 300)
					
					//ODDJOB_ENTER_CUTSCENE() //can't use wrapper because we have to display the money hud
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
					STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 15)
					SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)
					HANG_UP_AND_PUT_AWAY_PHONE()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)		
					DISPLAY_RADAR(FALSE)
					
					DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
					
					CLEAR_AREA_OF_PROJECTILES(Shop_Robberies.shopClerk.vPos, 3.0)
					
					ePlayerRobTillState = PLAYERROBTILL_QUICKCUT
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping from PLAYERROBTILL_WAIT_ACTIVATE to PLAYERROBTILL_QUICKCUT")
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> update robbing till] player hasn't triggered robbery yet")
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> update robbing till] use context is currently ", Shop_Robberies.iUseContext)
						IF Shop_Robberies.iUseContext <= 0
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> update robbing till] re registering use context!")
							Shop_Robberies.iUseContext = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(Shop_Robberies.iUseContext, CP_HIGH_PRIORITY, "SHR_ROBTILL")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_ROBTILL")
					CLEAR_HELP()
				ENDIF
				
				RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping back to PLAYERROBTILL_INIT")
				ePlayerRobTillState = PLAYERROBTILL_INIT
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_QUICKCUT
			
			IF IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				SET_ENTITY_COLLISION(Shop_Robberies.shopClerk.pedIndex, FALSE)
				FREEZE_ENTITY_POSITION(Shop_Robberies.shopClerk.pedIndex, TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(Shop_Robberies.shopClerk.vPos, Shop_Robberies.shopClerk.fHeading, <<-0.75, 0.0, -1.0>>))
				SET_ENTITY_HEADING(PLAYER_PED_ID(), Shop_Robberies.shopClerk.fHeading)
				
				CLEAR_SEQUENCE_TASK(siTemp)
				OPEN_SEQUENCE_TASK(siTemp)
//						IF bHasWeapon
//							TASK_SWAP_WEAPON(NULL, FALSE)
//						ENDIF
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "enter")
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 4000, AF_LOOPING)
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "exit", NORMAL_BLEND_IN, WALK_BLEND_OUT)
//						IF bHasWeapon
//							TASK_SWAP_WEAPON(NULL, TRUE)
//						ENDIF
				CLOSE_SEQUENCE_TASK(siTemp)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
				
				IF NOT DOES_CAM_EXIST(ciTill)
					ciTill = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
				ENDIF
				
				ATTACH_CAM_TO_ENTITY(ciTill, PLAYER_PED_ID(), <<-0.1878, 3.0635, 0.6800>>)
				POINT_CAM_AT_ENTITY(ciTill, PLAYER_PED_ID(), <<-0.0129, 0.0927, 0.3008>>)
				SET_CAM_FOV(ciTill, 35.00)
				SHAKE_CAM(ciTill, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE(ciTill, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			ENDIF
			
			ePlayerRobTillState = PLAYERROBTILL_QUICKCUT_INTERP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping from PLAYERROBTILL_QUICKCUT to PLAYERROBTILL_QUICKCUT_INTERP")
		BREAK
		
		CASE PLAYERROBTILL_QUICKCUT_INTERP
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT DOES_CAM_EXIST(ciTillInterp)
					ciTillInterp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
				ENDIF
				
				ATTACH_CAM_TO_ENTITY(ciTillInterp, PLAYER_PED_ID(), <<-1.0346, 2.9183, 0.6800>>)
				POINT_CAM_AT_ENTITY(ciTillInterp, PLAYER_PED_ID(), <<-0.0574, 0.1074, 0.3008>>)
				SET_CAM_FOV(ciTillInterp, 35.00)
				SHAKE_CAM(ciTillInterp, "HAND_SHAKE", 0.3)
				SET_CAM_ACTIVE_WITH_INTERP(ciTillInterp, ciTill, 8000)
			ENDIF
			
			iTillCamTimer = GET_GAME_TIMER()
			
			ePlayerRobTillState = PLAYERROBTILL_INTERP_FINISH
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping from PLAYERROBTILL_QUICKCUT_INTERP to PLAYERROBTILL_INTERP_FINISH")
		BREAK
		
		CASE PLAYERROBTILL_INTERP_FINISH
			//IF NOT IS_CAM_INTERPOLATING(ciTillInterp)
			IF (GET_GAME_TIMER() - iTillCamTimer) > 4800
			OR (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_RAGDOLL(PLAYER_PED_ID()))
			OR GET_NUMBER_OF_FIRES_IN_RANGE(Shop_Robberies.shopClerk.vPos, 3.0) > 0
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] interp done, Jumping from PLAYERROBTILL_INTERP_FINISH to PLAYERROBTILL_CLEANUP")
				
				ePlayerRobTillState = PLAYERROBTILL_CLEANUP
			
			ELIF (GET_GAME_TIMER() - iTillCamTimer) > 4500
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					IF NOT bIsPulseTriggered
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bIsPulseTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@shop_robbery@rob_till", "loop")
					fAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "oddjobs@shop_robbery@rob_till", "loop")
					
					IF ((fAnimPhase > 0.374 AND fAnimPhase <= 0.484)
						OR (fAnimPhase > 0.824 AND fAnimPhase <= 0.920))
						IF NOT bMoneyFlag
							iCashGrabbed = GET_RANDOM_INT_IN_RANGE(10, 51)
							CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iCashGrabbed)
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->PLAYER ROBBING TILL] player grabbed ", iCashGrabbed)
							
							iShopRobCashAmount += iCashGrabbed
							
							Shop_Robberies.shopCashBag.iCashSoundID = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(-1,"ROBBERY_MONEY_TOTAL","HUD_FRONTEND_CUSTOM_SOUNDSET")
							
							bMoneyFlag = TRUE
						ENDIF
					ELSE
						IF bMoneyFlag
							//ODDJOB_STOP_SOUND(Shop_Robberies.shopCashBag.iCashSoundID)
							bMoneyFlag = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_CLEANUP
			
			IF IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
				SET_ENTITY_COLLISION(Shop_Robberies.shopClerk.pedIndex, TRUE)
				FREEZE_ENTITY_POSITION(Shop_Robberies.shopClerk.pedIndex, FALSE)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF DOES_CAM_EXIST(ciTill)
				DESTROY_CAM(ciTill)
			ENDIF
			
			IF DOES_CAM_EXIST(ciTillInterp)
				DESTROY_CAM(ciTillInterp)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				vehVehicleToCheck = GET_PLAYERS_LAST_VEHICLE()
				VECTOR vTempVeh
				vTempVeh = GET_ENTITY_COORDS(vehVehicleToCheck)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc-> player's last vehicle is at ", vTempVeh)
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc-> player's last vehicle is now dead")
			ENDIF 	
			
			IF eCurrentWeapon <> WEAPONTYPE_UNARMED
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon, TRUE)
			ENDIF
			
			IF IS_BIT_SET(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
				CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "clearing buddy hide bit in rob till")
			ENDIF
			
			//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ODDJOB_EXIT_CUTSCENE()
			
			SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1,FALSE)
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Player state set to PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP.")
			eCurrentPlayerState	= PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP
			
			UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED()
			
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_HOLDUP)
			Shop_Robberies.bWriteToLeaderboard = TRUE			
			
			ePlayerRobTillState = PLAYERROBTILL_DONE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping from PLAYERROBTILL_CLEANUP to PLAYERROBTILL_DONE")
		BREAK
		
		CASE PLAYERROBTILL_SKIP
			IF IS_SCREEN_FADED_OUT()
				SET_CAM_ACTIVE(ciTillInterp, FALSE)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				
				IF iShopRobCashAmount < 15
					iCashGrabbed = GET_RANDOM_INT_IN_RANGE(25, 65)
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iCashGrabbed)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->PLAYER ROBBING TILL] player grabbed ", iCashGrabbed)
							
					iShopRobCashAmount += iCashGrabbed
				ENDIF
				
				DO_SCREEN_FADE_IN(500)
				
				ePlayerRobTillState = PLAYERROBTILL_CLEANUP
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_PLAYER_ROBBING_TILL] Jumping from PLAYERROBTILL_SKIP to PLAYERROBTILL_CLEANUP")
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

ENUM SNACK_BUY_ANIM_STATE
	SNACKBUY_INIT,
	SNACKBUY_PLAY_ANIM,
	SNACKBUY_CLEANUP_ANIM,
	SNACKBUY_DONE
ENDENUM

SNACK_BUY_ANIM_STATE eSnackBuyAnimState = SNACKBUY_DONE

//FUNC STRING GET_SNACK_ANIM(INT iWhichSnack)
//	SWITCH iWhichSnack
//		CASE 0		RETURN "purchase_cigarette"
//		CASE 1		RETURN "purchase_beer"
//		CASE 2		RETURN "purchase_energydrink"
//		DEFAULT 	RETURN "purchase_energydrink"
//	ENDSWITCH
//ENDFUNC
//
//FUNC STRING GET_SNACK_CLERK_ANIM(INT iWhichSnack)
//	SWITCH iWhichSnack
//		CASE 0		RETURN "purchase_cigarette_shopkeeper"
//		CASE 1		RETURN "purchase_beer_shopkeeper"
//		CASE 2		RETURN "purchase_energydrink_shopkeeper"
//		DEFAULT 	RETURN "purchase_energydrink_shopkeeper"
//	ENDSWITCH
//ENDFUNC

BOOL bTriggerSnackAnim
INT	iSnackBuySceneID
INT iSnackAppearTimer
INT iWhichSnackAnim

PROC UPDATE_SNACK_BUY_ANIM()
	
	IF bTriggerSnackAnim
		IF eSnackBuyAnimState = SNACKBUY_DONE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] anim request received and accepted")
			eSnackBuyAnimState = SNACKBUY_INIT
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] anim already running, forgetting anim request")
		ENDIF
		bTriggerSnackAnim = FALSE
	ENDIF
	
	SWITCH eSnackBuyAnimState
		CASE SNACKBUY_INIT
			
			//oiBoughtItem = CREATE_OBJECT(GET_SNACK_MODEL(iWhichSnackAnim), Shop_Robberies.cashRegisterSynchedScene.vPos - << 0, 0, 1.0>>)
			
			SET_ENTITY_VISIBLE(oiBoughtItem[iWhichSnackAnim], TRUE)
			iSnackBuySceneID = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(Shop_Robberies.cashRegisterSynchedScene.vPos, 
				Shop_Robberies.cashRegisterSynchedScene.vRotation.z, << -0.07, 0.06, -0.02 >>), Shop_Robberies.cashRegisterSynchedScene.vRotation)
			
			//iSnackBuySceneID = CREATE_SYNCHRONIZED_SCENE(Shop_Robberies.cashRegisterSynchedScene.vPos, Shop_Robberies.cashRegisterSynchedScene.vRotation)
			
			IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
				TASK_SYNCHRONIZED_SCENE (Shop_Robberies.shopClerk.pedIndex, iSnackBuySceneID, "mp_am_hold_up", "purchase_chocbar_shopkeeper", WALK_BLEND_IN, WALK_BLEND_OUT )
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBoughtItem[iWhichSnackAnim], iSnackBuySceneID, "purchase_chocbar","mp_am_hold_up", INSTANT_BLEND_IN, default, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] sync scene tasked, going to play anim")
				eSnackBuyAnimState = SNACKBUY_PLAY_ANIM
			ENDIF
		BREAK
		
		CASE SNACKBUY_PLAY_ANIM
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSnackBuySceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSnackBuySceneID) = 1
					
					IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
						CLEAR_PED_TASKS(Shop_Robberies.shopClerk.pedIndex)
					ENDIF
					
					IF DOES_ENTITY_EXIST(oiBoughtItem[iWhichSnackAnim])
						//STOP_SYNCHRONIZED_ENTITY_ANIM(oiBoughtItem[iWhichSnackAnim], INSTANT_BLEND_OUT, TRUE)
						SET_ENTITY_DYNAMIC(oiBoughtItem[iWhichSnackAnim], TRUE)
					ENDIF
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] sync scene is done, go to cleanup")
					iSnackAppearTimer = GET_GAME_TIMER()
					eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] sync scene not running, just go to cleanup")
				iSnackAppearTimer = GET_GAME_TIMER()
				eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
			ENDIF
		BREAK
		
		CASE SNACKBUY_CLEANUP_ANIM
			IF (GET_GAME_TIMER() - iSnackAppearTimer) > 1000
				IF DOES_ENTITY_EXIST(oiBoughtItem[iWhichSnackAnim])
					SET_ENTITY_VISIBLE(oiBoughtItem[iWhichSnackAnim], FALSE)
					SET_ENTITY_COORDS(oiBoughtItem[iWhichSnackAnim], GET_SNACK_INIT_POS(iWhichSnackAnim))
					SET_ENTITY_DYNAMIC(oiBoughtItem[iWhichSnackAnim], FALSE)
				ENDIF
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc-> snack anims] cleanup done, going to done")
				eSnackBuyAnimState = SNACKBUY_DONE
			ENDIF
		BREAK
		
		CASE SNACKBUY_DONE
			// nada here
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC STRING GET_SNACK_DESCRIPTION(INT iSnack)
	SWITCH iSnack
		CASE 0		RETURN "SNK_ITEM1_D"
		CASE 1		RETURN "SNK_ITEM2_D"
		CASE 2		RETURN "SNK_ITEM3_D"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC INT GET_SNACK_PRICE(INT iSnack)
	SWITCH iSnack
		CASE 0		RETURN 1
		CASE 1		RETURN 2
		CASE 2		RETURN 4
	ENDSWITCH

	RETURN 0
ENDFUNC

structTimer snackScrollTimerUp
structTimer snackScrollTimerDown

FUNC BOOL SNACK_MENU_UP()
	
	FLOAT fLeftY
	
	fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	IF fLeftY < -0.8 OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		IF NOT IS_TIMER_STARTED(snackScrollTimerUp)
			START_TIMER_NOW(snackScrollTimerUp)
			RETURN TRUE
		ELIF GET_TIMER_IN_SECONDS(snackScrollTimerUp) > 0.25
			RESTART_TIMER_NOW(snackScrollTimerUp)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(snackScrollTimerUp)
			CANCEL_TIMER(snackScrollTimerUp)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SNACK_MENU_DOWN()
	
	FLOAT fLeftY
	
	fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	IF fLeftY > 0.8 OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		IF NOT IS_TIMER_STARTED(snackScrollTimerDown)
			START_TIMER_NOW(snackScrollTimerDown)
			RETURN TRUE
		ELIF GET_TIMER_IN_SECONDS(snackScrollTimerDown) > 0.25
			RESTART_TIMER_NOW(snackScrollTimerDown)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(snackScrollTimerDown)
			CANCEL_TIMER(snackScrollTimerDown)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_SNACK_MENU()
	STRING sTempLabel
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	
	IF SNACK_MENU_UP()
		Shop_Robberies.shopSnacks.bSnackBought = FALSE
		CANCEL_TIMER(Shop_Robberies.shopSnacks.timerSnackReset)
		
		Shop_Robberies.shopSnacks.iCurrentSelection -= 1
		IF (Shop_Robberies.shopSnacks.iCurrentSelection < 0)
			Shop_Robberies.shopSnacks.iCurrentSelection = Shop_Robberies.shopSnacks.iMenuLength - 1
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_LIQUOR_STORE_SOUNDSET")
		
		SET_CURRENT_MENU_ITEM(Shop_Robberies.shopSnacks.iCurrentSelection)
		sTempLabel = GET_SNACK_DESCRIPTION(Shop_Robberies.shopSnacks.iCurrentSelection)
		Shop_Robberies.shopSnacks.bCheckMoney = FALSE
		Shop_Robberies.shopSnacks.bCheckHealth = FALSE
		
		IF DOES_TEXT_LABEL_EXIST(sTempLabel)
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
		ENDIF
	ENDIF
	
	IF SNACK_MENU_DOWN()
		Shop_Robberies.shopSnacks.bSnackBought = FALSE
		CANCEL_TIMER(Shop_Robberies.shopSnacks.timerSnackReset)
		
		Shop_Robberies.shopSnacks.iCurrentSelection += 1
		IF (Shop_Robberies.shopSnacks.iCurrentSelection > Shop_Robberies.shopSnacks.iMenuLength - 1)
			Shop_Robberies.shopSnacks.iCurrentSelection = 0
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_LIQUOR_STORE_SOUNDSET")
		
		SET_CURRENT_MENU_ITEM(Shop_Robberies.shopSnacks.iCurrentSelection)
		sTempLabel = GET_SNACK_DESCRIPTION(Shop_Robberies.shopSnacks.iCurrentSelection)
		Shop_Robberies.shopSnacks.bCheckMoney = FALSE
		Shop_Robberies.shopSnacks.bCheckHealth = FALSE
		
		IF DOES_TEXT_LABEL_EXIST(sTempLabel)
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
		ENDIF
	ENDIF
	
	IF NOT Shop_Robberies.shopSnacks.bCheckMoney
		Shop_Robberies.shopSnacks.bSnackAfford = (GET_TOTAL_CASH(GET_PLAYER_PED_ENUM(PLAYER_PED_ID()))  >= GET_SNACK_PRICE(Shop_Robberies.shopSnacks.iCurrentSelection))
		Shop_Robberies.shopSnacks.bCheckMoney = TRUE
	ENDIF
	
	IF NOT Shop_Robberies.shopSnacks.bCheckHealth
		Shop_Robberies.shopSnacks.bSnackNeeded = (GET_ENTITY_HEALTH(PLAYER_PED_ID()) < GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
		Shop_Robberies.shopSnacks.bCheckHealth = TRUE
	ENDIF
	
	IF Shop_Robberies.shopSnacks.bSnackBought
		IF GET_TIMER_IN_SECONDS_SAFE(Shop_Robberies.shopSnacks.timerSnackReset) < 3.0
			IF Shop_Robberies.shopSnacks.bSnackPurchased
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_BOUGHT")
			ELIF NOT Shop_Robberies.shopSnacks.bSnackAfford
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_AFFORD")
			ELIF NOT Shop_Robberies.shopSnacks.bSnackNeeded 
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_NEEDED")
			ENDIF
		ELSE
			Shop_Robberies.shopSnacks.bSnackBought = FALSE
			Shop_Robberies.shopSnacks.bSnackPurchased = FALSE
			CANCEL_TIMER(Shop_Robberies.shopSnacks.timerSnackReset)
			sTempLabel = GET_SNACK_DESCRIPTION(Shop_Robberies.shopSnacks.iCurrentSelection)
			IF DOES_TEXT_LABEL_EXIST(sTempLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CAMERA_INDEX ciSnacks


PROC EXIT_SNACK_MENU()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CONTROL_TAKEN)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	Shop_Robberies.shopSnacks.bSnackBought = FALSE
	
	CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
	
	IF DOES_CAM_EXIST(ciSnacks)
		DESTROY_CAM(ciSnacks)
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE,FALSE)
ENDPROC


PROC EXIT_SNACK_MENU_LITE()
	IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_CONTROL_TAKEN)
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CONTROL_TAKEN)
		ENDIF
	ENDIF
	
	Shop_Robberies.shopSnacks.bSnackBought = FALSE
	
	CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
	
	IF DOES_CAM_EXIST(ciSnacks)
		DESTROY_CAM(ciSnacks)
	ENDIF
ENDPROC

//INT iCurrentLocateFrame
//VECTOR vLocateCheck_LastCoords
//FLOAT fLocateCheck_LastHead

PROC UPDATE_SHOP_SNACKS()
//	INT iCount
//	VECTOR vShop = GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eCurrentShop)
	
	INT iMoneyPayed
	
	UPDATE_SNACK_BUY_ANIM()
	
	IF ((eCurrentShopClerkState = SHOP_CLERK_STATE_NORMAL) OR (eCurrentShopClerkState = SHOP_CLERK_STATE_STUBBORN))
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
	AND NOT IS_PED_INJURED(Shop_Robberies.pedPlayer)
	AND NOT IS_PLAYER_UNDER_ATTACK()
	AND NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
	//AND IS_PLAYER_INSIDE_SHOP()
	
		INT iCurrentHealth = GET_ENTITY_HEALTH(PLAYER_PED_ID())
		INT iMaxHealth = GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID())
		
		VECTOR vPlayerCoords 
//		FLOAT fPlayersHead 
		
		// controls failsafe
		IF eShopSnacksState != SHOPSNACKS_SETUP_MENU
		AND eShopSnacksState != SHOPSNACKS_UPDATE_MENU
		AND eShopSnacksState != SHOPSNACKS_BUY
		AND IS_PLAYER_INSIDE_SHOP()
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		BOOL bCursorAccept = FALSE // for mouse cursor
		STRING sTempLabel // Because the mouse cursor can now change the item description too
		
		SWITCH eShopSnacksState
			CASE SHOPSNACKS_INIT
				//CHEck LOCATES HERE
				
				IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
				AND (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_UNARMED)
					IF GET_SHOP_BUY_LOCATE_DATA(eCurrentShop, Shop_Robberies.cashRegisterSynchedScene.vPos, Shop_Robberies.cashRegisterSynchedScene.vRotation.z,
												Shop_Robberies.shopSnacks.vBuyEntryPos[0], Shop_Robberies.shopSnacks.vBuyEntryPos[1],
												Shop_Robberies.shopSnacks.vBuyLocateCenter, Shop_Robberies.shopSnacks.fBuyEntrySize)
						vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, Shop_Robberies.shopSnacks.vBuyLocateCenter) < 5.0
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), Shop_Robberies.shopSnacks.vBuyEntryPos[0], Shop_Robberies.shopSnacks.vBuyEntryPos[1], Shop_Robberies.shopSnacks.fBuyEntrySize)
								IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), Shop_Robberies.shopSnacks.vBuyLocateCenter, 135.0)
									
									IF (GET_GAME_TIMER() % 1000) < 50
										CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> update robbing till] player is in buy locate")
									ENDIF
									
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_HOLDUP_1")
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
									AND NOT IS_SELECTOR_CAM_ACTIVE()
									AND IS_FLOW_HELP_QUEUE_EMPTY()
										PRINT_HELP_FOREVER("SHR_MENU")
									ENDIF
									
									Shop_Robberies.iUseContext = NEW_CONTEXT_INTENTION
									REGISTER_CONTEXT_INTENTION(Shop_Robberies.iUseContext, CP_HIGH_PRIORITY, "SHR_MENU")
									
									CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
									CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_INIT)
									
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_SHOP_SNACKS] SHOPSNACKS_INIT -> SHOPSNACKS_WAIT_ACTIVATE_MENU")
									eShopSnacksState = SHOPSNACKS_WAIT_ACTIVATE_MENU
									
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE SHOPSNACKS_WAIT_ACTIVATE_MENU
				IF NOT IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), Shop_Robberies.shopSnacks.vBuyLocateCenter, 135.0)
				OR NOT (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_UNARMED)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_SHOP_SNACKS] Player left aisle, going back to SHOPSNACKS_INIT")
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
						CLEAR_HELP()
					ENDIF
					
					Shop_Robberies.shopSnacks.iCurrentLocate = -1
					
					RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->update snacks] jumping back to _init")
					eShopSnacksState = SHOPSNACKS_INIT
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_HOLDUP_1")
					AND IS_FLOW_HELP_QUEUE_EMPTY()
					AND NOT IS_CONTEXT_INTENTION_HELP_DISPLAYING(Shop_Robberies.iUseContext)
						PRINT_HELP_FOREVER("SHR_MENU")
					ENDIF
				ENDIF
				
				IF NOT IS_PED_RUNNING(PLAYER_PED_ID())
				AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
					IF HAS_CONTEXT_BUTTON_TRIGGERED(Shop_Robberies.iUseContext)
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_SHOP_SNACKS] SHOPSNACKS_WAIT_ACTIVATE_MENU -> SHOPSNACKS_BUY")
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
							CLEAR_HELP()
						ENDIF
						
						SET_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
						
						RESTART_TIMER_NOW(Shop_Robberies.shopSnacks.timerSnackReset)
						
						RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
						
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), Shop_Robberies.shopClerk.pedIndex)
						ENDIF
						
						eShopSnacksState = SHOPSNACKS_SETUP_MENU
					ENDIF
				ENDIF
			BREAK
			
			CASE SHOPSNACKS_SETUP_MENU
				IF NOT DOES_CAM_EXIST(ciSnacks)
					ciSnacks = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
				ENDIF
				
				FLOAT fSnackCamFov
				
				GET_SHOP_SNACK_CAM_DATA(eCurrentShop, Shop_Robberies.cashRegisterSynchedScene.vPos, Shop_Robberies.cashRegisterSynchedScene.vRotation.z,
										Shop_Robberies.shopSnacks.vCamPos, Shop_Robberies.shopSnacks.vCamRot, fSnackCamFov)
				
				SET_CAM_COORD(ciSnacks, Shop_Robberies.shopSnacks.vCamPos)
				SET_CAM_ROT(ciSnacks, Shop_Robberies.shopSnacks.vCamRot)
				SET_CAM_FOV(ciSnacks, 35.00)
				SHAKE_CAM(ciSnacks, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE(ciSnacks, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CONTROL_TAKEN)
				ENDIF
				
				Shop_Robberies.shopSnacks.szHeaderTxd = GET_SHOP_HEADER_TEXTURE_STRING(eCurrentShop)
				
				Shop_Robberies.shopSnacks.iCurrentSelection = 0
				Shop_Robberies.shopSnacks.iMenuLength = 3
				
				CLEAR_MENU_DATA()
				
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				SET_MENU_TITLE(GET_SHOP_ROB_NAME(eCurrentShop))
				
				SET_MENU_USES_HEADER_GRAPHIC(TRUE, Shop_Robberies.shopSnacks.szHeaderTxd, Shop_Robberies.shopSnacks.szHeaderTxd)
				
				SET_CURSOR_POSITION_FOR_MENU()
				
				// brute forcing these for now
				ADD_MENU_ITEM_TEXT(0, "SNK_ITEM1") 
				ADD_MENU_ITEM_TEXT(0, "ITEM_COST", 1) 
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_SNACK_PRICE(0))
				ADD_MENU_ITEM_TEXT(1, "SNK_ITEM2") 
				ADD_MENU_ITEM_TEXT(1, "ITEM_COST", 1) 
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_SNACK_PRICE(1))
				ADD_MENU_ITEM_TEXT(2, "SNK_ITEM3") 
				ADD_MENU_ITEM_TEXT(2, "ITEM_COST", 1) 
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_SNACK_PRICE(2))
				
				SET_TOP_MENU_ITEM(0)
				SET_CURRENT_MENU_ITEM(Shop_Robberies.shopSnacks.iCurrentSelection)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_ITEM1_D")
				
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT,	"ITEM_SELECT")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL,	"ITEM_BACK")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X,		"SNK_LIFT")
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
				
				eShopSnacksState = SHOPSNACKS_UPDATE_MENU
			BREAK
			CASE SHOPSNACKS_UPDATE_MENU
				
				///////////////////////////////////
				// Mouse menu support
				bCursorAccept = FALSE
				
				IF IS_PC_VERSION()	
					
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
											
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
						
						IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						
							IF g_iMenuCursorItem != Shop_Robberies.shopSnacks.iCurrentSelection
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								Shop_Robberies.shopSnacks.iCurrentSelection = g_iMenuCursorItem
								
								SET_CURRENT_MENU_ITEM(Shop_Robberies.shopSnacks.iCurrentSelection)
								
								Shop_Robberies.shopSnacks.bSnackBought = FALSE
								Shop_Robberies.shopSnacks.bCheckMoney = FALSE
								Shop_Robberies.shopSnacks.bCheckHealth = FALSE
								
								sTempLabel = GET_SNACK_DESCRIPTION(Shop_Robberies.shopSnacks.iCurrentSelection)
								IF DOES_TEXT_LABEL_EXIST(sTempLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
								ENDIF
								
							ELSE
								bCursorAccept = TRUE
							ENDIF						
						
						ENDIF
					
					ENDIF
				
				ENDIF
				
				DRAW_MENU(TRUE, ENUM_TO_INT(eCurrentShop))
				UPDATE_SNACK_MENU()
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE
				
					Shop_Robberies.shopSnacks.bSnackBought = TRUE
					Shop_Robberies.shopSnacks.bCheckMoney = FALSE
					Shop_Robberies.shopSnacks.bCheckHealth = FALSE
					
					IF Shop_Robberies.shopSnacks.bSnackAfford
					AND (Shop_Robberies.shopSnacks.bSnackNeeded)
						
						Shop_Robberies.shopSnacks.bSnackPurchased = TRUE
						
						PLAY_SOUND_FRONTEND(-1, "PURCHASE", "HUD_LIQUOR_STORE_SOUNDSET")
						bTriggerSnackAnim = TRUE
						iWhichSnackAnim = Shop_Robberies.shopSnacks.iCurrentSelection
						eShopSnacksState = SHOPSNACKS_BUY
					ELSE
						Shop_Robberies.shopSnacks.bSnackPurchased = FALSE
						
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_LIQUOR_STORE_SOUNDSET")
					ENDIF
					
					//EXIT_SNACK_MENU()
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					eShopSnacksState = SHOPSNACKS_SHOPLIFT2
					
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_LIQUOR_STORE_SOUNDSET")
					
					EXIT_SNACK_MENU()
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
					eShopSnacksState = SHOPSNACKS_RESET
					Shop_Robberies.shopSnacks.iHoldTime = GET_GAME_TIMER()
					PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_LIQUOR_STORE_SOUNDSET")
					
					EXIT_SNACK_MENU()
				ENDIF
				
			BREAK
			CASE SHOPSNACKS_BUY
				// anim?
				DRAW_MENU(TRUE, ENUM_TO_INT(eCurrentShop))
				
				SWITCH Shop_Robberies.shopSnacks.iCurrentSelection
					CASE 0
						iMoneyPayed = GET_SNACK_PRICE(Shop_Robberies.shopSnacks.iCurrentSelection)
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_SHOP_SNACKS] iCurrentHealth = ", iCurrentHealth, ", iMaxHealth = ", iMaxHealth)
						IF (iCurrentHealth + 10) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 10))
						ENDIF
						//NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("SNK_ITEM1")), PURCHASE_FOOD)
					BREAK
					CASE 1
						iMoneyPayed = GET_SNACK_PRICE(Shop_Robberies.shopSnacks.iCurrentSelection)
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE_SHOP_SNACKS] iCurrentHealth = ", iCurrentHealth, ", iMaxHealth = ", iMaxHealth)
						IF (iCurrentHealth + 20) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 20))
						ENDIF
						//NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("SNK_ITEM2")), PURCHASE_FOOD)
					BREAK
					CASE 2
						iMoneyPayed = GET_SNACK_PRICE(Shop_Robberies.shopSnacks.iCurrentSelection)
						IF (iCurrentHealth + 15) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 15))
						ENDIF
						//NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("SNK_ITEM3")), PURCHASE_FOOD)
					BREAK
				ENDSWITCH
				
				DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_SNACKS, iMoneyPayed)
				
				// dialog
				IF NOT Shop_Robberies.shopSnacks.bSnackBuyDialogueSaid
					eCurrentDialogState = DIALOG_STATE_CLERK_BUY_SNACK_REACTION
					Shop_Robberies.shopSnacks.bSnackBuyDialogueSaid = TRUE
				ENDIF
				
				RESTART_TIMER_NOW(Shop_Robberies.shopSnacks.timerSnackReset)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE SNACKS] snack bought, going to _UPDATE")
				eShopSnacksState = SHOPSNACKS_UPDATE_MENU
			BREAK
			CASE SHOPSNACKS_SHOPLIFT
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					
					IF NOT bOnAnyMission
						
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE SNACKS] anims tasked, going to _shoplift2")
					ELSE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE SNACKS] on mission so no anims, still going to _shoplift2")
					ENDIF
					eShopSnacksState = SHOPSNACKS_SHOPLIFT2
				ENDIF
			BREAK
			CASE SHOPSNACKS_SHOPLIFT2
				SWITCH Shop_Robberies.shopSnacks.iCurrentSelection
					CASE 0
						IF (iCurrentHealth + 10) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 10))
						ENDIF
					BREAK
					CASE 1
						IF (iCurrentHealth + 20) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 20))
						ENDIF
					BREAK
					CASE 2
						IF (iCurrentHealth + 15) > iMaxHealth
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						ELSE
							SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 15))
						ENDIF
					BREAK
				ENDSWITCH
					
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
				
				DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
				
				// dialog
				eCurrentDialogState = DIALOG_STATE_CLERK_STEAL_SNACK_REACTION
				
				IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
				AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex, "misscommon@response", "give_me_a_break")
				ENDIF
				
				//go wanted
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
					SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[ShopRobberies.sc->UPDATE SNACKS] shoplift complete, going to _cleanup")
				
				eShopSnacksState = SHOPSNACKS_CLEANUP
			BREAK
			CASE SHOPSNACKS_RESET
				IF (GET_GAME_TIMER() - Shop_Robberies.shopSnacks.iHoldTime) > 1000
					eShopSnacksState = SHOPSNACKS_INIT
				ELSE
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				ENDIF
			BREAK
			CASE SHOPSNACKS_CLEANUP
				
			BREAK
		ENDSWITCH
	
	ELSE
		
		IF eShopSnacksState != SHOPSNACKS_CLEANUP
		AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
			IF eShopSnacksState = SHOPSNACKS_UPDATE_MENU
			OR eShopSnacksState = SHOPSNACKS_SETUP_MENU
			OR eShopSnacksState = SHOPSNACKS_BUY
				EXIT_SNACK_MENU()
			ENDIF
			
			eShopSnacksState = SHOPSNACKS_CLEANUP
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_SNACKS")
			CLEAR_HELP()
			RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
		ENDIF
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
			CLEAR_HELP()
			RELEASE_CONTEXT_INTENTION(Shop_Robberies.iUseContext)
		ENDIF
		IF IS_BIT_SET(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
		AND ePlayerRobTillState < PLAYERROBTILL_QUICKCUT
			CLEAR_BIT(Shop_Robberies.buddyMonitor.iFlags, BUDDY_HIDE)
		ENDIF
		IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
			EXIT_SNACK_MENU_LITE()
		ENDIF
	ENDIF
ENDPROC

ENUM WANTED_LEVEL_MONITOR_STATE
	WANTEDSTATE_WAITING,
	WANTEDSTATE_WANTED,
	WANTEDSTATE_OUTSIDE,
	WANTEDSTATE_DONE
ENDENUM

WANTED_LEVEL_MONITOR_STATE eWantedLevelState

PROC UPDATE_WANTED_LEVEL_MONITOR()
	SWITCH eWantedLevelState
		CASE WANTEDSTATE_WAITING
			IF Shop_Robberies.wantedMonitor.bShopWantedLevelGiven
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->Wanted level monitor] wanted level set!  going from waiting to wanted")
				eWantedLevelState = WANTEDSTATE_WANTED
			ENDIF
		BREAK
		CASE WANTEDSTATE_WANTED
			IF IS_PLAYER_INSIDE_SHOP()
				IF IS_TIMER_STARTED(Shop_Robberies.wantedMonitor.timerOutOfShop)
					CANCEL_TIMER(Shop_Robberies.wantedMonitor.timerOutOfShop)
				ENDIF
				
				IF (GET_GAME_TIMER() % 1000) < 50
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->Wanted level monitor] currently suppresing the loss of wanted level")
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				ENDIF
			ELSE
				RESTART_TIMER_NOW(Shop_Robberies.wantedMonitor.timerOutOfShop)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->Wanted level monitor] Player left shop, going to _outside")
				eWantedLevelState = WANTEDSTATE_OUTSIDE
			ENDIF
		BREAK
		CASE WANTEDSTATE_OUTSIDE
			IF IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->Wanted level monitor] player went back inside shop, going back to _wanted")
				eWantedLevelState = WANTEDSTATE_WANTED
			ENDIF
		BREAK
		CASE WANTEDSTATE_DONE
			
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Update cash bag state.
PROC UPDATE_CASH_BAG()
	SWITCH(eCurrentCashBagState)
		// Keep track of where the cash bag is in the animation to alter its content or behavior.
		CASE CASH_BAG_STATE_WAIT_FOR_BAG_TO_BE_PAST_COUNTER
			IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					// The bag is perpendicular to the ground.
					IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.876
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from CASH_BAG_STATE_WAIT_FOR_BAG_TO_BE_PAST_COUNTER to CASH_BAG_STATE_WAIT_TO_CREATE_PICKUP")
						eCurrentCashBagState = CASH_BAG_STATE_WAIT_TO_CREATE_PICKUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Wait until the bag is on the ground to create the pick up.
		CASE CASH_BAG_STATE_WAIT_TO_CREATE_PICKUP
			IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.894
					OR (GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.871
						AND (VDIST2(GET_ENTITY_COORDS(Shop_Robberies.shopCashBag.objIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 1.0 ))
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from CASH_BAG_STATE_WAIT_TO_CREATE_PICKUP to CASH_BAG_STATE_CREATE_PICKUP")
						eCurrentCashBagState = CASH_BAG_STATE_CREATE_PICKUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CASH_BAG_STATE_CREATE_PICKUP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] CASH_BAG_STATE_PICKUP_ACTIVE: Setting wanted level here as per 958330.")
			SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
			
			CREATE_CASH_BAG_PICKUP()
			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(Shop_Robberies.shopCashBag.objIndex, FALSE)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from CASH_BAG_STATE_CREATE_PICKUP to CASH_BAG_STATE_PICKUP_ACTIVE")
			eCurrentCashBagState = CASH_BAG_STATE_PICKUP_ACTIVE
		BREAK
		
		CASE CASH_BAG_STATE_CREATE_EARLY_PICKUP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] CASH_BAG_STATE_PICKUP_ACTIVE: Setting wanted level here as per 958330.")
			SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
			
			CREATE_CASH_BAG_PICKUP(TRUE)
			SET_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex, FALSE)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from early pickup to CASH_BAG_STATE_PICKUP_ACTIVE")
			eCurrentCashBagState = CASH_BAG_STATE_PICKUP_ACTIVE
		BREAK
		
		CASE CASH_BAG_STATE_PICKUP_ACTIVE
			//IF HAS_PICKUP_BEEN_COLLECTED(Shop_Robberies.shopCashBag.pickupIndex)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(Shop_Robberies.shopCashBag.objIndex, PLAYER_PED_ID())
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iShopRobCashAmount)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] CASE CASH_BAG_STATE_PICKUP_ACTIVE: Pickup collected.  Hide cash bag.")
				
				SET_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex, FALSE)
				
				IF DOES_BLIP_EXIST(Shop_Robberies.shopCashBag.blipIndex)
					REMOVE_BLIP(Shop_Robberies.shopCashBag.blipIndex)
				ENDIF
				
				IF eCurrentPlayerState != PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP
					eCurrentPlayerState	= PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP
				ENDIF
				
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_HOLDUP)
				Shop_Robberies.bWriteToLeaderboard = TRUE
				
				Shop_Robberies.shopCashBag.iCashSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(-1, "ROBBERY_MONEY_TOTAL","HUD_FRONTEND_CUSTOM_SOUNDSET")
				
				RESTART_TIMER_NOW(Shop_Robberies.shopCashBag.timerBagSound)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from CASH_BAG_STATE_PICKUP_ACTIVE to CASH_BAG_STATE_LOOP_END")
				eCurrentCashBagState = CASH_BAG_STATE_LOOP_END
			ELSE
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
				AND IS_ENTITY_VISIBLE(Shop_Robberies.shopCashBag.objIndex)
					//IF NOT Shop_Robberies.bFakeGlowRendered
					//	RENDER_FAKE_PICKUP_GLOW(GET_ENTITY_COORDS(Shop_Robberies.shopCashBag.objIndex) - <<0,0,0.075>> , 1)
					//	Shop_Robberies.bFakeGlowRendered = TRUE
					//ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CASH_BAG_STATE_LOOP_END
			IF GET_TIMER_IN_SECONDS(Shop_Robberies.shopCashBag.timerBagSound) > 2.5
				//ODDJOB_STOP_SOUND(Shop_Robberies.shopCashBag.iCashSoundID)
				
				IF DOES_ENTITY_EXIST(Shop_Robberies.shopCashBag.objIndex)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(Shop_Robberies.shopCashBag.objIndex, PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
					DELETE_OBJECT(Shop_Robberies.shopCashBag.objIndex)
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_CASH_BAG] Jumping from CASH_BAG_STATE_LOOP_END to CASH_BAG_STATE_DONE")
				eCurrentCashBagState = CASH_BAG_STATE_DONE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Update patron state.
PROC UPDATE_SHOP_PATRON()
	IF eCurrentShopPatronState <> SHOP_PATRON_STATE_UNAVAILABLE
		SWITCH(eCurrentShopPatronState)
			CASE SHOP_PATRON_STATE_CHECK_AVAILABILITY
				IF IS_TAXI_TAKE_IT_EASY_PATRON_IN_SHOP()
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From SHOP_PATRON_STATE_CHECK_AVAILABILITY to SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM.")
					eCurrentShopPatronState = SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From SHOP_PATRON_STATE_CHECK_AVAILABILITY to SHOP_PATRON_STATE_UNAVAILABLE.")
					eCurrentShopPatronState = SHOP_PATRON_STATE_UNAVAILABLE
				ENDIF
			BREAK
		
			CASE SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopPatron.pedIndex)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM: Task patron to loop the shocked animation.")
					
					// Task patron to face player and react to the holdup.
					TASK_PATRON_TO_REACT_TO_HOLDUP()
					
					// Start counting timer for patron fleeing.
					START_TIMER_NOW(Shop_Robberies.shopPatron.timerBeforeFlee)
							
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM to SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT")
					eCurrentShopPatronState = SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT
				ENDIF			
			BREAK
			
			CASE SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopPatron.pedIndex)
					IF TIMER_DO_WHEN_READY(Shop_Robberies.shopPatron.timerBeforeFlee, 5.0)
					OR CHECK_AIMING_SHOP_ROB(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
					OR IS_PED_RAGDOLL(Shop_Robberies.shopPatron.pedIndex)
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT: Patron loop animation has finished playing!")
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From SHOP_PATRON_STATE_PLAY_SHOCKED_ANIM_WAIT to SHOP_PATRON_STATE_FLEES")
						eCurrentShopPatronState = SHOP_PATRON_STATE_FLEES
					ENDIF
				ENDIF				
			BREAK
			
			CASE SHOP_PATRON_STATE_FLEES
				IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopPatron.pedIndex)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From Patron: SHOP_PATRON_STATE_FLEES to Dialog: DIALOG_STATE_PATRON_REACTS_TO_HOLDUP")
					eCurrentDialogState = DIALOG_STATE_PATRON_REACTS_TO_HOLDUP
					
					TASK_SMART_FLEE_PED(Shop_Robberies.shopPatron.pedIndex, Shop_Robberies.pedPlayer, 500.0, -1)
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_PATRON] From SHOP_PATRON_STATE_FLEES to SHOP_PATRON_STATE_DONE")
					eCurrentShopPatronState = SHOP_PATRON_STATE_DONE
				ENDIF
				
				CANCEL_TIMER(Shop_Robberies.shopPatron.timerBeforeFlee)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:
///    Updates how the shop clerk reacts once the holdup is successful.
PROC UPDATE_SHOP_CLERK_HOLDUP_REACTION()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Procedure started.")

	// Check if player is holding a specific weapon.
	IF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_RPG)
		Shop_Robberies.bIsPlayerHoldingRocketLauncherAtHoldUp 	= TRUE
	ELIF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_KNIFE)
		Shop_Robberies.bIsPlayerHoldingKnifeAtHoldUp 			= TRUE
	ELIF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_HAMMER)
	OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_CROWBAR)
	OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_BAT)
	OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_DLC_BOTTLE)
		Shop_Robberies.bPlayerIsHoldingToolAtHoldUp 			= TRUE
	ELIF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_GRENADE)
	OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_SMOKEGRENADE)
	OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_MOLOTOV)
		Shop_Robberies.bPlayerIsHoldingThrowAtHoldUp 			= TRUE
	ELIF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_STICKYBOMB)
		Shop_Robberies.bPlayerIsHoldingStickyAtHoldUp 			= TRUE
	ENDIF
	
	// Start checking the player leaving the store.
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Player state set to PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP.")
	eCurrentPlayerState	= PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP

	// The player is holding up the shop.  Save the number of times the player has done this.
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Update number of times shop has been robbed.")
	UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED()

	
	// If the cops are set to ambush the player, they've been alerted.
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Cops state set to COPS_STATE_ALERTED.")
	eCurrentCopsState 	= COPS_STATE_ALERTED

	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Holdup state is ON.")
	eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Update state set to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG")
	eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_HOLDUP_REACTION] Update bIsPayingAttention to FALSE")
	Shop_Robberies.shopClerk.bIsPayingAttention = FALSE
ENDPROC


/// PURPOSE:
///    Update clerk animation state.
PROC UPDATE_SHOP_CLERK_ANIMATION()
	IF (eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_INVALID)
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
			SWITCH (eCurrentShopClerkAnimState)
				CASE SHOP_CLERK_ANIM_STATE_GREET
//					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szGreetAnimClip, 
//									SLOW_BLEND_IN, -2, -1, AF_DEFAULT)
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_GREET to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_FIRE
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szFireAnimClip, 
									NORMAL_BLEND_IN, -2, -1, AF_DEFAULT)
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_FIRE to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_WORRIED
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szWorriedAnimClip, 
									NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_WORRIED to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_SHOCKED
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szShockedAnimClip, 
									NORMAL_BLEND_IN, -2, -1, AF_DEFAULT)
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_SHOCKED to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_STUBBORN
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szStubbornAnimClip, 
									NORMAL_BLEND_IN, -2, -1, AF_DEFAULT)
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_STUBBORN to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_COWER
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_COWER to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT
				BREAK
				
				CASE SHOP_CLERK_ANIM_STATE_PUSHED
					TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex,  Shop_Robberies.shopClerk.szReactionAnimDict, Shop_Robberies.shopClerk.szPushedAnimClip, 
									NORMAL_BLEND_IN, -2, -1, AF_DEFAULT)		
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_ANIMATION] Jumping from SHOP_CLERK_ANIM_STATE_PUSHED to SHOP_CLERK_ANIM_STATE_WAIT.")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WAIT					
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Update clerk look-at state.
PROC UPDATE_SHOP_CLERK_LOOK()
	SWITCH (eCurrentShopClerkLookState)
		CASE SHOP_CLERK_LOOK_STATE_AT_PLAYER_AFTER_ENTERING_SHOP
			TASK_CLERK_TO_LOOK_AT_PLAYER()
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_LOOK] From SHOP_CLERK_LOOK_STATE_AT_PLAYER_AFTER_ENTERING_SHOP to SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_GETTING_CASH_IN_HOLDUP.")
			eCurrentShopClerkLookState = SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_GETTING_CASH_IN_HOLDUP
		BREAK
		
		CASE SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_GETTING_CASH_IN_HOLDUP
			IF (eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_GOING_FOR_REGISTER)
				TASK_CLERK_TO_STOP_LOOKING_AT_PLAYER()
				Shop_Robberies.shopClerk.bIsPayingAttention = FALSE
			
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_LOOK] From SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_GETTING_CASH_IN_HOLDUP to SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_HOLDUP_ENDS.")
				eCurrentShopClerkLookState = SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_HOLDUP_ENDS
			ENDIF
			
			IF (IS_PLAYER_INSIDE_COUNTER_AREA_OF_SHOP()	OR IS_PLAYER_INSIDE_BACKROOM_AREA_OF_SHOP())
			AND NOT Shop_Robberies.shopClerk.bIsPayingAttention
				Shop_Robberies.shopClerk.bIsPayingAttention = TRUE
			ENDIF
			
			IF NOT Shop_Robberies.shopClerk.bLeftShop
				IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
				AND IS_PED_FLEEING(Shop_Robberies.shopClerk.pedIndex) //IS_CLERK_INSIDE_SHOP()
					Shop_Robberies.shopClerk.bLeftShop = TRUE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_LOOK]Shop_Robberies.shopClerk.bLeftShop set to TRUE ")
				ENDIF
			ENDIF
			
			IF Shop_Robberies.shopClerk.bIsPayingAttention
			AND NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
			AND NOT (eCurrentShopClerkState = SHOP_CLERK_STATE_DONE)
				TASK_CLERK_TO_LOOK_AT_PLAYER_ALWAYS()
			ENDIF
		BREAK
		
		CASE SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_HOLDUP_ENDS
			IF (eCurrentCashRegisterScenePhase >= CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG)
				TASK_CLERK_TO_LOOK_AT_PLAYER()
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_LOOK] From SHOP_CLERK_LOOK_STATE_WAIT_UNTIL_HOLDUP_ENDS to SHOP_CLERK_LOOK_STATE_DONE.")
				eCurrentShopClerkLookState = SHOP_CLERK_LOOK_STATE_DONE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Check every frame if player is pouring petrol in the shop.
PROC UPDATE_CLERK_REACTING_TO_JERRY_CAN()
	IF NOT Shop_Robberies.bHasPlayerPouredJerryCan
		IF IS_CLERK_SEEING_PLAYER_USE_PETROL_CAN_IN_SHOP()
			IF NOT IS_TIMER_STARTED(Shop_Robberies.timerPlayerPouringPetrol)
				START_TIMER_NOW(Shop_Robberies.timerPlayerPouringPetrol)
			ELSE
				IF TIMER_DO_WHEN_READY(Shop_Robberies.timerPlayerPouringPetrol, 1.0)
					Shop_Robberies.bHasPlayerPouredJerryCan = TRUE
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Dialog: DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN.")
					eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_POUR_JERRYCAN
					
					IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
						TASK_SMART_FLEE_PED(Shop_Robberies.shopClerk.pedIndex, PLAYER_PED_ID(), 100.0, -1)
						SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_TIMER_STARTED(Shop_Robberies.timerPlayerPouringPetrol)
				CANCEL_TIMER(Shop_Robberies.timerPlayerPouringPetrol)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Check every frame if ped is fleeing, so we can cancel holdup.
PROC UPDATE_CLERK_FLEEING_STATUS()
	IF (eCurrentShopClerkState <> SHOP_CLERK_STATE_DONE)
		IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
			IF IS_PED_FLEEING(Shop_Robberies.shopClerk.pedIndex)
				TASK_SMART_FLEE_PED(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.pedPlayer, 100.0, -1)
			
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Clerk: SHOP_CLERK_STATE_DONE.")
				eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Clerk Look: SHOP_CLERK_LOOK_STATE_DONE.")
				eCurrentShopClerkLookState = SHOP_CLERK_LOOK_STATE_DONE
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Clerk Anim: SHOP_CLERK_ANIM_STATE_DONE.")
				eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_DONE
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Player: PLAYER_STATE_DONE.")
				eCurrentPlayerState = PLAYER_STATE_DONE
				
				IF	(eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Holdup: SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON.")
					eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Holdup: SHOP_ROBBERIES_HOLDUP_STATE_DONE.")
					eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE					
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Update: UPDATE_SHOP_ROBBERIES_DONE")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE

				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Setting Dialog: DIALOG_STATE_CLERK_FLEES.")
				eCurrentDialogState = DIALOG_STATE_CLERK_FLEES
				
				IF NOT Shop_Robberies.shopClerk.bLeftShop
					Shop_Robberies.shopClerk.bLeftShop = TRUE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK_LOOK]Shop_Robberies.shopClerk.bLeftShop set to TRUE in UPDATE_CLERK_FLEEING_STATUS")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Update clerk state.
PROC UPDATE_SHOP_CLERK()
	// Check every frame if ped is fleeing, so we can cancel holdup.
	UPDATE_CLERK_FLEEING_STATUS()
	
	// Check every frame if player is pouring petrol in the shop.
	UPDATE_CLERK_REACTING_TO_JERRY_CAN()

	// Get the player's current weapon.
	WEAPON_TYPE currentPlayerWeapon
	IF DOES_ENTITY_EXIST(Shop_Robberies.pedPlayer)
		GET_CURRENT_PED_WEAPON(Shop_Robberies.pedPlayer, currentPlayerWeapon)
		IF eCurrentShopClerkState = SHOP_CLERK_STATE_NORMAL
		AND eDynamicRequestState = DR_EXIT_SHOP
		AND (HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			OR IS_EXPLOSION_INSIDE_MAIN_AREA_OF_SHOP()
			OR IS_BULLET_INSIDE_MAIN_AREA_OF_SHOP())
			//OR currentPlayerWeapon <> WEAPONTYPE_UNARMED)
		// TODO: add general shooting conditions here, change the state change to a general flee thing
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			
			SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
			
			eCurrentShopClerkState = SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	// Update clerk state.
	UPDATE_SHOP_CLERK_LOOK()
	
	IF NOT bOnAnyMission
		// Update clerk animation state.
		UPDATE_SHOP_CLERK_ANIMATION()
	ENDIF
	
	SWITCH(eCurrentShopClerkState)
		// Player will react by default when being held up.
		CASE SHOP_CLERK_STATE_NORMAL
			IF IS_PLAYER_INSIDE_SHOP()
				IF (CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
						OR (CHECK_AIMING_SHOP_ROB(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)  AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())))
				AND (currentPlayerWeapon <> WEAPONTYPE_UNARMED)
					UPDATE_SHOP_CLERK_HOLDUP_REACTION()
					
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_NORMAL to Dialog: DIALOG_STATE_HOLDUP_STARTS")
					eCurrentDialogState = DIALOG_STATE_HOLDUP_STARTS
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_NORMAL to Print: PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT")
					eCurrentPrintEnum 	= PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_NORMAL to SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE")
					eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
					
					// used for friend reactions during friend activity
					SET_BIT(g_bitfieldFriendFlags, ENUM_TO_INT(FRIENDFLAG_IS_ROBBERY_UNDERWAY))
					
					TASK_NEARBY_PEDS_TO_COWER()
				ENDIF
				
				IF IS_BITMASK_AS_ENUM_SET( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
					IF NOT IS_BITMASK_AS_ENUM_SET( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
						
						IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
						AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
							TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex, "misscommon@response", "numbnuts")
						ENDIF
						
						eCurrentDialogState = DIALOG_STATE_CLERK_THREATENED
						
						SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
						
						TASK_NEARBY_PEDS_TO_FLEE()
						
						SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
						
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Clerk reacted to damage")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		// Player will refuse to give money when being held up.			
		CASE SHOP_CLERK_STATE_STUBBORN
			IF IS_PLAYER_INSIDE_SHOP()
				IF CHECK_AIMING_SHOP_ROB(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
				AND (currentPlayerWeapon <> WEAPONTYPE_UNARMED)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] SHOP_CLERK_STATE_STUBBORN: Player aimed weapon at stubborn clerk.")
					
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					
					Shop_Robberies.shopClerk.bIsStubborn = TRUE
					
					// The player is holding up the shop.  Save the number of times the player has done this.
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOP_CLERK_STATE_STUBBORN] Update number of times shop has been robbed.")
					UPDATE_TIMES_SHOP_HAS_BEEN_ROBBED()
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN to Dialog: DIALOG_STATE_HOLDUP_STARTS")
					eCurrentDialogState = DIALOG_STATE_HOLDUP_STARTS
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN to Print: PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT")
					eCurrentPrintEnum 	= PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN to Player: PLAYER_STATE_WAIT_FOR_FIRING_CLOSE_TO_CLERK")
					eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_FIRING_CLOSE_TO_CLERK
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_STUBBORN to SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH")
					eCurrentShopClerkState = SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH
					
					// used for friend reactions during friend activity
					SET_BIT(g_bitfieldFriendFlags, ENUM_TO_INT(FRIENDFLAG_IS_ROBBERY_UNDERWAY))
					
					TASK_NEARBY_PEDS_TO_COWER()
				ELIF CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] SHOP_CLERK_STATE_STUBBORN: Player fired a shot near stubborn clerk.")
					
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					TASK_NEARBY_PEDS_TO_FLEE()
					
					UPDATE_SHOP_CLERK_HOLDUP_REACTION()
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN to Dialog: DIALOG_STATE_HOLDUP_STARTS")
					eCurrentDialogState = DIALOG_STATE_HOLDUP_STARTS
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From SHOP_CLERK_STATE_STUBBORN to SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE")
					eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
					
					TASK_NEARBY_PEDS_TO_COWER()
				
				ELIF IS_BITMASK_AS_ENUM_SET( Shop_Robberies.iFlags, SHOPROB_OBJ_DESTROYED )
					IF NOT IS_BITMASK_AS_ENUM_SET( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
						
						IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
						AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
							TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex, "misscommon@response", "numbnuts")
						ENDIF
						
						eCurrentDialogState = DIALOG_STATE_CLERK_THREATENED
						
						SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
						
						TASK_NEARBY_PEDS_TO_FLEE()
						
						SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
						
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Clerk reacted to damage")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Wait for player hold up dialog to complete.
		CASE SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH
			IF eCurrentDialogState >= DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				AND NOT IS_AMBIENT_SPEECH_PLAYING(Shop_Robberies.shopClerk.pedIndex)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH to Dialog: DIALOG_STATE_CLERK_REFUSES_TO_HOLDUP")
					eCurrentDialogState = DIALOG_STATE_CLERK_REFUSES_TO_HOLDUP
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH to Clerk Anim: SHOP_CLERK_ANIM_STATE_STUBBORN")
					eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_STUBBORN
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH to Player: PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP")
					eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH to SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN")
					eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] SHOP_CLERK_STATE_STUBBORN_WAIT_FOR_PLAYER_HOLDUP_DIALOG_TO_FINISH: Current dialog state ",
						//ENUM_TO_INT(eCurrentDialogState), " is less than state ", ENUM_TO_INT(DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION), " DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION")
			ENDIF
		BREAK
		
		// Wait for player to shoot gun near stubborn clerk.
		CASE SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN
			IF CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
			AND IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN: Player fired gun near clerk.")
				UPDATE_SHOP_CLERK_HOLDUP_REACTION()
				
				IF Shop_Robberies.shopClerk.bIsGoingToAttack
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN to Dialog: DIALOG_STATE_HOLDUP_CLERK_ATTACKS")
					eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_ATTACKS
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN to Dialog: DIALOG_STATE_HOLDUP_CLERK_OBEYS")
					eCurrentDialogState = DIALOG_STATE_HOLDUP_CLERK_OBEYS
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_SHOOT_GUN to SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE")
				eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
			ENDIF
		BREAK

		// Wait for clerk to get shot by player.
		CASE SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
			//IF eCurrentCashRegisterScenePhase = CASH_REGISTER_SCENE_PHASE_TAKING_CASH_INTO_BAG
			IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
				IF CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
				AND IS_PLAYER_INSIDE_SHOP()
					IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					AND NOT Shop_Robberies.bMidHoldupLineSaid
						eCurrentDialogState = DIALOG_STATE_PLAYER_HURRY_UP
						Shop_Robberies.bMidHoldupLineSaid = TRUE
//						IF GET_SYNCHRONIZED_SCENE_PHASE(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID) > 0.60
//						AND NOT Shop_Robberies.bMidHoldupLineSaid
//							eCurrentDialogState = DIALOG_STATE_CLERK_MID_HOLDUP
//							Shop_Robberies.bMidHoldupLineSaid = TRUE
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eCurrentCashRegisterScenePhase >= CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS
			AND eCurrentCashRegisterScenePhase < CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG
				// The ped was hurt before releasing the bag.
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())
				IF CHECK_ATTACKED(Shop_Robberies.shopClerk.pedIndex, NULL, TRUE)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Player attacked clerk")
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Or sync scene was interrupted")
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE to SHOP_CLERK_STATE_KICKED_OUT_OF_SYNC_SCENE")
					
					// hmmmmmmmmmm...
//					IF DOES_ENTITY_EXIST(Shop_Robberies.shopTill.objIndex)
//						DELETE_OBJECT(Shop_Robberies.shopTill.objIndex)
//						REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_01)
//						REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_02)
//						REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_03)
//					ENDIF
					
					eCurrentShopClerkState = SHOP_CLERK_STATE_KICKED_OUT_OF_SYNC_SCENE
				ENDIF
			ENDIF
		BREAK
		
		// Player's synched scene animation of getting the cash was interrupted by player.
		CASE SHOP_CLERK_STATE_KICKED_OUT_OF_SYNC_SCENE
			// Stop the synching scene.
			IF eCurrentCashRegisterSynchedSceneState <> CASH_REGISTER_SYNCHED_SCENE_DONE
			AND eCurrentCashRegisterSynchedSceneState <> CASH_REGISTER_SYNCHED_SCENE_CLEANUP
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] Scene state set to CASH_REGISTER_SYNCHED_SCENE_CLEANUP")
				eCurrentCashRegisterSynchedSceneState = CASH_REGISTER_SYNCHED_SCENE_CLEANUP
			ENDIF
			
			// used for friend reactions during friend activity
			CLEAR_BIT(g_bitfieldFriendFlags, ENUM_TO_INT(FRIENDFLAG_IS_ROBBERY_UNDERWAY))
			
			IF NOT IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
				//TASK_CLERK_TO_COWER()
				TASK_CLERK_TO_COWER_IN_BACKROOM()
				
//				IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
//					TASK_SMART_FLEE_PED(Shop_Robberies.shopClerk.pedIndex, PLAYER_PED_ID(), 100, -1)
//				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_KICKED_OUT_OF_SYNC_SCENE to SHOP_CLERK_STATE_WAIT")
				eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT
			ENDIF
		BREAK
		
		// Handle the clerk reacting to being pushed by the player.
		CASE SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH
			IF NOT IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
				TASK_CLERK_TO_LOOK_AT_PLAYER()
				
				eCurrentDialogState = DIALOG_STATE_CLERK_THREATENED
						
				SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
				
				SET_BITMASK_AS_ENUM( Shop_Robberies.iFlags, SHOPROB_DESTROY_REACT )
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH to Clerk Anim: SHOP_CLERK_ANIM_STATE_PUSHED")
				eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_PUSHED
			
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH to SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED")
				eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED
			ENDIF
		BREAK
		
		// Handle the clear reacting to having a gun aimed at him after being pushed.
		CASE SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED
			IF ((CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
					OR CHECK_AIMING_SHOP_ROB(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs))
				AND IS_PLAYER_INSIDE_SHOP())
			AND (currentPlayerWeapon <> WEAPONTYPE_UNARMED)
				IF NOT Shop_Robberies.shopClerk.bIsGoingToAttack
					eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_SCARED
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED to Dialog: DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_SCARED")
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED to SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER")
					eCurrentShopClerkState = SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER
				ELSE
					TASK_SHOP_ROBBERIES_CLERK_TO_AGGRO_PLAYER()
					
					eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_AGGRO
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED to Dialog: DIALOG_STATE_CLERK_REACTS_TO_BUMP_AND_AIM_AGGRO")
			
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_AFTER_BEING_PUSHED to SHOP_CLERK_STATE_WAIT")
					eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT
				ENDIF
				
				Shop_Robberies.shopClerk.bIsPayingAttention = FALSE
			ENDIF
		BREAK
		
		// Handle the clear reacting to having a gun aimed at him after being pushed.
		CASE SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER
			// TASK_CLERK_TO_RAISE_HANDS_AND_COWER()
			TASK_CLERK_TO_COWER_IN_BACKROOM()
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_RAISE_HANDS_AND_COWER to SHOP_CLERK_STATE_DONE")
			eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
		BREAK
		
		// Handle the clear reacting to having a gun aimed at him after being pushed.
		CASE SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN
			IF ((CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
					OR CHECK_AIMING_SHOP_ROB(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs))
				AND IS_PLAYER_INSIDE_SHOP())
			AND (currentPlayerWeapon <> WEAPONTYPE_UNARMED) 
			AND NOT IS_TIMER_STARTED(Shop_Robberies.fleeDelay)
				START_TIMER_NOW(Shop_Robberies.fleeDelay)
				
				IF IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
					TASK_CLERK_TO_COWER_IN_BACKROOM()
					//TASK_CLERK_TO_COWER()
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN to Dialog: DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP")
				eCurrentDialogState = DIALOG_STATE_PLAYER_AIMS_AT_CLERK_AFTER_HOLDUP
			ELIF GET_TIMER_IN_SECONDS_SAFE(Shop_Robberies.fleeDelay) > 10.0
				IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
					TASK_SMART_FLEE_PED(Shop_Robberies.shopClerk.pedIndex, PLAYER_PED_ID(), 100, -1)
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN to SHOP_CLERK_STATE_DONE")
				eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
			ENDIF
		BREAK
		
		// The clerk will react normally to player actions.
		CASE SHOP_CLERK_STATE_DEFAULT_AI
			RESET_CLERK_TO_DEFAULT_AI()
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_CLERK] From SHOP_CLERK_STATE_DEFAULT_AI to SHOP_CLERK_STATE_DONE")
			eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Update cops state.
PROC UPDATE_SHOP_ROBBERIES_COPS()
	IF Shop_Robberies.bAreCopsGoingToAmbush
		SWITCH(eCurrentCopsState)
			CASE COPS_STATE_ALERTED
				IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
					CREATE_AND_TASK_SHOP_ROBBERIES_COPS()
					
					Shop_Robberies.shopCops.iSirenDelay = GET_GAME_TIMER()
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_COPS] Dispatching scripted cops to shop.")
					eCurrentCopsState = COPS_STATE_SIREN_DELAY
				ENDIF
			BREAK
			
			CASE COPS_STATE_SIREN_DELAY
				IF (GET_GAME_TIMER() - Shop_Robberies.shopCops.iSirenDelay) > GET_RANDOM_INT_IN_RANGE(1000, 3000)
					
					IF IS_VEHICLE_DRIVEABLE(Shop_Robberies.shopCops.vehCars[0])
						SET_VEHICLE_SIREN(Shop_Robberies.shopCops.vehCars[0], TRUE)
						//DISTANT_COP_CAR_SIRENS()
						//BLIP_SIREN(Shop_Robberies.shopCops.vehCars[0])
					ENDIF
					
					eCurrentCopsState = COPS_STATE_WAIT_FOR_PLAYER_TO_SEE_COPS
				ENDIF
			BREAK
			
			CASE COPS_STATE_WAIT_FOR_PLAYER_TO_SEE_COPS
				IF ARE_COPS_IN_PLAYER_VIEW() AND IS_ANY_COP_PED_OUT_OF_HIS_CAR()
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_COPS] COPS_STATE_WAIT_FOR_PLAYER_TO_SEE_COPS to COPS_STATE_WARN_PLAYER_TO_SURRENDER")
					eCurrentCopsState = COPS_STATE_WARN_PLAYER_TO_SURRENDER
				ENDIF
			BREAK
			
			CASE COPS_STATE_WARN_PLAYER_TO_SURRENDER
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_COPS] From Cops: COPS_STATE_WARN_PLAYER_TO_SURRENDER to Dialog: DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER")
				eCurrentDialogState = DIALOG_STATE_COPS_WARN_PLAYER_TO_SURRENDER
				
				 // Raise the wanted level to 1, so the cops don't yet shoot at the player.
				 SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_COPS] COPS_STATE_WARN_PLAYER_TO_SURRENDER to COPS_STATE_WAIT_FOR_PLAYER_TO_AGGRO_COPS")
				eCurrentCopsState = COPS_STATE_WAIT_FOR_PLAYER_TO_AGGRO_COPS
			BREAK
			
			CASE COPS_STATE_WAIT_FOR_PLAYER_TO_AGGRO_COPS
				INT iCopCounter, iCopCounter2
				REPEAT iNumOfShopRobberiesCops iCopCounter
					
					IF IS_PED_INJURED(Shop_Robberies.shopCops.pedCops[iCopCounter]) 
					OR ((NOT IS_ENTITY_DEAD(Shop_Robberies.shopCops.pedCops[iCopCounter])) 
						AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Shop_Robberies.shopCops.pedCops[iCopCounter], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
					OR IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex) 
					OR ((NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)) 
						AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(Shop_Robberies.shopClerk.pedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
					OR CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
						
						SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(2, FALSE)
						
						REPEAT iNumOfShopRobberiesCops iCopCounter2
							IF NOT IS_PED_INJURED(Shop_Robberies.shopCops.pedCops[iCopCounter2]) 
								
								TASK_COMBAT_PED(Shop_Robberies.shopCops.pedCops[iCopCounter2], PLAYER_PED_ID())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopCops.pedCops[iCopCounter2], FALSE)
							ENDIF
						ENDREPEAT
						// check for aggro
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_COPS] COPS_STATE_WAIT_FOR_PLAYER_TO_AGGRO_COPS to COPS_STATE_WAIT")
						eCurrentCopsState = COPS_STATE_WAIT
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE COPS_STATE_AGGRO
				// Set the clerk to take out a gun and attack player.
				SET_SHOP_ROBBERIES_COPS_TO_AGGRO_PLAYER_WHEN_WANTED()
				eCurrentCopsState = COPS_STATE_WAIT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:
///    Update player state.
PROC UPDATE_SHOP_ROBBERIES_PLAYER()
	// Get the player's current weapon.
	WEAPON_TYPE currentPlayerWeapon
	IF DOES_ENTITY_EXIST(Shop_Robberies.pedPlayer)
		GET_CURRENT_PED_WEAPON(Shop_Robberies.pedPlayer, currentPlayerWeapon)
	ELSE
		EXIT
	ENDIF
	
	INT iCopCounter2 = 0
	
	SWITCH(eCurrentPlayerState)		
		CASE PLAYER_STATE_ENTER_STORE_WITHOUT_WEAPON
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_ENTER_STORE_WITHOUT_WEAPON to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING
		BREAK
		
		CASE PLAYER_STATE_ENTER_STORE_WITH_BAT
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_ENTER_STORE_BAT to Dialog: PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_ENTER_STORE_BAT to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK
		
		CASE PLAYER_STATE_ENTER_STORE_WITH_TOOL
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_ENTER_STORE_WITH_TOOL to Dialog: DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_ENTER_STORE_WITH_TOOL to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK
		
		CASE PLAYER_STATE_ENTER_STORE_WITH_STICKY
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_ENTER_STORE_WITH_STICKY to Dialog: DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIP")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIP
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_ENTER_STORE_WITH_STICKY to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK

		CASE PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
			IF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) <> WEAPONTYPE_UNARMED)
			AND (currentPlayerWeapon <> WEAPONTYPE_BAT)
			AND (currentPlayerWeapon <> WEAPONTYPE_HAMMER)
			AND (currentPlayerWeapon <> WEAPONTYPE_CROWBAR)
			AND (currentPlayerWeapon <> WEAPONTYPE_DLC_BOTTLE)
			AND IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON to PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP")
				eCurrentPlayerState = PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP
			ENDIF
		BREAK
		
		CASE PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING
			IF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) <> WEAPONTYPE_UNARMED)
			AND IS_PLAYER_INSIDE_SHOP()
				IF (currentPlayerWeapon = WEAPONTYPE_BAT)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING to PLAYER_STATE_EQUIPS_BAT_IN_SHOP")
					eCurrentPlayerState = PLAYER_STATE_EQUIPS_BAT_IN_SHOP
				ELIF (currentPlayerWeapon = WEAPONTYPE_CROWBAR)
				OR (currentPlayerWeapon = WEAPONTYPE_HAMMER)
				OR (currentPlayerWeapon = WEAPONTYPE_DLC_BOTTLE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING to PLAYER_STATE_EQUIPS_TOOL_IN_SHOP")
					eCurrentPlayerState = PLAYER_STATE_EQUIPS_TOOL_IN_SHOP
				ELIF (currentPlayerWeapon = WEAPONTYPE_STICKYBOMB)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING to PLAYER_STATE_EQUIPS_STICKY_IN_SHOP")
					eCurrentPlayerState = PLAYER_STATE_EQUIPS_STICKY_IN_SHOP
				ELIF (currentPlayerWeapon <> WEAPONTYPE_PETROLCAN)
				AND (currentPlayerWeapon <> WEAPONTYPE_OBJECT)
				AND (currentPlayerWeapon <> WEAPONTYPE_BRIEFCASE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_ANYTHING to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
					eCurrentPlayerState = PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP					
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP to Dialog: DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS")
			eCurrentDialogState = DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP to Clerk Anim: SHOP_CLERK_ANIM_STATE_WORRIED")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WORRIED
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_EQUIPS_WEAPON_IN_SHOP to PLAYER_STATE_DONE")
			eCurrentPlayerState = PLAYER_STATE_DONE
		BREAK
		
		CASE PLAYER_STATE_EQUIPS_BAT_IN_SHOP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_BAT_IN_SHOP to Dialog: DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_BAT_EQUIP
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_BAT_IN_SHOP to Clerk Anim: SHOP_CLERK_ANIM_STATE_FIRE")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_FIRE
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_EQUIPS_BAT_IN_SHOP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK
		
		CASE PLAYER_STATE_EQUIPS_TOOL_IN_SHOP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_TOOL_IN_SHOP to Dialog: DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_TOOL_IN_SHOP to Clerk Anim: SHOP_CLERK_ANIM_STATE_FIRE")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_FIRE
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_EQUIPS_TOOL_IN_SHOP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK
		
		CASE PLAYER_STATE_EQUIPS_STICKY_IN_SHOP
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_STICKY_IN_SHOP to Dialog: DIALOG_STATE_CLERK_REACTS_TO_TOOL_EQUIP")
			eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_TO_STICKY_EQUIP
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_EQUIPS_STICKY_IN_SHOP to Clerk Anim: SHOP_CLERK_ANIM_STATE_FIRE")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_FIRE
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_EQUIPS_STICKY_IN_SHOP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON")
			eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_EQUIP_WEAPON
		BREAK		
		
		CASE PLAYER_STATE_WAIT_FOR_FIRING_CLOSE_TO_CLERK			
			IF CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
				UPDATE_SHOP_CLERK_HOLDUP_REACTION()
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_WAIT_FOR_FIRING_CLOSE_TO_CLERK to Clerk: SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE")
				eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_GET_SHOT_IN_SYNC_SCENE
			ENDIF
		BREAK
		
		CASE PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP
			IF NOT IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP: Player has left the shop.")
				
				IF NOT Shop_Robberies.bAreCopsGoingToAmbush
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP to Update: UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL")
					eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP to PLAYER_STATE_DONE.")
					eCurrentPlayerState = PLAYER_STATE_DONE
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_STOP_AFTER_LEAVING_SHOP.")
					eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_STOP_AFTER_LEAVING_SHOP
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYER_STATE_WAIT_FOR_PLAYER_TO_STOP_AFTER_LEAVING_SHOP
			IF (GET_ENTITY_SPEED(Shop_Robberies.pedPlayer) < 0.1) AND NOT HAS_PLAYER_LEFT_COP_SAFE_ZONE()  
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] PLAYER_STATE_WAIT_FOR_PLAYER_TO_STOP_AFTER_LEAVING_SHOP: Cops being tasked to arrest player.")
				SET_SHOP_ROBBERIES_COPS_TO_ARREST_PLAYER()
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST.")
				eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST
			ELIF HAS_PLAYER_LEFT_COP_SAFE_ZONE() AND NOT IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SHOP_AFTER_HOLDUP to PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST.")
				eCurrentPlayerState = PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST
			ENDIF
		BREAK
		
		CASE PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST
			IF (HAS_PLAYER_LEFT_COP_SAFE_ZONE()  AND  NOT IS_PLAYER_INSIDE_SHOP())
				
				REPEAT iNumOfShopRobberiesCops iCopCounter2
					IF NOT IS_PED_INJURED(Shop_Robberies.shopCops.pedCops[iCopCounter2]) 
						TASK_COMBAT_PED(Shop_Robberies.shopCops.pedCops[iCopCounter2], PLAYER_PED_ID())
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Shop_Robberies.shopCops.pedCops[iCopCounter2], FALSE)
					ENDIF
				ENDREPEAT
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST: Player has left cop safe zone.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From Player: PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST to Update: UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES_PLAYER] From  PLAYER_STATE_WAIT_FOR_PLAYER_TO_LEAVE_SAFE_ZONE_IF_COPS_EXIST to PLAYER_STATE_DONE")
				eCurrentPlayerState = PLAYER_STATE_DONE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Loop for updating Shop Robberies.
PROC UPDATE_SHOP_HOLD_UP_STATE()
	SWITCH (eCurrentShopRobberiesHoldUpState)
		CASE SHOP_ROBBERIES_HOLDUP_STATE_BEFORE
			
			// Check if clerk is kicked out of the position
			IF NOT bHasShopClerkBeenPushed
				IF HAS_SHOP_CLERK_BEEN_PUSHED()
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_HOLD_UP_STATE] From Hold Up: SHOP_ROBBERIES_HOLDUP_STATE_BEFORE to Dialog: DIALOG_STATE_PLAYER_BUMPS_CLERK_BEFORE_HOLDUP")
					eCurrentDialogState = DIALOG_STATE_PLAYER_BUMPS_CLERK_BEFORE_HOLDUP
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_HOLD_UP_STATE] From Hold Up: SHOP_ROBBERIES_HOLDUP_STATE_BEFORE to Clerk: SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH")
					eCurrentShopClerkState = SHOP_CLERK_STATE_REACTS_TO_PLAYER_PUSH
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_HOLD_UP_STATE] From Hold Up: SHOP_ROBBERIES_HOLDUP_STATE_BEFORE to Print: PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT")
					eCurrentPrintEnum = PRINT_HELP_HOLDUP_STORE_CLEAR_PRINT
				
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_HOLD_UP_STATE] From Hold Up: SHOP_ROBBERIES_HOLDUP_STATE_BEFORE to Update: UPDATE_SHOP_ROBBERIES_DONE")
					eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_HOLD_UP_STATE] From SHOP_ROBBERIES_HOLDUP_STATE_BEFORE to SHOP_ROBBERIES_HOLDUP_STATE_DONE")
					eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE
					
					bHasShopClerkBeenPushed = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE SHOP_ROBBERIES_HOLDUP_STATE_ON
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Loop for updating Shop Robberies.
PROC UPDATE_SHOP_ROBBERIES()
	// Update entities' states.
	UPDATE_SHOP_HOLD_UP_STATE()
	UPDATE_SHOP_ROBBERIES_PLAYER()
	UPDATE_SHOP_ROBBERIES_COPS()
	UPDATE_CASH_BAG()
	UPDATE_SHOP_CLERK()
	UPDATE_PLAYER_ROBBING_TILL()
	//IF NOT bOnAnyMission
		UPDATE_SHOP_PATRON()
	//ENDIF
	
	UPDATE_SHOP_SNACKS()
	UPDATE_WANTED_LEVEL_MONITOR()
	
	UPDATE_DYNAMIC_HOLDUP_ANIM_REQUEST()
	UPDATE_DYNAMIC_SHOP_ROB_REQUESTS()
	UPDATE_BUDDIES_FOR_SHOP_ROB()
	
	SHOP_ROB_PROCESS_ENTITY_DAMAGED_EVENTS()
	
	IF NOT IS_SELECTOR_CAM_ACTIVE()
		// Update dialog state.
		UPDATE_DIALOG()
		
		// Update print help state.
		UPDATE_PRINT_HELPS()
	ENDIF
	
	// Get the player's current weapon.
	WEAPON_TYPE currentPlayerWeapon
	IF DOES_ENTITY_EXIST(Shop_Robberies.pedPlayer)
		GET_CURRENT_PED_WEAPON(Shop_Robberies.pedPlayer, currentPlayerWeapon)
	ELSE
		EXIT
	ENDIF
	
	bPlayerAimingAtClerk = FALSE
	
	//Check if player has aimed a gun at the Staff
	IF NOT IS_PED_INJURED(Shop_Robberies.shopClerk.pedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		WEAPON_TYPE CurrentWeapon
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
			IF CurrentWeapon != WEAPONTYPE_UNARMED
			AND CurrentWeapon != WEAPONTYPE_OBJECT
			AND CurrentWeapon != WEAPONTYPE_FLARE
				IF ( IS_PLAYER_FREE_AIMING(PLAYER_ID()) AND IS_PED_FACING_PED(PLAYER_PED_ID(), Shop_Robberies.shopClerk.pedIndex, 45.0) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), Shop_Robberies.shopClerk.pedIndex, <<5.0, 5.0, 2.0>>) )
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), Shop_Robberies.shopClerk.pedIndex)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), Shop_Robberies.shopClerk.pedIndex)
					IF NOT IS_CELLPHONE_CAMERA_IN_USE()
					AND NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE()
						bPlayerAimingAtClerk = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Do Head Additive Anim
	IF bPlayerAimingAtClerk
		IF NOT bDoHeadAdditive
		AND IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DYN_REQUESTED)
			WEAPON_TYPE PlayerWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
			WEAPON_GROUP PlayerWeaponGroup = GET_WEAPONTYPE_GROUP(PlayerWeapon)
			IF PlayerWeaponGroup = WEAPONGROUP_PISTOL
				sHeadAdditiveAnim = "hold_up_head_additive_pistol"
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY|AF_ADDITIVE)
			ELIF PlayerWeaponGroup = WEAPONGROUP_MG
			OR PlayerWeaponGroup = WEAPONGROUP_RIFLE
			OR PlayerWeaponGroup = WEAPONGROUP_SHOTGUN
			OR PlayerWeaponGroup = WEAPONGROUP_SNIPER
			OR PlayerWeaponGroup = WEAPONGROUP_SMG
				sHeadAdditiveAnim = "hold_up_head_additive_rifle"
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY|AF_ADDITIVE)
			ELSE
				sHeadAdditiveAnim = ""
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] head additive anim processed")
			
			bDoHeadAdditive = TRUE
		ENDIF
	ELSE
		IF bDoHeadAdditive
			IF NOT IS_STRING_NULL_OR_EMPTY(sHeadAdditiveAnim)
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim)
					STOP_ANIM_TASK(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim)
				ENDIF
			ENDIF
			bDoHeadAdditive = FALSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] head additive anim stopped")
		ENDIF
	ENDIF
	
	SWITCH(eCurrentShopRobberiesUpdateState)
	
		// Wait for player to enter store.
		CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP
			IF IS_PLAYER_INSIDE_SHOP()
				// dynamic request of assets
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP: Player is in store, and already wanted!.")
					
					eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_PLAYER_ALREADY_WANTED
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP: Player is in store.")
					
					eCurrentPrintEnum = PRINT_HELP_HOLDUP_STORE
					
					// Set the clerk to look at the player.
					eCurrentShopClerkLookState = SHOP_CLERK_LOOK_STATE_AT_PLAYER_AFTER_ENTERING_SHOP
					
					IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
						IF NOT IS_PED_IN_ANY_VEHICLE(Shop_Robberies.pedPlayer)
							// The clerk should react differently depending on whether or not the player enters the store with a weapon in hand.
							IF (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_UNARMED)
							OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_OBJECT)
							OR (GET_PEDS_CURRENT_WEAPON(Shop_Robberies.pedPlayer) = WEAPONTYPE_BRIEFCASE)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITHOUT_WEAPON")
								eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITHOUT_WEAPON
							
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_GREET")
								eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_GREET
								
							ELIF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_IN_SHOP_ARMED)
								IF (currentPlayerWeapon = WEAPONTYPE_BAT)
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_BAT")
									eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_BAT
									
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
									eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
								ELIF (currentPlayerWeapon = WEAPONTYPE_CROWBAR)
								OR (currentPlayerWeapon = WEAPONTYPE_HAMMER)
								OR (currentPlayerWeapon = WEAPONTYPE_DLC_BOTTLE)
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_BAT")
									eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_TOOL
									
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
									eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
								ELIF (currentPlayerWeapon = WEAPONTYPE_STICKYBOMB)
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_STICKY")
									eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_STICKY
									
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
									eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
								ELSE
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_WORRIED")
									eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_WORRIED
								ENDIF
								
								SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_IN_SHOP_ARMED)
							ENDIF
						ELSE
							// The clerk should react in shock from the player entering the shop with a vehicle.
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED")
							eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED
						ENDIF
					ENDIF
//				ELSE // subsequent entries into the shop
//					IF NOT IS_ENTITY_DEAD(Shop_Robberies.pedPlayer)
//					AND 
//						IF NOT IS_PED_IN_ANY_VEHICLE(Shop_Robberies.pedPlayer)
//							// The clerk should react differently depending on whether or not the player enters the store with a weapon in hand.
//							IF (currentPlayerWeapon = WEAPONTYPE_BAT)
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_BAT")
//								eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_BAT
//								
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
//								eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
//							ELIF (currentPlayerWeapon = WEAPONTYPE_CROWBAR)
//							OR (currentPlayerWeapon = WEAPONTYPE_HAMMER)
//							OR (currentPlayerWeapon = WEAPONTYPE_DLC_BOTTLE)
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_BAT")
//								eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_TOOL
//								
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
//								eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
//							ELIF (currentPlayerWeapon = WEAPONTYPE_STICKYBOMB)
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to Player: PLAYER_STATE_ENTER_STORE_WITH_STICKY")
//								eCurrentPlayerState = PLAYER_STATE_ENTER_STORE_WITH_STICKY
//								
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
//								eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
//							ELSE
//								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_WORRIED")
//								eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_WORRIED
//							ENDIF
//						ELSE
//							// The clerk should react in shock from the player entering the shop with a vehicle.
//							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED")
//							eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED
//						ENDIF
//					ELSE
//						eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
//					ENDIF
				ENDIF
			ELIF IS_PLAYER_VEHICLE_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP: Player vehicle is in store.")
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: IS_PLAYER_VEHICLE_INSIDE_SHOP to Print: PRINT_HELP_HOLDUP_STORE.")
				eCurrentPrintEnum = PRINT_HELP_HOLDUP_STORE
				
				// The clerk should react in shock from the player entering the shop with a vehicle.
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP to UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED
			ELIF NOT IS_PLAYER_INSIDE_SHOP()
			AND CHECK_SHOT_NEAR(Shop_Robberies.pedPlayer, Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.aggroArgs)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP: Player shot at the clerk, going to already wanted.")
					
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_PLAYER_ALREADY_WANTED
			ENDIF
		BREAK
		
		// Clerk greets player.
		CASE UPDATE_SHOP_ROBBERIES_CLERK_GREET
			IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_NON_AGG_ENTRY)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CLERK_GREET to Dialog: DIALOG_STATE_CLERK_GREET_WHEN_PLAYER_ENTERS")
				eCurrentDialogState = DIALOG_STATE_CLERK_GREET_WHEN_PLAYER_ENTERS
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CLERK_GREET to Clerk Anim: SHOP_CLERK_ANIM_STATE_GREET")
				eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_GREET
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] setting flag for player entering shop nicely")
				SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_NON_AGG_ENTRY)
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_CLERK_GREET to UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP")
			eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP		
		BREAK
	
		// Clerk reacts to player being armed.
		CASE UPDATE_SHOP_ROBBERIES_CLERK_WORRIED
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CLERK_WORRIED to Dialog: DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS")
			eCurrentDialogState = DIALOG_STATE_CLERK_WORRIED_WHEN_PLAYER_ENTERS
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CLERK_WORRIED to Clerk Anim: SHOP_CLERK_ANIM_STATE_WORRIED")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_WORRIED
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_CLERK_WORRIED to UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_GUN")
			eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
		BREAK
		
		// Clerk reacts to player entering shop with vehicle.
		CASE UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED to Dialog: DIALOG_STATE_CLERK_SHOCKED_WHEN_PLAYER_ENTERS")
			eCurrentDialogState = DIALOG_STATE_CLERK_SHOCKED_WHEN_PLAYER_ENTERS
			
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] Jumping from UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED to SHOP_CLERK_ANIM_STATE_SHOCKED.")
			eCurrentShopClerkAnimState = SHOP_CLERK_ANIM_STATE_SHOCKED
		
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] Update: Jumping from UPDATE_SHOP_ROBBERIES_CLERK_SHOCKED to UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_AIM_OR_SHOOT_GUN")
			eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
		BREAK
		
		// Check if player leaves shop
		CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP
			IF NOT IS_PLAYER_INSIDE_SHOP()
			AND NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_IN_SHOP_ARMED)
				
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP
			ENDIF
		BREAK
		
		// Wait for player to hold up store.
		CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG
			IF eCurrentDialogState >= DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION
				IF Shop_Robberies.shopClerk.bIsGoingToAttack
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG to UPDATE_SHOP_ROBBERIES_CLERK_AGGRO")
						eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CLERK_AGGRO
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG to UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START")
					eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START
				ENDIF
				
				// SPECIAL CASE: Check if the player is robbing LIQ3 right after completing Taxi Take It Easy.
				eCurrentShopPatronState = SHOP_PATRON_STATE_CHECK_AVAILABILITY
			ELSE
				//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_WAIT_FOR_HOLDUP_DIALOG: Current dialog state ",
						//ENUM_TO_INT(eCurrentDialogState), " is less than state ", ENUM_TO_INT(DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION), " DIALOG_STATE_HOLDUP_WAIT_FOR_CLERK_REACTION")
			ENDIF
		BREAK
		
		// Clerk takes out a gun instead of giving money to player.
		CASE UPDATE_SHOP_ROBBERIES_CLERK_AGGRO
			IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_CLERK_AGGRO_SAID)
				TASK_SHOP_ROBBERIES_CLERK_TO_AGGRO_PLAYER()
				
				// used for friend reactions during friend activity
				CLEAR_BIT(g_bitfieldFriendFlags, ENUM_TO_INT(FRIENDFLAG_IS_ROBBERY_UNDERWAY))
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_CLERK_AGGRO to UPDATE_SHOP_ROBBERIES_DONE")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
			ENDIF
		BREAK
		
		// Start playing clerk cash register animation.
		CASE UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START
			IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_HLD_REQUESTED)
				IF IS_CASH_REGISTER_SYNCHED_SCENE_DONE()
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START: Cash register synched scene is done.")
				
					// TODO: Pick up what to do with the clerk from here, as he currently just freezes at the end of the synched anim.
					//			Account for the clerk having been interrupted.
					IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
						IF NOT IS_PED_RAGDOLL(Shop_Robberies.shopClerk.pedIndex)
							IF IS_PLAYER_INSIDE_SHOP()
								IF Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID >= 0
									IF IS_SYNCHRONIZED_SCENE_RUNNING(Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
										TASK_PLAY_ANIM(Shop_Robberies.shopClerk.pedIndex, Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict, "handsup_base", SLOW_BLEND_IN, 
														NORMAL_BLEND_OUT, -1, AF_LOOPING)
										
										CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START to Clerk: SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN")
										eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN
									ENDIF
								ENDIF
							ELSE
								TASK_SMART_FLEE_PED(Shop_Robberies.shopClerk.pedIndex, PLAYER_PED_ID(), 100.0, -1)
								eCurrentShopClerkState = SHOP_CLERK_STATE_WAIT_FOR_PLAYER_TO_HOLDUP_AGAIN
							ENDIF
						ENDIF
					ENDIF
					
					// used for friend reactions during friend activity
					CLEAR_BIT(g_bitfieldFriendFlags, ENUM_TO_INT(FRIENDFLAG_IS_ROBBERY_UNDERWAY))
					
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START to UPDATE_SHOP_ROBBERIES_DONE")
					eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
				ENDIF
			ENDIF
		BREAK
		
		// Add a wanted level once the player leaves the store.
		CASE UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL
			SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(2, FALSE)
			
			// Set the scripted cops to start firing.
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL to Cops: COPS_STATE_AGGRO")
			eCurrentCopsState = COPS_STATE_AGGRO
			
			IF eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL back to cash register start")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_CASH_REGISTER_START
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_RAISE_WANTED_LEVEL to UPDATE_SHOP_ROBBERIES_DONE")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
			ENDIF
		BREAK
		
		// Wait for player to enter shop before clerk can recognize him.
		CASE UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED
			IF IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED: Player entered shop.  Clerk will recognize him.")
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED to UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED
			ENDIF
		BREAK
		
		// Clerk reacts to recognizing player for robbing the shop.
		CASE UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED
			
			IF NOT IS_TIMER_STARTED(Shop_Robberies.timerWantedDelay)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED: Clerk recognized player.")
				
				// Task clerk to react to his recognizing player.
				IF NOT Shop_Robberies.shopClerk.bIsGoingToAttack
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to Dialog: DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED")
					eCurrentDialogState = DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to Dialog: DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO")
					eCurrentDialogState = DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_AGGRO
				ENDIF
				
				START_TIMER_NOW(Shop_Robberies.timerWantedDelay)
				
			ELIF GET_TIMER_IN_SECONDS(Shop_Robberies.timerWantedDelay) > 5
				SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to UPDATE_SHOP_ROBBERIES_DONE")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
				
			ELIF GET_TIMER_IN_SECONDS(Shop_Robberies.timerWantedDelay) > 1.5
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF eCurrentShopClerkState != SHOP_CLERK_STATE_DONE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to Clerk: SHOP_CLERK_STATE_DONE.")
						eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
						
						// Task clerk to react to his recognizing player.
						IF NOT Shop_Robberies.shopClerk.bIsGoingToAttack
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED: Clerk cowers and calls the cops, triggering wanted level.")
							TASK_CLERK_TO_COWER()
						ELSE
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED: Clerk pulls gun on player and starts shooting.")
							TASK_SHOP_ROBBERIES_CLERK_TO_AGGRO_PLAYER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE UPDATE_SHOP_ROBBERIES_PLAYER_ALREADY_WANTED
			IF NOT IS_TIMER_STARTED(Shop_Robberies.timerWantedDelay)
				//TASK_CLERK_TO_COWER()
				TASK_CLERK_TO_COWER_IN_BACKROOM()
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to Dialog: DIALOG_STATE_CLERK_RECOGNIZES_PLAYER_SCARED")
				eCurrentDialogState = DIALOG_STATE_CLERK_REACTS_PLAYER_ALREADY_WANTED
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From Update: UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to Clerk: SHOP_CLERK_STATE_DONE.")
				eCurrentShopClerkState = SHOP_CLERK_STATE_DONE
				
				START_TIMER_NOW(Shop_Robberies.timerWantedDelay)
			
			ELIF GET_TIMER_IN_SECONDS(Shop_Robberies.timerWantedDelay) > 3
				
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->UPDATE_SHOP_ROBBERIES] From UPDATE_SHOP_ROBBERIES_PLAYER_RECOGNIZED to UPDATE_SHOP_ROBBERIES_DONE")
				eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_DONE
			ENDIF
		BREAK
		
		CASE UPDATE_SHOP_ROBBERIES_DONE
			// dynamic release of assets
			// in relation to 1086563 - anims were getting released when still needed
			// the gist is that the player can still do a till robbery during this state, thus those anims are still needed
			// TODO: maybe hard release after a till robbery?  or set a flag if either kind of robbery are succesfully completed
			//eDynamicRequestState = DR_HARD_RELEASE
		BREAK
	ENDSWITCH
ENDPROC

// -------------------------------------------------
// 	E N D  ROBBERIES UPDATE PROCEDURES AND FUNCTIONS
// -------------------------------------------------





// ----------------------------------------------------
// 	ROBBERIES CLEANUP PROCEDURES AND FUNCTIONS
// ----------------------------------------------------


/// PURPOSE:
///    Clean up all assets used in Shop Robberies.
PROC CLEANUP_SHOP_ROBBERIES_ENTITIES()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEANUP_SHOP_ROBBERIES_ASSETS] Procedure called.")
	
	// Clear clerk's tasks.
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex)
		CLEAR_PED_TASKS(Shop_Robberies.shopClerk.pedIndex)
	ENDIF
	
	// Clerk no longer needed.
	IF DOES_ENTITY_EXIST(Shop_Robberies.shopClerk.pedIndex)
		RESET_CLERK_TO_DEFAULT_AI()
		SET_PED_AS_NO_LONGER_NEEDED(Shop_Robberies.shopClerk.pedIndex)
	ENDIF
	
	// Taxi Passenger no longer needed.
	IF DOES_ENTITY_EXIST(Shop_Robberies.shopPatron.pedIndex)
		SET_PED_AS_NO_LONGER_NEEDED(Shop_Robberies.shopPatron.pedIndex)
	ENDIF
	
	// Stop the cash bag from being in a synched scene.
	IF NOT IS_ENTITY_DEAD(Shop_Robberies.shopCashBag.objIndex)
		IF eCurrentCashRegisterScenePhase >= CASH_REGISTER_SCENE_PHASE_GRABBING_BAG
			STOP_SYNCHRONIZED_ENTITY_ANIM(Shop_Robberies.shopCashBag.objIndex, NORMAL_BLEND_OUT, TRUE)
		
			// Apply just a bit of force, so if the cash bag is kicked out of the scene, it falls to the ground.
			APPLY_FORCE_TO_ENTITY(Shop_Robberies.shopCashBag.objIndex, APPLY_TYPE_IMPULSE, << 0.0, 0.0, -0.1 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
		ENDIF
	ENDIF
	
	// Cash bag no longer needed.
	IF DOES_ENTITY_EXIST(Shop_Robberies.shopCashBag.objIndex)
		SET_OBJECT_AS_NO_LONGER_NEEDED(Shop_Robberies.shopCashBag.objIndex)
	ENDIF
	
	IF DOES_ENTITY_EXIST(Shop_Robberies.shopTill.objIndex)
		DELETE_OBJECT(Shop_Robberies.shopTill.objIndex)
		REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_01)
		REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_02)
		REMOVE_MODEL_HIDE(Shop_Robberies.cashRegisterSynchedScene.vPos, 0.5, PROP_TILL_03)
	ENDIF
	
	// Remove cash bag pickup.
	IF DOES_PICKUP_EXIST(Shop_Robberies.shopCashBag.pickupIndex)
		REMOVE_PICKUP(Shop_Robberies.shopCashBag.pickupIndex)
	ENDIF

	IF IS_TIMER_STARTED(Shop_Robberies.timerWantedDelay)
		CANCEL_TIMER(Shop_Robberies.timerWantedDelay)
	ENDIF
	
	// Free up animations.
	IF Shop_Robberies.bAnimsRequested
		
		IF NOT bOnAnyMission
			REMOVE_ANIM_DICT(Shop_Robberies.shopClerk.szReactionAnimDict)
		ENDIF
		
		Shop_Robberies.bAnimsRequested = FALSE
	ENDIF
ENDPROC


/// PURPOSE:
///    Clean up and terminate the Shop Robberies script.
PROC CLEANUP_SHOP_ROBBERIES()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEANUP_SHOP_ROBBERIES] Procedure called.")
	
	// Store the time this robbery took place, only if the player held up the store.
	IF eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON	
	OR eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON
	OR ePlayerRobTillState = PLAYERROBTILL_DONE
	OR (IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex) AND eCurrentShopRobberiesState = SHOP_ROBBERIES_UPDATE)
		IF eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_ON")
		ENDIF
		IF eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "eCurrentShopRobberiesHoldUpState = SHOP_ROBBERIES_HOLDUP_STATE_DONE_AFTER_ON")
		ENDIF
		IF ePlayerRobTillState = PLAYERROBTILL_DONE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "ePlayerRobTillState = PLAYERROBTILL_DONE")
		ENDIF
		IF (IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex) AND eCurrentShopRobberiesState = SHOP_ROBBERIES_UPDATE)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "(IS_ENTITY_DEAD(Shop_Robberies.shopClerk.pedIndex) AND eCurrentShopRobberiesState = SHOP_ROBBERIES_UPDATE)")
		ENDIF
		
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> setting last robbed time")
//		g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[eCurrentShop] = GET_GAME_TIMER()
//		g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop] = GET_GAME_TIMER()
		g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[eCurrentShop] = GET_CLOCK_HOURS()
		g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop] = GET_CLOCK_DAY_OF_MONTH()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> time shop robbed set to ", g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[eCurrentShop])
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> time player robbed set to ", g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop])
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc-> NOT setting last robbed time")
	ENDIF
	
	IF NETWORK_IS_SIGNED_ONLINE()
	AND HAS_IMPORTANT_STATS_LOADED()
	AND Shop_Robberies.bWriteToLeaderboard
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "online, writing to social club leaderboard")
		WRITE_SHOP_ROBBERIES_SCLB_DATA(eCurrentShop, bHasNumOfTimesShopRobbedUpdated, iShopRobCashAmount)
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "either not online or player didn't rob, so not writing to SCLB")
	ENDIF
	
	CLEANUP_SHOP_ROBBERIES_ENTITIES()
	
	// For safety, make sure we reset certain states.
	bHasNumOfTimesShopRobbedUpdated	= FALSE
	bHasCounterWarningDialogPlayed  = FALSE
	bHasBackroomWarningDialogPlayed = FALSE
	bHasShopClerkBeenPushed			= FALSE
	
	// Reset the max wanted level.
	IF NOT bOnAnyMission
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEANUP_SHOP_ROBBERIES] Resetting the player's max possible wanted level back to ", iMaxDefaultWantedLevel)
		SET_MAX_WANTED_LEVEL(iMaxDefaultWantedLevel)
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEANUP_SHOP_ROBBERIES] Player is on mission, not resetting wanted level")
	ENDIF
	
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiShopCLosed)
	
	#IF IS_DEBUG_BUILD
		//CLEANUP_ZVOLUME_WIDGETS()
	#ENDIF
	
	REMOVE_HELP_FROM_FLOW_QUEUE("SHR_HOLDUP_1")
	REMOVE_HELP_FROM_FLOW_QUEUE("SHR_SNK_TUT")
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_HOLDUP_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_SNK_TUT")
		CLEAR_HELP()
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_ITEMS_CREATED)
		IF DOES_ENTITY_EXIST(oiBoughtItem[0])
		AND DOES_ENTITY_EXIST(oiBoughtItem[1])
		AND DOES_ENTITY_EXIST(oiBoughtItem[2])
			DELETE_OBJECT(oiBoughtItem[0])
			DELETE_OBJECT(oiBoughtItem[1])
			DELETE_OBJECT(oiBoughtItem[2])
		ENDIF
	ENDIF
	
	IF eDynamicHoldUpRequestState = DR_EXIT_SHOP
		REMOVE_ANIM_DICT(Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict)
	ENDIF
	
	CLEANUP_LOAD_QUEUE_LIGHT(sLoadQueue)
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->CLEANUP_SHOP_ROBBERIES] Terminating Shop Robberies script.")
	TERMINATE_THIS_THREAD()
ENDPROC


/// PURPOSE:
///    Returns whether the current player will be recognized by the clerk when entering the shop.
FUNC BOOL HAS_SHOP_CLERK_FORGOTTEN_PLAYER()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->HAS_SHOP_CLERK_FORGOTTEN_PLAYER] Checking if clerk will recognize player.")
	
	// If the current player character is different than the last character who robbed the shop, then clerk won't recognize player.
	IF Shop_Robberies.iPlayerCharacterIndex <> g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[eCurrentShop]
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->HAS_SHOP_CLERK_FORGOTTEN_PLAYER] Current player character is ", Shop_Robberies.iPlayerCharacterIndex, 
				", which is different than the last player character who robbed this shop: ", g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops[eCurrentShop],
				".  Clerk will NOT recognize player.")
		RETURN TRUE
	ENDIF
	
	INT iDaysSinceThisPlayerLastRobbedThisShop = GET_CLOCK_DAY_OF_MONTH() - g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop]
	
	// Ensure this value is greater than the time it takes for the shop to be available after a robbery.
	//fHoursBeforeShopClerkForgetsLastPlayerRobber = 48
	iDaysBeforeShopClerkForgetsLastPlayerRobber = 2
	
	#IF IS_DEBUG_BUILD
		//fHoursBeforeShopClerkForgetsLastPlayerRobber = 48 // 0.01
		iDaysBeforeShopClerkForgetsLastPlayerRobber = 2
	#ENDIF
	
	IF iDaysSinceThisPlayerLastRobbedThisShop < 0
		iDaysSinceThisPlayerLastRobbedThisShop = (GET_CLOCK_DAY_OF_MONTH()) + 31
		iDaysSinceThisPlayerLastRobbedThisShop = iDaysSinceThisPlayerLastRobbedThisShop - g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop]
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "Overriding time, days since this shop was robbed = ", iDaysSinceThisPlayerLastRobbedThisShop)
	ENDIF
	
	//IF CONVERT_MILLISECONDS_TO_GAME_HOURS(iMilliSecsSinceThisPlayerLastRobbedThisShop) < fHoursBeforeShopClerkForgetsLastPlayerRobber
	IF iDaysSinceThisPlayerLastRobbedThisShop < iDaysBeforeShopClerkForgetsLastPlayerRobber
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_FORGOTTEN_PLAYER] Days passed since ", ENUM_TO_INT(eCurrentShop), " shop was last robbed by current player is ", 
					iDaysSinceThisPlayerLastRobbedThisShop, " so the clerk WILL RECOGNIZE the player!")
		RETURN FALSE
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_CLERK_FORGOTTEN_PLAYER] Days passed since ", ENUM_TO_INT(eCurrentShop), " shop was last robbed by current player is ", 
					iDaysSinceThisPlayerLastRobbedThisShop, " so the clerk HAS FORGOTTEN the player!")
	ENDIF
	
	RETURN TRUE
ENDFUNC

 
/// PURPOSE:
///    Returns whether the player has returned too recently to a shop he robbed before it reactivates.
FUNC BOOL HAS_SHOP_BEEN_ROBBED_TOO_RECENTLY_TO_LAUNCH()
	INT iHoursSinceShopWasRobbed = GET_CLOCK_HOURS() - g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[eCurrentShop]
	INT iDaysSinceShopWasRobbed = GET_CLOCK_DAY_OF_MONTH() - g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer[eCurrentShop]
	
	BOOL bEndOfDayOverride
	
	//fHoursBeforeShopCanBeRobbedAgain = 1.5 //3.0
	iHoursBeforeShopCanBeRobbedAgain = 2
	
	#IF IS_DEBUG_BUILD
		//fHoursBeforeShopCanBeRobbedAgain = 1.5 //3.0// 0.01
		iHoursBeforeShopCanBeRobbedAgain = 2
	#ENDIF
	
	IF iHoursSinceShopWasRobbed < 0
		iHoursSinceShopWasRobbed = (GET_CLOCK_HOURS()) + 24
		iHoursSinceShopWasRobbed = iHoursSinceShopWasRobbed - g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed[eCurrentShop]
		bEndOfDayOverride = TRUE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "Overriding time, hours since this shop was robbed = ", iHoursSinceShopWasRobbed)
	ENDIF
	
	//IF CONVERT_MILLISECONDS_TO_GAME_HOURS(iMilliSecsSinceShopWasRobbed) < fHoursBeforeShopCanBeRobbedAgain
	IF (iHoursSinceShopWasRobbed < iHoursBeforeShopCanBeRobbedAgain AND iDaysSinceShopWasRobbed = 0)
	OR (iHoursSinceShopWasRobbed < iHoursBeforeShopCanBeRobbedAgain AND bEndOfDayOverride)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_BEEN_ROBBED_TOO_RECENTLY_TO_LAUNCH] Hours passed since ", ENUM_TO_INT(eCurrentShop), " shop was robbed is ", 
					iHoursSinceShopWasRobbed, " so it's too soon to rob again.")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->HAS_SHOP_BEEN_ROBBED_TOO_RECENTLY_TO_LAUNCH] Hours passed since ", ENUM_TO_INT(eCurrentShop), " shop was robbed is ", 
					iHoursSinceShopWasRobbed, " so it's ready to be robbed again!")
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check any instances that requires the termination of Shop Robberies.
FUNC BOOL SHOULD_SHOP_ROBBERIES_CLEAN_UP()
	// Check if another mission is running.
//	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
//		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] Another mission is running.  Terminating Shop Robberies script.")
//		RETURN TRUE
//	ENDIF
	
	
	/* Disabling this, as it's causing some issues with shops not activating.  Will get back to this if it's a problem later.
	// Make sure multiple copies of this script aren't running.
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ShopRobberies")) > 1
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] Number of Shop Robberies scripts running are ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ShopRobberies")), ".  Terminating script.")
		RETURN TRUE
	ENDIF
	*/
	
	// Make sure our flowflag is active.
	IF (g_savedGlobals.sFlow.isGameflowActive)
    	IF NOT (Get_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_SHOP_ROBBERIES))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] Flowflag is not active. Cleaning up Shop Robberies.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Check if the player has left the Shop Robberies area.
	IF HAS_PLAYER_LEFT_SHOP_ACTIVE_AREA()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] Player has left the shop active area.  Cleaning up Shop Robberies.")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] World point is not within brain activation range.  Cleaning up Shop Robberies.")
		RETURN TRUE
	ENDIF
	
	IF IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC() //g_bTriggerSceneActive
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() is TRUE.  Cleaning up Shop Robberies.")
		RETURN TRUE
	ENDIF
	
	IF IS_MISSION_TRIGGER_ACTIVE() AND IS_RC_LEADIN_ACTIVE()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SHOULD_SHOP_ROBBERIES_CLEAN_UP] IS_MISSION_TRIGGER_ACTIVE() AND IS_RC_LEADIN_ACTIVE() is TRUE.  Cleaning up Shop Robberies.")
		RETURN TRUE
	ENDIF
	
	IF g_bIsOnRampage
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] g_bIsOnRampage is TRUE.  Cleaning up Shop Robberies script.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// No cleanup cases have occured.
	RETURN FALSE
ENDFUNC

// ----------------------------------------------------
// 	E N D  ROBBERIES CLEANUP PROCEDURES AND FUNCTIONS
// ----------------------------------------------------

// ============================================================
// 	E N D  ROBBERIES FUNCTIONS AND PROCEDURES
// ============================================================





// ============================================================
// 	ROBBERIES DEBUG FUNCTIONS AND PROCEDURES
// ============================================================

#IF IS_DEBUG_BUILD

// Timer handling how often to print debug RAG output.
structTimer timerPrintDebug

// Struct containing the debug menu.
CONST_INT MAX_STAGE_MENU_LENGTH 3

MissionStageMenuTextStruct skipMenuStruct[MAX_STAGE_MENU_LENGTH]

INT iReturnCurrentMenuStageSelection

/// PURPOSE:
///    Print the current scene phase of the clerk holdup animation.
PROC PRINT_DEBUG_SYNCHED_SCENE_PHASE()
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Synched Scene ID is: ", Shop_Robberies.cashRegisterSynchedScene.iClerkGetsCashSceneID)
	SWITCH (eCurrentCashRegisterScenePhase)
		CASE CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_GOING_FOR_REGISTER
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_GOING_FOR_REGISTER synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_PREPARING_BAG_AND_REGISTER
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_PREPARING_BAG_AND_REGISTER synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_GRABBING_BAG
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_GRABBING_BAG synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_TAKING_CASH_INTO_BAG
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_TAKING_CASH_INTO_BAG synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_LIFTING_BAG
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_LIFTING_BAG synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_LETTING_GO_OF_BAG synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS_WHEN_DONE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_HOLDING_UP_ARMS_WHEN_DONE synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_FINISHED
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_FINISHED synched scene.")
		BREAK
		
		CASE CASH_REGISTER_SCENE_PHASE_INVALID
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PRINT_DEBUG_SYNCHED_SCENE_PHASE] Clerk is currently in CASH_REGISTER_SCENE_PHASE_INVALID synched scene.")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Manage debug messages to print in RAG output.
PROC PRINT_SHOP_ROBBERIES_DEBUG()
	// Print the current scene phase of the clerk holdup animation.
	//PRINT_DEBUG_SYNCHED_SCENE_PHASE()
ENDPROC

/// PURPOSE
/// 	Activates a certain variation in the shop.
PROC SET_SHOP_ROB_STAGE()
	skipMenuStruct[0].sTxtLabel = "ENABLE STUBBORN CLERK"  	// Stage 0 Name
	skipMenuStruct[1].sTxtLabel = "ENABLE AGGRO CLERK"  	// Stage 1 Name
	skipMenuStruct[2].sTxtLabel = "ENABLE COPS"				// Stage 2 Name
ENDPROC

/// PURPOSE
/// 	Manage selections in the z-debug menu.
/// PURPOSE
/// 	Warps all racers to next leg.
PROC UPDATE_SHOP_ROB_STAGE_MENU()
	IF LAUNCH_MISSION_STAGE_MENU(skipMenuStruct, iReturnCurrentMenuStageSelection, 0)
		SWITCH (iReturnCurrentMenuStageSelection)
			CASE 0	// ENABLE STUBBORN CLERK
				eCurrentShopClerkState = SHOP_CLERK_STATE_STUBBORN	
			BREAK

			CASE 1	// ENABLE AGGRO CLERK
				Shop_Robberies.shopClerk.bIsGoingToAttack = TRUE
				GIVE_SHOP_ROBBERIES_CLERK_AGGRO_WEAPON()
			BREAK

			CASE 2	// ENABLE COPS
				Shop_Robberies.bAreCopsGoingToAmbush = TRUE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Manage debug processes and functions.
PROC SHOP_ROBBERIES_DEBUG()
	// Terminate script using the debug keyboard.
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		CLEANUP_SHOP_ROBBERIES()
	ENDIF
	
	// Print out debug information to the console output.
	IF TIMER_DO_ONCE_WHEN_READY(timerPrintDebug, 0.0)
		PRINT_SHOP_ROBBERIES_DEBUG()
		RESTART_TIMER_AT(timerPrintDebug, 0.0)
	ENDIF
	
	// Update the z-debug menu.
	IF eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP
		UPDATE_SHOP_ROB_STAGE_MENU()
	ENDIF
	
	// Update debug volumes.
	//UPDATE_ZVOLUME_WIDGETS()
ENDPROC

#ENDIF

// ============================================================
// 	E N D  ROBBERIES DEBUG FUNCTIONS AND PROCEDURES
// ============================================================





// ======================================
// 	ROBBERIES SCRIPT BLOCK
// ======================================

/// PURPOSE:
///    Determine whether or not we can roll the dice for scripted cops showing up at the shop.
PROC PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION()
	IF iTotalNumTimesPlayerHasRobbedShops >= 9
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] Number of robbed shops equal or above 9 for cops ambush at ", iTotalNumTimesPlayerHasRobbedShops)	
		IF g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush > 3
		
			// On the ninth robbery cops is enabled 100% of the time, but subsequent attempts will be randomized.
			INT iRandomChance
			IF iTotalNumTimesPlayerHasRobbedShops = 9
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] Number of robbed shops at exactly ", iTotalNumTimesPlayerHasRobbedShops)
				iRandomChance = 0
			ELSE
				iRandomChance = GET_RANDOM_INT_IN_RANGE(0, 5)
			ENDIF
			
			IF iRandomChance < 1
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] One in fifth chance succeeded!  Cops will dispatch!")
				Shop_Robberies.bAreCopsGoingToAmbush = TRUE
				g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush = 0
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] Cops will not dispatch this time.")
				Shop_Robberies.bAreCopsGoingToAmbush = FALSE
				g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush++
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] Cops ambushed player too recently, just ", 
						g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush, " robberies ago.  Updating to ", 
						g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush+1, ", robberies ago.")
			g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush++
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION] Number of robbed shops too low for cops ambush at ", iTotalNumTimesPlayerHasRobbedShops)
		g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush++
	ENDIF
ENDPROC


/// PURPOSE:
///    Determine whether or not the shop owner will decline to give money to the player.
PROC PROCESS_SHOP_ROBBERIES_CLERK_AGGRO()
	IF iTotalNumTimesPlayerHasRobbedShops >= 3
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_AGGRO] Number of robbed shops equal or above 3, at ", iTotalNumTimesPlayerHasRobbedShops)
		
		// On the fourth robbery aggro should occur 100% of the time, but subsequent attempts will be randomized.
		INT iRandomChance
		IF iTotalNumTimesPlayerHasRobbedShops = 3
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_AGGRO] Number of robbed shops at exactly ", iTotalNumTimesPlayerHasRobbedShops)
			iRandomChance = 0
		ELSE
			iRandomChance = GET_RANDOM_INT_IN_RANGE(0, 10)
		ENDIF
		
		IF iRandomChance < 1
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_AGGRO] One in tenth chance succeeded!  Clerk will aggro.")
			Shop_Robberies.shopClerk.bIsGoingToAttack = TRUE
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_AGGRO] Clerk will NOT go aggro this time.")
			Shop_Robberies.shopClerk.bIsGoingToAttack = FALSE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_AGGRO] Number of robbed shops too low to make clerk aggro at ", iTotalNumTimesPlayerHasRobbedShops)
	ENDIF
ENDPROC


/// PURPOSE:
///    Determine whether or not the shop owner will be stubborn, and at first refuse to give money to the player.
PROC PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN()
	IF iTotalNumTimesPlayerHasRobbedShops >= 6
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN] Number of robbed shops equal or above 6 for clerk being stubborn at ", iTotalNumTimesPlayerHasRobbedShops)
		
		// On the seventh robbery stubborn clerk is enabled 100% of the time, but subsequent attempts will be randomized.
		INT iRandomChance
		IF iTotalNumTimesPlayerHasRobbedShops = 6
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN] Number of robbed shops at exactly ", iTotalNumTimesPlayerHasRobbedShops)
			iRandomChance = 0
		ELSE
			iRandomChance = GET_RANDOM_INT_IN_RANGE(0, 7)
		ENDIF
		
		IF iRandomChance < 1
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN] One in seventh chance succeeded!  Clerk will be stubborn.")
			eCurrentShopClerkState = SHOP_CLERK_STATE_STUBBORN
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN] Clerk will NOT be stubborn this time.")
			eCurrentShopClerkState = SHOP_CLERK_STATE_NORMAL
		ENDIF
		
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN] Number of robbed shops too low for clerk being stubborn at ", iTotalNumTimesPlayerHasRobbedShops)
	ENDIF
ENDPROC


/// PURPOSE:
///    Process logic for determine what variations will take place in this instance 
///    of Shop Robberies.
PROC PROCESS_SHOP_ROBBERIES_VARIATIONS()
	// Determine whether or not the shop owner will be stubborn, and at first refuse to give money to the player.
	PROCESS_SHOP_ROBBERIES_CLERK_STUBBORN()
	
	// Determine whether or not the shop owner will decline to give money to the player.
	PROCESS_SHOP_ROBBERIES_CLERK_AGGRO()
		
	// Determine whether or not we can roll the dice for scripted cops showing up at the shop.
	PROCESS_SHOP_ROBBERIES_COPS_ACTIVATION()
ENDPROC


/// PURPOSE:
///    Init variables we need set in the main script block, before the main game loop.
PROC INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP(COORDS_STRUCT & shopCoords)
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 1")
	
	// Set initial enum states.
	eCurrentShopRobberiesState 				= SHOP_ROBBERIES_SETUP
	eCurrentShopRobberiesHoldUpState 		= SHOP_ROBBERIES_HOLDUP_STATE_BEFORE
	eCurrentShopRobberiesSetupState 		= SETUP_SHOP_ROBBERIES_WAIT_UNTIL_PLAYER_IS_CLOSE
	eCurrentShopRobberiesUpdateState 		= UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_ENTER_SHOP
	eCurrentPlayerState 					= PLAYER_STATE_WAIT
	eCurrentCopsState 						= COPS_STATE_UNAWARE
	eCurrentCashBagState					= CASH_BAG_STATE_WAIT
	eCurrentShopClerkState 					= SHOP_CLERK_STATE_NORMAL
	eCurrentShopClerkLookState 				= SHOP_CLERK_LOOK_STATE_WAIT
	eCurrentCashRegisterSynchedSceneState	= CASH_REGISTER_SYNCHED_SCENE_CREATE_AND_START
	eCurrentCashRegisterScenePhase 			= CASH_REGISTER_SCENE_PHASE_INVALID
	eCurrentDialogState 					= DIALOG_STATE_WAIT_FOR_PLAYER_TO_ENTER_STORE
	eCurrentPrintEnum 						= PRINT_HELP_WAIT_FOR_PLAYER_TO_ENTER_STORE

	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 2")
	
	// Set the player.
	Shop_Robberies.pedPlayer = PLAYER_PED_ID() //GET_PLAYER_PED(GET_PLAYER_INDEX())
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 2.5")
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Shop Robberies stack = ", GET_CURRENT_STACK_SIZE(), " max stack = ", GET_ALLOCATED_STACK_SIZE())
	
	// Get the current shop.
	GET_SHOP_ROBBERIES_SHOP_INDEX_BY_COORDS(shopCoords.vec_coord[0], Shop_Robberies.vShopPos, eCurrentShop)
	
	IF eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_1
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_2
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_3
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_4
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_5
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_6
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_7
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_8
	OR eCurrentShop = SHOP_ROBBERIES_SHOP_CONV_9
		bIsClerk247 = TRUE
	ENDIF
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 3")
	
	// Set the cleanup radius.  If the distance between the player and the shop is greater than this value, the shop will be cleaned up.
	//	NOTE: The distance at which the shop activates is 125m, as of 2/16/2012.  The distance it cleans up should always be greater than this distance.
	Shop_Robberies.fCleanupShopAtThisRadius = 150.0
	
	// Initialize animation strings.
	Shop_Robberies.shopClerk.szReactionAnimDict 						= "random@shop_robbery_reactions@" //"amb@reactions"
		Shop_Robberies.shopClerk.szGreetAnimClip							= "absolutely"
		Shop_Robberies.shopClerk.szFireAnimClip								= "is_this_it"
		Shop_Robberies.shopClerk.szWorriedAnimClip							= "shock"
		Shop_Robberies.shopClerk.szShockedAnimClip							= "anger_a"
		Shop_Robberies.shopClerk.szStubbornAnimClip							= "screw_you"//"negative"
		Shop_Robberies.shopClerk.szPushedAnimClip							= "but_why"
	
	Shop_Robberies.cashRegisterSynchedScene.szHoldUpAnimDict 			= "mp_am_hold_up"
		Shop_Robberies.cashRegisterSynchedScene.szClerkHandsUpAnimClip		= "guard_handsup_loop"
		Shop_Robberies.cashRegisterSynchedScene.szClerkCashRegisterAnimClip	= "holdup_victim_20s"
		Shop_Robberies.cashRegisterSynchedScene.szBagCashRegisterAnimClip	= "holdup_victim_20s_bag"
		Shop_Robberies.cashRegisterSynchedScene.szTillAnimClip				= "holdup_victim_20s_till"
	
	// Get the total number of robberies commited by the player.
	iTotalNumTimesPlayerHasRobbedShops = GET_NUM_OF_TIMES_PLAYER_HAS_ROBBED_A_SHOP()
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 4")
	
	// Process logic for determine what variations will take place in this instance of Shop Robberies.
	IF NOT bOnAnyMission
		PROCESS_SHOP_ROBBERIES_VARIATIONS()
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP] Not doing any variations since we're on a mission")
	ENDIF
	
	// We haven't updated the number of times a shop can be robbed.
	bHasNumOfTimesShopRobbedUpdated 	= FALSE
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 5")
	
	// Set whether or not the patron could be in the shop.
	IF eCurrentShop <> SHOP_ROBBERIES_SHOP_LIQ_3
		eCurrentShopPatronState = SHOP_PATRON_STATE_UNAVAILABLE
	ELSE
		eCurrentShopPatronState = SHOP_PATRON_STATE_WAIT
	ENDIF
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 6")
	
	// Store which character the player is currently using.
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		Shop_Robberies.iPlayerCharacterIndex = 0
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP] Player is Michael, so player character index is ", Shop_Robberies.iPlayerCharacterIndex)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		Shop_Robberies.iPlayerCharacterIndex = 1
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP] Player is Franklin, so player character index is ", Shop_Robberies.iPlayerCharacterIndex)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		Shop_Robberies.iPlayerCharacterIndex = 2
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP] Player is Trevor, so player character index is ", Shop_Robberies.iPlayerCharacterIndex)
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP] The player is not Michael, Franklin or Trevor.  What is this madness!?")
	ENDIF
	
	GET_SHOP_ROBBERIES_SHOP_LOCATE_DATA_BY_INDEX(eCurrentShop, 
													Shop_Robberies.vInteriorMainArea_Min, Shop_Robberies.vInteriorMainArea_Max, Shop_Robberies.fInteriorMainArea_Width, 
													Shop_Robberies.vInteriorHallArea_Min, Shop_Robberies.vInteriorHallArea_Max, Shop_Robberies.fInteriorHallArea_Width, 
													Shop_Robberies.vInteriorBackArea_Min, Shop_Robberies.vInteriorBackArea_Max, Shop_Robberies.fInteriorBackArea_Width, 
													Shop_Robberies.vInteriorBackDoorArea_Min, Shop_Robberies.vInteriorBackDoorArea_Max, Shop_Robberies.fInteriorBackDoorArea_Width,
													Shop_Robberies.vInteriorCounterArea_Min, Shop_Robberies.vInteriorCounterArea_Max, Shop_Robberies.fInteriorCounterArea_Width,
													Shop_Robberies.shopSnacks.vPos, Shop_Robberies.shopSnacks.fRadius)
	
	GET_SHOP_ROBBERIES_SHOP_SCENE_DATA_BY_INDEX(eCurrentShop, Shop_Robberies.cashRegisterSynchedScene.vPos, Shop_Robberies.cashRegisterSynchedScene.vRotation,
													Shop_Robberies.shopClerk.vPos, Shop_Robberies.shopClerk.fHeading, Shop_Robberies.shopClerk.modelName,
													Shop_Robberies.shopCashBag.vPos, Shop_Robberies.shopCashBag.vRot, Shop_Robberies.shopCashBag.modelName,
													Shop_Robberies.shopCashBag.iMinCash, Shop_Robberies.shopCashBag.iMaxCash, Shop_Robberies.shopSnacks.iNumLocates)
													
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "HIT 7")
	
	// Initialize debug functionality.
	#IF IS_DEBUG_BUILD
		START_TIMER_AT(timerPrintDebug, 1.0)
		SET_SHOP_ROB_STAGE()
		//INIT_ZVOLUME_WIDGETS()
	#ENDIF
ENDPROC

PROC SHOP_ROB_LOCK_DOORS(BOOL bActivate)
	
	INT i
		
	INT iDoorHash[2]
	MODEL_NAMES DoorModel[2]
	VECTOR vDoorCoords[2]
	SHOP_ROB_DOOR_DETAILS(eCurrentShop, iDoorHash, DoorModel, vDoorCoords)
	
	IF NOT IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_DOOR_CHECK)
			
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash[i])
			//Add Doors
			ADD_DOOR_TO_SYSTEM(iDoorHash[0], DoorModel[0], vDoorCoords[0], FALSE, FALSE, FALSE)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door wasn't registered with system, added it")
			IF iDoorHash[1] != -1
				ADD_DOOR_TO_SYSTEM(iDoorHash[1], DoorModel[1], vDoorCoords[1], FALSE, FALSE, FALSE)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "additional door wasn't registered with system, added it")
			ENDIF
			
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door was already registered on the system")
		ENDIF
		
		SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_DOOR_CHECK)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door system check finished")
	ENDIF
	
	//Lock and Unlock Doors based on Players being Inside
	REPEAT 2 i
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash[i])
			IF bActivate
				IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) != DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash[i], DOORSTATE_LOCKED, FALSE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door state set to locked")
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door state = ", DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]))
				ENDIF
			ELSE
				IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) != DOORSTATE_UNLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash[i], DOORSTATE_UNLOCKED, FALSE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door state set to unlocked")
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "door state = ", DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Shop Robberies starts here.
SCRIPT(COORDS_STRUCT shopCoords)
	
	// This function handles when the player is busted or killed, and if 
	// the player jumps from Singleplayer to Multiplayer.  When one of these
	// events occurs, the script jumps back here and returns TRUE, so this
	// should only be called once.
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO|FORCE_CLEANUP_FLAG_REPEAT_PLAY) //FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED|
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] HAS_FORCE_CLEANUP_OCCURRED is set.  Terminating Shop Robberies script.")
		CLEANUP_SHOP_ROBBERIES()
	ENDIF
	
	// Ensure no other mission is running before doing anything in Shop Robberies.
	//Ak: I've changed this to check for switches to prevent the issues in 1128002
	//this way it will not be suppressed on switch
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Another mission is running.  Setting bOnAnyMission to TRUE.")
		bOnAnyMission = TRUE
		iMissionMaxWantedLevel = GET_MAX_WANTED_LEVEL()
	ENDIF
	
	IF g_bMagDemoActive
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] g_bMagDemoActive is running.  Terminating Shop Robberies script.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		CLEAR_HELP()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Director mode is running.  Terminating Shop Robberies script.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	//Leave 3 frames between each load request for this script.
	SET_LOAD_QUEUE_LIGHT_FRAME_DELAY(sLoadQueue, 3) 
	
	// Init variables we need set in the main script block, before the main game loop.
	INIT_SHOP_ROBBERIES_BEFORE_START_OF_MAIN_LOOP(shopCoords)
	
	// conv 8
	VECTOR vClerkPos8 
	vClerkPos8 = << 24.0880, -1345.6227, 29.5082 >>
	VECTOR vScenePos8
	vScenePos8 = << 24.945620,-1344.954468,29.49 >>
	VECTOR vResult
	vResult = vClerkPos8 - vScenePos8
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "clerk pos - scene pos = ", vResult)
	vResult = vScenePos8 - vClerkPos8
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "scene pos - clerk pos = ", vResult)
	
	VECTOR vPos1
	FLOAT fRot = 147.297
	vPos1 = <<-1228.4434, -905.3416, 13.5230>>
	vResult = vPos1 - <<-1222.330566,-907.823364,12.31>>
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "1st counter vpos 1 = ", vResult)
	vResult = ROTATE_VECTOR_ABOUT_Z(vResult, fRot)
	//vResult = ROTATE_VECTOR_ABOUT_Z_ORTHO(vResult, ROTSTEP_270)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "After Rotation ", vResult)
	
	IF (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC()) //g_bTriggerSceneActive
	OR g_bBlockShopRoberies
	OR g_bIsOnRampage
	OR g_eCurrentRandomEvent != RE_NONE
		IF (IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() AND NOT IS_HIGH_MEMORY_PC())
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] IS_MISSION_LEADIN_WITH_HIGH_MEMORY_ACTIVE() is set.  Terminating Shop Robberies script.")
		ELIF g_bBlockShopRoberies
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] g_bBlockShopRoberies is set.  Terminating Shop Robberies script.")
		ELIF g_bIsOnRampage
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] g_bIsOnRampage is set.  Terminating Shop Robberies script.")
		ELIF g_eCurrentRandomEvent != RE_NONE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] g_eCurrentRandomEvent is not null.  Terminating Shop Robberies script.")
		ENDIF
		
		IF NOT IS_PLAYER_INSIDE_SHOP()
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] player is not inside the shop, safe to lock the doors.")
			CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_DOOR_CHECK)
			SHOP_ROB_LOCK_DOORS(TRUE)
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] player is somehow inside the shop, make sure doors are unlocked.")
			SHOP_ROB_LOCK_DOORS(FALSE)
		ENDIF
		CLEANUP_SHOP_ROBBERIES()
	
	// Check if the player robbed this shop too recently.
	ELIF g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed[eCurrentShop] > 0
		IF HAS_SHOP_BEEN_ROBBED_TOO_RECENTLY_TO_LAUNCH()
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Nearby shop robbed too recently.  Cleaning up Shop Robberies.")
			//CLEANUP_SHOP_ROBBERIES()
			
			VECTOR vMin, vMax
			GET_SHOP_SCENARIO_BLOCKING_INFO(eCurrentShop, Shop_Robberies.cashRegisterSynchedScene.vRotation.z, vMin, vMax)
			
			// scenario blocking here
			
			CLEAR_AREA_OF_PEDS(Shop_Robberies.vShopPos, 15.0)
			sbiShopCLosed = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
			//sbiShopCLosed = ADD_SCENARIO_BLOCKING_AREA(<<-720.06183, -921.87164, 18.0>>, <<-704.63171, -903.16638, 21.17624>>)
			
			IF NOT IS_PLAYER_INSIDE_SHOP()
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] player is not inside the shop, safe to lock the doors.")
				SHOP_ROB_LOCK_DOORS(TRUE)
			ELSE 
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] player is inside the shop, don't lock the doors, but flag to lock them once he's outside")
				SHOP_ROB_LOCK_DOORS(FALSE)
				SET_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CLERK_LOCK_DELAY)
			ENDIF
			eCurrentShopRobberiesState = SHOP_ROBBERIES_CLOSED
		ELSE
		
			SHOP_ROB_LOCK_DOORS(FALSE)
		ENDIF
		
		IF NOT HAS_SHOP_CLERK_FORGOTTEN_PLAYER()
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[Shop_Robberies.sc->SCRIPT] Set Update to UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED.")
			eCurrentShopRobberiesUpdateState = UPDATE_SHOP_ROBBERIES_WAIT_FOR_PLAYER_TO_BE_RECOGNIZED

			// Determine whether the clerk will aggro, or call the cops after recognizing player.
			INT iRandomNum = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandomNum = 0
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] When clerk recognizes player, he will attack.")
				Shop_Robberies.shopClerk.bIsGoingToAttack = TRUE
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] When clerk recognizes player, he will cower and call the cops.")
				Shop_Robberies.shopClerk.bIsGoingToAttack = FALSE
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Haven't robbed this store before, making sure doors are unlocked")
		SHOP_ROB_LOCK_DOORS(FALSE)
	ENDIF
	
	// Shop robberies main loop.
	WHILE TRUE
		
		#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 6000) = 0
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[ShopRobberies.sc->SCRIPT] Shop Robberies stack = ", GET_CURRENT_STACK_SIZE(), " max stack = ", GET_ALLOCATED_STACK_SIZE())
			ENDIF
		#ENDIF
		
		UPDATE_LOAD_QUEUE_LIGHT(sLoadQueue)
		
		// Check any instances that requires the termination of Shop Robberies.
		IF SHOULD_SHOP_ROBBERIES_CLEAN_UP()
			CLEANUP_SHOP_ROBBERIES()
		ENDIF
		
		//Check if we go on mission at any time during the life of this script.
		//If so clean up appropriately.
		IF NOT bOnAnyMission
			IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
				CLEANUP_FOR_ON_MISSION()
				bOnAnyMission = TRUE
			ENDIF
		ENDIF
		
		// Shop Robberies main loop.
		SWITCH (eCurrentShopRobberiesState)
			CASE SHOP_ROBBERIES_SETUP
				SETUP_SHOP_ROBBERIES()
			BREAK
			
			CASE SHOP_ROBBERIES_UPDATE
				UPDATE_SHOP_ROBBERIES()
			BREAK
			
			CASE SHOP_ROBBERIES_CLOSED
				
				// Only display help if no other message is on screen
				IF IS_PLAYER_INSIDE_SHOP()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_CLOSED", GET_SHOP_ROB_NAME(eCurrentShop))
						AND !IS_PLAYER_AN_ANIMAL(PLAYER_ID())
							ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
							PRINT_HELP_FOREVER_WITH_STRING("SHOP_CLOSED", GET_SHOP_ROB_NAME(eCurrentShop))
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_CLOSED", GET_SHOP_ROB_NAME(eCurrentShop))
						CLEAR_HELP()
					ENDIF
					IF IS_BITMASK_AS_ENUM_SET(Shop_Robberies.iFlags, SHOPROB_CLERK_LOCK_DELAY)
						SHOP_ROB_LOCK_DOORS(TRUE)
						CLEAR_BITMASK_AS_ENUM(Shop_Robberies.iFlags, SHOPROB_CLERK_LOCK_DELAY)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE SHOP_ROBBERIES_CLEANUP
				
			BREAK
		ENDSWITCH
		
		// Manage debug processes and functions.
		#IF IS_DEBUG_BUILD
		IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
			SHOP_ROBBERIES_DEBUG()
		ENDIF
		#ENDIF
		
		// Wait until next frame.
		WAIT(0)
	ENDWHILE
ENDSCRIPT

// ======================================
// 	E N D  ROBBERIES RACES SCRIPT BLOCK
// ======================================


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









