//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//	  SCRIPT NAME	 :   respawn_controller.sc											//
//	  AUTHOR		  :   Kenneth Ross													//
//	  DESCRIPTION	 :   Runs the appropriate routines when a player respawn has		//
//						  been deteted. This includes respawns at police stations,		//
//						  hospitals, and  savehouses.									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "Finance_Control_Public.sch"
USING "respawn_location_data.sch"
USING "respawn_cleanup_data.sch"
USING "mission_control_private.sch"

USING "script_player.sch"
USING "replay_public.sch"
USING "friends_public.sch"

USING "respawnCutscene_private.sch"
USING "minigame_midsized_message.sch"

#IF IS_DEBUG_BUILD
	USING "SP_Respawn_Debug.sch"
	USING "test_tools.sch"
#ENDIF

ENUM SCRIPT_STAGE_ENUM
	SCRIPT_STAGE_WAIT_FOR_DEATHARREST = 0,
	SCRIPT_STAGE_WAIT_FOR_RESPAWN,
	SCRIPT_STAGE_PERFORM_RESPAWN_ROUTINE,
	SCRIPT_STAGE_WAIT_FOR_MISSION_CLEANUP,
	SCRIPT_STAGE_CLEANUP
ENDENUM
SCRIPT_STAGE_ENUM eScriptStage = SCRIPT_STAGE_WAIT_FOR_DEATHARREST

RESPAWN_CUTSCENE_STRUCT sRespawnCutscene

INT iOffmissionRespawnEffectsStage = 99
STRING sOffmissionRespawnAudioScene = "", sOffmissionRespawnsRespawnCutsceneSoundSetName = ""
STRING sOffmissionRespawnMessageParam = ""
BOOL bOffmissionRespawnTextHitSoundPlayed, bOffmissionRespawnBedSoundPlayed

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID respawn_widget_group_id
#ENDIF

// ===========================================================================================================
//	  Script Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC CLEANUP_SCRIPT()
	 CPRINTLN(DEBUG_RESPAWN, "Respawn controller script cleaning up.")

	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(respawn_widget_group_id)
			DELETE_WIDGET_GROUP(respawn_widget_group_id)
		ENDIF
	#ENDIF
	
	IF (iOffmissionRespawnEffectsStage <> 99)
		
		IF IS_REPLAY_IN_PROGRESS()
		OR HAS_PLAYER_ACCEPTED_REPLAY()
		OR IS_REPLAY_BEING_PROCESSED()
			//
		ELSE
		
			// not started fail effect, do it now
			SET_FADE_OUT_AFTER_DEATH(TRUE)
			SET_FADE_OUT_AFTER_ARREST(TRUE)
			
			PAUSE_DEATH_ARREST_RESTART(FALSE)
			DISPLAY_HUD(TRUE)
			
			Replay_Control_Fail_Effect(FALSE)
			Replay_Control_Fail_Out_Effect(FALSE)
			Replay_Control_Slow_Motion(FALSE)
			IF NOT IS_STRING_NULL_OR_EMPTY(sOffmissionRespawnAudioScene)
				CPRINTLN(DEBUG_RESPAWN, "Stop audio scene \"", sOffmissionRespawnAudioScene, "\".")
				STOP_AUDIO_SCENE(sOffmissionRespawnAudioScene)
				sOffmissionRespawnAudioScene = ""
			ENDIF
		ENDIF
		iOffmissionRespawnEffectsStage = 99
	ENDIF
	
	// Reset anyhing that would block these effects, just to be safe.
	g_bBlockDeathFadeout = FALSE
	g_bBlockWastedTitle = FALSE
	g_bBlockUltraSloMoOnDeath = FALSE
	IF IS_RESULT_SCREEN_DISPLAYING()
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	ENDIF
	
	CPRINTLN(DEBUG_RESPAWN, "Respawn controller script terminating.")
	TERMINATE_THIS_THREAD()
ENDPROC


// ===========================================================================================================
//	  Player State Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the player ped is in a safe playable state after respawn has occured.
// 
PROC CLEANUP_PLAYER_STATE_ON_RESPAWN()

	CPRINTLN(DEBUG_RESPAWN, "Cleaning up player state after respawn.")
	
	PED_INDEX pedID = PLAYER_PED_ID()
	
	// Reset variations incase we are using temp mission variations
	IF g_bMagDemoActive
		// Do nothing in magdemo.
	ELSE
		IF NOT IS_REPLAY_IN_PROGRESS()
		AND NOT HAS_PLAYER_ACCEPTED_REPLAY()
			CPRINTLN(DEBUG_RESPAWN, "Restoring variations for player on respawn.")
			RESTORE_PLAYER_PED_VARIATIONS(pedID)
			SET_FLOW_OUTFIT_REQUIREMENTS(pedID)
			CLEAR_PED_PARACHUTE_PACK_VARIATION(pedID)
			REMOVE_PED_HELMET(pedID, TRUE)
		ELSE
			CPRINTLN(DEBUG_RESPAWN, "Not Restoring variations for player on respawn: replay accepted.")
		ENDIF
	ENDIF
	
	//Make sure any crazy special components are cleared on respawn. Parachutes, rucksacks, etc.
	PED_COMP_NAME_ENUM eSpecial = GET_DUMMY_COMPONENT_FOR_SLOT(GET_ENTITY_MODEL(pedID), COMP_TYPE_SPECIAL, -1)
	BOOL bClearSpecialItems = TRUE
	PED_COMP_NAME_ENUM eCurrentItem 
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID)
	
	IF eSpecial != DUMMY_PED_COMP
		
		// Add exceptions here
		
		eCurrentItem = GET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL)
		
		// Michael
		IF ePedModel = PLAYER_ZERO
			IF eCurrentItem = SPECIAL_P0_JANITOR_ACCS
			OR eCurrentItem = SPECIAL_P0_POUCHES
			OR eCurrentItem = SPECIAL_P0_BALACLAVA
			OR eCurrentItem = SPECIAL_P0_SECURITY_ACCS
			OR eCurrentItem = SPECIAL_P0_WATCH_AND_BRACELET
			OR eCurrentItem = SPECIAL_P0_EPSILON_MEDAL
			OR eCurrentItem = SPECIAL_P0_WATCH_LEATHER_STRAP
			OR eCurrentItem = SPECIAL_P0_TIE_BLACK
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special items")
			ENDIF
			
		// Franklin
		ELIF ePedModel = PLAYER_ONE
			IF eCurrentItem = SPECIAL_P1_WATCH_AND_BRACELET
			OR eCurrentItem = SPECIAL_P1_MASK
			OR eCurrentItem = SPECIAL_P1_BOWTIE
			OR eCurrentItem = SPECIAL_P1_POUCHES
			OR (eCurrentItem >= SPECIAL_P1_TIE_SHORT AND eCurrentItem <= SPECIAL_P1_TIE_SHORT_15)
			OR (eCurrentItem >= SPECIAL_P1_TIE_MEDIUM AND eCurrentItem <= SPECIAL_P1_TIE_MEDIUM_15)
			OR (eCurrentItem >= SPECIAL_P1_TIE_LONG AND eCurrentItem <= SPECIAL_P1_TIE_LONG_15)
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special items")
			ENDIF
			
		// Trevor
		ELIF ePedModel = PLAYER_TWO
			IF (eCurrentItem >= SPECIAL_P2_WATCH AND eCurrentItem <= SPECIAL_P2_WATCH_1)
			OR eCurrentItem = SPECIAL_P2_DOCK_WORKER
			OR eCurrentItem = SPECIAL_P0_WATCH_LEATHER_STRAP
			OR eCurrentItem = SPECIAL_P0_TIE_BLACK
			OR eCurrentItem = SPECIAL_P2_SECURITY
			OR eCurrentItem = SPECIAL_P2_BOWTIE
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special items")
			ENDIF
		ENDIF
		IF bClearSpecialItems = TRUE
			CPRINTLN(DEBUG_RESPAWN, "Respawn controller is clearing special items")
			SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, eSpecial, FALSE)
		ENDIF
	ENDIF
	
	// Do the same for Special2 items
	eSpecial = GET_DUMMY_COMPONENT_FOR_SLOT(GET_ENTITY_MODEL(pedID), COMP_TYPE_SPECIAL2, -1)
	IF eSpecial != DUMMY_PED_COMP
		bClearSpecialItems = TRUE
		
		// Add exceptions here
		eCurrentItem = GET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL2)
		
		// Michael
		IF ePedModel = PLAYER_ZERO
			IF eCurrentItem = SPECIAL2_P0_COP_BELT_2
			OR (eCurrentItem >= SPECIAL2_P0_WRESTLER_MASK_0 AND eCurrentItem <= SPECIAL2_P0_WRESTLER_MASK_5)
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special2 items")
			ENDIF
			
		// Franklin
		ELIF ePedModel = PLAYER_ONE
			IF (eCurrentItem >= SPECIAL2_P1_WRESTLER_MASK_0 AND eCurrentItem <= SPECIAL2_P1_WRESTLER_MASK_5)
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special2 items")
			ENDIF
			
		// Trevor
		ELIF ePedModel = PLAYER_TWO
			IF eCurrentItem = SPECIAL2_P2_HIGHWAY_PATROL
			OR (eCurrentItem >= SPECIAL2_P2_WRESTLER_MASK_0 AND eCurrentItem <= SPECIAL2_P2_WRESTLER_MASK_5)
			OR (eCurrentItem >= SPECIAL2_P2_MASK_MONSTER_GREEN AND eCurrentItem <= SPECIAL2_P2_MASK_MONSTER_RED)
				bClearSpecialItems = FALSE
				CPRINTLN(DEBUG_RESPAWN, "Respawn controller is NOT clearing special2 items")
			ENDIF
			
			
		ENDIF
		
		IF bClearSpecialItems = TRUE
			CPRINTLN(DEBUG_RESPAWN, "Respawn controller is clearing special2 items")
			SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL2, eSpecial, FALSE)
		ENDIF
	ENDIF
	
	// B*1116103 - Players weapons need to be restored on player death when on a Rampage
	IF (g_RestoreStoredWeaponsOnDeath)
		RESTORE_PLAYER_PED_WEAPONS(pedID)
		g_RestoreStoredWeaponsOnDeath = FALSE
	ENDIF
	
	IF (g_RestoreSnapshotWeaponsOnDeath)
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(pedID)
		g_RestoreSnapshotWeaponsOnDeath = FALSE
	ENDIF
	
	
	//Ensure the player is using default animation groups.
	IF IS_SCREEN_FADED_OUT()
		CLEAR_PED_TASKS_IMMEDIATELY(pedID)
	ELSE
		CLEAR_PED_TASKS(pedID)
		
		CPRINTLN(DEBUG_RESPAWN, "Clear tasks without a fadeout.")
	ENDIF
	
	RESET_PED_MOVEMENT_CLIPSET(pedID)
	RESET_PED_STRAFE_CLIPSET(pedID)
	RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedID)
	SET_PED_USING_ACTION_MODE(pedID, FALSE)
	SET_PED_STEALTH_MOVEMENT(pedID, FALSE)
	
	//Ensure ped flags are all at default state.
	SET_PED_CAN_BE_DRAGGED_OUT(pedID, TRUE)
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedID, KNOCKOFFVEHICLE_DEFAULT)
	SET_PED_CONFIG_FLAG(pedID, PCF_WillFlyThroughWindscreen, TRUE)
	SET_PED_CAN_SMASH_GLASS(pedID, TRUE, TRUE)
	SET_PED_CURRENT_WEAPON_VISIBLE(pedID, TRUE)
	SET_PED_DIES_IN_SINKING_VEHICLE(pedID, TRUE)
	SET_PED_MAX_TIME_UNDERWATER(pedID, -1)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedID, RELGROUPHASH_PLAYER)
	SET_PED_HELMET(pedID, TRUE)
	SET_PED_CAN_SWITCH_WEAPON(pedID, TRUE)
	RESET_PED_IN_VEHICLE_CONTEXT(pedID)
	CLEAR_PED_DRIVE_BY_CLIPSET_OVERRIDE(pedID)
	SET_PED_INFINITE_AMMO_CLIP(pedID, FALSE)
	SET_PED_CAN_PLAY_AMBIENT_ANIMS(pedID, TRUE)
	SET_PED_IS_DRUNK(pedID, FALSE)
	

	//Steve Taylor phone state reset
	CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME)
	HIDE_ACTIVE_PHONE(FALSE)
	HIDE_PHONE_FOR_HOTSWAP(FALSE)
	DISABLE_CELLPHONE(FALSE)
	g_Cellphone.PhoneDS = PDS_AWAY



	// player flags
	SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
	
	// Clear last seen player data.
	CPRINTLN(DEBUG_RESPAWN, "Resetting last known ped info.")
	ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
	
	// Delete ambient player characters.
	INT iPed
	REPEAT NUM_OF_PLAYABLE_PEDS iPed
		IF NOT IS_ENTITY_DEAD(g_sPlayerPedRequest.sSelectorPeds.pedID[iPed])
			IF (g_sPlayerPedRequest.sSelectorPeds.pedID[iPed] <> PLAYER_PED_ID())
				CPRINTLN(DEBUG_RESPAWN, "Deleteing global selector ped [", iPed, "].")
				
				SET_ENTITY_AS_MISSION_ENTITY(g_sPlayerPedRequest.sSelectorPeds.pedID[iPed], TRUE, TRUE)
				DELETE_PED(g_sPlayerPedRequest.sSelectorPeds.pedID[iPed])
			ENDIF
		ENDIF
	ENDREPEAT
	INT iQueue
	REPEAT COUNT_OF(g_ambientSelectorPedDeleteQueue) iQueue
		IF NOT IS_ENTITY_DEAD(g_ambientSelectorPedDeleteQueue[iQueue])
			IF (g_ambientSelectorPedDeleteQueue[iQueue] <> PLAYER_PED_ID())
				CPRINTLN(DEBUG_RESPAWN, "Deleting ambient queue ped [", iQueue, "].")
				
				SET_ENTITY_AS_MISSION_ENTITY(g_ambientSelectorPedDeleteQueue[iQueue], TRUE, TRUE)
				DELETE_PED(g_ambientSelectorPedDeleteQueue[iQueue])
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Reset the weather (primarily for North Yankton snowfall).
	IF NOT g_bSkipWeatherResetOnRespawn
		CPRINTLN(DEBUG_RESPAWN, "Clearing current weather and randomising as player respawns.")
		CLEAR_WEATHER_TYPE_PERSIST()
		CLEAR_TIMECYCLE_MODIFIER()
		SET_RANDOM_WEATHER_TYPE()
#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_RESPAWN, "Skipping randomisation of weather on respawn as the replay controller is setting a mission weather state.")
#ENDIF
	ENDIF
	g_bSkipWeatherResetOnRespawn = FALSE
	
	UNLOAD_ALL_CLOUD_HATS()
	
	// some of these settings would interfere with replay setup
	// so these aren't called if a replay is being processed
	// They will instead be called by the replay controller as the replay setup ends
	IF NOT IS_REPLAY_BEING_PROCESSED()
		REPLAY_RESET_PLAYER_STATE()
	ENDIF
ENDPROC


// ===========================================================================================================
//	  Respawn Routines
// ===========================================================================================================

/// PURPOSE: Ensures all the respawn locations have been initialised
///	
PROC CHECK_RESPAWN_LOCATION_SETUP()
	IF NOT Has_Savehouse_Respawns_Been_Initialised()
		Initialise_Savehouse_Respawn_Locations()
	ENDIF
	
	IF NOT Has_Police_Respawns_Been_Initialised()
		Initialise_Police_Respawn_Locations()
	ENDIF
	
	IF NOT Has_Hospital_Respawns_Been_Initialised()
		Initialise_Hospital_Respawn_Locations()
	ENDIF
ENDPROC


/// PURPOSE: Returns TRUE if the player has recently died.
FUNC BOOL HAS_PLAYER_HAD_RECENT_DEATH()

	INT iLastDeathTime = GET_TIME_SINCE_LAST_DEATH()
	INT iLastArrestTime = GET_TIME_SINCE_LAST_ARREST()
	
	IF iLastDeathTime != -1
		IF iLastArrestTime = -1
		OR iLastDeathTime < iLastArrestTime
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Returns TRUE if the player has recently been arrested.
FUNC BOOL HAS_PLAYER_HAD_RECENT_ARREST()
	INT iLastDeathTime = GET_TIME_SINCE_LAST_DEATH()
	INT iLastArrestTime = GET_TIME_SINCE_LAST_ARREST()
	
	IF iLastArrestTime != -1
		IF iLastDeathTime = -1
		OR iLastArrestTime < iLastDeathTime
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Returns TRUE if the player has recently died or been arrested.
FUNC BOOL HAS_PLAYER_HAD_RECENT_DEATHARREST()
	RETURN (HAS_PLAYER_HAD_RECENT_DEATH() OR HAS_PLAYER_HAD_RECENT_ARREST())
ENDFUNC

/// PURPOSE: Returns TRUE if the respawn controller should skip the message/audio/fadeout scene
///    Used in some minigames and special conditions

FUNC BOOL SHOULD_SKIP_RESPAWN_EFFECTS()
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_SPMC)
		CPRINTLN(DEBUG_RESPAWN, "Player is on an SPMC-type mission, skipping respawn effects")
		RETURN TRUE
	ENDIF
	
	//Basejumps perform their own Slo-mo death scene
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bj")) >0
		CPRINTLN(DEBUG_RESPAWN, "Player has died while basejumping, skipping respawn effects")
		RETURN TRUE
	ENDIF

	//Is a replay in process or being processed
	IF IS_REPLAY_IN_PROGRESS()
	OR HAS_PLAYER_ACCEPTED_REPLAY()
	OR IS_REPLAY_BEING_PROCESSED()
		CPRINTLN(DEBUG_RESPAWN, "A repeat play is in progress or being processed, skipping respawn effects")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///	


PROC PERFORM_OFFMISSION_RESPAWN_EFFECTS()
	
	IF SHOULD_SKIP_RESPAWN_EFFECTS()
		iOffmissionRespawnEffectsStage = 99
		EXIT
	ENDIF
	
   	CDEBUG3LN(DEBUG_RESPAWN, "Performing off-mission respawn effects ", iOffmissionRespawnEffectsStage, ".")
	
	//#1503566
	SUPPRESS_AMBIENT_PED_AGGRESSIVE_CLEANUP_THIS_FRAME()
	
//  /*
//  ok if you go to "Replay_Do_Blur" in the replay_controller.sc
//  as soon as the mission is failed it calls Replay_Control_Fail_Effect
//  which works out which fail effect to play, as there is 1 per character
//  that also triggers the 1st slow motion effect via Replay_Control_Slow_Motion

	IF (iOffmissionRespawnEffectsStage = 0)
		CPRINTLN(DEBUG_RESPAWN, "Replay_Control_Fail_Effect(TRUE).")
		
		// not started fail effect, do it now
		Replay_Control_Fail_Effect(TRUE)
		SET_FADE_OUT_AFTER_DEATH(FALSE)
		SET_FADE_OUT_AFTER_ARREST(FALSE)
		SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
		
		PAUSE_DEATH_ARREST_RESTART(TRUE)
		DISPLAY_HUD(FALSE)
		
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			sOffmissionRespawnAudioScene = "DEATH_SCENE"
			sOffmissionRespawnsRespawnCutsceneSoundSetName = "WastedSounds"
			sOffmissionRespawnMessageParam = "RESPAWN_W"
			
			CPRINTLN(DEBUG_RESPAWN, "Starting audio scene \"", sOffmissionRespawnAudioScene, "\".")
			START_AUDIO_SCENE(sOffmissionRespawnAudioScene)	 //1409759
		ENDIF
		IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
			sOffmissionRespawnAudioScene = "BUSTED_SCENE"
			sOffmissionRespawnsRespawnCutsceneSoundSetName = "BustedSounds"
			sOffmissionRespawnMessageParam = "RESPAWN_B"
			
			CPRINTLN(DEBUG_RESPAWN, "Starting audio scene \"", sOffmissionRespawnAudioScene, "\".")
			START_AUDIO_SCENE(sOffmissionRespawnAudioScene)	 //1445110
		ENDIF
		
		SETTIMERA(0)
		
		g_replay.mFailTextScaleform = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
		
		IF NOT IS_SCREEN_FADED_OUT()
			PLAY_SOUND_FRONTEND(-1, "ScreenFlash", sOffmissionRespawnsRespawnCutsceneSoundSetName)
		ENDIF
		
		REQUEST_SCRIPT_AUDIO_BANK("OFFMISSION_WASTED")
		
		iOffmissionRespawnEffectsStage = 1
	ENDIF
	
//  then 3 seconds later (timed via IF TIMERA() > FAIL_EFFECT_TIME * FAIL_EFFECT_SLO_MO to compensate for the slow motion effect)
//  it triggers the mission failed screen
//  and the fail out effect via Replay_Control_Fail_Out_Effect
//  this stops the fail effect and triggers the fail out effect
//  and also changes the slow motion so that it is even slower

	IF (iOffmissionRespawnEffectsStage = 1)
		IF NOT bOffmissionRespawnBedSoundPlayed
			IF REQUEST_SCRIPT_AUDIO_BANK("OFFMISSION_WASTED")
				PLAY_SOUND_FRONTEND(-1, "Bed", sOffmissionRespawnsRespawnCutsceneSoundSetName)
				bOffmissionRespawnBedSoundPlayed = TRUE
			ENDIF
		ENDIF
		IF NOT bOffmissionRespawnBedSoundPlayed
			IF TIMERA() > 0300
				CWARNINGLN(DEBUG_RESPAWN, "Emergency call to PLAY_SOUND_FRONTEND for url:bugstar:2024410 - call \"Bed\", \"", sOffmissionRespawnsRespawnCutsceneSoundSetName, "\"")
				
				PLAY_SOUND_FRONTEND(-1, "Bed", sOffmissionRespawnsRespawnCutsceneSoundSetName)
				bOffmissionRespawnBedSoundPlayed = TRUE
			ENDIF
		ENDIF
		
		IF TIMERA() > FAIL_EFFECT_TIME * FAIL_EFFECT_SLO_MO
			// not started fail effect, do it now
			Replay_Control_Fail_Out_Effect(TRUE)
			SETTIMERB(0)

			CPRINTLN(DEBUG_RESPAWN, "Replay_Control_Fail_Out_Effect(TRUE).")
			
			iOffmissionRespawnEffectsStage = 2
		ENDIF
	ENDIF
	
//  then 3 seconds later (calculated via IF TIMERB() > FAIL_OUT_EFFECT_TIME * FAIL_OUT_EFFECT_SLO_MO to compensate for the slow motion effect)
//  we start fading out
//  */

	BOOL bUpdate_shard_big_message_generic = FALSE
	IF (iOffmissionRespawnEffectsStage = 2)
		IF NOT g_bBlockWastedTitle
			IF NOT IS_STRING_NULL_OR_EMPTY(sOffmissionRespawnMessageParam)
				IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_replay.mFailTextScaleform)
					g_replay.mFailTextScaleform = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
				ELSE
					IF NOT bOffmissionRespawnTextHitSoundPlayed
						PLAY_SOUND_FRONTEND(-1, "TextHit", sOffmissionRespawnsRespawnCutsceneSoundSetName)
						SET_SHARD_BIG_MESSAGE_GENERIC(g_replay.mFailTextScaleform, sOffmissionRespawnMessageParam, DEFAULT, SHARD_MESSAGE_WASTED)
						bOffmissionRespawnTextHitSoundPlayed = TRUE
					ENDIF
					
					UPDATE_SHARD_BIG_MESSAGE_GENERIC(g_replay.mFailTextScaleform)
					bUpdate_shard_big_message_generic = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT g_bBlockDeathFadeout
			IF TIMERB() > FAIL_OUT_EFFECT_TIME * FAIL_OUT_EFFECT_SLO_MO
				// fade out the screen
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
				SETTIMERA(0)
				
				CPRINTLN(DEBUG_RESPAWN, "DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG).")
				iOffmissionRespawnEffectsStage = 3
			
#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_RESPAWN, "TIMERB() set to ", TIMERB(), "???")
#ENDIF

			ENDIF

#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_RESPAWN, "g_bBlockDeathFadeout set to true???")
#ENDIF

		ENDIF
	ENDIF
	
	IF iOffmissionRespawnEffectsStage = 3
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT bUpdate_shard_big_message_generic
				IF HAS_SCALEFORM_MOVIE_LOADED(g_replay.mFailTextScaleform)
					UPDATE_SHARD_BIG_MESSAGE_GENERIC(g_replay.mFailTextScaleform)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF (iOffmissionRespawnEffectsStage <> 99)
		IF NOT IS_SCREEN_FADED_OUT()
			//
		ELSE
			// not started fail effect, do it now
			SET_FADE_OUT_AFTER_DEATH(TRUE)
			SET_FADE_OUT_AFTER_ARREST(TRUE)
			
			PAUSE_DEATH_ARREST_RESTART(FALSE)
			DISPLAY_HUD(TRUE)
			
			Replay_Control_Fail_Effect(FALSE)
			Replay_Control_Fail_Out_Effect(FALSE)
			Replay_Control_Slow_Motion(FALSE)
			
			//CLEANUP_MG_MIDSIZED_MESSAGE(RespawnBigSize)
			
			CPRINTLN(DEBUG_RESPAWN, "Replay_Control_Fail_Effect(FALSE).")
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sOffmissionRespawnAudioScene)
				CPRINTLN(DEBUG_RESPAWN, "Stopping audio scene \"", sOffmissionRespawnAudioScene, "\".")
				STOP_AUDIO_SCENE(sOffmissionRespawnAudioScene)
				sOffmissionRespawnAudioScene = ""
			ENDIF 
			
			iOffmissionRespawnEffectsStage = 99
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///	
PROC RUN_DEATH_CUTSCENE()
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	HOSPITAL_NAME_ENUM eHospital = GET_CLOSEST_HOSPITAL(vPlayerCoord)
	STRING pHospitalDebugName = ""

	#IF IS_DEBUG_BUILD
	IF (g_eDebug_ForceHospitalRespawn <> NUMBER_OF_HOSPITAL_LOCATIONS)
		eHospital = g_eDebug_ForceHospitalRespawn
		CPRINTLN(DEBUG_RESPAWN, "g_eDebug_ForceHospitalRespawn.")
	ENDIF
	
	pHospitalDebugName = Get_Hospital_Respawn_Name(eHospital)
	CPRINTLN(DEBUG_RESPAWN, "Player death at ", pHospitalDebugName, ".")
	#ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			sRespawnCutscene.effectName = "RespawnMichael"
			sRespawnCutscene.pFacialDictName = "facials@P_M_ZERO@BASE"
			sRespawnCutscene.pFacialClipName = "mood_injured_1"
		BREAK
		CASE CHAR_FRANKLIN
			sRespawnCutscene.effectName = "RespawnFranklin"
			sRespawnCutscene.pFacialDictName = "facials@P_M_ONE@BASE"
			sRespawnCutscene.pFacialClipName = "mood_injured_1"
		BREAK
		CASE CHAR_TREVOR
			sRespawnCutscene.effectName = "RespawnTrevor"
			sRespawnCutscene.pFacialDictName = "facials@P_M_TWO@BASE"
			sRespawnCutscene.pFacialClipName = "mood_injured_1"
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_RESPAWN, "invalid character in RUN_DEATH_CUTSCENE()")
			#ENDIF
			EXIT
		BREAK
	ENDSWITCH
	
	sRespawnCutscene.soundSetName = "RESPAWN_SOUNDSET"
	sRespawnCutscene.soundNameA = "Hit"
	sRespawnCutscene.soundNameB = "Whoosh"
	
	SWITCH eHospital
		CASE HOSPITAL_RH	// 002339_01_GC_Respawn_Hospital_Rockford_Hills url:bugstar:922262
			sRespawnCutscene.pAnimDictName   = "RESPAWN@HOSPITAL@ROCKFORD"
			sRespawnCutscene.pAnimClipName   = "ROCKFORD"
			sRespawnCutscene.pAnimCamName	= "ROCKFORD_CAM"
			
			sRespawnCutscene.vAnimPos = <<-447.2036, -342.8395, 34.5020>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, 109.1352>>
			
			sRespawnCutscene.fDialoguePhase = 0.5
			
			sRespawnCutscene.fRollingBlendoutPhase = 0.85
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			sRespawnCutscene.gpCamPitch = 0.0
			sRespawnCutscene.gpCamHead = -27.0					   //388.665771 - 360
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[New anims submitted
		//	-please set the 1st person phase to be .75
		//	-make pushin duration 1 second
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.75	//#2048274
			sRespawnCutscene.vPushInDataDirectionMod = <<15.000,0,-11.000>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE HOSPITAL_SC	// 002340_01_GC_Respawn_Hospital_South_Central  url:bugstar:922304
			sRespawnCutscene.pAnimDictName   = "RESPAWN@HOSPITAL@SOUTH_CENTRAL"
			sRespawnCutscene.pAnimClipName   = "SOUTH_CENTRAL"
			sRespawnCutscene.pAnimCamName	= "SOUTH_CENTRAL_CAM"
			
			sRespawnCutscene.vAnimPos = <<342.7344, -1397.8510, 32.5092>>		//<<341.4144, -1396.2910, 32.5092>>
			sRespawnCutscene.vAnimRot = <<0.0, 0.0, 62.5160>>					//<<0,0,62.516>>
			
			sRespawnCutscene.fDialoguePhase = 0.5
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<-39.0000, 16.5000, 0.0>>
			sRespawnCutscene.vTrafficBounds = <<25,25,5>>
			
		//	[New anims submitted
		//	-please set the 1st person phase to be .65
		//	-make pushin duration 1 second
		//	-framing fine as default.
		//
		//	Note Ive rotated the animation so he now walks to the left of the trees.]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.65	//#2050602
			sRespawnCutscene.vPushInDataDirectionMod = <<7.650,0,2.550>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE HOSPITAL_DT	// 002341_01_GC_Respawn_Hospital_Downtown	   url:bugstar:922416
			sRespawnCutscene.pAnimDictName   = "RESPAWN@HOSPITAL@DOWNTOWN"
			sRespawnCutscene.pAnimClipName   = "DOWNTOWN"
			sRespawnCutscene.pAnimCamName	= "DOWNTOWN_CAM"
			
			sRespawnCutscene.vAnimPos = <<357.3475, -585.6215, 28.8310>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, -95.0926>>
			
			sRespawnCutscene.fDialoguePhase = 0.5
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<34.5, -18.0, 0.0>>
			sRespawnCutscene.vTrafficBounds = <<30.0, 30.0, 7.5>>
			
		//	[New anims submitted
		//	-please set the 1st person phase to be .75
		//	-make pushin duration 1 second
		//	-framing fine as default
		//
		//	I also adjusted the character animation to make him 
		//	end up away from the wall and facing otuside more to fix 
		//	another bug I had. This should mean 1st person 
		//	framing faces out more too.]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.75	//#2048509
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE HOSPITAL_SS	// 002342_01_GC_Respawn_Hospital_Sandy_shores   url:bugstar:922469
			sRespawnCutscene.pAnimDictName   = "RESPAWN@HOSPITAL@SANDY_SHORES"
			sRespawnCutscene.pAnimClipName   = "SANDY_SHORES"
			sRespawnCutscene.pAnimCamName	= "SANDY_SHORES_CAM"
			
			sRespawnCutscene.vAnimPos = <<1837.655, 3673.500, 34.308>>   
			sRespawnCutscene.vAnimRot = <<0,0,-146.160>>
			
			sRespawnCutscene.fDialoguePhase = 0.5
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[New anims submitted
		//	-please set the 1st person phase to be .7
		//	-make pushin duration 1 second
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.7	//#2049116
			sRespawnCutscene.vPushInDataDirectionMod = <<13.310,0,4.320>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE HOSPITAL_PB	// 002343_01_GC_Respawn_Hospital_Paleto_Bay	 url:bugstar:922491
			sRespawnCutscene.pAnimDictName   = "RESPAWN@HOSPITAL@PALETO_BAY"
			sRespawnCutscene.pAnimClipName   = "PALETO_BAY"
			sRespawnCutscene.pAnimCamName	= "PALETO_BAY_CAM"
			
			sRespawnCutscene.vAnimPos = <<-244.6081, 6324.9629, 32.4260>>
			sRespawnCutscene.vAnimRot = <<0, 0, -57.7613>>
			
			sRespawnCutscene.fDialoguePhase = 0.5
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[New anims submitted
		//	-please set the 1st person phase to be .7
		//	-make pushin duration 1 second
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.7	//#2048571
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_RESPAWN, "invalid eHospital \"", eHospital, "\" in RUN_DEATH_CUTSCENE()")
			#ENDIF
			EXIT
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
   		CPRINTLN(DEBUG_RESPAWN, "Player distance from ", Get_Hospital_Respawn_Name(eHospital), ": ", VDIST(vPlayerCoord, sRespawnCutscene.vAnimPos), "m.")
	#ENDIF
	
	IF (g_eRecentFriendChar <> NO_CHARACTER)
		
		enumCharacterList ePlayerChar   = GET_CURRENT_PLAYER_PED_ENUM()
		
		#IF IS_DEBUG_BUILD
		IF (g_eRecentFriendChar = CHAR_MULTIPLAYER)
			SWITCH ePlayerChar
				CASE CHAR_MICHAEL
					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
						g_eRecentFriendChar = CHAR_FRANKLIN
					ELSE
						g_eRecentFriendChar = CHAR_TREVOR
					ENDIF
				BREAK
				CASE CHAR_FRANKLIN
					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
						g_eRecentFriendChar = CHAR_TREVOR
					ELSE
						g_eRecentFriendChar = CHAR_MICHAEL
					ENDIF
				BREAK
				CASE CHAR_TREVOR
					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
						g_eRecentFriendChar = CHAR_MICHAEL
					ELSE
						g_eRecentFriendChar = CHAR_FRANKLIN
					ENDIF
				BREAK
				
				DEFAULT
					CASSERTLN(DEBUG_RESPAWN, "invalid ePlayerChar [", GET_PLAYER_PED_STRING(ePlayerChar), "]")
				BREAK
			ENDSWITCH
		ENDIF
		#ENDIF
		
		sRespawnCutscene.DialogueTextLabel = ""
		SWITCH ePlayerChar
			CASE CHAR_MICHAEL   sRespawnCutscene.DialogueTextLabel  = "MICS_" BREAK
			CASE CHAR_FRANKLIN  sRespawnCutscene.DialogueTextLabel  = "FRAS_" BREAK
			CASE CHAR_TREVOR	sRespawnCutscene.DialogueTextLabel  = "TRVS_" BREAK
			
			DEFAULT
				sRespawnCutscene.DialogueTextLabel = ""
				CASSERTLN(DEBUG_RESPAWN, "invalid ePlayerChar [", GET_PLAYER_PED_STRING(ePlayerChar), "]")
			BREAK
		ENDSWITCH
		
		SWITCH g_eRecentFriendChar
			CASE CHAR_MICHAEL   sRespawnCutscene.DialogueTextLabel += "DIEM" BREAK
			CASE CHAR_FRANKLIN  sRespawnCutscene.DialogueTextLabel += "DIEF" BREAK
			CASE CHAR_TREVOR	sRespawnCutscene.DialogueTextLabel += "DIET" BREAK
			
			CASE CHAR_LAMAR
			CASE CHAR_JIMMY
			CASE CHAR_AMANDA
				sRespawnCutscene.DialogueTextLabel = ""
				CPRINTLN(DEBUG_RESPAWN, "ignorable g_eRecentFriendChar [", GET_PLAYER_PED_STRING(g_eRecentFriendChar), "]")
			BREAK
			
			DEFAULT
				sRespawnCutscene.DialogueTextLabel = ""
				CASSERTLN(DEBUG_RESPAWN, "invalid g_eRecentFriendChar [", GET_PLAYER_PED_STRING(g_eRecentFriendChar), "]")
			BREAK
		ENDSWITCH
	ENDIF
	
	sRespawnCutscene.ePlayerDamage = PD_NONE
	SWITCH GET_RANDOM_INT_IN_RANGE(0,10)
		CASE 0 sRespawnCutscene.ePlayerDamage = PD_0_chinBruise	  BREAK
		CASE 1 sRespawnCutscene.ePlayerDamage = PD_1_plasterForehead BREAK
		CASE 2 sRespawnCutscene.ePlayerDamage = PD_2_foreheadBruise  BREAK
		CASE 3 sRespawnCutscene.ePlayerDamage = PD_3_noseBruise	  BREAK
		CASE 4 sRespawnCutscene.ePlayerDamage = PD_4_lefteyeBruise   BREAK
		CASE 5 sRespawnCutscene.ePlayerDamage = PD_5_splatterLeft	BREAK
		CASE 6 sRespawnCutscene.ePlayerDamage = PD_6_splatterLeft	BREAK
		CASE 7 sRespawnCutscene.ePlayerDamage = PD_7_splatterLeft	BREAK
		CASE 8 sRespawnCutscene.ePlayerDamage = PD_8_splatterLeft	BREAK
		CASE 9 sRespawnCutscene.ePlayerDamage = PD_9_cutNose		 BREAK
		
	ENDSWITCH
	
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	ADVANCE_FRIEND_TIMERS(10.0)
	
	#IF IS_DEBUG_BUILD
	sRespawnCutscene.respawn_widgetId = respawn_widget_group_id
	#ENDIF
	RUN_RESPAWN_CUTSCENE(pHospitalDebugName, sRespawnCutscene, "")	////"AM_H_WASTED",	// Kenneth R. - Removing for bug #1007089
	
	g_eRecentFriendChar = NO_CHARACTER
ENDPROC

/// PURPOSE:
///	
PROC RUN_ARREST_CUTSCENE()
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	POLICE_STATION_NAME_ENUM ePoliceStation = GET_CLOSEST_POLICE_STATION(vPlayerCoord)
	STRING pPoliceStationDebugName = ""

	#IF IS_DEBUG_BUILD
	IF (g_eDebug_ForcePoliceStationRespawn <> NUMBER_OF_POLICE_STATION_LOCATIONS)
		ePoliceStation = g_eDebug_ForcePoliceStationRespawn
		CPRINTLN(DEBUG_RESPAWN, "g_eDebug_ForcePoliceStationRespawn.")
	ENDIF
	
	pPoliceStationDebugName = Get_Police_Respawn_Name(ePoliceStation)
	CPRINTLN(DEBUG_RESPAWN, "Player arrest at ", pPoliceStationDebugName, ".")
	#ENDIF
	
	sRespawnCutscene.effectName = ""
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			sRespawnCutscene.effectName = "RespawnMichael"
			sRespawnCutscene.pFacialDictName = "facials@P_M_ZERO@BASE"
			sRespawnCutscene.pFacialClipName = "mood_angry_1"
		BREAK
		CASE CHAR_FRANKLIN
			sRespawnCutscene.effectName = "RespawnFranklin"
			sRespawnCutscene.pFacialDictName = "facials@P_M_ONE@BASE"
			sRespawnCutscene.pFacialClipName = "mood_angry_1"
		BREAK
		CASE CHAR_TREVOR
			sRespawnCutscene.effectName = "RespawnTrevor"
			sRespawnCutscene.pFacialDictName = "facials@P_M_TWO@BASE"
			sRespawnCutscene.pFacialClipName = "mood_angry_1"
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_RESPAWN, "invalid character in RUN_ARREST_CUTSCENE()")
			#ENDIF
			EXIT
		BREAK
	ENDSWITCH
	
	sRespawnCutscene.soundSetName = "RESPAWN_SOUNDSET"
	sRespawnCutscene.soundNameA = "Hit"
	sRespawnCutscene.soundNameB = "Whoosh"
	
	SWITCH ePoliceStation
		CASE POLICE_STATION_VB	  // 002346_01_GC_Respawn_Police_Vespucci		 url:bugstar:922600
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@VESPUCCI"
			sRespawnCutscene.pAnimClipName   = "VESPUCCI"
			sRespawnCutscene.pAnimCamName	= "VESPUCCI_CAM"
			
			sRespawnCutscene.vAnimPos = <<-1092.6000, -807.6234, 19.3040>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, 37.240>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[No new anims submitted 
		//	-add 1st person punch in 
		//	-please set the 1st person phase to be .8 
		//	-make pushin duration 1 second 
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.8	//#2050717
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE POLICE_STATION_SC	  // 002344_01_GC_Respawn_Police_South_Central	url:bugstar:922583
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@SOUTH_CENTRAL_A"
			sRespawnCutscene.pAnimClipName   = "SOUTH_CENTRAL_A"
			sRespawnCutscene.pAnimCamName	= "SOUTH_CENTRAL_A_CAM"
			
			sRespawnCutscene.vAnimPos = <<361.4820, -1584.9680, 29.2930>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, 45.0000>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[No changes to animations for this one.
		//	-add punch in for 1st person 
		//	-please set the 1st person phase to be .6 
		//	-make pushin duration 1 second COould you adjust the phase where we blend back to player control? Currently I think we have it fraction too soon, which is breaking the player out of his animation before he has properly turned away from the hedge. Another step and he is properly facing the road in the fbx file. This will affect 1st person framing too so hes not facing the wall.]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.6	//#2050671
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = 0.0
			sRespawnCutscene.gp3rdCamHead = 25.377075
		BREAK
		CASE POLICE_STATION_RH	  // 002347_02_GC_Respawn_Police_Rockford_hills   url:bugstar:922628
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@ROCKFORD"
			sRespawnCutscene.pAnimClipName   = "ROCKFORD"
			sRespawnCutscene.pAnimCamName	= "ROCKFORD_CAM"
			
			sRespawnCutscene.vAnimPos = <<-560.3680, -133.8910, 37.0520>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, 4.2000>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			sRespawnCutscene.gpCamPitch = 0.0
			sRespawnCutscene.gpCamHead = 20.0
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[No new anims submitted 
		//	-add 1st person punch in 
		//	-please set the 1st person phase to be .8 
		//	-make pushin duration 1 second 
		//	-please adjust 1st person framing to aim to the right towards the exit gap. as seen in screenshot]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.8	//#2050753
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = 0.0
			sRespawnCutscene.gp3rdCamHead = -30.0
		BREAK
		CASE POLICE_STATION_SS	  // 002348_01_GC_Respawn_Police_Sandy_Shores	 url:bugstar:922673
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@SANDY_SHORES"
			sRespawnCutscene.pAnimClipName   = "SANDY_SHORES"
			sRespawnCutscene.pAnimCamName	= "SANDY_SHORES_CAM"
			
			sRespawnCutscene.vAnimPos = <<1854.9810, 3683.8401, 34.2680>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, -132.0000>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[No new anims submitted 
		//	-add punch in to 1st person 
		//	-please set the 1st person phase to be .75 
		//	-make pushin duration 1 second 
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.75	//#2050789
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE POLICE_STATION_PB	  // Paleto Bay
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@PALETO_BAY"	   //SOUTH_CENTRAL_B
			sRespawnCutscene.pAnimClipName   = "PALETO_BAY"					  //SOUTH_CENTRAL_B
			sRespawnCutscene.pAnimCamName	= "PALETO_BAY_CAM"				  //SOUTH_CENTRAL_B
			
			sRespawnCutscene.vAnimPos = <<-440.7729, 6019.2891, 30.4900>>
			sRespawnCutscene.vAnimRot = <<0, 0, -135.0690>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[No new anims submitted 
		//	-add punch in to 1st person 
		//	-please set the 1st person phase to be .8 
		//	-make pushin duration 1 second 
		//	-framing fine as default]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.8	//#2050815
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE POLICE_STATION_HW	  // Vinewood
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@VINEWOOD"
			sRespawnCutscene.pAnimClipName   = "VINEWOOD"
			sRespawnCutscene.pAnimCamName	= "VINEWOOD_CAM"
			
			sRespawnCutscene.vAnimPos = <<641.5, -0.3, 81.8030>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, -170.0000>>
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			
			sRespawnCutscene.vTrafficOffset = <<0,0,0>>
			sRespawnCutscene.vTrafficBounds = <<0,0,0>>
			
		//	[New anims submitted
		//	-add punch in to 1st person
		//	-please set the 1st person phase to be .7
		//	-make pushin duration 1 second
		//	-framing fine as default]
		//	
		//	Note Ive heavily rotated the animations to fix the player 
		//	walking right off to the right of frame down the ramp. 
		//	Now he heads off into the open.
			sRespawnCutscene.fFirstPersonPunchPhase = 0.7	//#2051047
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = sRespawnCutscene.gpCamPitch
			sRespawnCutscene.gp3rdCamHead = sRespawnCutscene.gpCamHead
		BREAK
		CASE POLICE_STATION_DT	  // Downtown
			sRespawnCutscene.pAnimDictName   = "RESPAWN@POLICE@DOWN_TOWN"
			sRespawnCutscene.pAnimClipName   = "DOWN_TOWN"
			sRespawnCutscene.pAnimCamName	= "DOWN_TOWN_CAM"
			
			sRespawnCutscene.vAnimPos = <<480.3291, -976.4094, 26.9920>>
			sRespawnCutscene.vAnimRot = <<0.0000, 0.0000, 4.9310>>
			
			sRespawnCutscene.ePedMotionState = MS_ON_FOOT_WALK	   //MS_ON_FOOT_IDLE
			sRespawnCutscene.gpCamPitch = 999.0
			sRespawnCutscene.gpCamHead = 999.0
			
			sRespawnCutscene.vTrafficOffset = <<486.8887, -958.3694, 27.9336>>-sRespawnCutscene.vAnimPos
			sRespawnCutscene.vTrafficBounds = <<10,10,5>>
			
		//	[No new anims submitted 
		//	-add 1st person punch in 
		//	-please set the 1st person phase to be .75 
		//	-make pushin duration 1 second 
		//	-please adjust the 1st person framing to look a bit more to the right towards the gap for the stairs as shown in screenshot]
			sRespawnCutscene.fFirstPersonPunchPhase = 0.75	//#2050744
			sRespawnCutscene.vPushInDataDirectionMod = <<20,0,0>>
			sRespawnCutscene.gp3rdCamPitch = -15.000
			sRespawnCutscene.gp3rdCamHead = -15.000
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_RESPAWN, "invalid ePoliceStation in RUN_ARREST_CUTSCENE()")
			#ENDIF
			EXIT
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RESPAWN, "Player distance from ", Get_Police_Respawn_Name(ePoliceStation), ": ", VDIST(vPlayerCoord, sRespawnCutscene.vAnimPos), "m.")
	#ENDIF
	
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	ADVANCE_FRIEND_TIMERS(10.0)
	
	sRespawnCutscene.DialogueTextLabel = ""
	sRespawnCutscene.fDialoguePhase = -1
	
	#IF IS_DEBUG_BUILD
	sRespawnCutscene.respawn_widgetId = respawn_widget_group_id
	#ENDIF
	
	RUN_RESPAWN_CUTSCENE(pPoliceStationDebugName, sRespawnCutscene, "AM_H_BUSTED")
	
ENDPROC


/// PURPOSE: This is where we take care of all the code that needs to be run
///		  after a recent deatharrest. This proc should only be called once
///		  for every deatharrest.
PROC PERFORM_RESPAWN_ROUTINE_AFTER_DEATHARREST()

	CPRINTLN(DEBUG_RESPAWN, "Performing respawn routine after death/arrest.")
	
	IF (iOffmissionRespawnEffectsStage <> 99)
		// not started fail effect, do it now
		SET_FADE_OUT_AFTER_DEATH(TRUE)
		SET_FADE_OUT_AFTER_ARREST(TRUE)
		
		PAUSE_DEATH_ARREST_RESTART(FALSE)
		DISPLAY_HUD(TRUE)
		
		Replay_Control_Fail_Effect(FALSE)
		Replay_Control_Fail_Out_Effect(FALSE)
		Replay_Control_Slow_Motion(FALSE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sOffmissionRespawnAudioScene)
		   	CPRINTLN(DEBUG_RESPAWN, "Stopping audio scene \"", sOffmissionRespawnAudioScene, "\".")
			STOP_AUDIO_SCENE(sOffmissionRespawnAudioScene)
			sOffmissionRespawnAudioScene = ""
		ENDIF
		
		iOffmissionRespawnEffectsStage = 99
	ENDIF
	
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	
	// Generic player state cleanup.
	CLEANUP_PLAYER_STATE_ON_RESPAWN()
   
	// Run this once now. It will be run again in the next stage to ensure it has completed.
	RUN_CUSTOM_CLEANUP_ON_RESPAWN()
	
	// Fix for 1158192. Clear scene disrupted help on respawn.
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AM_H_DISRU")
		CLEAR_HELP(TRUE)
	ENDIF
	IF IS_FLOW_HELP_MESSAGE_QUEUED("AM_H_DISRU")
		REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_DISRU")
	ENDIF
	
	// only run this if g_bReplayDoRejectWarp = FALSE
	// this is set to true if the player fails a mission as a character that is not unlocked in flow
	// in this case we do not want to respawn him at the hospital / police station
	IF g_bReplayDoRejectWarp = FALSE
	
		// Death specific
		IF HAS_PLAYER_HAD_RECENT_DEATH()
			CPRINTLN(DEBUG_RESPAWN, "Processing recent death.")
			
			// Run custom animation and camera on a death #810348
			IF NOT IS_REPLAY_IN_PROGRESS()
			AND NOT HAS_PLAYER_ACCEPTED_REPLAY()
				RUN_DEATH_CUTSCENE()
			ENDIF

			// Deduct some cash (Bug #958353 - 5% of total, max of $5,000)
			INT iCash = (GET_TOTAL_CASH(ePed))
			iCash = FLOOR((TO_FLOAT(iCash/100) * 5.0))
			iCash = CLAMP_INT(iCash, 0, 5000)
			
			// Fix for B*1115962 
			// We don't take money of the player if they are accepting the replay
			IF NOT ((g_replay.replayStageID = RS_ACTIVATE) OR (g_replay.replayStageID = RS_ACCEPTING))
				IF iCash > 0				   
					DO_HOSPITAL_FINANCE_PENALTY(ePed,iCash, GET_CLOSEST_HOSPITAL(GET_PLAYER_COORDS(GET_PLAYER_INDEX())))
				ENDIF
			ENDIF
			
			//Fix for 184547. Hardcoding here saves us having to register savehouses specfifically for the anim test level.
			//This simply forces the player position after a death on the anim test level.
			#IF IS_DEBUG_BUILD
				IF GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_ANIM_TEST
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_PLAYER_START_POS())
					ENDIF
				ENDIF
			#ENDIF
		ENDIF
		
		
		// Arrest specific
		IF HAS_PLAYER_HAD_RECENT_ARREST()
			CPRINTLN(DEBUG_RESPAWN, "Processing recent arrest.")
			
			// Run custom animation and camera on a arrest #810348
			IF NOT IS_REPLAY_IN_PROGRESS()
			AND NOT HAS_PLAYER_ACCEPTED_REPLAY()
				STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
				INT iWeaponSlot
				REPEAT NUM_WEAPONSLOTS iWeaponSlot
					g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)].sWeaponInfo[iWeaponSlot].iAmmoCount = 0
				ENDREPEAT
				REPEAT NUMBER_OF_DLC_WEAPONS iWeaponSlot
					g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)].sDLCWeaponInfo[iWeaponSlot].iAmmoCount = 0
				ENDREPEAT
				RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
				
				RUN_ARREST_CUTSCENE()
			ENDIF
			
			// Deduct some cash (Bug #953002 - 300)
			INT iCash = (GET_TOTAL_CASH(ePed))
			iCash = CLAMP_INT(iCash, 0, 300)
			
			IF iCash > 0
				//DEBIT_BANK_ACCOUNT(ePed, BAAC_LESTER, iCash)
				IF iCash > 0
					DO_POLICE_FINANCE_PENALTY(ePed,iCash,GET_CLOSEST_POLICE_STATION(GET_PLAYER_COORDS(GET_PLAYER_INDEX())))
				ENDIF
			ENDIF
			
			g_bReplayDoRejectWarp = FALSE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_RESPAWN, "Skipping death/arrest cutscene as g_bReplayDoRejectWarp = TRUE")
	ENDIF
	
	// Reinstate all savehouse blips incase we died inside one.
	INT iSavehouse
	REPEAT NUMBER_OF_SAVEHOUSE_LOCATIONS iSavehouse
		Update_Savehouse_Respawn_Blip(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
	ENDREPEAT
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC MONITER_RESPAWN_MENU_SELECTION()
	
	IF (g_eDebug_ForceHospitalRespawn <> NUMBER_OF_HOSPITAL_LOCATIONS)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
			RUN_DEATH_CUTSCENE()
			
			iHospitalSelection = ENUM_TO_INT(g_eDebug_ForceHospitalRespawn)
			g_eDebug_ForceHospitalRespawn = NUMBER_OF_HOSPITAL_LOCATIONS
		ENDIF
	ENDIF
	IF (g_eDebug_ForcePoliceStationRespawn <> NUMBER_OF_POLICE_STATION_LOCATIONS)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
			RUN_ARREST_CUTSCENE()
			
			iPoliceStationSelection = ENUM_TO_INT(g_eDebug_ForcePoliceStationRespawn)
			g_eDebug_ForcePoliceStationRespawn = NUMBER_OF_POLICE_STATION_LOCATIONS
		ENDIF
	ENDIF
	
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD0)
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
			
		CASSERTLN(DEBUG_RESPAWN, "REMOVE_PLAYER_PED_AMMO")
		
		
		STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
		INT iWeaponSlot
		REPEAT NUM_WEAPONSLOTS iWeaponSlot
			g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)].sWeaponInfo[iWeaponSlot].iAmmoCount = 0
		ENDREPEAT
		RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
	ENDIF
	
ENDPROC
#ENDIF



// ===========================================================================================================
//	  Main Script
// ===========================================================================================================

SCRIPT

	CPRINTLN(DEBUG_RESPAWN, "Starting respawn controller.")

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DIRECTOR | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		CPRINTLN(DEBUG_RESPAWN, "respawn_controller.sc has been forced to cleanup (SP to MP or Director Mode).")
		
		CLEANUP_SCRIPT()
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode")) > 0 
		CPRINTLN(DEBUG_RESPAWN, "respawn_controller.sc has been forced to cleanup as Director Mode is already running.")
		CLEANUP_SCRIPT()
	ENDIF
		
	// Setup some debug widgets
	#IF IS_DEBUG_BUILD
	respawn_widget_group_id = SETUP_RESPAWN_CONTROL_WIDGETS()
	#ENDIF
	
	CHECK_RESPAWN_LOCATION_SETUP()
	
	// Main loop
	WHILE (TRUE)
	
		// Maintain the debug widgets
		#IF IS_DEBUG_BUILD
			MAINTAIN_RESPAWN_CONTROL_WIDGETS()
			MONITER_RESPAWN_MENU_SELECTION()
		#ENDIF
		
		// Perform the main processing
		IF (eScriptStage = SCRIPT_STAGE_WAIT_FOR_DEATHARREST)
			IF IS_PED_INJURED(PLAYER_PED_ID())
			OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
			
				iOffmissionRespawnEffectsStage = 0
				eScriptStage = SCRIPT_STAGE_WAIT_FOR_RESPAWN
				CPRINTLN(DEBUG_RESPAWN, "State change WAIT_FOR_DEATHARREST -> WAIT_FOR_RESPAWN")
			ELSE
			
				eScriptStage = SCRIPT_STAGE_WAIT_FOR_MISSION_CLEANUP
				CPRINTLN(DEBUG_RESPAWN, "State change WAIT_FOR_DEATHARREST -> WAIT_FOR_MISSION_CLEANUP")
				
				#IF IS_DEBUG_BUILD
					IF g_bDebug_KeepRespawnControllerRunning
						eScriptStage = SCRIPT_STAGE_WAIT_FOR_DEATHARREST
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		IF (eScriptStage = SCRIPT_STAGE_WAIT_FOR_RESPAWN)
			//Stops Respawn Controller from taking over replay_controller on-mission
			IF NOT	IS_BIT_SET(g_replay.iReplayBits,ENUM_TO_INT(RB_BLOCK_RESPAWN))
				// Check for deatharrest
				IF IS_PLAYER_PLAYING(PLAYER_ID())
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
				

					IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
						PERFORM_RESPAWN_ROUTINE_AFTER_DEATHARREST()
					ENDIF
					
					eScriptStage = SCRIPT_STAGE_WAIT_FOR_MISSION_CLEANUP
					CPRINTLN(DEBUG_RESPAWN, "State change WAIT_FOR_RESPAWN -> WAIT_FOR_MISSION_CLEANUP")
					
					#IF IS_DEBUG_BUILD
						IF g_bDebug_KeepRespawnControllerRunning
							eScriptStage = SCRIPT_STAGE_WAIT_FOR_DEATHARREST
						ENDIF
					#ENDIF
				ELSE
					IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
						PERFORM_OFFMISSION_RESPAWN_EFFECTS()
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_RESPAWN, "Can't process respawn right now as respawn controller is blocked")
			ENDIF
		ENDIF
		
		IF (eScriptStage = SCRIPT_STAGE_WAIT_FOR_MISSION_CLEANUP)
			IF RUN_CUSTOM_CLEANUP_ON_RESPAWN()
				eScriptStage = SCRIPT_STAGE_CLEANUP
				CPRINTLN(DEBUG_RESPAWN, "State change WAIT_FOR_MISSION_CLEANUP -> CLEANUP")
			ENDIF
		ENDIF
		
		IF (eScriptStage = SCRIPT_STAGE_CLEANUP)
			CLEANUP_SCRIPT()
		ENDIF
		
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
