

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "commands_ped.sch"
USING "script_player.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "commands_debug.sch"
USING "commands_replay.sch"
//USING "charsheet_global.sch"

int iped
enumCharacterList ePed

ped_index nearbyBuddy

FUNC BOOL GET_NEARBY_BUDDY(ped_index &pNeabyBuddy)
	
	REPEAT NUM_PLAYER_PED_IDS iPed
		IF DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[iPed])
		AND NOT IS_PED_INJURED(g_piCreatedPlayerPedIDs[iPed])
		//	cprintln(debug_Trevor3,"ped: ",iped)
			IF g_piCreatedPlayerPedIDs[iPed] != PLAYER_PED_ID()
				IF NOT IS_ENTITY_A_MISSION_ENTITY(g_piCreatedPlayerPedIDs[iPed])
			//	cprintln(debug_Trevor3,"not mission ped")
					IF IS_PED_IN_ANY_VEHICLE(g_piCreatedPlayerPedIDs[iPed])
					OR NOT IS_ENTITY_ATTACHED(g_piCreatedPlayerPedIDs[iPed])
					//	cprintln(debug_Trevor3,"not player")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_piCreatedPlayerPedIDs[iPed]),GET_ENTITY_COORDS(player_ped_id(),false)) < 10
						//	cprintln(debug_Trevor3,"close")
							
							ePed = GET_PLAYER_PED_ENUM(g_piCreatedPlayerPedIDs[iPed])
						//	cprintln(debug_Trevor3,"ePed = ",ePed)
							IF ePed = CHAR_MICHAEL
							OR ePed = CHAR_TREVOR
							OR ePed = CHAR_FRANKLIN
					//			cprintln(debug_Trevor3,"is a player character")
								IF ePed != GET_CURRENT_PLAYER_PED_ENUM()
									IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(g_piCreatedPlayerPedIDs[iPed],PLAYER_PED_ID())
										vehicle_index aVeh
										IF IS_PED_IN_ANY_VEHICLE(g_piCreatedPlayerPedIDs[iPed])										
											aVeh = GET_VEHICLE_PED_IS_IN(g_piCreatedPlayerPedIDs[iPed])										
										ENDIF
										
										IF (DOES_ENTITY_EXIST(aVeh) AND IS_VEHICLE_DRIVEABLE(aveh))
										OR NOT DOES_ENTITY_EXIST(aVeh)					
								    		pNeabyBuddy = g_piCreatedPlayerPedIDs[iPed]
											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
						    ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_SCREEN_FADED_OUT()
	//	cprintln(debug_Trevor3,"Exit cos screenfaded")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

	/*GET_PED_NEARBY_PEDS(player_ped_id(),pedArray,PEDTYPE_INVALID)
			
	REPEAT COUNT_OF(pedArray) i
		IF NOT IS_PED_INJURED(pedArray[i])
			THIS_MODEL = GET_ENTITY_MODEL(pedArray[i]) 
			
			IF THIS_MODEL = MODEL_FRANKLIN
			OR THIS_MODEL = MODEL_TREVOR
			OR THIS_MODEL = MODEL_MICHAEL
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedArray[i]),GET_ENTITY_COORDS(player_ped_id(),false)) < 10
					pNeabyBuddy = pedArray[i]
					RETURN TRUE
				ENDIF
			ENDIF				
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC*/

PROC DO_DEATH_COMMENT(ped_index speakingPed)
//	cprintln(debug_trevor3,"DO_DEATH_COMMENT wait")
	int iPauseTime = GET_GAME_TIMER() + 1000
	WHILE GET_GAME_TIMER() < iPauseTime
	AND NOT IS_SCREEN_FADED_OUT() //force skip this bit if screem fades out
		wait(0)
	ENDWHILE
//	cprintln(debug_trevor3,"DO_DEATH_COMMENT go")
	IF NOT IS_PED_INJURED(speakingPed)		
	//	cprintln(debug_trevor3,"Play speaking line")
		IF ePed = CHAR_FRANKLIN			
			SWITCH GET_PLAYER_PED_ENUM(player_ped_id())
				CASE CHAR_TREVOR
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_TREVOR_DEATH", "FRANKLIN_NORMAL",SPEECH_PARAMS_FORCE)//SPEECH_PARAMS_ALLOW_REPEAT)  // | SPEECH_PARAMS_FORCE_FRONTEND)			
				BREAK
				CASE CHAR_MICHAEL
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_MICHAEL_DEATH", "FRANKLIN_NORMAL",SPEECH_PARAMS_FORCE)
				BREAK
			ENDSWITCH
		ELIF ePed = CHAR_TREVOR
			SWITCH GET_PLAYER_PED_ENUM(player_ped_id())
				CASE CHAR_FRANKLIN
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_FRANKLIN_DEATH", "TREVOR_NORMAL",SPEECH_PARAMS_FORCE)//SPEECH_PARAMS_ALLOW_REPEAT)  // | SPEECH_PARAMS_FORCE_FRONTEND)			
				BREAK
				CASE CHAR_MICHAEL
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_MICHAEL_DEATH", "TREVOR_NORMAL",SPEECH_PARAMS_FORCE)
				BREAK
			ENDSWITCH
			//,SPEECH_PARAMS_ALLOW_REPEAT)// | SPEECH_PARAMS_FORCE_FRONTEND)
		ELIF ePed = CHAR_MICHAEL
			SWITCH GET_PLAYER_PED_ENUM(player_ped_id())
				CASE CHAR_TREVOR
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_TREVOR_DEATH", "MICHAEL_NORMAL",SPEECH_PARAMS_FORCE)//SPEECH_PARAMS_ALLOW_REPEAT)  // | SPEECH_PARAMS_FORCE_FRONTEND)			
				BREAK
				CASE CHAR_FRANKLIN
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(speakingPed, "BUDDY_SEES_FRANKLIN_DEATH", "MICHAEL_NORMAL",SPEECH_PARAMS_FORCE)
				BREAK
			ENDSWITCH
		ENDIF	
	ENDIF
ENDPROc

PROC DEATH_RESPONSE()
	
	WHILE NOT GET_NEARBY_BUDDY(nearbyBuddy)	
		WAIT(0)
	ENDWHILE
	IF NOT IS_PED_INJURED(nearbyBuddy)
		IF NOT g_bDontLetBuddiesReactToNextPlayerDeath
			SET_ENTITY_AS_MISSION_ENTITY(nearbyBuddy,TRUE,TRUE)					
		
			IF GET_SCRIPT_TASK_STATUS(nearbyBuddy,SCRIPT_TASK_ANY) != FINISHED_TASK			
				CLEAR_PED_TASKS(nearbyBuddy)			
			ENDIf

		
			vector vTemp = GET_ENTITY_COORDS(player_ped_id(),false)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(nearbyBuddy,TRUE)
			
			sequence_index seq
			OPEN_SEQUENCE_TASK(seq)
			IF NOT IS_PED_IN_ANY_VEHICLE(nearbyBuddy)
				IF NOT IS_PED_IN_COMBAT(nearbyBuddy)
				AND NOT IS_PED_IN_ANY_VEHICLE(nearbyBuddy)
					TASK_TURN_PED_TO_FACE_COORD(null,vTemp,6000)
				ENDIF
			ENDIF
			TASK_LOOK_AT_COORD(null,vTemp,6000)	
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(nearbyBuddy,seq)			
		
		ENDIF	
		DO_DEATH_COMMENT(nearbyBuddy)
	ENDIF
	
	WHILE NOT IS_SCREEN_FADED_OUT()
		WAIT(0)
	ENDWHILE

	//cleanup grabbed ped
	
	IF DOES_ENTITY_EXIST(nearbyBuddy)
		IF NOT IS_PED_INJURED(nearbyBuddy)
			SET_PED_KEEP_TASK(nearbyBuddy,TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(nearbyBuddy)
	ENDIF
	g_bDontLetBuddiesReactToNextPlayerDeath = FALSE
	
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
	OR REPLAY_SYSTEM_HAS_REQUESTED_A_SCRIPT_CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	DEATH_RESPONSE()
	
ENDSCRIPT
