//╒═════════════════════════════════════════════════════════════════════════════╕
//│               Author: Kevin Bolt              Date: 25/11/14                │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│                                                                             │
//│                          Director Mode Controller                           │
//│                                                                             │
//│     Give players more control over how they set up their 'movie scenes'.    │
//│     This includes things like the 'actor' they play, where the 'movie set   │
//│     is located, 'movie extras', 'Props' placement, and a number of other    │
//│     elements to help players make custom, streamlined videos.               │
//│                                                                             │
//│     https://devstar.rockstargames.com/wiki/index.php/NGREPLAY_DIRECTORMODE  │
//│                                                                             │
//╘═════════════════════════════════════════════════════════════════════════════╛
 

USING "globals.sch"
USING "rage_builtins.sch"

USING "commands_replay.sch"
USING "commands_audio.sch"
USING "candidate_public.sch"
USING "snapshot_private.sch"

USING "net_celebration_Screen.sch"
USING "net_interactions.sch"
USING "animals_private.sch"

USING "hud_drawing.sch"
USING "script_heist.sch"
USING "snapshot_private.sch"
USING "director_mode_public.sch"
USING "fmmc_corona_controller.sch"
USING "rich_presence_public.sch"

#IF FEATURE_SP_DLC_DIRECTOR_MODE

bool  bResetMenuNow		//This NEEDS to be here so the prop editor can reset the PI help when a prop is snappable

#IF FEATURE_SP_DLC_DM_PROP_EDITOR
USING "director_mode_props.sch"
#ENDIF

INT iMissionLaunchID = NO_CANDIDATE_ID
m_enumMissionCandidateReturnValue eMissionLaunchResult

enumCharacterList eStoryCharacter = CHAR_BLANK_ENTRY  // The character the player was before swapping in to Director Mode.
INT iSpecialStatValue = 0 // characters special ability bar % value

#IF FEATURE_GEN9_STANDALONE
SCRIPT_ROUTER_CONTEXT_MODE eExitMode = SRCM_INVALID
#ENDIF

//========================================== PI MENU ITEMS ============================================

// SUB MENU 0
ENUM DIRECTOR_MODE_PI_M0
	PI_M0_SETTINGS = 0,
	PI_M0_LOCATION,
	PI_M0_GESTURE,
	PI_M0_SPEECH,
	PI_M0_SHORTLIST,
	PI_M0_VEHICLE,
#IF FEATURE_SP_DLC_DM_PROP_EDITOR
	PI_M0_PROPS,
#ENDIF
	PI_M0_EDITOR,
	PI_M0_RETURN,
	PI_M0_EXIT,
#IF FEATURE_GEN9_STANDALONE
	PI_M0_MAIN_MENU,
#ENDIF
	PI_M0_MAX
ENDENUM

// -----------------------------

// SUB MENU 1 - VEHICLES
CONST_INT   PI_M1_VEH1   0
CONST_INT   PI_M1_VEH2   1
CONST_INT   PI_M1_VEH3   2
CONST_INT   PI_M1_VEH4   3
CONST_INT   PI_M1_VEH5   4
CONST_INT   PI_M1_VEH6   5
CONST_INT   PI_M1_VEH7   6
CONST_INT   PI_M1_VEH8   7
CONST_INT   PI_M1_VEH9   8
CONST_INT   PI_M1_VEH10  9
CONST_INT   PI_M1_VEH11  10 
CONST_INT   PI_M1_VEH12  11
CONST_INT   PI_M1_VEHMAX NUMBER_OF_BUYABLE_VEHICLES_SP//Altering this from TOTAL_BUYABLE_VEHICLES as part of 3190036 fix. It probably doesn't need to be this size at all, just 12 would suffice but this is the safest option.
CONST_INT   PI_M1_VEHGENMAX 21

int         iMaxVehs = 0

//----------------------------------

//SUB MENU 2 - SETTINGS
//  Uses the SEM_TOTAL const ints
//----------------------------------

CONST_INT       PI_SUBMENU_NONE         0
CONST_INT       PI_SUBMENU_VEHICLES     1
CONST_INT       PI_SUBMENU_SETTINGS     2
#IF FEATURE_SP_DLC_DM_PROP_EDITOR
CONST_INT       PI_SUBMENU_PROPS        3
CONST_INT       MAX_SUBMENUS            4
#ENDIF
#IF NOT FEATURE_SP_DLC_DM_PROP_EDITOR
CONST_INT       MAX_SUBMENUS            3
#ENDIF

CONST_INT       MAX_MENUS               17

CONST_INT       c_iMAX_PI_MENU_OPTIONS  7

STRUCT PI_LAUNCHER_MENU_STRUCT
    INT                         iSelection[MAX_MENUS][MAX_SUBMENUS]
    TEXT_LABEL_15               tl15
    INT                         iCurrentSelection
    INT                         iPreviousSelection
ENDSTRUCT

PI_LAUNCHER_MENU_STRUCT         sPI_MenuData
int                             iSubMenu                    = PI_SUBMENU_NONE
INT                             iStoredSelection
INT                             ijustClosedTimer
CONST_INT                       MENU_UPDATE_TIME                10000   // increased to 10 seconds for refresh other inputs will also update the menu
CONST_INT                       ci_MENU_HELP_KEYS_REFRESH_TIME  500     //  Refresh the help keys every 500ms
int                             iMenuUpdateTimer,   iMenuHelpKeysTimer
int                             iMaxLocationsUnlocked
VEHICLE_SETUP_STRUCT            sVehData
VEHICLE_SETUP_STRUCT_MP         sVehDataMPInUse
INT                             iVehicleSelectDelay

// bit set checks
INT             iBoolsBitSet            =   0   
CONST_INT       biSwitchButtonCheck         0
CONST_INT       biM_MenuSetup               1

BLIP_INDEX      blipVehPersonal
Vehicle_index   VehCurrentPersonal
model_names     mVehCurrentPersonal = DUMMY_MODEL_FOR_SCRIPT
bool            bCreateVehicle      = False
int             iVehicleCreateStage = 0
INT             iWarpStage          = 0
INT				iWarpTimeout		= 0
bool            bPedInVehicle       = False
MODEL_NAMES     eAvailableVehs[PI_M1_VEHMAX]
VEHICLE_GEN_NAME_ENUM eDynamicVehGens[PI_M1_VEHGENMAX] 
INT             eFromVehGen[PI_M1_VEHMAX]
//INT               iMaxLoadVehTime

//=================================================================================================================

CONST_INT cTOTAL_ANIMS 8

//brief scaleform 
float fLastAspectRatio
//float drawCoordFromMenu
FLOAT                     sf_y =       0.916
FLOAT                     sf_h =       1.000
FLOAT                     sf_x =       0.113 //0.1485
FLOAT                     sf_w =       0.2254 //0.297

FLOAT                     bg_y =       0.0415

FLOAT                     line_x =       0.112400
FLOAT                     line_w =       0.225

#if IS_DEBUG_BUILD

BOOL    outputMenudimensions
FLOAT                     wl_x =        0.113 //0.1458
FLOAT                     wl_y =        1.005100
FLOAT                     wl_w =        0.240600
FLOAT                     wl_h =        1.050000
INT                       w1_a =        128

FLOAT                     messageBox_w =       0.127
FLOAT                     messageBox_h =       1.0

#endif

FLOAT                     messageBox_x =       0.1134
FLOAT                     messageBox_y =       0.9586
FLOAT                     line_y =       0.415 
FLOAT                     line_h =       0.0027

FLOAT newScaleformX,newScaleformY

/*
TWEAK_FLOAT                     sf_x        0.134
TWEAK_FLOAT                     sf_y        0.929
TWEAK_FLOAT                     sf_w        0.267//0.225
TWEAK_FLOAT                     sf_h        1.000//0.225

TWEAK_FLOAT                     wl_x        0.125
TWEAK_FLOAT                     wl_y        0.427
TWEAK_FLOAT                     wl_w        0.25//0.225
TWEAK_FLOAT                     wl_h        0.003//0.225
TWEAK_INT                       w1_a        128*/

//possibly move this to definitions header file...?
ENUM enum_buildSceneState
    BSS_INIT,
    BSS_TRIGGER_PLAYER,
    BSS_WAIT_ON_CAMERA,
    BSS_BUILT
ENDENUM

enum_buildSceneState buildSceneState

ENUM enum_cameraState
    DIR_CAM_PENDING,
    DIR_CAM_GAME_FADE_OUT,
    DIR_CAM_GAME_FADED_OUT,
    DIR_CAM_PREP_TRAILER_SHOT,  
    DIR_TRAILER_CAM_WAIT_FOR_FADE_IN,
    DIR_TRAILER_CAM_FADE_IN,
    DIR_CAM_TRAILER_PANNING_TO_SMALL_ANIMALS,
    DIR_CAM_TRAILER_PANNING_TO_ANIMALS,
    DIR_CAM_TRAILER_PANNING_TO_TRAILER,
    DIR_CAM_TRAILER_AT_TRAILER,
    DIR_CAM_TRAILER_WAIT_FOR_WALKIN,
    DIR_CAM_TRAILER_CAMERA_TRIGGER_WALKIN,
    DIR_CAM_LEADOUT,
    DIR_CAM_ENTER_PLAY_MODE,
    DIR_CAM_ENTER_PLAY_MODE_DO_SHAPETEST,
    DIR_CAM_PLAY_MODE,
    DIR_CAM_MAP_WARP_REPOSITION,
    DIR_CAM_FADE_IN_FROM_WARP,
    DIR_CAM_TRAILER_TO_SMALL_ANIMALS,
    DIR_CAM_TRAILER_TO_ANIMALS,
    DIR_CAM_TRAILER_AT_ANIMALS,
    DIR_CAM_TRAILER_AT_SMALL_ANIMALS,
    DIR_CAM_TRAILER_TO_TRAILER
    
ENDENUM

//float panCamSpeed
enum_cameraState dirCamState
int iFadeTimer
bool bDoWalkin
BOOL bReturningFromActorMode
BOOL bScaleformShowing

ENUM enum_directorFlowState
    dfs_invalid,
    dfs_INIT,
    dfs_PREP_TRAILER_SCENE,
    dfs_RUN_TRAILER_SCENE,
    dfs_enter_PLAY_MODE,
    dfs_PLAY_MODE,
    dfs_warp_to_location,
    dfs_swap_player_model,
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR dfs_query_prop_scene_clear, dfs_query_overwrite_scene, dfs_query_load_scene, dfs_query_exit_scene, dfs_warp_player_to_prop , #ENDIF
    dfs_shortlist_full_query,
    dfs_query_editor,
    dfs_query_trailer,  
    dfs_query_terminate,
    dfs_request_terminate,
    dfs_terminated
ENDENUM

enum_directorFlowState directorFlowState = dfs_INIT, lastValidDirectorFlowState, directorFlowStateBeforeQuery

ENUM enum_dirMenuState
    dirMENU_LOAD,
    dirMENU_UPDATE,
    dirMENU_MOVE_TO_PLAY_MODE,
    dirMENU_PERFORMING_PAN,
    dirMENU_PLAY_MODE
ENDENUM

enum_dirMenuState dirMenuState

//animal state
ENUM PLAYER_ANIMAL_STATE 
    PAS_UNKNOWN,
    PAS_IS_AN_ANIMAL,
    PAS_IS_HUMAN
ENDENUM

PLAYER_ANIMAL_STATE ePlayerAnimalState

int iTotalMenuOptions,curMenuItem,iMenuItem
int iCurrentSelectedModel

// **************** Unfortunately we realised late on that this enum list is used in such a way that inserting items in to it will fuck up people's save game data **************
// *************** If more peds or categories are wanted to be added either 1) DONT or 2) add to the end of the enum list and think of a way to modify the script *************
// *************** so it can read from that data. Sorry. ***************

ENUM enumMenuCategory 
    MC_VOID,
    MC_SEX,
    MC_CURRENT,
    MC_MAIN,
    
    MC_ACTORS, 
    MC_SETTINGS,
    MC_OPTION,
    MC_SHORTLIST,
    MC_RECENTLYUSED,
    MC_QUIT_TO_MAIN_MENU,
    MC_QUIT_TO_STORY,
    MC_QUIT_TO_GTA_ONLINE,
    MC_PLAYDM,
    
    // ***************** BELOW IS A LIST OF EMPTY MENU ENUMS THAT CAN BE RENAMED POST RELEASE IF MORE MENU ITEMS ARE NEEDED **********************
    MC_EMPTY_MENU_1,
    MC_EMPTY_MENU_2,
    MC_EMPTY_MENU_3,
    MC_EMPTY_MENU_4,
    MC_EMPTY_MENU_5,
    MC_EMPTY_MENU_6,
    MC_EMPTY_MENU_7,
    MC_EMPTY_MENU_8,
    MC_EMPTY_MENU_9,
    MC_EMPTY_MENU_10,
    MC_EMPTY_MENU_11,
    MC_EMPTY_MENU_12,
    MC_EMPTY_MENU_13,
    MC_EMPTY_MENU_14,
    MC_EMPTY_MENU_15,
    MC_EMPTY_MENU_16,
    MC_EMPTY_MENU_17,
    MC_EMPTY_MENU_18,
    MC_EMPTY_MENU_19,
    MC_EMPTY_MENU_20,
    // ************** !!!!!!!DO NOT ADD ANY ENUMS BELOW HERE POST RELEASE!!!!!!!!!! *************************************

    
    MC_START_ACTOR_CATEGORIES, 
        
    MC_ANIMALS,  
    MC_BEACH,
    MC_COSTUME,
    MC_DOWNTOWN,
    MC_EMERGENCY,
    MC_GANG,
    MC_HEIST,
    MC_LABOURERS,   // ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_MILITARY,
    MC_ONLINE,
    MC_PROFESSIONALS,
    MC_SPECIAL,
    MC_SPORT,
    MC_STORY,
    MC_TRANSPORT, //17
    MC_UPTOWN,
    MC_VAGRANTS,  // ************** !!!!!!!DO NOT ADD ANY ENUMS ABOVE HERE POST RELEASE!!!!!!!!!! *************************************
    
    MC_CATEGORY_EMPTY_01,
    MC_CATEGORY_EMPTY_02,
    MC_CATEGORY_EMPTY_03,
    MC_CATEGORY_EMPTY_04,
    MC_CATEGORY_EMPTY_05,
    MC_CATEGORY_EMPTY_06,
    MC_CATEGORY_EMPTY_07,
    MC_CATEGORY_EMPTY_08,
    MC_CATEGORY_EMPTY_09,
    MC_CATEGORY_EMPTY_10,
    MC_CATEGORY_EMPTY_11,
    MC_CATEGORY_EMPTY_12,
    MC_CATEGORY_EMPTY_13,
    MC_CATEGORY_EMPTY_14,
    MC_CATEGORY_EMPTY_15,
    MC_CATEGORY_EMPTY_16,
    MC_CATEGORY_EMPTY_17,
    MC_CATEGORY_EMPTY_18,
    MC_CATEGORY_EMPTY_19,
    MC_CATEGORY_EMPTY_20,
    MC_CATEGORY_EMPTY_21,
    MC_CATEGORY_EMPTY_22,
    MC_CATEGORY_EMPTY_23,
    MC_CATEGORY_EMPTY_24,
    MC_CATEGORY_EMPTY_25,
    MC_CATEGORY_EMPTY_26,
    MC_CATEGORY_EMPTY_27,
    MC_CATEGORY_EMPTY_28,
    MC_CATEGORY_EMPTY_29,
    MC_CATEGORY_EMPTY_30,

    
    MC_END_ACTOR_CATEGORIES, // ************** !!!!!!!DO NOT ADD ANY ENUMS BELOW HERE POST RELEASE!!!!!!!!!! *************************************
    MC_START_ACTORS,
    
    MC_PROAIR, 
    MC_PROBAR,
    MC_PROBOU,
    MC_PROCEO,
    MC_PROCHE,
    MC_PROCOO,
    MC_PRODOC,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_PRODOR,
    MC_PRODRA,
    MC_PROIT, 
    MC_LABMAI,
    MC_PROMAR,
    MC_PROOFF,
    MC_PROSCI,
    MC_PROSTY,
    MC_PROVAL,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_PROVEN,
    MC_PROWAI,
    
    MC_PROAIH, 
    MC_PROAUP,
    MC_PROBIN,
    MC_PROFAS,
    MC_PROHAI,
    MC_PROINT,
    MC_PROLAW,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_PROPON,
    MC_PROREL,
    MC_PROSTR,
    MC_PROTEA,
    
    MC_UPTEAS,
    MC_UPTECC,
    MC_UPTHAW,
    MC_UPTMIR,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_UPTROC,  
    MC_UPTVIN,
    MC_UPTHIP,
    MC_UPTCOU,
    
    MC_UPTART,
    MC_UPTBOH,
    MC_UPTPOR,
    MC_UPTRIC,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_UPTROH,
    MC_UPTVIB,
    MC_UPTWAN,
    
    //downtown
    MC_DOWMAZ,
    MC_DOWLAS,
    MC_DOWSTR,
    MC_DOWRAN,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_DOWDAL,
    MC_DOWHAR,
    MC_DOWCO2,
    MC_DOWSTM,
    
    MC_DOWEMO,
    MC_DOWEM2,
    MC_DOWTEE,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_DOWCOL,
    MC_DOWSOC,
    MC_DOWDAV,
    
    //vagrants
    MC_VAGMIS,
    MC_VAGDO1,
    MC_VAGDO2,
    MC_VAGMET,
    MC_VAGSAN,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_VAGME2,
    MC_VAGOLD,
    MC_VAGYOU,
    MC_VAGPR1,
    MC_VAGPR2,
    
    //beach bums
    MC_BEATOU,
    MC_BEALOA,
    MC_BEAOLD,
    MC_BEAVES,
    MC_BEADEL,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_BEASUN,
    MC_BEASRF,
    MC_BEABO1,
    MC_BEABO2,
    MC_BEABOD,
    
    //sports
    MC_SPOYOG,
    MC_SPOSUR,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_SPOSKA,
    MC_SPOSK1,
    MC_SPOSK2,
    MC_SPOSK3,
    MC_SPOSK4,
    MC_SPOSK5,
    MC_SPORU1,
    MC_SPORU2,
    MC_SPORUN,
    MC_SPOCYC,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_SPOROC,
    MC_SPOMO1,
    MC_SPOMO2,
    MC_SPOTEN,
    MC_SPOGO1,
    MC_SPOGO2,
    MC_SPOGOF,
    
    MC_GANTRB,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_GANTG1,
    MC_GANTG2,
    MC_GANKOB,
    MC_GANKOL,
    MC_GANKOG,
    MC_GANCHO,
    MC_GANEPS1,
    MC_GANEPS2,
    MC_GANEPSF,
    MC_GANSTR,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_GANAZB,
    MC_GANVAB,
    MC_GANAZG,
    MC_GANVAG,
    MC_GANMGB,
    MC_GANMGG,
    MC_GANMGL,
    MC_GANMOB,
    MC_GANMOG,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_GANMOL,
    MC_GANLOO,
    MC_GANLOM,
    MC_GANLOS,
    MC_GANLOH,
    MC_GANBAE,
    MC_GANBLO,
    MC_GANOGB,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_GANBAS,
    MC_GANCHA,
    MC_GANDAV,
    MC_GANFOR,
    MC_GANALT,
    MC_GANAL1,
    MC_GANAL2,
    MC_GANAL3,
    MC_GANCULF,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    CG_GANCUL1,
    MC_GANCUL2,
    
    //costume
    MC_COSAST,
    MC_COSSPY,
    MC_COSCOR,
    MC_COSPOG,
    MC_COSSPA,
    MC_COSALI,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_COSCLO,
    MC_COSMIM,
    MC_COSSTR,
    
    MC_MILPL1,
    MC_MILPL2,
    MC_MILMIG,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_MILSRG,
    MC_MILCOR,
    MC_MILPRI,
    MC_MILBAT,
    MC_MILMIM,
    MC_MILMEP,
    MC_MILMEC,
    MC_MILMEL,
    
    MC_LABFAR,
    MC_LABMIG,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_LABCN1,
    MC_LABCN2,
    MC_LABCN3,
    MC_LABGAB,
    MC_LABFAC,
    MC_LABGAR,
    MC_LABJAN,
    MC_LABSAD,
    MC_LABDOC,
    MC_LABME1,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_LABME2,
    MC_LABBUG,
    
    MC_EMEFIR,
    MC_EMEPOL,
    MC_EMEFIB,
    MC_EMENOO,
    MC_EMEDOA,
    MC_EMEIAA,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_EMEHIG,
    MC_EMEPAR,
    MC_EMECOA,
    MC_EMERAN,
    MC_EMEBAY,
    MC_EMESHE,

    MC_TRAAR1,
    MC_TRAAR2,
    MC_TRAAIR,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_TRAPO1,
    MC_TRAPO2,
    MC_TRAPO3,
    MC_TRAPO4,
    MC_TRATRA,
    MC_TRATRU,

    MC_STOFRA,
    MC_STOTRE,
    MC_STOMIC,
    MC_STOAMA,
    MC_STOBEV,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_STOBRA,
    MC_STOCHR,
    MC_STODAV,
    MC_STODEV,
    MC_STODRF,
    MC_STOFAB,
    MC_STOFLO,
    MC_STOJIM,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_STOLAM,
    MC_STOLAZ,
    MC_STOLES,
    MC_STOMAU,
    MC_STOTHO,
    MC_STONER,
    MC_STOPAT,
    MC_STOSIM,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_STOSOL,
    MC_STOSTE,
    MC_STOSTR,
    MC_STOTAN,
    MC_STOTAO,
    MC_STOTRA,
    MC_STOWAD, //128
    
    
    
    MC_SPEAND,
    MC_SPEBAY,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_SPEBIL,
    MC_SPECLI,
    MC_SPEGRI,
    MC_SPEJAN,
    MC_SPEJER,
    MC_SPEJES,
    MC_SPEMAN,
    MC_SPEMIM,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_SPEPAM,
    MC_SPEIMP,
    MC_SPEZOM, //141
    

    
    MC_HEIGUS,
    MC_HEIKAR,
    MC_HEIPAC,
    MC_HEICHE,
    MC_HEIHUG,// ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_HEINOR,
    MC_HEIDAR,
    MC_HEIPAI,
    MC_HEICHR,
    MC_HEIRIC,
    MC_HEIEDD,
    MC_HEITAL,
    MC_HEIKRM, //154    


    // ************** !!!!!!!DO NOT ADD ANY ENUMS HERE POST RELEASE!!!!!!!!!! *************************************
    MC_ONLINE1,
    MC_ONLINE2, //156 
    
    MC_ACTOR_EMPTY_1, // *****************   THIS IS A LIST OF UNUSED ENUMS THAT CAN BE RENAMED AT A LATER DATE. MOVE THIS COMMENT DOWN AFTER MODIFYING **********************************
    MC_ACTOR_EMPTY_2,
    MC_ACTOR_EMPTY_3,
    MC_ACTOR_EMPTY_4,
    MC_ACTOR_EMPTY_5,
    MC_ACTOR_EMPTY_6,
    MC_ACTOR_EMPTY_7,
    MC_ACTOR_EMPTY_8,
    MC_ACTOR_EMPTY_9,
    MC_ACTOR_EMPTY_10,
    MC_ACTOR_EMPTY_11,
    MC_ACTOR_EMPTY_12,
    MC_ACTOR_EMPTY_13,
    MC_ACTOR_EMPTY_14,
    MC_ACTOR_EMPTY_15,
    MC_ACTOR_EMPTY_16,
    MC_ACTOR_EMPTY_17,
    MC_ACTOR_EMPTY_18,
    MC_ACTOR_EMPTY_19,
    MC_ACTOR_EMPTY_20,
    MC_ACTOR_EMPTY_21,
    MC_ACTOR_EMPTY_22,
    MC_ACTOR_EMPTY_23,
    MC_ACTOR_EMPTY_24,
    MC_ACTOR_EMPTY_25,
    MC_ACTOR_EMPTY_26,
    MC_ACTOR_EMPTY_27,
    MC_ACTOR_EMPTY_28,
    MC_ACTOR_EMPTY_29,
    MC_ACTOR_EMPTY_30,
    MC_ACTOR_EMPTY_31,
    MC_ACTOR_EMPTY_32,
    MC_ACTOR_EMPTY_33,
    MC_ACTOR_EMPTY_34,
    MC_ACTOR_EMPTY_35,
    MC_ACTOR_EMPTY_36,
    MC_ACTOR_EMPTY_37,
    MC_ACTOR_EMPTY_38,
    MC_ACTOR_EMPTY_39,
    MC_ACTOR_EMPTY_40,
    MC_ACTOR_EMPTY_41,
    MC_ACTOR_EMPTY_42,
    MC_ACTOR_EMPTY_43,
    MC_ACTOR_EMPTY_44,
    MC_ACTOR_EMPTY_45,
    MC_ACTOR_EMPTY_46,
    MC_ACTOR_EMPTY_47,
    MC_ACTOR_EMPTY_48,
    MC_ACTOR_EMPTY_49,
    MC_ACTOR_EMPTY_50,
    MC_ACTOR_EMPTY_51,
    MC_ACTOR_EMPTY_52,
    MC_ACTOR_EMPTY_53,
    MC_ACTOR_EMPTY_54,
    MC_ACTOR_EMPTY_55,
    MC_ACTOR_EMPTY_56,
    MC_ACTOR_EMPTY_57,
    MC_ACTOR_EMPTY_58,
    MC_ACTOR_EMPTY_59,
    MC_ACTOR_EMPTY_60,
    MC_ACTOR_EMPTY_61,
    MC_ACTOR_EMPTY_62,
    MC_ACTOR_EMPTY_63,
    MC_ACTOR_EMPTY_64,
    MC_ACTOR_EMPTY_65,
    MC_ACTOR_EMPTY_66,
    MC_ACTOR_EMPTY_67,
    MC_ACTOR_EMPTY_68,
    MC_ACTOR_EMPTY_69,
    MC_ACTOR_EMPTY_70,
    MC_ACTOR_EMPTY_71,
    MC_ACTOR_EMPTY_72,
    MC_ACTOR_EMPTY_73,
    MC_ACTOR_EMPTY_74,
    MC_ACTOR_EMPTY_75,
    MC_ACTOR_EMPTY_76,
    MC_ACTOR_EMPTY_77,
    MC_ACTOR_EMPTY_78,
    MC_ACTOR_EMPTY_79, 
    MC_ACTOR_EMPTY_80,
    MC_ACTOR_EMPTY_81,
    MC_ACTOR_EMPTY_82,
    MC_ACTOR_EMPTY_83,
    MC_ACTOR_EMPTY_84,
    MC_ACTOR_EMPTY_85,
    MC_ACTOR_EMPTY_86,
    MC_ACTOR_EMPTY_87,
    MC_ACTOR_EMPTY_88,
    MC_ACTOR_EMPTY_89,
    MC_ACTOR_EMPTY_90,
    MC_ACTOR_EMPTY_91,
    MC_ACTOR_EMPTY_92,
    MC_ACTOR_EMPTY_93,
    MC_ACTOR_EMPTY_94,
    MC_ACTOR_EMPTY_95,
    MC_ACTOR_EMPTY_96,
    MC_ACTOR_EMPTY_97,
    MC_ACTOR_EMPTY_98,
    MC_ACTOR_EMPTY_99,
    MC_ACTOR_EMPTY_100,

    // *****************  !!! END OF EMPTY ENUMS. DO NOT EVER EVER EVER ADD ANY MORE ENUMS!!! **********************************

    

    MC_ANIMALS_START, // ********************* DO NOT ADD ENUMS BELOW HERE POST RELEASE **********************
    MC_ANIBOAR,
     MC_ANICAT,
     MC_ANICOW, 
     MC_ANICOY, // ********************* DO NOT ADD ENUMS HERE POST RELEASE **********************
     MC_ANIDEE, 
     MC_ANIHUS, 
     MC_ANIMOU, 
     MC_ANIPIG, 
     MC_ANIPOO, 
     MC_ANIPUG, // ********************* DO NOT ADD ENUMS HERE POST RELEASE **********************
     MC_ANIRAB, 
     MC_ANIRET, 
     MC_ANIROT, 
     MC_ANISHE, 
     MC_ANIWES, 
     MC_ANICHI, // ********************* DO NOT ADD ENUMS HERE POST RELEASE **********************
     MC_ANICOR,
     MC_ANICRO, 
     MC_ANIHEN, 
     MC_ANIPIN, 
     MC_ANISEA,
     MC_ANISAS, // ********************* DO NOT ADD ENUMS ABOVE HERE POST RELEASE**********************
     MC_ANI_EMPTY_01, // ********************* EMPTY ENUMS BELOW THAT CAN BE RENAMED POST RELEASE **********************
     MC_ANI_EMPTY_02,
     MC_ANI_EMPTY_03,
     MC_ANI_EMPTY_04,
     MC_ANI_EMPTY_05,
     MC_ANI_EMPTY_06,
     MC_ANI_EMPTY_07,
     MC_ANI_EMPTY_08,
     MC_ANI_EMPTY_09,
     MC_ANI_EMPTY_10,
     MC_ANI_EMPTY_11,
     MC_ANI_EMPTY_12,
     MC_ANI_EMPTY_13,
     MC_ANI_EMPTY_14,
     MC_ANI_EMPTY_15,
     MC_ANI_EMPTY_16,
     MC_ANI_EMPTY_17,
     MC_ANI_EMPTY_18,
     MC_ANI_EMPTY_19,
     MC_ANI_EMPTY_20,
     MC_ANI_EMPTY_21,
     MC_ANI_EMPTY_22,
     MC_ANI_EMPTY_23,
     MC_ANI_EMPTY_24,
     MC_ANI_EMPTY_25,
     MC_ANI_EMPTY_26,
     MC_ANI_EMPTY_27,
     MC_ANI_EMPTY_28,
     MC_ANI_EMPTY_29,
     MC_ANI_EMPTY_30, // ********************* EMPTY ENUMS ABOVE THAT CAN BE MODIFIED POST RELEASE **********************
     MC_ANIMALS_END,
     MC_END_ACTORS 
     
     // ********************* MORE ENUMS CAN BE ADDED BELOW HERE **********************
ENDENUM

enumMenuCategory dirCurrentMenu = MC_MAIN

STRUCT structMenuData
    enumMenuCategory category
    INT index
    bool isMale,isFemale,selectable
ENDSTRUCT

structMenuData displayedMenuData[48]
enumMenuCategory activeMenuEntry
int activeMenuIndex
enumMenuCategory lastSelectableModelCategory
int lastSelectableModelIndex
//enumMenuCategory currentPlayerVariation,loadingVariation

enum enumMenuType
    MENUTYPE_MAINMENU,
    MENUTYPE_PI
endenum
enumMenuType selectedMenuType           

STRUCT structAnimalAnimSound
    TEXT_LABEL_63 strBarkDict
    TEXT_LABEL_15 strBarkAnim
    AnimalSoundData sBarkSound
    INT iBarkSoundID = -1
    INT barkTimer
ENDSTRUCT
structAnimalAnimSound animalAnimSoundData
bool audioUnloaded

struct structShortlistData
    enumMenuCategory category
    int modelVariation[13]
    int textureVariation [11]
    MODEL_NAMES model
    int modelVariationIndex
    bool bMale
    INT iSlot
    INT iVoiceHash
    
ENDSTRUCT

structShortlistData shortlistData[20] //0-9 = shortlist 10-19 = recently used
CONST_INT cTOTAL_SHORTLIST_ENTRIES 10

bool pedJustShortlisted

//settings menu

CONST_INT SEM_TIME          0 
CONST_INT SEM_WEATHER       1
CONST_INT SEM_WANTED        2
CONST_INT SEM_PEDDENSITY    3
CONST_INT SEM_VEHDENSITY    4
CONST_INT SEM_RESTRICTED    5
CONST_INT SEM_INVINCIBLE    6
//CONST_INT SEM_TOTAL 7
CONST_INT SEM_FIREBULLET    7
CONST_INT SEM_XPLODEBULLET  8
CONST_INT SEM_XPLODEPUNCH   9
CONST_INT SEM_SUPERJUMP     10
CONST_INT SEM_SLIDEY        11
CONST_INT SEM_LOWGRAVITY    12

CONST_INT SEM_CLEARAREA     13
CONST_INT SEM_TOTAL         14
//CONST_INT SEM_SLOWMO      12
//CONST_INT SEM_SLOWAIM     13

CONST_INT SEM_BEHAVIOUR     8 //not used for now

INT settingsMenu[SEM_TOTAL], tempSettingsMenu[SEM_TOTAL], settingsChange[SEM_TOTAL]
BOOL bTempSettingsHaveChanged = FALSE
BOOL bHideSettingsChangedDesc = FALSE
BOOL bTimeSettingFineTuned = FALSE
BOOL bTimeFrozen = FALSE
BOOL bApplyDMSettingsNow = FALSE
BOOL bWaitForVehiclePopulationToLoad = FALSE
BOOL bSpinnerSetOn = FALSE

#IF FEATURE_SP_DLC_DM_PROP_EDITOR
INT propSettingsMenu[SEM_TOTAL]
BOOL bInPropEdit = FALSE
INT iEditPropIndex
#ENDIF

bool bCoverIsBlocked,bCoverHelpGiven

ped_index swapToPed

CAMERA_INDEX dir_Camera,dir_zoomCamera
int iPanTime

bool bShowMenu
bool bWasMenuDrawn
bool PImenuON = FALSE
bool bMenuAssetsLoaded = FALSE
bool bBlockCinCam
bool bVoiceHashSetFromShortlist = FALSE
int aLeadoutTimer

//GTA:Online character loading.
bool bPlayerCardRequested = FALSE
bool bPlayerCardLoaded = FALSE

Vector vTrailerStartPos = <<-19.8146, -13.3768, 499.6126>>
float fTrailerStartHeading = 22.0
BOOL bTrailerLoadSceneStarted = FALSE

Vector vTrailerEndPos = <<-20.0092, -10.8837, 499.6>>
float fTrailerEndHeading = 310.0

vector vCamSmallAnimalTrailerPos = <<-15.7,-11.9,500.5>>
vector vCamSmallAnimalTrailerRot = <<-1.2,0.0,-139.7>>

vector vCamAnimalTrailerPos = <<-16.911, -9.5000, 500.3>>
vector vCamAnimalTrailerRot = <<-3.0, 0.0, -136.4>> 

vector vCamTrailerZoomPos = <<-17.5902, -7.8481, 500.7232>>
vector vCamTrailerZoomRot = <<5.7691, -0.0000, 145.0362>>

vector vCamTrailerPanEndPos = <<-14.6119, -3.3614, 499.8450>>
vector vCamTrailerPanEndRot = <<4.0059, 0.0000, 151.7677>>

float trailerCamZoom


bool zoomButtonJustReleased
bool zoomPCToggle
vector vZoomCamCoordStart,vZoomCamCoordEnd
VECTOR vZoomCamRotStart,vZoomCamRotEnd
FLOAT fZoomCamFOVStart,fZoomCamFOVEnd
FLOAT fStartPanFOV,fStartPanDOF

// menu controls

CONTROL_ACTION caMenuLeft = INPUT_FRONTEND_LEFT
CONTROL_ACTION caMenuRight = INPUT_FRONTEND_RIGHT
CONTROL_ACTION caMenuUp = INPUT_FRONTEND_UP
CONTROL_ACTION caMenuDown = INPUT_FRONTEND_DOWN

CONTROL_ACTION caMenuAccept = INPUT_FRONTEND_ACCEPT
CONTROL_ACTION caMenuCancel = INPUT_FRONTEND_CANCEL

//PI menu switchable controls
CONTROL_ACTION  caPiMenuWaypoint    = INPUT_FRONTEND_X
#IF FEATURE_SP_DLC_DM_PROP_EDITOR CONTROL_ACTION    caPiMenuProp        = INPUT_FRONTEND_X #ENDIF
CONTROL_ACTION caPiMenuMouseCameraOverride = INPUT_DUCK

/*
CONTROL_ACTION caMenuAccept = INPUT_FRONTEND_ACCEPT
CONTROL_ACTION caMenuCancel = INPUT_FRONTEND_CANCEL
CONTROL_ACTION caMenuScrollUp = INPUT_CURSOR_SCROLL_UP
CONTROL_ACTION caMenuScrollDown = INPUT_CURSOR_SCROLL_DOWN
*/

enumMenuCategory zoomCamAnimalChoice
/*
Vector vTrailerStartPos = <<-19.8146, -13.3768, 499.6126>>
float fTrailerStartHeading = 22.0

Vector vTrailerEndPos = <<-20.0092, -10.8837, 499.6126>>
float fTrailerEndHeading = 310.0
*/
//VECTOR vTrailerPos = <<-15.5,-7.9,500>> //<<-1049.4080, -520.5741, 35.5868>>


vector vLaunchCoords, vLaunchToeCoords
float fLaunchHeading
INTERIOR_INSTANCE_INDEX intLaunchInterior = NULL

//initialise / cleanup bools
BOOL bForceCleanupSetup = FALSE
BOOL bDirectorInitialised = FALSE
BOOL bBypassTrailerQuery = FALSE
BOOL bProcessingRespawn = FALSE

//pi menus

int selectedPI[MAX_MENUS]
int piMenuDelayTimer
bool forceWaitForPIreset
bool bEditorActivated
//bool bChangeTimeOfDay, bChangeWeather

//menu data
bool bMenu_sex_male = TRUE
bool menuDisplaySex
bool bForceRebuildPIMenu

//blocking gestures
bool bgesturesDisabled = FALSE

//Player data
vector vLastPlayerCoord
vector vRespawnCoords
float fPlayerHeading
int iPlayerHealthOnStart

//time and weather
int iStoredWeather[2],iClockHours,iClockMinutes
float fWeatherTransition
float fRainLevel
//int iWeather[9]

//shortlist ints
int lastSelectedShortlistPed

//gestures
//TEXT_LABEL_63 tLoadedAnimDict
//TEXT_LABEL_63 tAnimDictToLoad
//TEXT_LABEL_63 tAnimToPlay
//int iLastGestureSelected = -1
BOOL bGesturePlaying

//animal restricted state
bool bAnimalRestrictedState
model_names animalRestrictedModel
INT timerSaveCoords = 0

//Wanted level settings
INT wantedTimer = -1
INT iWantedLvlMin = 0, iWantedLvlMax = 5

//menu setup
//Background

Rect Background
TEXT_PLACEMENT TitlePlace
TEXT_STYLE TitleStyle

//TEXT_LABEL_15 TitleText

CONST_FLOAT fAlignX 0.0
CONST_FLOAT fAlignY -0.0755 

//heist related
CrewMember selectedHeistMember,lastSelectedHeistMember

//player model swap
bool bBlockOriginalPedDelete
bool firstTimeShowSaveVariation = true
ped_index originalPed

//Online character loading.
BOOL bOnlineCharactersAvailable = FALSE

//weapons
//bool bBlockGestureWeapons
//WEAPON_TYPE wTempRemovedWeapon

//gesture/speech button timer
int buttonFrameCounter = 0

bool bRequireNewActorModel,bLoadingNewModel
model_names requestedModel,loadedModel, lastModelGivenWeapons

//stored speech data

TEXT_LABEL_31 speechContextData[18]
int PISpeechDataForPed[18]
int iMaxSpeechEnries

//scaleform
SCALEFORM_INDEX sfMenu

bool bPassShapetest
bool bRespawnPass

//user locations
struct sUserLocation
    vector position
    float heading
    float camRelativePitch
    float camRelativeHeading
    vector bikePos
    float bikeRot
    model_names model
    BOOL interior = FALSE 
    BLIP_INDEX blipIndex
    TEXT_LABEL_15 tlLocName
ENDSTRUCT

CONST_INT MAX_ONLINE_CHARS 2

DIR_ONLINE_CHAR sOnlineChar[MAX_ONLINE_CHARS]
DIR_OUTFIT_DATA sOutfitsData

INT iWarpLocation
sUserLocation warpLocations[MAX_DTL]
INT iLocationSelected[MAX_DTL]
bool bQuickTravelErrorHelpShown

VEHICLE_INDEX vehSanchez, vehReturn = NULL, vehPlayer = NULL
VECTOR vReturnVehicleCoords, vPlayerVehicleCoords

INT crushDepthTimer = GET_GAME_TIMER()
BOOL diedInWater = FALSE
DOOR_STATE_ENUM doorArray[AUTODOOR_MAX]
BOOL bDoorDataMissing
        
INT iNavVertDirection,navVertTimer,iLastNavVertDirection,iNavAccelCounter
INT lastDirectionPress
BOOL navLeft,navRight,navAccept,navSwap,navBack,navRandom,leftStickReset,nav_PI,navY,navGesture,navSpeech,navTravel
BOOL navSaveLocation, navZoom, navFullBodyGesture
BOOL bCamIsMoving

#IF FEATURE_SP_DLC_DM_PROP_EDITOR
BOOL navPropEditorAccept, navPropEditorBack, bInPropEditorWarningScreen
#ENDIF

BOOL        navSetWaypoint, navDelWaypoint
CONST_FLOAT c_F_WAYPOINT_PLAYER_MIN_DIST    50.0        //  Set to 50m to comply with the pause menu distance.

#IF FEATURE_SP_DLC_DM_PROP_EDITOR  

    BOOL navDelProp, navWarpToProp//, navClearScene   
 
#ENDIF

int iWaypointLocIndex = -1
int checkDoubleTapFlag
int doubleTapTimer

BOOL waitingForPlayerToSettle = FALSE
INT waitTimer = 0
INT iPauseDisableTimer = 0
INT iLastCleanupCause
CAM_VIEW_MODE cvm_StartUp

BOOL bPiMouseCameraOverride, bLastFrameCameraOverride

SCENARIO_BLOCKING_INDEX blockingIndex = NULL
VECTOR mapMin = <<-10000.0, -10000.0, -1000.0>>
VECTOR mapMax = <<10000.0, 10000.0, 1000.0>>

//blocking cover usage
struct blockDataStruct
    bool componentBlockActive
    bool propBlockActive
    model_names blockOnModel
    PED_COMPONENT componentBlocked
    PED_PROP_POSITION propBlocked
    int unblockPropVariation
    int unblockDrawable
    int unblockTexture
endstruct

blockDataStruct blockData


//debug
#if IS_DEBUG_BUILD

    bool bDebugResetAfterWalk
    bool bDebugCoords
    bool bDebugForceIncrement

    bool bDebugHitLastVariation


    vector vDebugPlayerStartCoord
    //vector vDebugRespawnCoord
    vector vDebugShapetestFloorHit,vDebugShapetestChestHit,vDebugShapetestFloor,vDebugShapetestChest,vDebugFloorStart,vDebugFloorEnd


    INT debugWarpLocation
    FLOAT debugRelativeCamPitch
    float debugRelativeCamHeading
    BOOL debugApplyPitch


    WIDGET_GROUP_ID director_widget
    bool bUnlockAll,bResetAnimalPan
    BOOL bDebugUnlockLocations = FALSE
    
    bool bDisplayShortlist

    bool bDebugShowOutfit
    INT iOutfitRandInt
    int iOutfitID
    MODEL_NAMES mOutfitModel

    BOOL bDebugMousePIMenu
    BOOL bDebugMouseMainMenu
    
    INT mavMouseSelectDirection
    INT navMouseSelectItem = MENU_CURSOR_NO_ITEM
#ENDIF

//debug

#if IS_DEBUG_BUILD

struct structDebugBox
    vector pos
    vector size
    vector vRotation
    float fRadius
endstruct

structDebugBox debugBox[3]
structDebugBox debugSphere[2]

PROC setDebugBox(int iIndex, vector vPos, vector vZsize, vector vRotation)
    debugBox[iIndex].pos = vPos
    debugBox[iIndex].size = vZsize
    debugBox[iIndex].vRotation = vRotation
ENDPROC

PROC setDebugSphere(int iIndex, vector vpos, float fRadius)
    debugSphere[iIndex].pos = vpos
    debugSphere[iIndex].fRadius = fRadius
ENDPROC

PROC drawCollisionDebug()
    int i
    REPEAT COUNT_OF(debugBox) i
        DRAW_DEBUG_BOX(debugBox[i].pos - (debugBox[i].size / 2.0),debugBox[i].pos + (debugBox[i].size / 2.0),255,0,0,100)
    ENDREPEAT
    
    REPEAT COUNT_OF(debugSphere) i
        DRAW_DEBUG_SPHERE(debugSphere[i].pos,debugSphere[i].fRadius,0,255,0,100)
        //BOX(debugBox[i].pos - (debugBox[i].size / 2.0),debugBox[i].pos + (debugBox[i].size / 2.0))
    ENDREPEAT
    
ENDPROC


PROC CONTRL_DEBUG

    TEXT_LABEL_63 txtlbl
    
    IF NOT DOES_WIDGET_GROUP_EXIST(director_widget)
        director_widget = START_WIDGET_GROUP("Director Mode")
        
            ADD_WIDGET_BOOL("unlock all peds and categories",bUnlockAll)
            ADD_WIDGET_BOOL( "Unlock all locations", bDebugUnlockLocations )
            ADD_WIDGET_BOOL("Modify cam pan",bResetAnimalPan)
            ADD_WIDGET_BOOL("Test Model Variations",bDebugForceIncrement)
            ADD_WIDGET_BOOL("Show relevant coords",bDebugCoords)
            ADD_WIDGET_BOOL("Reset walk out",bDebugResetAfterWalk)
        
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        INIT_PROP_DATA_DEBUG_WIDGET()
        INIT_PROP_SCENE_INFO_HELP_DEBUG_WIDGET()
        #ENDIF
        
        START_WIDGET_GROUP("Menu dimensions")
            ADD_WIDGET_FLOAT_SLIDER("bg_y", bg_y, 0.0, 2.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("wl_w", wl_w, 0.0, 1.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("wl_h", wl_h, 0.0, 1.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("PosX", wl_x, -1.0, 1.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("PosY", wl_y, -1.0, 1.5, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("Help Box Width",wl_w, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("Height",wl_h, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("message PosX", messageBox_x, -1.0, 1.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("message PosY", messageBox_y, -1.0, 2.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("message Width",messageBox_w, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("message Height",messageBox_h, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("line PosX", line_x, -1.0, 1.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("line PosY", line_y, -1.0, 2.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("line Width",line_w, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("line Height",line_h, -10.0, 10.0, 0.0002)
            ADD_WIDGET_INT_SLIDER("WLHAlpha",w1_a, 0, 255, 1)
            ADD_WIDGET_FLOAT_SLIDER("menu scaleform width",sf_w, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("menu scaleform pos sfx",sf_x, -10.0, 10.0, 0.0002)
            ADD_WIDGET_FLOAT_SLIDER("menu scaleform pos sfy",sf_y, -10.0, 10.0, 0.0002)
            ADD_WIDGET_BOOL("Output dimensions", outputMenudimensions)
            
        
        STOP_WIDGET_GROUP() 
        
        
        START_WIDGET_GROUP("QA Debug")
            ADD_WIDGET_BOOL("Display clothing data",bDebugShowOutfit)
        STOP_WIDGET_GROUP()
        
        START_WIDGET_GROUP("Camera")    
            ADD_WIDGET_BOOL("Nav Zoom", navZoom)
            ADD_WIDGET_BOOL("Zoom Btn Released", zoomButtonJustReleased)
            ADD_WIDGET_BOOL("Zoom PC Toggle", zoomPCToggle)
            ADD_WIDGET_FLOAT_SLIDER("Trailer Cam Zoom", trailerCamZoom, 0, 1, 0.001)    
        //  ADD_WIDGET_FLOAT_SLIDER("DOF",fNewDebugDOF,-10.0,10.0,0.1)
        STOP_WIDGET_GROUP() 
        
        START_WIDGET_GROUP("Warp Locations")
            ADD_BIT_FIELD_WIDGET("Unlocked locations bitfield", g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed)
            ADD_WIDGET_INT_SLIDER("Warp Location",debugWarpLocation, 0, ENUM_TO_INT(MAX_DTL)-1, 1)
            ADD_WIDGET_FLOAT_SLIDER("Relative pitch",debugRelativeCamPitch, -50.0, 50.0, 0.1)
            ADD_WIDGET_FLOAT_SLIDER("Relative heading",debugRelativeCamHeading, -50.0, 50.0, 0.1)           
            ADD_WIDGET_BOOL("Apply relative pitch",debugApplyPitch)
                    
        STOP_WIDGET_GROUP()

        START_WIDGET_GROUP("Nav Check")
            ADD_WIDGET_BOOL("Nav Accept", navAccept)
            ADD_WIDGET_BOOL("Nav Left", navLeft)
            ADD_WIDGET_BOOL("Nav Right", navRight)
            ADD_WIDGET_BOOL("Nav Back", navBack)
            ADD_WIDGET_BOOL("Nav Swap", navSwap)
            ADD_WIDGET_BOOL("Nav Zoom", navZoom)
            ADD_WIDGET_BOOL("nav_PI",nav_PI)
            
        STOP_WIDGET_GROUP()
        
        START_WIDGET_GROUP("Shortlists")
            ADD_WIDGET_BOOL("Display shortlist", bDisplayShortlist)
        STOP_WIDGET_GROUP()
    
        START_WIDGET_GROUP("KB and Mouse")
            ADD_WIDGET_BOOL("Show Main Menu Debug", bDebugMouseMainMenu)
            ADD_WIDGET_BOOL("Show PI Debug", bDebugMousePIMenu)
            ADD_WIDGET_INT_READ_ONLY("navMouseSelectItem", navMouseSelectItem)
            ADD_WIDGET_INT_READ_ONLY("curMenuItem", curMenuItem)
            ADD_WIDGET_INT_READ_ONLY("sPI_MenuData.iCurrentSelection", sPI_MenuData.iCurrentSelection)
            ADD_WIDGET_INT_READ_ONLY("g_iMenuCursorItem", g_iMenuCursorItem)
            ADD_WIDGET_INT_READ_ONLY("mavMouseSelectDirection", mavMouseSelectDirection)
        STOP_WIDGET_GROUP() 
        
            //ADD_WIDGET_FLOAT_SLIDER("sf_x",sf_x,
        STOP_WIDGET_GROUP()
        

        
    ENDIF
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    UPDATE_PROP_DATA_DEBUG_WIDGET()
    #ENDIF
    
    IF (bDebugMousePIMenu) AND (selectedMenuType = MENUTYPE_PI)
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING", "DIRCURMENU")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.1, "NUMBER", ENUM_TO_INT(dirCurrentMenu))
        
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.2, "STRING", "PI CURITEM")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.2, "NUMBER", sPI_MenuData.iCurrentSelection)
        
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.3, "STRING", "PI DIFF")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.3, "NUMBER", g_iMenuCursorItem - sPI_MenuData.iCurrentSelection)
        
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.4, "STRING", "SUBMENU")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.4, "NUMBER", iSubMenu)
    ENDIF
    
    IF (bDebugMouseMainMenu) AND (selectedMenuType = MENUTYPE_MAINMENU)
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING", "DIRCURMENU")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.1, "NUMBER", ENUM_TO_INT(dirCurrentMenu))
    
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.2, "STRING", "CURITEM")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.2, "NUMBER", curMenuItem)
        
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.3, "STRING", "DIFF")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.3, "NUMBER", g_iMenuCursorItem - curMenuItem)
        
        SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.4, "STRING", "SUBMENU")
        SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.4, "NUMBER", iSubMenu)     
        
        IF (curMenuItem >= 0) AND (curMenuItem < COUNT_OF(displayedMenuData))
            SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.5, "STRING", "CURCAT")
            SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.5, "NUMBER", ENUM_TO_INT(displayedMenuData[curMenuItem].category))
        ENDIF
        
        IF (g_iMenuCursorItem >= 0) AND (g_iMenuCursorItem < COUNT_OF(displayedMenuData))
            SET_TEXT_SCALE(0.5, 0.25) DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.6, "STRING", "HOVCAT")
            SET_TEXT_SCALE(0.5, 0.5) DISPLAY_TEXT_WITH_NUMBER(0.6, 0.6, "NUMBER", ENUM_TO_INT(displayedMenuData[g_iMenuCursorItem].category))
        ENDIF
    ENDIF
    
    IF bDebugShowOutfit
        IF IS_MODEL_VALID(mOutfitModel)
            
            txtlbl = "Rnd i:"
            txtlbl +=iOutfitRandInt
            txtlbl+=" outfit:"
            txtlbl+=iOutfitID
            txtlbl+=" model:"
            txtlbl+= GET_MODEL_NAME_FOR_DEBUG(mOutfitModel)
            SET_TEXT_SCALE(0.4, 0.4)
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.6,0.1,"STRING",txtlbl)
        ENDIF
    ENDIF   
    
    IF outputMenudimensions
        outputMenudimensions = FALSE
        cprintln(debug_director,"wl_x = ",wl_x," wl_y = ",wl_y," wl_w = ",wl_w," wl_h = ",wl_h)
        cprintln(debug_director,"sf_x = ",sf_x," sf_y = ",sf_y," sf_w = ",sf_w," sf_h = ",sf_h)
        cprintln(debug_director,"messageBox_x = ",messageBox_x," messageBox_y =",messageBox_y," messageBox_w = ",messageBox_w," messageBox_h = ",messageBox_h)
        cprintln(debug_director,"line_x = ",line_x," line_y = ",line_y," line_w = ",line_w," line_h = ",line_h)
    ENDIF
    
    int i
    IF bDisplayShortlist
        repeat count_of(shortlistData) i
            txtlbl = i
            txtlbl += " "
            IF IS_MODEL_VALID(shortlistData[i].model)
                txtlbl += GET_MODEL_NAME_FOR_DEBUG(shortlistData[i].model)
            ELSE
                txtlbl += "NULL"
            ENDIF
            SET_TEXT_SCALE(0.3, 0.3)
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(i*0.02),"STRING",txtlbl)
        endrepeat
    ENDIF

    
    IF bResetAnimalPan
        bResetAnimalPan = FALSE
        
        vCamAnimalTrailerPos = GET_CAM_COORD(GET_DEBUG_CAM())
        vCamAnimalTrailerRot = GET_CAM_ROT(GET_DEBUG_CAM())
    ENDIF
    
    //IF DOES_CAM_EXIST(dir_Camera)
    //  SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,fNewDebugDOF)
    //ENDIF
    
    IF bDebugForceIncrement
        IF IS_MODEL_VALID(GET_ENTITY_MODEL(player_ped_id()))
            txtlbl = "Model: "
            txtlbl+= GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(player_ped_id()))
            SET_TEXT_SCALE(0.4, 0.4)
            
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.6,0.2,"STRING",txtlbl)
            
            txtlbl = "is Male?"
            SET_TEXT_SCALE(0.4, 0.4)
            
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.6,0.23,"STRING",txtlbl)
            
            IF IS_PLAYER_FEMALE()
                txtlbl = "FALSE"
            ELSE
                txtlbl = "TRUE"
            ENDIF

            SET_TEXT_SCALE(0.4, 0.4)
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.65,0.23,"STRING",txtlbl)
            
            DISPLAY_TEXT_WITH_LITERAL_STRING(0.65,0.23,"STRING",txtlbl)
        ENDIF
    ENDIF
    
    IF bDebugHitLastVariation
        SET_TEXT_SCALE(0.7, 0.7)
        DISPLAY_TEXT_WITH_LITERAL_STRING(0.6,0.25,"STRING","SELECT NEW GENDER / MODEL")
    ENDIF
    
    IF bDebugCoords
        SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
        IF NOT IS_VECTOR_ZERO(vDebugPlayerStartCoord)
            DRAW_DEBUG_SPHERE(vDebugPlayerStartCoord,0.1,255,0,0)
        ENDIF
        IF NOT IS_VECTOR_ZERO(vDebugShapetestFloor)
            DRAW_DEBUG_SPHERE(vDebugShapetestFloor,0.1,0,255,0)
        ENDIF
        IF NOT IS_VECTOR_ZERO(vDebugShapetestChest)
            DRAW_DEBUG_SPHERE(vDebugShapetestChest,0.1,0,0,255)
        ENDIF
        IF NOT IS_VECTOR_ZERO(vDebugShapetestFloorHit)
            DRAW_DEBUG_SPHERE(vDebugShapetestFloorHit,0.1,255,0,255)
        ENDIF
        IF NOT IS_VECTOR_ZERO(vDebugShapetestChestHit)
            DRAW_DEBUG_SPHERE(vDebugShapetestChestHit,0.1,255,255,0)
        ENDIF
        
        IF NOT IS_VECTOR_ZERO(vDebugFloorStart)
            DRAW_DEBUG_LINE(vDebugFloorStart,vDebugFloorEnd)
        ENDIF
        
        drawCollisionDebug()
        
    ENDIF
    
    IF debugApplyPitch
        debugApplyPitch = FALSE
        warpLocations[debugWarpLocation].camRelativePitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
        warpLocations[debugWarpLocation].camRelativeHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
    ENDIF

ENDPROC

#endif

//------------END OF DEFINITIONS------------------

SHAPETEST_INDEX shpTestFloor,shpTestFloorSphere,shpTestChestHeight, shapeTestRespawn


FUNC BOOL IS_SHAPETEST_RESULT_BAD(int iResult, entity_INDEX eHitEntity, vector vNormal)
    IF iResult = 0
        cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Hit nothing - GOOD")
        RELEASE_SCRIPT_GUID_FROM_ENTITY(eHitEntity)
        RETURN FALSE                
    ENDIF       
    
    //check hit entity is a wall
    IF does_entity_exist(eHitEntity)
        IF IS_ENTITY_AN_OBJECT(eHitEntity)
        OR IS_ENTITY_A_VEHICLE(eHitEntity)
            vector vMin,vMax
            GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(eHitEntity),vMin,vMax)
            cprintln(DEBUG_DIRECTOR,"vMin = ",vMin," vMax = ",vMax)
            
            IF ABSF(vMin.z) + ABSF(vMax.z) < 0.2
                RELEASE_SCRIPT_GUID_FROM_ENTITY(eHitEntity)
                cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Hit something small - GOOD")
                RETURN FALSE
            ELSE
                RELEASE_SCRIPT_GUID_FROM_ENTITY(eHitEntity)
                cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Hit a tall object/wall - BAD")
                RETURN TRUE
            ENDIF
        ELSE
            RELEASE_SCRIPT_GUID_FROM_ENTITY(eHitEntity)
            
            //This was an issue with spawning animals on a slope (rotating after load)
            IF vNormal.z < 0.2
            AND vNormal.z > -0.2
                cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Low hit normal, likely a slope, ignoring for now")
                //RETURN TRUE 
            ENDIF
            cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Hit entity  not an object or vehicle - GOOD")
            RETURN FALSE
        ENDIF
    ELSE
        RELEASE_SCRIPT_GUID_FROM_ENTITY(eHitEntity)
        cprintln(DEBUG_DIRECTOR,"IS_SHAPETEST_RESULT_BAD() Hit entity doesn't exist - GOOD")
        RETURN FALSE
    ENDIF
ENDFUNC

FUNC VECTOR GET_CLOSEST_SAFE_DIRECTOR_MODE_SPAWN_POINT()
    
    vector vPlayer = GET_ENTITY_COORDS(player_ped_id())
    
    vector vSpawnPoints[6]
    vSpawnPoints[0] = <<249.0731, -1290.2083, 28.1948>>
    vSpawnPoints[1] = <<-157.7122, -878.0799, 28.2652>>
    vSpawnPoints[2] = <<143.3925, 1665.6155, 227.9352>>
    vSpawnPoints[3] = <<916.2925, 3546.8999, 32.8042>>
    vSpawnPoints[4] = <<1698.7272, 4748.2246, 40.9841>>
    vSpawnPoints[5] = <<430.5542, 6535.0825, 26.9338>>

    float fClosest = 9999.9
    float fThisDist
    int i,iResult
    
    REPEAT COUNT_OF(vSpawnPoints) i
        fThisDist = GET_DISTANCE_BETWEEN_COORDS(vPlayer,vSpawnPoints[i])
        IF fThisDist < fClosest
            fClosest = fThisDist
            iResult = i
        ENDIF
    ENDREPEAT
    
    RETURN vSpawnPoints[iResult]
ENDFUNC

FUNC BOOL GET_SAFE_COORD_FOR_DIRECTOR_MODE(VECTOR vStartCoord, VECTOR &vReturned, FLOAT heading, MODEL_NAMES model)
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        IF NOT START_UP_SHAPETEST(vStartCoord,heading,model)
        OR model = A_C_WESTY    // Brute force fix for 2249455. Westy pysics capsule is bigger than its model dimensions and
                                // can cause weird behaviour if we position them near static geometry. Force Westy's to respawn
                                // on road nodes.
            CPRINTLN(DEBUG_DIRECTOR,"<> SHAPE_TEST: FAIL")
            
            IF START_UP_ROAD_NODE_TEST(vStartCoord,heading,model, DEFAULT, DEFAULT, 2.5)           
                CPRINTLN(DEBUG_DIRECTOR,"<> ROAD_NODE_TEST: PASS")
                vReturned = vStartCoord
                RETURN TRUE
            ELSE
                CPRINTLN(DEBUG_DIRECTOR,"<> ROAD_NODE_TEST: FAIL")
            ENDIF
        ELSE                
            CPRINTLN(DEBUG_DIRECTOR,"<> SHAPE_TEST: PASS")
            vReturned = vStartCoord
            RETURN TRUE
        ENDIF   
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL GET_SAFE_COORD_FOR_VEHICLE(VECTOR vStartCoord, VECTOR &vReturned)
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
    AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        VEHICLE_INDEX playerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        FLOAT heading = GET_ENTITY_HEADING(playerVehicle)
        MODEL_NAMES model = GET_ENTITY_MODEL(playerVehicle)
        IF GET_SAFE_COORD_FOR_DIRECTOR_MODE(vStartCoord, vReturned, heading, model) 
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL GET_SAFE_COORD_FOR_PED_ON_FOOT(VECTOR vStartCoord, VECTOR &vReturned, BOOL doPavementCheck, MODEL_NAMES modelOverride = DUMMY_MODEL_FOR_SCRIPT)
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        FLOAT heading = GET_ENTITY_HEADING(PLAYER_PED_ID())
        
        MODEL_NAMES model
        IF modelOverride != DUMMY_MODEL_FOR_SCRIPT
            model = modelOverride
        ELSE
            model = GET_ENTITY_MODEL(PLAYER_PED_ID())
        ENDIF
        
        IF doPavementCheck AND GET_SAFE_COORD_FOR_PED(vStartCoord,false,vReturned,GSC_FLAG_NOT_WATER | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_ONLY_NETWORK_SPAWN)
            //vReturned -= <<0,0,0.9>>
            CPRINTLN(DEBUG_DIRECTOR,"<> GET_SAFE_COORD_FOR_PED")
            RETURN TRUE
        ELIF GET_SAFE_COORD_FOR_DIRECTOR_MODE(vStartCoord+<<0,0,0.1>>, vReturned, heading, model)   //A bit of extra height to lift the bounding box from the ground
            CPRINTLN(DEBUG_DIRECTOR,"<> GET_SAFE_COORD_FOR_DIRECTOR_MODE")
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC VECTOR GET_CLOSEST_SAFE_POINT_FOR_PLAYER(vector vStartCoord, bool doPavementCheck = TRUE, bool doAnywhereCheck = TRUE, bool inVehicle = FALSE, MODEL_NAMES modelOverride = DUMMY_MODEL_FOR_SCRIPT)
    vector vReturned
    IF doPavementCheck
    AND GET_SAFE_COORD_FOR_PED(vStartCoord,false,vReturned,GSC_FLAG_ONLY_PAVEMENT | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_ONLY_NETWORK_SPAWN) 
        cprintln(DEBUG_DIRECTOR,"respawn on pavement")
        //vReturned -= <<0,0,0.9>>
    ELSE
        cprintln(DEBUG_DIRECTOR,"pavement safe test failed or was skipped. Check any safe coord")
        //no pavement found. Probably in the countryside then?
        //let's try finding next best safe coord.
        IF doAnywhereCheck          
        AND NOT inVehicle
        AND GET_SAFE_COORD_FOR_PED_ON_FOOT(vStartCoord,vReturned,doPavementCheck, modelOverride)
            cprintln(DEBUG_DIRECTOR,"respawn at safe point: ",vReturned)
        ELIF inVehicle
        AND GET_SAFE_COORD_FOR_VEHICLE(vStartCoord,vReturned)
            cprintln(DEBUG_DIRECTOR,"respawn at vehicle safe point: ",vReturned)
        ELSE
            //otherwise spawn at fixed location
            vReturned = GET_CLOSEST_SAFE_DIRECTOR_MODE_SPAWN_POINT()            
            cprintln(DEBUG_DIRECTOR,"No safe coord found. GET_CLOSEST_SAFE_DIRECTOR_MODE_SPAWN_POINT() : ",vReturned)
        ENDIF
    ENDIF
    
    RETURN vReturned
ENDFUNC

FUNC BOOL WAIT_FOR_PLAYER_TO_SETTLE()
    cprintln(DEBUG_DIRECTOR,"waitingForPlayerToSettle, in air:",IS_ENTITY_IN_AIR(PLAYER_PED_ID()),", falling:",IS_PED_FALLING(PLAYER_PED_ID()))
    //Don't wait to settle if the player is in water
    IF (NOT (IS_ENTITY_IN_AIR(PLAYER_PED_ID()) OR IS_PED_FALLING(PLAYER_PED_ID())) OR (GET_GAME_TIMER()-waitTimer>10000) OR (IS_ENTITY_IN_WATER(PLAYER_PED_ID()))) 
    AND (GET_GAME_TIMER()-waitTimer>500) //min/max wait
        cprintln(DEBUG_DIRECTOR,"player on ground")
        IF (GET_GAME_TIMER()-waitTimer)>10000
            cprintln(DEBUG_DIRECTOR,"player on ground-timer expired")
        ENDIF
        waitingForPlayerToSettle = FALSE
        waitTimer = 0
        RETURN TRUE
    ENDIF
    
    SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
    RETURN FALSE
ENDFUNC

FUNC BOOL GET_VEHICLE_NODE(MODEL_NAMES mTestModel = DUMMY_MODEL_FOR_SCRIPT)
    CPRINTLN(DEBUG_DIRECTOR, "GET_VEHICLE_NODE")
    IF START_UP_ROAD_NODE_TEST(vRespawnCoords, fPlayerHeading, mTestModel,TRUE, DEFAULT, 2.5)
        CPRINTLN(DEBUG_DIRECTOR, "Repositioned to ",vRespawnCoords)
        RETURN TRUE
    ELSE
        CPRINTLN(DEBUG_DIRECTOR, "Couldn't find valid road node position around ", vRespawnCoords)
        RETURN FALSE
    ENDIF
ENDFUNC

FUNC BOOL PERFORM_SHAPETEST_REPOSITION(VECTOR vPos, MODEL_NAMES switchToModel, vector vRespawnPoint, float fRespawnHeading, BOOL &bPass, BOOL &bPassRespawn, 
                                        BOOL doMapLoadWithReposition = FALSE, BOOL useRespawnCoordForGround = FALSE, BOOL bForceRoadNode = FALSE) //useRespawnCoordForGround hack used when adding warp menu option
    
	IF waitingForPlayerToSettle
    //allow player to settle
        RETURN WAIT_FOR_PLAYER_TO_SETTLE()
    ENDIF
    
    cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST()")
    
    IF IS_PLAYER_TELEPORT_ACTIVE()
        STOP_PLAYER_TELEPORT()
        CDEBUG1LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() IS_PLAYER_TELEPORT_ACTIVE = TRUE, RETURN FALSE (wait for teleport to finish)")
        RETURN FALSE
    ENDIF
    
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        switchToModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))        
        CDEBUG1LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Use vehicle model")
    ENDIF   
    
    IF bForceRoadNode
    //skip shapetests
        shpTestChestHeight  = NULL
        shpTestFloor        = NULL  
        shapeTestRespawn    = NULL  
    ELSE
    //run shapetests
        vector vMin,vMax
        bPass = FALSE
        
        GET_MODEL_DIMENSIONS(switchToModel,vMin,vMax)
        
        CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Shapetest at: ",vPos," respawn point: ",vRespawnPoint)
        CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() shpTestFloor = ",native_to_int(shpTestFloor)," shpTestChestHeight = ",native_to_int(shpTestChestHeight)," shapeTestRespawn = ",native_to_int(shapeTestRespawn))
        
        IF native_to_int(shpTestFloor)  = 0
        AND native_to_int(shpTestChestHeight) = 0
        AND native_to_int(shapeTestRespawn) = 0
            CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() START SHAPTESTS")      
            CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() safe point: ",vRespawnPoint," safe point dimensions : ",vMax.x-vMin.x,",",vMax.y-vMin.y,",",vMax.z-vMin.z)
            
            shapeTestRespawn = START_SHAPE_TEST_BOX(vRespawnPoint+<<0,0,vMax.z>>, <<vMax.x-vMin.x,vMax.y-vMin.y,vMax.z*0.5>>, <<0,0,fRespawnHeading>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS,PLAYER_PED_ID())
            
            #IF IS_DEBUG_BUILD
            setDebugBox(0,vRespawnPoint+<<0,0,vMax.z>>, <<vMax.x-vMin.x,vMax.y-vMin.y,vMax.z*0.5>>, <<0,fRespawnHeading,0>>)
            #ENDIF
        ENDIF
        
        int iResult
        vector vTemp1, vTemp2
        entity_index eHitEntity
        
        int iResultFloor
        vector vTemp1f, vTemp2f
        entity_index eHitEntityf
        
        IF shapeTestRespawn != NULL             
            IF GET_SHAPE_TEST_RESULT(shapeTestRespawn, iResult, vTemp1, vTemp2, eHitEntity) = SHAPETEST_STATUS_RESULTS_READY        

                cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() hit point = ",vTemp1," respawn point normal = ",vTemp2," hit something = ",iResult)
                
                IF IS_SHAPETEST_RESULT_BAD(iResult, eHitEntity, vTemp2)         
                    CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() hit something bad")
                    bPassRespawn = FALSE
                ELSE
                    CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() no bad hit detected")
                    bPassRespawn = TRUE
                ENDIF
                
                shapeTestRespawn  = NULL

                vector vGround
                IF !useRespawnCoordForGround
                    vGround = (GET_PED_BONE_COORDS(player_ped_id(),BONETAG_L_TOE,<<0,0,0>>) + GET_PED_BONE_COORDS(player_ped_id(),BONETAG_R_TOE,<<0,0,0>>)) / 2.0
                ELSE
                    vGround = vPos
                ENDIF
                
                CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Animal max height = ",vMax.z," ground pos = ",vGround)     
                
                shpTestChestHeight = START_SHAPE_TEST_BOX(vGround+<<0,0,vMax.z>>, <<(vMax.x-vMin.x)*1.2,(vMax.y-vMin.y)*1.2,vMax.z*0.5>>, <<0,0,fRespawnHeading>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS,PLAYER_PED_ID())
                shpTestFloorSphere = START_SHAPE_TEST_SWEPT_SPHERE(vGround+<<0,0,vMax.z>>,vGround - <<0,0,0.5>>,0.3, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER,PLAYER_PED_ID())   //START_SHAPE_TEST_BOX(vGround, <<vMax.x-vMin.x,vMax.y-vMin.y,vMax.z*0.2>>, <<0,fRespawnHeading,0>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS,PLAYER_PED_ID())  
                shpTestFloor = START_SHAPE_TEST_BOX(vGround, <<(vMax.x-vMin.x)*1.2,(vMax.y-vMin.y)*1.2,vMax.z*0.7>>, <<0,0,fRespawnHeading>>, EULER_YXZ, SCRIPT_INCLUDE_ALL,PLAYER_PED_ID())           //START_SHAPE_TEST_BOX(vGround, <<vMax.x-vMin.x,vMax.y-vMin.y,vMax.z*0.2>>, <<0,fRespawnHeading,0>>, EULER_YXZ, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS,PLAYER_PED_ID())  
                
                #IF IS_DEBUG_BUILD
                    setDebugBox(1,vGround+<<0,0,vMax.z>>, <<(vMax.x-vMin.x)*1.2,(vMax.y-vMin.y)*1.2,vMax.z*0.5>>, <<0,fRespawnHeading,0>>)
                    setDebugBox(2,vGround, <<(vMax.x-vMin.x)*1.2,(vMax.y-vMin.y)*1.2,vMax.z*0.7>>, <<0,fRespawnHeading,0>>)
                    setDebugSphere(0,vGround+<<0,0,vMax.z>>,0.3)
                    setDebugSphere(1,vGround - <<0,0,0.5>>,0.3)         
                
                    vDebugFloorStart = vGround+<<0,0,vMax.z>>
                    vDebugFloorEnd = vGround - <<0,0,0.5>>
                    vDebugShapetestFloor = vGround
                    vDebugShapetestChest = vGround+<<0,0,vMax.z>>
                #ENDIF          
            
            ENDIF
        ELSE
        
            IF shpTestFloor != NULL             
            AND shpTestFloorSphere != NULL
                IF GET_SHAPE_TEST_RESULT(shpTestFloorSphere, iResultFloor, vTemp1f, vTemp2f, eHitEntityf) = SHAPETEST_STATUS_RESULTS_READY
                AND GET_SHAPE_TEST_RESULT(shpTestFloor, iResult, vTemp1, vTemp2, eHitEntity) = SHAPETEST_STATUS_RESULTS_READY
                    CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() shapetest floor sphere Result = ",iResult," vTemp1 = ",vTemp1," vTemp2 = ",vTemp2," eHitEntity = ",native_to_int(eHitEntity))         
                    CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() shapetest floor Result = ",iResultFloor," vTemp1 = ",vTemp1f," vTemp2 = ",vTemp2f," eHitEntity = ",native_to_int(eHitEntityf))
                    
                    #IF IS_DEBUG_BUILD
                        vDebugShapetestFloorHit = vTemp1
                    #ENDIF                              
                    
                    IF IS_SHAPETEST_RESULT_BAD(iResult, eHitEntity, vTemp2)
                    OR IS_SHAPETEST_RESULT_BAD(iResultFloor, eHitEntityf, vTemp2f)
                        bPass = FALSE
                        shpTestFloor  = NULL
                        shpTestChestHeight = NULL
                        //RETURN TRUE
                    ELSE
                        bPass = TRUE
                    ENDIF
                    shpTestFloor  = NULL
                ENDIF
            ENDIF       
            
            IF shpTestChestHeight != NULL   
                IF GET_SHAPE_TEST_RESULT(shpTestChestHeight, iResult, vTemp1, vTemp2, eHitEntity) = SHAPETEST_STATUS_RESULTS_READY
                    CDEBUG3LN(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() shapetest chest Result = ",iResult," vTemp1 = ",vTemp1," vTemp2 = ",vTemp2," eHitEntity = ",native_to_int(eHitEntity))         
                    
                    #IF IS_DEBUG_BUILD
                        vDebugShapetestChestHit = vTemp1
                    #ENDIF
                    
                    IF iResult = 1
                        bPass = FALSE
                        shpTestFloor  = NULL
                        shpTestChestHeight = NULL
                        //RETURN TRUE
                    ELSE
                        bpass = TRUE
                    ENDIF
                    shpTestChestHeight  = NULL
                ENDIF
            ENDIF       
        ENDIF
    ENDIF

    //finished shapetests
    IF shpTestChestHeight = NULL
    AND shpTestFloor = NULL 
    AND shapeTestRespawn = NULL 
    
        IF !bPass OR bForceRoadNode
        //player failed the collision check. Need to reposition ped somewhere safe.
            IF bPassRespawn AND NOT bForceRoadNode                                          
                cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Shapetest fail. Respawn at closest safe point: ",vRespawnCoords)
                //DO_PLAYER_MAP_WARP_WITH_LOAD(vRespawnCoords, fPlayerHeading, FALSE, 12000, 1000)
            ELSE
                IF GET_VEHICLE_NODE(switchToModel)
                    //DO_PLAYER_MAP_WARP_WITH_LOAD(vRespawnCoords, fPlayerHeading, FALSE, 12000, 1000)
                    fRespawnHeading = fPlayerHeading
                    cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Shapetest fail and safe point fail. Respawn at closest road node: ",vRespawnCoords)
                ELSE                    
                    vRespawnCoords = GET_CLOSEST_SAFE_DIRECTOR_MODE_SPAWN_POINT()
                    cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Respawn at safe coord")

                    //Added by Steve T. Makes getting to the top of Mount Gordo in a vehicle much easier. Nothing was broken, just a bit more helpful. Bug 2557239 
                    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                        VEHICLE_INDEX tempPlayerVehIndex 
                        
                        tempPlayerVehIndex= GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID())
                        IF NOT (IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL (tempPlayerVehIndex))) //Wings likely to cause clipping / collision issues.
                        AND NOT (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL (tempPlayerVehIndex))) //Rotors could get caught against mountain side

                            IF ARE_STRINGS_EQUAL (warpLocations[iWarpLocation].tlLocName, "CM_LOC_28")//Mt. Gordo from americanReplay.txt
                                
                                cprintln(DEBUG_DIRECTOR,"Warp Location - Mount Gordo special case reposition cued whilst in vehicle. See bug 2557239.")
                                fRespawnHeading = 209.1812
                                vRespawnCoords = <<490.7735, 5589.5122, 793.0880>> //Reasonably flat ground at Gordo summit.  

                                //Setting these coords will eventually warp the player in most cases to the road leading up.

                            ENDIF
                           
                            IF ARE_STRINGS_EQUAL (warpLocations[iWarpLocation].tlLocName, "CM_LOC_27")//Cove from americanReplay.txt
                                
                                cprintln(DEBUG_DIRECTOR,"Warp Location - Cove special case reposition cued whilst in vehicle. See bug 2557239.")
                                fRespawnHeading = 99.5279
                                vRespawnCoords = <<3101.2766, 2244.6960, 71.6135>> //Above cove, in gorse bush and grass. You wouldn't get to the cove itself in a car.
                            
                                //This will warp the player to the above. No road nodes will be found.

                            ENDIF

                        ELSE

                            cprintln(DEBUG_DIRECTOR,"Warp Location - Special case Warp detected but player in forbidden vehicle. See bug 2557239.")

                        ENDIF

                    ENDIF
                    //end of section added for Bug 2557239.

                    //DO_PLAYER_MAP_WARP_WITH_LOAD(vRespawnCoords, fPlayerHeading, FALSE, 12000, 1000)  
                ENDIF
            ENDIF           
        ELSE
            vRespawnCoords = vLastPlayerCoord
            IF !doMapLoadWithReposition
                float fLastGround, fWaterHeight
                bool zCheck = GET_GROUND_Z_FOR_3D_COORD(vLastPlayerCoord,fLastGround)
                GET_WATER_HEIGHT(vLastPlayerCoord + <<0,0,50>>,fWaterHeight)
                IF zCheck
                    cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() vRespawnCoords.Z = ",vRespawnCoords.Z, ", fLastGround = ",fLastGround,"/",fWaterHeight)
                    vRespawnCoords.Z = FMAX(fLastGround, fWaterHeight)
                ENDIF
            ENDIF
            //SET_ENTITY_COORDS(player_ped_id(),vRespawnCoords-<<0,0,1.0>>)
            cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() Shapetest pass. Respawn at: ",vRespawnCoords)
        ENDIF
        
        cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() heading = ",fPlayerHeading)
        
        IF doMapLoadWithReposition
            float fFinalGroundCheck, fWaterHeight
            bool zCheck = GET_GROUND_Z_FOR_3D_COORD(vRespawnCoords + <<0,0,0.1>>,fFinalGroundCheck)
            GET_WATER_HEIGHT(vLastPlayerCoord + <<0,0,50>>,fWaterHeight)
            fFinalGroundCheck = FMAX(fFinalGroundCheck,fWaterHeight)
            IF vRespawnCoords.z < fFinalGroundCheck AND zCheck
                cprintln(DEBUG_DIRECTOR,"PERFORM_SHAPETEST() vRespawnCoords.z = ",vRespawnCoords.z," , fFinalGroundCheck = ",fFinalGroundCheck,"/",fWaterHeight)
                vRespawnCoords.z = FMAX(fFinalGroundCheck, fWaterHeight)
            ENDIF
        ENDIF
        
        SWITCH iWarpStage
            CASE 0
                if !IS_NEW_LOAD_SCENE_ACTIVE()
					CDEBUG1LN(debug_director,"PERFORM_SHAPETEST() Starting load scene at ",vRespawnCoords)
                    NEW_LOAD_SCENE_START_SPHERE(vRespawnCoords,20)
					iWarpTimeout = GET_GAME_TIMER() + 10000		//10 second time-out for the load scene.
                endif
                iWarpStage++
            BREAK
            CASE 1                  
                IF IS_NEW_LOAD_SCENE_LOADED()
                or !IS_NEW_LOAD_SCENE_ACTIVE()  
				or iWarpTimeout < GET_GAME_TIMER()
					CDEBUG1LN(debug_director,"PERFORM_SHAPETEST() Load scene completed or timed out, wait for player to settle")
					FLOAT fGroundZ, fWaterHeight
                    GET_GROUND_Z_FOR_3D_COORD(vRespawnCoords, fGroundZ)
                    GET_WATER_HEIGHT(vRespawnCoords+<<0,0,30>>,fWaterHeight)
                    BOOL bSnapToGround 
                    bSnapToGround = NOT (fGroundZ < fWaterHeight)        //Don't snap to ground if the ground is under the sea level
                    DO_PLAYER_TELEPORT(vRespawnCoords,fRespawnHeading,FALSE,bSnapToGround,20000)
                    //start wait for player to settle
                    iWarpStage = 0
                    waitTimer = GET_GAME_TIMER()
                    waitingForPlayerToSettle = TRUE
                    NEW_LOAD_SCENE_STOP()
                    
                    //Activate physics if in a vehicle
                    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                        ACTIVATE_PHYSICS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
                    ENDIF
                    
                    //No longer returning true, waiting for the player to settle
                ENDIF
            BREAK           
        ENDSWITCH   
    ENDIF
    
    RETURN FALSE
ENDFUNC


PROC RESTORE_DIRECTOR_MODE_STARTING_GAMESTATE(BOOL bIgnoreCharSwap = FALSE)
    IF ARE_STRINGS_EQUAL(g_snapshotScriptName, GET_THIS_SCRIPT_NAME())
        // We may have given the player lots of weapons with infinite ammo. 
        // Make sure we remove them.
        IF NOT bIgnoreCharSwap
            REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
        ENDIF
        Restore_Game_State_Snapshot(g_startSnapshot,bIgnoreCharSwap)
    ENDIF
ENDPROC


BUILDING_STATE_ENUM BS_Floyd_Window = BUILDINGSTATE_NORMAL
BUILDING_STATE_ENUM BS_Floyd_Tape = BUILDINGSTATE_NORMAL
BOOL bSavedApartment = FALSE
/// PURPOSE: removes police tape and bloody windows from Floyd's apartment    
PROC SET_TREVORS_APARTMENT_CLEAN()
    //Save current apartment look
    IF NOT bSavedApartment
        CDEBUG1LN(debug_director, "Director Mode: Saved Trevor's apartment's current state.")
        BS_Floyd_Window = GET_BUILDING_STATE(BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW)
        BS_Floyd_Tape = GET_BUILDING_STATE(BUILDINGNAME_IPL_FLOYDS_APPARTMENT_CRIME_TAPE)
        bSavedApartment = TRUE
    ENDIF
    // Clean-up tape and windows
    CDEBUG1LN(debug_director, "Director Mode: Cleaning up Trevor's apartment.")
    SET_BUILDING_STATE (BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW, BUILDINGSTATE_NORMAL)
    SET_BUILDING_STATE (BUILDINGNAME_IPL_FLOYDS_APPARTMENT_CRIME_TAPE, BUILDINGSTATE_NORMAL)
ENDPROC

PROC RESET_TREVORS_APARTMENT()
    IF bSavedApartment
        CDEBUG1LN(debug_director, "Director Mode: Resetting Trevor's apartment's state.")
        SET_BUILDING_STATE (BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW, BS_Floyd_Window)
        SET_BUILDING_STATE (BUILDINGNAME_IPL_FLOYDS_APPARTMENT_CRIME_TAPE, BS_Floyd_Tape)
    ENDIF
ENDPROC

BUILDING_STATE_ENUM BS_Showroom_Window = BUILDINGSTATE_NORMAL

PROC SET_SHOWROOM_BOARDED()
    BS_Showroom_Window = GET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS)
//  CPRINTLN(debug_dan,"Showroom boards current state is ",BS_Showroom_Window)
    IF BS_Showroom_Window = BUILDINGSTATE_DESTROYED
//      CPRINTLN(debug_dan,"Now setting to cleanup")
        SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS,BUILDINGSTATE_CLEANUP)
    ENDIF
ENDPROC

PROC RESET_SHOWROOM_BOARDED()
    IF BS_Showroom_Window = BUILDINGSTATE_DESTROYED
        SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, BS_Showroom_Window)
    ENDIF
ENDPROC

PROC CHECK_AND_TOGGLE_BUSY_SPINNER(BOOL bTurnOn, STRING stText)
	stText = stText
	IF bTurnOn
		//Set the spinner manually on
		IF IS_SCREEN_FADED_OUT()
			IF NOT BUSYSPINNER_IS_DISPLAYING() AND NOT bSpinnerSetOn
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(2)
				CPRINTLN(debug_dan,"Turning busy spinner on ",stText)
				bSpinnerSetOn = TRUE
			ENDIF
		ENDIF
	ELSE
		//Disable spinner manually if we have to
		IF bSpinnerSetOn
			IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
				CPRINTLN(debug_dan,"Turning busyspinner off ",stText)
				BUSYSPINNER_OFF()
				bSpinnerSetOn = FALSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC


FUNC BOOL REQUEST_PLAYER_CARD_STATS(GAMER_HANDLE& sGamerHandles[])

    //The request hasn't been made yet. Try and start the request.
    IF NOT bPlayerCardRequested
        CPRINTLN(DEBUG_DIRECTOR, "Filling out player menu player list.")
        IF FILLOUT_PM_PLAYER_LIST(sGamerHandles, 1, PLAYER_LIST_TYPE_DIRECTORY)
            REFRESH_PLAYER_LIST_STATS(PLAYER_LIST_TYPE_DIRECTORY)
            CPRINTLN(DEBUG_DIRECTOR, "Player list filled.")
            bPlayerCardRequested = TRUE
        ELSE
            CASSERTLN(DEBUG_DIRECTOR, "REQUEST_PLAYER_CARD_STATS: Native FILLOUT_PM_PLAYER_LIST failed.")
            RETURN FALSE
        ENDIF
    //The request has been made sucessfully. Check if it's finished loading.
    ELSE
        IF REFRESH_PLAYER_LIST_STATS(PLAYER_LIST_TYPE_DIRECTORY)
        //IF NOT NETWORK_CHECK_DATA_MANAGER_PENDING(ENUM_TO_INT(PLAYER_LIST_TYPE_DIRECTORY))
            IF NETWORK_CHECK_DATA_MANAGER_SUCCEEDED_FOR_HANDLE(ENUM_TO_INT(PLAYER_LIST_TYPE_DIRECTORY), sGamerHandles[0])
                //Request has loaded. We're done.
                NETWORK_SET_CURRENT_DATA_MANAGER_HANDLE(sGamerHandles[0])
                CPRINTLN(DEBUG_DIRECTOR, "REQUEST_PLAYER_CARD_STATS finished.")
                bOnlineCharactersAvailable = TRUE
                RETURN TRUE
            ELSE
                CASSERTLN(DEBUG_DIRECTOR, "REQUEST_PLAYER_CARD_STATS: Native NETWORK_CHECK_DATA_MANAGER_SUCCEEDED_FOR_HANDLE failed.")
                RETURN FALSE
            ENDIF
        
        ELSE
            CDEBUG1LN(DEBUG_DIRECTOR, "Waiting for NETWORK_CHECK_DATA_MANAGER_PENDING to return TRUE...")
        ENDIF
    ENDIF
    
    //Waiting on request to finish loading.
    RETURN FALSE
    
ENDFUNC


CONST_INT MP_CARD_STATS_TIMEOUT 10000//10 second timeout on grabbing MP characters

PROC LOAD_ONLINE_STATS_FOR_DIRECTOR_MODE()

    IF NOT bPlayerCardLoaded                        
        if GET_CURRENT_FRONTEND_MENU_VERSION() = FE_MENU_VERSION_EMPTY_NO_BACKGROUND    
            CPRINTLN(DEBUG_DIRECTOR, "Starting loading online stats for multiplayer chracter models...")
            GAMER_HANDLE sLocalGamerHandle[1]
            sLocalGamerHandle[0] = GET_LOCAL_GAMER_HANDLE()
            
            IF NETWORK_IS_SIGNED_ONLINE()
                CPRINTLN(DEBUG_DIRECTOR, "Network is signed in online. Starting to wait for player card stats to load...")
                INT iTimer = GET_GAME_TIMER()
                WHILE (NOT REQUEST_PLAYER_CARD_STATS(sLocalGamerHandle)
                and NETWORK_IS_SIGNED_ONLINE()) //backup for if online drops out after the request has started.
                and GET_GAME_TIMER() - iTimer < MP_CARD_STATS_TIMEOUT   
					CPRINTLN(DEBUG_DIRECTOR, "REQUEST_PLAYER_CARD_STATS looping, timeout in ",MP_CARD_STATS_TIMEOUT - (GET_GAME_TIMER() - iTimer)," ms")
					CHECK_AND_TOGGLE_BUSY_SPINNER(TRUE,"while waiting for stats")
                    WAIT(0)
                ENDWHILE
                bPlayerCardLoaded = TRUE
                CPRINTLN(DEBUG_DIRECTOR, "...Finished waiting for player card stats to load.")
            ELSE
                CPRINTLN(DEBUG_DIRECTOR,  "LOAD_ONLINE_STATS_FOR_DIRECTOR_MODE: Network was not signed in online.")
            ENDIF
            CPRINTLN(DEBUG_DIRECTOR, "...Online stats for multiplayer chracter models finished loading.")           
        endif           
    ENDIF
ENDPROC


//load 'barking' sounds/animation
FUNC BOOL LOAD_ANIMAL_AUDIO_AND_ANIM(MODEL_NAMES playerModel = DUMMY_MODEL_FOR_SCRIPT)
    cprintln(debug_director,"LOAD_ANIMAL_AUDIO_AND_ANIM()")
    BOOL returnValue = TRUE

    IF playerModel = DUMMY_MODEL_FOR_SCRIPT
        playerModel = GET_ENTITY_MODEL(player_ped_id())
    ENDIF
    
    IF Is_Model_A_Playable_Animal(playerModel)
        //CPRINTLN(DEBUG_DIRECTOR, "LOAD_ANIMAL_AUDIO_AND_ANIM")
        Get_Pickup_Sound_Data(animalAnimSoundData.sBarkSound,playerModel)                   
        
        IF NOT IS_STRING_NULL_OR_EMPTY(animalAnimSoundData.sBarkSound.strBarkBank)
            IF NOT REQUEST_SCRIPT_AUDIO_BANK(animalAnimSoundData.sBarkSound.strBarkBank)
                cprintln(debug_director,"LOAD_ANIMAL_AUDIO_AND_ANIM() return false A")
                returnValue = FALSE
            ENDIF
            //CPRINTLN(DEBUG_DIRECTOR, "REQUEST_SCRIPT_AUDIO_BANK ", animalAnimSoundData.sBarkSound.strBarkBank, " = ", returnValue)
        ENDIF
        
        Get_Pickup_Type_Bark_Anim_Data(animalAnimSoundData.strBarkDict,animalAnimSoundData.strBarkAnim,playerModel)
        
        IF NOT IS_STRING_NULL_OR_EMPTY(animalAnimSoundData.strBarkDict)
            REQUEST_ANIM_DICT(animalAnimSoundData.strBarkDict)
            IF NOT HAS_ANIM_DICT_LOADED(animalAnimSoundData.strBarkDict)
                cprintln(debug_director,"LOAD_ANIMAL_AUDIO_AND_ANIM() ",animalAnimSoundData.strBarkDict," return false B")
                returnValue = FALSE
            ENDIF
            //CPRINTLN(DEBUG_DIRECTOR, "HAS_ANIM_DICT_LOADED ", animalAnimSoundData.strBarkDict, " = ", returnValue)
        ENDIF
    ENDIF
    
    animalAnimSoundData.barkTimer = GET_GAME_TIMER()
    
    cprintln(debug_director,"LOAD_ANIMAL_AUDIO_AND_ANIM() return ",returnValue)
    RETURN returnValue
ENDFUNC 

//release bark sound id
PROC STOP_SHORTLISTED_ANIMAL_SOUND(INT &iBarkSoundID)
    IF iBarkSoundID != -1
        //CPRINTLN(DEBUG_DIRECTOR, "stop, release sound = ", iBarkSoundID)
        STOP_SOUND(iBarkSoundID)
        RELEASE_SOUND_ID(iBarkSoundID)
        iBarkSoundID = -1
    ENDIF
ENDPROC

//unload shortlisted 'barking' sounds/animation
PROC UNLOAD_ANIMAL_AUDIO_AND_ANIM(BOOL cleanUp = FALSE)
    CPRINTLN(debug_director,"UNLOAD_ANIMAL_AUDIO_AND_ANIM")
    STOP_SHORTLISTED_ANIMAL_SOUND(animalAnimSoundData.iBarkSoundID)
    
    IF NOT IS_STRING_NULL_OR_EMPTY(animalAnimSoundData.strBarkDict)
        REMOVE_ANIM_DICT(animalAnimSoundData.strBarkDict)
    ENDIF
    
    IF NOT IS_STRING_NULL_OR_EMPTY(animalAnimSoundData.sBarkSound.strBarkBank) 
        RELEASE_NAMED_SCRIPT_AUDIO_BANK(animalAnimSoundData.sBarkSound.strBarkBank) 
    ENDIF
    //CPRINTLN(DEBUG_DIRECTOR, "UNLOAD_ANIMAL_AUDIO_AND_ANIM = ", GET_ENTITY_MODEL(PLAYER_PED_ID()))
        
    //confirm reset values
    animalAnimSoundData.iBarkSoundID = -1
    animalAnimSoundData.sBarkSound.strBarkSound = ""
    animalAnimSoundData.sBarkSound.strBarkBank = ""
    animalAnimSoundData.strBarkDict = ""
    animalAnimSoundData.strBarkAnim = ""
    
    IF cleanUp
        RELEASE_SCRIPT_AUDIO_BANK()
    ENDIF
ENDPROC

/// PURPOSE: Adjusts return coordinates to compensate for edge cases ( close to objects / doors
PROC ADJUST_RETURN_COORDS()
    //Just outside Trevor's apartment
//  IF GET_DISTANCE_BETWEEN_COORDS(vLaunchCoords, <<-1150.0822, -1521.8291, 10.6331>>) < 2
//      vLaunchCoords = <<-1148.8474, -1522.7075, 9.6331>>
//      //Set the apartment door locked now if not open in flow
//      CPRINTLN(DEBUG_DIRECTOR,"Close to Trevor's appartment door, moving to ",vLaunchCoords)
//  ENDIF
ENDPROC


FUNC DOOR_STATE_ENUM GET_INSTANT_STATE_FOR_DOOR(DOOR_NAME_ENUM door)
    DOOR_STATE_ENUM tempDS = GET_DOOR_STATE_FOR_SAVEHOUSE(door)
    DOOR_DATA_STRUCT eData = GET_DOOR_DATA(door)
    #IF IS_DEBUG_BUILD
        CDEBUG3LN(DEBUG_DIRECTOR, "Door state for ", eData.dbg_name, " is ",tempDS)
    #ENDIF
    IF tempDS = DOORSTATE_UNLOCKED
        RETURN DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
    ELSE
        //Compare door interior with player's, don't force lock if the same
        INTERIOR_INSTANCE_INDEX intDoor = GET_INTERIOR_AT_COORDS(eData.coords)
        IF (intLaunchInterior <> intDoor) OR (intLaunchInterior = NULL)
            RETURN DOORSTATE_FORCE_LOCKED_THIS_FRAME
        ELSE 
            CPRINTLN(DEBUG_DIRECTOR,"Door and player are in the same interior, unlocking door ",eData.dbg_name)
            RETURN DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
        ENDIF
    ENDIF
ENDFUNC

/// Locks all savehouse doors. The building controller should pick up in a few frames and unlock the suitable ones
PROC LOCK_SAVEHOUSE_DOORS()
    DOOR_NAME_ENUM eDoor
    //Franklin
    FOR eDoor = DOORNAME_F_HOUSE_SC_F to DOORNAME_F_HOUSE_VH_F
        SET_DOOR_STATE(eDoor,GET_INSTANT_STATE_FOR_DOOR(eDoor))
    ENDFOR  
    //Michael
    FOR eDoor = DOORNAME_M_MANSION_F_L to DOORNAME_M_MANSION_BW
        SET_DOOR_STATE(eDoor,GET_INSTANT_STATE_FOR_DOOR(eDoor))
    ENDFOR  
    
    //Trevor
    FOR eDoor = DOORNAME_T_TRAILER_CS to DOORNAME_T_APARTMENT_VB
        SET_DOOR_STATE(eDoor,GET_INSTANT_STATE_FOR_DOOR(eDoor))
    ENDFOR  
    
    //B* 2239958: Lock the sweatshop doors after choosing the heli finale
    IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
    AND GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_AGENCY) = HEIST_CHOICE_AGENCY_HELICOPTER
        SET_DOOR_STATE(DOORNAME_SWEATSHOP_L,DOORSTATE_FORCE_LOCKED_THIS_FRAME) 
        SET_DOOR_STATE(DOORNAME_SWEATSHOP_R,DOORSTATE_FORCE_LOCKED_THIS_FRAME)
    ENDIF
ENDPROC


enum enumDirSex
    MALE,
    FEMALE,
    ANIMAL,
    IRRELEVANT
ENDENUM

//bool currentSelectedSexIsMale

PROC GET_SELECTED_MODEL(MODEL_NAMES findThisModel,int &iSelected,int &iTotal,enumDirSex desiredSex,enumDirSex storedDataSex, MODEL_NAMES &chosenModel, MODEL_NAMES model1,MODEL_NAMES model2=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model3=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model4=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model5=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model6=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model7=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model8=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model9=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model10=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model11=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model12=DUMMY_MODEL_FOR_SCRIPT)

        
    IF (desiredSex = storedDataSex)
        MODEL_NAMES modelList[12]
        modelList[0] = model1
        modelList[1] = model2
        modelList[2] = model3
        modelList[3] = model4
        modelList[4] = model5
        modelList[5] = model6
        modelList[6] = model7
        modelList[7] = model8
        modelList[8] = model9
        modelList[9] = model10
        modelList[10] = model11
        modelList[11] = model12
        
        iTotal = 0
        int i
        for i = 0 to COUNT_OF(modelList)-1
            iTotal = i+1
            IF modelList[i] = DUMMY_MODEL_FOR_SCRIPT            
                iTotal = i
                i = COUNT_OF(modelList)
            ENDIF
        endfor
        
        if iSelected >= iTotal
            iSelected = 0
        ENDIF
        
        if iSelected < 0
            iSelected = iTotal - 1
        ENDIF
        
        //this will be true if we're trying to find the specific index of a certain ped model. In which case overwrite the value returned in iTotal with the index
        IF findThisModel != DUMMY_MODEL_FOR_SCRIPT
            for i = 0 to COUNT_OF(modelList)-1
                IF findThisModel = modelList[i]
                    iTotal = i
                endif
            endfor
        ENDIF
        
        chosenModel = modelList[iSelected]

    ENDIF

ENDPROC

PROC GET_SELECTED_ANIMAL(MODEL_NAMES findThisModel,MODEL_NAMES &chosenModel, int &iTotal, MODEL_NAMES model1,MODEL_NAMES model2=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model3=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model4=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model5=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model6=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model7=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model8=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model9=DUMMY_MODEL_FOR_SCRIPT,MODEL_NAMES model10=DUMMY_MODEL_FOR_SCRIPT)
    int iSelectedVoid = 0
    GET_SELECTED_MODEL(findThisModel,iSelectedVoid,iTotal,ANIMAL,ANIMAL,chosenModel,model1,model2,model3,model4,model5,model6,model7,model8,model9,model10)
ENDPROC


FUNC MODEL_NAMES GET_ONLINE_CHARACTER_MODEL(INT iSlot)
    if sOnlineChar[islot].model = DUMMY_MODEL_FOR_SCRIPT        
        IF iSlot != -1
            SWITCH GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_CHAR_PICTURE, iSlot) 
                CASE 0      
                    cprintln(debug_director,"GET_ONLINE_CHARACTER_MODEL: iSlot[",iSlot,"] PACKED_CHAR_PICTURE: male ")
                    sOnlineChar[islot].model = MP_M_FREEMODE_01
                    RETURN MP_M_FREEMODE_01     
                BREAK
                CASE 1      
                    cprintln(debug_director,"GET_ONLINE_CHARACTER_MODEL: iSlot[",iSlot,"] PACKED_CHAR_PICTURE: female ")
                    sOnlineChar[islot].model = MP_F_FREEMODE_01
                    RETURN MP_F_FREEMODE_01     
                BREAK
                DEFAULT     
                    CASSERTLN(DEBUG_DIRECTOR, "GET_ONLINE_CHARACTER_MODEL: Tried to query online model but the char picture stat was not 0 or 1.")
                BREAK
            ENDSWITCH
        ELSE
            CASSERTLN(DEBUG_DIRECTOR, "GET_ONLINE_CHARACTER_MODEL: Tried to query online model but could not determine the last active character slot.")
        ENDIF
    else        
//      cprintln(debug_director,"GET_ONLINE_CHARACTER_MODEL: online model already set sOnlineChar[",iSlot,"].model: ",sOnlineChar[islot].model)
        RETURN sOnlineChar[islot].model     
    endif
    
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


FUNC MODEL_NAMES GET_CATEGORY_MODEL(enumMenuCategory modelCategory,enumDirSex sex, int iSelected, int &iTotal, MODEL_NAMES findThisModel = DUMMY_MODEL_FOR_SCRIPT)
    
    // If we are trying to load the Online model category, check if we
    // first need to load the online stats.
    IF modelCategory = MC_ONLINE
        LOAD_ONLINE_STATS_FOR_DIRECTOR_MODE()
    ENDIF
    
    model_names chosenModel
    iTotal = 1
    iSelected=iSelected
    SWITCH modelCategory
        
        //professionals
        
        CASE MC_PROAIR RETURN S_M_M_Pilot_01 BREAK
        CASE MC_PROBAR  RETURN  A_M_Y_Vinewood_02   BREAK
        CASE MC_PROBOU  RETURN  S_M_M_Bouncer_01    BREAK
        CASE MC_PROCEO  IF sex = MALE RETURN    A_M_Y_Business_01 ELSE RETURN A_F_M_BevHills_02 ENDIF BREAK
        CASE MC_PROCHE  RETURN  S_M_Y_Chef_01   BREAK
        CASE MC_PROCOO  RETURN  S_M_M_LineCook  BREAK
        CASE MC_PRODOC  RETURN  S_M_M_Doctor_01 BREAK
        CASE MC_PRODOR  RETURN  S_M_Y_Doorman_01    BREAK
        CASE MC_PRODRA  RETURN  A_M_M_TranVest_01   BREAK
        CASE MC_PROIT   RETURN  S_M_M_LIFEINVAD_01  BREAK       
        CASE MC_PROMAR  RETURN  S_M_M_Mariachi_01   BREAK
        CASE MC_PROOFF  IF sex = MALE RETURN A_M_Y_Business_02 ELSE RETURN  A_F_M_Business_02 ENDIF BREAK
        CASE MC_PROSCI  RETURN  S_M_M_Scientist_01  BREAK
        CASE MC_PROSTY  IF sex = MALE RETURN    A_M_Y_Vinewood_01 ELSE RETURN   A_F_Y_Hipster_04  ENDIF BREAK
        CASE MC_PROVAL  RETURN  S_M_Y_Valet_01  BREAK
        CASE MC_PROVEN  IF sex = MALE RETURN    A_M_Y_Business_03 ELSE RETURN A_F_M_BevHills_01 ENDIF BREAK
        CASE MC_PROWAI  RETURN  S_M_Y_Waiter_01 BREAK
                
        CASE MC_PROAIH  RETURN  S_F_Y_AirHostess_01 BREAK
        CASE MC_PROAUP  RETURN  A_F_Y_Hipster_02    BREAK
        CASE MC_PROBIN  RETURN  S_F_Y_Shop_MID  BREAK
        
        CASE MC_PROFAS  RETURN  U_F_Y_SpyActress    BREAK
        CASE MC_PROHAI  RETURN  S_F_M_FemBarber BREAK
        CASE MC_PROINT  RETURN  A_F_Y_Business_03   BREAK
        
        CASE MC_PROLAW  RETURN  A_F_Y_Business_01   BREAK
        CASE MC_PROPON  RETURN  S_F_M_Shop_HIGH BREAK
        CASE MC_PROREL  RETURN  A_F_Y_Business_02   BREAK
        
        CASE MC_PROSTR  RETURN  S_F_Y_Hooker_01 BREAK
        CASE MC_PROTEA  RETURN  A_F_Y_Business_04   BREAK


        //uptown
        CASE    MC_UPTEAS   RETURN  A_M_Y_Hipster_03    BREAK
        CASE    MC_UPTECC   RETURN  A_M_Y_Vinewood_03   BREAK
        CASE    MC_UPTHAW   RETURN A_M_Y_Hipster_02 BREAK
        CASE    MC_UPTMIR   RETURN A_M_Y_Hipster_01 BREAK
        CASE    MC_UPTHIP   RETURN  A_F_Y_Hipster_03 BREAK
        CASE    MC_UPTCOU   RETURN  A_F_Y_Hipster_01 BREAK
        CASE    MC_UPTROC   RETURN  A_M_M_BevHills_01   BREAK
        CASE    MC_UPTVIN   RETURN  A_M_Y_VinDouche_01  BREAK
                        
        CASE    MC_UPTART   RETURN  A_F_Y_Vinewood_01   BREAK
        CASE    MC_UPTBOH   RETURN  A_F_Y_Vinewood_04   BREAK
        CASE    MC_UPTPOR   RETURN  A_F_Y_BevHills_01   BREAK
        CASE    MC_UPTRIC   RETURN  A_F_Y_BevHills_03   BREAK
        CASE    MC_UPTROH   RETURN  A_F_Y_BevHills_02   BREAK
        CASE    MC_UPTVIB   RETURN  A_F_Y_BevHills_04   BREAK
        CASE    MC_UPTWAN   RETURN  A_F_Y_Genhot_01 BREAK

        //downtown
        CASE    MC_DOWMAZ   RETURN  A_M_M_SouCent_01    BREAK
        CASE    MC_DOWLAS   RETURN  A_M_Y_EastSA_01 BREAK
        CASE    MC_DOWSTR   RETURN  A_M_Y_Genstreet_02  BREAK
        CASE    MC_DOWRAN   RETURN  A_M_Y_Latino_01 BREAK
        CASE    MC_DOWSTM   IF sex = MALE RETURN A_M_M_FatLatin_01 ELSE RETURN  A_F_Y_SouCent_01 ENDIF BREAK
        CASE    MC_DOWHAR   IF sex = MALE RETURN A_M_M_Genfat_01 ELSE RETURN    A_F_M_FatWhite_01 ENDIF BREAK
        CASE    MC_DOWCO2   RETURN  A_M_M_Genfat_02 BREAK
        CASE    MC_DOWDAL   RETURN  A_M_Y_SouCent_03    BREAK
                        
        CASE    MC_DOWEMO   RETURN  A_F_M_EastSA_02 BREAK
        CASE    MC_DOWEM2   RETURN  A_F_Y_EastSA_01 BREAK
        CASE    MC_DOWTEE   RETURN  A_F_Y_EastSA_02 BREAK
        CASE    MC_DOWCOL   RETURN  A_F_Y_Indian_01 BREAK
        CASE    MC_DOWSOC   RETURN  A_F_M_FatBla_01 BREAK
        CASE    MC_DOWDAV   RETURN  A_F_Y_SouCent_02    BREAK
        //gangs
        
        //vagrants
        CASE    MC_VAGMIS   IF sex = MALE RETURN    A_M_M_SkidRow_01    ELSE RETURN     A_F_M_SkidRow_01    ENDIF BREAK
        CASE    MC_VAGDO1   IF sex = MALE RETURN    A_M_M_Tramp_01  ELSE RETURN     A_F_M_TrampBeac_01  ENDIF BREAK
        CASE    MC_VAGDO2   IF sex = MALE RETURN    A_M_O_Tramp_01  ELSE RETURN     A_F_M_Tramp_01  ENDIF BREAK
        CASE    MC_VAGMET   RETURN  A_M_Y_MethHead_01   BREAK           
        CASE    MC_VAGSAN   IF sex = MALE RETURN    A_M_Y_Salton_01 ELSE RETURN     A_F_M_Salton_01 ENDIF BREAK
        CASE    MC_VAGME2   RETURN  A_M_M_RurMeth_01    BREAK           
        CASE    MC_VAGOLD   RETURN  A_M_M_Hillbilly_01  BREAK           
        CASE    MC_VAGYOU   RETURN  A_M_M_Hillbilly_02  BREAK           
        CASE    MC_VAGPR1   RETURN  S_M_Y_PRISMUSCL_01  BREAK           
        CASE    MC_VAGPR2   RETURN  S_M_Y_Prisoner_01   BREAK           

        //beach bums
    
        CASE    MC_BEATOU   IF sex = MALE RETURN    A_M_M_Beach_01  ELSE RETURN     A_F_Y_Tourist_02    ENDIF BREAK
        CASE    MC_BEALOA   RETURN  A_M_M_Beach_02  BREAK           
        CASE    MC_BEAOLD   RETURN  A_M_O_Beach_01  BREAK           
        CASE    MC_BEAVES   IF sex = MALE RETURN    A_M_Y_Beach_01  ELSE RETURN A_F_Y_Beach_01  ENDIF BREAK
        CASE    MC_BEADEL   IF sex = MALE RETURN    A_M_Y_Beach_02  ELSE RETURN     A_F_M_Beach_01  ENDIF BREAK
        CASE    MC_BEASUN   RETURN  A_M_Y_Beach_03  BREAK           
        CASE    MC_BEASRF   RETURN  A_M_Y_StWhi_01  BREAK           
        CASE    MC_BEABO1   RETURN  A_M_Y_MusclBeac_01  BREAK           
        CASE    MC_BEABO2   RETURN  A_M_Y_MusclBeac_02  BREAK           
    
        CASE    MC_BEABOD   RETURN  A_F_M_BodyBuild_01  BREAK           

        //sports
        CASE    MC_SPOYOG   IF sex = MALE RETURN    A_M_Y_Yoga_01   ELSE RETURN A_F_Y_Yoga_01   ENDIF BREAK 
        CASE    MC_SPOSUR   RETURN  A_M_Y_Surfer_01 BREAK           
        CASE    MC_SPOSKA   RETURN  A_F_Y_Skater_01 BREAK           
        CASE    MC_SPOSK1   RETURN  A_M_M_Skater_01 BREAK           
        CASE    MC_SPOSK2   RETURN  A_M_Y_Skater_01 BREAK           
        CASE    MC_SPOSK3   RETURN  A_M_Y_Skater_02 BREAK           
        CASE    MC_SPOSK4   RETURN  A_M_Y_BeachVesp_01  BREAK           
        CASE    MC_SPOSK5   RETURN  A_M_Y_StWhi_02  BREAK           
        CASE    MC_SPORU1   RETURN  A_M_Y_Runner_01 BREAK           
        CASE    MC_SPORU2   RETURN  A_M_Y_Runner_02 BREAK           
        CASE    MC_SPORUN   RETURN  A_F_Y_Runner_01 BREAK           
        CASE    MC_SPOCYC   RETURN  A_M_Y_Cyclist_01    BREAK           
        CASE    MC_SPOROC   RETURN  A_M_Y_RoadCyc_01    BREAK           
        CASE    MC_SPOMO1   RETURN  A_M_Y_MotoX_01  BREAK           
        CASE    MC_SPOMO2   RETURN  A_M_Y_MotoX_02  BREAK           
        CASE    MC_SPOTEN   IF sex = MALE RETURN    A_M_M_Tennis_01 ELSE RETURN     A_F_Y_Tennis_01 ENDIF BREAK
        CASE    MC_SPOGO1   RETURN  A_M_Y_Golfer_01     BREAK           
        CASE    MC_SPOGO2   RETURN  A_M_M_Golfer_01 BREAK
        CASE    MC_SPOGOF RETURN    A_F_Y_Golfer_01 BREAK

        

        CASE    MC_GANTRB   RETURN  G_M_M_ChiBoss_01    BREAK
        CASE    MC_GANTG1   RETURN  G_M_M_ChiGoon_01    BREAK
        CASE    MC_GANTG2   RETURN  G_M_M_ChiGoon_02    BREAK
        CASE    MC_GANKOB   RETURN  G_M_M_KorBoss_01    BREAK
        CASE    MC_GANKOL   RETURN  G_M_Y_KorLieut_01   BREAK
        CASE    MC_GANKOG   RETURN  G_M_Y_Korean_01 BREAK
        CASE    MC_GANCHO   RETURN  G_F_Y_Vagos_01  BREAK
        CASE    MC_GANEPS1  RETURN A_M_Y_EPSILON_01 BREAK
        CASE    MC_GANEPS2  RETURN A_M_Y_EPSILON_02 BREAK
        CASE    MC_GANEPSF  RETURN A_F_Y_EPSILON_01 BREAK
        CASE    MC_GANSTR   RETURN  A_F_Y_EastSA_03 BREAK
        CASE    MC_GANAZB   RETURN  G_M_M_MexBoss_01    BREAK
        CASE    MC_GANVAB   RETURN  G_M_M_MexBoss_02    BREAK
        CASE    MC_GANAZG   RETURN  G_M_Y_Azteca_01 BREAK
        CASE    MC_GANVAG   RETURN  G_M_Y_MexGoon_03    BREAK
        CASE    MC_GANMGB   RETURN  G_M_Y_SalvaBoss_01  BREAK
        CASE    MC_GANMGG   RETURN  G_M_Y_SalvaGoon_01  BREAK
        CASE    MC_GANMGL   RETURN  G_M_Y_SalvaGoon_02  BREAK
        CASE    MC_GANMOB   RETURN  G_M_M_ArmBoss_01    BREAK
        CASE    MC_GANMOG   RETURN  G_M_M_ArmGoon_01    BREAK
        CASE    MC_GANMOL   RETURN  G_M_M_ArmLieut_01   BREAK
        CASE    MC_GANLOO   RETURN  G_M_Y_Lost_01   BREAK
        CASE    MC_GANLOM   RETURN  G_M_Y_Lost_02   BREAK
        CASE    MC_GANLOS   RETURN  G_M_Y_Lost_03   BREAK
        CASE    MC_GANLOH   RETURN  G_F_Y_Lost_01   BREAK
        CASE    MC_GANBAE   RETURN  G_M_Y_BallaEast_01  BREAK
        CASE    MC_GANBLO   RETURN  G_M_Y_BallaOrig_01  BREAK
        CASE    MC_GANOGB   RETURN  A_M_M_OG_Boss_01    BREAK
        CASE    MC_GANBAS   RETURN  G_M_Y_BallaSout_01  BREAK
        CASE    MC_GANCHA   RETURN  G_M_Y_FamCA_01  BREAK
        CASE    MC_GANDAV   RETURN  G_M_Y_FamDNF_01 BREAK
        CASE    MC_GANFOR   RETURN  G_M_Y_FamFor_01 BREAK
        CASE    MC_GANALT   RETURN  A_F_M_FATCULT_01    BREAK
        CASE    MC_GANAL1   RETURN  A_M_Y_ACult_02  BREAK
        CASE    MC_GANAL2   RETURN  A_M_O_ACult_01  BREAK
        CASE    MC_GANAL3   RETURN  A_M_O_ACult_02  BREAK


        CASE    MC_COSAST   RETURN  S_M_M_MovSpace_01   BREAK
        CASE    MC_COSSPY   RETURN  U_M_M_SpyActor  BREAK
        CASE    MC_COSCOR   RETURN  U_F_M_Corpse_01     BREAK
        CASE    MC_COSPOG   RETURN  U_M_Y_Pogo_01   BREAK
        CASE    MC_COSSPA   RETURN  U_M_Y_RSRanger_01   BREAK
        CASE    MC_COSALI   RETURN  S_M_M_MovAlien_01   BREAK
        CASE    MC_COSCLO   RETURN  S_M_Y_Clown_01  BREAK
        CASE    MC_COSMIM   RETURN  S_M_Y_Mime  BREAK
        CASE    MC_COSSTR   RETURN  S_M_M_StrPerf_01    BREAK

        CASE    MC_MILPL1   RETURN  S_M_M_Pilot_02  BREAK
        CASE    MC_MILPL2   RETURN  S_M_Y_Pilot_01  BREAK
        CASE    MC_MILMIG   RETURN  S_M_M_Marine_02 BREAK
        CASE    MC_MILSRG   RETURN  S_M_M_Marine_01 BREAK
        CASE    MC_MILCOR   RETURN  S_M_Y_Marine_01 BREAK
        CASE    MC_MILPRI   RETURN  S_M_Y_Marine_02 BREAK
        CASE    MC_MILBAT   RETURN  S_M_Y_Marine_03 BREAK
        CASE    MC_MILMIM   RETURN  S_M_Y_ArmyMech_01   BREAK
        CASE    MC_MILMEP   RETURN  S_M_Y_BlackOps_01   BREAK
        CASE    MC_MILMEC   RETURN  S_M_Y_BlackOps_02   BREAK
        CASE    MC_MILMEL   RETURN  S_M_M_ChemSec_01    BREAK
    ENDSWITCH
    
    SWITCH modelCategory
        CASE    MC_LABFAR   RETURN  A_M_M_Farmer_01 BREAK       
        CASE    MC_LABMIG   IF sex = MALE RETURN    S_M_M_Migrant_01    ELSE RETURN S_F_Y_Migrant_01    ENDIF BREAK
        CASE    MC_LABCN1   RETURN  S_M_Y_Construct_01  BREAK       
        CASE    MC_LABCN2   RETURN  S_M_Y_Construct_02  BREAK       
        CASE    MC_LABCN3   RETURN  S_M_M_LatHandy_01   BREAK       
        CASE    MC_LABGAB   RETURN  S_M_Y_Garbage   BREAK       
        CASE    MC_LABFAC   IF sex = MALE RETURN    S_M_Y_Factory_01    ELSE RETURN S_F_Y_Factory_01    ENDIF BREAK
        CASE    MC_LABMAI   RETURN  S_F_M_Maid_01   BREAK
        CASE    MC_LABGAR   RETURN  S_M_M_Gardener_01   BREAK       
        CASE    MC_LABJAN   RETURN  S_M_M_Janitor   BREAK       
        CASE    MC_LABSAD   RETURN  S_M_M_DockWork_01   BREAK       
        CASE    MC_LABDOC   RETURN  S_M_Y_Dockwork_01   BREAK       
        CASE    MC_LABME1   RETURN  S_M_Y_XMech_01  BREAK       
        CASE    MC_LABME2   RETURN  S_M_Y_XMech_02  BREAK       
        CASE    MC_LABBUG   RETURN  S_M_Y_PestCont_01   BREAK       

        CASE    MC_EMEFIR   RETURN  S_M_Y_Fireman_01    BREAK       
    CASE    MC_EMEPOL   IF sex = MALE RETURN    S_M_Y_Cop_01    ELSE RETURN S_F_Y_Cop_01    ENDIF BREAK
    CASE    MC_EMEFIB   RETURN  S_M_M_FIBOffice_01  BREAK       
    CASE    MC_EMENOO   RETURN  S_M_Y_Swat_01   BREAK       
    CASE    MC_EMEDOA   RETURN  S_M_M_ChemSec_01    BREAK       
    CASE    MC_EMEIAA   RETURN  S_M_M_CIASec_01 BREAK       
    CASE    MC_EMEHIG   RETURN  S_M_Y_HWayCop_01    BREAK       
    CASE    MC_EMEPAR   RETURN  S_M_M_Paramedic_01  BREAK       
    CASE    MC_EMECOA   RETURN  S_M_Y_USCG_01   BREAK       
    CASE    MC_EMERAN   RETURN  S_F_Y_RANGER_01 BREAK       
    CASE    MC_EMEBAY   IF sex = MALE RETURN    S_M_Y_Baywatch_01 ELSE RETURN   S_F_Y_BAYWATCH_01   ENDIF BREAK     
    CASE    MC_EMESHE   RETURN  S_F_Y_SHERIFF_01    BREAK       
    CASE    MC_TRAAR1   RETURN  S_M_M_Armoured_01   BREAK
    CASE    MC_TRAAR2   RETURN  S_M_M_Armoured_02   BREAK
    CASE    MC_TRAAIR   RETURN  S_M_Y_AirWorker BREAK
    CASE    MC_TRAPO1   RETURN  S_M_M_Postal_01 BREAK
    CASE    MC_TRAPO2   RETURN  S_M_M_Postal_02 BREAK
    CASE    MC_TRAPO3   RETURN  S_M_M_UPS_01    BREAK
    CASE    MC_TRAPO4   RETURN  S_M_M_UPS_02    BREAK
    CASE    MC_TRATRA   RETURN  S_M_M_LSMetro_01    BREAK
    CASE    MC_TRATRU   RETURN  S_M_M_Trucker_01    BREAK
        
        CASE MC_STOFRA  RETURN PLAYER_ONE BREAK
        CASE MC_STOTRE  RETURN PLAYER_TWO BREAK
        CASE MC_STOMIC RETURN PLAYER_ZERO BREAK
        CASE MC_STOAMA RETURN IG_AMANDATOWNLEY BREAK
        CASE MC_STOBEV RETURN IG_BEVERLY BREAK
        CASE MC_STOBRA RETURN IG_BRAD BREAK
        CASE MC_STOCHR RETURN IG_CHRISFORMAGE BREAK
        CASE MC_STODAV RETURN IG_DAVENORTON BREAK
        CASE MC_STODEV RETURN IG_DEVIN BREAK
        CASE MC_STODRF RETURN IG_DRFRIEDLANDER BREAK
        CASE MC_STOFAB RETURN IG_FABIEN BREAK
        CASE MC_STOFLO RETURN IG_FLOYD BREAK
        CASE MC_STOJIM RETURN IG_JIMMYDISANTO BREAK
        CASE MC_STOLAM RETURN IG_LAMARDAVIS BREAK
        CASE MC_STOLAZ RETURN IG_LAZLOW BREAK
        CASE MC_STOLES RETURN IG_LESTERCREST BREAK
        CASE MC_STOMAU RETURN IG_MAUDE BREAK
        CASE MC_STOTHO RETURN IG_MRS_THORNHILL BREAK
        CASE MC_STONER RETURN IG_NERVOUSRON BREAK   
        CASE MC_STOPAT RETURN IG_PATRICIA BREAK
        CASE MC_STOSIM RETURN IG_SIEMONYETARIAN BREAK
        CASE MC_STOSOL RETURN IG_SOLOMON BREAK
        CASE MC_STOSTE RETURN IG_STEVEHAINS BREAK       
        CASE MC_STOSTR RETURN IG_STRETCH BREAK
        CASE MC_STOTAN RETURN IG_TANISHA BREAK
        CASE MC_STOTAO RETURN IG_TAOCHENG BREAK
        CASE MC_STOTRA RETURN IG_TRACYDISANTO BREAK
        CASE MC_STOWAD RETURN IG_WADE BREAK
        
        
        CASE MC_SPEAND RETURN U_M_Y_Hippie_01 BREAK
        CASE MC_SPEBAY RETURN U_M_Y_BAYGOR BREAK
        CASE MC_SPEBIL RETURN U_M_O_FinGuru_01 BREAK
        CASE MC_SPECLI RETURN U_M_Y_MILITARYBUM BREAK
        CASE MC_SPEGRI RETURN U_M_M_GRIFF_01 BREAK
        CASE MC_SPEJAN RETURN U_F_Y_COMJane BREAK
        CASE MC_SPEJER RETURN S_M_M_StrPreach_01 BREAK
        CASE MC_SPEJES RETURN U_M_M_Jesus_01 BREAK
        CASE MC_SPEMAN RETURN U_M_Y_MANI BREAK
        CASE MC_SPEMIM RETURN S_M_Y_MIME BREAK
        CASE MC_SPEPAM RETURN U_F_O_MOVIESTAR BREAK
        CASE MC_SPEIMP RETURN U_M_Y_IMPORAGE BREAK
        CASE MC_SPEZOM RETURN U_M_Y_ZOMBIE_01 BREAK

        CASE MC_HEIGUS 
            selectedHeistMember = CM_GUNMAN_G_GUSTAV
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_G_GUSTAV)            
        BREAK
        CASE MC_HEIKAR 
            selectedHeistMember = CM_GUNMAN_G_KARL
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_G_KARL)          
        BREAK
        CASE MC_HEIPAC 
            selectedHeistMember = CM_GUNMAN_G_PACKIE_UNLOCK
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_G_PACKIE_UNLOCK)                         
        BREAK
        CASE MC_HEICHE 
            selectedHeistMember = CM_GUNMAN_G_CHEF_UNLOCK
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_G_CHEF_UNLOCK)           
        BREAK
        CASE MC_HEIHUG 
            selectedHeistMember = CM_GUNMAN_M_HUGH
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_M_HUGH)          
        BREAK
        CASE MC_HEINOR 
            selectedHeistMember = CM_GUNMAN_B_NORM
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_B_NORM)          
        BREAK
        CASE MC_HEIDAR 
            selectedHeistMember = CM_GUNMAN_B_DARYL
            RETURN GET_CREW_MEMBER_MODEL(CM_GUNMAN_B_DARYL)             
        BREAK
        CASE MC_HEIPAI 
            selectedHeistMember = CM_HACKER_G_PAIGE
            RETURN GET_CREW_MEMBER_MODEL(CM_HACKER_G_PAIGE)             
        BREAK
        CASE MC_HEICHR 
            selectedHeistMember = CM_HACKER_M_CHRIS
            RETURN GET_CREW_MEMBER_MODEL(CM_HACKER_M_CHRIS)             
        BREAK
        CASE MC_HEIRIC 
            selectedHeistMember = CM_HACKER_B_RICKIE_UNLOCK
            RETURN GET_CREW_MEMBER_MODEL(CM_HACKER_B_RICKIE_UNLOCK)             
        BREAK
        CASE MC_HEIEDD 
            selectedHeistMember = CM_DRIVER_G_EDDIE
            RETURN GET_CREW_MEMBER_MODEL(CM_DRIVER_G_EDDIE)             
        BREAK
        CASE MC_HEITAL 
            selectedHeistMember = CM_DRIVER_G_TALINA_UNLOCK
            RETURN GET_CREW_MEMBER_MODEL(CM_DRIVER_G_TALINA_UNLOCK)                 
        BREAK
        CASE MC_HEIKRM 
            selectedHeistMember = CM_DRIVER_B_KARIM
            RETURN GET_CREW_MEMBER_MODEL(CM_DRIVER_B_KARIM)                         
        BREAK

        CASE MC_ONLINE1     RETURN GET_ONLINE_CHARACTER_MODEL(0)                BREAK
        CASE MC_ONLINE2     RETURN GET_ONLINE_CHARACTER_MODEL(1)                BREAK

        CASE MC_ANIBOAR GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_BOAR) BREAK
        CASE MC_ANICAT GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_CAT_01) BREAK
        CASE MC_ANICOW GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_COW) BREAK
        CASE MC_ANICOY GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_COYOTE) BREAK
        CASE MC_ANIDEE GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_DEER) BREAK
        CASE MC_ANIHUS GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_HUSKY) BREAK
        CASE MC_ANIMOU GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_MTLION) BREAK
        CASE MC_ANIPIG GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_PIG) BREAK
        CASE MC_ANIPOO GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_POODLE) BREAK
        CASE MC_ANIPUG GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_PUG) BREAK
        CASE MC_ANIRAB GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_RABBIT_01) BREAK
        CASE MC_ANIRET GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_RETRIEVER) BREAK
        CASE MC_ANIROT GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_ROTTWEILER) BREAK
        CASE MC_ANISHE GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_SHEPHERD) BREAK
        CASE MC_ANIWES GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_WESTY) BREAK
        CASE MC_ANICHI GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_CHICKENHAWK) BREAK
        CASE MC_ANICOR GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_CORMORANT) BREAK
        CASE MC_ANICRO GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_CROW) BREAK
        CASE MC_ANIHEN GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_HEN) BREAK
        CASE MC_ANIPIN GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_PIGEON) BREAK
        CASE MC_ANISEA GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,A_C_SEAGULL) BREAK
        CASE MC_ANISAS GET_SELECTED_ANIMAL(findThisModel,chosenModel,iTotal,IG_ORLEANS) BREAK
    ENDSWITCH
    
    RETURN chosenModel
ENDFUNC


#if IS_DEBUG_BUILD
PROC debug_output_shortlist_data()
    CDEBUG3LN(debug_director,"Debug output shorlist data")
    int i,j
    structShortlistData bmk
    bmk=bmk
    repeat count_of(shortlistData) i                                        
        repeat count_of(bmk.modelVariation) j
            CDEBUG3LN(debug_director," shortlist entry = ",i," modelVariation ",j," = ",shortlistData[i].modelVariation[j])
        endrepeat
        repeat count_of(bmk.textureVariation) j
            CDEBUG3LN(debug_director," shortlist entry = ",i," textureVariation ",j," = ",shortlistData[i].textureVariation[j])
        endrepeat
    endrepeat
endproc
#endif

PROC SAVE_DATA()

    int i,j
    int iValueToStore
    int elementCount=0
    structShortlistData bmk
    bmk=bmk
    
    CPRINTLN(DEBUG_DIRECTOR,"Save Data")
    
    #IF IS_DEBUG_BUILD
    debug_output_shortlist_data()
    #ENDIF
    
    repeat count_of(shortlistData) i
        repeat count_of(bmk.modelVariation) j

            iValueToStore = shortlistData[i].modelVariation[j]          
            SET_PACKED_BITFIELD_VALUE_IN_ARRAY(g_savedGlobals.sDirectorModeData.iShortlistData, elementCount, 6, iValueToStore )
            elementCount++
        endrepeat
        repeat count_of(bmk.textureVariation) j
            iValueToStore = shortlistData[i].textureVariation[j]            
            SET_PACKED_BITFIELD_VALUE_IN_ARRAY(g_savedGlobals.sDirectorModeData.iShortlistData, elementCount, 6, iValueToStore )
            elementCount++
        endrepeat   
        g_savedGlobals.sDirectorModeData.shortlistCategories[i] = enum_to_int(shortlistData[i].category)
        g_savedGlobals.sDirectorModeData.shortlistModels[i] = enum_to_int(shortlistData[i].model)
        
        IF shortlistData[i].bMale
            SET_BIT(g_savedGlobals.sDirectorModeData.shortlistSex,i)
        ELSE
            CLEAR_BIT(g_savedGlobals.sDirectorModeData.shortlistSex,i)
        ENDIF
        
        //Save voice hash
        g_savedGlobals.sDirectorModeData.shortlistVoice[i] = shortlistData[i].iVoiceHash
    endrepeat

    g_savedGlobals.sDirectorModeData.vSavedMapCoord = vLastPlayerCoord
    CPRINTLN(DEBUG_DIRECTOR,"Saving vLastPlayerCoord to global value : ", vLastPlayerCoord)
    g_savedGlobals.sDirectorModeData.fSavedPlayerHeading = fPlayerHeading
    
    g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[0]   = warpLocations[ENUM_TO_INT(DTL_USER_1)].position
    g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading[0]    = warpLocations[ENUM_TO_INT(DTL_USER_1)].heading
    g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior[0]   = warpLocations[ENUM_TO_INT(DTL_USER_1)].interior   

    g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[1]   = warpLocations[ENUM_TO_INT(DTL_USER_2)].position
    g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading[1]    = warpLocations[ENUM_TO_INT(DTL_USER_2)].heading
    g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior[1]   = warpLocations[ENUM_TO_INT(DTL_USER_2)].interior
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    DMPROP_SAVE_SCENES_TO_GLOBALS()
    #ENDIF

ENDPROC

PROC SETUP_MP_CHAR_DATA()
    
    LOAD_ONLINE_STATS_FOR_DIRECTOR_MODE()
    
    int iChars
    repeat MAX_ONLINE_CHARS iChars
        if bOnlineCharactersAvailable
            if NETWORK_IS_SIGNED_ONLINE()
                INT iTeamStat = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_CHAR_TEAM, iChars)
                cprintln(debug_director,"SETUP_MP_CHAR_DATA: iChars[",iChars,"] char team stat: ",iTeamStat)        
                if  iTeamStat != TEAM_INVALID       
                and iTeamStat != 0
                    //grab outfit for mp char
                    DIRECTOR_CHAR_OUTFITS eOutfit  
                    if   iChars = 0
                        eOutfit = DCO_MULTIPLAYER_1 
                    elif iChars = 1 
                        eOutfit = DCO_MULTIPLAYER_2
                    endif
                    
                    MODEL_NAMES ePedmodel = GET_ONLINE_CHARACTER_MODEL(iChars)  
                            
                    if GET_DIRECTOR_CHAR_OUTFIT_DATA(sOutfitsData,eOutfit,ePedmodel)
                        cprintln(debug_director,"(GET_DIRECTOR_CHAR_OUTFIT_DATA) is TRUE for char:",iChars) 
                        sOnlineChar[iChars].outfitdata = sOutfitsData
                        sOnlineChar[iChars].bSetup = true
                    endif
                else
                    sOnlineChar[iChars].bSetup = false
                endif
            else
                sOnlineChar[iChars].bSetup = false
            endif
        else
            sOnlineChar[iChars].bSetup = false
        endif
    endrepeat
    
ENDPROC
PROC REMOVE_SHORTLIST_ENTRY(int iEntry)
    int i,iend
    
    IF iEntry < 10
        iEnd = 9
    ELSE
        iEnd = 19
    ENDIF
    
    for i = iEntry to iEnd-1
        shortlistData[i] = shortlistData[i+1]
    endfor
    shortlistData[iEnd].category = MC_VOID
    shortlistData[iEnd].model = DUMMY_MODEL_FOR_SCRIPT
ENDPROC
PROC DISABLE_FIRST_PERSON_VIEW_ON_RESTRICTED_MODELS_THIS_FRAME()
    BOOL bRestrictedModel = FALSE
    if GET_ENTITY_MODEL(player_ped_id())  != PLAYER_ZERO
    and GET_ENTITY_MODEL(player_ped_id()) != PLAYER_ONE
    and GET_ENTITY_MODEL(player_ped_id()) != PLAYER_TWO
    and GET_ENTITY_MODEL(player_ped_id()) != MP_F_FREEMODE_01
    and GET_ENTITY_MODEL(player_ped_id()) != MP_M_FREEMODE_01
        bRestrictedModel = TRUE
    endif
    
    //If in 1st person, switch to 3rd
    IF bRestrictedModel
        IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
            SET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT(), CAM_VIEW_MODE_THIRD_PERSON_NEAR)
        ENDIF
        DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
        DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
    ENDIF
ENDPROC
PROC LOAD_DATA()
    cprintln(debug_director,"LOAD_DATA()")
    int i,j
    int elementCount=0
    structShortlistData bmk
    bmk=bmk
    
    int iCurrentRemoveTotal = 0
    int iShortlistPedsToRemove[20]

    repeat count_of(shortlistData) i        
                    
        shortlistData[i].category = int_to_enum(enumMenuCategory,g_savedGlobals.sDirectorModeData.shortlistCategories[i])   
        
        //this needs to go outside the if statements below as it was getting skipped by the online check branch.
        //The data is loaded from the global array using the value of elementCount to keep track in where in the global array to load from.
        //but the MC_ONLINE branches below would skip this, so the elementCount wasn't updated as required to keep in sync with the shortlist data index.       
        repeat count_of(bmk.modelVariation) j
            shortlistData[i].modelVariation[j] = GET_PACKED_BITFIELD_VALUE_FROM_ARRAY(g_savedGlobals.sDirectorModeData.iShortlistData, elementCount, 6)
            IF shortlistData[i].modelVariation[j] = 63 shortlistData[i].modelVariation[j] = -1 ENDIF //reloaded data is turning -1 in to 63 as it's stored in a 6-bit value.
            CDEBUG3LN(debug_director," shortlist entry = ",i," modelVariation ",j," = ",shortlistData[i].modelVariation[j])
            elementCount++
        endrepeat
        repeat count_of(bmk.textureVariation) j
            shortlistData[i].textureVariation[j] = GET_PACKED_BITFIELD_VALUE_FROM_ARRAY(g_savedGlobals.sDirectorModeData.iShortlistData, elementCount, 6)
            IF shortlistData[i].textureVariation[j] = 63 shortlistData[i].textureVariation[j] = -1 ENDIF //reloaded data is turning -1 in to 63 as it's stored in a 6-bit value.
            CDEBUG3LN(debug_director," shortlist entry = ",i," textureVariation ",j," = ",shortlistData[i].textureVariation[j])
            elementCount++
        endrepeat
        
        //Do online ped shortlist seperate      
        if shortlistData[i].category = MC_ONLINE1
            if sOnlineChar[0].bSetup
                shortlistData[i].model = sOnlineChar[0].model
                cprintln(debug_director,"LOAD_DATA() online, model = ",shortlistData[i].model," category = ",enum_to_int(shortlistData[i].category))
                if sOnlineChar[0].model = MP_M_FREEMODE_01
                    shortlistData[i].bMale = true
                elif sOnlineChar[0].model = MP_F_FREEMODE_01
                    shortlistData[i].bMale = false
                endif
            else
                cprintln(debug_director,"LOAD_DATA: online char in online1 no longer exists add to remove list: ",i)                
                iShortlistPedsToRemove[iCurrentRemoveTotal] = i
                iCurrentRemoveTotal++
            endif   
            
        elif shortlistData[i].category = MC_ONLINE2
            if sOnlineChar[1].bSetup
                shortlistData[i].model = sOnlineChar[1].model
                cprintln(debug_director,"LOAD_DATA() online, model = ",shortlistData[i].model," category = ",enum_to_int(shortlistData[i].category))
                if sOnlineChar[1].model = MP_M_FREEMODE_01
                    shortlistData[i].bMale = true
                elif sOnlineChar[1].model = MP_F_FREEMODE_01
                    shortlistData[i].bMale = false
                endif
            else
                cprintln(debug_director,"LOAD_DATA: online char in online2 no longer exists add to remove list: ",i)    
                iShortlistPedsToRemove[iCurrentRemoveTotal] = i
                iCurrentRemoveTotal++
            endif
            
        else    // normal ped set up 
        
            
            
            shortlistData[i].bMale = IS_BIT_SET(g_savedGlobals.sDirectorModeData.shortlistSex,i)
            shortlistData[i].model = int_to_enum(MODEL_NAMES,g_savedGlobals.sDirectorModeData.shortlistModels[i])
            
            shortlistData[i].iVoiceHash = g_savedGlobals.sDirectorModeData.shortlistVoice[i]
                
            //this stores the modelVariationIndex.
            IF IS_MODEL_AN_ANIMAL(shortlistData[i].model)
                cprintln(debug_director,"LOAD_DATA() animal, model = ",shortlistData[i].model," category = ",enum_to_int(shortlistData[i].category))
                GET_CATEGORY_MODEL(shortlistData[i].category,ANIMAL,0,shortlistData[i].modelVariationIndex,shortlistData[i].model)          
            ELSE
                IF shortlistData[i].bMale
                    cprintln(debug_director,"LOAD_DATA() male, model = ",shortlistData[i].model," category = ",enum_to_int(shortlistData[i].category))
                    GET_CATEGORY_MODEL(shortlistData[i].category,MALE,0,shortlistData[i].modelVariationIndex,shortlistData[i].model)
                ELSE
                    cprintln(debug_director,"LOAD_DATA() female, model = ",shortlistData[i].model," category = ",enum_to_int(shortlistData[i].category))
                    GET_CATEGORY_MODEL(shortlistData[i].category,FEMALE,0,shortlistData[i].modelVariationIndex,shortlistData[i].model)
                ENDIF
            ENDIF   
        endif   
    endrepeat
                                                    
    //remove duplicate online peds
    int iDupCheck
    REPEAT COUNT_OF(shortlistData) i
        IF shortlistData[i].category = MC_ONLINE1 
        or shortlistData[i].category = MC_ONLINE2
            if i <= 9   // shortlist
                for iDupCheck = 0 to 9
                    if i != iDupCheck
                        IF shortlistData[iDupCheck].category = shortlistData[i].category //if another online shortlist has same category then remove from list
                            IF shortlistData[i].category = MC_ONLINE1
                                cprintln(debug_director,"LOAD_DATA: online 1 char in shortlist twice add to remove list: ",iDupCheck)
                            else
                                cprintln(debug_director,"LOAD_DATA: online 2 char in shortlist twice add to remove list: ",iDupCheck)
                            endif
                            iShortlistPedsToRemove[iCurrentRemoveTotal] = iDupCheck
                            iCurrentRemoveTotal++
                        ENDIF
                    ENDIF
                endfor          
            else        // recently used
                for iDupCheck = 10 to 19
                    if i != iDupCheck
                        IF shortlistData[iDupCheck].category = shortlistData[i].category //if another online shortlist has same category then remove from list
                            IF shortlistData[i].category = MC_ONLINE1
                                cprintln(debug_director,"LOAD_DATA: online 1 char in recently used twice add to remove list: ",iDupCheck)                       
                            else
                                cprintln(debug_director,"LOAD_DATA: online 2 char in recently used twice add to remove list: ",iDupCheck)                       
                            endif
                            iShortlistPedsToRemove[iCurrentRemoveTotal] = iDupCheck
                            iCurrentRemoveTotal++
                        ENDIF
                    ENDIF
                endfor  
            endif           
        ENDIF           
    ENDREPEAT
        
    REPEAT iCurrentRemoveTotal i
        cprintln(debug_director,"LOAD_DATA: Remove shortlist number: ",iShortlistPedsToRemove[i]," [iCurrentRemoveTotal]: ",iCurrentRemoveTotal)
        REMOVE_SHORTLIST_ENTRY(iShortlistPedsToRemove[i])
    ENDREPEAT
    
    vLastPlayerCoord = g_savedGlobals.sDirectorModeData.vSavedMapCoord
    fPlayerHeading = g_savedGlobals.sDirectorModeData.fSavedPlayerHeading
    
    warpLocations[ENUM_TO_INT(DTL_USER_1)].position     = g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[0]
    warpLocations[ENUM_TO_INT(DTL_USER_1)].heading      = g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading[0]
    warpLocations[ENUM_TO_INT(DTL_USER_1)].interior     = g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior[0]
    
    warpLocations[ENUM_TO_INT(DTL_USER_2)].position     = g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[1]
    warpLocations[ENUM_TO_INT(DTL_USER_2)].heading      = g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading[1]
    warpLocations[ENUM_TO_INT(DTL_USER_2)].interior     = g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior[1]

    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    DMPROP_LOAD_SCENES_FROM_GLOBALS()
    #ENDIF
ENDPROC


//Restores nearby/special vehicles on Director cleanup
PROC RESTORE_VEHICLE_ON_EXIT(VEHICLE_INDEX &vehVehicle, VECTOR vCoords)
    IF DOES_ENTITY_EXIST(vehVehicle)
            IF NOT IS_ENTITY_DEAD(vehVehicle)
                //B* 2230000: Vehicle cleanup on repeat play
                IF iLastCleanupCause = FORCE_CLEANUP_FLAG_REPEAT_PLAY
                    //Destroy the vehicle on cleanup
                    SET_VEHICLE_AS_NO_LONGER_NEEDED(vehVehicle)
                    vehVehicle = NULL
                ELSE
                    MODEL_NAMES mVehicle = GET_ENTITY_MODEL(vehVehicle)
                    VECTOR vSafeCoords
                    GET_SAFE_COORD_FOR_DIRECTOR_MODE(vCoords,vSafeCoords,GET_ENTITY_HEADING(vehVehicle),mVehicle)
                    
                    CDEBUG1LN(debug_director,"Director mode cleanup: Restoring vehicle at ",vSafeCoords)
                    if IS_VEHICLE_DRIVEABLE(vehVehicle)
                        SET_ENTITY_COORDS(vehVehicle,vSafeCoords)
                        FREEZE_ENTITY_POSITION(vehVehicle,FALSE)
                        SET_VEHICLE_ON_GROUND_PROPERLY(vehVehicle)
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
ENDPROC


ENUM DirectorCleanupType
    DCT_EARLY,          // A fast cleanup to run if Director Mode didn't finish launching or changing game state.
    DCT_REPEAT,         // A cleanup for repeat plays that has no waits but cleans up all UI/game elements DM may have configured.
    DCT_MULTIPLAYER,    // Cleanup to facilitate clean skycam transition. Leave some stuff for MP init to clean up for us.
    DCT_FULL            // Full aggressive clean up when dropping back to story mode with a fade.
ENDENUM


ENUM PLAYER_WARNING_STATE_SAFE
    PWSS_SAFE_FOR_CAPTURE_COORDS,
    PWSS_SAFE_FOR_SHORTLIST_SWAP,
    PWSS_SAFE_FOR_DIALOGUE,
    PWSS_SAFE_FOR_GESTURE,
    PWSS_SAFE_FOR_WARP_SET,
    PWSS_SAFE_FOR_WARPING,
    PWSS_SAFE_FOR_VEHICLE,
    PWSS_SAFE_FOR_PROPEDITOR
ENDENUM


INT lastWarningStateBitSet
INT currentWarningStateBitSet
INT exitWarningStateBitSet

CONST_INT       BIT_PWS_SAFE                    0
CONST_INT       BIT_PWS_INCAR                   1
CONST_INT       BIT_PWS_IS_PASSENGER            2
CONST_INT       BIT_PWS_INWATER                 3
CONST_INT       BIT_PWS_INDEEPWATER             4
CONST_INT       BIT_PWS_INAIR                   5
CONST_INT       BIT_PWS_ONVEHICLE               6
CONST_INT       BIT_PWS_ARRESTING               7
CONST_INT       BIT_PWS_AS_ANIMAL               8
CONST_INT       BIT_PWS_RECORDING_IN_PROGRESS   9
CONST_INT       BIT_PWS_IN_TRAIN                10
CONST_INT       BIT_PWS_IN_PRISON               11
CONST_INT       BIT_PWS_WANTED                  12
CONST_INT       BIT_PWS_INCOVER                 13
CONST_INT       BIT_PWS_DEAD                    14
CONST_INT       BIT_PWS_IN_BOAT                 15

#IF IS_DEBUG_BUILD
    PROC DEBUG_PRINT_WARNING_STATE(INT warningStateBitSet)
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_SAFE)
            cprintln(debug_director,"BIT_PWS_SAFE = TRUE")  
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_INCAR)
            cprintln(debug_director,"BIT_PWS_INCAR = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_INWATER)
            cprintln(debug_director,"BIT_PWS_INWATER = TRUE")   
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_INDEEPWATER)
            cprintln(debug_director,"BIT_PWS_INDEEPWATER = TRUE")   
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_INAIR)
            cprintln(debug_director,"BIT_PWS_INAIR = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_ONVEHICLE)
            cprintln(debug_director,"BIT_PWS_ONVEHICLE = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_ARRESTING)
            cprintln(debug_director,"BIT_PWS_ARRESTING = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_AS_ANIMAL)
            cprintln(debug_director,"BIT_PWS_AS_ANIMAL = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_RECORDING_IN_PROGRESS)
            cprintln(debug_director,"BIT_PWS_RECORDING_IN_PROGRESS = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_IN_TRAIN)
            cprintln(debug_director,"BIT_PWS_IN_TRAIN = TRUE")  
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_IN_PRISON)
            cprintln(debug_director,"BIT_PWS_IN_PRISON = TRUE") 
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_WANTED)
            cprintln(debug_director,"BIT_PWS_WANTED = TRUE")    
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_INCOVER)
            cprintln(debug_director,"BIT_PWS_INCOVER = TRUE")   
        ENDIF
        IF IS_BIT_SET(warningStateBitSet,BIT_PWS_DEAD)
            cprintln(debug_director,"BIT_PWS_DEAD = TRUE")  
        ENDIF
         IF IS_BIT_SET(warningStateBitSet,BIT_PWS_IN_BOAT)
            cprintln(debug_director,"BIT_PWS_IN_BOAT = TRUE")  
        ENDIF
    ENDPROC
#ENDIF

FUNC BOOL IS_PLAYER_MODEL_SAFE_FOR_GETURES()
    IF NOT IS_PED_INJURED(player_ped_ID())
        SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
            CASE S_M_M_MOVALIEN_01
            CASE U_M_Y_POGO_01
            CASE U_M_Y_RSRANGER_01
            CASE A_M_M_GENFAT_01
            CASE A_M_M_GENFAT_02
            CASE A_M_M_FATLATIN_01
            CASE A_F_M_FATWHITE_01
            CASE A_F_M_FATBLA_01
            CASE A_F_M_FATCULT_01
                RETURN FALSE
            BREAK
            DEFAULT
                RETURN TRUE
            BREAK
        ENDSWITCH
    ENDIF
    RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_MODEL_HOLD_WEAPONS(model_names playerModel = DUMMY_MODEL_FOR_SCRIPT)
    
    IF playerModel = DUMMY_MODEL_FOR_SCRIPT
        playerModel = GET_ENTITY_MODEL(player_ped_id())
    ENDIF
    
    IF (Is_Model_A_Playable_Animal(playerModel) AND playerModel != IG_ORLEANS)
    OR playerModel = S_M_M_MOVALIEN_01
    OR playerModel = U_M_Y_POGO_01
    OR playerModel = U_M_Y_RSRanger_01
    Or playerModel = S_M_M_MovSpace_01
        RETURN FALSE
    ENDIF
    
    RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_CATEGORY_VALID_FOR_WEAPONS(enumMenuCategory thisCategory)
    int iTotal
    IF !CAN_PLAYER_MODEL_HOLD_WEAPONS(GET_CATEGORY_MODEL(thisCategory,MALE,0,iTotal))
        RETURN FALSE
    ENDIF
    RETURN TRUE
ENDFUNC
        
FUNC BOOL IS_PED_CATEGORY_VALID_FOR_VEHICLE(enumMenuCategory thisCategory)
    SWITCH thisCategory
        CASE MC_COSPOG
        FALLTHRU
        CASE MC_SPEMAN
        FALLTHRU
        CASE MC_COSSPA
        FALLTHRU
        CASE MC_COSALI
        FALLTHRU
        CASE MC_COSAST
        FALLTHRU
        CASE MC_PRODRA
            RETURN FALSE
        BREAK       
    ENDSWITCH
    
//  IF thisCategory > MC_ANIMALS_START
//  AND thisCategory < MC_ANIMALS_END
//      RETURN FALSE
//  ENDIF
    
    RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_BALLISTIC_ARMOUR()
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE HC_GUNMAN
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2) = 1
                RETURN TRUE
            ENDIF
        BREAK

        CASE PLAYER_ZERO //michael
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 5
                RETURN TRUE
            ENDIF
        BREAK

        CASE PLAYER_TWO //Trevor
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 2
                RETURN TRUE
            ENDIF
        BREAK
        
    ENDSWITCH

    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_VALID_FOR_VEHICLE(ped_index pedID)
    IF NOT IS_PED_INJURED(pedID)
        IF IS_PLAYER_WEARING_BALLISTIC_ARMOUR()
            cprintln(debug_director,"IS_PED_VALID_FOR_VEHICLE() Ballistic arm FALSE")
            RETURN FALSE
        ENDIF
        SWITCH GET_ENTITY_MODEL(pedID)
            CASE U_M_Y_POGO_01
            FALLTHRU
            CASE U_M_Y_MANI
            FALLTHRU
            CASE S_M_M_MOVSPACE_01
            FALLTHRU
            CASE U_M_Y_RSRANGER_01
            FALLTHRU
            CASE S_M_M_MOVALIEN_01
            FALLTHRU
            CASE A_M_M_TranVest_01
            
                RETURN FALSE
            BREAK
        ENDSWITCH
    ENDIF

    RETURN TRUE
ENDFUNC





FUNC BOOL Is_Vehicle_Okay_For_Prop_Editor(VEHICLE_INDEX PassedVehicle)

    IF IS_THIS_MODEL_A_CAR (GET_ENTITY_MODEL (PassedVehicle))  //Allow cars in Prop Editor
    OR IS_THIS_MODEL_A_BIKE (GET_ENTITY_MODEL (PassedVehicle)) //Allow motorbikes in Prop Editor
    OR IS_THIS_MODEL_A_BOAT (GET_ENTITY_MODEL (PassedVehicle)) //Allow boats in Prop Editor


        IF GET_ENTITY_MODEL(PassedVehicle) = RHINO //2507540
        OR GET_ENTITY_MODEL(PassedVehicle) = TAXI  //2506955

            RETURN FALSE

        ENDIF               

        IF IS_THIS_MODEL_A_BICYCLE (GET_ENTITY_MODEL (PassedVehicle))
            
            RETURN FALSE

        ENDIF 

    ELSE

        RETURN FALSE

    ENDIF


    RETURN TRUE

ENDFUNC







FUNC BOOL IS_PLAYER_WARNING_STATE_SAFE(PLAYER_WARNING_STATE_SAFE safeForCheck)

    // Modified this function so you now only add fail cases to the below, otherwise this function will default to TRUE (ie the warning check returns "Safe").
    SWITCH safeForCheck
        CASE PWSS_SAFE_FOR_CAPTURE_COORDS
        
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ONVEHICLE) 
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_TRAIN)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_PRISON)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCOVER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_WANTED)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
                RETURN FALSE
            ENDIF           
            
        BREAK
        CASE PWSS_SAFE_FOR_SHORTLIST_SWAP
        
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCAR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_RECORDING_IN_PROGRESS)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_TRAIN)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_PRISON)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
                RETURN FALSE
            ENDIF   
            
        BREAK
        CASE PWSS_SAFE_FOR_DIALOGUE
        
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            //OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER) //this should be ok? Commenting out for now.
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
                RETURN FALSE
            ENDIF
            
        BREAK
        CASE PWSS_SAFE_FOR_GESTURE
        
            IF NOT IS_PLAYER_MODEL_SAFE_FOR_GETURES()
                RETURN FALSE
            ENDIF

            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCAR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_TRAIN)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCOVER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
                RETURN FALSE
            ENDIF
            
        BREAK
        CASE PWSS_SAFE_FOR_WARP_SET
        
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCAR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ONVEHICLE)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_TRAIN)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INCOVER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_WANTED)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_RECORDING_IN_PROGRESS)
                RETURN FALSE
            ENDIF
            
        BREAK
        CASE PWSS_SAFE_FOR_WARPING
        
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_RECORDING_IN_PROGRESS)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IS_PASSENGER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_BOAT)
                RETURN FALSE
            ENDIF
            
        BREAK
        CASE PWSS_SAFE_FOR_VEHICLE
            IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
            OR NOT IS_PED_VALID_FOR_VEHICLE(PLAYER_PED_ID())
                RETURN FALSE
            ENDIF
        BREAK

        CASE PWSS_SAFE_FOR_PROPEDITOR

            IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))

                VEHICLE_INDEX temp_EntryVehicleIndex

                temp_EntryVehicleIndex = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
                
                IF Is_Vehicle_Okay_For_Prop_Editor(temp_EntryVehicleIndex) = FALSE
      
                    RETURN FALSE  //If the player has begun entering *any* blacklisted vehicle, prevent the launch of the prop editor for extra safety. This is 
                
                ENDIF
 
            ENDIF
            
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)

                VEHICLE_INDEX temp_VehicleIndex
                temp_VehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID(), TRUE)

                IF Is_Vehicle_Okay_For_Prop_Editor(temp_VehicleIndex) = FALSE
  
                    RETURN FALSE  //If the player is in any blacklisted vehicle, return false. 
                                   
                ENDIF

            ENDIF


            IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID())) //Not letting the player use the Scene Creator as a Peyote transformation or an animal actor.
                 RETURN FALSE
            ENDIF


            IF (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) > PPS_INVALID) //This means the player is in a valid parachute state. Don't allow launch of Scene Creator
                RETURN FALSE
            ENDIF


          BREAK


    ENDSWITCH
    RETURN TRUE
ENDFUNC


PROC FIX_SCROLLING_TOP_ITEM(INT iCurMenuItem, INT iOldTop, INT iRows = 9)
    //hack to fix awkard scrolling issues because this section needed a "REBUILD_MENU" call which forces the list to scroll up/down when we only want the cursor to scrool.
    
    IF iCurMenuItem - iOldTop > iRows
        SET_TOP_MENU_ITEM(iCurMenuItem - iRows)
    ELSE
        IF iCurMenuItem < iOldTop
            SET_TOP_MENU_ITEM(iCurMenuItem)
        ELSE
            IF GET_TOP_MENU_ITEM() != iOldTop
                SET_TOP_MENU_ITEM(iOldTop)
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC CONTROL_RETURN_MENU_TOP_MENU_ITEM()
    INT iTopItem = 0
    IF sPI_MenuData.iCurrentSelection > c_iMAX_PI_MENU_OPTIONS - 1
        iTopItem = sPI_MenuData.iCurrentSelection - ( c_iMAX_PI_MENU_OPTIONS - 1 )
    ENDIF
    SET_TOP_MENU_ITEM( iTopItem )
ENDPROC

PROC SET_MENU_CURRENT_POSITION(INT iCurrent, INT iTop = -1)
    sPI_MenuData.iPreviousSelection = sPI_MenuData.iCurrentSelection
    sPI_MenuData.iCurrentSelection = iCurrent
    IF iCurrent < GET_TOP_MENU_ITEM()
        SET_TOP_MENU_ITEM(iCurrent)
    ENDIF
    IF iTop >= 0
        SET_TOP_MENU_ITEM(iTop)
    ENDIF
ENDPROC

PROC RESET_PI_MENU_NOW()
    bResetMenuNow = TRUE
//  CASSERTLN(debug_dan,"Reset menu called from")
ENDPROC

FUNC INT GET_MINI_MAP_LOCK_ANGLE_FROM_NORTH( VECTOR vCoords )

    INT iAngle = GET_ANGLE_OF_NORTH_BLIP_FROM_COORDS( vCoords )
    
    iAngle += 8     //  Hacky fix to make sure north is straight up, for some reason :/ it is 8 degrees out...
    
    //  Make to between 0 - 360 and negate.
    IF iAngle < 0
        iAngle *= -1
    ELSE
        iAngle = 360 - iAngle
    ENDIF
    
    //  Should never hit here, but added as absolute failsafe.
    IF iAngle > 360
        WHILE iAngle > 360
            iAngle -= 360
        ENDWHILE
    ELIF iAngle < 0
        WHILE iAngle < 0
            iAngle += 360
        ENDWHILE
    ENDIF
    RETURN iAngle
ENDFUNC

BOOL bPIMenuMiniMapSetup = FALSE

/// PURPOSE:
///    Cleans up and restores the map from ther location selector in the DM PI menu.
PROC CLEANUP_PI_MENU_MINI_MAP( BOOL bCleanBlips = TRUE )
    
    IF bCleanBlips
        CDEBUG1LN(DEBUG_DIRECTOR, " - CLEANUP_PI_MENU_MINI_MAP - Cleaning up the minimap from the PI menu...")
    ENDIF
    
    IF IS_MODEL_AN_ANIMAL( GET_PLAYER_MODEL() )
        DISPLAY_RADAR( FALSE )
    ENDIF
    SET_BIGMAP_ACTIVE( FALSE, FALSE )
    SET_BLIP_ALPHA( GET_NORTH_BLID_INDEX(), 255 )
    UNLOCK_MINIMAP_POSITION()
    UNLOCK_MINIMAP_ANGLE()
    THEFEED_SHOW()
    
    IF bCleanBlips
        INT iLocationIndex 
        // FOR iLocationIndex = 0 TO ( COUNT_OF( DirectorTravelLocation ) - 3 )
        FOR iLocationIndex = 0 TO ENUM_TO_INT( MAX_DTL ) - 1 
            IF DOES_BLIP_EXIST( warpLocations[ iLocationIndex ].blipIndex )
                REMOVE_BLIP( warpLocations[ iLocationIndex ].blipIndex )
            ENDIF
        ENDFOR
    ENDIF
    
    bPIMenuMiniMapSetup = FALSE
ENDPROC

/// PURPOSE:
///    Setups up the minimap and blips ready for the location selection
PROC SETUP_PI_MENU_MINI_MAP()

    IF iSubMenu = PI_SUBMENU_NONE
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION)
            IF NOT IS_REPLAY_RECORDING()
            AND NOT IS_REPLAY_BEING_SET_UP()
            AND NOT IS_REPLAY_BEING_PROCESSED()
                IF IS_PLAYER_WARNING_STATE_SAFE( PWSS_SAFE_FOR_WARPING )
                
                    CDEBUG1LN(DEBUG_DIRECTOR, " - SETUP_PI_MENU_MINI_MAP - Setting up the mini map for the PI menu...")
                    DISPLAY_RADAR( TRUE )
                    SET_BIGMAP_ACTIVE( TRUE, FALSE )
                    SET_BLIP_ALPHA( GET_NORTH_BLID_INDEX(), 0 )
                    THEFEED_HIDE()

                    INT iLocationIndex
                    //FOR iLocationIndex = 0 TO ( COUNT_OF( DirectorTravelLocation ) - 3 )
                    FOR iLocationIndex = 0 TO ENUM_TO_INT( MAX_DTL ) - 1 
                        #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                        IF INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) != DTL_WAYPOINT
                        #ENDIF
                            IF NOT DOES_BLIP_EXIST( warpLocations[ iLocationIndex ].blipIndex )
                            AND NOT IS_VECTOR_ZERO( warpLocations[ iLocationIndex ].position )
                                warpLocations[ iLocationIndex ].blipIndex = CREATE_BLIP_FOR_COORD( warpLocations[ iLocationIndex ].position )
                                SET_BLIP_SCALE( warpLocations[ iLocationIndex ].blipIndex, 0.66 )
                                SET_BLIP_ALPHA( warpLocations[ iLocationIndex ].blipIndex, 156 )
                                IF INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) = DTL_USER_1
                                OR INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) = DTL_USER_2
                                    SET_BLIP_COLOUR( warpLocations[ iLocationIndex ].blipIndex, BLIP_COLOUR_GREEN )
                                ELSE
                                    IF IS_DIRECTOR_LOCATION_UNLOCKED( INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) )
                                        SHOW_TICK_ON_BLIP( warpLocations[ iLocationIndex ].blipIndex, TRUE )
                                    ELSE
                                        SHOW_TICK_ON_BLIP( warpLocations[ iLocationIndex ].blipIndex, FALSE )
                                    ENDIF
                                ENDIF
                                SET_BLIP_AS_SHORT_RANGE( warpLocations[ iLocationIndex ].blipIndex, TRUE )
                                SET_BLIP_NAME_FROM_TEXT_FILE( warpLocations[ iLocationIndex ].blipIndex, warpLocations[ iLocationIndex ].tlLocName )
                                SHOW_HEIGHT_ON_BLIP( warpLocations[ iLocationIndex ].blipIndex, FALSE )
                            ENDIF
                        #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                        ENDIF
                        #ENDIF
                    ENDFOR
                    
                    INT iSelectedLocation = iLocationSelected[ selectedPI[ PI_M0_LOCATION ] ]
                    VECTOR vSelectedMapLocation = warpLocations[ iSelectedLocation ].position
                    
                    IF DOES_BLIP_EXIST(warpLocations[ iSelectedLocation ].blipIndex)
                        SET_BLIP_SCALE( warpLocations[ iSelectedLocation ].blipIndex, 1.0 )
                        SET_BLIP_ALPHA( warpLocations[ iSelectedLocation ].blipIndex, 255 )
                    ENDIF
                    IF IS_VECTOR_ZERO( vSelectedMapLocation )
                        VECTOR vPedCoords = GET_ENTITY_COORDS( PLAYER_PED_ID() )
                        LOCK_MINIMAP_POSITION( vPedCoords.x, vPedCoords.y )
                    ELSE
                        LOCK_MINIMAP_POSITION( vSelectedMapLocation.x, vSelectedMapLocation.y )
                        LOCK_MINIMAP_ANGLE( GET_MINI_MAP_LOCK_ANGLE_FROM_NORTH( vSelectedMapLocation ) )
                    ENDIF
                    
                    bPIMenuMiniMapSetup = TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDIF

ENDPROC

/// PURPOSE:
///    Per frame update of the minimap blips
PROC UPDATE_PI_MENU_MINI_MAP_PER_FRAME()

    IF iSubMenu = PI_SUBMENU_NONE 
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION)
        
            IF NOT IS_PLAYER_WARNING_STATE_SAFE( PWSS_SAFE_FOR_WARPING )
            OR IS_REPLAY_RECORDING()
                IF bPIMenuMiniMapSetup
                    CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_PER_FRAME - Cleaning up the mini map for the PI menu as location disabled...")
                    CLEANUP_PI_MENU_MINI_MAP()
                ELSE
                    CLEANUP_PI_MENU_MINI_MAP( FALSE )
                ENDIF
            ELSE
                IF NOT bPIMenuMiniMapSetup
                    IF NOT IS_REPLAY_RECORDING()
                        IF IS_PLAYER_WARNING_STATE_SAFE( PWSS_SAFE_FOR_WARPING )
                            CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_PER_FRAME - Setting up the mini map for the PI menu...")
                            SETUP_PI_MENU_MINI_MAP()
                        ENDIF
                    ENDIF
                ENDIF   
            
                INT iLocationIndex
                IF iLocationSelected[ selectedPI[ PI_M0_LOCATION ] ] = ENUM_TO_INT( DTL_USER_1 )
                OR iLocationSelected[ selectedPI[ PI_M0_LOCATION ] ] = ENUM_TO_INT( DTL_USER_2 )
                    iLocationIndex = iLocationSelected[ selectedPI[ PI_M0_LOCATION ] ]
                    VECTOR vSelectedMapLocation = warpLocations[ iLocationIndex ].position
                    
                    IF NOT DOES_BLIP_EXIST( warpLocations[ iLocationIndex ].blipIndex )
                    AND NOT IS_VECTOR_ZERO( warpLocations[ iLocationIndex ].position )
                        warpLocations[ iLocationIndex ].blipIndex = CREATE_BLIP_FOR_COORD( warpLocations[ iLocationIndex ].position )
                        SET_BLIP_AS_SHORT_RANGE( warpLocations[ iLocationIndex ].blipIndex, TRUE )
                        SET_BLIP_NAME_FROM_TEXT_FILE( warpLocations[ iLocationIndex ].blipIndex, PRIVATE_Get_Director_Travel_Location_Name_Label( INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) ) )
                        SET_BLIP_COLOUR( warpLocations[ iLocationIndex ].blipIndex, BLIP_COLOUR_GREEN )
                        SHOW_HEIGHT_ON_BLIP( warpLocations[ iLocationIndex ].blipIndex, FALSE )
                        LOCK_MINIMAP_POSITION( vSelectedMapLocation.x, vSelectedMapLocation.y )
                        LOCK_MINIMAP_ANGLE( GET_MINI_MAP_LOCK_ANGLE_FROM_NORTH( vSelectedMapLocation ) )
                    ENDIF
                    IF DOES_BLIP_EXIST( warpLocations[ iLocationIndex ].blipIndex )
                        IF NOT ARE_VECTORS_EQUAL( GET_BLIP_COORDS( warpLocations[ iLocationIndex ].blipIndex ), warpLocations[ iLocationIndex ].position ) 
                            CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_PER_FRAME - Resetting the blip for: ", PRIVATE_Get_Director_Travel_Location_String( INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) ) )
                            SET_BLIP_COORDS( warpLocations[ iLocationIndex ].blipIndex, warpLocations[ iLocationIndex ].position )
                            LOCK_MINIMAP_POSITION( vSelectedMapLocation.x, vSelectedMapLocation.y )
                            LOCK_MINIMAP_ANGLE( GET_MINI_MAP_LOCK_ANGLE_FROM_NORTH( vSelectedMapLocation ) )
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF

ENDPROC

/// PURPOSE:
///    Update the mini map per button press
PROC UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE()
            
    IF iSubMenu = PI_SUBMENU_NONE AND sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION)
    
        IF NOT IS_PLAYER_WARNING_STATE_SAFE( PWSS_SAFE_FOR_WARPING )
        OR IS_REPLAY_RECORDING()
            IF bPIMenuMiniMapSetup
                CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE - Cleaning up the mini map for the PI menu as location disabled...")
                CLEANUP_PI_MENU_MINI_MAP()
            ENDIF
        ELSE
            IF NOT bPIMenuMiniMapSetup
                IF NOT IS_REPLAY_RECORDING()
                    IF IS_PLAYER_WARNING_STATE_SAFE( PWSS_SAFE_FOR_WARPING )
                        CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE - Setting up the mini map for the PI menu...")
                        SETUP_PI_MENU_MINI_MAP()
                    ENDIF
                ENDIF
            ENDIF   
            
            CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE - Updating the mini map for the PI menu...")
            
            INT iTravelEntry = iLocationSelected[ selectedPI[ PI_M0_LOCATION ] ]
            VECTOR vSelectedMapLocation = warpLocations[ iTravelEntry ].position
            
            INT iLocationIndex
            FOR iLocationIndex = 0 TO ENUM_TO_INT( MAX_DTL ) - 1 
                #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                IF INT_TO_ENUM( DirectorTravelLocation, iLocationIndex ) != DTL_WAYPOINT
                #ENDIF
                    IF DOES_BLIP_EXIST( warpLocations[ iLocationIndex ].blipIndex )
                        IF iLocationIndex = iTravelEntry
                            SET_BLIP_SCALE( warpLocations[ iLocationIndex ].blipIndex, 1.0 )
                            SET_BLIP_ALPHA( warpLocations[ iLocationIndex ].blipIndex, 255 )
                        ELSE
                            SET_BLIP_SCALE( warpLocations[ iLocationIndex ].blipIndex, 0.66 )
                            SET_BLIP_ALPHA( warpLocations[ iLocationIndex ].blipIndex, 156 )
                        ENDIF
                    ENDIF
                #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                ENDIF
                #ENDIF
            ENDFOR
            
            IF !( IS_VECTOR_ZERO(vSelectedMapLocation ) )
                LOCK_MINIMAP_POSITION( vSelectedMapLocation.x, vSelectedMapLocation.y )
                LOCK_MINIMAP_ANGLE( GET_MINI_MAP_LOCK_ANGLE_FROM_NORTH( vSelectedMapLocation ) )
            ELSE
                UNLOCK_MINIMAP_POSITION()
                UNLOCK_MINIMAP_ANGLE()
            ENDIF
        ENDIF
    ELIF iSubMenu = PI_SUBMENU_NONE AND sPI_MenuData.iPreviousSelection = ENUM_TO_INT(PI_M0_LOCATION)
        CDEBUG1LN(DEBUG_DIRECTOR, " - UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE - Calling cleanup on the mini map for the PI menu...")
        CLEANUP_PI_MENU_MINI_MAP()
    ENDIF

ENDPROC

#IF FEATURE_SP_DLC_DM_PROP_EDITOR
PROC SWAP_PROP_SETTINGS_MENU_DATA()
    INT i, temp
    FOR i = 0 to SEM_TOTAL - 1
            temp = settingsMenu[i]
            settingsMenu[i] = propSettingsMenu[i]
            propSettingsMenu[i] = temp
    ENDFOR
ENDPROC
#ENDIF

PROC REMOVE_DM_WAYPOINT()
    SET_WAYPOINT_OFF()
    iWaypointLocIndex = -1
ENDPROC

PROC RUN_SHARED_CLEANUP()
	IF IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
		CPRINTLN(DEBUG_DIRECTOR,"Detected dead player ped on cleanup. Reviving locally")
		RESURRECT_PED(PLAYER_PED_ID())
		SET_ENTITY_HEALTH(PLAYER_PED_ID(),GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))               
		IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
			CPRINTLN(DEBUG_DIRECTOR,"Dead player is an animal. Moving to high altitude to avoid water death")
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE) + <<0,0,200>>, FALSE)
		ENDIF
	ENDIF

    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Remove the prop editor settings
    CLEANUP_DM_PROPS()
    IF bInPropEdit
        SWAP_PROP_SETTINGS_MENU_DATA()
    ENDIF
	
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
	DMPROP_TOPDOWN_CAMERA_TOGGLE(FALSE)
	#ENDIF
	
    #ENDIF

    RESTORE_AUTO_DOORS_STATES(doorArray, bDoorDataMissing)
    SAVE_DATA()
    
    //Cancel rain override
    SET_RAIN(-1.0)
    SET_AUDIO_FLAG("LoadMPData", FALSE)
    SET_AUDIO_FLAG("IsDirectorModeActive",FALSE)
    SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
    
    //Unpause the clock
    PAUSE_CLOCK(FALSE)
    
    Deactivate_Animal_Restricted_State()
    
    STOP_ALL_ALARMS(TRUE)
            
    REMOVE_ANIM_DICT("director@character_select_intro@male")
    REMOVE_ANIM_DICT("director@character_select_intro@female")
    REMOVE_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
    REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
    
    //Make sure the casting trailer is cleaned up.
    IF IS_IPL_ACTIVE("Replay")
        REMOVE_IPL("Replay")
    ENDIF

    //Reset changes made in script
    Set_Leave_Area_Flag_For_All_Blipped_Missions()
    
    //Reset tweaked interiors
    RESET_TREVORS_APARTMENT()   //Reset Trevor's apartment look (windows, police tape) to what it was
    RESET_SHOWROOM_BOARDED()
    
    //Cleanup camera
    IF DOES_CAM_EXIST(dir_Camera)
        DESTROY_CAM(dir_Camera)
    ENDIF
    RENDER_SCRIPT_CAMS(FALSE,FALSE)
    
    SET_STATIC_EMITTER_ENABLED("SE_DMOD_Trailer_Radio",false)
    SET_AMBIENT_ZONE_STATE("AZ_DMOD_TRAILER_01",false,true)
    SET_PED_POPULATION_BUDGET(3)
    SET_VEHICLE_POPULATION_BUDGET(3)
    
    STOP_AUDIO_SCENE("Director_Mode_No_Cars_Scene") 
    SET_AMBIENT_ZONE_LIST_STATE("AZL_VEHICLE_RADIO_EMITTER_ZONES",TRUE,TRUE)
    
    SET_MINIMAP_FOW_DO_NOT_UPDATE(FALSE)
    SET_MINIMAP_HIDE_FOW(FALSE)
    
    SET_WANTED_LEVEL_MULTIPLIER(1.0)
    SET_MAX_WANTED_LEVEL(5)
    CLEAR_PLAYER_WANTED_LEVEL(player_id())
    REMOVE_ALL_SHOCKING_EVENTS(FALSE)
    DISABLE_CELLPHONE(FALSE)

    //Restricted areas
    ENABLE_ALL_RESTRICTED_AREAS()
    AREA_CHECK_AREAS area
    FOR area = AC_GOLF_COURSE to AC_DOWNTOWN_POLICE
        RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(area,FALSE)
    ENDFOR
    g_bDisableArmyBase = FALSE  //Army base uses a special global

    //Enable stunt jumps
    SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
    
    //cleanup audio
    UNLOAD_ANIMAL_AUDIO_AND_ANIM(TRUE)
    
    //Cleanup menu/scaleform
    bMenuAssetsLoaded = FALSE
    CLEANUP_MENU_ASSETS()
    CLEANUP_PI_MENU_MINI_MAP()
    SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMenu)
        
    //Release mission flag
    IF iMissionLaunchID != NO_CANDIDATE_ID
        Mission_Over(iMissionLaunchID)                  // Release our "on-mission" state so other scripts can run again.
    ENDIF
    
    ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_TALK)
    ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_DUCK)
    ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
    ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_NEXT_RADIO)
    SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(),TRUE)
    SET_PLAYER_IS_IN_DIRECTOR_MODE(FALSE)
    SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(PLAYER_ID(),TRUE)
    DISABLE_TAXI_HAILING(FALSE) 
        
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DontBlipCop,FALSE)
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnorePedTypeForIsFriendlyWith,FALSE)
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_PreventAllMeleeTaunts,FALSE)
    ENDIF
    
    SET_ENABLE_SCUBA(PLAYER_PED_ID(),FALSE)
    SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), -1.0)
    SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
    
    SET_AMBIENT_PEDS_DROP_MONEY(TRUE)
    
    //Waypoints
    SET_WAYPOINT_OFF()  //Cancel any waypoint created by DM
    
    Quit_Drunk_Camera_Immediately() 
             
    //Reset distant cars / random trains
    SET_ALL_VEHICLE_GENERATORS_ACTIVE()
    //SET_ROADS_IN_AREA(<<-10000,-10000,-200>>,<<10000,10000,1000>>,TRUE) //2284564
    SET_ROADS_BACK_TO_ORIGINAL(<<-10000,-10000,-200>>,<<10000,10000,1000>>)
    SET_DISTANT_CARS_ENABLED(TRUE)
    DISABLE_VEHICLE_DISTANTLIGHTS(FALSE)
    SET_RANDOM_TRAINS(TRUE)
    SET_RANDOM_BOATS(TRUE)
    
    //Ensure the faked pause menu is cleaned up.
    SET_FRONTEND_ACTIVE(FALSE)
    SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE) 
    SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
    
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), TRUE)
    ENDIF
    
    //Set the camera view mode back to what it used to be
    SET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT(), cvm_StartUp)
    
    //B* 2503385: Release the snacks audio bank
    RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS")

    SET_DEFAULT_RICH_PRESENCE_FOR_SP()
    
ENDPROC


PROC SCRIPT_CLEANUP(DirectorCleanupType eCleanupType, BOOL bSkipFadeIn = FALSE)
    
	PRINTLN("Director SCRIPT_CLEANUP type: ", eCleanupType)
	
	INT iTries = 0 // iMaxTries = 100
    VECTOR vVehGenMin, vVehGenMax
    
    //confirm all cheats are 'deactivated' (NOT ON) and 'enabled' (ALLOWED TO START)
    DEACTIVATE_DIRECTOR_CHEATS()
    ENABLE_DIRECTOR_MODE_CHEATS()

    SWITCH eCleanupType
    
        // A fast cleanup to run if Director Mode didn't finish launching or changing game state.
        CASE DCT_EARLY
            CPRINTLN(DEBUG_DIRECTOR, "Performing Director Mode EARLY clean-up.")
            
            // Release our "on-mission" state so other scripts can run again.
            IF iMissionLaunchID != NO_CANDIDATE_ID
                Mission_Over(iMissionLaunchID)                  
            ENDIF
    
            // These get set during the script at various points, so resetting here for safety.
            SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS(PLAYER_ID(), FALSE)
            SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
            SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
            
            SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
            SAVE_DATA()
        BREAK
        
        // A cleanup for repeat plays/benchmark/R* editor that has no frame delays but cleans up all 
        // UI and game elements DM may have configured.
        CASE DCT_REPEAT
            CPRINTLN(DEBUG_DIRECTOR, "Performing Director Mode REPEAT clean-up.")
        
            //We don't need to restore gamestate as repeat plays will always load
            //a save once they're done.
        
            RUN_SHARED_CLEANUP()
            
            DO_SCREEN_FADE_OUT(0)
        BREAK
        
        
        // Cleanup to facilitate clean skycam transition. Leave some stuff for MP init to clean up for us.
        CASE DCT_MULTIPLAYER
            CPRINTLN(DEBUG_DIRECTOR, "Performing Director Mode MULTIPLAYER clean-up.")
            
            RESTORE_DIRECTOR_MODE_STARTING_GAMESTATE(TRUE)
            
            RUN_SHARED_CLEANUP()
			
			//url:bugstar:5462611 - Don't need to clear normal invincibility, MP scripts already doing that.
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS(PLAYER_ID(), FALSE)
            
            //Reset player's health to original value
            TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
            WAIT(10)    //Wait for it to take effect
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                SET_ENTITY_HEALTH(PLAYER_PED_ID(), iPlayerHealthOnStart)
            ENDIF
            WAIT(10)    //Toggle it back on the next frame
            TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
        BREAK
        
        
        // Full aggressive clean up when dropping back to story mode with a fade.
        CASE DCT_FULL
            CPRINTLN(DEBUG_DIRECTOR, "Performing Director Mode FULL clean-up.")
            
            //moved for bug 2352887
            //Leave PLAYER_IS_IN_ANIMAL_FORM set if we want multiplayer to swap the character
            //for us. Makes the switch into MP cleaner.
            SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
            
            RUN_SHARED_CLEANUP()
            
            RESTORE_DIRECTOR_MODE_STARTING_GAMESTATE()
            FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
            
            ANIMPOSTFX_STOP_ALL()
            
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
                VEHICLE_INDEX veh
                veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
                CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
                SET_ENTITY_AS_MISSION_ENTITY(veh)
                DELETE_VEHICLE(veh)
                SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
            ENDIF
            IF DOES_ENTITY_EXIST(VehCurrentPersonal)        
                SAFE_DELETE_VEHICLE(VehCurrentPersonal) 
            ENDIF
            IF DOES_BLIP_EXIST(blipVehPersonal)
                REMOVE_BLIP(blipVehPersonal)
            ENDIF
            
            //scuba resets
            SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_ID(),true)    
            if IS_USING_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
                CLEAR_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
            ENDIF
            
            SCENARIO_BLOCKING_INDEX vehgenBlock

            IF NOT IS_VECTOR_ZERO(vLaunchCoords)
                ADJUST_RETURN_COORDS() //Adjust return coordinates 
                            
//              //Delete and block code vehicle gens ahead of warp.
                vVehGenMin = vLaunchCoords - <<20,20,20>>
                vVehGenMax = vLaunchCoords + <<20,20,20>>

                REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vVehGenMin,vVehGenMax)
                vehgenBlock = ADD_SCENARIO_BLOCKING_AREA(vVehGenMin, vVehGenMax)
                                
                STOP_FIRE_IN_RANGE(vLaunchCoords, 200.0)
                CLEAR_AREA(vLaunchCoords, 200.0,true) //ensure projectiles azzznd fires removed
                
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    //Load vehicle gens early in the target area
                    SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vLaunchCoords + <<0.0, 0.0, 5.0>>)
                    
                    //Load in vehicle gens around the player after the warp.
                    iTries = GET_GAME_TIMER()
                    RESET_VEHICLE_GEN_LOADED_CHECKS()
                    WHILE (NOT HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER())
                    AND GET_GAME_TIMER() - iTries < 5000        //5 second max wait
                        WAIT(0)
                    ENDWHILE
                    CDEBUG1LN(DEBUG_DIRECTOR,"Nearby vehicle gens loaded after ",GET_GAME_TIMER() - iTries," ms (max: 5000)")

                    SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vLaunchCoords)
                
                    SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
                    SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
                    //Determine if it should keep the original Z (player on objects) or not
                    FLOAT fGroundZ
                    GET_GROUND_Z_FOR_3D_COORD(vLaunchCoords, fgroundZ)
                    IF fGroundZ < vLaunchToeCoords.z -0.1       //Player up on something, keep Z
                        CDEBUG1LN(DEBUG_DIRECTOR,"Player in the air, NOT snapping to ground, placing at ", vLaunchCoords)
                        START_PLAYER_TELEPORT(PLAYER_ID(), vLaunchCoords-<<0,0,0.6>> , fLaunchHeading, TRUE, FALSE)
                    ELSE                                    //Place on ground
                        START_PLAYER_TELEPORT(PLAYER_ID(), vLaunchCoords , fLaunchHeading, TRUE, TRUE)
                    ENDIF
                
                    CLEAR_PED_WETNESS(PLAYER_PED_ID())
                    TRIGGER_IDLE_ANIMATION_ON_PED(PLAYER_PED_ID())
                    FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
                ENDIF

                //Refresh the interior we're loading, if there is one
                IF intLaunchInterior <> NULL
                    CDEBUG1LN(DEBUG_DIRECTOR,"Refreshing interior ", NATIVE_TO_INT(intLaunchInterior))
                    REFRESH_INTERIOR(intLaunchInterior)
                ENDIF
            ENDIF
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)

            //Reset player's health to original value
            TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
            WAIT(10)    //Wait for it to take effect
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                SET_ENTITY_HEALTH(PLAYER_PED_ID(), iPlayerHealthOnStart)
            ENDIF
            WAIT(10)    //Toggle it back on the next frame
            TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)

            DISPLAY_HUD(TRUE)
            DISPLAY_RADAR(TRUE)

            //Relaunch killed scripts
            Request_and_Load_Scripts_for_Restore_Launch()
            iTries = 0
            //Wait until the player has been successfully teleported
            WHILE NOT UPDATE_PLAYER_TELEPORT(PLAYER_ID())
                WAIT(0)
                CDEBUG1LN(DEBUG_DIRECTOR,"Waiting for player teleport to finish for ", iTries, " frames")
                iTries+=1
            ENDWHILE
            iTries = iTries     //Release fix
            
            LOCK_SAVEHOUSE_DOORS()  //Lock all savehouse doors to avoid player entering/getting stuck in an unsuitable savehouse

            
            IF vehgenBlock != NULL
                REMOVE_SCENARIO_BLOCKING_AREA(vehgenBlock)
            ENDIF
                        
            //Restore player's vehicle + one he was standing on
            RESTORE_VEHICLE_ON_EXIT(vehPlayer, vPlayerVehicleCoords)
            RESTORE_VEHICLE_ON_EXIT(vehReturn, vReturnVehicleCoords)

            vLaunchCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
            wait(0) //B* 2235668: Wait for the car to be positioned / collision physics to kick in
            
            //Do local shapetest, reposition if colliding with an object.
            IF NOT START_UP_SHAPETEST(vLaunchCoords, fLaunchHeading)
                CPRINTLN(DEBUG_DIRECTOR, "Original launch coordinates failed shape test, repositioning to road node")
                
                IF START_UP_ROAD_NODE_TEST(vLaunchCoords, fLaunchHeading, DUMMY_MODEL_FOR_SCRIPT, TRUE, DEFAULT, 2.5)
                    CPRINTLN(DEBUG_DIRECTOR, "Repositioned to ",vLaunchCoords)
                    SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vLaunchCoords + <<0,0,0.7>> )
                ELSE
                    CPRINTLN(DEBUG_DIRECTOR, "Couldn't find valid road node position around ", vLaunchCoords)
                ENDIF
            ENDIF

            //these get set during the script at various points, so resetting here for safety.
            SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), FALSE )
            SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
            SET_PED_ABLE_TO_DROWN(PLAYER_PED_ID())
            SET_PLAYER_ICON_COLOUR(BLIP_COLOUR_DEFAULT) 
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableTakeOffScubaGear,false)
            
            //Wait for the player to settle on the ground
            waitTimer = GET_GAME_TIMER()
            WHILE NOT WAIT_FOR_PLAYER_TO_SETTLE()
                WAIT(0)
            ENDWHILE
            
            SET_GAMEPLAY_CAM_RELATIVE_HEADING()
            SET_GAMEPLAY_CAM_RELATIVE_PITCH()
            
            //Set the viewport camera in the same room
            intLaunchInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
            IF intLaunchInterior <> NULL
                CPRINTLN(DEBUG_DIRECTOR,"Forcing the game viewport to be in the same room as the player")
                FORCE_ROOM_FOR_GAME_VIEWPORT(intLaunchInterior, GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
            ENDIF

            IF bDirectorInitialised
                MAKE_AUTOSAVE_REQUEST()
            ENDIF 
            
        BREAK
        
    ENDSWITCH
    
    //Enable stats
    STAT_ENABLE_STATS_TRACKING()
    
    IF eCleanupType <> DCT_MULTIPLAYER
        //Enable special ability, only if saved global is set
        CPRINTLN(DEBUG_DIRECTOR,"Unlocking special abilities for SP characters (M/F/T): ",
            g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_MICHAEL],"/",
            g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_FRANKLIN],"/",
            g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_TREVOR])
            
        IF g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_MICHAEL] = TRUE
            SPECIAL_ABILITY_UNLOCK(PLAYER_ZERO)
        ENDIF
        IF g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_FRANKLIN] = TRUE
            SPECIAL_ABILITY_UNLOCK(PLAYER_ONE)
        ENDIF
        IF g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_TREVOR] = TRUE
            SPECIAL_ABILITY_UNLOCK(PLAYER_TWO)
        ENDIF       
        
        //restore special ability charge
        if GET_PLAYER_CHARACTER_FROM_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())) = eStoryCharacter
            CPRINTLN(DEBUG_DIRECTOR, "STAT_SET_INT was hit for: ",enum_to_int(eStoryCharacter))
            SWITCH eStoryCharacter
                CASE CHAR_MICHAEL           
                    STAT_SET_INT(SP0_SPECIAL_ABILITY, iSpecialStatValue)
                BREAK
                CASE CHAR_FRANKLIN
                    STAT_SET_INT(SP1_SPECIAL_ABILITY, iSpecialStatValue)
                BREAK
                CASE CHAR_TREVOR
                    STAT_SET_INT(SP2_SPECIAL_ABILITY, iSpecialStatValue)
                BREAK
            ENDSWITCH
        ENDIF   
    
        UPDATE_SPECIAL_ABILITY_FROM_STAT(PLAYER_ID())
    ENDIF
    
    //remove any scenario block
    IF blockingIndex != NULL
        #IF IS_DEBUG_BUILD
            INT iBlockingIndex = NATIVE_TO_INT(blockingIndex)
            CDEBUG1LN(debug_director,"REMOVE_SCENARIO_BLOCKING_AREA( ", iBlockingIndex ,")")
        #ENDIF
        REMOVE_SCENARIO_BLOCKING_AREA(blockingIndex)
        blockingIndex = NULL
    ENDIF
	
	//Make extra sure the wanted level is cleared
	IF eCleanupType <> DCT_EARLY
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
	ENDIF

    IF NOT bSkipFadeIn
        CPRINTLN(DEBUG_DIRECTOR, "Director Mode fading in at the end of cleanup.")
        DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
    ENDIF
    
    IF IS_PLAYSTATION_PLATFORM()
        CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
    ENDIF
    
	//Reset globals
	g_bDirectorModeRunning = FALSE
	g_bLaunchDirectorMode = FALSE
	
    CPRINTLN(DEBUG_DIRECTOR, "Director Mode terminating.")
    TERMINATE_THIS_THREAD()
    
ENDPROC


PROC CHANGE_TIME(INT iSelectedTime)
    SWITCH iSelectedTime
        CASE 0 SET_CLOCK_TIME(0,0,0) BREAK//midnight
        CASE 1 SET_CLOCK_TIME(5,0,0) BREAK//Pre-dawn
        CASE 2 SET_CLOCK_TIME(6,0,0) BREAK//Dawn
        CASE 3 SET_CLOCK_TIME(8,0,0) BREAK//Morning
        CASE 4 SET_CLOCK_TIME(12,0,0) BREAK//Midday
        CASE 5 SET_CLOCK_TIME(16,0,0) BREAK//Afternoon
        CASE 6 SET_CLOCK_TIME(18,30,0) BREAK//Sunset
        CASE 7 SET_CLOCK_TIME(21,0,0) BREAK//Dusk
    ENDSWITCH
ENDPROC



BOOL bIgnoreRestart
INT iRespawnTime
INT iDeathLingerTime

PROC Run_Director_Mode_Death_Arrest()
    IF NOT bProcessingRespawn
		CDEBUG3LN(DEBUG_DIRECTOR,"Running respawn routine for Director Mode.")
	    
	    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
	    //If in prop editor mode, clean up
	    IF bInPropEdit
	        SWAP_PROP_SETTINGS_MENU_DATA()  
	        DMPROP_EXIT_PROP_EDIT()
	        bInPropEdit = FALSE
	    ENDIF
	    #ENDIF
	    
	    bIgnoreRestart = FALSE
	    iDeathLingerTime = 2000
	    iRespawnTime = GET_GAME_TIMER() + iDeathLingerTime	//Doesn't perform a sum every frame
	    
		CDEBUG1LN(DEBUG_DIRECTOR,"Run_Director_Mode_Death_Arrest: Setting g_bDirectorSwitchToCastingTrailer to TRUE")
	    g_bDirectorSwitchToCastingTrailer = TRUE
	    bBypassTrailerQuery = TRUE  //Spawn at trailer without the query screen
	    bCreateVehicle = false // reset check back to default as player is back at the trailer now. 
	    
		//Set the respawn process going
		bProcessingRespawn = TRUE	
	ENDIF
		
ENDPROC

PROC Run_Director_Mode_Respawn()
	CDEBUG1LN(DEBUG_DIRECTOR,"Running post-death respawn before Trailer scene")
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
        IF NOT bIgnoreRestart
            bIgnoreRestart = TRUE
            IGNORE_NEXT_RESTART(TRUE)
            PAUSE_DEATH_ARREST_RESTART(FALSE)
        ENDIF
    
        IF NOT (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
        AND iRespawnTime < GET_GAME_TIMER()
            DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
        ENDIF
    ELSE
		CDEBUG1LN(DEBUG_DIRECTOR,"Player now alive, proceeding to Trailer scene")
		
	    //2279528 - Check for the player death/arrest over the map limits.
	    //If they are over the limits we need to pull their body inside the 
	    //limits so they don't get killed by code again after respawn.
	    VECTOR vDeathArrestCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	    IF ABSF(vDeathArrestCoords.x) > 7500.0 
	    OR ABSF(vDeathArrestCoords.y) > 7500.0 
	    OR vDeathArrestCoords.z > 2500.0
	    OR vDeathArrestCoords.z < -1500.0
	        CPRINTLN(DEBUG_DIRECTOR, "Found player over the map limits at position ", vDeathArrestCoords, ". Repositioning.")
	        SET_ENTITY_COORDS(PLAYER_PED_ID(), vTrailerEndPos, FALSE)
	//        FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
	    ENDIF
		
        CHANGE_TIME(settingsMenu[SEM_TIME]) //Set the correct TOD
		
		
		//Stop processing the respawn
		bProcessingRespawn = FALSE
	ENDIF
ENDPROC

//FUNC enumMenuCategory GET_CURRENT_PLAYER_MODEL_MENU_CATEGORY()
    //RETURN lastPlayerCategory
//ENDFUNC


FUNC enum_directorFlowState GET_DIRECTOR_FLOW_STATE()
    RETURN directorFlowState    
ENDFUNC


FUNC BOOL SECURE_MISSION_FLAG()
    //Set this true to ignore the PI menu being disabled this frame
    g_bDirectorPIMenuUp = TRUE
    
    FEATURE_BLOCK_REASON eBlockReason = GET_FEATURE_BLOCKED_REASON(DEFAULT,DEFAULT,DEBUG_DIRECTOR)
    
    IF eBlockReason != FBR_NONE
        CPRINTLN(DEBUG_DIRECTOR, "Director Mode failed to secure the mission flag. Block condition ", DEBUG_GET_FEATURE_BLOCK_REASON_STRING(eBlockReason,DEBUG_DIRECTOR), " triggered during the process.")
        DO_DIRECTOR_MODE_WARNING_SCREEN_WAIT(eBlockReason)
        SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE, FALSE)
        SCRIPT_CLEANUP(DCT_EARLY)
    ELSE
        eMissionLaunchResult = Request_Mission_Launch(iMissionLaunchID, MCTID_SELECTED_BY_PLAYER, MISSION_TYPE_DIRECTOR)
        WAIT(0) //Wait a frame for the candidate controller to process the ID
    
        IF eMissionLaunchResult = MCRET_DENIED
            CPRINTLN(DEBUG_DIRECTOR, "Director Mode failed to secure the mission flag. Another mission must have launched. Cleaning up.")
            SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE, FALSE)
            SCRIPT_CLEANUP(DCT_EARLY)
        ELIF eMissionLaunchResult = MCRET_ACCEPTED
            IF NOT IS_SAVE_IN_PROGRESS()
            AND NOT IS_AUTOSAVE_REQUEST_IN_PROGRESS() 
            AND g_sAutosaveData.iQueuedRequests = 0
                CPRINTLN(DEBUG_DIRECTOR, "Director Mode secured the mission flag and saving finished.")
                SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE, FALSE)
                SET_MISSION_FLAG(TRUE) //Gives us streaming priority and tells code we're on mission.
                RETURN TRUE
            ELSE
                CDEBUG2LN(DEBUG_DIRECTOR, "Director Mode secured the mission flag but is waiting for an autosave to finish.")
                RETURN FALSE
            ENDIF
        ENDIF
    ENDIF

    CDEBUG3LN(DEBUG_DIRECTOR, "Director Mode waiting for a response to mission launch request...")
    RETURN FALSE
ENDFUNC


PROC CLEAR_DM_MENU_DATA()
    CLEAR_MENU_DATA(TRUE)
    //CLEAR_HELP()
    //SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMenu)
ENDPROC

PROC PREP_MENU_DRAWING()

    CLEAR_DM_MENU_DATA()
    
//DRAW_RECT(sf_x,wl_y,sf_w,wl_h,0,0,0,255)
    Background.x = (0.0+(CUSTOM_MENU_W*0.5))
    Background.y = 0.092 + CUSTOM_MENU_ITEM_BAR_H
    Background.w = CUSTOM_MENU_W
    Background.h = ( CUSTOM_MENU_PIXEL_HEIGHT * 64.0 )//pixels          //  0.08888888888888888888888888888889
    
    sf_h = sf_h     //Unreferenced variable issues
    sf_y = sf_y
    
    INT iHudR, iHudG, iHudB, iHudA
    GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
    Background.r = iHudR    //68    //200
    Background.g = iHudG    //158   //66
    Background.b = iHudB    //208   //66
    Background.a = iHudA    //255
    
    //Player Name
    TitleStyle.aFont = FONT_CURSIVE
    IF IS_LANGUAGE_NON_ROMANIC()
        TitleStyle.XScale = 0.500
        TitleStyle.YScale = 0.75
    ELSE
        TitleStyle.XScale = 0.500
        TitleStyle.YScale = 1.0
    ENDIF
    
    TitleStyle.r = 255
    TitleStyle.g = 255
    TitleStyle.b = 255
    TitleStyle.a = 255
    TitleStyle.drop = DROPSTYLE_NONE
    TitleStyle.wrapEndX = 0
    TitleStyle.wrapStartX = 0
    
    TitlePlace.x = Background.x + 0.01 - (Background.w / 2.0)//Background.x -sf_w*0.5 + 0.01        //(Headshot.x +(CUSTOM_MENU_PIXEL_WIDTH*58))
    TitlePlace.y = 0.096
ENDPROC


//taken from creator.sc DRAW_FMMC_MENU_HEADER()
PROC DRAW_MENU_HEADER()
    IF IS_CUSTOM_MENU_SAFE_TO_DRAW(DEFAULT, TRUE)

        SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
        SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0) 
        //SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
        float xValueToChange = ADJUST_X_COORD_FOR_ASPECT_RATIO(0)
        xValueToChange=xValueToChange
        GET_SCRIPT_GFX_ALIGN_POSITION( xValueToChange, messageBox_y, newScaleformX, newScaleformY )             
    //  CPRINTLN(DEBUG_DIRECTOR,"messageBox_x = ",messageBox_x," xValueToChange = ",xValueToChange," newScaleformX = ",newScaleformX)
        //Move Main Menu Down
        CUSTOM_MENU_Y = ((CUSTOM_MENU_ITEM_BAR_H*3)+bg_y)           
        
        //CUSTOM_MENU_W = sf_w
        DRAW_RECTANGLE(Background)          
        DRAW_TEXT(TitlePlace, TitleStyle, "CM_DMO") //"18 point Chalet Comprine font"       "AdamRSN")//    WgWWWWWWWWWWWWWW
                
        RESET_SCRIPT_GFX_ALIGN()
    ENDIF
ENDPROC

FUNC STRING GET_ANIM_DICT(int iAnimIndex)
    IF iAnimIndex >= cTOTAL_ANIMS SCRIPT_ASSERT("Adjust cTOTAL_ANIMS") ENDIF
    SWITCH iAnimIndex
        CASE 0 RETURN "gestures@m@standing@casual" BREAK //Bring it
        CASE 1 RETURN "gestures@m@standing@casual" BREAK //Bring it
        CASE 2 RETURN "gestures@m@standing@casual" BREAK //Bring it
        CASE 3 RETURN "gestures@m@standing@casual" BREAK //Bring it
        CASE 4 RETURN "gestures@m@standing@casual" BREAK //Bring it
        CASE 5 RETURN "amb@world_human_yoga@male@base" BREAK //Bring it
        CASE 6 RETURN "anim@mp_player_intcelebrationmale@dj" BREAK //Bring it       
        //!!! Adjust cTOTAL_ANIMS if adding more
    ENDSWITCH
    RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME(int iAnimIndex)
    SWITCH iAnimIndex
        CASE 0 RETURN "gesture_bring_it_on" BREAK //Bring it
        CASE 1 RETURN "gesture_come_here_hard" BREAK //come here
        CASE 2 RETURN "gesture_damn" BREAK //Damn
        CASE 3 RETURN "gesture_easy_now" BREAK //Easy now
        CASE 4 RETURN "gesture_no_way" BREAK //Shake head
        CASE 5 RETURN "base_a" BREAK //Yoga
        CASE 6 RETURN "dj" BREAK //DJ
    ENDSWITCH
    RETURN ""
ENDFUNC

FUNC ANIMATION_FLAGS GET_ANIM_UPPERBODY_FLAG(int iAnimIndex)
    SWITCH iAnimIndex
        CASE 0 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //Bring it
        CASE 1 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //come here
        CASE 2 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //Damn
        CASE 3 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //Easy now
        CASE 4 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //Shake head
        CASE 5 RETURN AF_DEFAULT BREAK //Yoga
        CASE 6 RETURN AF_UPPERBODY | AF_SECONDARY BREAK //DJ
    ENDSWITCH
    RETURN AF_DEFAULT
ENDFUNC

FUNC INT PICK(int i1, int i2=-1, int i3=-1, int i4=-1, int i5=-1, int i6=-1, int i7=-1)
    int intList[7]
    intList[0] = i1
    intList[1] = i2
    intList[2] = i3
    intList[3] = i4
    intList[4] = i5
    intList[5] = i6
    intList[6] = i7
    
    int i,maxCount
    
    for i = 0 to COUNT_OF(intList)-1
        IF intList[i] = -1
        OR i = COUNT_OF(intList)-1
            maxCount = i
            i = COUNT_OF(intList)
        ENDIF
    endfor
    
    RETURN intList[GET_RANDOM_INT_IN_RANGE(0,maxCount)]
ENDFUNc


PROC UPDATE_PED_NEEDS_TO_BLOCK_CAR_ENTRY()
    IF NOT IS_PED_VALID_FOR_VEHICLE(player_ped_ID())
        DISABLE_CONTROL_ACTION(player_control,INPUT_ENTER)
    ENDIF   
ENDPROC

PROC UPDATE_BLOCK_WEAPON_WHEEL()
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        IF !CAN_PLAYER_MODEL_HOLD_WEAPONS()
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)          
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_WEAPON)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)
        ENDIF
    ENDIF
ENDPROC

FUNC enumMenuType GET_CURRENT_MENU_TYPE()
    RETURN selectedMenuType
ENDFUNC

FUNC string GET_EXIT_ANIM_FOR_PED_MODEL_CATEGORY(enumMenuCategory menuCategory, int iMenuEntry)
    IF menuCategory = MC_SHORTLIST menuCategory = shortlistData[iMenuEntry].category ENDIF
    IF menuCategory = MC_RECENTLYUSED menuCategory = shortlistData[iMenuEntry+cTOTAL_SHORTLIST_ENTRIES].category ENDIF
    
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE A_F_M_SALTON_01
        CASE A_F_Y_TOURIST_02
        CASE A_F_Y_SOUCENT_01
        CASE A_F_M_FatBla_01
        CASE A_F_M_FatWhite_01      
        CASE A_F_M_FATCULT_01
            RETURN "exit_trailer_female_fat"
        BREAK
        CASE A_M_M_HILLBILLY_01
        CASE A_M_M_FatLatin_01
        CASE A_M_M_Genfat_01
        CASE A_M_M_Genfat_02
            RETURN "exit_trailer_male_fat"
        BREAK
        CASE HC_GUNMAN
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2) = 1
                RETURN "exit_trailer_male_fat" 
            ENDIF
        BREAK //hillbilly gang
        
    ENDSWITCH
    
    IF IS_PLAYER_WEARING_BALLISTIC_ARMOUR()
        RETURN "exit_trailer_male_fat"
    ENDIF
    
    
    IF iMenuEntry >= 0
        SWITCH displayedMenuData[iMenuEntry].category
        

            CASE MC_VAGPR1 RETURN "exit_trailer_male_gang" BREAK //prisoner
            CASE MC_VAGPR2 RETURN "exit_trailer_male_gang" BREAK //prisoner
            CASE MC_PROSTR RETURN "exit_trailer_female_sexy" BREAK //stripper hooker
            CASE MC_SPECLI  RETURN "exit_trailer_male_drunk" BREAK //clinton
            CASE MC_SPEMAN RETURN "exit_trailer_male_drunk" BREAK //mani
            CASE MC_SPEZOM RETURN "exit_trailer_male_drunk" BREAK //zombie
            CASE MC_STOMAU RETURN "exit_trailer_female_fat" BREAK //Maude
            CASE MC_STOLAM RETURN "exit_trailer_male_gang" BREAK //Lamar
            CASE MC_STOSTR RETURN "exit_trailer_male_gang" BREAK //Stretch
            CASE MC_LABFAR RETURN "exit_trailer_male_fat" BREAK//farmer
            CASE MC_BEABO1 RETURN "exit_trailer_male_gang" BREAK //body builder
            CASE MC_BEABO2 RETURN "exit_trailer_male_gang" BREAK //body builder
        ENDSWITCH
    ENDIF
    
    SWITCH  menuCategory
        CASE MC_GANG
            IF IS_DIRECTOR_PED_MALE()
                RETURN "exit_trailer_male_gang"
            ENDIF
        BREAK
        CASE MC_VAGRANTS
            IF IS_DIRECTOR_PED_MALE()
                RETURN "exit_trailer_male_drunk" 
            ELSE
                RETURN "exit_trailer_female_drunk"
            ENDIF
        BREAK
        CASE MC_BEACH
            IF !IS_DIRECTOR_PED_MALE()
                RETURN "exit_trailer_female_sexy" 
            ENDIF
        BREAK
    ENDSWITCH
        
    IF IS_DIRECTOR_PED_MALE()
        RETURN "exit_trailer_male_generic"
    ELSE
        RETURN "exit_trailer_female_generic"
    ENDIF
ENDFUNC

        

FUNC INT GET_TOTAL_CATEGORY_MODEL_VARIATIONS(enumMenuCategory modelCategory,enumMenuCategory menuCategory)
    int iTotalCount
    int iSelectedVoid
    
    IF menuCategory = MC_ANIMALS
        GET_CATEGORY_MODEL(modelCategory,ANIMAL,iSelectedVoid,iTotalCount)
    ELIF bMenu_sex_male
        GET_CATEGORY_MODEL(modelCategory,MALE,iSelectedVoid,iTotalCount)
    ELSE
        GET_CATEGORY_MODEL(modelCategory,FEMALE,iSelectedVoid,iTotalCount)
    ENDIF
    RETURN iTotalCount
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_TO_LOAD(enumMenuCategory modelCategory,int iSelected, bool justCheckingModel = FALSE)
    cprintln(DEBUG_DIRECTOR,"GET_MODEL_TO_LOAD() modelCategory = ",modelCategory," index = ",iSelected)
    MODEL_NAMES chosenModel
    IF NOT justCheckingModel
        selectedHeistMember = CM_EMPTY
    ENDIF
    
    int iTotalCountVoid //not needed
    IF dirCurrentMenu = MC_ANIMALS
        chosenModel = GET_CATEGORY_MODEL(modelCategory,ANIMAL,iSelected,iTotalCountVoid)
    ELSE
        IF (dirCurrentMenu = MC_HEIST OR dirCurrentMenu = MC_SPECIAL OR dirCurrentMenu = MC_STORY)
            chosenModel = GET_CATEGORY_MODEL(modelCategory,IRRELEVANT,iSelected,iTotalCountVoid)
        ELSE
            IF bMenu_sex_male
                chosenModel = GET_CATEGORY_MODEL(modelCategory,MALE,iSelected,iTotalCountVoid)
            ELSE
                chosenModel = GET_CATEGORY_MODEL(modelCategory,FEMALE,iSelected,iTotalCountVoid)
            ENDIF
        ENDIF
    ENDIF
    
    
    IF chosenModel = DUMMY_MODEL_FOR_SCRIPT
        chosenModel = GET_RANDOM_PED_MODEL()
    ENDIF
    
    
    RETURN chosenModel
    
ENDFUNC

PROC SET_CAMERA_STATE(enum_cameraState newCamState)
    //cprintln(DEBUG_DIRECTOR,"SET_CAMERA_STATE() : ",newCamState)
    dirCamState = newCamState
ENDPROC

FUNC enum_cameraState GET_CAMERA_STATE()
    RETURN dirCamState
ENDFUNC


FUNC BOOL IS_MENU_CATEGORY_AN_ANIMAL(enumMenuCategory selectedItem)
    IF selectedItem > MC_ANIMALS_START
    AND selectedItem < MC_ANIMALS_END
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC
    

FUNC BOOL IS_MENU_CATEGORY_A_SMALL_ANIMAL(enumMenuCategory selectedItem)
    SWITCH selectedItem
        CASE MC_ANICAT
        FALLTHRU
        CASE MC_ANIRAB
        FALLTHRU
        CASE MC_ANICHI
        FALLTHRU
        CASE MC_ANICOR
        FALLTHRU
        CASE MC_ANICRO
        FALLTHRU
        CASE MC_ANIHEN
        FALLTHRU
        CASE MC_ANIPIN
        FALLTHRU
        CASE MC_ANISEA
            RETURN TRUE
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_MENU_CATEGORY_A_BIG_ANIMAL(enumMenuCategory selectedItem)
    IF IS_MENU_CATEGORY_AN_ANIMAL(selectedItem)
    AND !IS_MENU_CATEGORY_A_SMALL_ANIMAL(selectedItem)
        RETURN TRUE     
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_MODEL_A_SMALL_ANIMAL(model_names animalModel)
    SWITCH animalModel
        CASE A_C_CAT_01
        FALLTHRU
    //  CASE A_C_POODLE
    //  FALLTHRU
    //  CASE A_C_PUG
    //  FALLTHRU
        CASE A_C_RABBIT_01
        FALLTHRU
        CASE A_C_CHICKENHAWK
        FALLTHRU
        CASE A_C_CORMORANT
        FALLTHRU
        CASE A_C_CROW
        FALLTHRU
        CASE A_C_HEN
        FALLTHRU
        CASE A_C_PIGEON
        FALLTHRU
        CASE A_C_SEAGULL
            RETURN TRUE
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC

PROC GET_SPAWN_POSITIONS(MODEL_NAMES newModelToLoad,vector &vSpawn, float &fHeading)
    
    SWITCH directorFlowState
        CASE dfs_RUN_TRAILER_SCENE
        FALLTHRU
        CASE dfs_PREP_TRAILER_SCENE
            IF IS_MODEL_AN_ANIMAL(newModelToLoad)
                IF IS_MODEL_A_SMALL_ANIMAL(newModelToLoad)
                    vSpawn = <<-13.8082, -14.2325, 500.115>>
                    fHeading = 352.9452
                ELSE                
                    vSpawn = <<-11.9823, -14.7217, 499.0583>>
                    fHeading = 20
                ENDIF
            ELSE    
                IF directorFlowState = dfs_PREP_TRAILER_SCENE
                    vSpawn = vTrailerStartPos
                    fHeading = fTrailerStartHeading
                ELSE
                    vSpawn = vTrailerEndPos
                    fHeading = fTrailerEndHeading
                ENDIF
            ENDIF
        BREAK
        CASE dfs_PLAY_MODE
        
            cprintln(DEBUG_DIRECTOR,"respawn at ground coord")
            vSpawn =  GET_ENTITY_COORDS(player_ped_id())
            float returnZ
            GET_GROUND_Z_FOR_3D_COORD(vSpawn + <<0,0,2>>,returnZ)
            IF returnZ != 0.0
                IF ABSF(vSpawn.z - returnZ) > 2.0                   
                    GET_SAFE_COORD_FOR_PED(GET_ENTITY_COORDS(player_ped_id()),false,vSpawn,GSC_FLAG_NOT_INTERIOR | GSC_FLAG_ONLY_NETWORK_SPAWN)
                    vSpawn.z -= 0.9
                    cprintln(DEBUG_DIRECTOR,"respawn at ground too far, pick safe point")   
                ELSE
                    vSpawn.z = returnZ
                ENDIF
            ENDIF
            fHeading = GET_ENTITY_HEADING(player_ped_id())
        BREAK
    ENDSWITCH
ENDPROC

FUNC BOOL IS_VEHICLE_A_PREP_MISSION_ONE(VEHICLE_INDEX vehInd)
    VEHICLE_INDEX vehTemp
    IF vehInd <> NULL
        vehTemp = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)
        IF vehTemp <> NULL AND vehTemp = vehInd
            CPRINTLN(debug_director,"WARNING: Can't save last player vehicle, it's being used by FBI4 prep")
            RETURN TRUE
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL BUILD_TRAILER_SCENE()
    
    SWITCH buildSceneState
        CASE BSS_INIT
        CDEBUG3LN(debug_director,"BUILD_TRAILER_SCENE() CASE BSS_INIT")
            IF GET_CAMERA_STATE() = DIR_CAM_GAME_FADED_OUT
                //Make double sure these anim dicts have been requested
                REQUEST_ANIM_DICT("director@character_select_intro@male")
                REQUEST_ANIM_DICT("director@character_select_intro@female")
                REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
            
                IF HAS_ANIM_DICT_LOADED("director@character_select_intro@male")
                AND HAS_ANIM_DICT_LOADED("director@character_select_intro@female")
                AND HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")


                    SET_CAMERA_STATE(DIR_CAM_PREP_TRAILER_SHOT)
                    Quit_Drunk_Camera_Immediately() 
                    Force_All_Drunk_Peds_To_Become_Sober()
                    CLEAR_TIMECYCLE_MODIFIER()
                    CANCEL_ALL_POLICE_REPORTS()
                    DISTANT_COP_CAR_SIRENS(FALSE)
                    REMOVE_PLAYER_HELMET(PLAYER_ID(),TRUE)
                    CLEAR_PED_WETNESS(PLAYER_PED_ID())
                    
                //  sfMenu = REQUEST_SCALEFORM_MOVIE("TEXTFIELD")   
                
                    
                    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                        IF IS_VECTOR_ZERO(vLaunchCoords)
                            vLaunchCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
                            fLaunchHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
                            vLaunchToeCoords = (GET_PED_BONE_COORDS(player_ped_id(),BONETAG_L_TOE,<<0,0,0>>) + GET_PED_BONE_COORDS(player_ped_id(),BONETAG_R_TOE,<<0,0,0>>)) /2.0
                            intLaunchInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
                                                    
                            float groundZ
                            GET_GROUND_Z_FOR_3D_COORD(vLaunchCoords,groundZ) 
                            IF groundZ = 0.0
                                IF GET_SAFE_COORD_FOR_PED(vLaunchCoords,false,vLaunchCoords,GSC_FLAG_NOT_INTERIOR | GSC_FLAG_ONLY_NETWORK_SPAWN)
                                ELSE
                                    vLaunchCoords = <<-872.9634, -91.6508, 36.8744>>
                                ENDIF
                            ENDIF
                            
                            //Check if the player's vehicle is nearby
                            vehPlayer = GET_LAST_DRIVEN_VEHICLE()
                            IF DOES_ENTITY_EXIST(vehPlayer)
                                IF NOT IS_ENTITY_DEAD(vehPlayer)
                                    vPlayerVehicleCoords = GET_ENTITY_COORDS(vehPlayer)
                                    IF VDIST2(vLaunchCoords, vPlayerVehicleCoords) < 40000
                                    AND NOT IS_VEHICLE_A_PREP_MISSION_ONE(vehPlayer)
                                    AND IS_VEHICLE_SEAT_FREE(vehPlayer) //B* 2303063: stops previously used taxis from being saved
                                        //Store player vehicle data, mark as part of the mission.
                                        CPRINTLN(debug_director,"Player's vehicle is nearby, saving it as return vehicle")
                                        SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, FALSE, TRUE)
                                        
                                        //Set vehicle somewhere safe.
                                        SET_ENTITY_COORDS(vehPlayer,<<0,0,-40>>)
                                        FREEZE_ENTITY_POSITION(vehPlayer,TRUE)
                                    ENDIF
                                ENDIF
                            ENDIF
                            
                            //Check if the player is on top of a vehicle
                            vehReturn = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()),6,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
                            IF DOES_ENTITY_EXIST(vehReturn)
                                IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(),vehReturn)
                                AND NOT IS_ENTITY_DEAD(vehReturn)
                                AND vehReturn <> vehPlayer  //Ignore the player vehicle if player standing on it.
                                    CPRINTLN(debug_director,"Player is on top of a vehicle, saving it as return vehicle")
                                    SET_ENTITY_AS_MISSION_ENTITY(vehReturn, FALSE, TRUE)
                                    //Set vehicle somewhere safe.
                                    vReturnVehicleCoords = GET_ENTITY_COORDS(vehReturn)
                                    SET_ENTITY_COORDS(vehReturn,<<0,0,-80>>)
                                    FREEZE_ENTITY_POSITION(vehReturn,TRUE)
                                ENDIF
                            ENDIF
                            
                        ENDIF
                        If IS_VECTOR_ZERO(vLastPlayerCoord)
                            vLastPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
                            fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
                            
                            cprintln(DEBUG_DIRECTOR,"Last coords = ",vLastPlayerCoord)
                            
                            float groundZ
                            GET_GROUND_Z_FOR_3D_COORD(vLastPlayerCoord,groundZ) 
                            IF groundZ = 0.0
                                IF GET_SAFE_COORD_FOR_PED(vLastPlayerCoord,false,vLastPlayerCoord,GSC_FLAG_NOT_INTERIOR | GSC_FLAG_ONLY_NETWORK_SPAWN)
                                ELSE
                                    vLastPlayerCoord = <<-872.9634, -91.6508, 36.8744>>
                                ENDIF
                            ENDIF                       
                        ENDIF
                        
                        CLEAR_AREA(vLaunchCoords,1000.0,TRUE) //bug 2182732 ensure no vehicles left behind the player was in/
                        
                        //remove accessories from player
                        //CLEAR_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
                        //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL,0,0) 
                        //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,0,0)
                    ENDIF
                    //request assets
                    SET_WANTED_LEVEL_MULTIPLIER(0.0)
                    SET_MAX_WANTED_LEVEL(0)
                    DISABLE_ALL_RESTRICTED_AREAS()
                //  CLEAR_AREA(<<-1046.6920, -518.6486, 35.0386>>,100,TRUE)
                //  LOAD_SCENE(<<-1046.6920, -518.6486, 35.0386>>) //should change to new load scene
                    SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)                               
                    SET_PED_CAN_RAGDOLL( PLAYER_PED_ID(), FALSE )                     
                    
                    GET_CURR_WEATHER_STATE(iStoredWeather[0],iStoredWeather[1],fWeatherTransition)
                    iClockHours = GET_CLOCK_HOURS()
                    iClockMinutes = GET_CLOCK_MINUTES()
                    
                    fRainLevel = GET_RAIN_LEVEL()
                    
//                  SET_WEATHER_TYPE_NOW("EXTRASUNNY")
                    SET_STATIC_EMITTER_ENABLED("SE_DMOD_Trailer_Radio",true)
                    SET_AMBIENT_ZONE_STATE("AZ_DMOD_TRAILER_01",true,true)
                    //SET_CLOCK_TIME(11,0,0)
                    
                    IF NOT IS_PED_INJURED(player_ped_id())
                        SET_ENTITY_COORDS(PLAYER_PED_ID(),vTrailerEndPos)
                    ENDIF
                    
                    REQUEST_IPL("replay")
                            
                    //LOAD_SCENE(vTrailerEndPos)
                    
                    INTERIOR_INSTANCE_INDEX intTrailer
                    intTrailer = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-15.5,-7.9,500>>, "milo_replay")
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableTakeOffScubaGear,true)
                    WHILE NOT IS_INTERIOR_READY(intTrailer)
						IF NETWORK_IS_GAME_IN_PROGRESS()
							CPRINTLN(DEBUG_DIRECTOR, "Skipping interior wait as we've entered MP")
							
							RETURN FALSE
						ENDIF
						
                        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                            SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffScubaGear, true)
                            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableTakeOffScubaGear,true)
                        ENDIf
                        cprintln(debug_director,"Waiting for interior to load")
                        wait(0)
                    ENDWHILE
                
                    CLEAR_AREA_OF_PEDS(vTrailerStartPos,50.0)
                    ANIMPOSTFX_STOP_ALL()
                
                    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                        vector vSpawn
                        float fHeading
                        GET_SPAWN_POSITIONS(GET_ENTITY_MODEL(PLAYER_PED_ID()),vSpawn,fHeading)
                        SET_ENTITY_COORDS(PLAYER_PED_ID(),vSpawn)//vTrailerStartPos)
                        SET_ENTITY_HEADING(PLAYER_PED_ID(),fHeading) //fTrailerStartHeading)
                        CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
                        SET_PED_RESET_FLAG(player_ped_id(),PRF_SkipOnFootIdleIntro, TRUE)
                        FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_IDLE)
                        
                        IF NOT IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
                        OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = IG_ORLEANS
                            SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
                            cprintln(debug_director,"SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)")
                        ENDIF
                    ENDIF               
                    
                    buildSceneState = BSS_TRIGGER_PLAYER
                ENDIF
            ENDIF
        BREAK
        CASE BSS_TRIGGER_PLAYER
        CDEBUG3LN(debug_director,"BUILD_TRAILER_SCENE() CASE BSS_TRIGGER_PLAYER")
            IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_CAMERA_TRIGGER_WALKIN
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
                    sequence_index aSequence
                    OPEN_SEQUENCE_TASK(aSequence)
                        TASK_GO_STRAIGHT_TO_COORD(null,vTrailerendPos,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,ftrailerEndHeading)
                        //TASK_ACHIEVE_HEADING(null,ftrailerEndHeading)
                    CLOSE_SEQUENCE_TASK(aSequence)
                    TASK_PERFORM_SEQUENCE(player_ped_id(),aSequence)
                    CLEAR_SEQUENCE_TASK(aSequence)
                ENDIF
                
                FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_WALK, true, FAUS_DEFAULT )
                //SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
                
                buildSceneState = BSS_WAIT_ON_CAMERA
            ELIF GET_CAMERA_STATE() = DIR_CAM_TRAILER_PANNING_TO_ANIMALS OR GET_CAMERA_STATE() = DIR_CAM_TRAILER_PANNING_TO_SMALL_ANIMALS
            
                buildSceneState = BSS_WAIT_ON_CAMERA
            ENDIF
        BREAK
        
        
        CASE BSS_WAIT_ON_CAMERA
        CDEBUG3LN(debug_director,"BUILD_TRAILER_SCENE() CASE BSS_WAIT_ON_CAMERA")
            IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_ANIMALS
            OR GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_TRAILER
            OR GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                RETURN TRUE
            ENDIF
        BREAK
        DEFAULT
        CDEBUG3LN(debug_director,"BUILD_TRAILER_SCENE() CASE DEFAULT?")
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC

FUNC enum_directorFlowState GET_DIRECTOR_MODE_STATE()
    RETURN directorFlowState
ENDFUNC

FUNC BOOL HAS_DIRECTOR_MODE_TERMINATED()
    IF directorFlowState = dfs_terminated
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC


DEBUGONLY FUNC STRING GET_DFS_STRING(enum_directorFlowState state)
	SWITCH state
		CASE dfs_invalid RETURN "dfs_invalid"
		CASE dfs_INIT RETURN "dfs_INIT"
		CASE dfs_PREP_TRAILER_SCENE RETURN "dfs_PREP_TRAILER_SCENE"
		CASE dfs_RUN_TRAILER_SCENE RETURN "dfs_RUN_TRAILER_SCENE"
		CASE dfs_enter_PLAY_MODE RETURN "dfs_enter_PLAY_MODE"
		CASE dfs_PLAY_MODE RETURN "dfs_PLAY_MODE"
		CASE dfs_warp_to_location RETURN "dfs_warp_to_location"
		CASE dfs_swap_player_model RETURN "dfs_swap_player_model"
#IF FEATURE_SP_DLC_DM_PROP_EDITOR
		CASE dfs_query_prop_scene_clear RETURN "dfs_query_prop_scene_clear"
		CASE dfs_query_overwrite_scene RETURN "dfs_query_overwrite_scene"
		CASE dfs_query_load_scene RETURN "dfs_query_load_scene"
		CASE dfs_query_exit_scene RETURN "dfs_query_exit_scene"
		CASE dfs_warp_player_to_prop RETURN "dfs_warp_player_to_prop"
#ENDIF // FEATURE_SP_DLC_DM_PROP_EDITOR
		CASE dfs_shortlist_full_query RETURN "dfs_shortlist_full_query"
		CASE dfs_query_editor RETURN "dfs_query_editor"
		CASE dfs_query_trailer RETURN "dfs_query_trailer"
		CASE dfs_query_terminate RETURN "dfs_query_terminate"
		CASE dfs_request_terminate RETURN "dfs_request_terminate"
		CASE dfs_terminated RETURN "dfs_terminated"
		DEFAULT 
			ASSERTLN(DEBUG_DIRECTOR, "Unhandled director flow state: ", state)
			RETURN "dfs_invalid"
	ENDSWITCH
ENDFUNC

PROC SET_DIRECTOR_MODE_STATE(enum_directorFlowState newState,BOOL bRememberOldState = FALSE)
    cprintln(DEBUG_DIRECTOR,"SET_DIRECTOR_MODE_STATE() = ", GET_DFS_STRING(newState))
    IF bRememberOldState
        directorFlowStateBeforeQuery = directorFlowState
    ENDIF
    directorFlowState = newState
ENDPROC

FUNC BOOL IS_CONTEXT_BLOCKED_FOR_THIS_PED(TEXT_LABEL_31 contextToCheck)
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE a_f_y_business_03 IF ARE_STRINGS_EQUAL(contextToCheck,"GENERIC_INSULT_MED")  RETURN TRUE ENDIF BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC

PROC UPDATE_PLAYER_VOICE()

    //If the voice hash was set from the shortlist, don't set the voice to full range
    IF bVoiceHashSetFromShortlist
        bVoiceHashSetFromShortlist = FALSE
        CDEBUG1LN(debug_director,"UPDATE_PLAYER_VOICE(): Voice hash set from shortlist, not updating")
        EXIT
    ENDIF
    
    IF get_entity_model(player_ped_id()) != PLAYER_ZERO  
    and get_entity_model(player_ped_id()) != PLAYER_ONE
    and get_entity_model(player_ped_id()) != PLAYER_TWO     
        IF get_entity_model(player_ped_id()) != S_M_M_Paramedic_01 //special case which is covered by commands below
            cprintln(debug_director,"SET_PED_VOICE_FULL()")
            SET_PED_VOICE_FULL(PLAYER_PED_ID())
        ENDIF
    ENDIF

    cprintln(debug_director,"UPDATE_PLAYER_VOICE()")
    SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
        CASE IG_TANISHA
            SET_PED_GENDER(player_ped_id(),FALSE)
        BREAK
        
        CASE HC_DRIVER
        CASE HC_HACKER
            cprintln(debug_director,"UPDATE_PLAYER_VOICE() gunman")
            IF !IS_DIRECTOR_PED_MALE(GET_ENTITY_MODEL(player_ped_id()))
                SET_PED_GENDER(player_ped_id(),FALSE)
                cprintln(debug_director,"PED HASH")
                IF (GET_ENTITY_MODEL(player_ped_id()) = HC_DRIVER AND GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 2) //Talina
                    cprintln(debug_director,"voice group Talina. Hash key: ",GET_HASH_KEY("TALINA_PVG"))
                    SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("TALINA_PVG"))
                ENDIF
                IF (GET_ENTITY_MODEL(player_ped_id()) = HC_HACKER AND GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 1) //PAIGE
                    SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("PAIGE_PVG"))
                ENDIF               
            ENDIF
        BREAK
        
        CASE G_F_Y_Lost_01
            SWITCH GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD)
                CASE 0 //black
                    cprintln(debug_director,"UPDATE voice to black lost ped. Hash key: ",GET_HASH_KEY("FEMALE_LOST_BLACK_PVG")," dwbl: ",GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD))
                    SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("FEMALE_LOST_BLACK_PVG"))
                BREAK
                CASE 1 //white
                    cprintln(debug_director,"UPDATE voice to white lost ped. Hash key: ",GET_HASH_KEY("FEMALE_LOST_WHITE_PVG")," dwbl: ",GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD))
                    SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("FEMALE_LOST_WHITE_PVG"))
                BREAK
            ENDSWITCH
        BREAK
        
        CASE S_M_M_Paramedic_01
            SWITCH GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD)
                CASE 0 //mexican
                    cprintln(debug_director,"UPDATE_PLAYER_VOICE() paramedic mexican ")
                    SET_PED_VOICE_FULL(PLAYER_PED_ID())
                    //SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("FEMALE_LOST_BLACK_PVG"))
                BREAK
                CASE 1 //black
                    cprintln(debug_director,"UPDATE_PLAYER_VOICE() paramedic black ")
                    SET_PED_RACE_AND_VOICE_GROUP(player_ped_id(),2,GET_HASH_KEY("S_M_M_PARAMEDIC_01_BLACK_PVG"))
                    //SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("S_M_M_PARAMEDIC_01_BLACK_PVG"))
                BREAK
                CASE 2 //white
                    cprintln(debug_director,"UPDATE_PLAYER_VOICE() paramedic white ")
                    SET_PED_RACE_AND_VOICE_GROUP(player_ped_id(),1,GET_HASH_KEY("S_M_M_PARAMEDIC_01_WHITE_PVG"))
                    //SET_PED_VOICE_GROUP(player_ped_id(),GET_HASH_KEY("S_M_M_PARAMEDIC_01_WHITE_PVG"))
                BREAK
            ENDSWITCH
        BREAK
            
            
        //  FEMALE_LOST_LATINO_PVG

    ENDSWITCH
ENDPROC


PROC UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
    int i
    iMaxSpeechEnries = 0
    REPEAT COUNT_OF(speechContextData) i
        
        IF DOES_CONTEXT_EXIST_FOR_THIS_PED(player_ped_id(),speechContextData[i]) OR IS_STRING_NULL_OR_EMPTY(speechContextData[i])
            IF NOT IS_CONTEXT_BLOCKED_FOR_THIS_PED(speechContextData[i])
                PISpeechDataForPed[iMaxSpeechEnries] = i
                iMaxSpeechEnries++
            ENDIF
        ENDIF
    ENDREPEAT
    
    cprintln(DEBUG_DIRECTOR,"UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL() iMaxSpeechEnries = ",iMaxSpeechEnries)
    selectedPI[PI_M0_SPEECH] = 0        
ENDPROC

PROC REVISE_MOVEMENT_CLIPSETS_FOR_PED()
    RESET_PED_MOVEMENT_CLIPSET(player_ped_id())
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE HC_GUNMAN
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2) = 1
                SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "ANIM_GROUP_MOVE_BALLISTIC")
            ENDIF
        BREAK
    ENDSWITCH   
ENDPROC

PROC REVISE_COVER_USAGE_FOR_PED()
    //specific cover blocking list for peds with skirts and other cover unfriendly clothing
    bCoverIsBlocked = FALSE
    SET_PLAYER_CAN_USE_COVER(player_id(),TRUE)
    
    IF IS_PLAYER_WEARING_BALLISTIC_ARMOUR()
        bCoverIsBlocked = TRUE      
    ENDIF
    
    //CPRINTLN(DEBUG_DIRECTOR,"GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = ",GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG))
    
    SWITCH get_entity_model(player_ped_id())        
        
        //block all variations
        //CASE A_M_O_Beach_01       //Removed from block list. See bug 2378194
        //CASE A_M_Y_Beach_03       //Removed from block list. See bug 2378194
        //CASE A_M_Y_StWhi_01       //Removed from block list. See bug 2378194
        //CASE A_M_Y_Beach_01       //Removed from block list. See bug 2378194 
        CASE S_M_Y_Factory_01
        CASE S_M_Y_XMech_01
        CASE S_F_Y_Factory_01
        CASE S_F_M_Maid_01
        CASE U_M_M_GRIFF_01
        CASE S_M_M_LineCook
        CASE S_M_M_Doctor_01
        CASE S_M_M_Scientist_01
        CASE S_F_Y_AirHostess_01
        CASE A_F_Y_Hipster_02 
        CASE A_F_M_BevHills_02 
        CASE U_F_Y_SpyActress
        CASE S_F_M_Shop_HIGH
        CASE S_F_Y_Hooker_01
        CASE u_f_o_moviestar
        CASE IG_MRS_THORNHILL
        CASE S_M_M_Postal_02
        CASE S_M_Y_Chef_01  
        CASE A_F_M_Business_02
        CASE A_F_Y_Tennis_01
        CASE A_F_Y_BEVHILLS_03
            bCoverIsBlocked = TRUE
        BREAK
        
        //block specific variations     

        CASE A_M_Y_Beach_02     //Mentioned in bug 2378194 but left in for more info on variation during verify..
        CASE A_M_M_Genfat_01
        CASE A_F_Y_EastSA_02
        CASE S_M_Y_Construct_02
        CASE A_F_Y_Business_01 
        CASE A_F_Y_Business_02 
        CASE A_F_Y_Golfer_01
        CASE A_F_Y_Hipster_01
        CASE A_F_Y_BevHills_01
        CASE A_F_Y_Vinewood_04
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = 0
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE G_M_Y_BallaEast_01
        CASE G_M_M_MexBoss_02
        CASE G_F_Y_Lost_01
        CASE A_M_M_TranVest_01
        CASE S_F_M_FemBarber
        CASE A_F_Y_Business_03 
        CASE A_F_Y_Hipster_04 
        CASE A_F_M_BevHills_01 
        CASE A_M_Y_Golfer_01 
        CASE S_M_Y_AirWorker
        CASE S_M_M_UPS_01
        CASE S_M_M_UPS_02
        CASE A_F_Y_BUSINESS_04
        CASE G_M_Y_FamDNF_01
        CASE A_F_Y_BevHills_02
        CASE A_F_Y_Hipster_03
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = 1
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE HC_HACKER
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = 3
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE G_M_M_ArmBoss_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 2
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE  A_F_Y_Indian_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 1
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE S_M_Y_Waiter_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 0
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK   
        
        CASE IG_AMANDATOWNLEY
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 7
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        CASE IG_TRACYDISANTO
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = 1
            OR GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG) = 2
                bCoverIsBlocked = TRUE
            ENDIF
        BREAK
        
        DEFAULT
            SET_PLAYER_CAN_USE_COVER(player_id(),TRUE)
        BREAK
    ENDSWITCH
    
    IF bCoverIsBlocked
        SET_PLAYER_CAN_USE_COVER(player_id(),FALSE)
    ENDIF
ENDPROC

FUNC BOOL ARE_GESTURES_BLOCKED_FOR_FOR_PED_OR_CONTEXT()
    SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
        
        //shocked med only 
        CASE S_M_Y_HWayCop_01
        CASE A_F_M_TRAMPBEAC_01
        CASE A_M_M_SKIDROW_01
        CASE A_F_M_SKIDROW_01
        CASE S_M_Y_CHEF_01
        CASE A_F_Y_RUNNER_01
        CASE A_M_Y_YOGA_01
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_MED")
                RETURN TRUE     
            ENDIF
        BREAK
        
        //insults med 
        CASE A_M_Y_STWHI_01
        CASE A_F_M_TRAMP_01
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_MED")
                RETURN TRUE
            ENDIF
        BREAK
        
        //insults high
        CASE A_M_Y_EPSILON_02
        CASE U_M_Y_IMPORAGE
        CASE U_M_Y_MILITARYBUM
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_HIGH")
                RETURN TRUE
            ENDIF
        BREAK
        
        //both insults
        CASE U_M_O_FINGURU_01
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_HIGH")
            OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_MED")
                RETURN TRUE
            ENDIF
        BREAK
        
        //frightened med only 
        CASE A_M_O_TRAMP_01
        CASE S_M_Y_PRISONER_01
        CASE G_M_Y_LOST_01
        CASE A_M_Y_MOTOX_02
        CASE A_M_Y_Vinewood_03
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_FRIGHTENED_MED")
                RETURN TRUE
            ENDIF
        BREAK
        
        //frightened high only
        CASE PLAYER_ONE
        CASE IG_WADE
        CASE IG_LAMARDAVIS
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_FRIGHTENED_HIGH")
                RETURN TRUE
            ENDIF
        BREAK
        
        
        //whatever only
        CASE U_F_O_MOVIESTAR
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_WHATEVER")
                RETURN TRUE
            ENDIF
        BREAK
        
        //no only
        CASE S_M_M_POSTAL_01
        CASE S_M_M_UPS_02
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_NO")
                RETURN TRUE
            ENDIF
        BREAK
        
        
        // others ...
        CASE A_M_M_TRAMP_01
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_FRIGHTENED_MED")
            OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_MED")
                RETURN TRUE
            ENDIF
        BREAK
        CASE IG_MAUDE
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_MED")
            OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_HIGH")
                RETURN TRUE
            ENDIF
        BREAK
        CASE A_M_Y_Beach_01
            IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_MED")
            OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_CURSE_MED")
                RETURN TRUE
            ENDIF
        BREAK
        CASE A_M_Y_BEACH_02
        CASE A_M_Y_RUNNER_02
            IF NOT IS_PC_VERSION()
                IF ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_HI")
                OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_THANKS")
                OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_INSULT_MED")
                OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_CURSE_MED")
                OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_HIGH")
                OR ARE_STRINGS_EQUAL(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],"GENERIC_SHOCKED_MED")
                    RETURN TRUE
                ENDIF
            ENDIF
        BREAK
        
        
        
    ENDSWITCH
    RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_SPEECH_LABEL(int iArrayIndex) 
    TEXT_LABEL_63 textLbl
    textLbl = "CM_DI_"
    textLbl += (iArrayIndex+1) //Needs to be +1 so the played speech contexts match up with the text labels.
    RETURN textLbl
ENDFUNC


 
PROC PLAY_SPEECH()

    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        IF iSubMenu = PI_SUBMENU_PROPS
            EXIT //Fix for 2448400 - Prevent L3, the prop lower control, from playing dialogue.
        ENDIF
    #ENDIF

    cprintln(DEBUG_DIRECTOR,"Play speech()")
    cprintln(DEBUG_DIRECTOR,"selectedPI[PI_M0_SPEECH] = ",selectedPI[PI_M0_SPEECH])
    cprintln(DEBUG_DIRECTOR," PISpeechDataForPed[selectedPI[PI_M0_SPEECH]] = ",PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]) 
    cprintln(DEBUG_DIRECTOR," speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]] = ",speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]]) 
    IF NOT IS_STRING_NULL_OR_EMPTY(speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]])
        
        IF ARE_GESTURES_BLOCKED_FOR_FOR_PED_OR_CONTEXT()
        OR IS_ANY_INTERACTION_ANIM_PLAYING()
        OR IS_PLAYER_FREE_AIMING(player_id())
            bGesturesDisabled = TRUE
            SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),FALSE)
        ELSE
            bGesturesDisabled = FALSE
            SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),TRUE)
        ENDIF
        PLAY_PED_AMBIENT_SPEECH(player_ped_id(),speechContextData[PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]],SPEECH_PARAMS_FORCE_NORMAL)         
        cprintln(DEBUG_DIRECTOR,"PLAY_PED_AMBIENT_SPEECH()")
    ELSE
        cprintln(DEBUG_DIRECTOR,"N/A - PISpeechDataForPed[selectedPI[PI_M0_SPEECH]] = ",PISpeechDataForPed[selectedPI[PI_M0_SPEECH]]) 
    ENDIF
    
    
ENDPROC

PROC PLAY_ANIMAL_NOISE()
    Play_Animal_Sound(animalAnimSoundData.strBarkDict,
                        animalAnimSoundData.strBarkAnim, 
                        animalAnimSoundData.barkTimer,
                        eStoryCharacter,
                        animalAnimSoundData.iBarkSoundID,
                        animalAnimSoundData.sBarkSound.strBarkSound)
ENDPROC

PROC PLAY_ANIMATION(bool fullbody=FALSE)

    IF NOT IS_PED_INJURED(player_ped_id())
        IF NOT IS_AMBIENT_SPEECH_PLAYING(player_ped_id())
            bGesturePlaying = TRUE
            //commenting out pending net_interaction changes. See bug 2185673
            int animIndex = enum_to_int(PLAYER_INTERACTION_NONE)+selectedPI[PI_M0_GESTURE]

            START_INTERACTION_ANIM(2,animIndex,TRUE,TRUE,fullbody)          
        ENDIF
    ENDIF
ENDPROC

PROC CONTROL_WEAPON_SHOWN()
    
    //commenting out pending changes to net_interaction changes. See bug 2185673
    UPDATE_INTERACTION_ANIMS_SP()

ENDPROC

FUNC BOOL IS_PLAYER_IN_PRISON_AREA()
    VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
    
    //Calculate 2D distance squared
    vPlayerCoords.z = 0.0
    IF VDIST2(<< 1701.6664, 2586.2612, 0.0 >>, vPlayerCoords) < 40000
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_A_FLYING_ANIMAL()
    IF ePlayerAnimalState = PAS_IS_AN_ANIMAL
    AND Is_Model_A_Flying_Animal(GET_ENTITY_MODEL(player_ped_id()))
        VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
        Float fGroundZ
        //Get the toe bone coords to check if on the ground
        vPlayerPos = GET_PED_BONE_COORDS(player_ped_id(),BONETAG_L_TOE,<<0,0,0>>)
        GET_GROUND_Z_FOR_3D_COORD(vPlayerPos,fGroundZ)
        IF vplayerPos.z - fGroundZ > 0.3
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

PROC GET_PLAYER_WARNING_STATE()
        
    IF NOT IS_PED_INJURED(player_ped_id())

        currentWarningStateBitSet = 0
        
        diedInWater = FALSE
    
        IF ePlayerAnimalState = PAS_IS_AN_ANIMAL
            SET_BIT(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
        ENDIF
        IF IS_REPLAY_RECORDING()
            SET_BIT(currentWarningStateBitSet,BIT_PWS_RECORDING_IN_PROGRESS)
        ENDIF
        IF IS_PED_IN_ANY_VEHICLE(player_ped_id(),TRUE)
            SET_BIT(currentWarningStateBitSet,BIT_PWS_INCAR)
        ENDIF
        IF IS_ENTITY_IN_WATER(player_ped_id())      
        AND NOT IS_ENTITY_IN_DEEP_WATER(player_ped_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_INWATER)
        ENDIF
        IF IS_ENTITY_IN_DEEP_WATER(player_ped_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
        ENDIF           
        IF IS_ENTITY_IN_AIR(player_ped_id())    
        OR ( IS_PED_IN_FLYING_VEHICLE( PLAYER_PED_ID() ) AND IS_ENTITY_IN_AIR( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) ) )
        OR IS_PLAYER_A_FLYING_ANIMAL()
        OR IS_PLAYER_CLIMBING(PLAYER_ID())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_INAIR)
        ENDIF
        IF IS_PED_ON_VEHICLE(player_ped_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_ONVEHICLE)
        ENDIF
        IF IS_PLAYER_BEING_ARRESTED(player_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_ARRESTING)
        ENDIF
        IF IS_PLAYER_RIDING_TRAIN(player_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_IN_TRAIN)
        ENDIF
        IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),2)
            SET_BIT(currentWarningStateBitSet,BIT_PWS_WANTED)
        ENDIF                       
        IF IS_PLAYER_IN_PRISON_AREA()
            SET_BIT(currentWarningStateBitSet,BIT_PWS_IN_PRISON)
        ENDIF           
        IF IS_PED_IN_COVER(player_ped_id())
            SET_BIT(currentWarningStateBitSet,BIT_PWS_INCOVER)
        ENDIF       
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
            VEHICLE_INDEX vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
            IF GET_PED_IN_VEHICLE_SEAT(vehicle,VS_DRIVER) != PLAYER_PED_ID()
                SET_BIT(currentWarningStateBitSet,BIT_PWS_IS_PASSENGER)
            ENDIF
            if IS_VEHICLE_SUBMERSIBLE(GET_ENTITY_MODEL(vehicle))
                SET_BIT(currentWarningStateBitSet,BIT_PWS_IN_BOAT)
            ENDIF
            IF IS_VEHICLE_IN_WATER(vehicle,TRUE)
                 SET_BIT(currentWarningStateBitSet,BIT_PWS_INWATER)
            ENDIF
        ENDIF
        if IS_PED_IN_ANY_BOAT(player_ped_id())      
            SET_BIT(currentWarningStateBitSet,BIT_PWS_IN_BOAT)
        ENDIF
    ELSE
        IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER)
        OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
            diedInWater = TRUE
        ENDIF
        
        currentWarningStateBitSet = 0
        
        SET_BIT(currentWarningStateBitSet,BIT_PWS_DEAD)
        
    ENDIF       

ENDPROC

PROC CONTROL_WARNING_STATE()
    GET_PLAYER_WARNING_STATE()
    
    IF directorFlowState = dfs_PLAY_MODE
    AND GET_CAMERA_STATE() = DIR_CAM_PLAY_MODE
        IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
            IF diedInWater
            OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<-1031.0200, -399.0387, 37.4325>>) < 40.0 //waterfall killbox
                SET_BIT(exitWarningStateBitSet,BIT_PWS_INWATER)
            ENDIF
        ELSE
            exitWarningStateBitSet = currentWarningStateBitSet
        ENDIF
    ELSE
        IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
            IF diedInWater
                SET_BIT(exitWarningStateBitSet,BIT_PWS_INWATER)
            ENDIF
        ENDIF
    ENDIf
ENDPROC

FUNC BOOL ARE_SHORTLIST_ENTRIES_THE_SAME(int iEntryA, int iEntryB)

    CDEBUG3LN(debug_director,"shortlistData[iEntryA].category = ", shortlistData[iEntryA].category)
    CDEBUG3LN(debug_director,"shortlistData[iEntryB].category = ", shortlistData[iEntryB].category)
    CDEBUG3LN(debug_director,"shortlistData[iEntryA].model = ", shortlistData[iEntryA].model)
    CDEBUG3LN(debug_director,"shortlistData[iEntryB].model = ", shortlistData[iEntryB].model)
    CDEBUG3LN(debug_director,"MC_ONLINE1 = ", MC_ONLINE1)
    CDEBUG3LN(debug_director,"MC_ONLINE2 = ", MC_ONLINE2)
    
    if shortlistData[iEntryA].category = MC_ONLINE1
    and shortlistData[iEntryB].category = MC_ONLINE1        
        return TRUE
    ENDIF
    
    if shortlistData[iEntryA].category = MC_ONLINE2
    and shortlistData[iEntryB].category = MC_ONLINE2
        return TRUE
    ENDIF
    
    IF shortlistData[iEntryA].model != shortlistData[iEntryB].model
        RETURN FALSE
    ENDIF
    
    int i
    FOR i = 0 to 12
        IF shortlistData[iEntryA].modelVariation[i] != shortlistData[iEntryB].modelVariation[i]
            CDEBUG3LN(debug_director,"shortlistData[iEntryA].modelVariation[i] != shortlistData[iEntryB].modelVariation[i]")
            RETURN FALSE
        ENDIF
    ENDFOR
    
    FOR i = 0 to 10
        IF shortlistData[iEntryA].textureVariation[i] != shortlistData[iEntryB].textureVariation[i]
            CDEBUG3LN(debug_director,"shortlistData[iEntryA].textureVariation[i] != shortlistData[iEntryB].textureVariation[i]")
            RETURN FALSE
        ENDIF
    ENDFOR
    
    RETURN TRUE
    
ENDFUNC

FUNC BOOL IS_RECENTLY_USED_ENTRY_THE_SAME_AS_ANY_SHORTLISTED_PED(int recentlyUsedIndex)
    int i
    FOR i = 0 to cTOTAL_SHORTLIST_ENTRIES-1
        IF ARE_SHORTLIST_ENTRIES_THE_SAME(recentlyUsedIndex, i)
            RETURN TRUE
        ENDIF
    endfor
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(int iSelectedShortlist)

    IF iSelectedShortlist < 0 iSelectedShortlist = 0 ENDIF

    IF shortlistData[iSelectedShortlist].model != GET_ENTITY_MODEL(player_ped_id()) //fix for 2204034
        RETURN TRUE
    ENDIF
    
    IF shortlistData[iSelectedShortlist].modelVariation[0] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD)
    AND shortlistData[iSelectedShortlist].modelVariation[1] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_BERD)
    AND shortlistData[iSelectedShortlist].modelVariation[2] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAIR)
    AND shortlistData[iSelectedShortlist].modelVariation[3] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
    AND shortlistData[iSelectedShortlist].modelVariation[4] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG)
    AND shortlistData[iSelectedShortlist].modelVariation[5] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAND)
    AND shortlistData[iSelectedShortlist].modelVariation[6] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_FEET)
    AND shortlistData[iSelectedShortlist].modelVariation[7] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
    AND shortlistData[iSelectedShortlist].modelVariation[8] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
    AND shortlistData[iSelectedShortlist].modelVariation[9] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_DECL)
    AND shortlistData[iSelectedShortlist].modelVariation[10] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_JBIB)
    AND shortlistData[iSelectedShortlist].modelVariation[11] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD)
    AND shortlistData[iSelectedShortlist].modelVariation[12] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES)

    AND shortlistData[iSelectedShortlist].textureVariation[0] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HEAD)
    AND shortlistData[iSelectedShortlist].textureVariation[1] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_BERD)
    AND shortlistData[iSelectedShortlist].textureVariation[2] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAIR)
    AND shortlistData[iSelectedShortlist].textureVariation[3] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_TORSO)
    AND shortlistData[iSelectedShortlist].textureVariation[4] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_LEG)
    AND shortlistData[iSelectedShortlist].textureVariation[5] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAND)
    AND shortlistData[iSelectedShortlist].textureVariation[6] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_FEET)
    AND shortlistData[iSelectedShortlist].textureVariation[7] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
    AND shortlistData[iSelectedShortlist].textureVariation[8] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
    AND shortlistData[iSelectedShortlist].textureVariation[9] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_DECL)
    AND shortlistData[iSelectedShortlist].textureVariation[10] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_JBIB)               
        RETURN FALSE
    ENDIF
    RETURN TRUE
ENDFUNC



FUNC INT get_first_free_shortlist_slot(bool getFromRecentlyUsedList)
    int iFrom,iTo,i
    IF getFromRecentlyUsedList
        iFrom = 10
        iTo = 19
    ELSE
        iFrom = 0
        iTo = 9
    ENDIF
    
    For i = iFrom to iTo
        IF shortlistData[i].model = DUMMY_MODEL_FOR_SCRIPT
            RETURN i
        ENDIF
    endfor
    RETURN -1
ENDFUNC

FUNC INT get_total_active_shortlists(bool getFromRecentlyUsedList)
    int iFirstFree
    
    iFirstFree =  get_first_free_shortlist_slot(getFromRecentlyUsedList)
//  cprintln(debug_director,"get_total_active_shortlists() iFirstFree = ",iFirstFree)
    IF iFirstFree = -1
        iFirstFree = 10
    ELSE
        IF getFromRecentlyUsedList
            iFirstFree -= 10
        ENDIF
    ENDIF
    RETURN iFirstFree
ENDFUNC

FUNC INT get_total_PI_shortlist_options()
    
    //loop through recently used shortlist and only select those that are different to the shortlist peds.
    int i,r
    int totalEntries=0
    
    FOR i = 10 to COUNT_OF(shortlistData)-1
        IF shortlistData[i].model != DUMMY_MODEL_FOR_SCRIPT
            
            totalEntries++
            For r = 0 to 9
                CDEBUG3LN(debug_director,"get_total_PI_shortlist_options() i = ",i," r = ",r)
                IF shortlistData[r].model != DUMMY_MODEL_FOR_SCRIPT
                    IF ARE_SHORTLIST_ENTRIES_THE_SAME(i,r)
                        totalEntries--
                        r = 10
                    endif
                endif
            endfor
        endif
    ENDFOR
    
    FOR i = 0 to cTOTAL_SHORTLIST_ENTRIES-1
        IF shortlistData[i].model != DUMMY_MODEL_FOR_SCRIPT
            totalEntries++
        ENDIF
    endfor
    
    cprintln(debug_director,"get_total_PI_shortlist_options() = ",totalEntries)
    RETURN totalEntries
ENDFUNC

FUNC INT GET_NTH_USED_SHORTLIST_INDEX(int entry)
    int i
    
    REPEAT COUNT_OF(shortlistData) i
        IF shortlistData[i].model != DUMMY_MODEL_FOR_SCRIPT
            IF i < cTOTAL_SHORTLIST_ENTRIES
            OR NOT IS_RECENTLY_USED_ENTRY_THE_SAME_AS_ANY_SHORTLISTED_PED(i)            
                entry--
                if entry < 0
                    RETURN i
                endif
            endif
        ENDIF
    ENDREPEAT
    
    cprintln(debug_director," ** GET_NTH_USED_SHORTLIST_INDEX() entry = ",entry," model = ",GET_MODEL_NAME_FOR_DEBUG(shortlistData[entry].model))
    return 0
ENDFUNC

FUNC INT GET_PI_SHORTLIST_INDEX_FOR_PLAYER_FROM_SHORTLIST_ARRAY()
    int i
    FOR i = 0 to get_total_PI_shortlist_options()-1
        IF !IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(GET_NTH_USED_SHORTLIST_INDEX(i))
            RETURN i
        ENDIF
    ENDFOR
    
    RETURN 0
ENDFUNC

FUNC INT GET_EXISTING_PED_SHORTLIST(bool searchRecentlyUsed=FALSE)
    int i
    int iStart,iEnd
    
    IF searchRecentlyUsed
        iStart = 10
        iEnd = 19
    else
        iStart = 0
        iEnd = 9
    endif
    
    
    
    for i = iStart to iEnd
        IF NOT IS_PED_INJURED(player_ped_id())
            IF GET_ENTITY_MODEL(player_ped_id()) = shortlistData[i].model
                IF shortlistData[i].modelVariation[0] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD)
                AND shortlistData[i].modelVariation[1] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_BERD)
                AND shortlistData[i].modelVariation[2] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAIR)
                AND shortlistData[i].modelVariation[3] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
                AND shortlistData[i].modelVariation[4] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG)
                AND shortlistData[i].modelVariation[5] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAND)
                AND shortlistData[i].modelVariation[6] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_FEET)
                AND shortlistData[i].modelVariation[7] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
                AND shortlistData[i].modelVariation[8] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
                AND shortlistData[i].modelVariation[9] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_DECL)
                AND shortlistData[i].modelVariation[10] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_JBIB)
                AND shortlistData[i].modelVariation[11] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD)
                AND shortlistData[i].modelVariation[12] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES)
        
                AND shortlistData[i].textureVariation[0] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HEAD)
                AND shortlistData[i].textureVariation[1] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_BERD)
                AND shortlistData[i].textureVariation[2] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAIR)
                AND shortlistData[i].textureVariation[3] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_TORSO)
                AND shortlistData[i].textureVariation[4] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_LEG)
                AND shortlistData[i].textureVariation[5] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAND)
                AND shortlistData[i].textureVariation[6] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_FEET)
                AND shortlistData[i].textureVariation[7] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
                AND shortlistData[i].textureVariation[8] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
                AND shortlistData[i].textureVariation[9] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_DECL)
                AND shortlistData[i].textureVariation[10] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_JBIB)
                    RETURN i
                ENDIF
            ENDIF
        ENDIF
    endfor

    RETURN -1
ENDFUNC

PROC nudgeShortlistDown(bool editRecentlyUsedList)
    int i
    
    IF editRecentlyUsedList
        for i = 19 to 11 step -1
            shortlistData[i] = shortlistData[i-1]
        endfor
    ELSE
    for i = 9 to 1 step -1
        shortlistData[i] = shortlistData[i-1]
    endfor
    ENDIF
ENDPROC

PROC nudgeShortlistUp(bool editRecentlyUsedList)
    int i
    
    IF editRecentlyUsedList
        for i = 10 to 18
            shortlistData[i] = shortlistData[i+1]
        endfor
    ELSE
    for i = 0 to 8
        shortlistData[i] = shortlistData[i+1]
    endfor
    ENDIF
ENDPROC

FUNC BOOL CAN_USE_SHORTLIST_MENU_OPTION()
    IF NOT IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_SHORTLIST_SWAP)
        RETURN FALSE
    ENDIF
    
    //At least 2 entries - shortlist and last used
    IF (get_total_active_shortlists(false) + get_total_active_shortlists(true)) < 2
        RETURN FALSE
    ENDIF
    
    //If the shortlist and recently used actors are the same, don't allow swap
    IF (get_total_active_shortlists(false) + get_total_active_shortlists(true)) = 2
    AND IS_RECENTLY_USED_ENTRY_THE_SAME_AS_ANY_SHORTLISTED_PED(GET_NTH_USED_SHORTLIST_INDEX(1))
        RETURN FALSE
    ENDIF
    RETURN TRUE
ENDFUNC

FUNC CONTROL_ACTION GET_DIRECTOR_MODE_ZOOM_KEY()
    RETURN INPUT_FRONTEND_LT
ENDFUNC

PROC CONTROL_PLAYER_INPUT()

    //If holding the switch wheel key, activate mouse camera movement
    bPiMouseCameraOverride = FALSE
    IF GET_DIRECTOR_MODE_STATE() = dfs_PLAY_MODE
        IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
            IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL,caPiMenuMouseCameraOverride)
                bPiMouseCameraOverride = TRUE
            ENDIF
        ENDIF   
    ENDIF

    // to stop B*2217426 - We call HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS() at the frame start if we drew the menu last frame we need to process this
    IF (bWasMenuDrawn) AND NOT bPiMouseCameraOverride
        HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
        HANDLE_MENU_CURSOR(FALSE)
        bWasMenuDrawn = FALSE
    ENDIF
        
//  DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
//  DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
//  DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
    IF IS_SPECIAL_ABILITY_UNLOCKED(GET_ENTITY_MODEL(PLAYER_PED_ID()))
        SPECIAL_ABILITY_LOCK(GET_ENTITY_MODEL(PLAYER_PED_ID()))
    ENDIF
    
    IF GET_DIRECTOR_MODE_STATE() != dfs_PLAY_MODE
        DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_PAUSE)
        DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_PAUSE_ALTERNATE)
    ENDIF
    
    IF (GET_GAME_TIMER() < iPauseDisableTimer)
        DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
    ENDIF
    
    IF GET_DIRECTOR_MODE_STATE() = dfs_PREP_TRAILER_SCENE
    OR GET_DIRECTOR_MODE_STATE() = dfs_RUN_TRAILER_SCENE
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
    ENDIF
        
    
    navY = FALSE
    IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
        navY = TRUE
    ENDIF

    navRandom = FALSE
    IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
        navRandom = TRUE
    ENDIF

    navBack = FALSE
    IF ( IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caMenuCancel)
    OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
    OR IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE() )
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    AND NOT bInPropEditorWarningScreen
    #ENDIF
        navBack = TRUE      
    ENDIF
    
	
	IF DOES_CAM_EXIST(dir_Camera)
    AND GET_CAM_SPLINE_PHASE(dir_Camera) < 1.0
		bCamIsMoving = TRUE
	ELSE
		bCamIsMoving = FALSE
	ENDIF
	
    navAccept = FALSE
    IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)        
    OR IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE())
	AND NOT bCamIsMoving
	AND iNavVertDirection = 0
        navAccept = TRUE
    ENDIF
    
    int stickLX, stickLY, stickRX, stickRY  
    
    GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(stickLX, stickLY, stickRX, stickRY, TRUE)
    
    //modifying navigation to allow auto scroll
    
    iNavVertDirection = 0   
    if !bCreateVehicle
    and IS_SCREEN_FADED_IN()
	AND NOT navAccept
	AND NOT bCamIsMoving
        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
            iNavVertDirection += -1
        ENDIF
        
        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
            iNavVertDirection += 1
        ENDIF       
        
        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caMenuUp)
            iNavVertDirection -= 1
        ENDIF
        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caMenuDown)
            iNavVertDirection += 1
        ENDIF    
    
    IF PImenuON = FALSE //because player can still control camera movement while navigating, so don't use thumbstick
        IF stickLY < -100
            iNavVertDirection += -1         
        ENDIF
        
        IF stickLY > 100            
            iNavVertDirection += 1
        ENDIF
    ENDIF
    
    //Handle_vertical_navigation()
    endif
    
    
    If iNavVertDirection != 0
        IF iLastNavVertDirection != iNavVertDirection
            iLastNavVertDirection = iNavVertDirection
            iNavAccelCounter = 0
            navVertTimer = GET_GAME_TIMER()
        ELSE
            int iTimerStep = 250
            
            IF iNavAccelCounter > 3
                iTimerStep = 100
            ENDIF
            
            IF GET_GAME_TIMER() > navVertTimer + iTimerStep
                iNavAccelCounter++
                navVertTimer = GET_GAME_TIMER()
            ELSE
                iNavVertDirection = 0
            ENDIF
        ENDIF
        
        if iNavVertDirection > 1 
            iNavVertDirection = 1 
        endif
        if iNavVertDirection < -1 
            iNavVertDirection = -1 
        endif
        
    ELSE

        iLastNavVertDirection = 0
    ENDIF
    
    
    nav_PI = FALSE
    
    if not IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
        IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
            nav_PI = TRUE           
        ELSE
            IF forceWaitForPIreset 
                forceWaitForPIreset = FALSE 
            ENDIF
        ENDIF
    ELSE
        IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
            nav_PI = TRUE
            piMenuDelayTimer = 1
        ELSE
            IF forceWaitForPIreset 
                forceWaitForPIreset = FALSE 
            ENDIF
        ENDIF
    ENDIF
            
    
    
    
    
    navLeft = FALSE
    navRight = FALSE
    
    IF directorFlowState != dfs_PLAY_MODE
        IF stickLX < -100
            IF !leftStickReset
                navLeft = TRUE
                leftStickReset = TRUE
            ENDIF
        ELIF stickLX > 100
            IF !leftStickReset
                navRight = TRUE
                leftStickReset = TRUE
            ENDIF
        ELSE
            leftStickReset = FALSE
        ENDIF
    ENDIF
    
    IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caMenuLeft)
        navLeft = TRUE
    ENDIF
    IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caMenuRight)
        navRight = TRUE
    ENDIF
    
    
    navZoom = FALSE

    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_DIRECTOR_MODE_ZOOM_KEY())
            zoomPCToggle = !zoomPCToggle
        ENDIF
    ELSE
        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_DIRECTOR_MODE_ZOOM_KEY())
            navZoom = TRUE
        ENDIF
        
        zoomPCToggle = FALSE
    ENDIF
        
    IF zoomPCToggle = TRUE
        navZoom = TRUE
    ENDIF
    
    navGesture      = FALSE
    navSpeech       = FALSE
    navTravel       = FALSE
    navSetWaypoint  = FALSE
    navDelWaypoint  = FALSE
    navSaveLocation = FALSE
    navFullBodyGesture = FALSE
    
    //this trigger was previously disabled when PI menu is up. Not sure if that's needed. Have allwed it again for bug 2201862
    // we don't allow gesture if speech is playing OR if in the prop editor
    
    
    IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SPECIAL_ABILITY_PC))
    OR (!IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS) AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS) )
    AND NOT IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    AND iSubMenu != PI_SUBMENU_PROPS
    #ENDIF
        navGesture = TRUE
        //check for double tap...
        IF checkDoubleTapFlag = 0
            IF NOT IS_ANY_INTERACTION_ANIM_PLAYING()
                IF doubleTapTimer = 0
                    doubleTapTimer = GET_GAME_TIMER() + 500
                    checkDoubleTapFlag = 1
                ENDIF
            ENDIF
        ELIF checkDoubleTapFlag = 2
            checkDoubleTapFlag = 0          
            IF GET_GAME_TIMER() < doubleTapTimer
                navFullBodyGesture = TRUE
            ENDIF
            doubleTapTimer = 0
        ENDIF
    ELSE
        IF checkDoubleTapFlag = 1
            IF GET_GAME_TIMER() < doubleTapTimer
                checkDoubleTapFlag = 2
            ELSE
                checkDoubleTapFlag = 0
                doubleTapTimer = 0
                navFullBodyGesture = FALSE
            ENDIF
        ENDIF
    ENDIF
    
    // if there isn't interaction player we allow the speech button
    IF NOT IS_ANY_INTERACTION_ANIM_PLAYING() 
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
            //just pressed
                buttonFrameCounter = GET_FRAME_COUNT()
            ENDIF
        ELSE
            IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
            //just pressed
                buttonFrameCounter = GET_FRAME_COUNT()
            ENDIF
        ENDIF
    ENDIF
    
    // we only allow speech if there isn't interaction playing
    IF buttonFrameCounter != 0
    //been pressed
        IF navGesture = TRUE
        //cancel button press
            buttonFrameCounter = 0
        ELIF GET_FRAME_COUNT() - buttonFrameCounter > 10
            IF NOT IS_ANY_INTERACTION_ANIM_PLAYING() 
                navSpeech = true
            ENDIF
            buttonFrameCounter = 0
        ENDIF
    ENDIF
    
    IF PImenuON //if pi menu on, allow previewing speech and gestures in same way as multiplayer.
        switch iSubmenu
            case PI_SUBMENU_NONE
                switch INT_TO_ENUM(DIRECTOR_MODE_PI_M0, sPI_MenuData.iCurrentSelection)
                    case PI_M0_LOCATION                 
                        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
                            navSaveLocation = TRUE
                        ENDIF
                        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caPiMenuWaypoint)
                                                            
                            VECTOR  vCurrLocCoords, vWaypointCoords
                            BOOL    bWaypointActive
                            vCurrLocCoords  = warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position 
                      
                            bWaypointActive = IS_WAYPOINT_ACTIVE()
                            
                            IF bWaypointActive
                                vWaypointCoords = GET_BLIP_COORDS( GET_FIRST_BLIP_INFO_ID( GET_WAYPOINT_BLIP_ENUM_ID() ) )
                            ELSE
                                iWaypointLocIndex = -1
                            ENDIF
                    
                            IF bWaypointActive 
                            AND ( iWaypointLocIndex = selectedPI[PI_M0_LOCATION] OR ARE_VECTORS_ALMOST_EQUAL( vCurrLocCoords, vWaypointCoords, 0.5,  TRUE ) )
                                navDelWaypoint = TRUE
                            ELSE
                                navSetWaypoint = TRUE
                            ENDIF
                        ENDIF
                    break
//                    case PI_M0_GESTURE                  
//                        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS) 
//                        AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
//                            navGesture = TRUE
//                        ENDIF                                                                     //  Removed for B* 2433348
//                    break                                                 
//                    case PI_M0_SPEECH                   
//                        IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
//                            navSpeech = TRUE
//                        ENDIF
//                    break               
                    case PI_M0_SHORTLIST                
                        //if selectedPI[sPI_MenuData.iCurrentSelection] != lastSelectedShortlistPed
                        if IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]))
                        AND NOT ARE_SHORTLIST_ENTRIES_THE_SAME(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]),lastSelectedShortlistPed)
                        and navAccept
                            navSwap = TRUE
                        endif
                    break
                endswitch
            break
        endswitch
        
    ENDIF
    
    IF navGesture = TRUE
        DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_BEHIND)
    ENDIF
    
    IF dirCamState != DIR_CAM_PLAY_MODE 
    AND directorFlowState = dfs_RUN_TRAILER_SCENE
        SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
        SET_PED_CAN_RAGDOLL( PLAYER_PED_ID(), FALSE )
    ENDIF
    
    /*
    IF IS_PED_GESTURING(PLAYER_PED_ID()) 
        DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.2, "STRING", "GESTURE")
    ENDIF
    
    IF IS_ANY_INTERACTION_ANIM_PLAYING()
        DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.4, "STRING", "HANDS")
    ENDIF
    
    IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID()) OR IS_ANY_DIALOGUE_PLAYING(PLAYER_PED_ID())
        DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.6, "STRING", "SPEAK")
    ENDIF
    */
    
ENDPROC

FUNC BOOL BLOCKED_VARIATION(ped_index pedToChange,PED_COMPONENT pedComp, int value)

        IF GET_PED_DRAWABLE_VARIATION(pedToChange,pedComp) = value 
            RETURN TRUE 
        ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_MODEL_BLOCKED_FOR_CELLPHONE()
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE IG_MAUDE
        CASE G_M_Y_KorLieut_01
        CASE A_F_M_FATCULT_01
        CASE S_M_Y_CONSTRUCT_01
        CASE S_M_Y_CONSTRUCT_02
        CASE S_M_Y_GARBAGE
        CASE S_M_Y_Factory_01
        CASE S_M_M_Gardener_01
        CASE S_M_M_DockWork_01
        CASE S_M_Y_XMech_02
        CASE S_F_Y_Migrant_01
        CASE U_M_M_griff_01
        CASE U_M_Y_imporage
        CASE A_F_M_FATBLA_01
        CASE U_M_Y_Hippie_01
            RETURN TRUE
        BREAK
        
        CASE S_M_M_DOCTOR_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1)
            OR BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,2)
                RETURN TRUE
            ENDIF
        BREAK
    ENDSWITCH
    RETURN FALSE
ENDFUNC



FUNC BOOL HAS_PLAYER_HAD_A_VEHICLE_COMPONENT_BLOCKED()
    IF blockData.componentBlockActive
    OR blockData.propBlockActive
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

PROC blockComponentProp(PED_PROP_POSITION componentToBlock, int valueToSetTo)
    blockData.propBlockActive = true
    blockData.blockOnModel = get_entity_model(player_ped_id())
    blockData.propBlocked = componentToBlock
    blockData.unblockPropVariation = valueToSetTo
    CLEAR_PED_PROP(player_ped_id(),componentToBlock)
ENDPROC

PROC blockComponent(PED_COMPONENT componentToBlock, int valueToSetTo)
    blockData.componentBlockActive = true
    blockData.blockOnModel = get_entity_model(player_ped_id())
    blockData.componentBlocked = componentToBlock
    blockData.unblockDrawable = GET_PED_DRAWABLE_VARIATION(player_ped_id(),componentToBlock)
    blockData.unblockTexture = GET_PED_TEXTURE_VARIATION(player_ped_id(),componentToBlock)
    SET_PED_COMPONENT_VARIATION(player_ped_id(),componentToBlock,valueToSetTo,0)
ENDPROC

PROC unblockComponent()
    IF blockData.componentBlockActive
        SET_PED_COMPONENT_VARIATION(player_ped_id(),blockData.componentBlocked,blockData.unblockDrawable,blockData.unblockTexture)
        blockData.componentBlockActive = false
    ENDIF
    
    IF blockData.propBlockActive
        cprintln(debug_director," SET_PED_PROP_INDEX() a ",blockData.propBlocked," ",blockData.unblockPropVariation)
        SET_PED_PROP_INDEX(player_ped_id(),blockData.propBlocked,blockData.unblockPropVariation)
        blockData.propBlockActive = FALSE
    ENDIF
ENDPROC

PROC UPDATE_MODEL_VEHICLE_ISSUES(bool blockPedIssues)
    IF blockPedIssues
        SWITCH GET_ENTITY_MODEL(player_ped_id())
            CASE S_M_Y_FIREMAN_01
                IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) > 0             
                    blockComponent(PED_COMP_SPECIAL,0)
                ENDIF                           
            BREAK       
            CASE HC_GUNMAN
                cprintln(debug_director,"Block gunman in car: ",GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD))
                IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 3
                OR GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 4
                    blockComponent(PED_COMP_SPECIAL,0)
                ENDIF   
                IF GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD) = 1  
                    blockComponentProp(ANCHOR_HEAD,1)
                ENDIF
            BREAK
            CASE S_M_Y_FACTORY_01
                IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 0             
                    blockComponent(PED_COMP_SPECIAL,1)
                ENDIF
            BREAK
            CASE S_M_M_LINECOOK
                IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) < 2             
                    blockComponent(PED_COMP_SPECIAL,2)
                ENDIF
            BREAK
            CASE S_M_Y_CHEF_01
                IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 1             
                    blockComponent(PED_COMP_SPECIAL,0)
                ENDIF
            BREAK
        ENDSWITCH
    ELSE
        unblockComponent()  
    ENDIF
ENDPROC



PROC SET_MODEL_VARIATION(ped_index pedToChange,int dm_head,int dm_beard=0,int dm_hair=0,int dm_torso=0,int dm_leg=0,int dm_hand=0,int dm_feet=0,int dm_special=0,int dm_special2=0,int dm_decl=0, int dm_jbib=0)
    IF NOT IS_PED_INJURED(pedToChange)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HEAD,dm_head,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_BERD,dm_beard,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HAIR,dm_hair,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_TORSO,dm_torso,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_LEG,dm_leg,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HAND,dm_hand,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_FEET,dm_feet,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_SPECIAL,dm_special,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_SPECIAL2,dm_special2,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_DECL,dm_decl,0)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_JBIB,dm_jbib,0)
    ENDIF
ENDPROC

PROC SET_TEXTURE_VARIATION(ped_index pedToChange,int dm_head,int dm_beard=0,int dm_hair=0,int dm_torso=0,int dm_leg=0,int dm_hand=0,int dm_feet=0,int dm_special=0,int dm_special2=0,int dm_decl=0, int dm_jbib=0)
    IF NOT IS_PED_INJURED(pedToChange)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HEAD,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_HEAD),dm_head)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_BERD,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_BERD),dm_beard)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HAIR,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_HAIR),dm_hair)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_TORSO,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_TORSO),dm_torso)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_LEG,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_LEG),dm_leg)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_HAND,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_HAND),dm_hand)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_FEET,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_FEET),dm_feet)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_SPECIAL,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_SPECIAL),dm_special)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_SPECIAL2,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_SPECIAL2),dm_special2)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_DECL,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_DECL),dm_decl)
        SET_PED_COMPONENT_VARIATION(pedToChange,PED_COMP_JBIB,GET_PED_DRAWABLE_VARIATION(pedToChange,PED_COMP_JBIB),dm_jbib)
    ENDIF
ENDPROC

FUNC MODEL_NAMES GET_PED_VARIATION_FROM_SHORTLIST(bool useRecentlyUsedList)
    IF useRecentlyUsedList
        RETURN shortlistData[curMenuItem+10].model
    ELSE
    RETURN shortlistData[curMenuItem].model
    ENDIF
ENDFUNC

PROC SET_PED_VARIATION_FROM_SHORTLIST(int shortlistItem)
    cprintln(DEBUG_DIRECTOR,"SET_PED_VARIATION_FROM_SHORTLIST() shortlist item = ",shortlistItem)
    IF NOT IS_PED_INJURED(player_ped_id())
        if shortlistData[shortlistItem].category = MC_ONLINE1
            SET_CHAR_OUTFIT(player_ped_id(),DCO_MULTIPLAYER_1,sOnlineChar[0].outfitdata,false)
        elif shortlistData[shortlistItem].category = MC_ONLINE2
            SET_CHAR_OUTFIT(player_ped_id(),DCO_MULTIPLAYER_2,sOnlineChar[1].outfitdata,false)
        else
        
            SET_MODEL_VARIATION(player_ped_id(),
                shortlistData[shortlistItem].modelVariation[0],
                shortlistData[shortlistItem].modelVariation[1],
                shortlistData[shortlistItem].modelVariation[2],
                shortlistData[shortlistItem].modelVariation[3],
                shortlistData[shortlistItem].modelVariation[4],
                shortlistData[shortlistItem].modelVariation[5],
                shortlistData[shortlistItem].modelVariation[6],
                shortlistData[shortlistItem].modelVariation[7],
                shortlistData[shortlistItem].modelVariation[8],
                shortlistData[shortlistItem].modelVariation[9],
                shortlistData[shortlistItem].modelVariation[10])

            IF shortlistData[shortlistItem].modelVariation[11] != -1
                cprintln(debug_director," SET_PED_PROP_INDEX() b  ANCHOR_HEAD ",shortlistData[shortlistItem].modelVariation[11])
                SET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD,shortlistData[shortlistItem].modelVariation[11])
            ELSE    
                CLEAR_PED_PROP(player_ped_id(),ANCHOR_HEAD)
            ENDIF

            IF shortlistData[shortlistItem].modelVariation[12] != -1
                cprintln(debug_director," SET_PED_PROP_INDEX() c  ANCHOR_EYES ",shortlistData[shortlistItem].modelVariation[12])
                SET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES,shortlistData[shortlistItem].modelVariation[12])
            ELSE
                CLEAR_PED_PROP(player_ped_id(),ANCHOR_EYES)
            ENDIF
            
            cprintln(DEBUG_DIRECTOR,"SET_PED_VARIATION_FROM_SHORTLIST() texture variations ",shortlistData[shortlistItem].textureVariation[0]," ",shortlistData[shortlistItem].textureVariation[1]," ",shortlistData[shortlistItem].textureVariation[2]," ",shortlistData[shortlistItem].textureVariation[3],shortlistData[shortlistItem].textureVariation[4],shortlistData[shortlistItem].textureVariation[5],shortlistData[shortlistItem].textureVariation[6],shortlistData[shortlistItem].textureVariation[7],shortlistData[shortlistItem].textureVariation[8],shortlistData[shortlistItem].textureVariation[9],shortlistData[shortlistItem].textureVariation[10])
            
            SET_TEXTURE_VARIATION(player_ped_id(),
                shortlistData[shortlistItem].textureVariation[0],
                shortlistData[shortlistItem].textureVariation[1],
                shortlistData[shortlistItem].textureVariation[2],
                shortlistData[shortlistItem].textureVariation[3],
                shortlistData[shortlistItem].textureVariation[4],
                shortlistData[shortlistItem].textureVariation[5],
                shortlistData[shortlistItem].textureVariation[6],
                shortlistData[shortlistItem].textureVariation[7],
                shortlistData[shortlistItem].textureVariation[8],
                shortlistData[shortlistItem].textureVariation[9],
                shortlistData[shortlistItem].textureVariation[10])
                
            //Voice hash loading - only non-story characters
            IF shortlistData[shortlistItem].category < MC_STOFRA
            OR shortlistData[shortlistItem].category > MC_STOWAD
                IF shortlistData[shortlistItem].iVoiceHash <> 0
                    SET_AMBIENT_VOICE_NAME_HASH(PLAYER_PED_ID(),shortlistData[shortlistItem].iVoiceHash)
                    bVoiceHashSetFromShortlist = TRUE
                    CDEBUG1LN(debug_director,"SET_PED_VARIATION_FROM_SHORTLIST(): Set shortlist voice hash to ",shortlistData[shortlistItem].iVoiceHash)
                ELSE
                    shortlistData[shortlistItem].iVoiceHash = GET_AMBIENT_VOICE_NAME_HASH(PLAYER_PED_ID())
                    CDEBUG1LN(debug_director, "SET_PED_VARIATION_FROM_SHORTLIST(): Ped did not have a voice hash saved, has now been set to ", shortlistData[shortlistItem].iVoiceHash)
                ENDIF
            ENDIF
        ENDIF   
    ENDIF
        
ENDPROC


FUNC DirectorUnlockStory GET_DIRECTOR_STORY_UNLOCK_FROM_MENU_CATEGORY(enumMenuCategory category)
    SWITCH category
        CASE MC_STOFRA      RETURN DU_STORY_FRANKLIN    BREAK
        CASE MC_STOTRE      RETURN DU_STORY_TREVOR      BREAK
        CASE MC_STOMIC      RETURN DU_STORY_MICHAEL     BREAK
        CASE MC_STOAMA      RETURN DU_STORY_AMANDA_T    BREAK
        CASE MC_STOBEV      RETURN DU_STORY_BEVERLY     BREAK
        CASE MC_STOBRA      RETURN DU_STORY_BRAD        BREAK
        CASE MC_STOCHR      RETURN DU_STORY_CHRIS_F     BREAK
        CASE MC_STODAV      RETURN DU_STORY_DAVE_N      BREAK
        CASE MC_STODEV      RETURN DU_STORY_DEVIN       BREAK
        CASE MC_STODRF      RETURN DU_STORY_DR_F        BREAK
        CASE MC_STOFAB      RETURN DU_STORY_FABIEN      BREAK
        CASE MC_STOFLO      RETURN DU_STORY_FLOYD       BREAK
        CASE MC_STOJIM      RETURN DU_STORY_JIMMY       BREAK
        CASE MC_STOLAM      RETURN DU_STORY_LAMAR       BREAK
        CASE MC_STOLAZ      RETURN DU_STORY_LAZLOW      BREAK
        CASE MC_STOLES      RETURN DU_STORY_LESTER      BREAK
        CASE MC_STOMAU      RETURN DU_STORY_MAUDE       BREAK
        CASE MC_STOTHO      RETURN DU_STORY_MRS_T       BREAK
        CASE MC_STONER      RETURN DU_STORY_RON         BREAK
        CASE MC_STOPAT      RETURN DU_STORY_PATRICIA    BREAK
        CASE MC_STOSIM      RETURN DU_STORY_SIMEON      BREAK
        CASE MC_STOSOL      RETURN DU_STORY_SOLOMON     BREAK
        CASE MC_STOSTE      RETURN DU_STORY_STEVE_HAINS BREAK
        CASE MC_STOSTR      RETURN DU_STORY_STRETCH     BREAK
        CASE MC_STOTAN      RETURN DU_STORY_TANISHA     BREAK
        CASE MC_STOTAO      RETURN DU_STORY_TAO_C       BREAK
        CASE MC_STOTRA      RETURN DU_STORY_TRACEY      BREAK
        CASE MC_STOWAD      RETURN DU_STORY_WADE        BREAK
    ENDSWITCH
    RETURN DU_STORY_INVALID
ENDFUNC


FUNC DirectorUnlockHeist GET_DIRECTOR_HEIST_UNLOCK_FROM_MENU_CATEGORY(enumMenuCategory category)
    SWITCH category
        CASE MC_HEICHE      RETURN DU_HEIST_CHEF        BREAK
        CASE MC_HEICHR      RETURN DU_HEIST_CHRISTIAN   BREAK
        CASE MC_HEIDAR      RETURN DU_HEIST_DARYL       BREAK
        CASE MC_HEIEDD      RETURN DU_HEIST_EDDIE       BREAK
        CASE MC_HEIGUS      RETURN DU_HEIST_GUSTAV      BREAK
        CASE MC_HEIHUG      RETURN DU_HEIST_HUGH        BREAK
        CASE MC_HEIKRM      RETURN DU_HEIST_KARIM       BREAK
        CASE MC_HEIKAR      RETURN DU_HEIST_KARL        BREAK
        CASE MC_HEINOR      RETURN DU_HEIST_NORM        BREAK
        CASE MC_HEIPAC      RETURN DU_HEIST_PACKIE      BREAK
        CASE MC_HEIPAI      RETURN DU_HEIST_PAIGE       BREAK
        CASE MC_HEIRIC      RETURN DU_HEIST_RICKIE      BREAK
        CASE MC_HEITAL      RETURN DU_HEIST_TALINA      BREAK
    ENDSWITCH
    RETURN DU_HEIST_INVALID
ENDFUNC


FUNC DirectorUnlockSpecial GET_DIRECTOR_SPECIAL_UNLOCK_FROM_MENU_CATEGORY(enumMenuCategory category)
    SWITCH category
        CASE MC_SPEAND      RETURN DU_SPECIAL_ANDYMOON      BREAK
        CASE MC_SPEBAY      RETURN DU_SPECIAL_BAYGOR        BREAK
        CASE MC_SPEBIL      RETURN DU_SPECIAL_BILLBINDER    BREAK
        CASE MC_SPECLI      RETURN DU_SPECIAL_CLINTON       BREAK
        CASE MC_SPEGRI      RETURN DU_SPECIAL_GRIFF         BREAK
        CASE MC_SPEJAN      RETURN DU_SPECIAL_JANE          BREAK
        CASE MC_SPEJER      RETURN DU_SPECIAL_JEROME        BREAK
        CASE MC_SPEJES      RETURN DU_SPECIAL_JESSE         BREAK
        CASE MC_SPEMAN      RETURN DU_SPECIAL_MANI          BREAK
        CASE MC_SPEMIM      RETURN DU_SPECIAL_MIME          BREAK
        CASE MC_SPEPAM      RETURN DU_SPECIAL_PAMELADRAKE   BREAK
        CASE MC_SPEIMP      RETURN DU_SPECIAL_SUPERHERO     BREAK
        CASE MC_SPEZOM      RETURN DU_SPECIAL_ZOMBIE        BREAK
    ENDSWITCH
    RETURN DU_SPECIAL_INVALID
ENDFUNC


FUNC DirectorUnlockAnimal GET_DIRECTOR_ANIMAL_UNLOCK_FROM_MENU_CATEGORY(enumMenuCategory category)
    SWITCH category
        CASE MC_ANIBOAR     RETURN DU_ANIMAL_BOAR           BREAK
        CASE MC_ANICAT      RETURN DU_ANIMAL_CAT            BREAK
        CASE MC_ANICOW      RETURN DU_ANIMAL_COW            BREAK
        CASE MC_ANICOY      RETURN DU_ANIMAL_COYOTE         BREAK
        CASE MC_ANIDEE      RETURN DU_ANIMAL_DEER           BREAK
        CASE MC_ANIHEN      RETURN DU_ANIMAL_HEN            BREAK
        CASE MC_ANIHUS      RETURN DU_ANIMAL_HUSKY          BREAK
        CASE MC_ANIMOU      RETURN DU_ANIMAL_MTLION         BREAK
        CASE MC_ANIPIG      RETURN DU_ANIMAL_PIG            BREAK
        CASE MC_ANIPOO      RETURN DU_ANIMAL_POODLE         BREAK
        CASE MC_ANIPUG      RETURN DU_ANIMAL_PUG            BREAK
        CASE MC_ANIRAB      RETURN DU_ANIMAL_RABBIT         BREAK
        CASE MC_ANIRET      RETURN DU_ANIMAL_RETRIEVER      BREAK
        CASE MC_ANIROT      RETURN DU_ANIMAL_ROTTWEILER     BREAK
        CASE MC_ANISHE      RETURN DU_ANIMAL_SHEPHERD       BREAK
        CASE MC_ANIWES      RETURN DU_ANIMAL_WESTY          BREAK
        CASE MC_ANICHI      RETURN DU_ANIMAL_CHICKENHAWK    BREAK
        CASE MC_ANICOR      RETURN DU_ANIMAL_CORMORANT      BREAK
        CASE MC_ANICRO      RETURN DU_ANIMAL_CROW           BREAK
        CASE MC_ANIPIN      RETURN DU_ANIMAL_PIGEON         BREAK
        CASE MC_ANISEA      RETURN DU_ANIMAL_SEAGULL        BREAK
        CASE MC_ANISAS      RETURN DU_ANIMAL_SASQUATCH      BREAK
    ENDSWITCH
    RETURN DU_ANIMAL_INVALID
ENDFUNC


FUNC BOOL IS_MENU_CATEGORY_VISIBLE(enumMenuCategory category)
    
    #IF IS_DEBUG_BUILD
        if category  != MC_ONLINE
        and category != MC_ONLINE1
        and category != MC_ONLINE2
            IF bUnlockAll 
                RETURN TRUE 
            ENDIF
        ENDIF
    #ENDIF
    
    // Check if this menu category is a story unlock. 
    // If so query to see if the character has been met in the story.
    // NB. We don't want to reveal story characters early so they shouldn't 
    // appear in the menu until they are unlocked.
    DirectorUnlockStory eStoryUnlock = GET_DIRECTOR_STORY_UNLOCK_FROM_MENU_CATEGORY(category)
    IF eStoryUnlock != DU_STORY_INVALID
        IF NOT IS_DIRECTOR_STORY_CHARACTER_UNLOCKED(eStoryUnlock)
            RETURN FALSE
        ENDIF
    ENDIF
    
    // Check if this menu category is an animal unlock. 
    // If so query to see if the animal has been experienced through a Peyote trip yet.
    // NB. These are secret enough that they shouldn't appear in the menu until 
    // they are unlocked.
    DirectorUnlockAnimal eAnimalUnlock = GET_DIRECTOR_ANIMAL_UNLOCK_FROM_MENU_CATEGORY(category)
    IF eAnimalUnlock != DU_ANIMAL_INVALID
        IF NOT IS_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(eAnimalUnlock)
            RETURN FALSE
        ENDIF
    ENDIF
    
    RETURN TRUE
ENDFUNC


FUNC BOOL IS_MENU_CATEGORY_SELECTABLE(enumMenuCategory category)

    //Settings menu is not selectable
    IF category = MC_SETTINGS
        RETURN FALSE
    ENDIF

    //Settings menu is not selectable
    IF category = MC_VOID
        RETURN FALSE
    ENDIF
    
    #IF IS_DEBUG_BUILD        
        if category  != MC_ONLINE
        and category != MC_ONLINE1
        and category != MC_ONLINE2
            IF bUnlockAll 
                RETURN TRUE 
            ENDIF
        ENDIF   
    #ENDIF
    
    // Check if this menu category is a heist crew member unlock. 
    // If so query to see if the character has been available on a heist yet.
    DirectorUnlockHeist eHeistUnlock = GET_DIRECTOR_HEIST_UNLOCK_FROM_MENU_CATEGORY(category)
    IF eHeistUnlock != DU_HEIST_INVALID
        IF NOT IS_DIRECTOR_HEIST_CHARACTER_UNLOCKED(eHeistUnlock)
            RETURN FALSE
        ENDIF
    ENDIF
    
    // Check if this menu category is a special ped unlock. 
    // If so query to see if the character has been met in the world yet.
    DirectorUnlockSpecial eSpecialUnlock = GET_DIRECTOR_SPECIAL_UNLOCK_FROM_MENU_CATEGORY(category)
    IF eSpecialUnlock != DU_SPECIAL_INVALID
        IF NOT IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(eSpecialUnlock)
            RETURN FALSE
        ENDIF
    ENDIF
    
    if category = MC_ONLINE1
        if !sOnlineChar[0].bSetup   
            RETURN FALSE
        endif
    elif category = MC_ONLINE2  
        if !sOnlineChar[1].bSetup   
            RETURN FALSE
        endif   
    endif
    
    RETURN TRUE
ENDFUNC




FUNC BOOL ShortlistPed(enumMenuCategory category,int index=0,bool addToRecentlyUsedList =false, bool playSound = TRUE)
    IF (IS_MENU_CATEGORY_VISIBLE(category)
    AND IS_MENU_CATEGORY_SELECTABLE(category))
    OR addToRecentlyUsedList //recently used will get added when player enters game as an actor, so not dependant on menu unlocks.
        cprintln(DEBUG_DIRECTOR,"Shortlist ped: ",category," model = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(player_ped_id()))," : activeMenuEntry: ",activeMenuEntry," to recent:",addToRecentlyUsedList)
        structShortlistData newShortlistPedData
        IF playSound
            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)    
        ENDIF
        newShortlistPedData.category = category
        newShortlistPedData.modelVariationIndex = index
        
        IF NOT IS_PED_INJURED(player_ped_id())
            newShortlistPedData.model = GET_ENTITY_MODEL(player_ped_id())
            newShortlistPedData.modelVariation[0] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD)
            newShortlistPedData.modelVariation[1] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_BERD)
            newShortlistPedData.modelVariation[2] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAIR)
            newShortlistPedData.modelVariation[3] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
            newShortlistPedData.modelVariation[4] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_LEG)
            newShortlistPedData.modelVariation[5] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HAND)
            newShortlistPedData.modelVariation[6] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_FEET)
            newShortlistPedData.modelVariation[7] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
            newShortlistPedData.modelVariation[8] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
            newShortlistPedData.modelVariation[9] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_DECL)
            newShortlistPedData.modelVariation[10] = GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_JBIB)
            newShortlistPedData.modelVariation[11] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_HEAD)
            newShortlistPedData.modelVariation[12] = GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES)            
            
            newShortlistPedData.textureVariation[0] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HEAD)
            newShortlistPedData.textureVariation[1] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_BERD)
            newShortlistPedData.textureVariation[2] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAIR)
            newShortlistPedData.textureVariation[3] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_TORSO)
            newShortlistPedData.textureVariation[4] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_LEG)
            newShortlistPedData.textureVariation[5] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HAND)
            newShortlistPedData.textureVariation[6] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_FEET)
            newShortlistPedData.textureVariation[7] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL)
            newShortlistPedData.textureVariation[8] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2)
            newShortlistPedData.textureVariation[9] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_DECL)
            newShortlistPedData.textureVariation[10] = GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_JBIB)
            newShortlistPedData.bMale = IS_DIRECTOR_PED_MALE(newShortlistPedData.model)
            
            //Save current voice hash
            newShortlistPedData.iVoiceHash = GET_AMBIENT_VOICE_NAME_HASH(player_ped_id()) 
            CDEBUG1LN(debug_director,"Saved shortlist voice hash: ",newShortlistPedData.iVoiceHash)
            
            
            int iShortlistExistingEntry = GET_EXISTING_PED_SHORTLIST(addToRecentlyUsedList)
                        
                    
            //Run through current shortlist if any same online categorys exist dont add.            
            IF newShortlistPedData.category = MC_ONLINE1 
            OR newShortlistPedData.category = MC_ONLINE2    
                iShortlistExistingEntry = -1
                int iDupCheck
                int iStart
                int iEnd
                
                IF addToRecentlyUsedList
                    iStart = 10
                    iEnd = 19
                else
                    iStart = 0
                    iEnd = 9
                endif
                                        
                for iDupCheck = iStart to iEnd
                    IF shortlistData[iDupCheck].category = newShortlistPedData.category 
                        iShortlistExistingEntry = iDupCheck
                    ENDIF
                endfor              
            ENDIF       
                        
            IF iShortlistExistingEntry = -1 //this ped doesn't already exist in the list                                
                IF addToRecentlyUsedList
                    //if it's from the recently used, just bump the oldest one off the bottom of the list.
                    nudgeShortlistDown(addToRecentlyUsedList)
                    shortlistData[10] = newShortlistPedData
                ELSE
                    int iFirstFreeShortlist = get_first_free_shortlist_slot(addToRecentlyUsedList)
                    //if it's from the shortlist, check if shortlist is full and if so return FALSE
                    IF iFirstFreeShortlist = -1
                        RETURN FALSE
                    ELSE
                        shortlistData[iFirstFreeShortlist] = newShortlistPedData
                    ENDIF
                ENDIF               
                                                                
            ENDIF
        
        ENDIF   
        
    ENDIF
    RETURN TRUE
                    
ENDFUNC
        
FUNC STRING GET_CATEGORY_TITLE(enumMenuCategory category)
            
	#IF FEATURE_GEN9_STANDALONE
	SWITCH category
		CASE MC_QUIT_TO_MAIN_MENU   RETURN "UI_FLOW_DM_QUITMMLP_M" BREAK
		CASE MC_QUIT_TO_STORY	    RETURN "UI_FLOW_DM_QUITCMSM_M" BREAK
	ENDSWITCH
	#ENDIF
			
    SWITCH category

        CASE MC_ACTORS              RETURN "CM_TOP1" BREAK
        CASE MC_SETTINGS            RETURN "CM_TOP2" BREAK
        CASE MC_SHORTLIST           RETURN "CM_TOP3" BREAK
        CASE MC_RECENTLYUSED        RETURN "CM_TOP4" BREAK
        CASE MC_QUIT_TO_STORY       RETURN "CM_TOP5" BREAK
        CASE MC_QUIT_TO_GTA_ONLINE  RETURN "CM_TOP6" BREAK
        CASE MC_PLAYDM              RETURN "CM_TOP7" BREAK

        CASE MC_ANIMALS             RETURN "CM_LISANI" BREAK
        CASE MC_BEACH               RETURN "CM_LISBEA" BREAK
        CASE MC_COSTUME             RETURN "CM_LISCOS" BREAK
        CASE MC_DOWNTOWN            RETURN "CM_LISDOW" BREAK
        CASE MC_EMERGENCY           RETURN "CM_LISEME" BREAK
        CASE MC_GANG                RETURN "CM_LISGAN" BREAK
        CASE MC_HEIST               RETURN "CM_LISHEI" BREAK
        CASE MC_LABOURERS           RETURN "CM_LISLAB" BREAK
        CASE MC_MILITARY            RETURN "CM_LISMIL" BREAK
        CASE MC_ONLINE              RETURN "CM_LISONL" BREAK
        CASE MC_PROFESSIONALS       RETURN "CM_LISPRO" BREAK
        CASE MC_SPECIAL             RETURN "CM_LISSPE" BREAK
        CASE MC_SPORT               RETURN "CM_LISSPO" BREAK
        CASE MC_STORY               RETURN "CM_LISSTO" BREAK
        CASE MC_TRANSPORT           RETURN "CM_LISTRA" BREAK
        CASE MC_UPTOWN              RETURN "CM_LISUPT" BREAK
        CASE MC_VAGRANTS            RETURN "CM_LISVAG" BREAK

        CASE    MC_PROAIR   RETURN "CM_PROAIR" BREAK
        CASE    MC_PROBAR   RETURN "CM_PROBAR" BREAK
        CASE    MC_PROBOU   RETURN "CM_PROBOU" BREAK
        CASE    MC_PROCEO   RETURN "CM_PROCEO" BREAK
        CASE    MC_PROCHE   RETURN "CM_PROCHE" BREAK
        CASE    MC_PROCOO   RETURN "CM_PROCOO" BREAK
        CASE    MC_PRODOC   RETURN "CM_PRODOC" BREAK
        CASE    MC_PRODOR   RETURN "CM_PRODOR" BREAK
        CASE    MC_PRODRA   RETURN "CM_PRODRA" BREAK
        CASE    MC_PROIT    RETURN "CM_PROIT" BREAK
        CASE    MC_LABMAI   RETURN "CM_PROMAI" BREAK
        CASE    MC_PROMAR   RETURN "CM_PROMAR" BREAK
        CASE    MC_PROOFF   RETURN "CM_PROOFF" BREAK
        CASE    MC_PROSCI   RETURN "CM_PROSCI" BREAK
        CASE    MC_PROSTY   RETURN "CM_PROSTY" BREAK
        CASE    MC_PROVAL   RETURN "CM_PROVAL" BREAK
        CASE    MC_PROVEN   RETURN "CM_PROVEN" BREAK
        CASE    MC_PROWAI   RETURN "CM_PROWAI" BREAK
        CASE    MC_PROAIH   RETURN "CM_PROAIH" BREAK
        CASE    MC_PROAUP   RETURN "CM_PROAUP" BREAK
        CASE    MC_PROBIN   RETURN "CM_PROBIN" BREAK
        CASE    MC_PROFAS   RETURN "CM_PROFAS" BREAK
        CASE    MC_PROHAI   RETURN "CM_PROHAI" BREAK
        CASE    MC_PROINT   RETURN "CM_PROINT" BREAK
        CASE    MC_PROLAW   RETURN "CM_PROLAW" BREAK
        CASE    MC_PROPON   RETURN "CM_PROPON" BREAK
        CASE    MC_PROREL   RETURN "CM_PROREL" BREAK
        CASE    MC_PROSTR   RETURN "CM_PROSTR" BREAK
        CASE    MC_PROTEA   RETURN "CM_PROTEA" BREAK
        CASE    MC_UPTEAS   RETURN "CM_UPTEAS" BREAK
        CASE    MC_UPTECC   RETURN "CM_UPTECC" BREAK
        CASE    MC_UPTHAW   RETURN "CM_UPTHAW" BREAK
        CASE    MC_UPTMIR   RETURN "CM_UPTMIR" BREAK
        CASE    MC_UPTHIP   RETURN "CM_UPTHIP" BREAK 
        CASE    MC_UPTCOU   RETURN "CM_UPTCOU" BREAK
        CASE    MC_UPTROC   RETURN "CM_UPTROC" BREAK
        CASE    MC_UPTVIN   RETURN "CM_UPTVIN" BREAK
        CASE    MC_UPTART   RETURN "CM_UPTART" BREAK
        CASE    MC_UPTBOH   RETURN "CM_UPTBOH" BREAK
        CASE    MC_UPTPOR   RETURN "CM_UPTPOR" BREAK
        CASE    MC_UPTRIC   RETURN "CM_UPTRIC" BREAK
        CASE    MC_UPTROH   RETURN "CM_UPTROH" BREAK
        CASE    MC_UPTVIB   RETURN "CM_UPTVIB" BREAK
        CASE    MC_UPTWAN   RETURN "CM_UPTWAN" BREAK
        CASE    MC_DOWMAZ   RETURN "CM_DOWSOU" BREAK
        CASE    MC_DOWLAS   RETURN "CM_DOWEL1" BREAK
        CASE    MC_DOWSTR   RETURN "CM_DOWDAD" BREAK
        CASE    MC_DOWRAN   RETURN "CM_DOWEL2" BREAK
        CASE    MC_DOWDAL   RETURN "CM_DOWSTM" BREAK
        CASE    MC_DOWHAR   RETURN "CM_DOWCOU" BREAK
        CASE    MC_DOWCO2   RETURN "CM_DOWCO2" BREAK
        CASE    MC_DOWSTM   RETURN "CM_DOWSTR" BREAK
        CASE    MC_DOWEMO   RETURN "CM_DOWEMO" BREAK
        CASE    MC_DOWEM2   RETURN "CM_DOWEM2" BREAK
        CASE    MC_DOWTEE   RETURN "CM_DOWTEE" BREAK
        CASE    MC_DOWCOL   RETURN "CM_DOWCOL" BREAK
        CASE    MC_DOWSOC   RETURN "CM_DOWSOC" BREAK
        CASE    MC_DOWDAV   RETURN "CM_DOWDAV" BREAK
        CASE    MC_VAGMIS   RETURN "CM_VAGMIS" BREAK
        CASE    MC_VAGDO1   RETURN "CM_VAGDO1" BREAK
        CASE    MC_VAGDO2   RETURN "CM_VAGDO2" BREAK
        CASE    MC_VAGMET   RETURN "CM_VAGMET" BREAK
        CASE    MC_VAGSAN   RETURN "CM_VAGSAN" BREAK
        CASE    MC_VAGME2   RETURN "CM_VAGME2" BREAK
        CASE    MC_VAGOLD   RETURN "CM_VAGOLD" BREAK
        CASE    MC_VAGYOU   RETURN "CM_VAGYOU" BREAK
        CASE    MC_VAGPR1   RETURN "CM_VAGPR1" BREAK
        CASE    MC_VAGPR2   RETURN "CM_VAGPR2" BREAK
        CASE    MC_BEALOA   RETURN "CM_BEALOA" BREAK
        CASE    MC_BEAOLD   RETURN "CM_BEAOLD" BREAK
        CASE    MC_BEAVES   RETURN "CM_BEAVES" BREAK
        CASE    MC_BEADEL   RETURN "CM_BEADEL" BREAK
        CASE    MC_BEASUN   RETURN "CM_BEASUN" BREAK
        CASE    MC_BEASRF   RETURN "CM_BEABEA" BREAK
        CASE    MC_BEABO1   RETURN "CM_BEAMU1" BREAK
        CASE    MC_BEABO2   RETURN "CM_BEAMU2" BREAK
        CASE    MC_BEATOU   RETURN "CM_BEATOU" BREAK
        CASE    MC_BEABOD   RETURN "CM_BEAMUS" BREAK
        CASE MC_SPOYOG RETURN "CM_SPOYOG" BREAK
        CASE MC_SPOSUR RETURN "CM_SPOSUR" BREAK
        CASE MC_SPOSKA RETURN "CM_SPOSKA" BREAK
        CASE    MC_SPOSK1   RETURN "CM_SPOSK1" BREAK
        CASE    MC_SPOSK2   RETURN "CM_SPOSK2" BREAK
        CASE    MC_SPOSK3   RETURN "CM_SPOSK3" BREAK
        CASE    MC_SPOSK4   RETURN "CM_SPOSK4" BREAK
        CASE    MC_SPOSK5   RETURN "CM_SPOSK5" BREAK
        CASE    MC_SPORU1   RETURN "CM_SPORU1" BREAK
        CASE    MC_SPORU2   RETURN "CM_SPORU2" BREAK
        CASE MC_SPORUN RETURN "CM_SPORUN" BREAK
        CASE    MC_SPOCYC   RETURN "CM_SPOCYC" BREAK
        CASE    MC_SPOROC   RETURN "CM_SPOROC" BREAK
        CASE    MC_SPOMO1   RETURN "CM_SPOMO1" BREAK
        CASE    MC_SPOMO2   RETURN "CM_SPOMO2" BREAK
        CASE MC_SPOTEN RETURN "CM_SPOTEN" BREAK
        CASE    MC_SPOGO1   RETURN "CM_SPOGO1" BREAK
        CASE    MC_SPOGO2   RETURN "CM_SPOGO2" BREAK
        CASE    MC_SPOGOF   RETURN "CM_SPOGOF" BREAK
    ENDSWITCH

    SWITCH category
        CASE    MC_GANTRB   RETURN "CM_GANTRB" BREAK
        CASE    MC_GANTG1   RETURN "CM_GANTG1" BREAK
        CASE    MC_GANTG2   RETURN "CM_GANTG2" BREAK
        CASE    MC_GANKOB   RETURN "CM_GANKOB" BREAK
        CASE    MC_GANKOL   RETURN "CM_GANKOL" BREAK
        CASE    MC_GANKOG   RETURN "CM_GANKOG" BREAK
        CASE    MC_GANCHO   RETURN "CM_GANCHO" BREAK
        CASE    MC_GANEPS1  RETURN "CM_GANEPS1" BREAK
        CASE    MC_GANEPS2  RETURN "CM_GANEPS2" BREAK
        CASE    MC_GANEPSF  RETURN "CM_GANEPSF" BREAK
        CASE    MC_GANSTR   RETURN "CM_GANSTR" BREAK
        CASE    MC_GANAZB   RETURN "CM_GANAZB" BREAK
        CASE    MC_GANVAB   RETURN "CM_GANVAB" BREAK
        CASE    MC_GANAZG   RETURN "CM_GANAZG" BREAK
        CASE    MC_GANVAG   RETURN "CM_GANVAG" BREAK
        CASE    MC_GANMGB   RETURN "CM_GANMGB" BREAK
        CASE    MC_GANMGG   RETURN "CM_GANMGG" BREAK
        CASE    MC_GANMGL   RETURN "CM_GANMGL" BREAK
        CASE    MC_GANMOB   RETURN "CM_GANMOB" BREAK
        CASE    MC_GANMOG   RETURN "CM_GANMOG" BREAK
        CASE    MC_GANMOL   RETURN "CM_GANMOL" BREAK
        CASE    MC_GANLOO   RETURN "CM_GANLOO" BREAK
        CASE    MC_GANLOM   RETURN "CM_GANLOM" BREAK
        CASE    MC_GANLOS   RETURN "CM_GANLOS" BREAK
        CASE    MC_GANLOH   RETURN "CM_GANLOH" BREAK
        CASE    MC_GANBAE   RETURN "CM_GANBAE" BREAK
        CASE    MC_GANBLO   RETURN "CM_GANBLO" BREAK
        CASE    MC_GANOGB   RETURN "CM_GANOGB" BREAK
        CASE    MC_GANBAS   RETURN "CM_GANBAS" BREAK
        CASE    MC_GANCHA   RETURN "CM_GANCHA" BREAK
        CASE    MC_GANDAV   RETURN "CM_GANDAV" BREAK
        CASE    MC_GANFOR   RETURN "CM_GANFOR" BREAK
        CASE    MC_GANALT   RETURN "CM_GANALT" BREAK
        CASE    MC_GANAL1   RETURN "CM_GANAL1" BREAK
        CASE    MC_GANAL2   RETURN "CM_GANAL2" BREAK
        CASE    MC_GANAL3   RETURN "CM_GANAL3" BREAK
        CASE    MC_COSAST   RETURN "CM_COSAST" BREAK
        CASE    MC_COSSPY   RETURN "CM_COSSPY" BREAK
        CASE    MC_COSCOR   RETURN "CM_COSCOR" BREAK
        CASE    MC_COSPOG   RETURN "CM_COSPOG" BREAK
        CASE    MC_COSSPA   RETURN "CM_COSSPA" BREAK
        CASE    MC_COSALI   RETURN "CM_COSALI" BREAK
        CASE    MC_COSCLO   RETURN "CM_COSCLO" BREAK
        CASE    MC_COSMIM   RETURN "CM_COSMIM" BREAK
        CASE    MC_COSSTR   RETURN "CM_COSSTR" BREAK
        CASE    MC_MILPL1   RETURN "CM_MILPL1" BREAK
        CASE    MC_MILPL2   RETURN "CM_MILPL2" BREAK
        CASE    MC_MILMIG   RETURN "CM_MILMIG" BREAK
        CASE    MC_MILSRG   RETURN "CM_MILSRG" BREAK
        CASE    MC_MILCOR   RETURN "CM_MILCOR" BREAK
        CASE    MC_MILPRI   RETURN "CM_MILPRI" BREAK
        CASE    MC_MILBAT   RETURN "CM_MILBAT" BREAK
        CASE    MC_MILMIM   RETURN "CM_MILMIM" BREAK
        CASE    MC_MILMEP   RETURN "CM_MILMEP" BREAK
        CASE    MC_MILMEC   RETURN "CM_MILMEC" BREAK
        CASE    MC_MILMEL   RETURN "CM_MILMEL" BREAK
        CASE    MC_LABFAR   RETURN "CM_LABFAR" BREAK
        CASE    MC_LABMIG   RETURN "CM_LABMIG" BREAK
        CASE    MC_LABCN1   RETURN "CM_LABCN1" BREAK
        CASE    MC_LABCN2   RETURN "CM_LABCN2" BREAK
        CASE    MC_LABCN3   RETURN "CM_LABCN3" BREAK
        CASE    MC_LABGAB   RETURN "CM_LABGAB" BREAK
        CASE    MC_LABFAC   RETURN "CM_LABFAC" BREAK
        CASE    MC_LABGAR   RETURN "CM_LABGAR" BREAK
        CASE    MC_LABJAN   RETURN "CM_LABJAN" BREAK
        CASE    MC_LABSAD   RETURN "CM_LABSAD" BREAK
        CASE    MC_LABDOC   RETURN "CM_LABDOC" BREAK
        CASE    MC_LABME1   RETURN "CM_LABME1" BREAK
        CASE    MC_LABME2   RETURN "CM_LABME2" BREAK
        CASE    MC_LABBUG   RETURN "CM_LABBUG" BREAK
        CASE    MC_EMEFIR   RETURN "CM_EMEFIR" BREAK
        CASE    MC_EMEPOL   RETURN "CM_EMEPOL" BREAK
        CASE    MC_EMEFIB   RETURN "CM_EMEFIB" BREAK
        CASE    MC_EMENOO   RETURN "CM_EMENOO" BREAK
        CASE    MC_EMEDOA   RETURN "CM_EMEDOA" BREAK
        CASE    MC_EMEIAA   RETURN "CM_EMEIAA" BREAK
        CASE    MC_EMEHIG   RETURN "CM_EMEHIG" BREAK
        CASE    MC_EMEPAR   RETURN "CM_EMEPAR" BREAK
        CASE    MC_EMECOA   RETURN "CM_EMECOA" BREAK
        CASE    MC_EMERAN   RETURN "CM_EMERAN" BREAK
        CASE    MC_EMEBAY   RETURN "CM_EMEBAY" BREAK
        CASE    MC_EMESHE   RETURN "CM_EMESHE" BREAK
        CASE    MC_TRAAR1   RETURN "CM_TRAAR1" BREAK
        CASE    MC_TRAAR2   RETURN "CM_TRAAR2" BREAK
        CASE    MC_TRAAIR   RETURN "CM_TRAAIR" BREAK
        CASE    MC_TRAPO1   RETURN "CM_TRAPO1" BREAK
        CASE    MC_TRAPO2   RETURN "CM_TRAPO2" BREAK
        CASE    MC_TRAPO3   RETURN "CM_TRAPO3" BREAK
        CASE    MC_TRAPO4   RETURN "CM_TRAPO4" BREAK
        CASE    MC_TRATRA   RETURN "CM_TRATRA" BREAK
        CASE    MC_TRATRU   RETURN "CM_TRATRU" BREAK
        CASE    MC_ONLINE1  RETURN "CM_ONLINE1" BREAK
        CASE    MC_ONLINE2  RETURN "CM_ONLINE2" BREAK

    ENDSWITCH

    SWITCH category

        CASE MC_STOFRA RETURN "CM_STOFRA" BREAK
        CASE MC_STOTRE RETURN "CM_STOTRE" BREAK
        CASE MC_STOMIC RETURN "CM_STOMIC" BREAK
        CASE MC_STOAMA RETURN "CM_STOAMA" BREAK
        CASE MC_STOBEV RETURN "CM_STOBEV" BREAK
        CASE MC_STOBRA RETURN "CM_STOBRA" BREAK
        CASE MC_STOCHR RETURN "CM_STOCHR" BREAK
        CASE MC_STODAV RETURN "CM_STODAV" BREAK
        CASE MC_STODEV RETURN "CM_STODEV" BREAK
        CASE MC_STODRF RETURN "CM_STODRF" BREAK
        CASE MC_STOFAB RETURN "CM_STOFAB" BREAK
        CASE MC_STOFLO RETURN "CM_STOFLO" BREAK
        CASE MC_STOJIM RETURN "CM_STOJIM" BREAK
        CASE MC_STOLAM RETURN "CM_STOLAM" BREAK
        CASE MC_STOLAZ RETURN "CM_STOLAZ" BREAK
        CASE MC_STOLES RETURN "CM_STOLES" BREAK
        CASE MC_STOMAU RETURN "CM_STOMAU" BREAK
        CASE MC_STOTHO RETURN "CM_STOTHO" BREAK
        CASE MC_STONER RETURN "CM_STONER" BREAK
        CASE MC_STOPAT RETURN "CM_STOPAT" BREAK
        CASE MC_STOSIM RETURN "CM_STOSIM" BREAK
        CASE MC_STOSOL RETURN "CM_STOSOL" BREAK
        CASE MC_STOSTE RETURN "CM_STOSTE" BREAK
        CASE MC_STOSTR RETURN "CM_STOSTR" BREAK
        CASE MC_STOTAN RETURN "CM_STOTAN" BREAK
        CASE MC_STOTAO RETURN "CM_STOTAO" BREAK
        CASE MC_STOTRA RETURN "CM_STOTRA" BREAK
        CASE MC_STOWAD RETURN "CM_STOWAD" BREAK

        CASE MC_SPEAND RETURN "CM_SPEAND" BREAK
        CASE MC_SPEBAY RETURN "CM_SPEBAY" BREAK
        CASE MC_SPEBIL RETURN "CM_SPEBIL" BREAK
        CASE MC_SPECLI RETURN "CM_SPECLI" BREAK
        CASE MC_SPEGRI RETURN "CM_SPEGRI" BREAK
        CASE MC_SPEJAN RETURN "CM_SPEJAN" BREAK
        CASE MC_SPEJER RETURN "CM_SPEJER" BREAK
        CASE MC_SPEJES RETURN "CM_SPEJES" BREAK
        CASE MC_SPEMAN RETURN "CM_SPEMAN" BREAK
        CASE MC_SPEMIM RETURN "CM_SPEMIM" BREAK
        CASE MC_SPEPAM RETURN "CM_SPEPAM" BREAK
        CASE MC_SPEIMP RETURN "CM_SPEIMP" BREAK
        CASE MC_SPEZOM RETURN "CM_SPEZOM" BREAK
        
        CASE MC_HEIGUS RETURN "CM_HSTGUS" BREAK
        CASE MC_HEIKAR RETURN "CM_HSTKAR" BREAK
        CASE MC_HEIPAC RETURN "CM_HSTPAC" BREAK
        CASE MC_HEICHE RETURN "CM_HSTCHE" BREAK
        CASE MC_HEIHUG RETURN "CM_HSTHUG" BREAK
        CASE MC_HEINOR RETURN "CM_HSTNOR" BREAK
        CASE MC_HEIDAR RETURN "CM_HSTDAR" BREAK
        CASE MC_HEIPAI RETURN "CM_HSTPAI" BREAK
        CASE MC_HEICHR RETURN "CM_HSTCHR" BREAK
        CASE MC_HEIRIC RETURN "CM_HSTRIC" BREAK
        CASE MC_HEIEDD RETURN "CM_HSTEDD" BREAK
        CASE MC_HEITAL RETURN "CM_HSTTAL" BREAK
        CASE MC_HEIKRM RETURN "CM_HSTKRM" BREAK 
        
        CASE MC_ANIBOAR RETURN "CM_ANIBOAR" BREAK
        CASE MC_ANICAT RETURN "CM_ANICAT" BREAK
        CASE MC_ANICOW RETURN "CM_ANICOW" BREAK 
        CASE MC_ANICOY RETURN "CM_ANICOY" BREAK 
        CASE MC_ANIDEE RETURN "CM_ANIDEE" BREAK 
        CASE MC_ANIHUS RETURN "CM_ANIHUS" BREAK 
        CASE MC_ANIMOU RETURN "CM_ANIMOU" BREAK 
        CASE MC_ANIPIG RETURN "CM_ANIPIG" BREAK 
        CASE MC_ANIPOO RETURN "CM_ANIPOO" BREAK 
        CASE MC_ANIPUG RETURN "CM_ANIPUG" BREAK 
        CASE MC_ANIRAB RETURN "CM_ANIRAB" BREAK 
        CASE MC_ANIRET RETURN "CM_ANIRET" BREAK 
        CASE MC_ANIROT RETURN "CM_ANIROT" BREAK 
        CASE MC_ANISHE RETURN "CM_ANISHE" BREAK 
        CASE MC_ANIWES RETURN "CM_ANIWES" BREAK 
        CASE MC_ANICHI RETURN "CM_ANICHI" BREAK 
        CASE MC_ANICOR RETURN "CM_ANICOR" BREAK
        CASE MC_ANICRO RETURN "CM_ANICRO" BREAK 
        CASE MC_ANIHEN RETURN "CM_ANIHEN" BREAK 
        CASE MC_ANIPIN RETURN "CM_ANIPGN" BREAK 
        CASE MC_ANISEA RETURN "CM_ANISEA" BREAK
        CASE MC_ANISAS RETURN "HAIR_GROUP_E0" BREAK //deliberately misleading text file
    ENDSWITCH
    RETURN ""
ENDFUNC

FUNC TEXT_LABEL_31 GET_CATEGORY_HELP(enumMenuCategory category)
	TEXT_LABEL_31 tHelpDesc
	
	#IF FEATURE_GEN9_STANDALONE
	SWITCH category
		CASE MC_QUIT_TO_MAIN_MENU   
			tHelpDesc = "UI_FLOW_DM_QUITMMLP_D"
		BREAK
		CASE MC_QUIT_TO_STORY
			tHelpDesc = "UI_FLOW_DM_QUITCMSM_D"
		BREAK
		DEFAULT
			tHelpDesc = GET_CATEGORY_TITLE(category)
    		tHelpDesc += "HLP"	
		BREAK
	ENDSWITCH
	#ENDIF
	
	#IF NOT FEATURE_GEN9_STANDALONE
	tHelpDesc = GET_CATEGORY_TITLE(category)
	tHelpDesc += "HLP"
	#ENDIF
	RETURN tHelpDesc
ENDFUNC

FUNC BOOL DOES_CATEGORY_NEED_FEMININE_VERSION_OF_TEXT_LABEL(enumMenuCategory category)
    SWITCH category
        CASE MC_PROVEN
        CASE MC_PROSTY
        CASE MC_PROOFF
        CASE MC_PROCEO
        CASE MC_DOWHAR
        CASE MC_SPOYOG
        CASE MC_SPOTEN
        CASE MC_LABMIG
        CASE MC_LABFAC
        CASE MC_EMEPOL
        CASE MC_VAGSAN
        CASE MC_VAGDO1
        CASE MC_VAGDO2
            RETURN TRUE
        BREAK       
    ENDSWITCH
    RETURN FALSE
ENDFUNC

FUNC BOOL DOES_CATEGORY_HAVE_SEX_OPTION(enumMenuCategory selectedMenu = MC_CURRENT)

    IF selectedMenu = MC_CURRENT
        selectedMenu = dirCurrentMenu
    ENDIF
    
    //List of menus without gender Categories
    IF selectedMenu > MC_START_ACTOR_CATEGORIES
        IF selectedMenu = MC_ANIMALS
        OR selectedMenu = MC_SHORTLIST
        OR selectedMenu = MC_RECENTLYUSED
        OR selectedMenu = MC_MILITARY
        OR selectedMenu = MC_COSTUME
        OR selectedMenu = MC_STORY
        OR selectedMenu = MC_HEIST
        OR selectedMenu = MC_SPECIAL
        OR selectedMenu = MC_TRANSPORT
        OR selectedMenu = MC_ONLINE
            cprintln(debug_director,"DOES_CATEGORY_HAVE_SEX_OPTION() FALSE")
            RETURN FALSE
        ENDIF
    ELSE
        cprintln(debug_director,"DOES_CATEGORY_HAVE_SEX_OPTION() FALSE not actor menu")
        RETURN FALSE
    ENDIF
    
    cprintln(debug_director,"DOES_CATEGORY_HAVE_SEX_OPTION() TRUE")
    RETURN TRUE
ENDFUNC

PROC ADD_SELECTABLE_MODEL_CATEGORY(int iThisMenuItem,string txtTitle)

    ADD_MENU_ITEM_TEXT(iThisMenuItem,txtTitle,0,TRUE)
    ADD_MENU_ITEM_TEXT(iThisMenuItem,"CM_GENSEL",1,TRUE)
    ADD_MENU_ITEM_TEXT_COMPONENT_INT(iCurrentSelectedModel+1)   
ENDPROC

PROC ADD_MENU_CATEGORY(enumMenuCategory category, bool femaleCategory=TRUE, bool maleCategory=TRUE, bool makeSelectable = TRUE)
    cprintln(debug_director,"ADD_MENU_CATEGORY() Category = ",category," ",GET_CATEGORY_TITLE(category))
    TEXT_LABEL_63 mnuLabel = GET_CATEGORY_TITLE(category)
    cprintln(debug_director,"Label = ",mnuLabel)
    //check if an actor category
    IF category > MC_START_ACTORS
        IF (menuDisplaySex AND maleCategory)
        OR (!menuDisplaySex AND femaleCategory)
        OR (!DOES_CATEGORY_HAVE_SEX_OPTION(dirCurrentMenu))
        //dirCurrentMenu = MC_HEIST OR dirCurrentMenu = MC_SPECIAL OR dirCurrentMenu = MC_STORY)
            cprintln(debug_director,"ADD_MENU_CATEGORY() add @",iMenuItem,"/",iTotalMenuOptions)
            IF IS_MENU_CATEGORY_SELECTABLE(category)
                IF IS_MENU_CATEGORY_VISIBLE(category)
                    IF !menuDisplaySex
                    AND DOES_CATEGORY_NEED_FEMININE_VERSION_OF_TEXT_LABEL(category)
                        mnuLabel += "F"
                    ENDIF
                    
                    cprintln(debug_director,"ADD_MENU_CATEGORY() model variations in this category (",category," ) : ", GET_TOTAL_CATEGORY_MODEL_VARIATIONS(displayedMenuData[curMenuItem].category,dirCurrentMenu))
                    
                    
                    ADD_MENU_ITEM_TEXT(iMenuItem, mnuLabel,0,makeSelectable)
                    displayedMenuData[iMenuItem].category = category
                    displayedMenuData[iMenuItem].index = 0
                    iMenuItem++
                    iTotalMenuOptions++
                                    
                ELSE
                    ADD_MENU_ITEM_TEXT(iMenuItem, "CM_QUE",0,FALSE)
                    displayedMenuData[iMenuItem].category = MC_VOID                                     
                    displayedMenuData[iMenuItem].index = 0
                    iMenuItem++
                    iTotalMenuOptions++
                ENDIF
                    
            ELSE
                ADD_MENU_ITEM_TEXT(iMenuItem, "CM_QUE",0,FALSE)
                displayedMenuData[iMenuItem].category = MC_VOID                                     
                displayedMenuData[iMenuItem].index = 0
                iMenuItem++
                iTotalMenuOptions++

                ENDIF           
            ENDIF           
            
        ELSE
            cprintln(debug_director,"B")
            ADD_MENU_ITEM_TEXT(iMenuItem, mnuLabel,0,makeSelectable)
            displayedMenuData[iMenuItem].category = category
            displayedMenuData[iMenuItem].index = 0
            cprintln(debug_director,"ADD_MENU_CATEGORY ",category," index= ",iMenuItem," = ",displayedMenuData[iMenuItem].category)
            iMenuItem++
            iTotalMenuOptions++
        ENDIF   
    
ENDPROC

PROC ADD_MENU_CATEGORY_MALE(enumMenuCategory category, bool makeSelectable = TRUE)
    ADD_MENU_CATEGORY(category,false,true,makeSelectable)
ENDPROC

PROC ADD_MENU_CATEGORY_FEMALE(enumMenuCategory category, bool makeSelectable = TRUE)
    ADD_MENU_CATEGORY(category,true,false,makeSelectable)
ENDPROC

FUNC TEXT_LABEL_31 GET_FOREIGN_LANGUAGE_SHORT_LABEL_IF_EXISTS(TEXT_LABEL_31 inputLabel)
    TEXT_LABEL_31 txtExtra = ""
    TEXT_LABEL_31 txtCheck = inputLabel
    SWITCH GET_CURRENT_LANGUAGE()
        CASE LANGUAGE_ENGLISH txtExtra = "_ENG" BREAK
        CASE LANGUAGE_FRENCH txtExtra = "_FRE" BREAK
        CASE LANGUAGE_GERMAN txtExtra = "_GER" BREAK
        CASE LANGUAGE_ITALIAN txtExtra = "_ITA" BREAK
        CASE LANGUAGE_SPANISH txtExtra = "_SPA" BREAK
        CASE LANGUAGE_PORTUGUESE txtExtra = "_POR" BREAK
        CASE LANGUAGE_POLISH txtExtra = "_POL" BREAK
        CASE LANGUAGE_RUSSIAN txtExtra = "_RUS" BREAK
        CASE LANGUAGE_KOREAN txtExtra = "_KOR" BREAK
        CASE LANGUAGE_CHINESE txtExtra = "_CHI" BREAK
        CASE LANGUAGE_JAPANESE txtExtra = "_JAP" BREAK
        CASE LANGUAGE_MEXICAN txtExtra = "_MEX" BREAK
		CASE LANGUAGE_CHINESE_SIMPLIFIED txtExtra = "_CHI" BREAK
    ENDSWITCH
    
    txtCheck+= txtExtra
    
    IF NOT ARE_STRINGS_EQUAL(txtCheck,txtExtra)
        IF DOES_TEXT_LABEL_EXIST(txtCheck)
            RETURN txtCheck
        ENDIF
    ENDIF
    RETURN inputLabel
ENDFUNC

PROC ADD_SELECTABLE_MENU(int iThisMenuItem, int &iSelectedItem, string txtTitle,int iMenuEntries, string txtItemLabl, bool isSelectable = TRUE)

    TEXT_LABEL_31 thisMenuLabel
    thisMenuLabel = txtItemLabl
    IF NOT (iThisMenuItem = ENUM_TO_INT(PI_M0_SHORTLIST) AND selectedMenuType = MENUTYPE_PI  AND iSubMenu = PI_SUBMENU_NONE)
        IF iSelectedItem >= iMenuEntries
            iSelectedItem -= iMenuEntries
        ENDIF
        
        IF iSelectedItem < 0
            iSelectedItem += iMenuEntries
        ENDIF
    endif
    
    int iTextEnd
    iTextEnd = iSelectedItem+1
    thisMenuLabel += iTextEnd
    
    IF !(iThisMenuItem = ENUM_TO_INT(PI_M0_SHORTLIST) AND selectedMenuType = MENUTYPE_PI AND iSubMenu = PI_SUBMENU_NONE)
        ADD_MENU_ITEM_TEXT(iThisMenuItem,txtTitle,0,isSelectable)
    ENDIF

    int iListValue
    IF iThisMenuItem = ENUM_TO_INT(PI_M0_SHORTLIST)
    AND selectedMenuType = MENUTYPE_PI
    AND iSubMenu = PI_SUBMENU_NONE
        
        TEXT_LABEL_31 txtCheck,txtCheckFemale
        
        int iShortlistEntry
        iShortlistEntry = GET_NTH_USED_SHORTLIST_INDEX(selectedPI[PI_M0_SHORTLIST])
        If iShortlistEntry < 0 iShortlistEntry = 0 ENDIF
        
        //define catefory label
        thisMenuLabel = GET_CATEGORY_TITLE(shortlistData[iShortlistEntry].category)
        txtCheck = thisMenuLabel
        txtCheck += "f"
        
        //set new category label.
        IF DOES_TEXT_LABEL_EXIST(txtCheck)              
        AND !shortlistData[iShortlistEntry].bMale //added in for bug 2282265
            thisMenuLabel += "F"
        ENDIF
        
        cprintln(debug_director,"main shortlist category: ",thisMenuLabel)
    
        //cheeck if any entries prior to this
        int r, iListEntry
        for r = 0 to selectedPI[PI_M0_SHORTLIST]
            iListEntry = GET_NTH_USED_SHORTLIST_INDEX(r)            
            IF iListEntry >= 0      
            
                TEXT_LABEL_63 sA,sB
                txtCheck = GET_CATEGORY_TITLE(shortlistData[iListEntry].category)
                cprintln(debug_director,"index cat title : ",txtCheck)
                txtCheckFemale = txtCheck
                txtCheckFemale += "F"
            
                IF !shortlistData[iListEntry].bMale
                    IF DOES_TEXT_LABEL_EXIST(txtCheckFemale)
                        sA = GET_FILENAME_FOR_AUDIO_CONVERSATION(txtCheckFemale)
                    ELSE    
                        sA = GET_FILENAME_FOR_AUDIO_CONVERSATION(txtCheck)
                    ENDIF
                ELSE
                    sA = GET_FILENAME_FOR_AUDIO_CONVERSATION(txtCheck)
                ENDIF
                                                
                sB = GET_FILENAME_FOR_AUDIO_CONVERSATION(thisMenuLabel)
                
                cprintln(debug_director,"sA = ",sA," sB = ",sB)
                
                IF ARE_STRINGS_EQUAL(sA,sB)                         
                    iListValue++
                ENDIF
            ENDIF
        endfor
                
        
    ENDIF

    
    IF (iThisMenuItem = ENUM_TO_INT(PI_M0_SHORTLIST) AND selectedMenuType = MENUTYPE_PI  AND iSubMenu = PI_SUBMENU_NONE)
        IF iListValue > 1
            thisMenuLabel = GET_FOREIGN_LANGUAGE_SHORT_LABEL_IF_EXISTS(thisMenuLabel)
            ADD_MENU_ITEM_TEXT(iThisMenuItem,txtTitle,0,isSelectable)       
            ADD_MENU_ITEM_TEXT(iThisMenuItem,"CM_GENSELB",2,isSelectable)
            ADD_MENU_ITEM_TEXT_COMPONENT_STRING(thisMenuLabel)
            ADD_MENU_ITEM_TEXT_COMPONENT_INT(iListValue)
        ELSE
            thisMenuLabel = GET_FOREIGN_LANGUAGE_SHORT_LABEL_IF_EXISTS(thisMenuLabel)
            ADD_MENU_ITEM_TEXT(iThisMenuItem,txtTitle,0,isSelectable)       
            ADD_MENU_ITEM_TEXT(iThisMenuItem,thisMenuLabel,0,isSelectable)
        ENDIF
    ELSE
        ADD_MENU_ITEM_TEXT(iThisMenuItem,thisMenuLabel,0,isSelectable)
    ENDIF
    
ENDPROC


FUNC BOOL DOES_CURRENT_MENU_ITEM_HAVE_RANDOM_OPTION()

    IF dirCurrentMenu   < MC_START_ACTOR_CATEGORIES
        RETURN FALSE
    ENDIF
    
    SWITCH displayedMenuData[curMenuItem].category
        CASE MC_SEX     
        CASE MC_ANICHI 
        CASE MC_ANICOR 
        CASE MC_ANICRO 
        CASE MC_ANIHEN 
        CASE MC_ANIPOO 
        CASE MC_ANISAS 
        CASE MC_ANISEA 
        CASE MC_COSALI 
        CASE MC_COSPOG 
        CASE MC_COSSPA 
        CASE MC_SPEAND 
        CASE MC_SPEBAY 
        CASE MC_SPEBIL 
        CASE MC_SPECLI 
        CASE MC_SPEGRI 
//      CASE MC_SPEJAN      
        CASE MC_SPEJER 
        CASE MC_SPEJES 
        CASE MC_SPEMAN 
        CASE MC_SPEMIM 
        CASE MC_SPEPAM 
        CASE MC_SPEIMP 
        CASE MC_SPEZOM 
        CASE MC_STONER 
        CASE MC_STOMAU 
        CASE MC_STOTHO 
        CASE MC_STOTAN 
//      CASE MC_STOBRA 
        CASE MC_STOCHR 
        CASE MC_STOSTR 
//      CASE MC_STODRF
        CASE MC_STOPAT 
        CASE MC_STOBEV 
        CASE MC_STOSOL 
//      CASE MC_HEITAL
//      CASE MC_HEIPAI
        CASE MC_COSMIM
        CASE MC_COSSPY
        CASE MC_LABJAN
        CASE MC_PRODOR
        CASE MC_PROFAS
        CASE MC_PROPON
        CASE MC_STOLAZ
        CASE MC_ONLINE1
        CASE MC_ONLINE2
        CASE MC_VOID

            RETURN FALSE
        BREAK               
    ENDSWITCH
    
    RETURN TRUE
ENDFUNC

PROC SET_DIRECTOR_PARACHUTE_AMMO(MODEL_NAMES currentModel)
    IF currentModel = PLAYER_ZERO 
    OR currentModel = PLAYER_ONE
    OR currentModel = PLAYER_TWO
        GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),GADGETTYPE_PARACHUTE, 1,FALSE,FALSE)
        SET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
        SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(player_id(),TRUE)
    ELSE
        if IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
            GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),GADGETTYPE_PARACHUTE, 0,FALSE,FALSE)
        ENDIF   
        SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(player_id(),FALSE)
    ENDIF
ENDPROC

PROC GIVE_PLAYER_DIRECTOR_MODE_WEAPONS()
    //cprintln(DEBUG_DIRECTOR, "<WEAPONS> Attempting to give new character a weapon loadout...")
    MODEL_NAMES currentModel = GET_ENTITY_MODEL(player_ped_id())
    IF eStoryCharacter != CHAR_BLANK_ENTRY
        IF IS_PLAYER_PED_PLAYABLE(eStoryCharacter)
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                IF CAN_PLAYER_MODEL_HOLD_WEAPONS()
                    //Remove all ped weapons first
                    CPRINTLN(DEBUG_DIRECTOR,"<WEAPONS> remove all weapons.")
                    REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
                    
                    INT iWeaponIndex 
                    REPEAT 200 iWeaponIndex     //Very high max index for future-proofing
                        WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_ITERATED_INDEX(iWeaponIndex)
                        
                        IF eWeaponType != WEAPONTYPE_INVALID
                            CDEBUG3LN(DEBUG_DIRECTOR, "<WEAPONS> Checking weapon #",iWeaponIndex," type ", GET_WEAPON_NAME(eWeaponType), ".")
                            IF IS_WEAPON_VALID(eWeaponType) AND IS_WEAPON_VALID_FOR_DIRECTOR_MODE(eWeaponType)
                                IF IS_WEAPON_AVAILABLE_FOR_GAME(eWeaponType) 
                                    CDEBUG3LN(DEBUG_DIRECTOR, "<WEAPONS> Weapon type ", GET_WEAPON_NAME(eWeaponType), " is available in game.")
                                    
                                    IF (IS_PLAYER_PED_WEAPON_UNLOCKED(eStoryCharacter, eWeaponType) AND IS_WEAPON_UNLOCKED_IN_FLOW(eWeaponType) <> 0)
                                    AND NOT (currentModel = IG_ORLEANS AND eWeaponType = WEAPONTYPE_DLC_KNUCKLE)
                                        CDEBUG3LN(DEBUG_DIRECTOR, "<WEAPONS> Weapon type ", GET_WEAPON_NAME(eWeaponType), " is unlocked in game. Giving to player.")
                                        GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), eWeaponType, INFINITE_AMMO, FALSE, FALSE)
                                        IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),eWeaponType)
                                            SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), TRUE, eWeaponType)
                                            SET_AMMO_IN_CLIP(PLAYER_PED_ID(),eWeaponType,GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(),eWeaponType))
                                        ELSE
                                            CDEBUG1LN(DEBUG_DIRECTOR, "<WEAPONS> GIVE_WEAPON_TO_PED failed for ",GET_WEAPON_NAME(eWeaponType))
                                        ENDIF
                                        SET_DIRECTOR_PARACHUTE_AMMO(currentModel)
                                    ENDIF
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDREPEAT
					
					lastModelGivenWeapons = currentModel
                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC

FUNC CHEATS_BITSET_ENUM GET_SETTING_CHEAT_TYPE(INT iSetting)
    SWITCH iSetting
        CASE SEM_FIREBULLET     RETURN CHEAT_TYPE_FLAMING_BULLETS BREAK     
        CASE SEM_LOWGRAVITY     RETURN CHEAT_TYPE_0_GRAVITY BREAK       
        CASE SEM_SLIDEY         RETURN CHEAT_TYPE_SLIDEY_CARS BREAK     
        CASE SEM_SUPERJUMP      RETURN CHEAT_TYPE_SUPER_JUMP BREAK      
        CASE SEM_XPLODEBULLET   RETURN CHEAT_TYPE_BANG_BANG BREAK       
        CASE SEM_XPLODEPUNCH    RETURN CHEAT_TYPE_EXPLOSIVE_MELEE BREAK     
    ENDSWITCH
    
    CASSERTLN(DEBUG_DIRECTOR,"Tried to get a setting cheat for an non-existent option: ",iSetting)
    RETURN CHEAT_TYPE_MAX
ENDFUNC

FUNC BOOL GET_SETTING_CHEAT_ENABLED(INT iSetting)
    SWITCH iSetting
        CASE SEM_FIREBULLET     
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_FLAMING_BULLETS) BREAK      
        CASE SEM_LOWGRAVITY     
            IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
                RETURN FALSE
            ENDIF
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_0_GRAVITY)
        CASE SEM_SLIDEY         
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_SLIDEY_CARS) BREAK      
        CASE SEM_SUPERJUMP      
            
            IF IS_INTERIOR_SCENE()
            and directorFlowState = dfs_PLAY_MODE
                RETURN FALSE
            ENDIF
            
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_SUPER_JUMP) BREAK       
        CASE SEM_XPLODEBULLET   
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_BANG_BANG) BREAK        
        CASE SEM_XPLODEPUNCH    
            RETURN NOT IS_CHEAT_DISABLED(CHEAT_TYPE_EXPLOSIVE_MELEE) BREAK      
        CASE SEM_INVINCIBLE
            model_names playerModel
            playerModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
            IF Is_Model_A_Flying_Animal(playerModel)
            //AND playerModel != A_C_HEN
                RETURN FALSE
            ELSE
                RETURN TRUE
            ENDIF
        BREAK
    ENDSWITCH
    RETURN TRUE //Active by default
ENDFUNC

FUNC INT GET_TIME_SETTINGS_OPTION(INT iHour, INT iMinute)
    IF iHour < 5 
        RETURN 0
    ELIF iHour < 6 
        RETURN 1
    ELIF iHour < 8 
        RETURN 2
    ELIF iHour < 12 
        RETURN 3
    ELIF iHour < 16
        RETURN 4
    ELIF iHour < 18 
        RETURN 5
    ELIF (iHour = 18 AND iMinute <= 30)
        RETURN 5
    ELIF iHour < 21
        RETURN 6
    ELSE
        RETURN 7
    ENDIF
        
    RETURN -1   //Should never get here
ENDFUNC

PROC CHANGE_WEATHER(INT iSelectedWeather)
    SWITCH iSelectedWeather
        CASE 0 SET_WEATHER_TYPE_NOW_PERSIST("CLEAR") BREAK //clear
        CASE 1 SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS") BREAK //broken cloud
        CASE 2 SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST") BREAK //overcast
        CASE 3 SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY") BREAK //hazy
        CASE 4 SET_WEATHER_TYPE_NOW_PERSIST("XMAS") BREAK //xmas
        CASE 5 SET_WEATHER_TYPE_NOW_PERSIST("SMOG") BREAK //smog
        CASE 6 SET_WEATHER_TYPE_NOW_PERSIST("FOGGY") BREAK //fog
        CASE 7 SET_WEATHER_TYPE_NOW_PERSIST("RAIN") BREAK //rain
        CASE 8 SET_WEATHER_TYPE_NOW_PERSIST("THUNDER") BREAK //thunder
    ENDSWITCH
ENDPROC

PROC CHANGE_WANTED_LVL(INT IWantedLevel)
    SWITCH IWantedLevel
        CASE 0 iWantedLvlMin=0  iWantedLvlMax =5  BREAK
        CASE 1 iWantedLvlMin=2  iWantedLvlMax =2  BREAK
        CASE 2 iWantedLvlMin=3  iWantedLvlMax =3  BREAK
        CASE 3 iWantedLvlMin=5  iWantedLvlMax =5  BREAK
        CASE 4 iWantedLvlMin=0  iWantedLvlMax =0  BREAK
    ENDSWITCH
    //Multiplier
    SET_WANTED_LEVEL_MULTIPLIER(1.0)
ENDPROC

//Change ped and vehicle populations (0 = lowest, 3 = maximum)
PROC CHANGE_PED_VEHICLE_POPULATIONS(INT iPedPop, INT iVehPop)
    SET_PED_POPULATION_BUDGET(iPedPop)
    SET_VEHICLE_POPULATION_BUDGET(iVehPop)      
    
    //Disable distant cars if the setting is 0%
    IF iVehPop = 0
        SET_DISTANT_CARS_ENABLED(FALSE)
        DISABLE_VEHICLE_DISTANTLIGHTS(TRUE)
        SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-10000,-10000,-200>>,<<10000,10000,1000>>,FALSE)
        SET_ROADS_IN_AREA(<<-10000,-10000,-200>>,<<10000,10000,1000>>,FALSE)
        //Disable random trains
        SET_RANDOM_TRAINS(FALSE)
        SET_RANDOM_BOATS(FALSE)
        DELETE_ALL_TRAINS()
    ELSE
        SET_DISTANT_CARS_ENABLED(TRUE)
        DISABLE_VEHICLE_DISTANTLIGHTS(FALSE)
        SET_ALL_VEHICLE_GENERATORS_ACTIVE()
        //SET_ROADS_IN_AREA(<<-10000,-10000,-200>>,<<10000,10000,1000>>,TRUE) //bug 2284564
        SET_ROADS_BACK_TO_ORIGINAL(<<-10000,-10000,-200>>,<<10000,10000,1000>>)
        //Re-enable trains
        SET_RANDOM_TRAINS(TRUE)
        SET_RANDOM_BOATS(TRUE)
    ENDIF
    
    //changing ped/veh density remove existing block
    IF blockingIndex != NULL
        #IF IS_DEBUG_BUILD
            INT iBlockingIndex = NATIVE_TO_INT(blockingIndex)
            CDEBUG1LN(debug_director,"REMOVE_SCENARIO_BLOCKING_AREA( ", iBlockingIndex ,")")
        #ENDIF      
        REMOVE_SCENARIO_BLOCKING_AREA(blockingIndex)
        blockingIndex = NULL
    ENDIF
    
    //create map wide scenario block for either peds, vehicles or both
    IF iVehPop = 0 AND iPedPop = 0
        CDEBUG1LN(debug_director,"blocking index VEH+PED ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,TRUE,TRUE)")
        blockingIndex = ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,TRUE,TRUE)
    ELIF iVehPop = 0
        CDEBUG1LN(debug_director,"blocking index VEH ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,FALSE,TRUE)")
        blockingIndex = ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,FALSE,TRUE)
    ELIF iPedPop = 0
        CDEBUG1LN(debug_director,"blocking index PED ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,TRUE,FALSE)")
        blockingIndex = ADD_SCENARIO_BLOCKING_AREA(mapMin,MapMax,FALSE,TRUE,TRUE,FALSE)
    ENDIF
    
    //Selectively clear the area if ped or vehicle density has changed
    IF iPedPop = 0
        CPRINTLN(DEBUG_DIRECTOR,"APPLY_DM_GAME_SETTINGS: Clearing nearby peds as settings have changed")
        CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000)
    ELSE
        IF settingsChange[SEM_PEDDENSITY] != 0
            CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000)
        ENDIF
        INSTANTLY_FILL_PED_POPULATION() //Repopulate the world
    ENDIF
    
    IF iVehPop = 0
        CPRINTLN(DEBUG_DIRECTOR,"APPLY_DM_GAME_SETTINGS: Clearing nearby vehicles as settings have changed")
        CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000)
        START_AUDIO_SCENE("Director_Mode_No_Cars_Scene") 
        SET_AMBIENT_ZONE_LIST_STATE("AZL_VEHICLE_RADIO_EMITTER_ZONES", FALSE, TRUE)
    ELSE
        INSTANTLY_FILL_VEHICLE_POPULATION() //Repopulate the world with cars
        bWaitForVehiclePopulationToLoad = TRUE
        STOP_AUDIO_SCENE("Director_Mode_No_Cars_Scene") 
        SET_AMBIENT_ZONE_LIST_STATE("AZL_VEHICLE_RADIO_EMITTER_ZONES", TRUE, TRUE)
    ENDIF
    
    settingsChange[SEM_PEDDENSITY] = settingsChange[SEM_PEDDENSITY]     //Failsafe against unused variable
    
    IF iVehPop > 0
        
    ENDIF
ENDPROC

PROC CHANGE_ALL_AREA_SUPPRESSION(BOOL bSuppress = TRUE)
    AREA_CHECK_AREAS area
    FOR area = AC_GOLF_COURSE to AC_DOWNTOWN_POLICE
        IF bSuppress 
            SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(area)
            g_bDisableArmyBase = TRUE
            g_bDisablePrison = TRUE
        ELSE
            RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(area)
            g_bDisableArmyBase = FALSE
            g_bDisablePrison = FALSE
        ENDIF
    ENDFOR
ENDPROC

FUNC BOOL IS_PLAYER_WEARING_SCUBA_GEAR()
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE HC_GUNMAN
            IF GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES) = 0
                RETURN TRUE
            ENDIF
        BREAK
        CASE IG_DAVENORTON
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 1
                RETURN TRUE
            ENDIF
        BREAK
        CASE IG_STEVEHAINS
            IF GET_PED_PROP_INDEX(player_ped_id(),ANCHOR_EYES) = 1
                RETURN TRUE
            ENDIF
        BREAK
        CASE PLAYER_ZERO //michael
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 1
                RETURN TRUE
            ENDIF
        BREAK
        CASE PLAYER_ONE //franklin
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 8
                RETURN TRUE
            ENDIF
        BREAK
        CASE PLAYER_TWO //Trevor
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL) = 1
                RETURN TRUE
            ENDIF
        BREAK
        
    ENDSWITCH

    RETURN FALSE
ENDFUNC

PROC CHANGE_INVINCIBILITY()
    IF GET_SETTING_CHEAT_ENABLED(SEM_INVINCIBLE)    
        CPRINTLN(debug_director,"Changing invincibility to ",1-settingsMenu[SEM_INVINCIBLE])
        IF settingsMenu[SEM_INVINCIBLE] = 0
            CPRINTLN(debug_director,"SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)")
            SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), TRUE )
//            SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
//            SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)    // Remove for B* 2406755
            SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID())
        ELSE
            CPRINTLN(debug_director,"SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)")
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), FALSE )
            SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
            SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
            IF NOT IS_PLAYER_WEARING_SCUBA_GEAR()
                CPRINTLN(debug_director,"INVINCIBLE -NOT IS_PLAYER_WEARING_SCUBA_GEAR()")
                SET_PED_ABLE_TO_DROWN(PLAYER_PED_ID())
            ELSE
                CPRINTLN(debug_director,"INVINCIBLE -IS_PLAYER_WEARING_SCUBA_GEAR()")
                IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
                    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                    AND NOT IS_PED_INJURED(PLAYER_PED_ID())
                        SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), TRUE )
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ELSE
        CPRINTLN(debug_director,"Invincibility not enabled")
        settingsMenu[SEM_INVINCIBLE] = 1
        SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), FALSE )
        SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
        SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
    ENDIF
ENDPROC

//B*2425682
//every 30 frames check player z pos. turn off invincibility if <-190
PROC CHECK_PLAYER_MAX_DEPTH()
    VECTOR playerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
    IF playerPos.Z < -190
        settingsMenu[SEM_INVINCIBLE] = 1
        CHANGE_INVINCIBILITY()
    ENDIF
ENDPROC

PROC COPY_ACTIVE_CHEATS_TO_SETTINGS()
    int i
    FOR i = SEM_FIREBULLET to SEM_LOWGRAVITY
        settingsMenu[i] = 1
        IF IS_CHEAT_ACTIVE(GET_SETTING_CHEAT_TYPE(i))
            settingsMenu[i] = 0
        ENDIF
    ENDFOR
ENDPROC

PROC APPLY_DM_GAME_SETTINGS()
    //--------These unused references should really be removed
    iClockHours = iClockHours
    iClockMinutes = iClockMinutes
    //---------------------------
    
   
    //HUD Settings
    DISPLAY_HUD(TRUE)
    IF NOT bAnimalRestrictedState
    OR ( iSubMenu = PI_SUBMENU_NONE AND sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION) )
        DISPLAY_RADAR(TRUE)
    ENDIF
    
    
    //Clear area
    IF settingsMenu[SEM_CLEARAREA] = 0
        CPRINTLN(DEBUG_DIRECTOR,"APPLY_DM_GAME_SETTINGS: Clearing area as per settings option")
        CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000,TRUE)
        settingsMenu[SEM_CLEARAREA] = 1
    ENDIF
    //Ped/Veh populations
    CHANGE_PED_VEHICLE_POPULATIONS(settingsMenu[SEM_PEDDENSITY], settingsMenu[SEM_VEHDENSITY])
    
    //Wanted level
    CHANGE_WANTED_LVL(settingsMenu[SEM_WANTED])
    wantedTimer = -1 //reset wanted timer
    
    //Ped Reactions
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(),true,true)
    ENDIF
    SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(),RELGROUPHASH_PLAYER)
    
    //Weapons
    GIVE_PLAYER_DIRECTOR_MODE_WEAPONS()
    //Cellphone
    DISABLE_CELLPHONE(FALSE)

    //set PI menu weather / time of day
    IF NOT bTimeSettingFineTuned
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    AND iSubMenu != PI_SUBMENU_PROPS //Fix for 2406746. Moving into prop editor will no longer impact the time setting. 
    #ENDIF
        CHANGE_TIME(settingsMenu[SEM_TIME])
    ENDIF

    //Fix for 2414114. Moving into prop editor will no longer impact the weather setting.
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    IF iSubMenu != PI_SUBMENU_PROPS
    #ENDIF
        CHANGE_WEATHER(settingsMenu[SEM_WEATHER])
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    ENDIF
    #ENDIF
            
    //SET_CURR_WEATHER_STATE(iStoredWeather[0],iStoredWeather[1],fWeatherTransition)
    //SET_RAIN(fRainLevel)  
    
    //Restricted areas
    IF settingsMenu[SEM_RESTRICTED] = 0
        ENABLE_ALL_RESTRICTED_AREAS()
        CHANGE_ALL_AREA_SUPPRESSION(FALSE)
    ELSE
        DISABLE_ALL_RESTRICTED_AREAS()
        CHANGE_ALL_AREA_SUPPRESSION(TRUE)
    ENDIF
    
    //CHEATS
    CHANGE_INVINCIBILITY()  //Invulnerability
    SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
    
    CHEATS_BITSET_ENUM cheat
    int i
    FOR i = SEM_FIREBULLET to SEM_LOWGRAVITY
        cheat = GET_SETTING_CHEAT_TYPE(i)
        IF (IS_CHEAT_ACTIVE(cheat) AND settingsMenu[i] = 1)
        OR (NOT IS_CHEAT_ACTIVE(cheat) AND settingsMenu[i] = 0)
            TRIGGER_CHEAT(cheat)
        ENDIF
    ENDFOR
    
    //Reset temp settings changes - stores that no settings have been changed from this point
    FOR i = 0 to SEM_TOTAL - 1
        settingsChange[i] = 0
    ENDFOR
    
ENDPROC

PROC MAINTAIN_SETTINGS_EVERY_FRAME()
    IF settingsMenu[SEM_PEDDENSITY] = 0
        SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0,0)
    ENDIF
    IF settingsMenu[SEM_WANTED] = 4
        IF DOES_ENTITY_EXIST( PLAYER_PED_ID() )
        AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
            SET_LAW_PEDS_CAN_ATTACK_NON_WANTED_PLAYER_THIS_FRAME( PLAYER_ID() )
        ENDIF
    ENDIF
ENDPROC

INT iVehicleLoadPopulationTimer = 0
PROC CONTROL_DM_SETTINGS_CHANGE()
    IF bApplyDMSettingsNow
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        IF iSubMenu = PI_SUBMENU_PROPS

            //Steve T - I'm moving the fade handling to within TOGGLE_PROP_EDITOR. It's easier managed that way - especially when we come to add the scene menu tree
            //but it's better to still have the settings changed here.
            APPLY_DM_GAME_SETTINGS()
            bApplyDMSettingsNow = FALSE
            CPRINTLN(debug_director, "DM_SETTINGS_CHANGE Prop submenu bypassing DM fades.")
            HIDE_HUD_AND_RADAR_THIS_FRAME()

        ELSE
        #ENDIF
            //Start fade-out if not started already
            IF NOT IS_SCREEN_FADED_OUT()
                IF NOT IS_SCREEN_FADING_OUT()
                    DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
                    CPRINTLN(debug_director, "DM_SETTINGS_CHANGE fade out.")
                    HIDE_HUD_AND_RADAR_THIS_FRAME()
                ENDIF
            ELSE
                APPLY_DM_GAME_SETTINGS()
                
                #IF FEATURE_SP_DLC_DM_PROP_EDITOR
       
                    //Steve T - I'm moving the fade handling to within TOGGLE_PROP_EDITOR. It's easier managed that way - especially when we come to add the scene menu tree.

                    //If switching to prop editor, wait an extra second
                    /*
                    
                    IF bInPropEdit
                        FLOAT fTimer = 1//1    //Wait 1 second
                        WHILE fTimer >= 0
                            fTimer -= GET_FRAME_TIME()
                            WAIT(0)
                        ENDWHILE
                    ENDIF
                    */
                #ENDIF
                
                //Fade in if not waiting for for vehicle population
                IF NOT IS_SCREEN_FADING_IN()
                AND NOT bWaitForVehiclePopulationToLoad
                    DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
                    CPRINTLN(debug_director, "DM_SETTINGS_CHANGE fade in.")
                ENDIF
                bApplyDMSettingsNow = FALSE
            ENDIF

        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        ENDIF
        #ENDIF
        
    ENDIF
    //Fade screen in 
    IF bWaitForVehiclePopulationToLoad
        //Set timer if calling for the 1st time
        IF iVehicleLoadPopulationTimer = 0
            iVehicleLoadPopulationTimer = GET_GAME_TIMER()
        ENDIF
        
        IF HAS_INSTANT_FILL_VEHICLE_POPULATION_FINISHED()
        OR GET_GAME_TIMER() > iVehicleLoadPopulationTimer + 2000
//          CPRINTLN(debug_dan,"Waited for vehicle population to l oad for ",GET_GAME_TIMER() - iVehicleLoadPopulationTimer, " ms")
            bWaitForVehiclePopulationToLoad = FALSE
            iVehicleLoadPopulationTimer = 0
            IF NOT IS_SCREEN_FADING_IN()
                DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
            ENDIF
        ENDIF
    ENDIF
ENDPROC


FUNC INT GET_NUM_AVAILABLE_VEHS()
    int iTempMaxVehs = 0
    VEHICLE_SETUP_STRUCT sTempVeh
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_CAR_MICHAEL)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_CAR_FRANKLIN)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_CAR_TREVOR)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_MICHAEL_GARAGE_1)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_MICHAEL_GARAGE_2)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_MICHAEL_GARAGE_3)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_FRANKLIN_GARAGE_1)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_FRANKLIN_GARAGE_2)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_FRANKLIN_GARAGE_3)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_TREVOR_GARAGE_1)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_TREVOR_GARAGE_2)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_TREVOR_GARAGE_3)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HANGAR_FRANKLIN)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HANGAR_MICHAEL)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HANGAR_TREVOR)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HELIPAD_FRANKLIN)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HELIPAD_MICHAEL)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_MARINA_FRANKLIN)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_MARINA_MICHAEL)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sTempVeh,VEHGEN_WEB_MARINA_TREVOR)
    and sTempVeh.eModel != DUMMY_MODEL_FOR_SCRIPT
        iTempMaxVehs++
    endif
    
    return iTempMaxVehs
ENDFUNC

PROC REBUILD_HELP_KEYS()

    REMOVE_MENU_HELP_KEYS() 
    
    IF dirCurrentMenu = MC_MAIN
        IF displayedMenuData[curMenuItem].category = MC_SHORTLIST
        AND get_total_active_shortlists(false) > 0
            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "ITEM_SELECT")
        ENDIF
        IF displayedMenuData[curMenuItem].category = MC_RECENTLYUSED
        AND get_total_active_shortlists(true) > 0
            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "ITEM_SELECT")                                                    
        ENDIF
        
        IF displayedMenuData[curMenuItem].category != MC_SHORTLIST
        AND displayedMenuData[curMenuItem].category != MC_RECENTLYUSED
            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "ITEM_SELECT")
        ENDIF
        
        //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLMNU" )
    //ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "CM_HLPE") 
    ELIF dirCurrentMenu = MC_SETTINGS
        //Only Back is active
        ADD_MENU_HELP_KEY_CLICKABLE(caMenuCancel, "ITEM_BACK")
        IF GET_SETTING_CHEAT_ENABLED( curMenuItem )
            ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGSET" )
        ENDIF
        //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLSETT" )
        
    ELSE
        IF IS_MENU_CATEGORY_SELECTABLE(displayedMenuData[curMenuItem].category)
        AND IS_MENU_CATEGORY_VISIBLE(displayedMenuData[curMenuItem].category)
        AND displayedMenuData[curMenuItem].category != MC_VOID
        AND displayedMenuData[curMenuItem].category != MC_SEX
            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "ITEM_SELECT")
        ENDIF
        ADD_MENU_HELP_KEY_CLICKABLE(caMenuCancel, "ITEM_BACK")
        
        IF dirCurrentMenu   != MC_SHORTLIST
        AND dirCurrentMenu  >= MC_START_ACTOR_CATEGORIES
        OR dirCurrentMenu   = MC_RECENTLYUSED
            
            IF GET_EXISTING_PED_SHORTLIST() = -1 //!pedJustShortlisted 
            AND displayedMenuData[curMenuItem].category > MC_START_ACTOR_CATEGORIES
            AND IS_MENU_CATEGORY_SELECTABLE(displayedMenuData[curMenuItem].category)            
                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "CM_BOOK")
            ENDIF
            IF displayedMenuData[curMenuItem].category != MC_VOID       
            AND DOES_CURRENT_MENU_ITEM_HAVE_RANDOM_OPTION()
                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "CM_HLPR")
            ENDIF
        ELSE
            IF (dirCurrentMenu = MC_SHORTLIST AND get_total_active_shortlists(false) > 0)
                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "CM_REMO")
            ENDIF       
        ENDIF  
        
        IF dirCurrentMenu = MC_ACTORS
            //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLCATR" )
        ELSE
            IF displayedMenuData[curMenuItem].category = MC_SEX
                ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGGEND" )
            ENDIF
            IF ( dirCurrentMenu = MC_SHORTLIST AND get_total_active_shortlists( FALSE ) <= 1 )
            OR ( dirCurrentMenu = MC_RECENTLYUSED AND get_total_active_shortlists( TRUE ) <= 1 )
            ELSE
                //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLACT" )
            ENDIF
        ENDIF
    ENDIF

    ADD_MENU_HELP_KEY_CLICKABLE(GET_DIRECTOR_MODE_ZOOM_KEY(), "CM_BUTLT")   
ENDPROC

PROC REBUILD_PI_HELP_KEYS()

    //always an exit button
    ADD_MENU_HELP_KEY_CLICKABLE(caMenuCancel, "PIM_CEXI", default, bPiMouseCameraOverride)           //EXIT
    
    switch iSubMenu
        case PI_SUBMENU_NONE
        
            SWITCH INT_TO_ENUM(DIRECTOR_MODE_PI_M0, sPI_MenuData.iCurrentSelection)
                CASE PI_M0_SETTINGS
                CASE PI_M0_EDITOR   
                CASE PI_M0_RETURN   
                CASE PI_M0_EXIT     
				#IF FEATURE_GEN9_STANDALONE
                CASE PI_M0_MAIN_MENU
                    ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREOPEN", default, bPiMouseCameraOverride)
                BREAK
				#ENDIF // FEATURE_GEN9_STANDALONE
                CASE PI_M0_LOCATION         
                         
                    VECTOR vCurrLocCoords,  vPlayerCoords
                    vCurrLocCoords  = warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position 
                    vPlayerCoords   = GET_PLAYER_COORDS( PLAYER_ID() )
                    
                    BOOL    bWaypointActive
                    VECTOR  vWaypointCoords
                    bWaypointActive = IS_WAYPOINT_ACTIVE()
                    
                    IF bWaypointActive
                        vWaypointCoords = GET_BLIP_COORDS( GET_FIRST_BLIP_INFO_ID( GET_WAYPOINT_BLIP_ENUM_ID() ) )
                    ELSE
                        iWaypointLocIndex = -1
                    ENDIF
                    
                    //Change location buttons
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                        ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGLOC" )
                    ENDIF
                            
                        // Custom locations option
                    IF iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_1)
                    or iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_2)
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                            IF !IS_VECTOR_ZERO(warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position)      
                                ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREL", default, bPiMouseCameraOverride)
                                //Set waypoint
                                IF iWaypointLocIndex <> selectedPI[PI_M0_LOCATION]
                                OR ( bWaypointActive AND NOT ARE_VECTORS_ALMOST_EQUAL( vCurrLocCoords, vWaypointCoords, 0.5, TRUE ) )
                                    IF VDIST2( vCurrLocCoords , vPlayerCoords ) > ( c_F_WAYPOINT_PLAYER_MIN_DIST * c_F_WAYPOINT_PLAYER_MIN_DIST )
                                        ADD_MENU_HELP_KEY_CLICKABLE(caPiMenuWaypoint, "CM_BUTPRELW", default, bPiMouseCameraOverride)
                                    ENDIF
                                ELIF bWaypointActive 
                                AND ( iWaypointLocIndex = selectedPI[PI_M0_LOCATION] OR ARE_VECTORS_ALMOST_EQUAL( vCurrLocCoords, vWaypointCoords, 0.5,  TRUE ) )
                                    ADD_MENU_HELP_KEY_CLICKABLE(caPiMenuWaypoint, "CM_BUTDELW", default, bPiMouseCameraOverride)
                                ENDIF
                            ENDIF
                            //Set location
                            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARP_SET)
                                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "CM_BUTPRELS", default, bPiMouseCameraOverride)
                            ENDIF
                        ENDIF
                #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                    ELIF iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_WAYPOINT)
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREL", default, bPiMouseCameraOverride)
                            ADD_MENU_HELP_KEY_CLICKABLE(caPiMenuWaypoint, "CM_BUTDELW", default, bPiMouseCameraOverride)
                            //Don't add the Set Waypoint button
                        ENDIF
                #ENDIF
                    ELSE
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREL", default, bPiMouseCameraOverride)
                            IF iWaypointLocIndex <> selectedPI[PI_M0_LOCATION]
                            AND NOT ARE_VECTORS_ALMOST_EQUAL( vCurrLocCoords, vWaypointCoords, 0.5,  TRUE )
                            AND VDIST2( vCurrLocCoords , vPlayerCoords ) > ( c_F_WAYPOINT_PLAYER_MIN_DIST * c_F_WAYPOINT_PLAYER_MIN_DIST )
                                ADD_MENU_HELP_KEY_CLICKABLE(caPiMenuWaypoint, "CM_BUTPRELW", default, bPiMouseCameraOverride)
                            ELIF bWaypointActive 
                            AND ( iWaypointLocIndex = selectedPI[PI_M0_LOCATION] OR ARE_VECTORS_ALMOST_EQUAL( vCurrLocCoords, vWaypointCoords, 0.5,  TRUE ) )
                                ADD_MENU_HELP_KEY_CLICKABLE(caPiMenuWaypoint, "CM_BUTDELW", default, bPiMouseCameraOverride)
                            ENDIF
                        ENDIF
                    ENDIF
                BREAK               
                
                CASE PI_M0_GESTURE
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_GESTURE)  
                        
                        IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREG", default, bPiMouseCameraOverride)
                        ELSE
                            ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_SELECT, "CM_BUTPREG", default, bPiMouseCameraOverride)
                            
                            //INPUT_CELLPHONE_SELECT 

                            //ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("CM_BUTPREG", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RS), GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LS))
                        ENDIF
                        ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGACTN" )
                    ENDIF
                BREAK
                CASE PI_M0_SPEECH       
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_DIALOGUE)
                        IF selectedPI[PI_M0_SPEECH] > 0
                            IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                                ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPRES", default, bPiMouseCameraOverride)
                            ELSE
                                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_SELECT, "CM_BUTPRES", default, bPiMouseCameraOverride)
                            ENDIF                       
                        ENDIF
                        IF iMaxSpeechEnries > 1
                            ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGDIALG" )
                        ENDIF
                    ENDIF
                BREAK
                
                CASE PI_M0_VEHICLE
                    if GET_NUM_AVAILABLE_VEHS() > 0
                    and IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
                        ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREOPEN", default, bPiMouseCameraOverride)
                    endif   
                BREAK
                CASE PI_M0_SHORTLIST
                    IF CAN_USE_SHORTLIST_MENU_OPTION()
                        IF IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]))
                        AND NOT ARE_SHORTLIST_ENTRIES_THE_SAME(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]),lastSelectedShortlistPed)                       
                            cprintln(debug_director,"ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, CM_BUTSWAP)", default, bPiMouseCameraOverride)
                            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTSWAP", default, bPiMouseCameraOverride)
                        ENDIF
                        ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGACTR" )
                    ENDIF
                BREAK
                #IF FEATURE_SP_DLC_DM_PROP_EDITOR
                CASE PI_M0_PROPS
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_PROPEDITOR)
                        ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTPREOPEN", default, bPiMouseCameraOverride)
                    ENDIF
                BREAK
                #ENDIF
            ENDSWITCH
            
            //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLMNU" )
        
        BREAK
        CASE PI_SUBMENU_VEHICLES
            ADD_MENU_HELP_KEY_CLICKABLE(caMenuAccept, "CM_BUTVEH", default, bPiMouseCameraOverride)
        BREAK
        CASE PI_SUBMENU_SETTINGS
            IF GET_SETTING_CHEAT_ENABLED(sPI_MenuData.iCurrentSelection)
                    ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_LR, "CM_CHNGSET" )
            ENDIF
            //Use the Square/X key to apply
            IF bTempSettingsHaveChanged
                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "CM_BUTAPPLY", default, bPiMouseCameraOverride)
            ENDIF
            SWITCH sPI_MenuData.iCurrentSelection
                //Add RB/LB help if Time is highlighted
                CASE SEM_TIME
//                    ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_BUMPERS, "CM_BUTSCRUB" )
                    IF bTimeFrozen
                        ADD_MENU_HELP_KEY_CLICKABLE(INPUT_ENTER,"CM_BUTUNFRZ", default, bPiMouseCameraOverride)
                    ELSE
                        ADD_MENU_HELP_KEY_CLICKABLE(INPUT_ENTER,"CM_BUTFRZ", default, bPiMouseCameraOverride)
                    ENDIF
                    ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("CM_BUTSCRUB", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_COVER), 
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SELECT_WEAPON))
                BREAK
            ENDSWITCH
            
            //ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_CELLPHONE_NAVIGATE_UD, "CM_SCRLSETT" )

        BREAK
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        CASE PI_SUBMENU_PROPS
            DMPROP_REBUILD_HELP_PI_KEYS(sPI_MenuData.iCurrentSelection, bPiMouseCameraOverride)
        BREAK
        #ENDIF
    endswitch
    
    //Camera toggle button
    IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
    AND NOT bPiMouseCameraOverride
        ADD_MENU_HELP_KEY_INPUT(caPiMenuMouseCameraOverride,"CM_BUTMOUSECAM")
    ENDIF
    
ENDPROC


PROC REQUEST_PI_MENU_REBUILD()
    bForceRebuildPIMenu = TRUE
ENDPROC

FUNC INT GET_FROM_ALLOWED_LIST_OF_ACTIONS(int currentSelection,int lastDirection, PLAYER_INTERACTIONS allowedA, PLAYER_INTERACTIONS allowedB = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedC = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedD = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedE = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedF = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedG = PLAYER_INTERACTION_NONE, PLAYER_INTERACTIONS allowedH = PLAYER_INTERACTION_NONE) 
    //select the next allowable interaction from the given list, so skipping ones that aren't allowed.
    int i
    PLAYER_INTERACTIONS selectedInteraction
    int interactionsAvailable = 1
    
    IF allowedH != PLAYER_INTERACTION_NONE interactionsAvailable = 8    
    ELIF allowedG != PLAYER_INTERACTION_NONE interactionsAvailable = 7 
    ELIF allowedF != PLAYER_INTERACTION_NONE interactionsAvailable = 6 
    ELIF allowedE != PLAYER_INTERACTION_NONE interactionsAvailable = 5 
    ELIF allowedD != PLAYER_INTERACTION_NONE interactionsAvailable = 4 
    ELIF allowedC != PLAYER_INTERACTION_NONE interactionsAvailable = 3 
    ELIF allowedB != PLAYER_INTERACTION_NONE interactionsAvailable = 2 ENDIF
    
    PLAYER_INTERACTIONS allowableInteractions[8]
    
    allowableInteractions[0] = allowedA
    allowableInteractions[1] = allowedB
    allowableInteractions[2] = allowedC
    allowableInteractions[3] = allowedD
    allowableInteractions[4] = allowedE
    allowableInteractions[5] = allowedF
    allowableInteractions[6] = allowedG
    allowableInteractions[7] = allowedH
    
    int firstInteractionInEnumList = 999999
    int closestInteractionInEnumList = 99999
    
    IF lastDirection = -1
        firstInteractionInEnumList = 0
        closestInteractionInEnumList = 0
    ENDIF
    
    REPEAT (interactionsAvailable) i
        IF lastDirection = -1
            IF currentSelection < enum_to_int(allowableInteractions[i])
                IF enum_to_int(allowableInteractions[i]) > firstInteractionInEnumList               
                    firstInteractionInEnumList = enum_to_int(allowableInteractions[i])
                ENDIF               
            ELSE
                IF enum_to_int(allowableInteractions[i]) > closestInteractionInEnumList
                    closestInteractionInEnumList = enum_to_int(allowableInteractions[i])
                ENDIF
            ENDIF
        ELIF lastDirection = 1
            IF enum_to_int(allowableInteractions[i]) < currentSelection
                IF enum_to_int(allowableInteractions[i]) < firstInteractionInEnumList               
                    firstInteractionInEnumList = enum_to_int(allowableInteractions[i])
                ENDIF               
            ELSE
                IF enum_to_int(allowableInteractions[i]) < closestInteractionInEnumList
                    closestInteractionInEnumList = enum_to_int(allowableInteractions[i])
                ENDIF
            ENDIF
        ELSE
            IF enum_to_int(allowableInteractions[i]) < firstInteractionInEnumList               
                firstInteractionInEnumList = enum_to_int(allowableInteractions[i])          
            ENDIF
        ENDIF
    ENDREPEAT
    
    IF (closestInteractionInEnumList = 99999 AND lastDirection = 1)//current selection is higher than highest allowable interaction, so pick the first one from the list.
    OR (closestInteractionInEnumList = 0 AND lastDirection = -1)
        selectedInteraction = int_to_enum(PLAYER_INTERACTIONS,firstInteractionInEnumList)       
    ELSE
        IF lastDirection = 0
            selectedInteraction = int_to_enum(PLAYER_INTERACTIONS,firstInteractionInEnumList)   
        ELSE
            selectedInteraction = int_to_enum(PLAYER_INTERACTIONS,closestInteractionInEnumList)
        ENDIF
    ENDIF

    RETURN enum_to_int(selectedInteraction)
ENDFUNC

FUNC INT GET_ACTION_SKIPPING_BLOCKED_ACTIONS(int currentSelection,int lastDirection, PLAYER_INTERACTIONS allowed1, 
                                                    PLAYER_INTERACTIONS allowed2 = PLAYER_INTERACTION_NONE, 
                                                    PLAYER_INTERACTIONS allowed3 = PLAYER_INTERACTION_NONE, 
                                                    PLAYER_INTERACTIONS allowed4 = PLAYER_INTERACTION_NONE, 
                                                    PLAYER_INTERACTIONS allowed5 = PLAYER_INTERACTION_NONE, 
                                                    PLAYER_INTERACTIONS allowed6 = PLAYER_INTERACTION_NONE,
                                                    PLAYER_INTERACTIONS allowed7 = PLAYER_INTERACTION_NONE, 
                                                    PLAYER_INTERACTIONS allowed8 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed9 = PLAYER_INTERACTION_NONE,
                                                    PLAYER_INTERACTIONS allowed10 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed11 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed12 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed13 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed14 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed15 = PLAYER_INTERACTION_NONE,
													PLAYER_INTERACTIONS allowed16 = PLAYER_INTERACTION_NONE)
    
    PLAYER_INTERACTIONS selectedInteraction
    PLAYER_INTERACTIONS blockedInteractions[16]
    blockedInteractions[0] = allowed1
    blockedInteractions[1] = allowed2
    blockedInteractions[2] = allowed3
    blockedInteractions[3] = allowed4
    blockedInteractions[4] = allowed5
    blockedInteractions[5] = allowed6
    blockedInteractions[6] = allowed7
    blockedInteractions[7] = allowed8   
    blockedInteractions[8] = allowed9   
	blockedInteractions[9] = allowed10
	blockedInteractions[10] = allowed11
	blockedInteractions[11] = allowed12
	blockedInteractions[12] = allowed13
	blockedInteractions[13] = allowed14
	blockedInteractions[14] = allowed15
	blockedInteractions[15] = allowed16
    
    IF lastDirection = 0 lastDirection = 1 ENDIF        
    
    bool skippingBadAnims = TRUE
    int i
    
    WHILE skippingBadAnims
    
        //Avoid gestures locked by tunables
        IF IS_INTERACTION_LOCKED_BY_TUNABLE(2, currentSelection)
            CPRINTLN(DEBUG_DIRECTOR,"GET_ACTION_SKIPPING_BLOCKED_ACTIONS(",currentSelection,") - locked by tunables")
            currentSelection+=lastDirection
            IF currentSelection <= 0 currentSelection = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE)-1 ENDIF
            IF currentSelection >= ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) currentSelection = 1 ENDIF
        ELSE
            skippingBadAnims = FALSE
            selectedInteraction = INT_TO_ENUM(PLAYER_INTERACTIONS,currentSelection)
            REPEAT COUNT_OF(blockedInteractions) i
                IF blockedInteractions[i] != PLAYER_INTERACTION_NONE
                    IF selectedInteraction = blockedInteractions[i]
                        currentSelection+=lastDirection
                        IF currentSelection <= 0 currentSelection = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE)-1 ENDIF
                        IF currentSelection >= ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) currentSelection = 1 ENDIF
                        selectedInteraction = INT_TO_ENUM(PLAYER_INTERACTIONS,currentSelection)
                        skippingBadAnims = TRUE
                    ENDIF   
                ELSE
                    i = COUNT_OF(blockedInteractions)
                ENDIF
            ENDREPEAT
        ENDIF
    ENDWHILE
    RETURN currentSelection
ENDFUNC

FUNC INT FIND_NEXT_UNRESTRICTED_GESTURE(INT iGive, int lastDirection)
    bool bDirectionButtonPressed = TRUE
    
    If lastDirection = 0
        bDirectionButtonPressed = FALSE
    ENDIF
    
    //Initial check for the passed index
    IF iGive <= 0 iGive = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) -1 ENDIF
    IF iGive >= ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) iGive = 1 ENDIF
    
    int shortlistIndex = GET_NTH_USED_SHORTLIST_INDEX(selectedPI[PI_M0_SHORTLIST])
    IF shortlistIndex < 0 shortlistIndex = 0 ENDIF
    cprintln(DEBUG_DIRECTOR,"FIND_NEXT_UNRESTRICTED_GESTURE(",iGive,") shortlistIndex = ",GET_MODEL_NAME_FOR_DEBUG(shortlistData[shortlistIndex].model))
    
    IF lastDirection = 0 lastDirection = 1 ENDIF
    IF !IS_DIRECTOR_PED_MALE(shortlistData[shortlistIndex].model) //female
    OR shortlistData[shortlistIndex].model = IG_JIMMYDISANTO
            
        PLAYER_INTERACTIONS selectedInteraction
        bool skippingBadFemaleAnims = TRUE
        WHILE skippingBadFemaleAnims
        
            IF iGive <= 0 
                iGive = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE)-1 
            ENDIF
            
            IF iGive >= ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) 
                iGive = 1 
            ENDIF
            
            selectedInteraction = INT_TO_ENUM(PLAYER_INTERACTIONS,iGive)
            SWITCH selectedInteraction
                CASE PLAYER_INTERACTION_KNUCKLE_CRUNCH
                FALLTHRU
                CASE PLAYER_INTERACTION_SLOW_CLAP
                FALLTHRU
                CASE PLAYER_INTERACTION_FACE_PALM
                FALLTHRU
                CASE PLAYER_INTERACTION_DOCK
                FALLTHRU
                CASE PLAYER_INTERACTION_NOSE_PICK
                    CPRINTLN(DEBUG_DIRECTOR,"FIND_NEXT_UNRESTRICTED_GESTURE(",iGive,") - blocked for females (and Jimmy)")
                    IF bDirectionButtonPressed
                        iGive+=lastDirection                        
                    ELSE
                        iGive = 1
                        skippingBadFemaleAnims = FALSE
                    ENDIF
                BREAK           
                DEFAULT
                    skippingBadFemaleAnims = FALSE
                BREAK
            ENDSWITCH
        ENDWHILE
        

    ENDIF
    
    //Check for gestures locked by tunables
    WHILE IS_INTERACTION_LOCKED_BY_TUNABLE(2, iGive)
        CPRINTLN(DEBUG_DIRECTOR,"FIND_NEXT_UNRESTRICTED_GESTURE(",iGive,") - locked by tunables")
        iGive += LastDirection
        IF iGive <= 0 iGive = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) -1 ENDIF
        IF iGive >= ENUM_TO_INT(PLAYER_INTERACTION_SMOKE) iGive = 1 ENDIF
    ENDWHILE
    
    //restrict gestures for fireman

    SWITCH shortlistData[shortlistIndex].model
        CASE  S_M_Y_FIREMAN_01
            IF shortlistData[shortlistIndex].modelVariation[7] > 0 //fireman mask
                iGive = GET_FROM_ALLOWED_LIST_OF_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_FINGER,PLAYER_INTERACTION_WANK,PLAYER_INTERACTION_JAZZ_HANDS,PLAYER_INTERACTION_WAVE,PLAYER_INTERACTION_SURRENDER,PLAYER_INTERACTION_PHOTOGRAPHY,PLAYER_INTERACTION_AIR_SYNTH)
            ENDIF
        BREAK
        CASE G_M_Y_SALVABOSS_01
            IF shortlistData[shortlistIndex].modelVariation[7] = 1 //bandana covering face (special)
                iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_NOSE_PICK,PLAYER_INTERACTION_FACE_PALM)
            ENDIF
        BREAK   
        CASE S_M_Y_MARINE_03
            cprintln(debug_director,"Block marine actions? ",shortlistData[shortlistIndex].modelVariation[2])
            IF shortlistData[shortlistIndex].modelVariation[2] = 2
            OR shortlistData[shortlistIndex].modelVariation[2] = 3
                cprintln(debug_director,"Yes Block marine actions")
                iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_NOSE_PICK,PLAYER_INTERACTION_FACE_PALM,PLAYER_INTERACTION_SHUSH,PLAYER_INTERACTION_DJ)
            ENDIF
        BREAK
        CASE A_M_O_TRAMP_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_ROCK,PLAYER_INTERACTION_DOCK,PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_SLOW_CLAP,PLAYER_INTERACTION_PHOTOGRAPHY,PLAYER_INTERACTION_DJ)
        BREAK
        CASE S_M_M_MovSpace_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_DJ,PLAYER_INTERACTION_FACE_PALM,PLAYER_INTERACTION_NOSE_PICK,PLAYER_INTERACTION_SHUSH,PLAYER_INTERACTION_DJ
            //DLC-Specific
                , PLAYER_INTERACTION_CHIN_BRUSH     
               , PLAYER_INTERACTION_THUMB_ON_EARS  
            )
        BREAK
        CASE u_f_y_spyactress
        CASE u_f_m_corpse_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_DOCK,PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_SLOW_CLAP,PLAYER_INTERACTION_FACE_PALM,PLAYER_INTERACTION_NOSE_PICK,PLAYER_INTERACTION_DJ,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_SHUSH)
        BREAK
        case S_M_Y_Factory_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_KNUCKLE_CRUNCH)
        break
        CASE IG_STRETCH
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_DJ)
        BREAK
        CASE S_M_M_LSMetro_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_JAZZ_HANDS,PLAYER_INTERACTION_SHUSH
                , PLAYER_INTERACTION_NO_WAY     
               , PLAYER_INTERACTION_FREAKOUT   
				, PLAYER_INTERACTION_FINGER_KISS	
            )
        BREAK
        CASE S_M_M_Postal_02
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_FACE_PALM)
        BREAK
        //Special blocks for Maude
        CASE IG_MAUDE
                                                                                //Default female disabled animations
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive, lastDirection, PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_SLOW_CLAP,PLAYER_INTERACTION_DOCK,PLAYER_INTERACTION_NOSE_PICK, PLAYER_INTERACTION_FACE_PALM 
            //DLC-Specific
                , PLAYER_INTERACTION_NO_WAY, PLAYER_INTERACTION_CHICKEN_TAUNT 
               , PLAYER_INTERACTION_FREAKOUT, PLAYER_INTERACTION_THUMB_ON_EARS 
            )
        BREAK
        //Special blocks for Office Cool
        CASE A_F_Y_BUSINESS_03
                                                                                //Default female disabled animations
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive, lastDirection, PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_SLOW_CLAP,PLAYER_INTERACTION_DOCK,PLAYER_INTERACTION_NOSE_PICK, PLAYER_INTERACTION_FACE_PALM 
            //DLC-Specific
                , PLAYER_INTERACTION_NO_WAY     
               , PLAYER_INTERACTION_FREAKOUT   
            )
        BREAK
        //Special blocks for Aztecas Boss
        CASE G_M_M_MEXBOSS_01
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive, lastDirection, PLAYER_INTERACTION_SMOKE  //Dummy interaction for 1st param
            //DLC-Specific
                , PLAYER_INTERACTION_NO_WAY     	
               , PLAYER_INTERACTION_FREAKOUT   	
				, PLAYER_INTERACTION_FINGER_KISS	
            )
        BREAK
        //Special blocks for Mani
        CASE U_M_Y_MANI
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive, lastDirection,PLAYER_INTERACTION_JAZZ_HANDS, PLAYER_INTERACTION_THUMBS_UP, PLAYER_INTERACTION_ROCK, PLAYER_INTERACTION_SALUTE, PLAYER_INTERACTION_SURRENDER, PLAYER_INTERACTION_PHOTOGRAPHY, PLAYER_INTERACTION_DJ
            //DLC-Specific
               , PLAYER_INTERACTION_FREAKOUT, PLAYER_INTERACTION_THUMB_ON_EARS 
				, PLAYER_INTERACTION_YOU_LOCO		
            )
        BREAK
        //Special blocks for hat female models - Park Ranger, On the Promenade
        CASE A_F_M_BEACH_01
        CASE S_F_Y_RANGER_01                                                                    //Default female disabled animations
            iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive, lastDirection, PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_SLOW_CLAP,PLAYER_INTERACTION_DOCK,PLAYER_INTERACTION_NOSE_PICK, PLAYER_INTERACTION_FACE_PALM 
            //DLC-Specific
               , PLAYER_INTERACTION_FREAKOUT, PLAYER_INTERACTION_THUMB_ON_EARS  
				, PLAYER_INTERACTION_YOU_LOCO		
            )
        BREAK
    ENDSWITCH
    
    IF shortlistData[shortlistIndex].model = GET_CREW_MEMBER_MODEL(CM_DRIVER_G_EDDIE)
        cprintln(debug_director,"Block Heist driver (ex: Eddie Toh) actions")
        iGive = GET_ACTION_SKIPPING_BLOCKED_ACTIONS(iGive,lastDirection,PLAYER_INTERACTION_BLOW_KISS,PLAYER_INTERACTION_AIR_SHAGGING,PLAYER_INTERACTION_KNUCKLE_CRUNCH,PLAYER_INTERACTION_JAZZ_HANDS,PLAYER_INTERACTION_SURRENDER,PLAYER_INTERACTION_SHUSH,PLAYER_INTERACTION_PHOTOGRAPHY,PLAYER_INTERACTION_DJ, PLAYER_INTERACTION_SLOW_CLAP
		//DLC Specific
		   , PLAYER_INTERACTION_FREAKOUT, PLAYER_INTERACTION_THUMB_ON_EARS 		
			, PLAYER_INTERACTION_FINGER_KISS, PLAYER_INTERACTION_PEACE	
		)
    ENDIF
    
    RETURN iGive
ENDFUNC

PROC RESTRICT_ACTION_ENTRY(int &iIndexToChange)
    
    iIndexToChange = FIND_NEXT_UNRESTRICTED_GESTURE(iIndexToChange,lastDirectionPress)
    
    IF int_to_enum(PLAYER_INTERACTIONS,iIndexToChange) >= PLAYER_INTERACTION_SMOKE
        iIndexToChange = 1
    ENDIF   
    IF iIndexToChange < 1
        iIndexToChange = enum_to_int(PLAYER_INTERACTION_SMOKE) - 1
    ENDIF
ENDPROC
//=========   MENU FUNCTIONS  ==========

//PURPOSE: Returns the Max size of the current menu
FUNC INT GET_MENU_MAX()

    SWITCH iSubMenu
        CASE    PI_SUBMENU_NONE                     RETURN ENUM_TO_INT(PI_M0_MAX)
        CASE    PI_SUBMENU_VEHICLES                 RETURN iMaxVehs      
        CASE    PI_SUBMENU_SETTINGS                 RETURN SEM_TOTAL
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        CASE    PI_SUBMENU_PROPS                    RETURN DMPROP_GET_NUM_MENU_ENTRIES()
        #ENDIF
    ENDSWITCH
    
    RETURN 0
ENDFUNC

PROC SET_CURRENT_SETTINGS_ITEM_DESCRIPTION(int currentMenuItem)
    SWITCH currentMenuItem
        CASE SEM_TIME
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_TIMDE01")
        BREAK
        CASE SEM_WEATHER
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_WEADE01")
        BREAK
        CASE SEM_WANTED
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_WANDE01")
        BREAK
        CASE SEM_PEDDENSITY
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_PEDDE01")
        BREAK
        CASE SEM_VEHDENSITY
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_VEHDE01")
        BREAK
        CASE SEM_RESTRICTED
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_RESDE01")
        BREAK
        CASE SEM_INVINCIBLE
            IF GET_SETTING_CHEAT_ENABLED(SEM_INVINCIBLE)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_INVDE01")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
            ENDIF
        BREAK
        CASE SEM_FIREBULLET
            IF GET_SETTING_CHEAT_ENABLED(SEM_FIREBULLET)
                //Description not needed
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_FIREBDE1")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
            ENDIF
        BREAK
        CASE SEM_XPLODEBULLET
            IF GET_SETTING_CHEAT_ENABLED(SEM_XPLODEBULLET)
                //Description not needed
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_XPLODEBDE1")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
            ENDIF
        BREAK
        CASE SEM_XPLODEPUNCH
            IF GET_SETTING_CHEAT_ENABLED(SEM_XPLODEPUNCH)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_XPLODEPDE1")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
            ENDIF
            
        BREAK
        CASE SEM_SUPERJUMP
            IF GET_SETTING_CHEAT_ENABLED(SEM_SUPERJUMP)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_SUPERJDE1")
            ELSE
                IF IS_INTERIOR_SCENE()
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE2")
                ELSE
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
                ENDIF
            ENDIF
        BREAK
        CASE SEM_SLIDEY
            IF GET_SETTING_CHEAT_ENABLED(SEM_SLIDEY)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_SLIDEDE1")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
            ENDIF
        BREAK
        CASE SEM_LOWGRAVITY
            IF GET_SETTING_CHEAT_ENABLED(SEM_LOWGRAVITY)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_LOWGDE1")
            ELSE
                IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE2")
                ELSE
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_DISABLEDE1")
                ENDIF
            ENDIF
        BREAK
        CASE SEM_CLEARAREA
            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_CLEARDE1")
        BREAK
    ENDSWITCH
    IF bTempSettingsHaveChanged
    AND NOT bHideSettingsChangedDesc
        SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_SETWARN1")
    ENDIF
ENDPROC


FUNC INT GET_SETTING_MAX(INT iSetting)
    SWITCH iSetting
        CASE SEM_TIME           RETURN 8 BREAK
        CASE SEM_WEATHER        RETURN 9 BREAK
        CASE SEM_WANTED         RETURN 5 BREAK
        CASE SEM_PEDDENSITY     RETURN 4 BREAK
        CASE SEM_VEHDENSITY     RETURN 4 BREAK
        CASE SEM_RESTRICTED     RETURN 2 BREAK
        CASE SEM_INVINCIBLE     RETURN 2 BREAK      
        CASE SEM_FIREBULLET     RETURN 2 BREAK      
        CASE SEM_LOWGRAVITY     RETURN 2 BREAK      
        CASE SEM_SLIDEY         RETURN 2 BREAK      
//      CASE SEM_SLOWAIM        RETURN 2 BREAK      
//      CASE SEM_SLOWMO         RETURN 2 BREAK      
        CASE SEM_SUPERJUMP      RETURN 2 BREAK      
        CASE SEM_XPLODEBULLET   RETURN 2 BREAK      
        CASE SEM_XPLODEPUNCH    RETURN 2 BREAK  
        CASE SEM_CLEARAREA      RETURN 2 BREAK
    ENDSWITCH
    
    CASSERTLN(DEBUG_DIRECTOR,"Tried to get a setting count for an non-existent option: ",iSetting)
    RETURN -1
ENDFUNC

FUNC STRING GET_SETTING_LABEL(INT iSetting)
    SWITCH iSetting
        CASE SEM_TIME           RETURN "CM_IN_TI" BREAK
        CASE SEM_WEATHER        RETURN "CM_IN_WE" BREAK
        CASE SEM_WANTED         RETURN "CM_IN_WAN" BREAK
        CASE SEM_PEDDENSITY     RETURN "CM_IN_PED" BREAK
        CASE SEM_VEHDENSITY     RETURN "CM_IN_VED" BREAK
        CASE SEM_RESTRICTED     RETURN "CM_IN_RES" BREAK
        CASE SEM_INVINCIBLE     RETURN "CM_IN_INV" BREAK        
        CASE SEM_FIREBULLET     RETURN "CM_IN_FIREB" BREAK      
        CASE SEM_LOWGRAVITY     RETURN "CM_IN_LOWG" BREAK       
        CASE SEM_SLIDEY         RETURN "CM_IN_SLIDE" BREAK      
//      CASE SEM_SLOWAIM        RETURN "CM_IN_SLOWA" BREAK      
//      CASE SEM_SLOWMO         RETURN "CM_IN_SLOWM" BREAK      
        CASE SEM_SUPERJUMP      RETURN "CM_IN_SUPERJ" BREAK     
        CASE SEM_XPLODEBULLET   RETURN "CM_IN_XPBUL" BREAK      
        CASE SEM_XPLODEPUNCH    RETURN "CM_IN_XPPUN" BREAK
        CASE SEM_CLEARAREA      RETURN "CM_IN_CLR" BREAK
    ENDSWITCH
    
    CASSERTLN(DEBUG_DIRECTOR,"Tried to get a setting label for an non-existent option: ",iSetting)
    RETURN "ERROR"
ENDFUNC

FUNC STRING GET_SETTING_OPTIONS_LABEL(INT iSetting)
    SWITCH iSetting
        CASE SEM_TIME           RETURN "CM_TI_" BREAK
        CASE SEM_WEATHER        RETURN "CM_WE_" BREAK
        CASE SEM_WANTED         RETURN "CM_CR_" BREAK
        CASE SEM_PEDDENSITY     RETURN "CM_DE_" BREAK
        CASE SEM_VEHDENSITY     RETURN "CM_DE_" BREAK
        CASE SEM_RESTRICTED     RETURN "CM_RES_" BREAK
        CASE SEM_INVINCIBLE     RETURN "CM_RES_" BREAK      
        CASE SEM_FIREBULLET     RETURN "CM_RES_" BREAK      
        CASE SEM_LOWGRAVITY     RETURN "CM_RES_" BREAK      
        CASE SEM_SLIDEY         RETURN "CM_RES_" BREAK      
//      CASE SEM_SLOWAIM        RETURN "CM_RES_" BREAK      
//      CASE SEM_SLOWMO         RETURN "CM_RES_" BREAK      
        CASE SEM_SUPERJUMP      RETURN "CM_RES_" BREAK      
        CASE SEM_XPLODEBULLET   RETURN "CM_RES_" BREAK      
        CASE SEM_XPLODEPUNCH    RETURN "CM_RES_" BREAK      
        CASE SEM_CLEARAREA      RETURN "CM_RES_" BREAK
    ENDSWITCH
    
    CASSERTLN(DEBUG_DIRECTOR,"Tried to get a setting label for an non-existent option: ",iSetting)
    RETURN "ERROR"
ENDFUNC

//PURPOSE: Returns the max options for the current selection
FUNC INT GET_SELECTION_MAX_PI(INT iSelection)
    switch iSubMenu 
        case PI_SUBMENU_NONE
            SWITCH INT_TO_ENUM(DIRECTOR_MODE_PI_M0, iSelection)
                CASE PI_M0_LOCATION       
                    RETURN iMaxLocationsUnlocked - 1
                BREAK   
                CASE PI_M0_SPEECH  
                    RETURN iMaxSpeechEnries - 1
                BREAK               
                CASE PI_M0_SHORTLIST 
                    RETURN get_total_PI_shortlist_options() -1 
                BREAK
            ENDSWITCH   
        break
        case PI_SUBMENU_SETTINGS
            RETURN GET_SETTING_MAX(iSelection) - 1
        break
    endswitch   
    RETURN 0
ENDFUNC

/// PURPOSE: Sets whether the displayed settings have been changed without saving
PROC CHECK_IF_TEMP_SETTINGS_CHANGED()
    bTempSettingsHaveChanged = FALSE
    int i
    for i = 0 to SEM_TOTAL - 1
        if tempSettingsMenu[i] <> settingsMenu[i]
            bTempSettingsHaveChanged = TRUE
        endif
    endfor
ENDPROC

/// PURPOSE: Copies the settings to/from the temp menu for displaying in the PI or applying the settings
///    bToTemp - TRUE = copy to temporary storage (display only), FALSE = copy to permanent storage (to apply)
PROC COPY_SETTINGS_MENU_DATA(BOOL bToTemp = TRUE)
    int i
    IF bToTemp
        for i = 0 to SEM_TOTAL - 1
            settingsChange[i] = settingsMenu[i] - tempSettingsMenu[i]
            tempSettingsMenu[i] = settingsMenu[i]
        endfor
    else
        for i = 0 to SEM_TOTAL - 1
            settingsChange[i] = settingsMenu[i] - tempSettingsMenu[i]
            settingsMenu[i] = tempSettingsMenu[i]
        endfor
    endif
    bTempSettingsHaveChanged = FALSE
ENDPROC





//Please keep this section nicely formatted, with a bit of space between stuff.
/*
TOGGLE_PROP_EDITOR - Main entry / exit point into the Scene Creator / Prop Editor. 
Now uses its own fade out, in routine when entering the Prop editor rather than those in CONTROL_DM_SETTINGS_CHANGE(). This makes it far easier / safer for us to manipulate this alone.
*/


#IF FEATURE_SP_DLC_DM_PROP_EDITOR
PROC TOGGLE_PROP_EDITOR (BOOL bActivatePropEditor, BOOL bApplyEffects = TRUE)

    
    CPRINTLN(debug_director, "Toggle Prop Editor called.")
    
    IF bActivatePropEditor <> bInPropEdit

        CPRINTLN(debug_director, "Swapping current and prop settings. bActivatePropEditor = ",bActivatePropEditor," bApplyEffects = ",bApplyEffects)
        SWAP_PROP_SETTINGS_MENU_DATA()  

        INT i_PostClearArea_StartTime 
        INT i_PostClearAreaAdjustmentPauseTime = 2500        

        IF bApplyEffects

            IF iSubMenu = PI_SUBMENU_PROPS

                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) //Make sure player cannot be killed in the fade before applying settings.
                    SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), TRUE)
                    SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID()) 
                    
                    //Probable solution for 2512958 - Fort Zancudo jeep spawning.
                    DISABLE_ALL_RESTRICTED_AREAS()
                    SET_WANTED_LEVEL_MULTIPLIER(0.0)
                    SET_MAX_WANTED_LEVEL(0)
                 
                    DISABLE_TAXI_HAILING(TRUE) //2530261
                    
                    CPRINTLN(debug_director, "TOGGLE PROP Disabling player control.")
                    SET_PLAYER_CONTROL(PLAYER_ID(), FALSE) //Stop any vehicle entry / exit other weirdness after the Prop Editor has been launched.
                ENDIF

                DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME) 

                WHILE IS_SCREEN_FADING_OUT()
                    WAIT (0) ////the wait is okay in this context.
                    CPRINTLN(debug_director, "TOGGLE PROP fade out with hud hides.")
                    PropEditorHidesPerFrame(bPiMouseCameraOverride)
                ENDWHILE
        
                APPLY_DM_GAME_SETTINGS() //Apply any DM game settings.

                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) //Settings applied, make sure player cannot be killed during Prop Editor use.
                    SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), TRUE)
                    SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID())
             
                
                    //Ensure any personal vehicle is cleared if the player is not currently in / on it. 
                    IF DOES_ENTITY_EXIST(VehCurrentPersonal)
                        IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),VehCurrentPersonal,TRUE)
                            SET_VEHICLE_AS_NO_LONGER_NEEDED(VehCurrentPersonal)
                        ENDIF
                    ENDIF
                
                ENDIF




                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                    
                    VECTOR vTempPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
                    
                    CPRINTLN(debug_director, "Clearing peds, cops and vehicles after Prop Editor entry fade out.")
                    
                    CLEAR_AREA_OF_PEDS(vTempPlayer, 750.0) //2502459
                    CLEAR_AREA_OF_COPS(vTempPlayer, 750.0)
                    CLEAR_AREA_OF_VEHICLES(vTempPlayer, 1000.0)
                    CLEAR_AREA_OF_PROJECTILES(vTempPlayer, 1000.0)
                    STOP_FIRE_IN_RANGE(vTempPlayer, 200.0) //2502595

                    i_PostClearArea_StartTime = GET_GAME_TIMER()

                ENDIF

                //Don't remove this! The console CLEAR_AREA is a little slow.
                WHILE GET_GAME_TIMER() - i_PostClearArea_StartTime <  i_PostClearAreaAdjustmentPauseTime
                    WAIT (0) ////the wait is okay in this context.
                    CPRINTLN(debug_director, "PostClearArea pause ongoing")
                    PropEditorHidesPerFrame(bPiMouseCameraOverride)
                ENDWHILE


            ENDIF

        ENDIF

        DMPROP_EXIT_PROP_EDIT()        
        //Swapping current and prop settings
        bInPropEdit = bActivatePropEditor
        RESET_PI_MENU_NOW()

                    
            //Set existing player vehicle as no longer needed if player in another vehicle
            IF DOES_ENTITY_EXIST(VehCurrentPersonal)
                IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),VehCurrentPersonal,TRUE)
                AND GET_LAST_DRIVEN_VEHICLE() != VehCurrentPersonal
                    SET_VEHICLE_AS_NO_LONGER_NEEDED(VehCurrentPersonal)
                ENDIF
            ENDIF
            //Grab the vehicle the ped is entering
            DMPROP_STORE_PLAYERS_VEHICLE()



        IF bActivatePropEditor
        
            //B* 2509116: Hide weapons
            IF CAN_PLAYER_MODEL_HOLD_WEAPONS()
                SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
            ENDIF

            DISABLE_PROCOBJ_CREATION()

            DISPLAY_RADAR(TRUE)
                   
            //Set up map for expanded, zoomed in style for 2485910. This may change when we set up the scene menu tree. 
            SET_BIGMAP_ACTIVE( TRUE, FALSE )
            SET_RADAR_ZOOM_TO_DISTANCE(50.0)
            LOCK_MINIMAP_ANGLE( 0 )

            //After much confusion, finally found out where the "dust" and "splash" effects are kept.
            //In here. //depot/gta5/art/VFX/rmptfx_tu/fxlists/scr_mp_creator. So that's the named asset.         
            REQUEST_NAMED_PTFX_ASSET("scr_mp_creator")

            WHILE NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_mp_creator")
                PropEditorHidesPerFrame(bPiMouseCameraOverride)
                WAIT(0)
                CPRINTLN(debug_director, "Attempting to load Prop Editor Particle FX assets.")                
            ENDWHILE

            CPRINTLN(debug_director, "Prop Editor Particle FX loaded.")
            
            IF iCurrentScene > -1 
                IF GET_NUM_PROPS_IN_SCENE(iCurrentScene) > 0
                OR iNumProps > 0
                    DMPROP_LOAD_SCENE(iCurrentScene)
                ENDIF
            ENDIF

        ELSE

            ENABLE_PROCOBJ_CREATION()

        ENDIF     
        
        
        IF iSubMenu = PI_SUBMENU_PROPS

            IF (bApplyEffects OR IS_SCREEN_FADED_OUT())
            AND NOT bLoadSceneRunning

                DO_SCREEN_FADE_IN(2500) //Leave this. Otherwise the clear area work above is obvious on console!

                WHILE IS_SCREEN_FADING_IN()
                    
                    WAIT (0)  //the wait is okay in this context.
                    CPRINTLN(debug_director, "TOGGLE PROP fade in with hud hides.")
                    PropEditorHidesPerFrame(bPiMouseCameraOverride) 
                    DMPROP_PER_FRAME_PROCESSING()
                    
                ENDWHILE

            ENDIF


            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
                CPRINTLN(debug_director, "TOGGLE PROP Re-enabling player control.")
                SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
            ENDIF

            

        ELSE

            CPRINTLN(debug_director, "WARNING! PROP EDITOR should be fading in but iSubMenu was unexpectedly not equal to PI_SUBMENU_PROPS!")
            DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)

        ENDIF


          
    ELSE

        CPRINTLN(debug_director, "Attempted to swap prop editor settings but the new mode is the same as the current: ",bInPropEdit)
    
    ENDIF



    IF bInPropEdit = FALSE  
                
        //Absolve Prop Editor of any future issue by resetting these before checking cheat settings.
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
            SET_PLAYER_INVINCIBLE_BUT_HAS_REACTIONS( PLAYER_ID(), FALSE)
            SET_PED_ABLE_TO_DROWN(PLAYER_PED_ID())
        ENDIF

        CPRINTLN(debug_director, "Prop Editor exit set invincibility to false and player able to drown before applying game settings.")

        //On exit of the Prop Editor make sure the player invincibility status is returned to whatever is specified via cheats or other settings.
        APPLY_DM_GAME_SETTINGS() //Apply any DM game settings.
        CHANGE_INVINCIBILITY()

    ENDIF


ENDPROC
#ENDIF


















PROC CREATE_SETTINGS_MENU_OPTIONS(BOOL bUseTempSettings = FALSE)

    INT i
    FOR i = 0 to SEM_TOTAL - 1
        IF bUseTempSettings
            ADD_SELECTABLE_MENU(i,tempSettingsMenu[i],GET_SETTING_LABEL(i),GET_SETTING_MAX(i),GET_SETTING_OPTIONS_LABEL(i),GET_SETTING_CHEAT_ENABLED(i))
        ELSE
            ADD_SELECTABLE_MENU(i,settingsMenu[i],GET_SETTING_LABEL(i),GET_SETTING_MAX(i),GET_SETTING_OPTIONS_LABEL(i),GET_SETTING_CHEAT_ENABLED(i))
        ENDIF
    ENDFOR
ENDPROC

FUNC BOOL ADD_VEHICLE_MODEL_TO_AVAILABLE_LIST(MODEL_NAMES vehModel, INT iFromVehGen = -1)
    int i
    repeat PI_M1_VEHMAX i
        if eAvailableVehs[i] = DUMMY_MODEL_FOR_SCRIPT
            eAvailableVehs[i] = vehModel
            eFromVehGen[i]=iFromVehGen
            CDEBUG1LN(debug_director,"Added vehicle model ",GET_MODEL_NAME_FOR_DEBUG(vehModel)," in slot ",i)
            RETURN TRUE
        elif eAvailableVehs[i] = vehModel and iFromVehGen = -1  //Add the same model if from the vehicle gen
            CDEBUG1LN(debug_director,"Already found model ",GET_MODEL_NAME_FOR_DEBUG(vehModel)," in slot ",i)
            RETURN FALSE
        endif
    endrepeat
    CDEBUG1LN(debug_director,"Could not add model ",GET_MODEL_NAME_FOR_DEBUG(vehModel),", array full")
    RETURN FALSE
ENDFUNC

PROC CLEAN_AVAILABLE_VEHICLE_LIST()
    iMaxVehs = 0
    int i
    repeat PI_M1_VEHMAX i
        eFromVehGen[i] = -1
        eAvailableVehs[i] = DUMMY_MODEL_FOR_SCRIPT
    endrepeat
ENDPROC

PROC PROCCES_MAIN_MENU()

    SET_MENU_TITLE("CM_IN_MAIN")
    
    //====== Settings ========
    ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_SETTINGS),"CM_TOP2") //replaced with CM_TOP2 which is lower case
    
    //====== Locations =======
    
    
    int iLocationsLoop  
    repeat MAX_DTL iLocationsLoop
        iLocationSelected[iLocationsLoop] = -1
    endrepeat
    
    iMaxLocationsUnlocked = 0
    
    repeat MAX_DTL iLocationsLoop
        if  iLocationsLoop != -1
        and iLocationsLoop != enum_to_int(MAX_DTL)
            //IF IS_DIRECTOR_LOCATION_UNLOCKED(INT_TO_ENUM(DirectorTravelLocation, iLocationsLoop))  
            #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
            IF IS_WAYPOINT_ACTIVE()
            OR iLocationsLoop != ENUM_TO_INT(DTL_WAYPOINT)
            #ENDIF
                iLocationSelected[iMaxLocationsUnlocked] = iLocationsLoop
                iMaxLocationsUnlocked++
            #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
            ENDIF
                //Set-up position for the Waypoint location
                IF iLocationsLoop = ENUM_TO_INT(DTL_WAYPOINT)
                AND IS_WAYPOINT_ACTIVE()
                    warpLocations[DTL_WAYPOINT].position = GET_BLIP_COORDS(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
                    warpLocations[DTL_WAYPOINT].heading = 0
                    warpLocations[DTL_WAYPOINT].camRelativePitch = 0
                    warpLocations[DTL_WAYPOINT].camRelativeHeading = 0
                ENDIF
            #ENDIF
            //endif 
            endif 
    endrepeat
    
    //Location index needs updating
    IF selectedPI[PI_M0_LOCATION] >= iMaxLocationsUnlocked
    OR selectedPI[PI_M0_LOCATION] < 0
        selectedPI[PI_M0_LOCATION] = 0
    ENDIF
        
    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_LOCATION),"CM_IN_LO")
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_LOCATION), PRIVATE_Get_Director_Travel_Location_Name_Label(INT_TO_ENUM(DirectorTravelLocation,iLocationSelected[selectedPI[PI_M0_LOCATION]])))
    ELSE
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_LOCATION),"CM_IN_LO",0,false)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_LOCATION), PRIVATE_Get_Director_Travel_Location_Name_Label(INT_TO_ENUM(DirectorTravelLocation,iLocationSelected[selectedPI[PI_M0_LOCATION]])),0,false)
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION)
            SET_MENU_ITEM_TOGGLEABLE(FALSE,FALSE) 
        ENDIF           
    ENDIF
    
    //====== gesture =======
    
    int shortlistEntry = GET_NTH_USED_SHORTLIST_INDEX(selectedPI[PI_M0_SHORTLIST])
    IF shortlistEntry < 0 shortlistEntry = 0 ENDIF

    //IF IS_MODEL_AN_ANIMAL(shortlistData[shortlistEntry].model)
    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_GESTURE)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_GESTURE),"CM_IN_GES")
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_GESTURE),INTERACTION_ANIMS_TEXT_LABEL(enum_to_int(INTERACTION_ANIM_TYPE_PLAYER),selectedPI[PI_M0_GESTURE]))
    ELSE
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_GESTURE),"CM_IN_GES",0,false)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_GESTURE),"CM_DI_1",0,false)
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_GESTURE)
            SET_MENU_ITEM_TOGGLEABLE(FALSE,FALSE) 
        ENDIF   
    ENDIF
    
    // ===== Speech =====   
    IF selectedPI[PI_M0_SPEECH] >= iMaxSpeechEnries
        selectedPI[PI_M0_SPEECH] = 0
    ENDIF
    
    IF selectedPI[PI_M0_SPEECH] < 0
        selectedPI[PI_M0_SPEECH] = iMaxSpeechEnries-1
    ENDIF       
        
    TEXT_LABEL_63 txtSpeech
    IF iMaxSpeechEnries > 1
        txtSpeech = GET_SPEECH_LABEL(PISpeechDataForPed[selectedPI[PI_M0_SPEECH]])
    ENDIF
    
    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_DIALOGUE)
    AND iMaxSpeechEnries > 1
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_SPEECH),"CM_IN_DIA")                
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_SPEECH),txtSpeech)
    ELSE
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_SPEECH),"CM_IN_DIA",0,FALSE)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_SPEECH),"CM_DI_1",0,FALSE)
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SPEECH)
            SET_MENU_ITEM_TOGGLEABLE(FALSE,FALSE)
        ENDIF
    ENDIF
    
    //===== Shortlist =====
    IF CAN_USE_SHORTLIST_MENU_OPTION()
        ADD_SELECTABLE_MENU(ENUM_TO_INT(PI_M0_SHORTLIST),selectedPI[PI_M0_SHORTLIST],"CM_BOOD",get_total_active_shortlists(false),"CM_DI_")
    ELSE
        ADD_SELECTABLE_MENU(ENUM_TO_INT(PI_M0_SHORTLIST),selectedPI[PI_M0_SHORTLIST],"CM_BOOD",get_total_active_shortlists(false),"CM_DI_",FALSE)
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SHORTLIST)
            SET_MENU_ITEM_TOGGLEABLE(FALSE,FALSE) 
        ENDIF
    ENDIF
    
    
    //===== Vehicles =====
    if GET_NUM_AVAILABLE_VEHS() > 0
    and IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_VEHICLE),"CM_VEHOPT")
    else
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_VEHICLE),"CM_VEHOPT",0,FALSE)
    endif
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //===== Props =====
        ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_PROPS),"CM_PROPEDIT", 0, (IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_PROPEDITOR)))
    #ENDIF
    
    // ===== Exit menu options =====
    ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_EDITOR),"PM_PANE_MOV")
    ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_RETURN),"CM_IN_RET")
    #IF FEATURE_GEN9_STANDALONE
	IF GET_PLAYER_HAS_STORY_MODE_ENTITLEMENT()
    	ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_EXIT),"UI_FLOW_DM_QUITCMSM_M")
	ENDIF
    ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_MAIN_MENU),"UI_FLOW_DM_QUITCMLP_M")
    #ENDIF // FEATURE_GEN9_STANDALONE
    #IF NOT FEATURE_GEN9_STANDALONE
	ADD_MENU_ITEM_TEXT(ENUM_TO_INT(PI_M0_EXIT),"CM_IN_EXI")
    #ENDIF // NOT FEATURE_GEN9_STANDALONE
	
    IF IS_MODEL_AN_ANIMAL(shortlistData[shortlistEntry].model)
        IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_GESTURE)
        OR sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SPEECH)
            SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection,TRUE,FALSE)
        ELSE
            SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
        ENDIF
    ELSE
        SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
    ENDIF
    
ENDPROC

PROC PROCESS_SUBMENU_VEH()
    SET_MENU_TITLE("CM_IN_VEH")
    
    //Static list of vehicle gen vehicles       
    eDynamicVehGens[0]  = VEHGEN_WEB_CAR_MICHAEL
    eDynamicVehGens[1]  = VEHGEN_WEB_CAR_FRANKLIN
    eDynamicVehGens[2]  = VEHGEN_WEB_CAR_TREVOR     
    eDynamicVehGens[3]  = VEHGEN_MICHAEL_GARAGE_1
    eDynamicVehGens[4]  = VEHGEN_MICHAEL_GARAGE_2
    eDynamicVehGens[5]  = VEHGEN_MICHAEL_GARAGE_3
    eDynamicVehGens[6]  = VEHGEN_FRANKLIN_GARAGE_1
    eDynamicVehGens[7]  = VEHGEN_FRANKLIN_GARAGE_2
    eDynamicVehGens[8]  = VEHGEN_FRANKLIN_GARAGE_3
    eDynamicVehGens[9]  = VEHGEN_TREVOR_GARAGE_1    
    eDynamicVehGens[10] = VEHGEN_TREVOR_GARAGE_2    
    eDynamicVehGens[11] = VEHGEN_TREVOR_GARAGE_3
    eDynamicVehGens[12] = VEHGEN_WEB_HANGAR_FRANKLIN
    eDynamicVehGens[13] = VEHGEN_WEB_HANGAR_MICHAEL
    eDynamicVehGens[14] = VEHGEN_WEB_HANGAR_TREVOR
    eDynamicVehGens[15] = VEHGEN_WEB_HELIPAD_FRANKLIN
    eDynamicVehGens[16] = VEHGEN_WEB_HELIPAD_MICHAEL
    eDynamicVehGens[17] = VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
    eDynamicVehGens[18] = VEHGEN_WEB_MARINA_FRANKLIN
    eDynamicVehGens[19] = VEHGEN_WEB_MARINA_MICHAEL
    eDynamicVehGens[20] = VEHGEN_WEB_MARINA_TREVOR
    
    CLEAN_AVAILABLE_VEHICLE_LIST()
    
    int iVehloop1
    //Add vehicle gen vehicles
    repeat PI_M1_VEHGENMAX iVehloop1      
            if GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sVehData,eDynamicVehGens[iVehloop1])
            and sVehData.eModel != DUMMY_MODEL_FOR_SCRIPT
                IF ADD_VEHICLE_MODEL_TO_AVAILABLE_LIST(sVehData.eModel,iVehLoop1)
                    ADD_MENU_ITEM_TEXT(iMaxVehs,GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sVehData.eModel),default,true)
                    iMaxVehs++
                ENDIF
            endif
    endrepeat
    
    // Check for website vehicles
    REPEAT ENUM_TO_INT(NUMBER_OF_BUYABLE_VEHICLES_SP) iVehloop1 //Was TOTAL_BUYABLE_VEHICLES. Changed to fix 3190036 after discussion with Kenneth.
        SITE_BUYABLE_VEHICLE SiteBuyVeh = INT_TO_ENUM(SITE_BUYABLE_VEHICLE,iVehLoop1)
        MODEL_NAMES vehModel = GET_MODEL_FOR_BUYABLE_VEHICLE(sitebuyveh)
        IF vehModel <> DUMMY_MODEL_FOR_SCRIPT
            IF GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(sitebuyveh, CHAR_MICHAEL)
            or GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(sitebuyveh, CHAR_FRANKLIN)
            or GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(sitebuyveh, CHAR_TREVOR)
                //eAvailableVehs[iMaxVehs] = eDynamicVehGens[iVehloop1]
                IF ADD_VEHICLE_MODEL_TO_AVAILABLE_LIST (vehModel)
                    ADD_MENU_ITEM_TEXT(iMaxVehs,GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(sitebuyveh)),default,true)
                    iMaxVehs++
                ENDIF
            endif
        ENDIF
    ENDREPEAT
        
ENDPROC


PROC PROCESS_SUBMENU_SET()
    SET_MENU_TITLE("CM_IN_SET")
    CREATE_SETTINGS_MENU_OPTIONS(TRUE)
ENDPROC

/*
#IF FEATURE_SP_DLC_DM_PROP_EDITOR  - Steve T. For ease of reference, this is the old Prop Editor menu before changes were made to accommodate exclamation marks for scene changes in 2409831.
PROC PROCESS_SUBMENU_PROPS()
    TEXT_LABEL_15 tlType
    
    //Title
    SET_MENU_TITLE("CM_PROPEDCH") //Upper Case for heading title.
    
    //Scenes
    ADD_MENU_ITEM_TEXT(0,"DMPP_SCENES")
    ADD_MENU_ITEM_TEXT(0,"DMPP_PROPLIST",1)
    ADD_MENU_ITEM_TEXT_COMPONENT_INT(iSelectedScene)
    
    //Category
    ADD_MENU_ITEM_TEXT(1,"FMMC_VHL")
    ADD_MENU_ITEM_TEXT(1,eCategoryNames[eSelectedPropType[0]])
    //Type
    ADD_MENU_ITEM_TEXT(2,"FMMC_PDV_4")
    tlType = DMPROP_GET_NTH_TYPE_LABEL(eSelectedPropType[0],eSelectedPropType[1])
    ADD_MENU_ITEM_TEXT(2,tlType)
    
    //Elements
    INT i, iElems = DMPROP_GET_NUM_PROPS()
    REPEAT iElems  i
        ADD_MENU_ITEM_TEXT(i+ciPROPADDLINES,DMPROP_GET_PROP_NAME_AT(i))
    ENDREPEAT
//  ADD_MENU_ITEM_TEXT
ENDPROC

PROC UPDATE_PI_MENU_PROP_MOVEMENT()
    IF iSubMenu = PI_SUBMENU_PROPS
        DMPROP_UPDATE_PI_MENU(sPI_MenuData.iCurrentSelection)
    ENDIF
ENDPROC
#ENDIF
*/

#IF FEATURE_SP_DLC_DM_PROP_EDITOR
PROC PROCESS_SUBMENU_PROPS()    //Steve T. This is the new Prop Editor menu that implements exclamation marks for scene changes in 2409831.
    TEXT_LABEL_15 tlType, tlScene
    INT i = 0, iElems
    
    SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT, MENU_ITEM_ICON) 
    SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT, FONT_RIGHT)
   
    //Set the menu title to the scene title if we've selected a scene
    IF ePEState <> PES_SceneSelect
        tlScene = "CM_PROPEDSC"
        tlScene += (iCurrentScene+1)
        SET_MENU_TITLE(tlScene)
    ENDIF
    
    //Switch prop editor state
    SWITCH ePEState
        case PES_SceneSelect    //Scene selection menu
    SET_MENU_TITLE("CM_PROPEDCH") //Upper Case for heading title.
            REPEAT ciMAXSCENES i
                
                //2518982 will be fixed here...
                                       
                tlScene = "CM_PROPEDSC"
                tlScene += (i+1)

                ADD_MENU_ITEM_TEXT(i, tlScene)
                IF i = iCurrentScene 
                    IF b_PropSceneUpdatedSinceLastSave
                        ADD_MENU_ITEM_ICON(iCurrentScene, MENU_ICON_ALERT)
                    ELSE
                        ADD_MENU_ITEM_ICON(iCurrentScene, MENU_ICON_BOX_TICK)
                    ENDIF
                ENDIF               
            ENDREPEAT
        break
        case PES_SceneEdit      //Scene edit menu
            SET_MENU_TITLE(tlScene)
        
            ADD_MENU_ITEM_TEXT(ciPIPropPlace,"CM_PROPPLACE")
            ADD_MENU_ITEM_TEXT(ciPIPropList,"CM_PROPLIST",0,(DMPROP_GET_NUM_PROPS() > 0))
    
            ADD_MENU_ITEM_TEXT(ciPISceneSave,"CM_BUTSAVESCENE")
            IF b_PropSceneUpdatedSinceLastSave ADD_MENU_ITEM_ICON(ciPISceneSave, MENU_ICON_ALERT) ENDIF

            ADD_MENU_ITEM_TEXT(ciPISceneCopy,"CM_BUTOVRWSCENE")
            ADD_MENU_ITEM_TEXT(ciPISceneCopy,"DMPP_PROPLIST",1,(iCurrentScene <> iSelectedScene))
            ADD_MENU_ITEM_TEXT_COMPONENT_INT(iSelectedScene+1)  //+1 for scene numbers [1-4]
    
            IF DMPROP_GET_NUM_PROPS() > 0
                ADD_MENU_ITEM_TEXT(ciPISceneClear,"CM_BUTCLRSCENE")
            ELSE
                ADD_MENU_ITEM_TEXT(ciPISceneClear,"CM_BUTCLRSCENE", DEFAULT, FALSE )
            ENDIF
        break
        case PES_PropPlace      //Prop placement menu
            SET_MENU_TITLE(tlScene)

            //Category
            ADD_MENU_ITEM_TEXT(0,"DMPP_CATG")
            ADD_MENU_ITEM_TEXT(0,eCategoryNames[eSelectedPropType[0]])
            //Type
            ADD_MENU_ITEM_TEXT(1,"DMPP_TYPE")
            tlType = DMPROP_GET_NTH_TYPE_LABEL(eSelectedPropType[0],eSelectedPropType[1])
            ADD_MENU_ITEM_TEXT(1,tlType)
    
        break
        case PES_PropList       //Placed props list
            SET_MENU_TITLE(tlScene)
    //Elements
            iElems = DMPROP_GET_NUM_PROPS()
    REPEAT iElems  i
                ADD_MENU_ITEM_TEXT(i,DMPROP_GET_PROP_NAME_AT(i))
    ENDREPEAT
        break
    ENDSWITCH
    
    
    //Scenes
//    ADD_MENU_ITEM_TEXT(0,"DMPP_SCENES")
//    ADD_MENU_ITEM_TEXT(0,"DMPP_PROPLIST",1)
//    ADD_MENU_ITEM_TEXT_COMPONENT_INT(iSelectedScene)

//    IF b_PropSceneUpdatedSinceLastSave
//        ADD_MENU_ITEM_ICON(0, MENU_ICON_ALERT)
//    ENDIF
    
    //SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT) 
    
//  ADD_MENU_ITEM_TEXT
ENDPROC

PROC UPDATE_PI_MENU_PROP_MOVEMENT()
    IF iSubMenu = PI_SUBMENU_PROPS
    AND bInPropEdit
        DMPROP_UPDATE_PI_MENU(sPI_MenuData.iCurrentSelection)
    ENDIF
ENDPROC
#ENDIF

string menuHelpString
PROC SET_SCALEFORM_HELP_TEXT(string helpTextString)
    menuHelpString = helpTextString
ENDPROC

PROC PROCESS_BELOW_MENU_HELP()

    IF iSubMenu != PI_SUBMENU_NONE
        SET_SCALEFORM_HELP_TEXT("")
    ENDIF

    SWITCH iSubMenu
        CASE PI_SUBMENU_NONE                                    
            SWITCH INT_TO_ENUM(DIRECTOR_MODE_PI_M0, sPI_MenuData.iCurrentSelection)
                CASE PI_M0_SETTINGS
                    SET_SCALEFORM_HELP_TEXT("CM_SETDE1")
                BREAK
                #IF FEATURE_SP_DLC_DM_PROP_EDITOR
                CASE PI_M0_PROPS
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_PROPEDITOR)
                        SET_SCALEFORM_HELP_TEXT("CM_PROPDE1")
                    ELSE
                        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())     
                                
                            //If we add any more exceptions, it will be worthwhile adding warning flags rather than this IF block.
                                         
                            IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
                                SET_SCALEFORM_HELP_TEXT("CM_PROPANM") //You cannot enter the Scene Creator whilst playing as an animal.
                            ELSE
                                
                                IF (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) > PPS_INVALID)
                                    SET_SCALEFORM_HELP_TEXT("CM_PROPPAR") //You cannot enter the Scene Creator whilst skydiving.
                                ELSE
                                    SET_SCALEFORM_HELP_TEXT("CM_PROPNOV1") //You cannot enter the Scene Creator in this vehicle.
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                BREAK
                #ENDIF
                CASE PI_M0_LOCATION
                    IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                        IF !IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARP_SET)
                        AND( iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_1) 
                        OR iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_2))
                            IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INCAR)
                            OR IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_ONVEHICLE)
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN03")
                            ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INWATER)
                            OR IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INDEEPWATER)
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN04")
                            ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INAIR)
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN05")
                            ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_IN_TRAIN)
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN06")
                            ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INCOVER)
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN07")
                            ELSE    //Generic warning message
                                SET_SCALEFORM_HELP_TEXT("CM_WWARN11")
                            ENDIF               
                            
                        ELSE
                            //Pre-defined / Custom location help
                            IF iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_1)
                            OR iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_2)
                                IF IS_VECTOR_ZERO(warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position)
                                    SET_SCALEFORM_HELP_TEXT("CM_LOCDE02")
                                ELSE
                                    SET_SCALEFORM_HELP_TEXT("CM_LOCDE03")
                                ENDIF
                            ELSE
                                SET_SCALEFORM_HELP_TEXT("CM_LOCDE01")
                            ENDIF
                        ENDIF
                    ELSE
                    
                        IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_ARRESTING)
                            SET_SCALEFORM_HELP_TEXT("CM_WWARN01")
                        ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_RECORDING_IN_PROGRESS)
                            SET_SCALEFORM_HELP_TEXT("CM_WWARN02")
                        ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_IS_PASSENGER)
                            SET_SCALEFORM_HELP_TEXT("CM_WWARN08")
                        ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INAIR)
                            SET_SCALEFORM_HELP_TEXT("CM_WWARN09")
                        ELIF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_IN_BOAT)
                            SET_SCALEFORM_HELP_TEXT("CM_WWARN10")
                        ELSE
                            SET_SCALEFORM_HELP_TEXT("CM_LOCDE01")
                        ENDIF
                    
                    ENDIF
                BREAK
                CASE PI_M0_SHORTLIST
                
                    IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INWATER)
                    OR IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INDEEPWATER)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN01") 
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_IN_TRAIN)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN06")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INAIR)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN02")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_ARRESTING)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN04")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INCAR)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN03")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_RECORDING_IN_PROGRESS)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN05")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_IN_PRISON)
                        SET_SCALEFORM_HELP_TEXT("CM_WARN07")
                    ELSE
                        SET_SCALEFORM_HELP_TEXT("CM_BOODE1")
                    ENDIF           
                    
                BREAK
                CASE PI_M0_GESTURE
                
                    IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INDEEPWATER)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN01")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INAIR)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN02")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_ARRESTING)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN04")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INCAR)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN03")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_AS_ANIMAL)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN05")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INCOVER)
                        SET_SCALEFORM_HELP_TEXT("CM_GWARN07")
                    ELSE
                        IF IS_PLAYER_MODEL_SAFE_FOR_GETURES()           
                            //1st Person description
                            IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
                                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                                    SET_SCALEFORM_HELP_TEXT("CM_GESDE1PCFP") 
                                ELSE
                                    SET_SCALEFORM_HELP_TEXT("CM_GESDE1FP") 
                                ENDIF
                            ELSE
                            IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                                SET_SCALEFORM_HELP_TEXT("CM_GESDE1PC") 
                            ELSE
                                SET_SCALEFORM_HELP_TEXT("CM_GESDE1") 
                            ENDIF
                            ENDIF
                        ELSE                        
                            SET_SCALEFORM_HELP_TEXT("CM_GWARN06")                       
                        ENDIF
                    ENDIF               
                BREAK
                CASE PI_M0_SPEECH
                
                    IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INDEEPWATER)
                        SET_SCALEFORM_HELP_TEXT("CM_SWARN01")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_ARRESTING)
                        SET_SCALEFORM_HELP_TEXT("CM_SWARN02")
                    ELIF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_INAIR)
                        SET_SCALEFORM_HELP_TEXT("CM_SWARN04")
                    ELSE
                        IF IS_BIT_SET(lastWarningStateBitSet, BIT_PWS_AS_ANIMAL)                        
                            IF NOT IS_STRING_NULL_OR_EMPTY(animalAnimSoundData.sBarkSound.strBarkSound)
                                SET_SCALEFORM_HELP_TEXT("CM_SWARN06")
                            ELSE
                                SET_SCALEFORM_HELP_TEXT("CM_SWARN03")
                            ENDIF
                        ELIF iMaxSpeechEnries > 1   
                            SET_SCALEFORM_HELP_TEXT("CM_SPEDE1") 
                        ELSE
                            SET_SCALEFORM_HELP_TEXT("CM_SWARN05")
                        ENDIF
                    ENDIF
                    
                BREAK
                CASE PI_M0_VEHICLE
                    IF NOT IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
                        IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INWATER)
                        OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
                            SET_SCALEFORM_HELP_TEXT("CM_GARWATER")
                        ELIF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
                            SET_SCALEFORM_HELP_TEXT("CM_GARANIMAL")
                        ELSE
                            SET_SCALEFORM_HELP_TEXT("CM_GARDENIED")
                        ENDIF
                    ELIF GET_NUM_AVAILABLE_VEHS() <= 0
                        SET_SCALEFORM_HELP_TEXT("CM_GARNOVEH")
                    ELSE
                        SET_SCALEFORM_HELP_TEXT("CM_GARDE1")
                    ENDIF
                BREAK
                CASE PI_M0_EDITOR
                    SET_SCALEFORM_HELP_TEXT("CM_RSEDTR")
                BREAK
                CASE PI_M0_RETURN
                    SET_SCALEFORM_HELP_TEXT("CM_RTNCTRLR")
                BREAK
                CASE PI_M0_EXIT
					#IF FEATURE_GEN9_STANDALONE
                    SET_SCALEFORM_HELP_TEXT("UI_FLOW_DM_QUITCMSM_D")
					#ENDIF // FEATURE_GEN9_STANDALONE
					#IF NOT FEATURE_GEN9_STANDALONE
                    SET_SCALEFORM_HELP_TEXT("CM_TOP5HLP")
					#ENDIF // NOT FEATURE_GEN9_STANDALONE
                BREAK
				#IF FEATURE_GEN9_STANDALONE
                CASE PI_M0_MAIN_MENU
                    SET_SCALEFORM_HELP_TEXT("UI_FLOW_DM_QUITCMLP_D")
                BREAK
				#ENDIF // FEATURE_GEN9_STANDALONE
                DEFAULT
                    SET_SCALEFORM_HELP_TEXT("")
                BREAK
            ENDSWITCH
        BREAK
        case  PI_SUBMENU_VEHICLES
            IF NOT IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_GARWATER")
            ELSE
                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_SPWNVEH")
            ENDIF       
            //SET_SCALEFORM_HELP_TEXT("CM_SPWNVEH")         
        break
        CASE PI_SUBMENU_SETTINGS
            SET_CURRENT_SETTINGS_ITEM_DESCRIPTION(sPI_MenuData.iCurrentSelection) //settings appear in main menu, so created this function to work in both.         
        BREAK

        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        CASE PI_SUBMENU_PROPS

            IF ARE_STRINGS_EQUAL (sPropEditorHelpString, "RemovePropMenuHelp")
                SET_SCALEFORM_HELP_TEXT("")
            ELSE
                SET_SCALEFORM_HELP_TEXT(sPropEditorHelpString)
            ENDIF

        BREAK
        #ENDIF

    endswitch
ENDPROC
PROC REBUILD_PI_MENU()



    CLEAR_DM_MENU_DATA()
    
    #IF IS_DEBUG_BUILD
    //  messageBox_y = 0.182 + (PI_M0_MAX) * 0.035
    //  wl_y = 0.690 + (PI_M0_MAX) * 0.034
    
    //  IF get_total_active_shortlists(false) + get_total_active_shortlists(true) > 1
    //      messageBox_y += 0.035
    //      wl_y += 0.034
    //  ENDIF
    #ENDIF
        
    REMOVE_MENU_HELP_KEYS()
    SET_MENU_ITEM_TOGGLEABLE(FALSE,TRUE)
    SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
    SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
    
    SET_MAX_MENU_ROWS_TO_DISPLAY( c_iMAX_PI_MENU_OPTIONS )
    
    switch isubMenu
        Case PI_SUBMENU_NONE
            PROCCES_MAIN_MENU()             
        break
        case PI_SUBMENU_VEHICLES
            PROCESS_SUBMENU_VEH()
        break
        case PI_SUBMENU_SETTINGS
            PROCESS_SUBMENU_SET()
        break
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        case PI_SUBMENU_PROPS
            PROCESS_SUBMENU_PROPS()
        break
        #ENDIF
    endswitch
    
    SET_CURRENT_MENU_ITEM( sPI_MenuData.iCurrentSelection )
    SET_CURRENT_MENU_ITEM_DESCRIPTION("") // Pass in "" to clear
    
    //HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()    
    //SET_MENU_USES_HEADER_GRAPHIC(TRUE, "Director_Editor_Title", "Director_Editor_Title")
    REBUILD_PI_HELP_KEYS()
    //use menu_icon_tab to align "<" and ">"
    
    UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE()
    
    //Time  < 08 : 00 >
    //Weather < Sunny >
    //Chat < Hi >
    //Gesture < Angry >
    //Return to Trailer
    //Exit to game
    //SET_MENU_USES_HEADER_GRAPHIC(TRUE, "Director_Editor_Title", "Director_Editor_Title")
ENDPROC

FUNC BOOL GET_FIRST_UNLOCKED_MENU_ITEM(int &iReturnedEntry, INT iCheckFrom)
    int i = iCheckFrom
    INT iDirection = iNavVertDirection

    //fix for bug 2315776. In PC the value for iDirection can be other than 1/-1
    //special case for PC build. If player has picked a void entry, just revert to last selected entry. 
    IF iDirection > 1
        iDirection = 1
    ENDIF
    
    IF iDirection < -1
        iDirection = -1
    ENDIF
    
    CPRINTLN(debug_director,"GET_FIRST_UNLOCKED_MENU_ITEM() check from: ",iCheckFrom)
    IF iDirection = 0 iDirection = 1 ENDIF

    BOOL doCheck = TRUE
    WHILE doCheck
        IF displayedMenuData[i].category != MC_VOID
        AND (displayedMenuData[i].category != MC_SEX    OR iCheckFrom != 0)
            iReturnedEntry = i
            RETURN TRUE
        ENDIF
        i+= iDirection
        
        IF i >= iTotalMenuOptions i = 0 ENDIF
        If i < 0 i = iTotalMenuOptions-1 ENDIF
        
        IF i = iCheckFrom doCheck = FALSE ENDIF
    ENDWHILE
    RETURN FALSE
ENDFUNC







//added this currently just for the settings menu but would like to make it work for all menus if time allows it
//restricts menu index to maximum/minimum accordingly

PROC RESTRICT_VALUE_TO_RANGE(int &iEnteredValue,int low,int high)
    IF iEnteredValue < low iEnteredValue = high-1 ENDIF
    IF iEnteredValue >= high iEnteredValue = low ENDIF      
ENDPROC

FUNC enumMenuCategory GET_PARENT_MENU(enumMenuCategory currentMenu)
    SWITCH  currentMenu
        CASE MC_ACTORS RETURN MC_MAIN BREAK
        CASE MC_SETTINGS RETURN MC_MAIN BREAK
        CASE MC_SHORTLIST RETURN MC_MAIN BREAK
        CASE MC_RECENTLYUSED RETURN MC_MAIN BREAK                       
        DEFAULT RETURN MC_ACTORS BREAK
    ENDSWITCH

    RETURN MC_ACTORS
ENDFUNC


PROC REBUILD_MENU(enumMenuCategory menuCategory = MC_VOID, bool retainCurrentSelection=FALSE, int newMenuEntry=-1)
    int i
    cprintln(debug_director,"REBUILD_MENU category = ",menuCategory," newMenuEntry = ",newMenuEntry," curMenuItem = ",curMenuItem)
    
    enumMenuCategory lastMenuCategory = dirCurrentMenu
    
    IF menuCategory = MC_MAIN
            IF !retainCurrentSelection
            cprintln(debug_director,"rebuilding menu A")
                curMenuItem = 0
            ENDIF
        cprintln(debug_director,"rebuilding menu B")
            dirCurrentMenu = menuCategory
    ELSE        
        
        IF menuCategory != MC_CURRENT
            cprintln(debug_director,"rebuilding menu C")
            dirCurrentMenu = menuCategory
        ENDIF
        
        IF !retainCurrentSelection
            
            IF DOES_CATEGORY_HAVE_SEX_OPTION() 
                cprintln(debug_director,"rebuilding menu d")
                displayedMenuData[0].category = MC_SEX
                curMenuItem = 1
            ELSE
                cprintln(debug_director,"rebuilding menu E")
                curMenuItem = 0
            ENDIF
        ELSE
            IF newMenuEntry != -1
                cprintln(debug_director,"rebuilding menu F")
                curMenuItem = newMenuEntry
            ENDIF
        ENDIF
    ENDIF
    
    IF menuCategory != MC_VOID
        CLEAR_DM_MENU_DATA()
        REMOVE_MENU_HELP_KEYS()

    
        
        SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
        SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
        //SET_MAX_MENU_ROWS_TO_DISPLAY(11)
        
            
        
        menuDisplaySex = bMenu_sex_male
        
        IF DOES_CATEGORY_HAVE_SEX_OPTION()
            cprintln(debug_director,"rebuilding menu G")
            SET_MENU_ITEM_TOGGLEABLE(FALSE,TRUE)
            //ADD_MENU_ITEM_TEXT(0, "CM_GEN")   
            int iSex
            IF bMenu_sex_male
                //ADD_MENU_ITEM_TEXT(0, "CM_GENM")
                ADD_SELECTABLE_MENU(0,iSex,"CM_GEN",2,"CM_GENM")
            ELSE
                //ADD_MENU_ITEM_TEXT(0, "CM_GENF")
                ADD_SELECTABLE_MENU(0,iSex,"CM_GEN",2,"CM_GENF")                
            ENDIF
            
            iMenuItem = 1   
            iTotalMenuOptions = 1
        ELSE
            iMenuItem = 0
            iTotalMenuOptions = 0
        ENDIF

        SWITCH dirCurrentMenu
            CASE MC_MAIN
                                                    
                SET_MENU_TITLE("CASTMNU")
                
                ADD_MENU_CATEGORY(MC_ACTORS)
                ADD_MENU_CATEGORY(MC_SETTINGS)
                cprintln(debug_director,"get_total_active_shortlists(false) = ",get_total_active_shortlists(false))
                IF get_total_active_shortlists(false) > 0
                    ADD_MENU_CATEGORY(MC_SHORTLIST)
                ELSE
                    ADD_MENU_CATEGORY(MC_SHORTLIST,FALSE,FALSE,FALSE)
                ENDIF
                IF get_total_active_shortlists(true) > 0
                    ADD_MENU_CATEGORY(MC_RECENTLYUSED)
                ELSE
                    ADD_MENU_CATEGORY(MC_RECENTLYUSED,FALSE,FALSE,FALSE)
                ENDIF
                     
                #IF FEATURE_GEN9_STANDALONE
                ADD_MENU_CATEGORY(MC_QUIT_TO_MAIN_MENU)
                #ENDIF // FEATURE_GEN9_STANDALONE
                ADD_MENU_CATEGORY(MC_QUIT_TO_STORY)
                //ADD_MENU_CATEGORY(MC_QUIT_TO_GTA_ONLINE)
                ADD_MENU_CATEGORY(MC_PLAYDM)
            BREAK
    
            CASE MC_ACTORS
                SET_MENU_TITLE("CM_ACTTIT")
                cprintln(debug_director,"ADd category MC_ANIMALS")
                ADD_MENU_CATEGORY(MC_ANIMALS)                   
                ADD_MENU_CATEGORY(MC_BEACH)
                cprintln(debug_director,"ADd category MC_CITYCROWDS")
                ADD_MENU_CATEGORY(MC_COSTUME)
                ADD_MENU_CATEGORY(MC_DOWNTOWN)
                ADD_MENU_CATEGORY(MC_EMERGENCY) 
                ADD_MENU_CATEGORY(MC_GANG)
                ADD_MENU_CATEGORY(MC_HEIST)
                ADD_MENU_CATEGORY(MC_LABOURERS)
                ADD_MENU_CATEGORY(MC_MILITARY)
                ADD_MENU_CATEGORY(MC_ONLINE)                
                ADD_MENU_CATEGORY(MC_PROFESSIONALS)
                ADD_MENU_CATEGORY(MC_SPECIAL)
                ADD_MENU_CATEGORY(MC_SPORT)
                ADD_MENU_CATEGORY(MC_STORY)
                ADD_MENU_CATEGORY(MC_TRANSPORT)
                ADD_MENU_CATEGORY(MC_UPTOWN)
                ADD_MENU_CATEGORY(MC_VAGRANTS)                                          
            BREAK

            CASE MC_SETTINGS
                SET_MENU_ITEM_TOGGLEABLE(FALSE,TRUE)                
                SET_MENU_TITLE("SETTMNU")
//              ADD_SELECTABLE_MENU(SEM_TIME,settingsMenu[SEM_TIME],"CM_IN_TI",8,"CM_TI_")
//              ADD_SELECTABLE_MENU(SEM_WEATHER,settingsMenu[SEM_WEATHER],"CM_IN_WE",9,"CM_WE_")
//
//              ADD_SELECTABLE_MENU(SEM_WANTED,settingsMenu[SEM_WANTED],"CM_IN_WAN",5,"CM_CR_")
//              ADD_SELECTABLE_MENU(SEM_PEDDENSITY,settingsMenu[SEM_PEDDENSITY],"CM_IN_PED",4,"CM_DE_")
//              ADD_SELECTABLE_MENU(SEM_VEHDENSITY,settingsMenu[SEM_VEHDENSITY],"CM_IN_VED",4,"CM_DE_")
//              ADD_SELECTABLE_MENU(SEM_RESTRICTED,settingsMenu[SEM_RESTRICTED],"CM_IN_RES",2,"CM_RES_")
//              ADD_SELECTABLE_MENU(SEM_INVINCIBLE,settingsMenu[SEM_INVINCIBLE],"CM_IN_INV",2,"CM_RES_")
                CREATE_SETTINGS_MENU_OPTIONS()
                iTotalMenuOptions = SEM_TOTAL
            BREAK

            CASE MC_BEACH
            
            
            
                SET_MENU_TITLE("CM_TITBEA")
                ADD_MENU_CATEGORY(MC_BEAVES,TRUE) //Boardwalk
                ADD_MENU_CATEGORY_MALE(MC_BEABO1) //Bodybuilder
                ADD_MENU_CATEGORY_MALE(MC_BEALOA) //Day Tripper
                ADD_MENU_CATEGORY_MALE(MC_BEABO2) //Muscle Sands
                ADD_MENU_CATEGORY_FEMALE(MC_BEABOD) //Muscle Sands
                ADD_MENU_CATEGORY_MALE(MC_BEAOLD) //Old Timer
                ADD_MENU_CATEGORY(MC_BEADEL) //On the Prom
                ADD_MENU_CATEGORY_MALE(MC_BEASUN) //Sunbather
                ADD_MENU_CATEGORY_MALE(MC_BEASRF) //Surf Rat
                ADD_MENU_CATEGORY(MC_BEATOU) //Tourist
                    

            BREAK
    
            CASE MC_COSTUME
                cprintln(debug_director,"BUILD COSTUME MENU")
                SET_MENU_TITLE("CM_TITCOS")
                ADD_MENU_CATEGORY(MC_COSALI,false)
                ADD_MENU_CATEGORY(MC_COSAST,false)
                ADD_MENU_CATEGORY(MC_COSCLO,false)      
                ADD_MENU_CATEGORY(MC_COSMIM,false)
                ADD_MENU_CATEGORY(MC_COSPOG,false)
                ADD_MENU_CATEGORY(MC_COSSPA,false)
                ADD_MENU_CATEGORY(MC_COSSPY,false)
                ADD_MENU_CATEGORY(MC_COSSTR,false)

            BREAK

            CASE MC_DOWNTOWN
                SET_MENU_TITLE("CM_TITDOW")
                ADD_MENU_CATEGORY_MALE(MC_DOWHAR) //casual
                ADD_MENU_CATEGORY_MALE(MC_DOWDAL) //hooded
                ADD_MENU_CATEGORY_MALE(MC_DOWCO2) //relaxed
                ADD_MENU_CATEGORY_MALE(MC_DOWSTM) //shopper             
                ADD_MENU_CATEGORY_MALE(MC_DOWLAS)
                ADD_MENU_CATEGORY_MALE(MC_DOWMAZ)
                ADD_MENU_CATEGORY_MALE(MC_DOWSTR)   
                ADD_MENU_CATEGORY_MALE(MC_DOWRAN)
                                            
                                
                
                ADD_MENU_CATEGORY_FEMALE(MC_DOWHAR) //casual
                ADD_MENU_CATEGORY_FEMALE(MC_DOWEMO) //relaxed
                ADD_MENU_CATEGORY_FEMALE(MC_DOWSTM) //shopper
                ADD_MENU_CATEGORY_FEMALE(MC_DOWEM2) //sporty
                ADD_MENU_CATEGORY_FEMALE(MC_DOWDAV) //street                
                ADD_MENU_CATEGORY_FEMALE(MC_DOWTEE) //student               
                ADD_MENU_CATEGORY_FEMALE(MC_DOWSOC) //vibrant
                ADD_MENU_CATEGORY_FEMALE(MC_DOWCOL) //youthful
                
                                            
            BREAK
    
            CASE MC_EMERGENCY
                SET_MENU_TITLE("CM_TITEME")
                ADD_MENU_CATEGORY_FEMALE(MC_EMESHE)
                ADD_MENU_CATEGORY_MALE(MC_EMECOA)
                //ADD_MENU_CATEGORY_MALE(MC_EMEDOA) //removed bug 2242611
                ADD_MENU_CATEGORY_MALE(MC_EMEFIB)
                ADD_MENU_CATEGORY_MALE(MC_EMEHIG)
                ADD_MENU_CATEGORY_MALE(MC_EMEIAA)                                           
                ADD_MENU_CATEGORY(MC_EMEBAY)
                ADD_MENU_CATEGORY_MALE(MC_EMEFIR)
                ADD_MENU_CATEGORY(MC_EMEPOL)
                ADD_MENU_CATEGORY_MALE(MC_EMENOO)
                ADD_MENU_CATEGORY_MALE(MC_EMEPAR)

                ADD_MENU_CATEGORY_FEMALE(MC_EMERAN)             

            BREAK

            CASE MC_GANG
                SET_MENU_TITLE("CM_TITGAN")
                ADD_MENU_CATEGORY_FEMALE(MC_GANALT) 
                ADD_MENU_CATEGORY_FEMALE(MC_GANSTR) 
                ADD_MENU_CATEGORY_FEMALE(MC_GANEPSF)
                ADD_MENU_CATEGORY_FEMALE(MC_GANLOH)
                ADD_MENU_CATEGORY_FEMALE(MC_GANCHO)
                
                ADD_MENU_CATEGORY_MALE(MC_GANAL2) //altruist peon
                ADD_MENU_CATEGORY_MALE(MC_GANAL3) //altruist serf
                ADD_MENU_CATEGORY_MALE(MC_GANAL1) //altruist vassal
                ADD_MENU_CATEGORY_MALE(MC_GANAZG) //azteca
                ADD_MENU_CATEGORY_MALE(MC_GANAZB) //azteca boss     
                ADD_MENU_CATEGORY_MALE(MC_GANCHA) //chamberlain families
                ADD_MENU_CATEGORY_MALE(MC_GANDAV) //davis families
                ADD_MENU_CATEGORY_MALE(MC_GANFOR) //forum drive families
                ADD_MENU_CATEGORY_MALE(MC_GANBAE) //east ls balla   
                ADD_MENU_CATEGORY_MALE(MC_GANBLO) //og balla
                ADD_MENU_CATEGORY_MALE(MC_GANOGB) //OG BALLLA BOSS
                ADD_MENU_CATEGORY_MALE(MC_GANEPS2) //epsilonist
                ADD_MENU_CATEGORY_MALE(MC_GANEPS1) //eepsilonist thesis                                             
                ADD_MENU_CATEGORY_MALE(MC_GANKOB) //khangpae boss
                ADD_MENU_CATEGORY_MALE(MC_GANKOL) //khangpae enforcer
                ADD_MENU_CATEGORY_MALE(MC_GANKOG) //khangpae muscle
                ADD_MENU_CATEGORY_MALE(MC_GANLOM) //lost mc
                ADD_MENU_CATEGORY_MALE(MC_GANLOO) //lost officer    
                ADD_MENU_CATEGORY_MALE(MC_GANLOS) //lost road captain
                ADD_MENU_CATEGORY_MALE(MC_GANMGB) //marabunta boss
                ADD_MENU_CATEGORY_MALE(MC_GANMGL) //marabunta enforcer
                ADD_MENU_CATEGORY_MALE(MC_GANMGG) //marabunta grande
                ADD_MENU_CATEGORY_MALE(MC_GANMOG) //mob
                ADD_MENU_CATEGORY_MALE(MC_GANMOB) //mob boss        
                ADD_MENU_CATEGORY_MALE(MC_GANMOL) //mob enforcer                
                ADD_MENU_CATEGORY_MALE(MC_GANBAS) //south rancho balla                                          
                ADD_MENU_CATEGORY_MALE(MC_GANTG1) //Triad 
                ADD_MENU_CATEGORY_MALE(MC_GANTRB) //Trian boss
                ADD_MENU_CATEGORY_MALE(MC_GANTG2) //Triad No. 2
                ADD_MENU_CATEGORY_MALE(MC_GANVAG) //vagos
                ADD_MENU_CATEGORY_MALE(MC_GANVAB) //vagos boss                      
            BREAK
            
            CASE MC_LABOURERS               
                SET_MENU_TITLE("CM_TITLAB")
                ADD_MENU_CATEGORY_MALE(MC_LABBUG) //bugstar
                ADD_MENU_CATEGORY_MALE(MC_LABCN2) //construction                
                ADD_MENU_CATEGORY_MALE(MC_LABFAR) //farmer
                ADD_MENU_CATEGORY_MALE(MC_LABGAR) //gardner
                ADD_MENU_CATEGORY_MALE(MC_LABCN3) //handyman
                ADD_MENU_CATEGORY_MALE(MC_LABCN1) //highway maintenance                 
                ADD_MENU_CATEGORY_MALE(MC_LABJAN) //janitor
                ADD_MENU_CATEGORY_MALE(MC_LABSAD) //longshoreman
                ADD_MENU_CATEGORY_MALE(MC_LABME2) //LS customs
                ADD_MENU_CATEGORY_FEMALE(MC_LABMAI) //Maid (female)
                ADD_MENU_CATEGORY_MALE(MC_LABME1) //mechanic                
                ADD_MENU_CATEGORY(MC_LABMIG) //migrant worker                               
                ADD_MENU_CATEGORY(MC_LABFAC) //slaughterhouse
                ADD_MENU_CATEGORY_MALE(MC_LABGAB) //trash collector

            BREAK

            CASE MC_MILITARY
                SET_MENU_TITLE("CM_TITMIL")             
                ADD_MENU_CATEGORY_MALE(MC_MILCOR)
                ADD_MENU_CATEGORY_MALE(MC_MILPRI)
                ADD_MENU_CATEGORY_MALE(MC_MILMIM)
                ADD_MENU_CATEGORY_MALE(MC_MILSRG)
                ADD_MENU_CATEGORY_MALE(MC_MILMIG)                                                                               
                ADD_MENU_CATEGORY_MALE(MC_MILPL1)
                ADD_MENU_CATEGORY_MALE(MC_MILBAT)
                ADD_MENU_CATEGORY_MALE(MC_MILMEP)
                ADD_MENU_CATEGORY_MALE(MC_MILMEC)
                

            BREAK

            CASE MC_ONLINE
            
                SET_MENU_TITLE("CM_TITONL")

                LOAD_ONLINE_STATS_FOR_DIRECTOR_MODE()
                
                //make only the active mps selectable
                if sOnlineChar[0].bSetup
                    ADD_MENU_CATEGORY(MC_ONLINE1)
                else
                    ADD_MENU_CATEGORY(MC_ONLINE1,FALSE,FALSE,FALSE)
                endif
                if sOnlineChar[1].bSetup
                    ADD_MENU_CATEGORY(MC_ONLINE2)
                else
                    ADD_MENU_CATEGORY(MC_ONLINE2,FALSE,FALSE,FALSE)
                endif           
                
            BREAK

            CASE MC_PROFESSIONALS
                SET_MENU_TITLE("CM_TITPRO")
                ADD_MENU_CATEGORY_MALE(MC_PROAIR) //Airlinbe Pilot
                ADD_MENU_CATEGORY_MALE(MC_PROBOU) //bouncer
                ADD_MENU_CATEGORY_MALE(MC_PROVEN) //business casual
                ADD_MENU_CATEGORY_MALE(MC_PROCHE) //chef
                ADD_MENU_CATEGORY_MALE(MC_PRODOC) //doctor
                ADD_MENU_CATEGORY_MALE(MC_PRODRA) //drag queen
                ADD_MENU_CATEGORY_MALE(MC_PROBAR) //Hairdresser             
                ADD_MENU_CATEGORY_MALE(MC_PROIT) //It specialist
                ADD_MENU_CATEGORY_MALE(MC_PROCOO) //kitchen porter
                ADD_MENU_CATEGORY_MALE(MC_PROMAR) //mariachi
                ADD_MENU_CATEGORY_MALE(MC_PROSTY) //media type
                ADD_MENU_CATEGORY_MALE(MC_PROOFF) //office drone
                ADD_MENU_CATEGORY_MALE(MC_PROSCI) //scientist
                ADD_MENU_CATEGORY_MALE(MC_PROCEO) //suited
                ADD_MENU_CATEGORY_MALE(MC_PROVAL) //valet
                ADD_MENU_CATEGORY_MALE(MC_PROWAI) //waiter

                
                ADD_MENU_CATEGORY_FEMALE(MC_PROAIH) //air hostess
                ADD_MENU_CATEGORY_FEMALE(MC_PROTEA) //business casual
                ADD_MENU_CATEGORY_FEMALE(MC_PROAUP) //fashion               
                ADD_MENU_CATEGORY_FEMALE(MC_PROHAI) //hairdresser
                ADD_MENU_CATEGORY_FEMALE(MC_PROLAW) //legal team
                ADD_MENU_CATEGORY_FEMALE(MC_PROSTY) //Media Type
                ADD_MENU_CATEGORY_FEMALE(MC_PROFAS) //model 
                ADD_MENU_CATEGORY_FEMALE(MC_PROINT) //office cool
                ADD_MENU_CATEGORY_FEMALE(MC_PROOFF) //office drone
                ADD_MENU_CATEGORY_FEMALE(MC_PROPON) //ponsonbys
                ADD_MENU_CATEGORY_FEMALE(MC_PROVEN) //public relations
            //  ADD_MENU_CATEGORY_FEMALE(MC_PROREL) //real estate
                ADD_MENU_CATEGORY_FEMALE(MC_PROSTR) //streetwalker
            //  ADD_MENU_CATEGORY_FEMALE(MC_PROBIN) //suburban
            //  ADD_MENU_CATEGORY_FEMALE(MC_PROCEO) //suited
            BREAK

            CASE MC_SPORT
                SET_MENU_TITLE("CM_TITSPO")
                ADD_MENU_CATEGORY_MALE(MC_SPOCYC) //bike courier
                ADD_MENU_CATEGORY_MALE(MC_SPOROC) //bike pro
                ADD_MENU_CATEGORY_MALE(MC_SPORU2) //jogger
                
                ADD_MENU_CATEGORY_FEMALE(MC_SPOGOF)
                ADD_MENU_CATEGORY_MALE(MC_SPOGO1) //golf junior
                ADD_MENU_CATEGORY_MALE(MC_SPOGO2) //golf senior
                ADD_MENU_CATEGORY_MALE(MC_SPOMO1) //motocross armour
                ADD_MENU_CATEGORY_MALE(MC_SPOMO2) //motocross leather
                
                ADD_MENU_CATEGORY_FEMALE(MC_SPORUN)                             
                ADD_MENU_CATEGORY_FEMALE(MC_SPOSKA)
                ADD_MENU_CATEGORY_MALE(MC_SPORU1) //runner
                ADD_MENU_CATEGORY_MALE(MC_SPOSK1) //skate grunge
                ADD_MENU_CATEGORY_MALE(MC_SPOSK2) 
                ADD_MENU_CATEGORY_MALE(MC_SPOSK3)
                ADD_MENU_CATEGORY_MALE(MC_SPOSUR)
                ADD_MENU_CATEGORY(MC_SPOTEN)
                ADD_MENU_CATEGORY(MC_SPOYOG)

            BREAK
        
            CASE MC_TRANSPORT
                SET_MENU_TITLE("CM_TITTRA")                                         
                ADD_MENU_CATEGORY_MALE(MC_TRAPO1) //go postal depot
                ADD_MENU_CATEGORY_MALE(MC_TRAPO2) //go postal driver
                ADD_MENU_CATEGORY_MALE(MC_TRAAR2) //grupe sechs driver
                ADD_MENU_CATEGORY_MALE(MC_TRAAR1) //gruppe sechs officer                
                ADD_MENU_CATEGORY_MALE(MC_TRATRA) //ls transit
                ADD_MENU_CATEGORY_MALE(MC_TRAPO3) //post op depot
                ADD_MENU_CATEGORY_MALE(MC_TRAPO4) //post op driver
                ADD_MENU_CATEGORY_MALE(MC_TRAAIR)   //ramp agent
                ADD_MENU_CATEGORY_MALE(MC_TRATRU) //trucker
            BREAK
                
            CASE MC_UPTOWN
                SET_MENU_TITLE("CM_TITUPT")
                ADD_MENU_CATEGORY_FEMALE(MC_UPTROH)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTBOH)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTCOU)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTPOR)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTHIP)
                ADD_MENU_CATEGORY_MALE(MC_UPTEAS)
                ADD_MENU_CATEGORY_MALE(MC_UPTECC)
                ADD_MENU_CATEGORY_MALE(MC_UPTROC)               
                ADD_MENU_CATEGORY_FEMALE(MC_UPTART)
                //ADD_MENU_CATEGORY_FEMALE(MC_UPTVIB)
                ADD_MENU_CATEGORY_MALE(MC_UPTMIR)
                ADD_MENU_CATEGORY_MALE(MC_UPTHAW)
                ADD_MENU_CATEGORY_MALE(MC_UPTVIN)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTWAN)
                ADD_MENU_CATEGORY_FEMALE(MC_UPTRIC)
                
                
                
            BREAK
    
            CASE MC_VAGRANTS            
                SET_MENU_TITLE("CM_TITVAG")
                ADD_MENU_CATEGORY_MALE(MC_VAGYOU)               
                ADD_MENU_CATEGORY_MALE(MC_VAGPR2)
                ADD_MENU_CATEGORY_MALE(MC_VAGPR1)               
                ADD_MENU_CATEGORY_MALE(MC_VAGOLD)
                ADD_MENU_CATEGORY(MC_VAGMIS)
                ADD_MENU_CATEGORY(MC_VAGDO2)
                ADD_MENU_CATEGORY_MALE(MC_VAGMET)
                ADD_MENU_CATEGORY_MALE(MC_VAGME2)
                ADD_MENU_CATEGORY(MC_VAGDO1)                                                
                ADD_MENU_CATEGORY_MALE(MC_VAGSAN)
                ADD_MENU_CATEGORY_FEMALE(MC_VAGSAN)
                
                    
            BREAK
    
            CASE MC_STORY
                SET_MENU_TITLE("CM_TITSTO")
                ADD_MENU_CATEGORY(MC_STOFRA,FALSE)
                ADD_MENU_CATEGORY(MC_STOMIC,FALSE)
                ADD_MENU_CATEGORY(MC_STOTRE,FALSE)
                ADD_MENU_CATEGORY(MC_STOAMA,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_STOBEV,FALSE)
                ADD_MENU_CATEGORY(MC_STOBRA,FALSE)
                ADD_MENU_CATEGORY(MC_STOTAO,FALSE)
                ADD_MENU_CATEGORY(MC_STOCHR,FALSE)
                ADD_MENU_CATEGORY(MC_STODAV,FALSE)
                ADD_MENU_CATEGORY(MC_STODEV,FALSE)
                ADD_MENU_CATEGORY(MC_STODRF,FALSE)
                ADD_MENU_CATEGORY(MC_STOFAB,FALSE)
                ADD_MENU_CATEGORY(MC_STOFLO,FALSE)
                ADD_MENU_CATEGORY(MC_STOJIM,FALSE)
                ADD_MENU_CATEGORY(MC_STOLAM,FALSE)
                ADD_MENU_CATEGORY(MC_STOLAZ,FALSE)
                //ADD_MENU_CATEGORY(MC_STOLES,FALSE)            
                ADD_MENU_CATEGORY(MC_STOMAU,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_STOTHO,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_STONER,FALSE)
                ADD_MENU_CATEGORY(MC_STOPAT,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_STOSIM,FALSE)
                ADD_MENU_CATEGORY(MC_STOSOL,FALSE)
                ADD_MENU_CATEGORY(MC_STOSTE,FALSE)
                ADD_MENU_CATEGORY(MC_STOSTR,FALSE)
                ADD_MENU_CATEGORY(MC_STOTAN,TRUE,FALSE)                     
                ADD_MENU_CATEGORY(MC_STOTRA,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_STOWAD,FALSE)
            BREAK
            CASE MC_ANIMALS
                SET_MENU_TITLE("CM_TITANI")             
                ADD_MENU_CATEGORY(MC_ANICAT)                                
                ADD_MENU_CATEGORY(MC_ANICHI)
                ADD_MENU_CATEGORY(MC_ANICOR)
                ADD_MENU_CATEGORY(MC_ANICRO)
                ADD_MENU_CATEGORY(MC_ANIHEN)
                ADD_MENU_CATEGORY(MC_ANIPIN)
                ADD_MENU_CATEGORY(MC_ANIRAB)
                ADD_MENU_CATEGORY(MC_ANISEA)
                ADD_MENU_CATEGORY(MC_ANIBOAR)
                ADD_MENU_CATEGORY(MC_ANICOW) 
                ADD_MENU_CATEGORY(MC_ANICOY)
                ADD_MENU_CATEGORY(MC_ANIDEE)
                ADD_MENU_CATEGORY(MC_ANIHUS) 
                ADD_MENU_CATEGORY(MC_ANIMOU) 
                ADD_MENU_CATEGORY(MC_ANIPIG)
                ADD_MENU_CATEGORY(MC_ANIPOO) 
                ADD_MENU_CATEGORY(MC_ANIPUG)
                ADD_MENU_CATEGORY(MC_ANIRET) 
                ADD_MENU_CATEGORY(MC_ANIROT)                            
                ADD_MENU_CATEGORY(MC_ANISHE) 
                ADD_MENU_CATEGORY(MC_ANIWES) 
                ADD_MENU_CATEGORY(MC_ANISAS)
                
            BREAK
            CASE MC_SPECIAL
                SET_MENU_TITLE("CM_TITSPE")
                ADD_MENU_CATEGORY(MC_SPEAND,FALSE)
                ADD_MENU_CATEGORY(MC_SPEBAY,FALSE)
                ADD_MENU_CATEGORY(MC_SPEBIL,FALSE)
                ADD_MENU_CATEGORY(MC_SPECLI,FALSE)
                ADD_MENU_CATEGORY(MC_SPEGRI,FALSE)
                ADD_MENU_CATEGORY(MC_SPEIMP,FALSE)
                ADD_MENU_CATEGORY(MC_SPEJAN,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_SPEJER,FALSE)
                ADD_MENU_CATEGORY(MC_SPEJES,FALSE)
                ADD_MENU_CATEGORY(MC_SPEMAN,FALSE)
                //ADD_MENU_CATEGORY(MC_SPEMIM,FALSE)
                ADD_MENU_CATEGORY(MC_SPEPAM,TRUE,FALSE)             
                ADD_MENU_CATEGORY(MC_SPEZOM,FALSE)
            BREAK
            CASE MC_HEIST
                SET_MENU_TITLE("CM_TITHEI")
                ADD_MENU_CATEGORY(MC_HEICHE,FALSE)
                ADD_MENU_CATEGORY(MC_HEICHR,FALSE)
                ADD_MENU_CATEGORY(MC_HEIDAR,FALSE)
                ADD_MENU_CATEGORY(MC_HEIEDD,FALSE)
                ADD_MENU_CATEGORY(MC_HEIGUS,FALSE)
                ADD_MENU_CATEGORY(MC_HEIHUG,FALSE)
                ADD_MENU_CATEGORY(MC_HEIKRM,FALSE)
                ADD_MENU_CATEGORY(MC_HEIKAR,FALSE)              
                ADD_MENU_CATEGORY(MC_HEINOR,FALSE)
                ADD_MENU_CATEGORY(MC_HEIPAC,FALSE)
                ADD_MENU_CATEGORY(MC_HEIPAI,TRUE,FALSE)
                ADD_MENU_CATEGORY(MC_HEIRIC,FALSE)
                ADD_MENU_CATEGORY(MC_HEITAL,TRUE,FALSE)
            BREAK                   
            CASE MC_SHORTLIST
                SET_MENU_TITLE("CM_SHTIT")
                
                FOR i = 0 to 9
                    CDEBUG3LN(debug_director,"Shortlist ",i," = ",shortlistData[i].category)
                    IF shortlistData[i].category != MC_VOID
                        menuDisplaySex = shortlistData[i].bmale
                        ADD_MENU_CATEGORY(shortlistData[i].category,!shortlistData[i].bmale,shortlistData[i].bmale,TRUE)
                    ENDIF
                ENDFOR
            BREAK
            CASE MC_RECENTLYUSED
                SET_MENU_TITLE("CM_RETIT")
                
                FOR i = 10 to 19
                    CDEBUG3LN(debug_director,"Shortlist ",i," = ",shortlistData[i].category)
                    IF shortlistData[i].category != MC_VOID
                        menuDisplaySex = shortlistData[i].bmale
                        ADD_MENU_CATEGORY(shortlistData[i].category,!shortlistData[i].bmale,shortlistData[i].bmale,TRUE)                        
                    ENDIF
                ENDFOR
            BREAK
        ENDSWITCH


        
        IF displayedMenuData[curMenuItem].category = MC_VOID
        AND NOT (dirCurrentMenu = MC_SETTINGS)  //Ignore settings categories
            int iMenuEntry
            IF GET_FIRST_UNLOCKED_MENU_ITEM(iMenuEntry,curMenuItem)                     
                SET_CURRENT_MENU_ITEM(iMenuEntry)
                curMenuItem = iMenuEntry
            ENDIF
        ENDIF
        
        //when backing up through menus, set current item to last menu category
        IF menuCategory < MC_START_ACTORS
            IF !retainCurrentSelection          
                INT j
                REPEAT COUNT_OF(displayedMenuData) j
                    IF displayedMenuData[j].category = lastMenuCategory
                        curMenuItem = j
                        j = COUNT_OF(displayedMenuData)
                    ENDIF
                ENDREPEAT                
            ENDIF
        ENDIF
                          
        SET_CURRENT_MENU_ITEM(curMenuItem)

        REBUILD_HELP_KEYS()
        
        //HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()    
        
        //SET_MENU_USES_HEADER_GRAPHIC(TRUE, "Director_Editor_Title", "Director_Editor_Title")
    ENDIF           
ENDPROC

PROC CONTROL_GESTURES() 
    IF NOT IS_PED_INJURED(player_ped_id())
        //commenting out pending net_interaction changes. See bug 2185673
        UPDATE_INTERACTION_ANIMS_SP()
        
        IF IS_ANY_INTERACTION_ANIM_PLAYING()
            SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),FALSE)
        ENDIF
        
    ENDIF
/*
    IF iLastGestureSelected != selectedPI[PI_M0_GESTURE]
        IF IS_STRING_NULL_OR_EMPTY(tAnimDictToLoad)
        OR (NOT IS_STRING_NULL_OR_EMPTY(tAnimDictToLoad) AND NOT IS_ENTITY_PLAYING_ANIM(player_ped_id(),tAnimDictToLoad,tAnimToPlay))
    
            iLastGestureSelected = selectedPI[PI_M0_GESTURE]
            int animIndex = enum_to_int(PLAYER_INTERACTION_NONE)+selectedPI[PI_M0_GESTURE]+1
            GET_CELEBRATION_ANIM_TO_PLAY(animIndex,bMenu_sex_male,false,tAnimDictToLoad,tAnimToPlay)    
            IF NOT IS_STRING_NULL_OR_EMPTY(tLoadedAnimDict)
                REMOVE_ANIM_DICT(tLoadedAnimDict)   
            ENDIF
            
            REQUEST_ANIM_DICT(tAnimDictToLoad)
            tLoadedAnimDict = tAnimDictToLoad
        ENDIF
    ENDIF*/
ENDPROC

bool bDisplayExitHelpText = FALSE
bool bCanDisplayVehHelpText
bool bDisplayedWeaponsLimitedHelp = FALSE
model_names badModelName
int iLastHelpMessageTime = 0

PROC CONTROL_HELPTEXT()

    //========PLAY MODE Help management
    IF directorFlowState = dfs_PLAY_MODE
        
        //Exit Director Mode help text
        IF bDisplayExitHelpText
            IF g_savedGlobals.sDirectorModeData.iExitHelpShownCount < 3
                PRINT_HELP("CM_WHLP02")
                g_savedGlobals.sDirectorModeData.iExitHelpShownCount++
                bDisplayExitHelpText = FALSE
                CPRINTLN(DEBUG_DIRECTOR, "Displayed exit help message. New count is ", g_savedGlobals.sDirectorModeData.iExitHelpShownCount, ".")
            ENDIF
        ENDIF
                
        //Vehicle use disabled help
        IF IS_DISABLED_CONTROL_JUST_PRESSED(player_control,INPUT_ENTER)
            IF bCanDisplayVehHelpText
                IF NOT IS_PED_VALID_FOR_VEHICLE(player_ped_ID())    
                    VEHICLE_INDEX vehArray[1]
                    IF GET_PED_NEARBY_VEHICLES(player_ped_ID(),vehArray) > 0
                        IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_ID(),vehArray[0]) < 10.0
                            PRINT_HELP("CM_WHLP01")
                            bCanDisplayVehHelpText = FALSE
                            badModelName = GET_ENTITY_MODEL(player_ped_ID())
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
        IF NOT bCanDisplayVehHelpText
            IF NOT IS_PED_INJURED(player_ped_ID())
                IF GET_ENTITY_MODEL(player_ped_ID()) != badModelName
                    bCanDisplayVehHelpText = TRUE
                ENDIF
            ENDIF 
        ENDIF
        
        //cover use disabled
        IF bCoverIsBlocked
            IF !bCoverHelpGiven
                IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_COVER)
                AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                    bCoverHelpGiven = TRUE
                    PRINT_HELP("CM_CHLP01")
                    badModelName = GET_ENTITY_MODEL(player_ped_ID())
                ENDIF
            ELSE
                IF GET_ENTITY_MODEL(player_ped_ID()) != badModelName
                    bCoverHelpGiven = FALSE
                ENDIF
            ENDIF
        ELSE
            IF bCoverHelpGiven
                bCoverHelpGiven = FALSE
            ENDIF
        ENDIF
            
        //Weapon wheel help text
        IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
        AND GET_GAME_TIMER() - iLastHelpMessageTime > 5000          //5 seconds default help message wait
            IF CAN_PLAYER_MODEL_HOLD_WEAPONS() //this was checking if player is valid for vehicles. Sure this should have been weapons? Kev. Bug 2233829.
                IF NOT bDisplayedWeaponsLimitedHelp
                AND NOT IS_HELP_MESSAGE_ON_SCREEN()
                AND g_savedGlobals.sDirectorModeData.iExitHelpShownCount < 3
                    PRINT_HELP("CM_WHLP03",3000)
                    bDisplayedWeaponsLimitedHelp = TRUE
                ENDIF
            ELSE
                PRINT_HELP("CM_WHLP01WPN",3000)
            ENDIF

        ENDIF
        
    ENDIF   
ENDPROC

PROC CONTROL_FORCE_UPDATES()
    IF bForceRebuildPIMenu
        IF PImenuON
        AND directorFlowState = dfs_PLAY_MODE           
            REBUILD_PI_MENU()
        ENDIF
        bForceRebuildPIMenu = FALSE
    ENDIF   
    
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        IF IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
            IF bGesturesDisabled
            OR IS_AIM_CAM_ACTIVE()          
                SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),FALSE)
            ELSe
                SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),TRUE)
            ENDIF
        ENDIF
        
        REVISE_COVER_USAGE_FOR_PED()
        
    ENDIF
    
    
    IF GET_DIRECTOR_FLOW_STATE() = dfs_PLAY_MODE
        //remove clothing items if bad variations when ped is in vehicle.
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
                IF !HAS_PLAYER_HAD_A_VEHICLE_COMPONENT_BLOCKED()
                    UPDATE_MODEL_VEHICLE_ISSUES(true)                   
                ENDIF
            ENDIF
        ELSE
            IF HAS_PLAYER_HAD_A_VEHICLE_COMPONENT_BLOCKED()
                UPDATE_MODEL_VEHICLE_ISSUES(false)
            ENDIF
        ENDIF
        
        //block phone for severe clipping issues in selfie mode.
        IF IS_PLAYER_MODEL_BLOCKED_FOR_CELLPHONE()
            DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
        ENDIF
    
        
    ENDIF
ENDPROC

PROC SET_LOAD_NEW_MODEL_VARIATION(bool isRandomiseRequest = FALSE)

    bRequireNewActorModel = TRUE
    selectedHeistMember = CM_EMPTY
    If isRandomiseRequest
        //clear out data on previously stored model to ensure later checks against this don't rpevent model updating
        activeMenuEntry = MC_VOID
    ELSE
    
        lastSelectedHeistMember = CM_EMPTY
    ENDIF
ENDPROC

PROC REMOVE_EXISTING_MODELS()
    cprintln(debug_director,"REMOVE_EXISTING_MODELS()")
    IF IS_MODEL_VALID(loadedModel)
        SET_MODEL_AS_NO_LONGER_NEEDED(loadedModel)
    ENDIF   
    IF IS_MODEL_VALID(requestedModel)
        SET_MODEL_AS_NO_LONGER_NEEDED(requestedModel)
    ENDIF
ENDPROC

PROC CLEAR_MODEL_LOAD_REQUEST()

    bLoadingNewModel = FALSE
    requestedModel = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

PROC REQUEST_NEW_ACTOR_MODEL()
    cprintln(DEBUG_DIRECTOR,"REQUEST_NEW_ACTOR_MODEL() dirCurrentMenu = ",dirCurrentMenu)
    IF dirCurrentMenu > MC_START_ACTOR_CATEGORIES
    OR dirCurrentMenu = MC_SHORTLIST
    OR dirCurrentMenu = MC_RECENTLYUSED
        IF dirCurrentMenu != MC_SHORTLIST
        AND dirCurrentMenu != MC_RECENTLYUSED
            cprintln(DEBUG_DIRECTOR,"REQUEST_NEW_ACTOR_MODEL() curmenuitem = ",curMenuItem," category : ",displayedMenuData[curMenuItem].category," index : ",displayedMenuData[curMenuItem].index)
            SWITCH displayedMenuData[curMenuItem].category
                CASE  MC_SEX //player has just changed to a new gender.
                    int iMenuEntry
                    IF GET_FIRST_UNLOCKED_MENU_ITEM(iMenuEntry,curMenuItem)
                        //IF activeMenuEntry != displayedMenuData[iMenuEntry]
                                lastSelectableModelCategory = displayedMenuData[iMenuEntry].category
                                lastSelectableModelIndex = displayedMenuData[iMenuEntry].index
                                requestedModel = GET_MODEL_TO_LOAD(displayedMenuData[iMenuEntry].category,displayedMenuData[iMenuEntry].index)
                                activeMenuEntry = displayedMenuData[iMenuEntry].category
                                activeMenuIndex = displayedMenuData[iMenuEntry].index
                        //ELSE
                        //  CLEAR_MODEL_LOAD_REQUEST()
                        //ENDIF
                    ELSE
                        lastSelectableModelCategory = MC_VOID
                            lastSelectableModelIndex = 0
                        requestedModel = DUMMY_MODEL_FOR_SCRIPT
                    //  activeMenuEntry = MC_VOID
                    ENDIF
                BREAK
                CASE MC_VOID
                    requestedModel = DUMMY_MODEL_FOR_SCRIPT
                    //activeMenuEntry = MC_VOID
                BREAK
                DEFAULT 
                        requestedModel = GET_MODEL_TO_LOAD(displayedMenuData[curMenuItem].category,displayedMenuData[curMenuItem].index)
                        activeMenuEntry = displayedMenuData[curMenuItem].category
                        activeMenuIndex = displayedMenuData[curMenuItem].index
                BREAK
            ENDSWITCH

        ELSE
            IF dirCurrentMenu = MC_SHORTLIST
                requestedModel = GET_PED_VARIATION_FROM_SHORTLIST(false)
            ELSE
                requestedModel = GET_PED_VARIATION_FROM_SHORTLIST(true)
            ENDIF
            cprintln(DEBUG_DIRECTOR,"REQUEST_NEW_ACTOR_MODEL() model requested = ",requestedModel)
            activeMenuEntry = displayedMenuData[curMenuItem].category
            activeMenuIndex = displayedMenuData[curMenuItem].index
        ENDIF
    ENDIF
    
    bRequireNewActorModel = FALSE
    
    IF IS_MODEL_VALID(requestedModel)   
        REQUEST_MODEL(requestedModel)
        cprintln(DEBUG_DIRECTOR,"REQUEST_NEW_ACTOR_MODEL() new model : ",GET_MODEL_NAME_FOR_DEBUG(requestedModel)," activeMenuEntry = ",activeMenuEntry)
        bLoadingNewModel = TRUE
    ELSE
        bLoadingNewModel = FALSE
    ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_SELECTED_A_NEW_MENU_ACTOR()
    IF directorFlowState = dfs_RUN_TRAILER_SCENE    

        IF dirCurrentMenu > MC_MAIN
        AND displayedMenuData[curMenuItem].category != MC_SEX
        AND displayedMenuData[curMenuItem].category != MC_VOID
            IF (dirCurrentMenu = MC_SHORTLIST AND IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(curMenuItem))
            OR (dirCurrentMenu = MC_RECENTLYUSED AND IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(curMenuItem+cTOTAL_SHORTLIST_ENTRIES))
            OR (dirCurrentMenu != MC_SHORTLIST AND dirCurrentMenu != MC_RECENTLYUSED AND (lastSelectableModelCategory != displayedMenuData[curMenuItem].category OR lastSelectableModelIndex != displayedMenuData[curMenuItem].index))
            //IF (dirCurrentMenu = MC_SHORTLIST AND curMenuItem != lastMenuItem) //selected 
            //OR (dirCurrentMenu != MC_SHORTLIST AND lastSelectableModelCategory != displayedMenuData[curMenuItem]) //always switch model when a new menu category selected, or if on shortlist category skip that check, as same category can appear multiple times
                //ensure new menu item doesn't match the existing displayed menu item
                IF displayedMenuData[curMenuItem].category > MC_START_ACTORS
                    lastSelectableModelCategory = displayedMenuData[curMenuItem].category
                    lastSelectableModelIndex = displayedMenuData[curMenuItem].index
                    
                RETURN TRUE
                endif
            ENDIF
        ENDIF

    ENDIF
    RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_MOST_RECENTLY_SELECTED_VALID_MODEL()
    IF IS_MODEL_VALID(requestedModel)
        RETURN requestedModel
    ELIF IS_MODEL_VALID(loadedModel)
        RETURN loadedModel
    ENDIF
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC UPDATE_LOADED_MODEL()  
    //lastMenuItem = curMenuItem
    IF bRequireNewActorModel            
        IF displayedMenuData[curMenuItem].category != MC_VOID
            REMOVE_EXISTING_MODELS()
            REQUEST_NEW_ACTOR_MODEL()           
        ELSE
            bRequireNewActorModel = FALSE
            bLoadingNewModel = FALSE        
        ENDIF
    ENDIF
    
ENDPROC



PROC CHECK_SWITCH_CAM_LOCATION(int iLastItem=-1)
    cprintln(debug_director,"CHECK_SWITCH_CAM_LOCATION() selected menu = ",displayedMenuData[curMenuItem].category)
    iLastItem=iLastItem
    
    IF dirCurrentMenu = MC_SETTINGS 
        cprintln(debug_director,"CHECK_SWITCH_CAM_LOCATION() dirCurrentMenu = MC_SETTINGS")     
    ELSE
        if  dirCamState = DIR_CAM_TRAILER_AT_ANIMALS
        OR dirCamState = DIR_CAM_TRAILER_AT_TRAILER
        OR dirCamState = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
            IF IS_MENU_CATEGORY_A_SMALL_ANIMAL(displayedMenuData[curMenuItem].category)
            AND dirCamState != DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                IF dirCamState = DIR_CAM_TRAILER_AT_TRAILER
                    bBlockOriginalPedDelete = TRUE
                endif
                dirMenuState = dirMENU_PERFORMING_PAN
                SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_SMALL_ANIMALS)
            ENDIF
            
            IF IS_MENU_CATEGORY_A_BIG_ANIMAL(displayedMenuData[curMenuItem].category)
            AND dirCamState != DIR_CAM_TRAILER_AT_ANIMALS
                IF dirCamState = DIR_CAM_TRAILER_AT_TRAILER
                    bBlockOriginalPedDelete = TRUE
                endif
                dirMenuState = dirMENU_PERFORMING_PAN
                SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_ANIMALS)
            ENDIF
            
            IF !IS_MENU_CATEGORY_AN_ANIMAL(displayedMenuData[curMenuItem].category)
            AND displayedMenuData[curMenuItem].category > MC_START_ACTORS
            AND dirCamState != DIR_CAM_TRAILER_AT_TRAILER
                bBlockOriginalPedDelete = TRUE
                dirMenuState = dirMENU_PERFORMING_PAN
                SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_TRAILER)
            ENDIF
        endif
    ENDIF
ENDPROC

int uppr, iLastPickedNumber , iHackerOutFit,iGunmanOutfit, iDriverOutfit
//INT lowr,feet,textA, textB
int iStoredVariationA,iStoredVariationB

//make sure all random variations are shown once before beginning again
FUNC INT GET_RANDOM_INT_NOT_SAME_AS_LAST(int istart, int iend)
    
    int iMem[64]
    int iEntries = iend - istart - 1
    
    //get unused entry data from bitsets
    int i,iMemEntry
    for i=0 to iEntries
        if i < 32
            if !IS_BIT_SET(iStoredVariationA,i)
                iMem[iMemEntry] = i
                iMemEntry++
            endif
        else
            if !IS_BIT_SET(iStoredVariationB,i-32)
                iMem[iMemEntry] = i
                iMemEntry++
            endif
        endif
    endfor
    
    //check all variations have been toggled through and reset bitsets
    int picked
    INT iReturned

    if iMemEntry = 0
        iStoredVariationA = 0
        iStoredVariationB = 0
        
              
        picked = GET_RANDOM_INT_IN_RANGE(0,iEntries)
        
        //make sure can't pick last chosen entry
        if picked = iLastPickedNumber
            picked++
            if picked >= iEntries
                picked = 0
            endif
        endif
        iMem[picked] = picked
        iReturned = picked + iStart     
    ELSE
        picked = GET_RANDOM_INT_IN_RANGE(0,iMemEntry)       
        iReturned = iMem[picked] + iStart
    endif
    
    if iMem[picked] < 32
        SET_BIT(iStoredVariationA,iMem[picked])
    else
        SET_BIT(iStoredVariationB,iMem[picked]-32)
    endif
    
    iLastPickedNumber = iMem[picked]

    
    //set bit
    
    
    return iReturned
    
ENDFUNC

PROC SET_PLAYER_SCUBA_STATUS()
    
    IF IS_PLAYER_WEARING_SCUBA_GEAR()                       
        SET_ENABLE_SCUBA(PLAYER_PED_ID(),TRUE)
        SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), 100000000.00)
        SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
        SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_ID(),true)    
        CPRINTLN(debug_director,"SET_PLAYER_SCUBA_STATUS: TRUE")
    ELSE
        SET_ENABLE_SCUBA(PLAYER_PED_ID(),FALSE)
        SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_ID(),false)
        IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())
            SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), -1)
            SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
        ENDIF
    ENDIF
    
    //check if player has ped variations that aren't suitable for diving gear
    CPRINTLN(debug_director,"Model = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(player_ped_id()))," drawable = ",GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO))
   
    SWITCH GET_ENTITY_MODEL(player_ped_id())
        CASE PLAYER_ZERO //michael
            SWITCH GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
                CASE 1 FALLTHRU             
                CASE 5
                    CPRINTLN(debug_director,"DISABLE AUTO GIVE SCUBA")
                    SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(),FALSE)
                BREAK
            ENDSWITCH
        BREAK
        CASE PLAYER_ONE //franklin
            SWITCH GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
                CASE 10
                    SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(),FALSE)
                BREAK
            ENDSWITCH
        BREAK
        CASE PLAYER_TWO //Trevor
            SWITCH GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO)
                CASE 1 FALLTHRU
                CASE 2
                    SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(),FALSE)
                BREAK
            ENDSWITCH
        BREAK
    ENDSWITCH
    
ENDPROC

PROC FIX_BAD_PED_COMPONENT_VARIAITONS(model_names thisModel)
    CPRINTLN(debug_director,"FIX_BAD_PED_COMPONENT_VARIAITONS")
    SWITCH thisModel
        CASE A_F_M_BodyBuild_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_HAIR,2)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_HAIR,pick(0,1),pick(0,1,2))
            ENDIF
        BREAK
        CASE A_F_Y_BevHills_04
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_MODEL_VARIATION(player_ped_id(),0,0,pick(0,1),1,1)                                              
            ENDIF
        BREAK
        CASE A_F_Y_EastSA_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1,pick(0,1))
            ENDIF
        BREAK
        
        CASE A_F_Y_RUNNER_01
        FALLTHRU
        CASE A_M_Y_BEACH_02
        FALLTHRU
        CASE A_M_Y_Runner_01
        FALLTHRU        
        CASE A_M_Y_Skater_01
        FALLTHRU
        CASE A_M_Y_Skater_02
        FALLTHRU
        CASE a_m_y_vinewood_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1,0)
            ENDIF
        BREAK
        CASE G_F_Y_LOST_01
            IF GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 0
            AND GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 0
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_HEAD,1,0)
            ENDIF
        BREAK
        CASE A_M_O_ACULT_01
        CASE A_M_Y_ACULT_01
        
                IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_LEG,0)                                                
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,1,pick(0,1,2))
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)
                ENDIF
    
        BREAK
        
        CASE S_M_Y_BAYWATCH_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,1,0)
            ENDIF
        BREAK
        
        CASE S_F_Y_BAYWATCH_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,0,0)
            ENDIF
        BREAK

        CASE a_m_y_vinewood_02
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,2,0)      
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,0,0)
            ENDIF
        BREAK
        CASE u_m_m_spyactor
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,2,0)
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,2,0)
        BREAK                           
        CASE S_M_M_Janitor
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,0,0)
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,0)
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)
        BREAK
        CASE ig_beverly
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,1,0)
        BREAK
        CASE ig_chrisformage
        FALLTHRU
        CASE ig_brad
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)                                                  
        BREAK
        CASE S_M_Y_Chef_01
            IF !BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0,0)
            ENDIF
        BREAK
        CASE S_F_Y_Factory_01
            
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1,0)
            ENDIF
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                if GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(),PED_COMP_HEAD) = 0
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,pick(0,1))
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,pick(0,1))
                ELIF GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(),PED_COMP_HEAD) = 1
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,pick(2,3))
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,pick(2,3))
                ELIF GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(),PED_COMP_HEAD) = 2
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,pick(4,5))
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,pick(4,5))
                ENDIF               
            ENDIF
        BREAK
        CASE S_F_Y_Migrant_01 FALLTHRU
        CASE S_M_M_MIGRANT_01 
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1,0)
            ENDIF
        BREAK
        CASE S_M_Y_XMECH_02
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0,0)
            ENDIF
        BREAK
        CASE S_M_M_Pilot_02
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)
            ENDIF
        BREAK
        CASE S_M_M_LIFEINVAD_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 0 //black
                        
                int irandupper 
                irandupper = GET_RANDOM_INT_IN_RANGE(0,3)
                if irandupper = 2
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,irandupper,pick(2,3))
                else
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,irandupper,pick(0,1,2))
                endif               
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,pick(0,1),pick(2,3))
                
            Elif GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 1 //white
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,2,pick(0,1))
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,pick(0,1),pick(0,1))
            endif
        BREAK
        CASE U_F_Y_poppymich
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_LEG,1)
            or BLOCKED_VARIATION(player_ped_id(),PED_COMP_LEG,2)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,0)
            ENDIF
        BREAK
        CASE A_M_Y_MOTOX_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_SPECIAL,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0,pick(0,1))
            ENDIF
        BREAK
        CASE S_F_Y_AIRHOSTESS_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_HAIR,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_HAIR,pick(0,2,3),pick(0,1))
            ENDIF
        BREAK
        CASE G_M_Y_LOST_02
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,1)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,1,pick(0,1))
            elif BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,0,pick(0,1))
            ENDIF
        BREAK
        CASE S_M_Y_PRISMUSCL_01
            IF BLOCKED_VARIATION(player_ped_id(),PED_COMP_TORSO,0)
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_DECL,1,0)
            ENDIF
        BREAK  
        CASE A_M_M_GOLFER_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 0 
                if GET_PED_TEXTURE_VARIATION(player_ped_id(),PED_COMP_TORSO) <= 2
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,1,pick(0,1,2))                 
                else
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,1,pick(3,4,5))
                ENDIF
            endif   
        BREAK
        CASE S_M_M_DOCTOR_01
            IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_JBIB) = 1 
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,1,pick(0,1,2)) 
            elif  GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_JBIB) = 0
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(),PED_COMP_JBIB)) 
            ENDIF
        BREAK
        CASE A_F_Y_EPSILON_01
            if GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 1 
                if GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 0
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,1) 
                ELSE
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,0) 
                ENDIF
            ENDIF   
        BREAK
        CASE A_M_Y_Salton_01 
            if GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 1
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_LEG,0,pick(0,1,2))
            ENDIF
        BREAK
        CASE A_M_Y_METHHEAD_01
            if GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 0
                SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TORSO,1,pick(0,1))
            ENDIF
        BREAK
    ENDSWITCH
ENDPROC

PROC UPDATE_SHORTLIST_VARIATIONS()
    
ENDPROC

PROC SET_RANDOM_PLAYER_VARIATION()
    cprintln(debug_director,"SET_RANDOM_PLAYER_VARIATION()")
    if firstTimeShowSaveVariation
        iLastPickedNumber = -1
        iStoredVariationA = 0
        iStoredVariationB = 0
    endif

    IF selectedHeistMember = CM_EMPTY
        IF dirCurrentMenu != MC_SHORTLIST
        AND dirCurrentMenu != MC_RECENTLYUSED
            int icomponent                  
            
            SWITCH loadedModel
                CASE PLAYER_ZERO 
            //      if firstTimeShowSaveVariation
            //          RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
            //          RESTORE_PLAYER_PED_TATTOOS(player_ped_id())
            //          cprintln(debug_director,"PLAYER MAKE B")
            //      else
                        icomponent = GET_RANDOM_INT_NOT_SAME_AS_LAST(enum_to_int(OUTFIT_P0_DEFAULT),enum_to_int(OUTFIT_P0_DLC))                             
                        set_ped_comp_item_current_sp(player_ped_id(),COMP_TYPE_OUTFIT,int_to_enum(PED_COMP_NAME_ENUM,icomponent),FALSE)
                        cprintln(debug_director,"PLAYER MAKE C")
            //      endif
                
                BREAK
                CASE PLAYER_ONE 
            //      if firstTimeShowSaveVariation
            //          RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
            //          RESTORE_PLAYER_PED_TATTOOS(player_ped_id())
            //          cprintln(debug_director,"PLAYER MAKE B")
            //      else
                        icomponent = GET_RANDOM_INT_NOT_SAME_AS_LAST(enum_to_int(OUTFIT_P1_DEFAULT),enum_to_int(OUTFIT_P1_DLC))                                                                 
                        set_ped_comp_item_current_sp(player_ped_id(),COMP_TYPE_OUTFIT,int_to_enum(PED_COMP_NAME_ENUM,icomponent),FALSE)
                        cprintln(debug_director,"PLAYER MAKE C")
            //      endif               
                BREAK
                CASE PLAYER_TWO 
                //  if firstTimeShowSaveVariation
                //      RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
                //      RESTORE_PLAYER_PED_TATTOOS(player_ped_id())
                //      cprintln(debug_director,"PLAYER MAKE B")
                //  else
                        icomponent = GET_RANDOM_INT_NOT_SAME_AS_LAST(enum_to_int(OUTFIT_P2_DEFAULT),enum_to_int(OUTFIT_P2_DLC))                             
                        IF icomponent = enum_to_int(OUTFIT_P2_TOILET)
                            icomponent = enum_to_int(OUTFIT_P2_TOILET) + 1
                        ENDIF
                        set_ped_comp_item_current_sp(player_ped_id(),COMP_TYPE_OUTFIT,int_to_enum(PED_COMP_NAME_ENUM,icomponent),FALSE)
                        cprintln(debug_director,"PLAYER MAKE C")
                //  endif

                    
            
                BREAK
                //Story Mode
                CASE IG_AMANDATOWNLEY
                CASE IG_BEVERLY
                CASE IG_BRAD    
                CASE IG_CHRISFORMAGE                
                CASE IG_DAVENORTON
                CASE IG_DEVIN
                CASE IG_DENISE  
                CASE IG_DRFRIEDLANDER
                CASE IG_FABIEN
                CASE IG_FLOYD
                CASE IG_JIMMYDISANTO
                CASE IG_LAMARDAVIS
                CASE IG_LAZLOW
                CASE IG_LIFEINVAD_01 //RICKIE?
                CASE IG_LESTERCREST
                CASE IG_MAUDE
                CASE IG_MRS_THORNHILL
                case IG_NERVOUSRON
                CASE IG_PATRICIA    
                case IG_SIEMONYETARIAN
                case IG_SOLOMON
                CASE IG_STEVEHAINS
                CASE IG_STRETCH
                CASE IG_TAOCHENG
                CASE IG_TANISHA
                CASE IG_TRACYDISANTO
                CASE IG_WADE                
                CASE U_M_Y_ZOMBIE_01 
                CASE U_F_O_MOVIESTAR 
                CASE S_M_Y_MIME 
                CASE U_M_Y_MANI 
                CASE u_m_m_jesus_01 
                CASE U_M_O_TAPHILLBILLY 
                CASE S_M_M_StrPreach_01 
                CASE U_F_Y_COMJane 
                CASE U_M_Y_IMPORAGE 
                CASE U_M_M_GRIFF_01 
                CASE U_M_Y_MILITARYBUM 
                CASE U_M_O_FinGuru_01 
                CASE U_M_Y_BAYGOR 
                CASE U_M_Y_Hippie_01
                    int iOutfitsSize
                    int iRand
                    
                    DIRECTOR_CHAR_OUTFITS eCharOutfitDefault 
                    DIRECTOR_CHAR_OUTFITS eChosesnOutfit
                    
                    iOutfitsSize        = GET_CHAR_OUTFIT_MAX(loadedModel)
                    iRand               = GET_RANDOM_INT_NOT_SAME_AS_LAST(0,(iOutfitsSize+1))
                    eCharOutfitDefault  = GET_CHAR_OUTFIT_DEFAULT(loadedModel)  
                    eChosesnOutfit      = int_to_enum(DIRECTOR_CHAR_OUTFITS,(iRand + enum_to_int(eCharOutfitDefault)))
                    
                    #IF IS_DEBUG_BUILD
                        IF bDebugShowOutfit
                            iOutfitRandInt = iRand
                            iOutfitID = ENUM_TO_INT(eChosesnOutfit)
                            mOutfitModel = loadedModel
                        ENDIF
                    #ENDIF
                    SET_CHAR_OUTFIT(player_ped_id(),eChosesnOutfit,sOutfitsData)
                BREAK
                CASE MP_M_FREEMODE_01
                CASE MP_F_FREEMODE_01
                    //do nothing            
                BREAK
                                
                CASE U_F_M_CORPSE_01
                    uppr = GET_RANDOM_INT_NOT_SAME_AS_LAST(0,2)
                    switch uppr
                        CASE 0 
                            SET_MODEL_VARIATION(player_ped_id(),0,0,0,0,0)
                            SET_TEXTURE_VARIATION(player_ped_id(),0)
                        BREAK
                        CASE 1
                            SET_MODEL_VARIATION(player_ped_id(),1,0,0,0,0)
                            SET_TEXTURE_VARIATION(player_ped_id(),0,0,0,1,1)
                        BREAK
                    ENDSWITCH                                           
                BREAK
                
                CASE S_M_M_MOVALIEN_01 SET_MODEL_VARIATION(player_ped_id(),0) BREAK
                CASE U_M_Y_RSRANGER_01 SET_MODEL_VARIATION(player_ped_id(),0) BREAK
                CASE S_M_Y_FIREMAN_01
                    uppr = GET_RANDOM_INT_IN_RANGE(0,2)
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,uppr,0)
                    
                    uppr = GET_RANDOM_INT_NOT_SAME_AS_LAST(0,2)
                    switch uppr
                        CASE 0 SET_TEXTURE_VARIATION(player_ped_id(),0,0,0,0,0) BREAK
                        CASE 1 SET_TEXTURE_VARIATION(player_ped_id(),0,0,0,1,1) BREAK   
                    endswitch
                    
                    
                BREAK
                
                
                CASE IG_ORLEANS                             
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_TEETH,1,0)
                    SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_HEAD,1,1)
                BREAK
                
                CASE U_M_M_SPYACTOR
                    SET_MODEL_VARIATION(player_ped_id(),0)
                BREAK
                
                DEFAULT
                    IF selectedHeistMember = CM_EMPTY
                        SET_PED_RANDOM_COMPONENT_VARIATION(player_ped_id()) 
                        //above command normally works but some variations have been highlighted unusable. Exceptions to cater for this below with forced alternative.                                  
                        
                        FIX_BAD_PED_COMPONENT_VARIAITONS(loadedModel)                                               
                    ENDIF
                BREAK
            ENDSWITCH   
            UPDATE_PLAYER_VOICE()
            UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
            REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
            REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
        ELSE
            IF dirCurrentMenu = MC_SHORTLIST
                cprintln(debug_director,"SET_RANDOM_PLAYER_VARIATION() a SET_PED_VARIATION_FROM_SHORTLIST()")
                SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem)   
            ELSE
                cprintln(debug_director,"SET_RANDOM_PLAYER_VARIATION() b SET_PED_VARIATION_FROM_SHORTLIST()")
                SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem+cTOTAL_SHORTLIST_ENTRIES)
            ENDIF           
            UPDATE_PLAYER_VOICE()
            UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
            REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
            REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
        ENDIF
    ELSE
        
        IF dirCurrentMenu != MC_SHORTLIST
        AND dirCurrentMenu != MC_RECENTLYUSED
            SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_BERD, 1, 0, 0)
        ELSE
            IF dirCurrentMenu = MC_SHORTLIST
                cprintln(debug_director,"SET_RANDOM_PLAYER_VARIATION() c SET_PED_VARIATION_FROM_SHORTLIST()")
                SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem)   
            ELSE
                cprintln(debug_director,"SET_RANDOM_PLAYER_VARIATION() d SET_PED_VARIATION_FROM_SHORTLIST()")
                SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem+cTOTAL_SHORTLIST_ENTRIES)
            ENDIF
            UPDATE_PLAYER_VOICE()
            UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
            REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
            REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
        ENDIF
    ENDIF
    
    SET_PLAYER_SCUBA_STATUS()
    firstTimeShowSaveVariation = FALSE
ENDPROC

INT oldScrX,oldScrY

enum enum_MenuActions
    MENU_ACTION_NULL,
    MENU_ACTION_ENTER_GAME,
    MENU_ACTION_OPEN_NEW_MENU,
    MENU_ACTION_EXIT_TO_MAIN_MENU,
    MENU_ACTION_EXIT_TO_STORY,
    MENU_ACTION_EXIT_TO_GTAONLINE
endenum

FUNC enum_MenuActions GET_MENU_ACTION_ON_BUTTON_PRESS(enumMenuCategory selectedMenuItem)
                
                
    IF selectedMenuItem > MC_START_ACTORS
        RETURN MENU_ACTION_ENTER_GAME
    ELIF (selectedMenuItem > MC_START_ACTOR_CATEGORIES AND selectedMenuItem < MC_END_ACTOR_CATEGORIES)
        RETURN MENU_ACTION_OPEN_NEW_MENU
    ELSE
        SWITCH selectedMenuItem
			CASE MC_QUIT_TO_MAIN_MENU
				RETURN MENU_ACTION_EXIT_TO_MAIN_MENU
			BREAK
            CASE MC_QUIT_TO_STORY
                IF GET_DIRECTOR_FLOW_STATE() != dfs_request_terminate
                AND GET_DIRECTOR_FLOW_STATE() != dfs_terminated
                    RETURN MENU_ACTION_EXIT_TO_STORY
                ENDIF
            BREAK
            CASE MC_QUIT_TO_GTA_ONLINE
                RETURN MENU_ACTION_EXIT_TO_GTAONLINE
            BREAK
            CASE MC_PLAYDM
                RETURN MENU_ACTION_ENTER_GAME
            BREAK
            CASE MC_SHORTLIST
                IF get_total_active_shortlists(false) > 0
                    RETURN MENU_ACTION_OPEN_NEW_MENU
                ENDIF               
            BREAK
            CASE MC_RECENTLYUSED
                IF get_total_active_shortlists(true) > 0
                    RETURN MENU_ACTION_OPEN_NEW_MENU                    
                ENDIF
            BREAK

            CASE MC_ACTORS
            CASE MC_SETTINGS
                RETURN MENU_ACTION_OPEN_NEW_MENU
            BREAK

            DEFAULT
                RETURN MENU_ACTION_NULL
            BREAK
    ENDSWITCH
    ENDIF
    RETURN MENU_ACTION_NULL
ENDFUNC

PROC CONTROL_MAINMENU_MOUSE_INPUT()
    INT iCsr = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(0.045)

    IF (selectedMenuType != MENUTYPE_MAINMENU) 
        EXIT
    ENDIF
    
    iCsr = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(0.045)
    
    /*
    IF (iCsr = -999) AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM)
        SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
    ELSE
        SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
    ENDIF
    */
    
    IF NOT IS_CURSOR_ACCEPT_JUST_PRESSED()
        EXIT 
    ENDIF

    IF (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM)
    
        #IF IS_DEBUG_BUILD
            navMouseSelectItem = g_iMenuCursorItem
        #ENDIF

        IF (g_iMenuCursorItem = curMenuItem)
            IF (iCsr = -1)
                navLeft = TRUE 
            ENDIF
            
            IF (iCsr = 1)
                navRight = TRUE 
            ENDIF

            // don't allow accept if we have a sex option
            IF DOES_CATEGORY_HAVE_SEX_OPTION() AND curMenuItem = 0
                EXIT 
            ENDIF
            
            // allow accept if we are in the main category or the actors main category
            IF (dirCurrentMenu = MC_MAIN) OR (dirCurrentMenu = MC_ACTORS) OR (dirCurrentMenu = MC_RECENTLYUSED)
                navAccept = TRUE
                EXIT 
            ENDIF   
        
            // don't allow accept if we are in the setting menu
            IF (dirCurrentMenu = MC_SETTINGS)
                navAccept = FALSE
                EXIT 
            ENDIF   
            

            // allow accept if we are in the start actors category
            IF curMenuItem < COUNT_OF(displayedMenuData)                    
                IF displayedMenuData[curMenuItem].category > MC_START_ACTORS
                    navAccept = TRUE
                    EXIT 
                ENDIF
            ENDIF
            
            // allow accept if we are clicking on the left
            IF (iCsr = -999)
                navAccept = TRUE
            ENDIF
            
            iNavVertDirection = 0       
            EXIT
        ENDIF

        iNavVertDirection = (g_iMenuCursorItem - curMenuItem)
        EXIT 
    ENDIF

    IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN)
        iNavVertDirection = 1
        EXIT 
    ENDIF
    
    IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP)
        iNavVertDirection = -1
        EXIT
    ENDIF
ENDPROC

PROC CONTROL_PI_MENU_MOUSE_INPUT()
    INT iCsr 
    
    //Exit when using camera override
    IF (selectedMenuType != MENUTYPE_PI) 
    OR bPiMouseCameraOverride
        EXIT
    ENDIF
    
    FLOAT fLeftSideAccept = 0.045
    //Prop editor custom PC cursor controls
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    IF bInPropEdit
        fLeftSideAccept = 1.0
        IF ePEState = PES_SceneEdit AND g_iMenuCursorItem = ciPISceneCopy
            fLeftSideAccept = 0.2
        ELIF ePEState = PES_PropPlace
            fLeftSideAccept = 0.04
        ENDIF
    ENDIF
    #ENDIF
    
    iCsr = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(fLeftSideAccept)
    /*
    IF (iCsr = -999) AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM)
        SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
    ELSE
        SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
    ENDIF
    */
    
    IF NOT IS_CURSOR_ACCEPT_JUST_PRESSED()
        EXIT 
    ENDIF
    
    IF (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM)
    
        #IF IS_DEBUG_BUILD
            navMouseSelectItem = g_iMenuCursorItem
        #ENDIF

        IF (g_iMenuCursorItem = sPI_MenuData.iCurrentSelection)
            IF (iCsr = -1)
                navLeft = TRUE 
            ENDIF
            
            IF (iCsr = 1)
                navRight = TRUE 
            ENDIF

            IF (iCsr = -999)
                navAccept = TRUE
            ENDIF

            IF ((sPI_MenuData.iCurrentSelection < ENUM_TO_INT(PI_M0_LOCATION)) OR (sPI_MenuData.iCurrentSelection >= ENUM_TO_INT(PI_M0_VEHICLE)))
            AND (iSubMenu = PI_SUBMENU_NONE OR iSubMenu = PI_SUBMENU_VEHICLES)
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            AND NOT bInPropEdit
            #ENDIF
                navAccept = TRUE
            ENDIF   
        ENDIF

        iNavVertDirection = (g_iMenuCursorItem - sPI_MenuData.iCurrentSelection)
        EXIT 
    ENDIF

    IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN)
        iNavVertDirection = 1
        EXIT 
    ENDIF
    
    IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP)
        iNavVertDirection = -1
        EXIT
    ENDIF
ENDPROC

PROC CONTROL_MENU()
    
    IF selectedMenuType = MENUTYPE_PI
        selectedMenuType = MENUTYPE_MAINMENU
    ENDIF

    IF bShowMenu
    OR dirMenuState = dirMENU_PERFORMING_PAN
    //OR dirMenuState = dirMENU_MOVE_TO_PLAY_MODE
        SWITCH dirMenuState
            CASE dirMENU_LOAD
        //      REQUEST_STREAMED_TEXTURE_DICT("director_editor_title")
                bMenuAssetsLoaded = LOAD_MENU_ASSETS("DIR_MNU")
                IF bMenuAssetsLoaded
        //      AND HAS_STREAMED_TEXTURE_DICT_LOADED("director_editor_title")
                
                    IF dirCurrentMenu = MC_MAIN
                        REBUILD_MENU(MC_MAIN)
                    ELSE
                        REBUILD_MENU(MC_CURRENT,true)
                    ENDIF
                    
                    SET_CURSOR_POSITION_FOR_MENU()                  
                    

                    dirMenuState = dirMENU_UPDATE
                ENDIF
            BREAK   
            CASE dirMENU_UPDATE
            
                //check for resolution change
                INT scrX,scrY
                GET_ACTUAL_SCREEN_RESOLUTION(scrX,scrY)
                IF scrX != oldScrX
                OR scrY != oldScrY
                    
                    oldScrX = scrX
                    oldScrY = scrY
                    REBUILD_MENU(MC_CURRENT,TRUE)
                ENDIF
                
                IF GET_DIRECTOR_MODE_STATE() != dfs_query_terminate
                AND GET_DIRECTOR_MODE_STATE() != dfs_shortlist_full_query
                AND iNavVertDirection != 0
                    IF iTotalMenuOptions > 1
                        PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        //CPRINTLN(debug_director, "Checking this sound 1")

                        int iLastItem
                        iLastItem = curMenuItem
                        
                        curMenuItem += iNavVertDirection
                        IF curMenuItem >= iTotalMenuOptions
                            curMenuItem = 0
                        ENDIF
                        
                        IF curMenuItem < 0
                            curMenuItem += iTotalMenuOptions
                        ENDIF

                        iCurrentSelectedModel = 0
                        
                        int iTop
                        iTop = GET_TOP_MENU_ITEM()
                        
                        SET_CURRENT_MENU_ITEM(curMenuItem)
                        
                        REBUILD_MENU(MC_CURRENT,TRUE)   
                        
                        //Apply menu top mod for special animal
                        IF dirCurrentMenu = MC_ANIMALS AND curMenuItem = ENUM_TO_INT(MC_ANISAS - MC_ANIMALS_START) - 2
                        AND iNavVertDirection <= -1
                            SET_TOP_MENU_ITEM(curMenuItem - 8)
                            iTop = GET_TOP_MENU_ITEM()
                        ENDIF
                        
                        FIX_SCROLLING_TOP_ITEM(curMenuItem, iTop)
                        firstTimeShowSaveVariation = TRUE
                        
                        CHECK_SWITCH_CAM_LOCATION(iLastItem)
                        
                        
                        
                        IF pedJustShortlisted
                            pedJustShortlisted = FALSE                              
                        ENDIF
                    ENDIF

                    REBUILD_HELP_KEYS()
            
                ENDIF
                
                
                IF navLeft
                OR navRight
                    CPRINTLN(debug_director,"menu control navleft = true. current menu = ",displayedMenuData[curMenuItem].category)
                    IF dirCurrentMenu != MC_SETTINGS
                        IF DOES_CATEGORY_HAVE_SEX_OPTION()
                            IF iTotalMenuOptions > 0
                                IF curMenuItem = 0
                                    PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                    IF bMenu_sex_male
                                        bMenu_sex_male = FALSE                                  
                                        SET_LOAD_NEW_MODEL_VARIATION()
                                        REBUILD_MENU(MC_CURRENT,TRUE)
                                    ELSE                        
                                        bMenu_sex_male = TRUE
                                        SET_LOAD_NEW_MODEL_VARIATION()
                                        REBUILD_MENU(MC_CURRENT,TRUE)
                                    ENDIF
                                ENDIF
                            ENDIF
                        ENDIF                   
                        
                        IF GET_TOTAL_CATEGORY_MODEL_VARIATIONS(displayedMenuData[curMenuItem].category,dirCurrentMenu) > 1
                            if navLeft iCurrentSelectedModel-- endif
                            if navRight iCurrentSelectedModel++ endif
                            
                            IF iCurrentSelectedModel < 0 iCurrentSelectedModel = GET_TOTAL_CATEGORY_MODEL_VARIATIONS(displayedMenuData[curMenuItem].category,dirCurrentMenu)-1 ENDIF
                            IF iCurrentSelectedModel >= GET_TOTAL_CATEGORY_MODEL_VARIATIONS(displayedMenuData[curMenuItem].category,dirCurrentMenu) iCurrentSelectedModel = 0 ENDIF
                            SET_LOAD_NEW_MODEL_VARIATION()
                            REBUILD_MENU(MC_CURRENT,TRUE)
                        ENDIF   
                    ELSE //MENUTYPE_SETTINGS
                        CPRINTLN(debug_director,"Setting menu before ",curMenuItem," = ",settingsMenu[curMenuItem])
                        BOOL bAllowSideMovement
                        bAllowSideMovement = TRUE
                
                        //Disable side movement for disabled cheats
                        IF curMenuItem >= SEM_INVINCIBLE
                        AND curMenuItem <= SEM_LOWGRAVITY
                            bAllowSideMovement = GET_SETTING_CHEAT_ENABLED(curMenuItem)
                        ENDIF
                        
                        IF  bAllowSideMovement
                        
                            if navLeft settingsMenu[curMenuItem]-- endif
                            if navRight settingsMenu[curMenuItem]++ endif                   
                            CPRINTLN(debug_director,"Setting menu after ",curMenuItem," = ",settingsMenu[curMenuItem])
                            INT iTop 
                            iTop = GET_TOP_MENU_ITEM()
                            PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            REBUILD_MENU(MC_CURRENT,TRUE)
                            FIX_SCROLLING_TOP_ITEM(curMenuItem, iTop)   //Fix the scroll list moving
                            
                            //Change time and weather in real-time
                            IF curMenuItem = SEM_TIME
                                CHANGE_TIME(settingsMenu[curMenuItem])
                            ELIF curMenuItem = SEM_WEATHER
                                CHANGE_WEATHER(settingsMenu[curMenuItem])
                            ENDIF
                        
                        ENDIF
                        
                    ENDIF
                ENDIF
                
                //removed gesture and dialogu
                /*
                IF GET_CURRENT_MENU_TYPE() = MENUTYPE_SETTINGS
                    IF navGesture                   
                        if curMenuItem = SEM_ACTION
                            PLAY_ANIMATION()
                        ENDIF
                        //IF curMenuItem = SEM_DIALOGUE
                        //  navSpeech = TRUE    
                        //ENDIF
                    ELSE                
                        IF bGesturePlaying
                            int animIndex
                            animIndex = enum_to_int(PLAYER_INTERACTION_NONE)+selectedPI[PI_M0_GESTURE]
                            START_INTERACTION_ANIM(2,animIndex,FALSE)
                            bGesturePlaying = FALSE
                        ENDIF
                    ENDIF
                ENDIF*/
                
                IF navAccept
                    CPRINTLN(DEBUG_DIRECTOR,"CONTROL_MENU: navAccept true (1) GET_DIRECTOR_MODE_STATE() = ",GET_DIRECTOR_MODE_STATE())

                    IF  GET_DIRECTOR_MODE_STATE() != dfs_query_terminate
                    and GET_DIRECTOR_MODE_STATE() != dfs_query_editor
                    and GET_DIRECTOR_MODE_STATE() != dfs_query_trailer  
                    and GET_DIRECTOR_MODE_STATE() != dfs_shortlist_full_query
                    and GET_DIRECTOR_MODE_STATE() != dfs_PREP_TRAILER_SCENE
                        CPRINTLN(DEBUG_DIRECTOR,"CONTROL_MENU: navaccept ok")
                        if dirCurrentMenu = MC_SETTINGS
                            //Settings that use the Accept key
                            navAccept = FALSE
                        else
                        if curMenuItem < COUNT_OF(displayedMenuData)    
                            //check if player is entering actor mode with A button press
                            SWITCH GET_MENU_ACTION_ON_BUTTON_PRESS(displayedMenuData[curMenuItem].category)
                                CASE MENU_ACTION_ENTER_GAME
                                    IF GET_MOST_RECENTLY_SELECTED_VALID_MODEL() = GET_ENTITY_MODEL(player_ped_id()) //ensure player only enters play mode once player has swapped to the correct model.
                                        IF IS_MENU_CATEGORY_SELECTABLE(displayedMenuData[curMenuItem].category)
                                            IF dirCurrentMenu = MC_SHORTLIST                                            
                                                selectedPI[PI_M0_SHORTLIST] = curMenuItem
                                                lastSelectedShortlistPed = curMenuItem
                                            ELSE
                                                selectedPI[PI_M0_SHORTLIST] = 0
                                            ENDIF
                                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                            //player has chosen a character to play as...
                                            CLEAR_DM_MENU_DATA()
                                            SET_ENTITY_HEALTH(player_ped_id(),GET_ENTITY_MAX_HEALTH(player_ped_id()))   //Enforce 100% health, even if same model
                                            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                                                CHANGE_INVINCIBILITY()//SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)
                                                SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
                                            ENDIF
                                            dirMenuState = dirMENU_MOVE_TO_PLAY_MODE            
                                        ENDIF
                                    ENDIF
                                BREAK
                                CASE MENU_ACTION_EXIT_TO_MAIN_MENU
                                    CPRINTLN(DEBUG_DIRECTOR,"CONTROL_MENU: MENU_ACTION_EXIT_TO_MAIN_MENU")
                                    SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
                                    navAccept = false
                                    #IF FEATURE_GEN9_STANDALONE
                                    eExitMode = SRCM_LANDING_PAGE
                                    #ENDIF
                                BREAK
                                CASE MENU_ACTION_EXIT_TO_STORY
                                    CPRINTLN(DEBUG_DIRECTOR,"CONTROL_MENU: MENU_ACTION_EXIT_TO_MAIN_MENU")
                                    SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
                                    navAccept = false
                                    #IF FEATURE_GEN9_STANDALONE
                                    eExitMode = SRCM_STORY
                                    #ENDIF
                                BREAK
                                CASE MENU_ACTION_EXIT_TO_GTAONLINE
                                    CPRINTLN(DEBUG_DIRECTOR,"CONTROL_MENU: MENU_ACTION_EXIT_TO_GTAONLINE")
                                    SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
                                    navAccept = false
                                BREAK
                                CASE MENU_ACTION_OPEN_NEW_MENU
                                    //is it an actor menu
                                    //is it shortlist menu
                                    //does it require animal camera pan swap
                                    
                                    //ENDIF
                                            
                                            
                                            
                                                
                                    //set sex to male if no sex category option //this is bad. Causes confusion over current ped model sex. Also check if changing shortlists in game is causing bMenu_sex_male to be updated.
                                    //could really use a GET_MODEL_SEX() command.
                                    //IF NOT DOES_CATEGORY_HAVE_SEX_OPTION(displayedMenuData[curMenuItem].category)
                                    bMenu_sex_male = IS_DIRECTOR_PED_MALE()
                                    //          ENDIF
                                            
                                    CPRINTLN(debug_director,"navAccept curMenuItem = ",curMenuItem)
                                            //set first selectable ped to one that's available in the menu
                                    IF displayedMenuData[curMenuItem].category > MC_START_ACTOR_CATEGORIES
                                            int iMenuEntry
                                            IF GET_FIRST_UNLOCKED_MENU_ITEM(iMenuEntry,curMenuItem)                     
                                                SET_CURRENT_MENU_ITEM(iMenuEntry)
                                                curMenuItem = iMenuEntry
                                            CPRINTLN(debug_director,"navAccept B curMenuItem = ",curMenuItem)
                                            ENDIF
                                    ENDIF
                                            
                                    PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                    REBUILD_MENU(displayedMenuData[curMenuItem].category)
                                    CHECK_SWITCH_CAM_LOCATION()
                                            
                                BREAK
                            ENDSWITCH
                        ENDIF
                    ENDIF
                ENDIF
                ENDIF
                
                IF navBack //commented out option to exit director mode with back button. Bug 2232066
                    IF GET_DIRECTOR_MODE_STATE() != dfs_query_terminate
                    and GET_DIRECTOR_MODE_STATE() != dfs_query_editor
                    and GET_DIRECTOR_MODE_STATE() != dfs_query_trailer  
                    and GET_DIRECTOR_MODE_STATE() != dfs_shortlist_full_query
                        IF dirCurrentMenu != MC_MAIN
                            PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            REBUILD_MENU(GET_PARENT_MENU(dirCurrentMenu),FALSE)
                            CHECK_SWITCH_CAM_LOCATION()
                            //IF GET_DIRECTOR_MODE_STATE() = dfs_RUN_TRAILER_SCENE
                                //SET_DIRECTOR_MODE_STATE(dfs_query_terminate)
                            //ENDIF
                        //ELSE
                        //  PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        //  REBUILD_MENU(GET_PARENT_MENU(dirCurrentMenu),FALSE)
                        ENDIF
                    ENDIF
                ENDIF
                
                IF navRandom //changing this to randomise a model variation rather than grab a new model
                    IF DOES_CURRENT_MENU_ITEM_HAVE_RANDOM_OPTION()
                    and IS_MENU_CATEGORY_SELECTABLE(displayedMenuData[curMenuItem].category)
                        PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    //IF dirCurrentMenu != MC_MAIN
                    //  currentPlayerVariation = MC_VOID
                    //  loadingVariation = MC_VOID
                        //SET_LOAD_NEW_MODEL_VARIATION(TRUE)
                        
                        IF dirCurrentMenu != MC_HEIST
                            SET_RANDOM_PLAYER_VARIATION()
                        ELSE
                            SET_LOAD_NEW_MODEL_VARIATION(true)
                        ENDIF
                        
                        CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
                        
                        REBUILD_HELP_KEYS()
                    ENDIF
                ENDIF
                
                IF navY
                    IF dirCurrentMenu > MC_START_ACTOR_CATEGORIES
                    OR dirCurrentMenu = MC_RECENTLYUSED
                        IF GET_EXISTING_PED_SHORTLIST() = -1
                        AND displayedMenuData[curMenuItem].category > MC_START_ACTOR_CATEGORIES
                            
                            IF NOT bLoadingNewModel
                                IF shortlistData[9].category = MC_VOID                  
                                    //ensure model is not still loading
                                    pedJustShortlisted = TRUE
                                    
                                    //Sanity check the current active menu entry
                                    IF activeMenuEntry = MC_VOID
                                        activemenuEntry = displayedMenuData[curMenuItem].category
                                        activeMenuIndex = displayedMenuData[curMenuItem].index
                                    ENDIF
                                    ShortlistPed(activeMenuEntry,activeMenuIndex) //displayedMenuData[curMenuItem])
                                    REBUILD_HELP_KEYS()
                                ELSE
                                    SET_DIRECTOR_MODE_STATE(dfs_shortlist_full_query)
                                ENDIF
                            ENDIF
                        ENDIF
                    ELIF dirCurrentMenu = MC_SHORTLIST
                        cprintln(debug_director,"NAVY shortlist. Total shortlist peds = ",get_total_active_shortlists(false))
                        
                        IF get_total_active_shortlists(false) > 0
                            PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)                            
                            REMOVE_SHORTLIST_ENTRY(curMenuItem)
                            int iNewMenuEntry
                            iNewMenuEntry = curMenuItem-1
                            IF iNewMenuEntry < 0 
                                iNewMenuEntry = 0 
                                //lastMenuItem = 1 //will forece model update
                            ENDIF
                            
                            IF get_total_active_shortlists(false) = 0
                                REBUILD_MENU(MC_MAIN)                               
                            ELSE
                                REBUILD_MENU(MC_SHORTLIST,true,iNewMenuEntry)
                            ENDIF
                            CHECK_SWITCH_CAM_LOCATION()
                        ENDIF/*
                    ELIF dirCurrentMenu = MC_RECENTLYUSED
                        IF get_total_active_shortlists(true) > 0
                            PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            REMOVE_SHORTLIST_ENTRY(curMenuItem+10)
                            int iNewMenuEntry
                            iNewMenuEntry = curMenuItem-1
                            IF iNewMenuEntry < 0 
                                iNewMenuEntry = 0 
                                //lastMenuItem = 1 //will forece model update
                            ENDIF
                            
                            IF get_total_active_shortlists(true) = 0
                                REBUILD_MENU(MC_MAIN)                               
                            ELSE
                                REBUILD_MENU(MC_RECENTLYUSED,true,iNewMenuEntry)
                            ENDIF
                            CHECK_SWITCH_CAM_LOCATION()
                        ENDIF*/
                    ENDIF
                ENDIF
                
                
                //check if menu description needed
                IF dirCurrentMenu != MC_SETTINGS
                    TEXT_LABEL_31 tMenuDesc
                    IF dirCurrentMenu = MC_MAIN
                       	tMenuDesc = GET_CATEGORY_HELP(displayedMenuData[curMenuItem].category)
                       	SET_CURRENT_MENU_ITEM_DESCRIPTION(tMenuDesc)
                    ELSE
                        SWITCH dirCurrentMenu
                            CASE MC_ACTORS
                                tMenuDesc = GET_CATEGORY_HELP(displayedMenuData[curMenuItem].category)
                                SET_CURRENT_MENU_ITEM_DESCRIPTION(tMenuDesc)
                            BREAK
                            CASE MC_HEIST
                                SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_HEIHLP2")
                            BREAK
                            CASE MC_ONLINE
                                IF NOT (NETWORK_IS_CABLE_CONNECTED() OR NETWORK_IS_CLOUD_AVAILABLE())
                                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_NETCONHLP")
                                ELIF NOT NETWORK_IS_SIGNED_ONLINE()
                                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_ONLINEHLP")
                                ELIF NOT (sOnlineChar[0].bSetup OR sOnlineChar[1].bSetup)   //Also display if no online characters created
                                    SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_NOCHARHLP")
                                ENDIF
                            BREAK
                            DEFAULT
                                IF NOT IS_MENU_CATEGORY_AN_ANIMAL(displayedMenuData[curMenuItem].category)
                                    IF !IS_PED_CATEGORY_VALID_FOR_VEHICLE(displayedMenuData[curMenuItem].category)
                                    AND !IS_PED_CATEGORY_VALID_FOR_WEAPONS(displayedMenuData[curMenuItem].category)
                                        SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_WHLP01WV")
                                    ELSE
                                        IF !IS_PED_CATEGORY_VALID_FOR_VEHICLE(displayedMenuData[curMenuItem].category)
                                            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_WHLP01")                                  
                                        ENDIF
                                        
                                        IF !IS_PED_CATEGORY_VALID_FOR_WEAPONS(displayedMenuData[curMenuItem].category)
                                            SET_CURRENT_MENU_ITEM_DESCRIPTION("CM_WHLP01WPN")                                   
                                        ENDIF
                                    ENDIF
                                ENDIF                                                                   
                            BREAK
                        ENDSWITCH
                    ENDIF
                ELSE
                    SET_CURRENT_SETTINGS_ITEM_DESCRIPTION(curMenuItem)
                    
                ENDIF
                
                IF dirMenuState != dirMENU_PERFORMING_PAN   
                AND dirMenuState != dirMENU_MOVE_TO_PLAY_MODE
                
                    /*
                    HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
                    HANDLE_MENU_CURSOR(FALSE)
                    */
                    SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE) 
                    SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
                
                    DRAW_MENU_HEADER()
                    DRAW_MENU(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)     
                    bWasMenuDrawn = TRUE
                ENDIF
                
            BREAK
            CASE dirMENU_PERFORMING_PAN
                IF dirCamState = DIR_CAM_TRAILER_AT_ANIMALS
                OR dirCamState = DIR_CAM_TRAILER_AT_TRAILER
                OR dirCamState = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                    dirMenuState = dirMENU_UPDATE
                    IF bBlockOriginalPedDelete
                        IF DOES_ENTITY_EXIST(originalPed)
                            IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(originalPed,FALSE)
                                SET_ENTITY_AS_MISSION_ENTITY(originalPed,TRUE,TRUE)
                            ENDIF
                            DELETE_PED(originalPed)
                        ELSE //no model was created when performing pan so position player somewhere out of shot to prevent seeing duplicates next time pan is performed.
                            IF NOT IS_PED_INJURED(player_ped_id())
                                //SET_ENTITY_COORDS(player_ped_id(),<<-25.2822, -18.5930, 499.0417>>)
                            ENDIF
                        ENDIF
                        
                        
                        
                        bBlockOriginalPedDelete = FALSE
                    ENDIF                   
                ENDIF
            BREAK
            CASE dirMENU_MOVE_TO_PLAY_MODE
                CLEANUP_MENU_ASSETS()
                CLEANUP_PI_MENU_MINI_MAP()
                bMenuAssetsLoaded = FALSE
            BREAK
        ENDSWITCH
        DISPLAY_HUD(FALSE)
        DISPLAY_RADAR(FALSE)
        
        IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL) AND dirMenuState != dirMENU_LOAD
            REBUILD_HELP_KEYS()
        ENDIF
    ELSE
        IF dirMenuState <> dirMENU_LOAD
            bMenuAssetsLoaded = FALSE
            CLEANUP_PI_MENU_MINI_MAP()
            CLEANUP_MENU_ASSETS()
        ENDIF
        dirMenuState = dirMENU_LOAD
    ENDIF
ENDPROC

bool doOnce, bWasFaded = FALSE
VECTOR vPrevWaypointBlipCoords, vCurrWaypointBlipCoords

PROC MAINTAIN_ACTIVE_PI_MENU()
    
    //======== WAITING FOR PI MENU ASSETS TO LOAD ========
    IF NOT bMenuAssetsLoaded
        bMenuAssetsLoaded = LOAD_MENU_ASSETS("DIR_MNU")      
    ELSE
    
    IF IS_WAYPOINT_ACTIVE()
        vCurrWaypointBlipCoords = GET_BLIP_COORDS( GET_FIRST_BLIP_INFO_ID( GET_WAYPOINT_BLIP_ENUM_ID() ) )
    ENDIF
    
    IF NOT ARE_VECTORS_EQUAL( vPrevWaypointBlipCoords, vCurrWaypointBlipCoords )
        IF g_bHasPauseMapBeenAccessed
            iWaypointLocIndex = -1
        ENDIF
        RESET_PI_MENU_NOW()
    ENDIF
    
    //Reset menu if just coming back from screen fade
    IF bWasFaded AND IS_SCREEN_FADED_IN()
        RESET_PI_MENU_NOW()
    ENDIF
    bWasFaded = NOT IS_SCREEN_FADED_IN()
    
    // ---- resolution update check ----        
        INT scrX,scrY
        GET_ACTUAL_SCREEN_RESOLUTION(scrX,scrY)
        IF scrX != oldScrX
        OR scrY != oldScrY
            oldScrX = scrX
            oldScrY = scrY
            RESET_PI_MENU_NOW()
        ENDIF
        
    // ---- first menu setup ----
        IF NOT IS_BIT_SET(iBoolsBitSet, biM_MenuSetup)
            bResetMenuNow               = true
            g_bHasPauseMapBeenAccessed  = FALSE
            SET_BIT(iBoolsBitSet, biM_MenuSetup)            
        ENDIF
        
        IF g_bHasPauseMapBeenAccessed
            CPRINTLN( DEBUG_DIRECTOR, " - MAINTAIN_ACTIVE_PI_MENU - Paused game so refreshing menu." )
            
            #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
            IF IS_WAYPOINT_ACTIVE()
                IF NOT ARE_VECTORS_EQUAL( warpLocations[ DTL_WAYPOINT ].position, vCurrWaypointBlipCoords )
                    iWaypointLocIndex = -1
                ENDIF
            ENDIF           
            #ENDIF
            
            RESET_PI_MENU_NOW()
            g_bHasPauseMapBeenAccessed  = FALSE
        ENDIF
        
    // ------- REFRESH MENU -------- 
        IF bResetMenuNow     
            //CPRINTLN( DEBUG_DIRECTOR, " - MAINTAIN_ACTIVE_PI_MENU - bResetMenuNow = TRUE - REBUILD_PI_MENU being called." )  
            SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
            INT iTop = GET_TOP_MENU_ITEM()  
            
            PREP_MENU_DRAWING()
           
            REBUILD_PI_MENU()
            
            FIX_SCROLLING_TOP_ITEM(sPI_MenuData.iCurrentSelection, iTop, c_iMAX_PI_MENU_OPTIONS - 1)
            
            bResetMenuNow = FALSE
            iMenuUpdateTimer = get_Game_timer()  
            iMenuHelpKeysTimer = GET_GAME_TIMER()
        ELSE
            if Get_Game_timer() - iMenuUpdateTimer > MENU_UPDATE_TIME
                RESET_PI_MENU_NOW()
            elif iSubMenu = PI_SUBMENU_NONE AND sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION)
            AND GET_GAME_TIMER() - iMenuHelpKeysTimer > ci_MENU_HELP_KEYS_REFRESH_TIME
                CPRINTLN( DEBUG_DIRECTOR, " - MAINTAIN_ACTIVE_PI_MENU - REBUILD_PI_HELP_KEYS as ", ci_MENU_HELP_KEYS_REFRESH_TIME, "ms has past since last refresh." )  
                RESET_PI_MENU_NOW()
                iMenuHelpKeysTimer = GET_GAME_TIMER()
            endif
        ENDIF
                
        PROCESS_BELOW_MENU_HELP()    
        /*
        HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
        HANDLE_MENU_CURSOR(FALSE)
        */
        
        // B*2236239 - Inconsistency with using Esc while in the garage PI menu while in Director Mode.
        IF (selectedMenuType = MENUTYPE_PI) AND (GET_DIRECTOR_FLOW_STATE() = dfs_PLAY_MODE)
            iPauseDisableTimer = GET_GAME_TIMER() + 125
        ENDIF
        
        DRAW_MENU_HEADER()
        
        IF HAS_SCALEFORM_MOVIE_LOADED(sfMenu)       
            IF NOT IS_STRING_NULL_OR_EMPTY(menuHelpString)
                BEGIN_SCALEFORM_MOVIE_METHOD(sfMenu,"SET_TEXT")                 
                       BEGIN_TEXT_COMMAND_SCALEFORM_STRING(menuHelpString)
                       END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
                END_SCALEFORM_MOVIE_METHOD()  
                
                
                IF GET_ASPECT_RATIO(FALSE) != fLastAspectRatio
                    fLastAspectRatio = GET_ASPECT_RATIO(FALSE)
                    RESET_PI_MENU_NOW()
                    BEGIN_SCALEFORM_MOVIE_METHOD(sfMenu,"SET_TEXT_POINT_SIZE")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14)
                    END_SCALEFORM_MOVIE_METHOD() 
                ENDIF
                
                

                if !doOnce
                    doOnce = TRUE
                    BEGIN_SCALEFORM_MOVIE_METHOD(sfMenu,"SET_BACKGROUND_IMAGE") 
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("CommonMenu")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("Gradient_Bgd")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(73)
                         //BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CommonMenu")
    //                     END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
                            //BEGIN_TEXT_COMMAND_SCALEFORM_STRING("Gradient_Bgd")
                           //END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
                    END_SCALEFORM_MOVIE_METHOD() 
                    
                    BEGIN_SCALEFORM_MOVIE_METHOD(sfMenu,"SET_TEXT_POINT_SIZE")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(14)
                    END_SCALEFORM_MOVIE_METHOD() 
                endif
                
                FLOAT fCentreX_01_aspectOffset
                FLOAT mpng_centreX_01 = messageBox_X    //CUSTOM_MENU_TEXT_INDENT_X         
                
                IF IS_PC_VERSION()
                    fCentreX_01_aspectOffset = ((1.0 / GET_ASPECT_RATIO(FALSE)) * 0.2) - mpng_centreX_01 
                ENDIF
                    
                SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
                SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)

                IF IS_CUSTOM_MENU_ON_SCREEN() 
                AND IS_CUSTOM_MENU_SAFE_TO_DRAW()
                AND NOT IS_SCREEN_FADING_OUT() 
                AND NOT IS_SCREEN_FADED_OUT() 
                
                    FLOAT fBelowHelpY = sf_y
                    FLOAT fBelowLineY = line_y
                
                    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
                    IF iSubMenu = PI_SUBMENU_PROPS
                        INT iMenuOptions = IMIN(DMPROP_GET_NUM_MENU_ENTRIES(), c_iMAX_PI_MENU_OPTIONS+1)
                        //IF iPropCount <= c_iMAX_PI_MENU_OPTIONS // Top 3 elements are menu options- then props diplayed under.
                        FLOAT fYAdjustment = ( CUSTOM_MENU_ITEM_BAR_H * (c_iMAX_PI_MENU_OPTIONS-iMenuOptions+1) )   
                            fBelowHelpY -= fYAdjustment
                            fBelowLineY -= fYAdjustment
                            ENDIF
                        //ENDIF
                    #ENDIF
                    
                    ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9() //b* 2147964
                    DRAW_SCALEFORM_MOVIE(sfMenu,sf_x,fBelowHelpY,sf_w,sf_h, 255, 255, 255, 255)
                    ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9() //b* 2147964
                    DRAW_RECT(line_x,fBelowLineY,line_W,line_h,0,0,0,255)
                    fCentreX_01_aspectOffset=fCentreX_01_aspectOffset
                ENDIF
                
                RESET_SCRIPT_GFX_ALIGN()
                
            ENDIF
        ELSE
            sfMenu = REQUEST_SCALEFORM_MOVIE_WITH_IGNORE_SUPER_WIDESCREEN("TEXTFIELD") 
        ENDIF   
        
        UPDATE_PI_MENU_MINI_MAP_PER_FRAME()
        
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            IF IS_CUSTOM_MENU_ON_SCREEN() 
            AND IS_CUSTOM_MENU_SAFE_TO_DRAW()
            AND NOT IS_SCREEN_FADING_OUT() 
            AND NOT IS_SCREEN_FADED_OUT()       
                UPDATE_PI_MENU_PROP_MOVEMENT()
            ENDIF
        #ENDIF
        
        IF IS_WAYPOINT_ACTIVE()
            vPrevWaypointBlipCoords = GET_BLIP_COORDS( GET_FIRST_BLIP_INFO_ID( GET_WAYPOINT_BLIP_ENUM_ID() ) )
        ENDIF
        
        DRAW_MENU(DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, CUSTOM_MENU_W, DEFAULT, TRUE)
        bWasMenuDrawn = TRUE
    ENDIF
ENDPROC

PROC CONTROL_PI_MENU_MOVEMENT()

	/*
	#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
		IF iSubMenu = PI_SUBMENU_PROPS
			DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "Stunt Props PVW2")
		ENDIF
	#ENDIF
	*/

    //===================================== LEFT OR RIGHT =========================================
    IF navLeft
    OR navRight
        bool bAllowSideMovement = false
        // Allow left or right movement on current options
        switch iSubMenu
            case PI_SUBMENU_NONE
                switch INT_TO_ENUM(DIRECTOR_MODE_PI_M0, sPI_MenuData.iCurrentSelection)
                    CASE PI_M0_LOCATION
                        IF iMaxLocationsUnlocked > 0
                        AND IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                            bAllowSideMovement = true                           
                        endif                       
                    BREAK
                    case PI_M0_SPEECH
                        if NOT IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(player_ped_ID()))                        
                            IF iMaxSpeechEnries > 1
                                bAllowSideMovement = true                           
                            ELSE
                                selectedPI[PI_M0_SPEECH] = 0
                            ENDIF
                        ENDIF
                    break 
                    case PI_M0_GESTURE                  
                        if IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_GESTURE)          
                        and NOT IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(player_ped_ID()))
                            bAllowSideMovement = true       
                        ENDIF
                    break 
                    case PI_M0_SHORTLIST                                        
                        if IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_SHORTLIST_SWAP)
                        and get_total_active_shortlists(false) + get_total_active_shortlists(true)> 1
                    //  and NOT IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(player_ped_ID())) //not sure why this is here. It's preventing selecting through animals in the PI shortlist. Kev.
                            bAllowSideMovement = true                   
                        endif
                    break   

                endswitch
                // Increase / Decrease number of currently selected option
                IF  bAllowSideMovement
                    IF navLeft
                        selectedPI[sPI_MenuData.iCurrentSelection]-- 
                        //Gestures are restricted depending on model decrease differs               
                        if sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_GESTURE)
                            lastDirectionPress=-1                       
                            RESTRICT_ACTION_ENTRY(selectedPI[sPI_MenuData.iCurrentSelection])                           
                        else
                            IF selectedPI[sPI_MenuData.iCurrentSelection] < 0
                               selectedPI[sPI_MenuData.iCurrentSelection] = GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
                            ENDIF
                        endif               
                    ENDIF                           
                    IF navRight 
                        selectedPI[sPI_MenuData.iCurrentSelection]++
                        //Gestures are restricted depending on model increase differs
                        if sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_GESTURE)
                            lastDirectionPress=1
                            RESTRICT_ACTION_ENTRY(selectedPI[sPI_MenuData.iCurrentSelection])
                        else    
                            cprintln(debug_director,"selectedPI[sPI_MenuData.iCurrentSelection] = ",selectedPI[sPI_MenuData.iCurrentSelection]," GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection) = ",GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection))
                            IF selectedPI[sPI_MenuData.iCurrentSelection] > GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
                                cprintln(debug_director,"Wrap selection")
                                selectedPI[sPI_MenuData.iCurrentSelection] = 0
                            ENDIF
                        endif
                    ENDIF
                    cprintln(debug_director,"selectedPI[sPI_MenuData.iCurrentSelection] = ",selectedPI[sPI_MenuData.iCurrentSelection])
                    PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE()
                    RESET_PI_MENU_NOW() 
                endif
            break
            case PI_SUBMENU_SETTINGS
                //All options have side movement by default
                bAllowSideMovement = true
                               
                //Disable side movement for disabled cheats
                IF sPI_MenuData.iCurrentSelection >= SEM_INVINCIBLE
                AND sPI_MenuData.iCurrentSelection <= SEM_LOWGRAVITY
                    bAllowSideMovement = GET_SETTING_CHEAT_ENABLED(sPI_MenuData.iCurrentSelection)
                ENDIF
                
                IF  bAllowSideMovement
                    IF navLeft
                        tempSettingsMenu[sPI_MenuData.iCurrentSelection] --
                    ELIF navRight
                        tempSettingsMenu[sPI_MenuData.iCurrentSelection] ++
                    ENDIF
                    //Wrap between [0,menu_max]
                    IF tempSettingsMenu[sPI_MenuData.iCurrentSelection] > GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
                        tempSettingsMenu[sPI_MenuData.iCurrentSelection] = 0
                    ELIF tempSettingsMenu[sPI_MenuData.iCurrentSelection] < 0
                        tempSettingsMenu[sPI_MenuData.iCurrentSelection] = GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
                    ENDIF
                    
                    CHECK_IF_TEMP_SETTINGS_CHANGED()
                    //B*-2433381.  Any movement to the time setting, counts as a changed setting
                    IF sPI_MenuData.iCurrentSelection = SEM_TIME
                        bTempSettingsHaveChanged = TRUE
                    ENDIF
                    
                    IF bTempSettingsHaveChanged 
                        bHideSettingsChangedDesc = FALSE 
                    ENDIF
                    
                    PLAY_SOUND_FRONTEND(-1,"NAV_LEFT_RIGHT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    RESET_PI_MENU_NOW() 
                    
                    //Check if the time setting was changed through d-pad L/R
                    IF sPI_MenuData.iCurrentSelection = SEM_TIME
                        bTimeSettingFineTuned = FALSE
                    ENDIF
                ENDIF
            break
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            case PI_SUBMENU_PROPS
                IF ePEState = PES_SceneEdit
                AND sPI_MenuData.iCurrentSelection = ciPISceneCopy

                    PLAY_SOUND_FRONTEND (-1, "Cycle_Item", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //New sound for bug 2448537

                    IF navRight     iSelectedScene = (iSelectedScene + 1) % ciMAXSCENES
                    ELIF navLeft    iSelectedScene = (iSelectedScene - 1 + ciMAXSCENES) % ciMAXSCENES   ENDIF
                    
                    //Skip the scene number equal to the current scene
                    IF iCurrentScene = iSelectedScene
                        IF navRight     iSelectedScene = (iSelectedScene + 1) % ciMAXSCENES
                        ELIF navLeft    iSelectedScene = (iSelectedScene - 1 + ciMAXSCENES) % ciMAXSCENES   ENDIF
                    ENDIF
                    
                    RESET_PI_MENU_NOW()
                ELIF ePEState = PES_PropPlace
                    IF navRight     DMPROP_NEXT(sPI_MenuData.iCurrentSelection)
                    ELIF navLeft    DMPROP_PREV(sPI_MenuData.iCurrentSelection)     ENDIF
                    
                    RESET_PI_MENU_NOW()
                ENDIF
            break
            #ENDIF
        endswitch           
    ENDIF
    
    //===================================== UP AND DOWN =========================================
    
    IF iNavVertDirection != 0 //-1 = up //1 = down
        
        IF bInPropEdit //Fix for part of 2510283. Make sure vertical navigation sounds can't spam in Prop Editor placed prop edit if there's only one prop in the list.
            IF ePEState = PES_PropList
                IF iNumProps > 1
                    PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                ENDIF
            ELSE
                PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) //Play Nav sound at all other times in the the Prop Editor.  
            ENDIF
        ELSE
            PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) 
        ENDIF

        //CPRINTLN(debug_director, "Checking this sound 2")
 
        //Wrap around get_menu_max
        SET_MENU_CURRENT_POSITION((sPI_MenuData.iCurrentSelection + iNavVertDirection + GET_MENU_MAX()) % GET_MENU_MAX())
        
        //Hide 'Settings have changed' display
        IF iSubMenu = PI_SUBMENU_SETTINGS
            bHideSettingsChangedDesc = TRUE
        ENDIF
        
        UPDATE_PI_MENU_MINI_MAP_ON_SELECTION_CHANGE()
        
        RESET_PI_MENU_NOW() 
    ENDIF
    
    //===================================== SPECIAL KEYS =========================================
    TIMEOFDAY todNow = GET_CURRENT_TIMEOFDAY()
    INT iHour, iMinute
    BOOL bChanged = FALSE
    SWITCH iSubMenu
        CASE PI_SUBMENU_SETTINGS
            SWITCH sPI_MenuData.iCurrentSelection
                CASE SEM_TIME   //Adjust TOD with left and right shoulders
                    IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_COVER)
                        bChanged = TRUE
                        ADD_TIME_TO_TIMEOFDAY(todNow, 0, 1)
                    ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
                        bChanged = TRUE
                        SUBTRACT_TIME_FROM_TIMEOFDAY(todNow, 0, 1)
                    ENDIF
                    IF bChanged
                        //B* 2262090: Disable recording while fine tuning the time
                        REPLAY_PREVENT_RECORDING_THIS_FRAME()
                        
                        bTimeSettingFineTuned = TRUE
                        iHour = GET_TIMEOFDAY_HOUR(todNow)
                        iMinute = GET_TIMEOFDAY_MINUTE(todNow)
                        SET_CLOCK_TIME(iHour,iMinute, GET_TIMEOFDAY_SECOND(todNow)) //Set the current time
                        
                        //Copy time settings into the settings storage
                        tempSettingsMenu[SEM_TIME] = GET_TIME_SETTINGS_OPTION(iHour, iMinute)   //Reflect current time in the PI menu
                        settingsMenu[SEM_TIME] = tempSettingsMenu[SEM_TIME]
                        
                        CHECK_IF_TEMP_SETTINGS_CHANGED()
                        RESET_PI_MENU_NOW()
                        
                    ENDIF
                    
                    //Freeze/unfreeze time
                    IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
                        bTimeFrozen = not bTimeFrozen
                        PAUSE_CLOCK(bTimeFrozen)
                        RESET_PI_MENU_NOW()
                    ENDIF
                BREAK
            ENDSWITCH
        BREAK
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        //These controls will need looked at for PC!
        case PI_SUBMENU_PROPS
            IF NOT bInPropEditorWarningScreen
                IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caPiMenuProp)
                    navDelProp = TRUE
                ENDIF
//                IF sPI_MenuData.iCurrentSelection = 0
//                AND DMPROP_GET_NUM_PROPS() > 0
//                    IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_Y )
//                        navClearScene = TRUE
//                    ENDIF
//                ENDIF
                IF ePEState = PES_PropList
                    IF DMPROP_GET_NUM_PROPS() > 0
                    
                    SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_FRONTEND_Y)//2511755
                    
                    IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_Y )
                            navWarpToProp = TRUE
                        ENDIF
                    ENDIF
                ENDIF
                
                BOOL bPrevShowSceneInfo
                bPrevShowSceneInfo = bShowCurrentSceneInfo
                bShowCurrentSceneInfo = (IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_LB ) AND ePEState = PES_SceneSelect)
                IF bPrevShowSceneInfo <> bShowCurrentSceneInfo
                    RESET_PI_MENU_NOW()
                ENDIF
                IF b_PropEditorRequestsImmediateMenuReset
                    b_PropEditorRequestsImmediateMenuReset = FALSE
                    RESET_PI_MENU_NOW()
                ENDIF
            ENDIF
        break
        #ENDIF
    ENDSWITCH
    
ENDPROC


/// PURPOSE:
///    Overrides the default navAccept/navCancel var values, as alert screens must use frontend controls only.
PROC OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    IF bInPropEditorWarningScreen
        navPropEditorAccept = FALSE
        navPropEditorBack = FALSE
                
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
            navPropEditorAccept = TRUE
        ENDIF
        
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
            navPropEditorBack = TRUE
        ENDIF
    ENDIF
    IF iSubMenu != PI_SUBMENU_PROPS
    AND NOT bInPropEditorWarningScreen
    #ENDIF
        navAccept = FALSE
        navBack = FALSE
        
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
            navAccept = TRUE
        ENDIF
        
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
            navBack = TRUE
        ENDIF
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    ENDIF
    #ENDIF
    
ENDPROC


PROC CONTROL_PI_MENU_INPUT()
    IF navAccept            
        SWITCH iSubMenu
            case PI_SUBMENU_NONE
                switch INT_TO_ENUM(DIRECTOR_MODE_PI_M0, sPI_MenuData.iCurrentSelection)
                    case PI_M0_SETTINGS
                        iStoredSelection = sPI_MenuData.iCurrentSelection
                        SET_MENU_CURRENT_POSITION(0)
                        iSubMenu = PI_SUBMENU_SETTINGS
                        COPY_ACTIVE_CHEATS_TO_SETTINGS()    //Copy currently active cheats
                        COPY_SETTINGS_MENU_DATA()   //Copy settings to temp display array
                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        RESET_PI_MENU_NOW()
                    break
                    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
                    case PI_M0_PROPS
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_PROPEDITOR)
                            iStoredSelection = sPI_MenuData.iCurrentSelection
                            SET_MENU_CURRENT_POSITION(0)
                            iSubMenu = PI_SUBMENU_PROPS
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            //Activate special prop settings
                            TOGGLE_PROP_EDITOR(TRUE)
                        ENDIF
                    break
                    #ENDIF
                    case PI_M0_LOCATION
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                            navTravel = TRUE
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        ENDIF                   
                    break                   
                    case PI_M0_GESTURE
                        IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_GESTURE)
                            navGesture = TRUE
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        ENDIF                       
                    break
                    case PI_M0_SPEECH
                        if IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_DIALOGUE)
                        AND iMaxSpeechEnries > 1
                            navSpeech = TRUE
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        endif                       
                    break
                    case PI_M0_SHORTLIST
                        if IS_SELECTED_SHORTLIST_PED_DIFFERENT_TO_PLAYER_MODEL(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]))
                        AND NOT ARE_SHORTLIST_ENTRIES_THE_SAME(GET_NTH_USED_SHORTLIST_INDEX(selectedPI[sPI_MenuData.iCurrentSelection]),lastSelectedShortlistPed)
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            navSwap = TRUE
                        endif                       
                    break
                    case PI_M0_VEHICLE
                        if GET_NUM_AVAILABLE_VEHS() > 0
                        and IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
                            iStoredSelection = sPI_MenuData.iCurrentSelection
                            SET_MENU_CURRENT_POSITION(0)
                            iSubMenu = PI_SUBMENU_VEHICLES
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            RESET_PI_MENU_NOW()
                        endif
                        navAccept = false
                    break      
                    case PI_M0_EDITOR
                        CPRINTLN(DEBUG_MISSION,"PLAY_SOUND_FRONTEND 1") 
                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        SET_DIRECTOR_MODE_STATE(dfs_query_editor)                       
                        navAccept = false
                    break
                    case PI_M0_RETURN
                        CPRINTLN(DEBUG_MISSION,"PLAY_SOUND_FRONTEND 2") 
                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        SET_DIRECTOR_MODE_STATE(dfs_query_trailer)
                        navAccept = false
                    break
                    case PI_M0_EXIT
                        CPRINTLN(DEBUG_MISSION,"PLAY_SOUND_FRONTEND 3") 
                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
                        navAccept = false
                    break
					#IF FEATURE_GEN9_STANDALONE
                    case PI_M0_MAIN_MENU
                        CPRINTLN(DEBUG_MISSION,"PLAY_SOUND_FRONTEND 3") 
                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                        SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
                        navAccept = false
                        eExitMode = SRCM_LANDING_PAGE
                    break
					#ENDIF //FEATURE_GEN9_STANDALONE
                endswitch                   
            break
            case PI_SUBMENU_VEHICLES                
                if GET_GAME_TIMER() - iVehicleSelectDelay >= 2000
                and IS_SCREEN_FADED_IN()
				and sPI_MenuData.iCurrentSelection > -1		//B* 3515506: Somehow got to this update with index -1 after a cleanup from accepting an online invite
                    if IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_VEHICLE)
                        if eAvailableVehs[sPI_MenuData.iCurrentSelection] != DUMMY_MODEL_FOR_SCRIPT
                            //Get vehicle gen data if grabbed from one
                            if eFromVehGen[sPI_MenuData.iCurrentSelection] > -1
                                GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(sVehDataMPInUse.VehicleSetup,eDynamicVehGens[eFromVehGen[sPI_MenuData.iCurrentSelection]])
                            else
                                sVehDataMPInUse.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
                            endif
                            bCreateVehicle = TRUE      
                            iVehicleCreateStage = 0
                            if mVehCurrentPersonal != eAvailableVehs[sPI_MenuData.iCurrentSelection]
                                if mVehCurrentPersonal != DUMMY_MODEL_FOR_SCRIPT
                                    SET_MODEL_AS_NO_LONGER_NEEDED(mVehCurrentPersonal)
                                endif
                                mVehCurrentPersonal = eAvailableVehs[sPI_MenuData.iCurrentSelection]
                                REQUEST_MODEL(mVehCurrentPersonal)
                                cprintln(debug_director," REQUEST_MODEL(mVehCurrentPersonal=",GET_MODEL_NAME_FOR_DEBUG(mVehCurrentPersonal),")")
                            endif
                                                        
                            DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
                            iVehicleSelectDelay = GET_GAME_TIMER()
                            PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)                    
                        endif
                    ENDIF
                ENDIF   
                navAccept = false
            break
            case PI_SUBMENU_SETTINGS
                
            break
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            case PI_SUBMENU_PROPS
                IF NOT bInPropEditorWarningScreen
                    SWITCH ePEState
                        case PES_SceneSelect
                            IF sPI_MenuData.iCurrentSelection = iCurrentScene
                                iSelectedScene = (iCurrentScene+1) % ciMAXSCENES
                                SET_MENU_CURRENT_POSITION(0)
                                DMPROP_SET_STATE(PES_SceneEdit)
                                PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                RESET_PI_MENU_NOW()
                            ELIF b_PropSceneUpdatedSinceLastSave
                                SET_DIRECTOR_MODE_STATE( dfs_query_load_scene)
                                PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)  
                            ELSE
                                DMPROP_LOAD_SCENE(sPI_MenuData.iCurrentSelection)
                                SET_MENU_CURRENT_POSITION(0)
                                DMPROP_SET_STATE(PES_SceneEdit)
                                //PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                PLAY_SOUND_FRONTEND (-1, "Load_Scene", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //New sound for bug 2448537
                                RESET_PI_MENU_NOW()
                            ENDIF
                        break
                        case PES_SceneEdit
                            SWITCH sPI_MenuData.iCurrentSelection
                                case ciPIPropPlace  //Place props
                                    iStoredSelection = sPI_MenuData.iCurrentSelection
                                    SET_MENU_CURRENT_POSITION(0)
                                    DMPROP_SET_STATE(PES_PropPlace)
                                    PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                    RESET_PI_MENU_NOW()
                                break
                                case ciPIPropList   //Prop list
                                    IF DMPROP_GET_NUM_PROPS() > 0
                                        iStoredSelection = sPI_MenuData.iCurrentSelection
                                        SET_MENU_CURRENT_POSITION(0)
                                        DMPROP_SET_STATE(PES_PropList)
                                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                                        RESET_PI_MENU_NOW() 
                                    ELSE
                                        PLAY_SOUND_FRONTEND (-1, "Place_Prop_Fail", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //2506623                                   
                                    ENDIF
                                break
                                case ciPISceneSave  //Save scene
                                    PLAY_SOUND_FRONTEND (-1, "Save_Scene", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //New sound for bug 2448537
                                    DMPROP_SAVE_SCENE(iCurrentScene)
                                    //PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) //2506623 - Just keep the Save_Scene effect...
                                    RESET_PI_MENU_NOW()
                                break
                                case ciPISceneCopy  //Copy scene
                                    IF iCurrentScene != iSelectedScene
                                    AND sdSceneData[iSelectedScene].iTotalProps > 0
//                                      OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
                                        SET_DIRECTOR_MODE_STATE( dfs_query_overwrite_scene)
                                        PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)  
                                    ELSE
                                        PLAY_SOUND_FRONTEND (-1, "Save_Scene", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //New sound for bug 2448537
                                        DMPROP_SAVE_SCENE(iSelectedScene)
                                        RESET_PI_MENU_NOW()
                                    ENDIF
                                break
                                case ciPISceneClear //Clear scene
                                    IF DMPROP_GET_NUM_PROPS() > 0
                                        PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
//                                      OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
                                        SET_DIRECTOR_MODE_STATE( dfs_query_prop_scene_clear )
                                    ELSE
                                        PLAY_SOUND_FRONTEND (-1, "Place_Prop_Fail", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //2506623   
                                    ENDIF
                                break
                            ENDSWITCH
                        break
                        case PES_PropPlace
                            IF DMPROP_ADD_PROP()
                            
                                IF bEditingProp AND (DMPROP_GET_NUM_PROPS() > 0)
									SET_MENU_CURRENT_POSITION(iEditPropIndex, 0)
                                    DMPROP_SET_STATE(PES_PropList)
                                ENDIF
                                bEditingProp = FALSE
                            ENDIF
                            
                            //PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) - Need to modify the DMPROP_ADD_PROP routine for place or can't place scenarios.
                            RESET_PI_MENU_NOW()
                        break
                        case PES_PropList
							INT iNewSelection
							iNewSelection = sPI_MenuData.iCurrentSelection
							iEditPropIndex = iNewSelection		//iNewSelection is used to set the index to prop type when editing
							
							CPRINTLN(debug_director, "Edit Prop - List Selection was ", iEditPropIndex)
                            IF DMPROP_EDIT_PROP(iNewSelection)
                                SET_MENU_CURRENT_POSITION(iNewSelection, 0)
                                RESET_PI_MENU_NOW()
                            ENDIF
                        break
                    ENDSWITCH
                    
                    ENDIF
                    navAccept = FALSE
            break
            #ENDIF
        ENDSWITCH   
    ENDIF
    
    //X/Square button press
    IF navRandom
        SWITCH iSubMenu
            CASE PI_SUBMENU_NONE BREAK
            CASE PI_SUBMENU_VEHICLES BREAK
            CASE PI_SUBMENU_SETTINGS
                //Apply the new settings
                IF bTempSettingsHaveChanged
                    COPY_SETTINGS_MENU_DATA(FALSE)
                    bApplyDMSettingsNow = TRUE
                    RESET_PI_MENU_NOW()
                ENDIF
                navRandom = FALSE
            BREAK
        ENDSWITCH
    
    ENDIF
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Waypoint, X on pads
    IF navDelProp
        SWITCH iSubMenu
            CASE PI_SUBMENU_PROPS
                IF ePEState = PES_SceneSelect
                AND sPI_MenuData.iCurrentSelection = iCurrentScene
                    SET_DIRECTOR_MODE_STATE(dfs_query_load_scene)
                ELIF ePEState = PES_SceneEdit
                    //Now handled by Accept on the Save Scene option                    
                ELIF ePEState = PES_PropList       
                    //Delete prop
                    INT iTopItem
                    iTopItem = GET_TOP_MENU_ITEM()
                    DMPROP_DELETE_PROP(sPI_MenuData.iCurrentSelection)
                    PLAY_SOUND_FRONTEND (-1, "Delete_Placed_Prop", "DLC_Dmod_Prop_Editor_Sounds",FALSE) //New sound for bug 2448537
                    //PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    IF sPI_MenuData.iCurrentSelection = DMPROP_GET_NUM_MENU_ENTRIES()
                        SET_MENU_CURRENT_POSITION(sPI_MenuData.iCurrentSelection -1)
//                        sPI_MenuData.iPreviousSelection = sPI_MenuData.iCurrentSelection
//                        sPI_MenuData.iCurrentSelection -= 1
                    ENDIF
                    //iTopItem = sPI_MenuData.iCurrentSelection - c_iMAX_PI_MENU_OPTIONS - 1
                    iTopItem -= 1
                    IF iTopItem < 0
                        iTopItem = 0
                    ENDIF
                    SET_TOP_MENU_ITEM( iTopItem )
                    RESET_PI_MENU_NOW()
                ENDIF
            BREAK
        ENDSWITCH
        navDelProp = FALSE
    ENDIF
    IF navWarpToProp
        IF iSubMenu = PI_SUBMENU_PROPS

            IF NOT IS_PED_INJURED(PLAYER_PED_ID())  //No longer allowing warping whilst in a vehicle.
                IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
                    IF ePEState = PES_PropList
                    //IF sPI_MenuData.iCurrentSelection >= ciPROPADDLINES - 1
                        IF DMPROP_GET_NUM_PROPS() > 0
                            IF ARE_STRINGS_EQUAL( sPropEditorHelpString, "DMPP_HLP_PLCD2") //Any other help string means the warp has been disabled. 2506996
                                CPRINTLN( DEBUG_MISSION,"PLAY_SOUND_FRONTEND 1" ) 
                                PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                                SET_DIRECTOR_MODE_STATE( dfs_warp_player_to_prop )
                                navWarpToProp = FALSE
                            ENDIF
                        ENDIF
                    ENDIF

                ENDIF
            ENDIF

            navWarpToProp = FALSE

        ENDIF
    ENDIF
    IF navBack
        IF iSubMenu = PI_SUBMENU_PROPS
		
		
		
			//Indented to draw attention to this. This destroys any elevated cam we might have created for manipulating any Stunt Prop mover object when the user changes menu level.
			#if FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			IF ePEState = PES_SceneEdit OR ePEState = PES_PropPlace
				IF DOES_CAM_EXIST(ElevatedCam)
					DMPROP_TOPDOWN_CAMERA_TOGGLE(FALSE)
				ENDIF
			ENDIF
			#ENDIF
	
		
            //Go back one state
            IF ePEState = PES_PropList
            OR ePEState = PES_PropPlace
                IF bEditingProp AND (DMPROP_GET_NUM_PROPS() > 0)
                    DMPROP_SET_STATE(PES_PropList)
                    SET_MENU_CURRENT_POSITION(0,0)
                ELSE
                    DMPROP_SET_STATE(PES_SceneEdit)
                    SET_MENU_CURRENT_POSITION(iStoredSelection,0)
                ENDIF
                bEditingProp = FALSE
                
                PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) 
                navBack = FALSE
                RESET_PI_MENU_NOW()
            ELIF ePEState = PES_SceneEdit
                SET_MENU_CURRENT_POSITION(iCurrentScene,0)
                DMPROP_SET_STATE(PES_SceneSelect)
                PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) 
                navBack = FALSE
                RESET_PI_MENU_NOW()
            ENDIF
        ENDIF
    ENDIF
    #ENDIF
ENDPROC

//These need to be outside the function as it's called every frame
vector      vCloseRoadNode 
float       fRoadNodeHeading
BOOL        bPlayerFlyingInVehicle
INT         iVehicleLoadSceneTimeout
PROC CONTROL_GARAGE_VEHICLE_CREATION()
    MODEL_NAMES mPlayerVehModel
    if bCreateVehicle
    and mVehCurrentPersonal != DUMMY_MODEL_FOR_SCRIPT
        switch iVehicleCreateStage
            case 0
                IF HAS_MODEL_LOADED(mVehCurrentPersonal)
                and IS_SCREEN_FADED_OUT()
                    
                    CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 2.0)
                    cprintln(debug_director," HAS_MODEL_LOADED(mVehCurrentPersonal)")
                    vCloseRoadNode   =  GET_ENTITY_COORDS(player_ped_id())
                    fRoadNodeHeading =  GET_ENTITY_HEADING(player_ped_id())
                    
                    bPlayerFlyingInVehicle = FALSE
                    //Only swap if player is flying in generated vehicle
                    IF DOES_ENTITY_EXIST(VehCurrentPersonal)
                        IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VehCurrentPersonal)
                            mPlayerVehModel = GET_ENTITY_MODEL(VehCurrentPersonal)
                            IF (IS_THIS_MODEL_A_HELI(mPlayerVehModel) OR IS_THIS_MODEL_A_PLANE(mPlayerVehModel))
                            AND IS_ENTITY_IN_AIR(VehCurrentPersonal)
                                bPlayerFlyingInVehicle = TRUE
                            ENDIF
                        ENDIF
                    ENDIF
                    
                    //Do a ground coord check for non-flying vehicles
                    IF NOT (IS_THIS_MODEL_A_HELI(mVehCurrentPersonal) OR IS_THIS_MODEL_A_PLANE(mVehCurrentPersonal))
                        FLOAT fGroundZ
                        GET_GROUND_Z_FOR_3D_COORD(vCloseRoadNode,fGroundZ)
                        vCloseRoadNode.z = fGroundZ
                    ENDIF
                    
                    //If switching to a heli or plane, stay in the air
                    IF bPlayerFlyingInVehicle AND (IS_THIS_MODEL_A_HELI(mVehCurrentPersonal) OR IS_THIS_MODEL_A_PLANE(mVehCurrentPersonal))
                        CPRINTLN(debug_director,"Player switching to a flying vehicle in air, keep in air")
                        vCloseRoadNode = GET_ENTITY_COORDS(PLAYER_PED_ID())
                        fRoadNodeHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
                    ELSE    
                        bPlayerFlyingInVehicle = FALSE  //This will place the vehicle on the ground safely
                        IF NOT START_UP_ROAD_NODE_TEST(vCloseRoadNode,fRoadNodeHeading,mVehCurrentPersonal,true,false, 2.5)
                            GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN(vCloseRoadNode,fRoadNodeHeading,mVehCurrentPersonal)
                        ENDIF
                        cprintln(debug_director,"Finished START_UP_ROAD_NODE_TEST")
                    ENDIF
                    iVehicleCreateStage++
                ELSE
                    CDEBUG1LN(debug_director,"Still waiting for model ",GET_MODEL_NAME_FOR_DEBUG(mVehCurrentPersonal)," to load or screen is not faded out")
                ENDIF
            break
            case 1
                if DOES_ENTITY_EXIST(VehCurrentPersonal)
                    IF IS_PED_IN_VEHICLE(player_ped_id(),VehCurrentPersonal)
                        SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(player_ped_id())+<<0,0,1>>)
                        bPedInVehicle = TRUE
                    ENDIF
                    
                    SAFE_DELETE_VEHICLE(VehCurrentPersonal)                 
                    if DOES_BLIP_EXIST(blipVehPersonal)
                        remove_blip(blipVehPersonal)
                    endif
                endif
                    
                if !IS_NEW_LOAD_SCENE_ACTIVE()
                    NEW_LOAD_SCENE_START_SPHERE(vCloseRoadNode,10)
                endif
           
                VehCurrentPersonal = CREATE_VEHICLE(mVehCurrentPersonal,vCloseRoadNode, fRoadNodeHeading)
                FREEZE_ENTITY_POSITION(VehCurrentPersonal, TRUE)
                iVehicleLoadSceneTimeout = GET_GAME_TIMER() + 1500
                    
                IF bPedInVehicle
                    SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),VehCurrentPersonal)
                ENDIF
                
                //Setup vehicle data if it exists
                IF sVehDataMPInUse.VehicleSetup.eModel <> DUMMY_MODEL_FOR_SCRIPT
                    CPRINTLN(debug_director,"Set-up vehicle data from vehicle gen for model ",GET_MODEL_NAME_FOR_DEBUG(sVehDataMPInUse.VehicleSetup.eModel))
                    SET_VEHICLE_SETUP(VehCurrentPersonal,sVehDataMPInUse.VehicleSetup)
                ELIF mVehCurrentPersonal = MARSHALL
                    //Apply marshall-specific colour patching
                    SITE_BUYABLE_VEHICLE_COLOURS colTemp, colNew 
                    colNew = INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, GET_RANDOM_INT_IN_RANGE(1,26))
                    sVehDataMPInUse.VehicleSetup.eModel = MARSHALL
                    colTemp = g_eLastBuyableVehicleColourSelected
                    g_eLastBuyableVehicleColourSelected = colNew
                    CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(BV_NG_MARSHALL,sVehDataMPInUse,TRUE)
                    SET_VEHICLE_SETUP(VehCurrentPersonal,sVehDataMPInUse.VehicleSetup)
                    g_eLastBuyableVehicleColourSelected = colTemp
                    CPRINTLN(debug_director,"Set-up vehicle special case for Marshall with colour ",ENUM_TO_INT(colNew))
                endif
                
                if not DOES_BLIP_EXIST(blipVehPersonal)
                    blipVehPersonal = CREATE_BLIP_FOR_ENTITY(VehCurrentPersonal)
                    SET_BLIP_SPRITE(blipVehPersonal, RADAR_TRACE_GANG_VEHICLE )
                    SET_BLIP_NAME_FROM_TEXT_FILE( blipVehPersonal, "CM_GARGVEH" )
                endif
                iVehicleCreateStage++
            break
            case 2
                    bool bFinish
                    bFinish = False
                    IF IS_NEW_LOAD_SCENE_LOADED()
                    OR GET_GAME_TIMER() > iVehicleLoadSceneTimeout
                        NEW_LOAD_SCENE_STOP()
                        bFinish = TRUE
                    Elif !IS_NEW_LOAD_SCENE_ACTIVE()
                        bFinish = TRUE
                    endif
                    
                    IF bFinish
                        IF NOT IS_ENTITY_DEAD(VehCurrentPersonal)
                            FREEZE_ENTITY_POSITION(VehCurrentPersonal, FALSE)
                            //Keep player in the air if switching to another flying vehicle
                            IF bPlayerFlyingInVehicle   
                                ACTIVATE_PHYSICS(VehCurrentPersonal)
                                IF IS_THIS_MODEL_A_HELI(mVehCurrentPersonal)
                                    SET_HELI_BLADES_FULL_SPEED(VehCurrentPersonal)
                                ELIF IS_THIS_MODEL_A_PLANE(mVehCurrentPersonal)
                                    SET_VEHICLE_FORWARD_SPEED(VehCurrentPersonal,60)
                                    CONTROL_LANDING_GEAR(VehCurrentPersonal,LGC_RETRACT_INSTANT)
                                ENDIf
                            ELSE
                                SET_VEHICLE_ON_GROUND_PROPERLY(VehCurrentPersonal)
                            ENDIF
                        ENDIF
                        
                        DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)                
                        bCreateVehicle = false
                        bPedInVehicle = FALSE
                    ENDIF   
                BREAK
            ENDSWITCH     
    ENDIF
    
ENDPROC


PROC MAINTAIN_MENU_CONTROLS()
    
    BOOL bDisableVehEnterExit = FALSE
    BOOL bDisableFlyingVehYaw = FALSE
    IF ( iSubMenu = PI_SUBMENU_NONE     AND sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_LOCATION) )
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    OR ( iSubMenu = PI_SUBMENU_PROPS AND ePEState = PES_PropList )
    #ENDIF
        bDisableVehEnterExit = TRUE
    ENDIF
    IF ( iSubMenu = PI_SUBMENU_SETTINGS AND sPI_MenuData.iCurrentSelection = SEM_TIME )
        bDisableVehEnterExit = TRUE
        bDisableFlyingVehYaw = TRUE
    ENDIF
    
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
            
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
        
        IF NOT bPiMouseCameraOverride
            SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
            SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
        ENDIF
        
        ENABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
                
    ELSE
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
    ENDIF
    
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL) 
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)   
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)  
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
    IF bDisableVehEnterExit
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER) //player jacks vehicle when hitting Y on PI menu 
    ENDIF
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
    IF bDisableVehEnterExit
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT) //player Can jump out of a car while the PI menu is on
    ENDIF
    IF bDisableFlyingVehYaw
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT) // flying vehicle yaw (LB,RB) controls conflict with time scrubbing
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
    ENDIF

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Disable submenu-specific player controls
    IF iSubMenu = PI_SUBMENU_PROPS
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
        //Disable special abilities
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)

        //You cannot enter the Scene Creator whilst in a valid parachute stay now but make sure parachute controls are disabled as a failsafe for 2514818.
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_TURN_LR)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_TURN_LEFT_ONLY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_TURN_RIGHT_ONLY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_PITCH_UD)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_PITCH_UP_ONLY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_PITCH_DOWN_ONLY)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_BRAKE_LEFT)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_BRAKE_RIGHT)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_SMOKE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_PRECISION_LANDING)
    ENDIF
    #ENDIF
    
    //Only apply controls if the screen is not faded/fading
    IF IS_SCREEN_FADED_IN()
        CONTROL_PI_MENU_MOVEMENT()  
        CONTROL_PI_MENU_INPUT()
    ELSE
        CDEBUG1LN(debug_director, "Not processing PI inputs this frame, screen is not faded in")
    ENDIF
ENDPROC
    
    
FUNC BOOL MENU_OFF_CHECK()
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Don't close the PI menu in Prop Editor mode
    IF bInPropEdit
        RETURN FALSE
    ENDIF
    #ENDIF

    IF !bCreateVehicle // Make sure menu doesn't close if we are creating vehicle. 
        IF IS_PED_INJURED(player_ped_id())
        OR IS_PHONE_ONSCREEN()
        OR IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
        OR IS_PED_RAGDOLL(PLAYER_PED_ID())
        OR( IS_PED_FALLING(PLAYER_PED_ID())and IS_SCREEN_FADED_IN())
        OR g_bPlayerIsInTaxi //block PI in taxi, as ped bug 2195328     
        OR IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
            RETURN TRUE
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC
FUNC BOOL CONTROL_MENU_CLOSE()
    if !bCreateVehicle // Make sure menu doesn't close if we are creating vehicle. 
        if navBack    
            IF iSubMenu = PI_SUBMENU_NONE 
                PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                g_bHasPauseMapBeenAccessed  = FALSE
                RETURN TRUE
                
            ELIF iSubMenu = PI_SUBMENU_VEHICLES                
                PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_MENU_CURRENT_POSITION(iStoredSelection)
                CONTROL_RETURN_MENU_TOP_MENU_ITEM()
                iSubMenu = PI_SUBMENU_NONE           
                RESET_PI_MENU_NOW()            
                RETURN FALSE    
            ELIF iSubMenu = PI_SUBMENU_SETTINGS
                PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_MENU_CURRENT_POSITION(iStoredSelection)
                CONTROL_RETURN_MENU_TOP_MENU_ITEM()
                iSubMenu = PI_SUBMENU_NONE           
                RESET_PI_MENU_NOW()            
                RETURN FALSE    
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            ELIF (iSubMenu = PI_SUBMENU_PROPS
            AND (ePEState = PES_SceneSelect AND NOT bInPropEditorWarningScreen))
                PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                IF b_PropSceneUpdatedSinceLastSave
                    SET_DIRECTOR_MODE_STATE(dfs_query_exit_scene)
                ELSE
                    //Load old settings settings here
                    TOGGLE_PROP_EDITOR(FALSE)
                    iSubMenu = PI_SUBMENU_NONE    
                    SET_MENU_CURRENT_POSITION(iStoredSelection)
                    CONTROL_RETURN_MENU_TOP_MENU_ITEM()
                    RESET_PI_MENU_NOW()       
                ENDIF
                RETURN FALSE    
            #ENDIF
            ENDIF 
        endif
        
        if MENU_OFF_CHECK()
            SET_MENU_CURRENT_POSITION(0,0)
            RETURN TRUE
        endif
    endif
        
RETURN FALSE

ENDFUNC
PROC CONTROL_PI_MENU_TOGGLE()
    //========== TOGGLE PI MENU =========== 
    IF nav_PI       
        //check for resolution change
        IF NOT IS_PHONE_ONSCREEN()
        AND NOT g_bPlayerIsInTaxi //block taxi as per bug 2195328
        AND NOT IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
        AND NOT bCreateVehicle
        AND NOT IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        AND NOT bInPropEdit
        #ENDIF
            IF NOT forceWaitForPIreset
                IF piMenuDelayTimer = 0 
                    piMenuDelayTimer = GET_GAME_TIMER() + 350 
                ELSE                                
                    IF GET_GAME_TIMER() > piMenuDelayTimer
                        IF PImenuON 
                            PImenuON = FALSE 
                            PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            CLEAR_DM_MENU_DATA()
                            CLEANUP_PI_MENU_MINI_MAP()
                            CLEANUP_MENU_ASSETS()
                            bMenuAssetsLoaded = FALSE
                            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
                            DMPROP_EXIT_PROP_EDIT()
                            #ENDIF
                            ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
                            ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_NEXT_RADIO)                          
                            //anything added here, also add to exit with navBack above.
                        ELSE 
                            PImenuON                    = TRUE 
                            bResetMenuNow               = TRUE
                            g_bHasPauseMapBeenAccessed  = FALSE
                            PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                            DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
                            DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
                            DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
                            DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)                             
                            bMenuAssetsLoaded = LOAD_MENU_ASSETS("DIR_MNU")
                            
                            //Copy the active cheat settings
                            IF iSubMenu = PI_SUBMENU_SETTINGS
                                COPY_ACTIVE_CHEATS_TO_SETTINGS()    //Copy active cheat data
                                COPY_SETTINGS_MENU_DATA()           //Copy settings menu data
                            ENDIF
                        ENDIF
                        forceWaitForPIreset = TRUE                      
                        piMenuDelayTimer = 0
                    ENDIF
                ENDIF   
            ENDIF
        ENDIF
    ELSE
        piMenuDelayTimer = 0 
        forceWaitForPIreset = FALSE
    ENDIF
    
    //Closing current menu / submenu
    IF PiMenuON         
        IF CONTROL_MENU_CLOSE()     
            iStoredSelection            = 0
            iSubmenu                    = PI_SUBMENU_NONE
            bMenuAssetsLoaded           = FALSE
            PImenuON                    = FALSE 
            nav_PI                      = FALSE
            CLEAR_DM_MENU_DATA()
            CLEANUP_PI_MENU_MINI_MAP()
            CLEANUP_MENU_ASSETS()       
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            DMPROP_EXIT_PROP_EDIT()
            #ENDIF
            ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
            ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_NEXT_RADIO)      
            ijustClosedTimer = get_Game_Timer()
        endif   
    else
        if GET_GAME_TIMER() - ijustClosedTimer < 500
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
            
            // Disable Pause menu so it doesn't trigger if Esc is pressed when you quit.
            IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                IF NOT IS_PAUSE_MENU_ACTIVE()
                    DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
                ENDIF
            ENDIF            
        endif         
    endif
ENDPROC

PROC CONTROL_PI_MENU()

    IF directorFlowState = dfs_PLAY_MODE
        
        IF selectedMenuType != MENUTYPE_PI
            selectedMenuType = MENUTYPE_PI
        ENDIF
        
        if PImenuON
            bBlockCinCam = TRUE
            MAINTAIN_ACTIVE_PI_MENU()   // draw
            MAINTAIN_MENU_CONTROLS()    // Controls
            
            //checks to see if player is in a state where we'd want to block/unblock player switching
            IF lastWarningStateBitSet != currentWarningStateBitSet                          
                lastWarningStateBitSet = currentWarningStateBitSet              
                RESET_PI_MENU_NOW()
            ENDIF
            
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            IF iSubMenu = PI_SUBMENU_PROPS
                Prop_Info_Disabling_and_Deletion_Alpha_Management(sPI_MenuData.iCurrentSelection)
            ENDIF
            #ENDIF
        endif
        
    // ======= BLOCK CINEMATIC CAM =========
        IF bBlockCinCam
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
            IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
                bBlockCinCam = FALSE                
            ENDIF
        ENDIF           
        
        CONTROL_PI_MENU_TOGGLE()    // switch menu state
        
    ENDIF

ENDPROC

bool zoomCamActive
PROC ENABLE_ZOOM_CAM(bool setActive)
    zoomCamActive = setActive
    
    //reset zoom when enabled
    IF setActive != zoomCamActive
        IF setActive = TRUE
            trailerCamZoom = 0.0
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL IS_ZOOM_CAM_ACTIVE()
    RETURN zoomCamActive
ENDFUNC

PROC SET_ZOOM_CAM_START_DATA(vector vPos, vector vRot, float fFOV)
    vZoomCamCoordStart = vPos
    vZoomCamRotStart = vRot
    fZoomCamFOVStart = fFOV
ENDPROC

PROC SET_ZOOM_CAM_END_DATA(vector vPos, vector vRot, float fFOV)
    vZoomCamCoordEnd = vPos
    vZoomCamRotEnd = vRot
    fZoomCamFOVEnd = fFOV
ENDPROC

PROC SET_ZOOM_CAM_END_DATA_FOR_ANIMAL(enumMenuCategory animalCategory)
    //IF IS_MENU_CATEGORY_A_SMALL_ANIMAL(animalCategory)
    //  SET_ZOOM_CAM_END_DATA(<<-15.6488, -11.2981, 500.9780>>,<<-11.4782, 0.0093, -144.2508>>,14.3504)
    //ELSE                          
        SWITCH animalCategory
            CASE MC_ANICAT
            CASE MC_ANICHI
            CASE MC_ANICRO
            CASE MC_ANIHEN
            CASE MC_ANISEA
                SET_ZOOM_CAM_END_DATA(<<-15.4647, -12.0765, 500.4821>>, <<-2.6991, 0.0000, -139.4340>>,14.1462)
            BREAK
            
            CASE MC_ANICOR
            CASE MC_ANIRAB
                SET_ZOOM_CAM_END_DATA(<<-15.5688, -12.0097, 500.4877>>, <<-2.6991, -0.0000, -135.2235>>,15.6853)
            BREAK
            
            CASE MC_ANIPIN
                SET_ZOOM_CAM_END_DATA(<<-15.5516, -11.9948, 500.4877>>, <<-4.9149, -0.0000, -139.1845>>, 12.5571)
            BREAK
            
            CASE MC_ANICOY
            CASE MC_ANIHUS
            CASE MC_ANIMOU
            CASE MC_ANIPIG
            CASE MC_ANIPOO
            CASE MC_ANIPUG
            CASE MC_ANIRET
            CASE MC_ANIROT
            CASE MC_ANISHE
            CASE MC_ANIWES
                SET_ZOOM_CAM_END_DATA(<<-16.3959, -9.4538, 500.2198>>, <<-6.2257, -0.0000, -136.8540>>, 12.8570)
            BREAK
            
            CASE MC_ANIBOAR
            CASE MC_ANICOW
                SET_ZOOM_CAM_END_DATA(<<-16.7295, -9.1912, 500.5512>>, <<-6.8945, -0.0000, -134.8060>>, 16.3732)
            BREAK
            
            CASE MC_ANIDEE
            CASE MC_ANISAS
                SET_ZOOM_CAM_END_DATA(<<-16.6988, -9.0181, 500.5765>>, <<-4.6458, -0.0000, -136.5473>>, 15.9849)
            BREAK
        ENDSWITCH
    //ENDIF
ENDPROC

enumMenuCategory lastViewedActorCategory = MC_STOFRA
PROC handleZoomCam()
    vector camZoomPosition,camZoomRotation
    float camZoomDOF
    
    
    
    IF GET_DIRECTOR_MODE_STATE() != dfs_PLAY_MODE
    
        if curMenuItem >= 0
        and curMenuItem < count_of(displayedMenuData)
            IF displayedMenuData[curMenuItem].category > MC_START_ACTORS
                lastViewedActorCategory = displayedMenuData[curMenuItem].category
            ENDIF
        endif
    
        IF IS_ZOOM_CAM_ACTIVE()
            IF navZoom or zoomPCToggle
                IF !zoomButtonJustReleased
                    IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_ANIMALS
                    OR GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                        SET_ZOOM_CAM_END_DATA_FOR_ANIMAL(lastViewedActorCategory)
                        IF trailerCamZoom != 0
                            trailerCamZoom = 0.0
                            SET_ZOOM_CAM_START_DATA(GET_CAM_COORD(GET_RENDERING_CAM()),GET_CAM_ROT(GET_RENDERING_CAM()),GET_CAM_FOV(GET_RENDERING_CAM()))
                            SET_CAM_ACTIVE(dir_zoomCamera,FALSE)
                        ENDIF
                    ENDIF
                ENDIF
                zoomButtonJustReleased = TRUE
                trailerCamZoom += 1.5 * TIMESTEP()
                If trailerCamZoom > 1.0 trailerCamZoom = 1.0 ENDIF
            ENDIF
        ENDIF
        
        If !IS_ZOOM_CAM_ACTIVE()
        OR !navZoom
            IF zoomButtonJustReleased
                IF IS_MENU_CATEGORY_A_SMALL_ANIMAL(lastViewedActorCategory)
                    SET_ZOOM_CAM_START_DATA(vCamSmallAnimalTrailerPos,vCamSmallAnimalTrailerRot,25.0)
                ENDIF
                
                IF IS_MENU_CATEGORY_A_BIG_ANIMAL(lastViewedActorCategory)
                    SET_ZOOM_CAM_START_DATA(vCamAnimalTrailerPos,vCamAnimalTrailerRot,25.0)
                ENDIF
            
                IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_ANIMALS
                OR GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                    SET_ZOOM_CAM_END_DATA(GET_CAM_COORD(GET_RENDERING_CAM()),GET_CAM_ROT(GET_RENDERING_CAM()),GET_CAM_FOV(GET_RENDERING_CAM()))
                    trailerCamZoom = 1.0
                ENDIF
                zoomButtonJustReleased = false
            ENDIF
            IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_ANIMALS          
                SET_ZOOM_CAM_START_DATA(vCamAnimalTrailerPos,vCamAnimalTrailerRot,25.0)
            ELIF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_SMALL_ANIMALS
                SET_ZOOM_CAM_START_DATA(vCamSmallAnimalTrailerPos,vCamSmallAnimalTrailerRot,25.0)
            ELIF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_TRAILER
                SET_ZOOM_CAM_START_DATA(vCamTrailerPanEndPos,vCamTrailerPanEndRot,25.0)
            ENDIF
            
            trailerCamZoom -= 1.5 * TIMESTEP()
            If trailerCamZoom < 0.0 trailerCamZoom = 0.0 ENDIF
            
            IF trailerCamZoom = 0.0
                IF DOES_CAM_EXIST(dir_zoomCamera)
                    SET_CAM_ACTIVE(dir_zoomCamera,FALSE)
                ENDIF
            ENDIF
        ENDIF
                            
        camZoomPosition = COSINE_INTERP_VECTOR(vZoomCamCoordStart,vZoomCamCoordEnd,trailerCamZoom) 
        camZoomRotation = COSINE_INTERP_VECTOR(vZoomCamRotStart,vZoomCamRotEnd,trailerCamZoom)
        camZoomDOF = COSINE_INTERP_FLOAT(fZoomCamFOVStart,fZoomCamFOVEnd,trailerCamZoom)
        
        IF DOES_CAM_EXIST(dir_zoomCamera)
            IF trailerCamZoom != 0.0
                SET_CAM_COORD(dir_zoomCamera, camZoomPosition)
                SET_CAM_ROT(dir_zoomCamera, camZoomRotation)
                SET_CAM_FOV(dir_zoomCamera,camZoomDOF)
            ENDIF
        ENDIF
        
        IF trailerCamZoom != 0.0
            IF NOT IS_CAM_ACTIVE(dir_zoomCamera)
                SET_CAM_ACTIVE(dir_zoomCamera,TRUe)
            ENDIF
        ELSE
            IF DOES_CAM_EXIST(dir_zoomCamera)
            AND IS_CAM_ACTIVE(dir_zoomCamera)
                SET_CAM_ACTIVE(dir_zoomCamera,FALSE)
            ENDIF
        ENDIF
    ENDIF
    
ENDPROC

FUNC INT GET_PAN_TIME(vector vStart,vector vEnd,float startFOV)
    float fZoomMultiplier = 1.0
    IF trailerCamZoom > 0.0
        fZoomMultiplier = 0.0
    ENDIF
    
    RETURN FLOOR((GET_DISTANCE_BETWEEN_COORDS(vStart,vEnd) * 320.0)+((25.0 - startFOV)*100.0*fZoomMultiplier))
ENDFUNC

PROC CONTROL_DIRECTOR_MODE_CAMERA()

    vector vCamPos,vCamRot
    
    INTERIOR_INSTANCE_INDEX intRespawn //Interior to attempt respawn in
    INT iTries = 0  //Times to wait for an interior to load
    INT iMaxTries = 100 //Number of attempts to wait before loading an interior

    handleZoomCam()

    SWITCH dirCamState
        CASE DIR_CAM_GAME_FADE_OUT
            IF NOT IS_SCREEN_FADING_OUT()
            AND NOT IS_SCREEN_FADED_OUT()
                DO_SCREEN_FADE_OUT(500)
            ENDIF
            
            IF IS_SCREEN_FADED_OUT()
                SET_CAMERA_STATE(DIR_CAM_GAME_FADED_OUT)
            ENDIF
        BREAK
        CASE DIR_CAM_PREP_TRAILER_SHOT
        
            IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
                NEW_LOAD_SCENE_START(<<-12.4578, 0.5969, 499.5298>>,CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<4.0059, -0.0000, 150.3755>>),30)
                bTrailerLoadSceneStarted = TRUE
            ELIF bTrailerLoadSceneStarted
                IF IS_NEW_LOAD_SCENE_LOADED()
                    bTrailerLoadSceneStarted = FALSE
                    IF IS_NEW_LOAD_SCENE_ACTIVE()
                        NEW_LOAD_SCENE_STOP()
                    ENDIF
                    
                    IF DOES_CAM_EXIST(dir_camera)
                        DESTROY_CAM(dir_Camera)
                    ENDIF
                    
                    IF DOES_CAM_EXIST(dir_zoomCamera)
                        DESTROY_CAM(dir_zoomCamera)
                    ENDIF
                    dir_zoomCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
                    SET_CAM_COORD(dir_zoomCamera,<<-12.4578, 0.5969, 499.5298>>)
                    
                    //dir_Camera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
                    dir_Camera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)  
                    SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,1.6)
                    SET_CAM_DOF_FOCUS_DISTANCE_BIAS(dir_Camera,-2.5)
                    SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(dir_Camera,0)                    
                    
                    ADD_CAM_SPLINE_NODE(dir_Camera,<<-12.4578, 0.5969, 499.5298>>, <<4.0059, -0.0000, 150.3755>>,50000)
                    ADD_CAM_SPLINE_NODE(dir_Camera,<<-12.4578, 0.5969, 499.5298>>, <<4.0059, -0.0000, 150.3755>>,50000)
                    //ADD_CAM_SPLINE_NODE(dir_Camera,<<-14.6234, -3.0591, 499.2865>>, <<7.8333, -0.0000, 153.6705>>,4000)
                    
                //  ADD_CAM_SPLINE_NODE(dir_Camera,<<-12.9, 0.6, 499.6>>, <<0.8340, 0.0000, 151.8658>>,4000)
                //  ADD_CAM_SPLINE_NODE(dir_Camera,<<-14.8049, -2.4849, 499.6678>>, <<5.4260, 0.0000, 152.0556>>,4000)
                    SET_CAM_FOV(dir_Camera,25)  
                    SET_CAM_SPLINE_SMOOTHING_STYLE(dir_Camera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
                    
                    //SET_CAM_COORD(dir_Camera,<<-1047.4485, -519.8412, 36.7403>>)
                    //SET_CAM_ROT(dir_camera,<<-6.1021, 0.0000, 131.6806>>)
                    RENDER_SCRIPT_CAMS(TRUE,FALSE)
                    SET_CAMERA_STATE(DIR_TRAILER_CAM_WAIT_FOR_FADE_IN)
                    iFadeTimer = GET_GAME_TIMER() + 200
                        
                ENDIF
            ELSE
                WHILE((IS_NEW_LOAD_SCENE_ACTIVE()) AND (iTries < iMaxTries))
                    CPRINTLN(debug_director,"WARNING: Waiting for an existing load scene to stop, tries left: ",iMaxTries - iTries)
                    NEW_LOAD_SCENE_STOP()
                    iTries += 1
                    wait(20)
                ENDWHILE                
                IF iTries > iMaxTries
                    CERRORLN("WARNING: Director mode waited over ",iMaxTries," times, forcing the new loadscene")
                ENDIF
                NEW_LOAD_SCENE_START(<<-12.4578, 0.5969, 499.5298>>,CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<4.0059, -0.0000, 150.3755>>),30)
                bTrailerLoadSceneStarted = TRUE
            ENDIF                   
        BREAK
        CASE DIR_TRAILER_CAM_WAIT_FOR_FADE_IN
            IF GET_GAME_TIMER() > iFadeTimer
                SET_CAMERA_STATE(DIR_TRAILER_CAM_FADE_IN)
                iFadeTimer = GET_GAME_TIMER() + 250
            ENDIF
        BREAK
        CASE DIR_TRAILER_CAM_FADE_IN
			CHECK_AND_TOGGLE_BUSY_SPINNER(FALSE," coming into trailer screne")
            IF NOT IS_SCREEN_FADING_IN()
            OR IS_SCREEN_FADED_OUT()
                DO_SCREEN_FADE_IN(500)
            ENDIF
            
            IF GET_GAME_TIMER() > iFadeTimer
            
                //check where cam needs to pan to:
                IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(player_ped_id()))
                    IF IS_MODEL_A_SMALL_ANIMAL(GET_ENTITY_MODEL(player_ped_id()))
                        SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_SMALL_ANIMALS)
                    ELSE
                        SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_ANIMALS)
                    ENDIF
                    bDoWalkin=FALSE
                ELSE
                    bDoWalkin=TRUE
                    SET_CAMERA_STATE(DIR_CAM_TRAILER_TO_TRAILER)
                ENDIF
                
                iPanTime = 4000

            ENDIF
        BREAK
        
        CASE DIR_CAM_TRAILER_TO_TRAILER
            bShowMenu = FALSE
            
            vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
            vCamRot = GET_CAM_ROT(GET_RENDERING_CAM())
            fStartPanFOV = GET_CAM_FOV(GET_RENDERING_CAM())
            
            ENABLE_ZOOM_CAM(FALSE)
            
            IF DOES_CAM_EXIST(dir_camera)
                DESTROY_CAM(dir_Camera)
            ENDIF
                
            dir_Camera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)  
            SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,1.6)
            SET_CAM_DOF_FOCUS_DISTANCE_BIAS(dir_Camera,-2.5)
            SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(dir_Camera,0)
            SET_CAM_FOV(dir_Camera,fStartPanFOV)
            
            IF directorFlowState != dfs_PREP_TRAILER_SCENE
                iPanTime = GET_PAN_TIME(vCamPos,<<-14.6119, -3.3614, 499.8450>>,fStartPanFOV)
            ELSE
                iPanTime = 4000
            ENDIF
            ADD_CAM_SPLINE_NODE(dir_Camera,vCamPos,vCamRot,iPanTime)
            ADD_CAM_SPLINE_NODE(dir_Camera,<<-14.6119, -3.3614, 499.8450>>, <<4.0059, 0.0000, 151.7677>>,iPanTime)
            
            
            
            //iPanTime = 2500   

            SET_CAM_SPLINE_SMOOTHING_STYLE(dir_Camera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
            SHAKE_CAM(dir_Camera,"HAND_SHAKE",0.15)
            SET_CAMERA_STATE(DIR_CAM_TRAILER_WAIT_FOR_WALKIN)
        BREAK
        
        CASE DIR_CAM_TRAILER_TO_SMALL_ANIMALS
            bShowMenu = FALSE
            
            vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
            vCamRot = GET_CAM_ROT(GET_RENDERING_CAM())
            fStartPanFOV = GET_CAM_FOV(GET_RENDERING_CAM())
            fStartPanDOF = GET_CAM_FAR_DOF(GET_RENDERING_CAM())
            
            ENABLE_ZOOM_CAM(FALSE)
            
            
            IF DOES_CAM_EXIST(dir_camera)
                DESTROY_CAM(dir_Camera)
            ENDIF
                
            dir_Camera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)  
            SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,1.6)
            SET_CAM_DOF_FOCUS_DISTANCE_BIAS(dir_Camera,-2.5)
            SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(dir_Camera,0)        
            SET_CAM_FOV(dir_Camera,fStartPanFOV)
            
            IF directorFlowState != dfs_PREP_TRAILER_SCENE
                iPanTime = GET_PAN_TIME(vCamPos,vCamSmallAnimalTrailerPos,fStartPanFOV)
            ELSE
                iPanTime = 4000
            ENDIF
            ADD_CAM_SPLINE_NODE(dir_Camera,vCamPos,vCamRot,iPanTime)
            
        
            
            ADD_CAM_SPLINE_NODE(dir_Camera,vCamSmallAnimalTrailerPos,vCamSmallAnimalTrailerRot ,iPanTime)
            
            //iPanTime = 2500

            SET_CAM_SPLINE_SMOOTHING_STYLE(dir_Camera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
            SHAKE_CAM(dir_Camera,"HAND_SHAKE",0.15)
            SET_CAMERA_STATE( DIR_CAM_TRAILER_PANNING_TO_SMALL_ANIMALS)
        BREAK
    
        CASE DIR_CAM_TRAILER_TO_ANIMALS
            bShowMenu = FALSE
            
            vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
            vCamRot = GET_CAM_ROT(GET_RENDERING_CAM())
            fStartPanFOV = GET_CAM_FOV(GET_RENDERING_CAM())
            fStartPanDOF = GET_CAM_FAR_DOF(GET_RENDERING_CAM())
            
            ENABLE_ZOOM_CAM(FALSE)
            
            
            IF DOES_CAM_EXIST(dir_camera)
                DESTROY_CAM(dir_Camera)
            ENDIF
                
            dir_Camera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)  
            SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,1.6)
            SET_CAM_DOF_FOCUS_DISTANCE_BIAS(dir_Camera,-2.5)
            SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(dir_Camera,0)        
            SET_CAM_FOV(dir_Camera,fStartPanFOV)
            
            IF directorFlowState != dfs_PREP_TRAILER_SCENE
                iPanTime = GET_PAN_TIME(vCamPos,vCamAnimalTrailerPos,fStartPanFOV)
            ELSE
                iPanTime = 4000
            ENDIF
            ADD_CAM_SPLINE_NODE(dir_Camera,vCamPos,vCamRot,iPanTime)
            
            
            ADD_CAM_SPLINE_NODE(dir_Camera,vCamAnimalTrailerPos,vCamAnimalTrailerRot ,iPanTime)
            
            //iPanTime = 2500

            SET_CAM_SPLINE_SMOOTHING_STYLE(dir_Camera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
            SHAKE_CAM(dir_Camera,"HAND_SHAKE",0.15)
            SET_CAMERA_STATE( DIR_CAM_TRAILER_PANNING_TO_ANIMALS)
        BREAK
        
        CASE DIR_CAM_TRAILER_PANNING_TO_SMALL_ANIMALS
            IF DOES_CAM_EXIST(dir_Camera)
                
                IF GET_CAM_SPLINE_PHASE(dir_Camera) >= 1.0
                    STOP_CAM_SHAKING(dir_Camera)                                        
                    SET_CAM_FOV(dir_Camera,25.0)        
                    SET_CAMERA_STATE(DIR_CAM_TRAILER_AT_SMALL_ANIMALS)
                ELSE
                    float fPanFov,fNewDOF
                    fPanFov = fStartPanFOV + ((25.0 - fStartPanFOV) * GET_CAM_SPLINE_PHASE(dir_Camera))
                    fNewDOF = fStartPanDOF + ((10.0 - fStartPanDOF) * GET_CAM_SPLINE_PHASE(dir_Camera))
                    SET_CAM_FOV(dir_Camera,fPanFov)
                    SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,fNewDOF)
                ENDIF
            ENDIF
        BREAK
        
        CASE DIR_CAM_TRAILER_PANNING_TO_ANIMALS
            IF DOES_CAM_EXIST(dir_Camera)
                
                IF GET_CAM_SPLINE_PHASE(dir_Camera) >= 1.0
                    STOP_CAM_SHAKING(dir_Camera)                                        
                    SET_CAM_FOV(dir_Camera,25.0)
                    SET_CAMERA_STATE(DIR_CAM_TRAILER_AT_ANIMALS)
                ELSE
                    float fPanFov,fNewDOF
                    fPanFov = fStartPanFOV + ((25.0 - fStartPanFOV) * GET_CAM_SPLINE_PHASE(dir_Camera))
                    fNewDOF = fStartPanDOF + ((10.0 - fStartPanDOF) * GET_CAM_SPLINE_PHASE(dir_Camera))
                    SET_CAM_FOV(dir_Camera,fPanFov)
                    SET_CAM_DOF_FNUMBER_OF_LENS(dir_Camera,fNewDOF)
                ENDIF
            ENDIF
        BREAK
        
        CASE DIR_CAM_TRAILER_PANNING_TO_TRAILER
        FALLTHRU
        CASE DIR_CAM_TRAILER_CAMERA_TRIGGER_WALKIN
            IF DOES_CAM_EXIST(dir_Camera)
    
                IF GET_CAM_SPLINE_PHASE(dir_Camera) >= 1.0
                    STOP_CAM_SHAKING(dir_Camera)
                    SET_CAM_FOV(dir_Camera,25.0)
                    SET_CAMERA_STATE(DIR_CAM_TRAILER_AT_TRAILER)
                    bDoWalkin = FALSE
                ELSE
                    float fPanFov
                    fPanFov = fStartPanFOV + ((25.0 - fStartPanFOV) * GET_CAM_SPLINE_PHASE(dir_Camera))
                    SET_CAM_FOV(dir_Camera,fPanFov)
                ENDIF
            ENDIF
        BREAK
        
        CASE DIR_CAM_TRAILER_AT_ANIMALS
        FALLTHRU
        CASE DIR_CAM_TRAILER_AT_TRAILER
        CASE DIR_CAM_TRAILER_AT_SMALL_ANIMALS
        
            //handle zoom button
            IF !IS_ZOOM_CAM_ACTIVE()
                ENABLE_ZOOM_CAM(TRUE)           
                IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_TRAILER
                    SET_ZOOM_CAM_START_DATA(<<-14.6119, -3.3614, 499.8450>>,<<4.0059, 0.0000, 151.7677>>, 25.0)
                    SET_ZOOM_CAM_END_DATA(vCamTrailerZoomPos,vCamTrailerZoomRot,16.2090)
                ELSE
                    //update animal choice as player changes it.
                    IF GET_CAMERA_STATE() = DIR_CAM_TRAILER_AT_ANIMALS                  
                        SET_ZOOM_CAM_START_DATA(vCamAnimalTrailerPos,vCamAnimalTrailerRot, 25.0)
                    ELSE
                        SET_ZOOM_CAM_START_DATA(vCamSmallAnimalTrailerPos,vCamSmallAnimalTrailerRot, 25.0)
                    ENDIF
                    SET_ZOOM_CAM_END_DATA_FOR_ANIMAL(displayedMenuData[curMenuItem].category)
                ENDIF
            ELSE //already active. Check if player changes animal selection
                IF displayedMenuData[curMenuItem].category != zoomCamAnimalChoice
                AND IS_MENU_CATEGORY_AN_ANIMAL(displayedMenuData[curMenuItem].category)
                    zoomCamAnimalChoice = displayedMenuData[curMenuItem].category
                    SET_ZOOM_CAM_START_DATA(GET_CAM_COORD(GET_RENDERING_CAM()),GET_CAM_ROT(GET_RENDERING_CAM()),GET_CAM_FOV(GET_RENDERING_CAM()))
                    SET_ZOOM_CAM_END_DATA_FOR_ANIMAL(zoomCamAnimalChoice)
                    trailerCamZoom = 0.0
                ENDIF
            ENDIF
            
             
            bShowMenu = TRUE
        BREAK

        CASE DIR_CAM_TRAILER_WAIT_FOR_WALKIN
            
            IF bDoWalkin
                IF DOES_CAM_EXIST(dir_Camera)
                    IF GET_CAM_SPLINE_PHASE(dir_Camera) > 0.3
                        SET_CAMERA_STATE(DIR_CAM_TRAILER_CAMERA_TRIGGER_WALKIN)
                    ENDIF
                ENDIF
            ELSE
                SET_CAMERA_STATE(DIR_CAM_TRAILER_PANNING_TO_TRAILER)
            ENDIF           
        BREAK

        CASE DIR_CAM_LEADOUT
        
            
            ENABLE_ZOOM_CAM(FALSE)
            bShowMenu = FALSE
            
            IF HAS_ANIM_DICT_LOADED("director@character_select_intro@male")
            AND HAS_ANIM_DICT_LOADED("director@character_select_intro@female")
                IF NOT IS_PED_INJURED(player_ped_id())
                
                    vector vRot,vPos
                    vPos = <<-20.002,-10.778,500.615>> //GET_ENTITY_ROTATION(player_ped_id())
                    vRot = <<0,0,-56.160>>
                    int stepsScene 
                    stepsScene = CREATE_SYNCHRONIZED_SCENE(vPos,vRot)
                                    
                    TEXT_LABEL_63 anim
                    anim = GET_EXIT_ANIM_FOR_PED_MODEL_CATEGORY(dirCurrentMenu,curMenuItem)
                    
                    IF IS_DIRECTOR_PED_MALE()
                        TASK_SYNCHRONIZED_SCENE(player_ped_id(),stepsScene,"director@character_select_intro@male",anim,1.5,FAST_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE,2)
                    ELSE
                        TASK_SYNCHRONIZED_SCENE(player_ped_id(),stepsScene,"director@character_select_intro@female",anim,1.5,FAST_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE,2)                   
                    ENDIF
                ENDIF
                aLeadoutTimer = GET_GAME_TIMER() + 2600
                SET_CAMERA_STATE(DIR_CAM_ENTER_PLAY_MODE)
            ENDIF
        BREAK
        
        CASE DIR_CAM_ENTER_PLAY_MODE
		
            ENABLE_ZOOM_CAM(FALSE)
            IF GET_GAME_TIMER() > aLeadoutTimer
                IF NOT IS_SCREEN_FADING_OUT()
                AND NOT IS_SCREEN_FADED_OUT()
                    DO_SCREEN_FADE_OUT(500)
                ENDIF
                
				CHECK_AND_TOGGLE_BUSY_SPINNER(TRUE,"when entering play mode")
				
                IF IS_SCREEN_FADED_OUT() AND LOAD_ANIMAL_AUDIO_AND_ANIM()
                    bShowMenu = FALSE
                    DISPLAY_HUD(TRUE)
                    RENDER_SCRIPT_CAMS(FALSE,FALSE)
                    IF DOES_CAM_EXIST(dir_camera)
                        DESTROY_CAM(dir_camera)                                     
                    ENDIF
                    
                    #IF IS_DEBUG_BUILD
                        DEBUG_PRINT_WARNING_STATE(exitWarningStateBitSet)                   
                    #ENDIF          
                    SET_ENTITY_INVINCIBLE(player_ped_id(),TRUE) //to stop player dying straight away if spawned in water.

                    //Teleport player instead of map_warp_with_load
                    BOOL bGroundPlayerOnWarp
                    IF vLastPlayerCoord.z < 0
                        vLastPlayerCoord.z = 0.05
                        bGroundPlayerOnWarp = FALSE
                        CPRINTLN(DEBUG_DIRECTOR,"Not grounding player on warp...")
                    ELSE
                        bGroundPlayerOnWarp = TRUE
                        CPRINTLN(DEBUG_DIRECTOR,"Grounding player on warp...")
                    ENDIF
                    
                    //Teleport player instead of map_warp_with_load
                    DO_PLAYER_TELEPORT(vLastPlayerCoord, fPlayerHeading, FALSE, bGroundPlayerOnWarp)
                    
                    cprintln(DEBUG_DIRECTOR,"Find safe coord...")
                    IF IS_BIT_SET(exitWarningStateBitSet,BIT_PWS_INAIR)
                    OR IS_BIT_SET(exitWarningStateBitSet,BIT_PWS_INWATER)
                    OR IS_BIT_SET(exitWarningStateBitSet,BIT_PWS_INDEEPWATER)
                    OR IS_BIT_SET(exitWarningStateBitSet,BIT_PWS_IN_TRAIN)
                    OR IS_BIT_SET(exitWarningStateBitSet,BIT_PWS_INCAR)
                        vLastPlayerCoord = GET_CLOSEST_SAFE_POINT_FOR_PLAYER(GET_ENTITY_COORDS(player_ped_id()),TRUE)   
                        cprintln(DEBUG_DIRECTOR," safe closest coord = ",vLastPlayerCoord)
                        IF GET_DISTANCE_BETWEEN_COORDS(vLastPlayerCoord,GET_ENTITY_COORDS(player_ped_id())) < 1.0
                            vLastPlayerCoord = GET_CLOSEST_SAFE_POINT_FOR_PLAYER(GET_ENTITY_COORDS(player_ped_id()),TRUE,FALSE)                     
                            cprintln(DEBUG_DIRECTOR," safe point sucks. Try again.  coord = ",vLastPlayerCoord)
                        ENDIF
                        //Teleport player instead of map_warp_with_load
                        WHILE IS_PLAYER_TELEPORT_ACTIVE()
                        //stop previous teleport, wait
                            STOP_PLAYER_TELEPORT()
                            WAIT(0)
                        ENDWHILE
                        DO_PLAYER_TELEPORT(vLastPlayerCoord, fPlayerHeading, FALSE)
                    ELSE
                        vLastPlayerCoord = GET_PED_BONE_COORDS(player_ped_id(),BONETAG_L_FOOT,<<0,0,0>>)
                        cprintln(DEBUG_DIRECTOR," spawn point is not in water etc : ",vLastPlayerCoord)
                    ENDIF
                                        
                    cprintln(DEBUG_DIRECTOR,"Enter game. Do spawn coord check at ",vLastPlayerCoord)
                    vRespawnCoords = GET_CLOSEST_SAFE_POINT_FOR_PLAYER(vLastPlayerCoord,false)
                    shapeTestRespawn = null
                    shpTestChestHeight = null
                    shpTestFloor = null
                    SET_CAMERA_STATE(DIR_CAM_ENTER_PLAY_MODE_DO_SHAPETEST) //after spawening player above, then need to perform some checks to see if we need to reposition player somewhere safer below.
                ENDIF
            ENDIF
        BREAK


        CASE DIR_CAM_ENTER_PLAY_MODE_DO_SHAPETEST
			IF PERFORM_SHAPETEST_REPOSITION(vLastPlayerCoord,GET_ENTITY_MODEL(player_ped_id()),vRespawnCoords,fPlayerHeading,bPassShapetest,bRespawnPass,TRUE)
                cprintln(DEBUG_DIRECTOR,"MAP WARP. Player coords: ",vRespawnCoords)
                
                TRIGGER_IDLE_ANIMATION_ON_PED(PLAYER_PED_ID())
                //FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID()) //try bool to true if this didn't work
                               
                SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
                SET_PLAYER_CONTROL(player_id(),TRUE)
               
                IF IS_PLAYER_PLAYING(player_id())
                    SET_PLAYER_WANTED_LEVEL(player_id(),0)
                ENDIF
                
                SET_PLAYER_SCUBA_STATUS()
                cprintln(DEBUG_DIRECTOR,"DIR_CAM_ENTER_PLAY_MODE_DO_SHAPETEST: set up scuba stuff")
                
                //Wait until the interior is loaded in
                intRespawn = GET_INTERIOR_AT_COORDS(vRespawnCoords)
                IF intRespawn <> NULL
                    CDEBUG1LN(debug_director,"Player to spawn in an interior, waiting for it to be ready")
                    WHILE (NOT IS_INTERIOR_READY(intRespawn)) AND iTries < iMaxTries
                        DISABLE_FIRST_PERSON_VIEW_ON_RESTRICTED_MODELS_THIS_FRAME()  
                        CDEBUG1LN(debug_director, "Interior is not ready, waiting (",iMaxTries-iTries,"tries left)")
                        iTries += 1
                        wait(20)
                    ENDWHILE
                ENDIF
                
                //Set Trevor's appartment building state to normal
                SET_TREVORS_APARTMENT_CLEAN()
                SET_SHOWROOM_BOARDED()
                
                //Apply DM settings
                Apply_DM_Game_Settings()
                
                SET_CAMERA_STATE(DIR_CAM_PLAY_MODE  )
                DO_SCREEN_FADE_IN(500)
            ENDIF

        BREAK
                
        CASE DIR_CAM_FADE_IN_FROM_WARP
            IF GET_GAME_TIMER() > aLeadoutTimer
                DO_SCREEN_FADE_IN(500)
                SET_CAMERA_STATE(DIR_CAM_PLAY_MODE)
            ENDIF
        BREAK
		
		CASE DIR_CAM_PLAY_MODE
			//Monitor the spinner constantly, disable if active
			CHECK_AND_TOGGLE_BUSY_SPINNER(FALSE,"while in Play mode")
		BREAK

    ENDSWITCH
ENDPROC

PROC SET_LOCATION(DirectorTravelLocation dTravelLoc, float heading, float camrelativePitch=-8.0,float camrelativeHeading=0.0 )
    
    INT iEntry = ENUM_TO_INT( dTravelLoc )
    
    warpLocations[ iEntry ].position            = GET_DIRECTOR_TRAVEL_POSITION( dTravelLoc )
    warpLocations[ iEntry ].heading             = heading
    warpLocations[ iEntry ].camRelativePitch    = camrelativePitch
    warpLocations[ iEntry ].camRelativeHeading  = camrelativeHeading
    warpLocations[ iEntry ].tlLocName           = PRIVATE_Get_Director_Travel_Location_Name_Label( dTravelLoc )
ENDPROC

PROC SET_LOCATION_BIKE(int iEntry, vector pos, float heading, MODEL_NAMES model)
    warpLocations[iEntry].bikePos = pos
    warpLocations[iEntry].bikeRot = heading
    warpLocations[iEntry].model = model
ENDPROC

PROC SETUP_WARP_LOCATIONS()

    // GET_DIRECTOR_TRAVEL_POSITION extracted to a function so main.sc
    // can query if the location has been revealed on the map -BenR

    warpLocations[ ENUM_TO_INT( DTL_USER_1 ) ].tlLocName    = PRIVATE_Get_Director_Travel_Location_Name_Label( DTL_USER_1 )
    warpLocations[ ENUM_TO_INT( DTL_USER_2 ) ].tlLocName    = PRIVATE_Get_Director_Travel_Location_Name_Label( DTL_USER_2 )
    
    SET_LOCATION( DTL_LS_INTERNATIONAL,     148.6474,   17,     3.3     )
    SET_LOCATION( DTL_DOCKS,                49.6654,    3.78,   -6.5    )
    SET_LOCATION( DTL_LS_DOWNTOWN,          49.6931,    0,      -0.4    )
    SET_LOCATION( DTL_DEL_PERRO_PIER,       162.0262,   1.53            )
    SET_LOCATION( DTL_VINEWOOD_HILLS,       210.7911            )
    SET_LOCATION( DTL_WIND_FARM,            168,        2.6,    20.8    )
    SET_LOCATION( DTL_SANDY_SHORES,         98.1669,    -3.7)
    SET_LOCATION( DTL_STAB_CITY,            356.7321,   -2.95,  1.8     )
    SET_LOCATION( DTL_FORT_ZANCUDO,         40.3590,    -0.5,   13.7    )
    SET_LOCATION( DTL_CANYON,               263.7209,   -1.4)
    SET_LOCATION( DTL_CABLE_CAR,            136.4268,   -17.67, -7.0    )
    SET_LOCATION( DTL_PALETO,               313.4251)                                                 
    SET_LOCATION( DTL_LIGHTHOUSE,           268.4852,   -7.3,   -9.6    )
    
    SET_LOCATION( DTL_VINEYARD,             -24.7,      -12.7           )
    SET_LOCATION( DTL_STUDIO,               151.6,      4.3             )
    SET_LOCATION( DTL_GOLFCLUB,             -35,        1.9             )
    SET_LOCATION( DTL_ALTRUIST,             72.1,       11.9            )
    SET_LOCATION( DTL_POWERSTATION,         -157.4,     20.1            )
    SET_LOCATION( DTL_SAWMILL,              -39.5,      16.3            )
    SET_LOCATION( DTL_QUARRY,               -172,       13.4            )
    SET_LOCATION( DTL_DAM,                  105.2,      -5.9            )
    SET_LOCATION( DTL_STORMDRAIN,           144.8,      -8              )
    SET_LOCATION( DTL_OBSERVATORY,          161.5,      2.5             )
    SET_LOCATION( DTL_RADIOARRAY,           72.6,       18.4            )
    SET_LOCATION( DTL_LSU,                  -10.4,      3.7             )                 
    SET_LOCATION( DTL_COVE,                 -144.2,     -1.7            )
    SET_LOCATION( DTL_MTGORDO,              165.1,      -4.1            )
    
    //Bike locations    
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_VINEWOOD_HILLS), <<-1349.4957, 734.0758, 184.0018>>,     1.2863,     SANCHEZ)    
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_WIND_FARM),      <<2202.8694, 2484.1890, 87.1479>>,      112.4964,   SANCHEZ)
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_STAB_CITY),      <<92.1990, 3597.8613, 38.7236>>,        186.2282,   HEXER)  
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_FORT_ZANCUDO),   <<-1567.0304, 2782.2864, 16.3059>>,     77.8481,    BLAZER)
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_CANYON),         <<-883.6553, 4427.4507, 19.8718>>,      169.3992,   SANCHEZ)
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_CABLE_CAR),      <<471.2471, 5617.5845, 785.3168>>,      219.7432,   SANCHEZ)
    SET_LOCATION_BIKE( ENUM_TO_INT(DTL_LIGHTHOUSE),     <<3332.8276, 5159.6484, 17.3071>>,      157.0522,   TORNADO3)

ENDPROC

PROC SETUP_SETTINGS_MENU_DEFAULTS()
    settingsMenu[SEM_VEHDENSITY] = 3
    settingsMenu[SEM_PEDDENSITY] = 3
    
    
    //set weather and time
    //set weather and time of day settings...
    GET_CURR_WEATHER_STATE(iStoredWeather[0],iStoredWeather[1],fWeatherTransition)
    iClockHours = GET_CLOCK_HOURS()
    iClockMinutes = GET_CLOCK_MINUTES()
    //fRainLevel = GET_RAIN_LEVEL()
    
    settingsMenu[SEM_TIME] = GET_TIME_SETTINGS_OPTION(iClockHours, iClockMinutes)
    
//  IF iClockHours >= 23 OR iClockHours < 2 settingsMenu[SEM_TIME] = 0 ENDIF
//  IF iClockHours >= 2 AND iClockHours < 6 settingsMenu[SEM_TIME] = 1 ENDIF
//  IF iClockHours >= 6 AND iClockHours < 7 settingsMenu[SEM_TIME] = 2 ENDIF
//  IF iClockHours >= 7 AND iClockHours < 11 settingsMenu[SEM_TIME] = 3 ENDIF
//  IF iClockHours >= 11 AND iClockHours < 14 settingsMenu[SEM_TIME] = 4 ENDIF
//  IF iClockHours >= 14 AND iClockHours < 17 settingsMenu[SEM_TIME] = 5 ENDIF
//  IF iClockHours >= 17 AND iClockHours < 20 settingsMenu[SEM_TIME] = 6 ENDIF
//  IF iClockHours >= 20 AND iClockHours < 23 settingsMenu[SEM_TIME] = 7 ENDIF
//  
    int CurrentWeather
    IF fWeatherTransition > 0.6
        CurrentWeather = iStoredWeather[1]
    ELSE
        CurrentWeather = iStoredWeather[0]
    ENDIF
    
    int iWeather[9]
    iWeather[0] = GET_HASH_KEY("CLEAR") 
    iWeather[1] = GET_HASH_KEY("CLOUDS")
    iWeather[2] = GET_HASH_KEY("OVERCAST")              
    iWeather[3] = GET_HASH_KEY("HAZY")
    iWeather[4] = GET_HASH_KEY("XMAS")
    iWeather[5] = GET_HASH_KEY("SMOG")
    iWeather[6] = GET_HASH_KEY("FOGGY")
    iWeather[7] = GET_HASH_KEY("RAIN")
    iWeather[8] = GET_HASH_KEY("THUNDER")
    
    int i               
    settingsMenu[SEM_WEATHER] = 0
    REPEAT COUNT_OF(iWeather) i
        IF iWeather[i] = CurrentWeather
            settingsMenu[SEM_WEATHER] = i
        ENDIF
    ENDREPEAT   
    
    //CHEATS - Default off
    for i = SEM_INVINCIBLE to SEM_TOTAL - 1
        settingsMenu[i] = 1
    endfor
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Default Prop settings
    for i = 0 to SEM_TOTAL - 1
        propSettingsMenu[i] = settingsMenu[i]
    endfor
    propSettingsMenu[SEM_VEHDENSITY] = 0
    propSettingsMenu[SEM_PEDDENSITY] = 0
    propSettingsMenu[SEM_RESTRICTED] = 1
    propSettingsMenu[SEM_WANTED] = 4    //No wanted level
    propSettingsMenu[SEM_INVINCIBLE] = 0
    propSettingsMenu[SEM_CLEARAREA] = 0
    #ENDIF
                                
ENDPROC


PROC SET_TRAVEL_TO_LOCATION(int iTravelEntry)
    IF !IS_VECTOR_ZERO(warpLocations[iTravelEntry].position)
        SET_DIRECTOR_MODE_STATE(dfs_warp_to_location)
        CLEANUP_PI_MENU_MINI_MAP()
        CLEAR_DM_MENU_DATA()
        PImenuON = FALSE
        bMenuAssetsLoaded = FALSE
        CLEANUP_MENU_ASSETS()
        iWarpLocation = iTravelEntry
    ENDIF
ENDPROC




BOOL bAllSpecialCharasUnlocked          = FALSE
BOOL bAllStoryModeCharasUnlocked        = FALSE
BOOL bAlmostAllAnimalsUnlocked          = FALSE
BOOL bIsPlayingAsAnAnimal               = FALSE
BOOL bPlayingAsChrisFormage             = FALSE
BOOL bIsPlayingAsOnlineChara            = FALSE
BOOL bIsPlayingAsStoryChara             = FALSE
BOOL bIsPlayingAsSpecialChara           = FALSE
BOOL bAwardLocationUnlockAchievement    = FALSE
BOOL bWaitBeforeInput                   = FALSE
BOOL bPaused                            = FALSE


PROC CONTROL_DIRECTOR_MODE_FLOW()
    
    IF directorFlowState = dfs_invalid //something is causing this value to get reset.
        cprintln(DEBUG_DIRECTOR,"********************FLOW STATE GIVEN INVALID VALUE***********************")
        directorFlowState = lastValidDirectorFlowState
    ELSE
        lastValidDirectorFlowState = directorFlowState
    ENDIF
    
    FE_WARNING_FLAGS feWarningFlags
    
    BOOL bForceRoadNode
    
    //Check for switch requests from the Selector HUD.
    IF g_bDirectorSwitchToCastingTrailer
        CPRINTLN(DEBUG_DIRECTOR, "Director Mode detected a request to return to the Casting Trailer.")
		IF directorFlowState = dfs_query_trailer OR directorFlowState = dfs_PREP_TRAILER_SCENE OR directorFlowState = dfs_RUN_TRAILER_SCENE
			CPRINTLN(debug_director,"But we're already at the trailer! Cancelling return.")
		ELSE
        	SET_DIRECTOR_MODE_STATE(dfs_query_trailer,TRUE)
		ENDIF
		CDEBUG1LN(DEBUG_DIRECTOR,"CONTROL_DIRECTOR_MODE_FLOW: Setting g_bDirectorSwitchToCastingTrailer to FALSE")
        g_bDirectorSwitchToCastingTrailer = FALSE
    ENDIF
    
    IF g_bDirectorSwitchToLastPlayer
        CPRINTLN(DEBUG_DIRECTOR, "Director Mode detected a request to return to Story Mode.")
        SET_DIRECTOR_MODE_STATE(dfs_query_terminate, TRUE)
        g_bDirectorSwitchToLastPlayer = FALSE
    ENDIF
    
    SWITCH directorFlowState
        CASE dfs_INIT
            #IF FEATURE_SP_DLC_DM_PROP_EDITOR
            //Cleanup Prop editor leftovers
            DMPROP_EXIT_PROP_EDIT()
            bInPropEdit = FALSE
            #ENDIF
        
            DISABLE_RESTRICTED_AREA(eRestrictedArea_MovieStudio)
            IF bReturningFromActorMode
                DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
                DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            ENDIF
            SET_CAMERA_STATE(DIR_CAM_GAME_FADE_OUT)                     
            DISABLE_CELLPHONE(TRUE)
            
            CLEAR_HELP(TRUE)
            
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)  
            
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),TRUE)
            ENDIF
            
            buildSceneState = BSS_INIT
            directorFlowState = dfs_PREP_TRAILER_SCENE
            PImenuON = FALSE
            bMenuAssetsLoaded = FALSE
            selectedPI[PI_M0_GESTURE] = 1 //0 = NONE so avoid that.
            dirMenuState = dirMENU_LOAD
            
            PREP_MENU_DRAWING()
            
        BREAK
        CASE dfs_PREP_TRAILER_SCENE 
            IF bReturningFromActorMode
                DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
                DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
                SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
                //B* 2419941: Reset last selected actor values
                lastSelectableModelCategory = MC_VOID
                lastSelectableModelIndex = 0
                requestedModel = DUMMY_MODEL_FOR_SCRIPT
                activeMenuEntry = MC_VOID   //B* 2427351: Mark the active menu entry for refresh on returning from play mode
            ENDIF
            
            REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
            bScaleformShowing = FALSE
            CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)
            IF BUILD_TRAILER_SCENE()            
                directorFlowState = dfs_RUN_TRAILER_SCENE       
                
                UNLOAD_ANIMAL_AUDIO_AND_ANIM()
            ENDIF           
            DISPLAY_HUD(FALSE)
            DISPLAY_RADAR(FALSE)
            SET_PED_POPULATION_BUDGET(0)
            SET_VEHICLE_POPULATION_BUDGET(0)
        BREAK
        CASE dfs_RUN_TRAILER_SCENE
            bReturningFromActorMode = TRUE
            //SET_CLOCK_TIME(11,0,0)
            DISPLAY_HUD(FALSE)
            DISPLAY_RADAR(FALSE)
            SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
            SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
            SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0,0.0)
            
            CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)

            IF dirMenuState = dirMENU_MOVE_TO_PLAY_MODE     
                
                IF NOT IS_PED_INJURED(player_ped_id())
                    
                        IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(player_ped_id()))
                            SET_CAMERA_STATE(DIR_CAM_ENTER_PLAY_MODE)
                        ELSE
                            SET_CAMERA_STATE(DIR_CAM_LEADOUT)   
                        ENDIF
                    //ENDIF
                    SET_DIRECTOR_MODE_STATE(dfs_enter_PLAY_MODE)
                    
                    //play a line of dialogue as ped leaves trailer
                    string speechToPlay

                    switch GET_RANDOM_INT_IN_RANGE(0,6)
                        CASE 0 speechToPlay = "GENERIC_YES" BREAK
                        CASE 1 speechToPlay = "GENERIC_THANKS" BREAK
                        CASE 2 speechToPlay = "GENERIC_WHATEVER" BREAK
                    endswitch
                    
                    IF NOT IS_STRING_NULL_OR_EMPTY(speechToPlay)
                        IF DOES_CONTEXT_EXIST_FOR_THIS_PED(player_ped_id(),speechToPlay)
                            PLAY_PED_AMBIENT_SPEECH(player_ped_id(),speechToPlay,SPEECH_PARAMS_FORCE_NORMAL)
                        ENDIF
                    endif
                ENDIF
            ENDIF
            
        BREAK
        CASE dfs_enter_PLAY_MODE
            IF IS_SCREEN_FADED_OUT()
            
                REPLAY_PREVENT_RECORDING_THIS_FRAME()
            
                //Ped shortlists (now adds to recently used list)
                ShortlistPed(activeMenuEntry,displayedMenuData[curMenuItem].index,TRUE,FALSE)
                
                //B* 2525859: Selector
                g_sSelectorUI.iSelectedSlot = -1
    
                //Camera
                SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
                SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
                
                SET_FRONTEND_ACTIVE(FALSE)
                SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE) 
                SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)

                //Zone params
                SET_STATIC_EMITTER_ENABLED("SE_DMOD_Trailer_Radio",false)
                SET_AMBIENT_ZONE_STATE("AZ_DMOD_TRAILER_01",false,true)
                
                REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
                IF IS_IPL_ACTIVE("Replay")
                    REMOVE_IPL("Replay")
                ENDIF
                
                //Reset the PI menu settings
                iStoredSelection = sPI_MenuData.iCurrentSelection
                SET_MENU_CURRENT_POSITION(0)
                iSubMenu = PI_SUBMENU_NONE
                RESET_PI_MENU_NOW()
                
                //set current shorlist menu entry
                selectedPI[PI_M0_SHORTLIST] = GET_PI_SHORTLIST_INDEX_FOR_PLAYER_FROM_SHORTLIST_ARRAY()
                lastSelectedShortlistPed = selectedPI[PI_M0_SHORTLIST] 
                
				CDEBUG1LN(DEBUG_DIRECTOR,"CONTROL_DIRECTOR_MODE_FLOW: Setting g_bDirectorSwitchToCastingTrailer to FALSE")
                g_bDirectorSwitchToCastingTrailer = FALSE       
				
				IF bDoorDataMissing = TRUE
					STORE_AUTO_DOORS_STATES_AND_SET_ALL_UNLOCKED(doorArray, bDoorDataMissing)
				ENDIF
				
                UNLOCK_AUTO_DOORS(bDoorDataMissing)
                
                MODEL_NAMES mnCurrentModel
                mnCurrentModel  = GET_PLAYER_MODEL()
                
                bAllSpecialCharasUnlocked   = ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED()
                bAllStoryModeCharasUnlocked = ARE_ALL_DM_STORY_CHARAS_UNLOCKED()
                bAlmostAllAnimalsUnlocked   = ARE_ALL_DM_ANIMALS_UNLOCKED( TRUE )
                bIsPlayingAsAnAnimal        = IS_MODEL_AN_ANIMAL( mnCurrentModel )
                bIsPlayingAsOnlineChara     = IS_DIRECTOR_PED_AN_ONLINE_CHARA( mnCurrentModel )
                bIsPlayingAsStoryChara      = IS_DIRECTOR_PED_A_STORY_CHARACTER( mnCurrentModel )
                bIsPlayingAsSpecialChara    = IS_DIRECTOR_PED_A_SPECIAL_CHARACTER( mnCurrentModel )
                
                IF GET_PLAYER_MODEL() = IG_CHRISFORMAGE
                    bPlayingAsChrisFormage = TRUE
                ENDIF
                                
                #if IS_DEBUG_BUILD
                    IF bDebugResetAfterWalk
                        SET_DIRECTOR_MODE_STATE(dfs_INIT)
                    else
                        SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                    ENDIF
                #endif
                
                #if IS_FINAL_BUILD              
                    SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                #endif
                
                #if IS_DEBUG_BUILD
                    debug_output_shortlist_data()
                #endif
                            
            ENDIF
        BREAK
        CASE dfs_PLAY_MODE
        
            HIDE_HUD_COMPONENT_THIS_FRAME( NEW_HUD_CASH )
            HIDE_HUD_COMPONENT_THIS_FRAME( NEW_HUD_WEAPON_ICON )
        
            if bEditorActivated
            and not IS_PAUSE_MENU_ACTIVE()
                IF IS_SCREEN_FADED_OUT()
                    DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
                    CHANGE_INVINCIBILITY()      //Set invincibility as per settings
                    SET_PED_CAN_RAGDOLL( PLAYER_PED_ID(), TRUE )
                    bEditorActivated =  false
                endif
            endif
                
            //Only show the HUD scaleform movie if the camera isn't running
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appcamera")) > 0
                IF bScaleformShowing
                    bScaleformShowing = FALSE
                    REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
                ENDIF
            ELSE
                IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DIRECTOR_MODE)
                    IF !bScaleformShowing
                        bScaleformShowing = TRUE
                        BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_DIRECTOR_MODE,"SET_DIRECTOR_MODE_TEXT")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CM_MODE")
                        END_SCALEFORM_MOVIE_METHOD()
                    ENDIF
                ELSE
                    REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
                ENDIF
            ENDIF
            
            //Control garage vehicle creation
            CONTROL_GARAGE_VEHICLE_CREATION()
            
            //  Director mode achievements
            IF IS_SCREEN_FADED_IN()
                IF bAllSpecialCharasUnlocked 
                AND bIsPlayingAsSpecialChara 
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR3 )
                    AWARD_ACHIEVEMENT( ACHR3 )  //  [RBJ_ACH] - 'Humans Of Los Santos' - B* 2407390
                ENDIF
                
                IF NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR4 ) 
                    AWARD_ACHIEVEMENT( ACHR4 )  //  [RBJ_ACH] - 'First Time Director' - B* 2407392
                ENDIF
                
                IF ePlayerAnimalState = PAS_IS_AN_ANIMAL 
                AND bIsPlayingAsAnAnimal 
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR5 )
                    AWARD_ACHIEVEMENT( ACHR5 )  //  [RBJ_ACH] - 'Animal Lover' - B* 2407395
                ENDIF
                        
                IF bAllStoryModeCharasUnlocked 
                AND bIsPlayingAsStoryChara
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR6 )
                    AWARD_ACHIEVEMENT( ACHR6 )  //  [RBJ_ACH] - 'Ensemble Piece' - B* 2407396
                ENDIF
                
                IF bPlayingAsChrisFormage 
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR7 )
                    AWARD_ACHIEVEMENT( ACHR7 )  //  [RBJ_ACH] - 'Cult Movie' - B* 2407404
                ENDIF
                
                IF bAwardLocationUnlockAchievement
                    AWARD_ACHIEVEMENT( ACHR8 )  //  [RBJ_ACH] - 'Location Scout' - B* 2407407
                    bAwardLocationUnlockAchievement = FALSE
                ENDIF
                
                IF bIsPlayingAsOnlineChara 
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR9 )
                    AWARD_ACHIEVEMENT( ACHR9 )  //  [RBJ_ACH] - 'Method Actor' - B* 2407412
                ENDIF
                
                IF bAlmostAllAnimalsUnlocked 
                AND bIsPlayingAsAnAnimal
                AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR10 )
                    AWARD_ACHIEVEMENT( ACHR10 ) //  [RBJ_ACH] - 'Cryptozoologist' - B* 2407418
                ENDIF
            ENDIF
            
            //Functions that need to run every frame
            MAINTAIN_SETTINGS_EVERY_FRAME()
        
            if DOES_ENTITY_EXIST(VehCurrentPersonal)
            and IS_VEHICLE_DRIVEABLE(VehCurrentPersonal)
                IF IS_PED_IN_VEHICLE(player_ped_id(),VehCurrentPersonal)
                    if DOES_BLIP_EXIST(blipVehPersonal)
                        SET_BLIP_DISPLAY(blipVehPersonal,DISPLAY_NOTHING)
                    endif
                else
                    if DOES_BLIP_EXIST(blipVehPersonal)
                        SET_BLIP_DISPLAY(blipVehPersonal,DISPLAY_BLIP)
                    endif
                endif
            else
                IF DOES_BLIP_EXIST(blipVehPersonal)
                    REMOVE_BLIP(blipVehPersonal)
                ENDIF
            endif
            
            //Set the player's wanted rating based on given settings 1/30 frames
            IF GET_FRAME_COUNT() % 30 = 0
                SET_MAX_WANTED_LEVEL(iWantedLvlMax)
                IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < iWantedLvlMin OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > iWantedLvlMax
                    CLEAR_PLAYER_WANTED_LEVEL(Player_id())
                    SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),iWantedLvlMin)
                    SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
                ENDIF
                //B*2425682
                //every 30 frames check player z pos. turn off invincibility if <-190
                CHECK_PLAYER_MAX_DEPTH()
				
				//Check if we've given the current ped model weapons
				IF GET_ENTITY_MODEL(player_ped_id()) != lastModelGivenWeapons
					IF CAN_PLAYER_MODEL_HOLD_WEAPONS()
						CPRINTLN(DEBUG_DIRECTOR, "Current model wasn't given weapons, giving him weapons now!")
						GIVE_PLAYER_DIRECTOR_MODE_WEAPONS()
					ENDIF
				ENDIF
            ENDIF
            
            DISABLE_FIRST_PERSON_VIEW_ON_RESTRICTED_MODELS_THIS_FRAME()
            
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)  
            
            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_CAPTURE_COORDS)
            AND NOT IS_PED_EXITING_ANY_AIRBORN_VEHICLE(PLAYER_PED_ID())
            AND dirCamState = DIR_CAM_PLAY_MODE 
            AND timerSaveCoords <= GET_GAME_TIMER()
                IF NOT IS_PED_INJURED(player_ped_id())
                AND IS_SCREEN_FADED_IN()
                    timerSaveCoords = GET_GAME_TIMER() + 1000
                    vLastPlayerCoord = GET_ENTITY_COORDS(player_ped_id())
                    fPlayerHeading = GET_ENTITY_HEADING(player_ped_id())
                    //CDEBUG2LN(debug_director, "Captured last player coord at ",vLastPlayerCoord)//commented out this is 
                ENDIF
            ENDIF
            
            IF GET_PLAYER_MODEL() = S_M_M_MOVALIEN_01
            OR GET_PLAYER_MODEL() = U_M_Y_POGO_01
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
            ENDIF   
        BREAK
        
        CASE dfs_swap_player_model

            IF sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SHORTLIST)
                SET_PLAYER_CONTROL(player_id(),FALSE)
                SET_PLAYER_INVINCIBLE(player_id(),TRUE)
                SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)
                RESET_PI_MENU_NOW()
                DISABLE_FIRST_PERSON_VIEW_ON_RESTRICTED_MODELS_THIS_FRAME() //B* 2417714
                
                IF GET_CAMERA_STATE() = DIR_CAM_PLAY_MODE
                    
                    CLEAR_DM_MENU_DATA()
                    PImenuON = FALSE
                    bMenuAssetsLoaded = FALSE
                    CLEANUP_PI_MENU_MINI_MAP()
                    CLEANUP_MENU_ASSETS()
                    SET_CAMERA_STATE(DIR_CAM_GAME_FADE_OUT)
                ENDIF
                
                IF GET_CAMERA_STATE() = DIR_CAM_GAME_FADED_OUT
                    aLeadoutTimer = GET_GAME_TIMER() + 1000
                    IF (sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SHORTLIST) AND selectedPI[sPI_MenuData.iCurrentSelection] = lastSelectedShortlistPed)
                        SET_CAMERA_STATE(DIR_CAM_FADE_IN_FROM_WARP)
                        SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)  
                        PImenuON = TRUE
                        navSwap = FALSE
                        RESTRICT_ACTION_ENTRY(selectedPI[PI_M0_GESTURE])                    
                        SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)   
                        SET_PLAYER_CONTROL(player_id(),TRUE)
                        CLEAR_PED_WETNESS(PLAYER_PED_ID())
                        CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
                        RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
                        SET_ENTITY_HEALTH(player_ped_id(),GET_ENTITY_MAX_HEALTH(player_ped_id()))               
                        CHANGE_INVINCIBILITY() //SET_PLAYER_INVINCIBLE(player_id(),FALSE)
                        
                    ENDIF
                ENDIF  
            ELSE
                SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                PImenuON    = TRUE
                navSwap     = FALSE
            ENDIF
        BREAK
        
        CASE dfs_warp_to_location
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            SET_PLAYER_CONTROL(player_id(),FALSE)
            SET_PLAYER_INVINCIBLE(player_id(),TRUE)
            SET_PED_CAN_RAGDOLL( PLAYER_PED_ID(), FALSE )
            
            IF GET_CAMERA_STATE() = DIR_CAM_PLAY_MODE
                SET_CAMERA_STATE(DIR_CAM_GAME_FADE_OUT)
            ENDIF
            IF GET_CAMERA_STATE() = DIR_CAM_GAME_FADED_OUT
                            
                    cprintln(DEBUG_DIRECTOR,"Warp location = ",warpLocations[iWarpLocation].position," bike = ",warpLocations[iWarpLocation].bikePos)                               
                    
                    #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
                        //Snap to ground if warping to waypoint
                        IF iWarpLocation = ENUM_TO_INT(DTL_WAYPOINT)
                            FLOAT fGroundZ
                            warpLocations[iWarpLocation].position.z = 1000
                            GET_GROUND_Z_FOR_3D_COORD(warpLocations[iWarpLocation].position,fGroundZ)
                            INT iTries 
                            WHILE fGroundZ = 0.0 AND iTries < 10
                                DO_PLAYER_TELEPORT(warpLocations[iWarpLocation].position, warpLocations[iWarpLocation].heading, FALSE)
                                GET_GROUND_Z_FOR_3D_COORD(warpLocations[iWarpLocation].position,fGroundZ)
                                CDEBUG1LN(debug_director,"Waypoint warp: try ",iTries," found ground z ",fGroundZ, " at ",warpLocations[iWarpLocation].position)
                                warpLocations[iWarpLocation].position.z -= 100
                                iTries++
                            ENDWHILE
                            IF fGroundZ > 0.0
                                warpLocations[iWarpLocation].position.z = fGroundZ
                            ELSE
                                warpLocations[iWarpLocation].position.z = 0
                            ENDIF
                        ENDIF
                    #ENDIF
                    
                    IF NOT IS_PLAYER_TELEPORT_ACTIVE()
                        //Teleport player instead of map_warp_with_load
                        DO_PLAYER_TELEPORT(warpLocations[iWarpLocation].position, warpLocations[iWarpLocation].heading, FALSE,true,20000)
                    ENDIF
                    
                    //check for bike
                    IF DOES_ENTITY_EXIST(vehSanchez)
                        DELETE_VEHICLE(vehSanchez)
                    ENDIF
                    
                    IF !IS_VECTOR_ZERO(warpLocations[iWarpLocation].bikePos)
                                            
                        REQUEST_MODEL(warpLocations[iWarpLocation].model)
                        WHILE NOT HAS_MODEL_LOADED(warpLocations[iWarpLocation].model)
                            WAIT(0)
                        ENDWHILE
                                                                
                        vehSanchez = CREATE_VEHICLE(warpLocations[iWarpLocation].model,warpLocations[iWarpLocation].bikePos,warpLocations[iWarpLocation].bikeRot)
                            
                        SET_MODEL_AS_NO_LONGER_NEEDED(warpLocations[iWarpLocation].model)
                        
                    ENDIF
                    
                    iWarpStage = 0
                    vLastPlayerCoord = warpLocations[iWarpLocation].position
                    vRespawnCoords = GET_CLOSEST_SAFE_POINT_FOR_PLAYER(vLastPlayerCoord,false,DEFAULT,IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
                    //get actual ground coords for respawn point not just 0
                    vector vRespawnGround 
                    vRespawnGround = vRespawnCoords 
                    vRespawnGround.z += 50
                    float fRespawnGround
                    GET_GROUND_Z_FOR_3D_COORD(vRespawnGround,fRespawnGround)
                    vRespawnCoords.z = fRespawnGround
                    
                    shapeTestRespawn = null
                    shpTestChestHeight = null
                    shpTestFloor = null
                    cprintln(DEBUG_DIRECTOR,"player heading after warp = ",GET_ENTITY_HEADING(player_ped_id()))
                    SET_CAMERA_STATE(DIR_CAM_MAP_WARP_REPOSITION)
                
            ELSE
                //this nested in ELSE to ensure it happens the following frame, as otherwise the shapetest weren't working.
                IF GET_CAMERA_STATE() = DIR_CAM_MAP_WARP_REPOSITION     
                
                    IF warpLocations[iWarpLocation].interior
                        IF GET_INTERIOR_AT_COORDS(warpLocations[iWarpLocation].position) = NULL 
                        OR NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(warpLocations[iWarpLocation].position))
                            bForceRoadNode = TRUE
                        ENDIF
                    ENDIF
                    
                    MODEL_NAMES eModelToCheck
                    eModelToCheck = GET_ENTITY_MODEL(player_ped_id())
                    //If player is in a vehicle, use the vehicle model for repositioning
                    IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) <> NULL
                        eModelToCheck = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
                        bForceRoadNode = TRUE
                    ENDIF
                    
            
                    IF PERFORM_SHAPETEST_REPOSITION(warpLocations[iWarpLocation].position,eModelToCheck,vRespawnCoords,
                                                    warpLocations[iWarpLocation].heading,bPassShapetest,bRespawnPass,true,true,bForceRoadNode)
                    
                        IF !bQuickTravelErrorHelpShown
                            IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),warpLocations[iWarpLocation].position) > 1.0
                                bQuickTravelErrorHelpShown = TRUe
                                PRINT_HELP("CM_HLPTCO")                     
                            ENDIF
                        ENDIF
                    
                        SET_GAMEPLAY_CAM_RELATIVE_PITCH(warpLocations[iWarpLocation].camRelativePitch)
                        SET_GAMEPLAY_CAM_RELATIVE_HEADING(warpLocations[iWarpLocation].camRelativeHeading)
                        aLeadoutTimer = GET_GAME_TIMER() + 1000
                        SET_CAMERA_STATE(DIR_CAM_FADE_IN_FROM_WARP)
                        SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                        SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
                        SET_PLAYER_CONTROL(player_id(),TRUE)
                        CHANGE_INVINCIBILITY()//SET_PLAYER_INVINCIBLE(player_id(),FALSE)
                        IF DOES_ENTITY_EXIST(vehSanchez)
                            //IF IS_VEHICLE_DRIVEABLE(vehSanchez)
                                SET_VEHICLE_AS_NO_LONGER_NEEDED(vehSanchez)
                            //ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        BREAK
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        CASE dfs_query_prop_scene_clear
        
            cprintln(debug_director,"dfs_query_prop_scene_clear")
            
//            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
//            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            SET_WARNING_MESSAGE_WITH_HEADER( "CM_EWARN", "CM_DELSCENE", FE_WARNING_YESNO )
        
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
            IF bInPropEditorWarningScreen
                IF navPropEditorAccept
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    
                    //Delete all objects
					CLEANUP_DM_PROPS()	//B* 3981547 - full cleanup of prop editor
                    CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0)
                    DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
                    
//                  INT iIndex
//                  INT iMenuEntries
//                  iMenuEntries    = DMPROP_GET_NUM_MENU_ENTRIES()
//                  FOR iIndex = 0 TO iMenuEntries - ciPROPADDLINES - 1
//                      DMPROP_DELETE_PROP( 0 )
//                  ENDFOR
//                          
//                          SET_TOP_MENU_ITEM( 0 )
//                          IF sPI_MenuData.iCurrentSelection >= ciPROPADDLINES
//                              sPI_MenuData.iPreviousSelection = sPI_MenuData.iCurrentSelection
//                              sPI_MenuData.iCurrentSelection = 0
//                          ENDIF
//                          
                    SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorAccept         = FALSE
                    //navClearScene               = FALSE
                    bResetMenuNow               = TRUE
                ELIF navPropEditorBack
                    SET_GAME_PAUSED( FALSE )
                    DO_SCREEN_FADE_IN(0)
                    PLAY_SOUND_FRONTEND( -1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorBack           = FALSE
                   // navClearScene               = FALSE
                ENDIF
            ELIF IS_WARNING_MESSAGE_ACTIVE()
                DO_SCREEN_FADE_OUT(0)
                SET_GAME_PAUSED( TRUE )
                bInPropEditorWarningScreen = TRUE
            ENDIF
            
        BREAK
        CASE dfs_query_overwrite_scene
        
            CPRINTLN( debug_director, "dfs_query_overwrite_scene" )
            
//            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
//            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            SET_WARNING_MESSAGE_WITH_HEADER( "CM_EWARN", "CM_OVRWTSCN", FE_WARNING_YESNO )
        
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
            
            IF bInPropEditorWarningScreen
                IF navPropEditorAccept
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    DMPROP_SAVE_SCENE( iSelectedScene )
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorAccept         = FALSE
                    bResetMenuNow               = TRUE
                ELIF navPropEditorBack
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorBack           = FALSE
                ENDIF
            ELIF IS_WARNING_MESSAGE_ACTIVE()
                cprintln(debug_director, "Setting paused and in warning screen.")
                SET_GAME_PAUSED( TRUE )
                bInPropEditorWarningScreen = TRUE
            ENDIF
            
        BREAK
        
        CASE dfs_query_load_scene
        
            CPRINTLN( debug_director, "dfs_query_load_scene" )
//            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
//            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            IF iCurrentScene = iSelectedScene
                SET_WARNING_MESSAGE_WITH_HEADER( "CM_EWARN", "CM_RLDSCN", FE_WARNING_YESNO )
            ELSE
                SET_WARNING_MESSAGE_WITH_HEADER( "CM_EWARN", "CM_LOADSCN", FE_WARNING_YESNO )
            ENDIF
        
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
//            CPRINTLN(debug_dan,"In query, navaccept = ",navPropEditorAccept, " navBack = ",navPropEditorBack)
            
            IF bInPropEditorWarningScreen
                IF navPropEditorAccept
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    DMPROP_LOAD_SCENE( sPI_MenuData.iCurrentSelection )
                    DMPROP_SET_STATE(PES_SceneEdit)
                    
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorAccept         = FALSE
                    navPropEditorBack           = FALSE
                    bResetMenuNow               = TRUE
                ELIF navPropEditorBack
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorBack           = FALSE
                    navPropEditorAccept         = FALSE
                ENDIF
            ELIF IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
                bInPropEditorWarningScreen = TRUE
            ENDIF
            
        BREAK
        
        CASE dfs_warp_player_to_prop 
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())  //No longer allowing warping whilst in a vehicle.
                IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
                    IF ARE_STRINGS_EQUAL( sPropEditorHelpString, "DMPP_HLP_PLCD2") //Any other help string means the warp has been disabled. 2506996
                        CPRINTLN( debug_director, "dfs_warp_player_to_prop" )
                        DMPROP_WARP_TO_SELECTED_PROP( DMPROP_GET_NTH_PROP( sPI_MenuData.iCurrentSelection ) )
                        SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    ENDIF 
                ENDIF
            ENDIF               
        BREAK
        
        CASE dfs_query_exit_scene
        
            CPRINTLN( debug_director, "dfs_query_exit_scene" )

            SET_WARNING_MESSAGE_WITH_HEADER( "CM_EWARN", "CM_EXITSCN", FE_WARNING_YESNO )
        
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN()
            
            IF bInPropEditorWarningScreen
                IF navPropEditorAccept
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    //Load old settings settings here
                    TOGGLE_PROP_EDITOR(FALSE)
                    iSubMenu = PI_SUBMENU_NONE    
                    SET_MENU_CURRENT_POSITION(iStoredSelection)
                    CONTROL_RETURN_MENU_TOP_MENU_ITEM()
                    
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorAccept         = FALSE
                    navPropEditorBack           = FALSE
                    RESET_PI_MENU_NOW()        
                ELIF navPropEditorBack
                    SET_GAME_PAUSED( FALSE )
                    PLAY_SOUND_FRONTEND( -1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                    
                    SET_DIRECTOR_MODE_STATE( dfs_PLAY_MODE )
                    bInPropEditorWarningScreen  = FALSE
                    navPropEditorBack           = FALSE
                    navPropEditorAccept         = FALSE
                ENDIF
            ELIF IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
                bInPropEditorWarningScreen = TRUE
            ENDIF
            
        BREAK
    #ENDIF
        CASE dfs_shortlist_full_query
            
            cprintln(debug_director,"QUERY SHORTLIST")
            
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            IF NOT bPaused
            AND IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
            ENDIF
            

            
            feWarningFlags = FE_WARNING_OKCANCEL
            SET_WARNING_MESSAGE_WITH_HEADER("CM_EWARN", "CM_SHODEL", feWarningFlags)
            
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN() // B* 2335471
            
            IF navAccept    
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_DIRECTOR_MODE_STATE(dfs_RUN_TRAILER_SCENE)
                nudgeShortlistUp(false)
                REMOVE_SHORTLIST_ENTRY(9)
                pedJustShortlisted = TRUE
                ShortlistPed(activeMenuEntry,activeMenuIndex)
                REBUILD_HELP_KEYS()
                navAccept = false
                
                //replace oldest shortlist ped with current one
            ELIF navBack
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                navBack = false
                PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_DIRECTOR_MODE_STATE(dfs_RUN_TRAILER_SCENE)
            ENDIF   
        BREAK
        CASE dfs_query_editor
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            IF NOT bPaused
            AND IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
            ENDIF
            
            feWarningFlags = FE_WARNING_OKCANCEL
            SET_WARNING_MESSAGE_WITH_HEADER("CM_EWARN", "CM_EDIT", feWarningFlags)
            
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN() // B* 2335471
            
            IF navAccept   
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE 
                PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),TRUE)         
                ENDIF           
                SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                ACTIVATE_ROCKSTAR_EDITOR(EOFS_SCRIPT_DIRECTOR)
                bEditorActivated    =   TRUE
                navAccept           =   FALSE
                CPRINTLN(DEBUG_MISSION,"CONTROL_MENU: dfs_query_editor navAccept true ")
            ELIF navBack
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                navBack = false
                PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
            ENDIF           
                            
        BREAK
        CASE dfs_query_trailer
            //Bypass all checks if launched from the pause menu
            IF g_bDirectorSwitchToCastingTrailerFromCode
            OR GET_CONFIRM_INVITE_INTO_GAME_STATE()
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),TRUE)
                ENDIF
                SET_DIRECTOR_MODE_STATE(dfs_INIT)
				CDEBUG1LN(DEBUG_DIRECTOR,"CASE dfs_query_trailer: setting g_bDirectorSwitchToCastingTrailerFromCode to FALSE")
                g_bDirectorSwitchToCastingTrailerFromCode = FALSE
                BREAK
            ENDIF
        
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            
            IF NOT bPaused
            AND IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
            ENDIF
            
            feWarningFlags = FE_WARNING_OKCANCEL
            SET_WARNING_MESSAGE_WITH_HEADER("CM_EWARN", "CM_TRAILER", feWarningFlags)       
            
            OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN() // B* 2335471
            
            IF navAccept    
            OR bBypassTrailerQuery
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),TRUE)
                ENDIF
                IF navAccept
                    PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                ENDIF
                SET_DIRECTOR_MODE_STATE(dfs_INIT)
                CPRINTLN(DEBUG_MISSION,"CONTROL_MENU: dfs_query_trailer navAccept true ")   
                navAccept           =   FALSE
                bBypassTrailerQuery = FALSE
            ELIF navBack
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                navBack = false
                PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_DIRECTOR_MODE_STATE(dfs_PLAY_MODE)
                IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
                    DO_SCREEN_FADE_IN(0)
                ENDIF
            ENDIF
        BREAK
        CASE dfs_query_terminate
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()        
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()  
            
            IF NOT bPaused
            AND IS_WARNING_MESSAGE_ACTIVE()
                SET_GAME_PAUSED( TRUE )
            ENDIF
            
            feWarningFlags = FE_WARNING_YESNO
            SET_WARNING_MESSAGE_WITH_HEADER("CM_EWARN", "CM_EXI", feWarningFlags)
            
            IF bWaitBeforeInput                                     // B* 2447284 - Need to wait a frame for message to show.
                OVERRIDE_DEFAULT_NAV_INPUT_CHECK_FOR_ALERT_SCREEN() // B* 2335471
            ELSE
                bWaitBeforeInput = TRUE
            ENDIF
            
            IF navAccept    
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                SET_DIRECTOR_MODE_STATE(dfs_request_terminate)
                navAccept           =   FALSE
                bWaitBeforeInput    =   FALSE
                SET_ENTITY_INVINCIBLE( PLAYER_PED_ID(), TRUE )
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
                IF directorFlowStateBeforeQuery = dfs_PLAY_MODE
                    DO_SCREEN_FADE_OUT( 0 )
                ENDIF
                CPRINTLN(DEBUG_MISSION,"dfs_query_terminate navAccept true ") 											
            ELIF navBack
                SET_GAME_PAUSED( FALSE )
                bPaused = FALSE
                bWaitBeforeInput = FALSE
                IF dirCurrentMenu != MC_MAIN
                    PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    IF NOT PImenuON AND directorFlowStateBeforeQuery <> dfs_PLAY_MODE
                        REBUILD_MENU(dirCurrentMenu,TRUE)
                    ENDIF
                ENDIF   
                SET_DIRECTOR_MODE_STATE(directorFlowStateBeforeQuery)
            ENDIF
        BREAK
        CASE dfs_request_terminate
            DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()//DISABLE_FIRST_PERSON_CAMS()
            DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
            IF GET_CAMERA_STATE() != DIR_CAM_GAME_FADED_OUT
                SET_RAIN(fRainLevel)
                SET_CAMERA_STATE(DIR_CAM_GAME_FADE_OUT)
                bIsPlayingAsAnAnimal            = FALSE
                bPlayingAsChrisFormage          = FALSE
                bIsPlayingAsOnlineChara         = FALSE
                bIsPlayingAsStoryChara          = FALSE
                bIsPlayingAsSpecialChara        = FALSE
            ELSE
                SET_DIRECTOR_MODE_STATE(dfs_terminated)
#IF FEATURE_GEN9_STANDALONE
				IF eExitMode != SRCM_INVALID 
					CPRINTLN(DEBUG_MISSION,"dfs_request_terminate use script router")
					// Set script router link for return to story mode
					SCRIPT_ROUTER_CONTEXT_DATA srcData
					srcData.eSource = SRCS_SCRIPT
					srcData.eMode = eExitMode
					srcData.eArgType = SRCA_NONE
					SET_SCRIPT_ROUTER_LINK(srcData)
				ENDIF
#ENDIF // FEATURE_GEN9_STANDALONE
            ENDIF
        BREAK
    ENDSWITCH   
    
    //cprintln(DEBUG_DIRECTOR,"selectedPI[3] = ", selectedPI[3])
    IF selectedPI[PI_M0_SPEECH] != 0
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
    ENDIF
            
    IF bAnimalRestrictedState     
        Update_Animal_Restricted_State(animalRestrictedModel,wantedTimer)
    ENDIF
ENDPROC

PROC SWAP_PLAYER_MODEL(MODEL_NAMES newModelToLoad, vector vSpecificSpawnPoint, CrewMember heistCrew = CM_EMPTY,INT iOnlineChar = -1)
    cprintln(debug_director,"SWAP_PLAYER_MODEL() newModelToLoad = ",newModelToLoad)
    IF NOT IS_PED_INJURED(player_ped_id())
        if GET_ENTITY_MODEL(player_ped_id()) != newModelToLoad 
        OR heistCrew != CM_EMPTY
        or iOnlineChar != -1
            vector vSpawn
            float fHeading
            
            IF IS_VECTOR_ZERO(vSpecificSpawnPoint)
                GET_SPAWN_POSITIONS(newModelToLoad,vSpawn,fHeading)
            ELSE
                vSpawn = vSpecificSpawnPoint
            ENDIF
            
            SWITCH GET_ENTITY_MODEL(player_ped_id())
                CASE PLAYER_ZERO
                FALLTHRU
                CASE PLAYER_ONE
                FALLTHRU
                CASE PLAYER_TWO
                    IF IS_SPECIAL_ABILITY_ACTIVE( PLAYER_ID() )
                        SPECIAL_ABILITY_DEACTIVATE_FAST( PLAYER_ID() )
                    ENDIF
                BREAK
            ENDSWITCH

            vehicle_index playerVehicle
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                playerVehicle = GET_VEHICLE_PED_IS_IN(player_ped_id())
            ENDIF
            
            IF heistCrew = CM_EMPTY
            
                SWITCH newModelToLoad
                    CASE PLAYER_ZERO
                        swapToPed = CREATE_PED(PEDTYPE_PLAYER1,newModelToLoad,vSpawn,fHeading)

                    BREAK
                    CASE PLAYER_ONE
                        swapToPed = CREATE_PED(PEDTYPE_PLAYER2,newModelToLoad,vSpawn,fHeading)

                    BREAK
                    CASE PLAYER_TWO
                        swapToPed = CREATE_PED(PEDTYPE_PLAYER_UNUSED,newModelToLoad,vSpawn,fHeading)

                    BREAK                           
                    DEFAULT                     
                        swapToPed = CREATE_PED(PEDTYPE_MISSION,newModelToLoad,vSpawn,fHeading)      
                        switch newModelToLoad
                            CASE IG_AMANDATOWNLEY
                            CASE IG_BEVERLY
                            CASE IG_BRAD    
                            CASE IG_CHRISFORMAGE                
                            CASE IG_DAVENORTON
                            CASE IG_DEVIN
                            CASE IG_DENISE  
                            CASE IG_DRFRIEDLANDER
                            CASE IG_FABIEN
                            CASE IG_FLOYD
                            CASE IG_JIMMYDISANTO
                            CASE IG_LAMARDAVIS
                            CASE IG_LAZLOW
                            CASE IG_LIFEINVAD_01 //RICKIE?
                            CASE IG_LESTERCREST
                            CASE IG_MAUDE 
                            CASE IG_MRS_THORNHILL
                            case IG_NERVOUSRON
                            CASE IG_PATRICIA    
                            case IG_SIEMONYETARIAN
                            case IG_SOLOMON
                            CASE IG_STEVEHAINS
                            CASE IG_STRETCH
                            CASE IG_TAOCHENG
                            CASE IG_TANISHA
                            CASE IG_TRACYDISANTO
                            CASE IG_WADE                                                
                            CASE U_M_Y_ZOMBIE_01 
                            CASE U_F_O_MOVIESTAR 
                            CASE S_M_Y_MIME 
                            CASE U_M_Y_MANI 
                            CASE u_m_m_jesus_01 
                            CASE U_M_O_TAPHILLBILLY 
                            CASE S_M_M_StrPreach_01 
                            CASE U_F_Y_COMJane 
                            CASE U_M_Y_IMPORAGE 
                            CASE U_M_M_GRIFF_01 
                            CASE U_M_Y_MILITARYBUM 
                            CASE U_M_O_FinGuru_01 
                            CASE U_M_Y_BAYGOR 
                            CASE U_M_Y_Hippie_01
                                SET_CHAR_OUTFIT(swapToPed,GET_CHAR_OUTFIT_DEFAULT(newModelToLoad),sOutfitsData)         
                            BREAK
                            
                            CASE MP_M_FREEMODE_01
                            CASE MP_F_FREEMODE_01
                                if iOnlineChar != -1
                                    DIRECTOR_CHAR_OUTFITS eoutfit
                                    if iOnlineChar = 0
                                        eOutfit = DCO_MULTIPLAYER_1
                                    elif iOnlineChar = 1 
                                        eOutfit = DCO_MULTIPLAYER_2
                                    endif
                                    IF sOnlineChar[iOnlineChar].bSetup // If mp data is set up use cached instead of stats                                      
                                        SET_CHAR_OUTFIT(swapToPed,eOutfit,sOnlineChar[iOnlineChar].outfitdata,false)
                                    else
                                        SET_CHAR_OUTFIT(swapToPed,eOutfit,sOutfitsData) 
                                        sOnlineChar[iOnlineChar].outfitdata = sOutfitsData
                                        sOnlineChar[iOnlineChar].bSetup = true
                                    endif       
                                endif
                            BREAK
                            
                            //Special case for the prisoner model, default has hat and hair clipping through it
                            CASE S_M_Y_PRISONER_01
                                SET_PED_DEFAULT_COMPONENT_VARIATION(swapToPed)
                                SET_MODEL_VARIATION(swapToPed,0,0,2)
                                SET_TEXTURE_VARIATION(swapToPed,0)
                            BREAK
                            
                            DEFAULT
                                SET_PED_DEFAULT_COMPONENT_VARIATION(swapToPed)
                                SET_TEXTURE_VARIATION(swapToPed,0)
                            BREAK                           
                        ENDSWITCH
                        
                    BREAK
                ENDSWITCH   
            ELSE
            
                cprintln(debug_director," heistCrew = ",heistCrew," lastSelectedHeistMember = ",lastSelectedHeistMember)
                HeistCrewOutfit crewOutfit
                
                SWITCH GET_CREW_MEMBER_TYPE(heistCrew)
                    CASE CMT_GUNMAN
                        IF heistCrew = lastSelectedHeistMember
                            SWITCH iGunmanOutfit
                                CASE 0 
                                    crewOutfit = CREW_OUTFIT_DEFAULT    
                                    iGunmanOutfit++
                                BREAK
                                CASE 1 
                                    crewOutfit = CREW_OUTFIT_PEST_CONTROL   
                                    iGunmanOutfit++
                                BREAK
                                CASE 2 
                                    crewOutfit = CREW_OUTFIT_SCUBA     
                                    iGunmanOutfit++
                                BREAK
                                CASE 3 
                                    crewOutfit = CREW_OUTFIT_SCUBA_LAND      
                                    iGunmanOutfit++
                                BREAK
                                CASE 4 
                                    crewOutfit = CREW_OUTFIT_HEAVY_ARMOR  
                                    iGunmanOutfit++
                                BREAK
                                CASE 5 
                                    crewOutfit = CREW_OUTFIT_FIRE_FIGHTING  
                                    iGunmanOutfit++
                                BREAK
                                CASE 6 
                                    crewOutfit = CREW_OUTFIT_STEALTH 
                                    iGunmanOutfit++
                                BREAK
                                CASE 7 
                                    crewOutfit = CREW_OUTFIT_SECURITY   
                                    iGunmanOutfit++
                                BREAK
                                CASE 8 
                                    crewOutfit = CREW_OUTFIT_SUIT  
                                    iGunmanOutfit = 0
                                BREAK
                            ENDSWITCH                   
                        ELSE
                            iGunmanOutfit = 1 
                            crewOutfit = CREW_OUTFIT_DEFAULT
                        ENDIF
                    BREAK
                    CASE CMT_HACKER                     
                        IF heistCrew = lastSelectedHeistMember                            
                            SWITCH iHackerOutFit
                                CASE 0 
                                    crewOutfit = CREW_OUTFIT_DEFAULT 
                                    iHackerOutFit++
                                BREAK
                                CASE 1 
                                    crewOutfit = CREW_OUTFIT_STEALTH 
                                    iHackerOutFit = 0
                                BREAK
                            ENDSWITCH
                        ELSE
                            iHackerOutFit = 1
                            crewOutfit = CREW_OUTFIT_DEFAULT
                        ENDIF                       
                    BREAK
                    CASE CMT_DRIVER                 
                        IF heistCrew = lastSelectedHeistMember
                            SWITCH iDriverOutfit
                                CASE 0 
                                    crewOutfit = CREW_OUTFIT_DEFAULT 
                                    iDriverOutfit++
                                BREAK
                                CASE 1 
                                    crewOutfit = CREW_OUTFIT_PEST_CONTROL
                                    iDriverOutfit++
                                BREAK
                                CASE 2 
                                    crewOutfit = CREW_OUTFIT_STEALTH 
                                    iDriverOutfit++
                                BREAK
                                CASE 3 
                                    crewOutfit = CREW_OUTFIT_SECURITY 
                                    iDriverOutfit++
                                BREAK
                                CASE 4 
                                    crewOutfit = CREW_OUTFIT_SUIT 
                                    iDriverOutfit =0
                                BREAK
                            ENDSWITCH
                        ELSE
                            iDriverOutfit = 1
                            crewOutfit = CREW_OUTFIT_DEFAULT
                        ENDIF                   
                    BREAK
                    DEFAULT
                        crewOutfit = CREW_OUTFIT_DEFAULT
                    BREAK
                ENDSWITCH
                
                swapToPed = CREATE_HEIST_CREW_MEMBER(heistCrew,vSpawn,fHeading,crewOutfit)
                IF GET_CREW_MEMBER_TYPE(heistCrew) = CMT_GUNMAN
                    IF crewOutfit = CREW_OUTFIT_SCUBA
                        cprintln(debug_director," SET_PED_PROP_INDEX() d")
                        SET_PED_PROP_INDEX(swapToPed, INT_TO_ENUM(PED_PROP_POSITION,2), 0, 0)
                        SET_PED_PROP_INDEX(swapToPed, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
                        
                    ENDIF
                ENDIF
                SET_PED_COMPONENT_VARIATION(swapToPed,PED_COMP_BERD, 1, 0, 0)
                
                IF heistCrew = CM_DRIVER_G_TALINA_UNLOCK
                    IF GET_PED_DRAWABLE_VARIATION(swapToPed,PED_COMP_HAIR) = 0
                        SET_PED_COMPONENT_VARIATION(swapToPed,PED_COMP_HAIR, 2, 0, 0)
                    ENDIF
                ENDIF
                lastSelectedHeistMember = heistCrew
            ENDIF

            float fRelPitch,fRelHeading
            
            
            fRelHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
            fRelPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
            
            CHANGE_PLAYER_PED(player_id(),swapToPed)
            
            SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelHeading)
            SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelPitch)
            
            IF DOES_ENTITY_EXIST(playerVehicle)
                IF IS_VEHICLE_DRIVEABLE(playerVehicle)
                    IF IS_PED_VALID_FOR_VEHICLE(player_ped_id())
                        SET_PED_INTO_VEHICLE(player_ped_id(),playerVehicle)
                    ELSE
                        IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(playerVehicle)
                            SET_ENTITY_AS_MISSION_ENTITY(playerVehicle)
                            DELETE_VEHICLE(playerVehicle)
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
            //If police, army, medic, fireman B*-2187730, 2175947
            SWITCH newModelToLoad
                CASE S_F_Y_Cop_01
                CASE S_M_M_CIASec_01
                CASE S_M_M_FIBOffice_01
                CASE S_M_M_Paramedic_01
                CASE S_M_Y_Cop_01
                CASE S_M_Y_Fireman_01
                CASE S_M_Y_Swat_01      
                CASE S_M_M_Marine_01
                CASE S_M_Y_Marine_01
                CASE S_M_Y_Marine_02
                CASE S_M_Y_Marine_03
                CASE S_M_M_Marine_02
                CASE S_M_Y_BlackOps_01
                CASE S_M_Y_BlackOps_02
                CASE S_M_Y_ArmyMech_01
                CASE S_M_Y_USCG_01
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_IgnorePedTypeForIsFriendlyWith,TRUE)
                    REMOVE_COP_BLIP_FROM_PED(PLAYER_PED_ID())
                BREAK
                DEFAULT
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_IgnorePedTypeForIsFriendlyWith,FALSE)
                BREAK
            ENDSWITCH           
            
            IF directorFlowState != dfs_PLAY_MODE

                SET_ENTITY_COORDS(PLAYER_PED_ID(),vSpawn)
                SET_ENTITY_HEADING(PLAYER_PED_ID(),fHeading)
            ENDIF
            
            DISABLE_DIRECTOR_MODE_SETTINGS_CHEATS(FALSE)
            
            //is an animal
            IF IS_MODEL_AN_ANIMAL(newModelToLoad)
                ePlayerAnimalState = PAS_IS_AN_ANIMAL
                Activate_Animal_Restricted_State(newModelToLoad,DEFAULT,FALSE) 
                bAnimalRestrictedState = TRUE
                animalRestrictedModel =newModelToLoad
            ELSE
                ePlayerAnimalState = PAS_IS_HUMAN
                bAnimalRestrictedState = FALSE
                Deactivate_Animal_Restricted_State()
                //Re-disable stunt jumps
                SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
            ENDIF
            
            IF NOT IS_PED_VALID_FOR_VEHICLE(player_ped_id())
                DISABLE_TAXI_HAILING(TRUE)
            ENDIF
            
            DISABLE_UNSAFE_DIRECTOR_MODE_CHEATS(newModelToLoad)
            
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DontBlipCop,TRUE)
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_PreventAllMeleeTaunts,TRUE) //bug2436896
            SET_PED_RESET_FLAG(player_ped_id(),PRF_SkipOnFootIdleIntro, TRUE)
            FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_IDLE)
            FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
            FORCE_INSTANT_LEG_IK_SETUP(PLAYER_PED_ID())
            
            
            
            CPRINTLN(DEBUG_DIRECTOR,"swap_player_model(): UPDATE VOICE")
            UPDATE_PLAYER_VOICE()           
            UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
            REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
            REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
        
            
            //set new player blip colour as appropriate
            SWITCH newModelToLoad
                CASE PLAYER_ZERO //michael
                    SET_PLAYER_ICON_COLOUR(BLIP_COLOUR_MICHAEL)
                BREAK
                CASE PLAYER_ONE
                    SET_PLAYER_ICON_COLOUR(BLIP_COLOUR_FRANKLIN)
                BREAK
                CASE PLAYER_TWO
                    SET_PLAYER_ICON_COLOUR(BLIP_COLOUR_TREVOR)
                BREAK                   
                DEFAULT                     
                    SET_PLAYER_ICON_COLOUR(BLIP_COLOUR_DEFAULT)                                                                                                             
                BREAK
            ENDSWITCH
            
            bIsPlayingAsAnAnimal        = FALSE
            bPlayingAsChrisFormage      = FALSE
            bIsPlayingAsOnlineChara     = FALSE
            bIsPlayingAsStoryChara      = FALSE
            bIsPlayingAsSpecialChara    = FALSE
            
            MODEL_NAMES mnCurrentModel  = GET_PLAYER_MODEL()
            bIsPlayingAsAnAnimal        = IS_MODEL_AN_ANIMAL( mnCurrentModel )
            bIsPlayingAsOnlineChara     = IS_DIRECTOR_PED_AN_ONLINE_CHARA( mnCurrentModel )
            bIsPlayingAsStoryChara      = IS_DIRECTOR_PED_A_STORY_CHARACTER( mnCurrentModel )
            bIsPlayingAsSpecialChara    = IS_DIRECTOR_PED_A_SPECIAL_CHARACTER( mnCurrentModel )
            
            IF GET_PLAYER_MODEL() = IG_CHRISFORMAGE
                bPlayingAsChrisFormage = TRUE
            ENDIF
            
            
            REQUEST_PI_MENU_REBUILD()
            lastDirectionPress = 0
        ENDIF
    ENDIF
ENDPROC




PROC CONTROL_PLAYER_MODEL_SWAP()
    //check if player is on one of the character select menus
    

    
    //handle model loading/releasing
    //IF dirCurrentMenu = MC_MAIN
        //if in main menu, start loading model assets for whichever menu option you're hovered over
    //ENDIF
    

    IF HAS_PLAYER_SELECTED_A_NEW_MENU_ACTOR()
        SET_LOAD_NEW_MODEL_VARIATION()
    ENDIF
    
    UPDATE_LOADED_MODEL()
    
    IF directorFlowState = dfs_swap_player_model
        cprintln(debug_director,"CONTROL_PLAYER_MODEL_SWAP() sPI_MenuData.iCurrentSelection = ",sPI_MenuData.iCurrentSelection," PI_M0_SHORTLIST = ",selectedPI[PI_M0_SHORTLIST]," lastSelectedShortlistPed = ",lastSelectedShortlistPed)
        IF (sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SHORTLIST) AND selectedPI[PI_M0_SHORTLIST] != lastSelectedShortlistPed)
            IF GET_CAMERA_STATE() = DIR_CAM_GAME_FADED_OUT
                int shortlistEntry = GET_NTH_USED_SHORTLIST_INDEX(selectedPI[PI_M0_SHORTLIST])
                
                if shortlistEntry >= cTOTAL_SHORTLIST_ENTRIES
                    dirCurrentMenu = MC_RECENTLYUSED // shortlistData[shortlistEntry].category
                    curMenuItem = shortlistEntry - cTOTAL_SHORTLIST_ENTRIES
                    cprintln(debug_director,"A curMenuItem = ",curMenuItem) //wrong
                else
                    dirCurrentMenu = MC_SHORTLIST
                    curMenuItem = shortlistEntry
                    cprintln(debug_director,"B curMenuItem = ",curMenuItem)
                endif
                
                originalPed = player_ped_id()
                
                //unload previous animal sound ID
                If !audioUnloaded //added due to bug 2280673. The audio kept getting unloaded so the next IF statement below could never pass.
                    UNLOAD_ANIMAL_AUDIO_AND_ANIM()
                    audioUnloaded = TRUE
                ENDIF
                
                cprintln(debug_director,"shortlistEntry = ",shortlistEntry," model = ",GET_MODEL_NAME_FOR_DEBUG(shortlistData[shortlistEntry].model))
                
                REQUEST_MODEL(shortlistData[shortlistEntry].model)
                requestedModel = shortlistData[shortlistEntry].model
                cprintln(debug_director,"HAS_MODEL_LOADED() = ",HAS_MODEL_LOADED(shortlistData[shortlistEntry].model))
                IF HAS_MODEL_LOADED(shortlistData[shortlistEntry].model) AND LOAD_ANIMAL_AUDIO_AND_ANIM(shortlistData[shortlistEntry].model)
                    
                    cprintln(debug_director,"CONTROL_PLAYER_MODEL_SWAP() swap model loaded")
                    loadedModel = requestedModel
                    
                    vRespawnCoords = GET_CLOSEST_SAFE_POINT_FOR_PLAYER(GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE, DEFAULT, DEFAULT, loadedModel)
                    
                    fPlayerHeading = GET_ENTITY_HEADING(player_ped_id())
                    IF PERFORM_SHAPETEST_REPOSITION(GET_ENTITY_COORDS(player_ped_id()),shortlistData[shortlistEntry].model,vRespawnCoords,fPlayerHeading,bPassShapetest,bRespawnPass)
                        cprintln(debug_director,"CONTROL_PLAYER_MODEL_SWAP() shapetest complete")
                        lastSelectedShortlistPed = selectedPI[PI_M0_SHORTLIST]
                        requestedModel = shortlistData[shortlistEntry].model
                        CLEAR_MODEL_LOAD_REQUEST()                  
                        
                        SET_ENABLE_SCUBA(PLAYER_PED_ID(),FALSE)
                        SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), -1.0)
                        SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), true)
                        
                        if shortlistData[shortlistEntry].category   = MC_ONLINE1
                            SWAP_PLAYER_MODEL(sOnlineChar[0].model,vRespawnCoords,CM_EMPTY,0)   
                            
                        elif shortlistData[shortlistEntry].category =  MC_ONLINE2
                            SWAP_PLAYER_MODEL(sOnlineChar[1].model,vRespawnCoords,CM_EMPTY,1)
                            
                        else
                            SWAP_PLAYER_MODEL(shortlistData[shortlistEntry].model,vRespawnCoords,CM_EMPTY)  
                            cprintln(debug_director,"CONTROL_PLAYER_MODEL_SWAP() SET_PED_VARIATION_FROM_SHORTLIST()")
                            SET_PED_VARIATION_FROM_SHORTLIST(shortlistEntry)    
                        endif
                                            
                        SET_ENTITY_HEADING(player_ped_id(),fPlayerHeading)
                        GIVE_PLAYER_DIRECTOR_MODE_WEAPONS() 
                        UPDATE_PLAYER_VOICE()
                        UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
                        REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
                        REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
                        ShortlistPed(shortlistData[shortlistEntry].category,0,true,false)
                        SET_PLAYER_SCUBA_STATUS()
                        
                        audioUnloaded = FALSE
                        IF DOES_ENTITY_EXIST(originalPed)
                            IF(originalPed != player_ped_id())
                                IF NOT IS_ENTITY_A_MISSION_ENTITY(originalPed)
                                    cprintln(debug_director,"OriginalPed is not a mission entity!!!")
                                    //SET_ENTITY_AS_MISSION_ENTITY(originalPed,TRUE,TRUE)
                                ENDIF
                                
                                IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(originalPed,FALSE)
                                    cprintln(debug_director,"OriginalPed does not belong to this script!!!")
                                    SET_ENTITY_AS_MISSION_ENTITY(originalPed,TRUE,TRUE)
                                ENDIF
                            
                                
                                DELETE_PED(originalPed)
                            ENDIF
                        ENDIF
                        
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            navSwap = FALSE
        ENDIF
    ENDIF
    
    
    
    IF directorFlowState = dfs_RUN_TRAILER_SCENE
    AND (dirCurrentMenu > MC_START_ACTOR_CATEGORIES OR dirCurrentMenu = MC_SHORTLIST OR dirCurrentMenu = MC_RECENTLYUSED)
        if bLoadingNewModel 
        and IS_MODEL_VALID(requestedModel)
        and HAS_MODEL_LOADED(requestedModel)
                loadedModel = requestedModel
                
                CLEAR_MODEL_LOAD_REQUEST()          
                pedJustShortlisted = FALSE //tells us a new shortlist can now be added.
                                    
                    IF NOT IS_PED_INJURED(player_ped_id())                                      
                        originalPed = player_ped_id()
                        
                        if dirCurrentMenu = MC_ONLINE
                            if displayedMenuData[curMenuItem].category = MC_ONLINE1
                                SWAP_PLAYER_MODEL(loadedModel,<<0,0,0>>,selectedHeistMember,0)
                                
                            elif displayedMenuData[curMenuItem].category = MC_ONLINE2
                                SWAP_PLAYER_MODEL(loadedModel,<<0,0,0>>,selectedHeistMember,1)  
                            endif   
                        else
                            cprintln(debug_director," CLEAR PED PROPS ")
                            SWAP_PLAYER_MODEL(loadedModel,<<0,0,0>>,selectedHeistMember)                          
                            RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
                            RESTORE_PLAYER_PED_TATTOOS(player_ped_id())
                            
                            IF dirCurrentMenu = MC_SHORTLIST 
                            OR dirCurrentMenu = MC_RECENTLYUSED
                                IF dirCurrentMenu = MC_SHORTLIST
                                    SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem)   
                                ELSE                                
                                    SET_PED_VARIATION_FROM_SHORTLIST(curMenuItem+cTOTAL_SHORTLIST_ENTRIES)
                                ENDIF
                            ENDIF
                        endif
                        
                        
                        
                        //added this because it's needed to restrict bad ped variations which were being done in SET_RANDOM_PLAYER_VARIATIONS(). Bugs 2260880 and 2260915
                        IF dirCurrentMenu != MC_SHORTLIST 
                        AND dirCurrentMenu != MC_RECENTLYUSED
                            FIX_BAD_PED_COMPONENT_VARIAITONS(loadedModel) 
                        ENDIF
                        
                        UPDATE_PLAYER_VOICE()
                        UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
                        REVISE_WEAPON_ANIMS_FOR_PED_MODEL()
                        REVISE_MOVEMENT_CLIPSETS_FOR_PED()  
                                                
                        SET_PLAYER_SCUBA_STATUS()
                        
                        IF IS_MODEL_AN_ANIMAL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
                            SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(),FALSE)
                        ELSE
                            SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(),TRUE)
                        ENDIF

                    
                    IF DOES_ENTITY_EXIST(originalPed)
                    AND NOT bBlockOriginalPedDelete
                        IF originalPed != player_ped_id()
                            IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(originalPed,FALSE)
                                SET_ENTITY_AS_MISSION_ENTITY(originalPed,TRUE,TRUE)
                            ENDIF
                            DELETE_PED(originalPed)
                        ENDIF
                    ENDIF                   
                ENDIF

            REBUILD_HELP_KEYS()
            
        ENDIF
        DISPLAY_HUD(FALSE)
        DISPLAY_RADAR(FALSE)    
    ENDIF
    
ENDPROC

bool bMetricStored
PROC CONTROL_METRICS()
    IF IS_REPLAY_RECORDING()
        IF !bMetricStored
            bMetricStored = TRUE
            VideoClipMetric thisMetric
            thisMetric.m_characterHash = GET_HASH_KEY(GET_CATEGORY_TITLE(activeMenuEntry))
            thisMetric.m_timeOfDay = settingsMenu[SEM_TIME]
            IF settingsMenu[SEM_WANTED] > 0
                thisMetric.m_wantedLevel = TRUE
            ELSE
                thisMetric.m_wantedLevel = FALSE
            ENDIf
            thisMetric.m_pedDensity = settingsMenu[SEM_PEDDENSITY]
            thisMetric.m_vehicleDensity = settingsMenu[SEM_VEHDENSITY]
            IF settingsMenu[SEM_RESTRICTED] = 0
                thisMetric.m_restrictedArea = FALSE
            ELSE
                thisMetric.m_restrictedArea = TRUE
            ENDIF
            
            IF settingsMenu[SEM_INVINCIBLE] = 0
                thisMetric.m_invulnerability = FALSE
            ELSE
                thisMetric.m_invulnerability = TRUE
            ENDIF
            PLAYSTATS_APPEND_DIRECTOR_METRIC(thisMetric)
        ENDIF
    ELSE
        IF bMetricStored
            bMetricStored = FALSE
        ENDIF
    ENDIF
ENDPROC

PROC CONTROL_INTERACTION()
    IF directorFlowState = dfs_PLAY_MODE
        
        //IF !PImenuON
            IF navSpeech
            
                IF NOT IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INDEEPWATER)
                AND NOT IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_ARRESTING)
                AND NOT IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_DEAD)
                AND NOT IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_INAIR)
                
                    IF IS_BIT_SET(currentWarningStateBitSet,BIT_PWS_AS_ANIMAL)
                        PLAY_ANIMAL_NOISE()
                    ELSE
                        IF iMaxSpeechEnries > 1 AND NOT IS_CELLPHONE_CAMERA_IN_USE()
                            PLAY_SPEECH()                   
                        ENDIF
                    ENDIF       
                    
                ENDIF
                
            ENDIF
            
            IF navGesture
                IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_GESTURE)
                    IF navFullBodyGesture
                        PLAY_ANIMATION(TRUE)
                    ELSE
                //  IF GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
                //  AND NOT IS_ENTITY_PLAYING_ANIM(player_ped_id(),tLoadedAnimDict,tAnimToPlay)
                        PLAY_ANIMATION()
                    ENDIF
                //  ENDIF                                   
                ENDIF   
            ELSE
                IF bGesturePlaying
                    int animIndex = enum_to_int(PLAYER_INTERACTION_NONE)+selectedPI[PI_M0_GESTURE]
                    START_INTERACTION_ANIM(2,animIndex,FALSE)
                    bGesturePlaying = FALSE
                ENDIF
            ENDIF
        //ENDIF
        
        IF navSwap
            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_SHORTLIST_SWAP)
            AND sPI_MenuData.iCurrentSelection = ENUM_TO_INT(PI_M0_SHORTLIST)
                SET_DIRECTOR_MODE_STATE(dfs_swap_player_model)
            ELSE
                navSwap = FALSE
            ENDIF
        ENDIF
        
        IF navTravel
            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
                int iTravelEntry = iLocationSelected[selectedPI[PI_M0_LOCATION]]
                if iTravelEntry >= 0
                    PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    UNLOCK_DIRECTOR_TRAVEL_LOCATION( INT_TO_ENUM( DirectorTravelLocation, iTravelEntry ) )
                    IF ARE_ALL_DM_LOCATIONS_UNLOCKED()
                        IF NOT HAS_ACHIEVEMENT_BEEN_AWARDED( ACHR8 )
                            bAwardLocationUnlockAchievement = TRUE
                        ENDIF
                    ENDIF
                    SET_TRAVEL_TO_LOCATION(iTravelEntry)
                ENDIF
            ENDIF
        ENDIF
        
        IF navSaveLocation
            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARP_SET)
                if iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_1)
                OR iLocationSelected[selectedPI[PI_M0_LOCATION]] = ENUM_TO_INT(DTL_USER_2)
                    PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position = (GET_PED_BONE_COORDS(player_ped_id(),BONETAG_L_TOE,<<0,0,0>>) + GET_PED_BONE_COORDS(player_ped_id(),BONETAG_R_TOE,<<0,0,0>>)) / 2.0
                    warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].heading = GET_ENTITY_HEADING(player_ped_id())
                    warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].camRelativePitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
                    warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].camRelativeHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
                    IF GET_INTERIOR_AT_COORDS(warpLocations[selectedPI[PI_M0_LOCATION]].position) != NULL
                        warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].interior = TRUE
                    ELSE
                        warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].interior = FALSE
                    ENDIF
                    REMOVE_MENU_HELP_KEYS()
                    REBUILD_PI_HELP_KEYS()
                ENDIF
            ENDIF
        ENDIF
        
        IF navSetWaypoint
        
            VECTOR vCurrLocCoords,  vPlayerCoords
            vCurrLocCoords  = warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position 
            vPlayerCoords   = GET_PLAYER_COORDS( PLAYER_ID() )
                            
            IF IS_PLAYER_WARNING_STATE_SAFE(PWSS_SAFE_FOR_WARPING)
            AND NOT IS_VECTOR_ZERO(warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position)
            #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
            AND iLocationSelected[selectedPI[PI_M0_LOCATION]] <> ENUM_TO_INT(DTL_WAYPOINT) 
            #ENDIF
            AND VDIST2( vCurrLocCoords, vPlayerCoords ) > ( c_F_WAYPOINT_PLAYER_MIN_DIST * c_F_WAYPOINT_PLAYER_MIN_DIST )
                PLAY_SOUND_FRONTEND( -1, "WAYPOINT_SET", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE )
                SET_NEW_WAYPOINT(warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position.x,warpLocations[iLocationSelected[selectedPI[PI_M0_LOCATION]]].position.y)
                iWaypointLocIndex = selectedPI[PI_M0_LOCATION]
                RESET_PI_MENU_NOW()
            ENDIF
            navSetWaypoint = FALSE
        ELIF navDelWaypoint
            REMOVE_DM_WAYPOINT()
            bResetMenuNow   = TRUE
            navDelWaypoint  = FALSE
        ENDIF
        
        //commented out as these no longer get changed during game
        //IF bChangeTimeOfDay
        //  CHANGE_TIME()
        //ENDIF
        
        //IF bChangeWeather
        //  CHANGE_WEATHER()
        //ENDIF
        
        
        UPDATE_PED_NEEDS_TO_BLOCK_CAR_ENTRY()   
        UPDATE_BLOCK_WEAPON_WHEEL()
            
    ENDIF
ENDPROC

PROC CONTROL_FORCED_DEATH()
    IF GET_DIRECTOR_FLOW_STATE() = dfs_PLAY_MODE
    AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF Is_Model_A_Playable_Animal(GET_ENTITY_MODEL(PLAYER_PED_ID()))    //Only for animal actors
            IF IS_SCREEN_FADED_IN() AND Should_Animal_Mode_Exit(crushDepthTimer)
                IF NOT IS_PED_INJURED(player_ped_id())  
                    cprintln(DEBUG_DIRECTOR, "Exit condition fired. Kill Player.")
                    SET_ENTITY_HEALTH(player_ped_id(),0)
                ENDIF
            ENDIF
        ELSE    //Kill the player if under min Z otherwise
            VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
            IF vPos.z < -170 AND IS_ENTITY_IN_WATER(PLAYER_PED_ID())
            AND GET_GAME_TIMER() > crushDepthTimer
                cprintln(DEBUG_DIRECTOR, "Player swimming too deep, Dealing damage!")
                SET_ENTITY_HEALTH(player_ped_id(),GET_ENTITY_HEALTH(PLAYER_PED_ID())-15)
                SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
                crushDepthTimer = GET_GAME_TIMER() + 1000
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC CONTROL_PC_BUTTON_SWAPS()
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        caMenuLeft = INPUT_CELLPHONE_LEFT
        caMenuRight = INPUT_CELLPHONE_RIGHT
        caMenuUp = INPUT_CELLPHONE_UP
        caMenuDown = INPUT_CELLPHONE_DOWN

        caMenuAccept = INPUT_CELLPHONE_SELECT
        caMenuCancel = INPUT_FRONTEND_CANCEL//INPUT_CELLPHONE_CANCEL    
        //PI menu switches
        caPiMenuWaypoint    = INPUT_CONTEXT
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        caPiMenuProp        = INPUT_CONTEXT
        #ENDIF
        
    ELSE 
        caMenuLeft = INPUT_FRONTEND_LEFT
        caMenuRight = INPUT_FRONTEND_RIGHT
        caMenuUp = INPUT_FRONTEND_UP
        caMenuDown = INPUT_FRONTEND_DOWN

        caMenuAccept = INPUT_FRONTEND_ACCEPT
        caMenuCancel = INPUT_FRONTEND_CANCEL    
        
        //PI menu switches
        caPiMenuWaypoint    = INPUT_FRONTEND_X
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        caPiMenuProp        = INPUT_FRONTEND_X
        #ENDIF
    ENDIF
    
    //Rebuild PI keys if camera override has changed since last frame
    IF bPiMouseCameraOverride <> bLastFrameCameraOverride
    OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
        REMOVE_MENU_HELP_KEYS()
        REBUILD_PI_HELP_KEYS()
        bLastFrameCameraOverride = bPiMouseCameraOverride
    ENDIF
ENDPROC

PROC CONTROL_PC_BUTTON_HOLD()
    INT iCsr = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
    
    IF (bWasMenuDrawn = FALSE) OR NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        EXIT
    ENDIF
    
    IF IS_CURSOR_ACCEPT_JUST_PRESSED()
        g_iMenuCursorHoldTime = GET_GAME_TIMER() + MENU_CURSOR_ARROW_SCROLL_TIME
    ELSE
        IF IS_CURSOR_ACCEPT_HELD() AND (GET_GAME_TIMER() > g_iMenuCursorHoldTime)
            IF (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM)
                IF (iCsr = -1)
                    SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, caMenuLeft, 1.0)
                ENDIF
                
                IF (iCsr = 1)
                    SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, caMenuRight, 1.0)
                ENDIF   
            ELIF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP)
                SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, caMenuUp, 1.0)
                g_iMenuCursorHoldTime = GET_GAME_TIMER() + (MENU_CURSOR_ARROW_SCROLL_TIME / 2)
                EXIT    
            ELIF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN)
                SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, caMenuDown, 1.0)
                g_iMenuCursorHoldTime = GET_GAME_TIMER() + (MENU_CURSOR_ARROW_SCROLL_TIME / 2)
                EXIT
            ENDIF
            
            g_iMenuCursorHoldTime = GET_GAME_TIMER() + MENU_CURSOR_ARROW_SCROLL_TIME
        ENDIF
    ENDIF   
ENDPROC

PROC CONTROL_DIRECTOR_MODE()

    #IF IS_DEBUG_BUILD  IF bUnlockAll UNLOCK_ALL_DIRECTOR_PROGRESS_FOR_DEBUG()              ENDIF   #ENDIF
    #IF IS_DEBUG_BUILD  IF bDebugUnlockLocations UNLOCK_ALL_DIRECTOR_LOCATIONS_FOR_DEBUG()  ENDIF   #ENDIF
    
    CONTROL_WARNING_STATE()
    CONTROL_PC_BUTTON_SWAPS()
    CONTROL_PLAYER_INPUT()
    CONTROL_MAINMENU_MOUSE_INPUT()
    CONTROL_PI_MENU_MOUSE_INPUT()
    CONTROL_MENU()
    CONTROL_PI_MENU()
    CONTROL_DIRECTOR_MODE_FLOW()
    CONTROL_DIRECTOR_MODE_CAMERA()  
    CONTROL_PLAYER_MODEL_SWAP() 
    CONTROL_METRICS()
    
    CONTROL_INTERACTION()
    CONTROL_FORCED_DEATH()
    CONTROL_GESTURES()
    CONTROL_HELPTEXT()
    //CONTROL_WEAPON_SHOWN()
    
    CONTROL_FORCE_UPDATES()
    CONTROL_PC_BUTTON_HOLD()
    CONTROL_DM_SETTINGS_CHANGE()
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR 
		IF bInPropEdit DMPROP_PER_FRAME_PROCESSING()  
		ENDIF   
		#IF FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT
			DMPROP_DISABLE_PROP_COLLISION_IN_INTERIOR()
		#ENDIF
	#ENDIF
   
    
    #if IS_DEBUG_BUILD
        CONTRL_DEBUG()
    #endif
    
    IF NOT IS_PED_INJURED(player_ped_id())
        SET_PED_RESET_FLAG(player_ped_id(), PRF_DisableTakeOffScubaGear, true)
        SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableTakeOffScubaGear,true)
    ENDIF
    
    DISABLE_FIRST_PERSON_VIEW_ON_RESTRICTED_MODELS_THIS_FRAME() // constantly block FPS when not needed.
        
ENDPROC

PROC INIT_SPEECH_DATA()
    
    speechContextData[0] = "" //empty option
    speechContextData[1] = "GENERIC_HI"
    speechContextData[2] = "GENERIC_BYE"
    speechContextData[3] = "GENERIC_HOWS_IT_GOING"
    speechContextData[4] = "GENERIC_AGREE"
    speechContextData[5] = "GENERIC_YES"
    speechContextData[6] = "GENERIC_NO"
    speechContextData[7] = "GENERIC_THANKS"
    speechContextData[8] = "GENERIC_WHATEVER"
    speechContextData[9] = "GENERIC_CHEER"
    speechContextData[10] = "GENERIC_FRIGHTENED_HIGH"
    speechContextData[11] = "GENERIC_FRIGHTENED_MED"
    speechContextData[12] = "GENERIC_SHOCKED_HIGH"
    speechContextData[13] = "GENERIC_SHOCKED_MED"
    speechContextData[14] = "GENERIC_INSULT_HIGH"
    speechContextData[15] = "GENERIC_INSULT_MED"
    speechContextData[16] = "GENERIC_CURSE_HIGH"
    speechContextData[17] = "GENERIC_CURSE_MED"
ENDPROC

PROC INITIALISE_DIRECTOR_MODE()
    
	CPRINTLN(DEBUG_DIRECTOR,"Initialising Director Mode")
	
	//Set-up the running globals
	g_bDirectorModeRunning = TRUE
	g_bLaunchDirectorMode = FALSE
	
	CDEBUG1LN(DEBUG_DIRECTOR,"INITIALISE_DIRECTOR_MODE: setting g_bDirectorSwitchToCastingTrailer to FALSE")
	g_bDirectorSwitchToCastingTrailer = FALSE
	g_bDirectorSwitchToCastingTrailerFromCode = FALSE
    g_bDirectorSwitchToLastPlayer = FALSE //bug 2288860. This could previously get set to TRUE outside of DM, causing DM to go straight to a terminate state once it initiates.
    //B* 2290015: This needs to be here otherwise early cleanup could leave the player without control or HUD
    ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, FALSE)

    //B*-2348197
    SET_AUDIO_FLAG("LoadMPData", TRUE)
    INT timer = 0
    WHILE((NOT HAS_LOADED_MP_DATA_SET()) AND timer < 500)
        Wait(0)     //Wait for MP data to be loaded
        timer++
    ENDWHILE
    IF NOT HAS_LOADED_MP_DATA_SET()
        CPRINTLN(DEBUG_DIRECTOR,"#NOT HAS_LOADED_MP_DATA_SET()")
    ENDIF
    
    //Once it is safe to run save the game state.
    Store_Game_State_Snapshot_For_Start(GET_THIS_SCRIPT_NAME(), TRUE)

    eStoryCharacter = g_startSnapshot.eCharacter
    
    //save special ability charge
    SWITCH eStoryCharacter
        CASE CHAR_MICHAEL
            STAT_GET_INT(SP0_SPECIAL_ABILITY, iSpecialStatValue)
        BREAK
        CASE CHAR_FRANKLIN
            STAT_GET_INT(SP1_SPECIAL_ABILITY, iSpecialStatValue)
        BREAK
        CASE CHAR_TREVOR
            STAT_GET_INT(SP2_SPECIAL_ABILITY, iSpecialStatValue)
        BREAK
    ENDSWITCH
    
    //B* 2277536: Disable cheats on entry
    DISABLE_UNSAFE_DIRECTOR_MODE_CHEATS(DUMMY_MODEL_FOR_SCRIPT)
    
    CPRINTLN(DEBUG_DIRECTOR, "Stored startup story character as ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(eStoryCharacter), ".")
    SETUP_SETTINGS_MENU_DEFAULTS()
    SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)
    IF iPlayerHealthOnStart <= 0 iPlayerHealthOnStart = 50 ENDIF
    
    SETUP_MP_CHAR_DATA()
    
    #IF FEATURE_SP_DLC_DM_PROP_EDITOR
    //Initialise the prop storage
    INITIALISE_DM_PROPS()
    #ENDIF
    
    LOAD_DATA() //load shortlist peds etc
    
    //set up speech data array
    INIT_SPEECH_DATA()
    SET_AUDIO_FLAG("IsDirectorModeActive",true) 
    SET_AUDIO_FLAG("PoliceScannerDisabled", TRUE)

    // Disable stat tracking while a none story character.
    STAT_DISABLE_STATS_TRACKING()
    
    // Inform code that we are not playing as a story character 
    // We can use the animal form functionality set up for Peyotes.
    SET_PLAYER_IS_IN_ANIMAL_FORM(TRUE)
    
    SET_PLAYER_IS_IN_DIRECTOR_MODE(TRUE)
    SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(player_id(),FALSE)

    //Allow the player to see the whole map but not reveal any
    //fog of war while doing so.
    SET_MINIMAP_FOW_DO_NOT_UPDATE(TRUE)
    SET_MINIMAP_HIDE_FOW(TRUE)
    
    // Disable stunt jumps while in Director mode
    SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
    
    SET_AMBIENT_PEDS_DROP_MONEY(FALSE)
    
    REQUEST_ANIM_DICT("director@character_select_intro@male")
    REQUEST_ANIM_DICT("director@character_select_intro@female")
    REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")

    REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DIRECTOR_MODE)
    
    // Clean up the screen state.
    CLEAR_HELP(TRUE)
    CLEAR_PRINTS()
    THEFEED_FLUSH_QUEUE()
    
    //scuba issues in DM 
    SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_ID(),false)   
    
    GET_ACTUAL_SCREEN_RESOLUTION(oldScrX,oldScrY)
    
    STORE_AUTO_DOORS_STATES_AND_SET_ALL_UNLOCKED(doorArray, bDoorDataMissing)

    //Flag this true once as the script first starts up.
    //Display exit help text once for this session.
    bDisplayExitHelpText = TRUE
    
    //load save data here?
    SETUP_WARP_LOCATIONS()
    
    UPDATE_SPEECH_CONTEXTS_FOR_PLAYER_MODEL()
    
    requestedModel = GET_ENTITY_MODEL(player_ped_id())
    loadedModel =GET_ENTITY_MODEL(player_ped_id())
    switch loadedModel
        CASE PLAYER_ZERO activeMenuEntry = MC_STOMIC BREAK //michael
        CASE PLAYER_ONE activeMenuEntry = MC_STOFRA BREAK //franklin
        CASE PLAYER_TWO activeMenuEntry = MC_STOTRE BREAK //trevor
    ENDSWITCH
    
    //bug 2411074 - clear ped damage 
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        CLEAR_PED_WETNESS(PLAYER_PED_ID())
        CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
        RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
    ENDIF
    
    //Waypoint initialisation stuff
    SET_WAYPOINT_OFF()
    iWaypointLocIndex = -1
    
    // Make sure all context button registrations from the main game are cleaned up. Ensures
    // help text related to the context button is removed too.
    FORCE_CONTEXT_SYSTEM_RESET()
    
    //Add the phone contact to all player phonebooks if they don't already have it.
    IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_DIRECTOR, MICHAEL_BOOK)
        CPRINTLN(DEBUG_DIRECTOR, "Adding Director Mode contact to Michael's phonebook.")
        ADD_CONTACT_TO_PHONEBOOK(CHAR_DIRECTOR, MICHAEL_BOOK, FALSE)
    ENDIF
    IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_DIRECTOR, FRANKLIN_BOOK)
        CPRINTLN(DEBUG_DIRECTOR, "Adding Director Mode contact to Franklin's phonebook.")
        ADD_CONTACT_TO_PHONEBOOK(CHAR_DIRECTOR, FRANKLIN_BOOK, FALSE)
    ENDIF
    IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_DIRECTOR, TREVOR_BOOK)
        CPRINTLN(DEBUG_DIRECTOR, "Adding Director Mode contact to Trevor's phonebook.")
        ADD_CONTACT_TO_PHONEBOOK(CHAR_DIRECTOR, TREVOR_BOOK, FALSE)
    ENDIF
    
    IF IS_PLAYSTATION_PLATFORM()
        SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
    ENDIF
    
    SET_RICH_PRESENCE_FOR_SP_DIRECTOR_MODE()
    
    cvm_StartUp = GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT())
    
    //B* 2503385: Request snacks audio
    REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS")
    
    bAllSpecialCharasUnlocked   = ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED()
    bALLStoryModeCharasUnlocked = ARE_ALL_DM_STORY_CHARAS_UNLOCKED()
    bAlmostAllAnimalsUnlocked   = ARE_ALL_DM_ANIMALS_UNLOCKED( TRUE )
ENDPROC

PROC DO_DIRECTOR_MODE_FORCE_SAVE_DATA()
    CPRINTLN(debug_director,"Force save detected, forcing a data save")
    
    RUN_SHARED_CLEANUP()
    
    //SAVE_DATA()
    
    RESTORE_DIRECTOR_MODE_STARTING_GAMESTATE()
    
    //Prepare for autosave
    HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
    g_sAutosaveData.bFlushAutosaves = FALSE
    SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE,TRUE)
    SET_PLAYER_IS_IN_DIRECTOR_MODE(FALSE)
    SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
    MAKE_AUTOSAVE_REQUEST()
    CPRINTLN(debug_director,"Autosave request made, Checking Autosave controller")
    
    //Manually launch the Autosave controller
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("autosave_controller")) = 0
        CPRINTLN(debug_director,"No autosave controller running, manually launching")
        REQUEST_SCRIPT_WITH_NAME_HASH(HASH("autosave_controller"))
        WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("autosave_controller"))
            WAIT(0)
        ENDWHILE
        START_NEW_SCRIPT_WITH_NAME_HASH(HASH("autosave_controller"), DEFAULT_STACK_SIZE)
        SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("autosave_controller"))
    ENDIF   
    
    INT iTime = GET_GAME_TIMER()
    INT iWait = 5   //Wait max 5 frames 
    WHILE NOT IS_AUTOSAVE_REQUEST_IN_PROGRESS() //Wait for the save controller to pick up request
    AND iWait > 0
        iWait -=1
        Wait(0)
    ENDWHILE 
    
    WHILE IS_AUTOSAVE_REQUEST_IN_PROGRESS() //Wait until it's done with request
        Wait(0)
    ENDWHILE
    SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
    CPRINTLN(debug_director,"Autosave done, took ",GET_GAME_TIMER() - iTime," ms.")
            
ENDPROC

#ENDIF  // FEATURE_SP_DLC_DIRECTOR_MODE

SCRIPT

    //Compile an empty script if this feature isn't enabled.
    #IF FEATURE_SP_DLC_DIRECTOR_MODE
    
    CALCULATE_MAX_LOCATIONS_BIT_VALUE()
    
    //Get the player health as soon as the script starts - resets after the autosave request
    iPlayerHealthOnStart = GET_ENTITY_HEALTH(PLAYER_PED_ID())
    
    cprintln(debug_director,"Director Mode Start")
	
	//If the player has accessed Director Mode then they will have to reboot in order to gain access to MP
	#IF FEATURE_SUMMER_2020
	IF NOT g_sMPTunables.bDISABLE_DIRECTOR_MP_BLOCK
		SCRIPT_BLOCK_ACCESS_TO_MULTIPLAYER(TRUE)
	ENDIF
	#ENDIF
    
    // Main loop    -   Has moved to the top to process death/arrest without completely restarting the thread
    WHILE NOT HAS_DIRECTOR_MODE_TERMINATED()
    

        /*
        #if IS_DEBUG_BUILD  
            //Please leave! This is handy at times for sanity checking the scene menu work. Thanks, Steve T.    
            IF bInPropEdit
                DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.2,"STRING", "In Prop Edit)
            ELSE
                DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.2,"STRING", "Not Edit")
            ENDIF
        #endif
        */
        
        #IF FEATURE_SP_DLC_DM_PROP_EDITOR
        IF bInPropEdit //Sanity check for 2502459. Do not remove. Bug keeps coming back as unfixed.
            SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
            SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
        ENDIF
        #ENDIF




        //cprintln(debug_director,"directorFlowState = ",directorFlowState," bForceCleanupSetup = ",bForceCleanupSetup)
        
        //int iHudGetA,iHudGetB
        //GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudGetB, iHudGetB, iHudGetB, iHudGetA)
        //iHudGetB=iHudGetB
        //cprintln(debug_director,"Alpha = ",iHudGetA)
        
        //Force data save if terminated by special clean-up
        IF g_bDirectorForceSaveData
            DO_DIRECTOR_MODE_FORCE_SAVE_DATA()
            g_bDirectorForceSaveData = FALSE
        ENDIF
        
        WHILE NOT bForceCleanupSetup   //- Only setup the force cleanup if it's a fresh start or we've processed a death/arrest
            bForceCleanupSetup = TRUE
            IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
                iLastCleanupCause = GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP()
                #IF IS_DEBUG_BUILD
                    SWITCH iLastCleanupCause
                        CASE FORCE_CLEANUP_FLAG_SP_TO_MP
                    		CPRINTLN(DEBUG_DIRECTOR, "Director Mode is being forced to cleanup. Reason: SP to MP") BREAK
                        CASE FORCE_CLEANUP_FLAG_DEBUG_MENU
							CPRINTLN(DEBUG_DIRECTOR, "Director Mode is being forced to cleanup. Reason: Debug Menu") BREAK
                        CASE FORCE_CLEANUP_FLAG_REPEAT_PLAY
							CPRINTLN(DEBUG_DIRECTOR, "Director Mode is being forced to cleanup. Reason: Repeat Play") BREAK
                    ENDSWITCH
                #ENDIF
				
                
                SWITCH iLastCleanupCause
                    CASE FORCE_CLEANUP_FLAG_SP_TO_MP        SCRIPT_CLEANUP(DCT_MULTIPLAYER)         BREAK
                    CASE FORCE_CLEANUP_FLAG_REPEAT_PLAY     SCRIPT_CLEANUP(DCT_REPEAT, TRUE)        BREAK
                    CASE FORCE_CLEANUP_FLAG_DEBUG_MENU      SCRIPT_CLEANUP(DCT_FULL)                BREAK
                ENDSWITCH

            ENDIF
        ENDWHILE
		
		//Manual Death/Arrest detection
		IF NOT bProcessingRespawn AND NOT IS_PLAYER_PLAYING(PLAYER_ID())
			CPRINTLN(DEBUG_DIRECTOR, "Director Mode is being forced to cleanup. Reason: Death/Arrest")
			IF NOT bDirectorInitialised 
				SCRIPT_CLEANUP(DCT_EARLY)
			ELSE
                IF NOT bProcessingRespawn
                   	Run_Director_Mode_Death_Arrest()
				ENDIF
			ENDIF
		ENDIF
        
        //Only do initialisation once per script run
        IF NOT bDirectorInitialised
            SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE, TRUE)
            MAKE_AUTOSAVE_REQUEST()
            
            //Attempt to go on mission as soon as we launch.
            WHILE NOT SECURE_MISSION_FLAG()             
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableTakeOffScubaGear,true)
                    SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffScubaGear, true)
                ENDIf
                WAIT(0)
            ENDWHILE
                
            //Force other scripts to clean up dfrom going into Director Mode
            FORCE_CLEANUP(FORCE_CLEANUP_FLAG_DIRECTOR)

            INITIALISE_DIRECTOR_MODE()
            bDirectorInitialised = TRUE
        ENDIF
    
    
        #IF IS_DEBUG_BUILD
            //SET_TEXT_COLOUR(255, 255, 255, 255)
            //SET_TEXT_SCALE(0.75, 0.9)
            //SET_TEXT_WRAP(0.0, 1.0)
            //DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.63, "STRING", "Director Mode Running")
            IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F,KEYBOARD_MODIFIER_NONE,"Force quit Director")
                SCRIPT_CLEANUP(DCT_FULL)
            ENDIF
        #ENDIF
        
		//Only update the main director mode state machine if we're not bringing the player back to life or force-cleaning up
		IF g_bDirectorTerminateOnSpCleanup
			CPRINTLN(DEBUG_DIRECTOR, "g_bDirectorTerminateOnSpCleanup is set, terminating Director Mode outside a Force Cleanup")
			SCRIPT_CLEANUP(DCT_MULTIPLAYER, TRUE)
		ELIF bProcessingRespawn
			Run_Director_Mode_Respawn()
		ELSE
        	CONTROL_DIRECTOR_MODE()
		ENDIF
        
        IF NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE() = FALSE
        AND GET_CONFIRM_INVITE_INTO_GAME_STATE() = FALSE
        AND IS_INVITE_IS_TRANSITION_INVITE() = FALSE
            IF STAT_IS_STATS_TRACKING_ENABLED() 
                STAT_DISABLE_STATS_TRACKING()
                NET_NL()NET_PRINT("[BCLOAD] Director Mode: STAT_DISABLE_STATS_TRACKING called ")
            ENDIF
        ENDIF
        
        WAIT(0)
        
    ENDWHILE
    
    SCRIPT_CLEANUP(DCT_FULL)
    
    #ENDIF  // FEATURE_SP_DLC_DIRECTOR_MODE
ENDSCRIPT
