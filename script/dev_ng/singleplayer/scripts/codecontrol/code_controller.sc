//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 11/03/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  			Code Controller									│
//│																				│
//│		DESCRIPTION: A controller script used to launch specific blocks of 		│
//│		custom script by the communication controller and flow controller.		│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "family_public.sch"
USING "code_control_data_GTA5.sch"
USING "taxi_functions.sch"

SCRIPT
	// This script needs to cleanup only when the game moves from SP to MP
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_CODEID, "Code Controller has been forced to clean up.")
		TERMINATE_THIS_THREAD()
	ENDIF

	WHILE TRUE
		INT index
		
		IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			IF g_iCommsCandidateID = NO_CANDIDATE_ID 
				CPRINTLN(DEBUG_CODEID, "Code Controller cleaning up as we have gone on mission.")
				TERMINATE_THIS_THREAD()
			ENDIF
		ENDIF
		
		//For each scripted code ID...
		REPEAT CID_MAX index
			//Has this code ID be flagged to run?
			IF g_savedGlobals.sCodeControlData.bRunCodeID[index]
				//Wait for this code ID's delay timer.
				IF GET_GAME_TIMER() >= g_savedGlobals.sCodeControlData.iExecuteTimeForCodeID[index]
					
					//Execute specific script for this code ID.
					SWITCH(INT_TO_ENUM(CC_CodeID, index))
						
						// Activity activation code IDs.
						CASE CID_ACTIVATE_ACTIVITY_CINEMA
							Execute_Activate_Activity_Cinema()
						BREAK
						
						// Minigame activation code IDs.
						CASE CID_ACTIVATE_MINIGAME_BASE_JUMP
							Execute_Activate_Minigame_Basejumping()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_DARTS2
							Execute_Activate_Minigame_Darts2()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_TRAFFICKING
							Execute_Activate_Minigame_Trafficking()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_GOLF
							Execute_Activate_Minigame_Golf()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_HUNTING
							Execute_Activate_Minigame_Hunting()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_OFFROAD_RACES
							Execute_Activate_Minigame_Offroad_Races()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_PILOT_SCHOOL
							Execute_Activate_Minigame_Pilot_School()
						BREAK

						CASE CID_ACTIVATE_MINIGAME_SEA_RACES
							Execute_Activate_Minigame_Sea_Races()
						BREAK
						
						CASE CID_DISPLAYED_SEA_RACE_HELP
							Execute_Displayed_Sea_Race_Help()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_STREET_RACES
							Execute_Activate_Minigame_Street_Races()
						BREAK
						
						CASE CID_DISPLAY_STREET_RACE_HELP
							Execute_Display_Street_Race_Help()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_STRIPCLUB
							Execute_Activate_Minigame_Stripclub()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_STUNT_PLANES
							Execute_Activate_Minigame_Stunt_Planes()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_TAXI
							Execute_Activate_Minigame_Taxi_And_ShopRobberies()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_TOWING
							Execute_Activate_Minigame_Towing()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_TRIATHLON
							Execute_Activate_Minigame_Triathlon()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_YOGA
							Execute_Activate_Minigame_Yoga()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_COUNTRY_RACE
							Execute_Activate_Minigame_Country_Race()
						BREAK
						
						CASE CID_COUNTRY_RACE_TEXT_RECEIVED
							Execute_Country_Race_Text_Received()
						BREAK
						
						CASE CID_ACTIVATE_MINIGAME_POST_ARM3_BLOCK
							Execute_Activate_Minigame_Post_Armenian3_Block()
						BREAK


						// Random event activation code IDs.
						CASE CID_ACTIVATE_RE_AND_RC_MISSIONS
							Execute_Activate_RE_And_RC_Missions()
						BREAK
						
						CASE CID_ACTIVATE_RE_BLOCK_POST_LAMAR1
							Execute_Activate_REs_Post_Lamar1()
						BREAK
						
						CASE CID_ACTIVATE_RE_BLOCK_POST_TREV1
							Execute_Activate_REs_Post_Trevor1()
						BREAK
						
						CASE CID_ACTIVATE_RE_BLOCK_POST_TREV2
							Execute_Activate_REs_Post_Trevor2()
						BREAK
						
						CASE CID_ACTIVATE_RE_BLOCK_POST_TREV3
							Execute_Activate_REs_Post_Trevor3()
						BREAK
						
						CASE CID_ACTIVATE_RE_SIMEON_YETARIAN
							Execute_Activate_RE_Simeon_Yetarian()
						BREAK
						
						
						// Shop activation code IDs.
						CASE CID_ACTIVATE_SHOP_BARBERS
							Execute_Activate_Shop_Barbers()
						BREAK
						
						CASE CID_ACTIVATE_SHOP_CLOTHES_AMB
							Execute_Activate_Shop_Clothes_Amb()
						BREAK
						
						CASE CID_ACTIVATE_SHOP_CARMOD
							Execute_Activate_Shop_Carmod(0)
						BREAK
						
						CASE CID_CARMOD_UNLOCK_STAGE_1
							Execute_Activate_Shop_Carmod(1)
						BREAK
						
						CASE CID_CARMOD_UNLOCK_STAGE_2
							Execute_Activate_Shop_Carmod(2)
						BREAK
						
						CASE CID_CARMOD_UNLOCK_STAGE_3
							Execute_Activate_Shop_Carmod(3)
						BREAK
						
						CASE CID_ACTIVATE_GUNSHOP_AND_RANGE
							Execute_Activate_Gunshop_And_Range()
						BREAK
						
						CASE CID_SHOOTING_RANGE_HELP
							Execute_Shooting_Range_Help()
						BREAK
						
						
						// Player reset flow code IDs.
						CASE CID_RESET_PLAYER_VARIATION_MICHAEL
							Execute_Reset_Player_Variation(CHAR_MICHAEL)
						BREAK
						
						CASE CID_RESET_PLAYER_VARIATION_FRANKLIN
							Execute_Reset_Player_Variation(CHAR_FRANKLIN)
						BREAK
						
						CASE CID_RESET_PLAYER_VARIATION_TREVOR
							Execute_Reset_Player_Variation(CHAR_TREVOR)
						BREAK
						
						// Misc flow code IDs.
						CASE CID_START_EMAILS
							Execute_Start_Emails()
						BREAK
						
						CASE CID_STRETCH_TEXT_SENT
							Execute_Stretch_Text_Sent()
						BREAK
						
						CASE CID_QUEUE_BAGGER_TEXT_AND_UNLOCK_YETARIAN
							Execute_Queue_Bagger_Unlock_Text()
						BREAK
						
						CASE CID_AGENCY_HEIST_CALLS_COMPLETE
							Execute_Agency_Heist_Calls_Complete()
						BREAK
						
						CASE CID_FRANKLIN_UNLOCK_BAGGER
							Execute_Franklin_Unlock_Bagger()
						BREAK
						
						CASE CID_FRANKLIN_UNLOCK_HILLS_SAVEHOUSE
							Execute_Franklin_Unlock_Hills_Savehouse()
						BREAK
						
						CASE CID_FAMILY_SETUP_CARSTEAL4
							Set_Current_Event_For_FamilyMember(FM_FRANKLIN_LAMAR,	FAMILY_MEMBER_BUSY)
							Set_Current_Event_For_FamilyMember(FM_FRANKLIN_STRETCH,	FAMILY_MEMBER_BUSY)
						BREAK
						
						CASE CID_FAMILY_CLEANUP_CARSTEAL4
							Set_Current_Event_For_FamilyMember(FM_FRANKLIN_LAMAR,	NO_FAMILY_EVENTS)
							Set_Current_Event_For_FamilyMember(FM_FRANKLIN_STRETCH,	NO_FAMILY_EVENTS)
						BREAK
						
						CASE CID_DOCKS_PRE_HEIST_TEXTS_COMPLETE
							Execute_Docks_Pre_Heist_Texts_Complete()
						BREAK
						
						CASE CID_FBI_3_CALLS_COMPLETE
							Execute_FBI3_Calls_Complete()
						BREAK
						
						CASE CID_FBI_4_CALLS_COMPLETE
							Execute_FBI4_Calls_Complete()
						BREAK
						
						CASE CID_TEXT_ASS1_REMINDER
							Execute_text_ass1_reminder()
						BREAK
						
						CASE CID_ASS1_REMINDER_DONE
							Execute_ASS1_REMINDER_DONE()
						BREAK
						
						CASE CID_ASS1_UNLOCKED
							Execute_ASS1_UNLOCKED()
						BREAK
						
						CASE CID_AGENCY_HEIST_PREP_CALLS_DONE
							Execute_Agency_Heist_Prep_Calls_Done()
						BREAK
						
						CASE CID_CHOP_CALL_COMPLETE
							Execute_Chop_Unlock_Call_Complete()
						BREAK
						
						CASE CID_MIC4_COMMS_COMPLETE
							Execute_Michael4_Texts_Complete()
						BREAK
						
						CASE CID_TAXI_HAILING_DISABLE
							DISABLE_TAXI_HAILING(TRUE)
						BREAK
						
						CASE CID_TAXI_HAILING_ENABLE
							DISABLE_TAXI_HAILING(FALSE)
						BREAK
						
						CASE CID_ACTIVATE_HELIPAD_SAVE_GARAGES
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HELIPAD_MICHAEL, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HELIPAD_FRANKLIN, TRUE)
						BREAK
						
						CASE CID_ACTIVATE_MARINA_SAVE_GARAGES
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_MARINA_MICHAEL, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_MARINA_FRANKLIN, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_MARINA_TREVOR, TRUE)
						BREAK
						
						CASE CID_ACTIVATE_HANGAR_SAVE_GARAGES
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HANGAR_MICHAEL, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HANGAR_FRANKLIN, TRUE)
							//SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HANGAR_TREVOR, TRUE) // Trevor no longer buys this one
						BREAK
						
						CASE CID_ACTIVATE_CAR_SAVE_GARAGES
							ADD_HELP_TO_FLOW_QUEUE("AM_H_GARAGEP", FHP_MEDIUM, 0)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_MICHAEL, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_FRANKLIN, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_TREVOR, TRUE)
						BREAK
						
						CASE CID_FLASH_RANDOM_CHAR_BLIPS
							Execute_Flash_Random_Char_Blips()
						BREAK
						
						CASE CID_FLASH_RANDOM_EVENT_BLIP
							Execute_Flash_Random_Event_Blip()
						BREAK
						
						CASE CID_FLASH_SEA_RACE_BLIP
							Execute_Flash_Sea_Race_Blip()
						BREAK
						
						CASE CID_LAMAR_TREVOR_FRIEND_UNLOCK
							Execute_Lamar_Trevor_Friend_Unlock()
						BREAK
						
						CASE CID_MICHAEL_ADD_MARTIN_CONTACT
							Execute_michael_Add_martin_Contact()
						BREAK
						
						CASE CID_TREVOR_ADD_MARTIN_CONTACT
							Execute_Trevor_Add_martin_Contact()
						BREAK
						
						CASE CID_TREVOR_ADD_PATRICIA_CONTACT
							Execute_Trevor_Add_Patricia_Contact()
						BREAK
						
						CASE CID_UNLOCK_AIR_VEHICLE_PARACHUTE
							Execute_Unlock_Air_Vehicle_Parachute()
						BREAK
						
						CASE CID_UNLOCK_WATER_VEHICLE_SCUBA_GEAR
							Execute_Unlock_Water_Vehicle_Scuba_Gear()
						BREAK
						
						CASE CID_UNLOCK_LIFEHACK_CREW
							Execute_Unlock_Lifehack_Crew_Member()
						BREAK
						
						CASE CID_UNLOCK_CHEF_CREW
							Execute_Unlock_Chef_Crew_Member()
						BREAK
						
						CASE CID_EMAIL_HEIST_MONEY_JEWEL
							Execute_Jewel_heist_email()
						BREAK
						
						CASE CID_CREDIT_HEIST_MONEY_JEWEL
							Execute_Credit_Heist_Money_Jewel()
						BREAK
						
						CASE CID_CREDIT_HEIST_MONEY_RURAL
							Execute_Credit_Heist_Money_Rural()
						BREAK
						
						CASE CID_FRANK_AGENCY_EMAIL
							Execute_Frank_email_agency_heist()
						BREAK
						
						CASE CID_CREDIT_HEIST_MONEY_AGENCY
							Execute_Credit_Heist_Money_agency()
						BREAK
						
						CASE CID_SEND_FINALE_HEIST_EMAIL
							execute_CID_SEND_FINALE_HEIST_EMAIL()
						BREAK
						
						CASE CID_CREDIT_HEIST_MONEY_FINALE
							Execute_Credit_Heist_Money_Finale()
						BREAK
						
						CASE CID_FAM5_JIMMYTAKE
							Execute_Fam5_JimmyTake()
							UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_16_SILVERLAKE", "MIRRORPARK_LOCKED")
							SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_UNLOCK_SHINE_A_LIGHT)
						BREAK
						
						CASE CID_ME_AMANDA_FAIL_MONEY_CHECK
							Execute_ME_Amanda_Fail_Money_Check()
						BREAK
						
						CASE CID_ME_JIMMY_FAIL_MONEY_CHECK
							Execute_ME_Jimmy_Fail_Money_Check()
						BREAK
						
						CASE CID_ME_TRACEY_FAIL_MONEY_CHECK
							Execute_ME_Tracey_Fail_Money_Check()
						BREAK
						
						CASE CID_AUTO_SWITCH_FRANKLIN_AH2
							Execute_Auto_Switch_Franklin_AH2()
						BREAK
						
						CASE CID_TREV2_OSCAR_PAY_TREVOR
							Execute_Trev2_Oscar_Pay_Trevor()
						BREAK
						
						CASE CID_UNLOCK_SHOPS_POST_LESTER_1A
							Execute_Unlock_Shops_Post_Lester_1A()
							Execute_Activate_RE_Simeon_Yetarian()
						BREAK
						
						CASE CID_RURAL_HEIST_PREP_CALLS_DONE
							Execute_Rural_Heist_Prep_Calls_Done()
						BREAK
						
						CASE CID_INITIALISE_BANK_DATA
							BANK_INITIALISE_STARTING_BALANCES(TRUE) 
						BREAK
						
						CASE CID_INITIALISE_STAT_OFFSETS
							Execute_Initialise_Player_Stat_Offsets() 
						BREAK
	
						CASE CID_ACTIVATE_CABLECARS
							Execute_Activate_Cablecars()
						BREAK
						
						CASE CID_ACTIVATE_CARWASHES
							Execute_Activate_Carwashes()
						BREAK
	
						CASE CID_ACTIVATE_FAIRGROUND
							Execute_Activate_Fairground()
						BREAK
						
						CASE CID_UNLOCK_SUBMERSIBLE
							Execute_Unlock_Submersible()
						BREAK
						
						CASE CID_UNLOCK_CREW_PRE_RURAL_HEIST
							Execute_Unlock_Crew_Pre_Rural_Heist()
						BREAK
						
						CASE CID_UNLOCK_CREW_PRE_AGENCY_HEIST
							Execute_Unlock_Crew_Pre_Agency_Heist()
						BREAK
						
						CASE CID_UNLOCK_CREW_PRE_FINALE_HEIST
							Execute_Unlock_Crew_Pre_Finale_Heist()
						BREAK
						
						CASE CID_UNLOCK_LOST_HANGAR_POST_TREV2
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, TRUE)
							SET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, VEHGEN_S_FLAG_ACQUIRED, TRUE)
							SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HANGAR_TREVOR, TRUE)
							SET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WEB_HANGAR_TREVOR, VEHGEN_S_FLAG_ACQUIRED, TRUE)
						BREAK
						
						CASE CID_UNLOCK_LOST_HANGAR_TEXT
							Execute_highlight_lost_hanger()							
						BREAK
						
						CASE CID_UNLOCK_JEWEL_HEIST_OUTFITS
							Execute_Unlock_Jewel_Heist_Outfits()
						BREAK
						
						CASE CID_SEND_BIG_SCORE_PREPC_EMAIL
							Execute_Send_Big_Score_PrepC_Email()
						BREAK
						
						CASE CID_BIG_SCORE_PREPC_EMAIL_SENT
							Execute_Big_Score_PrepC_Email_Sent()
						BREAK
						
						CASE CID_BIG_SCORE_PREPA_COMPLETED
							Execute_Big_Score_Prep_A_Completed()
						BREAK
						
						CASE CID_BIG_SCORE_PREPB_COMPLETED
							Execute_Big_Score_Prep_B_Completed()
						BREAK

						CASE CID_BIG_SCORE_PREPC_COMPLETED
							Execute_Big_Score_Prep_C_Completed()
						BREAK
						
						CASE CID_BIG_SCORE_PREPD_COMPLETED
							Execute_Big_Score_Prep_D_Completed()
						BREAK
						
						CASE CID_BIG_SCORE_PREPE_COMPLETED
							Execute_Big_Score_Prep_E_Completed()
						BREAK
						
						CASE CID_BIG_SCORE_PREP_PART1_CALLS_DONE
							Execute_Big_Score_Prep_Part1_Calls_Done()
						BREAK
						
						CASE CID_BIG_SCORE_PREP_GAUNT1_CALL_DONE 
							Execute_BIG_SCORE_PREP_GAUNT1_CALL_DONE()
						BREAK
						
						CASE CID_BIG_SCORE_PREP_GAUNT2_CALL_DONE
							Execute_BIG_SCORE_PREP_GAUNT2_CALL_DONE()
						BREAK
						
						CASE CID_BIG_SCORE_PREP_PART2_CALLS_DONE
							Execute_Big_Score_Prep_Part2_Calls_Done()
						BREAK
						
						CASE CID_BIG_SCORE_PREP_PART3_CALLS_DONE
							Execute_Big_Score_Prep_Part3_Calls_Done()
						BREAK
						
						CASE CID_POST_PROLOGUE_FRANKLIN_POS_CHECK
							Execute_Post_Prologue_Franklin_Position_Check()
						BREAK
						
						CASE CID_UNLOCK_TATTOO_SHOPS_POST_TREV1
							Execute_Unlock_Tattoo_Shops_Post_Trevor1()
						BREAK
						
						CASE CID_FAST_TRACK_BZ_GAS_PREP_UNLOCK
							Execute_Fast_Track_BZ_Gas_Prep_Unlock()
						BREAK
						
						CASE CID_UNLOCK_BZ_GAS_PREP
							Execute_Unlock_BZ_Gas_Prep()
						BREAK

						//CASE CID_POST_DOCKS_PREP1
						//	Execute_Post_Docks_Prep1()
						//BREAK
						
						CASE CID_FAST_TRACK_JIMMY_EVENT
							Execute_Fast_Track_Jimmy_Event()
						BREAK
						
						CASE CID_FAST_TRACK_TRAIN_PREP_UNLOCK
							Execute_Fast_Track_Train_Prep_Unlock()
						BREAK
						
						CASE CID_FAST_TRACK_DOCKS_HEIST_UNLOCK
							Execute_Fast_Track_Docks_Heist_Unlock()
						BREAK
						
						CASE CID_FAMILY1_FRANKLIN_GATE_UNLOCK
							Execute_Family1_Franklin_Gate_Unlock()
						BREAK
						
						CASE CID_UNLOCK_EXILE1_PICKUPS
							Execute_Unlock_Exile1_Pickups()
						BREAK
						
						CASE CID_UNLOCK_LAMAR_1
							Execute_Unlock_Lamar_1()
						BREAK
						
						CASE CID_SET_LONG_AMANDA_TIMER
							Execute_Set_Long_Amanda_Timer()
						BREAK
						
						CASE CID_SET_LONG_JIMMY_TIMER
							Execute_Set_Long_Jimmy_Timer()
						BREAK
						
						CASE CID_SET_LONG_TRACEY_TIMER
							Execute_Set_Long_Tracey_Timer()
						BREAK
						
						CASE CID_SET_LONG_LESTER_TIMER
							Execute_Set_Long_lester_Timer()
						BREAK
						
						CASE CID_QUEUE_MIKE_DEAD_JIMMY_CALL2
							Execute_Queue_Mike_Dead_Jimmy_Call2()
						BREAK
						
						CASE CID_UNLOCK_END_GAME_OUTFITS
							Execute_Unlock_End_Game_Outfits()
						BREAK
						
						CASE CID_HEIST_CREW_UNLOCKED_HELP_SEEN
							SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_HEIST_CREW_UNLOCKED)
						BREAK

						CASE CID_DEACTIVATE_MICHAEL_TENNIS
							Execute_Set_Michael_Tennis_Active(FALSE)
						BREAK
						
						CASE CID_REACTIVATE_MICHAEL_TENNIS
							Execute_Set_Michael_Tennis_Active(TRUE)
						BREAK
						
						CASE CID_DEACTIVATE_EXILE_BLIPS
							Execute_Set_Exile_Blips_Active(FALSE)
						BREAK
						
						CASE CID_REACTIVATE_EXILE_BLIPS
							Execute_Set_Exile_Blips_Active(TRUE)
						BREAK
						
						CASE CID_RANDOM_EVENT_HELP_DISPLAYED
							SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_RE_ENCOUNTER)
						BREAK
						
						CASE CID_RE_STAT_BOOST_HELP_DISPLAYED
							SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_RE_STAT_BOOSTS)
						BREAK
						
						CASE CID_AFFECT_STOCKS_HELP_DISPLAYED
							SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_AFFECT_STOCKS)
						BREAK
						
						CASE CID_DISABLE_LOST_SCENARIO_GROUP
							Execute_Set_Lost_Scenario_Group_Active(FALSE)
						BREAK
						
						CASE CID_ENABLE_LOST_SCENARIO_GROUP
							Execute_Set_Lost_Scenario_Group_Active(TRUE)
						BREAK
						
						CASE CID_MARTIN1_CALLS_DONE
							Execute_Martin1_Calls_Done()
						BREAK
						
						CASE CID_QUEUE_SOL1_CALL_TO_UNLOCK_MARTIN1
							Execute_Queue_Sol1_Call_To_Unlock_Martin1()
						BREAK
						
						CASE CID_QUEUE_CAR2_CALL_TO_UNLOCK_MARTIN1
							Execute_Queue_Car2_Call_To_Unlock_Martin1()
						BREAK
						
						CASE CID_CANCEL_AMANDA_MICHAEL_EVENT
							Execute_Cancel_Amanda_Michael_Event()
						BREAK
						
						CASE CID_CAR1TEXT_UNLOCK
							Execute_Cid_Car1text_Unlock()
						BREAK
						
						CASE CID_EXTEND_PATRICIA_CALL_TIMERS
							Execute_Extend_Patricia_Call_Timers()
						BREAK
						
						CASE CID_UNLOCK_AGENCY_HEIST_2
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_2_UNLOCKED, TRUE)
						BREAK
						
						CASE CID_MAKE_RE_BURIAL_PAYMENT
							Execute_Make_RE_Burail_Payment()
						BREAK
						
						CASE CID_EXILE_INCREASE_M_WARN
							Execute_exile_increase_m_warn()
						BREAK
						
						CASE CID_EXILE_INCREASE_T_WARN
							Execute_exile_increase_t_warn()
						BREAK
						
						CASE CID_CLEANUP_FRAN_HOUSE_POST_ARM1
							Execute_Cleanup_Franklin_House_Post_Arm1()
						BREAK
						
						CASE CID_BLOCK_FRANKLIN_MISSIONS_POST_TREV1
							Execute_Block_Franklin_Missions_Post_Trev1()
						BREAK
												
						CASE CID_GIVE_NG_PREORDER_REWARD
							Execute_Give_NG_Preorder_Reward()
						BREAK
						
						#IF IS_JAPANESE_BUILD
						CASE CID_GIVE_NG_JP_PREORDER_REWARD
							Execute_Give_Ng_Jp_Preorder_Reward()
						BREAK
						#ENDIF
						// Random Char activation code IDs
						CASE CID_ABIGAIL1_FOUND_SUB_WRECKAGE
							Execute_Abigail1_Found_Sub_Wreckage()
						BREAK
						
						CASE CID_BARRY3_SEND_TEXT
							Execute_Barry3_Send_Text()
						BREAK
						
						CASE CID_BARRY3_TEXT_RECEIVED
							Execute_Barry3_Text_Received()
						BREAK
						
						CASE CID_BARRY4_SEND_TEXT
							Execute_Barry4_Send_Text()
						BREAK
						
						CASE CID_BARRY4_TEXT_RECEIVED
							Execute_Barry4_Text_Received()
						BREAK
						
						CASE CID_DREYFUSS1_COMPLETE_LETTER_SCRAPS
							Execute_Dreyfuss1_Complete_Letter_Scraps()
						BREAK
						
						CASE CID_EPSILON_6_TEXT_RECIEVED
							Execute_Epsilon_6_Text_Received()
						BREAK

						CASE CID_EXTREME2_ACQUIRE_OUTFIT
							Execute_Extreme2_Acquire_Outfit()
						BREAK
						
						CASE CID_EXTREME2_DOM_TEXT_RECEIVED
							Execute_Extreme2_Dom_Text_Received()
						BREAK
						
						CASE CID_EXTREME3_SEND_DOM_TEXT
							Execute_Extreme3_Send_Dom_Text()
						BREAK
						
						CASE CID_EXTREME3_DOM_TEXT_RECEIVED
							Execute_Extreme3_Dom_Text_Received()
						BREAK
						
						CASE CID_HUNTING1_SEND_TEXT
							Execute_Hunting1_Send_Text()
						BREAK
						
						CASE CID_HUNTING1_TEXT_RECEIVED
							Execute_Hunting1_Text_Received()
						BREAK

						CASE CID_JOSH1_SIGNS_DESTROYED
							Execute_Josh1_Signs_Destroyed()
						BREAK
						
						CASE CID_NIGEL1_SEND_EMAIL
							Execute_Nigel1_Send_Email()
						BREAK
						
						CASE CID_NIGEL1_EMAIL_RECEIVED
							Execute_Nigel1_Email_Received()
						BREAK
						
						CASE CID_OMEGA1_COMPLETE_SPACESHIP_PARTS
							Execute_Omega1_Complete_Spaceship_Parts()
						BREAK

						CASE CID_PAPARAZZO1_SEND_WILDLIFE_EMAIL
							Execute_Paparazzo1_Send_Wildlife_Email()
						BREAK
						
						CASE CID_PAPARAZZO1_EMAIL_RECEIVED
							Execute_Paparazzo1_Email_Received()
						BREAK
						
						CASE CID_PAPARAZZO3_SEND_TEXT
							Execute_Paparazzo3_Send_Text()
						BREAK
						
						CASE CID_PAPARAZZO3_TEXT_RECEIVED
							Execute_Paparazzo3_Text_Received()
						BREAK
						
						CASE CID_TONYA3_SEND_TEXT
							Execute_Tonya3_Send_Text()
						BREAK
						
						CASE CID_TONYA3_TEXT_RECEIVED
							Execute_Tonya3_Text_Received()
						BREAK
						
						CASE CID_TONYA4_SEND_TEXT
							Execute_Tonya4_Send_Text()
						BREAK
						
						CASE CID_TONYA4_TEXT_RECEIVED
							Execute_Tonya4_Text_Received()
						BREAK
						
						CASE CID_UNLOCK_NEXT_STREET_RACE
							Execute_Unlock_Next_Street_Race()
						BREAK
						
						CASE CID_DEACTIVATE_RANDOM_CHARS_MICHAEL
							Execute_Deactivate_Random_Chars_Michael()
						BREAK
						
						CASE CID_DEACTIVATE_RANDOM_CHARS_TREVOR
							Execute_Deactivate_Random_Chars_Trevor()
						BREAK

						//stock event CIDs
						CASE CID_STOCK_EVENT_LESTER1
							Execute_Stock_Event_Lester1()
							BREAK
						CASE CID_STOCK_EVENT_ASSASSIN1
							Execute_Stock_Event_Assassin1()
							BREAK
						CASE CID_STOCK_EVENT_ASSASSIN2
							Execute_Stock_Event_Assassin2()
							BREAK
						CASE CID_STOCK_EVENT_ASSASSIN3
							Execute_Stock_Event_Assassin3()
							BREAK
						CASE CID_STOCK_EVENT_ASSASSIN4
							Execute_Stock_Event_Assassin4()
							BREAK
						CASE CID_STOCK_EVENT_ASSASSIN5
							Execute_Stock_Event_Assassin5()
							BREAK
						CASE CID_STOCK_EVENT_RCHITCH1
							Execute_Stock_Event_RCHitch1()
							BREAK
							
						// Random event completion CCIDs
						CASE CID_RE_CARSTEAL2_REWARD
 							Execute_CarSteal2_Reward()
							BREAK
							
						CASE CID_RE_RESUCE_HOSTAGE_REWARD
 							Execute_resuce_hostage_Reward()
							BREAK
							
						CASE CID_RE_BIKE_THEFT_REWARD
 							Execute_bike_theft_Reward() 
							BREAK
							
						DEFAULT
							SWITCH(INT_TO_ENUM(CC_CodeID, index))
							
							CASE CID_HOSPITAL_CHARGE_RH
	 							Execute_Hospital_Charge_RH() 
								BREAK	
							CASE CID_HOSPITAL_CHARGE_SC
	 							Execute_Hospital_Charge_SC() 
								BREAK	
							CASE CID_HOSPITAL_CHARGE_DT
	 							Execute_Hospital_Charge_DT() 
								BREAK	
							CASE CID_HOSPITAL_CHARGE_SS
	 							Execute_Hospital_Charge_SS() 
								BREAK	
							CASE CID_HOSPITAL_CHARGE_PB
	 							Execute_Hospital_Charge_PB() 
							BREAK	
						
							CASE CID_TREVOR_UNLOCK_BLAZER3
								Execute_Trevor_Unlock_Blazer3()
							BREAK
							
							CASE CID_FBI4_P3_DONE_REMINDER
								CANCEL_COMMUNICATION(TEXT_FIB4_P3_M_REMINDER)
								CANCEL_COMMUNICATION(TEXT_FIB4_P3_FT_REMINDER)
							BREAK
							
							CASE CID_CAR3_REM_DONE
								CANCEL_COMMUNICATION(TEXT_CAR3_F_REM)
								CANCEL_COMMUNICATION(TEXT_CAR3_MT_REM)
							BREAK
							
							//Called after Michael 1 to stop Trevor replying
							//to Brad's emails now that he knows he's dead
							CASE CID_END_BRAD_EMAIL_THREAD
								END_EMAIL_THREAD(ENUM_TO_INT(BRAD_EMAIL_TRV1))
								END_EMAIL_THREAD(ENUM_TO_INT(BRAD_EMAIL_FIB1))
							BREAK
							
							CASE CID_COM_CHECK_VISITED_SITE
								Execute_Check_COM_Visited_Site()
							BREAK
							
							CASE CID_DIRECTOR_HELP_STORY_SEEN
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_DIRECTOR_MODE_STORY)
							BREAK
							
							CASE CID_DIRECTOR_HELP_HEISTS_SEEN
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_DIRECTOR_MODE_HEISTS)
							BREAK
							
							CASE CID_DIRECTOR_HELP_SPECIAL_SEEN
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_DIRECTOR_MODE_SPECIAL)
							BREAK
							
							CASE CID_DIRECTOR_HELP_ANIMALS_SEEN
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_DIRECTOR_MODE_ANIMALS)
							BREAK
							
							//Debug only routines.
							#IF IS_DEBUG_BUILD
								CASE CID_DEBUG_COMPLETE_TONYA1
									Execute_Debug_Complete_Tonya1()
								BREAK
								
								CASE CID_DEBUG_UNLOCK_WILDLIFE_PHOTO
									Execute_Debug_Unlock_Wildlife_Photo()
								BREAK
								
								CASE CID_DEBUG_UNLOCK_MONKEY_PHOTO_RE
									Execute_Debug_Unlock_Monkey_Photo_RE()
									// unlock all player character garages									
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_FRANKLIN, VEHGEN_S_FLAG_AVAILABLE, TRUE )
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_FRANKLIN, VEHGEN_S_FLAG_ACQUIRED, TRUE )
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_MICHAEL, VEHGEN_S_FLAG_AVAILABLE, TRUE )
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_MICHAEL, VEHGEN_S_FLAG_ACQUIRED, TRUE )
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_TREVOR, VEHGEN_S_FLAG_AVAILABLE, TRUE )
									SET_VEHICLE_GEN_SAVED_FLAG_STATE( VEHGEN_WEB_CAR_TREVOR, VEHGEN_S_FLAG_ACQUIRED, TRUE )
								BREAK
								
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT1 // STAGE_CARVING_2
									Execute_Debug_Unlock_Murder_Mystery( 0 )
								BREAK
								
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT2 // STAGE_CARVING_3
									Execute_Debug_Unlock_Murder_Mystery( 1 )
								BREAK
								
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT3 // STAGE_CARVING_4
									Execute_Debug_Unlock_Murder_Mystery( 2 )
								BREAK
								
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT4 // STAGE_SEA
									Execute_Debug_Unlock_Murder_Mystery( 3 )
								BREAK
							
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT5 // STAGE_LETTER
									Execute_Debug_Unlock_Murder_Mystery( 4 )
								BREAK
							
								CASE CID_DEBUG_COMPLETE_MURDER_MYSTERY_PT6 // STAGE_MINE
									Execute_Debug_Unlock_Murder_Mystery( 5 )
								BREAK
							
								CASE CID_DEBUG_UNLOCK_BASEJUMPING
									Execute_Debug_Unlock_Basejumping()
								BREAK
								
								CASE CID_DEBUG_UNLOCK_TRIATHLON
									Execute_Debug_Unlock_Triathlons()
								BREAK
								
								CASE CID_DEBUG_UNLOCK_TRAFFICKING
									Execute_Debug_Unlock_Trafficking()
								BREAK
								
								CASE CID_DEBUG_UNLOCK_HUNTING
									Execute_Debug_Unlock_Hunting()
								BREAK
								
								CASE CID_DEBUG_GIVE_PREP_BOILER_SUITS
									Execute_Debug_Give_Prep_Boiler_Suits()
								BREAK
								
								CASE CID_DEBUG_COMPLETE_FBI4_PREP3
									Execute_Debug_Complete_FBI4_Prep3()
								BREAK
								
								CASE CID_DEBUG_COMPLETE_AGENCY_PREP2
									Execute_Debug_Complete_Agency_Prep2()
								BREAK
								
								CASE CID_DEBUG_COMPLETE_FINALE_PREPE
									Execute_Debug_Complete_Finale_PrepE()
								BREAK
								
								CASE CID_DEBUG_CANCEL_BS_HACKER_EMAILS
									Execute_Debug_Cancel_Big_Score_Hacker_Emails()
								BREAK
								
								CASE CID_DEBUG_JEWEL_BOARD_DISPLAY_GROUPS_ON
									Execute_Debug_Jewel_Board_Display_Groups_On()
								BREAK
								
								CASE CID_DEBUG_DOCKS_BOARD_DISPLAY_GROUPS_ON
									Execute_Debug_Docks_Board_Display_Groups_On()
								BREAK
								
								CASE CID_DEBUG_RURAL_BOARD_DISPLAY_GROUPS_ON
									Execute_Debug_Rural_Board_Display_Groups_On()
								BREAK
								
								CASE CID_DEBUG_AGENCY_BOARD_DISPLAY_GROUPS_ON
									Execute_Debug_Agency_Board_Display_Groups_On()
								BREAK
								
								CASE CID_DEBUG_FINALE_BOARD_DISPLAY_GROUPS_ON
									Execute_Debug_Finale_Board_Display_Groups_On()
								BREAK
							#ENDIF	
							
							ENDSWITCH
						BREAK
						
					ENDSWITCH
					
					//Unflag this code ID now that it has been executed.
					g_savedGlobals.sCodeControlData.bRunCodeID[index] = FALSE
					
					CPRINTLN(DEBUG_CODEID, "Executed ",Get_Debug_String_For_Communication_Code_ID(INT_TO_ENUM(CC_CodeID, index)), ".")
				ENDIF
			ENDIF
		ENDREPEAT
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT