//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      
//      SCRIPT NAME     :   main_persistent.sc                                      
//      AUTHOR          :   Keith McLeman                                               
//      DESCRIPTION     :   This script will always remain active, even during MP.          
//                          It will handle the transition between SP and MP 
//                          from the SP side - tidying up any SP stuff on entering MP   
//                          and then reactivating any SP stuff on return from MP,       
//                          including the SP main.                                              
//                                                                                      
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "global_block_defines.sch"

USING "commands_brains.sch"
USING "commands_script.sch"
USING "script_player.sch"

USING "player_ped_public.sch"
USING "selector_public.sch"
USING "shop_public.sch"
USING "net_transition_sessions.sch"
USING "leader_board_common.sch"
USING "fmmc_corona_controller.sch"
USING "properties_public.sch"
USING "FM_Post_Mission_Cleanup.sch"
USING "Transition_Controller.sch"
USING "streamed_scripts.sch"
USING "weapons_public.sch"
USING "script_misc.sch"
USING "net_clouds.sch"
USING "beast_secret_fight.sch"
USING "net_process_events.sch"

#IF FEATURE_FREEMODE_ARCADE
USING "mphud_landing_page.sch"
#ENDIF

BOOL    m_launchSPMain  = FALSE

STRUCT TRANSITION_SESSION_VARS
    BOOL    bRequestScripts
    BOOL    bRequestSpinner
    BOOL    bRequestScriptsJip
    BOOL    bToggelRender
ENDSTRUCT

TRANSITION_SESSION_VARS sTranVars

#IF IS_DEBUG_BUILD
    BOOL m_doForceCleanupSPtoMP = FALSE
#ENDIF
 
BOOL bShownMarstonTicker
 
CONST_INT  BIT_SP_CLEANUP_COMPLETED            0
CONST_INT  BIT_SP_CLEANUP_RAN_FORCE_CLEANUP    1
CONST_INT  BIT_SP_CLEANUP_RAN_MISSION_CLEANUP  2
CONST_INT  BIT_SP_CLEANUP_STOPPED_CUTSCENES    3
 
INT iSPCleanupStateBitset = 0
INT iFramesWaitingForCutscenes
INT iClockHours
 
BOOL bPlayerHasScopedWeapon
WEAPON_TYPE ePlayerCurWeapon

#IF IS_DEBUG_BUILD 
    WIDGET_GROUP_ID main_pers_widget
    BOOL bForceMarstonTicker
    BOOL bForceMPSpecialTicker
    BOOL bForceMpCollectorsTicker
    BOOL bPrintMartsonTickerWidgets
    INT iStackSizeForScriptLaunch = 2
    TEXT_WIDGET_ID twScriptName
    BOOL bKillScriptsWithName
    BOOL bLaunchScriptWithName
    BOOL bRefreshBgNow, bRefreshTuneNow
	BOOL bDebugScriptRequestedForRelaunch
	INT iErrorListenerShutdownTime
	BOOL bHaveLaunchedErrorListener = FALSE

#ENDIF

CONST_INT ciERROR_LISTENER_RELAUNCH_DELAY 1000

 
// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

FUNC INT GET_STACK_SIZE_FROM_INT_WIDGET()
    SWITCH iStackSizeForScriptLaunch
        CASE 0  RETURN MICRO_STACK_SIZE                 BREAK//(128)  // 20
        CASE 1  RETURN MINI_STACK_SIZE                  BREAK//(512)  // 20
        CASE 2  RETURN DEFAULT_STACK_SIZE               BREAK//(1024) // 68 ?
        CASE 3  RETURN SPECIAL_ABILITY_STACK_SIZE       BREAK//(1828) // 5
        CASE 4  RETURN FRIEND_STACK_SIZE                BREAK//(2020) // 10
        CASE 5  RETURN SHOP_STACK_SIZE                  BREAK//(2024) // 6
        CASE 6  RETURN CELLPHONE_STACK_SIZE             BREAK//(2552) // 2
        CASE 7  RETURN VEHICLE_SPAWN_STACK_SIZE         BREAK//(2556) // 1
        CASE 8  RETURN PAUSE_MENU_SCRIPT_STACK_SIZE     BREAK//(3076) // 2
        CASE 9  RETURN APP_INTERNET_STACK_SIZE          BREAK//(3584) // 1
        CASE 10 RETURN MULTIPLAYER_MISSION_STACK_SIZE   BREAK//(3650) // 9
        CASE 11 RETURN CONTACTS_APP_STACK_SIZE          BREAK//(3800) // 1
        CASE 12 RETURN DEBUG_SCRIPT_STACK_SIZE          BREAK//(4080) // 1
        CASE 13 RETURN INTERACTION_MENU_STACK_SIZE      BREAK//(4150) // 1
        CASE 14 RETURN SCRIPT_XML_STACK_SIZE            BREAK//(8344) // 3
        CASE 15 RETURN INGAMEHUD_STACK_SIZE             BREAK//(4600) //1 //Brenda Needs this for the ingame hud. 
        CASE 16 RETURN PROPERTY_INT_STACK_SIZE          BREAK//(10024) // 1     //4626
        CASE 17 RETURN TRANSITION_STACK_SIZE            BREAK//(8032) //1
        CASE 18 RETURN FMMC_LAUNCHER_STACK_SIZE         BREAK//(14000) // 1
        CASE 19 RETURN MULTIPLAYER_FREEMODE_STACK_SIZE  BREAK//(21000) // 1
        CASE 20 RETURN MISSION_STACK_SIZE               BREAK//(15000) // 1
        CASE 21 RETURN DEBUG_MENU_STACK_SIZE            BREAK//(32000) // 1
        CASE 22 RETURN SHOP_CONTROLLER_STACK_SIZE       BREAK//(2200) // 1
    ENDSWITCH
    RETURN DEFAULT_STACK_SIZE
ENDFUNC

FUNC STRING GET_BAIL_RESPONSE_STRING(JOIN_RESPONSE_CODE aResponse)

    SWITCH aResponse
        CASE RESPONSE_ACCEPT RETURN "RESPONSE_ACCEPT"
        CASE RESPONSE_DENY_UNKNOWN RETURN "RESPONSE_DENY_UNKNOWN"
        CASE RESPONSE_DENY_WRONG_SESSION RETURN "RESPONSE_DENY_WRONG_SESSION"
        CASE RESPONSE_DENY_NOT_HOSTING RETURN "RESPONSE_DENY_NOT_HOSTING"
        CASE RESPONSE_DENY_NOT_READY RETURN "RESPONSE_DENY_NOT_READY"
        CASE RESPONSE_DENY_BLACKLISTED RETURN "RESPONSE_DENY_BLACKLISTED"
        CASE RESPONSE_DENY_INVALID_REQUEST_DATA RETURN "RESPONSE_DENY_INVALID_REQUEST_DATA"
        CASE RESPONSE_DENY_INCOMPATIBLE_ASSETS RETURN "RESPONSE_DENY_INCOMPATIBLE_ASSETS"
        CASE RESPONSE_DENY_SESSION_FULL RETURN "RESPONSE_DENY_SESSION_FULL"
        CASE RESPONSE_DENY_WRONG_VERSION RETURN "RESPONSE_DENY_WRONG_VERSION"
        CASE RESPONSE_DENY_NOT_VISIBLE RETURN "RESPONSE_DENY_NOT_VISIBLE"
        CASE RESPONSE_DENY_BLOCKING RETURN "RESPONSE_DENY_BLOCKING"
        CASE RESPONSE_DENY_AIM_PREFERENCE RETURN "RESPONSE_DENY_AIM_PREFERENCE"
        CASE RESPONSE_DENY_CHEATER RETURN "RESPONSE_DENY_CHEATER"
        CASE RESPONSE_DENY_TIMEOUT RETURN "RESPONSE_DENY_TIMEOUT"
        CASE RESPONSE_DENY_DATA_HASH RETURN "RESPONSE_DENY_DATA_HASH"
        CASE RESPONSE_DENY_CREW_LIMIT   RETURN "RESPONSE_DENY_CREW_LIMIT"

        CASE RESPONSE_DENY_POOL_NORMAL   RETURN "RESPONSE_DENY_POOL_NORMAL"
        CASE RESPONSE_DENY_POOL_BAD_SPORT   RETURN "RESPONSE_DENY_POOL_BAD_SPORT"
        CASE RESPONSE_DENY_POOL_CHEATER   RETURN "RESPONSE_DENY_POOL_CHEATER"
        CASE RESPONSE_DENY_NOT_JOINABLE   RETURN "RESPONSE_DENY_NOT_JOINABLE"
        CASE RESPONSE_DENY_PRIVATE_ONLY   RETURN "RESPONSE_DENY_PRIVATE_ONLY"
        CASE RESPONSE_DENY_DIFFERENT_BUILD   RETURN "RESPONSE_DENY_DIFFERENT_BUILD"
        CASE RESPONSE_DENY_DIFFERENT_CONTENT_SETTING   RETURN "RESPONSE_DENY_DIFFERENT_CONTENT_SETTING"
        CASE RESPONSE_DENY_NOT_FRIEND   RETURN "RESPONSE_DENY_NOT_FRIEND"
    
        CASE RESPONSE_DENY_REPUTATION   RETURN "RESPONSE_DENY_REPUTATION"
        CASE RESPONSE_DENY_FAILED_TO_ESTABLISH   RETURN "RESPONSE_DENY_FAILED_TO_ESTABLISH"
    	CASE RESPONSE_DENY_PREMIUM			RETURN "RESPONSE_DENY_PREMIUM"
    
    
    ENDSWITCH
    RETURN ""
ENDFUNC
#ENDIF

/// PURPOSE:
///    Requests any persistent script, waits for them to stream in, then launches them
PROC Request_And_Launch_Persistent_Scripts_With_Wait()

    CPRINTLN(DEBUG_INIT, "Starting mode independent persistant scripts.")

    REQUEST_SCRIPT("social_controller")
    REQUEST_SCRIPT("cellphone_controller")
    REQUEST_SCRIPT("dialogue_handler")
    REQUEST_SCRIPT("shop_controller")
    REQUEST_SCRIPT("selector")    
    REQUEST_SCRIPT("InGameHud")
    REQUEST_SCRIPT("context_controller")
    REQUEST_SCRIPT("stats_controller")
    REQUEST_SCRIPT("achievement_controller")
    REQUEST_SCRIPT("building_controller")
      
    
    //Steve T - It may be the case that this needs to be persistent as certain minor tasks, something like shooting pigeons for example, that contribute
    //to 100 percent completion can be progressed within multiplayer as well as singleplayer.
    //REQUEST_SCRIPT("CompletionPercentage_controller")  - Moved to initial.sc on a trial basis... Steve T 02.05.13
    
   // #IF IS_DEBUG_BUILD
   //     REQUEST_SCRIPT("coordinate_recorder")
   // #ENDIF
                                                   
    WHILE NOT HAS_SCRIPT_LOADED("social_controller")
    OR  NOT HAS_SCRIPT_LOADED("cellphone_controller")   // if normal game mode check cellphone controller
    OR  NOT HAS_SCRIPT_LOADED("dialogue_handler")       // if normal game mode check dialogue_handler
    OR  NOT HAS_SCRIPT_LOADED("shop_controller")                            // if normal game mode check shop controller
    OR  NOT HAS_SCRIPT_LOADED("selector")               // if normal game mode check selector    
    OR NOT HAS_SCRIPT_LOADED("InGameHud")
    OR NOT HAS_SCRIPT_LOADED("context_controller")
    OR NOT HAS_SCRIPT_LOADED("stats_controller")
    OR NOT HAS_SCRIPT_LOADED("achievement_controller")
    OR NOT HAS_SCRIPT_LOADED("building_controller") // if normal game mode check building controller
   
  //  #IF IS_DEBUG_BUILD
  //     OR NOT HAS_SCRIPT_LOADED("coordinate_recorder")
  //  #ENDIF

        #IF IS_DEBUG_BUILD
            IF NOT HAS_SCRIPT_LOADED("cellphone_controller")
                CPRINTLN(DEBUG_INIT, "...waiting for - cellphone_controller")
            ENDIF
            
            IF  NOT HAS_SCRIPT_LOADED("dialogue_handler")
                CPRINTLN(DEBUG_INIT, "...waiting for - dialogue_handler")
            ENDIF
            
            IF  NOT HAS_SCRIPT_LOADED("selector")
                CPRINTLN(DEBUG_INIT, "...waiting for - selector")
            ENDIF
            
            IF NOT (HAS_SCRIPT_LOADED("social_controller"))
                CPRINTLN(DEBUG_INIT, "...waiting for - social_controller")
            ENDIF
            
            IF NOT (HAS_SCRIPT_LOADED("InGameHud"))
                CPRINTLN(DEBUG_INIT, "...waiting for - InGameHud")
            ENDIF
            
            IF  NOT HAS_SCRIPT_LOADED("shop_controller")
                CPRINTLN(DEBUG_INIT, "...waiting for - shop_controller")
            ENDIF
            
            IF NOT (HAS_SCRIPT_LOADED("context_controller"))
                CPRINTLN(DEBUG_INIT, "...waiting for - context_controller")
            ENDIF
          
            IF NOT (HAS_SCRIPT_LOADED("stats_controller"))
                CPRINTLN(DEBUG_INIT, "...waiting for - stats_controller")
            ENDIF

            IF NOT (HAS_SCRIPT_LOADED("achievement_controller"))
                CPRINTLN(DEBUG_INIT, "...waiting for - achievement_controller")
            ENDIF
            
            IF  NOT HAS_SCRIPT_LOADED("building_controller")
                CPRINTLN(DEBUG_INIT, "...waiting for - building_controller")
            ENDIF
            
        #ENDIF
        
        WAIT(0)
                
        REQUEST_SCRIPT("social_controller")      
        REQUEST_SCRIPT("cellphone_controller")
        REQUEST_SCRIPT("dialogue_handler")        
        REQUEST_SCRIPT("shop_controller")        
        REQUEST_SCRIPT("selector")        
        REQUEST_SCRIPT("InGameHud")
        REQUEST_SCRIPT("context_controller")
        REQUEST_SCRIPT("stats_controller")
        REQUEST_SCRIPT("achievement_controller")   
        REQUEST_SCRIPT("building_controller")
        
    ENDWHILE
    
    START_NEW_SCRIPT("cellphone_controller", DEFAULT_STACK_SIZE)
    START_NEW_SCRIPT("dialogue_handler", DEFAULT_STACK_SIZE)
    START_NEW_SCRIPT("selector", DEFAULT_STACK_SIZE)   
    START_NEW_SCRIPT("InGameHud", MULTIPLAYER_MISSION_STACK_SIZE ) //INGAMEHUD_STACK_SIZE )//DEFAULT_STACK_SIZE)
    START_NEW_SCRIPT("context_controller", DEFAULT_STACK_SIZE)
    
    // Kenneth R.
    // Setting up level check so that the following scripts only get launched in the gta5 level.
    // This prevents asserts when trying to use assets that have not been set up for the test levels.
    #IF IS_DEBUG_BUILD
    IF g_eCachedLevel = LEVEL_GTA5
    #ENDIF
    
        START_NEW_SCRIPT("social_controller", SPECIAL_ABILITY_STACK_SIZE)
        START_NEW_SCRIPT("stats_controller", DEFAULT_STACK_SIZE)
        START_NEW_SCRIPT("achievement_controller", DEFAULT_STACK_SIZE)
        START_NEW_SCRIPT("shop_controller", SHOP_CONTROLLER_STACK_SIZE)
        START_NEW_SCRIPT("building_controller", DEFAULT_STACK_SIZE)
        
    #IF IS_DEBUG_BUILD
    ENDIF
    #ENDIF
       
    CPRINTLN(DEBUG_INIT, "All mode independent persistant scripts started.")
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Requests the 'main' script, and launches it if it has streamed in
/// RETURN VALUE:
///    BOOL     TRUE if main loaded and launched, otherwise FALSE
FUNC BOOL Request_And_Launch_Main_Script()

    INT theScriptNameHash    
    theScriptNameHash = HASH("main")    
    
    // Always request the script so that there is a record of it in code being required by scripts
    REQUEST_SCRIPT_WITH_NAME_HASH(theScriptNameHash)
    IF NOT (HAS_SCRIPT_WITH_NAME_HASH_LOADED(theScriptNameHash))
        RETURN FALSE
    ENDIF   
    

    // Main script is in memory, so ensure it is not about to launch a second instance
    IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(theScriptNameHash) > 0)
        IF g_TransitionData.bCouldNotJoinNetworkGame = FALSE
          NET_NL()PRINTSTRING(" About to launch main thread but its already launched. Used to be an assert but is now VALID if NETWORK_BAIL Happened ")NET_NL()
        ENDIF
        RETURN TRUE
    ENDIF
        
    // Launch a new instance of main.sc
    //START_NEW_SCRIPT(theScriptName, DEFAULT_STACK_SIZE)   //Steve T. Needed to increase the stack size to handle cross session processing of MP Joblist into SP.
    START_NEW_SCRIPT_WITH_NAME_HASH(theScriptNameHash, MULTIPLAYER_MISSION_STACK_SIZE)
    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(theScriptNameHash)
    
#IF FEATURE_GEN9_STANDALONE
	// Launching SP script, post start SP UDS activity
	NETWORK_POST_UDS_ACTIVITY_START("LaunchStoryMode")
#ENDIF // FEATURE_GEN9_STANDALONE

    RETURN TRUE
    
ENDFUNC

PROC PROCESS_NET_SIGN_IN_STATE_CHANGED_EVENT(STRUCT_NETWORK_SIGN_IN_STATE_CHANGED Event)
    NET_NL() NET_PRINT("PROCESS_NET_SIGN_IN_STATE_CHANGED_EVENT - called...") NET_NL()
    g_SignInStructDetails = Event
    
    IF ARE_PROFILE_SETTINGS_VALID()
        IF GET_PROFILE_SETTING(PROLOGUE_COMPLETE) = 0
            IF GET_MISSION_COMPLETE_STATE(SP_MISSION_PROLOGUE)
                CPRINTLN(DEBUG_INIT_SP, "Loading a save that has Prologue complete without MP unlocked. Unlocking MP as we init.")
                SET_PROFILE_SETTING_PROLOGUE_COMPLETE()
            ENDIF
        ENDIF
    ENDIF

ENDPROC


FUNC STRING GET_NET_APP_LAUNCHED_EVENT_STRING(STRUCT_NETWORK_APP_LAUNCHED Event)



    SWITCH Event.nFlags
        CASE LAUNCH_STRAIGHT_INTO_FREEMODE      RETURN "LAUNCH_STRAIGHT_INTO_FREEMODE"
        CASE LAUNCH_FROM_LIVE_AREA              RETURN "LAUNCH_FROM_LIVE_AREA"
        CASE LAUNCH_DIRECT_TO_CONTENT           RETURN "LAUNCH_DIRECT_TO_CONTENT"
        
    ENDSWITCH

    RETURN ""

ENDFUNC

PROC PROCESS_NET_APP_LAUNCHED_EVENT(STRUCT_NETWORK_APP_LAUNCHED Event)

    NET_NL() NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - called... with flags = ")NET_PRINT_INT(ENUM_TO_INT(Event.nFlags))
    NET_PRINT(" called ")NET_PRINT(GET_NET_APP_LAUNCHED_EVENT_STRING(Event))
    
    #IF USE_FINAL_PRINTS
        PRINTLN_FINAL("PROCESS_NET_APP_LAUNCHED_EVENT - called... with flags = ", ENUM_TO_INT(Event.nFlags), " called ", GET_NET_APP_LAUNCHED_EVENT_STRING(Event), " ")
    #ENDIF
    
    STRUCT_INVITE_EVENT InviteEvent
    
    IF LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
    AND GET_IS_LAUNCH_FROM_LIVE_AREA() = FALSE
    AND GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT() = FALSE
        IF SHOULD_BLOCK_INVITE(InviteEvent, TRUE, TRUE) 
            NET_NL()NET_PRINT("BC: PROCESS_NET_APP_LAUNCHED_EVENT - SHOULD_BLOCK_INVITE = TRUE, exit ")
            EXIT
        ENDIF
    ENDIF
	
    PRINTLN("[TS] *****************************************************************")
    PRINTLN("[TS] * PROCESS_NET_APP_LAUNCHED_EVENT - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
    #IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * PROCESS_NET_APP_LAUNCHED_EVENT - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
    PRINTLN("[TS] *****************************************************************")
    
    IF SHOULD_DO_TRANSITION_TO_GAME_RUN()
	    PRINTLN("[TS] * PROCESS_NET_APP_LAUNCHED_EVENT - Bail as we're already transitioning")
		CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
		CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
		CLEAR_PS4_LIVE_STREAM_LAUNCH_ACTIVE()
		EXIT
    ENDIF
	
    g_AppLaunchStructDetails = Event

//  IF ENUM_TO_INT(Event.nFlags) = 3
//      NET_PRINT(" Since the flag is 3, force it to be LAUNCH_FROM_LIVE_AREA as this is what needs to happen ")
//      g_AppLaunchStructDetails.nFlags = LAUNCH_FROM_LIVE_AREA
//  ENDIF
    
    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_STRAIGHT_INTO_FREEMODE = ")NET_PRINT_BOOL(IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_STRAIGHT_INTO_FREEMODE)))
    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_FROM_LIVE_AREA = ")NET_PRINT_BOOL(IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_FROM_LIVE_AREA)))
    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_DIRECT_TO_CONTENT = ")NET_PRINT_BOOL(IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_DIRECT_TO_CONTENT)))
    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - GET_IS_LOADING_SCREEN_ACTIVE = ")NET_PRINT_BOOL(GET_IS_LOADING_SCREEN_ACTIVE())

    #IF USE_FINAL_PRINTS
        PRINTLN_FINAL("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_STRAIGHT_INTO_FREEMODE = ", IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_STRAIGHT_INTO_FREEMODE)))
        PRINTLN_FINAL("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_FROM_LIVE_AREA = ", IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_FROM_LIVE_AREA)))
        PRINTLN_FINAL("PROCESS_NET_APP_LAUNCHED_EVENT - IS_BIT_SET LAUNCH_DIRECT_TO_CONTENT = ", IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_DIRECT_TO_CONTENT)))
        PRINTLN_FINAL("PROCESS_NET_APP_LAUNCHED_EVENT - GET_IS_LOADING_SCREEN_ACTIVE = ", GET_IS_LOADING_SCREEN_ACTIVE())
    #ENDIF

    
    
    IF IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_DIRECT_TO_CONTENT))
    AND NOT GET_IS_LOADING_SCREEN_ACTIVE()
        
        IF NETWORK_IS_GAME_IN_PROGRESS()
            SET_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
        ENDIF
        
        IF IS_ONLINE_POLICIES_MENU_ACTIVE() = FALSE
            IF GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU
                #IF IS_DEBUG_BUILD
                    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT: GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU  ")
                #ENDIF
                CLOSE_SOCIAL_CLUB_MENU()
            ENDIF
        ENDIF
        
        
        SET_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
        
        IF IS_TRANSITION_ACTIVE()
            NET_NL() NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - called while the transition is processing. Turn it all off and wait, then do it again. ")
            SET_LAUNCHED_LIVETILE_WHILE_TRANSITIONING(TRUE)
            RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()
            CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
            CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
        ELSE
            IF IS_PLAYER_SPECTATING(PLAYER_ID())
                NET_NL() NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - called while not transition is processing, but while spectating. Now going to some content ")
                SET_SKYFREEZE_FROZEN(TRUE)
                
            ENDIF
        ENDIF
    ENDIF
    
    IF IS_BIT_SET(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_FROM_LIVE_AREA))
    AND NOT GET_IS_LOADING_SCREEN_ACTIVE()
        
        IF IS_ONLINE_POLICIES_MENU_ACTIVE() = FALSE
            IF GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU
                #IF IS_DEBUG_BUILD
                    NET_NL()NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT: SET_SKYSWOOP_UP: GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU  ")
                #ENDIF
                CLOSE_SOCIAL_CLUB_MENU()
            ENDIF
        ENDIF
        
    
        IF IS_TRANSITION_ACTIVE()
            NET_NL() NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - called while the transition is processing. Turn it all off and wait, then do it again. ")
            SET_LAUNCHED_LIVETILE_WHILE_TRANSITIONING(TRUE)
            RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()
            CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
            CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
        ELSE
            IF IS_PLAYER_SPECTATING(PLAYER_ID())
                NET_NL() NET_PRINT("PROCESS_NET_APP_LAUNCHED_EVENT - called while not transition is processing, but while spectating. Now going to the live area ")
            
                SET_SKYFREEZE_FROZEN(TRUE)
            ENDIF
        ENDIF
    ENDIF
    
    
ENDPROC


PROC SET_ARM_WRESTLING_RADIO_EMITTER_STATE()
    PRINTLN("SET_ARM_WRESTLING_EMITTER_STATE() turning off for SP") 
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_01",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_02",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_03",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_04",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_05",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_06",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_07",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_08",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_09",FALSE)
    SET_STATIC_EMITTER_ENABLED("MP_ARM_WRESTLING_RADIO_10",FALSE)
	SET_STATIC_EMITTER_ENABLED("DLC_h4_SE_MPJO",FALSE)
ENDPROC

PROC RESERT_TRANSITION_SESSION_MAIN_PERSISTANT_VARS()
    PRINTLN("[TS] RESERT_TRANSITION_SESSION_MAIN_PERSISTANT_VARS called")
    TRANSITION_SESSION_VARS sTranVarsTemp
    sTranVars = sTranVarsTemp
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Resets some of the game settings and makes a call to the launch main script command
FUNC BOOL Setup_Main_Script_And_Reset_Settings()

	#IF FEATURE_GEN9_STANDALONE 
		IF g_Private_Gamemode_Joining = GAMEMODE_FM
			CPRINTLN(DEBUG_INIT_SP, "Setup_Main_Script_And_Reset_Settings - Cancelling as we are joining freemode.")
			m_launchSPMain = FALSE  
	        RESET_SINGLEPLAYER_RUNNING_NOW()
			RETURN TRUE
		ENDIF
	#ENDIF


    // Kenneth R.
    // Make sure any network game has cleaned up before starting the main singleplayer script.
    // This was initially added to resolve bug #33155.  

    #IF IS_DEBUG_BUILD
    IF g_eCachedLevel != LEVEL_NET_TEST
    #ENDIF
        IF NETWORK_IS_SESSION_ACTIVE()
        
            NET_NL()NET_PRINT("MAIN_PERSISTANT: IN WHILE NETWORK_IS_SESSION_ACTIVE()")
            
            IF NETWORK_CAN_SESSION_END()
                NET_PRINT_TIME() NET_PRINT("MAIN_PERSISTANT: - called NETWORK_SESSION_END()") NET_NL()  
                NETWORK_SESSION_END()
            ENDIF

           RETURN FALSE
        ENDIF
    #IF IS_DEBUG_BUILD
    ENDIF
    #ENDIF
    
    // Wait for any multi-part switches to end
    IF (IS_PLAYER_SWITCH_IN_PROGRESS() AND NOT IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED())
        PRINTLN("Setup_Main_Script_And_Reset_Settings - waiting for multipart switch to finish before we start SP.")
		RETURN FALSE
    ENDIF
    
    IF (Request_And_Launch_Main_Script())
        PRINTLN("Setup_Main_Script_And_Reset_Settings: (Request_And_Launch_Main_Script())")
		m_launchSPMain = FALSE
                
        RESET_SINGLEPLAYER_RUNNING_NOW()
        
		#IF FEATURE_GEN9_STANDALONE
		// url:bugstar:7208104 - Gen9 - Ignorable Assert - [Script_DEBUG_NET_CHARACTER] Error: Assert: SCRIPT: Script Name = MainTransition : Program Counter = 173690 : GET_TATTOO_CURRENT_BITSET_ENUM - Current Gamemode=GAMEMODE_EMPTY, Joining Gamemode=GAMEMODE_SP, - ENUM_INDEX=2
		// We need to find a safe place to clear the joining gamemode when we enter SP from the Landing Page. This seems like the safest place to do it.
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
		AND GET_JOINING_GAMEMODE() = GAMEMODE_SP
			PRINTLN("Setup_Main_Script_And_Reset_Settings - setting the joining gamemode to EMPTY now that we are in SP.")
			SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
		ENDIF
		#ENDIF
        
        CLEAR_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_COMPLETED)
        
        CLEANUP_LEADERBOARD_CAM()
        
        SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
        
        SHOW_GAME_TOOL_TIPS(FALSE)
        THEFEED_FLUSH_QUEUE()
         g_b_ReapplyStickySaveFailedFeed = FALSE
//      Refresh_MP_Script_Tunables(TUNE_CONTEXT_BASE_GLOBALS) // William Kennedy@RockstarNorth.com - refreshes base global tuneables used for both sp and mp - shops for Kenneth.
        
        // Main has been launched so reset some settings
        SET_MISSION_NAME(FALSE)
        SET_MAX_WANTED_LEVEL(5)
        SET_WANTED_LEVEL_MULTIPLIER(1.0)
        SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
        SET_CREATE_RANDOM_COPS(TRUE)
        SET_DITCH_POLICE_MODELS(FALSE)
        SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)
        SET_ABILITY_BAR_VALUE(-1, -1)
        UNLOCK_MINIMAP_POSITION()
        CLEAR_PLAYER_PARACHUTE_MODEL_OVERRIDE(PLAYER_ID())
        CLEAR_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID())
        RESET_CACHED_PARACHUTE_DRAWABLE_FOR_MP_VARIABLES()
        g_iOrbitalCannonRefundBS = 0
		
        IF g_bIsStartUp             //Don't want to do this on the first time round, but every time hereafter
            g_bIsStartUp = FALSE
        ELSE
            SET_INSTANCE_PRIORITY_MODE(INSTANCE_MODE_SINGLEPLAYER)
        ENDIF
        
        SET_ARM_WRESTLING_RADIO_EMITTER_STATE()
        
        ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
        ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
        
        IF DOES_SCENARIO_GROUP_EXIST("MP_POLICE")
            IF IS_SCENARIO_GROUP_ENABLED("MP_POLICE")
                SET_SCENARIO_GROUP_ENABLED("MP_POLICE", TRUE)
            ENDIF
        ENDIF
        
        RESET_AI_WEAPON_DAMAGE_MODIFIER()
        RESET_AI_MELEE_WEAPON_DAMAGE_MODIFIER()
        
        SET_ALL_VEHICLE_GENERATORS_ACTIVE()
        
        //Turn on dogs barking around Lester's house.
        SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
        
        // Fix for bug #502654 - Unsuppressing MP player vehicle models in SP.
        SET_VEHICLE_MODEL_IS_SUPPRESSED(CAVALCADE, FALSE)
        SET_VEHICLE_MODEL_IS_SUPPRESSED(DAEMON, FALSE)
        SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE3, FALSE)
        SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL2, FALSE)
        
        // Get all the shop scripts to re-initialise
        REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
        REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
        RESET_SHOP_CONTROLLER_SCRIPT()
        
        Initialise_Train_Lines_On_SP_Startup()

        //#1600378 - Enable assisted line for Trevor's trailer in SP only.
        ASSISTED_MOVEMENT_REQUEST_ROUTE("trev_steps")
        
        // Fix for bug #483366 - Set Vagos Spray Booth door to Open position
////        IF (GET_INDEX_OF_CURRENT_LEVEL() != LEVEL_NET_TEST) //BC: Fix as this asserts in the test bed
////            ADD_DOOR_TO_SYSTEM(MP_DOOR_VAGOS_PNS,V_ILEV_SPRAYDOOR,<<974.91,-1851.2,31.57>>,FALSE)
////            DOOR_SYSTEM_SET_HOLD_OPEN(MP_DOOR_VAGOS_PNS,TRUE)
////        ENDIF
        
        ENABLE_SELECTOR()
        
        // Make sure the save garages are re-enabled when we return from MP     
        INT iSavehouse
        if !g_bLoadedClifford and !g_bLoadedNorman      
            REPEAT ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS) iSavehouse
                Update_Savehouse_Respawn_Garage(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse), TRUE)
            ENDREPEAT
        endif
		
		// url:bugstar:4064514 - Silo - Stealth Tank - When destroying vehicles inside this interior the vehicle cleans up on it's own right after the explosion.
		DISABLE_TIDYING_UP_IN_GARAGE(HASH("Lockup_PSY_01"), TRUE)
		DISABLE_TIDYING_UP_IN_GARAGE(HASH("Lockup_PSY_02"), TRUE)
		DISABLE_TIDYING_UP_IN_GARAGE(HASH("Lockup_PSY_03"), TRUE)
        
        #IF IS_DEBUG_BUILD
        IF g_eCachedLevel != LEVEL_NET_TEST
        #ENDIF
             SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
        #IF IS_DEBUG_BUILD
        ENDIF
        #ENDIF
        
        //SP Intialisation saved bit state restoration.
        IF DOES_SCENARIO_GROUP_EXIST("LOST_BIKERS")
            SET_SCENARIO_GROUP_ENABLED("LOST_BIKERS", IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TURN_ON_LOST_BIKER_GROUP))
        ENDIF
        
        IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_RESTORE_SLEEP_MODE)
            IF NOT IS_CURRENT_PLAYER_IN_SLEEP_MODE()
                SET_CELLPHONE_PROFILE_TO_SLEEP()
            ENDIF
        ENDIF
        
        IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_UNLOCK_SHINE_A_LIGHT)
            UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_16_SILVERLAKE", "MIRRORPARK_LOCKED")
        ENDIF
                            
        
        IF g_Private_ReturningFromActivitySession 
            
            #IF IS_DEBUG_BUILD
                IF g_FMSP_Race_Data.bActive 
                    PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(",
                            "dont reset, g_Private_ReturningFromActivitySession = TRUE ",
                            ").")
                ENDIF
            #ENDIF
        
        ELSE
            // Reset last known ped info - for a clean slate when switching/creating ambient player peds
            ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
        ENDIF
        
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
            SET_PLAYER_MAX_ARMOUR(PLAYER_ID(), 100)
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLockonToRandomPeds, FALSE)
        ENDIF
        
        //Sets length of time it take before request for voice session times out.
        //Used for player to player calls
        NETWORK_SESSION_VOICE_SET_TIMEOUT(MP_VOICE_SESSION_TIMEOUT)
        PRINTLN("CDM MP: Setting NETWORK_SESSION_VOICE_SET_TIMEOUT to ", MP_VOICE_SESSION_TIMEOUT)
		
		//url:bugstar:3904336
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
		
   		RETURN TRUE
		
    ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_SHOULD_DISPLAY_MARSTON_TICKER()
    
    #IF IS_DEBUG_BUILD
        IF bForceMarstonTicker
            RETURN TRUE
        ENDIF
    #ENDIF
    
    IF NOT bShownMarstonTicker
        #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - bShownMarstonTicker = TRUE") ENDIF #ENDIF
        IF NETWORK_IS_GAME_IN_PROGRESS()
        OR (GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) AND NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE())
            #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - In MP game or off-mission in SP game = TRUE") ENDIF #ENDIF
        IF NOT IS_PAUSE_MENU_ACTIVE_EX()
            #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - IS_PAUSE_MENU_ACTIVE_EX() = FALSE") ENDIF #ENDIF
            IF IS_GTA_ONLINE_AVAILABLE()
                #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - IS_GTA_ONLINE_AVAILABLE() = TRUE") ENDIF #ENDIF
                IF ARE_PROFILE_SETTINGS_VALID()
                    #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - ARE_PROFILE_SETTINGS_VALID() = TRUE") ENDIF #ENDIF
                    IF GET_PROFILE_SETTING(TICKER_JOHN_MARSTON_IS_AVAILABLE) = 0
                        #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - GET_PROFILE_SETTING(TICKER_JOHN_MARSTON_IS_AVAILABLE) = 0") ENDIF #ENDIF
                        IF IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE)
                            #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) = TRUE") ENDIF #ENDIF
                            RETURN TRUE
                        ENDIF
                    ELSE
                        bShownMarstonTicker = TRUE
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC


FUNC BOOL ARE_SP_COMMS_SAFE()
    IF GET_PLAYER_MODEL() = PLAYER_ZERO
    OR GET_PLAYER_MODEL() = PLAYER_ONE
    OR GET_PLAYER_MODEL() = PLAYER_TWO
        RETURN PRIVATE_Is_Safe_To_Start_Communication(BIT_MICHAEL|BIT_TREVOR|BIT_FRANKLIN, CHAR_BLANK_ENTRY, CPR_HIGH)
    ENDIF
    
    RETURN TRUE
ENDFUNC


FUNC BOOL IS_IT_SAFE_TO_DISPLAY_CHEATER_FEED_MESSAGE()

    NET_NL()NET_PRINT("DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE - Checking if it's safe to display a feed message...")

    IF IS_PED_INJURED(PLAYER_PED_ID())
    OR IS_PLAYER_SWITCH_IN_PROGRESS()
    OR IS_CUTSCENE_PLAYING()
    OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
    OR IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()              // #1382357
    OR IS_INTERIOR_SCENE()
    OR NOT ARE_SP_COMMS_SAFE()
    
        #IF IS_DEBUG_BUILD
            NET_NL()NET_PRINT("[ROSCRED]  IS_PED_INJURED(PLAYER_PED_ID()) = ")NET_PRINT_BOOL(IS_PED_INJURED(PLAYER_PED_ID()))
            NET_NL()NET_PRINT("[ROSCRED]  IS_PLAYER_SWITCH_IN_PROGRESS = ")NET_PRINT_BOOL(IS_PLAYER_SWITCH_IN_PROGRESS())
            NET_NL()NET_PRINT("[ROSCRED]  IS_CUTSCENE_PLAYING = ")NET_PRINT_BOOL(IS_CUTSCENE_PLAYING())
            NET_NL()NET_PRINT("[ROSCRED]  IS_PLAYER_CONTROL_ON = ")NET_PRINT_BOOL(IS_PLAYER_CONTROL_ON(PLAYER_ID()))
            NET_NL()NET_PRINT("[ROSCRED]  IS_SCREEN_FADING_OUT = ")NET_PRINT_BOOL(IS_SCREEN_FADING_OUT())
            NET_NL()NET_PRINT("[ROSCRED]  IS_SCREEN_FADED_OUT = ")NET_PRINT_BOOL(IS_SCREEN_FADED_OUT())
            NET_NL()NET_PRINT("[ROSCRED]  IS_INTERIOR_SCENE = ")NET_PRINT_BOOL(IS_INTERIOR_SCENE())
            NET_NL()NET_PRINT("[ROSCRED]  ARE_SP_COMMS_SAFE = ")NET_PRINT_BOOL(ARE_SP_COMMS_SAFE())
        #ENDIF
    
        RETURN FALSE
    ENDIF
    
    NET_NL()NET_PRINT("DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE - Safe to display.")
    
    RETURN TRUE
ENDFUNC

PROC DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE()
    
    IF g_b_DisplayCheaterPoolTimeFeedMessage = FALSE
        IF NETWORK_IS_SIGNED_ONLINE()
            #IF IS_DEBUG_BUILD  
                IF g_b_displayROSCredentialsDroppedPrint
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_PED_INJURED(PLAYER_PED_ID()) = ")NET_PRINT_BOOL(IS_PED_INJURED(PLAYER_PED_ID()))
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_PLAYER_SWITCH_IN_PROGRESS = ")NET_PRINT_BOOL(IS_PLAYER_SWITCH_IN_PROGRESS())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_CUTSCENE_PLAYING = ")NET_PRINT_BOOL(IS_CUTSCENE_PLAYING())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_PLAYER_CONTROL_ON = ")NET_PRINT_BOOL(IS_PLAYER_CONTROL_ON(PLAYER_ID()))
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_SCREEN_FADING_OUT = ")NET_PRINT_BOOL(IS_SCREEN_FADING_OUT())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_SCREEN_FADED_OUT = ")NET_PRINT_BOOL(IS_SCREEN_FADED_OUT())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: IS_INTERIOR_SCENE = ")NET_PRINT_BOOL(IS_INTERIOR_SCENE())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: ARE_SP_COMMS_SAFE() = ")NET_PRINT_BOOL(ARE_SP_COMMS_SAFE())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: NETWORK_HAS_VALID_ROS_CREDENTIALS() = ")NET_PRINT_BOOL(NETWORK_HAS_VALID_ROS_CREDENTIALS())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: NETWORK_HAVE_ROS_BANNED_PRIV() = ")NET_PRINT_BOOL(NETWORK_HAVE_ROS_BANNED_PRIV())
                    NET_NL()NET_PRINT("[ROSCRED] DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE: NETWORK_HAVE_ROS_MULTIPLAYER_PRIV() = ")NET_PRINT_BOOL(NETWORK_HAVE_ROS_MULTIPLAYER_PRIV())
                ENDIF
            #ENDIF
        
            IF (NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE
            OR NETWORK_HAVE_ROS_BANNED_PRIV() = TRUE 
            OR NETWORK_HAVE_ROS_MULTIPLAYER_PRIV() = FALSE
            OR NETWORK_PLAYER_IS_CHEATER())

                #IF IS_DEBUG_BUILD  
                IF g_b_displayROSCredentialsDroppedPrint
                    NET_NL()NET_PRINT("[ROSCRED]  Counting down ")
                    NET_NL()NET_PRINT("[ROSCRED]  MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP = ")NET_PRINT_INT(MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP)
                    NET_NL()NET_PRINT("[ROSCRED]  GET_GAME_TIMER() = ")NET_PRINT_INT(GET_GAME_TIMER())
                    NET_NL()NET_PRINT("[ROSCRED]  GET_GAME_TIMER()-iCheaterTimeLeftTimerSP = ")NET_PRINT_INT(GET_GAME_TIMER()-MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP )
                
                ENDIF
                #ENDIF
                
                IF (GET_GAME_TIMER() - MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP) > 5000
                
                    NET_NL()NET_PRINT("[ROSCRED]  NETWORK_HAVE_ROS_BANNED_PRIV = ")NET_PRINT_BOOL(NETWORK_HAVE_ROS_BANNED_PRIV())
                    NET_NL()NET_PRINT("[ROSCRED]  NETWORK_HAVE_ROS_MULTIPLAYER_PRIV = ")NET_PRINT_BOOL(NETWORK_HAVE_ROS_MULTIPLAYER_PRIV())
                    NET_NL()NET_PRINT("[ROSCRED]  NETWORK_PLAYER_IS_CHEATER = ")NET_PRINT_BOOL(NETWORK_PLAYER_IS_CHEATER())
                    NET_NL()NET_PRINT("[ROSCRED]  NETWORK_HAS_VALID_ROS_CREDENTIALS = ")NET_PRINT_BOOL(NETWORK_HAS_VALID_ROS_CREDENTIALS())
                    BOOL bShowCheaterMessage = FALSE
                    NETWORK_ROS_PRIVILEGEID PrivID = RLROS_PRIVILEGEID_BANNED 
                    IF NETWORK_PLAYER_IS_CHEATER()
                        PrivID = RLROS_PRIVILEGEID_CHEATER_POOL
                        bShowCheaterMessage = TRUE
                        NET_NL()NET_PRINT("[ROSCRED]  GET_GAME_TIMER() Check RLROS_PRIVILEGEID_CHEATER_POOL  ")
                    ELSE
                        NET_NL()NET_PRINT("[ROSCRED]  GET_GAME_TIMER() Check RLROS_PRIVILEGEID_BANNED  ")
                    ENDIF
                    
                    INT isGranted
                    UGC_DATE aDate
                    IF NETWORK_HAS_VALID_ROS_CREDENTIALS() = TRUE //Need to be signed in. Hold until you are. 
                        IF NETWORK_HAS_ROS_PRIVILEGE_END_DATE(PrivID, isGranted, aDate)
                            
                            NET_NL()NET_PRINT("DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE - called isGranted = ")NET_PRINT_INT(isGranted)
                            NET_NL()NET_PRINT("DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE - called Date = ")NET_PRINT_UGC_DATE(aDate)NET_NL()
                            IF isGranted = 1
                        
                                IF IS_IT_SAFE_TO_DISPLAY_CHEATER_FEED_MESSAGE()
                                    STRING FeedMessage = "HUD_CHTFEED"
                                    IF bShowCheaterMessage = FALSE
                                        FeedMessage = "HUD_BANFEED"
                                    ENDIF
                    
                                    IF IS_LANGUAGE_NON_ROMANIC()
                                        PRINT_TICKER_WITH_THREE_INTS(FeedMessage, aDate.nYear, aDate.nMonth, aDate.nDay) 
                                    ELIF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
                                        PRINT_TICKER_WITH_THREE_INTS(FeedMessage, aDate.nMonth, aDate.nDay, aDate.nYear) 
                                    ELSE
                                        PRINT_TICKER_WITH_THREE_INTS(FeedMessage, aDate.nDay, aDate.nMonth, aDate.nYear) 
                                    ENDIF
                                    g_b_DisplayCheaterPoolTimeFeedMessage = TRUE
                                ELSE
                                    NET_NL()NET_PRINT("[ROSCRED]  Resetting timer as it isn't safe to display a feed message. Checking again in 5 seconds.")
                                    MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP = GET_GAME_TIMER()
                                ENDIF 
                                
                            ELSE
                                g_b_DisplayCheaterPoolTimeFeedMessage = TRUE
                            ENDIF
                        ELSE
                        
                            #IF IS_DEBUG_BUILD  
                            IF g_b_displayROSCredentialsDroppedPrint
                                NET_NL()NET_PRINT("[ROSCRED]  NETWORK_HAS_ROS_PRIVILEGE_END_DATE = FALSE ")
                            ENDIF
                            #ENDIF
                        
                        ENDIF
                    ELSE
                        #IF IS_DEBUG_BUILD  
                        IF g_b_displayROSCredentialsDroppedPrint
                            NET_NL()NET_PRINT("[ROSCRED]  NETWORK_HAS_VALID_ROS_CREDENTIALS = FALSE, hold and reset timer ")
                        ENDIF
                        #ENDIF
                        MPGlobalsHud_TitleUpdate.iCheaterTimeLeftTimerSP = GET_GAME_TIMER()
                    ENDIF
                
                ENDIF
            ELSE
                NET_NL()NET_PRINT("[ROSCRED]  Not Banned in any way, closing off feed message. ")

                g_b_DisplayCheaterPoolTimeFeedMessage = TRUE
            ENDIF
        ENDIF
    ENDIF

ENDPROC




PROC MAINTAIN_CONTENT_UNLOCK_MP_TICKERS()
    

    // If linked to the social club, tell the player John Marston is available.
    IF GET_SHOULD_DISPLAY_MARSTON_TICKER()
        #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - GET_SHOULD_DISPLAY_MARSTON_TICKER() = TRUE") ENDIF #ENDIF
        BEGIN_TEXT_COMMAND_THEFEED_POST("MARSTON_TICK2")
        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
        IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM) // Alwyn's stuff handles printing this ticker in SP - PROC MAINTAIN_CONTENT_UNLOCK_MP_TICKERS().
            #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - GET_CURRENT_GAMEMODE() = GAMEMODE_FM") ENDIF #ENDIF
            BEGIN_TEXT_COMMAND_THEFEED_POST("CONTENT_NEW_SC") 
            END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
        ENDIF
        SET_TICKER_JOHNMARSTON_IS_DONE()
        bShownMarstonTicker = TRUE
        #IF IS_DEBUG_BUILD bForceMarstonTicker = FALSE #ENDIF
    ELSE
        #IF IS_DEBUG_BUILD IF bPrintMartsonTickerWidgets PRINTLN("[WJK] - Marston Ticker - GET_SHOULD_DISPLAY_MARSTON_TICKER() = FALSE") ENDIF #ENDIF
    ENDIF

    DISPLAY_CHEATER_DAYS_LEFT_FEED_MESSAGE()
	
	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
		
		#IF FEATURE_COPS_N_CROOKS
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS
			EXIT
		ENDIF
		#ENDIF
		
	ENDIF
	#ENDIF
	
	
   
    
ENDPROC

// ===========================================================================================================

FUNC BOOL END_SINGLEPLAYER_READY_TO_START_MP()
    
    IF NOT IS_BIT_SET(iSPCleanupStateBitset, BIT_SP_CLEANUP_COMPLETED)
        CDEBUG1LN(DEBUG_CLEANUP_SP, "Running SP cleanup this frame...")
        #IF USE_FINAL_PRINTS 
            PRINTLN_FINAL("<2140379> Running SP cleanup this frame...")
        #ENDIF

        //Wait for initial thread to end.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initial")) > 0
            CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for GTAV intial thread to end.")
            RETURN FALSE
        ENDIF
        
        
        //Wait for startup_positioning thread to end.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) > 0
            CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for GTAV startup_positioning thread to end.")
            RETURN FALSE
        ENDIF
		
		//Wait for Director Mode to terminate, if we have to
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode")) > 0
			g_bDirectorTerminateOnSpCleanup = TRUE
			CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for Director Mode to end.")
			RETURN FALSE
		ENDIF
		g_bDirectorTerminateOnSpCleanup = FALSE

        //Start SP threads cleaning up.
        IF NOT IS_BIT_SET(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_FORCE_CLEANUP)
            FORCE_CLEANUP(FORCE_CLEANUP_FLAG_SP_TO_MP)
            
            // Run one time cleanup here to get it out the way as soon as possible.
            SET_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_RESET_FOR_MULTIPLAYER) //Reset property system.
            DISABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)                                      //Turn off SP world points.
            Cleanup_Train_Lines_On_SP_End()                                                                 //Turn off train lines.
            ASSISTED_MOVEMENT_REMOVE_ROUTE("trev_steps")                                                    //#1600378 - Enable assisted line for Trevor's trailer in SP only. Clean them up as we transition into MP.
            SAVE_EMAIL_SYSTEM_STATE()                                                                       //Ensure email state is saved
        
            IF IS_PLAYER_PLAYING(PLAYER_ID())
                SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)                               //#2171916 Ensure player air drag is cleaned up going into MP.
            ENDIF
    
            SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_FORCE_CLEANUP)
            CLEAR_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_STOPPED_CUTSCENES)
        
            //Give SP scripts a frame to process the force cleanup before allowing the transition to continue.
            //Clean up the stop cut scene flag
            CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting to allow force cleanup to run for one frame.")
            RETURN FALSE
        ENDIF
    
        //Wait for akward mission specific cleanup.
        IF NOT IS_BIT_SET(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_MISSION_CLEANUP)
            IF GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
            IF g_eRunningMission = SP_MISSION_MICHAEL_1
            OR g_eMissionRunningOnMPSwitchStart = SP_MISSION_MICHAEL_1
                IF NOT IS_SCREEN_FADED_OUT()
                        AND GET_CURRENT_HUD_STATE() != HUD_STATE_SELECT_CHARACTER_FM
                        AND GET_CURRENT_HUD_STATE() != HUD_STATE_CREATE_FACE
                    IF NOT IS_SCREEN_FADING_OUT()
                            CDEBUG1LN(DEBUG_CLEANUP_SP, "Starting screen fade out for Michael 1.")
                        
                        //Ensure the MP HUD can draw on top of this fade.
                        SET_NO_LOADING_SCREEN(TRUE)
                        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
                        
                        DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
                    ENDIF
                ELSE
                        IF NOT IS_TRANSITION_HEADING_FOR_CREATOR()
                            CDEBUG1LN(DEBUG_CLEANUP_SP, "Running delayed cleanup for Michael 1.")
                        RUN_RCI_MICHAEL1()
                    ENDIF
                    
                    g_eTriggerRCI = RCI_NONE
                        CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup for Michael 1 complete.")
                        SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_MISSION_CLEANUP)
                ENDIF
                    
                    CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for Michael 1 mission cleanup.")
                RETURN FALSE
            ENDIF
            
                IF g_eRunningMission = SP_MISSION_PROLOGUE
                OR g_eMissionRunningOnMPSwitchStart = SP_MISSION_PROLOGUE
                    IF NOT IS_SCREEN_FADED_OUT()
                    AND GET_CURRENT_HUD_STATE() != HUD_STATE_SELECT_CHARACTER_FM
                    AND GET_CURRENT_HUD_STATE() != HUD_STATE_CREATE_FACE
                        IF NOT IS_SCREEN_FADING_OUT()
                            CDEBUG1LN(DEBUG_CLEANUP_SP, "Starting screen fade out for Prologue 1.")
                            
                            //Ensure the MP HUD can draw on top of this fade.
                            SET_NO_LOADING_SCREEN(TRUE)
                            SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
                            
                            DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
                        ENDIF
                    ELSE
                        IF NOT IS_TRANSITION_HEADING_FOR_CREATOR()
                            CDEBUG1LN(DEBUG_CLEANUP_SP, "Running cleanup for Prologue 1.")
                            #IF IS_DEBUG_BUILD
                                IF g_AreBirdsDisplaying
                                    CDEBUG1LN(DEBUG_CLEANUP_SP, "Running cleanup for Prologue 1 with g_AreBirdsDisplaying = TRUE so skip fade out.")
                                ENDIF
                            #ENDIF
                            RUN_RCI_PROLOGUE()
                        ENDIF
                        
                        g_eTriggerRCI = RCI_NONE
                        SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_MISSION_CLEANUP)
                        CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup for Prologue 1 complete.")
                        RETURN FALSE
                    ENDIF
                        
                    CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for Prologue1 mission cleanup.")
                    RETURN FALSE
                ENDIF
#IF IS_DEBUG_BUILD
            ELSE
                SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_MISSION_CLEANUP)
                CDEBUG1LN(DEBUG_CLEANUP_SP, "Gamemode is currently GAMEMODE_CREATOR. Skipping mission specific cleanup.")
#ENDIF  
            ENDIF
    ENDIF
    
        //Halt any running cutscenes.
        IF NOT IS_BIT_SET(iSPCleanupStateBitset, BIT_SP_CLEANUP_STOPPED_CUTSCENES)
        IF IS_CUTSCENE_ACTIVE()
                CDEBUG1LN(DEBUG_CLEANUP_SP, "Telling cutscenes to clean up.")
            STOP_CUTSCENE_IMMEDIATELY()
                SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_STOPPED_CUTSCENES)
        ENDIF
    ENDIF
    
    //Wait for cutscenes to finish cleaning up.
    IF IS_CUTSCENE_ACTIVE()
                iFramesWaitingForCutscenes++
                IF iFramesWaitingForCutscenes > 60
                    CDEBUG1LN(DEBUG_CLEANUP_SP, "Running emergency stop cutscene after 60 frames of waiting.")
                    STOP_CUTSCENE_IMMEDIATELY()
                    iFramesWaitingForCutscenes = 0
                ENDIF
                
                CDEBUG1LN(DEBUG_CLEANUP_SP, "Cleanup waiting for cutscenes to end.")
        RETURN FALSE
    ENDIF

        iFramesWaitingForCutscenes = 0
    
    // Cleanup mission flag
    Force_Mission_Over()

    //Informs SP initial it is not starting from a fresh load.
    g_bHaveBeenInMultiplayer = TRUE 
    g_bInMultiplayer = TRUE
    
	SET_CHARACTER_MODEL_CHECK_DONE(TRUE) // This is to make sure we dont get stuck later on because this flag was never set. see url:bugstar:6478922 - Local player became stuck on skycam after changing session while sat in the drug vehicle

	
    CPRINTLN(DEBUG_CLEANUP_SP, "SP cleanup finished.")
    #IF USE_FINAL_PRINTS 
        PRINTLN_FINAL("<2140379> SP cleanup finished.")
    #ENDIF
    
    CLEAR_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_FORCE_CLEANUP)
    CLEAR_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_RAN_MISSION_CLEANUP)
    CLEAR_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_STOPPED_CUTSCENES)
        
		//url:bugstar:3964887 - [PUBLIC] [EXPLOIT] Fast Run cheat can still be brought online with specific timing
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
		
        //Flag cleanup as complete so we can't run it again until SP restarts.
        SET_BIT(iSPCleanupStateBitset, BIT_SP_CLEANUP_COMPLETED)
		
	#IF FEATURE_GEN9_STANDALONE
		// Singleplayer script cleanup completed, post end SP UDS activity
		NETWORK_POST_UDS_ACTIVITY_END("LaunchStoryMode")
	#ENDIF // FEATURE_GEN9_STANDALONE
		
    ELSE
        CPRINTLN(DEBUG_CLEANUP_SP, "SP cleanup has already run. Skipping cleanup this time.")
    ENDIF
	
    
    RETURN TRUE
ENDFUNC


//Make the bail enum print readable
#IF IS_DEBUG_BUILD

    /// PURPOSE: Requests the 'Debug' script, waits for it to stream in, then launches it
    ///   
    PROC Request_And_Launch_Debug_Script_With_Wait()
        INT debugScriptNameHash
        IF g_bLoadedClifford
            debugScriptNameHash = HASH("debugCLF")
        ELIF g_bLoadedNorman
            debugScriptNameHash = HASH("debugNRM")
        ELSE
            debugScriptNameHash = HASH("debug")
        ENDIF
        
        REQUEST_SCRIPT_WITH_NAME_HASH(debugScriptNameHash)
        WHILE NOT (HAS_SCRIPT_WITH_NAME_HASH_LOADED(debugScriptNameHash))
            CPRINTLN(DEBUG_CLEANUP_SP, "Request_And_Launch_Debug_Script_With_Wait in Loop...")
            REQUEST_SCRIPT_WITH_NAME_HASH(debugScriptNameHash)
            WAIT(0)
        ENDWHILE
        
        START_NEW_SCRIPT_WITH_NAME_HASH(debugScriptNameHash, DEBUG_MENU_STACK_SIZE)
        SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(debugScriptNameHash)
    ENDPROC
	
	FUNC BOOL Request_And_Launch_Debug_Script_Async()
        INT debugScriptNameHash
        IF g_bLoadedClifford
            debugScriptNameHash = HASH("debugCLF")
        ELIF g_bLoadedNorman
            debugScriptNameHash = HASH("debugNRM")
        ELSE
            debugScriptNameHash = HASH("debug")
        ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(debugScriptNameHash) = 0
			IF NOT bDebugScriptRequestedForRelaunch
				REQUEST_SCRIPT_WITH_NAME_HASH(debugScriptNameHash)
				bDebugScriptRequestedForRelaunch = FALSE
			ENDIF
	        
	        IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(debugScriptNameHash)
	        	START_NEW_SCRIPT_WITH_NAME_HASH(debugScriptNameHash, DEBUG_MENU_STACK_SIZE)
	        	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(debugScriptNameHash)
				bDebugScriptRequestedForRelaunch = FALSE
				CPRINTLN(DEBUG_CLEANUP_SP, "Request_And_Launch_Debug_Script_With_Wait - Debug.sc relaunched!")
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
    ENDFUNC



#ENDIF

//Is the player in a safe room for respawn
FUNC BOOL IS_LOCAL_PLAYER_IN_A_SAFE_ROOM()
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        IF g_sShopSettings.playerInterior != NULL
            
            INTERIOR_INSTANCE_INDEX interiorID_Lester = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1274.7242, -1713.8298, 53.7715>>, "v_lesters")
            
            IF g_sShopSettings.playerInterior != interiorID_Lester
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

#IF FEATURE_GEN9_STANDALONE
/// PURPOSE:
///    Determine whether an end session END_REASON should cause a bail from a transition during PROCESS_EVENT_NETWORK_SESSION_END_EVENT
/// PARAMS:
///    eEndReason - the reason to check
/// RETURNS:
///    True if transition should bail for eEndReason
FUNC BOOL SHOULD_NETWORK_SESSION_END_REASON_BAIL_TRANSITION(END_REASON eEndReason)

	SWITCH eEndReason
		CASE END_KICKED
		CASE END_ERROR
		CASE END_KICKED_ADMIN
			PRINTLN("SHOULD_NETWORK_SESSION_END_REASON_BAIL_TRANSITION - returning TRUE")
			RETURN TRUE		
	ENDSWITCH
	
	PRINTLN("SHOULD_NETWORK_SESSION_END_REASON_BAIL_TRANSITION - returning FALSE")
	RETURN FALSE

ENDFUNC

#ENDIF //FEATURE_GEN9_STANDALONE

PROC PROCESS_EVENT_NETWORK_SESSION_END_EVENT(INT iCount)
    STRUCT_SESSION_END_EVENT sEvent
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
    
    //got this reason then return to the same freemode
    IF sEvent.nEndReason = END_RESERVING_SLOT
        CLEAR_GETTING_CONTENT_BY_ID()
        CLEAR_CORONA_IS_DOWNLOADING_FROM_UGC_SERVER()
        //If the fake MP mode is not reset
        IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
            SET_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
            SET_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
            SET_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()                  
            HANG_UP_AND_PUT_AWAY_PHONE()
            //If we're starting an on-call then set it to clean up
            IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
            OR HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
                SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
            ENDIF           
            //If the flag is set to store the players vehicle 
            IF SHOULD_TRANSITION_SESSIONS_RESTORE_VEHICLE()
                
                // Fix for bug # 1826290 - We handle this in the carmod_shop cleanup with more vigorous checks, so lets skip.
                //SAVE_MY_EOM_VEHICLE_DATA(NULL, FALSE, TRUE, 4)
                // But we should still handle the end pos.
                CLEAR_TRANSITION_SESSION_END_SESSION_POS()
                IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
                    SET_TRANSITION_SESSION_END_SESSION_POS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
                ENDIF
            ELSE
                CLEAR_TRANSITION_SESSION_END_SESSION_POS()
                IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
                    SET_TRANSITION_SESSION_END_SESSION_POS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
                    IF IS_LOCAL_PLAYER_IN_A_SAFE_ROOM()
                    OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
                        SET_TRANSITION_SESSION_CONSIDER_INTERIORS_ON_RESPAWN()
                    ELSE
                        CLEAR_TRANSITION_SESSION_CONSIDER_INTERIORS_ON_RESPAWN()
                    ENDIF
                ENDIF
            ENDIF
        #IF IS_DEBUG_BUILD
        //otherwise just print about is
        ELSE
            PRINTSTRING("[TS] PROCESS_EVENT_NETWORK_SESSION_END_EVENT - IS_FAKE_MULTIPLAYER_MODE_SET")
        #ENDIF
        ENDIF
        
    //Get end session event
    // Re-launch SP Main
    ELIF sEvent.nEndReason != END_TRANSITION_LAUNCH
//        IF sEvent.nEndReason = END_KICKED_ADMIN   
//            PRINTLN("[TS] END_KICKED_ADMIN")                        
//            NETWORK_REQUEST_CLOUD_TUNABLES()
//            PRINTLN("[TS] NETWORK_REQUEST_CLOUD_TUNABLES - EVENT_NETWORK_SESSION_END")  
//        ENDIF
    
        
        IF DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
            IF sEvent.nEndReason = END_NORMAL
                PRINTSTRING("[TS]  (New Transition) Main Persistent Event Queue - Session End ignored because DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE")
                EXIT
            ENDIF
        ENDIF
        
    //NOT IS_TRANSITION_SESSION_LAUNCHING()
        #IF IS_DEBUG_BUILD
            PRINTSTRING("...KGM: (New Transition) Main Persistent Event Queue - Session End")
            PRINTNL()
            PRINTSTRING("... GET_END_REASON_STRING = ")
            PRINTSTRING(GET_END_REASON_STRING(sEvent.nEndReason))
            PRINTNL()
            
        #ENDIF
        
        IF sEvent.nEndReason = END_KICKED
             PRINTSTRING("[BCFORCE] sEvent.nEndReason = END_KICKED g_Private_Am_Kicked (HAVE_I_BEEN_KICKED) = TRUE")
            g_Private_Am_Kicked = TRUE
        ENDIF
        
        
        SET_PLAYER_HAVE_PRIVILEGES(HAS_ONLINE_PRIVILAGES())
        
        //Clean up every thing.
        IF NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
            RESET_TRANSITION_SESSION_NON_RESET_VARS()
            RESET_TRANSITION_SESSION_CORONA_BISET()
        ENDIF
//        IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) 
        IF IS_TRANSITION_ACTIVE()

                IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_TRANSITION_CREATE_PLAYER
                OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_IS_FM_AND_TRANSITION_READY
                OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_SWOOP_DOWN
                OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_FINAL_SETUP_PLAYER
                OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_MOVE_FM_TO_RUNNING_STATE
				OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_HOW_TO_TERMINATE
				#IF FEATURE_GEN9_STANDALONE
				// RM DND: url:bugstar:7375854 Check end reason for bail
				OR SHOULD_NETWORK_SESSION_END_REASON_BAIL_TRANSITION(sEvent.nEndReason)
				#ENDIF //FEATURE_GEN9_STANDALONE
                    TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
                    PRINTLN("BC: EVENT_NETWORK_SESSION_END - Code has called END_SESSION Head stright to the bail screen for default reason. ")
                    HUD_CHANGE_STATE(HUD_STATE_BAIL)
                    SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_BAILED)
                    SET_BAIL_HAPPENED_DURING_JOIN(TRUE)
                ELSE
                
                
                   NET_NL()NET_PRINT(" BC: EVENT_NETWORK_SESSION_END - Player has called END_SESSION but the transition is active, so don't do much. ")
                ENDIF

				//Set global tracking boolean for a Bail session happened. This is 
				// currently used and cleared in transition_controller.sch.
				//If you want to use it then probably best to also clear it ureself..
				g_Private_BailSessionHappened = TRUE
				
                //Maintransition needs to launch freemode, as this will kill fm dead. 2096079
                SET_TRANSITION_TO_IGNORE_LAUNCH_SCRIPT_START(FALSE)


//            IF LOBBY_LEAVE_MULTIPLAYER() //Remvoed as you can't quit back to SP in the code new game option. Script handles the script one
//                TELL_TRANSITION_MAYBE_FRONTEND_QUIT_MP(TRUE)
//                g_bInMultiplayer = FALSE
//                SET_TRANSITION_STRING("HUD_QUITTING")   
//                TRANSITION_CHANGE_STATE(TRANSITION_STATE_MP_SWOOP_UP)
//                HUD_CHANGE_STATE(HUD_STATE_LOADING)
//                NET_NL()NET_PRINT(" BC: EVENT_NETWORK_SESSION_END - Player has quit the game in the pause menu while the transition menu was up... ")
//            ENDIF
        ELSE
             //Done outside the MPHUD/Transition //Remvoed as you can't quit back to SP in the code new game option. Script handles the script one
//            IF LOBBY_LEAVE_MULTIPLAYER()
//                TELL_TRANSITION_MAYBE_FRONTEND_QUIT_MP(TRUE) //Commented out as when you're going from CNC to freemode this would send you to SP. 
//            ENDIF
        
            
            g_bInMultiplayer = FALSE
            
            IF g_Private_MultiplayerCreatorNeedsToEnd = FALSE
                TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
                
                IF GET_CONFIRM_INVITE_INTO_GAME_STATE() = FALSE
                AND HAVE_I_BEEN_KICKED() = FALSE
                    SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
                    SET_BAILED_HAPPENED_SOMEHOW(TRUE)
                    SET_END_SESSION_REASON(sEvent.nEndReason) 
                ENDIF
                

                NET_NL()NET_PRINT(" BC: EVENT_NETWORK_SESSION_END - Player has Quit the game somehow, bringing up the hud now. ")
            ELSE
                SET_FAKE_MULTIPLAYER_MODE(FALSE)
                
            ENDIF
            
            
            
            
            g_Private_MultiplayerCreatorNeedsToEnd = FALSE
            
        ENDIF
    //Got launch transition session
    ELSE
        SET_VARS_FOR_END_TRANSITION_LAUNCH()
    ENDIF
ENDPROC

CONST_INT ciMESSAGE_TYPE_CRASH              -1198671041 //crash
CONST_INT ciMESSAGE_TYPE_TUNABLE_REFRESH    -1494736646 //tunable

PROC PROCESS_EVENT_NETWORK_PRESENCE_STAT_UPDATE_EVENT(INT iCount)
    STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE sei
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        //Switch the stat type
        SWITCH sei.statHash
            //If it's a crash 
            CASE ciMESSAGE_TYPE_CRASH       
                //And their tunable says no!
                IF NOT NETWORK_ACCESS_TUNABLE_BOOL("BASE_GLOBALS", "MULTIPLAYER_DISABLED")
                    //Not sure we want to have this power as hackers might get a hold of it. 
                    //KILLING_YOU_SOFTLY_WITH_RECURSION()
                ENDIF
            BREAK       
            //If it's a tunable refresh
            CASE ciMESSAGE_TYPE_TUNABLE_REFRESH
                NETWORK_REQUEST_CLOUD_TUNABLES()
                PRINTLN("[BGSCRIPT] - NETWORK_REQUEST_CLOUD_TUNABLES - ciMESSAGE_TYPE_TUNABLE_REFRESH")         
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

PROC PROCESS_EVENT_NETWORK_NETWORK_ROS_CHANGED(INT iCount)

    STRUCT_NETWORK_ROS_CHANGED sEvent
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
    
    #IF IS_DEBUG_BUILD
        NET_NL()NET_PRINT("PROCESS_EVENT_NETWORK_NETWORK_ROS_CHANGED -  ")
        NET_NL()NET_PRINT("         - sEvent.bValidCredentials = ")NET_PRINT_BOOL(sEvent.bValidCredentials)
        NET_NL()NET_PRINT("         - sEvent.bValidRockstarID = ")NET_PRINT_BOOL(sEvent.bValidRockstarID)
        NET_NL()NET_PRINT("         - g_b_IsRockstarIDValidLastFrame = ")NET_PRINT_BOOL(g_b_IsRockstarIDValidLastFrame)
		 NET_NL()NET_PRINT("         - IS_TRANSITION_ACTIVE = ")NET_PRINT_BOOL(IS_TRANSITION_ACTIVE())
    #ENDIF
    
    IF g_b_IsRockstarIDValidLastFrame != sEvent.bValidRockstarID
    AND sEvent.bValidRockstarID = TRUE
        //Refresh the last gen checks.
        SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE) 
        SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE) 
        g_i_Private_LastGenCharacterAttempt = 0
        NET_NL()NET_PRINT("[LASTGEN] PROCESS_EVENT_NETWORK_NETWORK_ROS_CHANGED - bValidRockstarID is TRUE. Reset all the checks to run again. ")
    
        #IF USE_FINAL_PRINTS
            PRINTLN_FINAL("[LASTGEN] PROCESS_EVENT_NETWORK_NETWORK_ROS_CHANGED - bValidRockstarID is TRUE. Reset all the checks to run again. ")
        #ENDIF
    ENDIF
	
	
   
    g_b_IsRockstarIDValidLastFrame = sEvent.bValidRockstarID
    
    
    
    //DO nothing just now, as we like having no cloud now. 
//  //sEvent.bValidCredentials = False, no cloud connection. 
//  SET_FRONTEND_ACTIVE(FALSE)
//  SET_SKYFREEZE_FROZEN()
//  SET_NETWORK_ROS_CHANGED(TRUE)
//  
//  SET_QUIT_CURRENT_GAME(TRUE)
//  
//
//  IF IS_TRANSITION_ACTIVE() = FALSE
//      TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)                
//  ENDIF
            
ENDPROC

PROC PROCESS_TRANSITION_LOAD_SCENE()
    
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
        IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
            
            IF NOT IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
            AND NOT IS_CUTSCENE_PLAYING()
            AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
            AND NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete
            
                IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete
                    IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene
                        IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLoadSceneCoords)
                            IF NEW_LOAD_SCENE_START_SPHERE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLoadSceneCoords, 150.0)
                                PRINTLN("TRANSITION TIME - FREEMODE - TRANSITION NEW LOAD SCENE               - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")   
                                NET_PRINT("[PMC] - [SAC] - TRANSITION TIME -  started new load scene at coords: ")NET_PRINT_VECTOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLoadSceneCoords)NET_NL()
                                g_vInitialSpawnLocation = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLoadSceneCoords
                                PRINTLN("[PMC] - [SAC] - g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)
                                g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene = TRUE
                            ENDIF
                        ENDIF
                    ELSE
                        IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete
                            IF IS_NEW_LOAD_SCENE_ACTIVE()
                                IF IS_NEW_LOAD_SCENE_LOADED()
                                    NEW_LOAD_SCENE_STOP()
                                    g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete = TRUE
                                    NET_PRINT("[PMC] - [SAC] - TRANSITION TIME -  completed new load scene.")NET_NL()
                                ENDIF
                            ELSE
                                g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete = TRUE
                                NET_PRINT("[PMC] - [SAC] - TRANSITION TIME -  completed new load scene because it became inactive.")NET_NL()
                            ENDIF
                        ELSE
                            g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete = TRUE
                            NET_PRINT("[WJK] - TRANSITION TIME -  completed new load scene because it became inactive.")NET_NL()
                        ENDIF
                    ENDIF
                ENDIF
                
            ELSE
                
                IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete
                    #IF IS_DEBUG_BUILD
                    IF IS_PLAYER_WATCHING_A_MOCAP(player_id())
                        PRINTLN("[PMC] - [SAC] - TRANSITION TIME - IS_PLAYER_WATCHING_A_MOCAP, set bFirstLoadSceneComplete = TRUE but HAVE NOT done a load scene. Just setting flag TRUE so script can continue.")
                    ENDIF
                    IF IS_CUTSCENE_PLAYING()
                        PRINTLN("[PMC] - [SAC] - TRANSITION TIME - IS_CUTSCENE_PLAYING, set bFirstLoadSceneComplete = TRUE but HAVE NOT done a load scene. Just setting flag TRUE so script can continue.")
                    ENDIF
                    IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
                        PRINTLN("[PMC] - [SAC] - TRANSITION TIME - sGlobalCelebrationData bDoingHeistCelebration, set bFirstLoadSceneComplete = TRUE but HAVE NOT done a load scene. Just setting flag TRUE so script can continue.")
                    ENDIF
                    #ENDIF
                    g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete = TRUE
                ENDIF
                
            ENDIF
            
        ENDIF
    ENDIF
    
ENDPROC

PROC PROCESS_EVENT_NETWORK_ATM_LOG(INT iCount)
    IF g_iMPAtmLogEntryPendingIndex != -1
        PRINTLN("PROCESS_EVENT_NETWORK_ATM_LOG(", iCount, ") pending, g_iMPAtmLogEntryPendingIndex: ", g_iMPAtmLogEntryPendingIndex)
        EXIT
    ENDIF
    
    STRUCT_CASH_TRANSACTION_LOG_EVENT sEvent
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent)) 
    
    #IF IS_DEBUG_BUILD
    TEXT_LABEL_31 sLogEventId = "", sLogEventType = "", sLogEventItemId = ""
    SWITCH sEvent.iId
        CASE CASH_TRANSACTION_ATM       sLogEventId = "ATM" BREAK
        CASE CASH_TRANSACTION_GAMER     sLogEventId = "GAMER" BREAK
        CASE CASH_TRANSACTION_STORE     sLogEventId = "STORE" BREAK
        CASE CASH_TRANSACTION_BANK      sLogEventId = "BANK" BREAK
        
        DEFAULT
            sLogEventId  = "ID_"
            sLogEventId += ENUM_TO_INT(sEvent.iId)
        BREAK
    ENDSWITCH
    SWITCH sEvent.iType
        CASE CASH_TRANSACTION_DEPOSIT
            sLogEventType = "DEPOSIT"
            
            sLogEventItemId = DEBUG_GET_STRING_FROM_EARN_CATEGORIES(INT_TO_ENUM(EARN_CATEGORIES, sEvent.ItemId))
            IF IS_STRING_NULL_OR_EMPTY(sLogEventItemId)
                sLogEventItemId  = "LogDataUnknown_"
                sLogEventItemId += sEvent.ItemId
            ENDIF
            BREAK
        CASE CASH_TRANSACTION_WITHDRAW
            sLogEventType = "WITHDRAW"
            sLogEventItemId = DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(INT_TO_ENUM(SPEND_CATEGORIES, sEvent.ItemId))
            IF IS_STRING_NULL_OR_EMPTY(sLogEventItemId)
                sLogEventItemId  = "LogDataUnknown_"
                sLogEventItemId += sEvent.ItemId
            ENDIF
            BREAK
            
        DEFAULT
            sLogEventType  = "TYPE_"
            sLogEventType += ENUM_TO_INT(sEvent.iType)
        BREAK
    ENDSWITCH
    
    PRINTLN("PROCESS_EVENT_NETWORK_ATM_LOG(", iCount, "): trans = ", sEvent.iTransactionId,
            " ID = ", sLogEventId,
            " type = ", sLogEventType,
            " amount = $", sEvent.iAmount,
            " ItemID = ", sLogEventItemId)  //" gamer = '", gmr,"'")
    #ENDIF
    #IF NOT IS_DEBUG_BUILD
    PRINTLN("PROCESS_EVENT_NETWORK_ATM_LOG(", iCount, "): trans = ", sEvent.iTransactionId, 
            " ID = ", sEvent.iId,
            " type = ", sEvent.iType,
            " amount = $", sEvent.iAmount,
            " ItemID = ", sEvent.ItemId)
    #ENDIF
    
    //trigger log entry
    INT c = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret
    g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogValues[c] = sEvent.iAmount
    
    MP_ATM_LOG_ACTION_TYPE t
    
    eTransactionIds tId = INT_TO_ENUM(eTransactionIds, sEvent.iId)
    SWITCH tId
        CASE CASH_TRANSACTION_ATM   //Money deposited/withdrawn by the player - this is relation to VC commands - So deposit or withdraw from BANk Account.
            SWITCH INT_TO_ENUM(eTransactionTypes, sEvent.iType)
                CASE CASH_TRANSACTION_DEPOSIT
                    t = MALA_DEPOSIT
                    BREAK
                CASE CASH_TRANSACTION_WITHDRAW
                    t = MALA_WITHDRAW
                    BREAK
                DEFAULT
                    SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_ATM_LOG: invalid eventID")                         //= ", sLogEventId, " type = ", sLogEventType, " for amount $", sEvent.iAmount)
                    BREAK
            ENDSWITCH
            BREAK
        CASE CASH_TRANSACTION_GAMER //Money received/paid from friends.
            SWITCH INT_TO_ENUM(eTransactionTypes, sEvent.iType)
                CASE CASH_TRANSACTION_DEPOSIT
                    t = MALA_CASH_RECIEVED
                    BREAK
                CASE CASH_TRANSACTION_WITHDRAW
                    t = MALA_CASH_SENT
                    BREAK
                DEFAULT
                    SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_ATM_LOG: invalid eventID")                         //= ", sLogEventId, " type = ", sLogEventType, " for amount $", sEvent.iAmount)
                    BREAK
            ENDSWITCH
            BREAK
        CASE CASH_TRANSACTION_STORE //STORE money always credits the BANK account
            SWITCH INT_TO_ENUM(eTransactionTypes, sEvent.iType)
                CASE CASH_TRANSACTION_DEPOSIT
                    t = MALA_CASH_BOUGHT
                    SET_BIT(MPGlobalsAmbience.iFmNmhBitSet2, BI_FM_NMH2_DEPOSITED_CASH) // Dave W, for help text
                    BREAK
                CASE CASH_TRANSACTION_WITHDRAW
                    BREAK
                DEFAULT
                    SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_ATM_LOG: invalid eventID")                         //= ", sLogEventId, " type = ", sLogEventType, " for amount $", sEvent.iAmount)
                    BREAK
            ENDSWITCH
            BREAK
        CASE CASH_TRANSACTION_BANK
            //purchase
            SWITCH INT_TO_ENUM(eTransactionTypes, sEvent.iType)
                CASE CASH_TRANSACTION_WITHDRAW //they bought something
                    t = MALA_PURCHASE
                    BREAK
                CASE CASH_TRANSACTION_DEPOSIT //they got a refund of some kind
                    t = MALA_REFUND
                    BREAK
                DEFAULT
                    SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_ATM_LOG: invalid eventID")                         //= ", sLogEventId, " type = ", sLogEventType, " for amount $", sEvent.iAmount)
                    BREAK
            ENDSWITCH
            BREAK
        DEFAULT
            SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_ATM_LOG: invalid eventID")                         //= ", sLogEventId, " type = ", sLogEventType, " for amount $", sEvent.iAmount)
            EXIT
    ENDSWITCH
    
    g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogActionType[c]  = t
    
	g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[c] = ""
    IF INT_TO_ENUM(eTransactionIds, sEvent.iId) = CASH_TRANSACTION_GAMER //Money received/paid from friends.
    
        PRINTLN("PROCESS_EVENT_NETWORK_ATM_LOG: Getting player name process initiated.")
        
        //this has to be done outside since we can't wait in here 1627644
        IF g_iMPAtmLogEntryPendingIndex = -1
            g_iMPAtmLogEntryPendingIndex = c
            //the following because not messing with the stucture of the headers for this
            g_iMPAtmLogHandle[0] =  sEvent.hGamer.Data1
            g_iMPAtmLogHandle[1] =  sEvent.hGamer.Data2
            g_iMPAtmLogHandle[2] =  sEvent.hGamer.Data3
            g_iMPAtmLogHandle[3] =  sEvent.hGamer.Data4
            g_iMPAtmLogHandle[4] =  sEvent.hGamer.Data5
            g_iMPAtmLogHandle[5] =  sEvent.hGamer.Data6
            g_iMPAtmLogHandle[6] =  sEvent.hGamer.Data7
            g_iMPAtmLogHandle[7] =  sEvent.hGamer.Data8
            g_iMPAtmLogHandle[8] =  sEvent.hGamer.Data9
            g_iMPAtmLogHandle[9] =  sEvent.hGamer.Data10
            g_iMPAtmLogHandle[10] = sEvent.hGamer.Data11
            g_iMPAtmLogHandle[11] = sEvent.hGamer.Data12
            g_iMPAtmLogHandle[12] = sEvent.hGamer.Data13
        ENDIF
    ELSE
        //update item assignment if its a valid value
        INT id = sEvent.ItemId 
        IF id != 0
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[c] = 0
            IF sEvent.iType = CASH_TRANSACTION_WITHDRAW
                SPEND_CATEGORIES s = INT_TO_ENUM(SPEND_CATEGORIES, id)
                SWITCH s
                    CASE MONEY_SPENT_CONTACT_SERVICE
                    CASE MONEY_SPENT_PROPERTY_UTIL 
                    CASE MONEY_SPENT_JOB_ACTIVITY 
                    CASE MONEY_SPENT_BETTING 
                    CASE MONEY_SPENT_STYLE_ENT
                    CASE MONEY_SPENT_HEALTHCARE
                    CASE MONEY_SPENT_FROM_DEBUG
                    CASE MONEY_SPENT_DROPPED_STOLEN
                    CASE MONEY_SPENT_VEH_MAINTENANCE
                    CASE MONEY_SPENT_HOLDUPS
                    CASE MONEY_SPENT_PASSIVEMODE
                    CASE MONEY_SPENT_BANKINTEREST
                        CDEBUG1LN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: MpSavedATM.LogData[", c, "] spend:", DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(s))
                        g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[c] = id
                        
                    BREAK
                    CASE MONEY_SPENT_WEAPON_ARMOR
                    CASE MONEY_SPENT_NOCOPS
                    CASE MONEY_SPENT_ROCKSTAR_AWARD
                        //do nothing
                    BREAK
                    
                    DEFAULT
                        #IF IS_DEBUG_BUILD
                        IF NOT IS_STRING_NULL_OR_EMPTY(DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(s))
                            CASSERTLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: unknown spend activity \"", DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(s), "\"")
                        ELSE
                        #ENDIF
                            CWARNINGLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: unknown spend activity [", s, "]")
                        #IF IS_DEBUG_BUILD
                        ENDIF
                        #ENDIF
                    BREAK
                ENDSWITCH
            ELIF sEvent.iType = CASH_TRANSACTION_DEPOSIT 
                EARN_CATEGORIES e = INT_TO_ENUM(EARN_CATEGORIES,id)
                SWITCH e
                    CASE MONEY_EARN_JOBS
                    CASE MONEY_EARN_SELLING_VEH 
                    CASE MONEY_EARN_BETTING     
                    CASE MONEY_EARN_GOOD_SPORT   
                    CASE MONEY_EARN_PICKED_UP     
                    CASE MONEY_EARN_SHARED       
                    CASE MONEY_EARN_JOBSHARED     
                    CASE MONEY_EARN_ROCKSTAR_AWARD 
                    CASE MONEY_EARN_REFUND
                    CASE MONEY_EARN_FROM_HEIST_JOB
                    CASE MONEY_EARN_FROM_JOB_BONUS
                        CDEBUG1LN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: MpSavedATM.LogData[", c, "] earn:", DEBUG_GET_STRING_FROM_EARN_CATEGORIES(e))
                        g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[c] = id
                    BREAK
                        
                    DEFAULT
                        #IF IS_DEBUG_BUILD
                        IF NOT IS_STRING_NULL_OR_EMPTY(DEBUG_GET_STRING_FROM_EARN_CATEGORIES(e))
                            CASSERTLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: unknown earn activity \"", DEBUG_GET_STRING_FROM_EARN_CATEGORIES(e), "\"")
                        ELSE
                        #ENDIF
                            CWARNINGLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: unknown earn activity [", e, "]")
                        #IF IS_DEBUG_BUILD
                        ENDIF
                        #ENDIF
                    BREAK
                ENDSWITCH
            ELSE
                CWARNINGLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: unknown event type \"", sEvent.iType, "\"")
            ENDIF
		ELSE
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogData[c] = 0
			CWARNINGLN(DEBUG_INTERNET, "PROCESS_EVENT_NETWORK_ATM_LOG: invalid event item ID \"", sEvent.ItemId, "\"")
        ENDIF
    ENDIF
    
    ++g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret
    IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret >= MP_TOTAL_ATM_LOG_ENTRIES
        g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.iLogCaret = 0
    ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_CLOUD_EVENT_ID_DEBUG_PRINT(CLOUD_EVENT_ID nEventID)
    SWITCH nEventID
        CASE CLOUD_EVENT_TUNABLES_ADDED RETURN "CLOUD_EVENT_TUNABLES_ADDED"
        CASE CLOUD_EVENT_BGSCRIPT_ADDED RETURN "CLOUD_EVENT_BGSCRIPT_ADDED"
    ENDSWITCH
    PRINTLN("PROCESS_EVENT_NETWORK_CLOUD_EVENT - GET_CLOUD_EVENT_ID_DEBUG_PRINT out of range nEventID = ", ENUM_TO_INT(nEventID))
    RETURN ""
ENDFUNC
#ENDIF

//Process a cloud event
PROC PROCESS_EVENT_NETWORK_CLOUD_EVENT(INT iCount)
    STRUCT_NETWORK_CLOUD_EVENT sei
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        #IF IS_DEBUG_BUILD
        PRINTLN("PROCESS_EVENT_NETWORK_CLOUD_EVENT - sei.nEventID      = ", GET_CLOUD_EVENT_ID_DEBUG_PRINT(sei.nEventID))
        PRINTLN("PROCESS_EVENT_NETWORK_CLOUD_EVENT - sei.nEventParam1  = ", sei.nEventParam1)
        PRINTLN("PROCESS_EVENT_NETWORK_CLOUD_EVENT - sei.nEventParam2  = ", sei.nEventParam2)
        PRINTLN("PROCESS_EVENT_NETWORK_CLOUD_EVENT - sei.szEventString = ", sei.szEventString)      
        #ENDIF
        //Switch the event type
        SWITCH sei.nEventID
            //If it's a crash 
            CASE CLOUD_EVENT_TUNABLES_ADDED       
                PRINTLN("[BTTUN] - PROCESS_EVENT_NETWORK_CLOUD_EVENT - CLOUD_EVENT_TUNABLES_ADDED - g_sTunableLoadingStruct.bRefreshNow = TRUE")            
                g_sTunableLoadingStruct.bRefreshNow = TRUE
            BREAK       
            //If it's a tunable refresh
            CASE CLOUD_EVENT_BGSCRIPT_ADDED
                PRINTLN("[BTTUN] - PROCESS_EVENT_NETWORK_CLOUD_EVENT - CLOUD_EVENT_BGSCRIPT_ADDED - g_sTunableLoadingStruct.bRefreshNow = TRUE")            
                g_sBackGroundStruct.bKillActive = TRUE
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC






PROC PROCESS_MAINPER_NETWORK_EMAIL_RECEIVED_EVENT()

    IF NOT IS_ACCOUNT_OVER_17()

        #if IS_DEBUG_BUILD
            cdPrintString("Main_Persistent - Received Eyefind Email Event but Account is not over 17. No feed creation.")
            cdPrintnl()
        #endif
        
        EXIT
    ENDIF

    IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID()) //Fix for 2437163. Make sure that no feed message appears if the user account does not have communication privileges.
    
        IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)

            PRINTLN("Main_Persistent - PROCESS_NETWORK_EMAIL_RECEIVED_EVENT - Displaying feed notification of Eyefind message received.")

            BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_EMAIL_EVENT")   
            END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)

        ELSE
            
            IF GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) <> 1
                #if IS_DEBUG_BUILD
                    cdPrintString("Main_Persistent - Received Eyefind Email Event but PROFILE_FEED_FRIENDS did not equal 1. No feed creation.")
                    cdPrintnl()
                #endif
            ENDIF

        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintString("Main_Persistent - Received Eyefind Email Event but Account does not have chat privileges. No feed creation.")
            cdPrintnl()
        #endif

    ENDIF

ENDPROC





PROC PROCESS_MAINPER_NETWORK_MARKETING_EMAIL_RECEIVED_EVENT()

    //This should be fired from EVENT_NETWORK_MARKETING_EMAIL_RECEIVED

    IF NOT IS_ACCOUNT_OVER_17()

        #if IS_DEBUG_BUILD
            cdPrintString("Main_Persistent - Received Marketing Email Event but Account is not over 17. No feed creation.")
            cdPrintnl()
        #endif
        
        EXIT
    ENDIF

    IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID()) //Fix for 2437163. Make sure that no feed message appears if the user account does not have communication privileges.
    AND g_bInMultiplayer = TRUE //Do not display this feed message in SP. #2606359
    
        //IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)



            
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION) 

                #if IS_DEBUG_BUILD
                    cdPrintString("Main_Persistent - Process Marketing Email Received Event not displaying feed message as Cued This Session bit is already set.")
                    cdPrintnl()
                #endif

            ELSE

                PRINTLN("Main_Persistent - PROCESS_MAINPER_NETWORK_MARKETING_EMAIL_RECEIVED_EVENT - Displaying feed notification of Eyefind message received and setting Cued This Session bit.")

                
                #if IS_DEBUG_BUILD
                    cdPrintString("Main_Persistent - Process Marketing Email Received Event - Will display feed notification and set Cued This Session bit.")
                    cdPrintnl()
                #endif


                
                
                SET_BIT (BitSet_CellphoneTU, g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION) 

            
                /* Simplifying notification for 2534125. Remove Social Club branding.
                BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_EMKT_EVENT")   
                //END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)

                   TEXT_LABEL_63 s_SocialClubConstruct

                     //Check for Social Club sender here. If so, colour yellow. #1562653

                     s_SocialClubConstruct = "~HUD_COLOUR_SOCIAL_CLUB~"
                     s_SocialClubConstruct += "Social Club"
                                
                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_EMAIL, s_SocialClubConstruct)
                */

                //Simplifying notification for 2534125. Just displaying a standard ticker.
                BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_EMKT_EVENT")   
                END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)



                g_NumberOfUnreadMarketingEmailsThisSession ++ //Increment this for 2606295 and refresh the phone homescreen.
                REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST()

             ENDIF

            

        /*
        ELSE
            
            IF GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) <> 1
                #if IS_DEBUG_BUILD
                    cdPrintString("Main_Persistent - Received Marketing Email Event but PROFILE_FEED_FRIENDS did not equal 1. No feed creation.")
                    cdPrintnl()
                #endif
            ENDIF

        ENDIF
        */

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintString("Main_Persistent - Received Marketing Email Event but Account does not have chat privileges or is not in MP. No feed creation.")
            cdPrintnl()
        #endif

    ENDIF

ENDPROC





// ===========================================================================================================
//      Joblist NextGen Check Permissions Event Handling
// ===========================================================================================================

// PURPOSE: Process the event that returns the result of an Asynchronous check for an invite from a blocked player
PROC Process_Event_Network_Permission_Check_Result(INT paramEventID)

    // Retrieve the data
    STRUCT_NETWORK_PERMISSION_CHECK_RESULT theEventData
    IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
        PRINTLN(".KGM [JobList]: Process_Event_Network_Permission_Check_Result() - ERROR: Failed to retrieve the event data")
        SCRIPT_ASSERT("Process_Event_Network_Permission_Check_Result(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
        EXIT
    ENDIF
    
    INT     checkID     = theEventData.nCheckID
    BOOL    isAllowed   = theEventData.bIsAllowed

    #IF IS_DEBUG_BUILD
        NET_PRINT(".KGM [JobList]: Retrieved Asynchronous Blocking Check Result for checkID: ")
        NET_PRINT_INT(checkID)
        NET_PRINT(" - is Allowed? ")
        NET_PRINT_BOOL(isAllowed)
        NET_NL()
    #ENDIF
    
    //Deal with book marks!
    //Only if the UGC block is loaded!
    IF g_sCURRENT_UGC_STATUS.g_bUgcGlobalsBlockLoaded       
        IF(g_iBookMarkCheckPermissionsBitSet[0] != 0
        OR g_iBookMarkCheckPermissionsBitSet[1] != 0)
            PRINTLN("[UGC TYPE] PROCESS_EVENT_NETWORK_PERMISSION_CHECK_RESULT_FOR_UGC")         
            INT iloop, iArray, iBit, iRemoveLoop
            INT iMaxLoop = MAX_NUMBER_FMMC_SAVES+MAX_NUMBER_FMMC_SAVES
            REPEAT iMaxLoop iLoop
                //Find the event ID!
                IF g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.iCheckPermission[iLoop] = theEventData.nCheckID
                    //Clear the bit!
                    iBit = iLoop%32
                    iArray = FLOOR(TO_FLOAT(iLoop)/32.0)
                    PRINTLN("[UGC TYPE] PROCESS_EVENT_NETWORK_PERMISSION_CHECK_RESULT_FOR_UGC - CLEAR_BIT(sGetUGC_content.iCheckPermissionsBitSet[", iArray, "], ", iBit, ")")          
                    CLEAR_BIT(g_iBookMarkCheckPermissionsBitSet[iArray], iBit)
                    //Clear the permission checks!
                    g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.iCheckPermission[iLoop] = 0
                    //If it's allowed we don't care, just move on
                    IF theEventData.bIsAllowed = TRUE
                        PRINTLN("[UGC TYPE] PROCESS_EVENT_NETWORK_PERMISSION_CHECK_RESULT_FOR_UGC - theEventData.bIsAllowed = TRUE")
                        EXIT
                    //If it's not allowed we need to loop and remove it from the array!
                    ELSE
                        FOR iRemoveLoop = iLoop TO (iMaxLoop - 2)
                            g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iRemoveLoop] = g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iRemoveLoop + 1]
                        ENDFOR
                        //Get out
                        EXIT
                    ENDIF
                ELIF g_FMMC_HEADER_STRUCT.iCheckPermission[iLoop] = theEventData.nCheckID
                    //Clear the bit!
                    iBit = iLoop%32
                    iArray = FLOOR(TO_FLOAT(iLoop)/32.0)
                    PRINTLN("[UGC TYPE] PROCESS_EVENT_NETWORK_PERMISSION_CHECK_RESULT_FOR_UGC - CLEAR_BIT(sGetUGC_content.iCheckPermissionsBitSet[", iArray, "], ", iBit, ")")          
                    CLEAR_BIT(g_iBookMarkCheckPermissionsBitSet[iArray], iBit)
                    //Clear the permission checks!
                    g_FMMC_HEADER_STRUCT.iCheckPermission[iLoop] = 0
                    //If it's allowed we don't care, just move on
                    IF theEventData.bIsAllowed = TRUE
                        PRINTLN("[UGC TYPE] PROCESS_EVENT_NETWORK_PERMISSION_CHECK_RESULT_FOR_UGC - theEventData.bIsAllowed = TRUE")
                        EXIT
                    //If it's not allowed we need to loop and remove it from the array!
                    ELSE
                        FOR iRemoveLoop = iLoop TO (iMaxLoop - 2)
                            g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iRemoveLoop] = g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iRemoveLoop + 1]
                        ENDFOR
                        //Get out
                        EXIT
                    ENDIF
                ENDIF
            ENDREPEAT
        ENDIF
    ENDIF
    
    // Find this checkID within the same session Invite array
    INT tempLoop = 0
    REPEAT g_numMPJobListInvitations tempLoop
        IF (g_sJLInvitesMP[tempLoop].jliDoingPermissionCheck)
            IF (g_sJLInvitesMP[tempLoop].jliPermissionCheckID = checkID)
                // ...found it
                // Is this invite blocked or allowed?
                IF (isAllowed)
                    // ...allowed, so clear the variables to allow the invitation to be processed
                    g_sJLInvitesMP[tempLoop].jliPermissionCheckID       = 0
                    g_sJLInvitesMP[tempLoop].jliDoingPermissionCheck    = FALSE
                    g_sJLInvitesMP[tempLoop].jliPermissionCheckFail     = FALSE
                    
                    PRINTLN(".KGM [JobList]: Invite is NOT asynchronously blocked in array position: ", tempLoop)
                    EXIT
                ENDIF
                
                // ...not allowed, so blocking the invite
                g_sJLInvitesMP[tempLoop].jliPermissionCheckID       = 0
                g_sJLInvitesMP[tempLoop].jliDoingPermissionCheck    = FALSE
                g_sJLInvitesMP[tempLoop].jliPermissionCheckFail     = TRUE
                    
                PRINTLN(".KGM [JobList]: Invite is asynchronously blocked in array position: ", tempLoop)
                EXIT
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Find this checkID within the Basic Invite array
    REPEAT g_numMPRVInvites tempLoop
        IF (g_sJLRVInvitesMP[tempLoop].jrviDoingPermissionCheck)
            IF (g_sJLRVInvitesMP[tempLoop].jrviPermissionCheckID = checkID)
                // ...found it
                // Is this invite blocked or allowed?
                IF (isAllowed)
                    // ...allowed, so clear the variables to allow the Basic Invite to be processed
                    g_sJLRVInvitesMP[tempLoop].jrviPermissionCheckID        = 0
                    g_sJLRVInvitesMP[tempLoop].jrviDoingPermissionCheck = FALSE
                    g_sJLRVInvitesMP[tempLoop].jrviPermissionCheckFail      = FALSE
                    
                    PRINTLN(".KGM [JobList]: Basic Invite is NOT asynchronously blocked in array position: ", tempLoop)
                    EXIT
                ENDIF
                
                // ...not allowed, so blocking the invite
                g_sJLRVInvitesMP[tempLoop].jrviPermissionCheckID        = 0
                g_sJLRVInvitesMP[tempLoop].jrviDoingPermissionCheck = FALSE
                g_sJLRVInvitesMP[tempLoop].jrviPermissionCheckFail      = TRUE
                    
                PRINTLN(".KGM [JobList]: Basic Invite is asynchronously blocked in array position: ", tempLoop)
                EXIT
            ENDIF
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
        NET_PRINT(".KGM [JobList]: Asynchronous Blocking CheckID: ")
        NET_PRINT_INT(checkID)
        NET_PRINT(" - NOT FOUND ON SAME-SESSION INVITE OR BASIC INVITE ARRAYS")
        NET_NL()
    #ENDIF

ENDPROC

STRUCT_INVITE_EVENT CachedInviteEvent
BOOL bCachedInvite
PROC Process_cached_invite()
	IF (bCachedInvite)
		PRINTLN("Process_cached_invite - active")
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(0)
		ENDIF		
		PROCESS_INVITE_EVENTS(CachedInviteEvent, bCachedInvite)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears out the event queue - deals with any MP->SP transistion events
PROC CLEAR_EVENT_QUEUE()

    EVENT_NAMES eventType
    INT iCount = 0
    BOOL bGoBackToSP = FALSE
	BOOL bDoCameraUp = TRUE
    STRUCT_INVITE_EVENT InviteEvent
    STRUCT_SUMMON_EVENTS SummonEvent
    STRUCT_PLAYER_SCRIPT_EVENTS data
    STRUCT_NETWORK_SIGN_IN_STATE_CHANGED SignInEvent
    STRUCT_NETWORK_APP_LAUNCHED AppLaunchEvent
    STRUCT_INVITE_VIA_PRESENCE_EVENT PresenceInviteEvent
    STRUCT_ADMIN_INVITED_EVENT AdminInviteEvent
    STRUCT_NETWORK_SPECTATE_LOCAL SpecLocalEvent
    STRUCT_NETWORK_SYSTEM_SERVICE_EVENT SystemServiceEvent
    STRUCT_NETWORK_REQUEST_DELAY NetworkRequestDelayEvent
    SCADMIN_UPDATE_EVENT ScAdminPlayerUpdated
    EventReceivedCash scEventReceivedCash
	MULTIPLAYER_ACCESS_CODE nAccessCode

    INT I
    INT FindPlayerInt
    BOOL inventoryRefresh, PlayerBalanceRefresh
    PLAYER_INDEX FindPlayerIndex
    
    // Go through the event queue getting the head of the queue
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
        eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)      

        // HANDLE ALL EVENTS THAT THIS SCRIPT NEEDS TO KNOW ABOUT
		IF NOT ShouldBGScriptInterceptCodeEvent(eventType)
	        SWITCH (eventType)
	        
	            // The 'Session Start' event gets triggered when the MP game launches
	            CASE EVENT_NETWORK_SESSION_START
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SESSION_START")
	                IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
	                AND (GET_CURRENT_GAMEMODE() = GAMEMODE_SP OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY) //Added this as it also launches between missions. 
	                    //END_SINGLEPLAYER_READY_TO_START_MP()
	                    SET_SINGLEPLAYER_END_NOW()
	                ENDIF
	            BREAK
	            
	            //When we get a cloud event
	            CASE EVENT_NETWORK_CLOUD_EVENT
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_CLOUD_EVENT")
	                PROCESS_EVENT_NETWORK_CLOUD_EVENT(iCount)
	            BREAK        

	            // The 'session end' event triggers when MP is quit normally through the game
	            CASE EVENT_NETWORK_SESSION_END
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SESSION_END")
	                PROCESS_EVENT_NETWORK_SESSION_END_EVENT(iCount)
	            BREAK
	            
	            //If we get a presence stat update that is for a crash or for a tunable refresh then deal with it
	            CASE EVENT_NETWORK_PRESENCE_STAT_UPDATE
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PRESENCE_STAT_UPDATE")
	                PROCESS_EVENT_NETWORK_PRESENCE_STAT_UPDATE_EVENT(iCount)                
	            BREAK
	            
	            // The 'bail' event triggers when the game quits unexpectedly (ie: player signs out of PSN)
	            CASE EVENT_NETWORK_NETWORK_BAIL
	                PRINTLN("[TS] *****************************************************************")
	                PRINTLN("[TS] * NETWORK_BAIL - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
	                #IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * NETWORK_BAIL - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
	                PRINTLN("[TS] *****************************************************************")
	                IF GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
	                    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, g_BailEvent, SIZE_OF(g_BailEvent))
	                    
                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - g_BailEvent.nBailReason = ", GET_BAIL_REASON_STRING(g_BailEvent.nBailReason))
	                    
	                    RESET_SKYCAM_HOLD()
	                    
	                    IF GET_CURRENT_HUD_STATE() = HUD_STATE_CREATE_FACE
	                    OR GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
	//                      TRANSITION_CHANGE_STATE(TRANSITION_STATE_CREATION_ENTER_SESSION)
	                        
	                        PRINTLN("BC: CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - IGNORE BAIL JUST TRY AGAIN, ON CHAR SELECT OR CREATE SCREEN ", GET_BAIL_REASON_STRING(g_BailEvent.nBailReason))
	                        BREAK
	                    ENDIF
	                    
	                    IF GET_CURRENT_TRANSITION_BAILING_OPTIONS() = BAILING_OPTIONS_TIMEOUT_WAITING_TO_JOIN 
	                        
	                        PRINTLN("BC: CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - IGNORE BAIL, FORCING DISCONNECT AS IT TIMED OUT WHILE TRYING TO CONNECT BAIL SCREEN SHOULD ALREADY BE COMING UP THROUGH OTHER MEANS. ", GET_BAIL_REASON_STRING(g_BailEvent.nBailReason))
	                        BREAK
	                    ENDIF
							                    
	                    RESERT_TRANSITION_SESSION_MAIN_PERSISTANT_VARS()
	                    
	                    SET_PLAYER_HAVE_PRIVILEGES(HAS_ONLINE_PRIVILAGES())
	                    	                    
	                    RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	                    //SET_TRANSITION_SESSIONS_CAM_DOWN_AFTER_END_RESERVING_SLOT()

						IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED) = TRUE	
						AND g_BailEvent.nBailReason = BAIL_FROM_SCRIPT
	                    	PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - script Bail is already handled, skip TELL_TRANSITION_BAIL_HAPPENED(TRUE)")
						ELSE
							TELL_TRANSITION_BAIL_HAPPENED(TRUE)
						ENDIF						
						
	                    g_bInMultiplayer = FALSE
	                    
	                    //So the hud resets when you bail, and knows you're not online still. 
	                    SET_QUIT_CURRENT_GAME(TRUE)
	                    
	                    SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
	                    SET_BAIL_STILL_VALID(TRUE)
	                    
	                    IF g_BailEvent.nBailReason = BAIL_SYSTEM_SUSPENDED
	                      SET_HAS_KICKED_CONSTRAINED_TRANSITION(TRUE)
	                    ENDIF
	                 
	                    IF IS_TRANSITION_ACTIVE() = FALSE
	                        TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
	                    ELSE
	                        IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_JOIN_FM_SESSION
	                        OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_LOOK_FOR_FRESH_JOIN_FM
	                            TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
	                            
	                           PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - Head stright to the bail screen ", GET_BAIL_REASON_STRING(g_BailEvent.nBailReason))
	                            
	                            HUD_CHANGE_STATE(HUD_STATE_BAIL)
	                            SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_BAILED)
	                            SET_BAIL_HAPPENED_DURING_JOIN(TRUE)
	                        ENDIF
	                    ENDIF
	                    
	        			
	                                       
	                    SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
	                    SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)    

	                    
	                    //Set the transition sessions to clear up
	                    IF IS_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING() 
	                        SET_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT() 
	                    ELSE 
	                        RESET_TRANSITION_SESSION_NON_RESET_VARS() 
	                        RESET_TRANSITION_SESSION_CORONA_BISET() 
	                    ENDIF
	                    
	                    SET_SKYFREEZE_FROZEN()
	                ELSE
	                    PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_NETWORK_BAIL - Ignore it when in the Creators")
	                ENDIF
	            BREAK

	                /* BC: 08/05/2012 - Commented out as the hud chooses if the player needs to press a button to head back to SP. 
	                g_TransitionData.bCouldNotJoinNetworkGame = TRUE
	                //BC 11/02/2012 added this so when a bail happens in MP or joining MP, SP will launch back up ok.  
	                 m_launchSPMain = TRUE
	                 
	                
	                #IF IS_DEBUG_BUILD
	                    PRINTSTRING("...KGM: Main Persistent Event Queue - Bail")
	                    PRINTNL()
	                #ENDIF
	                    
	                // If not in MP, do nothing, let the game resume
	                // If in MP, then request a hotswap to the previous player character
	                IF (g_bInMultiplayer)
	                    // ...was in MP, so sets up a hotswap to keep the return to SP consistent
	                    MAKE_PLAYER_PED_SWITCH_REQUEST_FROM_MP_KICK(GET_LAST_KNOWN_PLAYER_PED_ENUM(), PR_TYPE_AMBIENT)
	                    
	                    // ...re-launch SP Main
	//                    m_launchSPMain = TRUE
	                ENDIF
	                
	                // An MP bailout can occur at any time (ie: the player signs out using the PSN dashboard)
	                // We need to reset some stuff on return to SP.
	                ENABLE_SELECTOR()
	                
	                g_bPerformingSessionChange = FALSE // incase the network fails while player is changing session. (added by Neil)
	                #IF IS_DEBUG_BUILD
	                    PRINTSTRING("g_bPerformingSessionChange - set to FALSE in main_persistent")
	                    PRINTNL()
	                #ENDIF
	                
	                g_bInMultiplayer = FALSE
	                
	                
	                
	                
	                BREAK
	                */
	            
	            CASE EVENT_NETWORK_JOIN_SESSION_RESPONSE
					PRINTLN("...KGM: CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_JOIN_SESSION_RESPONSE = ", GET_BAIL_RESPONSE_STRING(g_JoinResponseEvent.nResponseCode))
	                GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, g_JoinResponseEvent, SIZE_OF(g_JoinResponseEvent))
	            BREAK
	            	            
	            CASE EVENT_NETWORK_INVITE_ACCEPTED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_INVITE_ACCEPTED")
	                GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, InviteEvent, SIZE_OF(InviteEvent))
	                
					IF HAS_AIMTYPE_ALTERED_BY_INVITE()
						PUT_AIMTYPE_BACK_TO_WHERE_IT_WAS()
						SET_AIMTYPE_ALTERED_BY_INVITE(FALSE)
						PRINTLN("[AIMPREF] CLEAR_EVENT_QUE - Another Invite being processed - put aimtype back ")
					ENDIF
	            
	                IF IS_PED_INJURED(PLAYER_PED_ID())
	                    DO_SCREEN_FADE_OUT(0)
	                ENDIF
					
	                FORCE_PLAYER_BACK_INTO_PLAYING_STATE()
	                PRINTLN("...KGM: CLEAR_EVENT_QUE - (New Transition) Main Persistent Event Queue - EVENT_NETWORK_INVITE_ACCEPTED = ", GET_BAIL_RESPONSE_STRING(g_JoinResponseEvent.nResponseCode))
	                
	                IF SHOULD_BLOCK_INVITE(InviteEvent, TRUE, FALSE) = FALSE
	                
	                    //Is the pause menu is active, then close it
	                    IF NETWORK_IS_GAME_IN_PROGRESS()
	                    AND NOT IS_PLAYER_IN_CORONA()
	                        IF GET_PAUSE_MENU_STATE() != PM_INACTIVE
	                        OR IS_PAUSE_MENU_ACTIVE()
	                        OR IS_WARNING_MESSAGE_ACTIVE()
	                            CLEAR_DYNAMIC_PAUSE_MENU_ERROR_MESSAGE()
	                            PRINTLN("[TS] CLEAR_DYNAMIC_PAUSE_MENU_ERROR_MESSAGE")
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_SELECT_CHARACTER_FM
	                                PRINTLN("[TS] CLEAR_EVENT_QUE - GET_PAUSE_MENU_STATE() != PM_INACTIVE - SET_FRONTEND_ACTIVE")
	                                SET_FRONTEND_ACTIVE(FALSE)
	                            ELSE
	                                PRINTLN("[TS] CLEAR_EVENT_QUE - GET_PAUSE_MENU_STATE() != PM_INACTIVE - Trying to call SET_FRONTEND_ACTIVE(FALSE) but we're in the character selector screen.")
	                            ENDIF
	                        ENDIF
	                    ENDIF	                

	                    IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
	                        IF IS_PLAYER_SPECTATING(PLAYER_ID())
	                            SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_INVITE_ACCEPTED: MP_TV - GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE SET")
	                        ENDIF
	                    ENDIF

	                    IF IS_REPEAT_PLAY_ACTIVE()
							// [10/08/21] Alex P (Dundee) - Appears that this should actually call SET_SINGLEPLAYER_END_NOW
							// This section was added in CL 4813265 to address bugstar://1641900, almost 8 years old at this point so leaving as is
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_INVITE_ACCEPTED: IS_REPEAT_PLAY_ACTIVE = TRUE so calling SHOULD_END_SINGLEPLAYER")
	                        SHOULD_END_SINGLEPLAYER()
	                    ELSE
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_INVITE_ACCEPTED: IS_REPEAT_PLAY_ACTIVE = FALSE ")
	                    ENDIF
	                ENDIF
	            BREAK
	                
	            CASE EVENT_NETWORK_INVITE_CONFIRMED
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, InviteEvent, SIZE_OF(InviteEvent))    
	                    RESET_SKYCAM_HOLD()
						IF NOT (bCachedInvite)
	                    	PROCESS_INVITE_EVENTS(InviteEvent, bCachedInvite)
							IF (bCachedInvite)
								CachedInviteEvent = InviteEvent
								PRINTLN("CLEAR_EVENT_QUE - caching invite event ")
							ENDIF							
						ELSE
							PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_INVITE_CONFIRMED: already processing a chached invite. ")
						ENDIF
						
	                ENDIF
	            BREAK
	            
	            
	            CASE EVENT_NETWORK_INVITE_REJECTED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_INVITE_REJECTED")
						
	                IF GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
	                    SET_SKYFREEZE_CLEAR(TRUE)
	                    PRINTLN("CLEAR_EVENT_QUE - Invite rejected while on the character select screen - unfreeze the game. ")
	                ENDIF
	                
	                IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	                    IF STAT_IS_STATS_TRACKING_ENABLED()
	                        PRINTLN("[BCLOAD] CLEAR_EVENT_QUE - EVENT_NETWORK_INVITE_REJECTED: Player is in director mode. calling STAT_DISABLE_STATS_TRACKING()   ")
	                        STAT_DISABLE_STATS_TRACKING()
	                    ENDIF
	                ENDIF
										
					IF HAS_AIMTYPE_ALTERED_BY_INVITE()
						PUT_AIMTYPE_BACK_TO_WHERE_IT_WAS()
						SET_AIMTYPE_ALTERED_BY_INVITE(FALSE)
						PRINTLN("[AIMPREF] CLEAR_EVENT_QUE - Invite rejected - put aimtype back ")
					ENDIF
	                
	            BREAK
	                
	            CASE EVENT_NETWORK_SUMMON
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SUMMON")
					
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, SummonEvent, SIZE_OF(SummonEvent))
						PROCESS_NETWORK_SUMMON_EVENT(SummonEvent)
	                ENDIF
	            BREAK
	            
				#IF NOT FEATURE_GEN9_STANDALONE
	            CASE EVENT_NETWORK_SIGN_IN_STATE_CHANGED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SIGN_IN_STATE_CHANGED")
	                 IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, SignInEvent, SIZE_OF(SignInEvent))
	                     #IF IS_DEBUG_BUILD
	                        PRINTLN("...KGM: CLEAR_EVENT_QUE - (New Transition) Main Persistent Event Queue - Sign In Changed")
	                        PRINTLN("EVENT_NETWORK_SIGN_IN_STATE_CHANGED -called ")
	                    #ENDIF
	                    #IF USE_FINAL_PRINTS
	                        PRINTLN_FINAL("...KGM: CLEAR_EVENT_QUE - (New Transition) Main Persistent Event Queue - Sign In Changed")
	                        PRINTLN_FINAL("EVENT_NETWORK_SIGN_IN_STATE_CHANGED -called ")
	                    #ENDIF
	                    RESET_SKYCAM_HOLD()
	                    
	                    IF GET_CONTRAINED_EVENT_ACTIVE()
	                        PRINTLN("CLEAR_EVENT_QUE - GET_CONTRAINED_EVENT_ACTIVE = TRUE so ignore this signin event.")
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("CLEAR_EVENT_QUE - GET_CONTRAINED_EVENT_ACTIVE = TRUE so ignore this signin event.")
	                        #ENDIF
	                        
	                        SET_CONTRAINED_EVENT_ACTIVE(FALSE)
	                        EXIT
	                    ENDIF
	                    
	                    IF SignInEvent.bIsActiveUser
						
	                        IF IS_ONLINE_POLICIES_MENU_ACTIVE() = FALSE
	                            IF GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU
	                                #IF IS_DEBUG_BUILD
	                                    PRINTLN("EVENT_NETWORK_SIGN_IN_STATE_CHANGED: GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU , calling CLOSE_SOCIAL_CLUB_MENU ")
	                                #ENDIF
	                                CLOSE_SOCIAL_CLUB_MENU()
	                            ENDIF
	                        ENDIF	                    
	                    
	                        RESET_SKYCAM_HOLD()
	                        PROCESS_NET_SIGN_IN_STATE_CHANGED_EVENT(SignInEvent)
	                        SET_PLAYER_HAVE_PRIVILEGES(HAS_ONLINE_PRIVILAGES())
	                                
	                        g_b_Check_Chat_Restrictions_For_Message  = FALSE // Run Chat restriction message Checks again.
	                                
	                        IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
	                        OR GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
	                        
	                            SET_ALL_CODE_STATS_NOT_LOADED()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: All Save data is being flattend, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: All Save data is being flattend, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                            #ENDIF
	                            
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                            SET_RETURN_TO_SP_RELOAD(FALSE)	                            
	                                    
	                        ENDIF
	                    
	                        IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	                    
	                            SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IN FM All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called")
	                            
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IN FM All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called")
	                            #ENDIF
								
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                            SET_RETURN_TO_SP_RELOAD(FALSE)
	                        ENDIF
	                        
	                        IF HAS_IMPORTANT_STATS_LOADED()
	                        AND IS_TRANSITION_ACTIVE()
	                    
	                            SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: HAS_IMPORTANT_STATS_LOADED() and in the transition so flatten all the stats - called ")
	                            
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: HAS_IMPORTANT_STATS_LOADED() and in the transition so flatten all the stats - called ")
	                            #ENDIF
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                            SET_RETURN_TO_SP_RELOAD(FALSE)
	                        ENDIF
	                        
	                    ELSE
	                        NET_NL()NET_PRINT("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: SignInEvent.bIsActiveUser = FALSE ")
	                    ENDIF
	                    
	                    //if we were on line and not is on line, i.e. in the store
	                    IF SignInEvent.bWasOnline
	                    AND NOT SignInEvent.bIsOnline
	                    //And we're in the store
	                    AND SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
						
	                        //reset for SP bail
	                        RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	        
	                        IF IS_TRANSITION_ACTIVE() = FALSE
								IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
								OR GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
									
									PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: in FM or the creates and lost bIsOnline connection, head back to SP")
		                            #IF USE_FINAL_PRINTS
		                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: in FM or the creates and lost bIsOnline connection, head back to SP")
		                            #ENDIF
									
		                            SET_FRONTEND_ACTIVE(FALSE)
		                            SET_SKYFREEZE_FROZEN()                  
									REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, GET_CURRENT_HUD_STATE())
									TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
								ELSE
									PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: we're in SP, ignore event BUG 2624445")
		                            #IF USE_FINAL_PRINTS
		                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: we're in SP, ignore event - BUG 2624445")
		                            #ENDIF
								ENDIF
	                        ENDIF
	                        
	                        //fix for - 1984182 - Stuck transitioning back to freemode after losing PSN connection while in the game store
	                    ENDIF
	                    
						IF SignInEvent.bIsActiveUser
		                    IF (SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline)
		                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, mark flag to suppres the invites again when heading back to SP. \n")
		                        #IF USE_FINAL_PRINTS
		                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, mark flag to suppres the invites again when heading back to SP. \n")
		                        #ENDIF
								
								SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
		                        SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
		                        SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
		                        NETWORK_SUPPRESS_INVITE(TRUE)
		                        RESET_NET_TIMER(SignInLoadStatsTimer)
		                        g_Private_Is_Running_Boot_Invite_Supress = TRUE
		                        SET_RETURN_TO_SP_RELOAD(FALSE)
		                        
		                        IF IS_XBOX_PLATFORM()
		                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  IS_XBOX_PLATFORM() so freeze the camera ")
		                            #IF USE_FINAL_PRINTS
			                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  IS_XBOX_PLATFORM() so freeze the camera ")
			                        #ENDIF
									//Only do this if we're in MP
		                            IF IS_PLAYER_A_SP_CHARACTER() = FALSE 
		                            AND GET_IS_PLAYER_IN_ANIMAL_FORM() = FALSE
		                            AND IS_TRANSITION_ACTIVE() = FALSE
									AND IS_PLAYER_SWITCH_IN_PROGRESS() = FALSE
		                                SET_SKYFREEZE_FROZEN(TRUE)
		                            ENDIF
		                        ENDIF

		                    ENDIF
						ENDIF
	                
	                    IF IS_TRANSITION_ACTIVE()
	                    AND NOT HAVE_ANY_TRANSITION_BAILS_HAPPENED() = FALSE
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IS_TRANSITION_ACTIVE All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IS_TRANSITION_ACTIVE All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                        #ENDIF
	                        SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                        SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                        SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                        NETWORK_SUPPRESS_INVITE(TRUE)
	                        RESET_NET_TIMER(SignInLoadStatsTimer)
	                        g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                        SET_RETURN_TO_SP_RELOAD(FALSE)
	                        
	                        IF NETWORK_IS_SIGNED_ONLINE() = FALSE
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE, but already handling something going wrong, ignore \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                            
	                        ELIF ((IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM()) AND NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline)
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START

	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                        ELIF ((IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM()) AND SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline)
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
								
	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                        ENDIF
	                        
	                        #IF IS_DEBUG_BUILD
	                            SET_QUICKLAUNCH_STATE(QUICKLAUNCH_EMPTY)
	                        #ENDIF
	                        
	                        TELL_TRANSITION_BAIL_HAPPENED(TRUE)
	                    ELSE
	                        #IF USE_FINAL_PRINTS
	                            IF HAVE_ANY_TRANSITION_BAILS_HAPPENED() 
	                                PRINTLN("EVENT_NETWORK_SIGN_IN_STATE_CHANGED: ignore transition action as HAVE_ANY_TRANSITION_BAILS_HAPPENED = TRUE \n ")
	                            ENDIF
	                        #ENDIF	                        
	                    ENDIF
	                ENDIF
	            BREAK
				#ENDIF // NOT FEATURE_GEN9_STANDALONE
				
				#IF FEATURE_GEN9_STANDALONE
				CASE EVENT_NETWORK_SIGN_IN_STATE_CHANGED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SIGN_IN_STATE_CHANGED")
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, SignInEvent, SIZE_OF(SignInEvent))
					
						RESET_SKYCAM_HOLD()
	                    
	                    IF GET_CONTRAINED_EVENT_ACTIVE()
	                        PRINTLN("CLEAR_EVENT_QUE - GET_CONTRAINED_EVENT_ACTIVE = TRUE so ignore this signin event.")
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("CLEAR_EVENT_QUE - GET_CONTRAINED_EVENT_ACTIVE = TRUE so ignore this signin event.")
	                        #ENDIF
	                        
	                        SET_CONTRAINED_EVENT_ACTIVE(FALSE)
	                        EXIT
	                    ENDIF
					
						IF SignInEvent.bIsActiveUser
							IF IS_ONLINE_POLICIES_MENU_ACTIVE() = FALSE
								IF GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU
									#IF IS_DEBUG_BUILD
										PRINTLN("EVENT_NETWORK_SIGN_IN_STATE_CHANGED: GET_PAUSE_MENU_STATE() = PM_IN_SC_MENU , calling CLOSE_SOCIAL_CLUB_MENU ")
									#ENDIF
									CLOSE_SOCIAL_CLUB_MENU()
								ENDIF
							ENDIF
							
							g_b_Check_Chat_Restrictions_For_Message  = FALSE // Run Chat restriction message Checks again.
	                                
	                        IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
	                        OR GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
	                        
	                            SET_ALL_CODE_STATS_NOT_LOADED()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: All Save data is being flattend, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: All Save data is being flattend, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                            #ENDIF
	                            
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE                         
	                        ENDIF
	                    
	                        IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	                    
	                            SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IN FM All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called")
	                            
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IN FM All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called")
	                            #ENDIF
								
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                        ENDIF
	                        
	                        IF HAS_IMPORTANT_STATS_LOADED()
	                        AND IS_TRANSITION_ACTIVE()
	                    
	                            SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: HAS_IMPORTANT_STATS_LOADED() and in the transition so flatten all the stats - called ")
	                            
	                            #IF USE_FINAL_PRINTS
	                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: HAS_IMPORTANT_STATS_LOADED() and in the transition so flatten all the stats - called ")
	                            #ENDIF
	                            SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                            SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                            NETWORK_SUPPRESS_INVITE(TRUE)
	                            RESET_NET_TIMER(SignInLoadStatsTimer)
	                            g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                        ENDIF
							
						ENDIF
						
						//if we were on line and not is on line, i.e. in the store
	                    IF SignInEvent.bWasOnline
	                    AND NOT SignInEvent.bIsOnline
	                    //And we're in the store
	                    AND SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
						
	                        //reset for SP bail
	                        RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	        
	                        IF IS_TRANSITION_ACTIVE() = FALSE
								IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
								OR GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
									
									PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: in FM or the creates and lost bIsOnline connection, head back to SP")
		                            #IF USE_FINAL_PRINTS
		                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: in FM or the creates and lost bIsOnline connection, head back to SP")
		                            #ENDIF
									
		                            SET_FRONTEND_ACTIVE(FALSE)
		                            SET_SKYFREEZE_FROZEN()                  
									REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, GET_CURRENT_HUD_STATE())
									TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
								ELSE
									PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: we're in SP, ignore event BUG 2624445")
		                            #IF USE_FINAL_PRINTS
		                                PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: we're in SP, ignore event - BUG 2624445")
		                            #ENDIF
								ENDIF
	                        ENDIF
	                        
	                        //fix for - 1984182 - Stuck transitioning back to freemode after losing PSN connection while in the game store
	                    ENDIF
	                    
						IF SignInEvent.bIsActiveUser
		                    IF (SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline)
		                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, mark flag to suppres the invites again when heading back to SP. \n")
		                        #IF USE_FINAL_PRINTS
		                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, mark flag to suppres the invites again when heading back to SP. \n")
		                        #ENDIF
								
								SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
		                        SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
		                        SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
		                        NETWORK_SUPPRESS_INVITE(TRUE)
		                        RESET_NET_TIMER(SignInLoadStatsTimer)
		                        g_Private_Is_Running_Boot_Invite_Supress = TRUE
		                        SET_RETURN_TO_SP_RELOAD(FALSE)
		                        
		                        IF IS_XBOX_PLATFORM()
		                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  IS_XBOX_PLATFORM() so freeze the camera ")
		                            #IF USE_FINAL_PRINTS
			                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  IS_XBOX_PLATFORM() so freeze the camera ")
			                        #ENDIF
									//Only do this if we're in MP
		                            IF IS_PLAYER_A_SP_CHARACTER() = FALSE 
		                            AND GET_IS_PLAYER_IN_ANIMAL_FORM() = FALSE
		                            AND IS_TRANSITION_ACTIVE() = FALSE
									AND IS_PLAYER_SWITCH_IN_PROGRESS() = FALSE
		                                SET_SKYFREEZE_FROZEN(TRUE)
		                            ENDIF
		                        ENDIF

		                    ENDIF
						ENDIF
	                
	                    IF IS_TRANSITION_ACTIVE()
	                    AND NOT HAVE_ANY_TRANSITION_BAILS_HAPPENED() = FALSE
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IS_TRANSITION_ACTIVE All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: IS_TRANSITION_ACTIVE All Save data is being flatted, also add in the invite so this goes back to being TRUE NETWORK_SUPPRESS_INVITE(TRUE) - called ")
	                        #ENDIF
	                        SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()
	                        SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
	                        SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
	                        NETWORK_SUPPRESS_INVITE(TRUE)
	                        RESET_NET_TIMER(SignInLoadStatsTimer)
	                        g_Private_Is_Running_Boot_Invite_Supress = TRUE
	                        SET_RETURN_TO_SP_RELOAD(FALSE)
	                        
	                        IF NETWORK_IS_SIGNED_ONLINE() = FALSE
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE, but already handling something going wrong, ignore \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NETWORK_IS_SIGNED_ONLINE() = FALSE, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                            
	                        ELIF ((IS_PROSPERO_VERSION()) AND NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline)
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START

	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: NOT SignInEvent.bWasOnline AND SignInEvent.bIsOnline, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                        ELIF ((IS_PROSPERO_VERSION()) AND SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline)
	                            IF GET_CURRENT_HUD_STATE() != HUD_STATE_CLOUD_FAIL
	                            AND GET_CURRENT_HUD_STATE() != HUD_STATE_NOT_ONLINE
								AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
								
	                                SET_BAIL_STILL_VALID(FALSE)
	                                PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED:  SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline \n")
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline \n ")
	                                #ENDIF
	                                HUD_CHANGE_STATE(HUD_STATE_NOT_ONLINE)
	                                SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                                TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                            ELSE
	                                #IF USE_FINAL_PRINTS
	                                    PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED: SignInEvent.bWasOnline AND NOT SignInEvent.bIsOnline, but already handling something going wrong, ignore \n ")
	                                #ENDIF
	                            ENDIF
	                        ENDIF
	                        
	                        #IF IS_DEBUG_BUILD
	                            SET_QUICKLAUNCH_STATE(QUICKLAUNCH_EMPTY)
	                        #ENDIF
	                        
	                        TELL_TRANSITION_BAIL_HAPPENED(TRUE)
	                    ELSE
	                        #IF USE_FINAL_PRINTS
	                            IF HAVE_ANY_TRANSITION_BAILS_HAPPENED() 
	                                PRINTLN("EVENT_NETWORK_SIGN_IN_STATE_CHANGED: ignore transition action as HAVE_ANY_TRANSITION_BAILS_HAPPENED = TRUE \n ")
	                            ENDIF
	                        #ENDIF	                        
	                    ENDIF
					
						IF NOT SignInEvent.bIsSignedIn AND SignInEvent.bWasSignedIn
								
							RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
							
							PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SIGN_IN_STATE_CHANGED - EventData valid, requesting transition to IIS")
							CLEAR_SCRIPT_ROUTER_LINK()
							REQUEST_TRANSITION_TO_IIS()
						ENDIF
					ENDIF
				BREAK
				#ENDIF // NOT FEATURE_GEN9_STANDALONE
								            
	            CASE EVENT_NETWORK_APP_LAUNCHED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_APP_LAUNCHED")
	            
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, AppLaunchEvent, SIZE_OF(AppLaunchEvent))
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - App Launched.  EVENT_NETWORK_APP_LAUNCHED - called")
	                    
	                    #IF USE_FINAL_PRINTS
	                        PRINTLN_FINAL("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - App Launched.  EVENT_NETWORK_APP_LAUNCHED - called")
	                    #ENDIF

	                    PROCESS_NET_APP_LAUNCHED_EVENT(AppLaunchEvent)
	                ENDIF
	            BREAK
	            
	            CASE EVENT_NETWORK_SYSTEM_SERVICE_EVENT
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SYSTEM_SERVICE_EVENT")
					
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, SystemServiceEvent, SIZE_OF(SystemServiceEvent))
					
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue -  EVENT_NETWORK_SYSTEM_SERVICE_EVENT - called")

	                    #IF USE_FINAL_PRINTS
	                        PRINTLN_FINAL("...KGM: (New Transition) Main Persistent Event Queue -  EVENT_NETWORK_SYSTEM_SERVICE_EVENT - called")					
						#ENDIF

	                    IF IS_PLAYSTATION_PLATFORM() 
	                    AND NETWORK_IS_NP_AVAILABLE() = FALSE 
	                    AND SystemServiceEvent.nEventID != ENUM_TO_INT(EVENT_SUSPENDED)
	                    AND SystemServiceEvent.nEventID != ENUM_TO_INT(EVENT_RESUMING)
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE - NETWORK_GET_NP_UNAVAILABLE_REASON = ", NETWORK_GET_NP_UNAVAILABLE_REASON())
	                        #IF USE_FINAL_PRINTS
		                        PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE - NETWORK_GET_NP_UNAVAILABLE_REASON = ", NETWORK_GET_NP_UNAVAILABLE_REASON())				
							#ENDIF
							IF SystemServiceEvent.nEventID = ENUM_TO_INT(EVENT_INVITATION_RECEIVED)
	                            IF (NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_AGE)
	//                              SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, TRUE)
	                                bGoBackToSP = TRUE
	                            ELIF (NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_GAME_UPDATE)
	//                              SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GAME_UPDATE, TRUE)
	                                bGoBackToSP = TRUE
	                            ELIF (NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_SYSTEM_UPDATE)
	//                              SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE, TRUE)
	                                bGoBackToSP = TRUE
	                            ELIF (NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_CONNECTION)
	//                             	SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_NP_CONNECTION, TRUE)
									#IF FEATURE_GEN9_EXCLUSIVE
									IF IS_PROSPERO_VERSION()
									AND NOT NETWORK_IS_SIGNED_IN()
									AND (GET_CURRENT_GAMEMODE() = GAMEMODE_SP OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)
										//PS5 is no longer showing the sign in UI like PS4 so setting this will bring up the sign in screen
										SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN, TRUE)
									ELSE
									#ENDIF
										bGoBackToSP = TRUE
									#IF FEATURE_GEN9_EXCLUSIVE
									ENDIF
									#ENDIF
								#IF FEATURE_GEN9_EXCLUSIVE
								ELIF (NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_SIGNED_OUT)
								AND IS_PROSPERO_VERSION()
								AND (GET_CURRENT_GAMEMODE() = GAMEMODE_SP OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)
									//PS5 is no longer showing the sign in UI like PS4 so setting this will bring up the sign in screen
									SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN, TRUE)
								#ENDIF
	                            ENDIF
	                        ENDIF
	                    ELIF IS_PLAYSTATION_PLATFORM()
	                    AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE    
	                    AND SystemServiceEvent.nEventID != ENUM_TO_INT(EVENT_SUSPENDED)
	                    AND SystemServiceEvent.nEventID != ENUM_TO_INT(EVENT_RESUMING)
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_PLAYSTATION_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE")
	                        #IF USE_FINAL_PRINTS
		                        PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_PLAYSTATION_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE")					
							#ENDIF
							
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
							OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
								bDoCameraUp = FALSE
								PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - Skip Camera up as we're in SP and the player isn't going anywhere")
								#IF USE_FINAL_PRINTS
		                        PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - Skip Camera up as we're in SP and the player isn't going anywhere")					
								#ENDIF
							ENDIF
							
							NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
	                        g_bSystem_Service_event_Shown_Upgrade = TRUE
	//                      SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION, TRUE)
	                        bGoBackToSP = TRUE
													
	                    ELIF IS_XBOX_PLATFORM()
	                    AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
	                    AND NETWORK_IS_SIGNED_IN()
						AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						
							IF SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode) = FALSE
							AND nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE
								DOES_PLAYER_HAVE_XBOX_PERMISSIONS(TRUE)
							ELSE
								DOES_PLAYER_HAVE_XBOX_GOLD_MEMBERSHIP(TRUE)
							ENDIF
							
							g_bSystem_Service_event_Shown_Upgrade = TRUE
						
	//                      SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE,TRUE)
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_XBOX_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE  ")
	                        #IF USE_FINAL_PRINTS
		                        PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - IS_XBOX_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE ")						
							#ENDIF
							IF IS_TRANSITION_ACTIVE() = FALSE
	                        AND NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE() = FALSE
	                        AND NETWORK_HAS_CONFIRMED_INVITE() = FALSE
	                        AND LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
	                            SET_SYSTEM_SERVICE_TRIGGERED(TRUE)
	                        ENDIF
	                        bGoBackToSP = TRUE
	                    ELIF IS_XBOX_PLATFORM()
	                    AND SystemServiceEvent.nEventID = ENUM_TO_INT(EVENT_RESUMING) 
	                    AND GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT -  SystemServiceEvent.nEventID = ENUM_TO_INT(EVENT_RESUMING)   ")
	                        #IF USE_FINAL_PRINTS
		                        PRINTLN_FINAL("CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT - SystemServiceEvent.nEventID = ENUM_TO_INT(EVENT_RESUMING) ")						
							#ENDIF
							SET_CONTRAINED_EVENT_ACTIVE(TRUE)
	                        
	                        HUD_CHANGE_STATE(HUD_STATE_CONSTRAINED)
	                        SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_NOT_CONNECTED)
	                        TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	                        
	                    ENDIF
	                    
	                    IF bGoBackToSP
	        
	                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) > 0 
	                            TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("startup_positioning")
	                            CPRINTLN(DEBUG_INIT_SP, "<Main_Persistent> CLEAR_EVENT_QUE - EVENT_NETWORK_SYSTEM_SERVICE_EVENT has come in, Maintransition will cleanup so kill <startup_positioning> now ")
	                            PRINTLN(" EVENT_NETWORK_SYSTEM_SERVICE_EVENT -  Event came in while SP is still setting up, no need as the transition will setup SP again, so shutdown startup_positioning.sc  ")
							ENDIF
	        
	                        //reset for SP bail
	                        RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()

	                        IF IS_TRANSITION_ACTIVE() = FALSE
	                            SET_FRONTEND_ACTIVE(FALSE)
								IF bDoCameraUp 
	                           		SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
								ENDIF
	                            SET_SKYFREEZE_FROZEN()     
								IF (GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_SESSION)
									PRINTLN("EVENT_NETWORK_SYSTEM_SERVICE_EVENT - calling REQUEST_TRANSITION_TERMINATE_MP_SESSION")	 
									REQUEST_TRANSITION_TERMINATE_MP_SESSION()
								ELSE
									PRINTLN("EVENT_NETWORK_SYSTEM_SERVICE_EVENT - calling REQUEST_TRANSITION")
									REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, GET_CURRENT_HUD_STATE())
								ENDIF
																
								TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)                
	                        ENDIF
	                    ENDIF
	                    bGoBackToSP = FALSE
	                    bDoCameraUp = TRUE
	                ENDIF
	            BREAK
	            
	            CASE EVENT_NETWORK_REQUEST_DELAY
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_REQUEST_DELAY")
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, NetworkRequestDelayEvent, SIZE_OF(NetworkRequestDelayEvent))
	                    PRINTLN("...KGM: (New Transition) Main Persistent Event Queue -  EVENT_NETWORK_REQUEST_DELAY - called")	                    
	                    SET_SERVER_REQUEST_DELAY_TIME(NetworkRequestDelayEvent.delayTimeMs)	                                        
	                ENDIF	            
	            BREAK
	            
	            CASE EVENT_NETWORK_PRESENCE_INVITE
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PRESENCE_INVITE")            
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PresenceInviteEvent, SIZE_OF(PresenceInviteEvent))
	                    PRINTLN("...KGM: (New Transition) Main Persistent Event Queue - EVENT_NETWORK_PRESENCE_INVITE -called ")
	                ENDIF
	            BREAK
	            
	            CASE EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED")
	                //IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PresenceInviteEvent, SIZE_OF(PresenceInviteEvent))
					
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED -called ")

	                    IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
	                        SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE) 
	                        SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE) 
	                        g_i_Private_LastGenCharacterAttempt = 0
	                        PRINTLN("[LASTGEN] CLEAR_EVENT_QUE - EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED - NETWORK_HAS_SOCIAL_CLUB_ACCOUNT is TRUE. Reset all the checks to run again.")
	            
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("[LASTGEN] CLEAR_EVENT_QUE - EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED - NETWORK_HAS_SOCIAL_CLUB_ACCOUNT is TRUE. Reset all the checks to run again.")
	                        #ENDIF
	            
	                    ELSE
	                        NET_NL()NET_PRINT("[LASTGEN] CLEAR_EVENT_QUE - EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED - NETWORK_HAS_SOCIAL_CLUB_ACCOUNT is FALSE. Do nothing.")
	                    
	                        #IF USE_FINAL_PRINTS
	                            PRINTLN_FINAL("[LASTGEN] CLEAR_EVENT_QUE - EVENT_NETWORK_SOCIAL_CLUB_ACCOUNT_LINKED - NETWORK_HAS_SOCIAL_CLUB_ACCOUNT is FALSE. Do nothing.")
	                        #ENDIF
	                    ENDIF
	               // ENDIF
	            BREAK
	            
	            CASE EVENT_NETWORK_SCADMIN_RECEIVED_CASH
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SCADMIN_RECEIVED_CASH")
					
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, scEventReceivedCash, SIZE_OF(scEventReceivedCash))
	                    CPRINTLN(DEBUG_SYSTEM,"CLEAR_EVENT_QUE - Player ",scEventReceivedCash.characterId, " received cash value ",scEventReceivedCash.cashAmount," from SCAdmin")
	                    CREDIT_BANK_ACCOUNT(INT_TO_ENUM(enumCharacterList,scEventReceivedCash.characterId),BAAC_CASH_DEPOSIT,scEventReceivedCash.cashAmount)
	                ENDIF
	            BREAK
	            
	            CASE EVENT_NETWORK_SCADMIN_PLAYER_UPDATED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SCADMIN_PLAYER_UPDATED")
					
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ScAdminPlayerUpdated, SIZE_OF(ScAdminPlayerUpdated))
					
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED -called ")	                    
	                    
	                    inventoryRefresh = FALSE
	                    PlayerBalanceRefresh = FALSE
	                    
	                    //Possible hashes are : CASH, ITEMS, XP
	                    IF ScAdminPlayerUpdated.m_awardTypeHash = 1
	                        //Miguel will give me the hash, cash or items
	                        PRINTLN("[NET_SHOP] CLEAR_EVENT_QUE - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED: m_awardTypeHash = ", ScAdminPlayerUpdated.m_awardTypeHash, " ")
	                    ENDIF
	                    
	                    IF ScAdminPlayerUpdated.m_awardAmount != 0 
	                        //Cash amount
	                        PRINTLN("[NET_SHOP] CLEAR_EVENT_QUE - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED: m_awardAmount = ", ScAdminPlayerUpdated.m_awardAmount, " ")
	                        
							IF ScAdminPlayerUpdated.m_awardAmount = -99
								g_i_DoCashGiftMessageAmount = -99
	                            g_i_DoCashGiftACTUALAmount = ScAdminPlayerUpdated.m_awardAmount
	                        ELIF ScAdminPlayerUpdated.m_awardAmount < 0
	                            g_i_DoCashGiftMessageAmount = -1
	                            g_i_DoCashGiftACTUALAmount = ScAdminPlayerUpdated.m_awardAmount
	                        ELIF ScAdminPlayerUpdated.m_awardAmount > 0
	                            g_i_DoCashGiftMessageAmount = 1
	                            g_i_DoCashGiftACTUALAmount = ScAdminPlayerUpdated.m_awardAmount
	                        ENDIF
	                        
	                        PlayerBalanceRefresh = TRUE
	                        
	                    ENDIF

	                    FOR I = 0 TO MAX_NUM_ITEMS-1
	                        IF ScAdminPlayerUpdated.m_items[I] != 0
	                            //Item
	                            PRINTLN("[NET_SHOP] CLEAR_EVENT_QUE - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED: m_items[", I, "] = ", ScAdminPlayerUpdated.m_awardAmount, " ")
	                            inventoryRefresh = TRUE
	                            
	                        ENDIF
	                    ENDFOR
	                    
	                    //more might come through here. 	                    
	                    IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	                        //in SP ignore this
	                        //this will start a session refresh
	                        IF ScAdminPlayerUpdated.m_fullRefreshRequested = 1
	                            //call native command
	                            PRINTLN("[NET_SHOP] CLEAR_EVENT_QUE - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED: m_fullRefreshRequested = TRUE , inventoryRefresh = ",inventoryRefresh, " , PlayerBalanceRefresh = ", PlayerBalanceRefresh, " ")
	                            NET_GAMESERVER_START_SESSION_RESTART(inventoryRefresh, PlayerBalanceRefresh)	                            
	                        ENDIF	                    
	                    
	                        //in SP ignore this
	                        //in MP get the update player balance
	                        IF ScAdminPlayerUpdated.m_updatePlayerBalance = 1
	                            PRINTLN("[NET_SHOP] CLEAR_EVENT_QUE - EVENT_NETWORK_SCADMIN_PLAYER_UPDATED: m_updatePlayerBalance = TRUE ")
	                        ENDIF
	                    ENDIF
	                ENDIF
	            BREAK            
	            
	            CASE EVENT_NETWORK_ADMIN_INVITED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_ADMIN_INVITED")
					
	                //players in the session about to recieve an R* admin player are sent this. 
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, AdminInviteEvent, SIZE_OF(AdminInviteEvent))
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - EVENT_NETWORK_ADMIN_INVITED -called ")
	                    
	                    IF NETWORK_IS_HOST()
	                    
							PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_ADMIN_INVITED: IS HOST - NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = ")NET_PRINT_INT(NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV))
	                        IF NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = 0
	    //                      AdminInviteEvent.hInviter
	    //                          
	                            FOR FindPlayerInt = 0 TO NUM_NETWORK_PLAYERS-1
	                                
	                                FindPlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, FindPlayerInt)
	                                
	                                IF IS_PLAYER_SCTV(FindPlayerIndex)
	                                AND NETWORK_PLAYER_IS_ROCKSTAR_DEV(FindPlayerIndex) = FALSE
	                                    PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_ADMIN_INVITED: KICKING Spectator ", GET_PLAYER_NAME(FindPlayerIndex), " player index = ", FindPlayerInt)

	                                    BROADCAST_SCRIPT_EVENT_BAIL_ME_FOR_SCTV(FindPlayerIndex)
	                                    
	                                    FindPlayerInt = NUM_NETWORK_PLAYERS
	                                ENDIF
	                            
	                            ENDFOR
	                            
	                        ENDIF
	                    ELSE
	                       	PRINTLN("EVENT_NETWORK_ADMIN_INVITED: NOT HOST - NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = ", NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV))
	                    ENDIF	                    
	              ENDIF
	            BREAK

	            CASE EVENT_NETWORK_SPECTATE_LOCAL
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SPECTATE_LOCAL")
					
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, SpecLocalEvent, SIZE_OF(SpecLocalEvent))
	                    PRINTLN("CLEAR_EVENT_QUE - ...KGM: (New Transition) Main Persistent Event Queue - EVENT_NETWORK_SPECTATE_LOCAL -called ")
	                    
	                    IF NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) > 0	                    
	                    
	                        IF IS_GAMER_HANDLE_VALID(SpecLocalEvent.hGamer)
	                            //ThePlayerForSpecLocal = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(SpecLocalEvent.hGamer)
	                            
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SPECTATE_LOCAL: NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = ", NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV))

	                            IF NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = 0	                                
	                                
	                                FOR FindPlayerInt = 0 TO NUM_NETWORK_PLAYERS-1
	                                    
	                                    FindPlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, FindPlayerInt)
	                                    
	                                    IF IS_PLAYER_SCTV(FindPlayerIndex)
	                                    AND NETWORK_PLAYER_IS_ROCKSTAR_DEV(FindPlayerIndex) = FALSE
	                                        PRINTLN("EVENT_NETWORK_SPECTATE_LOCAL: KICKING Spectator ", GET_PLAYER_NAME(FindPlayerIndex), " player index = ", FindPlayerInt)

	                                        BROADCAST_SCRIPT_EVENT_BAIL_ME_FOR_SCTV(FindPlayerIndex)	                                        
	                                        
	                                        FindPlayerInt = NUM_NETWORK_PLAYERS
	                                    ENDIF
	                                
	                                ENDFOR
	                            ENDIF
	                            
	                            SET_FRONTEND_ACTIVE(FALSE)
	                            
	                            NETWORK_SESSION_SET_MATCHMAKING_GROUP(MM_GROUP_SCTV)

	                            FindPlayerIndex = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(SpecLocalEvent.hGamer)

	                            IF FindPlayerIndex != INVALID_PLAYER_INDEX()
	                                MPSpecGlobals.pedDesiredFocus = GET_PLAYER_PED(FindPlayerIndex)
	                                MPSpecGlobals.pedCurrentFocus = GET_PLAYER_PED(FindPlayerIndex)
	                                PRINTLN("EVENT_NETWORK_SPECTATE_LOCAL: MPSpecGlobals.pedDesiredFocus = ", GET_PLAYER_NAME(FindPlayerIndex))
	                                PRINTLN("EVENT_NETWORK_SPECTATE_LOCAL: MPSpecGlobals.pedCurrentFocus = ", GET_PLAYER_NAME(FindPlayerIndex))
	                                SET_SCTV_FOLLOW_TARGET(FindPlayerIndex)
	                            ENDIF
	                            
	                            //2473835, pause the renderphase if we're in chris's special SCTV camera
	                            IF NATIVE_TO_INT(PLAYER_ID()) > -1
	                                IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
	                                    SET_SKYFREEZE_FROZEN(TRUE)
	                                ENDIF   
	                            ENDIF
	                            
	                            PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SPECTATE_LOCAL: SET_PLAYER_TEAM(PLAYER_ID(), TEAM_SCTV)")
	                            SET_ENTERED_GAME_AS_SCTV(TRUE)
	                            IF IS_ROCKSTAR_DEV()
	                                SET_ENTERED_GAME_AS_PRIVATE_SCTV(TRUE)
	                            ENDIF
	                            SET_PLAYER_TEAM(PLAYER_ID(), TEAM_SCTV)	                            
	                        ENDIF
	                    ELSE
	                        PRINTLN("CLEAR_EVENT_QUE - EVENT_NETWORK_SPECTATE_LOCAL: NETWORK_SESSION_GET_MATCHMAKING_GROUP_FREE(MM_GROUP_SCTV) = 0, not moving.")	                        
	                        SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_NO_SPECTATE_SPACES, TRUE)
	                    ENDIF 
	                ENDIF
	            BREAK
				
	            //Transition session events.    
	            CASE EVENT_NETWORK_PLAYER_LEFT_SESSION
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PLAYER_LEFT_SESSION")
	                IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))
	                   PROCESS_EVENT_NETWORK_TRANSITION_PLAYER_LEFT(data)       
	                #IF IS_DEBUG_BUILD
	                ELSE
	                    SCRIPT_ASSERT("IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, eventID, data, SIZE_OF(data))")
	                #ENDIF
	                ENDIF
	            BREAK       
	            
	            CASE EVENT_NETWORK_TRANSITION_STARTED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_STARTED")
	                PROCESS_EVENT_NETWORK_TRANSITION_STARTED(iCount)
					IF NETWORK_IS_TRANSITION_HOST()
	                    PRINTLN("[TS] - CLEAR_EVENT_QUE - EVENT_NETWORK_TRANSITION_STARTED - NETWORK_SET_ACTIVITY_SPECTATOR_MAX(", GET_MAX_NUMBER_OF_SPECTATORS(), ")")
	                	NETWORK_SET_ACTIVITY_SPECTATOR_MAX(GET_MAX_NUMBER_OF_SPECTATORS())
	            	ENDIF
				BREAK
	            
	            CASE EVENT_NETWORK_TRANSITION_EVENT
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED")
	                PROCESS_EVENT_NETWORK_TRANSITION_EVENT(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED")
	                PROCESS_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_TRANSITION_STRING_CHANGED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_STRING_CHANGED")
	                PROCESS_EVENT_NETWORK_TRANSITION_STRING_CHANGED(iCount)
	            BREAK       
	            
	            CASE EVENT_NETWORK_TRANSITION_MEMBER_JOINED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_MEMBER_JOINED")
	                PROCESS_EVENT_NETWORK_TRANSITION_MEMBER_JOINED(iCount)
	            BREAK   
	            
	            CASE EVENT_NETWORK_TRANSITION_MEMBER_LEFT
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_MEMBER_LEFT")
	                PROCESS_EVENT_NETWORK_TRANSITION_MEMBER_LEFT(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_TRANSITION_GAMER_INSTRUCTION
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_TRANSITION_GAMER_INSTRUCTION")
	                PROCESS_EVENT_NETWORK_TRANSITION_GAMER_INSTRUCTION(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_PRESENCE_INVITE_REPLY
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PRESENCE_INVITE_REPLY")
	                PROCESS_EVENT_NETWORK_INVITE_REPLY(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_PRESENCE_TRIGGER
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PRESENCE_TRIGGER")
	                PROCESS_EVENT_NETWORK_PRESENCE_TRIGGER(iCount)              
	            BREAK
	            
	            CASE EVENT_NETWORK_PRESENCE_INVITE_REMOVED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PRESENCE_INVITE_REMOVED")
	                PROCESS_EVENT_NETWORK_INVITE_REMOVED(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_CASH_TRANSACTION_LOG
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_CASH_TRANSACTION_LOG")
	                PROCESS_EVENT_NETWORK_ATM_LOG(iCount)
	            BREAK
	            
	            CASE EVENT_NETWORK_NETWORK_ROS_CHANGED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_NETWORK_ROS_CHANGED")
	                PROCESS_EVENT_NETWORK_NETWORK_ROS_CHANGED(iCount)           
	            BREAK
	            
	            CASE EVENT_NETWORK_FOLLOW_INVITE_RECEIVED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_FOLLOW_INVITE_RECEIVED")
	                PROCESS_EVENT_NETWORK_FOLLOW_INVITE_RECEIVED(iCount)           
	            BREAK
	            
	            //Added by Steve T 16/01/15. We need to support Eyefind email notifications regardless of Social Club account linking.  See #2195214
	            CASE EVENT_NETWORK_EMAIL_RECEIVED
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_EMAIL_RECEIVED")
	                PROCESS_MAINPER_NETWORK_EMAIL_RECEIVED_EVENT()
	            BREAK

	             //Added by Steve T 14/10/15. We need to support email notifications for Marketing events.  #2534125 and #2556742	            
	            CASE EVENT_NETWORK_MARKETING_EMAIL_RECEIVED//We need the new Event from Camille put in here.
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_MARKETING_EMAIL_RECEIVED")    
	                #if IS_DEBUG_BUILD
	                    cdPrintString("Main_Persistent - Received Marketing Email Event. Processing..,")
	                    cdPrintnl()
	                #endif
	                PROCESS_MAINPER_NETWORK_MARKETING_EMAIL_RECEIVED_EVENT()
	            BREAK	            

	            // KGM 12/7/14: To Handle the asynchonous checking for blocked players when receiving a same-session Invite or Basic Invite from a player
	            CASE EVENT_NETWORK_PERMISSION_CHECK_RESULT
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_PERMISSION_CHECK_RESULT")
	                Process_Event_Network_Permission_Check_Result(iCount)
	            BREAK

	            CASE EVENT_NETWORK_SHOP_TRANSACTION
					PRINTLN("CLEAR_EVENT_QUE - Process event: EVENT_NETWORK_SHOP_TRANSACTION")
	                PRINTLN("[CASH] RECEIVED SHOP TRANSACTION EVENT: EVENT_NETWORK_SHOP_TRANSACTION")
	                PROCESS_EVENT_NETWORK_SHOP_TRANSACTION(iCount)
	            BREAK
				
				#IF FEATURE_GTAO_MEMBERSHIP
				CASE EVENT_NETWORK_SC_BENEFITS_STATUS
					PROCESS_EVENT_NETWORK_SC_BENEFITS_STATUS(iCount)
				BREAK
				#ENDIF
	        ENDSWITCH
		ENDIF
    ENDREPEAT
ENDPROC

USING "streamed_scripts.sch"
USING "transition_common.sch"
//Relaunch the main freemode script
FUNC BOOL MAINTAIN_LAUNCHING_FREEMODE_SCRIPT()
    SET_CURRENT_GAMEMODE(GAMEMODE_FM)               
    IF NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE) = FALSE
        PRINTLN("MAINTAIN_LAUNCHING_FREEMODE_SCRIPT - main_persistant - GET_FRAME_COUNT START_LAUNCH_SCRIPT = ", GET_FRAME_COUNT())
        IF START_LAUNCH_SCRIPT()
            DISABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)
            DISABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_CNC)
            ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_MULTIPLAYER)
            ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_FREEMODE)
            
            SET_TRANSITION_TO_IGNORE_LAUNCH_SCRIPT_START(TRUE)
            ENTERING_FRESH_GAME()
            PRINTLN("MAINTAIN_LAUNCHING_FREEMODE_SCRIPT - main_persistant - START_LAUNCH_SCRIPT = TRUE = ", GET_FRAME_COUNT())
            RETURN TRUE
        ENDIF
    ELSE
        PRINTLN("[TS] NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE) = TRUE")
    ENDIF
    RETURN FALSE
ENDFUNC 

PROC MAINTAIN_RELAUNCHING_FREEMODE_AFTER_JIP()
    //wait for the network game to be in progress and to have recieved the event that launch is done
    IF NETWORK_IS_GAME_IN_PROGRESS()
    AND HAS_TRANSITION_SESSION_HAS_RECIEVED_EVENT_TRANSITION_END_LAUNCH()
        //If we've reguested the scripts
        IF sTranVars.bRequestScriptsJip = TRUE
            //load and launch freemode
            IF MAINTAIN_LAUNCHING_FREEMODE_SCRIPT()
                //clean up
                RESET_TRANSITION_SESSION_NON_RESET_VARS()
                CLEAR_TRANSITION_SESSIONS_BITSET()
                CLEAR_TRANSITION_SESSION_LAUNCHING()
                //set up the jip flag
                SET_TRANSITION_SESSION_USED_JIP()
                sTranVars.bRequestScriptsJip = FALSE
            ENDIF
        ELSE
            PRINTLN("[TS] main_persistant - waiting for NETWORK_IS_GAME_IN_PROGRESS to be false")
        ENDIF
    ELSE
        //Network game not in progress request script and wait
        PRINTLN("[TS] main_persistant - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
        IF sTranVars.bRequestScriptsJip = FALSE
            SET_CURRENT_GAMEMODE(GAMEMODE_FM)
            REQUEST_FM_RESTART_SCRIPTS()
            PRINTLN("[TS] main_persistant - REQUEST_FM_RESTART_SCRIPTS")
            sTranVars.bRequestScriptsJip = TRUE
        ENDIF
    ENDIF
ENDPROC

//Need to load all the MP scripts for Davie G
PROC MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH()
    //If the flag to kill is still true
    IF SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
        IF NOT  NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE)
        AND NOT NETWORK_IS_SCRIPT_ACTIVE("fake_interiors", -1, TRUE)
        AND NOT NETWORK_IS_SCRIPT_ACTIVE("emergencycalllauncher", -1, TRUE)
        AND NOT NETWORK_IS_SCRIPT_ACTIVE("net_cloud_mission_loader", -1, TRUE)
        AND NOT NETWORK_IS_SCRIPT_ACTIVE("FMMC_Launcher", -1, TRUE)
            CLEAR_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
            SET_CURRENT_GAMEMODE(GAMEMODE_FM)
            REQUEST_FM_RESTART_SCRIPTS()
        #IF IS_DEBUG_BUILD
        ELSE
            IF NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE)
                PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - GET_LAUNCH_SCRIPT_NAME")
            ENDIF
            IF NETWORK_IS_SCRIPT_ACTIVE("fake_interiors", -1, TRUE)
                PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - fake_interiors")
            ENDIF
            IF NETWORK_IS_SCRIPT_ACTIVE("emergencycalllauncher", -1, TRUE)
                PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - emergencycalllauncher")
            ENDIF
            IF NETWORK_IS_SCRIPT_ACTIVE("net_cloud_mission_loader", -1, TRUE)
                PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - net_cloud_mission_loader")
            ENDIF
            IF NETWORK_IS_SCRIPT_ACTIVE("FMMC_Launcher", -1, TRUE)
                PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - FMMC_Launcher")
            ENDIF           
        #ENDIF
        ENDIF
    //Everything is dead then re quest scripts
    ELSE
        //load and launch freemode
        IF MAINTAIN_LAUNCHING_FREEMODE_SCRIPT()
            //clean up
            RESET_TRANSITION_SESSION_NON_RESET_VARS()
            CLEAR_TRANSITION_SESSIONS_BITSET()
            CLEAR_TRANSITION_SESSION_LAUNCHING()
            CLEAR_KILL_ALL_MP_SCRIPTS_FOR_SCTV_SWITCH()
            SET_TRANSITION_SESSION_ENTERING_FM_AFTER_SCRIPTS_KILLED_FOR_SCTV_SWITCH()
            PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - MAINTAIN_LAUNCHING_FREEMODE_SCRIPT")
        #IF IS_DEBUG_BUILD
        ELSE
            PRINTLN("[TS] main_persistant - MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH - MAINTAIN_LAUNCHING_FREEMODE_SCRIPT = FALSE")
        #ENDIF
        ENDIF
    ENDIF
ENDPROC

BOOL bPlayerFrosen
BOOL bWarpPlayer
BOOL bSetWarpPosition
BOOL bSpawnLocationNeedsCleared
INT iControlTimer
FUNC BOOL HAS_WALK_OUT_TIME_EXPIRED()
    IF NOT NETWORK_IS_GAME_IN_PROGRESS()
        RETURN TRUE
    ENDIF
    IF GET_GAME_TIMER() - iControlTimer > 0
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

//Pull the cam up for a transition session change
PROC MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()   

	PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - called ")

    //hide stuff
    THEFEED_HIDE_THIS_FRAME()
    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
    HIDE_HUD_AND_RADAR_THIS_FRAME()
    IF IS_SKYCAM_PAST_FIRST_CUT()
        IF bPlayerFrosen = FALSE
        
            bWarpPlayer = TRUE
        
            IF iControlTimer = 0
                iControlTimer = GET_GAME_TIMER() + 500
            ENDIF
            IF HAS_WALK_OUT_TIME_EXPIRED() 
                NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)  
                bPlayerFrosen = TRUE
                iControlTimer = 0
            ENDIF 
        ENDIF 
    ELSE
        IF NETWORK_IS_GAME_IN_PROGRESS()
            HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
        ENDIF
        bWarpPlayer = FALSE
        bPlayerFrosen = FALSE
        bSetWarpPosition = FALSE
        iControlTimer = 0
    ENDIF
    
    IF bWarpPlayer  
		
		IF IS_VECTOR_ZERO(g_vSafeLocationOutsideApartment)
			PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - g_vSafeLocationOutsideApartment is zero! ")
	        bWarpPlayer = FALSE
		ELSE
	        IF SHOULD_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY()
	        
	            IF NOT bSetWarpPosition
	                PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - Setup specific spawn location: ", g_vSafeLocationOutsideApartment)
	                FLOAT fHeading = 0.0
	                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	                    fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	                ENDIF
	                SETUP_SPECIFIC_SPAWN_LOCATION(g_vSafeLocationOutsideApartment, fHeading, 10.0, FALSE, 0, FALSE)
	                bSetWarpPosition = TRUE
	                bSpawnLocationNeedsCleared = TRUE
					
					MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES()
					
	            ELSE
	                VECTOR vSpawnLocation
	                FLOAT fSpawnHeading
	                IF HAS_GOT_SPAWN_LOCATION(vSpawnLocation, fSpawnHeading, SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE, FALSE, FALSE, FALSE, TRUE)
	                    
	                    PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - Warping outside property: ", vSpawnLocation, fSpawnHeading)
	                    SET_ENTITY_COORDS(PLAYER_PED_ID(), vSpawnLocation, FALSE, FALSE, FALSE)
	                    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	                        SET_ENTITY_HEADING(PLAYER_PED_ID(), fSpawnHeading)
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
	                    ENDIF

	                    bWarpPlayer = FALSE
	                    
	                    CLEAR_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY()
	                    bSpawnLocationNeedsCleared = FALSE
	                    CLEAR_SPECIFIC_SPAWN_LOCATION()
	                    
	                #IF IS_DEBUG_BUILD
	                ELSE
	                    PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - HAS_GOT_SPAWN_LOCATION() = FALSE")
	                #ENDIF
	                    
	                ENDIF
	            ENDIF
	        
	        ELIF SHOULD_TRANSITION_SESSION_SPAWN_PLAYER_AT_PROPERTY_COORDS()
	            
	            PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - Warping to property coords: ", g_vSafeLocationOutsideApartment, g_fSafeLocationOutsideApartmentHeading)
	            SET_ENTITY_COORDS(PLAYER_PED_ID(), g_vSafeLocationOutsideApartment, FALSE, FALSE, FALSE)
	            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	                SET_ENTITY_HEADING(PLAYER_PED_ID(), g_fSafeLocationOutsideApartmentHeading)
	            ENDIF

	            bWarpPlayer = FALSE
	            
	            CLEAR_TRANSITION_SESSION_SPAWN_PLAYER_AT_PROPERTY_COORDS()
			//If we are under the map, and we're quitting the lobby we should prbably mve the player.
	        ELIF IS_TRANSITION_SESSION_QUITING_CORONA()   
				PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - IS_TRANSITION_SESSION_QUITING_CORONA ")
				VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				IF vPos.z < 0.00
		            PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - IS_TRANSITION_SESSION_QUITING_CORONA - Warping to property coords: ", g_vSafeLocationOutsideApartment, g_fSafeLocationOutsideApartmentHeading)
		            SET_ENTITY_COORDS(PLAYER_PED_ID(), g_vSafeLocationOutsideApartment, FALSE, FALSE, FALSE)
		            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		                SET_ENTITY_HEADING(PLAYER_PED_ID(), g_fSafeLocationOutsideApartmentHeading)
		            ENDIF
				ENDIF
	            bWarpPlayer = FALSE
	        ELSE
	            PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - Don't need to warp: ", g_vSafeLocationOutsideApartment)
	            bWarpPlayer = FALSE
	        ENDIF
		ENDIF

    ENDIF   
    
    //Pull the cam up
    IF SET_SKYSWOOP_UP(FALSE, FALSE, DEFAULT, DEFAULT, SHOULD_TRANSITION_SESSION_PULL_CAM_UP_SKIP_INTRO())
    AND NOT bWarpPlayer
        CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
        CLEAR_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY()
        CLEAR_TRANSITION_SESSION_SPAWN_PLAYER_AT_PROPERTY_COORDS()
		CLEAR_TRANSITION_SESSION_PULL_CAM_UP_SKIP_INTRO()
        SET_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
        bPlayerFrosen = FALSE
        bWarpPlayer = FALSE
        bSetWarpPosition = FALSE
        
        IF GET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT()
            PRINTLN("MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE - Clear heist flags for skyswoop.")
            CLEAR_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT()
        ENDIF
    ENDIF
ENDPROC
//Pull the cam up for a transition session change
PROC MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
    //hide stuff
    THEFEED_HIDE_THIS_FRAME()
    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
    HIDE_HUD_AND_RADAR_THIS_FRAME()
    
    //If we need to hold the camera for the apratment script then do so
    IF TRANSITION_SESSION_HOLD_CAM_FOR_APARTMENT_SCRIPT()
        //If the apartment script is ready
        IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
            //Clear flag and move on
            CLEAR_TRANSITION_SESSION_HOLD_CAM_FOR_APARTMENT_SCRIPT()
        ELSE
            //Drop out
            PRINTLN("main_persistant - TRANSITION_SESSION_HOLD_CAM_FOR_APARTMENT_SCRIPT - Not PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY")
            EXIT
        ENDIF
    ENDIF
    
	IF g_SimpleInteriorData.bHoldSkycam
		PRINTLN("main_persistant - MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE - g_SimpleInteriorData.bHoldSkycam")
        EXIT
	ENDIF
	
	IF g_sTransitionSessionData.iTransitionSessionHoldForSMPLintRestart > -1
		SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX intBounds
		GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(INT_TO_ENUM(SIMPLE_INTERIORS, g_sTransitionSessionData.iTransitionSessionHoldForSMPLintRestart), intBounds, FALSE )
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_ENTITY_IN_AREA(PLAYER_PED_ID(), intBounds.vInsideBBoxMin, intBounds.vInsideBBoxMax)
			PRINTLN("main_persistant - MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE - iTransitionSessionHoldForSMPLintRestart")
	        EXIT
		ELSE
			PRINTLN("main_persistant - MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE - setting iTransitionSessionHoldForSMPLintRestart != -1. Player is not in the area. Are they alive? ",
						IS_ENTITY_ALIVE(PLAYER_PED_ID()), " Are they in the interior? ", IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID()))
	        g_sTransitionSessionData.iTransitionSessionHoldForSMPLintRestart = -1
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		PRINTLN("main_persistant - MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE - player is spectating")
		EXIT
	ENDIF
	
	
    //Pull the cam down
    IF SET_SKYSWOOP_DOWN(NULL, FALSE)
        CLEAR_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
        CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
        IF TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
			SET_SKYFREEZE_CLEAR(TRUE)
            NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
            CLEAR_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
        ENDIF
    ENDIF
ENDPROC


PROC MAINTAIN_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
    PRINTLN("main_persistant - MAINTAIN_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM - I'm hiding Things")
    //wait for the network game to be in progress and to have recieved the event that launch is done
    HIDE_HUD_AND_RADAR_THIS_FRAME()
    DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE(SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT() = FALSE)
    DISABLE_CELLPHONE_THIS_FRAME_ONLY()
    THEFEED_HIDE_THIS_FRAME()
    REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() 
    //If the player is back in freemode running the clean up the hide radar flag.
    IF GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID()) = MAIN_GAME_STATE_RUNNING
    AND NOT SHOULD_DO_TRANSIITON_TO_NEW_FREEMODE_SESSION_AFER_CORONA_AND_HEIST_CUTSCENE()
        CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
    ENDIF
ENDPROC

//Hide the radar if the sky cam is not at the ground. 
PROC MAINTAIN_HIDE_RADAR_WHEN_SKY_CAM_ACTIVE()
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF NOT IS_SKYSWOOP_AT_GROUND()
           // PRINTLN("main_persistant - MAINTAIN_HIDE_RADAR_WHEN_SKY_CAM_ACTIVE - I'm hiding Things")
            HIDE_ALL_SKYCAM_HUD_THIS_FRAME()
        ENDIF
    ENDIF
ENDPROC

PROC MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED()
    //wait for the network game to be in progress and to have recieved the event that launch is done
    
    //Hide the radra and disable the cell phone
    HIDE_HUD_AND_RADAR_THIS_FRAME()
    DISABLE_CELLPHONE_THIS_FRAME_ONLY()
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE) //Added for Exploit 2423584
    
    IF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
    OR SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
    OR SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()
    OR SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()           
    OR TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
    OR TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
        DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE(SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT() = FALSE)
        DISABLE_SELECTOR_THIS_FRAME()
    ENDIF
    
    //Pull the camera up after exiting FM after going into the store
    IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
    
        //Clean up the transition session if we're starting on Call
        IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()  
            IF CLEAN_UP_TRANSITION_SESSION_ONLY()
                CLEAR_TRANSITION_SESSIONS_STARTING_ON_CALL()
                g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE
            ENDIF
        ENDIF
        
        //If the main transition is active
        IF IS_TRANSITION_ACTIVE()
            CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
            IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
                CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
            ENDIF
            PRINTLN("[TS] MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED - calling RESET_STORE_NETWORK_GAME_TRACKING")
            RESET_STORE_NETWORK_GAME_TRACKING()
        ENDIF
        
        //Pull the camera up
        IF NOT SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
            //If we've not toggeled the render
            IF NOT sTranVars.bToggelRender
            //And the spinner is up
            AND sTranVars.bRequestSpinner
            AND NOT g_bStoreTransitionForCash
                TOGGLE_RENDERPHASES(TRUE)
                TOGGLE_RENDERPHASES(TRUE)
                sTranVars.bToggelRender = TRUE              
            ENDIF
            //add that spinner
            DO_FMMC_CORONA_SPINNER(sTranVars.bRequestSpinner,  "FMMC_PLYLOAD", ENUM_TO_INT(LOADING_ICON_LOADING), "")
            //delay the store opening
            DELAY_MP_STORE_OPEN()
            
            BOOL bComplete = FALSE
            
            IF g_bStoreTransitionForCash
                bComplete = SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
            ELSE
                bComplete = SET_SKYSWOOP_UP(FALSE, FALSE, TRUE, SWITCH_TYPE_LONG, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
            ENDIF
            
            #IF USE_FINAL_PRINTS PRINTLN_FINAL("MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED: 7799618 bComplete = ",bComplete) #ENDIF
            IF bComplete
                g_bStoreTransitionForCash = FALSE
                
                sTranVars.bRequestSpinner = FALSE
                sTranVars.bToggelRender = FALSE
                
                BUSYSPINNER_OFF()
                PRELOAD_BUSYSPINNER()
                SHOW_GAME_TOOL_TIPS(TRUE)
                SET_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
                //if the down flag is set then clear it. 
//              IF IS_TRANSITION_SESSIONS_CAM_DOWN_AFTER_END_RESERVING_SLOT()
//                  CLEAR_TRANSITION_SESSIONS_CAM_DOWN_AFTER_END_RESERVING_SLOT()
//              ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    //Maintain the button if the news feed is active
    MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION()
    
    //If we're back in the game and ready
    IF IS_TRANSITION_SESSION_READY_FOR_TO_GAME_LAUNCH(sTranVars.bRequestScripts)    
        //If we've reguested the scripts
        IF sTranVars.bRequestScripts = TRUE
            DO_FMMC_CORONA_SPINNER(sTranVars.bRequestSpinner,  "FMMC_PLYLOAD", ENUM_TO_INT(LOADING_ICON_LOADING), "FMMC_STTRANOC")
            //load and launch freemode
            IF MAINTAIN_LAUNCHING_FREEMODE_SCRIPT()
                //clean up
                CLEAR_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
                CLEAR_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
                //clear we got the event if we're waiting for it
                IF HAS_TRANSITION_SESSIONS_GOT_EVENT_TRANSITION_TO_GAME_FINISH()
                    CLEAR_TRANSITION_SESSIONS_GOT_EVENT_TRANSITION_TO_GAME_FINISH()
                ENDIF
                //If we are cleaning up after a corona walk out
                IF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
                    //Nothing
                ELIF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
                
                    
                    
                    //kill the spinner
                    sTranVars.bRequestSpinner = FALSE
                    sTranVars.bToggelRender = FALSE                 
                    IF TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE_SET_UP()
                        CLEAR_TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE_SET_UP()
                    ENDIF
                    //If we accepted an invite when in the end reserve slot state then kill it all!
                    IF TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
                        SET_SKIP_WAIT_FOR_TRANSITION_CAMERA()
                        SET_TRANSITION_SESSIONS_ACCEPTING_INVITE_FROM_SP()
                        CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
                        CLEAR_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT_IS_CAM_UP()
                        CLEAR_TRANSITION_SESSIONS_RESTORE_VEHICLE()
                        CLEAR_TRANSITION_SESSION_END_SESSION_POS()  
                    ENDIF
                //If we are cleaning up after a late join to a transition session
                ELIF TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
                    //Nothing
                //joinin a launched mission
                ELIF TRANSITION_SESSION_JOINING_LAUNCHED_TRANSITION_SESSION()
                    SET_SKIP_WAIT_FOR_TRANSITION_CAMERA()
                //If the sky cam needs to clean up
                ELIF SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
                OR SHOULD_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME() 
                    //Nothing                
                ELSE
                //a clean up after a mission it is then
                    SET_SKIP_WAIT_FOR_TRANSITION_CAMERA()
                    IF NOT IS_TRANSITION_SESSION_LAUNCHING()
                        //Set williams camera to start the clean up
                        g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanUpReadyToMoveOn = TRUE
                        PRINTLN("[TS] [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanUpReadyToMoveOn = TRUE - called from main persistants")
                        //set the clean up stage to the correct stage
                        g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage =  ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD
                        PRINTLN("[TS] [PMC] - Going to stage ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD - called from main persistants")
                    ENDIF
                    //If we were in the store and we've got here then we need to clean up the flag to push back into the post mission clean up
                    IF TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
                        PRINTLN("[TS] [PMC] - Calling ")
                        CLEAR_TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
                    ENDIF
                ENDIF
                
                //set that we need to hide the hud
                SET_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM() 
                
                //If we are not a spectator the clean it all up!
                IF NOT AM_I_A_TRANSITION_SESSION_SPECTATOR()
                AND NOT TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
                AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
				AND NOT TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED()
				AND NOT QM_LAUNCHED_TRANSITION_FROM_PAUSE_MENU()
                    CLEAR_TRANSITION_SESSION_LAUNCHING()
                    CLEAR_TRANSITION_SESSIONS_BITSET()
                    RESET_TRANSITION_SESSION_NON_RESET_VARS()
                ENDIF
                //Clear the script loaded flag
                sTranVars.bRequestScripts = FALSE
            ENDIF
        ELSE
            PRINTLN("[TS]main_persistant - TRANSITION_TO_GAME_JOINED - waiting for NETWORK_IS_GAME_IN_PROGRESS to be false")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED: TRANSITION_TO_GAME_JOINED - waiting for NETWORK_IS_GAME_IN_PROGRESS to be false") #ENDIF
            //Call do trasnrition to game if the game is dead
            IF  g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage = ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH 
                IF GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT() != 0
                    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()) = 0
                        //IF SET_UP_DO_TRANSITION_TO_GAME(g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars)
                            NET_PRINT("[PMC] - SHOULD_DO_TRANSITION_TO_GAME_RUN() = TRUE.")NET_NL()
                            GOTO_POST_MISSION_CLEANUP_STAGE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage, ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD)
                            g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPrepareForFreemodeDeathStage = 0
                            g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.iTranisitionSessionSetUpStage = 0
                            PRINTLN("[TS] SET_UP_DO_TRANSITION_TO_GAME - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPrepareForFreemodeDeathStage = 0")
                            PRINTLN("[TS] SET_UP_DO_TRANSITION_TO_GAME - g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.iTranisitionSessionSetUpStage = 0")
                        //ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ELSE        
        
        
         
//      WARNING_SCREEN_RETURN aWarningScreen = RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY()
//                          
//      IF aWarningScreen = WARNING_SCREEN_RETURN_NONE
//          NET_NL()NET_PRINT("[TS] main_persistant.sc -: Waiting on the player accepting joing failed warning screen: g_JoinResponseEvent.nResponseCode = ")NET_PRINT_INT(ENUM_TO_INT(g_JoinResponseEvent.nResponseCode))NET_PRINT(" (g_Private_BailWarningScreenEventParam = ")NET_PRINT_INT(g_Private_BailWarningScreenEventParam)NET_NL()
//          EXIT
//      ENDIF
        
        //Network game not in progress request script and wait
        PRINTLN("[TS]main_persistant - TRANSITION_TO_GAME_JOINED - NETWORK_IS_GAME_IN_PROGRESS = FALSE")

        
        IF sTranVars.bRequestScripts = FALSE
            IF NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
            AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
            AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
            AND NOT TRANSITION_SESSIONS_WAS_JOINING_WHEN_GOT_START_LAUNCH_EVENT()
            AND NOT TRANSITION_SESSION_JOINING_LAUNCHED_TRANSITION_SESSION()
            AND NOT AM_I_A_TRANSITION_SESSION_SPECTATOR()        
			AND NOT TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED()
                PRINTLN("[TS] main_persistant - TRANSITION_TO_GAME_JOINED - SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT() = FALSE, setting g_TransitionSessionNonResetVars.sPostMissionCleanupData.iForceIntoFreemodeDeadStage = 1.")
                g_TransitionSessionNonResetVars.sPostMissionCleanupData.iForceIntoFreemodeDeadStage = 1
            ENDIF
			IF TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED()
				TOGGLE_RENDERPHASES(FALSE) 
				TOGGLE_RENDERPHASES(FALSE)
				DO_SCREEN_FADE_OUT(SPEC_FADE_LOAD_TIME_SHORT)
			ENDIF
			CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000, TRUE, FALSE, FALSE, FALSE)
            PRINTLN("[TS] main_persistant - TRANSITION_TO_GAME_JOINED - CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000, TRUE, FALSE, FALSE, FALSE)")          
            SET_CURRENT_GAMEMODE(GAMEMODE_FM)
            REQUEST_FM_RESTART_SCRIPTS()
            PRINTLN("[TS] main_persistant - TRANSITION_TO_GAME_JOINED")
            sTranVars.bRequestScripts = TRUE    
            //clear we got the event if we're waiting for it
            IF HAS_TRANSITION_SESSIONS_GOT_EVENT_TRANSITION_TO_GAME_FINISH()
                CLEAR_TRANSITION_SESSIONS_GOT_EVENT_TRANSITION_TO_GAME_FINISH()
            ENDIF   
        //If we are accepting an invite to a transition session and we are in the store:
        ELIF TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE()
            //If we've got the started event
            IF DID_TRANSITION_SESSION_RECIEVED_STARTED()
                //And it's not set up
                IF NOT TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE_SET_UP()
                    //And we didn't join a launched one
                    IF NOT TRANSITION_SESSIONS_END_RESERVING_SLOT_GOT_JOINED_LAUNCHED()
                        PRINTLN("[TS] PROCESS_EVENT_TRANSITION_JOINED_EVENT - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
                        SET_PAUSE_MENU_REQUESTING_A_WARP()
                        SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION()
                        SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
                        IF SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
                        CLEAR_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
                        ENDIF
                        //if we killed freemode we now need to reboot it
                        IF DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
                        CLEAR_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
                        ENDIF

                        g_IsSomethingElseExittingMP = TRUE

                        g_Private_Players_FM_Menu_Choice = FM_MENU_CHOICE_JOIN_NEW_SESSION
                        g_Private_Players_FM_SESSION_Menu_Choice = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_ME_ONLY

                        SET_JOINING_GAMEMODE(GAMEMODE_FM)

                        IF IS_TRANSITION_ACTIVE() = FALSE
                        TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)                
                        ENDIF
                        SET_TRANSITION_SESSIONS_ACCEPTING_INVITE_FROM_SP()

                        g_bMissionCreatorPauseMenuActive = FALSE    // May not be the place to reset this.
                    ENDIF
                    //Set that I need to warp to the start of the mission
                    SET_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START() 
                    SET_TRANSITION_SESSIONS_END_RESERVING_SLOT_ACEPTING_INVITE_SET_UP()
                ENDIF   
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC MAINTAIN_KILL_FRONT_END()
    PRINTLN("[TS]MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE() - MAINTAIN_KILL_FRONT_END")
    //Kill all controls.
    DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
    DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
    PRINTLN("[TS]MAINTAIN_KILL_FRONT_END - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
    IF NOT IS_ONLINE_POLICIES_MENU_ACTIVE()
        DISABLE_ALL_CONTROL_ACTIONS(FRONTEND_CONTROL)
        
        //Kill the front end
        DISABLE_FRONTEND_THIS_FRAME()
        PRINTLN("[TS]MAINTAIN_KILL_FRONT_END - Frontend disabled")
    ELSE
        //If the front end is active, kill it
        IF IS_PAUSE_MENU_ACTIVE()
            SET_FRONTEND_ACTIVE(FALSE)
        ENDIF
        PRINTLN("[TS]MAINTAIN_KILL_FRONT_END - Frontend left on for online policies menu")
    ENDIF
ENDPROC


FUNC BOOL NEED_TO_BAIL_BECAUSE_FREEMODE_DEAD()
	IF g_TransitionData.SwoopStage = SKYSWOOP_NONE
	AND NOT g_sTransitionSessionData.bLaunchingTransitionSession
	AND IS_GAMEPLAY_CAM_RENDERING()
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(FREEMODE_HASH()) = 0
	AND NOT g_b_IsInTransition
	AND g_Private_Gamemode_Current = GAMEMODE_FM
	AND g_b_IsFreemodeRunningActive = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_FREEMODE_RUNNING_CHECK()
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableFMDeadKick")
			EXIT
		ENDIF
	#ENDIF
	
	IF g_B_TurnOnIsFreemodeRunningChecks = TRUE
		IF NEED_TO_BAIL_BECAUSE_FREEMODE_DEAD()
			IF HAS_NET_TIMER_EXPIRED(g_st_IsFreemodeRunningTimer, g_i_IsFreemodeRunningTimeoutTime, TRUE)
				PRINTLN("[TS] * BC: !!!! MAINTAIN_FREEMODE_RUNNING_CHECK = TRUE - bail !!!! ")
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * BC: !!!! MAINTAIN_FREEMODE_RUNNING_CHECK = TRUE - bail !!!! ")#ENDIF
				
				SET_FRONTEND_ACTIVE(FALSE)
				SET_SKYFREEZE_FROZEN()
				SET_HAS_KICKED_FOR_FM_DEAD(TRUE)
				SET_QUIT_CURRENT_GAME(TRUE)
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				SET_SCRIPT_BAILS_PROCESSING(TRUE)
				REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, GET_CURRENT_HUD_STATE())
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
				PLAYSTATS_BACKGROUND_SCRIPT_ACTION("FREEMODE_DEAD_KICK", 1)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(g_st_IsFreemodeRunningTimer)
				RESET_NET_TIMER(g_st_IsFreemodeRunningTimer)
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//Maintain cleaning old transition session and accepting an invite
PROC MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE()
    
    
    //If we are not in the store
    IF NOT IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()
        MAINTAIN_KILL_FRONT_END()
    ENDIF
    
    //Wait for the invite to be ready
    IF CLEAN_UP_TRANSITION_SESSION_AND_WAIT()
        //If we are in a fake MP
        IF IS_FAKE_MULTIPLAYER_MODE_SET() 
            //Set that we are waiting for the clean up
            SET_TRANSITION_SESSIONS_WAIT_FOR_FAKE_MP_CLEANUP()
            //Then clear the fake MP mode
            SET_FAKE_MULTIPLAYER_MODE(FALSE)
            NET_PRINT_TIME() NET_PRINT("[TS]MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE called SET_FAKE_MULTIPLAYER_MODE - FALSE ") NET_NL()   
            PRINTLN("[TS]MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE - EXIT")
            //Get out
            EXIT
        //If we are waiting for the clean up
        ELIF TRANSITION_SESSIONS_WAIT_FOR_FAKE_MP_CLEANUP()
            //And a net session is acticve
            IF NETWORK_IS_SESSION_ACTIVE()
                //Then get out
                PRINTLN("[TS]MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE - NETWORK_IS_SESSION_ACTIVE = TRUE")
                EXIT
            ENDIF
            //Net session is not active, then clean up that we need to wait
            PRINTLN("[TS]MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE - NETWORK_IS_SESSION_ACTIVE = FALSE")
            CLEAR_TRANSITION_SESSIONS_WAIT_FOR_FAKE_MP_CLEANUP()
        ENDIF 
    
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())       
            IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
            AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()))
                PRINTLN("[TS] MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE - in SP and never been in MP. ")
                SET_SKYFREEZE_FROZEN(TRUE)
                CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
            ENDIF
        ENDIF
        
        //Set all the vars needed when accepting an invite
        SET_VARS_WHEN_ACCEPTING_TRANSITION_SESSION_INVITE()
        
        NETWORK_SESSION_JOIN_INVITE()
//      SET_TRANSITION_SESSION_INVITER()
    ENDIF
ENDPROC

PROC DO_DRUNK_CONTROLLER_CHECKS()

#IF IS_DEBUG_BUILD
    IF NOT g_bDebug_KeepDrunkControllerRunning
        IF GET_COMMANDLINE_PARAM_EXISTS("sc_KeepDrunkControllerRunning")
            CPRINTLN(DEBUG_DRUNK, "...main_persistant.sc has activated sc_KeepDrunkControllerRunning = TRUE (command line param)")
            g_bDebug_KeepDrunkControllerRunning = TRUE
        ENDIF
    ENDIF
#ENDIF
    
    IF Should_Drunk_Controller_Be_Running()
    
#IF IS_DEBUG_BUILD
    OR g_bDebug_KeepDrunkControllerRunning
#ENDIF
    
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("drunk_controller")) = 0
            REQUEST_SCRIPT("drunk_controller")
            IF HAS_SCRIPT_LOADED("drunk_controller")
                START_NEW_SCRIPT("drunk_controller", DEFAULT_STACK_SIZE)
                SET_SCRIPT_AS_NO_LONGER_NEEDED("drunk_controller")
            ENDIF
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL SAFE_TO_CLEAN_ARENA_BACKGROUND_IMAGE()

	IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA() 
	AND NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF NOT g_bMissionEnding
	AND GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_RANDOM
	AND GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_RESTART
	AND GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
	AND NOT IS_PLAYER_IN_CORONA()	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_ARENA_BACKGROUND_IMAGE()

	SWITCH GET_ARENA_LOBBY_BACKGROUND_STAGE()
	
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_WAIT
			IF g_bMissionEnding
			AND DOES_CORONA_MISSION_HAVE_LOBBY_VIDEO()
			AND NETWORK_IS_ACTIVITY_SESSION()
				PREPARE_ARENA_LOBBY_BACKGROUND()
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_WAIT_FOR_CELEBRATION)
				g_sArenaBgVars.iAlpha = 0				
				FALLTHRU
			ENDIF			
		BREAK
		
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_WAIT_FOR_CELEBRATION
			IF g_bCelebrationScreenIsActive
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_WAIT_FOR_CELEBRATION_END)
				FALLTHRU
			ENDIF
			IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_DRAW)
				FALLTHRU
			ENDIF
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_WAIT)
			ENDIF
		BREAK
		
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_WAIT_FOR_CELEBRATION_END
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_WAIT)
			ENDIF
			IF NOT g_bCelebrationScreenIsActive
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_DRAW)
				FALLTHRU
			ENDIF
		BREAK
		
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_DRAW
			MAINTAIN_ARENA_LOBBY_BACKGROUND(IS_PLAYER_IN_CORONA(), 255)
			IF SAFE_TO_CLEAN_ARENA_BACKGROUND_IMAGE()
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_FADE_OUT)
				g_sArenaBgVars.iAlpha = 255				
			ENDIF
		BREAK
		
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_FADE_OUT
			MAINTAIN_ARENA_LOBBY_BACKGROUND(FALSE, g_sArenaBgVars.iAlpha)
			g_sArenaBgVars.iAlpha -= 10
			IF g_sArenaBgVars.iAlpha < 0 
				g_sArenaBgVars.iAlpha = 0
			ENDIF
			PRINTLN("[TS][ARENA_BG] ciDRAW_ARENA_BACKGROUND_STAGE_CLEAN_UP, g_sArenaBgVars.iAlpha = ", g_sArenaBgVars.iAlpha)
			IF g_sArenaBgVars.iAlpha = 0
				SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_CLEAN_UP)
			ENDIF
		BREAK
		
		CASE ciDRAW_ARENA_BACKGROUND_STAGE_CLEAN_UP
			SET_ARENA_LOBBY_BACKGROUND_STAGE(ciDRAW_ARENA_BACKGROUND_STAGE_WAIT)
			CLEAN_UP_ARENA_LOBBY_BACKGROUND()
		BREAK
		
	ENDSWITCH
	
ENDPROC

//Maintain bailing out of a transition session
PROC MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL()

//  //If we are on our way into a transition 
    IF (AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
    OR AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
    OR AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP())
    //And it's not a launched transition session
    AND NOT TRANSITION_SESSION_JOINING_LAUNCHED_TRANSITION_SESSION()    
    //And the launch time is greater than 0
    AND GET_TRANSITION_SESSION_LAUNCH_TIME() > 0
    //And the invite time more than 0
    AND GET_TRANSITION_SESSIONS_ACCEPTING_INVITE_POSIX_TIME() > 0
        PRINTLN("[TS]main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL ")      
        //If the current cloud time - the coroana start time is more than one and a half minutes then we're stuck
        CONST_INT ciInviteTimeOut 120
        INT iCloudTime = GET_CLOUD_TIME_AS_INT() 
        //If the timers have expired
        IF iCloudTime - GET_TRANSITION_SESSION_LAUNCH_TIME() > ciInviteTimeOut
        AND iCloudTime - GET_TRANSITION_SESSIONS_ACCEPTING_INVITE_POSIX_TIME() > ciInviteTimeOut
            PRINTLN("[TS]main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - GET_CLOUD_TIME_AS_INT()              = ", GET_CLOUD_TIME_AS_INT())
            PRINTLN("[TS]main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - GET_TRANSITION_SESSION_LAUNCH_TIME() = ", GET_TRANSITION_SESSION_LAUNCH_TIME())
            SCRIPT_ASSERT("[TS]main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - NT/PT for BOBBY WRIGHT")            
            //Get out as we are in a bad shituation!
            PRINTLN("[TS]main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - calling NETWORK_BAIL")            
            RESET_TRANSITION_SESSION_NON_RESET_VARS()
            CLEAR_TRANSITION_SESSIONS_BITSET()
            SET_TRANSITION_SESSIONS_INVITE_TIMED_OUT()
            CLEAR_TRANSITION_SESSIONS_ACCEPTING_INVITE_POSIX_TIME()
            IF NETWORK_CAN_BAIL()
                PRINTLN("[TS] main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - calling NETWORK_BAIL")            
                NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))  
            #IF IS_DEBUG_BUILD
            ELSE
                PRINTLN("[TS] main_persistant - MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL - calling NETWORK_BAIL")            
                #ENDIF 
            ENDIF
            CLEAR_TRANSITION_SESSION_MAINTAIN_SAFETY_BAIL()
        ENDIF
    ENDIF
ENDPROC

BOOL bBailFromMP = FALSE

SCRIPT_TIMER stDirectorModeTimer

PROC MAINTAIN_BAIL_CHECKS()
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND NOT g_bDirectorModeRunning
	AND GET_JOINING_GAMEMODE() != GAMEMODE_FM
	AND NOT IS_TRANSITION_STARTED_IN_MP()
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
			OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				IF NOT bBailFromMP				
					IF NOT HAS_NET_TIMER_STARTED(stDirectorModeTimer)
						PRINTLN("MAINTAIN_BAIL_CHECKS - Starting timer")
						
						START_NET_TIMER(stDirectorModeTimer)
					ELIF HAS_NET_TIMER_EXPIRED(stDirectorModeTimer, 100)
						PRINTLN("MAINTAIN_BAIL_CHECKS - Readying bail")
						
						bBailFromMP = TRUE
						
						PRINTLN("MAINTAIN_BAIL_CHECKS - Reset timer 1")
						
						RESET_NET_TIMER(stDirectorModeTimer)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(stDirectorModeTimer)
					PRINTLN("MAINTAIN_BAIL_CHECKS - Reset timer 2")
					
					bBailFromMP = FALSE
					
					RESET_NET_TIMER(stDirectorModeTimer)
				ENDIF
			ENDIF
		ELIF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			IF bBailFromMP
				PRINTLN("MAINTAIN_BAIL_CHECKS - Bailing")
				
				bBailFromMP = FALSE
				
				NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(stDirectorModeTimer)
				PRINTLN("MAINTAIN_BAIL_CHECKS - Reset timer 3")
				
				RESET_NET_TIMER(stDirectorModeTimer)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(stDirectorModeTimer)
			PRINTLN("MAINTAIN_BAIL_CHECKS - Reset timer 4")
			
			RESET_NET_TIMER(stDirectorModeTimer)
		ENDIF
		
		IF IS_TRANSITION_STARTED_IN_MP()
			PRINTLN("MAINTAIN_BAIL_CHECKS - IS_TRANSITION_STARTED_IN_MP - cancel bail")
			
			bBailFromMP = FALSE
		ENDIF
	ENDIF
ENDPROC

//Maintain the transition sessions in FM/SP
PROC MAINTAIN_TRANSITION_SESSIONS() 

    //Re launch freemode if it's been told to relaunch
    IF HAS_TRANSITION_SESSION_TOLD_FREEMODE_TO_RESTART()    
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - HAS_TRANSITION_SESSION_TOLD_FREEMODE_TO_RESTART = TRUE")
        MAINTAIN_RELAUNCHING_FREEMODE_AFTER_JIP()
    ENDIF
    
    //Need to kill for SCTV
    IF SHOULD_KILL_ALL_MP_SCRIPTS_FOR_SCTV_SWITCH()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_KILL_ALL_MP_SCRIPTS_FOR_SCTV_SWITCH = TRUE")
        MAINTAIN_RELAUNCHING_FREEMODE_AFTER_SCTV_SWITCH()    
    ENDIF
    
    //Hide the mini map when the sky cam is at the top
    MAINTAIN_HIDE_RADAR_WHEN_SKY_CAM_ACTIVE()
    
    //Pull the cam up for a transition session change
    IF SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE = TRUE")
        MAINTAIN_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
    ELIF SHOULD_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
        PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE = TRUE")
		MAINTAIN_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
    //If the spawn location needs cleared up
    ELIF bSpawnLocationNeedsCleared
        PRINTLN("MAINTAIN_TRANSITION_SESSIONS - bSpawnLocationNeedsCleared = TRUE")
        bSpawnLocationNeedsCleared = FALSE
        CLEAR_SPECIFIC_SPAWN_LOCATION()
    ENDIF
    //relaunch Freemode if needed
    IF SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT = TRUE")
        MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED()
    ELSE
		//PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM = FALSE")
        IF SHOULD_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
			//PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM = TRUE")
            MAINTAIN_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
            //Maintain the button if the news feed is active
            MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION()
        ENDIF
        sTranVars.bRequestScripts = FALSE
    ENDIF       
    
    //clean up old transition
    IF IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN = TRUE")
        MAINTAIN_CLEAN_TRANSITION_WHEN_ACCEPTING_INVITE()
    ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE = TRUE")
        IF NOT NETWORK_IS_GAME_IN_PROGRESS()
        AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
        //AND HAS_TRANSITION_SESSIONS_STARTED_SKYCAM_SWOOP()			
			PRINTLN("MAINTAIN_TRANSITION_SESSIONS - calling MAINTAIN_KILL_FRONT_END")
            MAINTAIN_KILL_FRONT_END()
        ENDIF
    ENDIF   
    
	IF HAS_UGC_GLOBAL_BLOCK_LOADED()
		//PRINTLN("MAINTAIN_TRANSITION_SESSIONS - HAS_UGC_GLOBAL_BLOCK_LOADED = TRUE")
		MAINTAIN_ARENA_BACKGROUND_IMAGE()
		#IF FEATURE_COPS_N_CROOKS
		MAINTAIN_CNC_TRANSITION_BACKGROUND()
		#ENDIF
		
	    //Maintain the transition session safty bail
	    IF SHOULD_TRANSITION_SESSION_MAINTAIN_SAFETY_BAIL()
			PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSION_MAINTAIN_SAFETY_BAIL = TRUE")
	        MAINTAIN_TRANSITION_SESSION_SAFETY_BAIL()
	    ENDIF
	ENDIF
	
    
    //hide that bastard radar!
    IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
		PRINTLN("MAINTAIN_TRANSITION_SESSIONS - SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT = TRUE")
        DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()
    ENDIF
    
	MAINTAIN_BAIL_CHECKS()
ENDPROC
// ===========================================================================================================


//BOOL bAccountSetDone = FALSE
//INT iAccountValueLast[3]
CONST_INT CLOSE_TO_MAX_SIGNED_INT 2100000000
/// PURPOSE:
///    To cope with the fact that code doesn't bother to check if incrementing
///    the finance value would overflow to minus on pickups. (Thanks Miguel)
PROC DO_CHECK_FOR_PICKUPS_OVERFLOWING_ACCOUNTS()
    INT ic
    STAT_GET_INT(SP0_TOTAL_CASH, ic)
    IF ic < -CLOSE_TO_MAX_SIGNED_INT
        INT iFix = g_BankAccounts[0].iBalance
        IF iFix < 0
            iFix = 2147483647
        ENDIF
        STAT_SET_INT(SP0_TOTAL_CASH, iFix)
    ENDIF
    STAT_GET_INT(SP1_TOTAL_CASH, ic)
    IF ic < -CLOSE_TO_MAX_SIGNED_INT
        INT iFix = g_BankAccounts[1].iBalance
        IF iFix < 0
            iFix = 2147483647
        ENDIF
        STAT_SET_INT(SP1_TOTAL_CASH, iFix)
    ENDIF
    STAT_GET_INT(SP2_TOTAL_CASH, ic)
    IF ic < -CLOSE_TO_MAX_SIGNED_INT 
        INT iFix = g_BankAccounts[2].iBalance
        IF iFix < 0
            iFix = 2147483647
        ENDIF
        STAT_SET_INT(SP2_TOTAL_CASH, iFix)
    ENDIF
ENDPROC


//Maintain sending data to the cloud!
PROC MAINTAIN_SEND_DATA_JSON_TO_CLOUD()
    //If we should send some data
    IF g_sSendDataJson.bDoSendDataJson
        //Then do so
        IF SEND_DATA_JSON_TO_CLOUD(g_sSendDataJson)
            //Clean up
            PRINTLN("[TS] ciEVENT_UPLOAD_DATA_JSON - g_sSendDataJson.bDoSendDataJson = FALSE")          
            g_sSendDataJson.bDoSendDataJson = FALSE
            g_sSendDataJson.bStatus = FALSE         
            g_sSendDataJson.iDoSendJson = 0
        ENDIF
    ENDIF
ENDPROC
BOOL bResetControl
SCRIPT_TIMER stNoFmTimer
PROC MAINTAIN_FM_NOT_RUNNING_BAIL()

	IF g_sMPTunables.bDisableFmBootCheck
		EXIT
	ENDIF

	#IF FEATURE_FREEMODE_ARCADE
	IF IS_ARCADE_PREVIEW_LOBBY_RUNNING()
		EXIT
	ENDIF
	#ENDIF

	//If we're in a network game, the aky cam is at the ground and Fm is not running
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_SKYSWOOP_AT_GROUND()
	AND GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT() != 0
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()) = 0 

		//Start a timer
		IF !HAS_NET_TIMER_STARTED(stNoFmTimer)
			START_NET_TIMER(stNoFmTimer)
            PRINTLN("[TS] MAINTAIN_FM_NOT_RUNNING_BAIL - START_NET_TIMER(stNoFmTimer)")          
			bResetControl = FALSE

		//If 10 seconds have passed, then turn off control
		ELIF NOT bResetControl
		AND HAS_NET_TIMER_EXPIRED(stNoFmTimer, 10000)
            PRINTLN("[TS] MAINTAIN_FM_NOT_RUNNING_BAIL - NET_SET_PLAYER_CONTROL")          
			bResetControl = TRUE
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION) 

		//If 120 seconds has passed then call network bail, main transtion will pick up on it. 
		ELIF HAS_NET_TIMER_EXPIRED(stNoFmTimer, g_sMPTunables.iFmBootCheckTimeOut)
            PRINTLN("[TS] * MAINTAIN_FM_NOT_RUNNING_BAIL - NETWORK_BAIL")          
			RESET_NET_TIMER(stNoFmTimer)
			bResetControl = FALSE
			NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_FM_NOT_RUNNING))
		ENDIF

	ELSE
		IF HAS_NET_TIMER_STARTED(stNoFmTimer)
            PRINTLN("[TS] MAINTAIN_FM_NOT_RUNNING_BAIL - RESET_NET_TIMER")          
			RESET_NET_TIMER(stNoFmTimer)
		ENDIF
		bResetControl = FALSE
	ENDIF

ENDPROC

PROC MAINTAIN_OUT_OF_JOB_TRANSITIONS_FRONT_END_RADIO()
    
    g_bPlayEndOfJobRadioFrontEnd = FALSE
//  
//  SWITCH g_iPlayEndOfJobRadioFrontEndState
//      
//      CASE 0
//          IF g_bPlayEndOfJobRadioFrontEnd
//              SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
//              SET_MOBILE_PHONE_RADIO_STATE(TRUE)
//              SET_AUDIO_FLAG("AllowRadioDuringSwitch", TRUE)
//              g_iPlayEndOfJobRadioFrontEndState++
//              PRINTLN("[WJK] - MAINTAIN_OUT_OF_JOB_TRANSITIONS_FRONT_END_RADIO - front end radio requested on.")
//          ENDIF
//      BREAK
//      
//      CASE 1
//          IF NOT g_bPlayEndOfJobRadioFrontEnd
//              SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
//              SET_MOBILE_PHONE_RADIO_STATE(FALSE)
//              SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE)
//              g_iPlayEndOfJobRadioFrontEndState = 0
//              PRINTLN("[WJK] - MAINTAIN_OUT_OF_JOB_TRANSITIONS_FRONT_END_RADIO - front end radio requested off.")
//          ENDIF
//      BREAK
//      
//  ENDSWITCH
//  
ENDPROC

// Ensures that the player can't zoom their scope in and out unless they are holding the aim key.
// Resolves conflicts between other button inputs (phone/selector) if a scope view is locked on
// screen without INPUT_AIM being held. -BenR
PROC MAINTAIN_BLOCK_ZOOM_INPUT_FOR_LOCKED_SCOPE_VIEWS()

    IF GET_ALLOW_MOVEMENT_WHILE_ZOOMED()
        PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	    IF NOT IS_PED_INJURED(piPlayerPed)
	    AND NOT IS_CELLPHONE_CAMERA_IN_USE()
		
	        WEAPON_TYPE ePlayerWeapon = WEAPONTYPE_INVALID
	    	GET_CURRENT_PED_WEAPON(piPlayerPed, ePlayerWeapon)
			
			IF ePlayerWeapon = WEAPONTYPE_UNARMED
				EXIT
			ENDIF
				
			//only need to update this on weapon change
			IF ePlayerWeapon != ePlayerCurWeapon
				bPlayerHasScopedWeapon = HAS_PED_WEAPON_GOT_A_SCOPE(piPlayerPed, ePlayerWeapon)
				ePlayerCurWeapon = ePlayerWeapon
			ENDIF
				        
	        //Player has a scoped weapon in their hand.
	        IF bPlayerHasScopedWeapon
			
	            //Player isn't holding aim. If the scope is up then it must
	            //be locked on screen by script.
	            IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	            AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
	                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
	                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY)
	                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY)
	                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_SECONDARY)
	                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_SECONDARY)
	            ENDIF
	        ENDIF
	    ENDIF
    ENDIF
ENDPROC


// Fix for 2413528: Block free cam recording while sniping from helis. The player's model
// is contorted while they shoot like this but is usually hidden behind the camera. We
// don't want the player seeing it in Director Mode. -BenR
PROC MAINTAIN_BLOCK_FREECAM_RECORDING_WHILE_HELI_SNIPING()
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        
            //Player is in a heli.
            IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
                WEAPON_TYPE ePlayerWeapon = WEAPONTYPE_INVALID
                GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), ePlayerWeapon)
                
                //Player has a scoped weapon in their hand.
                IF HAS_PED_WEAPON_GOT_A_SCOPE(PLAYER_PED_ID(), ePlayerWeapon)
                    //Player is aiming.
                    IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
                        REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC MAINTAIN_MP_POST_MISSION_TRANSITION_AUDIO()
    
    SWITCH g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.eState
        
        CASE MP_POST_MISSION_TRANSITION_AUDIO_STATE_OFF
        
            IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene
                START_AUDIO_SCENE("MP_POST_MATCH_TRANSITION_SCENE")
                g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.eState = MP_POST_MISSION_TRANSITION_AUDIO_STATE_ON
                PRINTLN("[PMC] - MAINTAIN_MP_POST_MISSION_TRANSITION_AUDIO - called START_AUDIO_SCENE(MP_POST_MATCH_TRANSITION_SCENE).")
            ENDIF
            
        BREAK
        
        CASE MP_POST_MISSION_TRANSITION_AUDIO_STATE_ON
        
            IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene
                IF IS_AUDIO_SCENE_ACTIVE("MP_POST_MATCH_TRANSITION_SCENE")
                    STOP_AUDIO_SCENE("MP_POST_MATCH_TRANSITION_SCENE")
                    g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.eState = MP_POST_MISSION_TRANSITION_AUDIO_STATE_OFF
                    PRINTLN("[PMC] - MAINTAIN_MP_POST_MISSION_TRANSITION_AUDIO - called STOP_AUDIO_SCENE(MP_POST_MATCH_TRANSITION_SCENE).")
                ENDIF
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
ENDPROC


PROC MAINTAIN_SCRIPT_EVENT_LISTENER()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("error_listener")) <= 0 
	
		#IF IS_DEBUG_BUILD
		IF iErrorListenerShutdownTime = -1
			iErrorListenerShutdownTime = GET_GAME_TIMER()
		ENDIF		
		
		IF NOT bHaveLaunchedErrorListener
		OR GET_GAME_TIMER() - iErrorListenerShutdownTime >= ciERROR_LISTENER_RELAUNCH_DELAY
		#ENDIF //IS_DEBUG_BUILD
		
			REQUEST_SCRIPT("error_listener")
			IF HAS_SCRIPT_LOADED("error_listener")
				START_NEW_SCRIPT( "error_listener", DEFAULT_STACK_SIZE) 
				PRINTLN("[error_listener] Launching error_listener.sc")
				SET_SCRIPT_AS_NO_LONGER_NEEDED("error_listener")
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[error_listener] Launching error_listener.sc")
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				iErrorListenerShutdownTime = -1
				bHaveLaunchedErrorListener = TRUE
				#ENDIF //IS_DEBUG_BUILD
				
			ENDIF
		
		#IF IS_DEBUG_BUILD	
		ENDIF
		#ENDIF //IS_DEBUG_BUILD
		
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
BOOL bSkyCamUp
BOOL bSkyCamDown
BOOL bFadeScreenIn
BOOL bToggleRender
BOOL bPlayerControlOn
BOOL bPostFxStopAll
PROC MAINTAIN_SKY_CAM_WIDGETS()
    IF bSkyCamUp
        bSkyCamUp = FALSE
        SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
    ENDIF
    IF bSkyCamDown
        bSkyCamDown = FALSE
        SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
    ENDIF
    IF bFadeScreenIn
        bFadeScreenIn = FALSE
        IF IS_SCREEN_FADED_OUT()
        OR IS_SCREEN_FADING_OUT()
            DO_SCREEN_FADE_IN(500)
        ENDIF
    ENDIF
    IF bToggleRender
        bToggleRender = FALSE
        TOGGLE_PAUSED_RENDERPHASES(TRUE)
        TOGGLE_PAUSED_RENDERPHASES(TRUE)
        RESET_PAUSED_RENDERPHASES()
    ENDIF
    IF bPlayerControlOn
        bPlayerControlOn = FALSE
        NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
    ENDIF
    IF bPostFxStopAll
        ANIMPOSTFX_STOP_ALL()
        bPostFxStopAll = FALSE
    ENDIF
ENDPROC
 // DEBUG: Set up a 'Force Cleanup' tickbox widget       
 PROC SET_UP_MAIN_Persistent_Widgets()
    IF (g_eCachedLevel != LEVEL_NET_TEST)
    main_pers_widget = main_pers_widget
    STRING stWidgetGroup = "Main Persistent Widgets"
    IF GET_COMMANDLINE_PARAM_EXISTS("sc_skyCamWidgets")
        stWidgetGroup = "     Main Persistent Widgets"
    ENDIF
    main_pers_widget = START_WIDGET_GROUP(stWidgetGroup)            
        IF GET_COMMANDLINE_PARAM_EXISTS("sc_skyCamWidgets")
        START_WIDGET_GROUP("    Sky Cam Widgets")
            ADD_WIDGET_BOOL("Fade In", bFadeScreenIn)
            ADD_WIDGET_BOOL("Sky Cam Down", bSkyCamDown)
            ADD_WIDGET_BOOL("Sky Cam Up", bSkyCamUp)
            ADD_WIDGET_BOOL("Player Control On", bPlayerControlOn)
            ADD_WIDGET_BOOL("Toggle Render Phase", bToggleRender)
            ADD_WIDGET_BOOL("ANIMPOSTFX_STOP_ALL", bPostFxStopAll)
        STOP_WIDGET_GROUP()
        ENDIF
        ADD_WIDGET_BOOL("bDoSendDataJson", g_sSendDataJson.bDoSendDataJson)
        ADD_WIDGET_BOOL("RefreshTuneNow", bRefreshTuneNow)
        ADD_WIDGET_BOOL("RefreshBgNow", bRefreshBgNow)
        ADD_WIDGET_BOOL("Force Cleanup (SP to MP)", m_doForceCleanupSPtoMP)
        ADD_WIDGET_BOOL("Force Relaunch (MP to SP)", m_launchSPMain)
		START_WIDGET_GROUP("MP Content Tickers")
            ADD_WIDGET_BOOL("bForceMarstonTicker", bForceMarstonTicker)
            ADD_WIDGET_BOOL("bForceMPSpecialTicker", bForceMPSpecialTicker)
            ADD_WIDGET_BOOL("bForceMpCollectorsTicker", bForceMpCollectorsTicker)
            ADD_WIDGET_BOOL("bPrintMartsonTickerWidgets", bPrintMartsonTickerWidgets)
        STOP_WIDGET_GROUP()
		
        
        START_WIDGET_GROUP("Script Relaunch")
			TEXT_LABEL_63 tlStack
            START_NEW_WIDGET_COMBO()
				tlStack = "MICRO_STACK_SIZE (" tlStack += MICRO_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "MINI_STACK_SIZE (" tlStack += MINI_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "DEFAULT_STACK_SIZE (" tlStack += DEFAULT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "SPECIAL_ABILITY_STACK_SIZE (" tlStack += SPECIAL_ABILITY_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "FRIEND_STACK_SIZE (" tlStack += FRIEND_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "SHOP_STACK_SIZE (" tlStack += SHOP_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "CELLPHONE_STACK_SIZE (" tlStack += CELLPHONE_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "VEHICLE_SPAWN_STACK_SIZE (" tlStack += VEHICLE_SPAWN_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "PAUSE_MENU_SCRIPT_STACK_SIZE (" tlStack += PAUSE_MENU_SCRIPT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "APP_INTERNET_STACK_SIZE (" tlStack += APP_INTERNET_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "MP_MISSION_STACK_SIZE (" tlStack += MULTIPLAYER_MISSION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "CONTACTS_APP_STACK_SIZE (" tlStack += CONTACTS_APP_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "DEBUG_SCRIPT_STACK_SIZE (" tlStack += DEBUG_SCRIPT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "INTERACTION_MENU_STACK_SIZE (" tlStack += INTERACTION_MENU_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "SCRIPT_XML_STACK_SIZE (" tlStack += SCRIPT_XML_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "INGAMEHUD_STACK_SIZE (" tlStack += INGAMEHUD_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "PROPERTY_INT_STACK_SIZE (" tlStack += PROPERTY_INT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "TRANSITION_STACK_SIZE (" tlStack += TRANSITION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "FMMC_LAUNCHER_STACK_SIZE (" tlStack += FMMC_LAUNCHER_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "MP_FREEMODE_STACK_SIZE (" tlStack += MULTIPLAYER_FREEMODE_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "MISSION_STACK_SIZE (" tlStack += MISSION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "DEBUG_MENU_STACK_SIZE (" tlStack += DEBUG_MENU_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
				tlStack = "SHOP_CONTROLLER_STACK_SIZE (" tlStack += SHOP_CONTROLLER_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
            STOP_WIDGET_COMBO("Stack Size", iStackSizeForScriptLaunch)
            twScriptName = ADD_TEXT_WIDGET("Script name")
            ADD_WIDGET_BOOL("Kill scripts with name", bKillScriptsWithName)
            ADD_WIDGET_BOOL("Launch script with name", bLaunchScriptWithName)
        STOP_WIDGET_GROUP()
		
    STOP_WIDGET_GROUP()             
    ENDIF
	
ENDPROC
#ENDIF


SCRIPT

    CPRINTLN(DEBUG_INIT, "main_persistant.sc started.")

    // Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    #IF IS_DEBUG_BUILD
        if g_bLoadedClifford
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debugCLF")) = 0
                Request_And_Launch_Debug_Script_With_Wait()
            ENDIF
        elif g_bLoadedNorman
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debugNRM")) = 0
                Request_And_Launch_Debug_Script_With_Wait()
            ENDIF   
        else
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debug")) = 0
                Request_And_Launch_Debug_Script_With_Wait()
            ENDIF
        endif
        
    #ENDIF
    
	
//  SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
	
	#IF IS_DEBUG_BUILD
	VALIDATE_ENUM_VALUES()
	#ENDIF
    
    // Load and launch all persistent scripts
    Request_And_Launch_Persistent_Scripts_With_Wait()
    
    // The SP Main script needs launched on startup (and then relaunched on return from MP)
    m_launchSPMain = TRUE
    g_bHaveBeenInMultiplayer = FALSE
    
     #IF IS_DEBUG_BUILD
        // DEBUG: Set up a 'Force Cleanup' tickbox widget       
        IF g_eCachedLevel != LEVEL_NET_TEST
            main_pers_widget = main_pers_widget
            main_pers_widget = START_WIDGET_GROUP("Main Persistent Widgets")            
                
                    ADD_WIDGET_BOOL("bDoSendDataJson", g_sSendDataJson.bDoSendDataJson)
                    ADD_WIDGET_BOOL("RefreshTuneNow", bRefreshTuneNow)
                    ADD_WIDGET_BOOL("RefreshBgNow", bRefreshBgNow)
                    ADD_WIDGET_BOOL("Force Cleanup (SP to MP)", m_doForceCleanupSPtoMP)
                    ADD_WIDGET_INT_SLIDER("PosixTimeMigrateCreationDate", g_iPosixTimeMigrateCreationDate, 0, 1920392540, 1)
                    ADD_WIDGET_BOOL("Force Relaunch (MP to SP)", m_launchSPMain)
					ADD_WIDGET_BOOL("g_b_DoIntroDLCBink", g_b_DoIntroDLCBink)
					ADD_WIDGET_BOOL("g_b_ClearIntroDLCBinkPackedStat", g_b_ClearIntroDLCBinkPackedStat)
					ADD_WIDGET_BOOL("g_b_DontSpawnAtCasinoAfterBink", g_b_DontSpawnAtCasinoAfterBink)
					#IF FEATURE_COPS_N_CROOKS
					ADD_WIDGET_BOOL("g_b_DontSpawnAtPoliceStationAfterBink",g_b_DontSpawnAtIntroDLCLocAfterBink)
					#ENDIF
                    START_WIDGET_GROUP("MP Content Tickers")
                        ADD_WIDGET_BOOL("bForceMarstonTicker", bForceMarstonTicker)
                        ADD_WIDGET_BOOL("bForceMPSpecialTicker", bForceMPSpecialTicker)
                        ADD_WIDGET_BOOL("bForceMpCollectorsTicker", bForceMpCollectorsTicker)
                        ADD_WIDGET_BOOL("bPrintMartsonTickerWidgets", bPrintMartsonTickerWidgets)
                    STOP_WIDGET_GROUP()
                    
                    START_WIDGET_GROUP("Script Relaunch")
                        TEXT_LABEL_63 tlStack
			            START_NEW_WIDGET_COMBO()
							tlStack = "MICRO_STACK_SIZE (" tlStack += MICRO_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "MINI_STACK_SIZE (" tlStack += MINI_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "DEFAULT_STACK_SIZE (" tlStack += DEFAULT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "SPECIAL_ABILITY_STACK_SIZE (" tlStack += SPECIAL_ABILITY_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "FRIEND_STACK_SIZE (" tlStack += FRIEND_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "SHOP_STACK_SIZE (" tlStack += SHOP_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "CELLPHONE_STACK_SIZE (" tlStack += CELLPHONE_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "VEHICLE_SPAWN_STACK_SIZE (" tlStack += VEHICLE_SPAWN_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "PAUSE_MENU_SCRIPT_STACK_SIZE (" tlStack += PAUSE_MENU_SCRIPT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "APP_INTERNET_STACK_SIZE (" tlStack += APP_INTERNET_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "MP_MISSION_STACK_SIZE (" tlStack += MULTIPLAYER_MISSION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "CONTACTS_APP_STACK_SIZE (" tlStack += CONTACTS_APP_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "DEBUG_SCRIPT_STACK_SIZE (" tlStack += DEBUG_SCRIPT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "INTERACTION_MENU_STACK_SIZE (" tlStack += INTERACTION_MENU_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "SCRIPT_XML_STACK_SIZE (" tlStack += SCRIPT_XML_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "INGAMEHUD_STACK_SIZE (" tlStack += INGAMEHUD_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "PROPERTY_INT_STACK_SIZE (" tlStack += PROPERTY_INT_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "TRANSITION_STACK_SIZE (" tlStack += TRANSITION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "FMMC_LAUNCHER_STACK_SIZE (" tlStack += FMMC_LAUNCHER_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "MP_FREEMODE_STACK_SIZE (" tlStack += MULTIPLAYER_FREEMODE_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "MISSION_STACK_SIZE (" tlStack += MISSION_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "DEBUG_MENU_STACK_SIZE (" tlStack += DEBUG_MENU_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
							tlStack = "SHOP_CONTROLLER_STACK_SIZE (" tlStack += SHOP_CONTROLLER_STACK_SIZE tlStack += ")" ADD_TO_WIDGET_COMBO(tlStack)
			            STOP_WIDGET_COMBO("Stack Size", iStackSizeForScriptLaunch)
                        twScriptName = ADD_TEXT_WIDGET("Script name")
                        ADD_WIDGET_BOOL("Kill scripts with name", bKillScriptsWithName)
                        ADD_WIDGET_BOOL("Launch script with name", bLaunchScriptWithName)
                    STOP_WIDGET_GROUP()										
					
                STOP_WIDGET_GROUP()    
				
        ENDIF
    #ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_NL()NET_PRINT("[TS] * BC: NEW GAME STARTING HERE! ")
        NET_NL()NET_PRINT("MAINPERSIST_START: NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE = ")NET_PRINT_BOOL(NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())
        NET_NL()NET_PRINT("MAINPERSIST_START: NETWORK_HAS_CONFIRMED_INVITE = ")NET_PRINT_BOOL(NETWORK_HAS_CONFIRMED_INVITE())
        NET_NL()NET_PRINT("MAINPERSIST_START: NETWORK_IS_SIGNED_ONLINE = ")NET_PRINT_BOOL(NETWORK_IS_SIGNED_ONLINE())
        NET_NL()NET_PRINT("MAINPERSIST_START: NETWORK_IS_SIGNED_IN = ")NET_PRINT_BOOL(NETWORK_IS_SIGNED_IN())
        NET_NL()NET_PRINT("MAINPERSIST_START: LOBBY_AUTO_MULTIPLAYER_FREEMODE = ")NET_PRINT_BOOL(LOBBY_AUTO_MULTIPLAYER_FREEMODE())
    #ENDIF
    
    #IF USE_FINAL_PRINTS
        PRINTLN_FINAL("MAINPERSIST_START: NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE = ", NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())
        PRINTLN_FINAL("MAINPERSIST_START: NETWORK_HAS_CONFIRMED_INVITE = ", NETWORK_HAS_CONFIRMED_INVITE())
        PRINTLN_FINAL("MAINPERSIST_START: NETWORK_IS_SIGNED_ONLINE = ", NETWORK_IS_SIGNED_ONLINE())
        PRINTLN_FINAL("MAINPERSIST_START: NETWORK_IS_SIGNED_IN = ", NETWORK_IS_SIGNED_IN())
        PRINTLN_FINAL("MAINPERSIST_START: LOBBY_AUTO_MULTIPLAYER_FREEMODE = ", LOBBY_AUTO_MULTIPLAYER_FREEMODE())               
    #ENDIF
	
    #IF FEATURE_SP_DLC_BEAST_SECRET
	#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
        Reset_Beast_Finale_Variables()
		
		#IF IS_DEBUG_BUILD
			Create_Beast_Finale_Widgets(main_pers_widget)
		#ENDIF
	#ENDIF
    #ENDIF
    
    SET_PLAYER_HAVE_PRIVILEGES(HAS_ONLINE_PRIVILAGES())
    g_Private_Is_Running_Boot_Invite_Supress = TRUE
    REINIT_NET_TIMER(SignInLoadStatsTimer, TRUE)
    NETWORK_SUPPRESS_INVITE(TRUE)
    
    NET_NL()NET_PRINT("MAINPERSIST_START: DONT_RENDER_IN_GAME_UI(FALSE) ")
    DONT_RENDER_IN_GAME_UI(FALSE)
    
    // Tell code that we're ready
    NETWORK_SET_SCRIPT_READY_FOR_EVENTS(TRUE)

    BOOL bLoadGlobalBlock
    IF (NETWORK_HAS_CONFIRMED_INVITE())
        bLoadGlobalBlock = TRUE
        NET_NL()NET_PRINT(" NETWORK_REQUEST_INVITE_CONFIRMED_EVENT - called from main_persistent.sc ")
        NETWORK_REQUEST_INVITE_CONFIRMED_EVENT()
        SET_INVITED_PROCESSING_AFTER_REPLAY(TRUE)
        CLEAR_SUPPRESS_INVITE_AT_BOOT()
        //IF joinging a transition invite
        SET_TRANSITION_SESSIONS_ACCEPTING_INVITE()
        //Set taht it's from boots
        SET_TRANSITION_SESSIONS_ACCEPTING_INVITE_FROM_BOOT()
    ENDIF
    
    
    
    INT iCouponStep = 60000
    INT iCouponTick = GET_GAME_TIMER() + iCouponStep
    
    
    REMOVE_RELATIONSHIP_GROUP(RELGROUPHASH_FAMILY_M)
    ADD_RELATIONSHIP_GROUP("RELGROUPHASH_FAMILY_M", RELGROUPHASH_FAMILY_M)
    
    REMOVE_RELATIONSHIP_GROUP(RELGROUPHASH_FAMILY_F)
    ADD_RELATIONSHIP_GROUP("RELGROUPHASH_FAMILY_F", RELGROUPHASH_FAMILY_F)
    
    REMOVE_RELATIONSHIP_GROUP(RELGROUPHASH_FAMILY_T)
    ADD_RELATIONSHIP_GROUP("RELGROUPHASH_FAMILY_T", RELGROUPHASH_FAMILY_T)
    
    if g_bLoadedClifford
        UPDATE_PLAYER_CHARACTER_RELATIONSHIPS(GET_CURRENT_PLAYER_PED_ENUMCLF())
    elif g_bLoadedNorman
        UPDATE_PLAYER_CHARACTER_RELATIONSHIPS(GET_CURRENT_PLAYER_PED_ENUMNRM())
    else
        UPDATE_PLAYER_CHARACTER_RELATIONSHIPS(GET_CURRENT_PLAYER_PED_ENUM())
    endif
    
    
    IF NETWORK_IS_OFFLINE_INVITE_PENDING()
        CPRINTLN(DEBUG_INIT, "NETWORK_IS_OFFLINE_INVITE_PENDING = TRUE")

        SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN, TRUE, -2)
        NETWORK_CLEAR_OFFLINE_INVITE_PENDING()
    ENDIF
    
    
    
    // Main loop
    WHILE (TRUE)
        MP_LOOP_WAIT_ZERO()
        
		#IF FEATURE_GEN9_STANDALONE
		// gen9 introduces the conecept of script router links, process any pending links now
   		PROCESS_PENDING_SCRIPT_ROUTER_LINK()    
        #ENDIF
		
		IF bLoadGlobalBlock
            IF HAS_UGC_GLOBAL_BLOCK_LOADED()
            OR LAUNCH_REGISTER_UGC_GLOBALS_SCRIPT()
                SET_UGC_GLOBAL_BLOCK_LOADED()
                bLoadGlobalBlock = FALSE
            ENDIF
        ENDIF
        
        #IF IS_DEBUG_BUILD
            IF g_bCleanupInGameHud = TRUE 
                SET_SCRIPT_AS_NO_LONGER_NEEDED("InGameHud")
                g_bCleanupInGameHud = FALSE 
            ENDIF
        #ENDIF
        
        #IF IS_DEBUG_BUILD
        IF g_eCachedLevel != LEVEL_NET_TEST
        #ENDIF

            IF SHOULD_END_SINGLEPLAYER()
                IF END_SINGLEPLAYER_READY_TO_START_MP()
                    RESET_SINGLEPLAYER_END_NOW()
                    
                    PRINTLN("SHOULD_END_SINGLEPLAYER() - set to TRUE in main_persistent.")
                    #IF USE_FINAL_PRINTS 
                        PRINTLN_FINAL("<2140379> SHOULD_END_SINGLEPLAYER() set to TRUE in main_persistent.")
                    #ENDIF

                ENDIF
            ELIF START_UP_SINGLEPLAYER()
                m_launchSPMain = TRUE
                g_bInMultiplayer = FALSE
            
                PRINTLN("START_UP_SINGLEPLAYER() - set to TRUE in main_persistent.")
                #IF USE_FINAL_PRINTS 
                    PRINTLN_FINAL("<2140379> START_UP_SINGLEPLAYER() set to TRUE in main_persistent.")
                #ENDIF

            ENDIF

        // Skip launching the singleplayer main thread if
        // running the MP test bed.
        #IF IS_DEBUG_BUILD
        ELIF m_launchSPMain
            RESET_SINGLEPLAYER_END_NOW()
            m_launchSPMain = FALSE
            g_bInMultiplayer = FALSE
        ENDIF
        #ENDIF
        
        // Process any script events
		Process_cached_invite()
        Clear_Event_Queue()
        DO_DRUNK_CONTROLLER_CHECKS()    //moved from DO_SCRIPT_LAUNCH_CHECKS() so it runs in MP
        
        //Purposefully "hidden" in amongst this script. We want it to be hard
        //for hackers to spot these scripts in decompiled output. -BenR
        #IF FEATURE_SP_DLC_BEAST_SECRET
		#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
            Maintain_Beast_Finale()

			#IF IS_DEBUG_BUILD
				Update_Beast_Finale_Widgets(FALSE)
			#ENDIF
		#ENDIF
        #ENDIF
        
        // Maintain the transition sessions in FM/SP
        MAINTAIN_TRANSITION_SESSIONS()
        
        //Maintain sending data to the cloud
        MAINTAIN_SEND_DATA_JSON_TO_CLOUD()
        
        // For mp. will put this into a stand alone script.
        PROCESS_LEADERBOARD_CAM()
        
        // Early load scene for MP session transitions when exiting jobs. to help speed up transitions by starting the load scene as soon as possible.
        PROCESS_TRANSITION_LOAD_SCENE()
        
        // Fake screen fade that we can make white. Used for white fades to cover transitions in special case MP situations. 
        MAINTAIN_RECTANGLE()
        
        // 2193949 clouds alpha 
        MAINTAIN_CLOUD_ALPHA()
		
		//Deal with FM not running
        MAINTAIN_FM_NOT_RUNNING_BAIL()
		
        //For both SP and mp check for new coupons that need to be updated
        IF iCouponTick < GET_GAME_TIMER()
            UPDATE_COUPONS()
            iCouponTick = GET_GAME_TIMER() + iCouponStep
        ENDIF
        
        // Shows Marston unlock ticker as soon as player is in the game and connected to the social club.
        MAINTAIN_CONTENT_UNLOCK_MP_TICKERS()
        
        MAINTAIN_TRAIN_LINE_ACTIVATION()

        DO_CHECK_FOR_PICKUPS_OVERFLOWING_ACCOUNTS()
        
        MAINTAIN_OUT_OF_JOB_TRANSITIONS_FRONT_END_RADIO()
        
        MAINTAIN_BLOCK_ZOOM_INPUT_FOR_LOCKED_SCOPE_VIEWS()
        
        MAINTAIN_MP_POST_MISSION_TRANSITION_AUDIO()
        
        MAINTAIN_CELEBRATION_AUDIO()
		
		MAINTAIN_FREEMODE_RUNNING_CHECK()
        
		MAINTAIN_SCRIPT_EVENT_LISTENER()
		
		iClockHours = GET_CLOCK_HOURS()
		g_TransitionSessionNonResetVars.sMagnateGangBossData.iClockHours = iClockHours
		
        // Replay Camera Movement Blocking
        IF IS_REPLAY_RECORDING()
            //(Strip Club)
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripclub")) > 0
            OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("stripclub_mp")) > 0
                IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)    //Fix for bugs 2209274, 2198600, 2195133, 2194867
                OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
                    IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<115.93143, -1290.48279, 27.94359>>) < (70*70)
                        REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2209617
                    ENDIF
                ENDIF
            ENDIF
            
            //(Ghost)
            IF iClockHours = 23
                IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<3122.3154, 5549.0068, 188.4373>>) < (150*150)
                    REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2238683
                ENDIF
            ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_GUNRUN_ENTITY_SPAWN_COORDS(GRV_CRASH_SITE_2, GunrunLocation_CrashSite2_Zancudo, 0)) < (500*500)
                REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2238683
            ENDIF
        ENDIF
        
        // Check if the SP Main script needs to be launched
        IF (m_launchSPMain)
            WHILE NOT Setup_Main_Script_And_Reset_Settings()
       			MP_LOOP_WAIT_ZERO()
		        // Process any script events
				Process_cached_invite()
		        Clear_Event_Queue()        	
			ENDWHILE
		ENDIF
        
        // KGM
        // NOTE: Not doing anything with this variable yet: g_bMPTutorialIsActive
        
        // Check for Forcing a Cleanup as though moving from SP to MP.
        #IF IS_DEBUG_BUILD
		
			IF (g_b_ClearIntroDLCBinkPackedStat)
				#IF FEATURE_COPS_N_CROOKS
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_SHOWN_CNC_INTRO_BINK, FALSE)
				PRINTLN("[DLC_INTRO_BINK] clearing PACKED_MP_STAT_HAS_SHOWN_CNC_INTRO_BINK ")	
				#ENDIF
				g_b_ClearIntroDLCBinkPackedStat = FALSE
			ENDIF
		
            IF (m_doForceCleanupSPtoMP)
    
                INT iNoIntialThreads = 0
                iNoIntialThreads = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initial"))
                WHILE iNoIntialThreads > 0
                    CPRINTLN(DEBUG_INIT_SP, "number of \"initial\" threads is ", iNoIntialThreads, ".")
                    WAIT(0)
                ENDWHILE
                iNoIntialThreads = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initialCLF"))
                WHILE iNoIntialThreads > 0
                    CPRINTLN(DEBUG_INIT_SP, "number of \"initialCLF\" threads is ", iNoIntialThreads, ".")
                    WAIT(0)
                ENDWHILE
                iNoIntialThreads = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initialNRM"))
                WHILE iNoIntialThreads > 0
                    CPRINTLN(DEBUG_INIT_SP, "number of \"initialNRM\" threads is ", iNoIntialThreads, ".")
                    WAIT(0)
                ENDWHILE
                
                FORCE_CLEANUP(FORCE_CLEANUP_FLAG_SP_TO_MP)
                m_doForceCleanupSPtoMP = FALSE
            ENDIF

            //Allow re-launching of the debug thread if it somehow dies.
            INT debugScriptNameHash
            IF g_bLoadedClifford
                debugScriptNameHash = HASH("debugCLF")
            ELIF g_bLoadedNorman
                debugScriptNameHash = HASH("debugNRM")
            ELSE
                debugScriptNameHash = HASH("debug")
            ENDIF
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(debugScriptNameHash) = 0
                IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F1,KEYBOARD_MODIFIER_SHIFT,"Start debug")
                    Request_And_Launch_Debug_Script_With_Wait()
                ENDIF
            ENDIF
            
            IF bKillScriptsWithName
                bKillScriptsWithName = FALSE
                IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))
                AND DOES_SCRIPT_EXIST(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))
                    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))) > 0
                        TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))
                    ENDIF
                ELSE
                    SCRIPT_ASSERT("Unable to launch script - invalid script name")
                ENDIF
            ENDIF
            IF bLaunchScriptWithName
                bLaunchScriptWithName = FALSE
                IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))
                AND DOES_SCRIPT_EXIST(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName))
                    IF GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(GET_STACK_SIZE_FROM_INT_WIDGET()) > 0
                        REQUEST_SCRIPT_WITH_NAME_HASH(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName)))
                        IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName)))
                            START_NEW_SCRIPT_WITH_NAME_HASH(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName)), GET_STACK_SIZE_FROM_INT_WIDGET())
                            SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twScriptName)))
                        ELSE
                            bLaunchScriptWithName = TRUE
                        ENDIF
                    ELSE
                        SCRIPT_ASSERT("Unable to launch script - stack size not available")
                    ENDIF
                ELSE
                    SCRIPT_ASSERT("Unable to launch script - invalid script name")
                ENDIF
            ENDIF
            IF bRefreshBgNow
//              IF NATIVE_TO_INT(PLAYER_ID()) != -1
//                  GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iBgScriptTime = 0
//              ENDIF
                PRINTLN("[TS] PROCESS_EVENT_NETWORK_PRESENCE_TRIGGER - NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS()")
                //For 1881326
                NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS()
                //g_sBackGroundStruct.bKillActive = TRUE
                bRefreshBgNow = FALSE
            ENDIF
            IF bRefreshTuneNow
                PRINTLN("[TS] bRefreshTuneNow - NETWORK_REQUEST_CLOUD_TUNABLES ()")
                NETWORK_REQUEST_CLOUD_TUNABLES()
                bRefreshTuneNow = FALSE
            ENDIF
			
			IF g_dRestartDebugSc
				IF Request_And_Launch_Debug_Script_Async()
					g_dRestartDebugSc = FALSE
				ENDIF
			ENDIF
			
        #ENDIF

            
        
    
    ENDWHILE
    
ENDSCRIPT

