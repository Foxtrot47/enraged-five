USING "global_block_defines.sch"

USING "globals.sch"
USING "title_update_globals.sch"

GLOBALS GLOBALS_BLOCK_SP_DLC TRUE

USING "globals_sp_dlc.sch"

// KEITH 25/3/14: Re-instated this saved block to fix bug 1795872 - however it now only contains one variable.
//					See x:\gta5\script\dev_network\singleplayer\include\globals\dlc\globals_sp_dlc.sch at
//					the bottom for a more detailed explanation. This saved block got released to the public with
//					Business1 but then removed for CTF_Creator, meaning that saved games contained the globals
//					but now there was no block getting registered to deal with them, so the variables ended up
//					hanging around in the same memory as used by the network heap meaning the network heap
//					bust its boundaries.


CONST_INT REGISTER_SP_DLC_SAVE		1

CONST_INT __ASSERT_IF_SAVEGAME_SIZES_DIFFER 1

#IF REGISTER_SP_DLC_SAVE

#IF IS_DEBUG_BUILD
INT RunningTotalOfStructures = 0

PROC VerifySizeOfSavedStructure(INT SizeOfMostRecentStructure, STRING NameOfMostRecentStructure, BOOL bSP = TRUE)
    CPRINTLN(DEBUG_INIT, "SIZE_OF(", NameOfMostRecentStructure, ") = ", SizeOfMostRecentStructure)
	
    RunningTotalOfStructures += SizeOfMostRecentStructure
	
    INT SizeOfRegisteredVariables = GET_SIZE_OF_SAVE_DATA(bSP)
    
    IF RunningTotalOfStructures <> SizeOfRegisteredVariables
        CPRINTLN(DEBUG_INIT, "Size of registered save variables differs from actual size of structure")
        CPRINTLN(DEBUG_INIT, "Size of all structures so far = ", RunningTotalOfStructures)
        CPRINTLN(DEBUG_INIT, "SizeOfRegisteredVariables = ", SizeOfRegisteredVariables)
        CPRINTLN(DEBUG_INIT, "Most Recent Structure = ", NameOfMostRecentStructure)
#IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
        SCRIPT_ASSERT("Size of save struct differs from size of registered save variables - check TTY")
#ENDIF
    ENDIF
ENDPROC
#ENDIF  //  IS_DEBUG_BUILD
#ENDIF	//	REGISTER_SP_DLC_SAVE 


SCRIPT

#IF REGISTER_SP_DLC_SAVE

	#IF IS_DEBUG_BUILD
	RunningTotalOfStructures = GET_SIZE_OF_SAVE_DATA(TRUE)
	TEXT_LABEL_63 textLabel63
	#ENDIF

	START_SAVE_DATA(g_fakeSpDLCSave, SIZE_OF(g_fakeSpDLCSave), TRUE)
	
		REGISTER_INT_TO_SAVE(g_fakeSpDLCSave.aFakeSavedInt, "aFakeSavedInt")
		
		#IF IS_DEBUG_BUILD
			textLabel63 = "g_fakeSpDLCSave"
			VerifySizeOfSavedStructure(SIZE_OF(g_fakeSpDLCSave), textLabel63, TRUE)
		#ENDIF
		
	STOP_SAVE_DATA()

	CPRINTLN(DEBUG_INIT, "sp_dlc_registration.sc - about to set SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED")
	SET_BIT(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)

	INT SizeOfDLCStruct = SIZE_OF(g_fakeSpDLCSave)
	INT SizeOfDLCRegisteredVariables = GET_SIZE_OF_SAVE_DATA(TRUE)
	CPRINTLN(DEBUG_INIT, "SizeOfDLCStruct = ", SizeOfDLCStruct)
	CPRINTLN(DEBUG_INIT, "SizeOfDLCRegisteredVariables = ", SizeOfDLCRegisteredVariables)

	#IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
	    IF SizeOfDLCStruct <> SizeOfDLCRegisteredVariables
	        SCRIPT_ASSERT("Size of DLC save struct differs from size of registered DLC save variables - check TTY")
	    ENDIF
	#ENDIF

	CPRINTLN(DEBUG_INIT, "Saved DLC globals registered and restored.")
	
#ENDIF	//	REGISTER_SP_DLC_SAVE 

ENDSCRIPT
