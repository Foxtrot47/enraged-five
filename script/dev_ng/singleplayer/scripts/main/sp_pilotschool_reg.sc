USING "global_block_defines.sch"

USING "globals.sch"
using "pilotschool_globals_reg.sch"
USING "title_update_globals.sch"

GLOBALS GLOBALS_BLOCK_SP_PILOT_SCHOOL_DLC TRUE

// MP_Globals.sch has started off empty, all it's contents have been moved to MP_Globals_Temp.sch
// MP_Globals.sch is now registered in it's own block. MP_Globals_TEMP.sch is still registered in the standard block.
// As much of MP_Globals_TEMP.sch will be pulled across into MP_Globals.sch as possible so that as many MP Globals
//			as possible will end up registered in their own block.
USING "globals_sp_pilotschool.sch"

CONST_INT REGISTER_SP_PILOT_SAVE		1

CONST_INT __ASSERT_IF_SAVEGAME_SIZES_DIFFER 1

#IF REGISTER_SP_PILOT_SAVE

#IF IS_DEBUG_BUILD
INT RunningTotalOfStructures = 0

PROC VerifySizeOfSavedStructure(INT SizeOfMostRecentStructure, STRING NameOfMostRecentStructure, BOOL bSP = TRUE)
    CPRINTLN(DEBUG_INIT, "SIZE_OF(", NameOfMostRecentStructure, ") = ", SizeOfMostRecentStructure)
	
    RunningTotalOfStructures += SizeOfMostRecentStructure
	
    INT SizeOfRegisteredVariables = GET_SIZE_OF_SAVE_DATA(bSP)
    
    IF RunningTotalOfStructures <> SizeOfRegisteredVariables
        CPRINTLN(DEBUG_INIT, "Size of registered save variables differs from actual size of structure")
        CPRINTLN(DEBUG_INIT, "Size of all structures so far = ", RunningTotalOfStructures)
        CPRINTLN(DEBUG_INIT, "SizeOfRegisteredVariables = ", SizeOfRegisteredVariables)
        CPRINTLN(DEBUG_INIT, "Most Recent Structure = ", NameOfMostRecentStructure)
#IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
        SCRIPT_ASSERT("Size of save struct differs from size of registered save variables - check TTY")
#ENDIF
    ENDIF
ENDPROC
#ENDIF  //  IS_DEBUG_BUILD
#ENDIF	//	REGISTER_SP_DLC_SAVE 

SCRIPT


#IF REGISTER_SP_PILOT_SAVE

	#IF IS_DEBUG_BUILD
	RunningTotalOfStructures = GET_SIZE_OF_SAVE_DATA(TRUE)	
	#ENDIF

	START_SAVE_DATA(g_savedGlobalsPilotSchool, SIZE_OF(g_savedGlobalsPilotSchool), TRUE)
		Register_DLCPilotSchool_Saved_Globals()		
	STOP_SAVE_DATA()

	CPRINTLN(DEBUG_INIT, "sp_pilotschool_reg.sc - about to set SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED")
	SET_BIT(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
	
	CPRINTLN(DEBUG_INIT, "Saved Pilot globals registered and restored.")
	
#ENDIF	

ENDSCRIPT

