USING "globals.sch"
USING "global_block_defines.sch"
 
GLOBALS GLOBALS_BLOCK_MP_SPEC_PROP TRUE

USING "mp_globals_block_mp_prop_special.sch"
USING "net_realty_details.sch"

USING "net_private_yacht.sch"

SCRIPT
	//do nothing
	PRINTLN("GLOBALS_BLOCK_MP_SPEC_PROP: Triggered")
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	INT i
	INIT_PRIVATE_YACHT_DATA()
	REPEAT NUMBER_OF_PRIVATE_YACHTS  i
		GET_MP_YACHT_DETAILS_FULL(mpYachts[i],i)
		PRINTLN("MP_PROP_GLOBAL_SPEC_BLOCK: filled in details for MP Yacht #",i)
	ENDREPEAT
ENDSCRIPT
//EOF
