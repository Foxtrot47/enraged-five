USING "global_block_defines.sch"

// Allow the globals to be created
//GLOBALS TRUE //GLOBALS_BLOCK_STANDARD TRUE


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
//USING "Flow_Mission_Data_GTA5.sch"
USING "Flow_Heist_Data_GTA5.sch"
USING "script_player.sch"
USING "streamed_scripts.sch"
USING "respawn_location_data.sch"
USING "stripclub_public.sch"
USING "properties_public.sch"
USING "trains_control_public.sch"
USING "charsheet_public.sch"
USING "CompletionPercentage_public.sch"
USING "mission_flow_initialiser.sch"
USING "decorator_initialiser.sch"
USING "friends_public.sch"
USING "flow_reset_GAME.sch"
USING "blip_ambient.sch"

// All the registration headers required to specify which global variables need to be saved
USING "flow_globals_reg.sch"
USING "heist_globals_reg.sch"
USING "stripclub_globals_reg.sch"
USING "pilotschool_globals_reg.sch"
USING "shop_globals_reg.sch"
USING "ambient_globals_reg.sch"
USING "player_ped_globals_reg.sch"
USING "charsheet_globals_reg.sch"
USING "respawn_location_globals_reg.sch"
USING "family_globals_reg.sch"
USING "player_scene_globals_reg.sch"
USING "friends_globals_reg.sch"
USING "comms_control_globals_reg.sch"
USING "code_control_globals_reg.sch"
USING "building_globals_reg.sch"
USING "vehicle_gen_globals_reg.sch"
USING "flow_help_globals_reg.sch"
USING "CompletionPercentage_globals_reg.sch"
USING "Cellphone_Settings_globals_reg.sch"
USING "social_globals_reg.sch"
USING "finance_globals_reg.sch"
USING "random_char_globals_reg.sch"
USING "towing_globals_reg.sch"
USING "assassin_globals_reg.sch"
USING "basejump_globals_reg.sch"
USING "trafficking_globals_reg.sch"
USING "taxi_globals_reg.sch"
USING "offroad_globals_reg.sch"
USING "golf_globals_reg.sch"
USING "range_globals_reg.sch"
USING "tennis_globals_reg.sch"
USING "sptt_globals_reg.sch"
USING "Triathlon_globals_reg.sch"
USING "shoprobberies_globals_reg.sch"
USING "shrink_globals_reg.sch"
USING "country_race_globals_reg.sch"
//USING "freemode_stunt_jumps.sch"
USING "specialPed_globals_reg.sch"
USING "email_globals_reg.sch"
USING "darts_globals_reg.sch"
USING "sea_race_globals_reg.sch"
USING "street_race_globals_reg.sch"
USING "rampage_globals_reg.sch"
USING "debug_channels_core.sch"
USING "feed_globals_reg.sch"
USING "snapshots_globals_reg.sch"
USING "BailBond_globals_reg.sch"
USING "properties_globals_reg.sch"
USING "mp_saved_vehicles_globals_reg.sch"
//USING "mp_saved_buddies_globals_reg.sch"
USING "mp_saved_carapp_globals_reg.sch"
USING "mp_saved_general_globals_reg.sch"
USING "mp_saved_big_ass_vehicles_globals_reg.sch"
USING "mp_saved_property_globals_reg.sch"
USING "mp_saved_bounty_reg.sch"
USING "MP_Globals_Block_MP_SG.sch"
USING "random_event_globals_reg.sch"
USING "ScriptSaves_Procs.sch"
USING "flyUnderBridges.sch"
USING "savegame_patching.sch"
USING "screens_header.sch"
USING "video_editor.sch"
USING "PreloadStartupScripts.sch"

#IF IS_DEBUG_BUILD
    USING "cellphone_public.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   startup.sc
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Contains all code that is immediately required when the game
//                              starts (ie: registering object scripts; restart points;
//                              creating player; etc).
//
//                          Sets up the Globals.
//                          Sets up the Persistent Globals.
//
//                          This script will load the main_persistent script then terminate.
//
//      GTA 5 SAVED VARS :  After the first WAIT(0), all the global variables that form part
//                              of the save game need to be registered. This not only names
//                              all the variables, but also restores the variables from a 
//                              previous save.
//                          Each system will have it's own global save variables. Each system
//                              will need an 'initialised' flag global save variable. Each
//                              time the game runs, startup.sc will set the initialised flag
//                              to FALSE. It will then reload the most recent save. If that
//                              system already contains initialised saved data then that
//                              initialised flag will have been saved as TRUE. When the system
//                              is launched it should initialise the global save variables only
//                              if the initialised variable is FALSE - if TRUE it should ignore
//                              the variables because they will have been reloaded with the
//                              desired values from the previous session.
//                          Each system should create a _reg.sch version of their global header
//                              file with functions that specify the variables to be registered.
//                              This file should be stored in shared/globals/registration/.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	eLevelIndex eLocalCachedLevel
#ENDIF

//FLOAT fInitialPlayerHeading = 135.0000

GAME_TYPE eGameType

PROC Set_Network_Privilege_Bools()	
    g_bHaveFull_NetworkPrivileges   = TRUE
    g_bHaveFriend_NetworkPrivileges = TRUE
    CPRINTLN(DEBUG_INIT, "Set_Network_Privilege_Bools - Setting g_bHaveFull_NetworkPrivileges to [", g_bHaveFull_NetworkPrivileges, "] and setting g_bHaveFriend_NetworkPrivileges to [", g_bHaveFriend_NetworkPrivileges, "]")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Requests a script, waits for it to stream in, then launches it
PROC Request_And_Launch_Script_With_Wait(INT iScriptHash, INT iStackSize)
    REQUEST_SCRIPT_WITH_NAME_HASH(iScriptHash)
    WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(iScriptHash)
        WAIT(0)
        REQUEST_SCRIPT_WITH_NAME_HASH(iScriptHash)
    ENDWHILE
    START_NEW_SCRIPT_WITH_NAME_HASH(iScriptHash, iStackSize)
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(GLOBAL_REGISTRATION_SCRIPT eRegScript)
	SWITCH eRegScript
		CASE MP_REGISTRATION						RETURN "MP_REGISTRATION"
		CASE MP_FM_REGISTRATION				        RETURN "MP_FM_REGISTRATION"
		CASE TUNABLES_REGISTRATION			        RETURN "TUNABLES_REGISTRATION"
		CASE SC_LB_GLOBAL_BLOCK				        RETURN "SC_LB_GLOBAL_BLOCK"
		CASE MP_SAVE_GAME_GLOBAL_BLOCK		        RETURN "MP_SAVE_GAME_GLOBAL_BLOCK"
		CASE MP_PROP_GLOBAL_BLOCK			        RETURN "MP_PROP_GLOBAL_BLOCK"
		CASE MP_PROP_SPECIAL_GLOBAL_BLOCK	        RETURN "MP_PROP_SPECIAL_GLOBAL_BLOCK"
		CASE TITLE_UPDATE_REGISTRATION		        RETURN "TITLE_UPDATE_REGISTRATION"
		CASE TITLE_UPDATE_REGISTRATION_2		    RETURN "TITLE_UPDATE_REGISTRATION_2"
		CASE GLOBALS_FMMC_STRUCT_REGISTRATION       RETURN "GLOBALS_FMMC_STRUCT_REGISTRATION"
		CASE GLOBALS_FMMCSTRUCT2_REGISTRATION       RETURN "GLOBALS_FMMCSTRUCT2_REGISTRATION"
		CASE SP_DLC_REGISTRATION				    RETURN "SP_DLC_REGISTRATION"
		CASE SP_PILOTSCHOOL_REG				        RETURN "SP_PILOTSCHOOL_REG"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

FUNC BOOL HAS_REGISTRATION_REQUIREMENT_BEEN_MET(GLOBAL_REGISTRATION_SCRIPT eRegScript, INT iLaunchedBitSet)
	
	SWITCH eRegScript
		CASE SP_DLC_REGISTRATION
			IF NOT IS_BIT_SET(iLaunchedBitSet, ENUM_TO_INT(TITLE_UPDATE_REGISTRATION_2))
				CPRINTLN(DEBUG_INIT, "REGISTER_ALL_SCRIPT_GLOBALS - HAS_REGISTRATION_REQUIREMENT_BEEN_MET = FALSE: ", GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(eRegScript), ".")
				RETURN FALSE
			ENDIF
		BREAK
		CASE SP_PILOTSCHOOL_REG
			IF NOT IS_BIT_SET(iLaunchedBitSet, ENUM_TO_INT(SP_DLC_REGISTRATION))
				CPRINTLN(DEBUG_INIT, "REGISTER_ALL_SCRIPT_GLOBALS - HAS_REGISTRATION_REQUIREMENT_BEEN_MET = FALSE: ", GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(eRegScript), ".")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_A_REGISTRATION_REQUIREMENT(GLOBAL_REGISTRATION_SCRIPT eRegScript)
	
	SWITCH eRegScript
		CASE TITLE_UPDATE_REGISTRATION_2
		CASE SP_DLC_REGISTRATION
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC REGISTER_ALL_SCRIPT_GLOBALS()

	GLOBAL_REGISTRATION_SCRIPT eRegScript
	BOOL bFinished
	INT iLaunchedBitSet
	
	WHILE NOT bFinished
		bFinished = TRUE
		REPEAT GLOBAL_REGISTRATION_SCRIPT_MAX eRegScript
			IF NOT IS_BIT_SET(iLaunchedBitSet, ENUM_TO_INT(eRegScript))
				INT iScriptHash = GET_GLOBAL_REGISTRATION_SCRIPT_HASH(eRegScript)
				REQUEST_SCRIPT_WITH_NAME_HASH(iScriptHash)
			    IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(iScriptHash)
				AND HAS_REGISTRATION_REQUIREMENT_BEEN_MET(eRegScript, iLaunchedBitSet)
					CPRINTLN(DEBUG_INIT, "REGISTER_ALL_SCRIPT_GLOBALS - About to request and launch: ", GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(eRegScript), ".")
			  		START_NEW_SCRIPT_WITH_NAME_HASH(iScriptHash, DEFAULT_STACK_SIZE)
					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(iScriptHash)
					SET_BIT(iLaunchedBitSet, ENUM_TO_INT(eRegScript))
					//Bail if we have a requirement so we launch the other script the next frame. 
					IF IS_A_REGISTRATION_REQUIREMENT(eRegScript)
						CPRINTLN(DEBUG_INIT, "REGISTER_ALL_SCRIPT_GLOBALS - IS_A_REGISTRATION_REQUIREMENT: ", GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(eRegScript), ".")
						bFinished = FALSE
						BREAKLOOP
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INIT, "REGISTER_ALL_SCRIPT_GLOBALS - Waiting on: ", GET_GLOBAL_REGISTRATION_SCRIPT_DEBUG_PRINT(eRegScript), ".")
					bFinished = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		WAIT(0)
	ENDWHILE	
	
ENDPROC



CONST_INT __ASSERT_IF_SAVEGAME_SIZES_DIFFER 1

#IF IS_DEBUG_BUILD
INT RunningTotalOfStructures = 0

PROC VerifySizeOfSavedStructure(INT SizeOfMostRecentStructure, STRING NameOfMostRecentStructure, BOOL bSP = TRUE)
    CPRINTLN(DEBUG_INIT, "SIZE_OF(", NameOfMostRecentStructure, ") = ", SizeOfMostRecentStructure)
	
    RunningTotalOfStructures += SizeOfMostRecentStructure
	
    INT SizeOfRegisteredVariables = GET_SIZE_OF_SAVE_DATA(bSP)
    
    IF RunningTotalOfStructures <> SizeOfRegisteredVariables
        CPRINTLN(DEBUG_INIT, "Size of registered save variables differs from actual size of structure")
        CPRINTLN(DEBUG_INIT, "Size of all structures so far = ", RunningTotalOfStructures)
        CPRINTLN(DEBUG_INIT, "SizeOfRegisteredVariables = ", SizeOfRegisteredVariables)
        CPRINTLN(DEBUG_INIT, "Most Recent Structure = ", NameOfMostRecentStructure)
#IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
        SCRIPT_ASSERT("Size of save struct differs from size of registered save variables - check TTY")
#ENDIF
    ENDIF
ENDPROC
#ENDIF  //  IS_DEBUG_BUILD




// ===========================================================================================================

SCRIPT

#IF IS_DEBUG_BUILD
	eLocalCachedLevel = GET_INDEX_OF_CURRENT_LEVEL()
	DEBUG_REGISTER_CHANNELS()
#ENDIF


	CPRINTLN(DEBUG_INIT, "Script initialisation began.")
	CPRINTLN(DEBUG_INIT, "Starting in game level ", DEBUG_GET_LEVEL_NAME(eLocalCachedLevel), ".")

	PRELOAD_ALL_STARTUP_SCRIPTS()

	//  Graeme - this doesn't seem like a good thing to do, but I need to make the script either safe for network game 
    //  or make it clean up when HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP) returns TRUE.
    //  The second option doesn't seem like a good idea either. How are we going to deal with the player entering
    //  multiplayer while this script is running e.g. the player accepts an invite on the XMB/Dashboard
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	//B*-2286368, confirm not in director mode/ animal form on startup
	SET_PLAYER_IS_IN_DIRECTOR_MODE(FALSE)
	SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
	
	// First thing we need to do is remove the prologue light IPL groups to fix bug # 1717335
	PRINTLN("Removing prologue IPL light groups on startup")
	REMOVE_IPL("prologue_DistantLights")
	REMOVE_IPL("prologue_LODLights")
	
	eGameType = eGameGTA
	
	//For bug 2149703 where JPN Final bail doesn't look like the game is faded out. 
	#IF IS_JAPANESE_BUILD
	IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
		CPRINTLN(DEBUG_INIT, "Game started faded in, fading out here. fadeout")
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("BC: Game started faded in, fading out here. fadeout ")
		#ENDIF
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	#ENDIF
	
#IF FEATURE_SP_DLC_SELECTION
	
// =================================================
//				DLC SELECTION SCREEN
// =================================================
	
	IF IS_DLC_PRESENT(HASH("dlc_agentTrevor"))	
	OR IS_DLC_PRESENT(HASH("spNorman"))
	
#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("ForceClifford")
			eGameType = eGameClifford
		ELIF GET_COMMANDLINE_PARAM_EXISTS("ForceNorman")
			eGameType = eGameNorman
		ELSE	
#ENDIF
			CPRINTLN(DEBUG_FLOW, "<DLC_SELECT> DLC selection beginning.")	
			SHUTDOWN_LOADING_SCREEN()
			DO_SCREEN_FADE_in(0)
			
			BOOL bChoiceMade = false		
			INT iSelectionChoiceTimer 
			FLOAT fCliffordX, fCliffordY
			FLOAT fNormanX, fNormanY
			INT fStickLeftX,fStickLeftY,fStickRightX,fStickRightY
			
			fCliffordX = 0.6
			fCliffordY = 0.5
			fNormanX = 0.5
			fNormanY = 0.6
			
			fStickLeftX= 0
			fStickLeftY= 0
			fStickRightX= 0
			fStickRightY= 0
			
			IF IS_DLC_PRESENT(HASH("spNorman"))
			AND NOT IS_DLC_PRESENT(HASH("dlc_agentTrevor"))
				fNormanX = 0.6
				fNormanY = 0.5
			ENDIF
			
			WHILE !bChoiceMade
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
				SET_TEXT_CENTRE(TRUE)	
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(fStickLeftX,fStickLeftY,fStickRightX,fStickRightY,true)
				
				SWITCH eGameType
				
					//GTA selected
					CASE eGameGTA
					
						//Green selected GTAV.
						SET_TEXT_SCALE(0.6, 0.6)
						SET_TEXT_COLOUR(0,255,0,255)
						DISPLAY_TEXT(0.4, 0.5, "DBG_GTAV")
					
						//Red un-selected Clifford and Norman.
						IF IS_DLC_PRESENT(HASH("dlc_agentTrevor"))
							SET_TEXT_SCALE(0.4, 0.4)
							SET_TEXT_COLOUR(255,0,0,150)
							DISPLAY_TEXT(fCliffordX, fCliffordY, "DBG_CLIF")
						ENDIF
						IF IS_DLC_PRESENT(HASH("spNorman"))
							SET_TEXT_SCALE(0.4, 0.4)
							SET_TEXT_COLOUR(255,0,0,150)
							DISPLAY_TEXT(fNormanX, fNormanY, "DBG_NORM")
						ENDIF	
						

						//Input detection.
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)		
#ENDIF
							CPRINTLN(DEBUG_FLOW, "<DLC_SELECT> Original GTA V selected.")
							bChoiceMade = TRUE
							DO_SCREEN_FADE_OUT(0)			
							
						ELIF fStickLeftX < -80
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) 	
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT) 	
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250
								IF NOT IS_DLC_PRESENT(HASH("spNorman"))								
									eGameType = eGameClifford
								ELSE
									eGameType = eGameNorman								
								ENDIF
								iSelectionChoiceTimer = GET_GAME_TIMER()							
							ENDIF
							
						ELIF fStickLeftX > 80
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) 
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250
								IF NOT IS_DLC_PRESENT(HASH("dlc_agentTrevor"))
									eGameType = eGameNorman
								ELSE
									eGameType = eGameClifford
								ENDIF
								iSelectionChoiceTimer = GET_GAME_TIMER()
								
							ENDIF
						ENDIF
					BREAK	
					
					//Clifford DLC selected.
					CASE eGameClifford
					
						//Green selected Clifford.
						SET_TEXT_SCALE(0.6, 0.6)
						SET_TEXT_COLOUR(0,255,0,255)
						DISPLAY_TEXT(fCliffordX, fCliffordY, "DBG_CLIF")
						
						//Red un-selected GTAV and Norman.
						SET_TEXT_SCALE(0.4, 0.4)
						SET_TEXT_COLOUR(255,0,0,150)
						DISPLAY_TEXT(0.4, 0.5, "DBG_GTAV")
						
						IF IS_DLC_PRESENT(HASH("spNorman"))
							SET_TEXT_SCALE(0.4, 0.4)
							SET_TEXT_COLOUR(255,0,0,150)
							DISPLAY_TEXT(fNormanX, fNormanY, "DBG_NORM")
						ENDIF
						
						//Input detection.
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)					
#ENDIF
							CPRINTLN(DEBUG_FLOW, "<DLC_SELECT> DLC Clifford selected.")	
							bChoiceMade = TRUE						
							DO_SCREEN_FADE_OUT(0)
							
						ELIF fStickLeftX < -80
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) 		
#IF IS_DEBUG_BUILD					
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT) 	
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250							
								eGameType = eGameGTA							
								iSelectionChoiceTimer = GET_GAME_TIMER()
							ENDIF
							
						ELIF fStickLeftX > 80
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) 
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250
								IF IS_DLC_PRESENT(HASH("spNorman"))
									eGameType = eGameNorman
								ELSE
									eGameType = eGameGTA
								ENDIF
								iSelectionChoiceTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					BREAK
					
					//Norman selected.
					CASE eGameNorman
					
						//Green selected Norman.					
						SET_TEXT_SCALE(0.6, 0.6)
						SET_TEXT_COLOUR(0,255,0,255)
						DISPLAY_TEXT(fNormanX, fNormanY, "DBG_NORM")
											
						//Red un-selected GTAV and Clifford.
						SET_TEXT_SCALE(0.4, 0.4)
						SET_TEXT_COLOUR(255,0,0,150)
						DISPLAY_TEXT(0.4, 0.5, "DBG_GTAV")
						
						IF IS_DLC_PRESENT(HASH("dlc_agentTrevor"))
							SET_TEXT_SCALE(0.4, 0.4)
							SET_TEXT_COLOUR(255,0,0,150)
							DISPLAY_TEXT(fCliffordX, fCliffordY, "DBG_CLIF")
						ENDIF									
						
						//Input detection.
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)				
#ENDIF
							CPRINTLN(DEBUG_FLOW, "<DLC_SELECT> DLC Norman selected.")	
							//will be setting globals to false here
							bChoiceMade = TRUE
							DO_SCREEN_FADE_OUT(0)
											
						ELIF fStickLeftX < -80
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) 			
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT) 	
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250
								IF IS_DLC_PRESENT(HASH("dlc_agentTrevor"))
									eGameType = eGameClifford
								ELSE
									eGameType = eGameGTA
								ENDIF
								iSelectionChoiceTimer = GET_GAME_TIMER()
							ENDIF
							
						ELIF fStickLeftX > 80
						OR 	IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) 						
#ENDIF
							IF GET_GAME_TIMER() - iSelectionChoiceTimer > 250							
								eGameType = eGameGTA							
								iSelectionChoiceTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH		
				
				WAIT(0)
			ENDWHILE
			CPRINTLN(DEBUG_FLOW, "<DLC_SELECT> DLC selection finished.")	
#IF IS_DEBUG_BUILD
		ENDIF
#ENDIF
	ENDIF
	
// =================================================
//			DLC SELECTION SCREEN - END
// =================================================

#ENDIF //FEATURE_SP_DLC_SELECTION
	
	
	//Cache and store whether or not we are booting directly into MP.
	//Read this now to avoid the decision changing during init rountines!
	BOOL bLocalMultiplayerOnStartup = LOAD_SCREEN_CHOICE_IS_MULTIPLAYER()	

	//Don't allow boots into Multiplayer for test levels.
	#IF IS_DEBUG_BUILD
		IF eLocalCachedLevel != LEVEL_GTA5
			CPRINTLN(DEBUG_INIT, "Overriding startup choice to be SP as we are loading test level ", DEBUG_GET_LEVEL_NAME(eLocalCachedLevel), ".")
			bLocalMultiplayerOnStartup = FALSE
		ENDIF
	#ENDIF

	IF IS_COMMANDLINE_END_USER_BENCHMARK()
	OR LANDING_SCREEN_STARTED_END_USER_BENCHMARK()
		CPRINTLN(DEBUG_INIT, "<LOAD-CHOICE> Overriding startup choice to be SP as we are loading an automatic benchmark.")
		bLocalMultiplayerOnStartup = FALSE
	ENDIF

	IF bLocalMultiplayerOnStartup
		CPRINTLN(DEBUG_INIT, "<LOAD-CHOICE> Commmiting to loading screen selection as multiplayer is selected.")
		#IF USE_FINAL_PRINTS 
			PRINTLN_FINAL("<2140379> <LOAD-CHOICE> Commmiting to loading screen selection as multiplayer is selected.")
		#ENDIF
		
		COMMIT_TO_LOADINGSCREEN_SELCTION()
		CPRINTLN(DEBUG_INIT, "<LOAD-CHOICE> Commmiting to loading screen selection.")
		#IF USE_FINAL_PRINTS 
			PRINTLN_FINAL("<2140379> <LOAD-CHOICE> Commmiting to loading screen selection.")
		#ENDIF
	ENDIF
           	
	// Setup streaming budgets.
	SET_INSTANCE_PRIORITY_MODE(INSTANCE_MODE_SINGLEPLAYER)
	       	
    // By default, switch off the blue 'locate' lines
	#IF IS_DEBUG_BUILD
	    SET_DEBUG_ACTIVE(FALSE) 
	#ENDIF
		
	// ------------------------ REGISTER OTHER GLOBAL BLOCKS -----------------------------------
	REGISTER_ALL_SCRIPT_GLOBALS()
	
	CPRINTLN(DEBUG_INIT, "startup.sc - about to reset iBitFieldOfRegisteredSaveData")
	iBitFieldOfRegisteredSaveData = 0
	
	CPRINTLN(DEBUG_INIT, "sp_dlc_registration.sc - about to set SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED")
	SET_BIT(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
	
	CPRINTLN(DEBUG_INIT, "sp_pilotschool_reg.sc - about to set SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED")
	SET_BIT(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)

	INT i
	REPEAT MAX_MP_PROPERTIES+1 i
		GET_MP_PROPERTY_DETAILS_FULL(mpProperties[i],i)
		PRINTLN("MP_PROP_GLOBAL_BLOCK: filled in details for MP property #",i)
	ENDREPEAT 
	
	Register_All_ReplayEditor_Fx()

	CPRINTLN(DEBUG_INIT, "Core variables initialised.")	
	
	// Re-create any global array data prior to reloading any saved data.
	SWITCH eGameType
		CASE eGameGTA
			CPRINTLN(DEBUG_INIT, "About to request and launch GLOBALS_BLOCK_STANDARD.")
			Request_And_Launch_Script_With_Wait(HASH("standard_global_init"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("standard_global_init"))
		BREAK
		
#IF FEATURE_SP_DLC_SELECTION
		CASE eGameClifford
			CPRINTLN(DEBUG_INIT, "About to request and launch GLOBALS_BLOCK_STANDARD.")
			Request_And_Launch_Script_With_Wait(HASH("sp_clifford_init"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("sp_clifford_init"))
		BREAK
		CASE eGameNorman			
			CPRINTLN(DEBUG_INIT, "About to request and launch GLOBALS_BLOCK_STANDARD.")
			Request_And_Launch_Script_With_Wait(HASH("sp_norman_init"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("sp_norman_init"))
		BREAK
#ENDIF //FEATURE_SP_DLC_SELECTION

	ENDSWITCH
	
    // IMPORTANT: THERE SHOULD BE NO 'WAIT' COMMANDS BEFORE HERE
    WAIT (0)
			
	g_bIsStartUp = TRUE	// Used to stop SET_GLOBAL_INSTANCE_PRIORITY_SP being called in main_persistant.sc
	g_bRunMultiplayerOnStartup = bLocalMultiplayerOnStartup
	
	//Store out what level we are running to a global.
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_INIT, "Cached the level being run as ", DEBUG_GET_LEVEL_NAME(eLocalCachedLevel), ".")
		g_eCachedLevel = eLocalCachedLevel
	#ENDIF
	
    SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)

// -----------------------------------------------------------------------------------------------------------
// SAVE GLOBALS REGISTRATION and RELOADING
// This will need to default the initialised flags for each system to FALSE
// Once all globals for a system are initialised it's initialised flag will
//      be set to TRUE and saved. After a reload the flag will therefore be
//      FALSE if no data for that system has previously been saved, or TRUE
//      if the globals for that system have already been initialised and saved.
// NOTE: After a reload, the system should initialise its global saved variables
//      if the flag is FALSE and leave them alone if the flag is TRUE.
// NOTE: If all global saved variables for a system are re-initialised prior to
//      this reload, then an 'initialised' flag isn't needed because there is no
//      initialisation being done after the reload takes place.
// -----------------------------------------------------------------------------------------------------------
	
	//  Mission Flow Globals are initialised PRIOR to reloading the save, so don't need an initialised flag for them
	//	KGM TEMP: Don't really want to reload anything at the moment until flow mode is activated
    //	Register all the global save variables
    //	(Add a _reg.sch header script to shared/globals/registration for each system)

	SWITCH eGameType

		CASE eGameGTA	
		
			g_bLoadedClifford = FALSE
			g_bLoadedNorman  = FALSE
			CPRINTLN(DEBUG_INIT, "About to request and launch GLOBALS_BLOCK_STANDARD.")
			Request_And_Launch_Script_With_Wait(HASH("standard_global_reg"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("standard_global_reg")
			
			WHILE (NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED)
					)
				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF
				
				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF
			
				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF
				
				WAIT(0)
			ENDWHILE

			// Something must be registered here as the first script to run must register atleast one global.
			START_SAVE_DATA(g_savedGlobals, SIZE_OF(g_savedGlobals.fSaveVersion), TRUE)
				REGISTER_FLOAT_TO_SAVE(g_savedGlobals.fSaveVersion , "fSaveVersion")
			STOP_SAVE_DATA()
						
			// Store out the version of the savefile we just loaded and then overwrite this value with our 
			// local savefile version. This will be written to any savefiles we make this session.
			// This must run after the SECOND registration of g_savedGlobals.fSaveVersion.
			// The first registration is done in Standard_global_reg.sc - if we write the value of g_savedGlobals.fSaveVersion
			// before the second registration we can lose it's new value.
			IF g_bRestoredSaveThisSession
				g_fLoadedSaveVersion = g_savedGlobals.fSaveVersion
			ENDIF		
			g_savedGlobals.fSaveVersion = SAVE_GAME_VERSION
			
			IF g_bRestoredSaveThisSession
				CPRINTLN(DEBUG_INIT, "-------------------------------------------")
				CPRINTLN(DEBUG_INIT, "Local Save Version:  ", SAVE_GAME_VERSION)
				CPRINTLN(DEBUG_INIT, "Loaded Save Version: ", g_fLoadedSaveVersion)
				CPRINTLN(DEBUG_INIT, "New Save Version:    ", g_savedGlobals.fSaveVersion)
				CPRINTLN(DEBUG_INIT, "-------------------------------------------")
				
				//Have we loaded a save from an older version of the game. If so apply any required data patches.
				IF g_fLoadedSaveVersion != SAVE_GAME_VERSION
					CPRINTLN(DEBUG_INIT, "<SAVE-PATCH> Applying save game script patching.")
					APPLY_SAVEGAME_PATCHING(g_fLoadedSaveVersion)
				ENDIF
			ENDIF			
					
		BREAK
		
#IF FEATURE_SP_DLC_SELECTION
		CASE eGameClifford
		
			g_bLoadedClifford = TRUE
			g_bLoadedNorman  = FALSE
			
			CPRINTLN(DEBUG_INIT, "About to request and launch GLOBALS_BLOCK_SP_CLF_DLC.")
			Request_And_Launch_Script_With_Wait(HASH("sp_clifford_reg"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("sp_clifford_reg"))
			
			WHILE (NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_CLIFFORD_SAVE_DATA_HAS_BEEN_REGISTERED)
					)
				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_CLIFFORD_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_CLIFFORD_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				WAIT(0)
			ENDWHILE
			
			START_SAVE_DATA(g_savedGlobals, SIZE_OF(g_savedGlobals.fSaveVersion), TRUE)
				REGISTER_FLOAT_TO_SAVE(g_savedGlobals.fSaveVersion , "fSaveVersion")
			STOP_SAVE_DATA()
		BREAK
		
		CASE eGameNorman
		
			g_bLoadedClifford = FALSE
			g_bLoadedNorman  = TRUE
			
			Request_And_Launch_Script_With_Wait(HASH("sp_norman_reg"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("sp_norman_reg"))
			
			WHILE (NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					OR NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_NORMAN_SAVE_DATA_HAS_BEEN_REGISTERED)
					)
				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				IF NOT IS_BIT_SET(iBitFieldOfRegisteredSaveData, SP_NORMAN_SAVE_DATA_HAS_BEEN_REGISTERED)
					CPRINTLN(DEBUG_INIT, "startup.sc - SP_NORMAN_SAVE_DATA_HAS_BEEN_REGISTERED is not set")
				ENDIF

				WAIT(0)
			ENDWHILE
						
			START_SAVE_DATA(g_savedGlobals, SIZE_OF(g_savedGlobals.fSaveVersion), TRUE)
				REGISTER_FLOAT_TO_SAVE(g_savedGlobals.fSaveVersion , "fSaveVersion")
			STOP_SAVE_DATA()
		BREAK
		
#ENDIF //FEATURE_SP_DLC_SELECTION
		
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			CPRINTLN(DEBUG_INIT, "About to request and launch profiler registration.")
			Request_And_Launch_Script_With_Wait(HASH("profiler_registration"), DEFAULT_STACK_SIZE)
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("profiler_registration"))
		#ENDIF
	#ENDIF
	    
	#IF IS_DEBUG_BUILD
		RunningTotalOfStructures = 0
	#ENDIF
		
	TEXT_LABEL_63 textLabel63
	//INT iSizeOfNonSavedData = 0
//	iSizeOfNonSavedData += SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedGeneral.iMultiPropertyValueArray)
	START_SAVE_DATA(g_savedMPGlobalsNew, SIZE_OF(g_savedMPGlobalsNew), FALSE)
		START_SAVE_ARRAY_WITH_SIZE(g_savedMPGlobalsNew.g_savedMPGlobals, SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals), "g_savedMPGlobals")
			#IF IS_DEBUG_BUILD
			VerifySizeOfSavedStructure(1, "g_savedMPGlobals",FALSE)
			#ENDIF
			REPEAT SAVE_GAME_CHARACTER_SLOTS i
				//iSizeOfNonSavedData = 0
//		        REGISTER_MP_VEHICLES_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
//				#IF IS_DEBUG_BUILD
//					textLabel63 = "g_savedMPGlobals.MpSavedVehicles"
//					textLabel63 += i
//					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedVehicles), textLabel63,FALSE)
//				#ENDIF
				REGISTER_MP_BIG_ASS_VEHICLES_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.MpSavedBigAssVehicles"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedBigAssVehicles),textLabel63 ,FALSE)
				#ENDIF
				REGISTER_MP_PROPERTY_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.MpSavedProperty"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedProperty), textLabel63,FALSE)
				#ENDIF
		       /* REGISTER_MP_BUDDIES_SAVED_GLOBALS()
				#IF IS_DEBUG_BUILD
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies), "g_savedMPGlobals.MpSavedBuddies",FALSE)
				#ENDIF*/
				REGISTER_MP_CARAPP_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.MpSavedCarApp"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedCarApp),textLabel63 ,FALSE)
				#ENDIF
				REGISTER_MP_GENERAL_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 =  "g_savedMPGlobals.MpSavedGeneral"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedGeneral),textLabel63,FALSE)
					//iSizeOfNonSavedData += SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedGeneral.iMultiPropertyValueArray)
				#ENDIF
				REGISTER_MP_BOUNTY_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.MpSavedBounty"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedBounty),textLabel63 ,FALSE)
				#ENDIF
				REGISTER_MP_ATM_SAVED_GLOBALS(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.MpSavedATM"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].MpSavedATM), textLabel63,FALSE)
				#ENDIF
				
				INIT_ALL_MP_SCRIPT_SAVES(g_savedMPGlobalsNew.g_savedMPGlobals[i],i)
				#IF IS_DEBUG_BUILD
					textLabel63 = "g_savedMPGlobals.mpSaveData"
					textLabel63 += i
					VerifySizeOfSavedStructure(SIZE_OF(g_savedMPGlobalsNew.g_savedMPGlobals[i].mpSaveData),textLabel63 ,FALSE)
				#ENDIF
			ENDREPEAT
		STOP_SAVE_STRUCT()
	STOP_SAVE_DATA()
	
 	INT SizeOfMPStruct = SIZE_OF(g_savedMPGlobalsNew)// - iSizeOfNonSavedData
    INT SizeOfMPRegisteredVariables = GET_SIZE_OF_SAVE_DATA(FALSE)
    CPRINTLN(DEBUG_INIT, "SizeOfMPStruct = ", SizeOfMPStruct)
    CPRINTLN(DEBUG_INIT, "SizeOfMPRegisteredVariables = ", SizeOfMPRegisteredVariables)
   
    #IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
        IF SizeOfMPStruct <> SizeOfMPRegisteredVariables
            SCRIPT_ASSERT("Size of MP save struct differs from size of registered MP save variables - check TTY")
        ENDIF
    #ENDIF
	
	CPRINTLN(DEBUG_INIT, "Saved MP globals registered and restored.")
	
// -----------------------------------------------------------------------------------------------------------
// SAVE GLOBALS REGISTRATION and RELOADING - END
// -----------------------------------------------------------------------------------------------------------

	// Start fetching all the commerce product data from the cloud
	IF NOT IS_COMMERCE_DATA_VALID()
	AND NOT IS_COMMERCE_DATA_FETCH_IN_PROGRESS()
		CPRINTLN(DEBUG_INIT, "Fetching thin commerce data.")
		TRIGGER_COMMERCE_DATA_FETCH(FALSE)
	ENDIF
	
	// Fix for bug 1310944 - Can someone use this new script command to 
	// prevent prologue nodes from streaming in when the map isn't loaded?
	SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)
	
	//Ensure code don't think we're still in animal form.
	SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
		
    // Set the network privilage bools
    Set_Network_Privilege_Bools()
	
	//Populate array with tunnel locations
	POPULATE_TUNNEL_INTERIOR_INDEX_LIST()
	
	
    CPRINTLN(DEBUG_INIT, "Startup.sc ended.")
    TERMINATE_THIS_THREAD()
ENDSCRIPT
