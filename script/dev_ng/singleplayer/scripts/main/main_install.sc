USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "script_buttons.sch"
USING "commands_audio.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_core.sch"
#ENDIF

//CAMERA_INDEX scriptedCamera
//VECTOR vCamAdjustTarget, vCamAdjustActive
//INT xLeft, yLeft, xRight, yRight

INT iToolTipNumber = -1
INT iToolTipPrevious

SCRIPT
	CPRINTLN(DEBUG_INIT, "GTA5 Install main thread started.")
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	
	CPRINTLN(DEBUG_INIT, "Setting up proglogue map IPLs.")
	//IPL Groups - Interior
	REQUEST_IPL("prologue06_int")

	//IPL Groups
	//NB: if these change remember to notfiy AI code so they can update the "GTA5_IplGroup.xml" used to create navmeshes
	REQUEST_IPL("prologue01")
	REQUEST_IPL("prologue02")
	REQUEST_IPL("prologue03")
	REQUEST_IPL("prologue04")
	REQUEST_IPL("prologue05")
	REQUEST_IPL("prologue06")
	REQUEST_IPL("prologuerd")

	//REQUEST_IPL("Prologue01b")
	REQUEST_IPL("Prologue01c")
	REQUEST_IPL("Prologue01d")
	REQUEST_IPL("Prologue01e")
	REQUEST_IPL("Prologue01f")
	REQUEST_IPL("Prologue01g")
	REQUEST_IPL("prologue01h")
	REQUEST_IPL("prologue01i")
	REQUEST_IPL("prologue01j")
	REQUEST_IPL("prologue01k")
	REQUEST_IPL("prologue01z")
	REQUEST_IPL("prologue03b")
	REQUEST_IPL("prologue04b")
	REQUEST_IPL("prologue05b")
	REQUEST_IPL("prologue06b")
	REQUEST_IPL("prologuerdb")
	
	REQUEST_IPL("prologue_occl")

	REQUEST_IPL("DES_ProTree_start")
	REQUEST_IPL("DES_ProTree_start_lod")

	REQUEST_IPL("prologue04_cover")

	REQUEST_IPL("prologue03_grv_fun")   //REQUEST_IPL("prologue03_grv_dug") //prologue03_grv_cov

	//REQUEST_IPL("prologue_LODLights") //REMOVE_IPL_FROM_REMOVAL_LIST("prologue_LODLights")
	//REQUEST_IPL("prologue_DistantLights") //REMOVE_IPL_FROM_REMOVAL_LIST("prologue_DistantLights")

	REMOVE_IPL("prologue03_grv_dug")
	REMOVE_IPL("prologue_grv_torch")

	//Cullbox
	SET_MAPDATACULLBOX_ENABLED("prologue", TRUE)


	CPRINTLN(DEBUG_INIT, "Swapping player model.")
	REQUEST_MODEL(CSB_PROLSEC)
	WHILE NOT HAS_MODEL_LOADED(CSB_PROLSEC)
		WAIT(0)
	ENDWHILE
	SET_PLAYER_MODEL(PLAYER_ID(), CSB_PROLSEC)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<5313.8, -5207.2, 83.50>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 169.2)
	ENDIF
	
	
	CPRINTLN(DEBUG_INIT, "Configuring cameras.")
	DISPLAY_RADAR(FALSE)
//	scriptedCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//	
//	SET_CAM_COORD(scriptedCamera,<<5313.733887,-5206.475098,84.177109>>)
//	SET_CAM_ROT(scriptedCamera,<<-10.127284,0.000000,169.204346>>)
//	SET_CAM_FOV(scriptedCamera,50.000000)
//	
//	SET_CAM_ACTIVE(scriptedCamera, TRUE)
	
//	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	
	CPRINTLN(DEBUG_INIT, "Starting to stream in map...")
	NEW_LOAD_SCENE_START_SPHERE(<<5312.995605,-5208.670898,83.905258>>, 10.0)
	
	INT iTimeout = GET_GAME_TIMER() + 10000
	
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED())
	AND GET_GAME_TIMER() < iTimeout
		WAIT(0)
	ENDWHILE
	
#IF IS_DEBUG_BUILD
	IF GET_GAME_TIMER() > iTimeout
		CPRINTLN(DEBUG_INIT, "Map loading timed out.")
	ELSE
		CPRINTLN(DEBUG_INIT, "Map loaded.")
	ENDIF
#ENDIF
	
	CPRINTLN(DEBUG_INIT, "Shutting down load screen and fading in.")
	SHUTDOWN_LOADING_SCREEN()
	
	START_AUDIO_SCENE("MISSION_FAILED_SCENE")
	
	DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
	NEW_LOAD_SCENE_STOP()

	REQUEST_ADDITIONAL_TEXT("NG_INSTALL", MISSION_TEXT_SLOT)


	CPRINTLN(DEBUG_INIT, "GTA5 Install main loop starting now...")
	WHILE TRUE
	
		IF NOT IS_SCREEN_FADED_IN()
      		DO_SCREEN_FADE_IN(0)
		ENDIF

		DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
		
		DRAW_RECT(0.5, 0.5, 0.8, 0.8, 20, 20, 20, 255)

		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_CENTRE(TRUE)
		
		CPRINTLN(DEBUG_INIT, "GTA5 Install main loop running...", iToolTipNumber, " ", iToolTipPrevious, " ", TIMERA()  )
	
		IF TIMERA() > 10000
		AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			iToolTipNumber = GET_RANDOM_INT_IN_RANGE(0, 5)			
			IF (iToolTipNumber - iToolTipPrevious) <> 0
				iToolTipPrevious = iToolTipNumber
				SETTIMERA(0)
			ENDIF
		ELSE
	
			SWITCH iToolTipNumber
			
				CASE 0
					DISPLAY_TEXT(0.5, 0.4, "NG_INST_TT_1")			
				BREAK
				
				CASE 1
					DISPLAY_TEXT(0.5, 0.4, "NG_INST_TT_2")			
				BREAK
				
				CASE 2
					DISPLAY_TEXT(0.5, 0.4, "NG_INST_TT_3")							
				BREAK
				
				CASE 3				
					DISPLAY_TEXT(0.5, 0.4, "NG_INST_TT_4")			
				BREAK
				
				CASE 4				
					DISPLAY_TEXT(0.5, 0.4, "NG_INST_TT_5")			
				BREAK
				
			ENDSWITCH
		ENDIF
	
		DISABLE_FRONTEND_THIS_FRAME()
		
//		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(xLeft, yLeft, xRight, yRight)
//	
//		vCamAdjustTarget.x = ((0.5 * xRight) /255)
//		vCamAdjustTarget.y = ((0.5 * yRight) /255)
//		
//		//Smooth active camera position to target position. Make it frame rate independent.
//		CONST_FLOAT 	CAM_INTERP_RATE		0.25
//		FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
//		 
//		vCamAdjustActive.x += (vCamAdjustTarget.x - vCamAdjustActive.x) * fFrameRateModifier * CAM_INTERP_RATE
//		vCamAdjustActive.y += (vCamAdjustTarget.y - vCamAdjustActive.y) * fFrameRateModifier * CAM_INTERP_RATE 
//
//		SET_CAM_COORD(scriptedCamera,<<5313.733887 + vCamAdjustActive.x, -5206.475098 + vCamAdjustActive.y, 84.177109>>)
//		
//		POINT_CAM_AT_COORD(scriptedCamera, <<5312.995605,-5208.670898,83.905258>>)
//	
		
		
	
		
		IF HAS_ASYNC_INSTALL_FINISHED()
			CLEANUP_ASYNC_INSTALL()
			EXIT
		ENDIF
		
		
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_INIT, "GTA5 Install FINISHED...")
	
ENDSCRIPT
