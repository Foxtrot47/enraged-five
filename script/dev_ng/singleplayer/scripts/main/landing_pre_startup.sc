USING "globals.sch"
USING "mp_globals_new_features_TU.sch"
USING "commands_landingpage.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "stack_sizes.sch"
USING "Transition_Controller.sch"
USING "PreloadStartupScripts.sch"

#IF IS_DEBUG_BUILD
USING "commands_debug.sch"
#ENDIF

#IF FEATURE_GEN9_STANDALONE

FUNC STRING GET_STARTUP_SCRIPT_NAME()
	STRING initialScriptName = "startup"
	IF NOT IS_GAME_INSTALLED()
		initialScriptName = "startup_install"
	ENDIF
	RETURN initialScriptName
ENDFUNC

PROC REQUEST_AND_LAUNCH_STARTUP_SCRIPT_WITH_WAIT()

	STRING initialScriptName = GET_STARTUP_SCRIPT_NAME()
	
#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("startupscript")
		initialScriptName = GET_COMMANDLINE_PARAM("startupscript")
	ENDIF
#ENDIF
	
	PRINTLN("[LANDING_PRE_STARTUP] Requesting ", initialScriptName)
	REQUEST_SCRIPT(initialScriptName)
	WHILE NOT HAS_SCRIPT_LOADED(initialScriptName)
		PRINTLN("[LANDING_PRE_STARTUP] Requesting ", initialScriptName)
		REQUEST_SCRIPT(initialScriptName)
		WAIT(0)
	ENDWHILE
	
	PRINTLN("[LANDING_PRE_STARTUP] Calling START_NEW_SCRIPT for ", initialScriptName)
	START_NEW_SCRIPT(initialScriptName, DEFAULT_STACK_SIZE)
	SET_SCRIPT_AS_NO_LONGER_NEEDED(initialScriptName)
ENDPROC

#ENDIF // FEATURE_GEN9_STANDALONE

SCRIPT
	
	#IF FEATURE_GEN9_STANDALONE
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF IS_GAME_INSTALLED()
		REQUEST_SCRIPT(GET_STARTUP_SCRIPT_NAME())
		PRELOAD_ALL_STARTUP_SCRIPTS()
	ENDIF
	
	WHILE IS_LANDING_PAGE_ACTIVE()
		WAIT(0)
	ENDWHILE
	
	// We don't register log channels until startup. Rather than change this, let's just print without one.
	PRINTLN("[LANDING_PRE_STARTUP] Finished running MAINTAIN_INITIAL_LANDING_PAGE")
	
	// After the initial Landing Page (if enabled) we still need to load the game session. Scripts must wait until this is complete.
	#IF IS_DEBUG_BUILD
	INT iWaitStartedAt = GET_FRAME_COUNT()
	#ENDIF
	WHILE NOT IS_SESSION_INITIALIZED()
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() - iWaitStartedAt) % 30 = 0
			PRINTLN("[LANDING_PRE_STARTUP] Waiting for game session")
		ENDIF
		#ENDIF
		WAIT(0)
	ENDWHILE

	PRINTLN("[LANDING_PRE_STARTUP] Calling REQUEST_AND_LAUNCH_STARTUP_SCRIPT_WITH_WAIT")
	REQUEST_AND_LAUNCH_STARTUP_SCRIPT_WITH_WAIT()
	
	#ENDIF // FEATURE_GEN9_STANDALONE
	
ENDSCRIPT
