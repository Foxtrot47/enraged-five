USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_core.sch"
#ENDIF


PROC Request_And_Launch_Script_With_Wait(STRING strScriptName, INT iStackSize)
    REQUEST_SCRIPT(strScriptName)
	CPRINTLN(DEBUG_INIT, "Requesting ", strScriptName)
    WHILE NOT HAS_SCRIPT_LOADED(strScriptName)
        WAIT(0)
        REQUEST_SCRIPT(strScriptName)
    ENDWHILE
	CPRINTLN(DEBUG_INIT, "Finished loading ", strScriptName)
	CPRINTLN(DEBUG_INIT, "Starting ", strScriptName)
    START_NEW_SCRIPT(strScriptName, iStackSize)
ENDPROC

SCRIPT
	#IF IS_DEBUG_BUILD
	    DEBUG_REGISTER_CHANNELS()
	#ENDIF

	CPRINTLN(DEBUG_INIT, "GTA5 Install startup script started.")
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	Request_And_Launch_Script_With_Wait("main_install", DEFAULT_STACK_SIZE)
ENDSCRIPT
