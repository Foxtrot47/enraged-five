USING "global_block_defines.sch"

USING "globals.sch"

GLOBALS GLOBALS_BLOCK_MP TRUE

// MP_Globals.sch has started off empty, all it's contents have been moved to MP_Globals_Temp.sch
// MP_Globals.sch is now registered in it's own block. MP_Globals_TEMP.sch is still registered in the standard block.
// As much of MP_Globals_TEMP.sch will be pulled across into MP_Globals.sch as possible so that as many MP Globals
//			as possible will end up registered in their own block.
USING "MP_globals.sch"


SCRIPT

ENDSCRIPT

