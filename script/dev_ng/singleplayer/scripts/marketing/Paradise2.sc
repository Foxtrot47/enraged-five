USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paradise 2 - Trailer Script
//		AUTHOR			:	A Ross and Craig production.
//		DESCRIPTION		:	Trailver 2 shot selection script.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_clock.sch"
USING "flow_public_core_override.sch"
USING "commands_cutscene.sch"
USING "commands_graphics.sch"

USING "select_mission_stage.sch"
USING "shared_debug.sch"
USING "script_debug.sch"
USING "scripted_cam_editor_public.sch"
USING "script_ped.sch"
USING "cellphone_public.sch"
using "selector_public.sch"
using "building_control_public.sch"
USING "flow_reset_GAME.sch"
Using "Locates_public.sch"

//USING "traffic.sch"
CONST_INT MAX_TRAIL_POINTS 80
USING "gas_trails.sch"
USING "cam_recording_public.sch"
//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_phone,
	mpf_fatmancross,
	mpf_oldman,
	mpf_driver,
	
	mpf_trev,
	mpf_frank,
	mpf_mike,
	mpf_slide,
	mpf_chop,
	
	mpf_pilot1,
	mpf_pilot2,
	mpf_jetPilot,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	
	veh_Driveway,
	
	veh_first_train,
	veh_Ghetto_train,
	veh_truck,
	
	veh_parked_car,
	veh_tornado3,
	
	veh_dune,
	
	veh_slide,
	
	veh_Bike,
	Veh_police1,
	veh_police2,
	veh_police3,
	veh_police4,
	veh_exp1,
	veh_exp2,
	veh_exp3,
	
	veh_heli1,
	veh_heli2,
	veh_jet,
		
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS

	STAGE_0_WAIT_FOR_STAGE,
	STAGE_1_RUN_THROUGH_SHOTS,
	
	STAGE_2_OBSERVATORY_SHOT,
	STAGE_2_OBSERVATORY_SHOT_alt,
	STAGE_3_FAM2_INT_P1,
	STAGE_4_DRIVEWAY_SHOT,
	STAGE_5_FAM1_INT_P1,	
	STAGE_6_CITY_TRAIN_SHOT,
	STAGE_7_COUNTRY_SCAPE_SHOT,
	STAGE_8_LIQUOR_ACE_SHOT,
	STAGE_9_TREV1_MCS3_P2_A,
	STAGE_10_CHIN1_INT_P2,
	STAGE_11_TREV1_MCS3_P2_B,
	AS_BUGGY,	
	AS_TREV_MOL,
	AS_FIRE_TRAIL,
	AS_BARN_BOOM,
	STAGE_12_CHIN1_MCS4,
	STAGE_13_DOCKS_BRIDGE_SHOT,
	STAGE_13_DOCKS_BRIDGE_SHOT_ALT,
	STAGE_14_GHETTO_TRAIN_SHOT,
	STAGE_15_ARM1_INT_P3,
	AS_LAUNCH_ARM1,//AS_ARMENIAN_CHASE,
	STAGE_16_FRA2_INT_P2,	
	AS_LAUNCH_CAR_STEAL_FINALE,//AS_CAR_TRANS_TRUCK,
	STAGE_17_ARM1_INT_P4,
	STAGE_17_ARM1_INT_P4_ALT,
	AS_LAUNCH_CAR_STEAL_3,//AS_RACE_SHOT,
	STAGE_18_ARM1_INT_P1,
	AS_LAUNCH_FAMILY_1,//AS_TREV_PLANE_TKOFF,			
	STAGE_19_FAM5_MCS5,
	STAGE_20_TREV1_INT_P4,
	AS_LAUNCH_TREV2,//AS_PLANE_CHASE,	
	AS_VAULT_EXP,
	AS_PRO_BLAST,//AS_PROL_EXP,
	AS_LAUNCH_FRANK_1,//AS_CHOP_DRIVE_BY,
	AS_CAR_SLIDE,
	AS_CHOP_ON_CAR,
	AS_LAUNCH_FBI_2,//	AS_MIKE_RAPPEL,
	AS_LAUNCH_JEWEL_2A,//AS_JEWL_HEIST,
	AS_FRANK_BIKE_EXP,
	AS_LAUNCH_EXILE_1,//	AS_CARGO_PLANE_JUMP_JEEP,
	AS_LAUNCH_OFFROAD_RACE_1,//AS_DIRT_BIKE,
	AS_LAUNCH_EXILE_3,
	STAGE_21_FBI2_MCS1_P1,
	STAGE_22_FIN_EXT_P2,
	AS_JET_FIGHT,
	STAGE_23_FINAL_SHOT,
	STAGE_23_FINAL_SHOT_ALT,
	
//	STAGE_RECORD,
	
	STAGE_NUM_LAST//Leave this as last stage, used to detect end of shotlist.
	
ENDENUM
//Object
ENUM WEAPON_REGISTER
	WEAPON_MICHAEL,
	WEAPON_TREVOR,
	WEAPON_BRAD
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
ENDSTRUCT
struct decal
	DECAL_ID the_decal_id
	DECAL_RENDERSETTING_ID decal_texture_id
	vector pos
	vector direction
	vector side
	float width
	float height
	float fAlpha
	float life
	float wash_amount
	blip_index blip
	bool decal_removed
endstruct
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
//CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					150
//CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					50	
//CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				25	
//CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		5
//CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK		8
//CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		5
CONST_INT				STAGE_ENTRY		      		0
CONST_INT				STAGE_EXIT		      		-1	 
CONST_INT 				TIMECYCLE_REGION_GLOBAL		0
CONST_INT 				TIMECYCLE_REGION_URBAN		1
//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------
//-------------------- General Variables --------------------
VEHICLE_STRUCT					Vehs[MVF_NUM_OF_VEH]		//holds all of the vehicles 
PEDS_STRUCT						peds[MPF_NUM_OF_PEDS]		//holds all of the peds

OBJECT_INDEX 					objWeapon[COUNT_OF(WEAPON_REGISTER)]
SELECTOR_PED_STRUCT				sSelectorPeds
SEQUENCE_INDEX					seq
RAYFIRE_INDEX					rfThisRayfire 
RAYFIRE_INDEX					rfVaultExplosion
STREAMVOL_ID					streamvol

int 							missionCandidateID = NO_CANDIDATE_ID

INT								syncsceneID
INTERIOR_INSTANCE_INDEX			intdepot

//PTFX_ID 						ptfxJet
//INT 							ievoTime
//float 							fthrottle

vector					vdrivewaypos = << -820.468, 156.507, 68.245 >>
float					fdrivewayheading = 110.255

vector 					vTrainCityPos = <<795.562,-477.994,28.228>>
vector					vtrainGhettopos = << 197.78703, -1948.25781, 19.89035>>

vector					vparkcarpos = << 1992.930, 3070.402,46.048 >>
float					fparkcarheading = 74.653

vector					vtornado3	= << 2006.4985, 3054.5190, 46.0491 >>
float					ftornado3	= 57.8467

vector 					vslidecar	= <<491.43, -537.51, 24.27>>
float 					fslidecar	= -91.61

vector					vprolStart = <<5294.97949, -5191.41504, 82.51868>>

decal blood[2]

//-------------------- Stage Management --------------------
STAGE_SWITCHSTATE		stageswitch				//current switching status

INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
 
//--------------------- widget stuff ----------------------
WIDGET_GROUP_ID paradiseWidgetGroup
MissionStageMenuTextStruct zMenuNames[STAGE_NUM_LAST]

bool				bplayerControl = false
bool				bresetScene = true

int 				iTrainConfiguration = 7
float				ftrainSpeed = 14.5

float				fLODscale = 1
bool 				bHighDofOn = false
bool 				bDisplayTitles = false
int 				iTitlesTimer
bool				bWideScreen = true
bool				bResetShadows = false

float 				fwatereflection = 25.4

int					iDockWorkerHeadID 	= 0
int					iDockWorkerHeadText = 2
int 				iDockWorkerUpperID 	= 0
int 				idockworkerUpperText= 0
int					idockworkerlowID	= 0
int					idockworkerlowText	= 1
int 				iDockPropID			= 0
int					idockpropText		= 1

vector 				vDebugPosition = << 197.78703, -1948.25781, 19.89035>>//<< 1993.3800, 3069.3523, 46.0483 >>
vector				vDebugRotation = <<0,0,0>>
float				fDebugHeading = 63.1031
float				fDebugSpeed = 3

vector				vCamPOS
vector				vCamROT
float				fCamFOV
vector 				vdebugcamPOS
vector				vdebugCamROT
float				fdebugcamFOV

int 				idebugSkipTime1 = 2300
int					idebugSkipTime2 = 2100
int 				idebugskipTime3 = 2000
int 				idebugskiptime4 = 2000
bool				bResetTOD  = true
bool				bPauseClock = true
bool				bUseNewTOD = false
int					iTOD = 12

float fTrafficDensity  	= 1
float fPedDensity		= 1
bool bClearArea = false
//--------------------- Scene stuff -----------------------
CAMERA_INDEX cameraIndex
CAMERA_INDEX camfollow
CAM_RECORDING_DATA 	camRecData
bool				bPlayThrough = false
int 				isplat = 0
INT InitialTimeOfDayHour[STAGE_NUM_LAST]


// ===========================================================================================================
//		Init
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			PRINTLN("[stageManagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedStage)
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 1500
			PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


// ===========================================================================================================
//		Trailer procedures
// ===========================================================================================================
FUNC BOOL IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC
PROC setPedVariation(PED_INDEX thisped, INT iSimpleVariation)

	set_ped_component_variation(thisped, ped_comp_head, iSimpleVariation, 0)
	set_ped_component_variation(thisped, PED_COMP_HAIR, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_hand, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_torso, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_leg, iSimpleVariation, 0)	
	set_ped_component_variation(thisped, PED_COMP_DECL, iSimpleVariation, 0)	
	set_ped_component_variation(thisped, PED_COMP_FEET, iSimpleVariation, 0)	

ENDPROC

FUNC CAMERA_INDEX CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE(STRING CameraName, VECTOR vecPos, VECTOR vecRot, FLOAT FOV = 65.0, BOOL startActivated = FALSE)

	CAMERA_INDEX returnedCamera
	
	IF (mission_stage <> enum_to_int(STAGE_0_WAIT_FOR_STAGE))
		//DESTROY_ALL_CAMS()
	ENDIF
	if IsEntityAlive(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vecPos)
			SET_ENTITY_HEADING(player_ped_id(),vecRot.z)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		IF (mission_stage = enum_to_int(STAGE_0_WAIT_FOR_STAGE))	
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		endif	
	endif
	IF NOT DOES_CAM_EXIST(cameraIndex)
		returnedCamera = CREATE_CAM_WITH_PARAMS(CameraName, vecPos, vecRot, FOV, startActivated)
	ELSE
		returnedCamera = cameraIndex
		SET_CAM_PARAMS(cameraIndex, vecPos, vecRot, FOV)//, startActivated)
	ENDIF
	
	if bResetTOD
		if BuseNewTOD
			SET_CLOCK_TIME(iTOD, 0, 0)			
		else
			SET_CLOCK_TIME(InitialTimeOfDayHour[mission_stage], 0, 0)
		endif
	endif
	
	if bPauseClock
		PAUSE_CLOCK(true)
	endif
	
	RETURN returnedCamera

ENDFUNC

PROC RESET_EVERYTHING()
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
//-------------------peds-----------------------
	int i
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif	
			if peds[i].id != player_ped_id()
				DELETE_PED(peds[i].id)	
			endif
		endif
	endfor	
//-------------------vehs-----------------------
	
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)
			if i = enum_to_int(veh_Ghetto_train)
			or i = enum_to_int(veh_first_train)
				DELETE_MISSION_TRAIN(vehs[i].id)
			else
				EMPTY_VEHICLE(vehs[i].id)
		 		DELETE_VEHICLE(vehs[i].id)
			endif
		endif
	endfor	
//-------------------Resets---------------------
	
	SET_RANDOM_TRAINS(true)
	
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
		IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) != RFMO_STATE_END
			SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_ENDING)
		ENDIF
	ENDIF
	
	rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)    
		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED) // Turn on interior
        SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)
	endif
	//Cullbox
	SET_MAPDATACULLBOX_ENABLED("prologue", FALSE)
	SET_MAPDATACULLBOX_ENABLED("Trailer_2_Train_Shot", false)
	SET_ENTITY_VISIBLE(player_ped_id(),false)
	SET_TIME_SCALE(1)

	fLODscale = 1
	//bools
	if bResetShadows
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0,0,0,0)	
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
	endif
	bplayerControl = false
	bHighDofOn = false
	WATER_REFLECTION_SET_HEIGHT(false,fwatereflection)
	remove_decals_in_range(<<1998.2826, 3058.0435, 46.0491>>, 10000.00)	
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
	SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(1)
	
//------------------Destroys--------------------
	REPEAT COUNT_OF(objWeapon) i
		IF DOES_ENTITY_EXIST(objWeapon[i])
			DELETE_OBJECT(objWeapon[i])
		ENDIF
	ENDREPEAT
	
	UNLOAD_ALL_CLOUD_HATS()
	CLEAR_TIMECYCLE_MODIFIER()
	STREAMVOL_DELETE(streamvol)
	DISABLE_EXTRA_DISTANTLIGHTS()
	//Interior	
	UNPIN_INTERIOR(intDepot)
	
	REMOVE_IPL("prologue01")
	REMOVE_IPL("prologue02")
	REMOVE_IPL("prologue03")
	REMOVE_IPL("prologue04")
	REMOVE_IPL("prologue05")
	REMOVE_IPL("prologue06")
	REMOVE_IPL("prologuerd")
	
	REMOVE_IPL("Prologue01c")
	REMOVE_IPL("Prologue01d")
	REMOVE_IPL("Prologue01e")
	REMOVE_IPL("Prologue01f")
	REMOVE_IPL("Prologue01g")
	REMOVE_IPL("prologue01h")
	REMOVE_IPL("prologue01i")
	REMOVE_IPL("prologue01j")
	REMOVE_IPL("prologue01k")
	REMOVE_IPL("prologue03b")
	REMOVE_IPL("prologue04b")
	REMOVE_IPL("prologue05b")
	REMOVE_IPL("prologue06b")
	REMOVE_IPL("prologuerdb")	
	REMOVE_IPL("prologue04_cover")	
	REMOVE_IPL("prologue03_grv_dug")	//prologue03_grv_cov
	
	REMOVE_IPL("TRV1_Trail_end")
	
	SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(TRUE)
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	CLEAR_FOCUS()
	
ENDPROC
// ===========================================================================================================
//		Termination
// ===========================================================================================================

PROC Mission_Cleanup(BOOL bTerminateScript)

	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
	
//-------------------Destroy-----------------------	
	STOP_CAM_RECORDING(camRecData)
	UNLOAD_ALL_CLOUD_HATS()
	DELETE_WIDGET_GROUP(paradiseWidgetGroup)
	STREAMVOL_DELETE(streamvol)
	
	rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
    IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)   
		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_ENDING)
	ENDIF
//-------------------Resets-----------------------
	RESET_EVERYTHING()
	

	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0,0,0,0)	
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
	
	SET_TIMECYCLE_REGION_OVERRIDE(-1)
	CLEAR_WEATHER_TYPE_PERSIST()
	SET_TIME_SCALE(1.0)	
	SET_WIND_SPEED(0.0)
	SET_WIND_DIRECTION(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
		
	DO_SCREEN_FADE_IN(0)
	
	if bTerminateScript			
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		DISPLAY_RADAR(TRUE)
		FLOAT groundZ
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), groundZ)
			VECTOR groundCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			groundCoord.z = groundZ
			SET_ENTITY_COORDS(PLAYER_PED_ID(), groundCoord)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		SET_PLAYER_CONTROL(player_id(),true)		
		g_bTrailer2Active = false
		g_savedGlobals.sFlow.isGameflowActive = TRUE
		TERMINATE_THIS_THREAD()
	endif
	
ENDPROC

FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED			
		return True
	else
		return false
	endif
ENDFUNC
proc Load_Stage_Assets(MSF_MISSION_STAGE_FLAGS load_stage)
	if bPlayThrough
		RESET_EVERYTHING()
	endif
	switch load_stage
		case STAGE_0_WAIT_FOR_STAGE	
		//inital opening stage
		break
		case STAGE_1_RUN_THROUGH_SHOTS			
		break		
		case STAGE_2_OBSERVATORY_SHOT
			request_model(A_F_Y_TOURIST_01)
			REQUEST_MODEL(A_F_M_BEVHILLS_02)
			REQUEST_MODEL(A_M_M_BEVHILLS_02)
			requesT_model(A_M_M_BUSINESS_01)		
		break
		case STAGE_2_OBSERVATORY_SHOT_alt
			request_model(A_F_Y_TOURIST_01)
			REQUEST_MODEL(A_F_M_BEVHILLS_02)
			REQUEST_MODEL(A_M_M_BEVHILLS_02)
			requesT_model(A_M_M_BUSINESS_01)
		break
		case STAGE_3_FAM2_INT_P1
			REQUEST_CUTSCENE("family_2_intp1_b")
			REQUEST_MODEL(S_M_M_GARDENER_01)
			REQUEST_ANIM_DICT("amb@world_human_gardener_plant@male@idle_a")
		break
		case STAGE_4_DRIVEWAY_SHOT
			REQUEST_MODEL(NINEF2)			
		break
		case STAGE_5_FAM1_INT_P1
			REQUEST_CUTSCENE("family_1_intp1")
		break
		case STAGE_6_CITY_TRAIN_SHOT
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)			
		break
		case STAGE_7_COUNTRY_SCAPE_SHOT
			request_model(packer)
			REQUEST_MODEL(TRAILERS)
		break
		case STAGE_8_LIQUOR_ACE_SHOT
			request_model(RATLOADER)
			request_model(tornado3)
			request_model(A_M_M_HILLBILLY_01)
			request_model(A_M_M_HILLBILLY_02)
			request_model(A_M_M_Salton_01)
			request_model(A_F_M_Salton_01)
			request_model(S_M_Y_XMech_02)
			REQUEST_ANIM_DICT("move_drunk_m")
			request_anim_dict("trailer2@shoved_peds")
			request_anim_dict("amb@world_human_leaning@female@wall@back@holding_elbow@idle_a")
			request_anim_dict("move_m@generic_variations@walk")
		break
		case STAGE_9_TREV1_MCS3_P2_A
			REQUEST_CUTSCENE("trv_1_mcs_3_p2")
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")
			REQUEST_MODEL(PROPTRAILER)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
		break
		case STAGE_10_CHIN1_INT_P2
			REQUEST_CUTSCENE("chinese_1_intp2")
		break
		case STAGE_11_TREV1_MCS3_P2_B 
			REQUEST_CUTSCENE("trv_1_mcs_3_p2")
			REQUEST_CUTSCENE("trv_1_mcs_3_p2")
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")
			REQUEST_MODEL(PROPTRAILER)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
		break
		case AS_BUGGY
			request_model(dune)
			LOAD_CLOUD_HAT("HORIZON")
		break
		case AS_TREV_MOL
			request_model(player_two)
			REQUEST_ANIM_DICT("trailer2@molotov_throw")
		break
		case AS_FIRE_TRAIL
			
		break
		case AS_BARN_BOOM				
//			request_model(player_two)
//			REQUEST_ANIM_DICT("trailer2@trevor_barn_walk")
			REQUEST_CUTSCENE("barnhouseexplosion")
		break
		case STAGE_12_CHIN1_MCS4
			REQUEST_MODEL(SENTINEL)  
			REQUEST_MODEL(Landstalker)
			REQUEST_MODEL(G_M_Y_PoloGoon_01)
			REQUEST_CUTSCENE("chi_1_mcs_4")
		break
		case STAGE_13_DOCKS_BRIDGE_SHOT		
		break
		case STAGE_14_GHETTO_TRAIN_SHOT	
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
			REQUEST_MODEL(DOMINATOR)
			REQUEST_MODEL(FUTO)
			REQUEST_MODEL(GRESLEY)
			REQUEST_MODEL(CAVALCADE)				
			REQUEST_MODEL(SERRANO)
			REQUEST_MODEL(ORACLE)
			request_model(bus)
		break
		case STAGE_15_ARM1_INT_P3
			REQUEST_CUTSCENE("armenian_1_intp3")
			REQUEST_ANIM_DICT("move_m@jogger")
			REQUEST_ANIM_DICT("move_f@jogger")
			REQUEST_MODEL(A_M_Y_Runner_01)
			REQUEST_MODEL(A_F_Y_Runner_01)
			request_model(A_F_Y_Beach_01)
			request_model(A_M_Y_Beach_01)
			request_model(A_M_Y_Beach_02)
			request_model(A_M_Y_Beach_03)
		break
		case STAGE_16_FRA2_INT_P2	
			REQUEST_CUTSCENE("fra_2_int_p2")
		break
//		case AS_CAR_TRANS_TRUCK	
//		break	
		case STAGE_17_ARM1_INT_P4	
			REQUEST_CUTSCENE("armenian_1_intp4")
		break
		case STAGE_17_ARM1_INT_P4_ALT
			REQUEST_CUTSCENE("armenian_1_intp4")
		break
		case STAGE_18_ARM1_INT_P1	
			REQUEST_CUTSCENE("armenian_1_intp1")
		break
		case STAGE_19_FAM5_MCS5		
			REQUEST_CUTSCENE("family_5_mcs_5")
		break
		case STAGE_20_TREV1_INT_P4		
			request_cutscene("trevor_1_intp4")
		break
		case STAGE_21_FBI2_MCS1_P1	
			request_cutscene("FBI_2_MCS_1_p1")
		break
		case AS_VAULT_EXP		
		REQUEST_IPL("prologue01")
		REQUEST_IPL("prologue02")
		REQUEST_IPL("prologue03")
		REQUEST_IPL("prologue04")
		REQUEST_IPL("prologue05")
		REQUEST_IPL("prologue06")
		REQUEST_IPL("prologuerd")		
		REQUEST_IPL("Prologue01c")
		REQUEST_IPL("Prologue01d")
		REQUEST_IPL("Prologue01e")
		REQUEST_IPL("Prologue01f")
		REQUEST_IPL("Prologue01g")
		REQUEST_IPL("prologue01h")
		REQUEST_IPL("prologue01i")
		REQUEST_IPL("prologue01j")
		REQUEST_IPL("prologue01k")
		REQUEST_IPL("prologue03b")
		REQUEST_IPL("prologue04b")
		REQUEST_IPL("prologue05b")
		REQUEST_IPL("prologue06b")
		REQUEST_IPL("prologuerdb")
		REQUEST_IPL("prologue04_cover")
		REQUEST_IPL("prologue03_grv_dug")	//prologue03_grv_cov
		REQUEST_CUTSCENE("vault_explosion")
		break
		case AS_PRO_BLAST
		REQUEST_IPL("prologue01")
		REQUEST_IPL("prologue02")
		REQUEST_IPL("prologue03")
		REQUEST_IPL("prologue04")
		REQUEST_IPL("prologue05")
		REQUEST_IPL("prologue06")
		REQUEST_IPL("prologuerd")		
		REQUEST_IPL("Prologue01c")
		REQUEST_IPL("Prologue01d")
		REQUEST_IPL("Prologue01e")
		REQUEST_IPL("Prologue01f")
		REQUEST_IPL("Prologue01g")
		REQUEST_IPL("prologue01h")
		REQUEST_IPL("prologue01i")
		REQUEST_IPL("prologue01j")
		REQUEST_IPL("prologue01k")
		REQUEST_IPL("prologue03b")
		REQUEST_IPL("prologue04b")
		REQUEST_IPL("prologue05b")
		REQUEST_IPL("prologue06b")
		REQUEST_IPL("prologuerdb")
		REQUEST_IPL("prologue04_cover")
		REQUEST_IPL("prologue03_grv_dug")	//prologue03_grv_cov
		REQUEST_CUTSCENE("doorexplosion")
		REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
		break
		case AS_CAR_SLIDE
			REQUEST_MODEL(IG_BALLASOG)
			request_model(EMPEROR)
			request_model(S_M_Y_AIRWORKER)
			REQUEST_ANIM_DICT("trailer2@bonnet_slide")
		break
		case AS_CHOP_ON_CAR
			request_model(GET_CHOP_MODEL())
			request_model(EMPEROR)
			request_model(S_M_Y_AIRWORKER)
			REQUEST_MODEL(bison2)
			REQUEST_ANIM_DICT("MISSFRA0_CHOP")
		break
		case AS_FRANK_BIKE_EXP
			request_model(police)
			request_model(police2)
			request_model(police3)
			request_model(POLICET)
			request_model(sentinel2)
			request_model(stratum)
			request_model(emperor2)
			request_model(akuma)
			request_vehicle_recording(6,"Trailer2REC")
		break
		case AS_JET_FIGHT
			REQUEST_VEHICLE_RECORDING(1,"Trailer2REC")
			REQUEST_VEHICLE_RECORDING(2,"Trailer2REC")
			request_vehicle_recording(3,"Trailer2REC")
			request_vehicle_recording(4,"Trailer2REC")
			request_vehicle_recording(5,"Trailer2REC")
			REQUEST_MODEL(polmav)
			request_model(S_M_Y_Pilot_01)
			request_model(Lazer)
			REQUEST_MODEL(ADDER)
		break
		case STAGE_22_FIN_EXT_P2	
			request_cutscene("FIN_EXT_P2")
			request_model(BUZZARD)
		break
		case STAGE_23_FINAL_SHOT	
			request_cutscene("Fbi_4_int_p3")
		break
		case STAGE_23_FINAL_SHOT_ALT	
			request_cutscene("Fbi_4_int_p3")
		break
	endswitch
endproc
proc set_up_stage(MSF_MISSION_STAGE_FLAGS setup_stage)	
	switch setup_stage
		case STAGE_0_WAIT_FOR_STAGE
			if IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			endif
			//do nothing
		break
		case STAGE_1_RUN_THROUGH_SHOTS	
			set_up_stage(STAGE_2_OBSERVATORY_SHOT)			
		break		
		case STAGE_2_OBSERVATORY_SHOT		
						
			NEW_LOAD_SCENE_START(<< -484.5379, 1121.4351, 346.8401 >>, << -18.7962, -0.0000, -160.6597 >>,300)
			SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(setup_stage)], 0, 0)
			REQUEST_COLLISION_AT_COORD(<< -484.5379, 1121.4351, 346.8401 >>)	
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -484.5379, 1121.4351, 346.8401 >>)
			endif	
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)	
			SET_WEATHER_TYPE_NOW_PERSIST("smog")			
			while not HAS_MODEL_LOADED(A_F_Y_TOURIST_01)
			or not has_model_loaded(A_F_M_BEVHILLS_02)
			or not has_model_loaded(A_M_M_BEVHILLS_02)
			or not has_model_loaded(A_M_M_BUSINESS_01)
				wait(0)
			endwhile 
		break
		case STAGE_2_OBSERVATORY_SHOT_alt			
			
			NEW_LOAD_SCENE_START(<< -484.5379, 1121.4351, 346.8401 >>, << -18.7962, -0.0000, -160.6597 >>,300)			
			SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(setup_stage)], 0, 0)
			REQUEST_COLLISION_AT_COORD(<< -484.5379, 1121.4351, 346.8401 >>)	
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -484.5379, 1121.4351, 346.8401 >>)
			endif	
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)	
			SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")	
			
			while not HAS_MODEL_LOADED(A_F_Y_TOURIST_01)
			or not has_model_loaded(A_F_M_BEVHILLS_02)
			or not has_model_loaded(A_M_M_BEVHILLS_02)
			or not has_model_loaded(A_M_M_BUSINESS_01)
				wait(0)
			endwhile 
		break
		case STAGE_3_FAM2_INT_P1
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -819.1180, 157.1071, 69.5948 >>)
			endif
			SET_WEATHER_TYPE_NOW_PERSIST("clouds")
			
			NEW_LOAD_SCENE_START(<< -761.6831, 127.7389, 82.0954 >>, << -10.5571, -0.0000, 32.2714 >>,100)
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			
			while not HAS_CUTSCENE_LOADED()
			or not HAS_MODEL_LOADED(S_M_M_GARDENER_01)
			or not HAS_ANIM_DICT_LOADED("amb@world_human_gardener_plant@male@idle_a")
				wait(0)
			endwhile
			
		break
		case STAGE_4_DRIVEWAY_SHOT
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<  -819.1180, 157.1071, 69.5948 >>)
			endif	
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				while not HAS_MODEL_LOADED(ninef2)
					wait(0)
				endwhile			
				LOAD_SCENE(<< -819.1180, 157.1071, 69.5948 >>)	
				CLEAR_AREA(<< -819.1180, 157.1071, 69.5948 >>,20,true)
				SET_WEATHER_TYPE_NOW_PERSIST("clouds")			
			
		break				
		case STAGE_5_FAM1_INT_P1
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -797.4166, 185.6019, 73.3086 >>)
			endif								
			LOAD_SCENE(<<  -797.4166, 185.6019, 73.3086 >>)
			SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")
			intdepot = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-797.4166, 185.6019, 73.3086>>,"v_michael")
			PIN_INTERIOR_IN_MEMORY(intdepot)
			while not IS_INTERIOR_READY(intdepot)
				wait(0)
			endwhile
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,<<-797.2319, 186.4233, 72.9825>>)
				wait(0)
			endwhile
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_TORSO,26,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_LEG,16,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_feet,2,0)
			
		break
		case STAGE_6_CITY_TRAIN_SHOT	
			WHILE NOT HAS_MODEL_LOADED(FREIGHT)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
			OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
			OR NOT HAS_MODEL_LOADED(TANKERCAR)
			OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
				WAIT(0)
			ENDWHILE					
			LOAD_CLOUD_HAT("horizon")
			SET_RANDOM_TRAINS(FALSE)
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(2)
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<798.96228, -479.49374, 28.22828>>)				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			endif
			fLODscale = 1.3
			fTrafficDensity = 3
			load_scene(<<798.96228, -479.49374, 28.22828>>)
		break
		case STAGE_7_COUNTRY_SCAPE_SHOT
			
			WHILE NOT HAS_MODEL_LOADED(PACKER)
			or not has_model_loaded(TRAILERS)
				wait(0)
			endwhile
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			load_cloud_hat("cirrus")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2535.44971, 3032.43750, 41.90246>>)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			endif	
			load_scene(<<2535.44971, 3032.43750, 41.90246>>)
			CLEAR_AREA(<<2535.44971, 3032.43750, 41.90246>>,1000,true)
		break
		case STAGE_8_LIQUOR_ACE_SHOT
			
			WHILE NOT HAS_MODEL_LOADED(RATLOADER)
			OR NOT HAS_MODEL_LOADED(tornado3)
			OR NOT HAS_MODEL_LOADED(A_M_M_HILLBILLY_01)
			OR NOT HAS_MODEL_LOADED(A_M_M_HILLBILLY_02)
			OR NOT HAS_MODEL_LOADED(A_M_M_Salton_01)
			OR NOT HAS_MODEL_LOADED(A_F_M_Salton_01)
			or not HAS_MODEL_LOADED(S_M_Y_XMech_02)
			or not HAS_ANIM_DICT_LOADED("move_drunk_m")
			or not HAS_ANIM_DICT_LOADED("trailer2@shoved_peds")	
			or not HAS_ANIM_DICT_LOADED("amb@world_human_leaning@female@wall@back@holding_elbow@idle_a")
			or not HAS_ANIM_DICT_LOADED("move_m@generic_variations@walk")
				WAIT(0)
			ENDWHILE
			if not DOES_ENTITY_EXIST(vehs[veh_parked_car].id)
				vehs[veh_parked_car].id = CREATE_VEHICLE(RATLOADER,vparkcarpos,fparkcarheading)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_parked_car].id,0)			
			endif
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1989.4751, 3074.6912, 47.3940 >>)
			endif	
			load_scene(<<1989.4751, 3074.6912, 47.3940 >>)
			
		break
		case STAGE_9_TREV1_MCS3_P2_A
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -31.2,3040.7,40>>)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			endif	
			SET_WEATHER_TYPE_NOW_PERSIST("extrasunny")
			
			while not HAS_CUTSCENE_LOADED()
			or not HAS_MODEL_LOADED(PROPTRAILER)
				wait(0)
			endwhile			
			objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT(WEAPONTYPE_PUMPSHOTGUN,100,<< -31.2,3040.7,40 >>,true)	
			LOAD_SCENE(<< -31.2,3040.7,40 >>)
		break
		case STAGE_10_CHIN1_INT_P2
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1987.9679, 3053.7041, 46.1257 >>)
			endif	
			blood[0].decal_texture_id = DECAL_RSID_BLOOD_DIRECTIONAL
			blood[0].pos = <<1984.59, 3050.58, 47.533>> //47.43
			blood[0].direction = <<0.0, 0.0, -1.0>>
			blood[0].side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
			blood[0].width = 0.2
			blood[0].height = 0.2
			blood[0].fAlpha = 1.0
			blood[0].life = -1
			blood[0].wash_amount = 0.3
			blood[0].decal_removed = false
			

			blood[1].decal_texture_id = DECAL_RSID_BLOOD_SPLATTER 
			blood[1].pos = <<1984.64, 3050.62, 47.423>> //47.32
			blood[1].direction = <<0.0, -1.0, 0.0>>//<<0.0, -1.0, 0.0>>
			blood[1].side = normalise_vector(<<1.0, 0.0, 0.0>>) //heading //<<1.0, 1.0, 0.0>>
			blood[1].width = 0.2
			blood[1].height = 0.2
			blood[1].fAlpha = 1.0
			blood[1].life = -1
			blood[1].wash_amount = 0.3
			blood[1].decal_removed = false
			LOAD_SCENE(<< 1987.9679, 3053.7041, 46.1257 >>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_11_TREV1_MCS3_P2_B
			
			LOAD_CLOUD_HAT("CIRROCUMULUS")
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-31.2,3040.7,40 >>)
			endif	
			objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT(WEAPONTYPE_PUMPSHOTGUN,100,<< -31.2,3040.7,40 >>,true)
			LOAD_SCENE(<< -31.2,3040.7,40 >>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case AS_BUGGY
			
			WHILE NOT HAS_MODEL_LOADED(dune)				
				wait(0)
			endwhile
			SET_WEATHER_TYPE_NOW_PERSIST("FOGGY")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2330.9, 3127.2, 48.8 >>)
			endif	
			load_scene(<<2330.9, 3127.2, 48.8>>)
			
		break
		case AS_TREV_MOL
			
			SET_WEATHER_TYPE_NOW_PERSIST("overcast")
			LOAD_CLOUD_HAT("RAIN")
			WHILE not has_model_loaded(player_two)
			or not HAS_ANIM_DICT_LOADED("trailer2@molotov_throw")
				wait(0)
			endwhile
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2461.0942, 4952.7583, 45.2938>>)
			endif	
			load_scene(<<2461.0942, 4952.7583, 45.2938>>)
			
		break
		case AS_FIRE_TRAIL
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2442.16333, 4969.87842, 45.81060>>)				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),true)
			endif	
			load_scene(<<2442.16333, 4969.87842, 45.81060>>)
			
		break
		case AS_BARN_BOOM
									
			SET_WEATHER_TYPE_NOW_PERSIST("overcast")
			LOAD_CLOUD_HAT("RAIN")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2461.0942, 4952.7583, 45.2938>>)			
			endif	
			load_scene(<<2461.0942, 4952.7583, 45.2938>>)
			
		break
		case STAGE_12_CHIN1_MCS4	
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1387.8705, 3594.2642, 33.8831>>)	
			endif	
			while not HAS_CUTSCENE_LOADED()
			or not HAS_MODEL_LOADED(SENTINEL)  
			or not HAS_MODEL_LOADED(G_M_Y_PoloGoon_01)
			or not HAS_MODEL_LOADED(Landstalker)
				wait(0)
			endwhile
			LOAD_SCENE(<< 1387.8705, 3594.2642, 33.8831 >>)
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		break
		case STAGE_13_DOCKS_BRIDGE_SHOT	
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<844.4641, -2696.1641, 64.8>>)
			endif	
			load_scene(<<844.4641, -2696.1641, 64.8>>)			
			
		break
		case STAGE_13_DOCKS_BRIDGE_SHOT_ALT	
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<844.4641, -2696.1641, 64.8>>)
			endif	
			load_scene(<<844.4641, -2696.1641, 64.8>>)		
			
		break
		case STAGE_14_GHETTO_TRAIN_SHOT				
			WHILE NOT HAS_MODEL_LOADED(FREIGHT)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
			OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
			OR NOT HAS_MODEL_LOADED(TANKERCAR)
			OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
			OR NOT HAS_MODEL_LOADED(DOMINATOR)
			OR NOT HAS_MODEL_LOADED(FUTO)
			OR NOT HAS_MODEL_LOADED(GRESLEY)
			OR NOT HAS_MODEL_LOADED(CAVALCADE)				
			OR NOT HAS_MODEL_LOADED(SERRANO)
			OR NOT HAS_MODEL_LOADED(ORACLE)
			or not has_model_loaded(bus)
				WAIT(0)
			ENDWHILE
			SET_MAPDATACULLBOX_ENABLED("Trailer_2_Train_Shot", TRUE)			
			
			LOAD_CLOUD_HAT("HORIZON")
			SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")
			SET_RANDOM_TRAINS(FALSE)
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(2)
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<263.4,-1907.7,31.7>>)
			endif					
			NEW_LOAD_SCENE_START(<< 263.4090, -1907.6572, 31.7329 >>, << -6.2916, 0.0000, 111.8651 >>,250)
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
		break
		case STAGE_15_ARM1_INT_P3
			
			SET_WEATHER_TYPE_NOW_PERSIST("clear")				
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<263.4,-1907.7,31.7>>)
			endif	
			LOAD_SCENE(<< -1882.8746, -621.5580, 11.2904 >>)
			while not HAS_CUTSCENE_LOADED()
			or not HAS_MODEL_LOADED(A_M_Y_Runner_01)
			or not HAS_MODEL_LOADED(A_F_Y_Runner_01)
			or not has_model_loaded(A_F_Y_Beach_01)
			or not has_model_loaded(A_M_Y_Beach_01)
			or not has_model_loaded(A_M_Y_Beach_02)
			or not has_model_loaded(A_M_Y_Beach_03)
			or not HAS_ANIM_DICT_LOADED("move_m@jogger")
			or not HAS_ANIM_DICT_LOADED("move_f@jogger")
				wait(0)
			endwhile
			
		break
		case STAGE_16_FRA2_INT_P2	
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1882.8746, -621.5580, 11.2904>>)
			endif	
			LOAD_SCENE(<< -1882.8746, -621.5580, 11.2904 >>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_17_ARM1_INT_P4	
			SET_WEATHER_TYPE_NOW_PERSIST("Clear")
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1882.8746, -621.5580, 11.2904>>)
			endif	
			LOAD_SCENE(<< -1882.8746, -621.5580, 11.2904 >>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile			
		break
		case STAGE_17_ARM1_INT_P4_ALT	
			SET_WEATHER_TYPE_NOW_PERSIST("Clear")
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1882.8746, -621.5580, 11.2904>>)
			endif	
			LOAD_SCENE(<< -1882.8746, -621.5580, 11.2904 >>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile			
		break
		case STAGE_18_ARM1_INT_P1
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1908.9,-573.5,18>>)
			endif	
			LOAD_SCENE(<< -1908.9,-573.5,18>>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_19_FAM5_MCS5	
			fLODscale = 1.5
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")			
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1175.2771, -889.9739, 14.0581>>)
			endif	
			LOAD_SCENE(<< -1175.2771, -889.9739, 14.0581>>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_20_TREV1_INT_P4	
			
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2019.6141, 3827.6531, 32.1370>>)
			endif	
			LOAD_SCENE(<<2019.6141, 3827.6531, 32.1370>>)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_21_FBI2_MCS1_P1
		
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			if IsEntityAlive(player_ped_id())		
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1352.650,-2056.3,51.05 >>)
			endif	
			streamvol = STREAMVOL_CREATE_FRUSTUM(<< 1369.5962, -2077.9436, 52.6681 >>, << -4.0552, -0.0002, -28.7179 >>,250,FLAG_MAPDATA)
			while not STREAMVOL_HAS_LOADED(streamvol)
				wait(0)
			endwhile
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
				
		break
		case AS_VAULT_EXP
			
			SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(FALSE)
			//Interior
			intDepot = GET_INTERIOR_AT_COORDS_WITH_TYPE(vprolStart, "V_CashDepot")		
			PIN_INTERIOR_IN_MEMORY(intDepot)
				
			WHILE NOT IS_INTERIOR_READY(intDepot)
				WAIT(0)
			ENDWHILE
			//Cullbox
			SET_MAPDATACULLBOX_ENABLED("prologue", TRUE)
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(player_ped_id(),vprolStart)
			endif	
			load_scene(vprolStart)
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case AS_PRO_BLAST
			SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(FALSE)
			//Interior
			intDepot = GET_INTERIOR_AT_COORDS_WITH_TYPE(vprolStart, "V_CashDepot")		
			PIN_INTERIOR_IN_MEMORY(intDepot)
				
			WHILE NOT IS_INTERIOR_READY(intDepot)
				WAIT(0)
			ENDWHILE
			//Cullbox
			SET_MAPDATACULLBOX_ENABLED("prologue", TRUE)
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(player_ped_id(),vprolStart)
			endif
			load_scene(vprolStart)
			while not HAS_CUTSCENE_LOADED()				
				wait(0)
			endwhile
			while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,<<5312.26074, -5182.11768, 82.51863>>)
				wait(0)
			endwhile
			while not HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
				wait(0)
			endwhile
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_TORSO,15,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_LEG,8,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_feet,1,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_mike].id,PED_COMP_SPECIAL2,1,0)
			objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(peds[mpf_mike].id, WEAPONTYPE_CARBINERIFLE)
			while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_trev].id,CHAR_TREVOR,<<5312.26074, -5182.11768, 82.51863>>)
				wait(0)
			endwhile
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_TORSO,9,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_LEG,9,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_feet,12,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_HAND,4,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_HAIR,4,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_SPECIAL,6,0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_trev].id,PED_COMP_SPECIAL2,1,0)				
			objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(peds[mpf_trev].id, WEAPONTYPE_CARBINERIFLE)	
		break
		case AS_CAR_SLIDE
			
			while not HAS_MODEL_LOADED(IG_BALLASOG)
			or not has_model_loaded(EMPEROR)
			or not has_model_loaded(S_M_Y_AIRWORKER)
			or not HAS_ANIM_DICT_LOADED("trailer2@bonnet_slide")
				wait(0)
			endwhile
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<492.1,-529.1,24.7>>)
			endif
			load_scene(<<492.1,-529.1,24.7>>)
			
		break
		case AS_CHOP_ON_CAR
			
			while not HAS_MODEL_LOADED(GET_CHOP_MODEL())
			or not has_model_loaded(EMPEROR)
			or not has_model_loaded(S_M_Y_AIRWORKER)
			or not has_model_loaded(BISON2)
			or not HAS_ANIM_DICT_LOADED("MISSFRA0_CHOP")
				wait(0)
			endwhile
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<492.1,-529.1,24.7>>)
			endif	
			load_scene(<<492.1,-529.1,24.7>>)
			
		break
		case AS_FRANK_BIKE_EXP
			
			while not HAS_MODEL_LOADED(Police3)
			or not HAS_MODEL_LOADED(Police2)
			or not HAS_MODEL_LOADED(Police)
			or not HAS_MODEL_LOADED(Policet)
			or not HAS_MODEL_LOADED(sentinel2)
			or not HAS_MODEL_LOADED(stratum)
			or not HAS_MODEL_LOADED(emperor2)
			or not HAS_MODEL_LOADED(akuma)
			or not has_vehicle_recording_been_loaded(6,"Trailer2REC")
				wait(0)
			endwhile		
			streamvol = STREAMVOL_CREATE_FRUSTUM(<< 685.5129, 1242.1111, 359.1691 >>, << -10.9803, 3.5355, 178.3254 >>,200,FLAG_MAPDATA)		
			
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2473.8787, -221.0034, 16.5239>>)
			endif		
			load_scene(<<-2473.8787, -221.0034, 16.5239>>)
		
		break
		case AS_JET_FIGHT
			while not HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"Trailer2REC")
			or not HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"Trailer2REC")
			or not has_vehicle_recording_been_loaded(3,"Trailer2REC")
			or not has_vehicle_recording_been_loaded(4,"Trailer2REC")
			or not has_vehicle_recording_been_loaded(5,"Trailer2REC")
			or not HAS_MODEL_LOADED(polmav)
			or not has_model_loaded(S_M_Y_Pilot_01)
			or not has_model_loaded(Lazer)
			or not HAS_MODEL_LOADED(ADDER)
				wait(0)
			endwhile	
			fLODscale = 1.6
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
		break
		case STAGE_22_FIN_EXT_P2
			
			while not HAS_MODEL_LOADED(BUZZARD)
				wait(0)
			endwhile
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			if IsEntityAlive(player_ped_id())	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1352.650,-2056.3,51.05>>)
			endif	
			LOAD_SCENE(<<1352.650,-2056.3,51.05>>)			
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
			
		break
		case STAGE_23_FINAL_SHOT		
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
		break
		case STAGE_23_FINAL_SHOT_ALT		
			while not HAS_CUTSCENE_LOADED()
				wait(0)
			endwhile
		break
	endswitch
endproc


PROC MISSION_SETUP()

	//zMenuNames[ENUM_TO_INT(STAGE_0_WAIT_FOR_STAGE)].bSelectable
	zMenuNames[ENUM_TO_INT(STAGE_0_WAIT_FOR_STAGE)].sTxtLabel 		= "Select Shot:"
	zMenuNames[ENUM_TO_INT(STAGE_0_WAIT_FOR_STAGE)].bSelectable 	= FALSE
	
	zMenuNames[ENUM_TO_INT(STAGE_1_RUN_THROUGH_SHOTS)].sTxtLabel 	= "RUN THROUGH ALL SHOTS"
	
	zMenuNames[ENUM_TO_INT(STAGE_2_OBSERVATORY_SHOT)].sTxtLabel 	= "Observatory opening scene"
	zMenuNames[ENUM_TO_INT(STAGE_2_OBSERVATORY_SHOT_alt)].sTxtLabel = "Observatory opening scene ALT"
	zMenuNames[ENUM_TO_INT(STAGE_3_FAM2_INT_P1)].sTxtLabel 			= "Tennis shot"
	zMenuNames[ENUM_TO_INT(STAGE_4_DRIVEWAY_SHOT)].sTxtLabel 		= "Driveway"
	zMenuNames[ENUM_TO_INT(STAGE_5_FAM1_INT_P1)].sTxtLabel 			= "Mike's Family/ sunbathing scene"
	zMenuNames[ENUM_TO_INT(STAGE_6_CITY_TRAIN_SHOT)].sTxtLabel 		= "Train and Highway shot of city"
	zMenuNames[ENUM_TO_INT(STAGE_7_COUNTRY_SCAPE_SHOT)].sTxtLabel 	= "Country road/radars shot"
	zMenuNames[ENUM_TO_INT(STAGE_8_LIQUOR_ACE_SHOT)].sTxtLabel 		= "Liquor ace shop shot"
	zMenuNames[ENUM_TO_INT(STAGE_9_TREV1_MCS3_P2_A)].sTxtLabel 		= "Trev shouting at man in water"
	zMenuNames[ENUM_TO_INT(STAGE_10_CHIN1_INT_P2)].sTxtLabel 		= "bar fight"
	zMenuNames[ENUM_TO_INT(STAGE_11_TREV1_MCS3_P2_B)].sTxtLabel 	= "trevor close up"	
	zMenuNames[ENUM_TO_INT(AS_BUGGY)].sTxtLabel 					= " ACTION SHOT: buggy"
	zMenuNames[ENUM_TO_INT(AS_TREV_MOL)].sTxtLabel 					= " ACTION SHOT: Petrol Bomb"
	zMenuNames[ENUM_TO_INT(AS_FIRE_TRAIL)].sTxtLabel 				= " ACTION SHOT: Fire trail"
	zMenuNames[ENUM_TO_INT(AS_BARN_BOOM)].sTxtLabel 				= " ACTION SHOT: barn blow up"	
//	zMenuNames[ENUM_TO_INT(AS_BARN_BOOM)].bSelectable				= false
	zMenuNames[ENUM_TO_INT(STAGE_12_CHIN1_MCS4)].sTxtLabel 			= "Trevor wave infront of bodies"
	zMenuNames[ENUM_TO_INT(STAGE_13_DOCKS_BRIDGE_SHOT)].sTxtLabel 	= "Docks bridge Shot"
	zMenuNames[ENUM_TO_INT(STAGE_13_DOCKS_BRIDGE_SHOT_ALT)].sTxtLabel = "Docks bridge Shot ALT"
	zMenuNames[ENUM_TO_INT(STAGE_14_GHETTO_TRAIN_SHOT)].sTxtLabel 	= "Train in ghetto shot"
	zMenuNames[ENUM_TO_INT(STAGE_15_ARM1_INT_P3)].sTxtLabel 		= "franklin, lamar and michael meet"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_ARM1)].sTxtLabel 				= " MISSION: Arm 1 - steal car"
	zMenuNames[ENUM_TO_INT(STAGE_16_FRA2_INT_P2)].sTxtLabel 		= "Franklin and his Girlfriend"	
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_CAR_STEAL_FINALE)].sTxtLabel 	= "MISSION: car steal Finale"
	zMenuNames[ENUM_TO_INT(STAGE_17_ARM1_INT_P4)].sTxtLabel 		= "Franklin and Lamar"
	zMenuNames[ENUM_TO_INT(STAGE_17_ARM1_INT_P4_ALT)].sTxtLabel 	= "Franklin and Lamar- ALT"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_CAR_STEAL_3)].sTxtLabel 		= "MISSION: car steal 3- race"
	zMenuNames[ENUM_TO_INT(STAGE_18_ARM1_INT_P1)].sTxtLabel	 		= "Mike at shrink's"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_FAMILY_1)].sTxtLabel 			= " MISSION: Family 1"
	zMenuNames[ENUM_TO_INT(STAGE_19_FAM5_MCS5)].sTxtLabel 			= "Jimmy lets bounce"
	zMenuNames[ENUM_TO_INT(STAGE_20_TREV1_INT_P4)].sTxtLabel 		= "Trevor stomps johnny"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_TREV2)].sTxtLabel 				= " MISSION: Trev 2"
	zMenuNames[ENUM_TO_INT(AS_VAULT_EXP)].sTxtLabel					= " ACTION SHOT: Vault door "
//	zMenuNames[ENUM_TO_INT(AS_VAULT_EXP)].bSelectable				= false
	zMenuNames[ENUM_TO_INT(AS_PRO_BLAST)].sTxtLabel 				= " ACTION SHOT: Prologue Blast doors"
//	zMenuNames[ENUM_TO_INT(AS_PRO_BLAST)].bSelectable 				= false
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_FRANK_1)].sTxtLabel 			= " MISSION: frank 1"
	zMenuNames[ENUM_TO_INT(AS_CAR_SLIDE)].sTxtLabel					= " ACTION SHOT: car slide "
	zMenuNames[ENUM_TO_INT(AS_CHOP_ON_CAR)].sTxtLabel				= " ACTION SHOT: car chop "
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_FBI_2)].sTxtLabel 				= " MISSION: FBI 2"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_JEWEL_2A)].sTxtLabel 			= " MISSION: jewel heist 2a"
	zMenuNames[ENUM_TO_INT(AS_FRANK_BIKE_EXP)].sTxtLabel			= " ACTION SHOT: Bike explosion "
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_EXILE_1)].sTxtLabel 			= " MISSION: exile 1"
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_OFFROAD_RACE_1)].sTxtLabel 	= " MISSION: offroad race"
//	zMenuNames[ENUM_TO_INT(AS_LAUNCH_OFFROAD_RACE_1)].bSelectable	= false
	zMenuNames[ENUM_TO_INT(AS_LAUNCH_EXILE_3)].sTxtLabel 			= " MISSION: Exile 3"
	zMenuNames[ENUM_TO_INT(STAGE_21_FBI2_MCS1_P1)].sTxtLabel 		= "Trevor Meet franklin"	
	zMenuNames[ENUM_TO_INT(STAGE_22_FIN_EXT_P2)].sTxtLabel 			= "therapist has a lot to answer for"
	zMenuNames[ENUM_TO_INT(AS_JET_FIGHT)].sTxtLabel					= " ACTION SHOT: Jet fight "
	zMenuNames[ENUM_TO_INT(STAGE_23_FINAL_SHOT)].sTxtLabel 			= "Final shot"
	zMenuNames[ENUM_TO_INT(STAGE_23_FINAL_SHOT_ALT)].sTxtLabel 		= "Final shot Alt"
	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_2_OBSERVATORY_SHOT)] 	= 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_2_OBSERVATORY_SHOT_alt)] = 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_3_FAM2_INT_P1)]			= 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_4_DRIVEWAY_SHOT)]		= 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_5_FAM1_INT_P1)]			= 22
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_6_CITY_TRAIN_SHOT)]		= 14
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_7_COUNTRY_SCAPE_SHOT)] 	= 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_8_LIQUOR_ACE_SHOT)]		= 15
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_9_TREV1_MCS3_P2_A)]		= 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_10_CHIN1_INT_P2)]		= 14
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_11_TREV1_MCS3_P2_B)]		= 19//17	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_12_CHIN1_MCS4)]			= 13
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_13_DOCKS_BRIDGE_SHOT)]	= 22
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_13_DOCKS_BRIDGE_SHOT_ALT)]= 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_14_GHETTO_TRAIN_SHOT)]	= 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_15_ARM1_INT_P3)]			= 7//13
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_16_FRA2_INT_P2)]			= 7	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_17_ARM1_INT_P4)]			= 13
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_17_ARM1_INT_P4_ALT)]		= 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_18_ARM1_INT_P1)]			= 17
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_19_FAM5_MCS5)]			= 22
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_20_TREV1_INT_P4)]		= 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_21_FBI2_MCS1_P1)]		= 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_22_FIN_EXT_P2)]			= 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_23_FINAL_SHOT)]			= 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_23_FINAL_SHOT_ALT)]		= 19
	
	InitialTimeOfDayHour[ENUM_TO_INT(AS_BUGGY)]				= 19
	InitialTimeOfDayHour[ENUM_TO_INT(AS_TREV_MOL)]			= 7
	InitialTimeOfDayHour[ENUM_TO_INT(AS_BARN_BOOM)]			= 6
	InitialTimeOfDayHour[ENUM_TO_INT(AS_FIRE_TRAIL)]		= 6
	InitialTimeOfDayHour[ENUM_TO_INT(AS_PRO_BLAST)]			= 0
	InitialTimeOfDayHour[ENUM_TO_INT(AS_VAULT_EXP)]			= 0
	InitialTimeOfDayHour[ENUM_TO_INT(AS_CAR_SLIDE)]			= 6
	InitialTimeOfDayHour[ENUM_TO_INT(AS_CHOP_ON_CAR)]		= 16
	InitialTimeOfDayHour[ENUM_TO_INT(AS_FRANK_BIKE_EXP)]	= 5
	InitialTimeOfDayHour[ENUM_TO_INT(AS_JET_FIGHT)]			= 22
	//Init Widgets
	paradiseWidgetGroup = START_WIDGET_GROUP("Paradise Trailer 2")
		
		ADD_WIDGET_BOOL("Reset scene Automatically",bresetScene)
		//widescreen 
		ADD_WIDGET_BOOL("set widescreen borders", bWideScreen)
		//plaer
		add_widget_bool("player control:", bplayerControl)
		//display titles		
		ADD_WIDGET_BOOL("bDisplayTitles", bDisplayTitles)		
		//DOF
		ADD_WIDGET_BOOL("Use High Quality Depth of Field", bHighDofOn)	
		//LODSCALE
		add_widget_float_slider("LOD scale",fLODscale,1,3,0.1)
		
		START_WIDGET_GROUP("timings debug")		
			add_widget_float_slider("debug speed", fDebugSpeed,0,100,0.5)		
			ADD_WIDGET_INT_SLIDER("Skip playback time:",idebugSkipTime1,0,20000,100)
			ADD_WIDGET_INT_SLIDER("Skip playback time2:",idebugSkipTime2,0,20000,100)
			ADD_WIDGET_INT_SLIDER("Skip playback time3:",idebugSkipTime3,0,20000,100)
			ADD_WIDGET_INT_SLIDER("Skip playback time4:",idebugSkipTime4,0,20000,100)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("train set up")
			ADD_WIDGET_INT_SLIDER("iTrainConfiguration", iTrainConfiguration, 0, 100, 1)
			add_widget_float_slider("Train Speed",ftrainSpeed,0,50,1)
		STOP_WIDGET_GROUP()
				
		START_WIDGET_GROUP("debug position")		
			ADD_WIDGET_VECTOR_SLIDER("vDebugPosition", vDebugPosition, -6000.0, 6000.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("vDebugRotation",vDebugRotation, -6000.0, 6000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fDebugheading", fDebugheading, 0.0, 360.0, 0.05)
		STOP_WIDGET_GROUP()
				
		START_WIDGET_GROUP("Cam output")
			START_WIDGET_GROUP("scene cam")
				ADD_WIDGET_VECTOR_SLIDER("Scene cam pos:",vCamPOS,-6000,6000,0.001)
				ADD_WIDGET_VECTOR_SLIDER("Scene cam ROT:",vCamROT,-6000,6000,0.001)
				ADD_WIDGET_FLOAT_READ_ONLY("current cam FOV",fCamFOV) 
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("debug cam")
				ADD_WIDGET_VECTOR_SLIDER("Debug cam pos:",vdebugcamPOS,-6000,6000,0.001)
				ADD_WIDGET_VECTOR_SLIDER("Debug cam ROT:",vdebugCamROT,-6000,6000,0.001)
				ADD_WIDGET_FLOAT_READ_ONLY("Debug cam FOV",fdebugcamFOV)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TOD")
			ADD_WIDGET_BOOL("Pause Time on reset",bPauseClock)
			ADD_WIDGET_BOOL("Reset TOD",bResetTOD)				
			ADD_WIDGET_BOOL("Use new TOD for Scene",bUseNewTOD)
			ADD_WIDGET_INT_SLIDER("New TOD: ", iTOD,0,23,1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Shadows")
			ADD_WIDGET_BOOL("Reset shadows", bResetShadows)
		stop_widget_group()
				
		START_WIDGET_GROUP("Peds/Traffic")
			ADD_WIDGET_FLOAT_SLIDER("ped density",fTrafficDensity,0,3,0.1)
			ADD_WIDGET_FLOAT_SLIDER("traffic density",fPedDensity,0,3,0.1)
			ADD_WIDGET_BOOL("Clear area (500m)", bClearArea)			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("water")
			ADD_WIDGET_FLOAT_SLIDER("Water Reflection",fwatereflection,0,50,0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Slide car scene's driver ped")
			ADD_WIDGET_INT_SLIDER("head ID",iDockWorkerHeadID,-1,2,1)
			ADD_WIDGET_INT_SLIDER("Head Text",iDockWorkerHeadText,-1,2,1)
			ADD_WIDGET_INT_SLIDER("torso ID",iDockWorkerUpperID,-1,2,1)
			ADD_WIDGET_INT_SLIDER("torso text",idockworkerUpperText,-1,2,1)
			ADD_WIDGET_INT_SLIDER("Lower ID",idockworkerlowID,-1,2,1)
			ADD_WIDGET_INT_SLIDER("Lower Text",idockworkerlowText,-1,2,1)
			ADD_WIDGET_INT_SLIDER("headprop id",iDockPropID,-1,2,1)
			ADD_WIDGET_INT_SLIDER("headprop text",idockpropText,-1,2,1)
		STOP_WIDGET_GROUP()
	
	STOP_WIDGET_GROUP()			
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	REQUEST_PTFX_ASSET()
	LOAD_CLOUD_HAT("horizon")
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	DISABLE_CELLPHONE(TRUE)
	
	bDrawMenu = TRUE
	
	mission_stage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
	Mission_Set_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
	mission_substage = STAGE_ENTRY
ENDPROC
// ===========================================================================================================
//		Shot procedures
// ===========================================================================================================

PROC ST_0_WAIT_FOR_STAGE()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			
			//DESTROY_ALL_CAMS()
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<482.3925, -807.1235, 130.9988>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cameraIndex, <<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			float z
			GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(player_ped_id()),z)
			SET_ENTITY_COORDS(player_ped_id(),<<482.3925, -807.1235,z>>)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			
			SETTIMERB(0)
			bDrawMenu = true
			mission_substage++			
		BREAK
		CASE 1
			IF TIMERB() > 100000
				mission_substage++	
			ENDIF
		BREAK
		
		CASE 2
			//DESTROY_ALL_CAMS()
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cameraIndex, <<482.3925, -807.1235, 130.9988>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			mission_substage++
		BREAK

		CASE 3
			IF TIMERB() > 100000
				mission_substage = 99
			ENDIF
		BREAK
		
		CASE 99			
			SET_CAM_PARAMS(cameraIndex, <<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SETTIMERB(0)
			mission_substage = 1
		BREAK		
	ENDSWITCH
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.03, "STRING","Press 'P' to Toggle scene reset")
ENDPROC
PROC ST_2_OBSERVATORY_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-508.391724,1114.447021,324.216095,15.607548)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-472.516602,1093.753418,324.216095,44.896576)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-519.602112,510.347992,249.415100,449.954987)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-445.712891,621.937988,239.018799,540.000000)

			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -484.5379, 1121.4351, 346.8401 >>, << -18.7962, -0.0000, -160.6597 >>, 56.8, TRUE)
			SET_CAM_PARAMS(cameraIndex, << -485.7652, 1120.8416, 347.2903 >>, << -18.7962, -0.0000, -160.6597 >>, 56.8000, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
				peds[mpf_driver].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEVHILLS_02,<<-466.37637, 1089.31213, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_driver].id,"WORLD_HUMAN_TOURIST_MOBILE",<<-466.37637, 1089.31213, 326.68176>>, 165.0717)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_oldman].id)
				peds[mpf_oldman].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BUSINESS_01,<<-462.71686, 1092.16846, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_oldman].id,"WORLD_HUMAN_HANG_OUT_STREET",<<-462.71686, 1092.16846, 326.68176>>,36.4846)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_phone].id)
				peds[mpf_phone].id = CREATE_PED(PEDTYPE_MISSION,A_F_Y_TOURIST_01,<<-463.09814, 1093.39160, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_phone].id,"WORLD_HUMAN_TOURIST_MAP",<<-463.09814, 1093.39160, 326.68176>>, 153.5232)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
				peds[mpf_pilot1].id = CREATE_PED(PEDTYPE_MISSION,A_F_M_BEVHILLS_02,<<-471.39084, 1104.63867, 326.25180>>)
//				TASK_START_SCENARIO_AT_POSITION(peds[mpf_pilot1].id,"WORLD_HUMAN_SEAT_WALL_EATING",<<-467.94592, 1091.92004, 327.68127>>,165)
				TASK_GO_STRAIGHT_TO_COORD(peds[mpf_pilot1].id,<<-456.53128, 1089.58118, 326.68176>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)				
			endif
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if TIMERA() > 2500		
			 	if not bDrawMenu 
				and bresetScene
					if bPlayThrough
						DO_SCREEN_FADE_OUT(0)
						Load_Stage_Assets(STAGE_3_FAM2_INT_P1)
						set_up_stage(STAGE_3_FAM2_INT_P1)
						Mission_Set_Stage(STAGE_3_FAM2_INT_P1)
						mission_substage = STAGE_ENTRY
					else	
						if IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_OUT(0)
						endif						
						if TIMERA() > 3500	
							if IsEntityAlive(peds[mpf_pilot1].id)
								delete_ped(peds[mpf_pilot1].id)
							endif
							mission_substage = STAGE_ENTRY
						endif
					endif
				endif
			endif
		break
	endswitch
	
endproc
PROC ST_2_OBSERVATORY_SHOT_ALT()
	switch mission_substage
		case STAGE_ENTRY
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-472.516602,1111.196167,324.216095,17.076283)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-472.516602,1093.753418,324.216095,44.896576)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-517.516052,901.473328,249.415375,150.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-451.142700,700.000000,239.018799,425.939728)

			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -484.5379, 1121.4351, 346.8401 >>, << -18.7962, -0.0000, -160.6597 >>, 56.8, TRUE)
			SET_CAM_PARAMS(cameraIndex, << -485.7652, 1120.8416, 347.2903 >>, << -18.7962, -0.0000, -160.6597 >>, 56.8000, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)			
			if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
				peds[mpf_driver].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEVHILLS_02,<<-466.37637, 1089.31213, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_driver].id,"WORLD_HUMAN_TOURIST_MOBILE",<<-466.37637, 1089.31213, 326.68176>>, 165.0717)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_oldman].id)
				peds[mpf_oldman].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BUSINESS_01,<<-462.71686, 1092.16846, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_oldman].id,"WORLD_HUMAN_HANG_OUT_STREET",<<-462.71686, 1092.16846, 326.68176>>,36.4846)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_phone].id)
				peds[mpf_phone].id = CREATE_PED(PEDTYPE_MISSION,A_F_Y_TOURIST_01,<<-463.09814, 1093.39160, 326.68176>>)
				TASK_START_SCENARIO_AT_POSITION(peds[mpf_phone].id,"WORLD_HUMAN_TOURIST_MAP",<<-463.09814, 1093.39160, 326.68176>>, 153.5232)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
				peds[mpf_pilot1].id = CREATE_PED(PEDTYPE_MISSION,A_F_M_BEVHILLS_02,<<-471.39084, 1104.63867, 326.25180>>)
//				TASK_START_SCENARIO_AT_POSITION(peds[mpf_pilot1].id,"WORLD_HUMAN_SEAT_WALL_EATING",<<-467.94592, 1091.92004, 327.68127>>,165)
				TASK_GO_STRAIGHT_TO_COORD(peds[mpf_pilot1].id,<<-456.53128, 1089.58118, 326.68176>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)				
			endif
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if TIMERA() > 2500		
			 	if not bDrawMenu
				and bresetScene
					if bPlayThrough
						DO_SCREEN_FADE_OUT(0)
						Load_Stage_Assets(STAGE_3_FAM2_INT_P1)
						set_up_stage(STAGE_3_FAM2_INT_P1)
						Mission_Set_Stage(STAGE_3_FAM2_INT_P1)
						mission_substage = STAGE_ENTRY
					else	
						if IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_OUT(0)
						endif						
						if TIMERA() > 3500	
							if IsEntityAlive(peds[mpf_pilot1].id)
								delete_ped(peds[mpf_pilot1].id)
							endif
							mission_substage = STAGE_ENTRY							
						endif
					endif
				endif
			endif
		break
	endswitch
	
Endproc
proc ST_3_FAM2_INT_P1() //tennis
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-773.452087,164.090302,67.376785,6.941813)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-773.452087,156.502289,67.376793,9.199070)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-818.972778,160.218796,76.246338,27.522293)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-800.428589,228.689377,79.433769,93.751991)
				START_CUTSCENE()				
				//camera
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -761.6831, 127.7389, 82.0954 >>, << -10.5571, -0.0000, 32.2714 >>, 44, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SETTIMERA(0)
				if not DOES_ENTITY_EXIST(peds[mpf_oldman].id)
					peds[mpf_oldman].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_GARDENER_01,<<-806.80261, 152.18236, 67.88828>>)										
					TASK_START_SCENARIO_AT_POSITION(peds[mpf_oldman].id, "WORLD_HUMAN_GARDENER_LEAF_BLOWER",<<-804.18408, 146.86296, 64.21335>>,217.2437)
					SET_ENTITY_LOD_DIST(peds[mpf_oldman].id,1000)
				endif
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 0//1500
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				SET_CAM_PARAMS(cameraIndex,<< -761.6831, 127.7389, 82.0954 >>, << -13.3528, 0.0000, 31.2356 >>, 44, 4500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 4500				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif			
		break
		case 3
			if HAS_CUTSCENE_FINISHED()
				if not bDrawMenu 
				and bresetScene
				DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_4_DRIVEWAY_SHOT)
						set_up_stage(STAGE_4_DRIVEWAY_SHOT)
						Mission_Set_Stage(STAGE_4_DRIVEWAY_SHOT)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_3_FAM2_INT_P1)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc ST_4_DRIVEWAY_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -823.6382, 153.6339, 69.3399 >>, << 10.9499, -0.0000, -39.4098 >>, 39.5356, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			fLODscale = 1.4
			//create car
			if not DOES_ENTITY_EXIST(vehs[veh_Driveway].id)
				vehs[veh_Driveway].id = CREATE_VEHICLE(NINEF2,vdrivewaypos,fdrivewayheading)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_Driveway].id,4)
				LOWER_CONVERTIBLE_ROOF(vehs[veh_Driveway].id,true)				
				SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_Driveway].id)
			endif
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if timera() > 1000				
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				SET_CAM_PARAMS(cameraIndex,<< -823.3452, 153.3983, 69.4799 >>, << 13.5878, 0.0000, -35.0619 >>, 39.5356, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 2
			if timera() > 4000
				if not bDrawMenu 
				and bresetScene
					if bPlayThrough	
						Load_Stage_Assets(STAGE_5_FAM1_INT_P1)
						set_up_stage(STAGE_5_FAM1_INT_P1)
						Mission_Set_Stage(STAGE_5_FAM1_INT_P1)
						mission_substage = STAGE_ENTRY
					else			
						if DOES_ENTITY_EXIST(vehs[veh_Driveway].id)
							DELETE_VEHICLE(vehs[veh_Driveway].id)
						endif
						DO_SCREEN_FADE_OUT(0)
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
//	if DOES_ENTITY_EXIST(vehs[veh_parked_car].id)
//		if not IS_ENTITY_DEAD(vehs[veh_parked_car].id)
//			SET_ENTITY_COORDS(vehs[veh_parked_car].id,vDebugPosition)
//			set_entity_heading(vehs[veh_parked_car].id,fDebugHeading)
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_parked_car].id)
//		endif	
//	endif
	
endproc
proc ST_5_FAM1_INT_P1()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
						WAIT(0)
			 		ENDWHILE	
				endif
				START_CUTSCENE()
				if IsEntityAlive(peds[mpf_mike].id)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_mike].id, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael",peds[mpf_mike].id)
				endif
				streamvol = STREAMVOL_CREATE_FRUSTUM(<< -769.5152, 161.4985, 84.0688 >>, << -20.3405, 0.9996, 32.1397 >>,100,FLAG_MAPDATA)
				//camera
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -797.2319, 186.4233, 72.9825 >>, << -3.5417, 0.9998, 102.7818 >>, 30.0174, TRUE)			
				SET_CAM_PARAMS(cameraIndex,<< -797.2648, 186.3080, 72.9884 >>, << -3.8154, 0.9998, 103.4982 >>,30,6000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SETTIMERA(0)	
				
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1900
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 5620				
				SET_CLOCK_TIME(13,0,0)
				SET_CAM_FAR_DOF(cameraIndex,270)				
				SET_CAM_NEAR_DOF(cameraIndex,5)
				SET_CAM_USE_SHALLOW_DOF_MODE(cameraIndex,true)	
				bHighDofOn = true
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 10033
				SET_CAM_PARAMS(cameraIndex,<< -778.3713, 187.8110, 72.5213 >>, << 2.4206, 1.0003, 99.5010 >>,16.4626,0)	
				SET_CAM_PARAMS(cameraIndex,<< -778.3713, 187.8110, 72.5213 >>, << 1.8857, 1.0001, 97.7170 >>,16.4626,9267,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)	
				mission_substage++
			endif
		break
		case 4
			if GET_CUTSCENE_TIME() >= 19300
				mission_substage++
			endif
		break
		case 5
			if GET_CUTSCENE_TIME() >= 38000
			or HAS_CUTSCENE_FINISHED()
				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif			
		break
		case 6
			if HAS_CUTSCENE_FINISHED()
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_6_CITY_TRAIN_SHOT)
						set_up_stage(STAGE_6_CITY_TRAIN_SHOT)
						Mission_Set_Stage(STAGE_6_CITY_TRAIN_SHOT)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_5_FAM1_INT_P1)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()
		if GET_CUTSCENE_TIME() >= 5666 and GET_CUTSCENE_TIME() < 10033 // clip 2
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-780.490601,187.876251,72.439651,3.258497)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-780.488770,187.767242,72.427795,5.257813)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-784.134216,187.964050,72.289421,6.553432)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
		elif GET_CUTSCENE_TIME() >= 10033 and GET_CUTSCENE_TIME() < 19300 // clip 3
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-786.769165,186.255463,72.794601,1.157311)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-784.134216,187.964920,72.288277,2.632771)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,0.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-787.414124,183.238968,73.160629,27.808306)
			SET_CLOCK_TIME(13,0,0)
			SET_WEATHER_TYPE_NOW_PERSIST("smog")
		elif GET_CUTSCENE_TIME() >= 19300	and GET_CUTSCENE_TIME() < 20900 // clip 6
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-780.479126,187.773575,72.423218,1.204188)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-780.488770,187.767242,72.427795,2.411242)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,0.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
			SET_CLOCK_TIME(16,0,0)
			SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")
		elif GET_CUTSCENE_TIME() >= 20900	and GET_CUTSCENE_TIME() < 29733 // clip 7
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-780.479126,187.773575,72.423218,0.469813)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-780.488770,187.767242,72.427795,2.051867)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,0.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
		elif GET_CUTSCENE_TIME() >= 29733	and GET_CUTSCENE_TIME() < 35900 // clip 8
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-780.479126,187.773575,72.423218,2.360440)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-780.488770,187.767242,72.427795,3.629992)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-784.134216,187.964050,72.289421,5.897182)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-780.464478,189.933487,72.388496,8.758831)
		elif GET_CUTSCENE_TIME() >= 35900	 								//clip 9 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-780.248596,188.432999,72.290451,0.635394)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-780.488770,187.767242,72.427795,3.629992)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-784.134216,187.964050,72.289421,5.897182)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-780.464478,189.933487,72.388496,8.758831)
		endif
	endif
	if mission_substage = 4	
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()		
	endif
	
endproc
proc ST_6_CITY_TRAIN_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()	
				DO_SCREEN_FADE_IN(0)
			endif
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,780.744873,-486.837524,29.516705,10.489002)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,796.368347,-480.491577,29.973850,13.402205)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,777.991943,-498.517029,35.446419,30.895958)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,622.286011,-561.551636,35.447056,179.981995)

			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 833.7414, -481.7881, 39.1400 >>, << -4.6010, 0.0000, 97.3758 >>, 35.9, TRUE)
			SET_CAM_PARAMS(cameraIndex,<< 833.7414, -481.7881, 39.1400 >>, << 5.2272, 0.0000, 103.9583 >>, 35.9, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			//create train			
			vehs[veh_first_train].id = CREATE_MISSION_TRAIN(3,vTrainCityPos, TRUE)
			SET_TRAIN_CRUISE_SPEED(vehs[veh_first_train].id, ftrainSpeed)
			SET_TRAIN_SPEED(vehs[veh_first_train].id, ftrainSpeed)
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if timera() > 5000				
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_7_COUNTRY_SCAPE_SHOT)
						set_up_stage(STAGE_7_COUNTRY_SCAPE_SHOT)
						Mission_Set_Stage(STAGE_7_COUNTRY_SCAPE_SHOT)
						mission_substage = STAGE_ENTRY
					else			
						DELETE_MISSION_TRAIN(vehs[veh_first_train].id)
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch	
endproc
proc ST_7_COUNTRY_SCAPE_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()	
				DO_SCREEN_FADE_IN(0)
			endif
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 2557.3301, 3037.0537, 44.0020 >>, << 5.1213, -0.0000, 125.3085 >>, 39.4673, TRUE)
			
			SET_CAM_PARAMS(cameraIndex,<< 2557.3301, 3037.0537, 44.0020 >>, << 5.1213, 0.0000, 123.3813 >>, 39.4673, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			//create truck	
			CLEAR_AREA(<<2545.50488, 3037.98584, 42.32346>>,7,true)
			vehs[veh_truck].id = CREATE_VEHICLE(packer,<<2545.50488, 3037.98584, 42.32346>>,135.13)
			SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_truck].id,1)
			vehs[veh_exp1].id = create_vehicle(TRAILERS,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[veh_truck].id, <<0.0, 3.5, 0.0>>), GET_ENTITY_HEADING(vehs[veh_truck].id))
			ATTACH_VEHICLE_TO_TRAILER(vehs[veh_truck].id,vehs[veh_exp1].id)
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if timera() > 2500
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_8_LIQUOR_ACE_SHOT)
						set_up_stage(STAGE_8_LIQUOR_ACE_SHOT)
						Mission_Set_Stage(STAGE_8_LIQUOR_ACE_SHOT)
						mission_substage = STAGE_ENTRY
					else		
						if IsEntityAlive(vehs[veh_truck].id)
						and IsEntityAlive(vehs[veh_exp1].id)
							DETACH_VEHICLE_FROM_TRAILER(vehs[veh_truck].id)
							DELETE_VEHICLE(vehs[veh_truck].id)	
							DELETE_VEHICLE(vehs[veh_exp1].id)
						endif
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
	if DOES_ENTITY_EXIST(vehs[veh_truck].id)
		if not IS_ENTITY_DEAD(vehs[veh_truck].id)
			SET_VEHICLE_FORWARD_SPEED(vehs[veh_truck].id,15)
		endif	
	endif
endproc
proc ST_8_LIQUOR_ACE_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 1989.5298, 3074.7014, 47.3174 >>, << -1.8617, -0.0000, -172.5692 >>,35.8189, TRUE)
			//women
			if not DOES_ENTITY_EXIST(peds[mpf_oldman].id)
				peds[mpf_oldman].id = create_ped(PEDTYPE_MISSION,A_F_M_Salton_01,<< 1999.8894, 3050.8081, 46.2142 >>, 33.0468)
				SET_PED_COMPONENT_VARIATION(peds[mpf_oldman].id,PED_COMP_TORSO,0,0)
				SET_PED_COMPONENT_VARIATION(peds[mpf_oldman].id,PED_COMP_LEG,1,2)
				SET_PED_COMPONENT_VARIATION(peds[mpf_oldman].id,PED_COMP_HAIR,0,0)
				TASK_PLAY_ANIM(peds[mpf_oldman].id,"amb@world_human_leaning@female@wall@back@holding_elbow@idle_a","idle_b",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
			endif
			//fat man
			if not DOES_ENTITY_EXIST(peds[mpf_fatmancross].id)
				peds[mpf_fatmancross].id = create_ped(PEDTYPE_MISSION,A_M_M_Salton_01,<< 1993.6366, 3057.4487, 46.0563 >>, 272.6865)						
				SET_PED_COMPONENT_VARIATION(peds[mpf_fatmancross].id,PED_COMP_TORSO,2,1)
				SET_PED_COMPONENT_VARIATION(peds[mpf_fatmancross].id,PED_COMP_LEG,0,1)
				SET_PED_COMPONENT_VARIATION(peds[mpf_fatmancross].id,PED_COMP_HAIR,1,0)
				TASK_PLAY_ANIM(peds[mpf_fatmancross].id,"move_m@generic_variations@walk","walk_e",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
			endif
			mission_substage++
		break
		case 1		
			if IS_SCREEN_FADED_OUT()	
				DO_SCREEN_FADE_IN(0)
			endif
	//cam
			SET_CAM_PARAMS(cameraIndex,<< 1988.9154, 3074.6213, 47.3186 >>, << 1.5679, 0.0003, -170.9200 >> ,35.8189, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.6)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1987.481812,3064.853027,46.956909,3.447119)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1990.990845,3071.017090,46.504059,1.589828)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1993.078857,3055.733887,48.113201,20.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,2015.878174,3000.344971,45.083389,87.052994)
	//ambiant peds / vehs
			if not DOES_ENTITY_EXIST(vehs[veh_parked_car].id)
				vehs[veh_parked_car].id = CREATE_VEHICLE(RATLOADER,vparkcarpos,fparkcarheading)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_parked_car].id,0)			
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_tornado3].id)
				vehs[veh_tornado3].id = CREATE_VEHICLE(tornado3,vtornado3,ftornado3)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_tornado3].id,0)	
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
				peds[mpf_driver].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_tornado3].id,PEDTYPE_MISSION,S_M_Y_XMech_02)
				SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_TORSO,0,0)
				SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_LEG,0,0)
				SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_LEG,0,0)
				SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_HAIR,0,0)
			endif
			//man drunk
			if not DOES_ENTITY_EXIST(peds[mpf_phone].id)
				peds[mpf_phone].id = create_ped(PEDTYPE_MISSION,A_M_M_HILLBILLY_02,<<1991.39758, 3067.16772, 46.02674>>,63)						
				SET_PED_COMPONENT_VARIATION(peds[mpf_phone].id,PED_COMP_TORSO,0,2)
				SET_PED_COMPONENT_VARIATION(peds[mpf_phone].id,PED_COMP_LEG,0,2)
				SET_PED_COMPONENT_VARIATION(peds[mpf_phone].id,PED_COMP_HAIR,1,0)
			endif			
				OPEN_SEQUENCE_TASK(seq)	
					TASK_PLAY_ANIM(null,"move_drunk_m","walk",INSTANT_BLEND_IN,SLOW_BLEND_OUT,-1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(peds[mpf_phone].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_MIN_MOVE_BLEND_RATIO(peds[mpf_phone].id,PEDMOVEBLENDRATIO_WALK)
				FORCE_PED_MOTION_STATE(peds[mpf_phone].id,MS_ON_FOOT_WALK,false,FAUS_CUTSCENE_EXIT)
			//fighter 1 
			if not DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
				peds[mpf_pilot1].id = create_ped(PEDTYPE_MISSION,A_M_M_HILLBILLY_01,<< 1991.02856, 3060.32104, 46.05356 >>,  66.1944 )
			
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot1].id,PED_COMP_TORSO,0,2)
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot1].id,PED_COMP_LEG,0,2)				
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot1].id,PED_COMP_HAIR,1,2)
			endif
			//fighter 2
			if not DOES_ENTITY_EXIST(peds[mpf_pilot2].id)
				peds[mpf_pilot2].id = create_ped(PEDTYPE_MISSION,A_M_M_HILLBILLY_01,<< 1989.69739, 3060.97339, 46.05430 >>, 244.8239)				
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot2].id,PED_COMP_TORSO,1,2)
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot2].id,PED_COMP_LEG,1,0)				
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot2].id,PED_COMP_HAIR,0,2)
			endif
						
//			TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_fatmancross].id,<< 2000.8536, 3057.7395, 46.0492 >>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
//			SET_PED_MIN_MOVE_BLEND_RATIO(peds[mpf_fatmancross].id,PEDMOVEBLENDRATIO_WALK)
//			FORCE_PED_MOTION_STATE(peds[mpf_fatmancross].id,MS_ON_FOOT_WALK,false,FAUS_CUTSCENE_EXIT)
			//fight
			syncsceneID = CREATE_SYNCHRONIZED_SCENE(<<1988.329,3061.971,46.05>>,<<0,0,40>>)
			
			TASK_SYNCHRONIZED_SCENE(peds[mpf_pilot1].id,syncsceneID,"trailer2@shoved_peds","B_SHOVE", INSTANT_BLEND_IN,NORMAL_BLEND_OUT)			
			TASK_SYNCHRONIZED_SCENE(peds[mpf_pilot2].id,syncsceneID,"trailer2@shoved_peds","A_SHOVE",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
			CLEAR_AREA(<< 1415.1364, 3581.8572, 34.8545 >>,50,true)
			mission_substage++
		break
		case 2
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) > 0.99
				if not bDrawMenu 
				and bresetScene 
					DO_SCREEN_FADE_OUT(0)
					
					if bPlayThrough	
						Load_Stage_Assets(STAGE_9_TREV1_MCS3_P2_A)
						set_up_stage(STAGE_9_TREV1_MCS3_P2_A)
						Mission_Set_Stage(STAGE_9_TREV1_MCS3_P2_A)
						mission_substage = STAGE_ENTRY
					else
						if DOES_ENTITY_EXIST(peds[mpf_driver].id)
							delete_ped(peds[mpf_driver].id)
						endif
						IF DOES_ENTITY_EXIST(vehs[veh_tornado3].id)
							IF NOT IS_ENTITY_DEAD(vehs[veh_tornado3].id)
								delete_vehicle(vehs[veh_tornado3].id)
							endif
						endif
						if DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
							delete_ped(peds[mpf_pilot1].id)
						endif
						if DOES_ENTITY_EXIST(peds[mpf_pilot2].id)
							delete_ped(peds[mpf_pilot2].id)
						endif
						if DOES_ENTITY_EXIST(peds[mpf_oldman].id)
							delete_ped(peds[mpf_oldman].id)
						endif
						if DOES_ENTITY_EXIST(peds[mpf_phone].id)
							delete_ped(peds[mpf_phone].id)
						endif
						if  DOES_ENTITY_EXIST(peds[mpf_fatmancross].id)
							delete_ped(peds[mpf_fatmancross].id)
						endif
						CLEAR_AREA(<< 1415.1364, 3581.8572, 34.8545>>,30,true)
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
	if DOES_ENTITY_EXIST(vehs[veh_tornado3].id)
		if not IS_ENTITY_DEAD(vehs[veh_tornado3].id)
			SET_VEHICLE_FORWARD_SPEED(vehs[veh_tornado3].id,10.8)
		endif	
	endif
//	if DOES_ENTITY_EXIST(vehs[veh_parked_car].id)
//		if not IS_ENTITY_DEAD(vehs[veh_parked_car].id)
//			SET_ENTITY_COORDS(vehs[veh_parked_car].id,vDebugPosition)
//			set_entity_heading(vehs[veh_parked_car].id,fDebugHeading)
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_parked_car].id)
//		endif	
//	endif
endproc
proc ST_9_TREV1_MCS3_P2_A()
	switch mission_substage
		case STAGE_ENTRY
					
			if HAS_CUTSCENE_LOADED()
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -42.4877, 3093.7954, 26.7999 >>, << 4.0862, -0.0000, 49.0044 >>,46.1490, TRUE)
			
				WATER_REFLECTION_SET_HEIGHT(true,fwatereflection)
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif				
				
				camfollow = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.3)
				SHAKE_CAM(camfollow,"HAND_SHAKE",0.4)
				SET_CLOCK_TIME(GET_CLOCK_HOURS(),30,0)		
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				START_CUTSCENE()
				SET_CUTSCENE_ORIGIN(<<-9.430,3051.75,40>>,23.603,0)				
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1000					
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 10200
				SET_CAM_PARAMS(cameraIndex,<< -42.4877, 3093.7954, 26.7999 >>, << 4.0862, -0.0000, 49.0044 >>,38.0423,3000)
				SET_CAM_PARAMS(camfollow,<< -42.4877, 3093.7954, 26.7999 >>, << 4.0862, -0.0000, 49.0044 >>,38.0423,0)
				POINT_CAM_AT_COORD(camfollow,<<-48.32471, 3100.03223, 26.24728>>)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 11200				
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,2500)
				mission_substage++
			endif
		break
		case 4
			if GET_CUTSCENE_TIME() >= 14000					
				mission_substage++
			endif
		break
		case 5
			if GET_CUTSCENE_TIME() >= 17800	
				SET_CAM_ACTIVE(cameraIndex,true)
				SET_CAM_PARAMS(cameraIndex,<< -44.8513, 3094.5393, 27.2668 >>, << 19.7181, 3.1424, -160.9558 >>,16.8474,0)
				SET_CAM_PARAMS(cameraIndex,<< -45.0337, 3094.4468, 27.1943 >>, << 23.6207, 3.1258, -154.9080 >>,15.5739,1200)//<< -45.0328, 3094.4441, 27.1954 >>, << 21.0296, 3.1424, -157.5487 >>,16.8474,1200)				
				mission_substage++
			endif
		break
		case 6
			if GET_CUTSCENE_TIME() >= 19200					
				STOP_CUTSCENE_IMMEDIATELY()				
				mission_substage++
			endif
		break
		case 7
			if HAS_CUTSCENE_FINISHED()		
				if not bDrawMenu
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_10_CHIN1_INT_P2)
						set_up_stage(STAGE_10_CHIN1_INT_P2)
						Mission_Set_Stage(STAGE_10_CHIN1_INT_P2)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_9_TREV1_MCS3_P2_A)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch


	if mission_substage > 2
	and mission_substage < 5
	and IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()		
	endif
endproc
proc ST_10_CHIN1_INT_P2() //bar fight
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				START_CUTSCENE()	
				clear_area(<<1987.9679, 3053.7041, 46.1257>>,20,true)
				//camera
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<1987.9679, 3053.7041, 46.1257>>, << 5.2109, 1.3016, -35.9444 >>, 45, TRUE)
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 2000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 24000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 3
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_11_TREV1_MCS3_P2_B)
						set_up_stage(STAGE_11_TREV1_MCS3_P2_B)
						Mission_Set_Stage(STAGE_11_TREV1_MCS3_P2_B)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_10_CHIN1_INT_P2)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_ACTIVE()
		//add blood decals
//		if not does_entity_exist(peds[mpf_pilot2].id)
//			if does_entity_exist(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Russian_Drunk", CS_RussianDrunk))
//
//				peds[mpf_pilot2].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Russian_Drunk", CS_RussianDrunk))
//
//			endif 
//		
//		else 

//			if not is_ped_injured(peds[mpf_pilot2].id)
							
				if GET_CUTSCENE_TIME() >= 22930 //has_anim_event_fired(peds[mpf_pilot2].id, get_hash_key("Blood_Splat"))			
					if not is_decal_alive(blood[0].the_decal_id)
						println("DECAL")
						blood[0].the_decal_id = add_decal(blood[0].decal_texture_id, blood[0].pos, blood[0].direction, blood[0].side, blood[0].width, blood[0].height, 0.196, 0, 0, blood[0].fAlpha, blood[0].life)
						blood[1].the_decal_id = add_decal(blood[1].decal_texture_id, blood[1].pos, blood[1].direction, blood[1].side, blood[1].width, blood[1].height, 0.196, 0, 0, blood[1].fAlpha, blood[1].life)
					endif 
				endif 
//			endif 

//		endif
	endif
endproc
proc ST_11_TREV1_MCS3_P2_B()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				START_CUTSCENE()
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<-31.2,3040.7,40>>, << 5.2109, 1.3016, -35.9444 >>, 45, TRUE)
				//camera
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 2000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 24000
			or HAS_CUTSCENE_FINISHED()				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 3
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(AS_BUGGY)
						set_up_stage(AS_BUGGY)
						Mission_Set_Stage(AS_BUGGY)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_11_TREV1_MCS3_P2_B)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
endproc
proc AST_BUGGY()
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_TREVOR
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
		 		ENDWHILE	
			endif
			
			CLEAR_AREA(<<2330.8523, 3127.2266, 48.0014  >>,30,true)
			bHighDofOn = true
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 2330.8523, 3127.2266, 48.0014 >>, << 0.5023, 4.0720, 143.7649 >>, 46.5, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			settimera(0)
			//buggy
			if not DOES_ENTITY_EXIST(vehs[veh_dune].id)
				vehs[veh_dune].id = CREATE_VEHICLE(dune,<< 2330.9966, 3107.8857, 47.1834 >>, 12.5717)	
				SET_VEHICLE_EXTRA(vehs[veh_dune].id,3,true)
				SET_VEHICLE_EXTRA(vehs[veh_dune].id,2,true)
				POINT_CAM_AT_ENTITY(cameraIndex,vehs[veh_dune].id,<<0,0,0>>)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehs[veh_dune].id)
				SET_VEHICLE_ENGINE_ON(vehs[veh_dune].id,true,true)
			endif			
			mission_substage++
		break
		case 1
			if timera() > 500
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				if IsEntityAlive(vehs[veh_dune].id)
					SET_VEHICLE_EXTRA(vehs[veh_dune].id,3,true)
					SET_VEHICLE_EXTRA(vehs[veh_dune].id,2,true)
				endif
				SET_CAM_PARAMS(cameraIndex, << 2330.8523, 3127.2266, 48.0014 >>, << 0.5313, -3.1592, 83.2309 >>, 36.5, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
				bplayerControl = true				
				mission_substage++
			endif
		break
		case 2
			if timera() > 10000
			if not bDrawMenu 
			and bresetScene
				DO_SCREEN_FADE_OUT(0)				
				if bPlayThrough	
					Load_Stage_Assets(AS_TREV_MOL)
					set_up_stage(AS_TREV_MOL)
					Mission_Set_Stage(AS_TREV_MOL)
					mission_substage = STAGE_ENTRY
				else					
					if IsEntityAlive(player_ped_id())
						SET_ENTITY_COORDS(player_ped_id(),<<2328.93408, 3109.98779, 47.17923>>)
					endif
					if IsEntityAlive(vehs[veh_dune].id)	
						EMPTY_VEHICLE(vehs[veh_dune].id)
						DELETE_VEHICLE(vehs[veh_dune].id)
					endif		
					DESTROY_ALL_CAMS()
					mission_substage = STAGE_ENTRY
				endif
			endif
			endif
		break
	endswitch	
		
endproc
proc AST_TREV_MOL()	
	switch mission_substage
		case STAGE_ENTRY			
			CLEAR_AREA(<<2461.0942, 4952.7583, 45.2938  >>,30,true)
			bHighDofOn = true
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 2461.9463, 4953.2314, 45.3812 >>, << 5.2509, 0.2879, -60.8458 >>, 45.9737, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)						
			if not DOES_ENTITY_EXIST(peds[mpf_trev].id)
				peds[mpf_trev].id = CREATE_PED(PEDTYPE_MISSION,player_two,<< 2463.4604, 4953.9409, 44.1651 >>, 61.5282)			
				SET_PED_COMP_ITEM_CURRENT_SP(peds[mpf_trev].id, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT)	
				GIVE_WEAPON_TO_PED(peds[mpf_trev].id,WEAPONTYPE_MOLOTOV,10,true,true)
				SET_CURRENT_PED_WEAPON(peds[mpf_trev].id,WEAPONTYPE_MOLOTOV,true)
				syncsceneID = CREATE_SYNCHRONIZED_SCENE(<<2463.460,4953.941,45.165>>,<<0,0,180>>)				
				TASK_SYNCHRONIZED_SCENE(peds[mpf_trev].id,syncsceneID,"trailer2@molotov_throw","trevor_throw_molotov",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_trev].id,false)
			endif
			mission_substage++
		break
		case 1
			if IsEntityAlive(peds[mpf_trev].id)
			and HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_trev].id)
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				SET_CAM_PARAMS(cameraIndex,<< 2461.9534, 4953.2207, 45.3811 >>, << 6.9905, 0.3115, -55.1523 >>,45.9737, 1000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.2)
				mission_substage++
			endif
		break
		case 2
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.37
				camfollow = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 2461.9534, 4953.2207, 45.3811 >>, << 9.4673, 0.3115, -58.1561 >>,39.2322)
				SHAKE_CAM(camfollow,"HAND_SHAKE",0.2)
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,700)
				mission_substage++
			endif	
		break
		case 3
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.86
				if not bDrawMenu 
				and bresetScene 
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(AS_FIRE_TRAIL)
						set_up_stage(AS_FIRE_TRAIL)
						Mission_Set_Stage(AS_FIRE_TRAIL)
						mission_substage = STAGE_ENTRY
					else
						if timerb() > 2000
							if DOES_ENTITY_EXIST(peds[mpf_trev].id)
								delete_ped(peds[mpf_trev].id)
							endif
							DESTROY_ALL_CAMS()
							mission_substage = STAGE_ENTRY
						endif
					endif
				else
					settimerb(0)
				endif
			endif
		break
	endswitch
endproc
PROC AST_FIRE_TRAIL()
	switch mission_substage
		case STAGE_ENTRY			
			if IS_SCREEN_FADED_OUT()					
				DO_SCREEN_FADE_IN(0)
			endif
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 2440.2930, 4969.5010, 47.2616 >>, << -18.8169, -0.7638, -66.2176 >>,60.0335, TRUE)
			SET_CAM_ACTIVE(cameraIndex,true)
			SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.8)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
  			CLEAR_AREA(<<2461.0942, 4952.7583, 45.2938  >>,50,true)						
			SET_CAM_PARAMS(cameraIndex,<< 2440.2578, 4969.5659, 47.2585 >>, << -27.1572, -0.4346, -83.5768 >>,59.6725,2500,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
			
			gasTrail[0].coord = <<2449.5710, 4973.7192, 45.8104>>
			gasTrail[1].coord = <<2448.6167, 4973.2974, 45.8106>>
			gasTrail[2].coord = <<2447.6245, 4973.0576, 45.8106>>
			gasTrail[3].coord = <<2446.6130, 4973.3550, 45.8106>>
			gasTrail[4].coord = <<2445.5569, 4973.1733, 45.8106>>
			gasTrail[5].coord = <<2445.3013, 4972.8477, 46.8068>>
			gasTrail[6].coord = <<2445.1509, 4972.8140, 45.8106>>
			gasTrail[7].coord = <<2444.3108, 4972.2383, 45.8106>>
			gasTrail[8].coord = <<2443.4321, 4971.7354, 45.8106>>
			gasTrail[9].coord = <<2443.1523, 4970.6953, 45.8106>>
			gasTrail[10].coord = <<2442.3347, 4970.1021, 45.8106>>
			gasTrail[11].coord = <<2442.0115, 4969.1426, 45.8106>>
			gasTrail[12].coord = <<2441.0894, 4968.6128, 45.8306>>
			gasTrail[13].coord = <<2440.7466, 4968.0190, 45.8106>>
			SETTIMERA(0)
			IGNITE_GAS_TRAIL()
			mission_substage++

		break
		case 1
			if TIMERA() > 2000
				if not bDrawMenu 
				and bresetScene 
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(AS_BARN_BOOM)
						set_up_stage(AS_BARN_BOOM)
						Mission_Set_Stage(AS_BARN_BOOM)
						mission_substage = STAGE_ENTRY
					else	
						STOP_FIRE_IN_RANGE(<<2440.7466, 4968.0190, 45.8106>>,30)
						CLEANUP_GAS_TRAILS()
						DESTROY_ALL_CAMS()
						mission_substage++
					endif
				endif
			endif
		break
		case 2
			if TIMERA() > 4500
				mission_substage = STAGE_ENTRY
			endif
		break
	endswitch
ENDPROC
proc AST_BARN_BLOW()
	switch mission_substage
		case STAGE_ENTRY			
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
						WAIT(0)
			 		ENDWHILE	
				endif
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 2474.4490, 4949.2695, 44.7158 >>, << 6.6523, -0.0000, 47.9417 >>, 23.7333, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_PARAMS(cameraIndex,<< 2474.4490, 4949.1992, 44.7158 >>, << 8.6303, 0.0000, 47.9417 >>, 25,1000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	  			CLEAR_AREA(<<2461.0942, 4952.7583, 45.2938  >>,50,true)
				CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,true)
				CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,true)
				SET_CUTSCENE_FADE_VALUES()
				START_CUTSCENE()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				mission_substage++
			endif		
		break
		case 1
			if GET_CUTSCENE_TIME() > 1000
				SET_CAM_PARAMS(cameraIndex,<< 2472.7263, 4943.2305, 44.5535 >>, << 13.5964, -0.0145, 35.8347 >>, 36.6522,0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(cameraIndex,<< 2472.7744, 4943.2002, 44.8689 >>, << 11.1380, -0.0145, 35.8347 >>, 36.6522,2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				settimera(0)
				mission_substage++
			endif	
		break
		case 2 
			if HAS_CUTSCENE_FINISHED()
			or GET_CUTSCENE_TIME() > 3000
				if IS_CUTSCENE_PLAYING()			
					STOP_CUTSCENE_IMMEDIATELY()
				endif	
				if not bDrawMenu 
				and bresetScene 
					if timerb() > 2000
						DO_SCREEN_FADE_OUT(0)
						if bPlayThrough	
							Load_Stage_Assets(STAGE_12_CHIN1_MCS4)
							set_up_stage(STAGE_12_CHIN1_MCS4)
							Mission_Set_Stage(STAGE_12_CHIN1_MCS4)
							mission_substage = STAGE_ENTRY
						else						
							bDoSkip = true
							iSkipToStage = enum_to_int(AS_BARN_BOOM)
							mission_substage = STAGE_ENTRY
						endif
					endif
				else
					settimerb(0)
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc							
proc ST_12_CHIN1_MCS4() // dont mind the bodies
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
					
				//camera
				START_CUTSCENE()	
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 1387.8705, 3594.2642, 33.8831 >>, << 5.2109, 1.3016, -35.9444 >>, 45, TRUE)
				CLEAR_AREA(<< 1387.8705, 3594.2642, 33.8831 >>,50,true)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1388.541382,3595.834717,34.822456,1.020051)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1368.177002,3599.519043,34.564472,8.601562)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1388.541382,3595.834717,34.822456,4.988801)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1382.398926,3606.866699,37.343925,37.099258)
				if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
					peds[mpf_driver].id = CREATE_PED(PEDTYPE_MISSION,G_M_Y_PoloGoon_01,<<1372.51465, 3596.12451, 35.89510>>,50)	
					SET_ENTITY_LOD_DIST(peds[mpf_driver].id ,200)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_frank].id)
					peds[mpf_frank].id = CREATE_PED(PEDTYPE_MISSION,G_M_Y_PoloGoon_01,<<1374.22021, 3598.77832, 35.89492>>,256)		
					SET_ENTITY_LOD_DIST(peds[mpf_frank].id ,200)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_mike].id)
					peds[mpf_mike].id = CREATE_PED(PEDTYPE_MISSION,G_M_Y_PoloGoon_01,<<1373.69910, 3593.69238, 35.88594>>,10)
					SET_ENTITY_LOD_DIST(peds[mpf_mike].id ,200)
				endif
				if not DOES_ENTITY_EXIST(vehs[veh_exp1].id)
					vehs[veh_exp1].id = CREATE_VEHICLE(LANDSTALKER,<< 1368.1614, 3599.2683, 33.8946 >>, 252.9023)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_exp1].id,0)
					SET_VEHICLE_DOOR_OPEN(vehs[veh_exp1].id,SC_DOOR_FRONT_LEFT,true)
				endif
				if not DOES_ENTITY_EXIST(vehs[veh_exp2].id)
					vehs[veh_exp2].id = CREATE_VEHICLE(SENTINEL,<< 1366.8489, 3594.2241, 33.8873 >>, 289.2585)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_exp2].id,0)
					SET_VEHICLE_DOOR_OPEN(vehs[veh_exp2].id,SC_DOOR_FRONT_LEFT,true)
					SET_VEHICLE_DOOR_OPEN(vehs[veh_exp2].id,SC_DOOR_FRONT_RIGHT,true)
				endif
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF				
				if IsEntityAlive(peds[mpf_driver].id)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,(GET_ENTITY_COORDS(peds[mpf_driver].id)+<<0,0,0.1>>),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
					EXPLODE_PED_HEAD(peds[mpf_driver].id)
				endif
				if IsEntityAlive(peds[mpf_frank].id)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,GET_ENTITY_COORDS(peds[mpf_frank].id),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
					EXPLODE_PED_HEAD(peds[mpf_frank].id)
				endif
				if IsEntityAlive(peds[mpf_mike].id)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,(GET_ENTITY_COORDS(peds[mpf_mike].id)+<<0,0,0.1>>),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
					EXPLODE_PED_HEAD(peds[mpf_mike].id)
				endif
				if IsEntityAlive(vehs[veh_exp1].id)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,(GET_ENTITY_COORDS(vehs[veh_exp1].id)+<<0.2,-0.1,0.3>>),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,GET_ENTITY_COORDS(vehs[veh_exp1].id),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
				endif
				if IsEntityAlive(vehs[veh_exp2].id)				
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<1382.8558, 3598.7285, 44.4848>>,GET_ENTITY_COORDS(vehs[veh_exp1].id),100,true,WEAPONTYPE_ASSAULTSHOTGUN)
				endif
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 25000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 3
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene				
					WASH_DECALS_IN_RANGE(<<1373.75684, 3597.12354, 33.89509>>,100,50)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_13_DOCKS_BRIDGE_SHOT)
						set_up_stage(STAGE_13_DOCKS_BRIDGE_SHOT)
						Mission_Set_Stage(STAGE_13_DOCKS_BRIDGE_SHOT)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_12_CHIN1_MCS4)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
endproc
proc ST_13_DOCKS_BRIDGE_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			fLODscale = 1.3
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 844.4641, -2703.3467, 64.8 >>, << -3.0696, 0.0000, 45.0314 >>, 34.0, TRUE)
			SET_CAM_PARAMS(cameraIndex, << 844.4641, -2696.1641, 64.8 >>, << -3.0696, 0.0000, 45.0314 >>, 34, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if TIMERA() > 3000
				if not bDrawMenu 
				and bresetScene 
					if bPlayThrough
						DO_SCREEN_FADE_OUT(0)
						Load_Stage_Assets(STAGE_14_GHETTO_TRAIN_SHOT)
						set_up_stage(STAGE_14_GHETTO_TRAIN_SHOT)
						Mission_Set_Stage(STAGE_14_GHETTO_TRAIN_SHOT)
						mission_substage = STAGE_ENTRY
					else			
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
endproc
proc ST_13_DOCKS_BRIDGE_SHOT_ALT()
	switch mission_substage
		case STAGE_ENTRY
			fLODscale = 1.3
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 844.4641, -2703.3467, 64.8 >>, << -3.0696, 0.0000, 45.0314 >>, 34.0, TRUE)
			SET_CAM_PARAMS(cameraIndex, << 844.4641, -2696.1641, 64.8 >>, << -3.0696, 0.0000, 45.0314 >>, 34, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if TIMERA() > 3000
				if not bDrawMenu 
				and bresetScene 
					if bPlayThrough
						DO_SCREEN_FADE_OUT(0)
						Load_Stage_Assets(STAGE_14_GHETTO_TRAIN_SHOT)
						set_up_stage(STAGE_14_GHETTO_TRAIN_SHOT)
						Mission_Set_Stage(STAGE_14_GHETTO_TRAIN_SHOT)
						mission_substage = STAGE_ENTRY
					else			
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
endproc
proc ST_14_GHETTO_TRAIN_SHOT()
	switch mission_substage
		case STAGE_ENTRY			
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 263.4090, -1907.6572, 31.7329 >>, << -6.2916, 0.0000, 111.8651 >>, 33.4236, TRUE)
			SET_CAM_PARAMS(cameraIndex,<< 263.3643, -1907.6705, 31.7278 >>, << -5.8188, -0.0000, 105.4892 >>, 30.0626, 3500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)				
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,229.604538,-1921.954956,24.265200,12.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,207.779190,-1932.754028,24.944870,12.731603)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,145.656769,-1956.644043,22.935163,72.007889)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,145.656769,-1956.644043,22.935163,250.000000)
			
			//create train			
			vehs[veh_Ghetto_train].id = CREATE_MISSION_TRAIN(iTrainConfiguration,vtrainGhettopos, TRUE)
			SET_TRAIN_CRUISE_SPEED(vehs[veh_Ghetto_train].id, 0)
			SET_TRAIN_SPEED(vehs[veh_Ghetto_train].id, 0)
			CLEAR_AREA(<<225.69148, -1918.71436, 23.08420>>,10,true)
			if not DOES_ENTITY_EXIST(vehs[veh_Bike].id)
				CLEAR_AREA(<< -461.0177, 1098.2268, 326.6818 >>,5,true)
				vehs[veh_Bike].id = CREATE_VEHICLE(DOMINATOR,<< -461.0177, 1098.2268, 326.6818 >>, 339.7852)
				peds[mpf_chop].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_Bike].id)
				TASK_VEHICLE_DRIVE_WANDER(peds[mpf_chop].id,vehs[veh_Bike].id,10,DRIVINGMODE_STOPFORCARS_STRICT)
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_Driveway].id)			
				CLEAR_AREA(<< 223.2482, -1906.0947, 23.7127 >>,5,true)
				vehs[veh_Driveway].id = CREATE_VEHICLE(FUTO,<< 223.2482, -1906.0947, 23.7127 >>, 138.0350)
				peds[mpf_driver].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_Driveway].id)
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_dune].id)
				CLEAR_AREA(<< 192.2951, -1908.0621, 22.3350>>,5,true)
				vehs[veh_dune].id = CREATE_VEHICLE(GRESLEY,<< 192.2951, -1908.0621, 22.3350 >>, 230.4381)
				peds[mpf_frank].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_dune].id)
				TASK_VEHICLE_DRIVE_WANDER(peds[mpf_frank].id,vehs[veh_dune].id,10,DRIVINGMODE_STOPFORCARS_STRICT)
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_exp1].id)
				CLEAR_AREA(<<195.5358, -1939.4250, 20.3868 >>,5,true)
				vehs[veh_exp1].id = CREATE_VEHICLE(CAVALCADE,<< 195.5358, -1939.4250, 20.3868 >>, 142.0388)
				peds[mpf_mike].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_exp1].id)
				TASK_VEHICLE_DRIVE_WANDER(peds[mpf_mike].id,vehs[veh_exp1].id,5,DRIVINGMODE_STOPFORCARS_STRICT)
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_exp2].id)
				CLEAR_AREA(<< 219.1808, -1948.7679, 21.0163 >>,5,true)
				vehs[veh_exp2].id = CREATE_VEHICLE(SERRANO,<< 219.1808, -1948.7679, 21.0163 >>, 321.7678)
				peds[mpf_oldman].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_exp2].id)				
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_exp3].id)
				CLEAR_AREA(<< 181.9075, -1950.0544, 19.2296 >>,5,true)
				vehs[veh_exp3].id = CREATE_VEHICLE(ORACLE,<< 181.9075, -1950.0544, 19.2296 >>, 140.7774)
				peds[mpf_phone].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_exp3].id)
				TASK_VEHICLE_DRIVE_WANDER(peds[mpf_phone].id,vehs[veh_exp3].id,15,DRIVINGMODE_STOPFORCARS_STRICT)
			endif
			if not DOES_ENTITY_EXIST(vehs[veh_jet].id)
				vehs[veh_jet].id = CREATE_VEHICLE(bus,<<228.8420, -1938.4072, 22.1604>>, 319.7927)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_jet].id,1)
				peds[mpf_jetPilot].id = CREATE_RANDOM_PED_AS_DRIVER(vehs[veh_jet].id)
				TASK_VEHICLE_DRIVE_WANDER(peds[mpf_jetPilot].id,vehs[veh_jet].id,20,DRIVINGMODE_STOPFORCARS_STRICT)
			endif
			SETTIMERA(0)
			mission_substage++
		break
		case 1
			if timera()> 1000
				if IS_SCREEN_FADED_OUT()	
					DO_SCREEN_FADE_IN(0)
				endif
				if IsEntityAlive(vehs[veh_Ghetto_train].id)
					SET_TRAIN_CRUISE_SPEED(vehs[veh_Ghetto_train].id, ftrainSpeed)
					SET_TRAIN_SPEED(vehs[veh_Ghetto_train].id, ftrainSpeed)
				endif
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 2
			if timera() > 2500	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough							
						Load_Stage_Assets(STAGE_15_ARM1_INT_P3)
						set_up_stage(STAGE_15_ARM1_INT_P3)
						Mission_Set_Stage(STAGE_15_ARM1_INT_P3)
						mission_substage = STAGE_ENTRY
					else						
						if timera() > 3500		
							if DOES_ENTITY_EXIST(vehs[veh_Bike].id)
								DELETE_VEHICLE(vehs[veh_Bike].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_chop].id)
								delete_ped(peds[mpf_chop].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_Driveway].id)			
								DELETE_VEHICLE(vehs[veh_Driveway].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_driver].id)	
								delete_ped(peds[mpf_driver].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_dune].id)
								DELETE_VEHICLE(vehs[veh_dune].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_frank].id)
								delete_ped(peds[mpf_frank].id)
							endif	
							if DOES_ENTITY_EXIST(vehs[veh_exp1].id)
								DELETE_VEHICLE(vehs[veh_exp1].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_mike].id)
								delete_ped(peds[mpf_mike].id)
							endif	
							if DOES_ENTITY_EXIST(vehs[veh_exp2].id)
								DELETE_VEHICLE(vehs[veh_exp2].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_oldman].id)
								delete_ped(peds[mpf_oldman].id)		
							endif
							if DOES_ENTITY_EXIST(vehs[veh_exp3].id)
								DELETE_VEHICLE(vehs[veh_exp3].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_phone].id)
								delete_ped(peds[mpf_phone].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_jet].id)
								DELETE_VEHICLE(vehs[veh_jet].id)
							endif
							if DOES_ENTITY_EXIST(peds[mpf_jetPilot].id)
								delete_ped(peds[mpf_jetPilot].id)
							endif
							
							DELETE_MISSION_TRAIN(vehs[veh_Ghetto_train].id)
							mission_substage = STAGE_ENTRY
						endif
					endif
				endif
			endif
		break
	endswitch
endproc
proc ST_15_ARM1_INT_P3() // frank lamar and mike
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_FRANKLIN
				or get_player_ped_enum(player_ped_id())	=CHAR_MICHAEL
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				START_CUTSCENE()				
				//camera
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -1882.8746, -621.5580, 11.2904 >>, << 5.2109, 1.3016, -35.9444 >>, 39.2, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SETTIMERA(0)
				
				//ambient peds
				if not DOES_ENTITY_EXIST(peds[mpf_slide].id)
					peds[mpf_slide].id = CREATE_PED(PEDTYPE_MISSION,A_F_Y_Runner_01,<< -1855.9010, -649.7280, 9.7416 >>, 226.4764)
					TASK_PLAY_ANIM(peds[mpf_slide].id,"move_f@jogger","jogging",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
					peds[mpf_driver].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_Runner_01,<<-1849.91382, -652.79443, 9.58069>>, 47.1811)
					TASK_PLAY_ANIM(peds[mpf_driver].id,"move_m@jogger","run",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_fatmancross].id)
					peds[mpf_fatmancross].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_Beach_01,<<-1868.48071, -641.28119, 10.17934>>)
					TASK_START_SCENARIO_AT_POSITION(peds[mpf_fatmancross].id,"WORLD_HUMAN_SMOKING",<<-1867.03259, -652.71924, 9.88202>>,144.1027)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_oldman].id)
					peds[mpf_oldman].id = CREATE_PED(PEDTYPE_MISSION,A_F_Y_Beach_01,<<-1869.61414, -641.03345, 10.09873>>)
					TASK_START_SCENARIO_AT_POSITION(peds[mpf_oldman].id,"WORLD_HUMAN_TOURIST_MOBILE",<<-1868.18555, -652.66718, 9.85372>>,144.1027)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
					peds[mpf_pilot1].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_Beach_02,<<-1832.87830, -658.80634, 9.39731>>)
					TASK_START_SCENARIO_AT_POSITION(peds[mpf_pilot1].id,"WORLD_HUMAN_DRINKING",<<-1855.64075, -643.02692, 9.79211>>,91.8124)
				endif
				if not DOES_ENTITY_EXIST(peds[mpf_phone].id)
					peds[mpf_phone].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_Beach_03,<<-1835.54700, -656.17584, 9.43967>>)
					TASK_START_SCENARIO_AT_POSITION(peds[mpf_phone].id,"WORLD_HUMAN_HANG_OUT_STREET",<<-1855.96375, -641.81024, 9.79603>>,156.6058)
				endif				
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 5000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				SET_CAM_PARAMS(cameraIndex,<< -1882.8746, -621.5580, 11.2904 >>, << 6.8497, 1.3016, -33.8065 >>, 39.2, 3500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)								
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 22600
				SET_CAM_PARAMS(cameraIndex,<< -1884.0699, -613.8306, 11.9532 >>, << -1.7772, 0.0002, -145.2922 >>, 18.5509, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)								
				SET_CAM_PARAMS(cameraIndex,<< -1884.1084, -613.8566, 11.9532 >>, << -1.0972, 0.0002, -144.8821 >>, 17.3141, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)								
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 25500	
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_16_FRA2_INT_P2)
						set_up_stage(STAGE_16_FRA2_INT_P2)
						Mission_Set_Stage(STAGE_16_FRA2_INT_P2)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_15_ARM1_INT_P3)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()
		if  GET_CUTSCENE_TIME() < 22600
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1881.205811,-619.626770,11.360289,1.192433)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1880.906982,-617.691467,11.383300,1.421409)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1872.708984,-608.497009,11.383300,11.796383)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1821.171021,-548.864075,13.820810,86.968231)

		elif GET_CUTSCENE_TIME() >= 22600
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1881.365967,-617.224670,11.402990,1.328125)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1881.205811,-619.626770,11.360289,1.192433)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1872.708984,-628.183899,11.383300,11.796383)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1821.171021,-637.418030,13.820810,86.968231)
		endif
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc LNCH_ARM_1()
TEXT_LABEL_15 sMissionName = "armenian1"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("armenian1")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc ST_16_FRA2_INT_P2() // frank and ex gf
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				fLODscale = 2
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_FRANKLIN
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				START_CUTSCENE()	
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<-1882.8746, -621.5580, 11.2904>>, << 5.2109, 1.3016, -35.9444 >>, 45, TRUE)
				SET_CLOCK_TIME(GET_CLOCK_HOURS(),50,0)
				//camera
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 0
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break		
		case 2
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_17_ARM1_INT_P4)//AS_CAR_TRANS_TRUCK)
						set_up_stage(STAGE_17_ARM1_INT_P4)//AS_CAR_TRANS_TRUCK)
						Mission_Set_Stage(STAGE_17_ARM1_INT_P4)//AS_CAR_TRANS_TRUCK)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_16_FRA2_INT_P2)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
endproc
proc LNCH_CAR_STEAL_FINALE()
TEXT_LABEL_15 sMissionName = "carsteal5"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
		
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("carsteal5")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc ST_17_ARM1_INT_P4() // frank and lamar talk
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_FRANKLIN
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				START_CUTSCENE()	
				
				//camera	
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -1889.1241, -611.5778, 12.1867 >>, << 0.8171, 0.0000, -110.1338 >>, 45.4540, TRUE)
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.5)
				SETTIMERA(0)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1888.470459,-611.470825,11.557186,1.308422)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1887.373779,-612.942749,11.580187,1.191715)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1881.138184,-611.045227,11.580190,9.152658)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1853.282837,-621.021423,-4.669847,40.000000)

				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 0
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 24800
				SET_CAM_PARAMS(cameraIndex,<< -1889.1896, -611.6677, 12.1862 >>, << -1.8354, -0.0000, -100.9288 >>,45.4540,3000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 27450
				SET_CAM_PARAMS(cameraIndex,<< -1888.8672, -612.3585, 12.1531 >>, << 2.0534, -0.0000, -44.9319 >>,35.7958,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				mission_substage++
			endif
		break
		case 4
			if GET_CUTSCENE_TIME() >= 30000
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 5
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_18_ARM1_INT_P1)
						set_up_stage(STAGE_18_ARM1_INT_P1)
						Mission_Set_Stage(STAGE_18_ARM1_INT_P1)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_17_ARM1_INT_P4)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()	
		if mission_substage > 2
			BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
		endif
	endif
endproc
proc ST_17_ARM1_INT_P4_ALT() // frank and lamar talk
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_FRANKLIN
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				START_CUTSCENE()	
				
				//camera	
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -1889.1241, -611.5778, 12.1867 >>, << 0.8171, 0.0000, -110.1338 >>, 45.4540, TRUE)
				SET_CLOCK_TIME(GET_CLOCK_HOURS(),50,0)
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.5)
				SETTIMERA(0)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1888.470459,-611.470825,11.557186,1.308422)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1887.373779,-612.942749,11.580187,1.191715)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1881.138184,-611.045227,11.580190,9.152658)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1853.282837,-621.021423,-4.669847,40.000000)

				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 0
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 24800
				SET_CAM_PARAMS(cameraIndex,<< -1889.1896, -611.6677, 12.1862 >>, << -1.8354, -0.0000, -100.9288 >>,45.4540,3000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 27450
				SET_CAM_PARAMS(cameraIndex,<< -1888.8672, -612.3585, 12.1531 >>, << 2.0534, -0.0000, -44.9319 >>,35.7958,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				mission_substage++
			endif
		break
		case 4
			if GET_CUTSCENE_TIME() >= 30000
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 5
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_18_ARM1_INT_P1)
						set_up_stage(STAGE_18_ARM1_INT_P1)
						Mission_Set_Stage(STAGE_18_ARM1_INT_P1)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_17_ARM1_INT_P4)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()	
		if mission_substage > 2
			BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
		endif
	endif
endproc
proc LNCH_CAR_STEAL_3()
TEXT_LABEL_15 sMissionName = "carsteal3"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("carsteal3")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc ST_18_ARM1_INT_P1() // shrink
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()									
				if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
						WAIT(0)
			 		ENDWHILE	
				endif
				START_CUTSCENE()
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1917.264038,-573.376099,21.201332,0.500000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1911.839233,-570.471497,18.748800,1.500000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1907.958618,-577.133362,18.529911,3.500000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1907.958618,-577.133362,18.529911,20.000000)

				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 2000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -1912.8066, -570.3843, 19.4885 >>, << -12.2202, 0.0005, -131.0556 >>, 58.5346, TRUE)
				SET_CAM_PARAMS(cameraIndex,<< -1912.8699, -570.4575, 19.4885 >>, << -12.2202, 0.0005, -131.0556 >>, 58.5346, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				mission_substage++
			endif
		break
		// 2nd shot
		case 2
			if GET_CUTSCENE_TIME() >= 4060
				SET_CAM_PARAMS(cameraIndex,<<-1906.9742, -576.9066, 19.5320>>, <<-21.2485, 0.0749, 142.5980>>, 22.5370, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				SET_CAM_PARAMS(cameraIndex,<<-1906.9495, -576.8750, 19.3882>>, <<-11.7333, 0.0749, 142.5980>>, 22.5370, 7000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1910.150269,-580.080017,18.250196,1.007878)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1910.150024,-578.385193,18.250200,2.914063)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1910.150269,-580.080017,18.250196,10.000000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,-1910.150269,-580.080017,18.250196,20.000000)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 11000			
				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
			if not bDrawMenu 
			and bresetScene
				DO_SCREEN_FADE_OUT(0)
				if bPlayThrough	
					Load_Stage_Assets(STAGE_19_FAM5_MCS5)
					set_up_stage(STAGE_19_FAM5_MCS5)
					Mission_Set_Stage(STAGE_19_FAM5_MCS5)
					mission_substage = STAGE_ENTRY
				else	
					iSkipToStage = enum_to_int(STAGE_18_ARM1_INT_P1)
					bDoSkip = true
				endif
			endif
			endif
		break
	endswitch
	if IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc LNCH_FAM_1()
TEXT_LABEL_15 sMissionName = "Family1"
	switch mission_substage
		case STAGE_ENTRY
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT)
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Family1")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc ST_19_FAM5_MCS5() // lets bounce
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
					SET_ENTITY_VISIBLE(player_ped_id(),true)
				endif
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -1175.5521, -890.1361, 14.0579 >>, << -2.8052, -0.0001, 15.8190 >>, 26.7097, TRUE)			
				streamvol = STREAMVOL_CREATE_FRUSTUM(<< -1175.5521, -890.1361, 14.0579 >>, << -2.8052, -0.0001, 15.8190 >>,250,FLAG_MAPDATA)
				START_CUTSCENE()							
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF		
				
				mission_substage++				
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 58500
				SET_CAM_PARAMS(cameraIndex,<< -1175.4164, -890.0877, 14.0579 >>, << -2.8052, -0.0001, 20.2137 >>, 26.7097, 7000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 64000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 3
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_20_TREV1_INT_P4)
						set_up_stage(STAGE_20_TREV1_INT_P4)
						Mission_Set_Stage(STAGE_20_TREV1_INT_P4)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_19_FAM5_MCS5)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if mission_substage > 1
	and IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc ST_20_TREV1_INT_P4() //trev stomps johnny
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
						WAIT(0)
			 		ENDWHILE	
				endif
				while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_trev].id,CHAR_TREVOR,<<2019.6141, 3827.6531, 32.1370 >>)				
					wait(0)
				endwhile			
				APPLY_PED_DIRT(peds[mpf_trev].id,PDZ_TORSO,0.731,0.791,231.120,0.3,0.5,false,"cs_trev1_dirt")		
				APPLY_PED_DIRT(peds[mpf_trev].id,PDZ_TORSO,0.97,0.604,64.4,0.448,0.291,false,"cs_trev1_dirt")
				APPLY_PED_DIRT(peds[mpf_trev].id,PDZ_TORSO,0.746,0.769,64.4,0.180,0.8,false,"cs_trev1_dirt")	
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_trev].id,"Trevor",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 2019.6141, 3827.6531, 32.1370 >>, << 13.3138, 0.0002, 75.5047 >>,37.78, TRUE)
				START_CUTSCENE()	
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,2017.730957,3828.103027,32.206337,1.176932)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,2013.080200,3825.589844,32.198395,1.354639)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,2013.274414,3830.489014,31.648689,4.221611)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,2016.102051,3827.890869,33.907627,22.204473)
				isplat = 0
				mission_substage++
			endif 
		break
		case 1						
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			mission_substage++			
		break
		case 2			
			if GET_CUTSCENE_TIME() >= 43000				
				SET_CAM_PARAMS(cameraIndex,<< 2019.6141, 3827.6531, 32.1370 >>, << 19.2118, 0.0002, 73.0449 >>, 30.3997, 1000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 44000
				set_clock_time(18,0,0)
				SET_WEATHER_TYPE_NOW_PERSIST("clear")
				SET_CAM_PARAMS(cameraIndex,<< 2010.5330, 3824.8005, 32.8034 >>, << -3.9662, -0.0001, -69.6521 >>, 22.5574, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				SET_CAM_PARAMS(cameraIndex,<< 2010.5808, 3824.6895, 32.8407 >>, << -2.9147, -0.0001, -67.5574 >>, 21.9827, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				mission_substage++
			endif
		break
		case 4
			if GET_CUTSCENE_TIME() >= 45500
				SET_CAM_PARAMS(cameraIndex,<< 2019.4073, 3827.1326, 31.8241 >>, << 19.8743, -0.0002, 55.2114 >>, 21.0824, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				SET_CAM_PARAMS(cameraIndex,<< 2019.4424, 3827.1033, 31.9482 >>, << 21.3875, -0.0002, 49.9836 >>, 12.6036, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				mission_substage++
			endif
		break
		case 5
			if GET_CUTSCENE_TIME() >= 47000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 6
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_21_FBI2_MCS1_P1)
						set_up_stage(STAGE_21_FBI2_MCS1_P1)
						Mission_Set_Stage(STAGE_21_FBI2_MCS1_P1)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_20_TREV1_INT_P4)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	
	if mission_substage > 2
	and IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()		
	endif
	
	if DOES_ENTITY_EXIST(peds[mpf_trev].id)
	and not IS_PED_INJURED(peds[mpf_trev].id)
	and IS_CUTSCENE_PLAYING()
//		if HAS_ANIM_EVENT_FIRED(peds[mpf_trev].id,GET_HASH_KEY("TRIGGER_DAMAGE_VFX"))
			switch isplat
			case 0
				if GET_CUTSCENE_TIME() >= 43160
					//ptfx
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++
				endif
			break
			case 1
				if timerb() > 150
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.657,0.566,126.360,0.07,3,0,"cs_trev1_blood")
					println("BLOOD VFX0")
					isplat++
				endif
			break
			case 2
				if GET_CUTSCENE_TIME() >= 43730
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++					
				endif
			break
			case 3
				if timerb() > 150
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.589,0.266,0,0.01,2,0,"cs_trev1_blood")
					println("BLOOD VFX1")
					isplat++
				endif
			break
			case 4
				if GET_CUTSCENE_TIME() >= 44200
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++
				endif
			break
			case 5
				if timerb() > 200
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.612,0.269,126.360,0.22,2,0,"cs_trev1_blood")
					println("BLOOD VFX2")
					isplat++
				endif
			break
			case 6
				if GET_CUTSCENE_TIME() >= 44660
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++
					
				endif
			break
			case 7
				if timerb() > 200
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.709,0.396,126.360,0.00,1,0,"cs_trev1_blood")
					println("BLOOD VFX3")
					isplat++
				endif
			break
			case 8 
				if GET_CUTSCENE_TIME() >= 45260
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++
					
				endif
			break
			case 9
				if timerb() > 200
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.582,0.383,200.360,0.1,2,0,"cs_trev1_blood")
					println("BLOOD VFX4")
					isplat++
				endif
			break
			case 10
				if GET_CUTSCENE_TIME() >= 46600
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_para_kick_blood",<<2018.33997, 3828.17310,31.874>>,vDebugRotation)
					settimerb(0)
					isplat++	
				endif
			break
			case 11
				if timerb() > 200
					APPLY_PED_BLOOD_SPECIFIC(peds[mpf_trev].id,0,0.582,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					println("BLOOD VFX5")
					isplat++
				endif
			break
			endswitch			
//		endif
	ENDIF
	
endproc
proc LNCH_TREV_2()
TEXT_LABEL_15 sMissionName = "Trevor2"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_TREVOR
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Trevor2")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc ST_21_FBI2_MCS1_P1()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				SET_ENTITY_VISIBLE(player_ped_id(),true)
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 1372.5259, -2076.6384, 52.4951 >>, << -3.1108, 2.2664, 27.1731 >>,31.0331, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				START_CUTSCENE()				
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 2000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
			endif
			if GET_CUTSCENE_TIME() >= 77000
				SET_CAM_PARAMS(cameraIndex,<< 1372.4467, -2076.6824, 52.4986 >>, << -3.1108, 2.2664, 30.5578 >>, 31.0331, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				
				bHighDofOn = false
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 79000				
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 82000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_22_FIN_EXT_P2)
						set_up_stage(STAGE_22_FIN_EXT_P2)
						Mission_Set_Stage(STAGE_22_FIN_EXT_P2)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_21_FBI2_MCS1_P1)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if mission_stage = 2
	and IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc AST_VAULT_EXP()
	switch mission_substage
		case stage_entry
			if HAS_CUTSCENE_LOADED()
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 5292.9336, -5190.3901, 83.7388 >>, << 1.2521, -1.0201, -75.8007 >>,43, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CUTSCENE_FADE_VALUES()
				START_CUTSCENE()	
				mission_substage++
			endif
		break
		case 1
			IF IS_CUTSCENE_PLAYING()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				SET_CAM_PARAMS(cameraIndex,<< 5293.0874, -5190.3535, 83.7433 >>, << 1.6198, -1.0201, -76.6550 >>, 42.5, 1000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				mission_substage++
			endif			
		break
		case 2
			if GET_CUTSCENE_TIME() >= 1000
				SET_CAM_PARAMS(cameraIndex,<< 5292.9707, -5190.3579, 83.7424 >>, << 0.4269, -1.0201, -90.5266 >>, 43, 776, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				shake_cam(cameraIndex,"SMALL_EXPLOSION_SHAKE",0.5)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 1390
				shake_cam(cameraIndex,"HAND_SHAKE",1)
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
				if bDrawMenu = false
				and bresetScene
					if timerb() > 2000
						DO_SCREEN_FADE_OUT(0)
						if bPlayThrough	
							Load_Stage_Assets(AS_CAR_SLIDE)
							set_up_stage(AS_CAR_SLIDE)
							Mission_Set_Stage(AS_CAR_SLIDE)
							mission_substage = STAGE_ENTRY
						else						
							iSkipToStage = enum_to_int(AS_VAULT_EXP)
							bDoSkip = true					
						endif
					endif
				else
					settimerb(0)
				endif
			endif
		break
	endswitch
	BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	
endproc
proc AST_PRO_BLAST()
	switch mission_substage
		case stage_entry
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif
			if HAS_CUTSCENE_LOADED()
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 5310.2490, -5175.8232, 83.5057 >>, << -2.2273, -0.0000, -131.0285 >>,30, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CUTSCENE_FADE_VALUES()					
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_mike].id, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael",peds[mpf_mike].id)								
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_trev].id, "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor",peds[mpf_trev].id)
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				START_CUTSCENE()
				mission_substage++
			endif
		break
		case 1
			IF IS_CUTSCENE_PLAYING()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				endif
				SET_CAM_PARAMS(cameraIndex,<< 5310.2490, -5175.8232, 83.5057 >>, << -2.2273, -0.0000, -131.0285 >>,29, 590, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)												
				camfollow = CREATE_CAMERA()
				SET_CAM_PARAMS(camfollow,<< 5310.2490, -5175.8232, 83.5057 >>, << -2.2273, -0.0000, -131.0285 >>,29,0)
				SHAKE_CAM(camfollow,"hand_shake",0.6)
				mission_substage++
			endif			
		break
		case 2
			if GET_CUTSCENE_TIME() >= 590
				shake_cam(cameraIndex,"SMALL_EXPLOSION_SHAKE",0.4)
				SET_CAM_PARAMS(cameraIndex,<< 5310.2085, -5175.8662, 83.4681 >>, << -4.2610, -1.9162, -125.5088 >>,30, 670, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_door_blast", <<5318.00, -5185.06, 83.82>>, <<0.0, 0.0, 0.0>>, 1.0)
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 1260				
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.6)
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,1500)
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
			or GET_CUTSCENE_TIME() > 3200
				if bDrawMenu = false
				and bresetScene
					if timerb() > 2000
						DO_SCREEN_FADE_OUT(0)
						if bPlayThrough	
							Load_Stage_Assets(AS_CAR_SLIDE)
							set_up_stage(AS_CAR_SLIDE)
							Mission_Set_Stage(AS_CAR_SLIDE)
							mission_substage = STAGE_ENTRY
						else						
							iSkipToStage = enum_to_int(AS_PRO_BLAST)
							bDoSkip = true					
						endif
					endif
				else
					settimerb(0)
				endif
			endif
		break
	endswitch
	BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
		
endproc
proc LNCH_FRANK_1()
TEXT_LABEL_15 sMissionName = "Franklin1"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Franklin1")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc AST_SLIDE_CAR()
	switch mission_substage
		case STAGE_ENTRY
			// create car
			if not DOES_ENTITY_EXIST(vehs[veh_slide].id)
				vehs[veh_slide].id = CREATE_VEHICLE(EMPEROR,vslidecar,fslidecar)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_slide].id,4)
			endif
			
			if not does_entity_exist(peds[mpf_slide].id)
				peds[mpf_slide].id = CREATE_PED(PEDTYPE_MISSION,IG_BALLASOG,<<492.19934, -542.82751, 23.75046>>,-8.93)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_slide].id,true)
				//syncsene
				syncsceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)//<<492.630, -544.440,24.850>>,<<0,0,357.1>>) 
				if IsEntityAlive(vehs[veh_slide].id)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncsceneID,vehs[veh_slide].id,0)
				endif
				TASK_SYNCHRONIZED_SCENE(peds[mpf_slide].id,syncsceneID,"trailer2@bonnet_slide","ballasog_carbonnetslide",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
				peds[mpf_driver].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_slide].id,PEDTYPE_MISSION,S_M_Y_AIRWORKER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_driver].id,true)
				TASK_LOOK_AT_ENTITY(peds[mpf_driver].id,peds[mpf_slide].id,-1,SLF_WIDEST_PITCH_LIMIT |SLF_WIDEST_YAW_LIMIT|SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV)
			endif
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 491.1316, -532.1777, 24.7727 >>, << -0.3714, -0.0000, -167.9387 >>,15.7136, TRUE)
			SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.5)
			RENDER_SCRIPT_CAMS(true,false)
			settimera(0)
			bHighDofOn = true
			clear_area(<<492.7981, -528.7627, 24.7392 >>,50,true)
			mission_substage++	
		break
		case 1
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.200
				SET_CAM_PARAMS(cameraIndex,<< 493.4366, -532.0567, 24.8464 >>, << 0.3594, 0.0687, 178.9825 >>, 15.7136, 1250, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				if IsEntityAlive(peds[mpf_slide].id)
				and IsEntityAlive(peds[mpf_driver].id)
					TASK_LOOK_AT_ENTITY(peds[mpf_driver].id,peds[mpf_slide].id,-1)//,SLF_WIDEST_PITCH_LIMIT |SLF_WIDEST_YAW_LIMIT|SLF_FAST_TURN_RATE|SLF_WHILE_NOT_IN_FOV)			
				endif
				mission_substage++
			endif
		break
		case 2
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.393
				if IsEntityAlive(vehs[veh_slide].id)
					APPLY_FORCE_TO_ENTITY(vehs[veh_slide].id, APPLY_TYPE_IMPULSE, <<0,0,-0.2>>, <<0,3,0>>, 0, TRUE, TRUE, TRUE)
				endif
				mission_substage++
			endif
		break
		case 3
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.60
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(AS_CHOP_ON_CAR)
						set_up_stage(AS_CHOP_ON_CAR)
						Mission_Set_Stage(AS_CHOP_ON_CAR)
						mission_substage = STAGE_ENTRY
					else
						if does_entity_exist(peds[mpf_slide].id)
							delete_ped(peds[mpf_slide].id)
						endif
						DESTROY_CAM(cameraIndex)
						mission_substage = STAGE_ENTRY
					endif
				endif
			endif
		break
	endswitch
	if IsEntityAlive(peds[mpf_driver].id)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_HEAD,iDockWorkerHeadID,iDockWorkerHeadText)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_TORSO,iDockWorkerUpperID,idockworkerUpperText)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_LEG,idockworkerlowID,idockworkerlowText)
		SET_PED_PROP_INDEX(peds[mpf_driver].id,ANCHOR_HEAD,iDockPropID,idockpropText)
	endif
//	if DOES_ENTITY_EXIST(vehs[veh_slide].id)
//		if not IS_ENTITY_DEAD(vehs[veh_slide].id)
//			SET_ENTITY_COORDS(vehs[veh_slide].id,vDebugPosition)
//			set_entity_heading(vehs[veh_slide].id,fDebugHeading)
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_slide].id)
//		endif	
//	endif
endproc
proc AST_CHOP_CAR()
	switch mission_substage
		case STAGE_ENTRY
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,492.537170,-540.263794,24.426411,1.013790)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,492.545013,-539.491394,24.426411,1.013790)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,490.956055,-539.331970,24.298565,3.543797)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,490.956055,-539.331970,24.298565,10.000000)

			if not DOES_ENTITY_EXIST(vehs[veh_slide].id)
				vehs[veh_slide].id = CREATE_VEHICLE(EMPEROR,(vslidecar + <<-0.35,-2.1,0>>),fslidecar)				
				SET_VEHICLE_DOOR_OPEN(vehs[veh_slide].id,SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[veh_slide].id,4)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_slide].id)
			endif			
			if not does_entity_exist(peds[mpf_chop].id)
				peds[mpf_chop].id = CREATE_PED(PEDTYPE_MISSION,GET_CHOP_MODEL(),(<<491.1316, -532.1777, 24.7727>>+ <<-0.35,-2.1,0>>),360)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_chop].id,true)
				SET_PED_COMPONENT_VARIATION(peds[mpf_chop].id,PED_COMP_TORSO,0,0)
				//sync
				syncsceneID = CREATE_SYNCHRONIZED_SCENE((<<492.630, -544.440, 24.150>>+ <<-0.35,-2.1,0>>),<<0,0,358>>)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_chop].id,syncsceneID,"MISSFRA0_chop","FRA_0_IG_9_CHOP_JUMP_OVER_CAR_HOOD",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
			endif
			if not DOES_ENTITY_EXIST(peds[mpf_driver].id)
				peds[mpf_driver].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_slide].id,PEDTYPE_MISSION,S_M_Y_AIRWORKER)
				TASK_LOOK_AT_ENTITY(peds[mpf_driver].id,peds[mpf_chop].id,-1)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_driver].id,true)
			endif
			//bison
			if not DOES_ENTITY_EXIST(vehs[veh_parked_car].id)
				vehs[veh_parked_car].id = CREATE_VEHICLE(BISON2,<< 489.3728, -545.1403, 23.7508 >>, 86.5515)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_parked_car].id)
			endif
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",(<< 495.3479, -534.4951, 24.5051 >>+ <<-0.35,-2.1,0>>), << -1.1364, -0.0000, 145.0519 >>,23.2901, TRUE)
			RENDER_SCRIPT_CAMS(true,false)			
			bHighDofOn = true
			clear_area(<<492.7981, -528.7627, 24.7392 >>,50,true)
			mission_substage++
		break
		case 1	
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >=0.32
				DO_SCREEN_FADE_IN(0)
				if IsEntityAlive(peds[mpf_driver].id)	
				and IsEntityAlive(peds[mpf_chop].id)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(null,peds[mpf_chop].id,-1,SLF_FAST_TURN_RATE)
						TASK_LEAVE_ANY_VEHICLE(null,0,ECF_DONT_CLOSE_DOOR)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_driver].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
				endif
				SET_CAM_PARAMS(cameraIndex,(<< 495.8520, -535.4316, 24.3789 >>+ <<-0.35,-2.1,0>>), << 4.5014, 0.0000, 99.9261 >>, 23.2901, 1100, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				mission_substage++
			endif
		break
		case 2
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.45
				if DOES_ENTITY_EXIST(vehs[veh_slide].id)
				and not IS_ENTITY_DEAD(vehs[veh_slide].id)
					APPLY_FORCE_TO_ENTITY(vehs[veh_slide].id, APPLY_TYPE_IMPULSE, <<0,0,-0.1>>, <<0,3,0>>, 0, TRUE, TRUE, TRUE)
				endif
				mission_substage++
			endif
		break
		case 3
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.595
				if DOES_ENTITY_EXIST(vehs[veh_slide].id)
				and not IS_ENTITY_DEAD(vehs[veh_slide].id)
					APPLY_FORCE_TO_ENTITY(vehs[veh_slide].id, APPLY_TYPE_IMPULSE, <<0,0,-0.1>>, <<0,3,0>>, 0, TRUE, TRUE, TRUE)
				endif
				mission_substage++
			endif
		break
		case 4
			if IS_SYNCHRONIZED_SCENE_RUNNING(syncsceneID)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneID) >= 0.8			
				if not bDrawMenu 
				and bresetScene
					if timerb() >2000
						DO_SCREEN_FADE_OUT(0)				
						if bPlayThrough	
							Load_Stage_Assets(AS_FRANK_BIKE_EXP)
							set_up_stage(AS_FRANK_BIKE_EXP)
							Mission_Set_Stage(AS_FRANK_BIKE_EXP)
							mission_substage = STAGE_ENTRY
						else						
							if does_entity_exist(peds[mpf_chop].id)
								delete_ped(peds[mpf_chop].id)
							endif
							if does_entity_exist(peds[mpf_driver].id)
								delete_ped(peds[mpf_driver].id)
							endif
							mission_substage = STAGE_ENTRY
						endif
					endif
				else
					settimerb(0)
				endif			
			endif
		break
	endswitch
	if IsEntityAlive(peds[mpf_driver].id)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_HEAD,iDockWorkerHeadID,iDockWorkerHeadText)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_TORSO,iDockWorkerUpperID,idockworkerUpperText)
		SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id,PED_COMP_LEG,idockworkerlowID,idockworkerlowText)
		SET_PED_PROP_INDEX(peds[mpf_driver].id,ANCHOR_HEAD,iDockPropID,idockpropText)
	endif
endproc
proc LNCH_FBI2()
TEXT_LABEL_15 sMissionName = "Fbi2"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_MICHAEL
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				SET_ROADS_IN_AREA(<<-103.62539, -539.09808, 29.47614>>,<<318.05725, -470.30832, 35.06704>>,false)				
				mission_substage++
			endif			
		break
		case 3
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Fbi2")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				SET_ROADS_BACK_TO_ORIGINAL(<<-103.62539, -539.09808, 29.47614>>,<<318.05725, -470.30832, 35.06704>>)
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc LNCH_JEWEL_HEIST_2A()
TEXT_LABEL_15 sMissionName = "jewelry_heist"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_MICHAEL
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_heist")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc AST_BIKE_EXP()
	switch mission_substage
		case STAGE_ENTRY
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			//police cars
			vehs[Veh_police1].id = CREATE_VEHICLE(police,  << -2385.2922, -261.0016, 13.8655 >>, 315.3952 )			
			vehs[Veh_police2].id = create_vehicle(police2, << -2390.2839, -266.6570, 13.9012 >>, 340.0167 )
			vehs[Veh_police3].id = create_vehicle(police3, << -2378.5684, -258.7115, 13.7511 >>, 135.2469)
			vehs[Veh_police4].id = create_vehicle(policet,<< -2396.4150, -271.3444, 13.8323 >>, 359.7728 )
			//Ambiant cars
			vehs[veh_exp1].id = create_vehicle(sentinel2,<< -2398.7073, -263.0657, 14.1163 >>, 237.4294)
			vehs[veh_exp2].id = create_vehicle(stratum,<< -2403.5388, -265.4770, 14.1341 >>, 238.7391)
			vehs[veh_exp3].id = create_vehicle(emperor2, << -2376.6907, -263.7331, 13.6691 >>, 59.4443)
			//bike
			vehs[veh_Bike].id = create_vehicle(AKUMA, << -2416.8469, -233.8673, 15.3493 >>, 150)			
			int i
			for i = 0 to ENUM_TO_INT(MVF_NUM_OF_VEH) - 1
				if IsEntityAlive(vehs[i].id)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehs[i].id)
				endif
			endfor
			fTrafficDensity = 0
			fPedDensity = 0
			//cam
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< -2545.8171, -173.9794, 19.4473 >>, << -1.1896, -5.5740, -122.2291 >>,15.5371, TRUE)
//			SET_ENTITY_COORDS(player_ped_id(),<< -1736.5225, -944.9641, 6.7451  >>)
			SET_CAM_MOTION_BLUR_STRENGTH(cameraIndex,0.1)
			SET_CAM_ACTIVE(cameraIndex,true)
			RENDER_SCRIPT_CAMS(true,false)		
			SHAKE_CAM(cameraIndex,"hand_shake",1)
			clear_area(<<  -2552.9766, -171.2143, 19.9467>>,300,true)
			STOP_FIRE_IN_RANGE(<<  -2552.9766, -171.2143, 19.9467 >>,300)
			
			mission_substage++
		break
		case 1
			if CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_frank].id,CHAR_FRANKLIN,vehs[veh_Bike].id)
				GIVE_PED_HELMET(peds[mpf_frank].id,true)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_frank].id)
				settimera(0)
				mission_substage++
			endif
		break
		case 2
			if IsEntityAlive(peds[mpf_frank].id)
			and timera() > 1000
				if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_frank].id)
					if IsEntityAlive(vehs[veh_Bike].id)
						START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_Bike].id,6,"Trailer2REC")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_bike].id,1700)
					endif	
					SET_CAM_PARAMS(cameraIndex,<< -2546.5476, -175.2028, 19.4944 >>, << -1.1896, -5.5740, -120.9157 >>,15.5371,2500,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					settimera(0)
					mission_substage++
				endif
			endif
		break
		case 3
			if timera() > 400
				DO_SCREEN_FADE_IN(0)
				camfollow = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< -2546.5476, -175.2028, 19.4944 >>, << -1.1896, -5.5740, -120.9157 >>,16)
				SET_CAM_MOTION_BLUR_STRENGTH(camfollow,0.6)
				if IsEntityAlive(vehs[veh_Bike].id)
					POINT_CAM_AT_ENTITY(camfollow,vehs[veh_Bike].id,<<0,0.3,0.1>>)
				endif
				if IsEntityAlive(vehs[Veh_police1].id)
					ADD_EXPLOSION(GET_ENTITY_COORDS(vehs[Veh_police1].id),EXP_TAG_CAR,1)
				endif
				if IsEntityAlive(vehs[Veh_police2].id)
					ADD_EXPLOSION(get_entity_coords(vehs[Veh_police2].id),EXP_TAG_CAR,1)
				endif
				if IsEntityAlive(vehs[Veh_police4].id)
					ADD_EXPLOSION(get_entity_coords(vehs[Veh_police4].id),EXP_TAG_CAR,1)
				endif
				ADD_EXPLOSION(<<-2389.32544, -260.25778, 13.95412>>,EXP_TAG_HI_OCTANE)
				ADD_EXPLOSION(<<-2392.40527, -262.88516, 13.99207>>,EXP_TAG_HI_OCTANE)
				mission_substage++
			endif
		break
		case 4
			if timera() > 900
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex, 2000)
				mission_substage++
			endif
		break
		case 5
			if timera() > 2000
				if not bDrawMenu 
				and bresetScene
					if timerb() >2000
						DO_SCREEN_FADE_OUT(0)				
						if bPlayThrough	
							Load_Stage_Assets(AS_JET_FIGHT)
							set_up_stage(AS_JET_FIGHT)
							Mission_Set_Stage(AS_JET_FIGHT)
							mission_substage = STAGE_ENTRY
						else	
							if DOES_ENTITY_EXIST(peds[mpf_frank].id)
								DELETE_PED(peds[mpf_frank].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_Bike].id)
								delete_vehicle(vehs[veh_Bike].id)
							endif
							if DOES_ENTITY_EXIST(vehs[Veh_police2].id)
								delete_vehicle(vehs[Veh_police2].id)
							endif
							if DOES_ENTITY_EXIST(vehs[Veh_police3].id)
								delete_vehicle(vehs[Veh_police3].id)
							endif
							if DOES_ENTITY_EXIST(vehs[Veh_police4].id)
								delete_vehicle(vehs[Veh_police4].id)
							endif
							if DOES_ENTITY_EXIST(vehs[Veh_police1].id)
								delete_vehicle(vehs[Veh_police1].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_exp1].id)
								delete_vehicle(vehs[veh_exp1].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_exp2].id)
								delete_vehicle(vehs[veh_exp2].id)
							endif
							if DOES_ENTITY_EXIST(vehs[veh_exp3].id)
								delete_vehicle(vehs[veh_exp3].id)
							endif
							DESTROY_ALL_CAMS()
							mission_substage = STAGE_ENTRY
						endif
					endif
				else
					settimerb(0)
				endif
			endif
		break
	endswitch
endproc
proc LNCH_EXILE_1()
TEXT_LABEL_15 sMissionName = "exile1"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2		
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("exile1")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc LNCH_Offroad_1()
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_FRANKLIN
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1		
			if IsEntityAlive(player_ped_id())
				SET_ENTITY_COORDS(player_ped_id(),<<-1958.1558, 4463.4121, 33.7800>>)
				Mission_substage++
			endif			
		break		
		case 2
			if IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(800)
				SET_CLOCK_TIME(13,0,0)
				mission_substage++
			endif
		break
		case 3
			Mission_Cleanup(true)
		break
	endswitch
endproc
proc LNCH_EXILE_3()
TEXT_LABEL_15 sMissionName = "exile3"
	switch mission_substage
		case STAGE_ENTRY
			if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_TREVOR
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
		 		ENDWHILE	
			endif		
			mission_substage++
		break
		case 1
			REQUEST_SCRIPT(sMissionName)
		    IF HAS_SCRIPT_LOADED(sMissionName)
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)	
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
					Mission_substage++
				ENDIF
			endif	
		break
		case 2
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(0)
				mission_substage++
			endif			
		break
		case 3
			if IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
			endif
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("exile3")) = 0					
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
				bDoSkip = true			
			ENDIF
		break
	endswitch
endproc
proc AST_JET_FIGHT()
	switch mission_substage
		case STAGE_ENTRY			
			cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 683.8909, 1242.1932, 359.0270 >>, << -11.9587, 3.4700, 177.9113 >>,31.3, TRUE)
			NEW_LOAD_SCENE_START(<< 683.8909, 1242.1932, 359.0270 >>, << -11.9587, 3.4700, 177.9113 >>,150)
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			ENABLE_EXTRA_DISTANTLIGHTS(<<383,-903,50.9>>,1000)				
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")			
			settimera(0)
			if not DOES_ENTITY_EXIST(vehs[veh_jet].id)
				vehs[veh_jet].id = CREATE_VEHICLE(LAZER,<< 723.2245, 1249.4069, 363.7921 >>,180)				
				SET_VEHICLE_ENGINE_ON(vehs[veh_jet].id,true,true)
				CONTROL_LANDING_GEAR(vehs[veh_jet].id,LGC_RETRACT_INSTANT)
				SET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehs[veh_jet].id,0)
				SET_ENTITY_INVINCIBLE(vehs[veh_jet].id,true)
				SET_ENTITY_LOD_DIST(vehs[veh_jet].id,1000)
				if not DOES_ENTITY_EXIST(peds[mpf_jetPilot].id)
					peds[mpf_jetPilot].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_jet].id,pedtype_mission,S_M_Y_Pilot_01)
				endif					
			endif
			
			mission_substage++
		break
		case 1
			if timera() > 1000
				DO_SCREEN_FADE_IN(0)				
				//heli 1
				if not DOES_ENTITY_EXIST(vehs[veh_heli1].id)
					vehs[veh_heli1].id = create_vehicle(polmav,<< 551.2347, 873.2770, 331.1218 >>)
					SET_HELI_BLADES_FULL_SPEED(vehs[veh_heli1].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id,1,"Trailer2REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id,800)
					SET_PLAYBACK_SPEED(vehs[veh_heli1].id,1.1)
					SET_ENTITY_ALWAYS_PRERENDER(vehs[veh_heli1].id,true)
					SET_ENTITY_LOD_DIST(vehs[veh_heli1].id,1000)
					SET_VEHICLE_SEARCHLIGHT(vehs[veh_heli1].id,true)
					if not DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
						peds[mpf_pilot1].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_heli1].id,pedtype_mission,S_M_Y_Pilot_01)
						TASK_VEHICLE_AIM_AT_COORD(peds[mpf_pilot1].id,<<565.02087, 807.79779, 200.03485>>)
					endif
				endif
				
				//heli 2
				if not DOES_ENTITY_EXIST(vehs[veh_heli2].id)
					vehs[veh_heli2].id = create_vehicle(polmav,<< 516.6041, 773.4421, 300.7780 >>)
					SET_HELI_BLADES_FULL_SPEED(vehs[veh_heli2].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli2].id,2,"Trailer2REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli2].id,800)
					SET_PLAYBACK_SPEED(vehs[veh_heli2].id,1.1)
					SET_ENTITY_LOD_DIST(vehs[veh_heli2].id,1000)
					SET_VEHICLE_SEARCHLIGHT(vehs[veh_heli2].id,true)
					if not DOES_ENTITY_EXIST(peds[mpf_pilot2].id)
						peds[mpf_pilot2].id = CREATE_PED_INSIDE_VEHICLE(vehs[veh_heli2].id,pedtype_mission,S_M_Y_Pilot_01)
						TASK_VEHICLE_AIM_AT_COORD(peds[mpf_pilot2].id,<<565.02087, 807.79779, 200.03485>>)
					endif
				endif
				//jet
				if IsEntityAlive(vehs[veh_jet].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_jet].id,3,"Trailer2REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_jet].id,2500)
					SET_PLAYBACK_SPEED(vehs[veh_jet].id,1.35)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehs[veh_jet].id,0)
				endif
				//cam
				SET_CAM_PARAMS(cameraIndex,<< 687.4604, 1242.0137, 359.3400 >>, << -9.7147, 3.4577, 178.6762 >>,31.3,1500,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(true,false)		
				SHAKE_CAM(cameraIndex,"hand_shake",1)
				clear_area(<< 686.7050, 1222.2231, 353.9688 >>,2000,true)
				STOP_FIRE_IN_RANGE(<< 686.7050, 1222.2231, 353.9688 >>,2000)
				settimera(0)
					
				FREEZE_ENTITY_POSITION(player_ped_id(),true)
				mission_substage++
			endif
		break
		case 2
			if timera() > 800
				camfollow = CREATE_CAM_WITH_PARAMS("default_scripted_camera",<< 687.4604, 1242.0137, 359.3400 >>, << -9.7147, 3.4577, 178.6762 >>,30)
				POINT_CAM_AT_ENTITY(camfollow,vehs[veh_jet].id,<<0,0,0>>)
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,1500)				
				mission_substage++
			endif
		break 
		case 3
			if timera() > 2000
			
//				NEW_LOAD_SCENE_START(<< 547.9744, 793.1989, 409.4529 >>, << 0.3927, -3.5066, -17.2993 >>,250)
//				while not IS_NEW_LOAD_SCENE_LOADED()
//					wait(0)
//				endwhile
				SET_CAM_PARAMS(cameraIndex,<< 547.9744, 793.1989, 409.4529 >>, << 0.3927, -3.5066, -17.2993 >>,29.7407,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(cameraIndex,<< 538.0171, 797.1656, 411.7323 >>, << -2.9483, -2.4574, -26.3961 >>,29.7407,2300,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_ACTIVE(cameraIndex,true)
				settimerA(0)
				SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")	
				//heli 				
				if IsEntityAlive(vehs[veh_heli1].id)
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[veh_heli1].id)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id)					
					endif
					SET_HELI_BLADES_FULL_SPEED(vehs[veh_heli1].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id,5,"Trailer2REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id,2300)
					SET_PLAYBACK_SPEED(vehs[veh_heli1].id,0.85)
					SET_ENTITY_HEALTH(vehs[veh_heli1].id,20)
					SET_VEHICLE_SEARCHLIGHT(vehs[veh_heli1].id,false)
				endif						
				if IsEntityAlive(vehs[veh_jet].id)
					//jet
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[veh_jet].id)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehs[veh_jet].id)
					endif
					START_PLAYBACK_RECORDED_VEHICLE(vehs[veh_jet].id,4,"Trailer2REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[veh_jet].id,1400)
					SET_PLAYBACK_SPEED(vehs[veh_jet].id,1)
					SET_VEHICLE_ENGINE_ON(vehs[veh_jet].id,true,true)
					CONTROL_LANDING_GEAR(vehs[veh_jet].id,LGC_RETRACT_INSTANT)
				endif
				if IsEntityAlive(peds[mpf_jetPilot].id)
				and IsEntityAlive(peds[mpf_pilot1].id)
					SET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_jetPilot].id,WEAPONTYPE_VEHICLE_SPACE_ROCKET)
					TASK_VEHICLE_SHOOT_AT_PED(peds[mpf_jetPilot].id,peds[mpf_pilot1].id)
				endif
				//get rid of other heli
				if DOES_ENTITY_EXIST(peds[mpf_pilot2].id)
					delete_ped(peds[mpf_pilot2].id)	
				endif
				
				if DOES_ENTITY_EXIST(vehs[veh_heli2].id)
				and not IS_ENTITY_DEAD(vehs[veh_heli2].id)
					DELETE_VEHICLE(vehs[veh_heli2].id)
				endif
				
				mission_substage++
			ENDIF
		break
		case 4
			if timera() > 2000
				SET_CAM_PARAMS(camfollow,<< 538.0171, 797.1656, 411.7323 >>, << -2.9483, -2.4574, -26.3961 >>,34,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,1000)//2300)
				mission_substage++
			endif
		break
		case 5
			if timera() > 2500
				SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
				mission_substage++
			endif
		break
		case 6
			if timera() > 3500	
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)				
					if bPlayThrough	
						Load_Stage_Assets(STAGE_22_FIN_EXT_P2)
						set_up_stage(STAGE_22_FIN_EXT_P2)
						Mission_Set_Stage(STAGE_22_FIN_EXT_P2)
						mission_substage = STAGE_ENTRY
					else
						if DOES_ENTITY_EXIST(peds[mpf_pilot1].id)
							delete_ped(peds[mpf_pilot1].id)	
						endif
						if DOES_ENTITY_EXIST(vehs[veh_heli1].id)
							DELETE_VEHICLE(vehs[veh_heli1].id)
						endif
						if DOES_ENTITY_EXIST(peds[mpf_pilot2].id)
							delete_ped(peds[mpf_pilot2].id)	
						endif
						if DOES_ENTITY_EXIST(vehs[veh_heli2].id)
							DELETE_VEHICLE(vehs[veh_heli2].id)
						endif
						if DOES_ENTITY_EXIST(peds[mpf_jetPilot].id)
							delete_ped(peds[mpf_jetPilot].id)	
						endif
						if DOES_ENTITY_EXIST(vehs[veh_jet].id)
							DELETE_VEHICLE(vehs[veh_jet].id)
						endif
						DESTROY_ALL_CAMS()
						CLEAR_FOCUS()
						mission_substage = STAGE_ENTRY
					endif	
				endif
			endif
		break
	endswitch 
	if IsEntityAlive(vehs[veh_heli1].id)
	and IsEntityAlive(peds[mpf_jetPilot].id)
		if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[veh_heli1].id,peds[mpf_jetPilot].id)
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[veh_heli1].id)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehs[veh_heli1].id)			
				SET_ENTITY_HEALTH(vehs[veh_heli1].id,0)
			endif
		endif
	endif
	if DOES_CAM_EXIST(cameraIndex)
		SET_FOCUS_POS_AND_VEL(GET_CAM_COORD(cameraIndex),<<0,0,0>>)
	endif	
endproc
proc ST_22_FIN_EXT_P2() //therepist has a lot to answer for
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				SET_ENTITY_VISIBLE(player_ped_id(),true)
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 1376.4539, -2062.7883, 51.4848 >>, << 7.5267, -0.0000, -14.6135 >>,24.7441, TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1377.702026,-2057.952881,51.962540,1.200000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1378.937988,-2049.780762,52.548439,7.243392)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1386.948975,-2031.549194,53.900768,20.271351)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1413.269043,-1985.094849,62.787762,70.397774)
				RENDER_SCRIPT_CAMS(true,false)
				START_CUTSCENE_AT_COORDS(<<1352.650,-2056.3,51.05>>)
				SET_CUTSCENE_ORIGIN(<<1367.650,-2041.300,51.050>>,62.436,0)
				if not DOES_ENTITY_EXIST(vehs[veh_heli1].id)
					vehs[veh_heli1].id = CREATE_VEHICLE(BUZZARD,<< 1379.3744, -2054.3933, 50.9981 >>, 225.8938)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehs[veh_heli1].id)					
				endif
				//camera
				SETTIMERA(0)
				mission_substage++
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break
		case 2
			if GET_CUTSCENE_TIME() >= 45000
				SET_CAM_PARAMS(cameraIndex,<< 1376.5262, -2062.5112, 51.5226 >>, << 7.5267, -0.0000, -14.6135 >>, 24.7441, 13000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)												
				mission_substage++
			endif
		break
		case 3
			if GET_CUTSCENE_TIME() >= 57000				
				STOP_CUTSCENE_IMMEDIATELY()
				mission_substage++
			endif
		break
		case 4
			if HAS_CUTSCENE_FINISHED()	
				if not bDrawMenu 
				and bresetScene
				DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						Load_Stage_Assets(STAGE_23_FINAL_SHOT)
						set_up_stage(STAGE_23_FINAL_SHOT)
						Mission_Set_Stage(STAGE_23_FINAL_SHOT)
						mission_substage = STAGE_ENTRY
					else	
						iSkipToStage = enum_to_int(STAGE_22_FIN_EXT_P2)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if mission_substage > 2
	and IS_CUTSCENE_PLAYING()
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc ST_23_FINAL_SHOT()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				bplayerControl = true
				if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_MICHAEL
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
						WAIT(0)
			 		ENDWHILE	
				endif
				if IsEntityAlive(player_ped_id())
					SET_ENTITY_VISIBLE(player_ped_id(),true)
					SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0,0)
					SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)
				endif
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<1372.5806, -2071.8660, 50.8653>>, <<23.2888, -0.0000, -165.3118>>,27.3521, TRUE)			

				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)	
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1373.540039,-2075.073730,51.945171,1.161457)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1373.540283,-2075.745361,51.945168,1.161457)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1403.187012,-2045.066650,56.329720,5.764066)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1380.006958,-2080.055908,53.470879,11.786053)
				
				SET_CAM_DOF_STRENGTH(cameraIndex,0)
				bHighDofOn = true
				SET_CAM_NEAR_DOF(cameraIndex,3.260)
				SET_CAM_FAR_DOF(cameraIndex,500)
				SET_CAM_USE_SHALLOW_DOF_MODE(cameraIndex,true)
				
				START_CUTSCENE()
				SET_CUTSCENE_ORIGIN(<<1382.56,-2059.8,51>>,-120.5,0)
				mission_substage++	
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break		
		case 2
			if HAS_CUTSCENE_FINISHED()
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
						bDoSkip = true
					else	
						iSkipToStage = enum_to_int(STAGE_23_FINAL_SHOT)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if GET_CUTSCENE_TIME() >= 43630
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
proc ST_23_FINAL_SHOT_ALT()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				bplayerControl = true
				if GET_PLAYER_PED_ENUM(player_ped_id()) != CHAR_MICHAEL
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
						WAIT(0)
			 		ENDWHILE	
				endif
				if IsEntityAlive(player_ped_id())
					SET_ENTITY_VISIBLE(player_ped_id(),true)
					SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL,0,0)
					SET_PED_COMPONENT_VARIATION(player_ped_id(),PED_COMP_SPECIAL2,0,0)
				endif
				cameraIndex = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<1372.9314, -2073.5801, 52.1832>>, <<7.0716, -0.0000, -168.5246>>,45, TRUE)			
				
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)	
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1373.540039,-2075.073730,51.945171,1.161457)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1373.540283,-2075.745361,51.945168,1.161457)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1403.187012,-2045.066650,56.329720,5.764066)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1380.006958,-2080.055908,53.470879,11.786053)
				
				SET_CAM_DOF_STRENGTH(cameraIndex,1)
				bHighDofOn = true
				SET_CAM_NEAR_DOF(cameraIndex,1)
				SET_CAM_FAR_DOF(cameraIndex,15)
				
				START_CUTSCENE()
				SET_CUTSCENE_ORIGIN(<<1382.56,-2059.8,51>>,-120.5,0)
				mission_substage++	
			endif
		break
		case 1
			if GET_CUTSCENE_TIME() >= 1000
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
				mission_substage++
			endif
		break		
		case 2
			if HAS_CUTSCENE_FINISHED()
				if not bDrawMenu 
				and bresetScene
					DO_SCREEN_FADE_OUT(0)
					if bPlayThrough	
						iSkipToStage = enum_to_int(STAGE_0_WAIT_FOR_STAGE)
						bDoSkip = true
					else	
						iSkipToStage = enum_to_int(STAGE_23_FINAL_SHOT)
						bDoSkip = true
					endif
				endif
			endif
		break
	endswitch
	if GET_CUTSCENE_TIME() >= 43630
		BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
	endif
endproc
//	STAGE_RECORD,

PROC MISSION_STAGE_SKIP()
//a skip has been made
	IF bDoSkip = TRUE
		
		//begin the skip if the switching is idle
		if stageSwitch = STAGESWITCH_IDLE
			if not IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(1000)
				ENDIF
			else
				Mission_Set_Stage(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, iSkipToStage))
			endif
		//Needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(false,false)
			
			RESET_EVERYTHING()
						
			Load_Stage_Assets(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
			Set_up_stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
			
			iTitlesTimer = GET_GAME_TIMER()			
			bDoSkip = FALSE
			
		ENDIF
#if Is_debug_build
	//Check is a skip being asked for, dont allow skip during setup stage
	ELIF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		if LAUNCH_MISSION_STAGE_MENU(zMenuNames, iSkipToStage, mission_stage, false,"Trailer 2 Shot List")
			IF iSkipToStage > ENUM_TO_INT(STAGE_NUM_LAST)-1
				Mission_Set_Stage(STAGE_0_WAIT_FOR_STAGE)			
			ELSE
				iSkipToStage = CLAMP_INT(iSkipToStage, 0, ENUM_TO_INT(STAGE_NUM_LAST)-1)
				if IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_OUT(0)
					bDoSkip = true
				endif
			endif
		endif
#endif
	ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		
		case STAGE_0_WAIT_FOR_STAGE 			
			ST_0_WAIT_FOR_STAGE() 				
		break
		case STAGE_1_RUN_THROUGH_SHOTS
			bPlayThrough = true
			bDoSkip = true
			iSkipToStage = ENUM_TO_INT(STAGE_2_OBSERVATORY_SHOT) 			
		break
		
		case STAGE_2_OBSERVATORY_SHOT 		st_2_OBSERVATORY_SHOT() 	break
		case STAGE_2_OBSERVATORY_SHOT_alt 	ST_2_OBSERVATORY_SHOT_ALT() break
		case STAGE_3_FAM2_INT_P1			ST_3_FAM2_INT_P1() 			break
		case STAGE_4_DRIVEWAY_SHOT			ST_4_DRIVEWAY_SHOT() 		break
		case STAGE_5_FAM1_INT_P1			ST_5_FAM1_INT_P1() 			break
		case STAGE_6_CITY_TRAIN_SHOT		ST_6_CITY_TRAIN_SHOT() 		break
		case STAGE_7_COUNTRY_SCAPE_SHOT		ST_7_COUNTRY_SCAPE_SHOT() 	break
		case STAGE_8_LIQUOR_ACE_SHOT		ST_8_LIQUOR_ACE_SHOT() 		break
		case STAGE_9_TREV1_MCS3_P2_A		ST_9_TREV1_MCS3_P2_A() 		break
		case STAGE_10_CHIN1_INT_P2			ST_10_CHIN1_INT_P2() 		break
		case STAGE_11_TREV1_MCS3_P2_B 		ST_11_TREV1_MCS3_P2_B() 	break		
		case STAGE_12_CHIN1_MCS4			ST_12_CHIN1_MCS4()			break
		case STAGE_13_DOCKS_BRIDGE_SHOT		ST_13_DOCKS_BRIDGE_SHOT()	break
		case STAGE_13_DOCKS_BRIDGE_SHOT_ALT	ST_13_DOCKS_BRIDGE_SHOT_ALT ()break
		case STAGE_14_GHETTO_TRAIN_SHOT		ST_14_GHETTO_TRAIN_SHOT()	break
		case STAGE_15_ARM1_INT_P3			ST_15_ARM1_INT_P3()			break
		case STAGE_16_FRA2_INT_P2			ST_16_FRA2_INT_P2()			break			
		case STAGE_17_ARM1_INT_P4			ST_17_ARM1_INT_P4()			break
		case STAGE_17_ARM1_INT_P4_ALT		ST_17_ARM1_INT_P4_ALT()		break
		case STAGE_18_ARM1_INT_P1			ST_18_ARM1_INT_P1()			break
		case STAGE_19_FAM5_MCS5				ST_19_FAM5_MCS5()			break
		case STAGE_20_TREV1_INT_P4			ST_20_TREV1_INT_P4()		break
		case STAGE_21_FBI2_MCS1_P1			ST_21_FBI2_MCS1_P1()		break
		case STAGE_22_FIN_EXT_P2			ST_22_FIN_EXT_P2()			break
		case STAGE_23_FINAL_SHOT			ST_23_FINAL_SHOT()			break	
		case STAGE_23_FINAL_SHOT_ALT		ST_23_FINAL_SHOT_ALT()		break	
		//action shots
		case AS_BUGGY						AST_BUGGY()					break
		case AS_TREV_MOL					AST_TREV_MOL()				break
		case AS_FIRE_TRAIL					AST_FIRE_TRAIL()			break
		case AS_BARN_BOOM					AST_BARN_BLOW()				break
		case AS_VAULT_EXP					AST_VAULT_EXP()				break		
		case AS_PRO_BLAST					AST_PRO_BLAST()				break
		case AS_CAR_SLIDE					AST_SLIDE_CAR()				break
		case AS_CHOP_ON_CAR					AST_CHOP_CAR()				break
		case AS_FRANK_BIKE_EXP				AST_BIKE_EXP()				break
		case AS_JET_FIGHT					AST_JET_FIGHT()				break
		// Mission launching
		case AS_LAUNCH_ARM1					LNCH_ARM_1()				break
		case AS_LAUNCH_CAR_STEAL_3			LNCH_CAR_STEAL_3()			break
		case AS_LAUNCH_CAR_STEAL_FINALE		LNCH_CAR_STEAL_FINALE()		break
		case AS_LAUNCH_EXILE_1				LNCH_EXILE_1()				break
		case AS_LAUNCH_EXILE_3				LNCH_EXILE_3()				break
		case AS_LAUNCH_FAMILY_1				LNCH_FAM_1()				break
		case AS_LAUNCH_FBI_2				LNCH_FBI2()					break
		case AS_LAUNCH_FRANK_1				LNCH_FRANK_1()				break
		case AS_LAUNCH_JEWEL_2A				LNCH_JEWEL_HEIST_2A()		break
		case AS_LAUNCH_OFFROAD_RACE_1		LNCH_Offroad_1()			break
		case AS_LAUNCH_TREV2				LNCH_TREV_2()				break
		
	endswitch
ENDPROC
Proc trailer_checks()
	
	//death check
	int i
	for i = 0 to enum_to_int(MPF_NUM_OF_PEDS) -1
		if DOES_ENTITY_EXIST(peds[i].id)
			if IS_PED_INJURED(peds[i].id)
				SET_PED_AS_NO_LONGER_NEEDED(peds[i].id) 
			ENDIF
		ENDIF
	ENDFOR
	for i = 0 to enum_to_int(MVF_NUM_OF_VEH) -1
		if DOES_ENTITY_EXIST(vehs[i].id)
			if not IS_VEHICLE_DRIVEABLE(vehs[i].id)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
			ENDIF
		ENDIF
	ENDFOR
	//
	CONTROL_GAS_TRAILS()
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		if bresetScene
			bresetScene = false
		else
			bresetScene = true
		endif
	ENDIF
	
	if mission_stage > enum_to_int(STAGE_0_WAIT_FOR_STAGE)
	and bWideScreen
	and not bDrawMenu
		DRAW_RECT_FROM_CORNER(0,0,1,0.1,0,0,0,255)
		DRAW_RECT_FROM_CORNER(0,0.9,1,0.1,0,0,0,255)
	endif
	IF bHighDofOn
		SET_USE_HI_DOF()
	ENDIF
	
	IF mission_stage <> enum_to_int(STAGE_0_WAIT_FOR_STAGE)
	AND mission_Stage <> enum_to_int(STAGE_1_RUN_THROUGH_SHOTS)
	AND bDisplayTitles
		IF GET_GAME_TIMER() - iTitlesTimer < 2500
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.04, "STRING", zMenuNames[mission_stage].sTxtLabel)
		ENDIF
	ENDIF
	
	if DOES_CAM_EXIST(cameraIndex)
		vCamPOS = GET_CAM_COORD(cameraIndex)
		vCamROT = GET_CAM_ROT(cameraIndex)
		fCamFOV = GET_CAM_FOV(cameraIndex)
	else
		vCamPOS = <<0,0,0>>
		fCamFOV =0
	endif
	if DOES_CAM_EXIST(GET_DEBUG_CAM())
		vdebugcamPOS= GET_CAM_COORD(GET_DEBUG_CAM())
		vdebugCamROT= get_cam_rot(GET_DEBUG_CAM())
		fdebugcamFOV= GET_CAM_FOV(GET_DEBUG_CAM())
	endif
	
	if bClearArea
		bClearArea = false
		CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),500,true)
	endif
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)	
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(fPedDensity)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fTrafficDensity)
	
	if IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		bDrawMenu = false
		SET_PLAYER_CONTROL(player_id(),true)
		SET_ENTITY_INVINCIBLE(player_ped_id(),false)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)	
	else
		if bplayerControl
			SET_PLAYER_CONTROL(player_id(),true)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)	
		else
			SET_PLAYER_CONTROL(player_id(),false)
		endif
		SET_ENTITY_INVINCIBLE(player_ped_id(),true)
	endif
		
	if mission_stage = enum_to_int(AS_LAUNCH_FBI_2)	
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(4)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(4)
		SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(4)				
	endif	
	
	OVERRIDE_LODSCALE_THIS_FRAME(fLODscale)
	g_bTrailer2Active = true
endproc
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Trailer 2 Launched")
	PRINTNL()
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		PRINTSTRING("...Trailer 2 clean up")
		Mission_Cleanup(true)		
	ENDIF
	
//	SET_MISSION_FLAG(TRUE)
	
	MISSION_SETUP()
	
	WHILE (TRUE)
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()
		mission_flow()	
		trailer_checks()
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
				Mission_Cleanup(true)					
			endif
		ENDIF
		if not IsEntityAlive(player_ped_id())
			Mission_Cleanup(true)
		endif
		
		WAIT(0)
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

#ENDIF

