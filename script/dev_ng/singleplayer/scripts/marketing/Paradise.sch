PROC  setupTrafficParadise2()


TrafficCarPos[0] = <<499.5055, 63.5685, 95.6534>>
TrafficCarQuatX[0] = 0.0216
TrafficCarQuatY[0] = -0.0042
TrafficCarQuatZ[0] = -0.1858
TrafficCarQuatW[0] = 0.9823
TrafficCarRecording[0] = 2
TrafficCarStartime[0] = 727643.0000
TrafficCarModel[0] = baller //patriot

TrafficCarPos[1] = <<496.7310, 56.5013, 94.8041>>
TrafficCarQuatX[1] = 0.0643
TrafficCarQuatY[1] = -0.0108
TrafficCarQuatZ[1] = -0.1846
TrafficCarQuatW[1] = 0.9806
TrafficCarRecording[1] = 3
TrafficCarStartime[1] = 2112.0000
TrafficCarModel[1] = emperor2

TrafficCarPos[2] = <<505.7717, 50.3505, 94.6112>>
TrafficCarQuatX[2] = 0.0652
TrafficCarQuatY[2] = -0.0109
TrafficCarQuatZ[2] = -0.1755
TrafficCarQuatW[2] = 0.9823
TrafficCarRecording[2] = 4
TrafficCarStartime[2] = 2706.0000
TrafficCarModel[2] = baller //patriot

TrafficCarPos[3] = <<494.0019, 49.4080, 93.6806>>
TrafficCarQuatX[3] = 0.0737
TrafficCarQuatY[3] = -0.0129
TrafficCarQuatZ[3] = -0.1817
TrafficCarQuatW[3] = 0.9805
TrafficCarRecording[3] = 5
TrafficCarStartime[3] = 3366.0000
TrafficCarModel[3] = emperor2

TrafficCarPos[4] = <<499.9030, 35.9050, 92.3417>>
TrafficCarQuatX[4] = 0.0649
TrafficCarQuatY[4] = -0.0138
TrafficCarQuatZ[4] = -0.2138
TrafficCarQuatW[4] = 0.9746
TrafficCarRecording[4] = 6
TrafficCarStartime[4] = 4818.0000
TrafficCarModel[4] = camper

TrafficCarPos[5] = <<487.4920, 32.3174, 90.9806>>
TrafficCarQuatX[5] = 0.0808
TrafficCarQuatY[5] = -0.0152
TrafficCarQuatZ[5] = -0.1659
TrafficCarQuatW[5] = 0.9827
TrafficCarRecording[5] = 7
TrafficCarStartime[5] = 5742.0000
TrafficCarModel[5] = buffalo

TrafficCarPos[6] = <<456.8145, 97.4314, 98.1378>>
TrafficCarQuatX[6] = 0.0104
TrafficCarQuatY[6] = -0.0152
TrafficCarQuatZ[6] = 0.8184
TrafficCarQuatW[6] = -0.5743
TrafficCarRecording[6] = 8
TrafficCarStartime[6] = 7986.0000
TrafficCarModel[6] = asterope

TrafficCarPos[7] = <<427.0568, 108.2706, 99.8678>>
TrafficCarQuatX[7] = 0.0104
TrafficCarQuatY[7] = -0.0264
TrafficCarQuatZ[7] = 0.8189
TrafficCarQuatW[7] = -0.5732
TrafficCarRecording[7] = 9
TrafficCarStartime[7] = 8316.0000
TrafficCarModel[7] = asterope

TrafficCarPos[8] = <<445.9427, 101.4947, 98.7527>>
TrafficCarQuatX[8] = 0.0129
TrafficCarQuatY[8] = -0.0187
TrafficCarQuatZ[8] = 0.8204
TrafficCarQuatW[8] = -0.5713
TrafficCarRecording[8] = 10
TrafficCarStartime[8] = 8514.0000
TrafficCarModel[8] = asterope

TrafficCarPos[9] = <<420.3567, 110.7781, 100.4330>>
TrafficCarQuatX[9] = 0.0173
TrafficCarQuatY[9] = -0.0270
TrafficCarQuatZ[9] = 0.8243
TrafficCarQuatW[9] = -0.5653
TrafficCarRecording[9] = 11
TrafficCarStartime[9] = 8844.0000
TrafficCarModel[9] = baller //patriot

TrafficCarPos[10] = <<403.5013, 132.7016, 101.5761>>
TrafficCarQuatX[10] = 0.0236
TrafficCarQuatY[10] = 0.0168
TrafficCarQuatZ[10] = 0.5753
TrafficCarQuatW[10] = 0.8174
TrafficCarRecording[10] = 12
TrafficCarStartime[10] = 11748.0000
TrafficCarModel[10] = buffalo

TrafficCarPos[11] = <<393.2503, 136.3298, 102.0354>>
TrafficCarQuatX[11] = 0.0205
TrafficCarQuatY[11] = 0.0140
TrafficCarQuatZ[11] = 0.5733
TrafficCarQuatW[11] = 0.8190
TrafficCarRecording[11] = 13
TrafficCarStartime[11] = 13398.0000
TrafficCarModel[11] = emperor2

TrafficCarPos[12] = <<351.2957, 131.5213, 102.6416>>
TrafficCarQuatX[12] = 0.0032
TrafficCarQuatY[12] = 0.0073
TrafficCarQuatZ[12] = 0.9999
TrafficCarQuatW[12] = 0.0105
TrafficCarRecording[12] = 14
TrafficCarStartime[12] = 18414.0000
TrafficCarModel[12] = emperor2

TrafficCarPos[13] = <<346.9432, 138.7514, 102.5528>>
TrafficCarQuatX[13] = -0.0024
TrafficCarQuatY[13] = 0.0027
TrafficCarQuatZ[13] = 0.8047
TrafficCarQuatW[13] = -0.5937
TrafficCarRecording[13] = 15
TrafficCarStartime[13] = 19206.0000
TrafficCarModel[13] = emperor2

TrafficCarPos[14] = <<333.1916, 144.4895, 102.7615>>
TrafficCarQuatX[14] = 0.0031
TrafficCarQuatY[14] = -0.0044
TrafficCarQuatZ[14] = 0.8149
TrafficCarQuatW[14] = -0.5796
TrafficCarRecording[14] = 16
TrafficCarStartime[14] = 20856.0000
TrafficCarModel[14] = asterope

TrafficCarPos[15] = <<325.9463, 147.0476, 103.1638>>
TrafficCarQuatX[15] = 0.0035
TrafficCarQuatY[15] = -0.0055
TrafficCarQuatZ[15] = 0.8186
TrafficCarQuatW[15] = -0.5743
TrafficCarRecording[15] = 17
TrafficCarStartime[15] = 21714.0000
TrafficCarModel[15] = baller //patriot

TrafficCarPos[16] = <<319.1542, 149.5604, 103.1892>>
TrafficCarQuatX[16] = 0.0014
TrafficCarQuatY[16] = -0.0027
TrafficCarQuatZ[16] = 0.8224
TrafficCarQuatW[16] = -0.5689
TrafficCarRecording[16] = 18
TrafficCarStartime[16] = 22572.0000
TrafficCarModel[16] = buffalo

TrafficCarPos[17] = <<311.6988, 152.3810, 103.4705>>
TrafficCarQuatX[17] = 0.0064
TrafficCarQuatY[17] = -0.0096
TrafficCarQuatZ[17] = 0.8239
TrafficCarQuatW[17] = -0.5666
TrafficCarRecording[17] = 19
TrafficCarStartime[17] = 23430.0000
TrafficCarModel[17] = baller //patriot

TrafficCarPos[18] = <<306.3315, 154.4167, 103.3814>>
TrafficCarQuatX[18] = 0.0028
TrafficCarQuatY[18] = -0.0041
TrafficCarQuatZ[18] = 0.8236
TrafficCarQuatW[18] = -0.5672
TrafficCarRecording[18] = 20
TrafficCarStartime[18] = 24222.0000
TrafficCarModel[18] = asterope

TrafficCarPos[19] = <<293.3890, 159.2870, 103.6578>>
TrafficCarQuatX[19] = 0.0054
TrafficCarQuatY[19] = -0.0081
TrafficCarQuatZ[19] = 0.8197
TrafficCarQuatW[19] = -0.5727
TrafficCarRecording[19] = 21
TrafficCarStartime[19] = 25938.0000
TrafficCarModel[19] = asterope

TrafficCarPos[20] = <<283.2899, 162.9896, 104.1003>>
TrafficCarQuatX[20] = 0.0118
TrafficCarQuatY[20] = -0.0158
TrafficCarQuatZ[20] = 0.8203
TrafficCarQuatW[20] = -0.5715
TrafficCarRecording[20] = 22
TrafficCarStartime[20] = 27258.0000
TrafficCarModel[20] = camper

ParkedCarPos[0] = <<260.8076, 188.0152, 104.2091>>
ParkedCarQuatX[0] = 0.0108
ParkedCarQuatY[0] = 0.0072
ParkedCarQuatZ[0] = 0.5724
ParkedCarQuatW[0] = 0.8199
ParkedCarModel[0] = asterope

TrafficCarPos[21] = <<250.0672, 180.7071, 104.5935>>
TrafficCarQuatX[21] = 0.0090
TrafficCarQuatY[21] = 0.0059
TrafficCarQuatZ[21] = 0.5689
TrafficCarQuatW[21] = 0.8223
TrafficCarRecording[21] = 23
TrafficCarStartime[21] = 31482.0000
TrafficCarModel[21] = asterope

TrafficCarPos[22] = <<237.1247, 179.6857, 104.7963>>
TrafficCarQuatX[22] = 0.0084
TrafficCarQuatY[22] = -0.0088
TrafficCarQuatZ[22] = 0.8177
TrafficCarQuatW[22] = -0.5755
TrafficCarRecording[22] = 24
TrafficCarStartime[22] = 32868.0000
TrafficCarModel[22] = emperor2

TrafficCarPos[23] = <<231.7195, 187.3206, 105.0670>>
TrafficCarQuatX[23] = 0.0081
TrafficCarQuatY[23] = 0.0064
TrafficCarQuatZ[23] = 0.5745
TrafficCarQuatW[23] = 0.8185
TrafficCarRecording[23] = 25
TrafficCarStartime[23] = 33660.0000
TrafficCarModel[23] = buffalo

TrafficCarPos[24] = <<209.9247, 189.0678, 105.1821>>
TrafficCarQuatX[24] = -0.0036
TrafficCarQuatY[24] = 0.0030
TrafficCarQuatZ[24] = 0.8136
TrafficCarQuatW[24] = -0.5814
TrafficCarRecording[24] = 26
TrafficCarStartime[24] = 35970.0000
TrafficCarModel[24] = buffalo

TrafficCarPos[25] = <<202.6294, 206.8578, 105.1821>>
TrafficCarQuatX[25] = 0.0011
TrafficCarQuatY[25] = 0.0064
TrafficCarQuatZ[25] = 0.9847
TrafficCarQuatW[25] = 0.1739
TrafficCarRecording[25] = 27
TrafficCarStartime[25] = 37356.0000
TrafficCarModel[25] = buffalo

TrafficCarPos[26] = <<207.1216, 219.3272, 105.0718>>
TrafficCarQuatX[26] = 0.0001
TrafficCarQuatY[26] = -0.0025
TrafficCarQuatZ[26] = 0.9860
TrafficCarQuatW[26] = 0.1664
TrafficCarRecording[26] = 28
TrafficCarStartime[26] = 39270.0000
TrafficCarModel[26] = emperor2

TrafficCarPos[27] = <<179.8858, 203.2519, 105.4420>>
TrafficCarQuatX[27] = 0.0088
TrafficCarQuatY[27] = -0.0131
TrafficCarQuatZ[27] = 0.8219
TrafficCarQuatW[27] = -0.5694
TrafficCarRecording[27] = 29
TrafficCarStartime[27] = 39534.0000
TrafficCarModel[27] = buffalo

TrafficCarPos[28] = <<174.1895, 200.3836, 105.4561>>
TrafficCarQuatX[28] = 0.0080
TrafficCarQuatY[28] = -0.0118
TrafficCarQuatZ[28] = 0.8256
TrafficCarQuatW[28] = -0.5641
TrafficCarRecording[28] = 30
TrafficCarStartime[28] = 40062.0000
TrafficCarModel[28] = buffalo

TrafficCarPos[29] = <<166.6617, 203.3121, 105.5834>>
TrafficCarQuatX[29] = 0.0101
TrafficCarQuatY[29] = -0.0148
TrafficCarQuatZ[29] = 0.8255
TrafficCarQuatW[29] = -0.5642
TrafficCarRecording[29] = 31
TrafficCarStartime[29] = 40920.0000
TrafficCarModel[29] = emperor2

TrafficCarPos[30] = <<162.0573, 199.3076, 105.9020>>
TrafficCarQuatX[30] = 0.0108
TrafficCarQuatY[30] = -0.0157
TrafficCarQuatZ[30] = 0.8252
TrafficCarQuatW[30] = -0.5645
TrafficCarRecording[30] = 32
TrafficCarStartime[30] = 42372.0000
TrafficCarModel[30] = baller //patriot

TrafficCarPos[31] = <<159.0472, 206.2464, 106.0580>>
TrafficCarQuatX[31] = 0.0097
TrafficCarQuatY[31] = -0.0140
TrafficCarQuatZ[31] = 0.8232
TrafficCarQuatW[31] = -0.5675
TrafficCarRecording[31] = 33
TrafficCarStartime[31] = 43362.0000
TrafficCarModel[31] = baller //patriot

TrafficCarPos[32] = <<154.2974, 202.2722, 105.9558>>
TrafficCarQuatX[32] = 0.0088
TrafficCarQuatY[32] = -0.0127
TrafficCarQuatZ[32] = 0.8224
TrafficCarQuatW[32] = -0.5687
TrafficCarRecording[32] = 34
TrafficCarStartime[32] = 43494.0000
TrafficCarModel[32] = asterope

TrafficCarPos[33] = <<145.6810, 205.5041, 106.3117>>
TrafficCarQuatX[33] = 0.0082
TrafficCarQuatY[33] = -0.0118
TrafficCarQuatZ[33] = 0.8221
TrafficCarQuatW[33] = -0.5692
TrafficCarRecording[33] = 35
TrafficCarStartime[33] = 44286.0000
TrafficCarModel[33] = buffalo

TrafficCarPos[34] = <<135.5659, 215.0542, 106.5517>>
TrafficCarQuatX[34] = 0.0110
TrafficCarQuatY[34] = -0.0155
TrafficCarQuatZ[34] = 0.8202
TrafficCarQuatW[34] = -0.5718
TrafficCarRecording[34] = 36
TrafficCarStartime[34] = 45540.0000
TrafficCarModel[34] = emperor2

TrafficCarPos[35] = <<129.2592, 211.5893, 106.9536>>
TrafficCarQuatX[35] = 0.0118
TrafficCarQuatY[35] = -0.0169
TrafficCarQuatZ[35] = 0.8200
TrafficCarQuatW[35] = -0.5719
TrafficCarRecording[35] = 37
TrafficCarStartime[35] = 48840.0000
TrafficCarModel[35] = camper

TrafficCarPos[36] = <<128.8566, 217.5238, 106.7828>>
TrafficCarQuatX[36] = 0.0071
TrafficCarQuatY[36] = -0.0083
TrafficCarQuatZ[36] = 0.8185
TrafficCarQuatW[36] = -0.5744
TrafficCarRecording[36] = 38
TrafficCarStartime[36] = 49698.0000
TrafficCarModel[36] = asterope

TrafficCarPos[37] = <<118.4317, 215.5769, 107.2480>>
TrafficCarQuatX[37] = 0.0082
TrafficCarQuatY[37] = -0.0118
TrafficCarQuatZ[37] = 0.8202
TrafficCarQuatW[37] = -0.5719
TrafficCarRecording[37] = 39
TrafficCarStartime[37] = 49830.0000
TrafficCarModel[37] = baller //patriot

TrafficCarPos[38] = <<28.1684, 246.8354, 108.9501>>
TrafficCarQuatX[38] = -0.0002
TrafficCarQuatY[38] = 0.0040
TrafficCarQuatZ[38] = 0.7876
TrafficCarQuatW[38] = -0.6161
TrafficCarRecording[38] = 40
TrafficCarStartime[38] = 55968.0000
TrafficCarModel[38] = asterope

TrafficCarPos[39] = <<25.3496, 253.1406, 108.9792>>
TrafficCarQuatX[39] = 0.0009
TrafficCarQuatY[39] = -0.0017
TrafficCarQuatZ[39] = 0.7655
TrafficCarQuatW[39] = -0.6434
TrafficCarRecording[39] = 41
TrafficCarStartime[39] = 56232.0000
TrafficCarModel[39] = emperor2

TrafficCarPos[40] = <<64.3568, 317.3943, 111.2906>>
TrafficCarQuatX[40] = 0.0060
TrafficCarQuatY[40] = 0.0159
TrafficCarQuatZ[40] = -0.5331
TrafficCarQuatW[40] = 0.8459
TrafficCarRecording[40] = 42
TrafficCarStartime[40] = 63756.0000
TrafficCarModel[40] = asterope

//SetPieceCarPos[0] = <<527.4999, 141.4522, 97.4913>>
//SetPieceCarQuatX[0] = -0.0221
//SetPieceCarQuatY[0] = -0.0383
//SetPieceCarQuatZ[0] = 0.9429
//SetPieceCarQuatW[0] = 0.3302
//SetPieceCarRecording[0] = 41
//SetPieceCarStartime[0] = 0.0000
//SetPieceCarRecordingSpeed[0] = 1.0000
//SetPieceCarModel[0] = sentinel2
//
//SetPieceCarPos[1] = <<528.8981, 147.9775, 97.8299>>
//SetPieceCarQuatX[1] = -0.0505
//SetPieceCarQuatY[1] = -0.0390
//SetPieceCarQuatZ[1] = 0.9896
//SetPieceCarQuatW[1] = 0.1286
//SetPieceCarRecording[1] = 42
//SetPieceCarStartime[1] = 0.0000
//SetPieceCarRecordingSpeed[1] = 1.0000
//SetPieceCarModel[1] = sentinel2
//
//
//
//SetPieceCarPos[2] = <<529.7094, 140.4224, 97.5247>>
//SetPieceCarQuatX[2] = -0.0116
//SetPieceCarQuatY[2] = -0.0387
//SetPieceCarQuatZ[2] = 0.9811
//SetPieceCarQuatW[2] = 0.1891
//SetPieceCarRecording[2] = 43
//SetPieceCarStartime[2] = 0.0000
//SetPieceCarRecordingSpeed[2] = 1.0000
//SetPieceCarModel[2] = sentinel2
//
//SetPieceCarPos[3] = <<522.0470, 143.9811, 97.5774>>
//SetPieceCarQuatX[3] = -0.0048
//SetPieceCarQuatY[3] = -0.0396
//SetPieceCarQuatZ[3] = 0.9831
//SetPieceCarQuatW[3] = 0.1789
//SetPieceCarRecording[3] = 44
//SetPieceCarStartime[3] = 0.0000
//SetPieceCarRecordingSpeed[3] = 1.0000
//SetPieceCarModel[3] = sentinel2
//
//
//SetPieceCarPos[4] = <<530.8116, 135.7735, 99.5075>>
//SetPieceCarQuatX[4] = 0.0049
//SetPieceCarQuatY[4] = -0.1317
//SetPieceCarQuatZ[4] = 0.9092
//SetPieceCarQuatW[4] = 0.3949
//SetPieceCarRecording[4] = 45
//SetPieceCarStartime[4] = 0.0000
//SetPieceCarRecordingSpeed[4] = 1.0000
//SetPieceCarModel[4] = maverick


ENDPROC


PROC setupTrafficParadise()
TrafficCarPos[0] = <<-3074.3276, 336.9916, 6.7837>>
TrafficCarQuatX[0] = -0.0125
TrafficCarQuatY[0] = 0.0026
TrafficCarQuatZ[0] = -0.1404
TrafficCarQuatW[0] = 0.9900
TrafficCarRecording[0] = 2
TrafficCarStartime[0] = 4618.0000
TrafficCarModel[0] = BURRITO

TrafficCarPos[1] = <<-3052.4834, 412.0713, 6.1210>>
TrafficCarQuatX[1] = -0.0041
TrafficCarQuatY[1] = 0.0003
TrafficCarQuatZ[1] = -0.1965
TrafficCarQuatW[1] = 0.9805
TrafficCarRecording[1] = 3
TrafficCarStartime[1] = 6588.0000
TrafficCarModel[1] = Radi

TrafficCarPos[2] = <<-3015.7375, 492.1247, 7.9461>>
TrafficCarQuatX[2] = 0.0250
TrafficCarQuatY[2] = 0.0076
TrafficCarQuatZ[2] = -0.1316
TrafficCarQuatW[2] = 0.9910
TrafficCarRecording[2] = 4
TrafficCarStartime[2] = 8824.0000
TrafficCarModel[2] = mule2

TrafficCarPos[3] = <<-3075.5544, 679.4883, 13.6981>>
TrafficCarQuatX[3] = 0.0274
TrafficCarQuatY[3] = -0.0802
TrafficCarQuatZ[3] = 0.9423
TrafficCarQuatW[3] = -0.3239
TrafficCarRecording[3] = 5
TrafficCarStartime[3] = 13517.0000
TrafficCarModel[3] = emperor

TrafficCarPos[4] = <<-3107.8474, 792.0723, 18.3499>>
TrafficCarQuatX[4] = -0.0088
TrafficCarQuatY[4] = 0.0098
TrafficCarQuatZ[4] = 0.9526
TrafficCarQuatW[4] = -0.3041
TrafficCarRecording[4] = 6
TrafficCarStartime[4] = 16519.0000
TrafficCarModel[4] = camper

TrafficCarPos[5] = <<-3096.4512, 797.4673, 18.2281>>
TrafficCarQuatX[5] = -0.0202
TrafficCarQuatY[5] = -0.0072
TrafficCarQuatZ[5] = 0.3067
TrafficCarQuatW[5] = 0.9516
TrafficCarRecording[5] = 7
TrafficCarStartime[5] = 16585.0000
TrafficCarModel[5] = bison

TrafficCarPos[6] = <<-3119.0132, 832.4313, 16.5294>>
TrafficCarQuatX[6] = -0.0230
TrafficCarQuatY[6] = -0.0066
TrafficCarQuatZ[6] = 0.2560
TrafficCarQuatW[6] = 0.9664
TrafficCarRecording[6] = 8
TrafficCarStartime[6] = 18450.0000
TrafficCarModel[6] = Sadler

TrafficCarPos[7] = <<-3140.1758, 845.9546, 15.4528>>
TrafficCarQuatX[7] = -0.0047
TrafficCarQuatY[7] = 0.0151
TrafficCarQuatZ[7] = 0.9734
TrafficCarQuatW[7] = -0.2287
TrafficCarRecording[7] = 9
TrafficCarStartime[7] = 19374.0000
TrafficCarModel[7] = Radi

TrafficCarPos[8] = <<-3136.9851, 870.2493, 15.5044>>
TrafficCarQuatX[8] = -0.0165
TrafficCarQuatY[8] = -0.0137
TrafficCarQuatZ[8] = 0.1747
TrafficCarQuatW[8] = 0.9844
TrafficCarRecording[8] = 10
TrafficCarStartime[8] = 19964.0000
TrafficCarModel[8] = mule2

TrafficCarPos[9] = <<-3145.3176, 898.5646, 14.3065>>
TrafficCarQuatX[9] = -0.0053
TrafficCarQuatY[9] = -0.0023
TrafficCarQuatZ[9] = 0.1167
TrafficCarQuatW[9] = 0.9932
TrafficCarRecording[9] = 11
TrafficCarStartime[9] = 21416.0000
TrafficCarModel[9] = Sadler

TrafficCarPos[10] = <<-3142.3857, 886.9725, 14.2969>>
TrafficCarQuatX[10] = -0.0087
TrafficCarQuatY[10] = -0.0015
TrafficCarQuatZ[10] = 0.1291
TrafficCarQuatW[10] = 0.9916
TrafficCarRecording[10] = 12
TrafficCarStartime[10] = 21548.0000
TrafficCarModel[10] = emperor

TrafficCarPos[11] = <<-3162.9072, 943.8142, 14.0261>>
TrafficCarQuatX[11] = 0.0002
TrafficCarQuatY[11] = -0.0026
TrafficCarQuatZ[11] = 1.0000
TrafficCarQuatW[11] = -0.0091
TrafficCarRecording[11] = 13
TrafficCarStartime[11] = 22604.0000
TrafficCarModel[11] = Radi

TrafficCarPos[12] = <<-3150.0720, 950.0905, 14.5912>>
TrafficCarQuatX[12] = 0.0004
TrafficCarQuatY[12] = -0.0029
TrafficCarQuatZ[12] = -0.0252
TrafficCarQuatW[12] = 0.9997
TrafficCarRecording[12] = 14
TrafficCarStartime[12] = 22868.0000
TrafficCarModel[12] = boxville2

TrafficCarPos[13] = <<-3151.9373, 996.1603, 16.5674>>
TrafficCarQuatX[13] = -0.0152
TrafficCarQuatY[13] = -0.0369
TrafficCarQuatZ[13] = 0.9787
TrafficCarQuatW[13] = 0.2012
TrafficCarRecording[13] = 15
TrafficCarStartime[13] = 24170.0000
TrafficCarModel[13] = camper

TrafficCarPos[14] = <<-3143.8186, 983.1268, 15.7516>>
TrafficCarQuatX[14] = 0.0334
TrafficCarQuatY[14] = -0.0073
TrafficCarQuatZ[14] = -0.1780
TrafficCarQuatW[14] = 0.9834
TrafficCarRecording[14] = 16
TrafficCarStartime[14] = 24236.0000
TrafficCarModel[14] = bison

TrafficCarPos[15] = <<-3138.0833, 997.2960, 16.8741>>
TrafficCarQuatX[15] = 0.0388
TrafficCarQuatY[15] = -0.0097
TrafficCarQuatZ[15] = -0.2207
TrafficCarQuatW[15] = 0.9745
TrafficCarRecording[15] = 17
TrafficCarStartime[15] = 24302.0000
TrafficCarModel[15] = BURRITO

TrafficCarPos[16] = <<-3121.6365, 1032.0818, 19.1018>>
TrafficCarQuatX[16] = 0.0150
TrafficCarQuatY[16] = -0.0001
TrafficCarQuatZ[16] = -0.1807
TrafficCarQuatW[16] = 0.9834
TrafficCarRecording[16] = 18
TrafficCarStartime[16] = 25427.0000
TrafficCarModel[16] = BURRITO

TrafficCarPos[17] = <<-3130.3274, 1044.5157, 19.5555>>
TrafficCarQuatX[17] = 0.0006
TrafficCarQuatY[17] = -0.0168
TrafficCarQuatZ[17] = 0.9864
TrafficCarQuatW[17] = 0.1633
TrafficCarRecording[17] = 19
TrafficCarStartime[17] = 25742.0000
TrafficCarModel[17] = Sadler

TrafficCarPos[18] = <<-3105.1438, 1115.9083, 19.9617>>
TrafficCarQuatX[18] = 0.0032
TrafficCarQuatY[18] = 0.0002
TrafficCarQuatZ[18] = -0.0448
TrafficCarQuatW[18] = 0.9990
TrafficCarRecording[18] = 20
TrafficCarStartime[18] = 28230.0000
TrafficCarModel[18] = bison

TrafficCarPos[19] = <<-3100.2661, 1179.1038, 19.8327>>
TrafficCarQuatX[19] = -0.0193
TrafficCarQuatY[19] = 0.0005
TrafficCarQuatZ[19] = -0.0351
TrafficCarQuatW[19] = 0.9992
TrafficCarRecording[19] = 21
TrafficCarStartime[19] = 29994.0000
TrafficCarModel[19] = emperor

TrafficCarPos[20] = <<-3111.4070, 1198.2913, 19.7982>>
TrafficCarQuatX[20] = -0.0017
TrafficCarQuatY[20] = -0.0023
TrafficCarQuatZ[20] = 0.9993
TrafficCarQuatW[20] = 0.0375
TrafficCarRecording[20] = 22
TrafficCarStartime[20] = 30447.0000
TrafficCarModel[20] = emperor

TrafficCarPos[21] = <<-3098.8862, 1199.2389, 20.0264>>
TrafficCarQuatX[21] = -0.0049
TrafficCarQuatY[21] = -0.0002
TrafficCarQuatZ[21] = -0.0376
TrafficCarQuatW[21] = 0.9993
TrafficCarRecording[21] = 23
TrafficCarStartime[21] = 30509.0000
TrafficCarModel[21] = Sadler

TrafficCarPos[22] = <<-3097.8960, 1211.7771, 20.5240>>
TrafficCarQuatX[22] = -0.0010
TrafficCarQuatY[22] = -0.0006
TrafficCarQuatZ[22] = -0.0409
TrafficCarQuatW[22] = 0.9992
TrafficCarRecording[22] = 24
TrafficCarStartime[22] = 30811.0000
TrafficCarModel[22] = mule2

TrafficCarPos[23] = <<-3109.7583, 1218.6968, 19.9939>>
TrafficCarQuatX[23] = -0.0002
TrafficCarQuatY[23] = -0.0022
TrafficCarQuatZ[23] = 0.9991
TrafficCarQuatW[23] = 0.0415
TrafficCarRecording[23] = 25
TrafficCarStartime[23] = 31002.0000
TrafficCarModel[23] = Sadler

TrafficCarPos[24] = <<-3078.6292, 1368.1868, 19.7997>>
TrafficCarQuatX[24] = -0.0035
TrafficCarQuatY[24] = -0.0082
TrafficCarQuatZ[24] = 0.9772
TrafficCarQuatW[24] = 0.2123
TrafficCarRecording[24] = 26
TrafficCarStartime[24] = 34959.0000
TrafficCarModel[24] = emperor

TrafficCarPos[25] = <<-3056.8870, 1407.3260, 21.3432>>
TrafficCarQuatX[25] = -0.0174
TrafficCarQuatY[25] = -0.0391
TrafficCarQuatZ[25] = 0.9532
TrafficCarQuatW[25] = 0.2991
TrafficCarRecording[25] = 27
TrafficCarStartime[25] = 36040.0000
TrafficCarModel[25] = emperor

TrafficCarPos[26] = <<-3034.7795, 1433.9921, 25.1451>>
TrafficCarQuatX[26] = -0.0063
TrafficCarQuatY[26] = -0.0432
TrafficCarQuatZ[26] = 0.9353
TrafficCarQuatW[26] = 0.3510
TrafficCarRecording[26] = 28
TrafficCarStartime[26] = 36806.0000
TrafficCarModel[26] = mule2

TrafficCarPos[27] = <<-3020.6768, 1431.5659, 25.3068>>
TrafficCarQuatX[27] = 0.0275
TrafficCarQuatY[27] = -0.0150
TrafficCarQuatZ[27] = -0.3417
TrafficCarQuatW[27] = 0.9393
TrafficCarRecording[27] = 29
TrafficCarStartime[27] = 36938.0000
TrafficCarModel[27] = camper

TrafficCarPos[28] = <<-3022.2170, 1449.5756, 26.0517>>
TrafficCarQuatX[28] = -0.0064
TrafficCarQuatY[28] = -0.0301
TrafficCarQuatZ[28] = 0.9499
TrafficCarQuatW[28] = 0.3110
TrafficCarRecording[28] = 30
TrafficCarStartime[28] = 37261.0000
TrafficCarModel[28] = BURRITO

TrafficCarPos[29] = <<-3010.5378, 1466.9150, 27.1086>>
TrafficCarQuatX[29] = -0.0027
TrafficCarQuatY[29] = -0.0163
TrafficCarQuatZ[29] = 0.9618
TrafficCarQuatW[29] = 0.2731
TrafficCarRecording[29] = 31
TrafficCarStartime[29] = 37704.0000
TrafficCarModel[29] = Sadler

TrafficCarPos[30] = <<-2989.7595, 1506.4966, 27.6564>>
TrafficCarQuatX[30] = 0.0051
TrafficCarQuatY[30] = -0.0080
TrafficCarQuatZ[30] = 0.9842
TrafficCarQuatW[30] = 0.1768
TrafficCarRecording[30] = 32
TrafficCarStartime[30] = 38742.0000
TrafficCarModel[30] = Sadler

TrafficCarPos[31] = <<-2978.4070, 1501.7958, 27.4418>>
TrafficCarQuatX[31] = 0.0055
TrafficCarQuatY[31] = 0.0025
TrafficCarQuatZ[31] = -0.1699
TrafficCarQuatW[31] = 0.9854
TrafficCarRecording[31] = 33
TrafficCarStartime[31] = 38808.0000
TrafficCarModel[31] = Radi

TrafficCarPos[32] = <<-2972.4597, 1532.5985, 27.8676>>
TrafficCarQuatX[32] = 0.0037
TrafficCarQuatY[32] = 0.0046
TrafficCarQuatZ[32] = -0.0066
TrafficCarQuatW[32] = 1.0000
TrafficCarRecording[32] = 34
TrafficCarStartime[32] = 39532.0000
TrafficCarModel[32] = BURRITO

TrafficCarPos[33] = <<-2980.2161, 1580.3375, 28.8290>>
TrafficCarQuatX[33] = 0.0141
TrafficCarQuatY[33] = 0.0030
TrafficCarQuatZ[33] = 0.1564
TrafficCarQuatW[33] = 0.9876
TrafficCarRecording[33] = 35
TrafficCarStartime[33] = 40836.0000
TrafficCarModel[33] = bison

TrafficCarPos[34] = <<-2995.1406, 1619.9470, 29.8057>>
TrafficCarQuatX[34] = 0.0058
TrafficCarQuatY[34] = 0.0022
TrafficCarQuatZ[34] = 0.2009
TrafficCarQuatW[34] = 0.9796
TrafficCarRecording[34] = 36
TrafficCarStartime[34] = 41698.0000
TrafficCarModel[34] = emperor

TrafficCarPos[35] = <<-3010.9133, 1625.4941, 30.7871>>
TrafficCarQuatX[35] = 0.0029
TrafficCarQuatY[35] = -0.0101
TrafficCarQuatZ[35] = 0.9815
TrafficCarQuatW[35] = -0.1912
TrafficCarRecording[35] = 37
TrafficCarStartime[35] = 41990.0000
TrafficCarModel[35] = mule2

TrafficCarPos[36] = <<-3026.1504, 1667.4147, 31.1102>>
TrafficCarQuatX[36] = -0.0004
TrafficCarQuatY[36] = -0.0101
TrafficCarQuatZ[36] = 0.9900
TrafficCarQuatW[36] = -0.1404
TrafficCarRecording[36] = 38
TrafficCarStartime[36] = 43131.0000
TrafficCarModel[36] = Sadler

TrafficCarPos[37] = <<-3016.7668, 1679.9830, 31.1324>>
TrafficCarQuatX[37] = 0.0045
TrafficCarQuatY[37] = -0.0015
TrafficCarQuatZ[37] = 0.1225
TrafficCarQuatW[37] = 0.9925
TrafficCarRecording[37] = 39
TrafficCarStartime[37] = 43325.0000
TrafficCarModel[37] = BURRITO

TrafficCarPos[38] = <<-3019.2700, 1690.6971, 31.1991>>
TrafficCarQuatX[38] = 0.0025
TrafficCarQuatY[38] = -0.0013
TrafficCarQuatZ[38] = 0.1016
TrafficCarQuatW[38] = 0.9948
TrafficCarRecording[38] = 40
TrafficCarStartime[38] = 43651.0000
TrafficCarModel[38] = Radi

TrafficCarPos[39] = <<-3035.0552, 1710.8929, 31.6482>>
TrafficCarQuatX[39] = -0.0015
TrafficCarQuatY[39] = -0.0086
TrafficCarQuatZ[39] = 0.9988
TrafficCarQuatW[39] = -0.0486
TrafficCarRecording[39] = 41
TrafficCarStartime[39] = 44260.0000
TrafficCarModel[39] = camper

TrafficCarPos[40] = <<-3034.8884, 1758.6390, 31.4939>>
TrafficCarQuatX[40] = -0.0018
TrafficCarQuatY[40] = 0.0016
TrafficCarQuatZ[40] = 0.9993
TrafficCarQuatW[40] = 0.0360
TrafficCarRecording[40] = 42
TrafficCarStartime[40] = 45496.0000
TrafficCarModel[40] = BURRITO

TrafficCarPos[41] = <<-3032.5757, 1782.3616, 31.3468>>
TrafficCarQuatX[41] = -0.0012
TrafficCarQuatY[41] = 0.0068
TrafficCarQuatZ[41] = 0.9982
TrafficCarQuatW[41] = 0.0594
TrafficCarRecording[41] = 43
TrafficCarStartime[41] = 46155.0000
TrafficCarModel[41] = Sadler

TrafficCarPos[42] = <<-3022.0024, 1763.8870, 31.4376>>
TrafficCarQuatX[42] = -0.0024
TrafficCarQuatY[42] = -0.0008
TrafficCarQuatZ[42] = -0.0474
TrafficCarQuatW[42] = 0.9989
TrafficCarRecording[42] = 44
TrafficCarStartime[42] = 46347.0000
TrafficCarModel[42] = Sadler

TrafficCarPos[43] = <<-3020.9019, 1774.8822, 31.2692>>
TrafficCarQuatX[43] = -0.0039
TrafficCarQuatY[43] = -0.0002
TrafficCarQuatZ[43] = -0.0563
TrafficCarQuatW[43] = 0.9984
TrafficCarRecording[43] = 45
TrafficCarStartime[43] = 46656.0000
TrafficCarModel[43] = bison

TrafficCarPos[44] = <<-3026.5488, 1815.2367, 30.3174>>
TrafficCarQuatX[44] = -0.0005
TrafficCarQuatY[44] = 0.0176
TrafficCarQuatZ[44] = 0.9918
TrafficCarQuatW[44] = 0.1266
TrafficCarRecording[44] = 46
TrafficCarStartime[44] = 47023.0000
TrafficCarModel[44] = bison

TrafficCarPos[45] = <<-3002.5322, 1844.0995, 28.6423>>
TrafficCarQuatX[45] = -0.0273
TrafficCarQuatY[45] = 0.0030
TrafficCarQuatZ[45] = -0.2289
TrafficCarQuatW[45] = 0.9731
TrafficCarRecording[45] = 47
TrafficCarStartime[45] = 47877.0000
TrafficCarModel[45] = Radi

TrafficCarPos[46] = <<-3011.9250, 1852.7974, 29.2006>>
TrafficCarQuatX[46] = -0.0013
TrafficCarQuatY[46] = 0.0211
TrafficCarQuatZ[46] = 0.9730
TrafficCarQuatW[46] = 0.2299
TrafficCarRecording[46] = 48
TrafficCarStartime[46] = 48065.0000
TrafficCarModel[46] = mule2

TrafficCarPos[47] = <<-3006.9146, 1862.4734, 28.0032>>
TrafficCarQuatX[47] = 0.0021
TrafficCarQuatY[47] = 0.0133
TrafficCarQuatZ[47] = 0.9700
TrafficCarQuatW[47] = 0.2428
TrafficCarRecording[47] = 49
TrafficCarStartime[47] = 48387.0000
TrafficCarModel[47] = emperor

TrafficCarPos[48] = <<-2962.3752, 1939.4952, 28.2864>>
TrafficCarQuatX[48] = -0.0002
TrafficCarQuatY[48] = 0.0014
TrafficCarQuatZ[48] = -0.1321
TrafficCarQuatW[48] = 0.9912
TrafficCarRecording[48] = 50
TrafficCarStartime[48] = 50612.0000
TrafficCarModel[48] = boxville2

TrafficCarPos[49] = <<-2957.2656, 1960.2211, 28.7586>>
TrafficCarQuatX[49] = 0.0065
TrafficCarQuatY[49] = 0.0019
TrafficCarQuatZ[49] = -0.1138
TrafficCarQuatW[49] = 0.9935
TrafficCarRecording[49] = 51
TrafficCarStartime[49] = 51070.0000
TrafficCarModel[49] = camper

TrafficCarPos[50] = <<-2950.6829, 1988.4847, 29.4945>>
TrafficCarQuatX[50] = 0.0122
TrafficCarQuatY[50] = -0.0028
TrafficCarQuatZ[50] = -0.1270
TrafficCarQuatW[50] = 0.9918
TrafficCarRecording[50] = 52
TrafficCarStartime[50] = 51687.0000
TrafficCarModel[50] = BURRITO

TrafficCarPos[51] = <<-2925.2280, 2045.5244, 30.4021>>
TrafficCarQuatX[51] = 0.0115
TrafficCarQuatY[51] = -0.0060
TrafficCarQuatZ[51] = -0.3166
TrafficCarQuatW[51] = 0.9485
TrafficCarRecording[51] = 53
TrafficCarStartime[51] = 53159.0000
TrafficCarModel[51] = bison

TrafficCarPos[52] = <<-2918.9021, 2070.7156, 30.7861>>
TrafficCarQuatX[52] = -0.0073
TrafficCarQuatY[52] = -0.0079
TrafficCarQuatZ[52] = 0.9145
TrafficCarQuatW[52] = 0.4044
TrafficCarRecording[52] = 54
TrafficCarStartime[52] = 53778.0000
TrafficCarModel[52] = emperor

TrafficCarPos[53] = <<-2890.0454, 2093.3591, 31.3753>>
TrafficCarQuatX[53] = 0.0044
TrafficCarQuatY[53] = 0.0071
TrafficCarQuatZ[53] = 0.8873
TrafficCarQuatW[53] = 0.4611
TrafficCarRecording[53] = 55
TrafficCarStartime[53] = 55283.0000
TrafficCarModel[53] = emperor

TrafficCarPos[54] = <<-2852.6333, 2123.4368, 30.6514>>
TrafficCarQuatX[54] = 0.0021
TrafficCarQuatY[54] = -0.0001
TrafficCarQuatZ[54] = 0.9132
TrafficCarQuatW[54] = 0.4076
TrafficCarRecording[54] = 56
TrafficCarStartime[54] = 55897.0000
TrafficCarModel[54] = Sadler

TrafficCarPos[55] = <<-2853.7678, 2106.0281, 30.5747>>
TrafficCarQuatX[55] = -0.0109
TrafficCarQuatY[55] = 0.0052
TrafficCarQuatZ[55] = -0.4186
TrafficCarQuatW[55] = 0.9081
TrafficCarRecording[55] = 57
TrafficCarStartime[55] = 55963.0000
TrafficCarModel[55] = emperor

TrafficCarPos[56] = <<-2827.5266, 2148.5337, 30.2797>>
TrafficCarQuatX[56] = 0.0017
TrafficCarQuatY[56] = 0.0006
TrafficCarQuatZ[56] = 0.9319
TrafficCarQuatW[56] = 0.3628
TrafficCarRecording[56] = 58
TrafficCarStartime[56] = 56743.0000
TrafficCarModel[56] = bison

TrafficCarPos[57] = <<-2783.3347, 2205.4866, 30.8497>>
TrafficCarQuatX[57] = -0.0014
TrafficCarQuatY[57] = -0.0082
TrafficCarQuatZ[57] = 0.9554
TrafficCarQuatW[57] = 0.2953
TrafficCarRecording[57] = 59
TrafficCarStartime[57] = 58268.0000
TrafficCarModel[57] = BURRITO

TrafficCarPos[58] = <<-2765.6985, 2209.8733, 31.1651>>
TrafficCarQuatX[58] = 0.0047
TrafficCarQuatY[58] = -0.0004
TrafficCarQuatZ[58] = -0.2768
TrafficCarQuatW[58] = 0.9609
TrafficCarRecording[58] = 60
TrafficCarStartime[58] = 58582.0000
TrafficCarModel[58] = Sadler

TrafficCarPos[59] = <<-2773.2297, 2221.2017, 31.0734>>
TrafficCarQuatX[59] = -0.0011
TrafficCarQuatY[59] = -0.0057
TrafficCarQuatZ[59] = 0.9611
TrafficCarQuatW[59] = 0.2763
TrafficCarRecording[59] = 61
TrafficCarStartime[59] = 58842.0000
TrafficCarModel[59] = bison

TrafficCarPos[60] = <<-2708.4578, 2326.2175, 33.9414>>
TrafficCarQuatX[60] = 0.0020
TrafficCarQuatY[60] = -0.0007
TrafficCarQuatZ[60] = -0.1754
TrafficCarQuatW[60] = 0.9845
TrafficCarRecording[60] = 62
TrafficCarStartime[60] = 61150.0000
TrafficCarModel[60] = boxville2

TrafficCarPos[61] = <<-2719.3611, 2332.5232, 33.8431>>
TrafficCarQuatX[61] = 0.0000
TrafficCarQuatY[61] = -0.0160
TrafficCarQuatZ[61] = 0.9842
TrafficCarQuatW[61] = 0.1765
TrafficCarRecording[61] = 63
TrafficCarStartime[61] = 61210.0000
TrafficCarModel[61] = camper

TrafficCarPos[62] = <<-2700.9690, 2390.6116, 34.7704>>
TrafficCarQuatX[62] = -0.0090
TrafficCarQuatY[62] = -0.0040
TrafficCarQuatZ[62] = 0.9904
TrafficCarQuatW[62] = 0.1382
TrafficCarRecording[62] = 64
TrafficCarStartime[62] = 62389.0000
TrafficCarModel[62] = BURRITO

TrafficCarPos[63] = <<-2697.5386, 2412.6831, 34.8087>>
TrafficCarQuatX[63] = 0.0004
TrafficCarQuatY[63] = 0.0015
TrafficCarQuatZ[63] = 0.9965
TrafficCarQuatW[63] = 0.0833
TrafficCarRecording[63] = 65
TrafficCarStartime[63] = 62883.0000
TrafficCarModel[63] = bison

TrafficCarPos[64] = <<-2695.0559, 2427.4722, 34.7742>>
TrafficCarQuatX[64] = -0.0000
TrafficCarQuatY[64] = 0.0010
TrafficCarQuatZ[64] = 0.9964
TrafficCarQuatW[64] = 0.0852
TrafficCarRecording[64] = 66
TrafficCarStartime[64] = 63182.0000
TrafficCarModel[64] = Radi

TrafficCarPos[65] = <<-2686.2935, 2446.0300, 34.9997>>
TrafficCarQuatX[65] = -0.0030
TrafficCarQuatY[65] = 0.0008
TrafficCarQuatZ[65] = -0.0880
TrafficCarQuatW[65] = 0.9961
TrafficCarRecording[65] = 67
TrafficCarStartime[65] = 63602.0000
TrafficCarModel[65] = Sadler

TrafficCarPos[66] = <<-2689.2139, 2461.0273, 34.8526>>
TrafficCarQuatX[66] = 0.0007
TrafficCarQuatY[66] = -0.0023
TrafficCarQuatZ[66] = 0.9967
TrafficCarQuatW[66] = 0.0813
TrafficCarRecording[66] = 68
TrafficCarStartime[66] = 63915.0000
TrafficCarModel[66] = emperor

TrafficCarPos[67] = <<-2686.8630, 2475.8623, 34.8340>>
TrafficCarQuatX[67] = -0.0000
TrafficCarQuatY[67] = 0.0001
TrafficCarQuatZ[67] = 0.9964
TrafficCarQuatW[67] = 0.0849
TrafficCarRecording[67] = 69
TrafficCarStartime[67] = 64168.0000
TrafficCarModel[67] = bison

TrafficCarPos[68] = <<-2680.6641, 2512.8162, 35.0255>>
TrafficCarQuatX[68] = 0.0003
TrafficCarQuatY[68] = -0.0045
TrafficCarQuatZ[68] = 0.9967
TrafficCarQuatW[68] = 0.0805
TrafficCarRecording[68] = 70
TrafficCarStartime[68] = 64945.0000
TrafficCarModel[68] = camper

TrafficCarPos[69] = <<-2676.1624, 2540.4529, 34.8574>>
TrafficCarQuatX[69] = 0.0001
TrafficCarQuatY[69] = -0.0016
TrafficCarQuatZ[69] = 0.9964
TrafficCarQuatW[69] = 0.0847
TrafficCarRecording[69] = 71
TrafficCarStartime[69] = 65526.0000
TrafficCarModel[69] = BURRITO

TrafficCarPos[70] = <<-2674.9399, 2547.8064, 34.9938>>
TrafficCarQuatX[70] = 0.0001
TrafficCarQuatY[70] = -0.0026
TrafficCarQuatZ[70] = 0.9965
TrafficCarQuatW[70] = 0.0832
TrafficCarRecording[70] = 72
TrafficCarStartime[70] = 65722.0000
TrafficCarModel[70] = Sadler

TrafficCarPos[71] = <<-2665.4424, 2573.1648, 34.7796>>
TrafficCarQuatX[71] = -0.0044
TrafficCarQuatY[71] = 0.0001
TrafficCarQuatZ[71] = -0.0775
TrafficCarQuatW[71] = 0.9970
TrafficCarRecording[71] = 73
TrafficCarStartime[71] = 66234.0000
TrafficCarModel[71] = emperor

TrafficCarPos[72] = <<-2666.4038, 2601.3513, 34.8068>>
TrafficCarQuatX[72] = 0.0004
TrafficCarQuatY[72] = 0.0017
TrafficCarQuatZ[72] = 0.9969
TrafficCarQuatW[72] = 0.0792
TrafficCarRecording[72] = 74
TrafficCarStartime[72] = 66813.0000
TrafficCarModel[72] = bison

TrafficCarPos[73] = <<-2663.4048, 2621.3582, 34.7753>>
TrafficCarQuatX[73] = 0.0005
TrafficCarQuatY[73] = -0.0021
TrafficCarQuatZ[73] = 0.9973
TrafficCarQuatW[73] = 0.0735
TrafficCarRecording[73] = 75
TrafficCarStartime[73] = 67191.0000
TrafficCarModel[73] = Radi

TrafficCarPos[74] = <<-2657.1033, 2627.4395, 35.5304>>
TrafficCarQuatX[74] = 0.0001
TrafficCarQuatY[74] = 0.0003
TrafficCarQuatZ[74] = -0.0700
TrafficCarQuatW[74] = 0.9975
TrafficCarRecording[74] = 76
TrafficCarStartime[74] = 67321.0000
TrafficCarModel[74] = mule2

TrafficCarPos[75] = <<-2654.5120, 2645.7666, 34.8099>>
TrafficCarQuatX[75] = -0.0043
TrafficCarQuatY[75] = 0.0002
TrafficCarQuatZ[75] = -0.0701
TrafficCarQuatW[75] = 0.9975
TrafficCarRecording[75] = 77
TrafficCarStartime[75] = 67704.0000
TrafficCarModel[75] = emperor

TrafficCarPos[76] = <<-2659.4851, 2648.9880, 34.8011>>
TrafficCarQuatX[76] = -0.0001
TrafficCarQuatY[76] = -0.0014
TrafficCarQuatZ[76] = 0.9974
TrafficCarQuatW[76] = 0.0722
TrafficCarRecording[76] = 78
TrafficCarStartime[76] = 67770.0000
TrafficCarModel[76] = Radi

TrafficCarPos[77] = <<-2657.5430, 2662.3494, 35.0340>>
TrafficCarQuatX[77] = -0.0005
TrafficCarQuatY[77] = -0.0032
TrafficCarQuatZ[77] = 0.9973
TrafficCarQuatW[77] = 0.0738
TrafficCarRecording[77] = 79
TrafficCarStartime[77] = 68230.0000
TrafficCarModel[77] = Sadler

TrafficCarPos[78] = <<-2644.0073, 2718.5476, 34.8222>>
TrafficCarQuatX[78] = -0.0087
TrafficCarQuatY[78] = 0.0006
TrafficCarQuatZ[78] = -0.0682
TrafficCarQuatW[78] = 0.9976
TrafficCarRecording[78] = 80
TrafficCarStartime[78] = 70496.0000
TrafficCarModel[78] = BURRITO

TrafficCarPos[79] = <<-2649.4763, 2718.3833, 35.0097>>
TrafficCarQuatX[79] = -0.0005
TrafficCarQuatY[79] = -0.0056
TrafficCarQuatZ[79] = 0.9976
TrafficCarQuatW[79] = 0.0690
TrafficCarRecording[79] = 81
TrafficCarStartime[79] = 70496.0000
TrafficCarModel[79] = camper

TrafficCarPos[80] = <<-2639.8306, 2747.9612, 34.4025>>
TrafficCarQuatX[80] = -0.0093
TrafficCarQuatY[80] = -0.0000
TrafficCarQuatZ[80] = -0.0756
TrafficCarQuatW[80] = 0.9971
TrafficCarRecording[80] = 82
TrafficCarStartime[80] = 71923.0000
TrafficCarModel[80] = emperor

TrafficCarPos[81] = <<-2645.2173, 2748.2183, 34.4306>>
TrafficCarQuatX[81] = 0.0004
TrafficCarQuatY[81] = 0.0068
TrafficCarQuatZ[81] = 0.9971
TrafficCarQuatW[81] = 0.0761
TrafficCarRecording[81] = 83
TrafficCarStartime[81] = 71923.0000
TrafficCarModel[81] = BURRITO

TrafficCarPos[82] = <<-2637.2393, 2802.2688, 32.6776>>
TrafficCarQuatX[82] = 0.0015
TrafficCarQuatY[82] = 0.0192
TrafficCarQuatZ[82] = 0.9973
TrafficCarQuatW[82] = 0.0713
TrafficCarRecording[82] = 84
TrafficCarStartime[82] = 74282.0000
TrafficCarModel[82] = bison

TrafficCarPos[83] = <<-2636.2813, 2809.0195, 32.3384>>
TrafficCarQuatX[83] = 0.0015
TrafficCarQuatY[83] = 0.0186
TrafficCarQuatZ[83] = 0.9974
TrafficCarQuatW[83] = 0.0700
TrafficCarRecording[83] = 85
TrafficCarStartime[83] = 74479.0000
TrafficCarModel[83] = emperor

TrafficCarPos[84] = <<-2628.2749, 2826.7864, 31.6813>>
TrafficCarQuatX[84] = -0.0267
TrafficCarQuatY[84] = 0.0011
TrafficCarQuatZ[84] = -0.0738
TrafficCarQuatW[84] = 0.9969
TrafficCarRecording[84] = 86
TrafficCarStartime[84] = 75064.0000
TrafficCarModel[84] = Sadler

TrafficCarPos[85] = <<-2631.6060, 2840.5779, 30.9516>>
TrafficCarQuatX[85] = 0.0014
TrafficCarQuatY[85] = 0.0240
TrafficCarQuatZ[85] = 0.9966
TrafficCarQuatW[85] = 0.0785
TrafficCarRecording[85] = 87
TrafficCarStartime[85] = 75459.0000
TrafficCarModel[85] = Sadler

TrafficCarPos[86] = <<-2623.8550, 2855.3733, 29.8767>>
TrafficCarQuatX[86] = -0.0317
TrafficCarQuatY[86] = 0.0026
TrafficCarQuatZ[86] = -0.0777
TrafficCarQuatW[86] = 0.9965
TrafficCarRecording[86] = 88
TrafficCarStartime[86] = 75921.0000
TrafficCarModel[86] = emperor

TrafficCarPos[87] = <<-2621.2634, 2872.0557, 28.9665>>
TrafficCarQuatX[87] = -0.0274
TrafficCarQuatY[87] = 0.0024
TrafficCarQuatZ[87] = -0.0746
TrafficCarQuatW[87] = 0.9968
TrafficCarRecording[87] = 89
TrafficCarStartime[87] = 76383.0000
TrafficCarModel[87] = bison

TrafficCarPos[88] = <<-2626.4844, 2873.5168, 29.2736>>
TrafficCarQuatX[88] = -0.0021
TrafficCarQuatY[88] = 0.0213
TrafficCarQuatZ[88] = 0.9969
TrafficCarQuatW[88] = 0.0759
TrafficCarRecording[88] = 90
TrafficCarStartime[88] = 76383.0000
TrafficCarModel[88] = boxville2

TrafficCarPos[89] = <<-2619.5107, 2883.7571, 28.2272>>
TrafficCarQuatX[89] = -0.0313
TrafficCarQuatY[89] = 0.0021
TrafficCarQuatZ[89] = -0.0736
TrafficCarQuatW[89] = 0.9968
TrafficCarRecording[89] = 91
TrafficCarStartime[89] = 76712.0000
TrafficCarModel[89] = Radi

TrafficCarPos[90] = <<-2623.4456, 2894.1394, 27.8859>>
TrafficCarQuatX[90] = 0.0075
TrafficCarQuatY[90] = 0.0226
TrafficCarQuatZ[90] = 0.9971
TrafficCarQuatW[90] = 0.0717
TrafficCarRecording[90] = 92
TrafficCarStartime[90] = 76976.0000
TrafficCarModel[90] = camper

TrafficCarPos[91] = <<-2612.9287, 2932.8608, 25.2130>>
TrafficCarQuatX[91] = -0.0322
TrafficCarQuatY[91] = 0.0024
TrafficCarQuatZ[91] = -0.0639
TrafficCarQuatW[91] = 0.9974
TrafficCarRecording[91] = 93
TrafficCarStartime[91] = 78181.0000
TrafficCarModel[91] = Radi

TrafficCarPos[92] = <<-2610.1104, 2954.5071, 24.5798>>
TrafficCarQuatX[92] = -0.0318
TrafficCarQuatY[92] = -0.0034
TrafficCarQuatZ[92] = -0.0650
TrafficCarQuatW[92] = 0.9974
TrafficCarRecording[92] = 94
TrafficCarStartime[92] = 78773.0000
TrafficCarModel[92] = mule2

TrafficCarPos[93] = <<-2614.6958, 2960.6982, 23.5657>>
TrafficCarQuatX[93] = 0.0004
TrafficCarQuatY[93] = 0.0275
TrafficCarQuatZ[93] = 0.9975
TrafficCarQuatW[93] = 0.0649
TrafficCarRecording[93] = 95
TrafficCarStartime[93] = 78971.0000
TrafficCarModel[93] = BURRITO

TrafficCarPos[94] = <<-2603.4131, 3012.8484, 20.5447>>
TrafficCarQuatX[94] = -0.0277
TrafficCarQuatY[94] = 0.0016
TrafficCarQuatZ[94] = -0.0443
TrafficCarQuatW[94] = 0.9986
TrafficCarRecording[94] = 96
TrafficCarStartime[94] = 80205.0000
TrafficCarModel[94] = BURRITO

TrafficCarPos[95] = <<-2605.6248, 3051.9956, 18.8943>>
TrafficCarQuatX[95] = -0.0001
TrafficCarQuatY[95] = 0.0182
TrafficCarQuatZ[95] = 0.9991
TrafficCarQuatW[95] = 0.0387
TrafficCarRecording[95] = 97
TrafficCarStartime[95] = 81037.0000
TrafficCarModel[95] = Sadler

TrafficCarPos[96] = <<-2599.6750, 3059.6604, 18.3785>>
TrafficCarQuatX[96] = -0.0185
TrafficCarQuatY[96] = 0.0007
TrafficCarQuatZ[96] = -0.0366
TrafficCarQuatW[96] = 0.9992
TrafficCarRecording[96] = 98
TrafficCarStartime[96] = 81225.0000
TrafficCarModel[96] = bison

TrafficCarPos[97] = <<-2597.5576, 3092.4019, 17.2509>>
TrafficCarQuatX[97] = -0.0195
TrafficCarQuatY[97] = -0.0003
TrafficCarQuatZ[97] = -0.0364
TrafficCarQuatW[97] = 0.9991
TrafficCarRecording[97] = 99
TrafficCarStartime[97] = 81917.0000
TrafficCarModel[97] = emperor

TrafficCarPos[98] = <<-2596.3452, 3109.7783, 16.6432>>
TrafficCarQuatX[98] = -0.0177
TrafficCarQuatY[98] = 0.0009
TrafficCarQuatZ[98] = -0.0270
TrafficCarQuatW[98] = 0.9995
TrafficCarRecording[98] = 100
TrafficCarStartime[98] = 82355.0000
TrafficCarModel[98] = emperor

TrafficCarPos[99] = <<-2601.8242, 3109.3655, 16.9104>>
TrafficCarQuatX[99] = -0.0018
TrafficCarQuatY[99] = 0.0071
TrafficCarQuatZ[99] = 0.9996
TrafficCarQuatW[99] = 0.0282
TrafficCarRecording[99] = 101
TrafficCarStartime[99] = 82355.0000
TrafficCarModel[99] = camper

TrafficCarPos[100] = <<-2601.4290, 3116.5879, 16.6742>>
TrafficCarQuatX[100] = -0.0007
TrafficCarQuatY[100] = 0.0106
TrafficCarQuatZ[100] = 0.9995
TrafficCarQuatW[100] = 0.0284
TrafficCarRecording[100] = 102
TrafficCarStartime[100] = 82551.0000
TrafficCarModel[100] = Sadler

TrafficCarPos[101] = <<-2593.7629, 3143.2371, 16.1593>>
TrafficCarQuatX[101] = -0.0242
TrafficCarQuatY[101] = 0.0019
TrafficCarQuatZ[101] = -0.0500
TrafficCarQuatW[101] = 0.9985
TrafficCarRecording[101] = 103
TrafficCarStartime[101] = 83143.0000
TrafficCarModel[101] = boxville2

TrafficCarPos[102] = <<-2598.4346, 3150.5391, 15.6202>>
TrafficCarQuatX[102] = 0.0010
TrafficCarQuatY[102] = 0.0136
TrafficCarQuatZ[102] = 0.9986
TrafficCarQuatW[102] = 0.0503
TrafficCarRecording[102] = 104
TrafficCarStartime[102] = 83341.0000
TrafficCarModel[102] = bison

TrafficCarPos[103] = <<-2597.9968, 3155.0806, 15.4737>>
TrafficCarQuatX[103] = -0.0001
TrafficCarQuatY[103] = 0.0059
TrafficCarQuatZ[103] = 0.9987
TrafficCarQuatW[103] = 0.0500
TrafficCarRecording[103] = 105
TrafficCarStartime[103] = 83473.0000
TrafficCarModel[103] = Radi

TrafficCarPos[104] = <<-2588.9031, 3228.2659, 14.2905>>
TrafficCarQuatX[104] = 0.0004
TrafficCarQuatY[104] = 0.0050
TrafficCarQuatZ[104] = 0.9972
TrafficCarQuatW[104] = 0.0751
TrafficCarRecording[104] = 106
TrafficCarStartime[104] = 85257.0000
TrafficCarModel[104] = Sadler

TrafficCarPos[105] = <<-2586.3799, 3245.8450, 13.9232>>
TrafficCarQuatX[105] = 0.0008
TrafficCarQuatY[105] = 0.0044
TrafficCarQuatZ[105] = 0.9974
TrafficCarQuatW[105] = 0.0716
TrafficCarRecording[105] = 107
TrafficCarStartime[105] = 85648.0000
TrafficCarModel[105] = BURRITO

TrafficCarPos[106] = <<-2585.4324, 3252.4094, 13.7874>>
TrafficCarQuatX[106] = 0.0003
TrafficCarQuatY[106] = 0.0079
TrafficCarQuatZ[106] = 0.9974
TrafficCarQuatW[106] = 0.0719
TrafficCarRecording[106] = 108
TrafficCarStartime[106] = 85839.0000
TrafficCarModel[106] = bison

TrafficCarPos[107] = <<-2576.6423, 3275.1892, 14.2249>>
TrafficCarQuatX[107] = -0.0060
TrafficCarQuatY[107] = 0.0010
TrafficCarQuatZ[107] = -0.0743
TrafficCarQuatW[107] = 0.9972
TrafficCarRecording[107] = 109
TrafficCarStartime[107] = 86332.0000
TrafficCarModel[107] = mule2

TrafficCarPos[108] = <<-2570.6602, 3314.0232, 13.1224>>
TrafficCarQuatX[108] = -0.0056
TrafficCarQuatY[108] = 0.0002
TrafficCarQuatZ[108] = -0.0799
TrafficCarQuatW[108] = 0.9968
TrafficCarRecording[108] = 110
TrafficCarStartime[108] = 87180.0000
TrafficCarModel[108] = Radi

TrafficCarPos[109] = <<-2569.9907, 3349.6997, 13.1068>>
TrafficCarQuatX[109] = -0.0005
TrafficCarQuatY[109] = -0.0008
TrafficCarQuatZ[109] = 0.9953
TrafficCarQuatW[109] = 0.0967
TrafficCarRecording[109] = 111
TrafficCarStartime[109] = 87948.0000
TrafficCarModel[109] = Sadler

TrafficCarPos[110] = <<-2564.9102, 3347.5544, 12.9066>>
TrafficCarQuatX[110] = -0.0072
TrafficCarQuatY[110] = 0.0004
TrafficCarQuatZ[110] = -0.0949
TrafficCarQuatW[110] = 0.9955
TrafficCarRecording[110] = 112
TrafficCarStartime[110] = 88146.0000
TrafficCarModel[110] = emperor

TrafficCarPos[111] = <<-2562.0510, 3361.9763, 12.9206>>
TrafficCarQuatX[111] = -0.0037
TrafficCarQuatY[111] = -0.0015
TrafficCarQuatZ[111] = -0.1061
TrafficCarQuatW[111] = 0.9943
TrafficCarRecording[111] = 113
TrafficCarStartime[111] = 88146.0000
TrafficCarModel[111] = BURRITO

TrafficCarPos[112] = <<-2558.2065, 3379.5364, 12.8853>>
TrafficCarQuatX[112] = -0.0048
TrafficCarQuatY[112] = -0.0011
TrafficCarQuatZ[112] = -0.1197
TrafficCarQuatW[112] = 0.9928
TrafficCarRecording[112] = 114
TrafficCarStartime[112] = 88542.0000
TrafficCarModel[112] = emperor

TrafficCarPos[113] = <<-2555.8091, 3389.0991, 12.8013>>
TrafficCarQuatX[113] = -0.0003
TrafficCarQuatY[113] = -0.0004
TrafficCarQuatZ[113] = -0.1202
TrafficCarQuatW[113] = 0.9928
TrafficCarRecording[113] = 115
TrafficCarStartime[113] = 88740.0000
TrafficCarModel[113] = bison

TrafficCarPos[114] = <<-2546.0896, 3425.4338, 12.6863>>
TrafficCarQuatX[114] = -0.0019
TrafficCarQuatY[114] = -0.0013
TrafficCarQuatZ[114] = -0.1482
TrafficCarQuatW[114] = 0.9889
TrafficCarRecording[114] = 116
TrafficCarStartime[114] = 89531.0000
TrafficCarModel[114] = Radi

TrafficCarPos[115] = <<-2549.8618, 3431.2937, 13.0714>>
TrafficCarQuatX[115] = -0.0016
TrafficCarQuatY[115] = -0.0079
TrafficCarQuatZ[115] = 0.9886
TrafficCarQuatW[115] = 0.1502
TrafficCarRecording[115] = 117
TrafficCarStartime[115] = 89663.0000
TrafficCarModel[115] = boxville2

TrafficCarPos[116] = <<-2537.5386, 3471.5200, 12.6946>>
TrafficCarQuatX[116] = -0.0039
TrafficCarQuatY[116] = -0.0018
TrafficCarQuatZ[116] = 0.9875
TrafficCarQuatW[116] = 0.1575
TrafficCarRecording[116] = 118
TrafficCarStartime[116] = 92297.0000
TrafficCarModel[116] = Radi

TrafficCarPos[117] = <<-2530.0986, 3477.0945, 12.9198>>
TrafficCarQuatX[117] = -0.0043
TrafficCarQuatY[117] = 0.0009
TrafficCarQuatZ[117] = -0.1527
TrafficCarQuatW[117] = 0.9883
TrafficCarRecording[117] = 119
TrafficCarStartime[117] = 92363.0000
TrafficCarModel[117] = Sadler

//SetPieceCarPos[0] = <<-3043.8757, 227.7076, 15.6764>>
//SetPieceCarQuatX[0] = -0.0033
//SetPieceCarQuatY[0] = 0.0004
//SetPieceCarQuatZ[0] = 0.6385
//SetPieceCarQuatW[0] = 0.7696
//SetPieceCarRecording[0] = 120
//SetPieceCarStartime[0] = 0.0000
//SetPieceCarRecordingSpeed[0] = 1.0000
//SetPieceCarModel[0] = comet2


ENDPROC


