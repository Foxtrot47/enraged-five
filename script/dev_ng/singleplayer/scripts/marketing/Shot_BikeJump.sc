

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "cellphone_public.sch"

#IF IS_DEBUG_BUILD

CAMERA_INDEX	camMain
PED_INDEX		pedRider
VEHICLE_INDEX	vehBike

WIDGET_GROUP_ID	widgetDebug

VECTOR 			v_cam_pos				= << -443.2827, 4901.8940, 182.1549 >>
VECTOR			v_cam_rot				= << 17.5898, 0.0000, -105.1185 >>
FLOAT			f_cam_fov				= 50.0
BOOL			b_cam_update_with_final	= FALSE

VECTOR			v_Bike_Pos				= <<-428.1149, 4886.1885, 191.5>>
VECTOR			v_Bike_Rot				= <<9.5783, 8.8572, 103.1838>>

BOOL			b_pause_anim			= FALSE
BOOL			b_pause_anim_prev		= FALSE
FLOAT			f_anim_phase			= 0.0

INT				i_anim_selection		= 2
INT				i_anim_selection_prev 	= 0
TEXT_LABEL_31	str_anim_dict			= "VEH@BICYCLE@MOUNTAINFRONT@BASE"
TEXT_LABEL_31	str_anim_name_rider
TEXT_LABEL_31	str_anim_name_bike

PROC CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SAFE_WAIT(INT iDur)
	WAIT(iDur)
	IS_PED_INJURED(pedRider)
	IS_VEHICLE_DRIVEABLE(vehBike)
ENDPROC

FUNC BOOL GET_ANIMIATION_NAME(INT i_anim_no, TEXT_LABEL_31 &str_rider, TEXT_LABEL_31 &str_bike)
	SWITCH i_anim_no
		CASE 0
			str_rider 	= "slow_freewheel_CHAR"
			str_bike	= "slow_freewheel_BIKE"
		BREAK
		CASE 1
			str_rider 	= "slow_freewheel_left_CHAR"
			str_bike	= "slow_freewheel_left_BIKE"
		BREAK
		CASE 2
			str_rider 	= "slow_freewheel_right_CHAR"
			str_bike	= "slow_freewheel_right_BIKE"
		BREAK
		CASE 3
			str_rider 	= "slow_pedal_CHAR"
			str_bike	= "slow_pedal_BIKE"
		BREAK
		CASE 4
			str_rider 	= "slow_pedal_left_CHAR"
			str_bike	= "slow_pedal_left_BIKE"
		BREAK
		CASE 5
			str_rider 	= "slow_pedal_right_CHAR"
			str_bike	= "slow_pedal_right_BIKE"
		BREAK
		CASE 6
			str_rider 	= "fast_freewheel_CHAR"
			str_bike	= "fast_freewheel_BIKE"
		BREAK
		CASE 7
			str_rider 	= "fast_freewheel_left_CHAR"
			str_bike	= "fast_freewheel_left_BIKE"
		BREAK
		CASE 8
			str_rider 	= "fast_freewheel_right_CHAR"
			str_bike	= "fast_freewheel_right_BIKE"
		BREAK
		CASE 9
			str_rider 	= "fast_pedal_CHAR"
			str_bike	= "fast_pedal_BIKE"
		BREAK
		CASE 10
			str_rider 	= "fast_pedal_left_CHAR"
			str_bike	= "fast_pedal_left_BIKE"
		BREAK
		CASE 11
			str_rider 	= "fast_pedal_right_CHAR"
			str_bike	= "fast_pedal_right_BIKE"
		BREAK
		DEFAULT
			str_rider 	= ""
			str_bike	= ""
		BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(str_bike)
	OR IS_STRING_NULL_OR_EMPTY(str_rider)
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

PROC SETUP_SCENE()		
	
// SET UP WIDGETS
//-------------------------------------------------
	widgetDebug = START_WIDGET_GROUP("Bike Jump Settings")
		START_WIDGET_GROUP("Camera Controls")
			ADD_WIDGET_VECTOR_SLIDER("Position", v_cam_pos, -99999, 99999, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", v_cam_rot, -99999, 99999, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("FOV", f_cam_fov, 5.0, 180.0, 0.25)
			ADD_WIDGET_BOOL("Update camera from Debug", b_cam_update_with_final)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Bike & Rider")
			ADD_WIDGET_VECTOR_SLIDER("Bike position", v_Bike_Pos, -9999, 9999, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("Bike rotation", v_Bike_Rot, -360, 360, 0.5)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Sitting/Still/Straight")
				ADD_TO_WIDGET_COMBO("Sitting/Still/Left")
				ADD_TO_WIDGET_COMBO("Sitting/Still/Right")
				ADD_TO_WIDGET_COMBO("Sitting/Pedal/Straight")
				ADD_TO_WIDGET_COMBO("Sitting/Pedal/Left")
				ADD_TO_WIDGET_COMBO("Sitting/Pedal/Right")
				ADD_TO_WIDGET_COMBO("Standing/Still/Straight")
				ADD_TO_WIDGET_COMBO("Standing/Still/Left")
				ADD_TO_WIDGET_COMBO("Standing/Still/Right")
				ADD_TO_WIDGET_COMBO("Standing/Pedal/Straight")
				ADD_TO_WIDGET_COMBO("Standing/Pedal/Left")
				ADD_TO_WIDGET_COMBO("Standing/Pedal/Right")
			STOP_WIDGET_COMBO("Anim Set", i_anim_selection)
			ADD_WIDGET_FLOAT_SLIDER("Animation phase", f_anim_phase, 0.0, 1.0, 0.01)
			ADD_WIDGET_BOOL("Pause animation", b_pause_anim)
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	

	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

	REQUEST_MODEL(SCORCHER)
	REQUEST_MODEL(PLAYER_ZERO)
	REQUEST_ANIM_DICT(str_anim_dict)

	SET_ENTITY_COORDS(PLAYER_PED_ID(), << -443.7504, 4900.6182, 178.7109 >>)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 247.7392) 

	camMain = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, v_cam_pos, v_cam_rot, f_cam_fov, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	DISABLE_CELLPHONE(TRUE)
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	
// SETUP THE SCENE
	WHILE NOT HAS_MODEL_LOADED(SCORCHER)
	OR NOT HAS_MODEL_LOADED(PLAYER_ZERO)
	OR NOT HAS_ANIM_DICT_LOADED(str_anim_dict)
		SAFE_WAIT(0)
	ENDWHILE
	
	vehBike = CREATE_VEHICLE(SCORCHER, v_Bike_Pos, 0)
	SET_ENTITY_COORDS_NO_OFFSET(vehBike, v_Bike_Pos)
	SET_ENTITY_ROTATION(vehBike, v_Bike_Rot)
	FREEZE_ENTITY_POSITION(vehBike, TRUE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(SCORCHER)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	
	VECTOR vBonePos = GET_WORLD_POSITION_OF_ENTITY_BONE(vehBike, GET_ENTITY_BONE_INDEX_BY_NAME(vehBike, "seat_f"))
	
	pedRider = CREATE_PED(PEDTYPE_MISSION, PLAYER_ZERO, vBonePos)
	SET_ENTITY_COORDS_NO_OFFSET(pedRider, vBonePos)
	SET_ENTITY_ROTATION(pedRider, GET_ENTITY_ROTATION(vehBike))
	FREEZE_ENTITY_POSITION(pedRider, TRUE)
	SET_ENTITY_NO_COLLISION_ENTITY(vehBike, pedRider, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRider, TRUE)
	
	GET_ANIMIATION_NAME(i_anim_selection, str_anim_name_rider, str_anim_name_bike)
	TASK_PLAY_ANIM(pedRider, str_anim_dict, str_anim_name_rider,
			INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION, f_anim_phase)	
	PLAY_ENTITY_ANIM(vehBike, str_anim_name_bike, str_anim_dict, INSTANT_BLEND_IN, TRUE, FALSE)
	
	SET_ENTITY_LOD_DIST(vehBike, 1000)
	SET_ENTITY_LOD_DIST(pedRider, 1000)
	
	// give the player the component parts of the programmer outfit
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_TORSO, TORSO_P0_GILET_0, TRUE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_TORSO, TORSO_P0_GILET_0, TRUE)
	SET_PED_COMP_ITEM_CURRENT_SP(pedRider, COMP_TYPE_TORSO, TORSO_P0_GILET_0)
	
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, TRUE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, TRUE)
	SET_PED_COMP_ITEM_CURRENT_SP(pedRider, COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0)
	
ENDPROC


PROC CLEANUP_SCENE()
	DELETE_WIDGET_GROUP(widgetDebug)

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	IF DOES_CAM_EXIST(camMain)
		DESTROY_CAM(camMain)
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	IF DOES_ENTITY_EXIST(pedRider)
		DELETE_PED(pedRider)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBike)
		DELETE_VEHICLE(vehBike)
	ENDIF
	
	REMOVE_ANIM_DICT(str_anim_dict)
	
	// remove the programmer outfit	
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_TORSO, TORSO_P0_GILET_0, FALSE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_TORSO, TORSO_P0_GILET_0, FALSE)
	
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, FALSE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, FALSE)
ENDPROC


PROC FADE_OUT()
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		WHILE NOT IS_SCREEN_FADED_OUT()
			SAFE_WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC Update_Camera()
	IF b_cam_update_with_final
		v_cam_pos = GET_FINAL_RENDERED_CAM_COORD()
		v_cam_rot = GET_FINAL_RENDERED_CAM_ROT()
		f_cam_fov = GET_FINAL_RENDERED_CAM_FOV()
		b_cam_update_with_final = FALSE
	ENDIF

	IF NOT ARE_VECTORS_EQUAL(v_cam_pos, GET_CAM_COORD(camMain))
		SET_CAM_COORD(camMain, v_cam_pos)
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(v_cam_rot, GET_CAM_ROT(camMain))
		SET_CAM_ROT(camMain, v_cam_rot)
	ENDIF
	IF f_cam_fov != GET_CAM_FOV(camMain)
		SET_CAM_FOV(camMain, f_cam_fov)
	ENDIF
ENDPROC

PROC Update_Bike()
	IF NOT ARE_VECTORS_EQUAL(v_Bike_Pos, GET_ENTITY_COORDS(vehBike))
	OR NOT ARE_VECTORS_EQUAL(v_Bike_Rot, GET_ENTITY_ROTATION(vehBike))
		FREEZE_ENTITY_POSITION(vehBike, FALSE)
		SET_ENTITY_COORDS_NO_OFFSET(vehBike, v_Bike_Pos)
		SET_ENTITY_ROTATION(vehBike, v_Bike_Rot)
		FREEZE_ENTITY_POSITION(vehBike, TRUE)
		
		FREEZE_ENTITY_POSITION(pedRider, FALSE)
		SET_ENTITY_COORDS_NO_OFFSET(pedRider, GET_WORLD_POSITION_OF_ENTITY_BONE(vehBike, GET_ENTITY_BONE_INDEX_BY_NAME(vehBike, "seat_f")))
		SET_ENTITY_ROTATION(pedRider, GET_ENTITY_ROTATION(vehBike))
		FREEZE_ENTITY_POSITION(pedRider, TRUE)
	ENDIF
ENDPROC

PROC Update_Animation()
	IF i_anim_selection != i_anim_selection_prev
		GET_ANIMIATION_NAME(i_anim_selection, str_anim_name_rider, str_anim_name_bike)
		TASK_PLAY_ANIM(pedRider, str_anim_dict, str_anim_name_rider,
				INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION, f_anim_phase)	
		PLAY_ENTITY_ANIM(vehBike, str_anim_name_bike, str_anim_dict, INSTANT_BLEND_IN, TRUE, FALSE)
		
		 i_anim_selection_prev = i_anim_selection
		 
	ELIF IS_ENTITY_PLAYING_ANIM(vehBike, str_anim_dict, str_anim_name_bike)
	AND IS_ENTITY_PLAYING_ANIM(pedRider, str_anim_dict, str_anim_name_rider)
		IF NOT b_pause_anim
			f_anim_phase = GET_ENTITY_ANIM_CURRENT_TIME(vehBike, str_anim_dict, str_anim_name_bike)
		ELSE
			IF GET_ENTITY_ANIM_CURRENT_TIME(vehBike, str_anim_dict, str_anim_name_bike) != f_anim_phase
				SET_ENTITY_ANIM_CURRENT_TIME(vehBike, str_anim_dict, str_anim_name_bike, f_anim_phase)
				SET_ENTITY_ANIM_SPEED(vehBike, str_anim_dict, str_anim_name_bike, 0.0)
			ENDIF
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedRider, str_anim_dict, str_anim_name_rider) != f_anim_phase
				SET_ENTITY_ANIM_CURRENT_TIME(pedRider, str_anim_dict, str_anim_name_rider, f_anim_phase)
				SET_ENTITY_ANIM_SPEED(pedRider, str_anim_dict, str_anim_name_rider, 0.0)
			ENDIF
		ENDIF
	ENDIF

	IF b_pause_anim != b_pause_anim_prev
	AND IS_ENTITY_PLAYING_ANIM(vehBike, str_anim_dict, str_anim_name_bike)
	AND IS_ENTITY_PLAYING_ANIM(pedRider, str_anim_dict, str_anim_name_rider)
		IF b_pause_anim
			SET_ENTITY_ANIM_SPEED(pedRider, str_anim_dict, str_anim_name_rider, 0.0)
			SET_ENTITY_ANIM_SPEED(vehBike, str_anim_dict, str_anim_name_bike, 0.0)
		ELSE
			SET_ENTITY_ANIM_SPEED(pedRider, str_anim_dict, str_anim_name_rider, 1.0)
			SET_ENTITY_ANIM_SPEED(vehBike, str_anim_dict, str_anim_name_bike, 1.0)
		ENDIF
		
		b_pause_anim_prev = b_pause_anim
	ENDIF
ENDPROC

#ENDIF


SCRIPT

	#IF IS_DEBUG_BUILD
	
		IF (HAS_FORCE_CLEANUP_OCCURRED())
			CLEANUP()
		ENDIF

		FADE_OUT()
		SETUP_SCENE()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		
		//Display scene until X or Esc is pressed.
		WHILE 	NOT IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
		AND 	NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
		AND		NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		AND		NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		AND		NOT IS_PED_INJURED(pedRider)
			SAFE_WAIT(0)
			Update_Camera()
			Update_Bike()
			Update_Animation()
		ENDWHILE
		
		FADE_OUT()
		CLEANUP_SCENE()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		
	#ENDIF

	TERMINATE_THIS_THREAD()
ENDSCRIPT
