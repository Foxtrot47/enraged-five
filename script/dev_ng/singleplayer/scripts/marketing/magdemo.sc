//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	magdemo.sc 													//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:																//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_streaming.sch"
USING "commands_event.sch"
USING "commands_brains.sch"
USING "commands_cutscene.sch"

USING "player_ped_public.sch"
USING "selector_public.sch"
USING "respawn_location_private.sch"
USING "flow_reset_GAME.sch"
USING "script_blip.sch"
USING "chase_hint_cam.sch"
USING "select_mission_stage.sch"
USING "player_ped_scenes.sch"

USING "family_public.sch"


	// Widget data
	BOOL bDisplayStreetNames
	BOOL bDisplayAreaNames
	BOOL bKillScript
	
	STRUCT Z_SKIP_DATA
		INT iSelectedStage
		INT iCurrentStage
		BOOL bDoSkip
		MissionStageMenuTextStruct SkipMenuStruct[5]
	ENDSTRUCT
	Z_SKIP_DATA sZSkipData

// Script data
ENUM MD_STAGE_ENUM
	MD_STAGE_CREATE_INITIAL_SCENE = 0,
	MD_STAGE_MICHAEL_DRIVE,
	MD_STAGE_TREVOR_DRIVE,
	MD_STAGE_FRANKLIN_DRIVE,
	MD_STAGE_FBI2,
	MD_STAGE_RESET,
	MD_STAGE_CLEANUP
ENDENUM
MD_STAGE_ENUM eMDStage

INT iControl = 0
VEHICLE_INDEX vehID_MichaelBike, vehID_MichaelCar, vehID_FranklinCar, vehID_FranklinHeli, vehID_TrevorsCar, vehID_TrevorsQuad, vehID_TrevorsRebel
VEHICLE_INDEX vehID_Police[3]
PED_INDEX pedID_Cop[3]
PICKUP_INDEX pickupID_PetrolTank
BOOL bGPSRouteSet = FALSE
BOOL bLowerRoofOnVehEntry = FALSE

INT iLoungerCamInterpDuration = 4000
BOOL bHintCam_Set = FALSE
BOOL bHintCam_Required = FALSE
BOOL bHintCam_Active = FALSE
VECTOR vHintCamCoord
VECTOR vHintCamArea1,vHintCamArea2
FLOAT fHintCamArea

BOOL bModelRequest_Scorcher
BOOL bModelRequest_Rebel
BOOL bModelRequest_Ninef2
BOOL bModelRequest_Maverick
BOOL bModelRequest_Blazer
BOOL bModelRequest_Bodhi2
BOOL bModelRequest_Tailgater
BOOL bModelRequest_Police
BOOL bModelRequest_Michael
BOOL bModelRequest_Trevor
BOOL bModelRequest_Franklin
BOOL bModelRequest_Dave
BOOL bModelRequest_Cop
BOOL bModelRequest_MimeHat
BOOL bAnimRequest_Lounger
BOOL bAudioRequest_Lounger

BOOL bRadioSetup_Ninef2, bRadioTurnedOn_Ninef2
BOOL bRadioSetup_Bodhi2, bRadioSetup_Blazer
BOOL bRadioSetup_Sentinel2, bRadioTurnedOn_Sentinel2
INT iRadioTurnOnTimer_Ninef2
INT iRadioTurnOnDelay_Ninef2 = 1000

BOOL bRequestedFBI2Cutscene
BOOL bReplayFBI2
BOOL bGardenAudioSceneStarted
BOOL bMichaelBikeAudioSceneStarted, bMichaelBikeAudioSceneCleared
BOOL bPoliceCreated, bPoliceTaskGiven, bPoliceAudioSetup
BOOL bPedWallReset
BOOL bPedWallSet
BOOL bHidingBinBag
BOOL bTurnedOffTrevorRoads

VECTOR vCopCoord[3]
FLOAT fCopHeading[3]

INT missionCandidateID = NO_CANDIDATE_ID

CHASE_HINT_CAM_STRUCT localChaseHintCamStruct

SCENARIO_BLOCKING_INDEX scenarioBlockID_fbi2, scenarioBlockID_trevorDrive

MODEL_NAMES eStoredModel

OBJECT_INDEX objID_MimeHat


FUNC BOOL LOAD_MODEL_ASSET(MODEL_NAMES eModel, BOOL &bRequestFlag)
	REQUEST_MODEL(eModel)
	
	#IF IS_DEBUG_BUILD
		IF NOT bRequestFlag
			PRINTLN("MAGDEMO: LOAD_MODEL_ASSET(", GET_MODEL_NAME_FOR_DEBUG(eModel), ")")
		ENDIF
	#ENDIF
	
	bRequestFlag = TRUE
	RETURN HAS_MODEL_LOADED(eModel)
ENDFUNC
PROC CLEANUP_MODEL_ASSET(MODEL_NAMES eModel, BOOL &bRequestFlag)
	IF bRequestFlag
		#IF IS_DEBUG_BUILD
			PRINTLN("MAGDEMO: CLEANUP_MODEL_ASSET(", GET_MODEL_NAME_FOR_DEBUG(eModel), ")")
		#ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

FUNC BOOL LOAD_ANIM_ASSET(STRING sAnimDict, BOOL &bRequestFlag)
	REQUEST_ANIM_DICT(sAnimDict)
	bRequestFlag = TRUE
	RETURN HAS_ANIM_DICT_LOADED(sAnimDict)
ENDFUNC
PROC CLEANUP_ANIM_ASSET(STRING sAnimDict, BOOL &bRequestFlag)
	IF bRequestFlag
		REMOVE_ANIM_DICT(sAnimDict)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

FUNC BOOL LOAD_AUDIO_ASSET(STRING sAudioBank, BOOL &bRequestFlag)
	bRequestFlag = TRUE
	RETURN REQUEST_SCRIPT_AUDIO_BANK(sAudioBank)
ENDFUNC
PROC CLEANUP_AUDIO_ASSET(STRING sAudioBank, BOOL &bRequestFlag)
	IF bRequestFlag
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(sAudioBank)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

PROC CLEANUP_PREVIOUS_STAGE_ASSETS()

	// Do a hard cleanup of all previous assets
	// Note, this should only be called when we have switched cahracter and are processing the next stage.
	
	/*
		MD_STAGE_CREATE_INITIAL_SCENE = 0,
		MD_STAGE_MICHAEL_DRIVE,
		MD_STAGE_TREVOR_DRIVE,
		MD_STAGE_FRANKLIN_DRIVE,
		MD_STAGE_FBI2,
		MD_STAGE_RESET,
		MD_STAGE_CLEANUP
	*/
	
	// Cleanup Michael stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_TREVOR_DRIVE)
	
		CLEANUP_AUDIO_ASSET("Magdemo_Sunlounger", bAudioRequest_Lounger)
		
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_GARDEN_SCENE")
		STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_GARDEN_SCENE")
		bGardenAudioSceneStarted = FALSE
		
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_BIKE_SCENE")
		STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_BIKE_SCENE")
		bMichaelBikeAudioSceneStarted = FALSE
		bMichaelBikeAudioSceneCleared = FALSE
	
		// Scorcher
		IF DOES_ENTITY_EXIST(vehID_MichaelBike)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_MichaelBike)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_MichaelBike, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_MichaelBike)
		ENDIF
	ENDIF
	
	// Cleanup Trevor stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_FRANKLIN_DRIVE)
		// Bodhi2
		IF DOES_ENTITY_EXIST(vehID_TrevorsCar)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_TrevorsCar)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_TrevorsCar, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_TrevorsCar)
		ENDIF
		// Quad
		IF DOES_ENTITY_EXIST(vehID_TrevorsQuad)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_TrevorsQuad)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_TrevorsQuad, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_TrevorsQuad)
		ENDIF
		// Rebel
		IF DOES_ENTITY_EXIST(vehID_TrevorsRebel)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_TrevorsRebel)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_TrevorsRebel, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_TrevorsRebel)
		ENDIF
		// Jerry can
		IF DOES_PICKUP_EXIST(pickupID_PetrolTank)
			REMOVE_PICKUP(pickupID_PetrolTank)
		ENDIF
		// End of trevor drive
		PRINTLN("MAGDEMO: Turning on roads for end of Trevor stage")
		SET_ROADS_IN_ANGLED_AREA(<<1440.352417,3672.524658,10.284130>>, <<415.225677,3596.012695,59.100624>>, 250.0, FALSE, TRUE)
		//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
		bTurnedOffTrevorRoads = FALSE
	ENDIF
	
	// Cleanup Franklin stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_FBI2)
		// Ninef2
		IF DOES_ENTITY_EXIST(vehID_FranklinCar)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_FranklinCar)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_FranklinCar, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_FranklinCar)
		ENDIF
		// Maverick
		IF DOES_ENTITY_EXIST(vehID_FranklinHeli)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_FranklinHeli)
				SET_ENTITY_AS_MISSION_ENTITY(vehID_FranklinHeli, FALSE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehID_FranklinHeli)
		ENDIF
		INT iCop
		REPEAT 3 iCop
			// Cop Ped
			IF DOES_ENTITY_EXIST(pedID_Cop[iCop])
				IF NOT IS_PED_INJURED(pedID_Cop[iCop])
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedID_Cop[iCop])
						SET_ENTITY_AS_MISSION_ENTITY(pedID_Cop[iCop], FALSE, TRUE)
					ENDIF
					DELETE_PED(pedID_Cop[iCop])
				ENDIF
			ENDIF
			// Cop Car
			IF DOES_ENTITY_EXIST(vehID_Police[iCop])
				IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_Police[iCop])
					SET_ENTITY_AS_MISSION_ENTITY(vehID_Police[iCop], FALSE, TRUE)
				ENDIF
				DELETE_VEHICLE(vehID_Police[iCop])
			ENDIF
		ENDREPEAT
		// Mime hat
		IF DOES_ENTITY_EXIST(objID_MimeHat)
			DELETE_OBJECT(objID_MimeHat)
		ENDIF
		
		// Cop car sirens
		IF IS_AUDIO_SCENE_ACTIVE("MAGDEMO_SIRENS_FRANKLIN")
			PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_SIRENS_FRANKLIN")
			STOP_AUDIO_SCENE("MAGDEMO_SIRENS_FRANKLIN")
		ENDIF
	ENDIF
ENDPROC

PROC SET_HINT_CAM(VECTOR vCoord, VECTOR vArea1, VECTOR vArea2, FLOAT fWidth)
	vHintCamCoord = vCoord
	vHintCamArea1 = vArea1
	vHintCamArea2 = vArea2
	fHintCamArea = fWidth
	bHintCam_Set = TRUE
ENDPROC

PROC CLEAR_HINT_CAM()
	bHintCam_Set = FALSE
ENDPROC

PROC UPDATE_HINT_CAM()
	
	bHintCam_Required = FALSE
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF bHintCam_Set
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vHintCamArea1, vHintCamArea2, fHintCamArea)
			//AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				bHintCam_Required = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bHintCam_Required
		CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vHintCamCoord)
		bHintCam_Active = TRUE
	ELIF bHintCam_Active
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		bHintCam_Active = FALSE
	ENDIF
ENDPROC

PROC SET_MAGDEMO_SCENE()

	SWITCH eMDStage
		CASE MD_STAGE_CREATE_INITIAL_SCENE
		CASE MD_STAGE_MICHAEL_DRIVE
			// Scene 1 - micheal
			// Extra sunny 14:00
			// cirrocumulus cloud hat
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			UNLOAD_ALL_CLOUD_HATS()			
			LOAD_CLOUD_HAT("cirrocumulus")	//LOAD_CLOUD_HAT("Cirrus")	//#740433
			SET_CLOCK_TIME(14, 0, 0)		//SET_CLOCK_TIME(11, 0, 0)	//#740433
			SCRIPT_OVERRIDES_WIND_TYPES(TRUE, "WEATHER_TYPES_MAGDEMO_MICHAEL", "WEATHER_TYPES_MAGDEMO_MICHAEL")
		BREAK
		CASE MD_STAGE_TREVOR_DRIVE
			// Scene 2 - trevor
			// Smog 11:00
			// Clear cloud hat
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			UNLOAD_ALL_CLOUD_HATS()
			LOAD_CLOUD_HAT("Cirrus")
			SET_CLOCK_TIME(19, 0, 0)
			SCRIPT_OVERRIDES_WIND_TYPES(TRUE, "WEATHER_TYPES_MAGDEMO_TREVOR", "WEATHER_TYPES_MAGDEMO_TREVOR")
		BREAK
		CASE MD_STAGE_FRANKLIN_DRIVE
			// Scene 3 - franklin
			// Overcast 09:00
			// Horizon cloud hat
			SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST")
			UNLOAD_ALL_CLOUD_HATS()
			LOAD_CLOUD_HAT("RAIN")
			SET_CLOCK_TIME(9, 0, 0)
			SCRIPT_OVERRIDES_WIND_TYPES(TRUE, "WEATHER_TYPES_MAGDEMO_FRANKLIN", "WEATHER_TYPES_MAGDEMO_FRANKLIN")
		BREAK
		CASE MD_STAGE_FBI2
			// Scene 4 – fbi2
			// Smog 19:30 – 20:00
			// Wispy cloud hat
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			UNLOAD_ALL_CLOUD_HATS()
			LOAD_CLOUD_HAT("Wispy")
			SET_CLOCK_TIME(19, 45, 0)
			SCRIPT_OVERRIDES_WIND_TYPES(FALSE, "", "")
		BREAK
	ENDSWITCH
	
	PAUSE_CLOCK(TRUE)
ENDPROC

/// PURPOSE: Ensure the player is invincible and will not fly through windscreens
PROC UPDATE_PLAYER_PED_STATES()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			eStoredModel = DUMMY_MODEL_FOR_SCRIPT
		ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) != eStoredModel
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
			SET_WANTED_LEVEL_MULTIPLIER(0)
			SET_MAX_WANTED_LEVEL(0)
			
			eStoredModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			
			PRINTLN("MAGDEMO: UPDATE_PLAYER_PED_STATES() - States updated")
		ENDIF
	ENDIF
ENDPROC

PROC SET_MD_STAGE(MD_STAGE_ENUM eStage)
	
	SWITCH eStage
		CASE MD_STAGE_CREATE_INITIAL_SCENE	PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)")	BREAK
		CASE MD_STAGE_MICHAEL_DRIVE			PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_MICHAEL_DRIVE)")		BREAK
		CASE MD_STAGE_TREVOR_DRIVE			PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_TREVOR_DRIVE)")			BREAK
		CASE MD_STAGE_FRANKLIN_DRIVE		PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_FRANKLIN_DRIVE)")		BREAK
		CASE MD_STAGE_FBI2					PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_FBI2)")					BREAK
		CASE MD_STAGE_RESET					PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_RESET)")				BREAK
		CASE MD_STAGE_CLEANUP				PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_CLEANUP)")				BREAK
	ENDSWITCH
	
	eMDStage = eStage
	iControl = 0
	
	#IF IS_DEBUG_BUILD
		// Set the new Z-Skip stage
		IF eStage = MD_STAGE_CREATE_INITIAL_SCENE
			sZSkipData.iCurrentStage = 0 // "Michael at poolside"
		ELIF eStage = MD_STAGE_MICHAEL_DRIVE
			sZSkipData.iCurrentStage = 1 // "Michael on bicycle"
		ELIF eStage = MD_STAGE_TREVOR_DRIVE
			sZSkipData.iCurrentStage = 2 // "Trevor in trailer"
		ELIF eStage = MD_STAGE_FRANKLIN_DRIVE
			sZSkipData.iCurrentStage = 3 // "Franklin at muscle beach"
		ELIF eStage = MD_STAGE_FBI2
			sZSkipData.iCurrentStage = 4 // "FBI2"
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL IS_MAGDEMO_SAFE_TO_PROCEED()

	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_INITIAL_MAGDEMO_GAME_STATE()
	
	g_bMagDemoActive = TRUE
	g_bMagDemoKillFamilySceneM = TRUE
	
	bReplayFBI2 = FALSE
	bPoliceTaskGiven = FALSE
	bPoliceCreated = FALSE
	bPoliceAudioSetup = FALSE
	
	g_bUseCharacterFilters = TRUE
	UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
	
	g_savedGlobals.sFlow.isGameflowActive = TRUE
	RESET_GAMEFLOW()
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	g_bTaxiHailingIsDisabled = TRUE
	
//	g_bUseProtoTypeCamSpline = TRUE
//	g_bUseProtoTypeCamStreaming = TRUE
	
	#IF IS_DEBUG_BUILD
		g_bDrawLiteralSceneString = FALSE
	#ENDIF

	// Initialise Michaels family timetable events
	g_eCurrentFamilyEvent[FM_MICHAEL_SON] = NO_FAMILY_EVENTS
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_SON)
	
	g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER] = NO_FAMILY_EVENTS
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_DAUGHTER)
	
	g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = NO_FAMILY_EVENTS
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_WIFE)
	
	g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID] = NO_FAMILY_EVENTS
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_MEXMAID)
	
	g_eCurrentFamilyEvent[FM_MICHAEL_GARDENER] = NO_FAMILY_EVENTS
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_GARDENER)
	
	// Unlock secirity random event
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV1)
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV4)
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV5)
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV6)
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV7)

	// Unlock savehouses
	Set_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_BH, TRUE)
	Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC, TRUE)
	Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)	
	
	// Set characters we can switch to
	SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
	SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, FALSE)
	SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF2, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(UTILLITRUCK2, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(UTILLITRUCK3, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FBI2, TRUE)
	
	// Limit radio station selection to 4.
	SET_AUDIO_FLAG("LimitAmbientRadioStations", TRUE)
	
	// Fix for bug #814560 - Hiding bin bags as they have no alpha, causing them to pop
	IF NOT bHidingBinBag
		PRINTLN("MAGDEMO: Hiding model 'prop_rub_binbag_03b'")
		CREATE_MODEL_HIDE(<<1927.89, 3925.41, 31.42>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("prop_rub_binbag_03b")), TRUE)
		bHidingBinBag = TRUE
	ENDIF
	
	// Cleanup audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("MAGDEMO_MICHAEL_GARDEN_SCENE")
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_GARDEN_SCENE")
		STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_GARDEN_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MAGDEMO_MICHAEL_BIKE_SCENE")
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_BIKE_SCENE")
		STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_BIKE_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MAGDEMO_SIRENS_FRANKLIN")
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_SIRENS_FRANKLIN")
		STOP_AUDIO_SCENE("MAGDEMO_SIRENS_FRANKLIN")
	ENDIF
	
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	SET_DEBUG_CAM_ACTIVE(FALSE)
	
	SET_RANDOM_TRAINS(FALSE)	//740410
	
	SCRIPT_OVERRIDES_WIND_TYPES(FALSE, "", "")
	
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_LOCKED)
	
	
	WAIT(0)
	WAIT(0)
	WAIT(0)
	
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_MAGDEMO)
	
	WAIT(0)
	WAIT(0)
	WAIT(0)
	
	// Blocking areas
	scenarioBlockID_fbi2 = ADD_SCENARIO_BLOCKING_AREA(<<1321.17, -2143.95, -10>>, <<1468.61, -2001.32, 110>>)
	// - Hill outside Michaels mansion
	SET_PED_PATHS_IN_AREA(<< -881.7, 86.3, 45.5 >>, << -839.7, 216.2, 82.6 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -881.6, 0.4, 31.8 >>, << -819.5, 96.4, 68.9 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -845.9, -76.4, 20.3 >>, << -771.6, 38.8, 58.4 >>, FALSE)
	// - End of Trevor drive
	scenarioBlockID_trevorDrive = ADD_SCENARIO_BLOCKING_AREA(<< 883.5, 3581.7, 19.0 >>, << 1041.2, 3678.1, 50.2 >>)
	
	// - Muscle beach
	SET_PED_PATHS_IN_AREA(<< -1177.2, -1523.7, 0 >>, << -1168.4, -1518.2, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1180.9, -1526.9, 0 >>, << -1172.3, -1521.4, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1185.4, -1530.3, 0 >>, << -1176.8, -1524.6, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1189.7, -1533.0, 0 >>, << -1181.1, -1527.6, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1193.9, -1535.8, 0 >>, << -1185.3, -1530.4, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1198.1, -1538.8, 0 >>, << -1189.4, -1533.3, 10 >>, FALSE)
	SET_PED_PATHS_IN_AREA(<< -1197.2, -1541.7, 0 >>, << -1192.5, -1536.7, 10 >>, FALSE)
ENDPROC

PROC DO_STAGE_CREATE_INITIAL_SCENE()

	SWITCH iControl
		CASE 0
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(1000)
					START_AUDIO_SCENE("MISSION_FAILED_SCENE")
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					START_AUDIO_SCENE("MISSION_FAILED_SCENE")
					CLEAR_AREA(<< -779.3461, 185.2559, 71.8350 >>, 250.0, TRUE, FALSE)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -779.3461, 185.2559, 71.8350 >>)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					NEW_LOAD_SCENE_START(<< -779.3461, 185.2559, 71.8350 >>, << 0.12, -0.99, 0.08 >>, 1800.0)
					
					iControl++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF IS_NEW_LOAD_SCENE_LOADED()
					NEW_LOAD_SCENE_STOP()
					iControl++
				ENDIF
			ELSE
				iControl++
			ENDIF
		BREAK
		CASE 2
			IF LOAD_MODEL_ASSET(SCORCHER, bModelRequest_Scorcher)
			AND LOAD_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
			AND LOAD_ANIM_ASSET("SWITCH@MICHAEL@SUNLOUNGER", bAnimRequest_Lounger)
			AND LOAD_AUDIO_ASSET("Magdemo_Sunlounger", bAudioRequest_Lounger)
				iControl++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initial")) = 0 // Wait for all the controller scripts to be intialised before we proceed (required for runscript command line param)
			
				SET_INITIAL_MAGDEMO_GAME_STATE()
			
				// Michael at poolside
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT , OUTFIT_P0_YOGA, FALSE)
				//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT , OUTFIT_DEFAULT, FALSE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), "SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_Idle",
						<< -780.694, 187.325, 72.8351 >>,		//<< -780.704, 187.322, 72.827 >>,
						<< 0.000, 0.000, 166.320 >>,			//<< 0.000, 0.000, 166.500 >>,
						INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_TURN_OFF_COLLISION | AF_NOT_INTERRUPTABLE | AF_LOOPING)
				
				
				// Michaels bike in garage
				IF DOES_ENTITY_EXIST(vehID_MichaelBike)
					DELETE_VEHICLE(vehID_MichaelBike)
				ENDIF
				vehID_MichaelBike = CREATE_VEHICLE(SCORCHER,<< -810.8484, 189.7630, 71.4785 >>, 131.0119, FALSE, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_MichaelBike)
				
				SET_VEHICLE_COLOURS(vehID_MichaelBike, 29, 3)
				
				CLEANUP_MODEL_ASSET(SCORCHER, bModelRequest_Scorcher)
				CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
				
				iControl++
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
			
			IF IS_SCREEN_FADED_IN()
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_CENTRE(TRUE)
				DISPLAY_TEXT(0.5, 0.4, "MD_TITLE_1")
				
				IF IS_MAGDEMO_SAFE_TO_PROCEED()
				
					SET_MAGDEMO_SCENE()
					
					DO_SCREEN_FADE_OUT(0)
					DO_SCREEN_FADE_IN(1000)
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					PRINTLN("MAGDEMO: Starting audio scene MAGDEMO_MICHAEL_GARDEN_SCENE")
					START_AUDIO_SCENE("MAGDEMO_MICHAEL_GARDEN_SCENE") // Kill this when we enter the house
					bGardenAudioSceneStarted = TRUE
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(25.694843)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-8.558015)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					
					SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_ON_LOUNGER_CAMERA", iLoungerCamInterpDuration)

					iControl++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
			
				// Continue to update the lounger cam until we blend into the get up anim
				SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_ON_LOUNGER_CAMERA", iLoungerCamInterpDuration)
				
				IF IS_SCREEN_FADED_IN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
					INT iLeftX, iLeftY, iRightX, iRightY
					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
					
					IF iLeftY < -64
						TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), "SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_GetUp",
								<< -780.694, 187.325, 72.8351 >>,		//<< -780.704, 187.322, 72.827 >>,
								<< 0.000, 0.000, 166.320 >>,			//<< 0.000, 0.000, 166.500 >>,
								NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1,
								AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION |
								AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE)
						iControl++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				BOOL bAnimComplete
				
				INT iLeftX, iLeftY, iRightX, iRightY
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
				
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_GetUp")
					
					SET_PED_DESIRED_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(PLAYER_PED_ID()))
					
					FLOAT fStartPhase
					
					/* old, relies on anim tag
					FLOAT fEndPhase
					IF FIND_ANIM_EVENT_PHASE("SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_GetUp", "WalkInterruptible", fStartPhase, fEndPhase)
						//fStartPhase: 0.6150
					*/
					
					/* new, relies on phase time */
					fStartPhase = 0.617	//0.6150
					FLOAT fPlayerGetupAnimCurrentTime
					fPlayerGetupAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),
							"SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_GetUp")
					IF (fPlayerGetupAnimCurrentTime >= fStartPhase)
					/* * */
						
						IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "SWITCH@MICHAEL@SUNLOUNGER", "SunLounger_GetUp") >= fStartPhase
							IF iLeftX < -64 OR iLeftX > 64 // Left/Right
							OR iLeftY < -64 OR iLeftY > 64 // Forwards/Backwards
								SET_ENTITY_ANIM_BLEND_OUT_SPEED(PLAYER_PED_ID(),
										"SunLounger_GetUp",
										"SWITCH@MICHAEL@SUNLOUNGER",
										REALLY_SLOW_BLEND_OUT)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								
								SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),
										PEDMOVEBLENDRATIO_WALK,
										5000)
								
								bAnimComplete = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					bAnimComplete = TRUE
				ENDIF
				
				IF bAnimComplete
					CLEANUP_ANIM_ASSET("SWITCH@MICHAEL@SUNLOUNGER", bAnimRequest_Lounger)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
					
					DISABLE_CELLPHONE(FALSE)
					
					SET_MD_STAGE(MD_STAGE_MICHAEL_DRIVE)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_STAGE_STAGE_MICHAEL_DRIVE()

	SWITCH iControl
		CASE 0
		
			// Fix for bug #750345 - The sky shifts a few seconds into the magdemo
			// - We only get to this stage from the lounger stage so no need to set time again.
			//SET_MAGDEMO_SCENE()
			
			// Set characters we can switch to
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
			
			CLEAR_GPS_MULTI_ROUTE()
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
			ADD_POINT_TO_GPS_MULTI_ROUTE(<< -867.0273, 147.1164, 61.4276 >>) // Codor Drive
			ADD_POINT_TO_GPS_MULTI_ROUTE(<< -851.6210, 40.6316, 47.2088 >>) // Spanish Avenue
			ADD_POINT_TO_GPS_MULTI_ROUTE(<< -821.6188, -13.7117, 38.9218 >>) // Caesar Place
			ADD_POINT_TO_GPS_MULTI_ROUTE(<< -835.8494, -80.5153, 36.7526 >>) // Portaolla Drive
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			bGPSRouteSet = TRUE
			
			//SET_HINT_CAM(<< -852.10, -60.87, 38.36 >>, <<-896.800293,-117.732574,36.968113>>, <<-778.729492,-54.647026,46.905807>>, 52.937500)
			
			iControl++
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				// Manage Amandas vehicle created by the timetable
				VEHICLE_INDEX vehSentinel2
				INT iFlags
				iFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
				vehSentinel2 = GET_CLOSEST_VEHICLE(<< -826.2563, 177.8511, 70.1881 >>, 10.0, SENTINEL2, iFlags)
				
				IF DOES_ENTITY_EXIST(vehSentinel2)
				AND IS_VEHICLE_DRIVEABLE(vehSentinel2)
				
					// Set the radio station
					IF NOT bRadioSetup_Sentinel2
						PRINTLN("MAGDEMO: Setting up SENTINEL2 radio")
						SET_VEH_RADIO_STATION(vehSentinel2, "RADIO_02_POP")
						FREEZE_RADIO_STATION("RADIO_02_POP")
						//SET_RADIO_TRACK("RADIO_02_POP", "RADIO_02_POP_MAG_DEMO_LOOP_POP")
						SET_VEHICLE_RADIO_LOUD(vehSentinel2, TRUE)
						bRadioSetup_Sentinel2 = TRUE
						bRadioTurnedOn_Sentinel2 = FALSE
					
					// Turn on the radio when Amandas gets in the vehicle
					ELIF NOT bRadioTurnedOn_Sentinel2
						IF GET_IS_VEHICLE_ENGINE_RUNNING(vehSentinel2)
							IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehSentinel2, VS_DRIVER))
								PRINTLN("MAGDEMO: Turning on SENTINEL2 radio")
								//SET_RADIO_AUTO_UNFREEZE(TRUE) // This is player vehicle only
								UNFREEZE_RADIO_STATION("RADIO_02_POP")
								bRadioTurnedOn_Sentinel2 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// Clear gps when player reaches destination
				IF bGPSRouteSet
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -835.8494, -80.5153, 36.7526 >>, <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS>>)
						CLEAR_GPS_MULTI_ROUTE()
						SET_GPS_MULTI_ROUTE_RENDER(FALSE)
						bGPSRouteSet = FALSE
					ENDIF
				ENDIF
				
				// Clear the audio scene when Michael enters the house
				IF bGardenAudioSceneStarted
					IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != 0
						PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_GARDEN_SCENE")
						STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_GARDEN_SCENE")
						bGardenAudioSceneStarted = FALSE
					ENDIF
				ENDIF
				
				// Start audio scene when Michael gets on his bike
				IF NOT bMichaelBikeAudioSceneStarted
				AND NOT bMichaelBikeAudioSceneCleared
					IF DOES_ENTITY_EXIST(vehID_MichaelBike)
					AND IS_VEHICLE_DRIVEABLE(vehID_MichaelBike)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehID_MichaelBike)
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-813.113892,190.087921,71.316238>>, <<-810.811523,184.379974,74.666481>>, 7.812500)
							PRINTLN("MAGDEMO: Starting audio scene MAGDEMO_MICHAEL_BIKE_SCENE")
							START_AUDIO_SCENE("MAGDEMO_MICHAEL_BIKE_SCENE")
							bMichaelBikeAudioSceneStarted = TRUE
						ENDIF
					ENDIF					
				ENDIF
				
				// Clear the audio scene when we start to switch to Trevor
				IF bMichaelBikeAudioSceneStarted
					IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_MICHAEL_BIKE_SCENE")
						STOP_AUDIO_SCENE("MAGDEMO_MICHAEL_BIKE_SCENE")
						bMichaelBikeAudioSceneStarted = FALSE
						bMichaelBikeAudioSceneCleared = TRUE
					ENDIF
				ENDIF
				
				// Wait for player to switch to Trevor
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
					bGPSRouteSet = FALSE
					CLEAR_HINT_CAM()
					CLEAR_GPS_MULTI_ROUTE()
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					SET_MD_STAGE(MD_STAGE_TREVOR_DRIVE)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			iControl = 1
			MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_TREVOR)	//, PR_TYPE_AMBIENT)
		ENDIF
	#ENDIF
ENDPROC

PROC DO_STAGE_TREVOR_DRIVE()
	SWITCH iControl
		CASE 0
		
			CLEANUP_PREVIOUS_STAGE_ASSETS()
		
			SET_MAGDEMO_SCENE()
		
			// Set characters we can switch to
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
			
			IF LOAD_MODEL_ASSET(REBEL, bModelRequest_Rebel)
			
				IF DOES_ENTITY_EXIST(vehID_TrevorsRebel)
					DELETE_VEHICLE(vehID_TrevorsRebel)
				ENDIF
				CLEAR_AREA(<< 899.3571, 3653.0366, 32.3460 >>, 50.0, TRUE)
				vehID_TrevorsRebel = CREATE_VEHICLE(REBEL, << 899.3571, 3653.0366, 32.3460 >>, 271.1189, FALSE, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_TrevorsRebel)
				SET_VEHICLE_COLOURS(vehID_TrevorsRebel, 128, 0)
				SET_VEHICLE_EXTRA_COLOURS(vehID_TrevorsRebel, 23, 0)
				SET_VEHICLE_DIRT_LEVEL(vehID_TrevorsRebel, 15.0)
			
				CLEAR_GPS_MULTI_ROUTE()
				START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1997.9603, 3835.7959, 31.3112 >>) // Marina Drive
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1959.2478, 3904.3870, 31.3785 >>) // Algonquin Blvd
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1816.0470, 3944.7961, 32.5367 >>) // Niland Ave
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1707.3000, 3900.7368, 33.8820 >>) // Armadillo Ave
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1589.1426, 3791.5486, 33.8437 >>) // Panorama Drive
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1380.3497, 3682.6082, 32.6357 >>) // Lesbos Lane
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1011.7350, 3633.9250, 31.5949 >>) // Meringue Lane
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< 895.0152, 3636.4546, 31.7189 >>) // Alamo Sea
				
				SET_GPS_MULTI_ROUTE_RENDER(TRUE)
				bGPSRouteSet = TRUE
				
				// Make sure Trevor has a pistol with ammo
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
						SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 999)
					ELSE
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 999, FALSE, FALSE)
					ENDIF
				ENDIF
				
				// Create jerry can pickup
				INT iPlacementFlags
				iPlacementFlags = 0
				SET_BIT(iPlacementFlags, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, enum_to_int(PLACEMENT_FLAG_UPRIGHT))
				IF DOES_PICKUP_EXIST(pickupID_PetrolTank)
					REMOVE_PICKUP(pickupID_PetrolTank)
				ENDIF
				pickupID_PetrolTank = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PETROLCAN, << 903.22, 3657.42, 31.61 >>, <<0,0,115>>, iPlacementFlags)
			
				iControl++
			ENDIF
		BREAK
		CASE 1
			// Manage Trevors vehicle created by the switch
			VEHICLE_INDEX vehBodhi2
			INT iFlags
			iFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
			vehBodhi2 = GET_CLOSEST_VEHICLE(<< 1982.8469, 3807.0891, 31.1713 >>, 10.0, BODHI2, iFlags)
			IF DOES_ENTITY_EXIST(vehBodhi2)
			AND IS_VEHICLE_DRIVEABLE(vehBodhi2)
			
				// Set the radio station
				IF NOT bRadioSetup_Bodhi2
					PRINTLN("MAGDEMO: Setting up BODHI2 radio")
					SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
					FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
					//SET_RADIO_TRACK("RADIO_01_CLASS_ROCK", "RADIO_01_CLASS_ROCK_MAG_DEMO_LOOP_ROCK")
					SET_RADIO_AUTO_UNFREEZE(TRUE)
					
					SET_ENTITY_AS_MISSION_ENTITY(vehBodhi2, TRUE, TRUE)
					vehID_TrevorsCar = vehBodhi2
					
					bRadioSetup_Bodhi2 = TRUE
				ENDIF
			ENDIF
			
			VEHICLE_INDEX vehBlazer
			INT iFlags2
			iFlags2 = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
			vehBlazer = GET_CLOSEST_VEHICLE(<< 1981.93, 3829.35, 31.89 >>, 10.0, Blazer, iFlags2)
			IF DOES_ENTITY_EXIST(vehBlazer)
			AND IS_VEHICLE_DRIVEABLE(vehBlazer)
			
				// Set the radio station
				IF NOT bRadioSetup_Blazer
//					PRINTLN("MAGDEMO: Setting up Blazer radio")
//					SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
//					FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
//					SET_RADIO_TRACK("RADIO_01_CLASS_ROCK", "RADIO_01_CLASS_ROCK_MAG_DEMO_LOOP_ROCK")
//					SET_RADIO_AUTO_UNFREEZE(TRUE)
					
					SET_ENTITY_AS_MISSION_ENTITY(vehBlazer, TRUE, TRUE)
					vehID_TrevorsQuad = vehBlazer
					
					bRadioSetup_Blazer = TRUE
				ENDIF
			ENDIF
			
			// Clear gps when player reaches destination
			IF bGPSRouteSet
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 895.0152, 3636.4546, 31.7189 >>, <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS>>)
					CLEAR_GPS_MULTI_ROUTE()
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					bGPSRouteSet = FALSE
				ENDIF
			ENDIF
			
			// Turn off the roads when we get to the jerry can area and suppress help text
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << 903.22, 3657.42, 31.61 >>) < 130
				
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PU_HLP")
						CLEAR_HELP()
						PRINTLN("MAGDEMO: Clearing help text PU_HLP")
					ENDIF
					
					IF NOT bTurnedOffTrevorRoads
						PRINTLN("MAGDEMO: Turning off roads for end of Trevor stage")
						SET_ROADS_IN_ANGLED_AREA(<<1440.352417,3672.524658,10.284130>>, <<415.225677,3596.012695,59.100624>>, 250.0, FALSE, TRUE)
						//SET_VEHICLE_DENSITY_MULTIPLIER(0.0)
						bTurnedOffTrevorRoads = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Wait for player to switch to Franklin
			IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				bGPSRouteSet = FALSE
				CLEAR_GPS_MULTI_ROUTE()
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				CLEAR_HINT_CAM()
				SET_MD_STAGE(MD_STAGE_FRANKLIN_DRIVE)
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			iControl = 1
			MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_FRANKLIN)	//, PR_TYPE_AMBIENT)
		ENDIF
	#ENDIF
ENDPROC

PROC DO_STAGE_FRANKLIN_DRIVE()
	SWITCH iControl
		CASE 0
		
			CLEANUP_PREVIOUS_STAGE_ASSETS()
		
			SET_MAGDEMO_SCENE()
			
			// Set characters we can switch to
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
			
			REQUEST_VEHICLE_RECORDING(001, "scene_m_fbi2_")
			
			IF LOAD_MODEL_ASSET(NINEF2, bModelRequest_Ninef2)
			AND LOAD_MODEL_ASSET(PROP_BUSKER_HAT_01, bModelRequest_MimeHat)
			
				// Franklins car outside muscle beach juice bar
				IF DOES_ENTITY_EXIST(vehID_FranklinCar)
					DELETE_VEHICLE(vehID_FranklinCar)
				ENDIF
				vehID_FranklinCar = CREATE_VEHICLE(NINEF2, << -1189.5853, -1530.3488, 3.3942 >>, 306.7344, FALSE, FALSE)
				
				SET_VEHICLE_COLOURS(vehID_FranklinCar,			38, 38)
				SET_VEHICLE_EXTRA_COLOURS(vehID_FranklinCar,	37, 0)
				
				SET_VEHICLE_TYRES_CAN_BURST(vehID_FranklinCar, FALSE)
				SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(vehID_FranklinCar, TRUE)
				
				RAISE_CONVERTIBLE_ROOF(vehID_FranklinCar, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_FranklinCar)
				bLowerRoofOnVehEntry = TRUE
				
				// Radio station
				bRadioSetup_Ninef2 = FALSE
				
				// Mime hat
				objID_MimeHat = CREATE_OBJECT(PROP_BUSKER_HAT_01, << -1203.84, -1547.44, 3.29 >>, FALSE, FALSE)
				
				CLEANUP_MODEL_ASSET(NINEF2, bModelRequest_Ninef2)
				CLEANUP_MODEL_ASSET(PROP_BUSKER_HAT_01, bModelRequest_MimeHat)
				
				CLEAR_GPS_MULTI_ROUTE()
				START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1180.6521, -1525.1920, 3.3951 >>) // Goma St
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1162.4807, -1469.4825, 3.3640 >>) // Magellan Ave
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1163.1174, -1413.8507, 3.7935 >>) // Aguja St
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1117.6749, -1386.2495, 4.1312 >>) // Bay City Avenue
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1069.4630, -1355.6085, 4.1434 >>) // Goma St
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1011.1686, -1270.0898, 5.0311 >>) // Palomino Ave
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -737.4883, -1106.3109, 10.1622 >>) // S Rockford Drive
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -630.4440, -914.2029, 23.1145 >>) // Streetname missing
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -617.4307, -866.2444, 24.0506 >>) // Vespucci Blvd
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -466.3704, -845.9417, 29.4671 >>) // Calais Av
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -338.9257, -858.3804, 30.5959 >>) // Los Puerta Fwy
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -224.9029, -880.0365, 28.9778 >>) // Peaceful St
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -164.6611, -850.4457, 29.0890 >>) // Alta St
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -104.3242, -681.2265, 34.1526 >>) // San Andreas Ave
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -73.0694, -596.5261, 35.3148 >>) // Pillbox Hill
				ADD_POINT_TO_GPS_MULTI_ROUTE(<< -104.6129, -610.6462, 35.0561 >>) // Pillbox Hill
				SET_GPS_MULTI_ROUTE_RENDER(TRUE)
				bGPSRouteSet = TRUE
				
				SET_HINT_CAM(<< 722.11, 1194.88, 324.72 >>, <<-75.158951,-588.093567,35.313877>>, <<10.122264,-339.821533,47.310104>>, 94.562500)
				
				// Unlock secirity random event
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV1)
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV4)
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV5)
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV6)
				REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_SECV7)
				
				iControl++
			ENDIF
		BREAK
		CASE 1
			// Manage Franklins vehicle created by this script
			IF DOES_ENTITY_EXIST(vehID_FranklinCar)
			AND IS_VEHICLE_DRIVEABLE(vehID_FranklinCar)
			
				// Set the radio station
				IF NOT bRadioSetup_Ninef2
					PRINTLN("MAGDEMO: Setting up NINEF2 radio")
					SET_INITIAL_PLAYER_STATION("RADIO_15_MOTOWN")
					FREEZE_RADIO_STATION("RADIO_15_MOTOWN")
					SET_RADIO_AUTO_UNFREEZE(FALSE)
					//SET_RADIO_TRACK("RADIO_15_MOTOWN", "RADIO_15_MOTOWN_MAG_DEMO_LOOP_MOTT")
					bRadioSetup_Ninef2 = TRUE
					bRadioTurnedOn_Ninef2 = FALSE
					iRadioTurnOnTimer_Ninef2 = -1
				
				// Turn on the radio when Franklin gets in the vehicle
				ELIF NOT bRadioTurnedOn_Ninef2
					IF GET_IS_VEHICLE_ENGINE_RUNNING(vehID_FranklinCar)
						IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehID_FranklinCar, VS_DRIVER))
							IF iRadioTurnOnTimer_Ninef2 = -1
								iRadioTurnOnTimer_Ninef2 = GET_GAME_TIMER()
							ELIF (GET_GAME_TIMER() - iRadioTurnOnTimer_Ninef2) > iRadioTurnOnDelay_Ninef2
								PRINTLN("MAGDEMO: Turning on NINEF2 radio")
								UNFREEZE_RADIO_STATION("RADIO_15_MOTOWN") 
								bRadioTurnedOn_Ninef2 = TRUE
							ENDIF
						ELSE
							iRadioTurnOnTimer_Ninef2 = -1
						ENDIF
					ENDIF
				ENDIF
				
				// Keep engine health above 400
				IF GET_VEHICLE_ENGINE_HEALTH(vehID_FranklinCar) < 400
					SET_VEHICLE_ENGINE_HEALTH(vehID_FranklinCar, 450)
				ENDIF
			ENDIF
			
			// Clear gps when player reaches destination
			IF bGPSRouteSet
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -63.8986, -567.1836, 37.3575 >>, <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS>>)
					CLEAR_GPS_MULTI_ROUTE()
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					bGPSRouteSet = FALSE
				ENDIF
			ENDIF
			
			// Lower the roof of the ninef on entry
			IF bLowerRoofOnVehEntry
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_VEHICLE_DRIVEABLE(vehID_FranklinCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehID_FranklinCar)
						IF GET_IS_VEHICLE_ENGINE_RUNNING(vehID_FranklinCar)
							IF GET_PED_IN_VEHICLE_SEAT(vehID_FranklinCar, VS_DRIVER) = PLAYER_PED_ID()
								IF GET_CONVERTIBLE_ROOF_STATE(vehID_FranklinCar) = CRS_RAISED
									LOWER_CONVERTIBLE_ROOF(vehID_FranklinCar, FALSE)
									bLowerRoofOnVehEntry = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Create the Police car that passes us when we pass the Secuirty Van random event
			IF NOT bPoliceCreated
			
				vCopCoord[0] = << -311.2928, -846.2677, 31.3261 >>	fCopHeading[0] = 79.1191
				vCopCoord[1] = << -300.1753, -848.3715, 31.3886 >>	fCopHeading[1] = 79.2345
				vCopCoord[2] = << -289.2143, -850.4190, 31.3895 >>	fCopHeading[2] = 79.0697
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCopCoord[0]) < 400
				
					IF LOAD_MODEL_ASSET(POLICE, bModelRequest_Police)
					AND LOAD_MODEL_ASSET(S_M_Y_Cop_01, bModelRequest_Cop)
						
						INT iCop
						REPEAT 3 iCop
						
							IF DOES_ENTITY_EXIST(vehID_Police[iCop])
								DELETE_VEHICLE(vehID_Police[iCop])
							ENDIF
						
							vehID_Police[iCop] = CREATE_VEHICLE(POLICE, vCopCoord[iCop], fCopHeading[iCop], FALSE, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehID_Police[iCop])
							SET_VEHICLE_DOORS_LOCKED(vehID_Police[iCop], VEHICLELOCK_CANNOT_ENTER)
							FREEZE_ENTITY_POSITION(vehID_Police[iCop], TRUE)
							SET_VEHICLE_SIREN(vehID_Police[iCop], TRUE)
							
							pedID_Cop[iCop] = CREATE_PED_INSIDE_VEHICLE(vehID_Police[iCop], PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER, FALSE, FALSE)
							SET_PED_DEFAULT_COMPONENT_VARIATION(pedID_Cop[iCop])
						ENDREPEAT
						
						CLEANUP_MODEL_ASSET(POLICE, bModelRequest_Police)
						CLEANUP_MODEL_ASSET(S_M_Y_Cop_01, bModelRequest_Cop)
						
						bPoliceCreated = TRUE
						bPoliceTaskGiven = FALSE
						bPoliceAudioSetup = FALSE
					ENDIF
				ENDIF
				
			ELIF NOT bPoliceAudioSetup
			
				INT iCop
				REPEAT 3 iCop
					IF DOES_ENTITY_EXIST(vehID_Police[iCop])
					AND IS_VEHICLE_DRIVEABLE(vehID_Police[iCop])
						PRINTLN("MAGDEMO: Adding cop vehicle , ", iCop, " to audio mix group MAGDEMO_SIRENS_FRANKLIN_MIXGROUP")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehID_Police[iCop], "MAGDEMO_SIRENS_FRANKLIN_MIXGROUP")
					ENDIF
				ENDREPEAT
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("MAGDEMO_SIRENS_FRANKLIN")
					PRINTLN("MAGDEMO: Starting audio scene MAGDEMO_SIRENS_FRANKLIN")
					START_AUDIO_SCENE("MAGDEMO_SIRENS_FRANKLIN")
				ENDIF
				
				bPoliceAudioSetup = TRUE
				
			ELIF NOT bPoliceTaskGiven
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(pedID_Cop[0])
				AND IS_VEHICLE_DRIVEABLE(vehID_Police[0])
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehID_Police[0])) < 200
					
					PRINTLN("MAGDEMO: Tasking cop peds to drive to target")
					
					CLEAR_AREA(vCopCoord[0], 25.0, TRUE)
					
					INT iCop
					REPEAT 3 iCop
						IF NOT IS_PED_INJURED(pedID_Cop[iCop])
						AND IS_VEHICLE_DRIVEABLE(vehID_Police[iCop])
							FREEZE_ENTITY_POSITION(vehID_Police[iCop], FALSE)
							
							CLEAR_PED_TASKS(pedID_Cop[iCop])
							TASK_VEHICLE_MISSION_COORS_TARGET(pedID_Cop[iCop], vehID_Police[iCop], << -831.7271, -832.5468, 18.5985 >>, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS, -1, -1, TRUE)
							
							// Boost speed
							SET_VEHICLE_FORWARD_SPEED(vehID_Police[iCop], 30.0 - (iCop*5.0))
						ENDIF
					ENDREPEAT
					
					bPoliceTaskGiven = TRUE
				ENDIF
			ENDIF
			
			// Create Maverick for interal demo
			IF NOT DOES_ENTITY_EXIST(vehID_FranklinHeli)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<10.209121,-575.036804,36.644875>>, <<-37.664272,-558.832642,45.459064>>, 26.625000)
						IF LOAD_MODEL_ASSET(MAVERICK, bModelRequest_Maverick)
							
							IF DOES_ENTITY_EXIST(vehID_FranklinHeli)
								DELETE_VEHICLE(vehID_FranklinHeli)
							ENDIF
							
							vehID_FranklinHeli = CREATE_VEHICLE(MAVERICK, << -12.0208, -568.7522, 36.7434 >>, 281.7917, FALSE, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehID_FranklinHeli)
							CLEANUP_MODEL_ASSET(MAVERICK, bModelRequest_Maverick)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Pre-stream the fbi2 cutscene when we're near the end of the drive
			IF NOT bRequestedFBI2Cutscene
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -104.6129, -610.6462, 35.0561 >>) < 100
						//PRINTLN("MAGDEMO: Requesting FBI2 cutscene 'fbi_2_mcs_1'")
						//bRequestedFBI2Cutscene = TRUE
						//REQUEST_CUTSCENE("fbi_2_mcs_1")
					ENDIF
				ENDIF
			ENDIF
			
			// Wait for player to switch to Michael
			IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				bGPSRouteSet = FALSE
				CLEAR_GPS_MULTI_ROUTE()
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				CLEAR_HINT_CAM()
				SET_MD_STAGE(MD_STAGE_FBI2)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT bPedWallReset
		// Set ped walla when the sync scene starts
		IF NOT bPedWallSet
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			AND GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) < 0.18
				// Wait for it to get to the correct stage.
			ELSE
				PRINTLN("MAGDEMO: Setting ped walla density yo 0.85")
				SET_PED_WALLA_DENSITY(0.85, 1.0)
				bPedWallSet = TRUE
			ENDIF
		ENDIF
	
		// Reset ped walla when player gets in vehicle
		IF bPedWallSet
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehID_FranklinCar)
				AND IS_VEHICLE_DRIVEABLE(vehID_FranklinCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehID_FranklinCar)
						PRINTLN("MAGDEMO: Resetting ped walla density")
						SET_PED_WALLA_DENSITY(0.5, 0.0)
						bPedWallSet = FALSE
						bPedWallReset = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			iControl = 1
			MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_MICHAEL)	//, PR_TYPE_AMBIENT)
		ENDIF
	#ENDIF
ENDPROC

PROC DO_STAGE_FBI2()

	TEXT_LABEL_15 sMissionName = "fbi2"
	
	SWITCH iControl
		CASE 0
		
			CLEANUP_PREVIOUS_STAGE_ASSETS()
			
			SET_MAGDEMO_SCENE()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX tempVeh
					tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(tempVeh)
					AND IS_VEHICLE_DRIVEABLE(tempVeh)
						IF GET_ENTITY_MODEL(tempVeh) = TAILGATER
							g_sMagDemoFBI2Entities.blip = CREATE_BLIP_FOR_COORD(<<1366.83, -2029.61, 51.04>>, TRUE)
							g_sMagDemoFBI2Entities.bBlipActive = TRUE
							
							SETTIMERA(0)
							iControl++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF TIMERA() > 2000
				REQUEST_SCRIPT(sMissionName)
			    IF HAS_SCRIPT_LOADED(sMissionName)
					IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					
						THREADID threadMission
						threadMission = START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)
						
						IF bRequestedFBI2Cutscene
							IF HAS_THIS_CUTSCENE_LOADED("fbi_2_mcs_1")
								SET_SCRIPT_CAN_START_CUTSCENE(threadMission)
							ENDIF
						ENDIF
						bRequestedFBI2Cutscene = FALSE
						
						SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
						iControl++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			
			IF DOES_BLIP_EXIST(g_sMagDemoFBI2Entities.blip)
				IF NOT g_sMagDemoFBI2Entities.bBlipActive
					REMOVE_BLIP(g_sMagDemoFBI2Entities.blip)
				ENDIF
			ENDIF
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fbi2")) = 0
				//bReplayFBI2 = (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_2))
				bReplayFBI2 = (NOT HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_FBI2))
				
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				SET_MD_STAGE(MD_STAGE_RESET)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Cleans up all script assets and states.
PROC CLEANUP_SCRIPT(BOOL bTerminateScript)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehID_MichaelBike)
		DELETE_VEHICLE(vehID_MichaelBike)
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_MichaelCar)
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehID_MichaelCar)
			DELETE_VEHICLE(vehID_MichaelCar)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_FranklinCar)
		DELETE_VEHICLE(vehID_FranklinCar)
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_FranklinHeli)
		DELETE_VEHICLE(vehID_FranklinHeli)
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_TrevorsCar)
		DELETE_VEHICLE(vehID_TrevorsCar)
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_TrevorsQuad)
		DELETE_VEHICLE(vehID_TrevorsQuad)
	ENDIF
	IF DOES_ENTITY_EXIST(vehID_TrevorsRebel)
		DELETE_VEHICLE(vehID_TrevorsRebel)
	ENDIF
	INT iCop
	REPEAT 3 iCop
		IF DOES_ENTITY_EXIST(pedID_Cop[iCop])
			DELETE_PED(pedID_Cop[iCop])
		ENDIF
		IF DOES_ENTITY_EXIST(vehID_Police[iCop])
			DELETE_VEHICLE(vehID_Police[iCop])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
	AND NOT IS_PED_INJURED(g_pScene_buddy)
	
		SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)

		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_pScene_buddy)
			DELETE_PED(g_pScene_buddy)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(objID_MimeHat)
		DELETE_OBJECT(objID_MimeHat)
	ENDIF
	IF DOES_PICKUP_EXIST(pickupID_PetrolTank)
		REMOVE_PICKUP(pickupID_PetrolTank)
	ENDIF
	
	IF bRequestedFBI2Cutscene
		REMOVE_CUTSCENE()
		bRequestedFBI2Cutscene = FALSE
	ENDIF
	
	// Cop car sirens
	IF IS_AUDIO_SCENE_ACTIVE("MAGDEMO_SIRENS_FRANKLIN")
		PRINTLN("MAGDEMO: Stopping audio scene MAGDEMO_SIRENS_FRANKLIN")
		STOP_AUDIO_SCENE("MAGDEMO_SIRENS_FRANKLIN")
	ENDIF
	
	// End of trevor drive
	PRINTLN("MAGDEMO: Turning on roads for end of Trevor stage")
	SET_ROADS_IN_ANGLED_AREA(<<1440.352417,3672.524658,10.284130>>, <<415.225677,3596.012695,59.100624>>, 250.0, FALSE, TRUE)
	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	bTurnedOffTrevorRoads = FALSE
	
	CLEAR_GPS_MULTI_ROUTE()
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	bGPSRouteSet = FALSE
	
	CLEAR_HINT_CAM()
	
	CLEANUP_MODEL_ASSET(SCORCHER, bModelRequest_Scorcher)
	CLEANUP_MODEL_ASSET(NINEF2, bModelRequest_Ninef2)
	CLEANUP_MODEL_ASSET(MAVERICK, bModelRequest_Maverick)
	CLEANUP_MODEL_ASSET(BLAZER, bModelRequest_Blazer)
	CLEANUP_MODEL_ASSET(BODHI2, bModelRequest_Bodhi2)
	CLEANUP_MODEL_ASSET(REBEL, bModelRequest_Rebel)
	CLEANUP_MODEL_ASSET(TAILGATER, bModelRequest_Tailgater)
	CLEANUP_MODEL_ASSET(POLICE, bModelRequest_Police)
	CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
	CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_TREVOR), bModelRequest_Trevor)
	CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), bModelRequest_Franklin)
	CLEANUP_MODEL_ASSET(GET_NPC_PED_MODEL(CHAR_DAVE), bModelRequest_Dave)
	CLEANUP_MODEL_ASSET(S_M_Y_Cop_01, bModelRequest_Cop)
	CLEANUP_MODEL_ASSET(PROP_BUSKER_HAT_01, bModelRequest_MimeHat)
	CLEANUP_ANIM_ASSET("SWITCH@MICHAEL@SUNLOUNGER", bAnimRequest_Lounger)
	CLEANUP_AUDIO_ASSET("Magdemo_Sunlounger", bAudioRequest_Lounger)
	
	missionCandidateID = NO_CANDIDATE_ID
	
	// Reset radio flags
	bRadioSetup_Ninef2 = FALSE
	bRadioTurnedOn_Ninef2 = FALSE
	bRadioSetup_Bodhi2 = FALSE
	bRadioSetup_Blazer = FALSE
	bRadioSetup_Sentinel2 = FALSE
	bRadioTurnedOn_Sentinel2 = FALSE
	
	eStoredModel = DUMMY_MODEL_FOR_SCRIPT
	
	IF DOES_BLIP_EXIST(g_sMagDemoFBI2Entities.blip)
		REMOVE_BLIP(g_sMagDemoFBI2Entities.blip)
	ENDIF
	g_sMagDemoFBI2Entities.bBlipActive = FALSE
	
	g_bMagDemoFBI2Retry = FALSE
	
	SET_PED_WALLA_DENSITY(0.5, 0.0)
	bPedWallSet = FALSE
	bPedWallReset = FALSE
	
	// Ending magdemo so ensure gameplay can resume
	IF bTerminateScript
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		ENDIF
		
		// Fix for bug #814560 - Hiding bin bags as they have no alpha, causing them to pop
		IF bHidingBinBag
			REMOVE_MODEL_HIDE(<<1927.89, 3925.41, 31.42>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("prop_rub_binbag_03b")), TRUE)
			bHidingBinBag = FALSE
		ENDIF
		
		// Unblock areas
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockID_fbi2)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockID_trevorDrive)
		// - Hill outside Michaels mansion
		SET_PED_PATHS_IN_AREA(<< -881.7, 86.3, 45.5 >>, << -839.7, 216.2, 82.6 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -881.6, 0.4, 31.8 >>, << -819.5, 96.4, 68.9 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -845.9, -76.4, 20.3 >>, << -771.6, 38.8, 58.4 >>, TRUE)
		// - Muscle beach
		SET_PED_PATHS_IN_AREA(<< -1177.2, -1523.7, 0 >>, << -1168.4, -1518.2, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1180.9, -1526.9, 0 >>, << -1172.3, -1521.4, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1185.4, -1530.3, 0 >>, << -1176.8, -1524.6, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1189.7, -1533.0, 0 >>, << -1181.1, -1527.6, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1193.9, -1535.8, 0 >>, << -1185.3, -1530.4, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1198.1, -1538.8, 0 >>, << -1189.4, -1533.3, 10 >>, TRUE)
		SET_PED_PATHS_IN_AREA(<< -1197.2, -1541.7, 0 >>, << -1192.5, -1536.7, 10 >>, TRUE)
		
		g_bMagDemoActive = FALSE
		
		// Reset flags
		g_bTaxiHailingIsDisabled = FALSE
		
		DO_SCREEN_FADE_IN(1000)
		STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
		
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_MAX_WANTED_LEVEL(5)
		PAUSE_CLOCK(FALSE)
		SCRIPT_OVERRIDES_WIND_TYPES(FALSE, "", "")
		
		SET_RANDOM_TRAINS(TRUE)	//740410
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF2, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(UTILLITRUCK2, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(UTILLITRUCK3, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(FBI2, FALSE)
		
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

PROC DO_STAGE_RESET()
	// Fade out and then delete all assets
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(1000)
			START_AUDIO_SCENE("MISSION_FAILED_SCENE")
		ENDIF
	
	// Only process when player is alive
	ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		CLEANUP_SCRIPT(FALSE)
		
		SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)
		
		// Failed FBI2 so retry
		IF bReplayFBI2
#IF IS_DEBUG_BUILD
		OR (sZSkipData.bDoSkip AND (sZSkipData.iSelectedStage = 4)) // "FBI2"
#ENDIF
		
			SET_INITIAL_MAGDEMO_GAME_STATE()
			
			g_bMagDemoFBI2Retry = TRUE
			
			// Set up the fbi2 scene
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			
			PED_REQUEST_SCENE_ENUM eScene = PR_SCENE_Ma_FBI1end	//PR_SCENE_M_MD_FBI2
			
			// SCENE COORDS
			VECTOR vCreateCoords    //coord to place the player in the scene
			FLOAT fCreateHead       //heading of the player for the scene
			TEXT_LABEL_31 tRoom     //room name for the scene if in an interior (not needed for FBI2)
			GET_PLAYER_PED_POSITION_FOR_SCENE(eScene, vCreateCoords, fCreateHead, tRoom)
			
			// SCENE ASSETS
			PED_SCENE_STRUCT sPedScene
			PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
			sPedScene.eScene = eScene
			SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)

			//SCENE VEHICLE
			PED_VEH_DATA_STRUCT sVehData                                //veh data struct for creating the vehicle. But as it’s the player car this shouldn’t be needed
			VECTOR vVehCoordsOffset                                     //<<0,0,0>> for scene as Player is in the vehicle
			FLOAT fVehHeadOffset
			VECTOR vDriveOffset                                         //where player will drive the vehicle to
			FLOAT fDriveSpeed                                           //how fast the player will drive the vehicle
			GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_MICHAEL, eScene, sVehData, vVehCoordsOffset, fVehHeadOffset, vDriveOffset, fDriveSpeed)
			
			WHILE NOT LOAD_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
			OR NOT LOAD_MODEL_ASSET(TAILGATER, bModelRequest_Tailgater)
			OR NOT LOAD_MODEL_ASSET(GET_NPC_PED_MODEL(CHAR_DAVE), bModelRequest_Dave)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL, FALSE)
				WAIT(0)
			ENDWHILE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT , OUTFIT_P0_DEFAULT, FALSE)
			ENDIF
			
			CREATE_PLAYER_VEHICLE(vehID_MichaelCar, CHAR_MICHAEL,
					vCreateCoords+vVehCoordsOffset, fCreateHead+fVehHeadOffset,
					FALSE, VEHICLE_TYPE_CAR)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehID_MichaelCar)
			
			SET_VEH_RADIO_STATION(vehID_MichaelCar, "RADIO_01_CLASS_ROCK")
            FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
            //SET_RADIO_TRACK("RADIO_01_CLASS_ROCK", "MAG_DEMO_ADULT_EDUCATION")
            SET_RADIO_AUTO_UNFREEZE(TRUE)
			
			// #746924 
			// Can you please turn the radio off for Michael's car now
			SET_VEHICLE_RADIO_ENABLED(vehID_MichaelCar, FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehID_MichaelCar)
				TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehID_MichaelCar,
						GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID_MichaelCar, vDriveOffset),
						fDriveSpeed, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(vehID_MichaelCar),
						DRIVINGMODE_AVOIDCARS, 4.0, 30)
				SET_VEHICLE_FORWARD_SPEED(vehID_MichaelCar, fDriveSpeed)
			ENDIF
			
			WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(g_pScene_buddy, CHAR_DAVE, vehID_MichaelCar, VS_FRONT_RIGHT, FALSE)
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_PED_INJURED(g_pScene_buddy)
				SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy)
				SET_PED_CAN_BE_TARGETTED(g_pScene_buddy, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(g_pScene_buddy, RELGROUPHASH_PLAYER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_pScene_buddy, TRUE)
			ENDIF
			
			CLEANUP_MODEL_ASSET(TAILGATER, bModelRequest_Tailgater)
			CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
			CLEANUP_MODEL_ASSET(GET_NPC_PED_MODEL(CHAR_DAVE), bModelRequest_Dave)
			
			g_sMagDemoFBI2Entities.bCreated = TRUE
			
			TEXT_LABEL_31 sPlayerTimetableAdditional_script
			IF SETUP_TIMETABLE_SCRIPT_FOR_SCENE(eScene, sPlayerTimetableAdditional_script)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sPlayerTimetableAdditional_script)) <= 0
				
					REQUEST_SCRIPT(sPlayerTimetableAdditional_script)
					WHILE NOT HAS_SCRIPT_LOADED(sPlayerTimetableAdditional_script)
						WAIT(0)
					ENDWHILE
					
					START_NEW_SCRIPT(sPlayerTimetableAdditional_script, FRIEND_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sPlayerTimetableAdditional_script)

					g_sMagDemoFBI2Entities.bCreated = FALSE
					
					WHILE NOT g_sMagDemoFBI2Entities.bCreated
						
						printstring("waiting on g_sMagDemoFBI2Entities.bCreated...")
						printnl()
						
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
			
			
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			
			
			// Fade in and give control back
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
			
			SET_MD_STAGE(MD_STAGE_FBI2)
			
			#IF IS_DEBUG_BUILD
				sZSkipData.bDoSkip = FALSE
			#ENDIF
			
		// Set up the scene if we have used the Z-Skip menu
#IF IS_DEBUG_BUILD
		ELIF sZSkipData.bDoSkip
		
			SET_INITIAL_MAGDEMO_GAME_STATE()
		
			IF (sZSkipData.iSelectedStage = 0) // "Initial scene"
				// Let the initial scene set this one up
				SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)
			
			ELIF (sZSkipData.iSelectedStage = 1) // "Michael on bicycle"
			
				WHILE NOT LOAD_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
				OR NOT LOAD_MODEL_ASSET(SCORCHER, bModelRequest_Scorcher)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL, FALSE)
					WAIT(0)
				ENDWHILE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT , OUTFIT_P0_YOGA, FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehID_MichaelBike)
					DELETE_VEHICLE(vehID_MichaelBike)
				ENDIF
				
				vehID_MichaelBike = CREATE_VEHICLE(SCORCHER,<< -810.8484, 189.7630, 71.4785 >>, 131.0119, FALSE, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_MichaelBike)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehID_MichaelBike)
				ENDIF
				
				CLEANUP_MODEL_ASSET(SCORCHER, bModelRequest_Scorcher)
				CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), bModelRequest_Michael)
				
				// Fade in and give control back
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
				
				SET_MD_STAGE(MD_STAGE_MICHAEL_DRIVE)
			
			ELIF (sZSkipData.iSelectedStage = 2) // "Trevor in trailer"
			
				WHILE NOT LOAD_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_TREVOR), bModelRequest_Trevor)
				OR NOT LOAD_MODEL_ASSET(BLAZER, bModelRequest_Blazer)
				OR NOT LOAD_MODEL_ASSET(BODHI2, bModelRequest_Bodhi2)
				OR NOT LOAD_MODEL_ASSET(REBEL, bModelRequest_Rebel)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR, FALSE)
					WAIT(0)
				ENDWHILE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P2_SWEAT_PANTS, FALSE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1972.5209, 3816.7981, 33.4337 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 210.9937)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehID_TrevorsCar)
					DELETE_VEHICLE(vehID_TrevorsCar)
				ENDIF
				
				CREATE_PLAYER_VEHICLE(vehID_TrevorsCar, CHAR_TREVOR, << 1982.8802, 3807.0273, 31.2023 >>, 299.7608, FALSE, VEHICLE_TYPE_CAR)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_TrevorsCar)
				
				IF DOES_ENTITY_EXIST(vehID_TrevorsQuad)
					DELETE_VEHICLE(vehID_TrevorsQuad)
				ENDIF
				
				vehID_TrevorsQuad = CREATE_VEHICLE(BLAZER, << 1981.9261, 3829.3467, 31.3876 >>, 262.5776, FALSE, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehID_TrevorsQuad)
				
				CLEANUP_MODEL_ASSET(BODHI2, bModelRequest_Bodhi2)
				CLEANUP_MODEL_ASSET(BLAZER, bModelRequest_Blazer)
				CLEANUP_MODEL_ASSET(REBEL, bModelRequest_Rebel)
				CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_TREVOR), bModelRequest_Trevor)
				
				// Fade in and give control back
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
				
				SET_MD_STAGE(MD_STAGE_TREVOR_DRIVE)
			
			ELIF (sZSkipData.iSelectedStage = 3) // "Franklin at muscle beach"
				
				WHILE NOT LOAD_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), bModelRequest_Franklin)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN, FALSE)
					WAIT(0)
				ENDWHILE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1174.7980, -1573.3339, 4.3630 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 172.9187)
				ENDIF
				
				CLEANUP_MODEL_ASSET(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), bModelRequest_Franklin)
				
				SET_MD_STAGE(MD_STAGE_FRANKLIN_DRIVE)
				
				// Fade in and give control back
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			sZSkipData.bDoSkip = FALSE
#ENDIF
		ENDIF
		
		SET_MAGDEMO_SCENE()
		
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC SETUP_MAGDEMO_WIDGETS()
		START_WIDGET_GROUP("MAGDEMO")
			ADD_WIDGET_BOOL("Display street names", bDisplayStreetNames)
			ADD_WIDGET_BOOL("Display area names", bDisplayAreaNames)
			ADD_WIDGET_INT_SLIDER("Ninef2 - Radio Delay (ms)", iRadioTurnOnDelay_Ninef2, 0, 10000, 100)
			ADD_WIDGET_BOOL("Ninef2 - Setup", bRadioSetup_Ninef2)
			ADD_WIDGET_BOOL("Kill script", bKillScript)
		STOP_WIDGET_GROUP()
		
		sZSkipData.SkipMenuStruct[0].sTxtLabel = "Michael sitting by the pool"
		sZSkipData.SkipMenuStruct[1].sTxtLabel = "Michael on bicycle"
		sZSkipData.SkipMenuStruct[2].sTxtLabel = "Trevor in trailer"
		sZSkipData.SkipMenuStruct[3].sTxtLabel = "Franklin at muscle beach"
		sZSkipData.SkipMenuStruct[4].sTxtLabel = "FBI2"
	ENDPROC

	PROC MAINTAIN_MAGDEMO_WIDGETS()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
				IF GET_HASH_KEY(GET_NAME_OF_SCRIPT_TO_AUTOMATICALLY_START()) != GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
					SET_MD_STAGE(MD_STAGE_CLEANUP)
				ENDIF
			ENDIF
		ENDIF
		IF bDisplayStreetNames
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		ENDIF
		IF bDisplayAreaNames
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		ENDIF
		IF bKillScript
			SET_MD_STAGE(MD_STAGE_CLEANUP)
		ENDIF
		
		IF eMDStage != MD_STAGE_RESET
		AND eMDStage != MD_STAGE_CLEANUP
		AND NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF LAUNCH_MISSION_STAGE_MENU(sZSkipData.SkipMenuStruct, sZSkipData.iSelectedStage, sZSkipData.iCurrentStage, FALSE, "MAGDEMO", FALSE)
				sZSkipData.bDoSkip = TRUE
				SET_MD_STAGE(MD_STAGE_RESET)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT

	/*
		NOTES
		
		-runscript=magdemo
		  startup.sc checks if the magdemo has been specificed and sets the g_bMagDemoActive to TRUE.
		  It then requests and starts this script once main_persistent.sc has been started.
		  main.sc no longer fades the game in if g_bMagDemoActive is set to TRUE.
		  player_controller.sc no longer sets a random character on startup if g_bMagDemoActive is set to TRUE.
		  
		- DO_STAGE_CREATE_INITIAL_SCENE()
		  This proc must wait for initial.sc to terminate for the force cleanup to be successful.
		  
	*/

	PRINTLN("LAUNCHING MAGDEMO SCRIPT")
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CLEANUP_SCRIPT(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SETUP_MAGDEMO_WIDGETS()
	#ENDIF
	
	WHILE TRUE
	
		#IF IS_DEBUG_BUILD
			MAINTAIN_MAGDEMO_WIDGETS()
		#ENDIF
		
		SWITCH eMDStage
		
			////////////////////////////////////////////////////////////////
			///       CREATE INITIAL SCENE
			CASE MD_STAGE_CREATE_INITIAL_SCENE
				DO_STAGE_CREATE_INITIAL_SCENE()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       MICHAEL DRIVES DOWN STREET TO ROCKFORD HILLS SIGN
			CASE MD_STAGE_MICHAEL_DRIVE
				DO_STAGE_STAGE_MICHAEL_DRIVE()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       TREVOR DRIVES AROUND SANDY SHORES TO A LIQUOR SIGN
			CASE MD_STAGE_TREVOR_DRIVE
				DO_STAGE_TREVOR_DRIVE()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       FRANKLIN DRIVES AROUND THE CITY TOWARD VINEWOOD
			CASE MD_STAGE_FRANKLIN_DRIVE
				DO_STAGE_FRANKLIN_DRIVE()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       TRIGGER FBI2 MISSION
			CASE MD_STAGE_FBI2
				DO_STAGE_FBI2()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       RESET
			CASE MD_STAGE_RESET
				DO_STAGE_RESET()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       CLEANUP
			CASE MD_STAGE_CLEANUP
				CLEANUP_SCRIPT(TRUE)
			BREAK
		ENDSWITCH
		
		UPDATE_HINT_CAM()
		
		UPDATE_PLAYER_PED_STATES()
		
		// #826620 block shocking events throughout entire magdemo
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_AFFECTS_OTHERS)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_POTENTIALLY_DANGEROUS)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_SERIOUS_DANGER)
		
		// #739576 always suppress scenario exits
		SUPPRESS_NORMAL_SCENARIO_EXITS_NEXT_FRAME()
		
		// #725141
		SUPPRESS_SCENARIO_ATTRACTION_NEXT_FRAME()
		
		// #728489
		SUPPRESS_BREAKOUT_SCENARIO_EXITS_NEXT_FRAME()
		
		// #831289 and #824566
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_PURSUE_WHEN_HIT_BY_CAR, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_STEAL_VEHICLE, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_JAY_WALK_LIGHTS, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_ON_FOOT_FROM_VEHICLE, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_ON_FOOT, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_WARNING, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_FLEE, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_VEHICLE_DRIVING_FAST, FALSE)
		
		// #834333
		SUPPRESS_AGITATION_EVENTS_NEXT_FRAME()
		
		
		WAIT(0)
		
	ENDWHILE
ENDSCRIPT
#ENDIF

