//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	fame_or_shame_set.sc 										//
//		AUTHOR			:	Craig Vincent												//
//		DESCRIPTION		:																//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF


// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "chase_hint_cam.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
USING "flow_mission_trigger_public.sch" //trigger stuff

OBJECT_INDEX objstage
OBJECT_INDEX objmic
OBJECT_INDEX objdesk

proc Mission_cleanup()

	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
	
	if DOES_ENTITY_EXIST(objmic)
		DELETE_OBJECT(objmic)
	endif
	if DOES_ENTITY_EXIST(objstage)
		DELETE_OBJECT(objstage)
	endif
	if DOES_ENTITY_EXIST(objdesk)
		DELETE_OBJECT(objdesk)
	endif
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(true)
	TERMINATE_THIS_THREAD()
	
endproc
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...fame or shame Launched")
	PRINTNL()
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		PRINTSTRING("...fame or shame clean up")
		Mission_Cleanup()		
	ENDIF
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	
	if DOES_ENTITY_EXIST(player_ped_id())
	and not IS_PED_INJURED(player_ped_id())
		set_entity_coords(player_ped_id(),<<681.9520, 565.8090, 128.0459>>)
	endif
	
	REQUEST_MODEL(V_ILEV_FOS_TVSTAGE)
	request_model(V_ILev_FOS_MIC)
	request_model(V_ILev_FOS_DESK)
	
	while not HAS_MODEL_LOADED(V_ILev_FOS_MIC)
	or not HAS_MODEL_LOADED(V_ILev_FOS_DESK)
	or not HAS_MODEL_LOADED(V_ILEV_FOS_TVSTAGE)
		wait(0)
	endwhile
//	
//	objdesk = CREATE_OBJECT(V_ILev_FOS_DESK,<<677.415, 578.216, 129.461>>)
//	SET_ENTITY_ROTATION(objdesk,<<0.0, 0.0, 35.0>>)
//	objmic = CREATE_OBJECT(V_ILev_FOS_MIC,<<683.415, 569.716, 129.461>>)
//	SET_ENTITY_ROTATION(objmic,<<0,0,-20>>)	
//	objstage = CREATE_OBJECT(V_ILEV_FOS_TVSTAGE,<<687.215, 580.716, 129.461>>)
//	set_entity_rotation(objstage,<<0,0,-20>>)
	
	
	WHILE TRUE	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)			
			Mission_Cleanup()	
		ENDIF		
		WAIT(0)		
	ENDWHILE
ENDSCRIPT

#ENDIF
