//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	magdemo2.sc 												//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:																//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_streaming.sch"
USING "commands_event.sch"
USING "commands_brains.sch"
USING "commands_cutscene.sch"

USING "player_ped_public.sch"
USING "selector_public.sch"
USING "respawn_location_private.sch"
USING "flow_reset_GAME.sch"
USING "script_blip.sch"
USING "chase_hint_cam.sch"
USING "select_mission_stage.sch"
USING "player_ped_scenes.sch"

USING "family_public.sch"


	// Widget data
	BOOL bKillScript
	
	INT iJSkipStage
	
	STRUCT Z_SKIP_DATA
		INT iSelectedStage
		INT iCurrentStage
		BOOL bDoSkip
		MissionStageMenuTextStruct SkipMenuStruct[4]
	ENDSTRUCT
	Z_SKIP_DATA sZSkipData


// Script data
ENUM MD_STAGE_ENUM
	MD_STAGE_CREATE_INITIAL_SCENE = 0,
	MD_STAGE_FRANKLIN_BASEJUMP,
	MD_STAGE_TREVOR_DIVING,
	MD_STAGE_MICHAEL_WALK_AROUND_CITY,
	MD_STAGE_FRA2,
	MD_STAGE_RESET,
	MD_STAGE_CLEANUP
ENDENUM
MD_STAGE_ENUM eMDStage

INT iControl = 0

BOOL bHintCam_Set = FALSE
BOOL bHintCam_Required = FALSE
BOOL bHintCam_Active = FALSE
VECTOR vHintCamCoord
VECTOR vHintCamArea1,vHintCamArea2
FLOAT fHintCamArea
CHASE_HINT_CAM_STRUCT localChaseHintCamStruct
MODEL_NAMES eStoredModel
INT missionCandidateID = NO_CANDIDATE_ID

BOOL bSafeToProgress
BOOL bFBI4LoadSceneStart
INT iNewLoadSceneStage

BOOL bAudioScene_MikeWalk
BOOL bAudioScene_TrevWake
BOOL bAudioScene_TrevBoat
BOOL bAudioScene_TrevDive
BOOL bAudioGroup_Ranger
BOOL bAudioGroup_Tourbus

INT iNavBlockID
INT iWindSound = -1

BOOL bTriggerSpaceRangerDialogue = FALSE

// FRANKLIN STAGE
//BLIP_INDEX blipTitanPlane
VEHICLE_INDEX viBaseJumpTitan
PED_INDEX piBaseJumpTitanPilot
VEHICLE_INDEX viRanger

//MICHAEL STAGE
VEHICLE_INDEX viClosestCar, viClosestCarPrev
VEHICLE_INDEX viSuperD, viVoltic
PED_INDEX piClosestCarDriver
PED_INDEX piClosestCarDriver2

// TREVOR STAGE
BOOL bRelGroupSetup
REL_GROUP_HASH sharkGroup
BLIP_INDEX blipSharks[10]
INT iBlippedSharks
BLIP_INDEX blipBoat
BLIP_INDEX blipDiverPed
//BLIP_INDEX blipSharkLocation
//BLIP_INDEX blipWreckLocation
BLIP_INDEX blipWreckDiveLocation
PED_INDEX piDiverPed
PED_INDEX piDiverPed2

BOOL bTrevorScubaOutfitPreloaded
BOOL bUnderwaterMusic
INT iSoundID_Breathing = -1

SEQUENCE_INDEX seqDiverDelay

BOOL bModelRequest_Cargo
BOOL bModelRequest_Pilot
BOOL bModelRequest_Steve, bModelRequest_Franklin
BOOL bModelRequest_Driver
BOOL bAnimRequest_Dive, bAnimRequest_Swim

// FRANKLIN 2 STAGE
BOOL bReplayFRA2
TEXT_LABEL_15 sMissionName = "franklin2"

FUNC BOOL LOAD_MODEL_ASSET(MODEL_NAMES eModel, BOOL &bRequestFlag)
	REQUEST_MODEL(eModel)
	
	#IF IS_DEBUG_BUILD
		IF NOT bRequestFlag
			PRINTLN("MAGDEMO: LOAD_MODEL_ASSET(", GET_MODEL_NAME_FOR_DEBUG(eModel), ")")
		ENDIF
	#ENDIF
	
	bRequestFlag = TRUE
	RETURN HAS_MODEL_LOADED(eModel)
ENDFUNC
PROC CLEANUP_MODEL_ASSET(MODEL_NAMES eModel, BOOL &bRequestFlag)
	IF bRequestFlag
		#IF IS_DEBUG_BUILD
			PRINTLN("MAGDEMO: CLEANUP_MODEL_ASSET(", GET_MODEL_NAME_FOR_DEBUG(eModel), ")")
		#ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

FUNC BOOL LOAD_ANIM_ASSET(STRING sAnimDict, BOOL &bRequestFlag)
	REQUEST_ANIM_DICT(sAnimDict)
	bRequestFlag = TRUE
	RETURN HAS_ANIM_DICT_LOADED(sAnimDict)
ENDFUNC
PROC CLEANUP_ANIM_ASSET(STRING sAnimDict, BOOL &bRequestFlag)
	IF bRequestFlag
		REMOVE_ANIM_DICT(sAnimDict)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

FUNC BOOL LOAD_AUDIO_ASSET(STRING sAudioBank, BOOL &bRequestFlag)
	bRequestFlag = TRUE
	RETURN REQUEST_SCRIPT_AUDIO_BANK(sAudioBank)
ENDFUNC
PROC CLEANUP_AUDIO_ASSET(STRING sAudioBank, BOOL &bRequestFlag)
	IF bRequestFlag
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(sAudioBank)
		bRequestFlag = FALSE
	ENDIF
ENDPROC

PROC CLEAR_AUDIO_SCENES()
	// Michael
	IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WALK_THROUGH_VINEWOOD")
		STOP_AUDIO_SCENE("MAG_2_WALK_THROUGH_VINEWOOD")
		bAudioScene_MikeWalk = FALSE
	ENDIF
		
	// Trevor
	IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WAKE_UP")
		STOP_AUDIO_SCENE("MAG_2_WAKE_UP")
		bAudioScene_TrevWake = FALSE
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MAG_2_DRIVE_DINGHY")
		STOP_AUDIO_SCENE("MAG_2_DRIVE_DINGHY")
		bAudioScene_TrevBoat = FALSE
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MAG_2_SCUBA_DIVE")
		STOP_AUDIO_SCENE("MAG_2_SCUBA_DIVE")
		bAudioScene_TrevDive = FALSE
	ENDIF
ENDPROC

PROC CLEANUP_PREVIOUS_STAGE_ASSETS()

	// Do a hard cleanup of all previous assets
	// Note, this should only be called when we have switched cahracter and are processing the next stage.
	
	/*
		MD_STAGE_CREATE_INITIAL_SCENE = 0,
		MD_STAGE_FRANKLIN_BASEJUMP,
		MD_STAGE_TREVOR_DIVING,
		MD_STAGE_MICHAEL_WALK_AROUND_CITY,
		MD_STAGE_FRA2,
		MD_STAGE_RESET,
		MD_STAGE_CLEANUP
	*/
	
	// Cleanup Franklin stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_FRANKLIN_BASEJUMP)
//		IF DOES_BLIP_EXIST(blipTitanPlane)
//			REMOVE_BLIP(blipTitanPlane)
//		ENDIF
		IF DOES_ENTITY_EXIST(viBaseJumpTitan)
			DELETE_VEHICLE(viBaseJumpTitan)
		ENDIF
		IF DOES_ENTITY_EXIST(piBaseJumpTitanPilot)
			DELETE_PED(piBaseJumpTitanPilot)
		ENDIF
		
		REMOVE_VEHICLE_RECORDING(2, "MAGDEMOTITAN")
		
		IF DOES_ENTITY_EXIST(viRanger)
		AND IS_VEHICLE_DRIVEABLE(viRanger)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viRanger)
			viRanger = NULL
		ENDIF
		
		// Cleanup wind sound when we jump
		IF iWindSound != -1
			PRINTLN("MAGDEMO: Stopping sound Helicopter_Wind")
			STOP_SOUND(iWindSound)
			iWindSound = -1
		ENDIF
		
		CLEANUP_MODEL_ASSET(CARGOPLANE, bModelRequest_Cargo)
		CLEANUP_MODEL_ASSET(S_M_M_PILOT_01, bModelRequest_Pilot)
	ENDIF
	
	// Cleanup Trevor stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_MICHAEL_WALK_AROUND_CITY)
		INT iBlip
		REPEAT COUNT_OF(blipSharks) iBlip
			IF DOES_BLIP_EXIST(blipSharks[iBlip])
				REMOVE_BLIP(blipSharks[iBlip])
			ENDIF
		ENDREPEAT
		iBlippedSharks = 0
		
		IF DOES_ENTITY_EXIST(piDiverPed)
			DELETE_PED(piDiverPed)
		ENDIF
		IF DOES_ENTITY_EXIST(piDiverPed2)
			DELETE_PED(piDiverPed2)
		ENDIF
		IF DOES_BLIP_EXIST(blipDiverPed)
			REMOVE_BLIP(blipDiverPed)
		ENDIF
		IF DOES_BLIP_EXIST(blipWreckDiveLocation)
			REMOVE_BLIP(blipWreckDiveLocation)
		ENDIF
		IF DOES_BLIP_EXIST(blipBoat)
			REMOVE_BLIP(blipBoat)
		ENDIF
//		IF DOES_BLIP_EXIST(blipSharkLocation)
//			REMOVE_BLIP(blipSharkLocation)
//		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("MAG_2_SCUBA_DIVE")
			STOP_AUDIO_SCENE("MAG_2_SCUBA_DIVE")
			bAudioScene_TrevDive = FALSE
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WAKE_UP")
			STOP_AUDIO_SCENE("MAG_2_WAKE_UP")
			bAudioScene_TrevWake = FALSE
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MAG_2_DRIVE_DINGHY")
			STOP_AUDIO_SCENE("MAG_2_DRIVE_DINGHY")
			bAudioScene_TrevBoat = FALSE
		ENDIF
		
		IF bTrevorScubaOutfitPreloaded
			RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			bTrevorScubaOutfitPreloaded = FALSE
		ENDIF
		
		IF bUnderwaterMusic
			IF NOT HAS_SOUND_FINISHED(iSoundID_Breathing)
				STOP_SOUND(iSoundID_Breathing)
			ENDIF
			TRIGGER_MUSIC_EVENT("MG2_STOP")
			bUnderwaterMusic = FALSE
		ENDIF
		RELEASE_SCRIPT_AUDIO_BANK()
		
		CLEANUP_MODEL_ASSET(IG_STEVEHAINS, bModelRequest_Steve)
		
		CLEANUP_ANIM_ASSET("missheistchem2", bAnimRequest_Dive)
		CLEANUP_ANIM_ASSET("SWIMMING@scuba", bAnimRequest_Swim)
	ENDIF
	
	// Cleanup Michael stage
	IF ENUM_TO_INT(eMDStage) >= ENUM_TO_INT(MD_STAGE_FRA2)
		IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WALK_THROUGH_VINEWOOD")
			STOP_AUDIO_SCENE("MAG_2_WALK_THROUGH_VINEWOOD")
			bAudioScene_MikeWalk = FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(piClosestCarDriver)
			DELETE_PED(piClosestCarDriver)
		ENDIF
		IF DOES_ENTITY_EXIST(piClosestCarDriver2)
			DELETE_PED(piClosestCarDriver2)
		ENDIF
		IF DOES_ENTITY_EXIST(viSuperD)
			DELETE_VEHICLE(viSuperD)
			SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)
		ENDIF
		IF DOES_ENTITY_EXIST(viVoltic)
			DELETE_VEHICLE(viVoltic)
			SET_MODEL_AS_NO_LONGER_NEEDED(VOLTIC)
		ENDIF
		
		CLEANUP_MODEL_ASSET(PLAYER_ONE, bModelRequest_Franklin)
		
		CLEANUP_MODEL_ASSET(G_M_Y_BALLASOUT_01, bModelRequest_Driver)
		
		bTriggerSpaceRangerDialogue = FALSE
	ENDIF
	
	
ENDPROC

PROC SET_HINT_CAM(VECTOR vCoord, VECTOR vArea1, VECTOR vArea2, FLOAT fWidth)
	vHintCamCoord = vCoord
	vHintCamArea1 = vArea1
	vHintCamArea2 = vArea2
	fHintCamArea = fWidth
	bHintCam_Set = TRUE
ENDPROC

PROC CLEAR_HINT_CAM()
	bHintCam_Set = FALSE
ENDPROC

PROC UPDATE_HINT_CAM()
	
	bHintCam_Required = FALSE
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF bHintCam_Set
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vHintCamArea1, vHintCamArea2, fHintCamArea)
			//AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				bHintCam_Required = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bHintCam_Required
		CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vHintCamCoord)
		bHintCam_Active = TRUE
	ELIF bHintCam_Active
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		bHintCam_Active = FALSE
	ENDIF
ENDPROC


INT iExitBoatStage
INT iBoatSceneID
VECTOR vScenePos, vSceneRot
VEHICLE_INDEX viPlayerBoat
CAMERA_INDEX ciUnderwaterCam

PROC UPDATE_EXIT_BOAT_CAM()

	SWITCH iExitBoatStage
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
				IF NOT IS_AUDIO_SCENE_ACTIVE("MAG_2_SCUBA_DIVE")
					START_AUDIO_SCENE("MAG_2_SCUBA_DIVE")
					bAudioScene_TrevDive = TRUE
				ENDIF
				iExitBoatStage = 7
				
			ELIF eMDStage = MD_STAGE_TREVOR_DIVING
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
				LOAD_ANIM_ASSET("missheistchem2", bAnimRequest_Dive)
				LOAD_ANIM_ASSET("SWIMMING@scuba", bAnimRequest_Swim)
				PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_SCUBA_WATER)
				bTrevorScubaOutfitPreloaded = TRUE
				iExitBoatStage++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			AND LOAD_ANIM_ASSET("missheistchem2", bAnimRequest_Dive)
			AND LOAD_ANIM_ASSET("SWIMMING@scuba", bAnimRequest_Swim)
				viPlayerBoat = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				iExitBoatStage++
				SETTIMERA(0)
			ENDIF
		BREAK
		CASE 2
			IF TIMERA() > 1250
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND DOES_ENTITY_EXIST(viPlayerBoat)
				AND IS_VEHICLE_DRIVEABLE(viPlayerBoat)
				
					//vBoatCoords = GET_ENTITY_COORDS(viPlayerBoat)
					//fBoatHead = GET_ENTITY_HEADING(viPlayerBoat)
					SET_ENTITY_COORDS(viPlayerBoat, <<3114.312, -401.92, 0.5>>)
					SET_ENTITY_HEADING(viPlayerBoat, RAD_TO_DEG(0.840))
					SET_BOAT_ANCHOR(viPlayerBoat, TRUE)
					
					// Cut the cam to under the water
					IF NOT DOES_CAM_EXIST(ciUnderwaterCam)
						ciUnderwaterCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
					ENDIF
					SET_CAM_ACTIVE(ciUnderwaterCam, TRUE)
					SET_CAM_PARAMS(ciUnderwaterCam,<<3123.315186,-406.689240,-5.353999>>,<<19.894205,12.873194,79.032074>>,50.000000, 0)
					SET_CAM_PARAMS(ciUnderwaterCam,<<3124.211670,-406.338837,-2.572298>>,<<1.050243,12.873191,83.351349>>,50.000000, 6000)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					// Dress the player and put in dive position
					SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), 100000000.00)
					SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
					SET_ENABLE_SCUBA(player_ped_id(), true)
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_WATER, FALSE)
					RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
					bTrevorScubaOutfitPreloaded = FALSE
					
					vScenePos = <<0.0, 0.0, 0.0>> 
					vSceneRot = <<0.0, 0.0, 0.0>> 
					iBoatSceneID = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneRot)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iBoatSceneID, "missheistchem2", "Boat_Dive_Exit_Player", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbf_none, NORMAL_BLEND_IN)     
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iBoatSceneID, viPlayerBoat, 0)
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WAKE_UP")
					STOP_AUDIO_SCENE("MAG_2_WAKE_UP")
					bAudioScene_TrevWake = FALSE
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("MAG_2_DRIVE_DINGHY")
					STOP_AUDIO_SCENE("MAG_2_DRIVE_DINGHY")
					bAudioScene_TrevBoat = FALSE
				ENDIF
				
				iExitBoatStage++
			ENDIF
		BREAK
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBoatSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBoatSceneID) >= 0.5
					IF NOT IS_AUDIO_SCENE_ACTIVE("MAG_2_SCUBA_DIVE")
						START_AUDIO_SCENE("MAG_2_SCUBA_DIVE")
						bAudioScene_TrevDive = TRUE
					ENDIF
					iExitBoatStage++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBoatSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBoatSceneID) >= 0.8
					// interp back to gameplay
					
					
					//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE)
					
					RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME)
					
					//RENDER_SCRIPT_CAMS(FALSE, FALSE)
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					IF DOES_CAM_EXIST(ciUnderwaterCam)
						DESTROY_CAM(ciUnderwaterCam)
					ENDIF
					iExitBoatStage++
				ENDIF
			ENDIF	
		BREAK
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBoatSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBoatSceneID) >= 0.99
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SEQUENCE_INDEX seq						
						OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "SWIMMING@scuba", "dive_run", walk_BLEND_IN, walk_BLEND_OUT, 1000, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_FORCE_START)
						TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_diving_swim))
						TASK_GO_STRAIGHT_TO_COORD(NULL, <<3159.7, -371.8, -13.0>>, PEDMOVE_RUN, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						SETTIMERA(0)
						CLEANUP_ANIM_ASSET("missheistchem2", bAnimRequest_Dive)
						CLEANUP_ANIM_ASSET("SWIMMING@scuba", bAnimRequest_Swim)
						iExitBoatStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF TIMERA() > 3500
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					iExitBoatStage++
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
				IF IS_AUDIO_SCENE_ACTIVE("MAG_2_SCUBA_DIVE")
					STOP_AUDIO_SCENE("MAG_2_SCUBA_DIVE")
					bAudioScene_TrevDive = FALSE
				ENDIF
				iExitBoatStage = 0
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iExitBoatStage > 2
	AND iExitBoatStage < 5
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
ENDPROC

PROC SET_MAGDEMO_SCENE()

	// IMPORTANT
	// These need to be in sync with the scene setup in PRIVATE_SetClockTimeForSwitchScene()

	SWITCH eMDStage
		CASE MD_STAGE_CREATE_INITIAL_SCENE
		CASE MD_STAGE_FRANKLIN_BASEJUMP
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			UNLOAD_ALL_CLOUD_HATS()
			SET_CLOCK_TIME(7, 0, 0)
			g_bMagDemoBJStarted = TRUE
		BREAK
		CASE MD_STAGE_TREVOR_DIVING
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			UNLOAD_ALL_CLOUD_HATS()
			SET_CLOCK_TIME(18, 0, 0)
		BREAK
		CASE MD_STAGE_MICHAEL_WALK_AROUND_CITY
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			UNLOAD_ALL_CLOUD_HATS()
			SET_CLOCK_TIME(22, 0, 0)
		BREAK
		CASE MD_STAGE_FRA2
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			UNLOAD_ALL_CLOUD_HATS()
			
			IF g_iMagDemoVariation = 1
				SET_CLOCK_TIME(22, 0, 0)
			ELSE
				SET_CLOCK_TIME(18, 0, 0)
			ENDIF
		BREAK
	ENDSWITCH
	
	PAUSE_CLOCK(TRUE)
ENDPROC

/// PURPOSE: Ensure the player is invincible and will not fly through windscreens
PROC UPDATE_PLAYER_PED_STATES()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			eStoredModel = DUMMY_MODEL_FOR_SCRIPT
		ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) != eStoredModel
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
			SET_WANTED_LEVEL_MULTIPLIER(0)
			SET_MAX_WANTED_LEVEL(0)
			
			eStoredModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			
			PRINTLN("MAGDEMO: UPDATE_PLAYER_PED_STATES() - States updated")
		ENDIF
	ENDIF
ENDPROC

PROC SET_MD_STAGE(MD_STAGE_ENUM eStage)
	
	SWITCH eStage
		CASE MD_STAGE_CREATE_INITIAL_SCENE		PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)") 	BREAK
		CASE MD_STAGE_FRANKLIN_BASEJUMP			PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_FRANKLIN_BASEJUMP)") 		BREAK
		CASE MD_STAGE_MICHAEL_WALK_AROUND_CITY	PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_MICHAEL_WALK_AROUND_CITY)") BREAK
		CASE MD_STAGE_TREVOR_DIVING				PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_TREVOR_DIVING)") 			BREAK
		CASE MD_STAGE_FRA2						PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_FRA2)") 					BREAK
		CASE MD_STAGE_RESET						PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_RESET)") 					BREAK
		CASE MD_STAGE_CLEANUP					PRINTLN("MAGDEMO: SET_MD_STAGE(MD_STAGE_CLEANUP)") 					BREAK
	ENDSWITCH
	
	eMDStage = eStage
	iControl = 0
	
	#IF IS_DEBUG_BUILD
	
		iJSkipStage = 0
		
		// Set the new Z-Skip stage
		IF eStage = MD_STAGE_CREATE_INITIAL_SCENE
		OR eStage = MD_STAGE_FRANKLIN_BASEJUMP
			sZSkipData.iCurrentStage = 0 // "Franklin in helicopter"
		ELIF eStage = MD_STAGE_TREVOR_DIVING
			sZSkipData.iCurrentStage = 1 // "Trevor diving"
		ELIF eStage = MD_STAGE_MICHAEL_WALK_AROUND_CITY
			sZSkipData.iCurrentStage = 2 // "Michael walking around city"
		ELIF eStage = MD_STAGE_FRA2
			sZSkipData.iCurrentStage = 3 // "FRA2"
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL IS_MAGDEMO_SAFE_TO_PROCEED()

	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_INITIAL_MAGDEMO_GAME_STATE()
	
	g_bMagDemoActive = TRUE
	
	bReplayFRA2 = FALSE
	
	g_bMagDemoBJReady = FALSE
	g_bMagDemoBJStarted = FALSE
	g_bMagDemoREPapDone = FALSE
	
	g_bUseCharacterFilters = TRUE
	UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
	
	g_savedGlobals.sFlow.isGameflowActive = TRUE
	RESET_GAMEFLOW()
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	g_bTaxiHailingIsDisabled = TRUE
	
	g_savedGlobals.sRandomEventData.iREVariationComplete[RE_PAPARAZZI] = 0
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALLOW_RANDOM_EVENTS, TRUE)
	
	// Unlock savehouses
	Set_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_BH, TRUE)
	Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC, TRUE)
	Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)	
	
	// Set characters we can switch to
	SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
	SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
	SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, FALSE)
	
	
	// Update all player stats
	SETUP_DEFAULT_PLAYER_STATS()
	UPDATE_ALL_PLAYER_STAT_SETTINGS(GET_CURRENT_PLAYER_PED_ENUM())
	
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	CLEAR_AUDIO_SCENES()
	
	SET_DEBUG_CAM_ACTIVE(FALSE)
	
	SET_RANDOM_TRAINS(FALSE)	//740410
	
	SCRIPT_OVERRIDES_WIND_TYPES(FALSE, "", "")
	
	
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
	
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_MAGDEMO)
	
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	WAIT(0)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
ENDPROC

PROC DO_STAGE_CREATE_INITIAL_SCENE()

	// Keep screen faded out until we display prompt
	IF iControl < 6
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(2500)
			ENDIF
		ENDIF
		IF NOT IS_AUDIO_SCENE_ACTIVE("MISSION_FAILED_SCENE")
			START_AUDIO_SCENE("MISSION_FAILED_SCENE")
		ENDIF
	ENDIF
	
	SWITCH iControl
		CASE 0
			IF IS_SCREEN_FADED_OUT()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("initial")) = 0 // Wait for all the controller scripts to be intialised before we proceed (required for runscript command line param)
					START_AUDIO_SCENE("MISSION_FAILED_SCENE")
					CLEAR_AREA(<<-1368.1553, 4299.1650, 1.4856>>, 250.0, TRUE, FALSE)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1368.1553, 4299.1650, 1.4856>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 61.0253)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					VECTOR vDir
					vDir = NORMALISE_VECTOR(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-1368.1553, 4299.1650, 1.4856>>, 61.0253, <<0,1,0>>) - <<-1368.1553, 4299.1650, 1.4856>>)
					NEW_LOAD_SCENE_START(<<-1368.1553, 4299.1650, 1.4856>>, vDir, 1800.0)
					SETTIMERA(0)
					iControl++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR TIMERA() > 20000
					NEW_LOAD_SCENE_STOP()
					iControl++
				ENDIF
			ELSE
				iControl++
			ENDIF
		BREAK
		CASE 2
			SET_INITIAL_MAGDEMO_GAME_STATE()
			iControl++
		BREAK
		CASE 3
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN, TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 11, 2)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 11, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 8, 0)		//#1271807
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 14, 0)
				CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				
				iControl++
			ENDIF
		BREAK
		CASE 4
			REQUEST_SCRIPT("bj")
		    IF HAS_SCRIPT_LOADED("bj")
				IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_MINIGAME, TRUE) = MCRET_ACCEPTED
					BJ_LAUNCHER_ARGS bjLauncherArgs
					g_savedGlobals.sBasejumpData.iLaunchRank = 10 // The 1K
					START_NEW_SCRIPT_WITH_ARGS("bj", bjLauncherArgs, SIZE_OF(bjLauncherArgs), MISSION_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("bj")
					iControl++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// Wait for basejumping script to give us the green light
			IF g_bMagDemoBJReady
				iControl++
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(0)
			ENDIF
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
			
			IF IS_SCREEN_FADED_IN()
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_CENTRE(TRUE)
				DISPLAY_TEXT(0.5, 0.4, "MD_TITLE_1")
				
				IF IS_MAGDEMO_SAFE_TO_PROCEED()
				
					SET_MAGDEMO_SCENE()
					
					DO_SCREEN_FADE_OUT(0)
					DO_SCREEN_FADE_IN(1000)
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					PRINTLN("MAGDEMO: Playing sound Helicopter_Wind")
					iWindSound = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iWindSound, "Helicopter_Wind", "BASEJUMPS_SOUNDS")
					
					SET_MD_STAGE(MD_STAGE_FRANKLIN_BASEJUMP)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_STAGE_STAGE_FRANKLIN_BASEJUMP()

	// Add park ranger to the mix group when it's available
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT DOES_ENTITY_EXIST(viRanger)
			VEHICLE_INDEX nearbyVehs[5]
			INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			INT iVeh
			REPEAT iNearbyVehs iVeh
				IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
				AND IS_VEHICLE_DRIVEABLE(nearbyVehs[iVeh])
				AND GET_ENTITY_MODEL(nearbyVehs[iVeh]) = PRANGER
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(nearbyVehs[iVeh], "MAG_2_RANGER_CAR_GROUP")
					viRanger = nearbyVehs[iVeh]
					
					PRINTLN("MAGDEMO: viRanger added to MAG_2_RANGER_CAR_GROUP (GET_PED_NEARBY_VEHICLES)")
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viRanger)
			VEHICLE_INDEX nearbyVeh
			nearbyVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, PRANGER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_BOATS_ONLY)
			
			IF DOES_ENTITY_EXIST(nearbyVeh)
			AND IS_VEHICLE_DRIVEABLE(nearbyVeh)
			AND GET_ENTITY_MODEL(nearbyVeh) = PRANGER
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(nearbyVeh, "MAG_2_RANGER_CAR_GROUP")
				viRanger = nearbyVeh
				
				PRINTLN("MAGDEMO: viRanger added to MAG_2_RANGER_CAR_GROUP (GET_CLOSEST_VEHICLE)")
			ENDIF
		ENDIF
	ENDIF
	
	// Cleanup wind sound when we jump
	IF iWindSound != -1
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			PRINTLN("MAGDEMO: Stopping sound Helicopter_Wind")
			STOP_SOUND(iWindSound)
			iWindSound = -1
		ENDIF
	ENDIF
	
	SWITCH iControl
		CASE 0
		
			// Set up - TODO
			
			// Set characters we can switch to
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
			
			REQUEST_VEHICLE_RECORDING(2, "MAGDEMOTITAN")
			
			LOAD_MODEL_ASSET(CARGOPLANE, bModelRequest_Cargo)
			LOAD_MODEL_ASSET(S_M_M_PILOT_01, bModelRequest_Pilot)
			iControl++
		BREAK
		
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "MAGDEMOTITAN")
			AND HAS_MODEL_LOADED(CARGOPLANE)
			AND HAS_MODEL_LOADED(S_M_M_PILOT_01)
				iControl++
			ENDIF
		BREAK
		
		CASE 2
			
			IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
					
				viBaseJumpTitan = CREATE_VEHICLE(CARGOPLANE, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(2, 0.0, "MAGDEMOTITAN"))
				piBaseJumpTitanPilot = CREATE_PED_INSIDE_VEHICLE(viBaseJumpTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piBaseJumpTitanPilot, TRUE)
				
				START_PLAYBACK_RECORDED_VEHICLE(viBaseJumpTitan, 2, "MAGDEMOTITAN")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viBaseJumpTitan, 26000.0)
				SET_ENTITY_LOD_DIST(viBaseJumpTitan, 4000)
				SET_VEHICLE_LOD_MULTIPLIER(viBaseJumpTitan, 100.0)
				SET_VEHICLE_ENGINE_ON(viBaseJumpTitan, TRUE, TRUE)
				
//				blipTitanPlane = CREATE_BLIP_FOR_VEHICLE(viBaseJumpTitan, FALSE, TRUE)
				
				iControl++
			ENDIF
		BREAK
				
		CASE 3
			// Wait for the basejump script to end
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bj")) = 0
				Mission_Over(missionCandidateID)
				missionCandidateID = NO_CANDIDATE_ID
				iControl++
			ENDIF
		BREAK
		CASE 4
			// Wait for player to switch to Michael
			IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
//				IF DOES_BLIP_EXIST(blipTitanPlane)
//					REMOVE_BLIP(blipTitanPlane)
//				ENDIF
				IF DOES_ENTITY_EXIST(viBaseJumpTitan)
					DELETE_VEHICLE(viBaseJumpTitan)
				ENDIF
				IF DOES_ENTITY_EXIST(piBaseJumpTitanPilot)
					DELETE_PED(piBaseJumpTitanPilot)
				ENDIF
				
				REMOVE_VEHICLE_RECORDING(2, "MAGDEMOTITAN")
				
				CLEANUP_MODEL_ASSET(CARGOPLANE, bModelRequest_Cargo)
				CLEANUP_MODEL_ASSET(S_M_M_PILOT_01, bModelRequest_Pilot)
				
				CLEAR_HINT_CAM()
				SET_MD_STAGE(MD_STAGE_TREVOR_DIVING)
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF iControl = 3
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1368.1553, 4299.1650, 1.4856>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 61.0253)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ELIF iControl = 4
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_TREVOR)	//, PR_TYPE_AMBIENT)
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

VECTOR vBlipDiveLocation = <<3096.9629, -401.3707, 0.0>>
//VECTOR vBlipWreckLocation = <<3181.4639, -345.3424, -31.1768>>
BOOL bBoatBlipCreated
BOOL bSharksSpotted = FALSE

PROC DO_STAGE_TREVOR_DIVING()

	// Blip the boat then the dive location
	IF NOT DOES_BLIP_EXIST(blipBoat)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND NOT bBoatBlipCreated
	
		PED_VEH_DATA_STRUCT sVehData
		VECTOR vTemp
		FLOAT fTemp
		IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_TREVOR, PR_SCENE_T_NAKED_ISLAND, sVehData, vTemp, fTemp, vTemp, fTemp)
			VEHICLE_INDEX nearbyVehs[5]
			INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			INT iVeh
			REPEAT iNearbyVehs iVeh
				IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
				AND IS_VEHICLE_DRIVEABLE(nearbyVehs[iVeh])
				AND GET_ENTITY_MODEL(nearbyVehs[iVeh]) = sVehData.model
					
					blipBoat = CREATE_BLIP_FOR_VEHICLE(nearbyVehs[iVeh], FALSE, FALSE)
					bBoatBlipCreated = TRUE
					SET_VEHICLE_RADIO_ENABLED(nearbyVehs[iVeh], FALSE)
					SET_VEH_RADIO_STATION(nearbyVehs[iVeh], "RADIO_06_COUNTRY")
					SET_CUSTOM_RADIO_TRACK_LIST("RADIO_06_COUNTRY", "MAGDEMO2_RADIO_DINGHY", TRUE)
					iVeh = iNearbyVehs+1//Bail
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		IF IS_PED_INJURED(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("MAG_2_DRIVE_DINGHY")
				START_AUDIO_SCENE("MAG_2_DRIVE_DINGHY")
				bAudioScene_TrevBoat = TRUE
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("MAG_2_WAKE_UP")
				STOP_AUDIO_SCENE("MAG_2_WAKE_UP")
				bAudioScene_TrevWake = FALSE
			ENDIF
		
		
			IF DOES_BLIP_EXIST(blipBoat)
			
				REMOVE_BLIP(blipBoat)
				
				VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehID)
				AND IS_VEHICLE_DRIVEABLE(vehID)
					SET_VEHICLE_RADIO_ENABLED(vehID, TRUE)
				ENDIF
				
				blipWreckDiveLocation = CREATE_BLIP_FOR_COORD(vBlipDiveLocation)
				SET_BLIP_ROUTE(blipWreckDiveLocation, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// Blip the sharks
	IF eMDStage = MD_STAGE_TREVOR_DIVING
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF NOT bRelGroupSetup
			TEXT_LABEL_15 sRelGroup
			sRelGroup = "SHARKPET"
			ADD_RELATIONSHIP_GROUP(sRelGroup, sharkGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, sharkGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, sharkGroup, RELGROUPHASH_PLAYER)
			bRelGroupSetup = TRUE
		ENDIF

		// Make all sharks friendly
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			PED_INDEX nearbyPeds[10]
			INT iNearbyPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
			INT iPed
			REPEAT iNearbyPeds iPed
				IF DOES_ENTITY_EXIST(nearbyPeds[iPed])
				AND NOT IS_PED_INJURED(nearbyPeds[iPed])
					IF GET_ENTITY_MODEL(nearbyPeds[iPed]) = A_C_SHARKTIGER
					AND GET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[iPed]) != sharkGroup
					AND IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
					//AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), nearbyPeds[iPed]) < 150.0
						PRINTLN("MAGDEMO: Processing shark, ped_index=", NATIVE_TO_INT(nearbyPeds[iPed]))
						SET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[iPed], sharkGroup)
						
						IF iBlippedSharks < COUNT_OF(blipSharks)-1
							blipSharks[iBlippedSharks] = CREATE_BLIP_FOR_PED(nearbyPeds[iPed], FALSE)
							SET_BLIP_ROUTE(blipSharks[iBlippedSharks], FALSE)
							iBlippedSharks++
						ENDIF
						
//						IF DOES_BLIP_EXIST(blipSharkLocation)
//							REMOVE_BLIP(blipSharkLocation)
//						ENDIF
						IF DOES_BLIP_EXIST(blipDiverPed)
							REMOVE_BLIP(blipDiverPed)
						ENDIF
					ENDIF
					
					IF bSharksSpotted = FALSE
						IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), nearbyPeds[iPed]) < 30.0
								IF GET_ENTITY_MODEL(nearbyPeds[iPed]) = A_C_SHARKTIGER
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "DROWNING","WAVELOAD_PAIN_TREVOR", SPEECH_PARAMS_FORCE_FRONTEND)
									bSharksSpotted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDREPEAT
		ENDIF
	ELSE
		IF iBlippedSharks > 0
			INT iBlip
			REPEAT COUNT_OF(blipSharks) iBlip
				IF DOES_BLIP_EXIST(blipSharks[iBlip])
					REMOVE_BLIP(blipSharks[iBlip])
				ENDIF
			ENDREPEAT
			iBlippedSharks = 0
		ENDIF
	ENDIF
	
	// Manage music
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND REQUEST_SCRIPT_AUDIO_BANK("Underwater")
		IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
		AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			IF NOT bUnderwaterMusic
				TRIGGER_MUSIC_EVENT("MG2_DIVE")
				iSoundID_Breathing = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(iSoundID_Breathing, "UW_Rebreather", PLAYER_PED_ID())
				bUnderwaterMusic = TRUE
			ENDIF
		ELSE
			IF bUnderwaterMusic
				IF NOT HAS_SOUND_FINISHED(iSoundID_Breathing)
					STOP_SOUND(iSoundID_Breathing)
				ENDIF
				TRIGGER_MUSIC_EVENT("MG2_STOP")
				bUnderwaterMusic = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Hold the end of the switch spline
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		IF bTrevorScubaOutfitPreloaded
			RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			bTrevorScubaOutfitPreloaded = FALSE
		ENDIF
	ENDIF
	
	VECTOR vStartPosition
	SWITCH iControl
		CASE 0
		
			IF NOT DOES_BLIP_EXIST(blipBoat)
				bBoatBlipCreated = FALSE
			ENDIF
		
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
		
			CLEANUP_PREVIOUS_STAGE_ASSETS()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("MAG_2_WAKE_UP")
				START_AUDIO_SCENE("MAG_2_WAKE_UP")
				bAudioScene_TrevWake = TRUE
			ENDIF
		
			// Set characters we can switch to
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// We're now going to give the outfit in script
				SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(), FALSE)
			ENDIF
			
			SET_PED_MAX_TIME_UNDERWATER(PLAYER_PED_ID(), 1000000.0)	
			SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
			SET_ENABLE_SCUBA(PLAYER_PED_ID(), TRUE)
										
			//Request assets
			REQUEST_WAYPOINT_RECORDING("MAGDEMODIVE2")
			REQUEST_WAYPOINT_RECORDING("MAGDEMODIVE3")
			LOAD_MODEL_ASSET(IG_STEVEHAINS, bModelRequest_Steve)
			REQUEST_SCRIPT_AUDIO_BANK("Underwater")
			
			bSharksSpotted = FALSE
			
			iControl++
		BREAK
		
		CASE 1
			//Wait for assets to load
			IF GET_IS_WAYPOINT_RECORDING_LOADED("MAGDEMODIVE3")
			AND GET_IS_WAYPOINT_RECORDING_LOADED("MAGDEMODIVE2")
			AND HAS_MODEL_LOADED(IG_STEVEHAINS)
				iControl++
			ENDIF
		BREAK
		
		CASE 2
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBlipDiveLocation, <<30.0, 30.0, 30.0>>)
			AND IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
				IF DOES_BLIP_EXIST(blipWreckDiveLocation)
					REMOVE_BLIP(blipWreckDiveLocation)
//					blipWreckLocation = CREATE_BLIP_FOR_COORD(vBlipWreckLocation)
//					SET_BLIP_COLOUR(blipWreckLocation, BLIP_COLOUR_GREEN)
				ENDIF
			ENDIF
			
			//When trevor  gets close to the boatwreck spawn the diver.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3158.247803,-222.377258,-51.817005>>, <<3197.752197,-433.242889,-3.360249>>, 131.000000, FALSE, FALSE)
			AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
			AND NOT DOES_BLIP_EXIST(blipWreckDiveLocation)	
			
//				IF DOES_BLIP_EXIST(blipWreckLocation)
//					REMOVE_BLIP(blipWreckLocation)
//				ENDIF
				
				WAYPOINT_RECORDING_GET_COORD("MAGDEMODIVE3", 7, vStartPosition)
				piDiverPed = CREATE_PED(PEDTYPE_MISSION, IG_STEVEHAINS, vStartPosition)
				
				WAYPOINT_RECORDING_GET_COORD("MAGDEMODIVE2", 0, vStartPosition)
				piDiverPed2 = CREATE_PED(PEDTYPE_MISSION, IG_STEVEHAINS, vStartPosition)
				
				blipDiverPed = CREATE_BLIP_FOR_PED(piDiverPed, FALSE)
				
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(piDiverPed, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
				SET_PED_PROP_INDEX(piDiverPed, INT_TO_ENUM(PED_PROP_POSITION,1), 1, 0)
				
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(piDiverPed2, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
				SET_PED_PROP_INDEX(piDiverPed2, INT_TO_ENUM(PED_PROP_POSITION,1), 1, 0)
				
				SET_PED_MAX_TIME_UNDERWATER(piDiverPed, 1000000.0)
				SET_PED_DIES_IN_WATER(piDiverPed, FALSE)
				SET_ENABLE_SCUBA(piDiverPed, TRUE)
				SET_PED_STEERS_AROUND_PEDS(piDiverPed, FALSE)
				SET_PED_NAME_DEBUG(piDiverPed, "piDiverPed")
				TASK_FOLLOW_WAYPOINT_RECORDING(piDiverPed, "MAGDEMODIVE3", 7, EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
								
				SET_PED_MAX_TIME_UNDERWATER(piDiverPed2, 1000000.0)
				SET_PED_DIES_IN_WATER(piDiverPed2, FALSE)
				SET_ENABLE_SCUBA(piDiverPed2, TRUE)
				SET_PED_STEERS_AROUND_PEDS(piDiverPed2, FALSE)
				SET_PED_STEERS_AROUND_PEDS(piDiverPed2, FALSE)
				SET_PED_NAME_DEBUG(piDiverPed2, "piDiverPed2")
				
				OPEN_SEQUENCE_TASK(seqDiverDelay)
					TASK_PAUSE(NULL, 500)
					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "MAGDEMODIVE2", 0, EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
				CLOSE_SEQUENCE_TASK(seqDiverDelay)
				TASK_PERFORM_SEQUENCE(piDiverPed2, seqDiverDelay)
				CLEAR_SEQUENCE_TASK(seqDiverDelay)
			
				iControl++
			ENDIF
		
		BREAK
		
		CASE 3
			
			RESET_PLAYER_STAMINA(PLAYER_ID())
			
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3161.642578,-298.652344,-15.723236>>, <<3096.343994,-167.582458,-29.656219>>, 46.750000)
			
				IF iBlippedSharks = 0
//					blipSharkLocation = CREATE_BLIP_FOR_COORD(<<3114.1111, -190.8658, -31.0156>>)
//					SET_BLIP_COLOUR(blipSharkLocation, BLIP_COLOUR_GREEN)
					
					IF DOES_BLIP_EXIST(blipDiverPed)
						REMOVE_BLIP(blipDiverPed)
					ENDIF
				ENDIF
				
				iControl++
			ENDIF
		BREAK
			
		CASE 4
			
			RESET_PLAYER_STAMINA(PLAYER_ID())
			
			//Cleanup
			IF NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(piDiverPed)
					DELETE_PED(piDiverPed)
				ENDIF
				IF DOES_ENTITY_EXIST(piDiverPed2)
					DELETE_PED(piDiverPed2)
				ENDIF
				IF DOES_BLIP_EXIST(blipDiverPed)
					REMOVE_BLIP(blipDiverPed)
				ENDIF
//				IF DOES_BLIP_EXIST(blipWreckLocation)
//					REMOVE_BLIP(blipWreckLocation)
//				ENDIF
				IF DOES_BLIP_EXIST(blipBoat)
					REMOVE_BLIP(blipBoat)
				ENDIF
//				IF DOES_BLIP_EXIST(blipSharkLocation)
//					REMOVE_BLIP(blipSharkLocation)
//				ENDIF
				
				CLEANUP_MODEL_ASSET(IG_STEVEHAINS, bModelRequest_Steve)
			
				iControl++
			ENDIF
		BREAK
		
		CASE 5
			// nothing to do.
		BREAK
	ENDSWITCH
	
	// Wait for player to switch to Michael
	IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		CLEAR_HINT_CAM()
		SET_MD_STAGE(MD_STAGE_MICHAEL_WALK_AROUND_CITY)
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	
		VECTOR vWarpCoords[3]
		FLOAT fHeading[3]
		
		// Next to boat
		vWarpCoords[0] = <<2793.1936, -1433.4767, -0.5368>>
		fHeading[0] = 155.1430
		// Above sunken ship
		vWarpCoords[1] = <<3268.8042, -437.3923, 5.5>>
		fHeading[1] = 353.9825
		// In water
		vWarpCoords[2] = <<3220.5657, -357.8888, -36.1978>>
		fHeading[2] = 253.1632
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF iJSkipStage = 0
				IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					iJSkipStage++
				ENDIF
			ELIF iJSkipStage = 1
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWarpCoords[iJSkipStage], FALSE) < 10.0
				OR NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					iJSkipStage++
				ENDIF
			ELIF iJSkipStage = 2
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWarpCoords[iJSkipStage]) < 10.0
					iJSkipStage++
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF iJSkipStage = 0
				VEHICLE_INDEX tempVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_BOATS_ONLY)
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_VEHICLE_DRIVEABLE(tempVeh)
				AND IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(tempVeh))
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), tempVeh, VS_DRIVER)
				ENDIF
			ELIF iJSkipStage = 1
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(tempVeh)
					AND IS_VEHICLE_DRIVEABLE(tempVeh)
						SET_ENTITY_COORDS(tempVeh, vWarpCoords[iJSkipStage], TRUE, TRUE)
						SET_VEHICLE_ON_GROUND_PROPERLY(tempVeh)
						SET_ENTITY_HEADING(tempVeh, fHeading[iJSkipStage])
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ENDIF
				ENDIF
			ELIF iJSkipStage = 2
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoords[iJSkipStage])
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading[iJSkipStage])
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ELSE
				iControl = 5
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_MICHAEL)	//, PR_TYPE_AMBIENT)
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

//BLIP_INDEX blipClosestCar

PROC DO_STAGE_MICHAEL_WALK_AROUND_CITY()

	// Add park ranger to the mix group when it's available
	IF bTriggerSpaceRangerDialogue
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		PED_INDEX nearbyPeds[10]
		INT iNearbyPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
		INT iPed
		REPEAT iNearbyPeds iPed
			IF DOES_ENTITY_EXIST(nearbyPeds[iPed])
			AND NOT IS_PED_INJURED(nearbyPeds[iPed])
				IF GET_ENTITY_MODEL(nearbyPeds[iPed]) = U_M_Y_RSRanger_01
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), nearbyPeds[iPed]) < 5.0
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(nearbyPeds[iPed], "PBSH_AXAA", "SPACERANGER", "SPEECH_PARAMS_FORCE")
					bTriggerSpaceRangerDialogue = FALSE
					iPed = iNearbyPeds+1// Bail
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	SWITCH iControl
		CASE 0
		
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_NEAR)
		
			CLEANUP_PREVIOUS_STAGE_ASSETS()
			
			bSafeToProgress = FALSE
		
			// Set characters we can switch to
			IF g_iMagDemoVariation = 1
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
			ELSE
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
				INCRAMENT_Pause_Outro_Count()
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("MAG_2_WALK_THROUGH_VINEWOOD")
				START_AUDIO_SCENE("MAG_2_WALK_THROUGH_VINEWOOD")
				bAudioScene_MikeWalk = TRUE
			ENDIF
			
			// Set up - TODO
			bTriggerSpaceRangerDialogue = TRUE
			
			REQUEST_MODEL(SUPERD)
			REQUEST_MODEL(VOLTIC)
			LOAD_MODEL_ASSET(G_M_Y_BALLASOUT_01, bModelRequest_Driver)
			iControl++
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(SUPERD)
			AND HAS_MODEL_LOADED(VOLTIC)
			AND LOAD_MODEL_ASSET(G_M_Y_BALLASOUT_01, bModelRequest_Driver)
			
				IF NOT DOES_ENTITY_EXIST(viSuperD)
					viSuperD = CREATE_VEHICLE(SUPERD, <<374.5235, 159.3893, 102.0563>>, 340.6834)
					SET_VEHICLE_ENGINE_ON(viSuperD,true,true)
					piClosestCarDriver = CREATE_PED_INSIDE_VEHICLE(viSuperD, PEDTYPE_MISSION, G_M_Y_BALLASOUT_01)
					
					viClosestCar = viSuperD
					
					viVoltic = CREATE_VEHICLE(VOLTIC, <<387.8163, 210.5225, 102.0972>>, 161.3396)
					SET_VEHICLE_ENGINE_ON(viVoltic,true,true)
					piClosestCarDriver2 = CREATE_PED_INSIDE_VEHICLE(viVoltic, PEDTYPE_MISSION, G_M_Y_BALLASOUT_01)
					
					SET_VEHICLE_RADIO_ENABLED(viVoltic, TRUE)
					SET_VEHICLE_RADIO_LOUD(viVoltic, TRUE)
					//SET_VEH_RADIO_STATION(viClosestCar, "RADIO_03_HIPHOP_NEW")	
					TASK_VEHICLE_DRIVE_TO_COORD(piClosestCarDriver2, viVoltic, <<484.2128, 87.5613, 96.0259>>, 7.5, DRIVINGSTYLE_NORMAL, SUPERD, DRIVINGMODE_STOPFORCARS, 2.0, 5.0)
					SETTIMERA(0)
					
					CLEANUP_MODEL_ASSET(G_M_Y_BALLASOUT_01, bModelRequest_Driver)
					
				ENDIF
				
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT Is_Player_Timetable_Scene_In_Progress()
					IF NOT IS_ENTITY_DEAD(viSuperD)						
						SET_VEHICLE_RADIO_ENABLED(viSuperD, TRUE)
						SET_VEHICLE_RADIO_LOUD(viSuperD, TRUE)
						//SET_VEH_RADIO_STATION(viClosestCar, "RADIO_03_HIPHOP_NEW")	
						TASK_VEHICLE_DRIVE_WANDER(piClosestCarDriver, viSuperD, 7.0, DRIVINGMODE_STOPFORCARS)
						SETTIMERA(0)
						
						LOAD_MODEL_ASSET(PLAYER_ONE, bModelRequest_Franklin)	//#1271792
						
						iControl++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
						
			IF DOES_ENTITY_EXIST(viClosestCar)
			AND TIMERA() > 5000
			AND viClosestCarPrev <> viClosestCar
			AND (IS_VEHICLE_DRIVEABLE(viClosestCar) AND GET_IS_VEHICLE_ENGINE_RUNNING(viClosestCar))
			AND GET_ENTITY_MODEL(viClosestCar) <> TAXI
				SET_VEHICLE_RADIO_ENABLED(viClosestCar, TRUE)
				SET_VEHICLE_RADIO_LOUD(viClosestCar, TRUE)
				viClosestCarPrev = viClosestCar
				SETTIMERA(0)
			ELSE
				//SET_VEHICLE_AS_NO_LONGER_NEEDED(viClosestCar)
				//SET_PED_AS_NO_LONGER_NEEDED(piClosestCarDriver)
//				REMOVE_BLIP(blipClosestCar)
				viClosestCar = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 26.6, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
//				blipClosestCar= CREATE_BLIP_FOR_VEHICLE(viClosestCar)
			ENDIF
			
			//is what you want
			//you can optionally control the station via SET_VEH_RADIO_STATION
			//but SET_VEHICLE_RADIO_LOUD will ensure it gets a radio station, and will play it a little louder than normal
			
			// Delete the voltic and superd when they get out of range
			// -SUPERD
			IF DOES_ENTITY_EXIST(viSuperD)
			AND IS_VEHICLE_DRIVEABLE(viSuperD)
			AND DOES_ENTITY_EXIST(piClosestCarDriver)
			AND NOT IS_PED_INJURED(piClosestCarDriver)
			AND IS_ENTITY_IN_ANGLED_AREA(viSuperD, <<391.999237,306.042023,101.397812>>, <<450.516998,285.500183,107.205986>>, 43.437500)
				DELETE_PED(piClosestCarDriver)
				DELETE_VEHICLE(viSuperD)
				SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)
			ENDIF
			// -VOLTIC
			IF DOES_ENTITY_EXIST(viVoltic)
			AND IS_VEHICLE_DRIVEABLE(viVoltic)
			AND DOES_ENTITY_EXIST(piClosestCarDriver2)
			AND NOT IS_PED_INJURED(piClosestCarDriver2)
			AND IS_ENTITY_IN_ANGLED_AREA(viVoltic, <<501.130402,107.262054,94.584984>>, <<485.152954,71.751945,102.145523>>, 36.250000)
				DELETE_PED(piClosestCarDriver2)
				DELETE_VEHICLE(viVoltic)
				SET_MODEL_AS_NO_LONGER_NEEDED(VOLTIC)
			ENDIF
			
			CLEANUP_MODEL_ASSET(G_M_Y_BALLASOUT_01, bModelRequest_Driver)
			
			
			// Wait for player to switch to Franklin
			IF NOT bSafeToProgress
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
					bSafeToProgress = TRUE
				ENDIF
			ELIF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
			OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				CLEAR_HINT_CAM()
				SET_MD_STAGE(MD_STAGE_FRA2)
				
				IF DOES_ENTITY_EXIST(piClosestCarDriver)
					DELETE_PED(piClosestCarDriver)
				ENDIF
				IF DOES_ENTITY_EXIST(piClosestCarDriver2)
					DELETE_PED(piClosestCarDriver2)
				ENDIF
				IF DOES_ENTITY_EXIST(viSuperD)
					DELETE_VEHICLE(viSuperD)
					SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)
				ENDIF
				IF DOES_ENTITY_EXIST(viVoltic)
					DELETE_VEHICLE(viVoltic)
					SET_MODEL_AS_NO_LONGER_NEEDED(VOLTIC)
				ENDIF
				
				CLEAR_HINT_CAM()
				
				SET_MD_STAGE(MD_STAGE_FRA2)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	
		VECTOR vWarpCoords[4]
		FLOAT fHeading[4]
		
		// Pamela Drake
		vWarpCoords[0] = <<381.7144, 157.2653, 102.1663>>
		fHeading[0] = 181.7842
		// Vinewoord Blvrd
		vWarpCoords[1] = <<358.4988, 160.7451, 102.0595>>
		fHeading[1] = 72.3215
		// Impotent Rage
		vWarpCoords[2] = <<309.0139, 175.9244, 102.9660>>
		fHeading[2] = 39.0201
		// Celeb
		vWarpCoords[3] = <<266.9224, 149.1344, 103.5237>>
		fHeading[3] = 160.7408
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF (iJSkipStage < COUNT_OF(vWarpCoords))
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWarpCoords[iJSkipStage]) < 10.0
					iJSkipStage++
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF (iJSkipStage < COUNT_OF(vWarpCoords))
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoords[iJSkipStage])
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading[iJSkipStage])
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ELIF g_bMagDemoREPapDone
			
				iControl = 2
				INCRAMENT_Pause_Outro_Count()
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_FRANKLIN)	//, PR_TYPE_AMBIENT)
			ENDIF
		ENDIF
	#ENDIF
ENDPROC


PROC DO_STAGE_FRA2()

	SWITCH iControl
		CASE 0
			CLEANUP_PREVIOUS_STAGE_ASSETS()
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			
			iControl++
		BREAK
		CASE 1
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) = 0
				REQUEST_SCRIPT(sMissionName)
			    IF HAS_SCRIPT_LOADED(sMissionName)
					IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY, TRUE) = MCRET_ACCEPTED
						g_bMagDemoFRA2Passed = FALSE
						g_bMagDemoFRA2Ready = FALSE
						START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
						iControl++
					ENDIF
				ENDIF
			ELSE
				iControl++
			ENDIF
		BREAK
		CASE 2
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) = 0
				bReplayFRA2 = (NOT g_bMagDemoFRA2Passed)
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				SET_MD_STAGE(MD_STAGE_RESET)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_STAGE_FBI4()

	SWITCH iControl
		CASE 0
			CLEANUP_PREVIOUS_STAGE_ASSETS()
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			
			// Set stored data for Trevor
			SETUP_DEFAULT_PLAYER_VARIATIONS(CHAR_TREVOR)
			g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR].iDrawableVariation[PED_COMP_TORSO] = 0
			g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR].iDrawableVariation[PED_COMP_LEG] = 0
			g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR].iDrawableVariation[PED_COMP_FEET] = 0
			g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
			
			bFBI4LoadSceneStart = TRUE
			iNewLoadSceneStage = 0
			
			iControl++
		BREAK
		CASE 1
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) = 0
				REQUEST_SCRIPT(sMissionName)
			    IF HAS_SCRIPT_LOADED(sMissionName)
					IF REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY, TRUE) = MCRET_ACCEPTED
						g_bMagDemoFRA2Passed = FALSE
						g_bMagDemoFRA2Ready = FALSE
						START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
						iControl++
					ENDIF
				ENDIF
			ELSE
				iControl++
			ENDIF
		BREAK
		CASE 2
		
			IF bFBI4LoadSceneStart
				IF iNewLoadSceneStage = 0
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						PRINTLN("MAGDEMO: Starting new load scene for FBI4 [1]")
						NEW_LOAD_SCENE_START_SPHERE(<<1376.7286, -2079.4268, 50.9983>>, 400.0)
						iNewLoadSceneStage++
					ENDIF
				ELIF iNewLoadSceneStage = 1
					IF IS_CUTSCENE_PLAYING()
						iNewLoadSceneStage++
					ENDIF
				ELIF iNewLoadSceneStage = 2
					IF NOT IS_CUTSCENE_PLAYING()
						PRINTLN("MAGDEMO: Stopping new load scene for FBI4 [1]")
						IF IS_NEW_LOAD_SCENE_ACTIVE()	
							NEW_LOAD_SCENE_STOP()
						ENDIF
						bFBI4LoadSceneStart = FALSE
					ENDIF
				ENDIF
			ENDIF
		
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) = 0
				bReplayFRA2 = (NOT g_bMagDemoFRA2Passed)
				missionCandidateID = NO_CANDIDATE_ID
				g_savedGlobals.sFlow.isGameflowActive = TRUE
				RESET_GAMEFLOW()
				g_savedGlobals.sFlow.isGameflowActive = FALSE
				SET_MD_STAGE(MD_STAGE_RESET)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Cleans up all script assets and states.
PROC CLEANUP_SCRIPT(BOOL bTerminateScript)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
	ENDIF
	
	CLEAR_HINT_CAM()
	
	missionCandidateID = NO_CANDIDATE_ID
	
	eStoredModel = DUMMY_MODEL_FOR_SCRIPT
	
	g_bMagDemoFBI2Retry = FALSE
	g_bMagDemoFRA2Retry = FALSE
	g_bMagdemoTakingOverSwitch = FALSE
	g_bMagdemoDoTakeOverSwitch = FALSE
	
	
	IF iWindSound != -1
		PRINTLN("MAGDEMO: Stopping sound Helicopter_Wind")
		STOP_SOUND(iWindSound)
		iWindSound = -1
	ENDIF
	
	IF bRelGroupSetup
		REMOVE_RELATIONSHIP_GROUP(sharkGroup)
		bRelGroupSetup = FALSE
	ENDIF
	
	// Ending magdemo so ensure gameplay can resume
	IF bTerminateScript
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		ENDIF
		
		//g_bMagDemoActive = FALSE
		
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockID)
		
		// Reset flags
		g_bTaxiHailingIsDisabled = FALSE
		
		DO_SCREEN_FADE_IN(1000)
		STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
		
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_MAX_WANTED_LEVEL(5)
		PAUSE_CLOCK(FALSE)
		SCRIPT_OVERRIDES_WIND_TYPES(FALSE, "", "")
		
		SET_RANDOM_TRAINS(TRUE)	//740410
		
		BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
		
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

PROC DO_STAGE_RESET()

	IF bFBI4LoadSceneStart
		PRINTLN("MAGDEMO: Stopping new load scene for FBI4 [1]")
		IF IS_NEW_LOAD_SCENE_ACTIVE()	
			NEW_LOAD_SCENE_STOP()
		ENDIF
		bFBI4LoadSceneStart = FALSE
	ENDIF
	
	// Fade out and then delete all assets
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(2500)
			START_AUDIO_SCENE("MISSION_FAILED_SCENE")
		ENDIF
	
	// Only process when player is alive
	ELIF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		CLEANUP_PREVIOUS_STAGE_ASSETS()
		
		CLEANUP_SCRIPT(FALSE)
		
		SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)
		
		// Failed FRA2 so retry
		IF bReplayFRA2
#IF IS_DEBUG_BUILD
		OR (sZSkipData.bDoSkip AND (sZSkipData.iSelectedStage = 3) AND g_iMagDemoVariation = 0) // "FRA2"
#ENDIF
		
		
			PRINTSTRING("test one")PRINTNL()
			
			VECTOR vPlayerCoord, vPlayerCreateCoord, vLoadSceneCoord
			FLOAT fPlayerCoord, fPlayerCreateCoord
			
			MODEL_NAMES ePlayerVehModel
			
			IF g_iMagDemoVariation = 1		//FBI4
				vPlayerCoord = <<1376.7286, -2079.4268, 50.9983>>
				fPlayerCoord = 15.5269
				
				vLoadSceneCoord = vPlayerCoord + (<<-768.1193, 5323.7915, 75.8220>> - <<-766.7336, 5324.4829, 73.7515>>)
				
				vPlayerCreateCoord = <<1381.4761, -2072.2507, 50.9983>>
				fPlayerCreateCoord = 38.3348
				
				ePlayerVehModel = TRASH
			ELSE							//FRANKLIN2
				vPlayerCoord = <<-766.7336, 5324.4829, 73.7515>>
				fPlayerCoord = 256.4759
				
				vLoadSceneCoord = <<-768.1193, 5323.7915, 75.8220>>
				
				vPlayerCreateCoord = <<-760.3937, 5322.8789, 73.3588>>
				fPlayerCreateCoord = 261.4380
				
				ePlayerVehModel = BUFFALO2
			ENDIF
			
			SET_INITIAL_MAGDEMO_GAME_STATE()
			
			g_bMagDemoFRA2Retry = TRUE
			
			// Set up the fra2 scene
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN, TRUE)
				
				PRINTSTRING("test one.one - wait on creating franklin")PRINTNL()
				
				WAIT(0)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDWHILE
			
			// Fade in and give control back
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_AREA(vPlayerCoord, 500.0, TRUE, TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerCoord)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
			ENDIF
			
			NEW_LOAD_SCENE_START(vLoadSceneCoord, <<-6.0072, -0.0000, -96.6791>>, 500.0)
			WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
				
				PRINTSTRING("test one.two - wait on loadscene [")PRINTVECTOR(vLoadSceneCoord)PRINTSTRING("]")PRINTNL()
				
				WAIT(0)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDWHILE
			NEW_LOAD_SCENE_STOP()
			
			
			IF (ePlayerVehModel = BUFFALO2)
				VEHICLE_INDEX vehID
				WHILE NOT CREATE_PLAYER_VEHICLE(vehID, CHAR_FRANKLIN, vPlayerCreateCoord, fPlayerCreateCoord, TRUE, VEHICLE_TYPE_CAR)
					PRINTSTRING("test one.three = wait on creating vehicle")PRINTNL()
					
					WAIT(0)
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDWHILE
				IF DOES_ENTITY_EXIST(vehID)
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehID)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehID)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			REQUEST_SCRIPT(sMissionName)
		    WHILE NOT HAS_SCRIPT_LOADED(sMissionName)
				
				PRINTSTRING("test one.four - wait on starting mission script")PRINTNL()
				
				WAIT(0)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDWHILE
			WHILE REQUEST_MISSION_LAUNCH(missionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY, TRUE) != MCRET_ACCEPTED
				
				PRINTSTRING("test one.five - wait on registering mission")PRINTNL()
				
				WAIT(0)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDWHILE
			
			g_bMagDemoFRA2Passed = FALSE
			g_bMagDemoFRA2Ready = FALSE
			START_NEW_SCRIPT(sMissionName, MISSION_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(sMissionName)
			
			// Wait for the mission script to give us to all clear
			WHILE NOT g_bMagDemoFRA2Ready
				
				PRINTSTRING("test one.six - wait on g_bMagDemoFRA2Ready")PRINTNL()
				
				WAIT(0)
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDWHILE
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
			
			
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			//	--------
			
			SET_MD_STAGE(MD_STAGE_FRA2)
			
			#IF IS_DEBUG_BUILD
				sZSkipData.bDoSkip = FALSE
			#ENDIF
			
			PRINTSTRING("test two")PRINTNL()
			
			
		// Set up the scene if we have used the Z-Skip menu
#IF IS_DEBUG_BUILD
		ELIF sZSkipData.bDoSkip
		
			SET_INITIAL_MAGDEMO_GAME_STATE()
		
			IF (sZSkipData.iSelectedStage = 0) // "Franklin basejumping scene"
				
				PRINTSTRING("test three")PRINTNL()
				
				// Let the initial scene set this one up
				SET_MD_STAGE(MD_STAGE_CREATE_INITIAL_SCENE)
			
			ELIF (sZSkipData.iSelectedStage = 1) // "Trevor diving"
				
				
				PRINTSTRING("test five")PRINTNL()
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR, TRUE)
					WAIT(0)
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDWHILE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 16, 0)
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 22, 0)
					CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2803.9402, -1441.6550, 0.2566>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 60.2669)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF
				
				SET_MD_STAGE(MD_STAGE_TREVOR_DIVING)
				
				// Create the dead peds
				g_eRecentlySelectedScene = PR_SCENE_T_NAKED_ISLAND
				REQUEST_SCRIPT("player_scene_t_tie")
				WHILE NOT HAS_SCRIPT_LOADED("player_scene_t_tie")
					WAIT(0)
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDWHILE
				START_NEW_SCRIPT("player_scene_t_tie", FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("player_scene_t_tie")
				
				// Create the boat
				PED_VEH_DATA_STRUCT sVehData
				VECTOR vTemp
				FLOAT fTemp
				VEHICLE_INDEX tempVeh
				IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_TREVOR, PR_SCENE_T_NAKED_ISLAND, sVehData, vTemp, fTemp, vTemp, fTemp)
					REQUEST_MODEL(sVehData.model)
					WHILE NOT HAS_MODEL_LOADED(sVehData.model)
						WAIT(0)
						IF NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(0)
						ENDIF
					ENDWHILE
					tempVeh = CREATE_VEHICLE(sVehData.model,
						<<2785.829,-1433.771,0.506>>,		//<<2793.1936, -1433.4767, -0.5368>>,
						60.1532,							//155.1434,
						FALSE, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(tempVeh)
					SET_VEHICLE_COLOURS(tempVeh, 0, 0)
					SET_VEHICLE_RADIO_ENABLED(tempVeh, TRUE)
					SET_VEH_RADIO_STATION(tempVeh, "RADIO_06_COUNTRY")
					SET_VEHICLE_EXTRA_COLOURS(tempVeh, 0, 0)
					SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
				ENDIF
				
				// Fade in and give control back
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					// Make sure player gets the scuba gear
					SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(), TRUE)
					
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
				
				IF DOES_ENTITY_EXIST(tempVeh)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVeh)
				ENDIF
				
				PRINTSTRING("test six")PRINTNL()
				
			ELIF (sZSkipData.iSelectedStage = 2) // "Michael walking around city"
			OR (sZSkipData.iSelectedStage = 3 AND g_iMagDemoVariation = 1) // "FBI4"
				
				PRINTSTRING("test four")PRINTNL()
				
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL, TRUE)
					WAIT(0)
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDWHILE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BLACK_SUIT, FALSE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<389.0900, 178.8800, 102.1781>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 146.8972)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF
				
				// Fade in and give control back
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE, TRUE)
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					STOP_AUDIO_SCENE("MISSION_FAILED_SCENE")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
				
				SET_MD_STAGE(MD_STAGE_MICHAEL_WALK_AROUND_CITY)
				
				IF (sZSkipData.iSelectedStage = 3 AND g_iMagDemoVariation = 1) // "FBI4"
					SET_MD_STAGE(MD_STAGE_FRA2)
					INCRAMENT_Pause_Outro_Count()
					MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_FRANKLIN)	//, PR_TYPE_AMBIENT)
				ENDIF
				
			ENDIF
			
			sZSkipData.bDoSkip = FALSE
#ENDIF
		ENDIF
		
		PRINTSTRING("test seven")PRINTNL()
		
		SET_MAGDEMO_SCENE()
		
		PRINTSTRING("test eight")PRINTNL()
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
	INT iDebugStage
	PROC SETUP_MAGDEMO_WIDGETS()
		START_WIDGET_GROUP("MAGDEMO 2")
			
			ADD_WIDGET_BOOL("Kill script", bKillScript)
			ADD_WIDGET_INT_READ_ONLY("Stage", iDebugStage)
			ADD_WIDGET_INT_READ_ONLY("Stage control", iControl)
			
			ADD_WIDGET_BOOL("bAudioScene_MikeWalk", bAudioScene_MikeWalk)
			ADD_WIDGET_BOOL("bAudioScene_TrevWake", bAudioScene_TrevWake)
			ADD_WIDGET_BOOL("bAudioScene_TrevBoat", bAudioScene_TrevBoat)
			ADD_WIDGET_BOOL("bAudioScene_TrevDive", bAudioScene_TrevDive)
			ADD_WIDGET_BOOL("bAudioGroup_Ranger", bAudioGroup_Ranger)
			ADD_WIDGET_BOOL("bAudioGroup_Tourbus", bAudioGroup_Tourbus)
			
		STOP_WIDGET_GROUP()
		
		sZSkipData.SkipMenuStruct[0].sTxtLabel = "Franklin base jumping"
		sZSkipData.SkipMenuStruct[1].sTxtLabel = "Trevor diving"
		sZSkipData.SkipMenuStruct[2].sTxtLabel = "Michael walking around city"
		
		
		IF g_iMagDemoVariation = 1
			sZSkipData.SkipMenuStruct[3].sTxtLabel = "FBI4"
		ELSE
			sZSkipData.SkipMenuStruct[3].sTxtLabel = "FRA2"
		ENDIF
	ENDPROC

	PROC MAINTAIN_MAGDEMO_WIDGETS()
	
		iDebugStage = ENUM_TO_INT(eMDStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
				IF GET_HASH_KEY(GET_NAME_OF_SCRIPT_TO_AUTOMATICALLY_START()) != GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
					SET_MD_STAGE(MD_STAGE_CLEANUP)
				ENDIF
			ENDIF
		ENDIF
		IF bKillScript
			SET_MD_STAGE(MD_STAGE_CLEANUP)
		ENDIF
		
		IF eMDStage != MD_STAGE_RESET
		AND eMDStage != MD_STAGE_CLEANUP
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF LAUNCH_MISSION_STAGE_MENU(sZSkipData.SkipMenuStruct, sZSkipData.iSelectedStage, sZSkipData.iCurrentStage, FALSE, "MAGDEMO 2", FALSE)
				sZSkipData.bDoSkip = TRUE
				SET_MD_STAGE(MD_STAGE_RESET)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT

	/*
		NOTES
		
		-runscript=magdemo2
		  startup.sc checks if the magdemo2 has been specificed and sets the g_bMagDemoActive to TRUE.
		  It then requests and starts this script once main_persistent.sc has been started.
		  main.sc no longer fades the game in if g_bMagDemoActive is set to TRUE.
		  player_controller.sc no longer sets a random character on startup if g_bMagDemoActive is set to TRUE.
		  
		- DO_STAGE_CREATE_INITIAL_SCENE()
		  This proc must wait for initial.sc to terminate for the force cleanup to be successful.
		  
	*/

	PRINTLN("LAUNCHING MAGDEMO 2 SCRIPT")
	PRINTLN("...variation=", g_iMagDemoVariation)
	
	
	bAudioScene_MikeWalk = bAudioScene_MikeWalk
	bAudioScene_TrevWake = bAudioScene_TrevWake
	bAudioScene_TrevBoat = bAudioScene_TrevBoat
	bAudioScene_TrevDive = bAudioScene_TrevDive
	bAudioGroup_Ranger = bAudioGroup_Ranger
	bAudioGroup_Tourbus = bAudioGroup_Tourbus
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CLEANUP_SCRIPT(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SETUP_MAGDEMO_WIDGETS()
	#ENDIF
	
	
	IF g_iMagDemoVariation = 1
		sMissionName = "fbi4"	
	ELSE
		sMissionName = "franklin2"
	ENDIF
	
	
	iNavBlockID = ADD_NAVMESH_BLOCKING_OBJECT( <<257.373, 154.617, 104.466>>, <<5.025, 2.9, 3.175>>, -0.351, FALSE )
	
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_SHARK, RELGROUPHASH_PLAYER) 
	
	WHILE TRUE
	
		#IF IS_DEBUG_BUILD
			MAINTAIN_MAGDEMO_WIDGETS()
		#ENDIF
		
		SWITCH eMDStage
		
			////////////////////////////////////////////////////////////////
			///       CREATE INITIAL SCENE
			CASE MD_STAGE_CREATE_INITIAL_SCENE
				DO_STAGE_CREATE_INITIAL_SCENE()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       FRANKLIN BASE JUMPING
			CASE MD_STAGE_FRANKLIN_BASEJUMP
				DO_STAGE_STAGE_FRANKLIN_BASEJUMP()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       TREVOR WAKES ON BEACH THEN GOES DIVING
			CASE MD_STAGE_TREVOR_DIVING
				DO_STAGE_TREVOR_DIVING()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       MICHAEL EXITS RESTAURANT AND WALKS AROUND CITY
			CASE MD_STAGE_MICHAEL_WALK_AROUND_CITY
				DO_STAGE_MICHAEL_WALK_AROUND_CITY()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       TRIGGER FRA2 MISSION
			CASE MD_STAGE_FRA2
			
				IF g_iMagDemoVariation = 1
					DO_STAGE_FBI4()
				ELSE
					DO_STAGE_FRA2()
				ENDIF
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       RESET
			CASE MD_STAGE_RESET
				DO_STAGE_RESET()
			BREAK
			
			////////////////////////////////////////////////////////////////
			///       CLEANUP
			CASE MD_STAGE_CLEANUP
				CLEANUP_SCRIPT(TRUE)
			BREAK
		ENDSWITCH
		
		
		UPDATE_EXIT_BOAT_CAM()
		
		UPDATE_HINT_CAM()
		
		UPDATE_PLAYER_PED_STATES()
		
		// #826620 block shocking events throughout entire magdemo
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_AFFECTS_OTHERS)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_POTENTIALLY_DANGEROUS)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_SERIOUS_DANGER)
		
		// #739576 always suppress scenario exits
		SUPPRESS_NORMAL_SCENARIO_EXITS_NEXT_FRAME()
		
		// #725141
		SUPPRESS_SCENARIO_ATTRACTION_NEXT_FRAME()
		
		// #728489
		SUPPRESS_BREAKOUT_SCENARIO_EXITS_NEXT_FRAME()
		
		// #831289 and #824566
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_PURSUE_WHEN_HIT_BY_CAR, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_STEAL_VEHICLE, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_PED_JAY_WALK_LIGHTS, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_ON_FOOT_FROM_VEHICLE, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_ON_FOOT, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_WARNING, FALSE)
		//SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_FLEE, FALSE)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_VEHICLE_DRIVING_FAST, FALSE)
		
		// #834333
		SUPPRESS_AGITATION_EVENTS_NEXT_FRAME()
		
		// #1249237
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE, FALSE)
 		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_FLEE_SPAWNED, FALSE)
		
		
		WAIT(0)
		
	ENDWHILE
ENDSCRIPT
#ENDIF
