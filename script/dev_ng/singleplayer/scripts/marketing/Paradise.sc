USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_clock.sch"
USING "flow_public_core_override.sch"
USING "commands_cutscene.sch"
USING "commands_graphics.sch"

USING "select_mission_stage.sch"
USING "shared_debug.sch"
USING "script_debug.sch"
USING "scripted_cam_editor_public.sch"
USING "script_ped.sch"
USING "cellphone_public.sch"

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					150
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					50	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				25	
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		5
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK		8
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		5


USING "traffic.sch"
USING "paradise.sch"

USING "cam_recording_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paradise - Trailer Script
//		AUTHOR			:	A Ross and Matt production.
//		DESCRIPTION		:	Trailer shot selection script.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//VECTOR vCrashedCarCoords = <<673.621, -148.652, 48.387>>
//VECTOR vCrashedCarRotation = <<-190.100, -93.800, -2.700>>

CONST_INT TIMECYCLE_REGION_GLOBAL		0
CONST_INT TIMECYCLE_REGION_URBAN		1

FLOAT fWindSpeed = 3.0
FLOAT fWindDirectionRadians = 6.283 //2.6 

VECTOR vDogPos = << -119.809, -1620.550, 31.150 >>
FLOAT fDogHeading = 50.0
FLOAT fDogPhase = 0.3

VECTOR vCrashedCarCoords = << 1012.782, 277.450, 81.087 >>
VECTOR vCrashedCarRotation = <<0.0, 0.0, 0.764>>

CONST_INT MAX_SKIP_MENU_LENGTH 43                      // number of stages in mission + 2 (for menu )
INT iReturnStage                                       // mission stage to jump to
MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

INT InitialTimeOfDayHour[MAX_SKIP_MENU_LENGTH]   

INT shotLength[MAX_SKIP_MENU_LENGTH]   

FLOAT shotLengthScale = 1.0

FLOAT fMainPlaybackSpeed = 1.0

INT i_current_event
BOOL bDisplayTitles = FALSE

BOOL bSwitchCam
BOOL bUsedZMenu		= FALSE
BOOL bChaseCullEnabled = FALSE
BOOL bJoggersCullEnabled = FALSE
BOOL bChase2CullEnabled = FALSE
BOOL bWorklineCullEnabled = FALSE
BOOL bYogaCullEnabled = FALSE
BOOL bGangCullEnabled = FALSE

CAM_RECORDING_DATA camRecData

VECTOR vDebugPosition = << 147.8630, -1200.4750, 28.2951 >>
FLOAT fDebugHeading

VECTOR vRoads

INT iTrainConfiguration = 2
INT iRepeatsSinceLastForceVehPop = 0

FLOAT fConvertibleAnimStartTime = 0.20

FLOAT fWindmillPlaneStartTime = 16700.000
FLOAT fOilDerricksPlaneStartTime = 25700.000

FLOAT fJetSkiChopperStartTime = 7000.0

FIRE_INDEX fiScriptFire
FIRE_INDEX fiScriptFire2

SEQUENCE_INDEX trailerSeq

VEHICLE_INDEX vehCam
VEHICLE_INDEX vehCam2
VEHICLE_INDEX vehCam3
VEHICLE_INDEX vehCam4
VEHICLE_INDEX vehCam5
VEHICLE_INDEX vehCam6


VEHICLE_INDEX trailerVehicle
VEHICLE_INDEX trailerVehicle2
VEHICLE_INDEX trailerVehicle3
//VEHICLE_INDEX trailerVehicle4
//VEHICLE_INDEX trailerVehicle5
//VEHICLE_INDEX trailerVehicle6

VEHICLE_INDEX trailerVehicles[12]


//VECTOR curCoords
//VECTOR prevCoords
//FLOAT zSpeed
//FLOAT prevZspeed
//FLOAT fShakeThreshold = 1.0
//FLOAT fShakeAmount = 0.5

VECTOR scenePositionOffset
VECTOR sceneRotationOffset

VECTOR vBenchPosition = <<-1276.10, 33.900, 49.60>>
VECTOR vBenchRotation = <<0.0, 0.0, -53.900>>

TEXT_LABEL_63 debugPedName

PED_INDEX trailerPeds[19]

INT iterator

STRUCT BIRD_STRUCT

	PED_INDEX ped
	INT i_event
	
ENDSTRUCT

BIRD_STRUCT s_birds[9]

INT sceneId
INT sceneId2
INT sceneId3

INT iCameraChoice

INT iCameraStage

//VECTOR vsubcoords

INT iRepositionPlayer

//FLOAT fMod

PTFX_ID trailerParticles[7]

VECTOR vParticlePosition[7]
VECTOR vParticleRotation[7]

VECTOR scenePosition
VECTOR sceneRotation

VECTOR scenePosition2
VECTOR sceneRotation2


VECTOR scenePosition3
VECTOR sceneRotation3

OBJECT_INDEX oiTrailerProp
OBJECT_INDEX oiTrailerProp2
OBJECT_INDEX oiTrailerProp3
OBJECT_INDEX oiTrailerProp4
OBJECT_INDEX oiTrailerProp5
OBJECT_INDEX oiTrailerProp6
OBJECT_INDEX oiTrailerProp7

VECTOR vGolfBagOffset = <<-0.20, -1.10, 0.580>>

WIDGET_GROUP_ID paradiseWidgetGroup

CAMERA_INDEX cam_main

BOOL bRepeatShot = TRUE
BOOL bTurnOffRoads = FALSE
BOOL bHighDofOn = FALSE
BOOL bFirstTimePlayingThisShot = FALSE

BOOL bResetPlayer = TRUE
BOOL bDontSkipTimeInDusterRecording = FALSE
BOOL bDontClearAreaBetweenRepeatPlays = FALSE

INT iDrawable = 0
INT iTexture  = 0

STRUCT PACKER_VEHICLES
	VEHICLE_INDEX vehTrailer
	VEHICLE_INDEX vehCars[6]
ENDSTRUCT

PACKER_VEHICLES sPackerRear

ENUM MISSION_STAGE_FLAG 

	STAGE_WAIT_FOR_STAGE,
	STAGE_RUN_THROUGH_SHOTS,
	STAGE_INTRO_SHOT,
	STAGE_JET_SKI,
	STAGE_JOGGERS4,
	STAGE_ROCKSTAR_GAMES,
	STAGE_GOLF,
	STAGE_GOLF_SHOT_2,
	STAGE_HIKING_NEW2,
	STAGE_LD_CONVERTIBLES2,
	STAGE_WINDMILLS,
	STAGE_WINDMILLS_ALT,
	STAGE_YOGA_NEW_6,
	STAGE_MUSCLE_BEACH,
	STAGE_GANGBANGER_NEW,
	STAGE_CONSTRUCTION_SITE,
	STAGE_MICHAEL,
	STAGE_MICHAEL_ALT,
	STAGE_DUSTER3,
	STAGE_JEWEL_HEIST,
	STAGE_CHASE2,
	STAGE_CHASE2_SECOND_SHOT,
	STAGE_DOCKS,
//	STAGE_BIKES,
//	STAGE_BIKES2,
//	STAGE_BIKES3,
	STAGE_TRAIN,
	STAGE_TRAIN_B,

	
	STAGE_WORK_LINE,
	STAGE_FORECLOSURE2,
//	STAGE_FORECLOSURE1,
	STAGE_SALTON_SEA_HOOKERS,
	STAGE_SALTON_SEA_HOOKERS_2,
	STAGE_BEGGARS1,
	STAGE_TENT_CITY_TOWN,
	STAGE_FIGHTER_JET,
	STAGE_CLUB_THROWOUT,
	STAGE_NEW_OIL_DERRICKS,
	STAGE_NEW_OIL_DERRICKS_ALT,
	STAGE_POLICE_CHASE_RED_CAR,
	STAGE_ALLEY_CHASE_DAY,
	STAGE_ALLEY_CHASE,
	STAGE_CRASHED_CAR2,
	STAGE_VINEWOOD_SIGN,
	STAGE_VINEWOOD_SIGN_ALT,
	STAGE_FINAL_SHOT,
	STAGE_FINAL_SHOT_ALT,

//	STAGE_RECORD,
	
	STAGE_NO_MORE_STAGES			//Leave this as last stage, used to detect end of shotlist.


	
ENDENUM

MISSION_STAGE_FLAG mission_stage = STAGE_WAIT_FOR_STAGE


CONST_INT BIRD_SIZE 3
STRUCT BIRDS
       PED_INDEX ped
       VECTOR position
       VECTOR velocity
ENDSTRUCT

BIRDS bird_flock[BIRD_SIZE]
MODEL_NAMES model_bird = A_C_SEAGULL
VECTOR start_position

FUNC BOOL IsEntityAlive(ENTITY_INDEX mEntity)
      If DOES_ENTITY_EXIST(mEntity)
            if not IS_ENTITY_DEAD(mEntity)
                  return TRUE 
            ENDIF
      ENDIF
      return FALSE
ENDFUNC

FUNC VECTOR CENTRE_MASS(BIRDS& bird, BIRDS& flock[])
       VECTOR v = <<0,0,0>>
       VECTOR original_pos = bird.position
       VECTOR coords 
       INT i
       REPEAT COUNT_OF(flock) i 
              IF IsEntityAlive(flock[i].ped)
                     IF bird.ped != flock[i].ped
                           coords = flock[i].position
                           v += coords
                     ENDIF
              ENDIF
       ENDREPEAT
       v = v / TO_FLOAT(COUNT_OF(flock) - 1)
       v = v - original_pos
       v /= 10000.0
       return v
ENDFUNC

FUNC VECTOR KEEP_DISTANCE(BIRDS& bird, BIRDS& flock[])
       VECTOR v = <<0,0,0>>
       INT i 
       REPEAT COUNT_OF(flock) i 
              IF IsEntityAlive(flock[i].ped)
                     IF bird.ped != flock[i].ped
                           IF GET_DISTANCE_BETWEEN_ENTITIES(bird.ped, flock[i].ped) < 1
                                  v = v - (bird.position - flock[i].position)/8.0
                           ENDIF
                     ENDIF 
              ENDIF
       ENDREPEAT 
       return v
ENDFUNC

FUNC VECTOR MATCH_VELOCITY(BIRDS& bird, BIRDS& flock[])
	VECTOR v = <<0,0,0>>
	INT i
	REPEAT COUNT_OF(flock) i 
	      IF IsEntityAlive(flock[i].ped)
	             IF bird.ped != flock[i].ped
	                   v = v + flock[i].velocity
	             ENDIF
	      ENDIF
	ENDREPEAT
	v = v / TO_FLOAT(COUNT_OF(flock) - 1)
	v = v - bird.velocity 
	v /= 100.0
	return v 
ENDFUNC

FUNC VECTOR TEND_TO_POSITION(BIRDS& bird, VECTOR dest)
   VECTOR v = <<0,0,0>>
   v = (dest - bird.position)/5000.0
   return v
ENDFUNC

PROC UPDATE_FLOCK(BIRDS& flock[])
       INT i 
       VECTOR v1 
       //VECTOR v2
       //VECTOR v3 
       VECTOR v4
       REPEAT COUNT_OF(flock) i  
              IF ISEntityAlive(flock[i].ped)
                     v1 = CENTRE_MASS(flock[i], flock)
                     //v2 = KEEP_DISTANCE(flock[i], flock)
                     //v3 = MATCH_VELOCITY(flock[i], flock)
                     v4 = TEND_TO_POSITION(flock[i], start_position)
                     flock[i].velocity = flock[i].velocity + v1 + v4//+ v2 + v3
                    GET_HEADING_FROM_VECTOR_2D(flock[i].velocity.x, flock[i].velocity.y)
                     flock[i].position = flock[i].position + flock[i].velocity
                     SET_ENTITY_COORDS(flock[i].ped, flock[i].position)
//                     FLOAT yy = GET_HEADING_FROM_VECTOR_2D(flock[i].position.x + flock[i].velocity.x, flock[i].position.y + flock[i].velocity.y)
                    // PRINTFLOAT(yy) PRINTNL()
                     SET_ENTITY_HEADING(flock[i].ped, GET_HEADING_FROM_VECTOR_2D(flock[i].velocity.x, flock[i].velocity.y))
              ENDIF
       ENDREPEAT
ENDPROC


PROC CREATE_FLOCK(BIRDS& flock[], VECTOR v_bird_1, VECTOR v_bird_2, VECTOR v_bird_3)
       INT i 
       start_position = v_bird_1
       REPEAT COUNT_OF(flock) i 
              VECTOR start_pos = v_bird_1
              
			  IF i = 0
			  	start_pos = v_bird_1
			  ELIF i = 1
			  	start_pos = v_bird_2
			  ELIF i = 2
			  	start_pos = v_bird_3
			  ENDIF
			  
              /*FLOAT r = 3.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
              r *= 5
              start_pos.x += r
              
              r = 3.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
              r *= 5
              start_pos.y += r
              
              r = 3.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
              r *= 5
              start_pos.z += r/2.0*/
			  
			  
              
              FLOAT v = (GET_RANDOM_FLOAT_IN_RANGE(0, 1) * 2.0) -1
              v *= 0.1
              flock[i].velocity.x += v
              
              v = (GET_RANDOM_FLOAT_IN_RANGE(0, 1) * 2.0) -1
              v *= 0.1
              flock[i].velocity.y += v
              
              v = (GET_RANDOM_FLOAT_IN_RANGE(0, 1) * 2.0) -1
              v *= 0.1
              flock[i].velocity.z += v 
               
              flock[i].position = start_pos
              flock[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, flock[i].position, TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0, 360)))
			  
			  IF i % 2 = 0
	              TASK_PLAY_ANIM(flock[i].ped, "creatures@gull@move", "flapping_bank_l_wide", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_IGNORE_GRAVITY|AF_NOT_INTERRUPTABLE | AF_LOOPING)
	              WAIT(2) 
	              IF ISEntityAlive(flock[i].ped)
	                     SET_ENTITY_ANIM_SPEED(flock[i].ped, "creatures@gull@move", "flapping_bank_l_wide", 0.5 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.5))
	                     SET_ENTITY_COORDS(flock[i].ped, flock[i].position)
	              ENDIF
				ELSE
					TASK_PLAY_ANIM(flock[i].ped, "creatures@gull@move", "flapping_bank_r_wide", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_IGNORE_GRAVITY|AF_NOT_INTERRUPTABLE | AF_LOOPING)
	              WAIT(2) 
	              IF ISEntityAlive(flock[i].ped)
	                     SET_ENTITY_ANIM_SPEED(flock[i].ped, "creatures@gull@move", "flapping_bank_r_wide", 0.5 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.5))
	                     SET_ENTITY_COORDS(flock[i].ped, flock[i].position)
	              ENDIF
				ENDIF
				
				SET_PED_GRAVITY(flock[i].ped, FALSE)
       ENDREPEAT
ENDPROC

PROC DELETE_FLOCK(BIRDS& flock[])
       INT i 
       
       REPEAT COUNT_OF(flock) i 
              IF ISEntityAlive(flock[i].ped)
                     DELETE_PED(flock[i].ped) 
              ENDIF
       ENDREPEAT
ENDPROC



// ===========================================================================================================
//		Init
// ===========================================================================================================


PROC stageInit()
	
	//SkipMenuStruct[ENUM_TO_INT(STAGE_WAIT_FOR_STAGE)].bSelectable
	SkipMenuStruct[ENUM_TO_INT(STAGE_WAIT_FOR_STAGE)].sTxtLabel = "Select Shot:"
	SkipMenuStruct[ENUM_TO_INT(STAGE_WAIT_FOR_STAGE)].bSelectable = FALSE
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_RUN_THROUGH_SHOTS)].sTxtLabel = "RUN THROUGH ALL SHOTS"	
	SkipMenuStruct[ENUM_TO_INT(STAGE_ROCKSTAR_GAMES)].sTxtLabel = "Rockstar Games Presents"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_LD_CONVERTIBLES2)].sTxtLabel = "Convertibles - Opt B"
	SkipMenuStruct[ENUM_TO_INT(STAGE_SALTON_SEA_HOOKERS)].sTxtLabel = "Salton Sea Hookers"
	SkipMenuStruct[ENUM_TO_INT(STAGE_SALTON_SEA_HOOKERS_2)].sTxtLabel = "Salton Sea Hookers -NIGHT"
		
	SkipMenuStruct[ENUM_TO_INT(STAGE_GANGBANGER_NEW)].sTxtLabel = "Old and Young Gangbangers (New loc)"
		
	SkipMenuStruct[ENUM_TO_INT(STAGE_MUSCLE_BEACH)].sTxtLabel = "Muscle Beach"
	SkipMenuStruct[ENUM_TO_INT(STAGE_JOGGERS4)].sTxtLabel = "Joggers - 4 peds"

	SkipMenuStruct[ENUM_TO_INT(STAGE_JEWEL_HEIST)].sTxtLabel = "Jewel Heist"
	SkipMenuStruct[ENUM_TO_INT(STAGE_JET_SKI)].sTxtLabel = "Jet Ski"
	SkipMenuStruct[ENUM_TO_INT(STAGE_MICHAEL)].sTxtLabel = "Michael - close up"
	SkipMenuStruct[ENUM_TO_INT(STAGE_MICHAEL_ALT)].sTxtLabel = "Michael (alt lighting)"
	SkipMenuStruct[ENUM_TO_INT(STAGE_INTRO_SHOT)].sTxtLabel = "Intro Beach Shot - Lifeguard hut"
	SkipMenuStruct[ENUM_TO_INT(STAGE_WINDMILLS)].sTxtLabel = "Windmills"
	SkipMenuStruct[ENUM_TO_INT(STAGE_WINDMILLS_ALT)].sTxtLabel = "Windmills (alt lighting)"
	//SkipMenuStruct[ENUM_TO_INT(STAGE_HIKING_NEW)].sTxtLabel = "Hiking(new location)"
	SkipMenuStruct[ENUM_TO_INT(STAGE_HIKING_NEW2)].sTxtLabel = "Hiking(new location 2)"
	//SkipMenuStruct[ENUM_TO_INT(STAGE_HIKING1)].sTxtLabel = "Hiking - 1"
	SkipMenuStruct[ENUM_TO_INT(STAGE_DUSTER3)].sTxtLabel = "Crop Duster."
	SkipMenuStruct[ENUM_TO_INT(STAGE_CRASHED_CAR2)].sTxtLabel = "Crashed car on fire"
	SkipMenuStruct[ENUM_TO_INT(STAGE_BEGGARS1)].sTxtLabel = "Beggars 1"
	SkipMenuStruct[ENUM_TO_INT(STAGE_NEW_OIL_DERRICKS)].sTxtLabel = "Oil Derricks Skyline: Shot 32"
	SkipMenuStruct[ENUM_TO_INT(STAGE_NEW_OIL_DERRICKS_ALT)].sTxtLabel = "Oil Derricks Skyline: Alt lighting"
	SkipMenuStruct[ENUM_TO_INT(STAGE_TENT_CITY_TOWN)].sTxtLabel = "Tent City 1 - Town"
	SkipMenuStruct[ENUM_TO_INT(STAGE_TRAIN)].sTxtLabel = "Train - Opt A"
	SkipMenuStruct[ENUM_TO_INT(STAGE_TRAIN_B)].sTxtLabel = "Train - Opt B"
	SkipMenuStruct[ENUM_TO_INT(STAGE_FINAL_SHOT)].sTxtLabel = "Final Shot - GTA V Logo shot."
	SkipMenuStruct[ENUM_TO_INT(STAGE_FINAL_SHOT_ALT)].sTxtLabel = "Final Shot - GTA V Logo shot. (Alt lighting)"
	
	//SkipMenuStruct[ENUM_TO_INT(STAGE_CHASE)].sTxtLabel = "Cinematic Car Chase"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CHASE2)].sTxtLabel = "Vinewood Cruise (first shot)"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CHASE2_SECOND_SHOT)].sTxtLabel = "Vinewood Cruise (second shot)"
	//SkipMenuStruct[ENUM_TO_INT(STAGE_LD_CONVERTIBLES)].sTxtLabel = "Convertibles"
	SkipMenuStruct[ENUM_TO_INT(STAGE_LD_CONVERTIBLES2)].sTxtLabel = "Convertibles - Opt B"
	
	
//	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_1)].sTxtLabel = "Yoga(New) 1 - House on hills"
//	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_2)].sTxtLabel = "Yoga(New) 2 - House, Pool"
//	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_3)].sTxtLabel = "Yoga(New) 3 - House"
//	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_4)].sTxtLabel = "Yoga(New) 4 - House"
//	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_5)].sTxtLabel = "Yoga(New) 5 - House"
	SkipMenuStruct[ENUM_TO_INT(STAGE_YOGA_NEW_6)].sTxtLabel = "Yoga(New) 6 - House"	
	SkipMenuStruct[ENUM_TO_INT(STAGE_GOLF)].sTxtLabel = "Golf"
	SkipMenuStruct[ENUM_TO_INT(STAGE_GOLF_SHOT_2)].sTxtLabel = "Golf shot 2"
	SkipMenuStruct[ENUM_TO_INT(STAGE_ALLEY_CHASE_DAY)].sTxtLabel = "Alley Chase - Day"
	SkipMenuStruct[ENUM_TO_INT(STAGE_ALLEY_CHASE)].sTxtLabel = "Alley Chase (second shot)"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CLUB_THROWOUT)].sTxtLabel = "Club Throwout"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_WORK_LINE)].sTxtLabel = "Work Line - Los Santos Graffiti"
	SkipMenuStruct[ENUM_TO_INT(STAGE_FORECLOSURE2)].sTxtLabel = "Foreclosure"

	SkipMenuStruct[ENUM_TO_INT(STAGE_VINEWOOD_SIGN)].sTxtLabel = "VINEWOOD sign"
	SkipMenuStruct[ENUM_TO_INT(STAGE_VINEWOOD_SIGN_ALT)].sTxtLabel = "VINEWOOD sign - Alternate lighting"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_CONSTRUCTION_SITE)].sTxtLabel = "Construction site"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_DOCKS)].sTxtLabel = "Dock yards"
	SkipMenuStruct[ENUM_TO_INT(STAGE_FIGHTER_JET)].sTxtLabel = "Fighter Jet Shot"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_POLICE_CHASE_RED_CAR)].sTxtLabel = "Police Chase Red Car"
	
	

	TEXT_LABEL_63 menuIndex
		
	FOR iterator = 0 TO MAX_SKIP_MENU_LENGTH - 1

		menuIndex = ""
		menuIndex += iterator
		menuIndex += ": "
		menuIndex += SkipMenuStruct[iterator].sTxtLabel
				
		SkipMenuStruct[iterator].sTxtLabel =  menuIndex
		//PRINTSTRING(SkipMenuStruct[iterator].sTxtLabel) PRINTNL()
	ENDFOR
	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_INTRO_SHOT)] = 19
					
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_WORK_LINE)] = 12
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_GOLF)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_GOLF_SHOT_2)] = 16
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_ALLEY_CHASE)] = 16//22
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_ALLEY_CHASE_DAY)] = 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_CLUB_THROWOUT)] = 22
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_WAIT_FOR_STAGE)] = 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_ROCKSTAR_GAMES)] = 6
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_YOGA_NEW_6)] = 16
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_JEWEL_HEIST)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_SALTON_SEA_HOOKERS)] = 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_SALTON_SEA_HOOKERS_2)] = 00
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_MUSCLE_BEACH)] = 18	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_GANGBANGER_NEW)] = 9
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_JOGGERS4)] = 12
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_MICHAEL)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_MICHAEL_ALT)] = 9
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_JET_SKI)] = 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_WINDMILLS)] = 12
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_WINDMILLS_ALT)] = 12
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_HIKING_NEW2)] = 16
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_DUSTER3)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_CRASHED_CAR2)] = 16
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_FORECLOSURE2)] = 9
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_BEGGARS1)] = 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_NEW_OIL_DERRICKS)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_NEW_OIL_DERRICKS_ALT)] = 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_TENT_CITY_TOWN)] = 18
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_TRAIN)] = 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_TRAIN_B)] = 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_FINAL_SHOT)] = 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_FINAL_SHOT_ALT)] = 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_LD_CONVERTIBLES2)] = 12
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_CHASE2)] = 19
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_CHASE2_SECOND_SHOT)] = 16
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_VINEWOOD_SIGN)] = 21
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_VINEWOOD_SIGN_ALT)] = 19
	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_CONSTRUCTION_SITE)] = 16
	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_DOCKS)] = 7
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_FIGHTER_JET)] = 9
	
	InitialTimeOfDayHour[ENUM_TO_INT(STAGE_POLICE_CHASE_RED_CAR)] = 7
	
	vParticlePosition[0] = <<2.7, 2.2, -0.1>>
	vParticlePosition[1] = <<-2.7, 2.2, -0.1>>
	vParticlePosition[2] = <<1.95, -1.9, -0.45>>
	vParticlePosition[3] = <<-1.95, -1.9, -0.45>>
	
	
	//Init Widgets
	paradiseWidgetGroup = START_WIDGET_GROUP("Paradise Trailer")
		
		ADD_WIDGET_BOOL("bDisplayTitles", bDisplayTitles)
		ADD_WIDGET_BOOL("bRepeatShot", bRepeatShot)
		
		ADD_WIDGET_BOOL("Use High Quality Depth of Field", bHighDofOn)
		
		ADD_WIDGET_BOOL("bTurnOffRoads", bTurnOffRoads)
		ADD_WIDGET_BOOL("bDontSkipTimeInDusterRecording", bDontSkipTimeInDusterRecording)
		
		ADD_WIDGET_STRING("Wind")
		ADD_WIDGET_FLOAT_SLIDER("fWindSpeed", fWindSpeed, 0.0, 12.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fWindDirectionRadians", fWindDirectionRadians, -(2*CONST_PI), (2*CONST_PI), 0.1)
		
		ADD_WIDGET_STRING("Gang Banger offset adjustment")
		
		ADD_WIDGET_VECTOR_SLIDER("scenePositionOffset", scenePositionOffset, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("sceneRotationOffset", sceneRotationOffset, -3000.0, 3000.0, 0.1)
		
		ADD_WIDGET_VECTOR_SLIDER("vDogPos", vDogPos, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fDogHeading", fDogHeading, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fDogPhase", fDogPhase, 0.0, 1.0, 0.05)
		
		
		ADD_WIDGET_STRING("Distant Plane time offset")
		ADD_WIDGET_FLOAT_SLIDER("fWindmillPlaneStartTime", fWindmillPlaneStartTime, 0.0, 100000.0, 500.00)
		ADD_WIDGET_FLOAT_SLIDER("fOilDerricksPlaneStartTime", fOilDerricksPlaneStartTime, 0.0, 100000.0, 500.00)		
		ADD_WIDGET_FLOAT_SLIDER("fJetSkiChopperStartTime", fJetSkiChopperStartTime, 0.0, 100000.0, 500.00)				
				
		ADD_WIDGET_INT_SLIDER("i_current_event", i_current_event, -100, 100, 1)		
		ADD_WIDGET_INT_SLIDER("iTrainConfiguration", iTrainConfiguration, 0, 100, 1)
								
		
		ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.x", vDebugPosition.x, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.y", vDebugPosition.y, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.z", vDebugPosition.z, -3000.0, 3000.0, 0.05)
		
		ADD_WIDGET_FLOAT_SLIDER("fDebugheading", fDebugheading, 0.0, 360.0, 0.05)
								
		ADD_WIDGET_FLOAT_SLIDER("shotLengthScale", shotLengthScale, 0.0, 10.0, 0.1)
		
		ADD_WIDGET_BOOL("bSwitchCam", bSwitchCam)
		
		ADD_WIDGET_INT_SLIDER("iDrawable", iDrawable, 0, 10, 1)
		ADD_WIDGET_INT_SLIDER("iDrawable", iTexture, 0, 10, 1)
	
		ADD_WIDGET_STRING("Crash car adjustment")
		ADD_WIDGET_VECTOR_SLIDER("vCrashedCarCoords", vCrashedCarCoords, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("vCrashedCarRotation", vCrashedCarRotation, -360.0, 360.0, 0.1)
		
		ADD_WIDGET_VECTOR_SLIDER("vBenchPosition", vBenchPosition, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("vBenchRotation", vBenchRotation, -360.0, 360.0, 0.1)
				
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[0].x", vParticlePosition[0].x, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[0].y", vParticlePosition[0].y, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[0].z", vParticlePosition[0].z, -3000.0, 3000.0, 0.05)
		
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[1].x", vParticlePosition[1].x, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[1].y", vParticlePosition[1].y, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[1].z", vParticlePosition[1].z, -3000.0, 3000.0, 0.05)
		
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[2].x", vParticlePosition[2].x, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[2].y", vParticlePosition[2].y, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[2].z", vParticlePosition[2].z, -3000.0, 3000.0, 0.05)
		
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[3].x", vParticlePosition[3].x, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[3].y", vParticlePosition[3].y, -3000.0, 3000.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("vParticlePosition[3].z", vParticlePosition[3].z, -3000.0, 3000.0, 0.05)
		
		ADD_WIDGET_FLOAT_SLIDER("fConvertibleAnimStartTime", fConvertibleAnimStartTime, 0.0, 1.00, 0.05)
		
		ADD_WIDGET_STRING("Golf")
		ADD_WIDGET_VECTOR_SLIDER("vGolfBagOffset", vGolfBagOffset, -3000.0, 3000.0, 0.1)
		
		//ADD_WIDGET_FLOAT_SLIDER("fShakeAmount", fShakeAmount, 0.0, 1.00, 0.05)
										
	STOP_WIDGET_GROUP()		
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	DISABLE_CELLPHONE(TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PATRIOT, TRUE)
	
	bDrawMenu = TRUE
	bFirstTimePlayingThisShot = TRUE
ENDPROC


// ===========================================================================================================
//		Trailer procedures
// ===========================================================================================================

FUNC INT GET_SHOT_LENGTH()

	RETURN ROUND(TO_FLOAT(shotLength[ENUM_TO_INT(mission_stage)]) * shotLengthScale)

ENDFUNC


PROC setPedVariation(PED_INDEX thisped, INT iSimpleVariation)

	set_ped_component_variation(thisped, ped_comp_head, iSimpleVariation, 0)
	set_ped_component_variation(thisped, PED_COMP_HAIR, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_hand, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_torso, iSimpleVariation, 0)
	set_ped_component_variation(thisped, ped_comp_leg, iSimpleVariation, 0)
	
	set_ped_component_variation(thisped, PED_COMP_DECL, iSimpleVariation, 0)	
	set_ped_component_variation(thisped, PED_COMP_FEET, iSimpleVariation, 0)	

ENDPROC

FUNC CAMERA_INDEX CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE(STRING CameraName, VECTOR vecPos, VECTOR vecRot, FLOAT FOV = 65.0, BOOL startActivated = FALSE)

	CAMERA_INDEX returnedCamera

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	IF (mission_stage <> STAGE_WAIT_FOR_STAGE)
		//DESTROY_ALL_CAMS()
	ENDIF
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vecPos)
	
	IF bFirstTimePlayingThisShot
		REQUEST_COLLISION_AT_COORD(vecPos)
		
		LOAD_SCENE(vecPos)
	ENDIF
	IF NOT DOES_CAM_EXIST(cam_main)
		returnedCamera = CREATE_CAM_WITH_PARAMS(CameraName, vecPos, vecRot, FOV, startActivated)
	ELSE
		returnedCamera = cam_main
		SET_CAM_PARAMS(cam_main, vecPos, vecRot, FOV)//, startActivated)
	ENDIF

	IF NOT IS_ENTITY_DEAD(trailerVehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(trailerVehicle)
		ENDIF
	ENDIF
	

	IF (mission_stage <> STAGE_WAIT_FOR_STAGE)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vecPos)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		IF bRepeatShot = FALSE
			SETTIMERB(0)
			
			WHILE TIMERB() < 5000
				SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)
				WAIT(0)
			ENDWHILE
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
		ENDIF
		
		//Currently shadows aren't frozen on the first playthrough of a scene, however if they do get frozen the sunlight angle doesn't update properly.
		//This seems to be because there needs to be a bit of time for the sun to update to match the clock time, so this fudge forces the clock time for a bit, 
		//then freezes the shadows.
		IF NOT bResetPlayer
			SETTIMERB(0)
			
			WHILE TIMERB() < 500
				SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)
				WAIT(0)
			ENDWHILE
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(trailerVehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle)
			UNPAUSE_PLAYBACK_RECORDED_VEHICLE(trailerVehicle)
		ENDIF
	ENDIF
	
	bFirstTimePlayingThisShot = FALSE
	
	RETURN returnedCamera

ENDFUNC

//PURPOSE: Loads assets and sets a vehicle recording playing at desired start time and speed.
FUNC BOOL INITIALISE_TRAILER_VEHICLE_SET_PIECE(INT recNum, STRING recName, MODEL_NAMES modelName, VEHICLE_INDEX &thisVehicle, FLOAT startTime = 0.0, FLOAT playbackSpeed = 1.0)

	REQUEST_VEHICLE_RECORDING(recNum, recName)
	REQUEST_MODEL(modelName)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelName, TRUE)
	
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(recNum, recName)
	AND HAS_MODEL_LOADED(modelName)
	AND NOT DOES_ENTITY_EXIST(thisVehicle)
		
		thisVehicle = CREATE_VEHICLE(modelName, <<0.0, 0.0, 0.0>>)
		START_PLAYBACK_RECORDED_VEHICLE(thisVehicle, recNum, recName)
		SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisVehicle, startTime)
		SET_PLAYBACK_SPEED(thisVEhicle, playbackSpeed)

		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


//PURPOSE: Loads assets and sets a vehicle recording playing at desired start time and speed.
FUNC BOOL INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(MODEL_NAMES modelName, VEHICLE_INDEX &thisVehicle, VECTOR vecPosition, FLOAT fHeading)

	REQUEST_MODEL(modelName)
	
	IF HAS_MODEL_LOADED(modelName)
		
		thisVehicle = CREATE_VEHICLE(modelName, vecPosition, fHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(thisVehicle)
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Cleans up a vehicle and it's Model.
PROC CLEANUP_TRAILER_VEHICLE_SETPIECE(VEHICLE_INDEX thisVehicle)

	IF DOES_ENTITY_EXIST(thisVehicle)
		//IF IS_ENTITY_DEAD
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_ENTITY_MODEL(thisVehicle), FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(thisVehicle))
		
	//ELSE
		//PRINTSTRING("Cleaning up entity that doesnt exist") PRINTNL()
		//PRINTSTRING("ENUM no: ") PRINTINT(ENUM_TO_INT(mission_stage)) PRINTNL()
	ENDIF
	DELETE_VEHICLE(thisVehicle)
	
ENDPROC

//PURPOSE: Cleans up an ENTITY and it's Model.
PROC CLEANUP_TRAILER_ENTITY(ENTITY_INDEX thisEntity)

	IF DOES_ENTITY_EXIST(thisEntity)
		
	
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(thisEntity))
		
	//ELSE
		//PRINTSTRING("Cleaning up entity that doesnt exist") PRINTNL()
		//PRINTSTRING("ENUM no: ") PRINTINT(ENUM_TO_INT(mission_stage)) PRINTNL()
	ENDIF
	
	DELETE_ENTITY(thisEntity)
	
ENDPROC

//PURPOSE: Cleans up an ENTITY and it's Model.
PROC CLEANUP_TRAILER_PED(PED_INDEX thisPed)

	IF DOES_ENTITY_EXIST(thisPed)
		
	
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(thisPed))
		
	//ELSE
		//PRINTSTRING("Cleaning up entity that doesnt exist") PRINTNL()
		//PRINTSTRING("ENUM no: ") PRINTINT(ENUM_TO_INT(mission_stage)) PRINTNL()
	ENDIF
	
	DELETE_PED(thisPed)
	
ENDPROC


PROC CLEANUP_PACKER_VEHICLES()	
	
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[0])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[1])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[2])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[3])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[4])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehCars[5])
	CLEANUP_TRAILER_VEHICLE_SETPIECE(sPackerRear.vehTrailer)
	
ENDPROC

// ===========================================================================================================
//		Shot procedures
// ===========================================================================================================


PROC stageYogaHouse()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -882.2856, 98.5925, 54.5837 >>, << 12.1143, 0.0000, 52.7182 >>, 50.0000, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE

				
			
			/* START SYNCHRONIZED SCENE - trailer_yoga_c.xml */
			scenePosition = << -890.290, 108.980, 53.916 >>
			sceneRotation = << 0.000, 0.000, -88.200 >>

			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)

			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)

			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(5250-5550)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(5250-5550)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


		
			SET_CAM_PARAMS(cam_main, << -883.5505, 100.8229, 54.5837 >>, << 12.1143, -0.0000, 52.7182 >>, 50.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > GET_SHOT_LENGTH()
				
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


PROC stageYogaHouse2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -890.3007, 97.6830, 54.3904 >>, << 11.9525, -0.0000, 5.5822 >>, 50.0000, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE

				
			
			/* START SYNCHRONIZED SCENE - trailer_yoga_c.xml */
			scenePosition = << -890.290, 108.980, 53.916 >>
			sceneRotation = << 0.000, 0.000, -88.200 >>

			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)

			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(5250-5550)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(5250-5550)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


		
			SET_CAM_PARAMS(cam_main, << -890.3007, 97.6830, 54.3904 >>, << 11.9525, -0.0000, 5.5822 >>, 45.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > GET_SHOT_LENGTH()
				
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageYogaBigPark()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1350.3209, -1399.6086, 5.4041 >>, << 8.0398, -0.0000, -39.9797 >>, 58.8889, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE

				
			/* START SYNCHRONIZED SCENE - trailer_yoga_d.xml */
			scenePosition = << -1343.595, -1394.715, 3.626 >>
			sceneRotation = << 0.000, 0.000, 163.440 >>

			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)

			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)

			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(7100-7400)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(7100-7400)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
		
			SET_CAM_PARAMS(cam_main, << -1348.3600, -1397.2252, 5.4041 >>, << 8.0397, 0.0000, -39.9794 >>, 58.2536, GET_SHOT_LENGTH(), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > GET_SHOT_LENGTH()
				
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


PROC stageyogaNew1()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<21.158800,538.562561,174.154449>>,<<6.668653,-0.000000,176.217590>>,45.000000, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
						
			/* START SYNCHRONIZED SCENE - trailer_yoga_a.xml */
			scenePosition = << 19.191, 532.338, 173.608 >>
			sceneRotation = << 0.000, 0.000, -38.750 >>
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
						
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )



		
			SET_CAM_PARAMS(cam_main, <<21.158800,537.064514,174.154449>>,<<6.668653,-0.000000,176.217590>>,45.000000, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC


PROC stageyogaNew2()

		SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<28.83,519.11,170.26>>,<<8.38,0.22,72.34>>,50.000000, TRUE)
					
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
						
			/* START SYNCHRONIZED SCENE - trailer_yoga_a.xml */
			scenePosition = << 22.616, 521.763, 169.243 >>
			sceneRotation = << 0.000, 0.000, -13.5 >>
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
		//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main,  << 26.9030, 520.3645, 170.2600 >>, << 8.3800, 0.2200, 72.3400 >>, 50.0, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC


PROC stageyogaNew3()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<3.671074,520.949707,169.785477>>,<<6.792352,0.220000,-152.118912>>,50.732414, TRUE)
					
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
						
			/* START SYNCHRONIZED SCENE - trailer_yoga_a.xml */
			scenePosition = << 5.028, 518.113, 169.230 >>
			sceneRotation = << 0.000, 0.000, -58.5 >>
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
		//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main,  <<4.583732,519.392090,169.785477>>,<<6.792352,0.220000,-152.118912>>,50.732414, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC


PROC stageyogaNew4()

		SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<3.671074,520.949707,169.785477>>,<<6.792352,0.220000,-152.118912>>,50.732414, TRUE)
					
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
						
			/* START SYNCHRONIZED SCENE - trailer_yoga_a.xml */
			scenePosition = << 5.552, 518.113, 169.230 >>
			sceneRotation = << 0.000, 0.000, 129.0 >>
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(1200-1500)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main,  <<4.583732,519.392090,169.785477>>,<<6.792352,0.220000,-152.118912>>,50.732414, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC


PROC stageyogaNew5()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-690.465210,882.478760,230.707489>>,<<-4.159828,-0.000000,-141.774368>>,50.000000, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
			scenePosition = << -673.01, 868.6130, 223.788 >>
			sceneRotation = << 0.000, 0.000, 168.250 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
		//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main, <<-687.309204,878.471741,230.240311>>,<<-5.692473,-0.000000,-141.774368>>,50.0000000, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC


PROC stageyogaNew6()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			SET_WEATHER_TYPE_NOW_PERSIST("overcast")
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL) 
			REQUEST_IPL("TRAILERshot_yoga")
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1025.934082,660.546875,160.320770,1.000000) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1016.898071,660.057556,156.048660,0.495719) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1011.452637,659.605103,156.409607,0.414500) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(42.0)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			SET_MAPDATACULLBOX_ENABLED("Traileryoga", TRUE) 
			bYogaCullEnabled = TRUE
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1033.156860,652.859497,162.708115>>,<<-18.651062,-0.185040,-53.440609>>,41.060978, TRUE)
		
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
			scenePosition = << -1023.6, 659.25, 160.275 >>
			sceneRotation = << 0.000, 0.000, 87.50 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
					
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main, <<-1032.599854,653.281799,164.778564>>,<<-18.651062,-0.185040,-53.440609>>,41.060978, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-999.27, 664.23, 160.82>>)
		
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH





ENDPROC

PROC stageyogaPark()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_F_Y_Yoga_01 )
			REQUEST_MODEL(A_M_Y_Yoga_01)
			REQUEST_ANIM_DICT("trailer@yoga")
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1148.337769,-524.080139,64.448601>>,<<2.571655,0.000000,99.920822>>,45.0000000, TRUE)
		
			
			
			WHILE NOT HAS_ANIM_DICT_LOADED("trailer@yoga")
			OR NOT HAS_MODEL_LOADED(A_F_Y_Yoga_01 )
			OR NOT HAS_MODEL_LOADED(A_M_Y_Yoga_01 )
				WAIT(0)
			ENDWHILE
			
			scenePosition = << 1137.900, -526.150, 63.550 >>
			sceneRotation = << 0.000, 0.000, 0.000 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Yoga_01, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Yoga_01, scenePosition, 0.0)
			
			//Loading ped model: A_M_Y_Yoga_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@yoga", "a_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@yoga", "b_000990_01_gc_yoga_1(a+b)(500-800)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

		
			SET_CAM_PARAMS(cam_main, <<1142.749878,-525.057495,64.703392>>,<<2.571655,0.000000,99.920822>>,45.000000, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Yoga_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Yoga_01)
				REMOVE_ANIM_DICT("trailer@yoga")
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC

PROC stageTrevorsTrailer()

	SWITCH i_current_event
		CASE 0
		
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(TOWTRUCK, trailerVehicle, << 1983.2673, 3825.2563, 31.4287 >>, 173.2730)
				WAIT(0)
			ENDWHILE
		
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1991.708130,3818.017334,32.899258>>,<<1.741572,-0.000000,81.986099>>,46.296680, TRUE)
			SET_CAM_PARAMS(cam_main, <<1991.784668,3818.284912,32.899258>>,<<1.741572,-0.000000,81.986099>>,46.296680, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 3000
				SETTIMERB(0)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
					
			SET_CAM_PARAMS(cam_main,<<1986.580078,3825.573486,32.863838>>,<<5.964571,-0.000000,83.044884>>,37.078899)
			SET_CAM_PARAMS(cam_main,<<1986.762817,3827.096436,32.863838>>,<<5.964571,-0.000000,83.044884>>,37.078899, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				
			SETTIMERB(0)
			i_current_event++
	
		BREAK
		
		
		CASE 3
			IF TIMERB() > 2500
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				i_current_event++
			ENDIF
		BREAK
		
		
	ENDSWITCH



ENDPROC


PROC stageWindmills()

	//SET_USE_HI_DOF()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
						
			REQUEST_MODEL(S_M_M_Security_01)
			
			WHILE NOT HAS_MODEL_LOADED(S_M_M_Security_01)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(2, "TrailerPlane", CUBAN800, trailerVehicle2, fWindmillPlaneStartTime)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_LOD_DIST(trailerVehicle2, 3000)
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(7, "Trailer", bodhi2, trailerVehicle, 10000.0, 0.5)
			
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_M_M_Security_01)
				//CLEAR_AREA(GET_ENTITY_COORDS(trailerVehicle), 200.0, TRUE)
				REMOVE_PARTICLE_FX_IN_RANGE(<<2176.9424, 1945.0815, 98.0490>>, 200.0)
			ENDIF
					
			//timecycle = CLearing (rural) @ 12:00 
			LOAD_CLOUD_HAT("horizon")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL)
			
			IF mission_stage = STAGE_WINDMILLS
				SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			ELIF mission_stage = STAGE_WINDMILLS_ALT
				SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			ENDIF
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,2120.014893,1870.651001,127.794350,1.276813)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,2216.709717,1938.416748,106.930191,2.682062)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,2147.351074,1968.718018,93.243904,1.181000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,2179.983398,2021.767212,105.121956,1.532313)	
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<2113.751221,1859.358032,129.624695>>,<<-1.976946,-2.275559,-30.016825>>,37.619801, TRUE)
			SET_CAM_FAR_DOF(cam_main, 30.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main,  <<2113.751221,1859.358032,129.624695>>,<<-2.450298,-2.275560,-35.917198>>,37.619801, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SET_CAM_FAR_DOF(cam_main, 30.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 3000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC stageOilFields()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(7, "Trailer", bodhi2, trailerVehicle, 1500.0, 0.7)
				WAIT(0)
			ENDWHILE
					
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1658.7899, -2334.7329, 99.7743 >>, << 4.5727, 0.0000, 54.0277 >>,45.0, TRUE)
			SET_CAM_PARAMS(cam_main, << 1652.5103, -2334.7329, 99.7743 >>, << 7.8744, 0.0000, 54.0277 >>,45.00, GET_SHOT_LENGTH(), GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)


			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK
		
		
		CASE 1
			IF TIMERB() > GET_SHOT_LENGTH()
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				i_current_event++
			ENDIF
		BREAK

		
	ENDSWITCH
	
	
ENDPROC

PROC stageOilFields2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(7, "Trailer", bodhi2, trailerVehicle, 1500.0, 0.7)
				WAIT(0)
			ENDWHILE
					
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1651.9153, -2333.6904, 101.5529 >>, << -3.5483, -0.0000, 51.0197 >>,45.0, TRUE)
			SET_CAM_PARAMS(cam_main, << 1646.2758, -2333.0046, 101.5529 >>, << -3.0094, -0.0000, 51.0196 >>,45.00, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)


			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK
		
		
		CASE 1
			IF TIMERB() > 8000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				i_current_event++
			ENDIF
		BREAK

		
	ENDSWITCH
	
	
ENDPROC

PROC stageOilFields3()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(7, "Trailer", bodhi2, trailerVehicle, 1500.0, 0.7)
				WAIT(0)
			ENDWHILE
					
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1624.0944, -1940.2352, 106.3006 >>, << -0.8295, -0.0000, 59.6128 >>,50.0, TRUE)
			SET_CAM_PARAMS(cam_main, << 1606.5898, -1940.2352, 106.3006 >>, << -0.8295, -0.0000, 59.6128 >>,50.00, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)


			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK
		
		
		CASE 1
			IF TIMERB() > 12000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				i_current_event++
			ENDIF
		BREAK

		
	ENDSWITCH
	
	
ENDPROC


PROC stageTheHills()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-680.3326, 1197.1796, 275.1175>>, <<22.1571, -0.2421, -128.9074>>, 42.2909, TRUE)
			SET_CAM_PARAMS(cam_main, <<-686.4666, 1186.2679, 297.2020>>, <<-4.9507, -0.2421, -164.3681>>, 42.2909, 10000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageCWalker1()
	
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-553.3742, 256.0414, 88.3968>>, <<-6.2444, 0.0000, 14.9367>>, 36.6027, TRUE)
			SET_CAM_PARAMS(cam_main, <<-537.7068, 265.0772, 86.5144>>, <<-10.2429, 0.0000, 60.0641>>, 36.6027, 8000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageCWalker2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1274.818359,-1097.051270,6.846625>>,<<6.104463,-0.000000,-156.820450>>,45.000000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1271.850464,-1102.984131,6.846625>>,<<6.104463,-0.000000,-156.820450>>,45.000000, 20000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 22000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageCWalker3()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-212.719574,-1508.091553,32.842598>>,<<1.057526,0.000000,76.324303>>,48.76889, TRUE)
			SET_CAM_PARAMS(cam_main, <<-210.409531,-1507.328247,32.738033>>,<<1.057526,0.000000,84.401230>>,48.76889, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 4000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
		
ENDPROC

PROC stageJewelHeist()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	SWITCH i_current_event
		CASE 0
		
			REQUEST_IPL("TRAILERSHOT_heist")
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL)
		
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
		
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
		
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-681.4552, -220.6665, 37.3730>>, <<22.6704, 0.0000, -113.9922>>, 45.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-654.6998, -230.9473, 39.0903>>, <<1.5750, 0.0000, -111.0818>>, 45.0000, 23400, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

//			set_ped_component_variation(PLAYER_PED_ID(), ped_comp_head, 6, 0)
//			set_ped_component_variation(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
//			set_ped_component_variation(PLAYER_PED_ID(), ped_comp_hand, 0, 0)
//			set_ped_component_variation(PLAYER_PED_ID(), ped_comp_torso, 1, 2)
//			set_ped_component_variation(PLAYER_PED_ID(), ped_comp_leg, 1, 1)
//			//set_ped_component_variation(pedBuddy1, ped_comp_special, 3, 0)	
//			set_ped_component_variation(PLAYER_PED_ID(), ped_comp_special, 9, 0)	
//			set_ped_component_variation(PLAYER_PED_ID(), PED_COMP_DECL, 4, 0)	
//			set_ped_component_variation(PLAYER_PED_ID(), PED_COMP_FEET, 1, 0)	

			
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 12, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 11, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 7, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 4, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 9, 0)
			
			//Accs_009, decl_004, feet_001, hair_000, hand_000, head_000, lowr_011, task_000, teef_000,
			//uppr_012 and p_head_006 is the cap.
		
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			
			REQUEST_CUTSCENE("JH_2b_mcs_1p1")
			i_current_event++
		BREAK
			
		CASE 2
		
	
			IF HAS_CUTSCENE_LOADED()
				i_current_event++
			ENDIF
		
		BREAK
		
		CASE 3
			
			START_CUTSCENE()
			i_current_event++
		BREAK
		
		CASE 4
			
			IF HAS_CUTSCENE_FINISHED()
			
			
			//IF TIMERB() > 21800
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			//ENDIF
			
			ENDIF
		BREAK
	ENDSWITCH
	
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 45.00, TRUe)

//	REQUEST_CUTSCENE("JH_2b_mcs_1p1")
//	
//	WHILE NOT HAS_CUTSCENE_LOADED()
//		WAIT(0)
//	ENDWHILE
//	
//	START_CUTSCENE()
//	
//	WHILE NOT HAS_CUTSCENE_FINISHED()
//		WAIT(0)
//	ENDWHILE


ENDPROC


PROC stageBeggars1()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			REQUEST_IPL("SP1_02_SHOT_beggar")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)	
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
						
			REQUEST_MODEL(A_M_O_Tramp_01)	
			//REQUEST_MODEL(A_F_M_Skidrow_01)
			REQUEST_MODEL(Prop_Beggers_Sign_01)
			REQUEST_ANIM_DICT("trailer@freeway_beggars")
			REQUEST_ANIM_DICT("trailer@joggers")
			
			WHILE NOT HAS_MODEL_LOADED(A_M_O_Tramp_01)
			//OR NOT HAS_MODEL_LOADED(A_F_M_Skidrow_01)
			OR NOT HAS_MODEL_LOADED(Prop_Beggers_Sign_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@joggers")
			OR NOT HAS_ANIM_DICT_LOADED("trailer@freeway_beggars")
				WAIT(0)
			ENDWHILE
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(3.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(3.0)
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 75.1202, -478.5743, 34.0588 >>, << 0.2371, -0.0042, -174.7053 >>,29.100000, TRUE)
			
			SET_CAM_PARAMS(cam_main, << 74.7051, -478.6117, 34.2945 >>, << -0.8300, -0.0042, -169.5340 >>,29.100000, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
//			scenePosition = << 75.000, -478.600, 33.000 >>
//			sceneRotation = << 0.000, 0.000, 120.960 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_Tramp_01, scenePosition, 0.0)
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@freeway_beggars", "b_001005_01_beggar(_a)(1355-1655)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			scenePosition = << 75.431, -481.348, 32.938 >>
			sceneRotation = << 0.000, 0.000, -138.600 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_Tramp_01, scenePosition, 0.0)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@freeway_beggars", "e_001006_01_beggar_alt1(_b)(900-1200)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


			/*scenePosition2 = << 80.368, -480.481, 32.916 >> //scenePosition2 = << 80.34, -479.99, 32.916 >>
			sceneRotation2 = << 0.000, 0.000, -34.200 >>
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition2, sceneRotation2)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_M_Skidrow_01, scenePosition2, 0.0)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId2, "trailer@freeway_beggars", "g_001006_01_beggar_alt1(_b)(2000-2300)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )*/

			oiTrailerProp = CREATE_OBJECT(Prop_Beggers_Sign_01, GET_PED_BONE_COORDS(trailerPeds[0], BONETAG_PH_R_HAND, <<0.0, 0.0, 0.0>>))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			

			//oiTrailerProp2 = CREATE_OBJECT(Prop_Beggers_Sign_01, GET_PED_BONE_COORDS(trailerPeds[1], BONETAG_PH_R_HAND, <<0.0, 0.0, 0.0>>))
			//ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[1], GET_PED_BONE_INDEX(trailerPeds[1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)

			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Beggers_Sign_01)
			
			//woman
			/*SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 1, 4, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)*/
			//guy
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)


			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.1
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					
					//IF NOT IS_PED_INJURED(trailerPeds[1])
					//	TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					//ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.25
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					
					//IF NOT IS_PED_INJURED(trailerPeds[1])
					//	TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					//ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.45
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					
					//IF NOT IS_PED_INJURED(trailerPeds[1])
					//	TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					//ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 4
			IF TIMERB() > 5000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
				DELETE_PED(trailerPeds[0])
				//DELETE_PED(trailerPeds[1])
				DELETE_OBJECT(oiTrailerProp)
				DELETE_OBJECT(oiTrailerProp2)
				REMOVE_ANIM_DICT("trailer@freeway_beggars")
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_O_Tramp_01)
				//SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_Skidrow_01)
				
				CLEAR_AREA_OF_VEHICLES(<<87.4716, -476.2936, 35.0342>>, 10.0)
				bDontClearAreaBetweenRepeatPlays = TRUE
			ENDIF
		BREAK
		
		
	ENDSWITCH

		//USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
		
		//IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
		//	PRINTLN(GET_SYNCHRONIZED_SCENE_PHASE(sceneId))
		//ENDIF

ENDPROC

PROC stageTaco1()

	SWITCH i_current_event
		CASE 0
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(TACO, trailerVehicle, << 974.6849, -1882.2277, 30.2634 >>, 82.7283)
				WAIT(0)
			ENDWHILE
			
			SET_VEHICLE_ON_GROUND_PROPERLY(trailerVehicle)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_RIGHT)
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<979.926453,-1886.232544,31.538656>>,<<6.014281,-0.000000,22.269024>>,45.000000, TRUE)
			
			WAIT(1000)
			
			SET_CAM_PARAMS(cam_main, <<978.522583,-1886.278564,31.538656>>,<<6.014282,0.000000,21.940672>>,45.000000, 4000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 4000				
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
			
	ENDSWITCH


ENDPROC
			

PROC stageTaco2()
	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(TACO, trailerVehicle, << 882.4876, -1745.5172, 28.8092 >>, 77.2686)
				WAIT(0)
			ENDWHILE
			
			REQUEST_MODEL( S_M_M_LineCook)
			REQUEST_MODEL(A_F_Y_BevHills_02)
			REQUEST_MODEL(A_M_M_Beach_01)
			REQUEST_MODEL(PROP_TACO_01)
			
			REQUEST_ANIM_DICT("trailer@taco_van")
				
			WHILE NOT HAS_MODEL_LOADED( S_M_M_LineCook)
			OR NOT HAS_MODEL_LOADED(A_M_M_Beach_01)
			OR NOT HAS_MODEL_LOADED(A_F_Y_BevHills_02)
			OR NOT HAS_MODEL_LOADED(PROP_TACO_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@taco_van")
				WAIT(0)
			ENDWHILE
			
		
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<879.158081,-1738.221924,29.706244>>,<<5.519522,0.000000,-126.051697>>,31.358673, TRUE)
			
			
			
			SET_VEHICLE_ON_GROUND_PROPERLY(trailerVehicle)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_RIGHT)
		
			FREEZE_ENTITY_POSITION(trailerVehicle, TRUE)
			SET_ENTITY_COLLISION(trailerVehicle, FALSE)
		
		
			/* START SYNCHRONIZED SCENE - trailer_taco_van_a.xml */
			//scenePosition = << 882.257, -1745.600, 29.865 >>
			sceneRotation = << 0.000, 0.000, 86.400 >>
			
			
			scenePosition = GET_ENTITY_COORDS(trailerVehicle)
			sceneRotation = GET_ENTITY_ROTATION(trailerVehicle)
		
		
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION,  S_M_M_LineCook, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Beach_01, scenePosition, 0.0)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_02, scenePosition, 0.0)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_02, scenePosition, 0.0)
			
			
			setPedVariation(trailerPeds[2], 1)
			setPedVariation(trailerPeds[3], 3)
			
			oiTrailerProp = CREATE_OBJECT(PROP_TACO_01, GET_ENTITY_COORDS(trailerPeds[0]))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		
			//scenePosition.z = scenePosition.z + 0.2
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, trailerVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehicle, "chassis"))
		
			
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_a_m", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_b_m", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_c_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_d_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

					
			SET_CAM_PARAMS(cam_main, <<879.158081,-1742.389771,29.706244>>,<<5.519523,0.000000,-126.051704>>,31.358673, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8000
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				DELETE_PED(trailerPeds[2])
				DELETE_PED(trailerPeds[3])
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageTaco3()
	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(TACO, trailerVehicle, << 882.4876, -1745.5172, 28.8092 >>, 77.2686)
				WAIT(0)
			ENDWHILE
			
			REQUEST_MODEL( S_M_M_LineCook)
			REQUEST_MODEL(A_F_Y_BevHills_02)
			REQUEST_MODEL(A_M_M_Beach_01)
			REQUEST_ANIM_DICT("trailer@taco_van")
				
			WHILE NOT HAS_MODEL_LOADED( S_M_M_LineCook)
			OR NOT HAS_MODEL_LOADED(A_M_M_Beach_01)
			OR NOT HAS_MODEL_LOADED(A_F_Y_BevHills_02)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@taco_van")
				WAIT(0)
			ENDWHILE

		
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 889.2681, -1741.8649, 30.1091 >>, << 1.3500, -0.0000, 115.0339 >>, 24.9111, TRUE)
			
			
			
			SET_VEHICLE_ON_GROUND_PROPERLY(trailerVehicle)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(trailerVehicle, SC_DOOR_REAR_RIGHT)
		
			FREEZE_ENTITY_POSITION(trailerVehicle, TRUE)
			SET_ENTITY_COLLISION(trailerVehicle, FALSE)
		
		
			/* START SYNCHRONIZED SCENE - trailer_taco_van_a.xml */
			//scenePosition = << 882.257, -1745.600, 29.865 >>
			sceneRotation = << 0.000, 0.000, 86.400 >>
			
			
			scenePosition = GET_ENTITY_COORDS(trailerVehicle)
			sceneRotation = GET_ENTITY_ROTATION(trailerVehicle)
		
		
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION,  S_M_M_LineCook, scenePosition, 0.0)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Beach_01, scenePosition, 0.0)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_02, scenePosition, 0.0)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_02, scenePosition, 0.0)
			
			oiTrailerProp = CREATE_OBJECT(PROP_TACO_01, GET_ENTITY_COORDS(trailerPeds[0]))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		
			//scenePosition.z = scenePosition.z + 0.2
			//sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, trailerVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehicle, "chassis"))
		
			
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_a_m", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_b_m", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_c_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId, "trailer@taco_van", "001010_01_gc_taco_van_2450_2790_d_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

					
			SET_CAM_PARAMS(cam_main, << 888.8848, -1740.9349, 30.1091 >>, << 1.3500, -0.0000, 115.0339 >>, 24.9111, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8000
				DELETE_PED(trailerPeds[0])
				DELETE_PED(trailerPeds[1])
				DELETE_PED(trailerPeds[2])
				DELETE_PED(trailerPeds[3])
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC stageTraffic()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<740.573669,-595.729736,37.494827>>,<<4.428050,-0.230196,95.399956>>,45.000000, TRUE)
			SET_CAM_PARAMS(cam_main, <<741.889587,-606.283508,37.494827>>,<<4.428050,-0.230196,95.399956>>,45.00000, 12000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_RANDOM_TRAINS(FALSE) 
			SETTIMERB(0)
			INSTANTLY_FILL_VEHICLE_POPULATION() 
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 16200
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
	
ENDPROC


PROC stageTraffic2()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(2.0)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<740.111450,-606.868469,36.020683>>,<<3.443049,-0.230196,68.476593>>,16.333324, TRUE)
			SET_CAM_PARAMS(cam_main, <<749.774475,-606.868469,36.020683>>,<<3.443049,-0.230196,69.461578>>,16.333324, 13900, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_RANDOM_TRAINS(FALSE) 
			SETTIMERB(0)
			INSTANTLY_FILL_VEHICLE_POPULATION() 
			i_current_event++
		BREAK

		CASE 1
		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 740.1115, -606.8685, 35.3375 >>)
		
			IF TIMERB() > 16200
				//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
	
ENDPROC

PROC stageTraffic3()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1062.758423,-1355.920410,30.064930>>,<<-7.904456,-0.230196,0.908152>>,25.552528, TRUE)
			SET_CAM_PARAMS(cam_main, <<1062.603027,-1353.297852,58.513630>>,<<-4.163051,-0.230196,0.908147>>,25.552528, 20000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_RANDOM_TRAINS(FALSE) 
			SETTIMERB(0)
			INSTANTLY_FILL_VEHICLE_POPULATION() 
			i_current_event++
		BREAK

		CASE 1
		
			IF TIMERA() > 2000
			
				SWITCH iRepositionPlayer
				
					CASE 0
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1059.0529, -1292.0365, 25.1656 >> )
						
						iRepositionPlayer++
					BREAK
				
					CASE 1
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1055.6957, -1224.7317, 35.5315 >> )
						iRepositionPlayer++
					BREAK
					
					CASE 2
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1060.2885, -1221.7279, 45.3204 >> )
						iRepositionPlayer++
					BREAK
				
					CASE 3	
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1058.5249, -1184.6841, 54.8239 >> )
						iRepositionPlayer = 0
					BREAK
					
				ENDSWITCH
				SETTIMERA(0)
			ENDIF
		
			//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.0, 255,0,0, 128)
		
			IF TIMERB() > 20000
				//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
	
ENDPROC


PROC stageBikes()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<2073.787354,3064.329590,45.535412>>,<<3.025856,-0.225199,68.213318>>,18.149513, TRUE)
			
			REQUEST_ANIM_DICT("trailer@bike_riders")	
			REQUEST_MODEL(A_M_Y_Cyclist_01)
						
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(9, "Trailer", SCORCHER, trailerVehicle, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(10, "Trailer", SCORCHER, trailerVehicle2, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
				
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Cyclist_01)
				WAIT(0)
			ENDWHILE			
			
			CLEAR_AREA(<<2063.921875,3060.268555,47.048405>>, 40.00, TRUE)
			
			//Area at end of shot...
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			//Area near bikes
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			//Start
			CLEAR_AREA(<<2038.00,3076.25,46.048405>>, 10.00, TRUE)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
				ATTACH_ENTITY_TO_ENTITY(trailerPeds[0], trailerVehicle, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle2)
				trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
				ATTACH_ENTITY_TO_ENTITY(trailerPeds[1], trailerVehicle2, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF		
			//SET_ENTITY_COLLISION(trailerPeds[0], FALSE)
			
			//TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			//TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			
			//The anim can be found in this dictionary:  TRAILER@BIKE_RIDERSand the anim is called:     001022_1Bike3(1515_1591)_BIKE
			PLAY_ENTITY_ANIM(trailerVehicle, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			PLAY_ENTITY_ANIM(trailerVehicle2, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2)
			
			//CLEAR_AREA_OF_VEHICLES(<<2063.921875,3060.268555,47.048405>>, 30.00)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 3000
				SET_CAM_PARAMS(cam_main, <<2073.787354,3064.329590,45.535412>>,<<3.025856,-0.225200,149.346817>>,18.149513, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				
				i_current_event++
			ENDIF
		BREAK

		CASE 2
			IF TIMERB() > 13000
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageBikes2()
	
	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	SWITCH i_current_event
		CASE 0
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			//DESTROY_ALL_CAMS()
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<2051.0, 3080.0, 46.96>>,<<3.23,0.78, 65.64>>, 25.5, TRUE)
			
			REQUEST_ANIM_DICT("trailer@bike_riders")	
			REQUEST_MODEL(A_M_Y_Cyclist_01)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(9, "Trailer", SCORCHER, trailerVehicle, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(10, "Trailer", SCORCHER, trailerVehicle2, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
				
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Cyclist_01)
				WAIT(0)
			ENDWHILE			
			
			CLEAR_AREA(<<2063.921875,3060.268555,47.048405>>, 40.00, TRUE)
			
			
			//Area at end of shot...
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			
			
			//Area near bikes
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			
			//Start
			CLEAR_AREA(<<2038.00,3076.25,46.048405>>, 10.00, TRUE)
			
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				ATTACH_ENTITY_TO_ENTITY(trailerPeds[0], trailerVehicle, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			IF NOT IS_ENTITY_DEAD(trailerVehicle2)
				ATTACH_ENTITY_TO_ENTITY(trailerPeds[1], trailerVehicle2, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF		
			//SET_ENTITY_COLLISION(trailerPeds[0], FALSE)
			
			//TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			//TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
				
			//The anim can be found in this dictionary:  TRAILER@BIKE_RIDERSand the anim is called:     001022_1Bike3(1515_1591)_BIKE
			PLAY_ENTITY_ANIM(trailerVehicle, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			PLAY_ENTITY_ANIM(trailerVehicle2, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2)
			
			//CLEAR_AREA_OF_VEHICLES(<<2063.921875,3060.268555,47.048405>>, 30.00)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 750
				SET_CAM_PARAMS(cam_main, <<2051.0, 3080.0, 46.96>>,<<3.23,0.78, 144.9>>, 25.0, 6500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				
				i_current_event++
			ENDIF
		BREAK

		CASE 2
			IF TIMERB() > 7000
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageBikes3()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)
	
		SWITCH i_current_event
		CASE 0
		
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
		
			//DESTROY_ALL_CAMS()
			

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<2048.26, 3061.63, 46.96>>,<<4.85, 0.78, 39.18>>, 25.5, TRUE)
			
			REQUEST_ANIM_DICT("trailer@bike_riders")
			REQUEST_MODEL(A_M_Y_Cyclist_01)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(9, "Trailer", SCORCHER, trailerVehicle, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(10, "Trailer", SCORCHER, trailerVehicle2, 5000.0, 0.8)
				WAIT(0)
			ENDWHILE
				
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Cyclist_01)
				WAIT(0)
			ENDWHILE			
			
			CLEAR_AREA(<<2063.921875,3060.268555,47.048405>>, 40.00, TRUE)
			
			
			//Area at end of shot...
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			
			
			//Area near bikes
			CLEAR_AREA(<<2041.00,3014.25,45.048405>>, 15.00, TRUE)
			
			//Start
			CLEAR_AREA(<<2038.00,3076.25,46.048405>>, 10.00, TRUE)
			
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			ATTACH_ENTITY_TO_ENTITY(trailerPeds[0], trailerVehicle, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Cyclist_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			ATTACH_ENTITY_TO_ENTITY(trailerPeds[1], trailerVehicle2, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
			//SET_ENTITY_COLLISION(trailerPeds[0], FALSE)
			
			//TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			//TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_01_gc_bike_riders_3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[0], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			TASK_PLAY_ANIM(trailerPeds[1], "trailer@bike_riders", "001022_1Bike3(1515_1591)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
				
			//The anim can be found in this dictionary:  TRAILER@BIKE_RIDERSand the anim is called:     001022_1Bike3(1515_1591)_BIKE
			PLAY_ENTITY_ANIM(trailerVehicle, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			PLAY_ENTITY_ANIM(trailerVehicle2, "001022_1Bike3(1515_1591)_BIKE", "TRAILER@BIKE_RIDERS", INSTANT_BLEND_IN, TRUE, FALSE)
			
			
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2)
			
			//CLEAR_AREA_OF_VEHICLES(<<2063.921875,3060.268555,47.048405>>, 30.00)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 4000
				SET_CAM_PARAMS(cam_main, <<2073.787354,3064.329590,45.535412>>,<<3.025856,-0.225200,149.346817>>,18.149513, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				i_current_event++
			ENDIF
		BREAK

		CASE 2
			IF TIMERB() > 13000
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageJogBoardwalk4()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			
			REQUEST_MODEL(A_F_Y_Runner_01)
			REQUEST_MODEL(A_M_Y_Runner_01)
			
			REQUEST_MODEL(A_M_Y_Hipster_01)
			REQUEST_MODEL(A_F_Y_Hipster_03)
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			
			REQUEST_IPL("TRAILERSHOT_joggers")
			
			REQUEST_ANIM_DICT("trailer@joggers")
			
			LOAD_CLOUD_HAT("wispy")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			SET_WIND_SPEED(1.0)
			SET_WIND_DIRECTION(3.14)
			
			SET_MAPDATACULLBOX_ENABLED("TrailerJoggers", TRUE) 
			bJoggersCullEnabled = TRUE
			
			/*//SET_VEHICLE_DENSITY_MULTIPLIER(2.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(2.0)
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF*/
			
			//Fix for banner wind getting cleared: do a clear before the waits later on, and don't use the global clear that happens after the waits.
			bDontClearAreaBetweenRepeatPlays = TRUE
			CLEAR_AREA(<<-1709.450806,-1069.320557,12.624747>>, 20.0, TRUE)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1709.450806,-1069.320557,12.624747>>,<<2.418234,0.221708,-26.113852>>,24.951670, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_F_Y_Runner_01)
			OR NOT HAS_MODEL_LOADED(A_M_Y_Runner_01)
			
			OR NOT HAS_ANIM_DICT_LOADED("trailer@joggers")
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerJog", EMPEROR2, trailerVehicles[0], 9000)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(2, "TrailerJog", ASTEROPE, trailerVehicles[1], 3000)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(3, "TrailerJog", PACKER, trailerVehicles[2], 4000)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(4, "TrailerJog", BALLER, trailerVehicles[3], 4000)
				WAIT(0)
			ENDWHILE
			
			trailerPeds[4] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Hipster_01, << -1689.6435, -1047.0630, 12.0175 >>, 146.7813)
			trailerPeds[5] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Hipster_03, << -1690.8296, -1046.9320, 12.0175 >>, 132.3982)

			SET_PED_NAME_DEBUG(trailerPeds[4], "background1")
			SET_PED_NAME_DEBUG(trailerPeds[5], "background2")
			
			TASK_FOLLOW_NAV_MESH_TO_COORD(trailerPeds[4], << -1704.6565, -1065.1899, 12.0174 >>, PEDMOVE_WALK)
			TASK_FOLLOW_NAV_MESH_TO_COORD(trailerPeds[5], << -1705.4664, -1064.1115, 12.0174 >>, PEDMOVE_WALK)
		
			TASK_LOOK_AT_ENTITY(trailerPeds[4], trailerPeds[5], INFINITE_TASK_TIME)
			TASK_LOOK_AT_ENTITY(trailerPeds[5], trailerPeds[4], INFINITE_TASK_TIME)
		
			WAIT(1500) //Allow background to start walking
					
			
			WHILE NOT HAS_MODEL_LOADED(A_F_Y_Runner_01)
			OR NOT HAS_MODEL_LOADED(A_M_Y_Runner_01)
			
			OR NOT HAS_ANIM_DICT_LOADED("trailer@joggers")
				WAIT(0)
			ENDWHILE
			
//			scenePosition = << -1705.000, -1064.650, 12.000 >>
//			sceneRotation = << 0.000, 0.000, -36.000 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Runner_01, scenePosition)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Runner_01, scenePosition)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Runner_01, scenePosition)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Runner_01, scenePosition)
			
			
				
			
			SET_PED_NAME_DEBUG(trailerPeds[0], "trailerPeds[0]")
			SET_PED_NAME_DEBUG(trailerPeds[1], "trailerPeds[1]")
			SET_PED_NAME_DEBUG(trailerPeds[2], "trailerPeds[2]")
			SET_PED_NAME_DEBUG(trailerPeds[3], "trailerPeds[3]")
			

			
//			setPedVariation(trailerPeds[0], 0)
//			
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_LEG, 1, 0)
			
			
			//Left most girl
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)

			SET_PED_PROP_INDEX(trailerPeds[3], ANCHOR_EYES, 0, 1)

			//Could she also be wearing sunglasses? prop 0, texture b.

			//middle girl
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 2), 0, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 8), 0, 2, 0) //(accs)

			//middle guy
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)



			//rightmost guy
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 1, 5, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 1, 5, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)




			
			scenePosition = << -1706.334, -1066.874, 12.017 >>
			sceneRotation = << 0.000, 0.000, -37.080 >>
			
			trailerParticles[0] =  START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_jogging_litter", << -1707.5134, -1064.7878, 12.2287 >>, sceneRotation)
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@joggers", "m_a001013_01_gc_people_jogging(a+b+c+d)(960-1150)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@joggers", "m_b001013_01_gc_people_jogging(a+b+c+d)(960-1150)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@joggers", "f_a001013_01_gc_people_jogging(a+b+c+d)(960-1150)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId, "trailer@joggers", "f_b001013_01_gc_people_jogging(a+b+c+d)(960-1150)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.22
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.42
					IF NOT IS_PED_INJURED(trailerPeds[1])
						TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.532
					SET_CAM_PARAMS(cam_main, <<-1709.855957,-1070.218994,12.618631>>,<<1.425321,0.189391,-21.269804>>,18.220839, 3000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)
					i_current_event++
				ENDIF
			ENDIF		
		BREAK

		
		CASE 4
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.56
					IF NOT IS_PED_INJURED(trailerPeds[2])
						TASK_PLAY_ANIM(trailerPeds[2], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.65
					IF NOT IS_PED_INJURED(trailerPeds[3])
						TASK_PLAY_ANIM(trailerPeds[3], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.74
					IF NOT IS_PED_INJURED(trailerPeds[2])
						TASK_PLAY_ANIM(trailerPeds[2], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 7
			IF TIMERB() > 8000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				REMOVE_ANIM_DICT("trailer@joggers")
	
			ENDIF
		BREAK
	ENDSWITCH	

ENDPROC

PROC stageJogVenice()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1251.2162, -1467.8400, 4.4706>>, <<3.4916, 0.0000, 23.3113>>, 45.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1237.2130, -1489.9825, 4.2852>>, <<3.4916, 0.0000, 23.3113>>, 45.0000, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

//PROC stageHiking1()
//
//	SWITCH i_current_event
//		CASE 0
//			//DESTROY_ALL_CAMS()
//			
//			REQUEST_MODEL(A_F_Y_HIKER_01)
//			REQUEST_MODEL(A_M_Y_Hiker_01)
//			REQUEST_ANIM_DICT("trailer@hikers")
//			
//			REQUEST_IPL("TRAILERSHOT_hikers")
//						
//			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-792.32,2863.14,19.93>>,<<-4.99,-0.08,167.52>>,38.50, TRUE)			
//			
//			WHILE NOT HAS_MODEL_LOADED(A_F_Y_HIKER_01)
//			OR NOT HAS_MODEL_LOADED(A_M_Y_Hiker_01)
//			OR NOT HAS_ANIM_DICT_LOADED("trailer@hikers")
//				WAIT(0)
//			ENDWHILE
//			
//			scenePosition = << -796.697, 2856.530, 15.508 >>
//			sceneRotation = << 0.000, 0.000, 0.000 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			
//			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Hiker_01, scenePosition)
//			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
//			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
//			
//			SET_PED_NAME_DEBUG(trailerPeds[0], "trailerPeds[0]")
//			SET_PED_NAME_DEBUG(trailerPeds[2], "trailerPeds[2]")
//			SET_PED_NAME_DEBUG(trailerPeds[1], "trailerPeds[1]")
//						
//			//Black girl
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 1, 0, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 5, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 8), 3, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 10), 1, 1, 0) //(decl)
//
//			
////			“Accs_005” (006 if with hoody) with texture “a” for the girl on her own.
////			“Accs_003” (004 if with hoody) with texture “a” for the girl in the couple.
////			“Accs_000” with texture “c” for the guy.
//
//			
//			//Black guy
//			//Loading ped model: A_M_Y_Hiker_01
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
//
//			//Guy in the middle:
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 2, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 1, 3, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 8), 0, 2, 0) //(accs)
//
//			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 1)
//
//			//white girl
//			//Loading ped model: A_F_Y_Hiker_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,8), 5, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
//			
//			
////			SET_PED_COMPONENT_VARIATION(trailerPeds[1], PED_COMP_TORSO, 0, 0)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[2], PED_COMP_TORSO, 0, 1)
//			
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@hikers", "m_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@hikers", "f_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@hikers", "f_b001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			
//			SET_CAM_PARAMS(cam_main, <<-791.06, 2861.77, 19.28>>,<<-0.55, -0.08, 154.11>>,38.50, 5500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//
//				
//
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
//			SETTIMERB(0)
//			i_current_event++
//		BREAK
//
//		CASE 1
//			IF TIMERB() > 5500	
//			
////				IF NOT IS_PED_INJURED(trailerPeds[2])
////					SET_PED_COMPONENT_VARIATION(trailerPeds[2], PED_COMP_SPECIAL, iDrawable, iTexture)
////				ENDIF	
//			
//				//DESTROY_ALL_CAMS()
//			
//				SET_CAM_FOV(cam_main, 70.0)
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//ENDPROC


//PROC stageHikingNew()
//
//	SWITCH i_current_event
//		CASE 0
//			//DESTROY_ALL_CAMS()
//			
//			REQUEST_MODEL(A_F_Y_HIKER_01)
//			REQUEST_MODEL(A_M_Y_Hiker_01)
//			REQUEST_ANIM_DICT("trailer@hikers")
//			
//			REQUEST_IPL("TRAILERSHOT_hikers")
//						
//			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-570.42, 5997.86,30.31>>,<<8.36,0.0,-176.14>>,50.00, TRUE)			
//			
//			WHILE NOT HAS_MODEL_LOADED(A_F_Y_HIKER_01)
//			OR NOT HAS_MODEL_LOADED(A_M_Y_Hiker_01)
//			OR NOT HAS_ANIM_DICT_LOADED("trailer@hikers")
//				WAIT(0)
//			ENDWHILE
//			
//			/* START SYNCHRONIZED SCENE - trailer_hikers_uphill_c.xml */
//			scenePosition = << -571.850, 5994.150, 28.100 >>
//			sceneRotation = << 0.000, 0.000, -61.200 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			
//			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Hiker_01, scenePosition)
//			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
//			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
//			
//			SET_PED_NAME_DEBUG(trailerPeds[0], "trailerPeds[0]")
//			SET_PED_NAME_DEBUG(trailerPeds[2], "trailerPeds[2]")
//			SET_PED_NAME_DEBUG(trailerPeds[1], "trailerPeds[1]")
//					
//			//Black girl
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 1, 0, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 5, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 8), 3, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 10), 1, 1, 0) //(decl)
//
//			
////			“Accs_005” (006 if with hoody) with texture “a” for the girl on her own.
////			“Accs_003” (004 if with hoody) with texture “a” for the girl in the couple.
////			“Accs_000” with texture “c” for the guy.
//
//			
//			//Black guy
//			//Loading ped model: A_M_Y_Hiker_01
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
////			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
//
//			//Guy in the middle:
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 2, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 1, 3, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 8), 0, 2, 0) //(accs)
//
//			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 1)
//
//			//white girl
//			//Loading ped model: A_F_Y_Hiker_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,8), 5, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
//			
//			
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@hikers", "m_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@hikers", "f_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@hikers", "f_b001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			
//			SET_CAM_PARAMS(cam_main, <<-570.253723,5995.869629,30.609308>>,<<8.524121,-0.000000,-172.199951>>,50.000000, 5500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//
//				
//
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
//			SETTIMERB(0)
//			i_current_event++
//		BREAK
//
//		CASE 1
//			IF TIMERB() > 5500	
//			
////				IF NOT IS_PED_INJURED(trailerPeds[2])
////					SET_PED_COMPONENT_VARIATION(trailerPeds[2], PED_COMP_SPECIAL, iDrawable, iTexture)
////				ENDIF	
//			
//				//DESTROY_ALL_CAMS()
//			
//				SET_CAM_FOV(cam_main, 70.0)
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//ENDPROC

PROC stageHikingNew2()


	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_F_Y_HIKER_01)
			REQUEST_MODEL(A_M_Y_Hiker_01)
			REQUEST_ANIM_DICT("trailer@hikers")
			
			REQUEST_IPL("TRAILERSHOT_hikers")
			
			/*CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1821.266724,6403.093750,40.757168,0.558219)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1825.913330,6378.907715,39.313503,1.819750)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1671.534302,6249.715820,103.445595,1.963469)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1683.010498,6260.103516,75.723198,1.000000)*/
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(42.0)
			
			LOAD_CLOUD_HAT("cloudy 01")
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
									
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<1822.820557,6405.271484,40.836533>>,<<21.893269,-0.000001,148.796082>>,41.106232, TRUE)			
			
			WHILE NOT HAS_MODEL_LOADED(A_F_Y_HIKER_01)
			OR NOT HAS_MODEL_LOADED(A_M_Y_Hiker_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@hikers")
				WAIT(0)
			ENDWHILE
			
			/* START SYNCHRONIZED SCENE - trailer_hikers_uphill_c.xml */
			scenePosition = << 1822.310, 6399.530, 39.438 >>
			sceneRotation = << 0.000, 0.000, 156.750 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Hiker_01, scenePosition)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_HIKER_01, scenePosition)
			
			SET_PED_NAME_DEBUG(trailerPeds[0], "trailerPeds[0]")
			SET_PED_NAME_DEBUG(trailerPeds[2], "trailerPeds[2]")
			SET_PED_NAME_DEBUG(trailerPeds[1], "trailerPeds[1]")
						
			//Girl on right
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,8), 3, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)

			SET_PED_PROP_INDEX(trailerPeds[1], ANCHOR_EYES, 0)
			
//			“Accs_005” (006 if with hoody) with texture “a” for the girl on her own.
//			“Accs_003” (004 if with hoody) with texture “a” for the girl in the couple.
//			“Accs_000” with texture “c” for the guy.

			
			//Black guy
			//Loading ped model: A_M_Y_Hiker_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)

			//Guy in the middle:
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)

			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 1)

			//white girl
			//Loading ped model: A_F_Y_Hiker_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,8), 5, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
			
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@hikers", "m_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@hikers", "f_a001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@hikers", "f_b001008_01_gc_hikers_up_down_hill(a+b+c)(3538-3709)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			
			SET_CAM_PARAMS(cam_main, <<1821.407593,6403.430176,41.342972>>,<<25.291790,0.000000,151.830841>>,37.700008, 5500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

				

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 5500	
			
//				IF NOT IS_PED_INJURED(trailerPeds[2])
//					SET_PED_COMPONENT_VARIATION(trailerPeds[2], PED_COMP_SPECIAL, iDrawable, iTexture)
//				ENDIF	
			
				//DESTROY_ALL_CAMS()
			
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC stageCombine1()

	SWITCH i_current_event
		CASE 0
			
			REQUEST_MODEL(A_M_M_Farmer_01 )
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "Trailer", tractor, trailerVehicle, 1000.0, 0.6)
				WAIT(0)
			ENDWHILE
					
			WHILE NOT HAS_MODEL_LOADED(A_M_M_Farmer_01)
				WAIT(0)
			ENDWHILE
					
			trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_M_Farmer_01)
			
			
			trailerParticles[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_fric_bushes_trailer", trailerVehicle, vParticlePosition[0], vParticleRotation[0])
			trailerParticles[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_fric_bushes_trailer", trailerVehicle, vParticlePosition[1], vParticleRotation[1])
			trailerParticles[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_fric_bushes_trailer", trailerVehicle, vParticlePosition[2], vParticleRotation[2])
			trailerParticles[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_fric_bushes_trailer", trailerVehicle, vParticlePosition[3], vParticleRotation[3])
			
			trailerParticles[4] = START_PARTICLE_FX_LOOPED_ON_ENTITY("veh_combine_churn", trailerVehicle, vParticlePosition[2], vParticleRotation[2])
			trailerParticles[5] = START_PARTICLE_FX_LOOPED_ON_ENTITY("veh_combine_grain_arm", trailerVehicle, vParticlePosition[3], vParticleRotation[3])

			
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1140.026978,1993.093506,58.605759>>,<<4.899950,0.000980,-143.870026>>,45.000000, TRUE)
			SET_CAM_PARAMS(cam_main,<<1139.947998,1993.200806,60.154064>>,<<4.899950,0.000980,-143.870026>>,45.000000, 15000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.0500)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
		
			SET_PARTICLE_FX_LOOPED_OFFSETS(trailerParticles[0], vParticlePosition[0], vParticleRotation[0])
			SET_PARTICLE_FX_LOOPED_OFFSETS(trailerParticles[1], vParticlePosition[1], vParticleRotation[1])
			SET_PARTICLE_FX_LOOPED_OFFSETS(trailerParticles[2], vParticlePosition[2], vParticleRotation[2])
			SET_PARTICLE_FX_LOOPED_OFFSETS(trailerParticles[3], vParticlePosition[3], vParticleRotation[3])
		
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, vParticlePosition[0]), 0.2)
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, vParticlePosition[1]), 0.2)
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, vParticlePosition[2]), 0.2)
				DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, vParticlePosition[3]), 0.2)
			ENDIF
			
			IF TIMERB() > 15000
				
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				
				CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle)
			
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageCombine2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(2, "Trailer", tractor, trailerVehicle, 3000.0, 0.8)
				WAIT(0)
			ENDWHILE
		
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1971.8175, 4975.0444, 42.1265>>, <<8.9552, 0.0000, 54.6036>>, 45.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<1971.8407, 4986.6738, 41.8382>>, <<7.9528, -0.0000, 66.6060>>, 45.0000, 20000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 20000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle)
			
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


PROC stageCombine3()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(3, "Trailer", tractor, trailerVehicle, 0.0, 0.6)
				WAIT(0)
			ENDWHILE
					
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<752.4945, 6503.6138, 28.3150>>, <<1.2508, 0.0000, 113.2321>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<727.0432, 6507.7036, 28.3150>>, <<1.2508, 0.0000, 113.2321>>, 60.0000, 20000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 20000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageDuster1()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
				
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(5, "Trailer", DUSTER, trailerVehicle, 8000.0, 0.6)
				WAIT(0)
			ENDWHILE
		
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1862.2942, 1921.6179, 146.2380>>, <<6.3764, 0.0000, -56.1506>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1862.2942, 1921.6179, 146.2380>>, <<-0.5364, 0.0000, 15.0069>>, 60.0000, 4000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2000)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 9000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageDuster2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
				
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(5, "Trailer", DUSTER, trailerVehicle, 8000.0, 0.6)
				WAIT(0)
			ENDWHILE
		
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1848.821045,1965.164917,137.710815>>,<<10.825972,-1.031207,-91.320831>>,45.000000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1848.821045,1965.164917,137.710815>>,<<64.831757,-1.031207,-91.320831>>,45.000000, 4500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2000)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 9000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
	RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

PROC DO_BIRDS()

STRING str_bird_anims = "creatures@gull@move"

IF NOT DOES_ENTITY_EXIST(s_birds[0].ped)

    REQUEST_MODEL(model_bird)
    REQUEST_ANIM_DICT(str_bird_anims)
    
    IF HAS_MODEL_LOADED(model_bird)
    AND HAS_ANIM_DICT_LOADED(str_bird_anims)
          s_birds[0].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1781.5833, 2168.6079, 113.8400 >> + <<0.0, 0.0, -0.7>>, 278.8218)
          s_birds[1].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1780.3245, 2169.2310, 113.8637 >> + <<0.0, 0.0, -0.7>>, 322.6699)
          s_birds[2].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1778.5002, 2169.8376, 114.0258 >> + <<0.0, 0.0, -0.7>>, 32.0168)
          s_birds[3].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1776.0347, 2169.7070, 114.6546 >> + <<0.0, 0.0, -0.7>>, 320.6784)
          s_birds[4].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1774.8462, 2169.5710, 114.8727 >> + <<0.0, 0.0, -0.7>>, 272.0369)
          s_birds[5].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1773.6199, 2170.1023, 114.8030 >> + <<0.0, 0.0, -0.7>>, 152.0615)
          s_birds[6].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1771.0143, 2169.8828, 115.2609 >> + <<0.0, 0.0, -0.7>>, 352.3865)
          s_birds[7].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1772.4401, 2170.2888, 114.8754 >> + <<0.0, 0.0, -0.7>>, 304.2578)
          s_birds[8].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -1769.2491, 2169.9517, 115.5001 >> + <<0.0, 0.0, -0.7>>, 25.9012)
          SET_MODEL_AS_NO_LONGER_NEEDED(model_bird)
          
		  INT i
		  
          REPEAT COUNT_OF(s_birds) i
                TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_birds[i].ped, TRUE)
                SET_PED_CAN_RAGDOLL(s_birds[i].ped, FALSE)
                SET_ENTITY_INVINCIBLE(s_birds[i].ped, TRUE)
                FREEZE_ENTITY_POSITION(s_birds[i].ped, TRUE)
                SET_ENTITY_COLLISION(s_birds[i].ped, FALSE)
				SET_ENTITY_LOD_DIST(s_birds[i].ped, 2000)
                s_birds[i].i_event = 0
          ENDREPEAT
    ENDIF
     
ELSE
	INT i
	REPEAT COUNT_OF(s_birds) i
	    IF NOT IS_PED_INJURED(s_birds[i].ped)
	        IF s_birds[i].i_event = 0
	            //IF VDIST2(GET_ENT, GET_ENTITY_COORDS(s_birds[i].ped)) < 225.0
	                    FREEZE_ENTITY_POSITION(s_birds[i].ped, FALSE)
	                    SET_ENTITY_COLLISION(s_birds[i].ped, TRUE)
	                    VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(s_birds[i].ped) + <<45.0, 0.0, 0.0>>)
	                    SET_ENTITY_VELOCITY(s_birds[i].ped, v_dir * 7.0)
	                
	                      //OPEN_SEQUENCE_TASK(seq)
	                      //    TASK_PLAY_ANIM(NULL, str_bird_anims, "takeoff", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
	                        TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "flapping", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_IGNORE_GRAVITY)
	                      //CLOSE_SEQUENCE_TASK(seq)
	                      
	                      //TASK_PERFORM_SEQUENCE(s_birds[i].ped, seq)
	                      //CLEAR_SEQUENCE_TASK(seq)
	                      
	                    s_birds[i].i_event++
	            //ENDIF
	        ELIF s_birds[i].i_event = 1
	            IF IS_ENTITY_PLAYING_ANIM(s_birds[i].ped, str_bird_anims, "flapping")
	                SET_ENTITY_ANIM_SPEED(s_birds[i].ped, str_bird_anims, "flapping", 4.0)
	        	ENDIF
	                
	           	VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(s_birds[i].ped) + <<45.0, 0.0, 0.0>>)
	            SET_ENTITY_VELOCITY(s_birds[i].ped, v_dir * 7.0)
	        ENDIF
	          
//          IF f_current_playback_time > 82000.0
//          OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//	                REMOVE_PED(s_birds[i].ped)
//	          ENDIF
	    ENDIF
	ENDREPEAT
ENDIF


ENDPROC


PROC stageDuster3()

	IF i_current_event > 1
		DO_BIRDS()
	ENDIF

	
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_PTFX_ASSET()
		
			REQUEST_MODEL(S_F_Y_MIGRANT_01)
			REQUEST_MODEL(S_M_M_MIGRANT_01)
			REQUEST_ANIM_DICT("trailer@crop_duster")
			
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
		
		
			WHILE NOT HAS_MODEL_LOADED(S_F_Y_MIGRANT_01)
			OR NOT HAS_MODEL_LOADED(S_M_M_MIGRANT_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@crop_duster")
				WAIT(0)
			ENDWHILE
			
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(8, "Trailer", DUSTER, trailerVehicle, 9000.0, 0.6)
				WAIT(0)
			ENDWHILE	
			
			trailerPeds[8] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_F_Y_MIGRANT_01)
			
			SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
			SET_HELI_BLADES_FULL_SPEED(trailerVehicle)
			
			//Timecycle = CLOUDY (urban) @ 18:00extra lights IPL group = TRAILERSHOT_cropdload cloudhat: puffs
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			
			REQUEST_IPL("TRAILERSHOT_cropd")
			
			LOAD_CLOUD_HAT("puffs")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1770.792969,2174.963379,116.961281>>,<<17.618620,-0.186535,-130.985214>>,50.082756, TRUE)
					
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[4] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[5] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[6] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			trailerPeds[7] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition)
			
				
			FOR iterator = 0 TO 7
				debugPedName = "TRPed "
				debugPedName += iterator
				SET_PED_NAME_DEBUG(trailerPeds[iterator], debugPedName)
			ENDFOR	
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 2, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			
			//Red checked shirt
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			
			//another blue tshirt
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
			
			
//			Blue tshirt guy, white baseball hat (1 - a)
//			light shirt guy, straw hat (0 - a)
//			red checked shirt guy - white baseball hat (1 - a)
//			blue tshirt guy - beige baseball hat (1 - c)
			
			SET_PED_PROP_INDEX(trailerPeds[2], ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(trailerPeds[1], ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(trailerPeds[3], ANCHOR_HEAD, 1, 2)

			
			//I dont think you see the guys in the back row, but anyway, left to right
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			
			//with a white baseball hat (1 - a)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			
			//with a beige baseball hat (1 - c)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			
			//He should have no hat or bandana (heat stroke will kill him)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT,3), 2, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			
			//white baseball hat (1 - a)
			SET_PED_PROP_INDEX(trailerPeds[4], ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(trailerPeds[7], ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(trailerPeds[5], ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(trailerPeds[6], ANCHOR_HEAD, 1)
			
			/* START SYNCHRONIZED SCENE - trailer_crop_duster_d.xml */
			scenePosition = << -1766.492, 2170.214, 116.462 >>
			 sceneRotation = << 0.000, 0.000, 86.760 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@crop_duster", "m_c001019_01_gc_crop_duster_2(a+b+c+d)(1350-1850)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@crop_duster", "m_d001019_01_gc_crop_duster_2(a+b+c+d)(1350-1850)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@crop_duster", "m_a001019_01_gc_crop_duster_2(a+b+c+d)(1350-1850)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId, "trailer@crop_duster", "m_b001019_01_gc_crop_duster_2(a+b+c+d)(1350-1850)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			
			/* START SYNCHRONIZED SCENE - trailer_crop_duster_a.xml */
			scenePosition2 = << -1760.126, 2165.666, 118.405 >>
			sceneRotation2 = << 0.000, 0.000, 91.800 >>
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition2, sceneRotation2)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[4], sceneId2, "trailer@crop_duster", "m_c001018_01_gc_crop_duster_1(a+b+c+d)(350-650)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[5], sceneId2, "trailer@crop_duster", "m_d001018_01_gc_crop_duster_1(a+b+c+d)(350-650)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[6], sceneId2, "trailer@crop_duster", "m_a001018_01_gc_crop_duster_1(a+b+c+d)(350-650)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[7], sceneId2, "trailer@crop_duster", "m_b001018_01_gc_crop_duster_1(a+b+c+d)(350-650)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )




//						
//			/* START SYNCHRONIZED SCENE - trailer_crop_duster_c.xml */
//
//		
//			scenePosition3 = << -1770.750, 2170.450, 115.700 >>
//			sceneRotation3 = << -7.000, -0.000, 86.760 >>
//
//			sceneId3 = CREATE_SYNCHRONIZED_SCENE(scenePosition3, sceneRotation3)
//
//			trailerPeds[4] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition3)
//			trailerPeds[5] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MIGRANT_01, scenePosition3)
//
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[4], sceneId3, "trailer@crop_duster", "f_a001018_01_gc_crop_duster_1(a+b+c+d)(3750-4300)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//
//
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[5], sceneId3, "trailer@crop_duster", "f_b001018_01_gc_crop_duster_1(a+b+c+d)(3750-4300)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//
//
//			//TASK_SYNCHRONIZED_SCENE (trailerPeds[4], sceneId3, "trailer@crop_duster", "m_a001018_01_gc_crop_duster_1(a+b+c+d)(3750-4300)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//
//
//		//	TASK_SYNCHRONIZED_SCENE (trailerPeds[5], sceneId3, "trailer@crop_duster", "m_b001018_01_gc_crop_duster_1(a+b+c+d)(3750-4300)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
////

			
			SET_CAM_PARAMS(cam_main, <<-1769.218628,2174.874756,117.288788>>,<<17.673813,-0.183133,-146.574448>>,50.082756, 3250, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3000)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			WAIT(0)
			
			
				
			trailerParticles[1] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_crop_insects", << -1764.4720, 2168.6252, 117.2702 >>,  <<0.0, 0.0, 0.0>>)
		

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 1000
			
		
			
					IF NOT IS_ENTITY_DEAD(trailerVehicle)
						trailerParticles[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_crop_spreading_spray", trailerVehicle, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ENDIF
				
				i_current_event++
			ENDIF
		BREAK

		CASE 2
			IF TIMERB() > 3250
				
				IF NOT bDontSkipTimeInDusterRecording
				//IF NOT DOES_CAM_EXIST(GET_DEBUG_CAM())
					//IF NOT IS_CAM_ACTIVE(GET_DEBUG_CAM())
						IF NOT IS_ENTITY_DEAD(trailerVehicle)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, -600)
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
				//SET_CLOCK_TIME(12, 00, 0)
				SET_CAM_PARAMS(cam_main,<<-1775.755493,2170.783691,116.499367>>,<<14.494264,-0.183135,-99.779587>>,39.192780)
				SET_CAM_PARAMS(cam_main,<<-1775.755493,2170.783691,116.499367>>,<<53.232471,-0.183133,-126.885590>>,39.192780, 1800, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		
				i_current_event++
			ENDIF
		BREAK

		CASE 3
			IF TIMERB() > 8000
				STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				STOP_PARTICLE_FX_LOOPED(trailerParticles[1])
				//DESTROY_ALL_CAMS()

				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC stageCrashedCar2()
	INT i = 0

	MODEL_NAMES model_traffic[5]
	MODEL_NAMES model_traffic_ped = A_M_Y_Business_01
	
	model_traffic[0] = EMPEROR
	model_traffic[1] = HABANERO
	model_traffic[2] = INGOT
	model_traffic[3] = STANIER
	model_traffic[4] = SPEEDO	

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REPEAT COUNT_OF(model_traffic) i
				REQUEST_MODEL(model_traffic[i])
			ENDREPEAT
			
			REQUEST_MODEL(S_M_M_PARAMEDIC_01)
			REQUEST_MODEL(S_M_Y_COP_01)
			REQUEST_MODEL(model_traffic_ped)
			REQUEST_PTFX_ASSET()
			
			WHILE NOT HAS_MODEL_LOADED(S_M_M_PARAMEDIC_01)
			OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
			OR NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(AMBULANCE, trailerVehicle2, << 998.8652, 260.8638, 80.3044 >>, 297.3032 )
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(POLICE, trailerVehicles[0], << 996.1667, 266.1225, 80.4528 >>, 46.5483 )
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(POLICE, trailerVehicles[1], << 990.1310, 271.6541, 80.7240 >>, 47.6021 )
				WAIT(0)
			ENDWHILE
	
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle2)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle2, PEDTYPE_MISSION, S_M_M_PARAMEDIC_01)
				trailerPeds[1] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle2, PEDTYPE_MISSION, S_M_M_PARAMEDIC_01, VS_FRONT_RIGHT)
				SET_VEHICLE_ENGINE_ON(trailerVehicle2, TRUE, TRUE)
				SET_VEHICLE_SIREN(trailerVehicle2, TRUE)
				TASK_LOOK_AT_ENTITY(trailerPeds[0], trailerPeds[1], 5000)
				TASK_LOOK_AT_ENTITY(trailerPeds[1], trailerPeds[0], 5000)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicles[0])
				trailerPeds[2] = CREATE_PED_INSIDE_VEHICLE(trailerVehicles[0], PEDTYPE_MISSION, S_M_Y_COP_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicles[0], TRUE, TRUE)
				SET_VEHICLE_SIREN(trailerVehicles[0], TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicles[1])
				trailerPeds[3] = CREATE_PED_INSIDE_VEHICLE(trailerVehicles[1], PEDTYPE_MISSION, S_M_Y_COP_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicles[1], TRUE, TRUE)
				SET_VEHICLE_SIREN(trailerVehicles[1], TRUE)
			ENDIF

			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(POLICE, trailerVehicle, vCrashedCarCoords, vCrashedCarRotation.z )
				WAIT(0)
			ENDWHILE
			FREEZE_ENTITY_POSITION(trailerVehicle, TRUE)
			SET_VEHICLE_CAN_LEAK_PETROL(trailerVehicle, TRUE)
			SET_VEHICLE_PETROL_TANK_HEALTH(trailerVehicle, 100.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			
			//fiScriptFire = START_SCRIPT_FIRE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -1.5, 0.0>>), 5)
			//fiScriptFire2 = START_SCRIPT_FIRE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -1.0, 0.0>>), 5)
			
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(trailerVehicle, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(trailerVehicle, SC_DOOR_FRONT_RIGHT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(trailerVehicle, SC_DOOR_REAR_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(trailerVehicle, SC_DOOR_REAR_RIGHT, FALSE)
			
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			
			WAIT(1000)
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				EXPLODE_VEHICLE(trailerVehicle)
			ENDIF
			WAIT(1000)
			
			
			FREEZE_ENTITY_POSITION(trailerVehicle, FALSE)
			
			//timecycle = overcast (urban) @ 16:00
			//extra light IPL= TRAILERshot_carburn
			//cloudhat loaded = altostratus
			
			SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST")
			REQUEST_IPL("TRAILERshot_carburn")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			LOAD_CLOUD_HAT("altostratus")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(2.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(2.0)
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF
			
			//Create traffic cars
			INT i_model_index
			
			CLEAR_AREA(<< 967.5585, 247.0504, 79.1006 >>, 50.0, FALSE)
			
			REPEAT 10 i
				i_model_index = GET_RANDOM_INT_IN_RANGE(0, 5)
				
				WHILE NOT HAS_MODEL_LOADED(model_traffic[i_model_index])
				OR NOT HAS_MODEL_LOADED(model_traffic_ped)
					WAIT(0)
				ENDWHILE
			
				IF i = 0
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 967.5585, 247.0504, 79.1006 >>, 319.5162)
				ELIF i = 1
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 956.9549, 234.6244, 78.1964 >>, 319.5188)
				ELIF i = 2
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 941.5156, 216.5299, 76.8794 >>, 319.5142)
				ELIF i = 3
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 960.1879, 229.2903, 77.9170 >>, 316.8388)
				ELIF i = 4
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 950.3649, 218.8552, 77.1501 >>, 316.6343)
				ELIF i = 5
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 928.4793, 192.2963, 75.0931 >>, 323.4070)
				ELIF i = 6
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 968.9425, 230.9074, 77.9723 >>, 312.9387)
				ELIF i = 7
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 952.5487, 212.2077, 76.5960 >>, 320.1035)
				ELIF i = 8
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 934.0394, 188.2550, 74.8161 >>, 323.1431)
				ELIF i = 9
					trailerVehicles[2+i] = CREATE_VEHICLE(model_traffic[i_model_index], << 922.6775, 176.0445, 73.9667 >>, 319.5067)
				ENDIF
				
				SET_VEHICLE_ON_GROUND_PROPERLY(trailerVehicles[2+i] )
				SET_VEHICLE_FORWARD_SPEED(trailerVehicles[2+i] , 5.0)
				
				//Create a ped to sit in the car
				trailerPeds[4+i] = CREATE_PED_INSIDE_VEHICLE(trailerVehicles[2+i], PEDTYPE_MISSION, model_traffic_ped)
				//TASK_VEHICLE_MISSION(trailerPeds[1+i], veh, NULL, MISSION_CRUISE, 15.0, DRIVINGMODE_STOPFORCARS_STRICT, 5.0, 10.0, FALSE)
				IF i = 0 OR i = 3 OR i = 6
					TASK_VEHICLE_DRIVE_TO_COORD(trailerPeds[4+i], trailerVehicles[2+i], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicles[2+i], <<0.0, 20.0, 0.0>>), 
												10.0, DRIVINGSTYLE_STRAIGHTLINE, model_traffic[i_model_index], DRIVINGMODE_STOPFORCARS_STRICT, 2.0, 10.0)
				ELSE
					TASK_VEHICLE_DRIVE_TO_COORD(trailerPeds[4+i], trailerVehicles[2+i], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicles[2+i], <<0.0, 25.0, 0.0>>), 10.0, 
												DRIVINGSTYLE_STRAIGHTLINE, model_traffic[i_model_index], DRIVINGMODE_STOPFORCARS_STRICT, 2.0, 10.0)
				ENDIF
				//TASK_STAND_STILL(trailerPeds[1+i], -1)
				SET_PED_KEEP_TASK(trailerPeds[4+i], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trailerPeds[4+i], TRUE)
				
				//Remove from memory
				//SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
				//MARK_CHAR_AS_NO_LONGER_NEEDED_KEEP_ID(ped)
			ENDREPEAT
			
			REPEAT COUNT_OF(model_traffic) i
				SET_MODEL_AS_NO_LONGER_NEEDED(model_traffic[i])
			ENDREPEAT
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model_traffic_ped)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1017.0508, 278.5205, 82.6931 >>, << 3.3668, 0.0000, 115.4823 >>, 33.5974, TRUE)
			SET_CAM_PARAMS(cam_main, << 1017.0508, 278.5205, 82.6931 >>, << 8.2856, -0.0000, 118.6459 >>, 33.5974, 8000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			//SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2000)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			trailerParticles[0] =  START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_car_fire", GET_ENTITY_COORDS(trailerVehicle, FALSE), sceneRotation)
			
			
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
		
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				
				
				SET_ENTITY_COORDS(trailerVehicle, vCrashedCarCoords)
				SET_ENTITY_ROTATION(trailerVehicle, vCrashedCarRotation)
				
			ENDIF
		
			IF TIMERB() > 8000
				STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH



ENDPROC

PROC stageYacht()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-2045.438843,-1044.254272,6.436977>>,<<-2.599109,-0.300769,-80.745171>>,38.574070, TRUE)
			SET_CAM_PARAMS(cam_main, <<-2039.374756,-1043.266235,6.164606>>,<<-1.702572,-0.300769,-80.745171>>,38.574070, 14000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			
			REQUEST_CUTSCENE("Family_2_MCS_3p3")
			i_current_event++
		BREAK
			
		CASE 2
		
	
			IF HAS_CUTSCENE_LOADED()
				i_current_event++
			ENDIF
		
		BREAK
		
		CASE 3
			
			START_CUTSCENE()
			i_current_event++
		BREAK
		
		CASE 4
			
			IF HAS_CUTSCENE_FINISHED()
			
			
			//IF TIMERB() > 21800
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			//ENDIF
				 
			ENDIF
		BREAK
	ENDSWITCH

//	REQUEST_CUTSCENE("Family_2_MCS_3p3")
//	
//	WHILE NOT HAS_CUTSCENE_LOADED()
//		WAIT(0)
//	ENDWHILE
//	
//	START_CUTSCENE()
//	
//	WHILE NOT HAS_CUTSCENE_FINISHED()
//		WAIT(0)
//	ENDWHILE

ENDPROC


PROC stageMuscleBeach()

//
//ATTACH_ENTITY_TO_ENTITY( oiTrailerProp[0], PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, FALSE)
//so it's all zero
//these are setup fine
//Prop_barbell_02
//PROP_BARBELL_80KG
//PROP_BARBELL_60KG
//PROP_BARBELL_50KG

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_ANIM_DICT("trailer@muscle_beach")
			REQUEST_MODEL(A_M_Y_MUSCLBEAC_01)
			REQUEST_MODEL(A_M_Y_MUSCLBEAC_02)
			REQUEST_MODEL(A_F_Y_BEACH_01)
			//REQUEST_MODEL(A_F_M_BodyBuild_01)
			
			REQUEST_MODEL(PROP_CURL_BAR_01)
			
			REQUEST_MODEL(Prop_barbell_02)
			
			
			//timecycle = neutral (urban) @ 18:00
			//cloudhat loaded= horizon and wispy
			
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			LOAD_CLOUD_HAT("horizon")
			LOAD_CLOUD_HAT("wispy")
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1196.479858,-1562.327271,3.347473,0.510312)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1199.167725,-1559.487305,3.361219,0.222875)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-1195.995850,-1565.668945,4.607301,0.654031)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1190.344360,-1564.195190,4.739864>>,<<-0.381852,0.271697,73.991272>>,30.033113, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_MUSCLBEAC_01)
			OR NOT HAS_MODEL_LOADED(A_M_Y_MUSCLBEAC_02)
			//OR NOT HAS_MODEL_LOADED(A_F_M_BodyBuild_01)
			OR NOT HAS_MODEL_LOADED(Prop_barbell_02)
			OR NOT HAS_MODEL_LOADED(PROP_CURL_BAR_01)
			OR NOT HAS_MODEL_LOADED(A_F_Y_BEACH_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@muscle_beach")
				WAIT(0)
			ENDWHILE
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MUSCLBEAC_01, scenePosition)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MUSCLBEAC_01, scenePosition)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MUSCLBEAC_02, scenePosition)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MUSCLBEAC_02, scenePosition)
			trailerPeds[4] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEACH_01, <<-1189.7118, -1567.5682, 3.2893>>, 32.4585)
			
//			/* START SYNCHRONIZED SCENE - trailer_muscle_bench_lifter_a.xml */
//			scenePosition = << -1209.112, -1562.170, 3.757 >>
//			sceneRotation = << 0.000, 0.000, -53.640 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@muscle_beach", "001014_01_gc_muscle_head_1(b)(900-1200)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


			/* START SYNCHRONIZED SCENE - trailer_muscle_spotter_and_lifter_b */
			scenePosition = << -1209.149, -1562.077, 3.607 >>
			sceneRotation = << 0.000, 0.000, -55.080 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@muscle_beach", "m_a001015_01_gc_muscle_head_2(a+b)(900-1350)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId, "trailer@muscle_beach", "m_b001015_01_gc_muscle_head_2(a+b)(900-1350)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )





			oiTrailerProp = CREATE_OBJECT(Prop_barbell_02, scenePosition)
//
			ATTACH_ENTITY_TO_ENTITY( oiTrailerProp, trailerPeds[3], GET_PED_BONE_INDEX(trailerPeds[3], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, FALSE)

			/* START SYNCHRONIZED SCENE - trailer_muscle_curl_a.xml */
			scenePosition2 = << -1209.244, -1561.501, 3.607 >>
			sceneRotation2 = << 0.000, 0.000, -56.160 >>
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition2, sceneRotation2)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId2, "trailer@muscle_beach", "001014_01_gc_muscle_head_4(b)(314-816)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			oiTrailerProp2 = CREATE_OBJECT(PROP_CURL_BAR_01, scenePosition)

			ATTACH_ENTITY_TO_ENTITY( oiTrailerProp2, trailerPeds[1], GET_PED_BONE_INDEX(trailerPeds[1], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, FALSE)

			setPedVariation(trailerPeds[1], 0)
			
			/* START SYNCHRONIZED SCENE - trailer_muscle_dips_a.xml */
			scenePosition3 = << -1208.108, -1566.380, 3.440 >>
			sceneRotation3 = << 0.000, 0.000, 34.500 >>

//
//			/* START SYNCHRONIZED SCENE - trailer_muscle_dips_a.xml */
//			scenePosition3 = << -1207.924, -1566.607, 3.457 >>
//			sceneRotation3 = << 0.000, 0.000, 36.000 >>
			sceneId3 = CREATE_SYNCHRONIZED_SCENE(scenePosition3, sceneRotation3)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId3, "trailer@muscle_beach", "001014_01_gc_muscle_head_3(a)(450-996)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			
			TASK_GO_STRAIGHT_TO_COORD(trailerPeds[4], <<-1207.3439, -1542.8217, 3.2859>>, PEDMOVE_WALK, -1, 35.1129)
			
			SET_CAM_PARAMS(cam_main, <<-1190.345337,-1564.193970,4.957347>>,<<-0.381852,0.271697,71.461479>>,30.033113, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			SET_PED_PROP_INDEX(trailerPeds[1], ANCHOR_EYES, 0)
			SET_PED_PROP_INDEX(trailerPeds[2], ANCHOR_EYES, 0)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 1, 3, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)

			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)

			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[4], INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)


			

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 15000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageSaltonHookers()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
		
			REQUEST_ANIM_DICT("trailer@hookers")
			REQUEST_MODEL(PHOENIX)
			REQUEST_MODEL(S_F_Y_Hooker_01)
			REQUEST_MODEL(S_F_Y_Hooker_02)
			
			REQUEST_MODEL(A_M_M_Salton_02)
			
			IF mission_stage = STAGE_SALTON_SEA_HOOKERS
				//REQUEST_IPL("TRAILERSHOT_hookers_day")
				REQUEST_IPL("TRAILERSHOT_hookers_night")
			ELSE //STAGE_SALTON_SEA_HOOKERS_2
				REQUEST_IPL("TRAILERSHOT_hookers_night")
				LOAD_CLOUD_HAT("puffs")
			ENDIF
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(3.292)
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL)
			SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE,0.002)
					
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1403.4667, 3590.0764, 35.3755>>, <<32.8563, 0.0000, 75.4714>>, 45.0000, TRUE)
					
			WHILE NOT HAS_MODEL_LOADED(S_F_Y_Hooker_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@hookers")
			OR NOT HAS_MODEL_LOADED(PHOENIX)
			OR NOT HAS_MODEL_LOADED(S_F_Y_Hooker_02)
			OR NOT HAS_MODEL_LOADED(A_M_M_Salton_02)
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(scenePosition, 35.0, TRUE)
						
			/* START SYNCHRONIZED SCENE - trailer_hookers_car_a.xml */
			scenePosition = << 1403.480, 3593.853, 34.012 >>
			sceneRotation = << 0.000, 0.000, 122.400 >>
			
			scenePosition2 = << 1403.480, 3593.853, 34.002 >>
			sceneRotation2 = << 0.000, 0.000, 122.400 >>
			
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Salton_02, scenePosition)	
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_Hooker_01, scenePosition)	
			
			trailerVehicle = CREATE_VEHICLE(PHOENIX, scenePosition)		
			SET_ENTITY_COORDS_NO_OFFSET(trailerVehicle, << 1403.480, 3593.853, 34.255 >>)
			SET_ENTITY_ROTATION(trailerVehicle, << 0.438828, 1.74669, 122.321 >>)
			//FREEZE_ENTITY_POSITION(trailerVehicle, TRUE)
//			SET_ENTITY_COLLISION(trailerVehicle, FALSE)
			
			//SET_VEHICLE_COLOURS(trailerVehicle, 4, 0) //Light grey
			//SET_VEHICLE_COLOURS(trailerVehicle, 30, 0) //Scarlet
			SET_VEHICLE_COLOURS(trailerVehicle, 6, 0) //Drak grey
			//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 4, 0)
			//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 30, 0)
			SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 6, 0)
			SET_VEHICLE_DIRT_LEVEL(trailerVehicle, 5.0)
			SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
			
			
			//scenePosition.z = scenePosition.z  - 0.25
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition2, sceneRotation2)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[2], sceneId, "trailer@hookers", "001025_01_gc_hooker_movement_2_1840_2450_a_m", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[3], sceneId2, "trailer@hookers", "001025_01_gc_hooker_movement_2_1840_2450_c_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )	
			
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 2), 1, 1, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 8), 1, 1, 0) //(accs)


			
			//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, trailerVehicle, 0)
			
			SET_CAM_PARAMS(cam_main, << 1411.5350, 3592.4348, 34.4607 >>, << 10.4836, -0.9933, 67.7788 >>, 45.0000, 20000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 7000
				scenePosition = << 1396.709, 3599.516, 35.07 >>
				sceneRotation = << 0.000, 0.000, 124.920 >>
				sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				
				trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_Hooker_02, scenePosition)		
				TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@hookers", "001023_01_gc_hooker_movement_1_120_320", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							
				//scenePosition2 = << 1407.7759, 3596.7100, 34.67 >>
				//sceneRotation2 = << 0.000, 0.000, 75.600 >>
				
				
				
				/* START SYNCHRONIZED SCENE - trailer_hooker_movement_c.xml */
				scenePosition2 = << 1407.000, 3596.710, 34.86 >>
				sceneRotation2 = << 0.000, 0.000, 0.000 >>
							
				sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition2, sceneRotation2)
		
				trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_Hooker_02, scenePosition2)
				TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId2, "trailer@hookers", "001023_01_gc_hooker_movement_1_1580_2040", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					
				SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 2), 2, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)

				i_current_event++
			ENDIF
			
		BREAK

		CASE 2
			IF TIMERB() > 21500 
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
		SET_VEHICLE_BRAKE_LIGHTS(trailerVehicle, TRUE)
		
		//PRINTVECTOR(GET_ENTITY_ROTATION(trailerVehicle))
		//PRINTNL()
	ENDIF

ENDPROC

PROC stageGangBangersNew1()

	scenePosition = << -601.87, 277.109, 81.024 >>
	sceneRotation = << 0.000, 0.000, 97.250 >>

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_ANIM_DICT("trailer@gangbanger_walk")
			REQUEST_ANIM_DICT("trailer@joggers")
			REQUEST_MODEL(A_M_M_SouCent_04)
			REQUEST_MODEL(A_M_Y_SouCent_04)

			REQUEST_IPL("TRAILERshot_gangb")
			
			LOAD_CLOUD_HAT("wispy")  
			
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)

			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)

			SET_MAPDATACULLBOX_ENABLED("TrailerGBangers", TRUE) 
			bGangCullEnabled = TRUE

			WHILE NOT HAS_MODEL_LOADED(A_M_M_SouCent_04)
			OR NOT HAS_MODEL_LOADED(A_M_Y_SouCent_04)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@gangbanger_walk")
			OR NOT HAS_ANIM_DICT_LOADED("trailer@joggers")
				WAIT(0)
			ENDWHILE

			//SET_VEHICLE_DENSITY_MULTIPLIER(2.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(2.0)
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-610.66, 276.63, 82.19>>, <<2.15, -0.17, -82.15>>,22.38, TRUE)
			
			CLEAR_AREA(scenePosition, 10.0, TRUE)
			
	
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
	
//			scenePosition = << 11.532, -1386.698, 28.289 >>
//			sceneRotation = << 0.000, 0.000, -94.680 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//		
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_M_SouCent_04, scenePosition)	
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_04, scenePosition)	
//			

			//Loading ped model: a_m_m_soutcent_04
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0)//(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0)//(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)


			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId2, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			//SET_CAM_PARAMS(cam_main, <<9.618936,-1382.448486,29.683922>>,<<0.02, -2.91,-117.08>>,20.464531, 17000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			TASK_PLAY_ANIM(trailerPeds[1], "trailer@gangbanger_walk", "Gangbanger_Walk_EyelineAnim", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
			
			SET_CAM_PARAMS(cam_main,<<-608.526306,277.504669,82.282875>>,<<1.892235,-0.163712,-98.307716>>,22.379999, 13000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.175)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK
		
		CASE 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.17
					IF NOT IS_PED_INJURED(trailerPeds[1])
						//TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.32
					IF NOT IS_PED_INJURED(trailerPeds[1])
						//TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.42
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.590909
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.57
					IF NOT IS_PED_INJURED(trailerPeds[1])
						//TASK_PLAY_ANIM(trailerPeds[1], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId2) >= 0.73
					IF NOT IS_PED_INJURED(trailerPeds[0])
						TASK_PLAY_ANIM(trailerPeds[0], "trailer@joggers", "joggers_blink", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY)
					ENDIF
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF TIMERB() > 17000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
		SET_SYNCHRONIZED_SCENE_ORIGIN(sceneId2, (scenePosition + scenePositionOffset), (sceneRotation + sceneRotationOffset) )
	ENDIF

	//IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
	//	PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(sceneId))
	//	PRINTNL()
	//ENDIF

ENDPROC

PROC stageGangBangersNew2()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_ANIM_DICT("trailer@gangbanger_walk")
			REQUEST_MODEL(A_M_M_SouCent_04)
			REQUEST_MODEL(A_M_Y_SouCent_04)

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-570.47, 272.37, 83.13>>, <<3.98, -0.17, -71.36>>,31.40, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_M_SouCent_04)
			OR NOT HAS_MODEL_LOADED(A_M_Y_SouCent_04)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@gangbanger_walk")
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(scenePosition, 10.0, TRUE)
			
			scenePosition = << -565.30, 274.284, 82.024 >>
			sceneRotation = << 0.000, 0.000, 97.250 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			sceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
		
//			scenePosition = << 11.532, -1386.698, 28.289 >>
//			sceneRotation = << 0.000, 0.000, -94.680 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//		
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_M_SouCent_04, scenePosition)	
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_04, scenePosition)	

			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0)//(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0)//(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId2, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			//SET_CAM_PARAMS(cam_main, <<9.618936,-1382.448486,29.683922>>,<<0.02, -2.91,-117.08>>,20.464531, 17000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			
			SET_CAM_PARAMS(cam_main,<<-571.139404,270.820770,83.099030>>,<<5.441072,-0.154579,-68.671562>>,31.40000, 13000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.175)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId2)
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneId2, (scenePosition + scenePositionOffset), (sceneRotation + sceneRotationOffset) )
			ENDIF
		
			IF TIMERB() > 17000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageGangBangers1()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_ANIM_DICT("trailer@gangbanger_walk")
			REQUEST_MODEL(A_M_M_SouCent_04)
			REQUEST_MODEL(A_M_Y_SouCent_04)

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<9.618936,-1382.448486,29.683922>>,<<-0.021669,-2.911164,123.650185>>,20.464531, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_M_SouCent_04)
			OR NOT HAS_MODEL_LOADED(A_M_Y_SouCent_04)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@gangbanger_walk")
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(scenePosition, 10.0, TRUE)
			
			scenePosition = << 10.177, -1386.781, 28.285 >>
			sceneRotation = << 0.000, 0.000, -94.680 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
	
//			scenePosition = << 11.532, -1386.698, 28.289 >>
//			sceneRotation = << 0.000, 0.000, -94.680 >>
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//		
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_M_SouCent_04, scenePosition)	
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_04, scenePosition)	

			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0)//(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0)//(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)

			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			//SET_CAM_PARAMS(cam_main, <<9.618936,-1382.448486,29.683922>>,<<0.02, -2.91,-117.08>>,20.464531, 17000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			
			SET_CAM_PARAMS(cam_main,<<9.618936,-1382.448486,29.683922>>,<<0.020000,3.33,-117.079994>>,20.464531, 13000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.175)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 17000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageGangBangers2()
SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			
			REQUEST_ANIM_DICT("trailer@gangbanger_walk")
			REQUEST_MODEL(A_M_Y_SouCent_01)
			REQUEST_MODEL(A_M_Y_SouCent_02)

			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 43.5838, -1381.7651, 29.2976 >>, << 2.1434, -0.0029, -149.4521 >>, 23.4883, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_SouCent_01)
			OR NOT HAS_MODEL_LOADED(A_M_Y_SouCent_02)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@gangbanger_walk")
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(scenePosition, 10.0, TRUE)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_02, scenePosition)	
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_01, scenePosition)	
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0)//(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0)//(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			
			/* START SYNCHRONIZED SCENE - trailer_gangbanger_walk_b.xml */
			scenePosition = << 46.743, -1386.846, 28.328 >>
			sceneRotation = << 0.000, 0.000, -87.120 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@gangbanger_walk", "001003_01_gc_gangbanger_walk_alt1_420_870_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


			
			SET_CAM_PARAMS(cam_main,<< 42.9515, -1383.0348, 29.7400 >>, << -3.5608, 0.0227, -133.8741 >>, 23.4881, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.175)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageGangBangers3()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1118.6584, -1607.2472, 4.3658>>, <<13.8177, 0.0000, -122.2072>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1110.0729, -1620.5419, 4.3658>>, <<13.8177, 0.0000, -122.2072>>, 60.0000, 20000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 20000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageSouthCentral()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<72.7646, -1501.4686, 29.8145>>, <<4.8289, 0.0000, 22.1624>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<59.1703, -1491.5582, 29.8145>>, <<4.8289, 0.0000, 22.1624>>, 60.0000, 20000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 20000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageForeClosure2()


	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_M_Y_Business_02)
			
			REQUEST_MODEL(Prop_Forsale_sign_01)
			REQUEST_MODEL(Prop_sign_mallet)
			REQUEST_ANIM_DICT("trailer@foreclosure")
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Business_02)
			OR NOT HAS_MODEL_LOADED(Prop_sign_mallet)
			OR NOT HAS_MODEL_LOADED(Prop_Forsale_sign_01)
			OR NOT HAS_ANIM_DICT_LOADED("trailer@foreclosure")
				WAIT(0)
			ENDWHILE
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
		
			CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE(3) 
			CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE("CSM_ST_DITHER16") 
		
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,930.783203,-486.656891,59.200497,0.500000) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,927.611450,-480.563049,62.565857,0.500000) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,919.544739,-476.792847,63.979912,0.500000) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.500000)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(0.3)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(0.3)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<939.777649,-495.797485,59.336365>>,<<4.876900,-0.013515,43.017632>>,26.864319, TRUE)
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(FUTO, trailerVehicle, << 933.6661, -481.9772, 59.5381 >>, 26.4865)
				WAIT(0)
			ENDWHILE
		
			/* START SYNCHRONIZED SCENE - trailer_foreclosure_a.xml */
			scenePosition = << 933.098, -489.064, 60.016 >>
			sceneRotation = << 0.000, 0.000, 180.000 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Business_02, scenePosition)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			
								
			oiTrailerProp = CREATE_OBJECT(Prop_Forsale_sign_01, GET_ENTITY_COORDS(trailerPeds[0]))
			oiTrailerProp2 = CREATE_OBJECT(Prop_sign_mallet, GET_ENTITY_COORDS(trailerPeds[0]))
			
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0],BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0],BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@foreclosure", "001001_04_gc_man_forclosure_(760_1110)", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			SET_CAM_PARAMS(cam_main,<<939.258057,-496.276489,59.336231>>,<<4.876900,-0.013515,41.129715>>,26.864319, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		
			
		
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.240)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
		
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				SET_VEHICLE_NUMBER_PLATE_TEXT(trailerVehicle, "JAX1079W")
			ENDIF
		
			IF TIMERB() > 6000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC



PROC DO_BIRDS_JETSKI()

	STRING str_bird_anims = "creatures@gull@move"

	IF NOT DOES_ENTITY_EXIST(s_birds[0].ped)

	    REQUEST_MODEL(model_bird)
	    REQUEST_ANIM_DICT(str_bird_anims)
	    
		
	    IF HAS_MODEL_LOADED(model_bird)
	    AND HAS_ANIM_DICT_LOADED(str_bird_anims)
	          s_birds[0].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -327.3060, -2419.3550, 5.0000 >> + <<0.0, 0.0, -0.7>>, 251.4349)
	          s_birds[1].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -322.3221, -2418.5366, 5.0004 >> + <<0.0, 0.0, -0.7>>, 322.6699)
	          s_birds[2].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -320.5606, -2419.7383, 5.0004 >> + <<0.0, 0.0, -0.7>>, 32.0168)
	          SET_MODEL_AS_NO_LONGER_NEEDED(model_bird)
	          
			  INT i
			  
	          REPEAT COUNT_OF(s_birds) i
			  	IF NOT IS_PED_INJURED(s_birds[i].ped)
	                TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
	                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_birds[i].ped, TRUE)
	                SET_PED_CAN_RAGDOLL(s_birds[i].ped, FALSE)
	                SET_ENTITY_INVINCIBLE(s_birds[i].ped, TRUE)
	                FREEZE_ENTITY_POSITION(s_birds[i].ped, TRUE)
	                SET_ENTITY_COLLISION(s_birds[i].ped, FALSE)
	                s_birds[i].i_event = 0
				ENDIF
	          ENDREPEAT
	    ENDIF
	     
	ELSE
		INT i
		REPEAT COUNT_OF(s_birds) i
		    IF NOT IS_PED_INJURED(s_birds[i].ped)
		        IF s_birds[i].i_event = 0
		            //IF VDIST2(GET_ENT, GET_ENTITY_COORDS(s_birds[i].ped)) < 225.0
		                    FREEZE_ENTITY_POSITION(s_birds[i].ped, FALSE)
		                    SET_ENTITY_COLLISION(s_birds[i].ped, TRUE)
		                    VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(s_birds[i].ped) + <<45.0, 0.0, 0.0>>)
		                    SET_ENTITY_VELOCITY(s_birds[i].ped, v_dir * 7.0)
		                
		                      //OPEN_SEQUENCE_TASK(seq)
		                      //    TASK_PLAY_ANIM(NULL, str_bird_anims, "takeoff", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
		                        TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "flapping", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_IGNORE_GRAVITY)
		                      //CLOSE_SEQUENCE_TASK(seq)
		                      
		                      //TASK_PERFORM_SEQUENCE(s_birds[i].ped, seq)
		                      //CLEAR_SEQUENCE_TASK(seq)
		                      
		                    s_birds[i].i_event++
		            //ENDIF
		        ELIF s_birds[i].i_event = 1
		            IF IS_ENTITY_PLAYING_ANIM(s_birds[i].ped, str_bird_anims, "flapping")
		                SET_ENTITY_ANIM_SPEED(s_birds[i].ped, str_bird_anims, "flapping", 1.0)
		        	ENDIF
		                
		           	VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(s_birds[i].ped) + <<45.0, 0.0, 0.0>>)
		            SET_ENTITY_VELOCITY(s_birds[i].ped, v_dir * 7.0)
		        ENDIF
		          
	//          IF f_current_playback_time > 82000.0
	//          OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	//	                REMOVE_PED(s_birds[i].ped)
	//	          ENDIF
		    ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

PROC stageJetSkiBay()

	//SET_USE_HI_DOF()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_ANIM_DICT("trailer@jetski")
			REQUEST_MODEL(A_M_Y_JetSki_01)
	
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_JetSki_01)
				WAIT(0)
			ENDWHILE	
			
			WHILE NOT 	INITIALISE_TRAILER_VEHICLE_SET_PIECE(3, "TrailerPlane", POLMAV, trailerVehicle2, fJetSkiChopperStartTime)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_LOD_DIST(trailerVehicle2, 3000)
			SET_VEHICLE_ENGINE_ON(trailerVehicle2, TRUE, TRUE)
			SET_HELI_BLADES_FULL_SPEED(trailerVehicle2)
			
			//WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(4, "Trailer", SEASHARK2, trailerVehicle, 7000.0, 0.6)
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(SEASHARK, trailerVehicle, <<-453.8944, -2375.7385, 0.0>>, 110.642 )
				WAIT(0)
			ENDWHILE
	
			//Colours to match vid: 30, 0, 0, 0
			SET_VEHICLE_COLOURS(trailerVehicle, 88, 0)
			SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 0, 0)
	
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			
			REQUEST_IPL("SP1_02_SHOT_jetski")
			
			LOAD_CLOUD_HAT("Horizon")
			LOAD_CLOUD_HAT("wispy")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
	
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
	
						
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<-481.458679,-2381.773682,0.529865>>,<<1.490308,-0.239643,-92.339439>>,20.364780, TRUE)
			
			SET_CAM_FAR_DOF(cam_main, 40.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_JetSki_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			ATTACH_ENTITY_TO_ENTITY(trailerPeds[0], trailerVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehicle, "seat_dside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			SET_ENTITY_COLLISION(trailerPeds[0], FALSE)	
			TASK_PLAY_ANIM(trailerPeds[0], "trailer@jetski", "001009_01_Jetski_2730_3230", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)					
			
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_JetSki_01, <<-968.2619, -1208.3999, 4.2513>>, 303.5482)
			SET_PED_INTO_VEHICLE(trailerPeds[2], trailerVehicle)						
			SET_VEHICLE_FORWARD_SPEED(trailerVehicle, 20)

			SET_ENTITY_VISIBLE(trailerPeds[2], FALSE)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle2)
				trailerPeds[1] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle2, PEDTYPE_MISSION, A_M_Y_JetSki_01)
			
				SET_VEHICLE_ENGINE_ON(trailerVehicle2, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(trailerVehicle2)
			ENDIF
			
//			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_HAIR, 1, 0)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_HAND, 1, 0)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], PED_COMP_LEG, 1, 0)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 1, 3, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 9), 1, 0, 0) //(task)
			
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 1)
			
			
		
			OPEN_SEQUENCE_TASK (trailerSeq)
      			TASK_FLUSH_ROUTE()
      			//TASK_EXTEND_ROUTE(<< -453.8944, -2375.7385, 0.5 >>)
      			TASK_EXTEND_ROUTE(<<-474.2, -2389.2, 0.5>>) 
      			TASK_EXTEND_ROUTE(<<-502.6, -2407.0792, 0.5>>)
      			TASK_EXTEND_ROUTE( << -519.8077, -2418.9141, 0.5 >>)								
      			TASK_DRIVE_POINT_ROUTE(NULL, trailerVehicle, 20.0)
			CLOSE_SEQUENCE_TASK(trailerSeq)

			TASK_PERFORM_SEQUENCE(trailerPeds[2], trailerSeq)
			CLEAR_SEQUENCE_TASK(trailerSeq)
			
			
			
			
			SET_CAM_PARAMS(cam_main, <<-481.458679,-2381.773682,0.529865>>,<<1.326142,-0.239643,-132.661499>>,20.364780, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SET_CAM_FAR_DOF(cam_main, 40.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.70)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

	
		CASE 1
		
			DO_BIRDS_JETSKI()
		
			IF TIMERB() > 3000
				//DESTROY_ALL_CAMS()
				
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageNewOilDerricks()

	//USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerPlane", CARGOPLANE, trailerVehicle, fOilDerricksPlaneStartTime)
				WAIT(0)
			ENDWHILE
			SET_MODEL_AS_NO_LONGER_NEEDED(CARGOPLANE)
			
			REQUEST_IPL("TRAILERSHOT_derricks")
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			LOAD_CLOUD_HAT("contrails")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(6.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(6.0)
			
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1531.684448,-1863.949341,91.435524>>,<<0.904901,0.026290,52.550423>>,38.983158, TRUE)
			SET_CAM_PARAMS(cam_main, <<1532.672974,-1862.658325,91.434753>>,<<0.904901,0.026290,52.550423>>,38.983158, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			//cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1607.937378,-1944.433594,105.308289>>,<<4.100698,0.000001,44.394157>>,35.940353, TRUE)
			//SET_CAM_PARAMS(cam_main, <<1618.755493,-1955.473999,103.944977>>,<<6.989292,0.000001,43.949715>>,35.940353, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			//SET_ENTITY_LOD_DIST(trailerVehicle, 3000)
			//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1575.4905, -1916.9518, 95.3550 >>)
			//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1575.4905, -1916.9518, 95.3550 >>)
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8100
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

					

ENDPROC

PROC stageTentCityTown()

//
//
//	SWITCH i_current_event
//		CASE 0
//			//DESTROY_ALL_CAMS()
//			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<144.241028,-1197.733643,29.080606>>,<<-0.097397,-0.210036,-84.906212>>,28.155552, TRUE)
//			SET_CAM_PARAMS(cam_main, <<144.261032,-1201.029663,29.080606>>,<<-0.097397,-0.210036,-84.906212>>,28.155552, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3100)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//
//			SETTIMERB(0)
//			i_current_event++
//		BREAK
//
//		CASE 1
//			IF TIMERB() > 8100
//				//DESTROY_ALL_CAMS()
//				SET_CAM_FOV(cam_main, 70.0)
//			ENDIF
//		BREAK
//	ENDSWITCH


	SWITCH i_current_event
		CASE 0
			
			REQUEST_MODEL(A_F_M_SkidRow_01)
			REQUEST_MODEL(A_F_M_Tramp_01)
			REQUEST_MODEL(A_M_O_TRAMP_01)
			REQUEST_MODEL(Prop_CS_Beer_Bot_02)
			
			
			REQUEST_ANIM_DICT("amb@bums@male@stationary@sat_on_bench@idle_a")
			REQUEST_ANIM_DICT("amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_b")
			REQUEST_ANIM_DICT("amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_a")
			REQUEST_ANIM_DICT("AMB@BUMS@MALE@STATIONARY@STANDING@IDLE_A")
			
			REQUEST_PTFX_ASSET()
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//depot/gta5/art/anim/export_mb/AMB@/BUMS@/MALE@/STATIONARY@/LAYING_AGAINST_WALL@/IDLE_A/IDLE_C.anim
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<126.536377,-1199.120361,29.537498>>,<<4.581490,-0.210036,-85.062126>>,32.929657, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_O_TRAMP_01)
			OR NOT HAS_MODEL_LOADED(A_F_M_SkidRow_01)
			OR NOT HAS_MODEL_LOADED(A_F_M_Tramp_01)
			OR NOT HAS_MODEL_LOADED(Prop_CS_Beer_Bot_02)
			OR NOT HAS_PTFX_ASSET_LOADED()
			OR NOT HAS_ANIM_DICT_LOADED("amb@bums@male@stationary@sat_on_bench@idle_a")
			OR NOT HAS_ANIM_DICT_LOADED("amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_b")
			OR NOT HAS_ANIM_DICT_LOADED("AMB@BUMS@MALE@STATIONARY@STANDING@IDLE_A")
				WAIT(0)
			ENDWHILE
			
			
			
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_O_TRAMP_01, << 150.450, -1195.445, 27.850 >>, 128.880)
			
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[0], <<150.3000, -1195.6050, 28.8500>>)
			SET_ENTITY_ROTATION(trailerPeds[0], <<0.0000, 0.0000, 128.8800>>)
			
			FREEZE_ENTITY_POSITION(trailerPeds[0], TRUE)
			SET_ENTITY_COLLISION(trailerPeds[0], FALSE)
			
			TASK_PLAY_ANIM(trailerPeds[0], "amb@bums@male@stationary@sat_on_bench@idle_a", "idle_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			//The old guy that is sitting down:
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)

			
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_F_M_SkidRow_01, << 156.2295, -1198.4453, 28.2951 >>, 35.02254)
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[1], <<156.2308, -1198.5110, 29.3419>>)
			SET_ENTITY_ROTATION(trailerPeds[1], <<0.0393, 0.0561, -60.1200>>)
			FREEZE_ENTITY_POSITION(trailerPeds[1], TRUE)
			SET_ENTITY_COLLISION(trailerPeds[1], FALSE)
			TASK_PLAY_ANIM(trailerPeds[1], "amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_b", "idle_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
//			//I found another female tramp standing up at the back, I don’t think that she gets seen. Anyway:
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
//			
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)

			
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_F_M_Tramp_01, << 161.2295, -1197.4453, 28.2951 >>, 60.022)
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[2], <<162.0039, -1194.5240, 29.3089>>)
			SET_ENTITY_ROTATION(trailerPeds[2], <<0.0685, 0.0395, 135.0000>>)
			FREEZE_ENTITY_POSITION(trailerPeds[2], TRUE)
			SET_ENTITY_COLLISION(trailerPeds[2], FALSE)
			TASK_PLAY_ANIM(trailerPeds[2], "amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_a", "idle_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				
							
			//The lying down female tramp is the one that Jim will want to change. If she stays then please use:
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)

			
			
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_F_M_Tramp_01, << 138.8630, -1200.4750, 28.2951 >>, 12.1954 )
			TASK_PLAY_ANIM(trailerPeds[3], "AMB@BUMS@MALE@STATIONARY@STANDING@IDLE_A", "IDLE_C", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)

			
			//oiTrailerProp = CREATE_OBJECT(P_CS_Bottle_01, <<0.0, 0.0, 10.0>>)
			//ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			oiTrailerProp2 = CREATE_OBJECT(Prop_CS_Beer_Bot_02, <<0.0, 0.0, 10.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[1], GET_PED_BONE_INDEX(trailerPeds[1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			oiTrailerProp3 = CREATE_OBJECT(Prop_CS_Beer_Bot_02, <<0.0, 0.0, 10.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp3, trailerPeds[2], GET_PED_BONE_INDEX(trailerPeds[2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			oiTrailerProp4 = CREATE_OBJECT(Prop_CS_Beer_Bot_02, <<0.0, 0.0, 10.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp4, trailerPeds[3], GET_PED_BONE_INDEX(trailerPeds[3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			
			SET_CAM_PARAMS(cam_main, <<126.618629,-1200.075684,29.541004>>,<<4.581490,-0.210036,-85.062126>>,32.929657, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3)
			

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			WAIT(0)
			
			trailerParticles[0] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_tentcity_insects", << 150.0, -1199.0, 28.3 >>, <<0.0, 0.0, 0.0>>)
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		/*CASE 1
			IF TIMERB() > 3000
				SETTIMERB(0)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
					
			SET_CAM_PARAMS(cam_main,<<144.241028,-1197.733643,29.080606>>,<<-0.097397,-0.210036,-84.906212>>,28.155552)
			SET_CAM_PARAMS(cam_main, <<144.261032,-1201.029663,29.080606>>,<<-0.097397,-0.210036,-84.906212>>,28.155552, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				
			SETTIMERB(0)
			i_current_event++
	
		BREAK*/
		
		
		CASE 1
			IF TIMERB() > 4900
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				i_current_event++
			ENDIF
		BREAK
		
		
	ENDSWITCH

//	IF DOES_ENTITY_EXIST(trailerPeds[3])
//		IF NOT IS_ENTITY_DEAD(trailerPeds[3])
//			SET_ENTITY_COORDS(trailerPeds[3], vDebugPosition)
//			SET_ENTITY_HEADING(trailerPeds[3], fDebugHeading)
//			//STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
//						
//		ENDIF
//	ENDIF
	
ENDPROC


PROC stageTrain()
	SWITCH i_current_event
		CASE 0
			
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
			
			WHILE NOT HAS_MODEL_LOADED(FREIGHT)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
			OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
			OR NOT HAS_MODEL_LOADED(TANKERCAR)
			OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
				WAIT(0)
			ENDWHILE

			SET_RANDOM_TRAINS(FALSE) 
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 695.9028, -1117.5914, 26.5320 >>, << 7.2638, 0.0000, 59.8372 >>, 52.6563, TRUE)
			SET_CAM_PARAMS(cam_main, << 696.4451, -1111.0150, 26.5320 >>, << 7.2638, 0.0000, 59.8371 >>, 52.6563, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			//SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			trailerVehicle = CREATE_MISSION_TRAIN(iTrainConfiguration,<< 669.5091, -1039.5405, 21.6860 >>, TRUE)
			SET_TRAIN_CRUISE_SPEED(trailerVehicle, 10.0)
			SET_TRAIN_SPEED(trailerVehicle, 10.0)
			

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				SET_RANDOM_TRAINS(TRUE) 
				//SET_MISSION_TRAINS_AS_NO_LONGER_NEEDED() 
				DELETE_MISSION_TRAIN(trailerVehicle) 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageTrainB()
	SWITCH i_current_event
		CASE 0
			
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
			
			WHILE NOT HAS_MODEL_LOADED(FREIGHT)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
			OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
			OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
			OR NOT HAS_MODEL_LOADED(TANKERCAR)
			OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
				WAIT(0)
			ENDWHILE

			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)

			SET_RANDOM_TRAINS(FALSE) 
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 696.4066, -1105.3583, 23.5076 >>, << 12.2921, -0.0000, 49.7727 >>, 56.0507, TRUE)
			SET_CAM_PARAMS(cam_main, << 696.4066, -1101.4175, 23.5076 >>, << 12.2920, 0.0000, 49.7724 >>, 56.0507, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			//SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			trailerVehicle = CREATE_MISSION_TRAIN(iTrainConfiguration, << 669.5091, -1039.5405, 21.6860 >>, TRUE)
			SET_TRAIN_CRUISE_SPEED(trailerVehicle, 10.0)
			SET_TRAIN_SPEED(trailerVehicle, 10.0)
			

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				SET_RANDOM_TRAINS(TRUE) 
				//SET_MISSION_TRAINS_AS_NO_LONGER_NEEDED() 
				DELETE_MISSION_TRAIN(trailerVehicle) 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageLastShot()

	SWITCH i_current_event
		CASE 0
		
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(STUNT, trailerVehicle, << -1789.3167, -115.0694, 107.9308 >>, 327.8710)
				WAIT(0)
			ENDWHILE
					
				
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1790.848755,-111.794945,108.276558>>,<<10.159096,0.000000,-118.036453>>,50.000000, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1788.560181,-112.355095,108.276558>>,<<11.604136,-0.000000,-119.955833>>,50.000000, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			//SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)

			ENDIF
		BREAK
	ENDSWITCH
	

ENDPROC


PROC stageLDPacker()
	SWITCH i_current_event
		CASE 0		
			REQUEST_MODEL(MONROE)
			REQUEST_MODEL(CHEETAH)
			REQUEST_MODEL(STINGER)
			REQUEST_MODEL(JB700)
			REQUEST_MODEL(ENTITYXF)
			REQUEST_MODEL(ZTYPE)
			REQUEST_MODEL(TR2)
			REQUEST_MODEL(A_M_Y_GenStreet_01)
				
			IF HAS_MODEL_LOADED(MONROE)
			AND HAS_MODEL_LOADED(CHEETAH)
			AND HAS_MODEL_LOADED(STINGER)
			AND HAS_MODEL_LOADED(JB700)
			AND HAS_MODEL_LOADED(ENTITYXF)
			AND HAS_MODEL_LOADED(ZTYPE)
			AND HAS_MODEL_LOADED(TR2)
			AND HAS_MODEL_LOADED(A_M_Y_GenStreet_01)
				IF INITIALISE_TRAILER_VEHICLE_SET_PIECE(2, "MattTrailer", PACKER, trailerVehicle, 20000.0, 0.8)
				
					WAIT(0)
						
					IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
						trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_GenStreet_01)
						SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GenStreet_01)
					
						sPackerRear.vehTrailer = CREATE_VEHICLE(TR2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(trailerVehicle))
						ATTACH_VEHICLE_TO_TRAILER(trailerVehicle, sPackerRear.vehTrailer)
						SET_MODEL_AS_NO_LONGER_NEEDED(TR2)
						
						sPackerRear.vehCars[0] = CREATE_VEHICLE(MONROE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -5.0, 0.85>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[0], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[0], sPackerRear.vehTrailer, -1, <<0.0, -5.0, 0.85>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[0], CARSTEAL_COLOURS_MONROE, CARSTEAL_COLOURS_MONROE)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[0], CARSTEAL_COLOURS_MONROE, CARSTEAL_COLOURS_MONROE)
						SET_MODEL_AS_NO_LONGER_NEEDED(MONROE)
						
						sPackerRear.vehCars[1] = CREATE_VEHICLE(CHEETAH, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, 0.0, 0.95>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[1], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[1], sPackerRear.vehTrailer, -1, <<0.0, 0.0, 0.95>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[1], CARSTEAL_COLOURS_CHEETAH, CARSTEAL_COLOURS_CHEETAH)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[1], CARSTEAL_COLOURS_CHEETAH, CARSTEAL_COLOURS_CHEETAH)
						SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
						
						sPackerRear.vehCars[2] = CREATE_VEHICLE(STINGER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, 5.0, 0.85>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[2], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[2], sPackerRear.vehTrailer, -1, <<0.0, 5.0, 0.85>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[2], CARSTEAL_COLOURS_STINGER, CARSTEAL_COLOURS_STINGER)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[2], CARSTEAL_COLOURS_STINGER, CARSTEAL_COLOURS_STINGER)
						SET_MODEL_AS_NO_LONGER_NEEDED(STINGER)
						
						sPackerRear.vehCars[3] = CREATE_VEHICLE(JB700, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -5.0, 3.2>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[3], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[3], sPackerRear.vehTrailer, -1, <<0.0, -5.0, 3.2>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[3], CARSTEAL_COLOURS_JB700, CARSTEAL_COLOURS_JB700)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[3], CARSTEAL_COLOURS_JB700, CARSTEAL_COLOURS_JB700)
						SET_MODEL_AS_NO_LONGER_NEEDED(JB700)
						
						sPackerRear.vehCars[4] = CREATE_VEHICLE(ENTITYXF, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -0.25, 2.95>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[4], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[4], sPackerRear.vehTrailer, -1, <<0.0, -0.25, 2.95>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[4], CARSTEAL_COLOURS_ENTITYXF, CARSTEAL_COLOURS_ENTITYXF)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[4], CARSTEAL_COLOURS_ENTITYXF, CARSTEAL_COLOURS_ENTITYXF)
						SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
						
						sPackerRear.vehCars[5] = CREATE_VEHICLE(ZTYPE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, 5.0, 2.95>>)) 
						SET_VEHICLE_DIRT_LEVEL(sPackerRear.vehCars[5], 0.0)
						ATTACH_ENTITY_TO_ENTITY(sPackerRear.vehCars[5], sPackerRear.vehTrailer, -1, <<0.0, 5.0, 2.95>>, <<0.0, 0.0, 0.0>>)
						SET_VEHICLE_COLOURS(sPackerRear.vehCars[5], CARSTEAL_COLOURS_ZTYPE, CARSTEAL_COLOURS_ZTYPE)
						SET_VEHICLE_EXTRA_COLOURS(sPackerRear.vehCars[5], CARSTEAL_COLOURS_ZTYPE, CARSTEAL_COLOURS_ZTYPE)
						SET_MODEL_AS_NO_LONGER_NEEDED(ZTYPE)
						
						//DESTROY_ALL_CAMS()
						cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1429.734009,-743.514343,23.589846>>,<<2.643185,-0.168835,76.205055>>,25.0, TRUE)
						SET_CAM_PARAMS(cam_main, <<-1429.532349,-743.514343,23.589846>>,<<2.643184,-0.168835,140.654388>>,25.0, 14000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
						
						//cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1472.482300,-722.695129,26.735218>>,<<3.548307,-0.159151,72.393509>>,24.029781, TRUE)
						//SET_CAM_PARAMS(cam_main, <<-1437.662598,-755.826294,23.573498>>,<<3.548321,-0.158846,72.383659>>,24.029781, 4000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
						
						SHAKE_CAM(cam_main, "HAND_SHAKE", 0.1)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)

						#IF IS_DEBUG_BUILD
							START_CAM_EDITOR_PLAYBACK_THIS_FRAME()
						#ENDIF

						SETTIMERB(0)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 1
			IF TIMERB() > 8900			
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC stageLDConvertibles2()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)
		

	SWITCH i_current_event
		CASE 0
		
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			REQUEST_MODEL(A_F_Y_BevHills_03)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			REQUEST_ANIM_DICT("TRAILER@CONVERTIBLE")
			
			REQUEST_IPL("SP1_02_SHOT_convertible")
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL)
			CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE,0.002)
		
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
		
			IF HAS_MODEL_LOADED(A_F_Y_BevHills_03)
			AND HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			AND HAS_ANIM_DICT_LOADED("TRAILER@CONVERTIBLE")
				IF INITIALISE_TRAILER_VEHICLE_SET_PIECE(3, "MattTrailer", RAPIDGT2, trailerVehicle, 3000.0)
					trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_03, << -1144.3708, -1386.2308, 4.1517 >>, 303.4403)
										
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,7), 1, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)

					SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 0, 0)
				
					SET_ROADS_IN_ANGLED_AREA(<<-1164.383789,-1343.805420,3.580653>>, <<-1222.467651,-1371.465088,11.879992>>, 35.250000, FALSE, FALSE)
					
					trailerPeds[1] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
					
					SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT2, TRUE)
					
					SET_VEHICLE_DIRT_LEVEL(trailerVehicle, 0.0)
					//SET_VEHICLE_COLOURS(trailerVehicle, 35, 35)
					//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 35, 35)
					SET_VEHICLE_COLOURS(trailerVehicle, 31, 31)
					SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 25, 0)
					
					//SET_VEHICLE_DENSITY_MULTIPLIER(2.0)
					//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(2.0)
					IF bFirstTimePlayingThisShot
						INSTANTLY_FILL_VEHICLE_POPULATION()
					ENDIF
					
					//DESTROY_ALL_CAMS()
					cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1139.988037,-1374.330566,6.234421>>,<<1.146660,-0.288135,164.568161>>,22.122177, TRUE)
					SET_CAM_FAR_DOF(cam_main, 20.0)
					SET_CAM_NEAR_DOF(cam_main, 0.0)
					SET_CAM_PARAMS(cam_main, <<-1139.460083,-1374.532715,5.076072>>,<<1.146660,-0.288135,158.845078>>,22.122177, 15000) //, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
					SET_CAM_FAR_DOF(cam_main, 20.0)
					SET_CAM_NEAR_DOF(cam_main, 0.0)
					SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2080)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					
					scenePosition = << -1143.802944,-1385.471558,4.129526 >>
					sceneRotation = << 0.000, 0.000, -62.4517630 >>
					sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					TASK_SYNCHRONIZED_SCENE(trailerPeds[0], sceneId, "TRAILER@CONVERTIBLE", "GIRL_TRAILER_CONVERTIBLE_SYNC", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					

					//TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@CONVERTIBLE", "GIRL_TRAILER_CONVERTIBLE", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@CONVERTIBLE", "FRANKLIN_TRAILER_CONVERTIBLE", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

					#IF IS_DEBUG_BUILD
						START_CAM_EDITOR_PLAYBACK_THIS_FRAME()
					#ENDIF

					SETTIMERB(0)
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 1
			IF NOT IS_ENTITY_DEAD(trailerPeds[0])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneId, fConvertibleAnimStartTime)
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(trailerPeds[1])	
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[1],"TRAILER@CONVERTIBLE", "FRANKLIN_TRAILER_CONVERTIBLE")
					SET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[1],"TRAILER@CONVERTIBLE", "FRANKLIN_TRAILER_CONVERTIBLE", fConvertibleAnimStartTime )
					
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF TIMERB() > 1000
				IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
					LOWER_CONVERTIBLE_ROOF(trailerVehicle)
					
					SETTIMERB(0)
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ENTITY_DEAD(trailerPeds[0])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.7
						SET_CAM_PARAMS(cam_main, <<-1130.812378,-1382.121338,5.682544>>,<<-2.015316,-0.227195,88.400063>>,20.992794, 0)
						SET_CAM_PARAMS(cam_main, <<-1130.828613,-1382.122925,5.693985>>,<<-2.513215,-0.329864,81.898201>>,20.992794, 5000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
						
						CLEAR_AREA(<<-1136.91, -1379.59, 4.90>>, 6.0, TRUE)
						
						SETTIMERB(0)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(trailerPeds[0])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.99
						SET_CAM_FOV(cam_main, 70.0)
					ENDIF
				ENDIF
			ENDIF
		BREAK
//		
//		CASE 4
//			IF TIMERB() > 1000
//				SHAKE_CAM(cam_main, "HAND_SHAKE", 0.1080)
//				//SET_CAM_PARAMS(cam_main, <<-955.906555,-1207.263306,5.649543>>,<<-5.185903,-0.088929,57.557415>>,19.543890)
//				//SET_CAM_PARAMS(cam_main, <<-951.312195,-1203.525879,5.649543>>,<<-5.185903,-0.088929,72.429176>>,19.543890, 6000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//				
//				SETTIMERB(0)
//				i_current_event++
//			ENDIF
//		BREAK
	ENDSWITCH
	
	IF NOT IS_ENTITY_DEAD(trailerVehicle)
		SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(trailerVehicle, TRUE)
	ENDIF
	
ENDPROC


PROC stageIntroShot()
	SWITCH i_current_event
		CASE 0
			////DESTROY_ALL_CAMS()
			
			REQUEST_PTFX_ASSET()
			REQUEST_ANIM_DICT("creatures@gull@move")
			REQUEST_ANIM_DICT("Move_f@arrogant")
			REQUEST_ANIM_DICT("Move_m@sad")
			REQUEST_ANIM_DICT("TRAILER@DOG")
			REQUEST_MODEL(A_F_Y_Beach_01)
			REQUEST_MODEL(A_M_Y_Beach_02)
			REQUEST_MODEL(A_C_ROTTWEILER)
			
			REQUEST_MODEL(model_bird)
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Beach_02)
			OR NOT HAS_MODEL_LOADED(A_F_Y_Beach_01)
			OR NOT HAS_MODEL_LOADED(model_bird)
			OR NOT HAS_MODEL_LOADED(A_C_ROTTWEILER)
			OR NOT HAS_ANIM_DICT_LOADED("creatures@gull@move")
			OR NOT HAS_ANIM_DICT_LOADED("Move_f@arrogant")
			OR NOT HAS_ANIM_DICT_LOADED("Move_m@sad")
			OR NOT HAS_ANIM_DICT_LOADED("TRAILER@DOG")
				WAIT(0)
			ENDWHILE
			
			//CREATE_FLOCK(bird_flock, << -1482.2725, -1511.8994, 5.9216 >>)
			CREATE_FLOCK(bird_flock, <<-1463.64, -1517.52, 4.29>>, <<-1465.35, -1512.47, 5.65>>, <<-1464.01, -1516.77, 7.32>>)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Beach_01, << -1433.3919, -1517.4151, 1.1268 >>, 113.1838)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_Beach_02, << -1433.9932, -1516.6936, 1.1027 >>, 115.6090)
			
			//Ross, the two peds for the opening beack shot:
			//Female:A_F_Y_Beach_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			//MaleA_M_Y_Beach_02please note - he has an ACCS backpack on in these variations, we do NOT want this to spawn. Stew is removing this.
			//the correct ACCS will be 1,0
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)Stew is doing a quick pass on them.
			
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_C_ROTTWEILER, << -1434.9, -1518.91, 1.11 >>, 115.6090)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trailerPeds[2], TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_C_ROTTWEILER)
			
			REQUEST_IPL("TRAILERSHOT_intro")
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			LOAD_CLOUD_HAT("HORIZON")
			CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE,-0.001)
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1434.927612,-1517.728516,1.104435,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-1428.447632,-1511.193115,1.160198,0.478375)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1417.286743,-1512.982422,3.990722>>,<<0.050206,0.017528,89.766426>>,34.422237, TRUE)

			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//			BARREL:
//			X: -1433.15Y: -1514.1Z: 1.13  [Effect Scale: 0.9]
			trailerParticles[0] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1433.15, -1514.1, 1.13>>, <<0.0, 0.0, 0.0>>, 0.9)
//			BOTTOM OF RAMP:
//			X: -1436.3, -1513.8, 1.05  [Effect Scale: 0.8]
			trailerParticles[1] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1436.3, -1513.8, 1.05>>, <<0.0, 0.0, 0.0>>, 0.8)
//			CANOE:
//			X: -1432.4, -1511.2, 1.15  [Effect Scale: 0.5]
			trailerParticles[2] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1432.4, -1511.2, 1.15>>, <<0.0, 0.0, 0.0>>, 0.5)
//			HUT STILT#1:
//			X: -1430.0, -1510.8, 1.22  [Effect Scale: 0.6]
			trailerParticles[3] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1430.0, -1510.8, 1.22>>, <<0.0, 0.0, 0.0>>, 0.6)
//			HUT STILT#2:
//			X: -1426.93, -1509.6, 1.3   [Effect Scale: 0.6]
			trailerParticles[4] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1426.93, -1509.6, 1.3>>, <<0.0, 0.0, 0.0>>, 0.6)
//			HUT STILT#3:
//			X: -1427.86, -1507.2, 1.3   [Effect Scale: 1.0]
			trailerParticles[5] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1427.86, -1507.2, 1.3>>, <<0.0, 0.0, 0.0>>, 1.0)
//			HUT STILT#4:
//			X: -1430.92, -1508.28, 1.3   [Effect Scale: 1.0]
			trailerParticles[6] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trailer_beach_sand", <<-1430.92, -1508.28, 1.3>>, <<0.0, 0.0, 0.0>>, 1.0)
			
			IF NOT IS_PED_INJURED(trailerPeds[0])
				TASK_PLAY_ANIM(trailerPeds[0], "Move_f@arrogant", "move_arrogant_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			
			IF NOT IS_PED_INJURED(trailerPeds[1])
				TASK_PLAY_ANIM(trailerPeds[1], "Move_m@sad", "move_sad_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF

			IF NOT IS_PED_INJURED(trailerPeds[2])
				//scenePosition = << -119.667, -1620.939, 31.516 >>
				//sceneRotation = << 0.000, 0.000, 51.000 >>
				//sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				//TASK_SYNCHRONIZED_SCENE(trailerPeds[2], sceneId, "trailer@dog", "jumpingfence", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
				TASK_PLAY_ANIM(trailerPeds[2], "TRAILER@DOG", "On_Beach", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING, 0.0)
			ENDIF
			
			SET_CAM_PARAMS(cam_main, <<-1417.020752,-1512.589111,4.115966>>,<<-0.360210,0.017528,90.505173>>,34.422237, 4500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2080)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			

			SETTIMERB(0)

			i_current_event++
		BREAK

		CASE 1
			//UPDATE_FLOCK(bird_flock)
		
			IF TIMERB() > 5000
				bDontClearAreaBetweenRepeatPlays = TRUE
				SET_CAM_FOV(cam_main, 70.0)	//This is how the script detects a reset.
				////DESTROY_ALL_CAMS()
				//SET_CAM_FOV(cam_main, 70.0)
				IF trailerParticles[0] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[0])
					trailerParticles[0] = NULL
				ENDIF
				IF trailerParticles[1] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[1])
					trailerParticles[1] = NULL
				ENDIF
				IF trailerParticles[2] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[2])
					trailerParticles[2] = NULL
				ENDIF
				IF trailerParticles[3] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[3])
					trailerParticles[3] = NULL
				ENDIF
				IF trailerParticles[4] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[4])
					trailerParticles[4] = NULL
				ENDIF
				IF trailerParticles[5] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[5])
					trailerParticles[5] = NULL
				ENDIF
				IF trailerParticles[6] <> NULL
					REMOVE_PARTICLE_FX(trailerParticles[6])
					trailerParticles[6] = NULL
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_PED_INJURED(trailerPeds[0])
		IF IS_ENTITY_PLAYING_ANIM(trailerPeds[0], "Move_f@arrogant", "move_arrogant_a")
			SET_ENTITY_ANIM_SPEED(trailerPeds[0], "Move_f@arrogant", "move_arrogant_a", 0.75)
		ENDIF
	ENDIF

ENDPROC

PROC stageLDMovingCranes()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<907.6016, -2945.5857, 7.3874>>, <<25.3630, 0.0000, -45.5801>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<906.4911, -2944.4524, 7.3874>>, <<25.3630, 0.0000, -45.5801>>, 60.0000, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.1120)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8100
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageLDArm2()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1075.3075, -1665.5632, 4.5125>>, <<5.5610, 0.0000, -175.3737>>, 30.5624, TRUE)
			SET_CAM_PARAMS(cam_main, <<-1074.9048, -1665.5309, 4.5125>>, <<5.5610, 0.0000, -175.3737>>, 30.5624, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3100)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8100
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageLDHollywoodBowl()
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<643.8430, 534.4665, 132.2601>>, <<-2.9419, -0.0709, -51.5174>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<644.0040, 534.6006, 136.3332>>, <<-6.8433, -0.0709, -51.5174>>, 60.0000, 8300, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 8100
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC stageMichael()
//
//	SWITCH i_current_event
//		CASE 0
//			//DESTROY_ALL_CAMS()
//			
//			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
//			
//
//			REQUEST_ANIM_DICT("TRAILER@MICHAEL_CLOSEUP")
//			
//			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-775.467163,-1221.635864,58.373985>>,<<5.127292,-0.054592,-67.902184>>,9.301698, TRUE)
//			
//			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
//			OR NOT HAS_ANIM_DICT_LOADED("TRAILER@MICHAEL_CLOSEUP")
//				WAIT(0)
//			ENDWHILE
//			
//			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), << -774.9791, -1221.5624, 56.7475 >>, 80.2138)
//	
//			
//			TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@MICHAEL_CLOSEUP", "Michael_Closeup", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//			
//			//TASK_PLAY_ANIM(trailerPeds[0], "MISSFBI_S4MOP", "Guard_idle_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//						
//			SET_CAM_PARAMS(cam_main, <<-776.395081,-1222.087402,58.179218>>,<<7.880782,-0.019134,-65.209846>>,13.745982, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.2050)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//
//			//SET_ENTITY_ANIM_SPEED(trailerPeds[0], "MISSFBI_S4MOP", "Guard_idle_a", 0.5)
//			
//		
//			SETTIMERB(0)
//			i_current_event++
//		BREAK
//
//		CASE 1
//			
//			IF NOT IS_PED_INJURED(trailerPeds[0])
//				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[0], "MISSFBI_S4MOP", "Guard_idle_a")
//					SET_ENTITY_ANIM_SPEED(trailerPeds[0], "MISSFBI_S4MOP", "Guard_idle_a", 0.5)
//					SET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[0], "MISSFBI_S4MOP", "Guard_idle_a", 0.1)
//					i_current_event++
//				ENDIF
//			ENDIF
//			
//		BREAK
//		
//		CASE 2
//			IF TIMERB() > 12000
//				DELETE_PED(trailerPeds[0])
//				//DESTROY_ALL_CAMS()
//				
//				SET_TIME_SCALE(1.0)
//				
//				SET_CAM_FOV(cam_main, 70.0)
//			ENDIF
//		BREAK
//	ENDSWITCH


	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			REQUEST_IPL("SP1_02_SHOT_micheal")
			
			IF mission_stage = STAGE_MICHAEL
				SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
				LOAD_CLOUD_HAT("altostratus")
			ELIF mission_stage = STAGE_MICHAEL_ALT
				//CLEAR URBAN @9:00 and no cloud hats
				SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			ENDIF			
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",  <<-775.467163,-1221.635864,58.373985>>,<<5.127292,-0.054592,-67.902184>>,9.301698, TRUE)
			SET_CAM_PARAMS(cam_main,  <<-776.395081,-1222.087402,58.179218>>,<<7.880782,-0.019134,-65.209846>>,13.745982, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			
			REQUEST_CUTSCENE("Michael_closeup")
			
			i_current_event++
		BREAK
			
		CASE 2
		
	
			IF HAS_CUTSCENE_LOADED()
				i_current_event++
			ENDIF
		
		BREAK
		
		CASE 3
			SET_CUTSCENE_TRIGGER_AREA(<< -767.979, -1213.56, 56.748 >>, 10.0, 118.887, 118.887)
			START_CUTSCENE_AT_COORDS(<< -767.979, -1213.56, 56.748 >>)			
			//CUTSCENE
//			START_CUTSCENE()
			i_current_event++
		BREAK
		
		CASE 4
			
			IF HAS_CUTSCENE_FINISHED()
			
			
			//IF TIMERB() > 21800
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			//ENDIF
				 
			ENDIF
		BREAK
	ENDSWITCH
	
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 45.00, TRUe)

//	REQUEST_CUTSCENE("JH_2b_mcs_1p1")
//	
//	WHILE NOT HAS_CUTSCENE_LOADED()
//		WAIT(0)
//	ENDWHILE
//	
//	START_CUTSCENE()
//	
//	WHILE NOT HAS_CUTSCENE_FINISHED()
//		WAIT(0)
//	ENDWHILE


ENDPROC
//
//PROC stageSubmarine()
//
//	SWITCH i_current_event
//		CASE 0
//			//DESTROY_ALL_CAMS()
//			
//			REQUEST_MODEL(SUBMARINE)
//			
//			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-137.584702,-2888.806396,2.981524>>,<<-0.143304,0.034611,18.666828>>,45.000000, TRUE)
//			
//					
//			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(SUBMARINE, trailerVehicle,  << -141.5509, -2874.1113, -10.2664 >>, 338.4431   )
//				WAIT(0)
//			ENDWHILE
//			
//			vsubcoords = << -141.5509, -2874.1113, -10.2664 >>
//			fMod = 1.0	
//			SET_CAM_PARAMS(cam_main, <<-137.584702,-2883.581055,2.981524>>,<<2.605899,0.034611,18.666817>>,45.000000, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3050)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SETTIMERB(0)
//			i_current_event++
//		BREAK
//
//		CASE 1
//			
//			//PRINTSTRING("Frametime ") PRINTFLOAT(vsubcoords.z) PRINTNL()
//			
//			IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
//            	IF vsubcoords.z < -7.5 				
//					fMod = (-7.5 - vsubcoords.z) / 100.00
//                	//vsubcoords.z = (vsubcoords.z + (GET_FRAME_TIME() * fMod * 0.998)) // + GET_FRAME_TIME()
//				     vsubcoords.z = (vsubcoords.z + (fMod * 20.00 * GET_FRAME_TIME()))
//											
//	          //  ELIF vsubcoords.z < -7.5
//               //     vsubcoords.z = vsubcoords.z + (1+(-vsubcoords.z-7.000)) * 0.04
//
//            		
//				ENDIF
//            	
//				SET_ENTITY_COORDS(trailerVehicle,vsubcoords)
//				SET_ENTITY_ROTATION(trailerVehicle, <<0.0, 0.0, 0.0>>)
//				
//				//FREEZE_ENTITY_POSITION(trailerVehicle, TRUE)
//      		ENDIF
//				
//			IF TIMERB() > 12000
//				//DESTROY_ALL_CAMS()
//				SET_CAM_FOV(cam_main, 70.0)
//			ENDIF
//		BREAK
//	ENDSWITCH
//
//ENDPROC

PROC stageRecord()

	SWITCH i_current_event
	
		CASE 0
		
			REQUEST_MODEL(SENTINEL2)
			WHILE NOT HAS_MODEL_LOADED(SENTINEL2)
				WAIT(0)
			ENDWHILE
			
			trailerVehicle = CREATE_VEHICLE(SENTINEL2, << 458.2242, 115.7294, 97.7469 >>, 67.7427)
		
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), trailerVehicle)
		
			INIT_UBER_RECORDING("Temp2")
			set_uber_parent_widget_group(paradiseWidgetGroup)
			i_current_event++
			
		BREAK
		
		
		CASE 1
			
			UPDATE_UBER_RECORDING()
			
		BREAK
		
	ENDSWITCH
	


ENDPROC

PROC playbackCameraCuts()

	SWITCH iCameraStage

		CASE 0
			IF fTriggerCarPlaybackTime > 2308.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 1
			IF fTriggerCarPlaybackTime > 3156.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 2
			IF fTriggerCarPlaybackTime > 4844.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK

		CASE 3
			IF fTriggerCarPlaybackTime > 7973.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 4
			IF fTriggerCarPlaybackTime > 10793.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 5
			IF fTriggerCarPlaybackTime > 19846.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK

		CASE 6
			IF fTriggerCarPlaybackTime > 23021.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 7
			IF fTriggerCarPlaybackTime > 25746.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 8
			IF fTriggerCarPlaybackTime > 30431.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 9
			IF fTriggerCarPlaybackTime > 34146.000 //34566.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK

		CASE 10
			IF fTriggerCarPlaybackTime > 39447.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 11
			IF fTriggerCarPlaybackTime > 47307.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 12
			IF fTriggerCarPlaybackTime > 51734.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK

		CASE 13
			IF fTriggerCarPlaybackTime > 55705.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 14
			IF fTriggerCarPlaybackTime > 59193.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 15
			IF fTriggerCarPlaybackTime > 64955.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK
		
		CASE 16
			IF fTriggerCarPlaybackTime > 73705.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK
		
//		CASE 16
//			IF fTriggerCarPlaybackTime > 77766.0000
//				iCameraChoice = 2
//				iCameraStage++
//			ENDIF
//		BREAK

		CASE 17
			IF fTriggerCarPlaybackTime > 82420.0000
				iCameraChoice = 0
				iCameraStage++
			ENDIF
		BREAK

		CASE 18
			IF fTriggerCarPlaybackTime > 87007.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 19
			IF fTriggerCarPlaybackTime > 89199.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK



	ENDSWITCH

ENDPROC


PROC playbackCameraCuts2()

	SWITCH iCameraStage

		CASE 0 
			IF fTriggerCarPlaybackTime > 30000.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		/*CASE 0
			IF fTriggerCarPlaybackTime > 6000.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 1
			IF fTriggerCarPlaybackTime > 8800.0000
				iCameraChoice = 3
				iCameraStage++
			ENDIF
		BREAK

		CASE 2
			IF fTriggerCarPlaybackTime > 14740.0000
				iCameraChoice = 4
				iCameraStage++
			ENDIF
		BREAK

		CASE 3
			IF fTriggerCarPlaybackTime > 18714.0000
				iCameraChoice = 5
				iCameraStage++
			ENDIF
		BREAK

		CASE 4
			IF fTriggerCarPlaybackTime > 19894.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 5
			IF fTriggerCarPlaybackTime > 21934.0000
				iCameraChoice = 4
				iCameraStage++
			ENDIF
		BREAK

		CASE 6
			IF fTriggerCarPlaybackTime > 23122.0000
				iCameraChoice = 3
				iCameraStage++
			ENDIF
		BREAK

		CASE 7
			IF fTriggerCarPlaybackTime > 27204.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 8
			IF fTriggerCarPlaybackTime > 30778.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 9
			IF fTriggerCarPlaybackTime > 34000.0000
				iCameraChoice = 3
				iCameraStage++
			ENDIF
		BREAK

		CASE 10
			IF fTriggerCarPlaybackTime > 37378.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 11
			IF fTriggerCarPlaybackTime > 38962.0000
				iCameraChoice = 4
				iCameraStage++
			ENDIF
		BREAK

		CASE 12
			IF fTriggerCarPlaybackTime > 40612.0000
				iCameraChoice = 6
				iCameraStage++
			ENDIF
		BREAK

		CASE 13
			IF fTriggerCarPlaybackTime > 42262.0000
				iCameraChoice = 5
				iCameraStage++
			ENDIF
		BREAK

		CASE 14
			IF fTriggerCarPlaybackTime > 44638.0000
				iCameraChoice = 3
				iCameraStage++
			ENDIF
		BREAK

		CASE 15
			IF fTriggerCarPlaybackTime > 49060.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 16
			IF fTriggerCarPlaybackTime > 51568.0000
				iCameraChoice = 5
				iCameraStage++
			ENDIF
		BREAK

		CASE 17
			IF fTriggerCarPlaybackTime > 53086.0000
				iCameraChoice = 1
				iCameraStage++
			ENDIF
		BREAK

		CASE 18
			IF fTriggerCarPlaybackTime > 57112.0000
				iCameraChoice = 2
				iCameraStage++
			ENDIF
		BREAK

		CASE 19
			IF fTriggerCarPlaybackTime > 59158.0000
				iCameraChoice = 4
				iCameraStage++
			ENDIF
		BREAK

		CASE 20
			IF fTriggerCarPlaybackTime > 64824.0000
				iCameraChoice = 3
				iCameraStage++
			ENDIF
		BREAK*/
	
	ENDSWITCH

ENDPROC

PROC stageChase()

	SWITCH i_current_event
	
		CASE 0
		
			REQUEST_MODEL(rapidgt2)
			REQUEST_MODEL(POLICE)
			REQUEST_MODEL(MAVERICK)
			REQUEST_MODEL(A_F_Y_FITNESS_02)
			REQUEST_VEHICLE_RECORDING(1, "Temp")
			REQUEST_VEHICLE_RECORDING(120, "Temp")
			REQUEST_VEHICLE_RECORDING(121, "Temp")
			
			REQUEST_CAM_RECORDING(1, "tempCam")
			REQUEST_CAM_RECORDING(2, "tempCam")
			REQUEST_CAM_RECORDING(3, "tempCam")
			
			WHILE NOT HAS_MODEL_LOADED(rapidgt2)
			OR NOT HAS_MODEL_LOADED(rapidgt2)
			OR NOT HAS_MODEL_LOADED(POLICE)
			OR NOT HAS_MODEL_LOADED(A_F_Y_FITNESS_02)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Temp")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(120, "Temp")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(121, "Temp")
			OR NOT HAS_CAM_RECORDING_LOADED(1, "tempCam")
			OR NOT HAS_CAM_RECORDING_LOADED(2, "tempCam")
			OR NOT HAS_CAM_RECORDING_LOADED(3, "tempCam")
				WAIT(0)
			ENDWHILE
			
			cam_main = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -3035.0415, 225.6110, 15.0973 >>,<<-0.143304,0.034611,18.666828>>,45.000000, TRUE)
			
			LOAD_SCENE(<< -3035.0415, 225.6110, 15.0973 >>)
			
				
			trailerVehicle = CREATE_VEHICLE(rapidgt2, << -3083.8896, 222.4359, 13.3367 >>, 329.6470)
			trailerVehicle2 = CREATE_VEHICLE(POLICE, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			trailerVehicle3 = CREATE_VEHICLE(MAVERICK, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			
			vehCam = CREATE_VEHICLE(rapidgt2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam2 = CREATE_VEHICLE(rapidgt2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam3 = CREATE_VEHICLE(rapidgt2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_FITNESS_02, <<0.0, 0.0, 0.0>>)
			
			SET_PED_INTO_VEHICLE(trailerPeds[0], trailerVehicle)
				
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), trailerVehicle2)
			
			SET_VEHICLE_SIREN(trailerVehicle2, TRUE)
		
			SET_VEHICLE_COLOURS(trailerVehicle, 28, 28)
			
		//.//	SET_VEHICLE_COLOURS(trailerVehicle2, carColour1, carColour1)
			
			SET_ENTITY_VISIBLE(trailerVehicle3, FALSE)
			
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(paradiseWidgetGroup)
			#ENDIF
			
			INITIALISE_UBER_PLAYBACK("Temp", TRUE)
			
		
			
			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, 1, "Temp")
			
		
			setupTrafficParadise()
			
			CREATE_ALL_WAITING_UBER_CARS()
			
		
			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, 120, "Temp")
			
			
			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle3, 121, "Temp")			
			
			
			//SET_PLAYBACK_SPEED(trailerVehicle, 0.95)
			//SET_PLAYBACK_SPEED(trailerVehicle2, 0.95)
			
			//START_CAM_RECORDING_RELATIVE_TO_ENTITY(camRecData, trailerVehicle3, vehCam, <<0.0, 0.75, -1.2>>, <<0.0, 0.0, 0.0>>, 35.0, "tempCam", 3, TRUE)
		
					
			
			START_CAM_PLAYBACK(cam_main, vehCam, "tempCam", 1)//, 32.0)
			START_CAM_PLAYBACK(cam_main, vehCam2, "tempCam", 2)//, 32.0)
			START_CAM_PLAYBACK(cam_main, vehCam3, "tempCam", 3)//, 35.0)
			
			iCameraStage = 0
			iCameraChoice = 2
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_MOTION_BLUR_STRENGTH(cam_main, 0.075)
			
			i_current_event++
			
		
			
		BREAK
		
		
		CASE 1
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
				
				//Output script to change
				
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 0") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
				
				iCameraChoice = 0
				
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 1") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 1
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
			
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 2") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 2
			ENDIF
			
			playbackCameraCuts()
			
			SWITCH iCameraChoice
				CASE 0
					UPDATE_CAM_PLAYBACK(cam_main, vehCam, 1.0)
				BREAK
			
				CASE 1
					UPDATE_CAM_PLAYBACK(cam_main, vehCam2, 1.0)
				BREAK
				
				CASE 2
					UPDATE_CAM_PLAYBACK(cam_main, vehCam3, 1.0)
				BREAK
			
			ENDSWITCH
					
			
			UPDATE_UBER_PLAYBACK(trailerVehicle, 1.0)
			
			IF fTriggerCarPlaybackTime > 95192.000
				i_current_event++
			ENDIF
			
			
		BREAK
		
		CASE 2
			STOP_CAM_PLAYBACK(vehCam)
			STOP_CAM_PLAYBACK(vehCam2)
			STOP_CAM_PLAYBACK(vehCam3)
		
			SET_MODEL_AS_NO_LONGER_NEEDED(rapidgt2)
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICE)
			SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_FITNESS_02)
			REMOVE_VEHICLE_RECORDING(1, "Temp")
			REMOVE_VEHICLE_RECORDING(120, "Temp")
			REMOVE_VEHICLE_RECORDING(121, "Temp")
			
			REMOVE_VEHICLE_RECORDING(1, "tempCam")
			REMOVE_VEHICLE_RECORDING(2, "tempCam")
			REMOVE_VEHICLE_RECORDING(3, "tempCam")
		
			//DESTROY_ALL_CAMS()
			SET_CAM_FOV(cam_main, 70.0)
			//STOP_CAM_RECORDING(camRecData)
		BREAK
		
	ENDSWITCH

ENDPROC


PROC stageChase2()
	FLOAT f_start_time = 7000.0

	IF mission_stage = STAGE_CHASE2_SECOND_SHOT
		f_start_time = 30000.0
	ENDIF

	SWITCH i_current_event
	
		CASE 0
		
			REQUEST_MODEL(SENTINEL2)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			REQUEST_VEHICLE_RECORDING(1, "Temp2")
			REQUEST_VEHICLE_RECORDING(41, "Temp2")
			REQUEST_VEHICLE_RECORDING(42, "Temp2")
			REQUEST_VEHICLE_RECORDING(43, "Temp2")
			REQUEST_VEHICLE_RECORDING(44, "Temp2")
			REQUEST_VEHICLE_RECORDING(45, "Temp2")
			
			REQUEST_CAM_RECORDING(1, "tempCam2")
			REQUEST_CAM_RECORDING(2, "tempCam2")
			REQUEST_CAM_RECORDING(3, "tempCam2")
			REQUEST_CAM_RECORDING(4, "tempCam2")
			REQUEST_CAM_RECORDING(5, "tempCam2")
			REQUEST_CAM_RECORDING(6, "tempCam2")
			REQUEST_CAM_RECORDING(7, "tempCam2")
			REQUEST_CAM_RECORDING(8, "tempCam2")
			REQUEST_CAM_RECORDING(9, "tempCam2")
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			IF mission_stage = STAGE_CHASE2
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,FALSE,0.000000,0.000000,0.000000,1.000000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0.000000,0.000000,0.000000,1.000000)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,470.590424,82.493744,96.554207,0.558219)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,452.513214,-0.710566,88.952438,0.895281)
				CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE, 0.005)
			ELIF mission_stage = STAGE_CHASE2_SECOND_SHOT
				
			ENDIF
				
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//TrailerCineVine
			SET_MAPDATACULLBOX_ENABLED("TrailerCineVine", TRUE) 
			bChase2CullEnabled = TRUE
			
			CLEANUP_UBER_PLAYBACK(TRUE)
			CLEAR_AREA(<< 514.2904, 125.7428, 120.5388 >>, 1000.0, TRUE)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<< 514.2904, 125.7428, 120.5388 >>, << -48.2689, 4.9847, -43.6235 >>,45.000000, TRUE)
			
			WHILE NOT HAS_MODEL_LOADED(SENTINEL2)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "temp2")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(41, "temp2")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(42, "temp2")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(43, "temp2")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(44, "temp2")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "temp2")
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			OR NOT HAS_CAM_RECORDING_LOADED(1, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(2, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(3, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(4, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(5, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(6, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(7, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(8, "tempCam2")
			OR NOT HAS_CAM_RECORDING_LOADED(9, "tempCam2")
				WAIT(0)
			ENDWHILE
			
			SET_CLOCK_TIME(19,0, 0)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<504.0671, 82.2990, 95.7912>>)
				
			trailerVehicle = CREATE_VEHICLE(SENTINEL2, <<504.0671, 82.2990, 95.7912>>, 329.6470)
//			trailerVehicle2 = CREATE_VEHICLE(SENTINEL2, <<508.0671, 82.2990, 95.7912>>, 99.7070)
//			trailerVehicle3 = CREATE_VEHICLE(SENTINEL2, <<512.0671, 82.2990, 95.7912>>, 99.7070)
			

			
			vehCam = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			
			vehCam2 = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam3 = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam4 = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam5 = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
			vehCam6 = CREATE_VEHICLE(SENTINEL2, << -3035.0415, 225.6110, 15.0973 >>, 99.7070)
				
	
			trailerVehicle = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
			SET_VEHICLE_NAME_DEBUG(trailerVehicle, "Veh1")
			
			trailerVehicle2 = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
			SET_VEHICLE_NAME_DEBUG(trailerVehicle2, "Veh2") //Main car
			
			LOWER_CONVERTIBLE_ROOF(trailerVehicle2, TRUE)
			
			WHILE GET_CONVERTIBLE_ROOF_STATE(trailerVehicle2) <> CRS_LOWERED
				WAIT(0)
			ENDWHILE
			
			//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), trailerVehicle2)
			
			trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle2, PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			//SET_VEHICLE_COLOURS(trailerVehicle2, 18, 18)
			//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle2, 18, 0)
			SET_VEHICLE_COLOURS(trailerVehicle2, 68, 68)
			SET_VEHICLE_EXTRA_COLOURS(trailerVehicle2, 81, 0)
			
			
			//Camera cars
//			trailerVehicle3 = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
//			SET_VEHICLE_NAME_DEBUG(trailerVehicle3, "Veh3")
//			trailerVehicle4 = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
//			SET_VEHICLE_NAME_DEBUG(trailerVehicle4, "Veh4")
//			trailerVehicle5 = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
//			SET_VEHICLE_NAME_DEBUG(trailerVehicle5, "Veh5")
//			trailerVehicle6 = CREATE_VEHICLE(SENTINEL2, << 0.0, 0.0, 0.0 >>, 99.7070)
//			SET_VEHICLE_NAME_DEBUG(trailerVehicle6, "Veh6")


			
//			SET_ENTITY_VISIBLE(trailerVehicle, FALSE)
//			SET_ENTITY_VISIBLE(trailerVehicle3, FALSE)
//			SET_ENTITY_VISIBLE(trailerVehicle4, FALSE)
//			SET_ENTITY_VISIBLE(trailerVehicle5, FALSE)
//			SET_ENTITY_VISIBLE(trailerVehicle6, FALSE)
//			
//			SET_ENTITY_COLLISION(trailerVehicle, FALSE)
//			SET_ENTITY_COLLISION(trailerVehicle3, FALSE)
//			SET_ENTITY_COLLISION(trailerVehicle4, FALSE)
//			SET_ENTITY_COLLISION(trailerVehicle5, FALSE)
//			SET_ENTITY_COLLISION(trailerVehicle6, FALSE)
			
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(paradiseWidgetGroup)
				
			#ENDIF
			
			INITIALISE_UBER_PLAYBACK("Temp2", 1, TRUE)
			
			//CREATE_ALL_WAITING_UBER_CARS() add this 
			
			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, 1, "Temp2")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, f_start_time)
			
			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, 41, "Temp2")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, f_start_time)
//			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle3, 42, "Temp2")
//			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle4, 43, "Temp2")
//			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle5, 44, "Temp2")
//			START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle6, 45, "Temp2")
			SET_UBER_PLAYBACK_TO_TIME_NOW(trailerVehicle, f_start_time)

			SET_ENTITY_VISIBLE(trailerVehicle, FALSE)
			SET_ENTITY_COLLISION(trailerVehicle, FALSE)
			
			START_CAM_PLAYBACK(cam_main, vehCam,  "tempCam2", 9)//, 32.0)
			START_CAM_PLAYBACK(cam_main, vehCam2, "tempCam2", 2)//, 32.0)
			START_CAM_PLAYBACK(cam_main, vehCam3, "tempCam2", 3)//, 35.0)
			START_CAM_PLAYBACK(cam_main, vehCam4, "tempCam2", 4)//, 32.0)
			START_CAM_PLAYBACK(cam_main, vehCam5, "tempCam2", 5)//, 22.5)
			
			START_CAM_PLAYBACK(cam_main, vehCam6, "tempCam2", 6)//, 22.5)

			
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam, f_start_time + 500.00)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam2, f_start_time + 500.00)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam3, f_start_time + 500.00)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam4, f_start_time + 500.00)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam5, f_start_time + 750.00)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCam6, f_start_time + 500.00)
			
			//SET_CAM_RECORDING_WIDGET_GROUP()
		//	START_CAM_RECORDING_RELATIVE_TO_ENTITY(camRecData, trailerVehicle3, vehCam, <<0.1, -1.75, 0.0>>, <<0.0, 0.0, 0.0>>, 22.5, "tempCam2", 6, TRUE, TRUE)		
			
			setupTrafficParadise2()
			
			//CREATE_ALL_WAITING_UBER_CARS()
									
			iCameraStage = 0
			
			IF mission_stage = STAGE_CHASE2
				iCameraChoice = 3
			ELIF mission_stage = STAGE_CHASE2_SECOND_SHOT
				iCameraChoice = 1
			ENDIF
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_MOTION_BLUR_STRENGTH(cam_main, 0.01)
			
			i_current_event++
			
		
			
		BREAK
				
		CASE 1
		
			PRINTSTRING("fTriggerCarPlaybackTime: ") PRINTFLOAT(fTriggerCarPlaybackTime) PRINTSTRING("angle ") PRINTINT(iCameraChoice) PRINTNL()
		 		 
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "Camera 1")
				
				//Output script to change
				
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 1") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
				
				iCameraChoice = 1
				
			ENDIF
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_2, KEYBOARD_MODIFIER_SHIFT, "Camera 2")
				
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 1") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 2
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_3, KEYBOARD_MODIFIER_SHIFT, "Camera 3")
			
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 3") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 3
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_4, KEYBOARD_MODIFIER_SHIFT, "Camera 4")
			
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 4") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 4
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_5, KEYBOARD_MODIFIER_SHIFT, "Camera 5")
			
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 5") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 5
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_6, KEYBOARD_MODIFIER_SHIFT, "Camera 6")
			
				SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iCameraStage) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	IF fTriggerCarPlaybackTime > ") SAVE_FLOAT_TO_DEBUG_FILE(fTriggerCarPlaybackTime) SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraChoice = 6") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		iCameraStage++") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	ENDIF") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				iCameraStage++
			
				iCameraChoice = 6
			ENDIF
			
	
			playbackCameraCuts2()
		
			SWITCH iCameraChoice
				CASE 0
				CASE 1
					UPDATE_CAM_PLAYBACK(cam_main, vehCam, fMainPlaybackSpeed)
					SET_CAM_FOV(cam_main, 22.00)
				BREAK
			
				CASE 2
					UPDATE_CAM_PLAYBACK(cam_main, vehCam2, fMainPlaybackSpeed)
					SET_CAM_FOV(cam_main, 22.00)
				BREAK
				
				CASE 3
					UPDATE_CAM_PLAYBACK(cam_main, vehCam3, fMainPlaybackSpeed)
	 				SET_CAM_FOV(cam_main, 22.00)
				BREAK
				
				CASE 4
					UPDATE_CAM_PLAYBACK(cam_main, vehCam4, fMainPlaybackSpeed)
					SET_CAM_FOV(cam_main, 22.00)
				BREAK
			
				CASE 5
					UPDATE_CAM_PLAYBACK(cam_main, vehCam5, fMainPlaybackSpeed)
					SET_CAM_FOV(cam_main, 26.5)
				BREAK
				
				
				CASE 6
					UPDATE_CAM_PLAYBACK(cam_main, vehCam6, fMainPlaybackSpeed)
					SET_CAM_FOV(cam_main, 26.5)
				BREAK
			
			ENDSWITCH
			
			DISPLAY_TEXT_WITH_NUMBER(0.05, 0.04, "", iCameraChoice)
		//	UPDATE_CAM_RECORDING(camRecData)
			
			UPDATE_UBER_PLAYBACK(trailerVehicle, fMainPlaybackSpeed)
			
			IF fTriggerCarPlaybackTime > 60000.0
			OR (fTriggerCarPlaybackTime > 29000.0 AND mission_stage = STAGE_CHASE2)
				i_current_event++
			ENDIF
			
			
		BREAK
		
		CASE 2
			STOP_CAM_PLAYBACK(vehCam)
			STOP_CAM_PLAYBACK(vehCam2)
			STOP_CAM_PLAYBACK(vehCam3)
			STOP_CAM_PLAYBACK(vehCam4)
			STOP_CAM_PLAYBACK(vehCam5)
			STOP_CAM_PLAYBACK(vehCam6)
//		
			SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL2)
			
			REMOVE_VEHICLE_RECORDING(1, "Temp2")
			REMOVE_VEHICLE_RECORDING(41, "Temp2")
			REMOVE_VEHICLE_RECORDING(42, "Temp2")
			REMOVE_VEHICLE_RECORDING(43, "Temp2")
			REMOVE_VEHICLE_RECORDING(44, "Temp2")
			REMOVE_VEHICLE_RECORDING(45, "Temp2")
			
			REMOVE_CAM_RECORDING(1, "tempCam2")
			REMOVE_CAM_RECORDING(2, "tempCam2")
			REMOVE_CAM_RECORDING(3, "tempCam2")
			REMOVE_CAM_RECORDING(4, "tempCam2")
			REMOVE_CAM_RECORDING(5, "tempCam2")
			REMOVE_CAM_RECORDING(6, "tempCam2")
			REMOVE_CAM_RECORDING(7, "tempCam2")
			REMOVE_CAM_RECORDING(8, "tempCam2")
			REMOVE_CAM_RECORDING(9, "tempCam2")

			//DESTROY_ALL_CAMS()
			CLEANUP_UBER_PLAYBACK(TRUE)
			SET_CAM_FOV(cam_main, 70.0)
			
			
		BREAK
		
	ENDSWITCH

ENDPROC


PROC stageWaitForShots()


	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<482.3925, -807.1235, 130.9988>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, TRUE)
			
			
			SET_CAM_PARAMS(cam_main, <<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF

			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 99
			
			SET_CAM_PARAMS(cam_main, <<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			SETTIMERB(0)
			i_current_event=1
		BREAK

		CASE 1
			IF TIMERB() > 100000
				i_current_event++	
			ENDIF
		BREAK
		
		CASE 2
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA",<<176.7817, -821.2585, 136.3927>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, TRUE)
			SET_CAM_PARAMS(cam_main, <<482.3925, -807.1235, 130.9988>>, <<1.0100, 0.2558, 92.6482>>, 60.0000, 100000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 3
			IF TIMERB() > 100000
				i_current_event = 99
			ENDIF
		BREAK
		
		
		
	ENDSWITCH


ENDPROC


PROC AltFirstShot()


	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1805.9865, -819.0432, 32.8839 >>, << 4.8724, 0.0000, -14.4822 >>, 50.0000, TRUE)
			
			
			SET_CAM_PARAMS(cam_main, << -1764.8693, -841.0320, 32.8839 >>, << 4.8724, -0.0000, -91.2175 >>, 50.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF

			SETTIMERB(0)
			i_current_event++
		BREAK

		
		CASE 1
	
			IF TIMERB() > GET_SHOT_LENGTH()
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
	
		BREAK
		
		
		
	ENDSWITCH


ENDPROC

PROC AltFirstShot2()


	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1434.03, 212.09, 184.95 >>, << 12.0, 0.0000, 162.01 >>, 50.0000, TRUE)
			
			
			SET_CAM_PARAMS(cam_main, << 1434.03, 212.09, 184.95 >>, << 0.12, -0.0000, 78.18 >>, 50.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF

			SETTIMERB(0)
			i_current_event++
		BREAK

		
		CASE 1
	
			IF TIMERB() > GET_SHOT_LENGTH()
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
	
		BREAK
		
		
		
	ENDSWITCH


ENDPROC

PROC AltFirstShot3()


	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << 1559.11, -2115.00, 95.28 >>, << 0.27, 0.0000, 131.70 >>, 50.0000, TRUE)
			
			
			SET_CAM_PARAMS(cam_main, << 1559.11, -2115.00, 95.28 >>, << 0.27, 0.0000, 37.69 >>, 50.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF

			SETTIMERB(0)
			i_current_event++
		BREAK

		
		CASE 1
	
			IF TIMERB() > GET_SHOT_LENGTH()
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
	
		BREAK
		
		
		
	ENDSWITCH


ENDPROC

PROC AltFirstShot4()


	SWITCH i_current_event
		CASE 0
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -386.68.9865, -2133.10, 70.54 >>, << -0.74, 0.0000, 26.15 >>, 50.0000, TRUE)
			
			
			SET_CAM_PARAMS(cam_main, << -305.75, -2133.12, 70.54 >>, << -0.74, -0.0000, -62.28 >>, 50.0000, GET_SHOT_LENGTH(), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF

			SETTIMERB(0)
			i_current_event++
		BREAK

		
		CASE 1
	
			IF TIMERB() > GET_SHOT_LENGTH()
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
	
		BREAK
		
		
		
	ENDSWITCH


ENDPROC


PROC stageCarousel()


SWITCH i_current_event
	CASE 0
		//DESTROY_ALL_CAMS()
		cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1676.8145, -1132.2067, 22.1569 >>, << 50.3973, 0.0000, -30.8295 >>, 50.0000, TRUE)
		SET_CAM_PARAMS(cam_main, << -1677.2449, -1132.9281, 21.1415 >>, << 50.3973, 0.0000, -30.8295 >>, 50.0000, 8500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

		RENDER_SCRIPT_CAMS(TRUE, FALSE)

		SETTIMERB(0)
		i_current_event++
	BREAK

	CASE 1
		IF TIMERB() > 8700
			//DESTROY_ALL_CAMS()
			SET_CAM_FOV(cam_main, 70.0)
		ENDIF
	BREAK
ENDSWITCH

ENDPROC


PROC stageCarousel_B()


SWITCH i_current_event
	CASE 0
		//DESTROY_ALL_CAMS()
		cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1674.6660, -1145.7526, 12.6749 >>, << 13.9740, -0.0000, -22.9135 >>, 50.0000, TRUE)
		SET_CAM_PARAMS(cam_main, << -1674.7487, -1144.4440, 12.6749 >>, << 14.6691, -0.0000, -22.9135 >>, 50.0000, 8500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

		RENDER_SCRIPT_CAMS(TRUE, FALSE)

		SETTIMERB(0)
		i_current_event++
	BREAK

	CASE 1
		IF TIMERB() > 8700
			//DESTROY_ALL_CAMS()
			SET_CAM_FOV(cam_main, 70.0)
		ENDIF
	BREAK
ENDSWITCH

ENDPROC

PROC stageClubThrowout()


	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			REQUEST_MODEL(A_M_Y_MotoX_02)
			REQUEST_MODEL(A_M_Y_BevHills_01)
			REQUEST_MODEL(S_M_Y_Doorman_01 )
			
			REQUEST_ANIM_DICT("TRAILER@DRUNK_EXIT")
			
			REQUEST_IPL("TRAILERshot_clubthrow")
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-541.595886,268.495880,82.106903>>,<<12.044127,-0.198895,69.701294>>,25.835318, TRUE)
			

			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_MotoX_02)
			OR NOT HAS_MODEL_LOADED(A_M_Y_BevHills_01)
			OR NOT HAS_MODEL_LOADED(S_M_Y_Doorman_01 )
			OR NOT HAS_ANIM_DICT_LOADED("TRAILER@DRUNK_EXIT")
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE_STATIONARY(VADER, trailerVehicle,<< -543.4199, 270.2294, 81.9104 >>, 178.3497 )
			
				WAIT(0)
			ENDWHILE
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				SET_VEHICLE_LIGHTS(trailerVehicle, FORCE_VEHICLE_LIGHTS_ON)
				//SET_VEHICLE_COLOURS(trailerVehicle, 84, 0)
				//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 84, 0)
				SET_VEHICLE_COLOURS(trailerVehicle, 90, 0)
				SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 90, 0)
			ENDIF
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Doorman_01 , << -556.6351, 274.6769, 82.0197 >>, 171.1936)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_BevHills_01, << -556.6351, 274.6769, 82.0197 >>, 171.1936)
			
			
			/* START SYNCHRONIZED SCENE - trailer_drunk_exit.xml */
			scenePosition = << -558.075, 272.096, 82.020 >>
			sceneRotation = << 0.000, 0.000, -96.480 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE (trailerPeds[1], sceneId, "trailer@drunk_exit", "drunk_exit_m_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "trailer@drunk_exit", "drunk_exit_m_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )


			SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.2)
			
			//TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@DRUNK_EXIT", "drunk_exit_m_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			//TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@DRUNK_EXIT", "drunk_exit_m_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			
			SET_CAM_PARAMS(cam_main,  <<-541.338806,268.007416,82.106903>>,<<10.108362,-0.198895,66.656929>>,25.835318, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			trailerPeds[2] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_MotoX_02)
			SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.35)
			SETTIMERB(0)
			i_current_event++
		BREAK
//
		CASE 1
			IF TIMERB() > 8000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

PROC stageGolf()

	SWITCH i_current_event
		CASE 0
			REQUEST_MODEL(A_M_Y_Golfer_01)
			REQUEST_MODEL(CADDY)
			//REQUEST_MODEL(PROP_GOLF_DRIVER)
			REQUEST_MODEL(PROP_GOLF_IRON_01)
			REQUEST_MODEL(PROP_GOLF_BALL)
			//REQUEST_MODEL(Prop_Golf_Bag_01)
			REQUEST_MODEL(Prop_Golf_Bag_01b)
			REQUEST_MODEL(Prop_Golf_Bag_01c)
			
			REQUEST_PTFX_ASSET()
			
			REQUEST_ANIM_DICT("TRAILER@GOLF")
			
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-1272.981201,40.955051,48.936085,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0.000000,0.000000,0.000000,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
		
			CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE, 0.002)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(42.0)
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			REQUEST_IPL("TRAILERSHOT_golf1")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1279.7787, 44.7545, 50.6703 >>, << 0.6655, -0.0000, -115.5935 >>, 41.7762, TRUE)

			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Golfer_01)
			OR NOT HAS_ANIM_DICT_LOADED("TRAILER@GOLF")
			OR NOT HAS_MODEL_LOADED(CADDY)
			//OR NOT HAS_MODEL_LOADED(PROP_GOLF_DRIVER)
			OR NOT HAS_MODEL_LOADED(PROP_GOLF_IRON_01)
			OR NOT HAS_MODEL_LOADED(PROP_GOLF_BALL)
			//OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01)
			OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01b)
			OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01c)
			OR NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1270.9558, 38.1187, 48.6748 >>, 149.7231)
			
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1273.2590, 41.7768, 48.9810 >>, 228.4267)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1274.3878, 41.4250, 49.0294 >>, 218.8317)
			
			//trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1274.3878, 41.4250, 49.0294 >>, 218.8317)
			
			trailerVehicle = CREATE_VEHICLE(CADDY, << -1270.4747, 44.3574, 48.9492 >>, 194.8641 )
			
			SET_VEHICLE_COLOURS(trailerVehicle, 26, 26)
			
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1275.7152, 32.9097, 48.4998 >>, 122.9796)
			
			//ATTACH_ENTITY_TO_ENTITY(trailerPeds[3], trailerVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehicle, "seat_dside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], <<-1275.1000, 37.5402, 48.6496>>)
			SET_ENTITY_ROTATION(trailerPeds[3], <<0.0660, 0.0184, 129.9600>>)
			
			FREEZE_ENTITY_POSITION(trailerPeds[3], TRUE)
			SET_ENTITY_COLLISION(trailerPeds[3], FALSE)
			
			oiTrailerProp = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 1.0>>)
			oiTrailerProp2 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 2.0>>)
			oiTrailerProp3 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 3.0>>)
			
			oiTrailerProp5 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 4.0>>)
			
			oiTrailerProp4 = CREATE_OBJECT(PROP_GOLF_BALL, GET_ENTITY_COORDS(trailerPeds[3]) +  <<0.0, 0.0, 5.0>>)
//			
			oiTrailerProp6 = CREATE_OBJECT(Prop_Golf_Bag_01b, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 6.0>>)
			
			oiTrailerProp7 = CREATE_OBJECT(Prop_Golf_Bag_01c, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 7.0>>)
//		
			
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0],  GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp4, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[1], GET_PED_BONE_INDEX(trailerPeds[1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp3, trailerPeds[2], GET_PED_BONE_INDEX(trailerPeds[2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp5, trailerPeds[3], GET_PED_BONE_INDEX(trailerPeds[3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			IF NOT IS_PED_INJURED(trailerPeds[3])
				SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], vBenchPosition)
				SET_ENTITY_ROTATION(trailerPeds[3], vBenchRotation)
			ENDIF
			
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)

			
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 0)
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 4), 0, 2, 0) //(lowr)

			//SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[0], << -1270.9558, 38.1187, 49.6248 >>)
			//FREEZE_ENTITY_POSITION(trailerPeds[0], TRUE)
			
			scenePosition = << -1270.460, 39.070, 48.655 >>
			sceneRotation = << 0.000, 0.000, 59.000 >>

			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			
			TASK_SYNCHRONIZED_SCENE (trailerPeds[0], sceneId, "TRAILER@GOLF", "002077_01_gc_golf_drive_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			
			//TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//			
			TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@GOLF", "002077_01_gc_golf_drive_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[2], "TRAILER@GOLF", "002077_01_gc_golf_drive_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[3], "TRAILER@GOLF", "002077_01_gc_golf_drive_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			//SET_CAM_PARAMS(cam_main, << -1279.7787, 44.7545, 50.6703 >>, << 10.9501, -0.0000, -115.5935 >>, 41.7762, 3500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.25)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 6000
				SET_CAM_PARAMS(cam_main, << -1279.7787, 44.7545, 50.6703 >>, << 10.9501, -0.0000, -115.5935 >>, 41.7762, 3500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
				SHAKE_CAM(cam_main, "HAND_SHAKE", 0.25)
				SETTIMERB(0)
				i_current_event++
			ENDIF
		BREAK
		
		
		CASE 2
			IF NOT IS_PED_INJURED(trailerPeds[0])
				//IF IS_ENTITY_PLAYING_ANIM(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d")
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					//IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d") > 0.72
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.72
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_golf_strike", << -1271.0563, 37.2104, 48.7628 >>,  <<0.0, 0.0, 0.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 3
			IF TIMERB() > 4000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF

			IF NOT IS_PED_INJURED(trailerPeds[3])
				SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], vBenchPosition)
				SET_ENTITY_ROTATION(trailerPeds[3], vBenchRotation)
			ENDIF

		BREAK
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(oiTrailerProp6)
	AND NOT IS_ENTITY_DEAD(trailerVehicle)
		ATTACH_ENTITY_TO_ENTITY(oiTrailerProp6, trailerVehicle, 0, vGolfBagOffset, <<0.0, 0.0, 6.0>>)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiTrailerProp7)
	AND NOT IS_ENTITY_DEAD(trailerVehicle)
		ATTACH_ENTITY_TO_ENTITY(oiTrailerProp7, trailerVehicle, 0, <<vGolfBagOffset.x * -1.0, vGolfBagOffset.y, vGolfBagOffset.z>>, <<0.0, 0.0, -2.0>>)
	ENDIF

ENDPROC

PROC stageGolfShot2()

	SWITCH i_current_event
		CASE 0
			REQUEST_MODEL(A_M_Y_Golfer_01)
			REQUEST_MODEL(CADDY)
			//REQUEST_MODEL(PROP_GOLF_DRIVER)
			REQUEST_MODEL(PROP_GOLF_IRON_01)
			REQUEST_MODEL(PROP_GOLF_BALL)
			//REQUEST_MODEL(Prop_Golf_Bag_01)
			REQUEST_MODEL(Prop_Golf_Bag_01b)
			REQUEST_MODEL(Prop_Golf_Bag_01c)
						
			REQUEST_ANIM_DICT("TRAILER@GOLF")
			//DESTROY_ALL_CAMS()
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			REQUEST_IPL("TRAILERSHOT_golf2")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-1269.302368,36.470341,48.577194>>,<<25.869299,-0.055910,51.154312>>,23.657412, TRUE)

			WHILE NOT HAS_MODEL_LOADED(A_M_Y_Golfer_01)
			OR NOT HAS_ANIM_DICT_LOADED("TRAILER@GOLF")
			OR NOT HAS_MODEL_LOADED(CADDY)
			//OR NOT HAS_MODEL_LOADED(PROP_GOLF_DRIVER)
			OR NOT HAS_MODEL_LOADED(PROP_GOLF_IRON_01)
			OR NOT HAS_MODEL_LOADED(PROP_GOLF_BALL)
			//OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01)
			OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01b)
			OR NOT HAS_MODEL_LOADED(Prop_Golf_Bag_01c)
				WAIT(0)
			ENDWHILE
			
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1270.9558, 38.1187, 48.6748 >>, 149.7231)
			
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1273.2590, 41.7768, 48.9810 >>, 228.4267)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1274.3878, 41.4250, 49.0294 >>, 218.8317)
			
			//trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1274.3878, 41.4250, 49.0294 >>, 218.8317)
			
			trailerVehicle = CREATE_VEHICLE(CADDY, << -1270.4747, 44.3574, 48.9492 >>, 194.8641 )
			
			SET_VEHICLE_COLOURS(trailerVehicle, 26, 26)
			
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_GOLFER_01, << -1275.7152, 32.9097, 48.4998 >>, 122.9796)
			
			//ATTACH_ENTITY_TO_ENTITY(trailerPeds[3], trailerVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehicle, "seat_dside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], <<-1275.1000, 37.5402, 48.6496>>)
			SET_ENTITY_ROTATION(trailerPeds[3], <<0.0660, 0.0184, 129.9600>>)
			
			FREEZE_ENTITY_POSITION(trailerPeds[3], TRUE)
			SET_ENTITY_COLLISION(trailerPeds[3], FALSE)
			
			oiTrailerProp = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 1.0>>)
			oiTrailerProp2 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 2.0>>)
			oiTrailerProp3 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 3.0>>)
			
			oiTrailerProp5 = CREATE_OBJECT(PROP_GOLF_IRON_01, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 4.0>>)
			
			oiTrailerProp4 = CREATE_OBJECT(PROP_GOLF_BALL, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 5.0>>)
//			
			oiTrailerProp6 = CREATE_OBJECT(Prop_Golf_Bag_01b, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 6.0>>)
			
			oiTrailerProp7 = CREATE_OBJECT(Prop_Golf_Bag_01c, GET_ENTITY_COORDS(trailerPeds[3]) + <<0.0, 0.0, 7.0>>)
//		
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[0],  GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp4, trailerPeds[0], GET_PED_BONE_INDEX(trailerPeds[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[1], GET_PED_BONE_INDEX(trailerPeds[1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp3, trailerPeds[2], GET_PED_BONE_INDEX(trailerPeds[2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp5, trailerPeds[3], GET_PED_BONE_INDEX(trailerPeds[3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			IF NOT IS_PED_INJURED(trailerPeds[3])
				SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], vBenchPosition)
				SET_ENTITY_ROTATION(trailerPeds[3], vBenchRotation)
			ENDIF
			
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			//SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)

			
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(trailerPeds[0], ANCHOR_EYES, 0)
						
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 1, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)


			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 4), 0, 2, 0) //(lowr)

			
			
			TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//			
			TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@GOLF", "002077_01_gc_golf_drive_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[2], "TRAILER@GOLF", "002077_01_gc_golf_drive_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[3], "TRAILER@GOLF", "002077_01_gc_golf_drive_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			//SET_CAM_PARAMS(cam_main, << -1279.7787, 44.7545, 50.6703 >>, << 10.9501, -0.0000, -115.5935 >>, 41.7762, 3500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

			SET_CAM_PARAMS(cam_main, <<-1269.374146,36.460720,48.695023>>,<<26.216413,-0.055911,50.168285>>,23.657412, 3500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_CAM_MOTION_BLUR_STRENGTH(cam_main, 0.2)
						
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.25)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF NOT IS_PED_INJURED(trailerPeds[0])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d")
					SET_ANIM_PHASE(trailerPeds[0], 0.62)
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 2
			IF NOT IS_PED_INJURED(trailerPeds[0])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[0], "TRAILER@GOLF", "002077_01_gc_golf_drive_d") > 0.72
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_golf_strike", << -1271.0563, 37.2104, 48.7628 >>,  <<0.0, 0.0, 0.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 3
			IF TIMERB() > 10000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
		
			ENDIF

			IF NOT IS_PED_INJURED(trailerPeds[3])
				SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], vBenchPosition)
				SET_ENTITY_ROTATION(trailerPeds[3], vBenchRotation)
			ENDIF

		BREAK
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(oiTrailerProp6)
	AND NOT IS_ENTITY_DEAD(trailerVehicle)
		ATTACH_ENTITY_TO_ENTITY(oiTrailerProp6, trailerVehicle, 0, vGolfBagOffset, <<0.0, 0.0, 6.0>>)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiTrailerProp7)
	AND NOT IS_ENTITY_DEAD(trailerVehicle)
		ATTACH_ENTITY_TO_ENTITY(oiTrailerProp7, trailerVehicle, 0, <<vGolfBagOffset.x * -1.0, vGolfBagOffset.y, vGolfBagOffset.z>>, <<0.0, 0.0, -2.0>>)
	ENDIF

ENDPROC


PROC stageAlleyChase()

	SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)

	SWITCH i_current_event
		CASE 0
			REQUEST_ANIM_DICT("trailer@criminals")
			REQUEST_ANIM_DICT("TRAILER@DOG")
			REQUEST_PTFX_ASSET()
			
			REQUEST_MODEL(S_M_Y_COP_01)
			REQUEST_MODEL(G_M_Y_Strpunk_02 )
			REQUEST_MODEL(S_M_M_PILOT_01)
			REQUEST_MODEL(POLMAV)
			REQUEST_MODEL(POLICE)
			REQUEST_MODEL(A_C_ROTTWEILER)
			
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			//timecycle =clearing(urban) @ 16:00extra light IPL= trailershot_alley
			IF mission_stage = STAGE_ALLEY_CHASE_DAY
				SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
			ELSE
				SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
			ENDIF
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			REQUEST_IPL("TRAILERSHOT_ALLEY")
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(37.0)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV(47.0)
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-117.608574,-1612.432373,30.943308,2.809813)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-123.477402,-1617.517090,31.019413,2.586250)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,-138.057632,-1633.734619,31.326286,1.404562)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
					
			i_current_event++
		BREAK
		
		CASE 1
			IF HAS_ANIM_DICT_LOADED("trailer@criminals")
			AND HAS_ANIM_DICT_LOADED("trailer@DOG")
			AND HAS_PTFX_ASSET_LOADED()
			AND HAS_MODEL_LOADED(S_M_Y_COP_01)
			AND HAS_MODEL_LOADED(G_M_Y_Strpunk_02 )
			AND HAS_MODEL_LOADED(S_M_M_PILOT_01)
			AND HAS_MODEL_LOADED(POLMAV)
			AND HAS_MODEL_LOADED(POLICE)
			AND HAS_MODEL_LOADED(A_C_ROTTWEILER)
			
				i_current_event++
			ENDIF
		BREAK

		CASE 2
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_920-1520_a.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_920-1520_b.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_920-1520_c.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_920-1520_d.anim
			
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_a.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_b.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_c.anim
			//depot/gta5/art/anim/export_mb/TRAILER@/CRIMINALS/002076_04_GC_CRIM_RUN_1_d.anim


			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerPolMav", POLMAV, trailerVehicle, 1000.0, 0.8)
				WAIT(0)
			ENDWHILE
			SET_PLAYBACK_SPEED(trailerVehicle, 0.0)
			SET_HELI_BLADES_FULL_SPEED(trailerVehicle)
			trailerPeds[4] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_M_M_PILOT_01)
			SET_PED_PROP_INDEX(trailerPeds[4], ANCHOR_HEAD, 0, 0)
			
			
			trailerVehicle2 = CREATE_VEHICLE(POLICE, << -105.0974, -1596.7554, 30.6149 >>, 164.7702)
			trailerVehicle3 = CREATE_VEHICLE(POLICE, << -117.3874, -1606.4032, 31.1291 >>, 131.3932)
			
			SET_VEHICLE_SIREN(trailerVehicle2, TRUE)
			SET_VEHICLE_DOOR_CONTROL(trailerVehicle2, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 1.0)
			//SET_VEHICLE_DOOR_CONTROL(trailerVehicle2, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.75)
			
			SET_VEHICLE_SIREN(trailerVehicle3, TRUE)
			SET_VEHICLE_DOOR_CONTROL(trailerVehicle3, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.8)
		SET_VEHICLE_DOOR_CONTROL(trailerVehicle3, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.99)
						
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -126.1536, -1620.5604, 31.0477 >>, 138.9211)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -129.1582, -1621.4021, 31.1574 >>, 138.9211)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -125.8053, -1617.8832, 31.1017 >>, 138.9211)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_Strpunk_02 , << -136.4161, -1631.4719, 31.2683 >>, 138.9211 )
			
			
			trailerPeds[5] = CREATE_PED(PEDTYPE_MISSION, A_C_ROTTWEILER, vDogPos, fDoGHeading)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trailerPeds[5], TRUE)
			
			scenePosition = << -127.624, -1613.902, 32.056 >>
			sceneRotation = << 0.000, 0.000, -131.600 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE(trailerPeds[5], sceneId, "trailer@dog", "jumpingfence", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			
			//TASK_PLAY_ANIM(trailerPeds[5], "TRAILER@DOG", "jumpingfence", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, - 1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			//FREEZE_ENTITY_POSITION(trailerPeds[5], TRUE)
			
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 5), 0, 0, 0) //(hand)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 8), 3, 2, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 9), 1, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 10), 0, 1, 0) //(decl)
			SET_PED_PROP_INDEX(trailerPeds[2], ANCHOR_HEAD, 0, 0)
			
			//Could he also have a hat on - p_head_000 texture 0
			//The middle guy:
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 5), 1, 0, 0) //(hand)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 9), 2, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(trailerPeds[1], INT_TO_ENUM(PED_COMPONENT, 10), 0, 0, 0) //(decl)
			
			SET_PED_PROP_INDEX(trailerPeds[1], ANCHOR_HEAD, 0, 0)
			
			//Also with a hatThe rightmost guy
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 1, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 5), 0, 1, 0) //(hand)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 6), 0, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 9), 0, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 10), 0, 0, 0) //(decl)
			
			
			
			//SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], PED_COMP_TORSO, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], PED_COMP_LEG, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], PED_COMP_HEAD, 1, 2) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], PED_COMP_SPECIAL, 1, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) //(hair)
			//SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 9), 1, 0, 0) //(task)
			
			/*Uppr_000 - texture a
			Lowr_000 - texture a
			Head_001 - texture c
			Accs_001 - texture a*/
			
			IF NOT IS_VEHICLE_SEARCHLIGHT_ON(trailerVehicle)
				SET_VEHICLE_SEARCHLIGHT(trailerVehicle,true)
			ENDIF
			
			WHILE TIMERB() < 3000
				
								
				if IS_VEHICLE_DRIVEABLE(trailerVehicle)
	      		
					//ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
	      			
					IF NOT  IS_PED_INJURED(trailerPeds[4])
					AND NOT IS_PED_INJURED(trailerPeds[3])
		        	    TASK_VEHICLE_AIM_AT_COORD(trailerPeds[4],GET_ENTITY_COORDS(trailerPeds[3]))
					ENDIF
				ENDIF
				WAIT(0)
			ENDWHILE
			

			
			//DESTROY_ALL_CAMS()
			IF mission_stage = STAGE_ALLEY_CHASE_DAY
				cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-140.912918,-1638.378784,31.770519>>,<<8.708959,-0.443657,-33.466763>>,24.602549, TRUE)
				SET_CAM_PARAMS(cam_main,  <<-149.332550,-1648.648438,31.770519>>,<<8.708960,2.623013,-32.399677>>,24.602549, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			ELSE
				cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-99.946846,-1580.827271,54.399387>>,<<-21.017738,-0.191652,148.608368>>,26.318777, TRUE)
				SET_CAM_PARAMS(cam_main,  <<-99.946846,-1580.827271,54.399387>>,<<-21.017738,-0.191652,148.608368>>,25.243645, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			ENDIF

			IF NOT IS_PED_INJURED(trailerPeds[0])
				TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			IF NOT IS_PED_INJURED(trailerPeds[1])
				TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			IF NOT IS_PED_INJURED(trailerPeds[2])
				TASK_PLAY_ANIM(trailerPeds[2], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			IF NOT IS_PED_INJURED(trailerPeds[3])
				TASK_PLAY_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			ENDIF


			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.35)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			//IF mission_stage =  STAGE_ALLEY_CHASE_DAY
				//timecycle =clearing(urban) @ 16:00extra light IPL= trailershot_alley
				//SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
				//SET_TIMECYCLE_REGION_OVERRIDE(0)
				//REQUEST_IPL("TRAILERSHOT_ALLEY")
			//ENDIF

			SETTIMERB(0)
			i_current_event++
		BREAK
		
		CASE 3
			
			IF NOT IS_PED_INJURED(trailerPeds[5])
				//IF IS_ENTITY_PLAYING_ANIM(trailerPeds[5], "TRAILER@DOG", "jumpingfence")
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneId, fDogPhase)
					//SET_ANIM_PHASE(trailerPeds[5], fDogPhase)
					i_current_event++
				ENDIF
			ENDIF
		
		BREAK

		CASE 4
			IF NOT IS_PED_INJURED(trailerPeds[3])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d") > 0.1
						VECTOR v_foot_pos 
						v_foot_pos = GET_PED_BONE_COORDS(trailerPeds[3], BONETAG_L_FOOT, <<0.0, 0.0, -0.05>>)
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_puddle_splash", v_foot_pos, <<0.0, 0.0, 142.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(trailerPeds[3])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d") > 0.1666
						VECTOR v_foot_pos 
						v_foot_pos = GET_PED_BONE_COORDS(trailerPeds[3], BONETAG_R_FOOT, <<0.0, 0.0, -0.05>>)
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_puddle_splash", v_foot_pos, <<0.0, 0.0, 142.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_PED_INJURED(trailerPeds[3])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d") > 0.2333
						VECTOR v_foot_pos 
						v_foot_pos = GET_PED_BONE_COORDS(trailerPeds[3], BONETAG_L_FOOT, <<0.0, 0.0, -0.05>>)
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_puddle_splash", v_foot_pos, <<0.0, 0.0, 142.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF NOT IS_PED_INJURED(trailerPeds[3])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d") > 0.2987
						VECTOR v_foot_pos 
						v_foot_pos = GET_PED_BONE_COORDS(trailerPeds[3], BONETAG_R_FOOT, <<0.0, 0.0, -0.05>>)
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_puddle_splash", v_foot_pos, <<0.0, 0.0, 142.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF NOT IS_PED_INJURED(trailerPeds[3])
				IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
					IF GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d") > 0.3608
						VECTOR v_foot_pos 
						v_foot_pos = GET_PED_BONE_COORDS(trailerPeds[3], BONETAG_L_FOOT, <<0.0, 0.0, -0.05>>)
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trailer_puddle_splash", v_foot_pos, <<0.0, 0.0, 142.0>>)
						i_current_event++
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 9
			IF TIMERB() > 4000
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH
	
	if IS_VEHICLE_DRIVEABLE(trailerVehicle)
    	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle)
			SET_PLAYBACK_SPEED(trailerVehicle, 0.8)
			//ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
	    	
			IF NOT IS_PED_INJURED(trailerPeds[4])
			AND NOT IS_PED_INJURED(trailerPeds[3])
			    TASK_VEHICLE_AIM_AT_COORD(trailerPeds[4],GET_ENTITY_COORDS(trailerPeds[3]))
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_PED_INJURED(trailerPeds[3])
			IF IS_ENTITY_PLAYING_ANIM(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d")
				//PRINTLN(GET_ENTITY_ANIM_CURRENT_TIME(trailerPeds[3], "TRAILER@CRIMINALS", "002076_04_GC_CRIM_RUN_1_d"))
				
				//0.1 (left)
				//0.1666
				//0.2333
				//0.2987
				//0.3608
			ENDIF
		ENDIF	
	#ENDIF

ENDPROC


PROC stageWorkLine()

	SWITCH i_current_event
		CASE 0
			REQUEST_ANIM_DICT("TRAILER@WORK_LINE_1")
			
			REQUEST_ANIM_DICT("AMB@MOBILE@MALE@STATIONARY@CALL@IDLE_A")
			
			REQUEST_ANIM_DICT("AMB@SMOKING@STANDING@MALE@IDLE_A")
			REQUEST_ANIM_DICT("AMB@LEAN@MALE@STATIONARY@SMOKING@IDLE_A")
			
			REQUEST_ANIM_DICT("AMB@BUSSTOP@CHAR1@IDLE_A")
			
			REQUEST_IPL("SP1_02_SHOT_work")
			
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			REQUEST_MODEL(Prop_CS_Ciggy_01)
			REQUEST_MODEL(Prop_Phone_ING)

			REQUEST_MODEL(S_M_M_Migrant_01)
			REQUEST_MODEL(S_M_M_Gardener_01)
			REQUEST_MODEL(S_M_M_AutoShop_01)
			REQUEST_MODEL(A_M_M_MexLabor_01)
			
			i_current_event++
		BREAK
		
		CASE 1
			IF HAS_ANIM_DICT_LOADED("TRAILER@WORK_LINE_1")
			AND HAS_ANIM_DICT_LOADED("AMB@MOBILE@MALE@STATIONARY@CALL@IDLE_A")
			AND HAS_ANIM_DICT_LOADED("AMB@SMOKING@STANDING@MALE@IDLE_A")
			AND HAS_ANIM_DICT_LOADED("AMB@BUSSTOP@CHAR1@IDLE_A")
			AND HAS_MODEL_LOADED(S_M_M_Migrant_01)
			AND HAS_MODEL_LOADED(S_M_M_Gardener_01)
			AND HAS_MODEL_LOADED(S_M_M_AutoShop_01)
			AND HAS_MODEL_LOADED(A_M_M_MexLabor_01)
			AND HAS_MODEL_LOADED(Prop_CS_Ciggy_01)
			AND HAS_MODEL_LOADED(Prop_Phone_ING)

				i_current_event++
			ENDIF
		BREAK
//A_M_M_MexLabor_01 
		CASE 2
								
			trailerPeds[0] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01 ,   <<-260.1057, -1216.5510, 25.4528>>, 98.2)
			trailerPeds[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_MexLabor_01 ,  <<-260.8085, -1215.2061, 25.3220>>, 38.1600)
			trailerPeds[2] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01 , << -261.7828, -1214.0167, 24.2270 >>, 1.4112)
			trailerPeds[3] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Gardener_01, << -261.6500, -1211.3795, 24.0114 >>, 358.8112 )
			trailerPeds[4] = CREATE_PED(PEDTYPE_MISSION, S_M_M_AutoShop_01 , << -261.6086, -1208.5464, 23.7811 >>, 359.0558)
			trailerPeds[5] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01, << -261.6521, -1205.4790, 23.5487 >>, 0.8371 )
			trailerPeds[6] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Gardener_01, << -261.6384, -1201.4806, 23.2558 >>, 354.3695 )
		
			trailerPeds[7] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01 ,  << -261.5668, -1194.2793, 22.8029 >>, 202.6970)
			trailerPeds[8] = CREATE_PED(PEDTYPE_MISSION, A_M_M_MexLabor_01 , <<-260.535828,-1196.641357,23.940573>>, 165.3288)
			trailerPeds[9] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01 , << -260.9562, -1200.3579, 23.1826 >>, 113.3534)
			trailerPeds[10] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Gardener_01,<< -260.5458, -1203.2354, 23.5625 >>, 77.2623)
			trailerPeds[11] = CREATE_PED(PEDTYPE_MISSION, S_M_M_AutoShop_01 , << -260.8011, -1204.4816, 23.4749 >>, 187.4545 )
			trailerPeds[12] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Migrant_01, << -262.1537, -1200.3533, 23.1823 >>, 181.5052)
			trailerPeds[13] = CREATE_PED(PEDTYPE_MISSION, A_M_M_MexLabor_01, << -261.0477, -1209.6021, 23.8669 >>, 216.7605)


			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[8], <<-260.9037, -1196.3790, 23.9581>>)
			SET_ENTITY_ROTATION(trailerPeds[8], <<0.0174, -0.0663, 136.8000>>)
		
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 10), 0, 1, 0) //(decl)
		
			
			//Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[0], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
//			//Loading ped model: S_M_M_Gardener_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 0, 0, 1, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 3, 1, 2, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 4, 1, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 8, 1, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 10, 0, 1, 0) //(decl)
			
			//Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 3), 2, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[2], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
			//Loading ped model: S_M_M_Gardener_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 3), 1, 4, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[3], INT_TO_ENUM(PED_COMPONENT, 10), 1, 1, 0) //(decl)
			
//			//Loading ped model: S_M_M_Migrant_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 0, 1, 1, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 3, 2, 0, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 4, 1, 0, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 6, 1, 0, 0) //(feet)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 8, 1, 0, 0) //(accs)
			
			//This guy is currently sitting at the T:Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[5], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
			//Loading ped model: S_M_M_Gardener_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 0), 0, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[6], INT_TO_ENUM(PED_COMPONENT, 10), 0, 1, 0) //(decl)
		
			//Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT, 0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT, 3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[7], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
//			//Loading ped model: S_M_M_Gardener_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 0, 0, 1, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 3, 1, 2, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 4, 1, 1, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 8, 1, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[1], 10, 0, 1, 0) //(decl)
			
			//Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[9], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[9], INT_TO_ENUM(PED_COMPONENT, 3), 2, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[9], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[9], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[9], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
			//Loading ped model: S_M_M_Gardener_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[10], INT_TO_ENUM(PED_COMPONENT, 0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[10], INT_TO_ENUM(PED_COMPONENT, 3), 1, 4, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[10], INT_TO_ENUM(PED_COMPONENT, 4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[10], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(trailerPeds[10], INT_TO_ENUM(PED_COMPONENT, 10), 1, 1, 0) //(decl)
			
//			//Loading ped model: S_M_M_Migrant_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 0, 1, 1, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 3, 2, 0, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 4, 1, 0, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 6, 1, 0, 0) //(feet)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[4], 8, 1, 0, 0) //(accs)
			
			//This guy is currently sitting at the T:Loading ped model: S_M_M_Migrant_01
			SET_PED_COMPONENT_VARIATION(trailerPeds[12], INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(trailerPeds[12], INT_TO_ENUM(PED_COMPONENT, 3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[12], INT_TO_ENUM(PED_COMPONENT, 4), 1, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(trailerPeds[12], INT_TO_ENUM(PED_COMPONENT, 6), 1, 0, 0) //(feet)
			SET_PED_COMPONENT_VARIATION(trailerPeds[12], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
			
			//Loading ped model: S_M_M_Gardener_01
//			SET_PED_COMPONENT_VARIATION(trailerPeds[13], INT_TO_ENUM(PED_COMPONENT, 0), 0, 2, 0) //(head)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[13], INT_TO_ENUM(PED_COMPONENT, 3), 1, 0, 0) //(uppr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[13], INT_TO_ENUM(PED_COMPONENT, 4), 1, 2, 0) //(lowr)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[13], INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) //(accs)
//			SET_PED_COMPONENT_VARIATION(trailerPeds[13], INT_TO_ENUM(PED_COMPONENT, 10), 0, 1, 0) //(decl)
//		
		
			
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[0], <<-259.947357,-1216.394165,25.433516>>)
			SET_ENTITY_ROTATION(trailerPeds[0], <<-1.0178, -0.0000, 98.2800>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[1], <<-260.8085, -1215.2061, 25.3220>>)
			SET_ENTITY_ROTATION(trailerPeds[1], <<-0.0004, 0.0791, 38.1600>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[2], <<-259.608978,-1212.833740,25.129515>>)
			SET_ENTITY_ROTATION(trailerPeds[2], <<0.0020, 2.7791, 103.3200>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[3], <<-260.364319,-1210.571777,24.945757>>)
			SET_ENTITY_ROTATION(trailerPeds[3], <<-0.0016, 0.0791, 58.3200>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[4], <<-260.367432,-1208.282227,24.759613>>)
			SET_ENTITY_ROTATION(trailerPeds[4], <<-0.0011, 0.0685, 95.0400>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[5], <<-259.909393,-1205.492432,24.549686>>)
			SET_ENTITY_ROTATION(trailerPeds[5], <<0.0010, 0.0685, 91.8000>>)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[6], <<-260.578308,-1202.107788,24.299198>>)
			SET_ENTITY_ROTATION(trailerPeds[6], <<0.0000, 3.2400, 88.2000>>)
			
			FOR iterator = 0 TO 6
				FREEZE_ENTITY_POSITION(trailerPeds[iterator], TRUE)
			ENDFOR
						
			TASK_PLAY_ANIM(trailerPeds[0], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[1], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[2], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[3], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[4], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_e", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[5], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			TASK_PLAY_ANIM(trailerPeds[6], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			TASK_PLAY_ANIM(trailerPeds[7], "AMB@SMOKING@STANDING@MALE@IDLE_A", "IDLE_A", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			oiTrailerProp = CREATE_OBJECT(Prop_CS_Ciggy_01, GET_PED_BONE_COORDS(trailerPeds[7], BONETAG_PH_R_HAND, <<0.0, 0.0, 0.0>>))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp, trailerPeds[7], GET_PED_BONE_INDEX(trailerPeds[7], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[10], <<-260.180359,-1202.708008,24.343624>>)
			SET_ENTITY_ROTATION(trailerPeds[10], <<0.0668, 0.0151, 166.6800>>)
			FREEZE_ENTITY_POSITION(trailerPeds[10], TRUE)
			TASK_PLAY_ANIM(trailerPeds[10], "AMB@LEAN@MALE@STATIONARY@SMOKING@IDLE_A", "IDLE_A", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			oiTrailerProp3 = CREATE_OBJECT(Prop_CS_Ciggy_01, GET_PED_BONE_COORDS(trailerPeds[10], BONETAG_PH_R_HAND, <<0.0, 0.0, 0.0>>))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp3, trailerPeds[10], GET_PED_BONE_INDEX(trailerPeds[10], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			//TASK_PLAY_ANIM(trailerPeds[11], "AMB@LEAN@MALE@STATIONARY@SMOKING@IDLE_A", "IDLE_C", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)

			SET_ENTITY_COORDS_NO_OFFSET(trailerPeds[8], <<-260.535828,-1196.641357,23.940573>>)
			FREEZE_ENTITY_POSITION(trailerPeds[8], TRUE)		
			TASK_PLAY_ANIM(trailerPeds[8], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			
			TASK_PLAY_ANIM(trailerPeds[12], "AMB@MOBILE@MALE@STATIONARY@CALL@IDLE_A", "IDLE_A", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			oiTrailerProp2 = CREATE_OBJECT(Prop_Phone_ING , GET_PED_BONE_COORDS(trailerPeds[12], BONETAG_PH_R_HAND, <<0.0, 0.0, 0.0>>))
			ATTACH_ENTITY_TO_ENTITY(oiTrailerProp2, trailerPeds[12], GET_PED_BONE_INDEX(trailerPeds[12], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)

			
			TASK_PLAY_ANIM(trailerPeds[13], "TRAILER@WORK_LINE_1", "work_line_(a+b)_ranges_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerWorkLine", bodhi2, trailerVehicle, 1500)
				WAIT(0)
			ENDWHILE
					
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				
				trailerPeds[14] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_M_M_Gardener_01)
				trailerPeds[15] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_M_M_AutoShop_01 , VS_FRONT_RIGHT)
				trailerPeds[16] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, S_M_M_Migrant_01, VS_BACK_LEFT)
				trailerPeds[17] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_M_MexLabor_01, VS_BACK_RIGHT)
				
				SET_PED_COMPONENT_VARIATION(trailerPeds[14], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(trailerPeds[14], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[14], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[14], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(trailerPeds[14], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
				
				SET_PED_COMPONENT_VARIATION(trailerPeds[15], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(trailerPeds[15], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[15], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[15], INT_TO_ENUM(PED_COMPONENT,10), 0, 2, 0) //(decl)
				
				SET_PED_COMPONENT_VARIATION(trailerPeds[16], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(trailerPeds[16], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[16], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[16], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(trailerPeds[16], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				
				SET_PED_COMPONENT_VARIATION(trailerPeds[17], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(trailerPeds[17], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(trailerPeds[17], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				
				IF NOT IS_PED_INJURED(trailerPeds[16])
					TASK_PLAY_ANIM(trailerPeds[16], "TRAILER@WORK_LINE_1", "Sit_Rear_DS", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				
				IF NOT IS_PED_INJURED(trailerPeds[17])
					TASK_PLAY_ANIM(trailerPeds[17], "TRAILER@WORK_LINE_1", "Sit_Rear_PS", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
			ENDIF
			
			FOR iterator = 0 TO 17
				IF DOES_ENTITY_EXIST(trailerPeds[iterator])
					IF NOT IS_ENTITY_DEAD(trailerPeds[iterator])
						debugPedName = "TRPed "
						debugPedName += iterator
						SET_PED_NAME_DEBUG(trailerPeds[iterator], debugPedName)
					ENDIF
				ENDIF
			ENDFOR	
			
			//TrailerWorkLine
			SET_MAPDATACULLBOX_ENABLED("TrailerWorkLine", TRUE) 
			bWorklineCullEnabled = TRUE
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(3.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(3.0)
			IF bFirstTimePlayingThisShot
				INSTANTLY_FILL_VEHICLE_POPULATION()
			ENDIF
			
			//DESTROY_ALL_CAMS()
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-266.859589,-1228.904297,26.957239>>,<<-4.011270,0.235184,-14.847782>>,45.000000, TRUE)
			SET_CAM_PARAMS(cam_main,  <<-266.987976,-1221.967529,26.323891>>,<<-1.303318,0.235183,-19.100765>>,45.000000, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)


			SHAKE_CAM(cam_main, "HAND_SHAKE", 1.0)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK
//
		CASE 3
			REQUEST_MODEL(A_M_Y_SouCent_02)
			//REQUEST_ANIM_DICT("move_m@generic")
			
			IF TIMERB() < 1500
				IF NOT IS_ENTITY_DEAD(trailerVehicle)
					SET_VEHICLE_INDICATOR_LIGHTS(trailerVehicle, TRUE, TRUE)	
				ENDIF
			ENDIF
			
			IF TIMERB() > 3000
				IF HAS_MODEL_LOADED(A_M_Y_SouCent_02)
					trailerPeds[18] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_SouCent_02, <<-288.759827,-1125.032593,21.972956>>, -14.007539)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_SouCent_02)
					
					SET_PED_COMPONENT_VARIATION(trailerPeds[18], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(trailerPeds[18], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(trailerPeds[18], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(trailerPeds[18], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_PROP_INDEX(trailerPeds[18], ANCHOR_EYES, 1, 0)
					
					//TASK_GO_STRAIGHT_TO_COORD(trailerPeds[18], <<-282.1218, -1105.9077, 22.3657>>, PEDMOVE_WALK, -1, 341.8674)
					TASK_PLAY_ANIM(trailerPeds[18], "move_m@generic", "walk", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING, 0.5)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(trailerPeds[18])
				
					SET_CAM_PARAMS(cam_main,<<-289.184235,-1120.747681,23.348108>>,<<-0.267122,0.384143,-160.916382>>,24.001860)
					SET_CAM_PARAMS(cam_main,<<-289.184235,-1120.747681,23.348108>>,<<-0.267122,0.384143,-160.916382>>,20.242884, 2800, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					SHAKE_CAM(cam_main, "HAND_SHAKE", 1.0)
					SETTIMERB(0)
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 4
			IF TIMERB() > 4000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
//			
		BREAK
	ENDSWITCH

ENDPROC


PROC stageRockstarGamesPresents()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_M_Y_JetSki_01)
			REQUEST_IPL("TRAILERSHOT_citypan")
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_JetSki_01)
				WAIT(0)
			ENDWHILE
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerHeli", MAVERICK, trailerVehicle, 1000)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_LOD_DIST(trailerVehicle, 3000)
			
			
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_JetSki_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(trailerVehicle)
			ENDIF
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			//LOAD_CLOUD_HAT("Horizon")
			LOAD_CLOUD_HAT("Horizon")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			//SET_VEHICLE_DENSITY_MULTIPLIER(6.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(6.0)
			//SET_PED_DENSITY_MULTIPLIER(1.5)
			IF bFirstTimePlayingThisShot
			OR iRepeatsSinceLastForceVehPop > 2
				CLEAR_AREA_OF_VEHICLES(<<-1219.25, -1370.14, 4.93>>, 200.0)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				
				iRepeatsSinceLastForceVehPop = 0
			ENDIF
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", << -1325.4222, -1442.3396, 25.4198 >>, << -1.2041, 0.0000, -58.9124 >>, 29.6455, TRUE)
			SET_CAM_PARAMS(cam_main, << -1325.3661, -1442.3062, 28.4680 >>, << -1.2041, 0.0000, -58.9124 >>, 29.6455, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SETTIMERB(0)
			i_current_event++
			iRepeatsSinceLastForceVehPop++
		BREAK

		CASE 1
			IF TIMERB() > 5000
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageVinewoodSign()
	
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_M_Y_JetSki_01)
			//REQUEST_IPL("TRAILERSHOT_citypan")
			
			REQUEST_PTFX_ASSET()
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_JetSki_01)
			OR NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerJet", SHAMAL, trailerVehicle, 13500)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_LOD_DIST(trailerVehicle, 3000)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_JetSki_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				CONTROL_LANDING_GEAR(trailerVehicle, LGC_RETRACT_INSTANT)
				SET_VEHICLE_COLOURS(trailerVehicle, 7, 68)
				SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 7, 0)
			ENDIF
			
			IF mission_stage = STAGE_VINEWOOD_SIGN
				SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
				SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			ELSE
				SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
				SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			ENDIF
			
			LOAD_CLOUD_HAT("WISPY")
			LOAD_CLOUD_HAT("HORIZON")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<767.291565,1037.989258,301.629364>>,<<14.050559,-0.000000,17.578516>>,45.000000, TRUE)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main, <<767.291565,1038.532959,301.629364>>,<<15.280192,-0.000000,17.578520>>,45.000000, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerParticles[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trailer_jet_private", trailerVehicle, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 5000
				//DESTROY_ALL_CAMS()
				//STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageFinalShot()
	
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_M_Y_JetSki_01)
			//REQUEST_IPL("TRAILERSHOT_citypan")
			
			REQUEST_PTFX_ASSET()
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_JetSki_01)
			OR NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(3, "TrailerJet", SHAMAL, trailerVehicle, 3000)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_LOD_DIST(trailerVehicle, 3000)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_JetSki_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				CONTROL_LANDING_GEAR(trailerVehicle, LGC_RETRACT_INSTANT)
				SET_VEHICLE_COLOURS(trailerVehicle, 7, 68)
				SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 7, 0)
			ENDIF
			
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			LOAD_CLOUD_HAT("WISPY")
			LOAD_CLOUD_HAT("HORIZON")
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<897.922424,-407.255859,271.059204>>,<<0.319277,-0.000000,95.204201>>,44.497265, TRUE)
			//SET_CAM_FAR_DOF(cam_main, 230.0)
			//SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main, <<936.305420,-409.035004,271.059204>>,<<0.319277,-0.000000,95.204201>>,44.497265, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			//SET_CAM_FAR_DOF(cam_main, 230.0)
			//(cam_main, 0.0)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerParticles[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trailer_jet_private", trailerVehicle, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 1500
				IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
					CONTROL_LANDING_GEAR(trailerVehicle, LGC_DEPLOY)
					
					i_current_event++
				ENDIF
			ENDIF
		BREAK

		CASE 2
			IF TIMERB() > 5000
				//DESTROY_ALL_CAMS()
				//STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageConstructionSite()
	
	SWITCH i_current_event
		CASE 0
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			LOAD_CLOUD_HAT("stormy 01")
		
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,-110.046654,-992.459534,86.764687,6.227125) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,-109.105942,-1023.805237,49.199226,2.921594) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0.000000,0.000000,0.000000,1.000000) 
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,1.000000)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(42.0)
		
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
		
			//DESTROY_ALL_CAMS()	
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-81.831665,-1077.870483,30.668085>>,<<27.583368,0.228816,9.344581>>,41.919060, TRUE)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main, <<-87.292931,-1069.465454,47.160694>>,<<38.554344,0.228815,21.546518>>,36.112942, 3500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
						
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 3500
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC stageDocks()

	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()	
			
			REQUEST_MODEL(TRAILERS2)
			REQUEST_MODEL(model_bird)
			REQUEST_IPL("docks")
			REQUEST_ANIM_DICT("creatures@gull@move")
			
			WHILE NOT HAS_MODEL_LOADED(TRAILERS2)
			OR NOT HAS_MODEL_LOADED(model_bird)
			OR NOT HAS_ANIM_DICT_LOADED("creatures@gull@move")
				WAIT(0)
			ENDWHILE
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerDocks", PHANTOM, trailerVehicle, 7000)
				WAIT(0)
			ENDWHILE
			
			SET_VEHICLE_DIRT_LEVEL(trailerVehicle, 10.0)
			
			trailerVehicle2 = CREATE_VEHICLE(TRAILERS2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(trailerVehicle, <<0.0, -6.0, 0.0>>), GET_ENTITY_HEADING(trailerVehicle))
			SET_VEHICLE_DIRT_LEVEL(trailerVehicle2, 10.0)
			
			//CREATE_FLOCK(bird_flock, << 1104.9736, -3107.8608, 11.3862 >>)
			CREATE_FLOCK(bird_flock, <<1101.2140, -3106.0430, 10.4195>>, <<1105.3129, -3108.6931, 10.6843>>, <<1106.4873, -3102.1492, 10.7847 >>)
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1072.461548,-3110.402344,4.905646,1.281250)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1101.443970,-3092.378906,4.918741,0.558219)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1112.008057,-3077.687988,4.911760,1.000000)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,TRUE,1172.993042,-3055.267090,5.002614,1.000000)
			CASCADE_SHADOWS_SET_DEPTH_BIAS(TRUE, 0.002)

//			LOAD_CLOUD_HAT("stormy 01")
//		
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_GLOBAL) //Bug says use global
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(30.0)
			//SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<1062.657227,-3115.166748,5.870315>>,<<6.491007,0.000001,-68.572647>>,29.612511, TRUE)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main, <<1063.176025,-3115.877930,5.993696>>,<<10.435406,0.118340,-54.553585>>,28.358284, 2500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
			SET_CAM_FAR_DOF(cam_main, 230.0)
			SET_CAM_NEAR_DOF(cam_main, 0.0)
			SHAKE_CAM(cam_main, "HAND_SHAKE", 0.3)
						
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1

			//Call this every frame.
			//UPDATE_FLOCK(bird_flock)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
			AND NOT IS_ENTITY_DEAD(trailerVehicle2)
				ATTACH_VEHICLE_TO_TRAILER(trailerVehicle, trailerVehicle2)
			ENDIF
			
			IF TIMERB() > 4500
				//DESTROY_ALL_CAMS()
				DELETE_FLOCK(bird_flock)
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


PROC stageFighterJet()
	
	SWITCH i_current_event
		CASE 0
			//DESTROY_ALL_CAMS()
			
			REQUEST_MODEL(A_M_Y_JetSki_01)
			//REQUEST_IPL("TRAILERSHOT_citypan")
			
			REQUEST_PTFX_ASSET()
			
			WHILE NOT HAS_MODEL_LOADED(A_M_Y_JetSki_01)
			OR NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
						
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(5, "TrailerJet", CUBAN800, trailerVehicle, 27000)
				WAIT(0)
			ENDWHILE
			
			SET_PLAYBACK_SPEED(trailerVehicle, 4.0)
			
			SET_ENTITY_LOD_DIST(trailerVehicle, 3000)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, A_M_Y_JetSki_01)
				SET_VEHICLE_ENGINE_ON(trailerVehicle, TRUE, TRUE)
				CONTROL_LANDING_GEAR(trailerVehicle, LGC_RETRACT_INSTANT)
			ENDIF
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-69.056793,-581.968384,131.351776>>,<<29.225712,-0.000037,161.309677>>,45.000000, TRUE)
			//SET_CAM_FAR_DOF(cam_main, 230.0)
			//SET_CAM_NEAR_DOF(cam_main, 0.0)
			SET_CAM_PARAMS(cam_main, <<-69.056793,-581.968384,131.351776>>,<<13.836164,-0.000037,161.309677>>,45.000000, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			//SET_CAM_FAR_DOF(cam_main, 230.0)
			//(cam_main, 0.0)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF NOT IS_ENTITY_DEAD(trailerVehicle)
				trailerParticles[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trailer_jet_fighter", trailerVehicle, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 1500
				//DESTROY_ALL_CAMS()
				//STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
		BREAK
	ENDSWITCH

	//IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
	//	PRINTFLOAT(GET_ENTITY_SPEED(trailerVehicle))
	//	PRINTNL()
	//ENDIF

ENDPROC

PROC stagePoliceChaseRedCar

	SWITCH i_current_event
		CASE 0
			
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			REQUEST_MODEL(S_M_Y_COP_01)
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(1, "TrailerRed", RAPIDGT2, trailerVehicle, 2500)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT INITIALISE_TRAILER_VEHICLE_SET_PIECE(2, "TrailerRed", POLICE, trailerVehicle2, 4300)
				WAIT(0)
			ENDWHILE
			
			SET_MAPDATACULLBOX_ENABLED("TrailerCarChase", TRUE) 
			bChaseCullEnabled = TRUE
			
			SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT2)
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICE)
			
			SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT2, TRUE)
			
			SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")
			SET_TIMECYCLE_REGION_OVERRIDE(TIMECYCLE_REGION_URBAN)
			
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(3.0)
			//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(3.0)
			
			IF bFirstTimePlayingThisShot
			OR iRepeatsSinceLastForceVehPop > 2
				CLEAR_AREA_OF_VEHICLES(<<-186.168091,-874.375061,29.202194>>, 100.0)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				iRepeatsSinceLastForceVehPop = 0
			ENDIF
			
			iRepeatsSinceLastForceVehPop++
			CLEAR_AREA_OF_VEHICLES(<<-186.168091,-874.375061,29.202194>>, 20.0)
			
			SET_WIND_SPEED(fWindSpeed)
			SET_WIND_DIRECTION(fWindDirectionRadians)
			
			cam_main = CREATE_CAM_WITH_PARAMS_AND_SETUP_SCENE("DEFAULT_SCRIPTED_CAMERA", <<-186.168091,-874.375061,29.202194>>,<<-2.689459,0.084089,-159.271759>>,30.000000, TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
				SET_VEHICLE_DIRT_LEVEL(trailerVehicle, 0.0)
				//SET_VEHICLE_COLOURS(trailerVehicle, 35, 35)
				//SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 35, 35)
				SET_VEHICLE_COLOURS(trailerVehicle, 31, 31)
				SET_VEHICLE_EXTRA_COLOURS(trailerVehicle, 25, 0)
				
				POINT_CAM_AT_ENTITY(cam_main, trailerVehicle, <<0.0, 0.0, 0.3>>)
				SHAKE_CAM(cam_main, "HAND_SHAKE", 1.0)
				
				WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
					WAIT(0)
				ENDWHILE
				
				trailerPeds[0] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle, PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle2)
				SET_VEHICLE_ENGINE_ON(trailerVehicle2, TRUE, TRUE)
				SET_VEHICLE_SIREN(trailerVehicle2, TRUE)
				
				WHILE NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
					WAIT(0)
				ENDWHILE
				
				trailerPeds[1] = CREATE_PED_INSIDE_VEHICLE(trailerVehicle2, PEDTYPE_MISSION, S_M_Y_COP_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			ENDIF
			
			//SET_CAM_PARAMS(cam_main, <<-195.644211,-879.739441,30.028566>>,<<-2.952162,-0.001508,-156.050888>>,29.034260, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
				STOP_PLAYBACK_RECORDED_VEHICLE(trailerVehicle)
				START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, 1, "TrailerRed")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, 3500)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle2)
				STOP_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2)
				START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, 2, "TrailerRed")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, 5300)
			ENDIF
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SETTIMERB(0)
			i_current_event++
		BREAK

		CASE 1
			IF TIMERB() > 2500
				//DESTROY_ALL_CAMS()
				SET_CAM_FOV(cam_main, 70.0)
			ENDIF
			
			/*IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle)
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle, 1, "TrailerRed")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(trailerVehicle)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(trailerVehicle2)
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						START_PLAYBACK_RECORDED_VEHICLE(trailerVehicle2, 2, "TrailerRed")
					ENDIF
				ENDIF
			ENDIF*/
		BREAK
	ENDSWITCH

ENDPROC

PROC CleanupAllEntitiesAndAnimations()
	
	IF NOT HAS_CUTSCENE_FINISHED()
		STOP_CUTSCENE()
	ENDIF
	
	WHILE IS_CUTSCENE_ACTIVE()
		WAIT(0)
	ENDWHILE
	
	INT j
	REPEAT COUNT_OF(s_birds) j
		DELETE_PED(s_birds[j].ped)
		s_birds[j].i_event = 0
	ENDREPEAT
	
	DELETE_FLOCK(bird_flock)

//	REMOVE_IPL("SP1_02_SHOT_jetski")
//	REMOVE_IPL("TRAILERSHOT_joggers")
//	REMOVE_IPL("TRAILERSHOT_intro")
//	REMOVE_IPL("TRAILERSHOT_citypan")

	IF trailerParticles[0] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[0])
		trailerParticles[0] = NULL
	ENDIF
	IF trailerParticles[1] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[1])
		trailerParticles[1] = NULL
	ENDIF
	IF trailerParticles[2] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[2])
		trailerParticles[2] = NULL
	ENDIF
	IF trailerParticles[3] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[3])
		trailerParticles[3] = NULL
	ENDIF
	IF trailerParticles[4] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[4])
		trailerParticles[4] = NULL
	ENDIF
	IF trailerParticles[5] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[5])
		trailerParticles[5] = NULL
	ENDIF
	IF trailerParticles[6] <> NULL
		REMOVE_PARTICLE_FX(trailerParticles[6])
		trailerParticles[6] = NULL
	ENDIF
	
	REMOVE_SCRIPT_FIRE(fiScriptFire)
	REMOVE_SCRIPT_FIRE(fiScriptFire2)

	REMOVE_ANIM_DICT("trailer@yoga")
	REMOVE_ANIM_DICT("trailer@criminals")
	REMOVE_ANIM_DICT("trailer@freeway_beggars")
	REMOVE_ANIM_DICT("trailer@taco_van")
	REMOVE_ANIM_DICT("trailer@bike_riders")
	REMOVE_ANIM_DICT("trailer@joggers")
	REMOVE_ANIM_DICT("trailer@hikers")
	REMOVE_ANIM_DICT("trailer@crop_duster")
	REMOVE_ANIM_DICT("trailer@muscle_beach")
	REMOVE_ANIM_DICT("trailer@hookers")
	REMOVE_ANIM_DICT("trailer@gangbanger_walk")
	REMOVE_ANIM_DICT("trailer@foreclosure")
	REMOVE_ANIM_DICT("trailer@jetski")
	REMOVE_ANIM_DICT("trailer@convertible")
	REMOVE_ANIM_DICT("trailer@golf")
	REMOVE_ANIM_DICT("trailer@work_line_1")
	REMOVE_ANIM_DICT("trailer@drunk_exit")
	REMOVE_ANIM_DICT("trailer@dog")
	REMOVE_ANIM_DICT("amb@bums@male@stationary@sat_on_bench@idle_a")
	REMOVE_ANIM_DICT("amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_b")
	REMOVE_ANIM_DICT("amb@bums@male@stationary@LAYING_AGAINST_WALL@IDLE_a")
	REMOVE_ANIM_DICT("AMB@BUMS@MALE@STATIONARY@STANDING@IDLE_A")
	REMOVE_ANIM_DICT("AMB@MOBILE@MALE@STATIONARY@CALL@IDLE_A")
	REMOVE_ANIM_DICT("AMB@SMOKING@STANDING@MALE@IDLE_A")
	REMOVE_ANIM_DICT("AMB@LEAN@MALE@STATIONARY@SMOKING@IDLE_A")
	REMOVE_ANIM_DICT("AMB@BUSSTOP@CHAR1@IDLE_A")
	REMOVE_ANIM_DICT("MISSFBI_S4MOP")
	REMOVE_ANIM_DICT("creatures@gull@move")
	REMOVE_ANIM_DICT("Move_f@arrogant")
	REMOVE_ANIM_DICT("Move_m@sad")
	
	IF DOES_ENTITY_EXIST(trailerVehicle)
		IF GET_ENTITY_MODEL(trailerVehicle) = FREIGHT
			DELETE_MISSION_TRAIN(trailerVehicle) 
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTGRAIN)
	SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
		
	CLEANUP_PACKER_VEHICLES()
	CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle2)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicle3)
	
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam2)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam3)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam4)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam5)
	CLEANUP_TRAILER_VEHICLE_SETPIECE(vehCam6)
	
	INT i
	FOR i = 0 TO 17
		CLEANUP_TRAILER_PED(trailerPeds[i])
	ENDFOR
	
	REPEAT COUNT_OF(trailerVehicles) i
		CLEANUP_TRAILER_VEHICLE_SETPIECE(trailerVehicles[i])
	ENDREPEAT
	
	CLEANUP_TRAILER_ENTITY(oiTrailerProp)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp2)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp3)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp4)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp5)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp6)
	CLEANUP_TRAILER_ENTITY(oiTrailerProp7)
	
	
	STOP_PARTICLE_FX_LOOPED(trailerParticles[0])
	STOP_PARTICLE_FX_LOOPED(trailerParticles[1])
	STOP_PARTICLE_FX_LOOPED(trailerParticles[2])
	STOP_PARTICLE_FX_LOOPED(trailerParticles[3])
	STOP_PARTICLE_FX_LOOPED(trailerParticles[4])
	STOP_PARTICLE_FX_LOOPED(trailerParticles[5])
	
				STOP_CAM_PLAYBACK(vehCam)
	STOP_CAM_PLAYBACK(vehCam2)
	STOP_CAM_PLAYBACK(vehCam3)
	STOP_CAM_PLAYBACK(vehCam4)
	STOP_CAM_PLAYBACK(vehCam5)
	STOP_CAM_PLAYBACK(vehCam6)
//		
	SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL2)
	
	REMOVE_VEHICLE_RECORDING(1, "Temp2")
	REMOVE_VEHICLE_RECORDING(41, "Temp2")
	REMOVE_VEHICLE_RECORDING(42, "Temp2")
	REMOVE_VEHICLE_RECORDING(43, "Temp2")
	REMOVE_VEHICLE_RECORDING(44, "Temp2")
	REMOVE_VEHICLE_RECORDING(45, "Temp2")
	
	REMOVE_CAM_RECORDING(1, "tempCam2")
	REMOVE_CAM_RECORDING(2, "tempCam2")
	REMOVE_CAM_RECORDING(3, "tempCam2")
	REMOVE_CAM_RECORDING(4, "tempCam2")
	REMOVE_CAM_RECORDING(5, "tempCam2")
	REMOVE_CAM_RECORDING(6, "tempCam2")
	REMOVE_CAM_RECORDING(7, "tempCam2")
	REMOVE_CAM_RECORDING(8, "tempCam2")
	REMOVE_CAM_RECORDING(9, "tempCam2")
	
	REMOVE_PTFX_ASSET()
	
	IF bUsedZMenu
	OR bRepeatShot = FALSE
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0,0,0,0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0,0,0,0)
		
		CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE(1.0) 
		CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE("CSM_ST_DITHER4") 
		CASCADE_SHADOWS_SET_DEPTH_BIAS(FALSE,0.01)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV(0.0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV(0.0)
		
		UNLOAD_ALL_CLOUD_HATS()
		SET_TIMECYCLE_REGION_OVERRIDE(-1)
		CLEAR_WEATHER_TYPE_PERSIST()
		
		CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000.0, TRUE)
		SET_ROADS_IN_ANGLED_AREA(<<-1164.383789,-1343.805420,3.580653>>, <<-1222.467651,-1371.465088,11.879992>>, 35.250000, FALSE, TRUE)
		
		SET_WIND_SPEED(0.0)
		SET_WIND_DIRECTION(0.0)
		
		bFirstTimePlayingThisShot = TRUE
	ENDIF
	
	IF bChaseCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerCarChase", FALSE) 
		bChaseCullEnabled = FALSE
	ENDIF
	
	IF bJoggersCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerJoggers", FALSE) 
		bJoggersCullEnabled = FALSE
	ENDIF
	
	IF bChase2CullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerCineVine", FALSE) 
		bChase2CullEnabled = FALSE
	ENDIF
	
	IF bWorklineCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerWorkLine", FALSE) 
		bWorklineCullEnabled = FALSE
	ENDIF
	
	IF bYogaCullEnabled
		SET_MAPDATACULLBOX_ENABLED("Traileryoga", FALSE) 
		bYogaCullEnabled = FALSE
	ENDIF
	
	IF bGangCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerGBangers", FALSE) 
		bGangCullEnabled = FALSE
	ENDIF
		
	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_PED_DENSITY_MULTIPLIER(1.0)
	
	IF DOES_CAM_EXIST(cam_main)
		SET_CAM_MOTION_BLUR_STRENGTH(cam_main, 0.0)
		STOP_CAM_SHAKING(cam_main, TRUE)
		STOP_CAM_POINTING(cam_main)
	ENDIF
ENDPROC



PROC maintainTrailer()

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
		
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
	
		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
	
		GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
		vCoords = GET_ENTITY_COORDS(thiscar)
	
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
		SAVE_STRING_TO_DEBUG_FILE(")")
		
		
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
		SAVE_STRING_TO_DEBUG_FILE(")")
			
	ENDIF
	
	
	IF DOES_CAM_EXIST(cam_main)
		//IF IS_CAM_ACTIVE(cam_main)
		IF GET_CAM_FOV(cam_main) < 70.00
			
			//Clear the area by pressing X
			IF IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
				IF bTurnOffRoads = TRUE
					bTurnOffRoads = FALSE
					PRINT_HELP("TR_ROADS1")
					vRoads = GET_CAM_COORD(cam_main)
					SET_ROADS_IN_AREA(<<vRoads.x - 20.0, vRoads.y - 20.0, vRoads.z - 20.0>>, <<vRoads.x + 20.0, vRoads.y + 20.0, vRoads.z + 20.0>>, FALSE)
					CLEAR_AREA(vRoads, 40.0, TRUE)
				ELSE
					PRINT_HELP("TR_ROADS2")
					SET_ROADS_IN_AREA(<<-3000.0, -3000.0, -3000.0>>, <<-3000.0, -3000.0, -3000.0>>, TRUE)
					SET_ROADS_IN_AREA(<<vRoads.x - 20.0, vRoads.y - 20.0, vRoads.z - 20.0>>, <<vRoads.x + 20.0, vRoads.y + 20.0, vRoads.z + 20.0>>, TRUE)
					bTurnOffRoads = TRUE
				ENDIF
			ENDIF
			
			IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
				IF bHighDofOn
					bHighDofOn = FALSE
				ELSE
					bHighDofOn = TRUE
				ENDIF
			ENDIF
			
			IF bHighDofOn
				SET_USE_HI_DOF()
			ENDIF
			
			SET_CLOCK_TIME(InitialTimeOfDayHour[ENUM_TO_INT(mission_stage)], 0, 0)
				
			IF bResetPlayer = TRUE
				//SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
				IF NOT bDontClearAreaBetweenRepeatPlays
					CLEAR_AREA(GET_CAM_COORD(cam_main), 20.0, TRUE)
				ELSE
					bDontClearAreaBetweenRepeatPlays = FALSE //Keep this off as default.
				ENDIF
				SETTIMERA(0)			
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(TRUE)
				bResetPlayer = FALSE
			ENDIF
			
			//Dont hide the player during mocap
			IF NOT IS_CUTSCENE_ACTIVE()
			
				//IF mission_stage <> STAGE_TRAFFIC
				IF mission_stage <> STAGE_NEW_OIL_DERRICKS
				AND mission_stage <> STAGE_YOGA_NEW_6
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_CAM_COORD(cam_main))
				ENDIF
				
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)	
			ELSE
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)	
			ENDIF
					
		
			IF mission_stage <> STAGE_WAIT_FOR_STAGE
			AND mission_Stage <> STAGE_RUN_THROUGH_SHOTS
			AND bDisplayTitles
				IF TIMERA() < 2500
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.04, "STRING", SkipMenuStruct[mission_stage].sTxtLabel)
				ENDIF
			ENDIF
			
			
			
		ELSE
		
	
			IF bResetPlayer = FALSE
				FLOAT groundZ
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), groundZ)
					VECTOR groundCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
					groundCoord.z = groundZ
					SET_ENTITY_COORDS(PLAYER_PED_ID(), groundCoord)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF bRepeatShot = FALSE
					mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(mission_stage) + 1)
				ENDIF
			
				IF mission_stage  = STAGE_NO_MORE_STAGES
					bDrawMenu = TRUE
					mission_stage = STAGE_WAIT_FOR_STAGE
				ENDIF
			
				i_current_event = 0
				
				CleanupAllEntitiesAndAnimations()
				
				SET_TIME_SCALE(1.0)
				//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
				//SET_PED_DENSITY_MULTIPLIER(1.0)
							
				bResetPlayer = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	

	
ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

PROC Mission_Cleanup()

	SET_WIND_SPEED(0.0)
	SET_WIND_DIRECTION(0.0)
	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_PED_DENSITY_MULTIPLIER(1.0)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)

	UNLOAD_ALL_CLOUD_HATS()
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0,0,0,0)
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
	SET_TIMECYCLE_REGION_OVERRIDE(-1)
	CLEAR_WEATHER_TYPE_PERSIST()

	STOP_CAM_RECORDING(camRecData)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PATRIOT, FALSE)
	
	FLOAT groundZ
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), groundZ)
		VECTOR groundCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		groundCoord.z = groundZ
		SET_ENTITY_COORDS(PLAYER_PED_ID(), groundCoord)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF
	SET_TIME_SCALE(1.0)
	DELETE_WIDGET_GROUP(paradiseWidgetGroup)

	STOP_CUTSCENE()

	DISPLAY_RADAR(TRUE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	
	IF bChaseCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerCarChase", FALSE) 
		bChaseCullEnabled = FALSE
	ENDIF
	
	IF bJoggersCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerJoggers", FALSE) 
		bJoggersCullEnabled = FALSE
	ENDIF
	
	IF bChase2CullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerCineVine", FALSE) 
		bChase2CullEnabled = FALSE
	ENDIF
	
	IF bWorklineCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerWorkLine", FALSE) 
		bWorklineCullEnabled = FALSE
	ENDIF
	
	IF bYogaCullEnabled
		SET_MAPDATACULLBOX_ENABLED("Traileryoga", FALSE) 
		bYogaCullEnabled = FALSE
	ENDIF
	
	IF bGangCullEnabled
		SET_MAPDATACULLBOX_ENABLED("TrailerGBangers", FALSE) 
		bGangCullEnabled = FALSE
	ENDIF
	
	 g_OnMissionState = MISSION_TYPE_OFF_MISSION// = FALSE
	Mission_Flow_Mission_Passed()
	TERMINATE_THIS_THREAD()
	
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================


SCRIPT

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	stageInit()
	
	WHILE (TRUE)
		
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE, "Trailer Shot List")
			//Set stage enum to required stage
			mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
			i_current_event = 0
			
			STOP_CUTSCENE()
			
			//CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP(FALSE)
			bUsedZMenu = TRUE
			CleanupAllEntitiesAndAnimations()
	 	ENDIF
		
		IF NOT bUsedZMenu 
			maintainTrailer()
		ENDIF
		
		bUsedZMenu = FALSE
	
		SWITCH mission_stage
			
			CASE STAGE_WAIT_FOR_STAGE
				stageWaitForShots()	
				//AltFirstShot()
			BREAK
			
			CASE STAGE_RUN_THROUGH_SHOTS
				bResetPlayer = TRUE
				bRepeatShot = FALSE
				//Stage 2 in Enum list is first shot
				mission_stage =  INT_TO_ENUM(MISSION_STAGE_FLAG, 2) 
			BREAK
			
						
			CASE STAGE_INTRO_SHOT
				stageIntroShot()
			BREAK
//			
			CASE STAGE_WINDMILLS
			CASE STAGE_WINDMILLS_ALT
				stageWindmills()
			BREAK

			CASE STAGE_YOGA_NEW_6
				stageyogaNew6()
			BREAK
			
			CASE STAGE_BEGGARS1
				stageBeggars1()
			BREAK
			
			CASE STAGE_HIKING_NEW2
				stageHikingNew2()
			BREAK

			CASE STAGE_DUSTER3
				stageDuster3()
			BREAK
		
			CASE STAGE_CRASHED_CAR2
				stageCrashedCar2()
			BREAK
			
			CASE STAGE_FORECLOSURE2
				stageForeClosure2()
			BREAK
			
			CASE STAGE_NEW_OIL_DERRICKS
			CASE STAGE_NEW_OIL_DERRICKS_ALT
				stageNewOilDerricks()
			BREAK
			
			CASE STAGE_TENT_CITY_TOWN
				stageTentCityTown()
			BREAK
			
			CASE STAGE_TRAIN
				stageTrain()
			BREAK
			
			CASE STAGE_TRAIN_B
				stageTrainB()
			BREAK
		
			CASE STAGE_LD_CONVERTIBLES2
				stageLDConvertibles2()
			BREAK

			CASE STAGE_JEWEL_HEIST
				stageJewelHeist()
			BREAK		
			
			CASE STAGE_SALTON_SEA_HOOKERS
			CASE STAGE_SALTON_SEA_HOOKERS_2
				stageSaltonHookers()
			BREAK		
			
			CASE STAGE_JOGGERS4
				stageJogBoardwalk4()
			BREAK	
			
			CASE STAGE_GANGBANGER_NEW
				stageGangBangersNew1()
			BREAK
			
			CASE STAGE_MUSCLE_BEACH
				stageMuscleBeach()
			BREAK
			
			CASE STAGE_JET_SKI
				stageJetSkiBay()
			BREAK
			
			CASE STAGE_MICHAEL
			CASE STAGE_MICHAEL_ALT
				stageMichael()
			BREAK
						
			CASE STAGE_CHASE2
			CASE STAGE_CHASE2_SECOND_SHOT
				stageChase2()
			BREAK
				
			CASE STAGE_GOLF
				stageGolf()
			BREAK
			
			CASE STAGE_GOLF_SHOT_2
				stageGolfShot2()
			BREAK
			
			CASE STAGE_CLUB_THROWOUT
				stageClubThrowout()
			BREAK
			
			CASE STAGE_ALLEY_CHASE_DAY
			CASE STAGE_ALLEY_CHASE
				stageAlleyChase()
			BREAK
			
			CASE STAGE_WORK_LINE
				stageWorkLine()
			BREAK
	
			CASE STAGE_ROCKSTAR_GAMES
				stageRockstarGamesPresents()
			BREAK
			
			CASE STAGE_VINEWOOD_SIGN
			CASE STAGE_VINEWOOD_SIGN_ALT
				stageVinewoodSign()
			BREAK
	
			CASE STAGE_FINAL_SHOT
			CASE STAGE_FINAL_SHOT_ALT
				stageFinalShot()
			BREAK
			
			CASE STAGE_CONSTRUCTION_SITE
				stageConstructionSite()
			BREAK
			
			CASE STAGE_DOCKS
				stageDocks()
			BREAK
			
			CASE STAGE_FIGHTER_JET
				stageFighterJet()
			BREAK
	
			CASE STAGE_POLICE_CHASE_RED_CAR
				stagePoliceChaseRedCar()
			BREAK
	
//			CASE STAGE_RECORD
//				stageRecord()
//			BREAK
	
		ENDSWITCH
					
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Cleanup()
		ENDIF
	
		WAIT(0)
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

#ENDIF

