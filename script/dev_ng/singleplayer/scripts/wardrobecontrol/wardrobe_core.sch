//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	wardrobe.sc 												//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Allows the player to select from a list of clothes that 	//
//							have been acquired and are available at the current stage.	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
 
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "ped_component_public.sch"
USING "commands_graphics.sch"
USING "cellphone_public.sch"
USING "brains.sch"
USING "commands_camera.sch"
USING "wardrobe_private.sch"
USING "player_ped_public.sch"
USING "LineActivation.sch"
USING "net_comms_public.sch"
USING "scaleform_helper.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "respawn_location_private.sch"
USING "menu_public.sch"
USING "screens_header.sch"
USING "context_control_public.sch"
USING "savegame_public.sch"
USING "fmmc_keyboard.sch"
USING "net_friend_request.sch"
USING "net_simple_interior.sch"
USING "net_transition_sessions_private.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

CONST_INT UI_ITEM_OWNED_BIT 	0
CONST_INT UI_ITEM_NEW_BIT 		1
CONST_INT UI_ITEM_CURRENT_BIT 	2

CONST_INT WARDROBE_BIT_LAUCNHED_IN_SP	0
CONST_INT WARDROBE_BIT_LAUCNHED_IN_MP	1

STRING sGraphicHeader
BOOL bHeaderGraphicRequested

CONTROL_ACTION caZoomInput = INPUT_FRONTEND_LT // Input used for zooming - different on PC.

TEXT_LABEL_63 tlNewOutfitName
STRUCT KEYBOARD_DATA 
    OSK_STATUS eStatus
    INT iProfanityToken
    INT iCurrentStatus
ENDSTRUCT
KEYBOARD_DATA sKeyBoardData

PIM_VHEADER_DATA VHeaderData
PIM_GLARE_DATA GlareData

BOOL bMainMenuInitialised = FALSE
BOOL bZoomPCToggle

// ---------------wardrobe light functions---------------------------
/// PURPOSE: Load model for wardrobe light
FUNC BOOL LOAD_WARDROBE_LIGHT(WARDROBE_INFO_STRUCT &sWardrobeInfo)
		
	REQUEST_MODEL(sWardrobeInfo.eWardrobeLight)	
	REQUEST_MODEL(sWardrobeInfo.eWardrobeHeadLight)
	IF HAS_MODEL_LOADED(sWardrobeInfo.eWardrobeLight)
	AND HAS_MODEL_LOADED(sWardrobeInfo.eWardrobeHeadLight)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Unload model for wardrobe light
PROC UNLOAD_WARDROBE_LIGHT(WARDROBE_INFO_STRUCT &sWardrobeInfo)
	IF IS_MODEL_IN_CDIMAGE(sWardrobeInfo.eWardrobeLight)
		SET_MODEL_AS_NO_LONGER_NEEDED(sWardrobeInfo.eWardrobeLight)
	ENDIF
	IF IS_MODEL_IN_CDIMAGE(sWardrobeInfo.eWardrobeHeadLight)
		SET_MODEL_AS_NO_LONGER_NEEDED(sWardrobeInfo.eWardrobeHeadLight)
	ENDIF
ENDPROC

FUNC BOOL IS_WARDROBE_IN_CASINO_APARTMENT_MASTER_BEDROOM()
	RETURN  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<976.4120, 64.7793, 116.9141>>) < 2.0
ENDFUNC

FUNC BOOL IS_WARDROBE_IN_CASINO_APARTMENT_GEUST_BEDROOM()
	RETURN GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<984.6170, 60.0772, 116.9142>> ) < 2.0
ENDFUNC

/// PURPOSE: Turn on the wardrobe head light, used for head close ups
PROC ACTIVATE_WARDROBE_HEAD_LIGHT(WARDROBE_DATA_STRUCT &sData, BOOL bActivate)
	IF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
		IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		OR IS_PLAYER_INSIDE_OWN_ARMORY_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		OR IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())			
		#IF FEATURE_FIXER
		OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#ENDIF
		
			EXIT
		ENDIF
	ENDIF
	
	IF bActivate = TRUE
		// activate wardrobe light
		CPRINTLN(DEBUG_PED_COMP, "Activating the wardrobe head light now")
		IF DOES_ENTITY_EXIST(sData.mWardrobeHeadLight)
			DELETE_OBJECT(sData.mWardrobeHeadLight)
		ENDIF
					
		sData.mWardrobeHeadLight = CREATE_OBJECT_NO_OFFSET(sData.sWardrobeInfo.eWardrobeHeadLight, sData.sWardrobeInfo.vPlayerCoords + <<0.0, 0.0, -1.0>>, FALSE, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(sData.mWardrobeHeadLight)
			CPRINTLN(DEBUG_PED_COMP, "Succesfully created head light")
			SET_ENTITY_HEADING(sData.mWardrobeHeadLight, sData.sWardrobeInfo.fPlayerHead)
			SET_ENTITY_COLLISION(sData.mWardrobeHeadLight, FALSE)
			FREEZE_ENTITY_POSITION(sData.mWardrobeHeadLight, TRUE)
			// don't set model as no longer needed, as we may need to recreate this light object
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "failed to create head light")
		ENDIF
	ELSE
		// turn off wardrobe light (delete it)
		IF DOES_ENTITY_EXIST(sData.mWardrobeHeadLight)
			CPRINTLN(DEBUG_PED_COMP, "Deleting the wardrobe head light now")
			DELETE_OBJECT(sData.mWardrobeHeadLight)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Turn on the wardrobe light
PROC ACTIVATE_WARDROBE_LIGHT(WARDROBE_DATA_STRUCT &sData, BOOL bActivate)
	IF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
		IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		OR IS_PLAYER_INSIDE_OWN_ARMORY_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		OR IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		#IF FEATURE_FIXER
		OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#ENDIF
		
		
			EXIT
		ENDIF
	ENDIF
	
	IF bActivate = TRUE
		// activate wardrobe light
		CPRINTLN(DEBUG_PED_COMP, "Activating the wardrobe light now")
		IF DOES_ENTITY_EXIST(sData.mWardrobeLight)
			DELETE_OBJECT(sData.mWardrobeLight)
		ENDIF
					
		sData.mWardrobeLight = CREATE_OBJECT_NO_OFFSET(sData.sWardrobeInfo.eWardrobeLight, sData.sWardrobeInfo.vPlayerCoords + <<0.0, 0.0, -1.0>>, FALSE, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(sData.mWardrobeLight)
			CPRINTLN(DEBUG_PED_COMP, "Succesfully created light")
			SET_ENTITY_HEADING(sData.mWardrobeLight, sData.sWardrobeInfo.fPlayerHead)
			SET_ENTITY_COLLISION(sData.mWardrobeLight, FALSE)
			FREEZE_ENTITY_POSITION(sData.mWardrobeLight, TRUE)
			// don't set model as no longer needed, as we may need to recreate this light object
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "failed to create light")
		ENDIF
	ELSE
		// turn off wardrobe light (delete it)
		IF DOES_ENTITY_EXIST(sData.mWardrobeLight)
			CPRINTLN(DEBUG_PED_COMP, "Deleting the wardrobe light now")
			DELETE_OBJECT(sData.mWardrobeLight)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		sData.bWardrobeLightOn = bActivate
	#ENDIF
ENDPROC

// ----------------------------debug functions------------------------------
#IF IS_DEBUG_BUILD
	PROC SETUP_WARDROBE_DEBUG_WIDGETS(WARDROBE_DATA_STRUCT &sData)
		START_WIDGET_GROUP("Wardrobe Debug")
			ADD_WIDGET_BOOL("Can change clothes on mission", g_bPlayerCanChangeClothesOnMission)
			ADD_WIDGET_FLOAT_READ_ONLY("Distance to player", sData.fDistToPlayer)
			ADD_WIDGET_BOOL("Kill script", sData.bKillScript)
			ADD_WIDGET_BOOL("Reset", sData.sBrowseInfo.bReset)
			ADD_WIDGET_BOOL("Update savehouse blip", sData.bUpdateSavehouseBlip)
			ADD_WIDGET_BOOL("All clothes available", g_bAllClothesAvailable)
			ADD_WIDGET_BOOL("All clothes acquired", g_bAllClothesAcquired)
			ADD_WIDGET_BOOL("Allow in Freemode", sData.bEnableDebugWardrobeInFM)
			ADD_WIDGET_FLOAT_SLIDER("Menu X", sData.fMenuX, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Menu Y", sData.fMenuY, 0.0, 1.0, 0.01)
			ADD_WIDGET_BOOL("Update player position", sData.bUpdatePlayerPosition)
			ADD_WIDGET_BOOL("Output player posistion", sData.bOutputPlayerPosition)
			ADD_WIDGET_FLOAT_SLIDER("Player head", sData.fPlayerHead, 0.0, 360.0, 1.0)
			ADD_WIDGET_VECTOR_SLIDER("Player coords", sData.vPlayerCoords, -9000.0, 9000.0, 0.001)
			ADD_WIDGET_BOOL("Output cam offsets", sData.bOutputCamOffsets)
			ADD_WIDGET_BOOL("Update look cam height", sData.bUpdateLookCamHeight)
			ADD_WIDGET_FLOAT_SLIDER("Camera look height", sData.fLookHeightOffset, -2.0, 3.0, 1.0)
			ADD_WIDGET_INT_SLIDER("Temp Rank", g_iPedComponentTemp_Rank, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("Temp Days", g_iPedComponentTemp_DaysPlayed, 0, 100, 1)
			ADD_WIDGET_BOOL("ToggleWardrobeLight", sData.bToggleWardrobeLight)
			ADD_WIDGET_BOOL("g_bScriptsSetSafeForCutscene (READ ONLY)", g_bScriptsSetSafeForCutscene)
		STOP_WIDGET_GROUP()
	ENDPROC
	PROC UPDATE_WARDROBE_DEBUG_WIDGETS(WARDROBE_DATA_STRUCT &sData)
		IF sData.bOutputPlayerPosition
			CPRINTLN(DEBUG_PED_COMP, "PLAYER POSITION")
			CPRINTLN(DEBUG_PED_COMP, GET_STRING_FROM_VECTOR(sData.vPlayerCoords), sData.fPlayerHead)
			sData.bOutputPlayerPosition = FALSE
		ENDIF
		IF sData.bUpdatePlayerPosition
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT sData.bInitPlayerPosition
					sData.vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					sData.fPlayerHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
					sData.bInitPlayerPosition = TRUE
				ENDIF
				
				SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), sData.vPlayerCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.fPlayerHead)
				
			ENDIF
		ELSE
			sData.bInitPlayerPosition = FALSE
		ENDIF
		IF sData.bOutputCamOffsets
			CPRINTLN(DEBUG_PED_COMP, "CAM OFFSET FROM PLAYER")
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CPRINTLN(DEBUG_PED_COMP, "VECTOR vOffset = ", GET_STRING_FROM_VECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD())))
				CPRINTLN(DEBUG_PED_COMP, "VECTOR vRot = ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(PLAYER_PED_ID()) - GET_FINAL_RENDERED_CAM_ROT()))
				CPRINTLN(DEBUG_PED_COMP, "FLOAT fFOV = ", GET_FINAL_RENDERED_CAM_FOV())
			ENDIF
			sData.bOutputCamOffsets = FALSE
		ENDIF
		
		IF sData.bUpdateLookCamHeight
			sData.sWardrobeInfo.fCameraHeightOffset = sData.fLookHeightOffset
		ENDIF
		IF sData.bToggleWardrobeLight
			ACTIVATE_WARDROBE_LIGHT(sData, !sData.bWardrobeLightOn)
			sData.bToggleWardrobeLight = FALSE
		ENDIF
	ENDPROC
#ENDIF

// ---------------------------- functions------------------------------

/// PURPOSE:
///    If the player equipes or unequips a jacket we need to regrab the clothes info in MP.
PROC HANDLE_JACKET_CHANGE_MP(WARDROBE_DATA_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BOOL bIsSpecialJacket, bWearingJacket
		bIsSpecialJacket = FALSE
		bWearingJacket = IS_PED_WEARING_JACKET_MP(PLAYER_PED_ID(), bIsSpecialJacket)
		IF sData.bWearingJacket <> bWearingJacket
			sData.bWearingJacket = bWearingJacket
			sData.iCurrentItemBeforeJacketChange = sData.sBrowseInfo.iCurrentItem
			CPRINTLN(DEBUG_PED_COMP, "bWearingJacket has changed: grab clothes info again. sData.bWearingJacket = ", sData.bWearingJacket, " currentItem = ", sData.sBrowseInfo.iCurrentItem)
			CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)		
			
			sData.sBrowseInfo.iCurrentItem = -1
			GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
			sData.bRebuildMenu = TRUE
		ENDIF
	ENDIF
ENDPROC
#if USE_CLF_DLC
FUNC BOOL IS_WARDROBE_AVAILABLECLF(SAVEHOUSE_NAME_ENUM eSavehouse)

	IF eSavehouse <> NUMBER_OF_CLF_SAVEHOUSE
		IF NOT Is_Savehouse_Respawn_Available(eSavehouse)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL IS_WARDROBE_AVAILABLENRM(SAVEHOUSE_NAME_ENUM eSavehouse)

	IF eSavehouse <> NUMBER_OF_NRM_SAVEHOUSE
		IF NOT Is_Savehouse_Respawn_Available(eSavehouse)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
#endif
FUNC BOOL IS_WARDROBE_AVAILABLE(SAVEHOUSE_NAME_ENUM eSavehouse)
#if USE_CLF_DLC
	return IS_WARDROBE_AVAILABLECLF(eSavehouse) 
#endif
#if USE_NRM_DLC
	return IS_WARDROBE_AVAILABLENRM(eSavehouse)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF eSavehouse <> NUMBER_OF_SAVEHOUSE_LOCATIONS
		IF NOT Is_Savehouse_Respawn_Available(eSavehouse)
			RETURN FALSE
		ENDIF
		
		IF eSavehouse = SAVEHOUSE_FRANKLIN_SC
			IF Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH)
				RETURN FALSE
			ENDIF
		ENDIF
		//B* 2185792: Wardrobe unavailable during Director mode
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
			RETURN FALSE
		ENDIF
	ENDIF	
#endif
#endif

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_FACING_WARDROBE(VECTOR vLookAt)
	VECTOR vToWardrobe = NORMALISE_VECTOR((vLookAt - GET_ENTITY_COORDS(PLAYER_PED_ID())))
	FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vToWardrobe)
	
	RETURN fDot >= 0
ENDFUNC

/// PURPOSE: Check some condiions to see if it is safe to browse wardrobe
FUNC BOOL IS_WARDROBE_SAFE_TO_BROWSE(WARDROBE_INFO_STRUCT &sWardrobeInfo, BOOL bCheckPlayerControl = TRUE, BOOL bCheckCutsceneFlag = TRUE, BOOL bEntryCheck = FALSE, BOOL bOccupiedCheck = FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		// Network players must be in their own apartment or a shop
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
			AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
			AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
			AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
			AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			AND	NOT	IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
			AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			#IF FEATURE_FIXER
			AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			#ENDIF
			
			AND NOT IS_LOCAL_PLAYER_IN_IE_WAREHOUSE_THEY_OWN()			
			AND NOT (sWardrobeInfo.eWardrobe = PW_FREEMODE AND IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty))
			AND (NOT IS_PLAYER_IN_ANY_SHOP() OR IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID()) OR IS_LOCAL_PLAYER_IN_IE_WAREHOUSE_THEY_OWN() 
				 OR IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) OR IS_PLAYER_IN_BUNKER(PLAYER_ID())  
				OR IS_PLAYER_IN_HANGAR(PLAYER_ID()) OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()) 
				OR IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()) OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())  OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID()) 
				OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
				#IF FEATURE_FIXER
				OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
				#ENDIF
				)
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player not in their own property")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			// Anyone can use the yacht wardrobe so check if area is occuipied.
			IF bOccupiedCheck
				FLOAT fDistToLaunch = 1.5
				INT iPlayer
				REPEAT NUM_NETWORK_PLAYERS iPlayer
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
					AND INT_TO_PLAYERINDEX(iPlayer) != PLAYER_ID()
						IF GET_DISTANCE_BETWEEN_COORDS(sWardrobeInfo.vPlayerCoords, GET_ENTITY_COORDS(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)), FALSE)) <= fDistToLaunch
							#IF IS_DEBUG_BUILD
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Area is occupied by player - ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
								ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bCheckPlayerControl
				IF IS_PAUSE_MENU_ACTIVE()
					#IF IS_DEBUG_BUILD
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Pause menu is active")
						ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF g_bCelebrationScreenIsActive = TRUE
				CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE :g_bCelebrationScreenIsActive =TRUE")
				RETURN FALSE
			ENDIF
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				IF GET_LOCAL_DEFAULT_OUTFIT(GET_PLAYER_HEIST_TEAM()) != ENUM_TO_INT(OUTFIT_MP_DEFAULT)
				AND GET_LOCAL_DEFAULT_OUTFIT(GET_PLAYER_HEIST_TEAM()) != ENUM_TO_INT(OUTFIT_MP_FREEMODE)
					CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE :Custom outfit set ", GET_MP_OUTFIT_NAME_FROM_ENUM(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(GET_PLAYER_HEIST_TEAM()))))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bCheckPlayerControl
			IF (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
			OR (NOT IS_GAMEPLAY_CAM_RENDERING() AND NOT IS_AIM_CAM_ACTIVE())
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						IF (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
							CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player control off")
						ELIF (NOT IS_GAMEPLAY_CAM_RENDERING() AND NOT IS_AIM_CAM_ACTIVE())
							CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Gameplay cam not rendering and aim cam not active")
						ENDIF
					ENDIF
				#ENDIF
			
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF bEntryCheck
			IF IS_CUSTOM_MENU_ON_SCREEN()
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Custom menu on screen")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT (IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))
		OR (IS_ENTITY_IN_AIR(PLAYER_PED_ID()))
		OR (IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()))
		OR (IS_PED_RAGDOLL(PLAYER_PED_ID()))
		OR (IS_PED_GETTING_UP(PLAYER_PED_ID()))
		OR (IS_PLAYER_BEING_ARRESTED(PLAYER_ID()))
		OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
		OR (IS_PLAYER_CLIMBING(PLAYER_ID()))
		OR (IS_FIRST_PERSON_AIM_CAM_ACTIVE())
		
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					IF NOT (IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player not ready for cutscene")
					ELIF (IS_ENTITY_IN_AIR(PLAYER_PED_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is in the air")
					ELIF (IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is getting into a vehicle")
					ELIF (IS_PED_RAGDOLL(PLAYER_PED_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is ragdolling")
					ELIF (IS_PED_GETTING_UP(PLAYER_PED_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is getting up")
					ELIF (IS_PLAYER_BEING_ARRESTED(PLAYER_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is being arrested")
					ELIF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is in a vehilce")
					ELIF (IS_PLAYER_CLIMBING(PLAYER_ID()))
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is climbing")
					ELIF (IS_FIRST_PERSON_AIM_CAM_ACTIVE())
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is aiming")
					ENDIF
				ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
		
		IF (IS_PHONE_ONSCREEN())
		OR (IS_PLAYER_PED_SWITCH_IN_PROGRESS())
		OR (IS_TRANSITION_ACTIVE())
		OR (IS_RESULT_SCREEN_DISPLAYING())
		OR (IS_SELECTOR_ONSCREEN())
		OR (g_bScriptsSetSafeForCutscene AND bCheckCutsceneFlag)
		
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					IF IS_PHONE_ONSCREEN()
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Phone on screen")
					ELIF (IS_PLAYER_PED_SWITCH_IN_PROGRESS())
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Swich in progress")
					ELIF (IS_TRANSITION_ACTIVE())
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : MP Hud active")
					ELIF (IS_RESULT_SCREEN_DISPLAYING())
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Result screen is displaying")
					ELIF (IS_SELECTOR_ONSCREEN())
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Switch UI is on screen")
					ELIF (g_bScriptsSetSafeForCutscene)
						CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Scripts configured to play a cutscene")
					ENDIF
				ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
		
		IF (NOT CAN_PLAYER_CHANGE_CLOTHES_ON_MISSION() AND IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE())
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Cant change clothes on this mission")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		// Savehouse must be available
		IF NOT IS_WARDROBE_AVAILABLE(sWardrobeInfo.eSavehouse)
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : The wardrobe is unavailable")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		// Player ped must be valid
		IF sWardrobeInfo.eCharacter <> NO_CHARACTER
			IF NOT IS_PED_THE_CURRENT_PLAYER_PED(sWardrobeInfo.eCharacter)
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						IF g_bDisplayWardrobeDebug
							CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Not available for character")
						ENDIF
					ENDIF
				#ENDIF
			RETURN FALSE
			ENDIF
		ENDIF
		
		// Cops must not be in area.
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		AND IS_COP_PED_IN_AREA_3D(sWardrobeInfo.vWardrobeCoords-<<10.0, 10.0, 10.0>>, sWardrobeInfo.vWardrobeCoords+<<10.0, 10.0, 10.0>>)
		
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : cops are too close")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// Shop needs to be open for changing room.
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NOT IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
			AND NOT IS_LOCAL_PLAYER_IN_IE_WAREHOUSE_THEY_OWN()
			AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
			AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
			AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) 
			AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
			AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
			AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			#IF FEATURE_FIXER
			AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			#ENDIF
			
				IF IS_PLAYER_IN_ANY_SHOP()
				AND NOT IS_SHOP_OPEN_FOR_BUSINESS(GET_SHOP_PLAYER_LAST_USED())
					#IF IS_DEBUG_BUILD
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : shop is closed")
						ENDIF
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF	
			
			IF IS_PLAYER_IN_CORONA()
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : player is in corona")
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Fix for bug # 1948192 - Blocking wardrobe when rampage is active.
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		AND (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RAMPAGE) OR g_bIsOnRampage)
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : player on SP rampage")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// B* 2184349: Block wardrobe while in Director mode
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				CPRINTLN(DEBUG_PED_COMP,"IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player in Director mode")
			ENDIF
			RETURN FALSE
		ENDIF
		
		// Blocking wardrobe when player is on a heist
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				CPRINTLN(DEBUG_PED_COMP,"IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player on heist")
			ENDIF
			RETURN FALSE
		ENDIF
		
		// Blocking wardrobe when player is the beast.
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE - NO: Player is critical to FM event")
				ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					CPRINTLN(DEBUG_PED_COMP, "IS_WARDROBE_SAFE_TO_BROWSE - NO: Player is on a time trial")
				ENDIF
				RETURN FALSE
			ENDIF
		
		// Block shop when wearing juggernaut suit.
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
				PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : wearing jugg suit")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Block shop when wearing juggernaut suit.
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF IS_PED_WEARING_A_DUFFEL_BAG(PLAYER_PED_ID())
				PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : wearing duffel bag")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Blocking wardrobe when in a goon or boss outfit
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : wearing boss outfit")
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
			AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE
				#IF IS_DEBUG_BUILD
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : wearing goon outfit")
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			AND  GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())  = FMMC_TYPE_BIKER_CONTRABAND_SELL 
				#IF IS_DEBUG_BUILD
					PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : FMMC_TYPE_BIKER_CONTRABAND_SELL is active")
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF GET_PLAYER_RIVAL_ENTITY_TYPE(PLAYER_ID()) = eRIVALENTITYTYPE_SMUGGLER
			OR GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
				#IF IS_DEBUG_BUILD
					PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : Player is carrying cargo so blocking shop")
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Blocking wardrobe when yacht is moving
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					PRINTLN("IS_WARDROBE_SAFE_TO_BROWSE() = FALSE : moving yacht")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// No issues found
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Allow the trigger to appear when we end arm1.
FUNC BOOL ALLOW_ENTRY_FOR_THE_END_OF_ARMENIAN1(WARDROBE_DATA_STRUCT &sData)
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("armenian1")) > 0
		sData.bDoEndOfArm1Checks = TRUE
		sData.bArm1ResultsUp = FALSE
		sData.vArm1Coords = <<0,0,0>>
	ELSE
		IF sData.bDoEndOfArm1Checks
			IF ARE_VECTORS_EQUAL(sData.vArm1Coords, <<0,0,0>>)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					sData.vArm1Coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF g_bResultScreenDisplaying
				sData.bArm1ResultsUp = TRUE
			ELIF sData.bArm1ResultsUp
				IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), sData.vArm1Coords-<<0.75, 0.75, 3.0>>, sData.vArm1Coords+<<0.75, 0.75, 3.0>>)
					RETURN TRUE
				ELSE
					sData.bDoEndOfArm1Checks = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Makes a copy of all the current ped components
PROC STORE_CURRENT_PED_COMPONENTS(WARDROBE_DATA_STRUCT &sData)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		// If the cutscene ped exists, set the items on them, otherwise set it on the player
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			GET_PED_VARIATIONS(PLAYER_PED_ID(), sData.sCurrentClothes)
			g_sLastStoredClothesInWardrobe = sData.sCurrentClothes
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.eCrewLogoTattoo = GET_CURRENT_CREW_LOGO_TATTOO()
			ENDIF
		ENDIF
		
		// We are updating the current clothes, make sure we set them on the player
		IF PLAYER_PED_ID() != PLAYER_PED_ID()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())				
				SET_PED_VARIATIONS(PLAYER_PED_ID(), sData.sCurrentClothes)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets player clothes to the ones we previously stored
PROC RESTORE_CURRENT_PED_COMPONENTS(WARDROBE_DATA_STRUCT &sData)
	
	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_VARIATIONS(PLAYER_PED_ID(), sData.sCurrentClothes)
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// re-equip the torso decals (only on jbibs at the moment- special under jbibs are handled in the function)
			PED_COMP_NAME_ENUM eItem
			CPRINTLN(DEBUG_PED_COMP, "Equipping torso decal in RESTORE_CURRENT_PED_COMPONENTS")
			eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), sData.sCurrentClothes.iDrawableVariation[ENUM_TO_INT(PED_COMP_JBIB)], sData.sCurrentClothes.iTextureVariation[ENUM_TO_INT(PED_COMP_JBIB)], COMP_TYPE_JBIB)
			EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eItem, FALSE)
			
			// re-equip crew logo tattoo
			IF sData.eCrewLogoTattoo <> INVALID_TATTOO
				CPRINTLN(DEBUG_PED_COMP, "Re-equipping crew logo tattoo, ",  sData.eCrewLogoTattoo)
				SET_MP_TATTOO_CURRENT(sData.eCrewLogoTattoo, TRUE, g_iPedComponentSlot)
			ENDIF
			
			INT iDLCBerdHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
			INT iDLCSpecialHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_SPECIAL), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
			INT iDLCEyePropHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_EYES), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_EYES))
			
			// Force remove certain items on the player.
			IF sData.sBrowseInfo.eMenu != CLO_MENU_MP_SCUBA_OUTFITS
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCBerdHash, DLC_RESTRICTION_TAG_SCUBA_GEAR, ENUM_TO_INT(SHOP_PED_COMPONENT))
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
					iDLCBerdHash = -1
				ENDIF
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCSpecialHash, DLC_RESTRICTION_TAG_SCUBA_GEAR, ENUM_TO_INT(SHOP_PED_COMPONENT))
					IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
						iDLCSpecialHash = -1
					ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0)
						iDLCSpecialHash = -1
					ENDIF
				ENDIF
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCEyePropHash, DLC_RESTRICTION_TAG_SCUBA_GEAR, ENUM_TO_INT(SHOP_PED_PROP))
					CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
				ENDIF
			ENDIF
			
			
			//Check if we need to shrink the peds head when using restricted tags
			IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCBerdHash, DLC_RESTRICTION_TAG_SHRINK_HEAD, ENUM_TO_INT(SHOP_PED_COMPONENT))
				
				scrPedHeadBlendData playerBlendData
				GET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), playerBlendData)
				
				IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
					CPRINTLN(DEBUG_PED_COMP, "SET_MP_HEIST_GEAR: Force male head to small - Berd item tag is set: DLC_RESTRICTION_TAG_SHRINK_HEAD")
					SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), 0,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0,  playerBlendData.m_texBlend, 0.0)
				ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
					CPRINTLN(DEBUG_PED_COMP, "SET_MP_HEIST_GEAR: Force female head to small - Berd item tag is set: DLC_RESTRICTION_TAG_SHRINK_HEAD")
					
					#IF NOT IS_NEXTGEN_BUILD
					SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), 14,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0, playerBlendData.m_texBlend, 0.0)
					#ENDIF
					#IF IS_NEXTGEN_BUILD
					SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), 21,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0, playerBlendData.m_texBlend, 0.0)
					#ENDIF
				ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
				SET_PED_HEAD_BLEND_HAS_GRANDPARENTS(PLAYER_PED_ID(), FALSE)
				#ENDIF
				#IF IS_NEXTGEN_BUILD
				MICRO_MORPH_TYPE microMorph
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH microMorph
					SET_PED_MICRO_MORPH(PLAYER_PED_ID(), microMorph, 0.0)
				ENDREPEAT
				#ENDIF
				
			ELSE
				INT head0, head1, head2, tex0, tex1, tex2
				FLOAT headBlend, texBlend, varBlend
				BOOL parent
				head0 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD0, g_iPedComponentSlot)
				head1 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD1, g_iPedComponentSlot)
				head2 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD2, g_iPedComponentSlot)
				tex0 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX0, g_iPedComponentSlot)
				tex1 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX1, g_iPedComponentSlot)
				tex2 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX2, g_iPedComponentSlot)
				headBlend 	= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_HEADBLEND, g_iPedComponentSlot)
				texBlend 	= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_TEXBLEND, g_iPedComponentSlot)
				varBlend 	= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_VARBLEND, g_iPedComponentSlot)
				parent 		= GET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_ISPARENT, g_iPedComponentSlot)
				
				CPRINTLN(DEBUG_PED_COMP, "[RESTORE_CURRENT_PED_COMPONENTS]: Force head to normal (head ", head0, ", ", head1, ", ", head2, ", tex ", tex0, ", ", tex1, ", ", tex2, ")")
				SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), head0, head1, head2, tex0, tex1, tex2,headBlend, texBlend, varBlend, parent)
				
				#IF NOT IS_NEXTGEN_BUILD
				SET_PED_HEAD_BLEND_HAS_GRANDPARENTS(PLAYER_PED_ID(), GET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_HASGRANDPARENT, g_iPedComponentSlot))
				#ENDIF
				#IF IS_NEXTGEN_BUILD
				INT iMicroMorph
				MICRO_MORPH_TYPE microMorph
				MP_FLOAT_STATS microMorphStat
				FLOAT fMorphBlend
				
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH iMicroMorph
					microMorph = INT_TO_ENUM(MICRO_MORPH_TYPE, iMicroMorph)
					microMorphStat = GET_CHARACTER_FEATURE_BLEND_STAT(microMorph)
					fMorphBlend = GET_MP_FLOAT_CHARACTER_STAT(microMorphStat, g_iPedComponentSlot)
					
					CPRINTLN(DEBUG_PED_COMP, "	- [micromorph \"", GET_CHARACTER_FEATURE_TEXT(microMorph), "\": ", fMorphBlend)
					
					SET_PED_MICRO_MORPH(PLAYER_PED_ID(), microMorph, fMorphBlend)
				ENDREPEAT
				#ENDIF
			ENDIF
			
			// Fix for url:bugstar:3506519 - Female Clothing - Hairstyles - Multiple Hairstyles are clipping through the local player's hood when the hood option is set to 'Up'
			// Reapply berd whenever we switch jbib to fix hair.
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Attempts to try on selected item
FUNC BOOL TRY_ON_SELECTED_CLOTHING_ITEM(WARDROBE_DATA_STRUCT &sData)

	CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "*")
	CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM")
	CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "*")
	
	sData.bSelectedItemIsCurrent = FALSE
	sData.bCurrentItemIsNew = FALSE
	
	IF sData.sBrowseInfo.iCurrentItem != -1
	AND IS_CLOTHES_MENU_LABEL_OVERRIDE_A_SUB_MENU(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]))
		RETURN FALSE
	ENDIF
	
	IF sData.sBrowseInfo.iCurrentItem = -1
	OR g_bShopMenuOptionDisabled[sData.sBrowseInfo.iCurrentItem]
		RETURN FALSE
	ENDIF
	
	IF sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
	OR (NETWORK_IS_GAME_IN_PROGRESS() AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu) AND NOT g_bShopMenuDisplayMaskOptions)
		RETURN FALSE
	ENDIF
	
	IF g_sShopCompItemData.iItemCount = 0
	AND (sData.sBrowseInfo.eMenu != CLO_MENU_MP_OUTFITS OR NOT NETWORK_IS_GAME_IN_PROGRESS())
		RETURN FALSE
	ENDIF

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, TRUE)
	ENDIF
	
	// Saved outfits
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS
		IF sData.bPreviewing
			SET_MP_OUTFIT_STORED_IN_SLOT(PLAYER_PED_ID(), sData.sBrowseInfo.iCurrentItem, TRUE) // bBlockDecalUpdate
			UPDATE_TATOOS_MP(PLAYER_PED_ID(), TRUE)
			
			PED_COMP_NAME_ENUM eCurrentJbib
			eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
			TATTOO_NAME_ENUM eTorsoDecalTattoo
			eTorsoDecalTattoo = GET_TORSO_DECAL_TATTOO_MP(sData.ePedModel, COMP_TYPE_JBIB, eCurrentJbib, g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSavedOutfit_ShirtDecal[sData.sBrowseInfo.iCurrentItem])
			IF eTorsoDecalTattoo <> INVALID_TATTOO
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), eTorsoDecalTattoo, TRUE)
			ENDIF
			
			// Give temp outfit logos too.
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewA[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_A, TRUE)
			ENDIF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewB[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_B, TRUE)
			ENDIF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewC[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_C, TRUE)
			ENDIF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewD[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_D, TRUE)
			ENDIF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewE[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_E, TRUE)
			ENDIF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.bSaveOutfit_TatCrewF[sData.sBrowseInfo.iCurrentItem] 
				GIVE_PED_TEMP_TATTOO_MP(PLAYER_PED_ID(), TATTOO_MP_FM_CREW_F, TRUE)
			ENDIF
		ELSE
			REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), PLAYER_PED_ID())
			SET_MP_OUTFIT_STORED_IN_SLOT(PLAYER_PED_ID(), sData.sBrowseInfo.iCurrentItem)
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
		ENDIF
		RETURN TRUE
	ENDIF
	
	
	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_COMP_NAME_ENUM eCompName = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
	PED_COMP_TYPE_ENUM eCompType = g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem]
	PED_COMP_ITEM_DATA_STRUCT sItemDetails = CALL sData.fpGetPedCompDataForItem(sData.ePedModel, eCompType, eCompName)
	PED_COMP_NAME_ENUM eJacketRemovalItem = DUMMY_PED_COMP
	PED_COMP_NAME_ENUM eJacketItem = DUMMY_PED_COMP
	PED_COMP_NAME_ENUM eDecalItem = DUMMY_PED_COMP
	//INT iCurrentJbibId = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB)
	INT iDeclDraw, iDeclTex
	
	sData.bSelectedItemIsCurrent = CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName)
	sData.bCurrentItemIsNew = CALL sData.fpIsPedCompItemNew(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, eCompName)
	
	// If we're removing the body warmer, re-equip the torso
	PED_COMP_NAME_ENUM eForcedTorso = DUMMY_PED_COMP
	IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_BODY_WARMER
		eForcedTorso = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TORSO)
	ENDIF
	
	// MP multi decal check
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND eCompType < COMP_TYPE_OUTFIT
	AND g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem] != 0
		IF NOT IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem]))
			sData.bSelectedItemIsCurrent = FALSE
		ENDIF
		sData.bCurrentItemIsNew = (NOT IS_MP_TATTOO_VIEWED(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem])))
	ENDIF
	
	// if selecting "no mask", it is current if not already wearing a mask
	IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_MASK
		IF IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
			sData.bSelectedItemIsCurrent = FALSE
		ELSE
			sData.bSelectedItemIsCurrent = TRUE
		ENDIF
		
	// if selecting "no hat", it is current if not already wearing a hat
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_HAT
		
		PED_COMP_NAME_ENUM eHeadProp = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
		
		IF IS_ITEM_A_HAT(sData.ePedModel, COMP_TYPE_PROPS, eHeadProp)
		OR IS_ITEM_EAR_DEFENDERS(sData.ePedModel, COMP_TYPE_PROPS, eHeadProp)
		OR IS_ITEM_A_HELMET(sData.ePedModel, COMP_TYPE_PROPS, eHeadProp)
		OR IS_ITEM_A_CLOSED_HELMET(sData.ePedModel, COMP_TYPE_PROPS, eHeadProp)
		OR IS_PED_WEARING_A_HAT(PLAYER_PED_ID())
			sData.bSelectedItemIsCurrent = FALSE
		ELSE
			sData.bSelectedItemIsCurrent = TRUE
		ENDIF
		
	// if selecting "no jacket", it is current if not already wearing a jacket
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_JACKET
		BOOL bIsSpecialJacket
		bIsSpecialJacket = FALSE
		
		IF IS_PED_WEARING_JACKET_MP(PLAYER_PED_ID(), bIsSpecialJacket)
			sData.bSelectedItemIsCurrent = FALSE
		ELSE
			sData.bSelectedItemIsCurrent = TRUE
		ENDIF
		
	// if selecting "no body warmer", it is current if not already wearing one
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_BODY_WARMER
		
		IF IS_PED_WEARING_A_BODY_WARMER(PLAYER_PED_ID())
			sData.bSelectedItemIsCurrent = FALSE
		ELSE
			sData.bSelectedItemIsCurrent = TRUE
		ENDIF
		
	// Check some of the 'No item' options that share the same slots.
	ELIF NETWORK_IS_GAME_IN_PROGRESS() AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_EARRINGS
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		PED_COMP_NAME_ENUM eCurrentPropEars = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_EARS))
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_EARRINGS(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth) AND NOT IS_ITEM_EARRINGS(sData.ePedModel, COMP_TYPE_PROPS, eCurrentPropEars))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_TIE
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_TIE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_CHAIN
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_CHAIN(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_SCARF
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_SCARF(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_SWEATBAND
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_SWEATBAND(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_BANGLES
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_BANGLE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_STRAPS
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_STRAP(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_NECKLACE
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_NECKLACE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_CUFFS
		PED_COMP_NAME_ENUM eCurrentPropRightWrist = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_RIGHT_WRIST))
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_A_CUFF(sData.ePedModel, COMP_TYPE_PROPS, eCurrentPropRightWrist))
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_BRACES
		PED_COMP_NAME_ENUM eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		sData.bSelectedItemIsCurrent = (NOT IS_ITEM_BRACES(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
		
	// try and restore jacket aftwerwards
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			eJacketItem = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
			eDecalItem = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_DECL)
			iDeclDraw = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL)
			iDeclTex = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL)
			
			INT iDLCDeclHash = -1
			IF sData.ePedModel = MP_M_FREEMODE_01
				iDLCDeclHash = GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eDecalItem, COMP_TYPE_DECL, 3)
			ELIF sData.ePedModel = MP_F_FREEMODE_01
				iDLCDeclHash = GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eDecalItem, COMP_TYPE_DECL, 4)
			ENDIF
			
			IF IS_JBIB_COMPONENT_A_JACKET(sData.ePedModel, eJacketItem)
				IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_TOP
				AND IS_JBIB_COMPONENT_AN_OPEN_CHECK_SHIRT(sData.ePedModel, eJacketItem)
					eJacketItem = DUMMY_PED_COMP
					eDecalItem = DUMMY_PED_COMP
					CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - player is wearing an open check shirt")
				ELSE
					CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - player is wearing a jacket")
				ENDIF
			ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCDeclHash, DLC_RESTRICTION_TAG_ACCS_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
				eJacketItem = DUMMY_PED_COMP
				// keep decl.
				CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - player is wearing a x17 body armour")
			ELSE
				eJacketItem = DUMMY_PED_COMP
				eDecalItem = DUMMY_PED_COMP
			ENDIF
		ENDIF
	ENDIF
	
	// clear blood decals + sweat from new clothes
	CLEAR_BLOOD_DECALS_FOR_ITEM(PLAYER_PED_ID(), eCompType, sItemDetails.ePropType)
	SET_PED_SWEAT(PLAYER_PED_ID(), 0.0)
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	
	// Track last item previewed
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		g_sShopSettings.eLastPreviewType[ePed] = eCompType
		g_sShopSettings.eLastPreviewItem[ePed] = eCompName
	ENDIF
	
	INT iTattooHash = 0
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND sData.sBrowseInfo.iCurrentItem != -1
	AND g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem] != 0
		iTattooHash = g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem]
	ENDIF

	// Set outfits in full + try to re-equip accessories like glasses, hats + masks (sp only)
	IF eCompType = COMP_TYPE_OUTFIT
		IF NOT NETWORK_IS_GAME_IN_PROGRESS() 
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, eCompName, FALSE, DEFAULT, DEFAULT, sData.bPreviewing, TRUE)
		ELSE
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, eCompName, FALSE, DEFAULT, DEFAULT, sData.bPreviewing)		
			
			// update decals
			CPRINTLN(DEBUG_PED_COMP, "RTNP OUTFIT")
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
			
			// re-equip decal if previewing
			IF	sData.bPreviewing = TRUE
				PED_COMP_NAME_ENUM eCurrentJbib
				eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eCurrentJbib, sData.bPreviewing, iTattooHash)
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	// Special cases for jbib menus (SP only)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS() 
		IF sData.sBrowseInfo.eMenu = CLO_MENU_UNDERSHIRTS_W
			IF NOT IS_PED_WEARING_VARIATION_OF_ITEM(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_OPEN_SHIRT, TORSO_P0_OPEN_SHIRT_15)
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_OPEN_SHIRT, FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
				CALL sData.fpForceValidPedCompComboForItem(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_OPEN_SHIRT)
				CPRINTLN(DEBUG_PED_COMP, "In under shirts menu, equipping Michael's open shirt.")
			ENDIF
		ENDIF
	ENDIF
	
	// try to equip item
	BOOL bEquip = TRUE
	
	
	// Restore last t-shirt?
	BOOL bRestoringLastTop = FALSE
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_JACKET
		IF g_eLastStoredCompName != DUMMY_PED_COMP
			IF g_eLastStoredCompType = COMP_TYPE_JBIB
				eCompName = g_eLastStoredCompName
				eCompType = COMP_TYPE_JBIB
				bRestoringLastTop = TRUE
				CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - restoring old t-shirt from jbib")
			ELIF g_eLastStoredCompType = COMP_TYPE_SPECIAL
				PED_COMP_NAME_ENUM eJBIBFromSpecial = GET_JBIB_FROM_SPECIAL(sData.ePedModel, g_eLastStoredCompName, g_iLastStoredCompTexture)
				IF eJBIBFromSpecial != DUMMY_PED_COMP
					eCompName = eJBIBFromSpecial
					eCompType = COMP_TYPE_JBIB
					bRestoringLastTop = TRUE
					CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - restoring old t-shirt from special")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	// "no mask" option
	IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_MASK
		IF sData.bSelectedItemIsCurrent = FALSE // means we're wearing a mask
			REMOVE_ALL_MASKS(PLAYER_PED_ID(), sData.fpSetPedCompItemCurrent)
		ELSE
			// we're not wearing a mask
			// allow equip if props head none is current
			IF CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName)
				// not wearing a head prop -fine
				CPRINTLN(DEBUG_PED_COMP, "no mask on + no head prop on: allow equip.")
			ELSE
				// wearing a head prop
				// still need to be able to remove any masks that can be worn with hats
				// e.g Franklin's bandana mask
				IF CHECK_STRUCT_FOR_ITEM_OF_TYPE(PLAYER_PED_ID(), sData.sCurrentClothes, TRUE, FALSE)
					// was wearing mask, allow equip
					CPRINTLN(DEBUG_PED_COMP, "found mask in struct.")
					bEquip = TRUE
				ELSE
					// wasn't wearing mask- block
					CPRINTLN(DEBUG_PED_COMP, "didnt find mask in struct.")
					bEquip = FALSE
				ENDIF
			ENDIF
		ENDIF
		
	// "no hat" option 
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_HAT
		IF sData.bSelectedItemIsCurrent = FALSE // means we're wearing a hat
			IF NOT CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName, FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
				CPRINTLN(DEBUG_PED_COMP, "set no hat current failed.")
				bEquip = FALSE
			ELSE
				REMOVE_ALL_HATS(PLAYER_PED_ID(), sData.fpSetPedCompItemCurrent)
			ENDIF
		ELSE
			// we're not wearing a hat
			// allow equip if props head none is current
			bEquip = CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName)
			CPRINTLN(DEBUG_PED_COMP, "no hat on + no head prop on: allow equip.")
			CALL sData.fpSetPedCompItemIsNew(sData.ePedModel, eCompType, eCompName, FALSE)
		ENDIF
		
	// "no jacket" option 
	ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_JACKET
	AND NOT bRestoringLastTop
		IF sData.bSelectedItemIsCurrent = FALSE // means we're wearing a jacket
			eJacketRemovalItem = REMOVE_JACKET_MP(PLAYER_PED_ID(), sData.fpSetPedCompItemCurrent, sData.bPreviewing)
		ELSE
			// we're not wearing a jacket - allow equip
		ENDIF
		
	// Crew t-shirt
	ELIF (eCompType = COMP_TYPE_JBIB
	AND IS_PED_COMPONENT_ITEM_CREW_ITEM(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, eCompName))
		CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - item is a crew shirt")
	
	 	IF ShouldPlayerWearCrewTshirt(TSHIRT_STATES_INGAME_FORCE_ON, GET_ACTIVE_CHARACTER_SLOT())
			CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - dressing player in crew shirt")
			PED_INDEX mPed = PLAYER_PED_ID()
			CALL sData.fpDressFreemodePlayerAtStartTorso(mPed, TSHIRT_STATES_INGAME_FORCE_ON, GET_ACTIVE_CHARACTER_SLOT(), FALSE)
			REMOVE_TATTOOS_NOT_PURCHASED(mPed)
		ENDIF
	ELSE
		// only preview items that are not current!
		IF NOT sData.bSelectedItemIsCurrent
			
			// Remove earrings.
			// if selecting no earrings, remove all earrings.
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_EARRINGS
				IF IS_ITEM_EARRINGS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_TEETH, CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH))
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH, TEETH_FMM_0_0, FALSE, -1, FALSE, sData.bPreviewing)
				ENDIF
				IF IS_ITEM_EARRINGS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_EARS)))
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EARS_NONE, FALSE, -1, FALSE, sData.bPreviewing)
				ENDIF
				
			// normal item
			ELIF NOT CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName, FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
				bEquip = FALSE
			ELSE
			
				IF g_bInMultiplayer
					// Update patches for current clothes
					IF eCompType = COMP_TYPE_JBIB
					OR eCompType = COMP_TYPE_SPECIAL
						CPRINTLN(DEBUG_PED_COMP, "RTNP 6")
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
						
						// re-equip any temp torso decals for this item here:
						IF sData.bPreviewing = TRUE
							IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_JACKET
							AND NOT bRestoringLastTop
								IF eJacketRemovalItem <> DUMMY_PED_COMP
									EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eJacketRemovalItem, sData.bPreviewing, iTattooHash)
								ENDIF
							ELSE
								EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), eCompType, eCompName, sData.bPreviewing, iTattooHash)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// MP multi decal check
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND eCompType < COMP_TYPE_OUTFIT
			AND sData.sBrowseInfo.iCurrentItem != -1
			AND g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem] != 0
				SET_MP_TATTOO_VIEWED(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem]), TRUE)
			ENDIF
		ELSE
			// item is already current: flag it as viewed, man!
			// and return true without doing force valid checks, as item is already equipped
			CALL sData.fpSetPedCompItemIsNew(sData.ePedModel, eCompType, eCompName, FALSE)
			
			// MP multi decal check
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND eCompType < COMP_TYPE_OUTFIT
			AND sData.sBrowseInfo.iCurrentItem != -1
			AND g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem] != 0
				SET_MP_TATTOO_VIEWED(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem]), TRUE)
			ENDIF
			
			bEquip = FALSE
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				// also update the golf outfit if needed
				UPDATE_GOLF_OUTFIT(sData.ePedModel, eCompType, eCompName)
			ELSE
				// equip torso decals for jbib / special items (MP only)
				IF eCompType = COMP_TYPE_JBIB
				OR eCompType = COMP_TYPE_SPECIAL
					CPRINTLN(DEBUG_PED_COMP, "REMOVED: Equipping new jbib / special, update torso decals. bPreviewing = ", sData.bPreviewing)
//					EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), eCompType, eCompName,  sData.bPreviewing, iTattooHash)
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	// if equipped force valid combo
	IF bEquip = TRUE
	
		// Re-equip torso
		IF eForcedTorso != DUMMY_PED_COMP
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TORSO, eForcedTorso, FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
		ENDIF
	
		// if we are equipping a decal, we're really equipping a torso
		// so do the force valid combo for the torso instead
		IF eCompType = COMP_TYPE_DECL
		
			CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM : Equipping a DECL")
			
			PED_COMP_NAME_ENUM eMainItem = eCompName
			
			PED_COMP_NAME_ENUM eRequisite
			eRequisite =  CALL sData.fpGetPedComponentItemRequisite(PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_DECL, eMainItem, COMP_TYPE_TORSO)
			IF eRequisite <> DUMMY_PED_COMP
				CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM : Equipping a DECL, change force combo checks to TORSO. eRequisite= ", eRequisite)
				eCompType = COMP_TYPE_TORSO
				eCompName = eRequisite
			ENDIF
			
			eRequisite =  CALL sData.fpGetPedComponentItemRequisite(PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_DECL, eMainItem, COMP_TYPE_JBIB)
			IF eRequisite <> DUMMY_PED_COMP
				CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM : Equipping a DECL, change force combo checks to JBIB. eRequisite= ", eRequisite)
				eCompType = COMP_TYPE_JBIB
				eCompName = eRequisite
			ENDIF
		ENDIF
		
		// force valid combo
		IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]) = CLO_LBL_NO_JACKET	
		AND NOT bRestoringLastTop
			// jacket removal (don't force valid combo if we're not actually removing jacket
			IF eJacketRemovalItem <> DUMMY_PED_COMP
				REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), eCompType, eJacketRemovalItem)
				CALL sData.fpForceValidPedCompComboForItem(PLAYER_PED_ID(), eCompType, eJacketRemovalItem)
			ENDIF
		ELSE
			// all other items, force valid combo
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF eCompType != COMP_TYPE_OUTFIT
				
					PED_COMP_NAME_ENUM eForceValidItem = eCompName
					PED_COMP_TYPE_ENUM eForceValidType = eCompType
					
					// Re-equip jacket if we set a jbib.
					IF eCompType = COMP_TYPE_JBIB
					AND eJacketItem != DUMMY_PED_COMP
					//AND GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
					AND NOT IS_JBIB_COMPONENT_A_JACKET(sData.ePedModel, eCompName)
						// First grab the SPECIAL version of the JBIB
						PED_COMP_NAME_ENUM eCurrentJBIB = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_JBIB)
						PED_COMP_NAME_ENUM eSpecialForJBIB = GET_SPECIAL_COMP_FOR_JBIB_JACKET_VERSION(sData.ePedModel, eCurrentJBIB, eJacketItem, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
						IF eSpecialForJBIB != DUMMY_PED_COMP
							// Now grab the JBIB version of the SPECIAL
							PED_COMP_NAME_ENUM eJBIBForSpecialJBIB = GET_JBIB_FROM_SPECIAL(sData.ePedModel, eSpecialForJBIB, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
							// Check the JBIB matches the JBIB we are wearing
							IF eJBIBForSpecialJBIB != DUMMY_PED_COMP
							AND eJBIBForSpecialJBIB = eCurrentJBIB
							AND CALL sData.fpCanPedComponentItemMixWithItem(sData.ePedModel, COMP_TYPE_SPECIAL, eSpecialForJBIB, COMP_TYPE_JBIB, eJacketItem, PLAYER_PED_ID())
							AND CALL sData.fpCanPedComponentItemMixWithItem(sData.ePedModel, COMP_TYPE_SPECIAL, eSpecialForJBIB, COMP_TYPE_JBIB, eJacketItem, PLAYER_PED_ID())
								// If this is an open check shirt, make sure we keep the jbib as the main validator
								IF sData.ePedModel = MP_M_FREEMODE_01
								AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eJacketItem, COMP_TYPE_JBIB, 3), DLC_RESTRICTION_TAG_LOW2_OPEN_CHECK, ENUM_TO_INT(SHOP_PED_COMPONENT))
								AND NOT DO_JBIB_AND_LEGS_CHECK_MP(sData.ePedModel, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_LEGS), eCompName)
									CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - re-applying open check shirt")
									CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_JBIB, eJacketItem, FALSE, -1, FALSE, sData.bPreviewing)
								ELIF sData.ePedModel = MP_F_FREEMODE_01
								AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eJacketItem, COMP_TYPE_JBIB, 4), DLC_RESTRICTION_TAG_LOW2_OPEN_CHECK, ENUM_TO_INT(SHOP_PED_COMPONENT))
								AND NOT DO_JBIB_AND_LEGS_CHECK_MP(sData.ePedModel, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_LEGS), eCompName)
									CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - re-applying open check shirt")
									CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_JBIB, eJacketItem, FALSE, -1, FALSE, sData.bPreviewing)	
								ELSE
									CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - re-applying jacket")
									CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_JBIB, eJacketItem, FALSE, -1, FALSE, sData.bPreviewing)
									
									IF eDecalItem != DUMMY_PED_COMP
										IF DO_DECAL_CHECK_MP(sData.ePedModel, eDecalItem, COMP_TYPE_JBIB, eJacketItem)
										AND GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL) = 0
											CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM - re-applying crew emblem")
											SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, iDeclDraw, iDeclTex)
										ENDIF
									ENDIF
									
									// Update force valid checks to query SPECIAL instead of JBIB.
									eForceValidItem = eSpecialForJBIB
									eForceValidType = COMP_TYPE_SPECIAL
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), eCompType, eCompName)
					
					CALL sData.fpForceValidPedCompComboForItem(PLAYER_PED_ID(), eForceValidType, eForceValidItem)
					
					IF eCompType != COMP_TYPE_BERD
						// Remove hazmat suit gas mask if we are no longer wearing suit or suitable helmet
						IF IS_PED_WEARING_HAZ_MASK(PLAYER_PED_ID())
							BOOL bHoodIsUp = FALSE
							BOOL bHoodIsTucked = FALSE
							IS_PED_WEARING_HOODED_JACKET(PLAYER_PED_ID(), bHoodIsUp, bHoodIsTucked)
							
							IF NOT IS_PED_WEARING_HAZ_TOP(PLAYER_PED_ID())
							AND (NOT bHoodIsUp)
							AND (NOT IS_PED_WEARING_A_HELMET(PLAYER_PED_ID()) OR IS_PED_WEARING_A_FULL_FACE_HELMET(PLAYER_PED_ID()))
								CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
							ELIF IS_PED_WEARING_FITTED_HOOD(PLAYER_PED_ID())
								CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE, DEFAULT, DEFAULT, sData.bPreviewing)
							ENDIF
						ENDIF
					ENDIF
					
					// Re-apply crew logo if we are still wearing util vest.
					IF eDecalItem != DUMMY_PED_COMP
					
						INT iDLCDeclHash = -1
						IF sData.ePedModel = MP_M_FREEMODE_01
							iDLCDeclHash = GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eDecalItem, COMP_TYPE_DECL, 3)
						ELIF sData.ePedModel = MP_F_FREEMODE_01
							iDLCDeclHash = GET_NAME_HASH_FROM_PED_COMP_ITEM(sData.ePedModel, eDecalItem, COMP_TYPE_DECL, 4)
						ENDIF
						
						IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCDeclHash, DLC_RESTRICTION_TAG_ACCS_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
							PED_COMP_NAME_ENUM eCurrentSpecial = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
							IF DO_DECAL_CHECK_MP(sData.ePedModel, eDecalItem, COMP_TYPE_SPECIAL, eCurrentSpecial)
							AND GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL) = 0
								CPRINTLN(DEBUG_PED_COMP, GET_PED_COMP_DEBUG_ID(), "TRY_ON_SELECTED_CLOTHING_ITEM : Re-applying crew emblem for accs")
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, iDeclDraw, iDeclTex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), eCompType, eCompName)
				CALL sData.fpForceValidPedCompComboForItem(PLAYER_PED_ID(), eCompType, eCompName)
			ENDIF
		ENDIF
		
		// If jbib has been changed (forced) we will need to check torso decal again
		//IF eCompType != COMP_TYPE_JBIB
		//AND eCompType != COMP_TYPE_SPECIAL
		//AND GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB) != iCurrentJbibId
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
			EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_JBIB), sData.bPreviewing, iTattooHash)
		//ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Adds the wardrobe blip if the player is inside the savehouse
PROC PROCESS_WARDROBE_BLIP(WARDROBE_DATA_STRUCT &sData, BOOL bForceRemove = FALSE)
	#if USE_CLF_DLC
		IF sData.sWardrobeInfo.eSavehouse = NUMBER_OF_CLF_SAVEHOUSE
		OR NOT IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
			EXIT
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF sData.sWardrobeInfo.eSavehouse = NUMBER_OF_NRM_SAVEHOUSE
		OR NOT IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
			EXIT
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF sData.sWardrobeInfo.eSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS
		OR NOT IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
			EXIT
		ENDIF
	#endif
	#endif
	
	
	BOOL bAddBlip = FALSE
	BOOL bStateChanged = FALSE
	
	IF NOT bForceRemove
		IF sData.sWardrobeInfo.eStage != INITIALISE
			IF IS_PLAYER_IN_SAVEHOUSE(sData.sWardrobeInfo.eSavehouse)
			AND IS_WARDROBE_AVAILABLE(sData.sWardrobeInfo.eSavehouse)
				IF (CAN_PLAYER_CHANGE_CLOTHES_ON_MISSION() OR NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE())
					IF (sData.sWardrobeInfo.eCharacter = NO_CHARACTER OR IS_PED_THE_CURRENT_PLAYER_PED(sData.sWardrobeInfo.eCharacter))
						bAddBlip = TRUE
					ELIF (sData.sWardrobeInfo.eWardrobe = PW_MICHAEL_COUNTRYSIDE AND IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR))
						bAddBlip = TRUE
					ELIF (sData.sWardrobeInfo.eWardrobe = PW_TREVOR_COUNTRYSIDE AND IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL))
						bAddBlip = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bAddBlip
		IF NOT DOES_BLIP_EXIST(sData.wardrobeBlipID)
			sData.wardrobeBlipID = ADD_BLIP_FOR_COORD(sData.sWardrobeInfo.vWardrobeCoords)
			SET_BLIP_SPRITE(sData.wardrobeBlipID, RADAR_TRACE_CLOTHES_STORE)
			SET_BLIP_DISPLAY(sData.wardrobeBlipID, DISPLAY_BOTH)
			SET_BLIP_PRIORITY(sData.wardrobeBlipID, BLIPPRIORITY_LOW)
			SET_BLIP_FLASHES(sData.wardrobeBlipID, FALSE)
			SET_BLIP_NAME_FROM_TEXT_FILE(sData.wardrobeBlipID, "WARD_BLIP")
			
			
			PRINTLN("adding wardrobe blip***************************************")
			PRINTLN("...savehouse = ", ENUM_TO_INT(sData.sWardrobeInfo.eSavehouse))
			PRINTLN("...character = ", ENUM_TO_INT(sData.sWardrobeInfo.eCharacter))
			
			bStateChanged = TRUE
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sData.wardrobeBlipID)
			REMOVE_BLIP(sData.wardrobeBlipID)
			bStateChanged = TRUE
			
			PRINTLN("removing wardrobe blip***************************************")
			PRINTLN("...savehouse = ", ENUM_TO_INT(sData.sWardrobeInfo.eSavehouse))
			PRINTLN("...character = ", ENUM_TO_INT(sData.sWardrobeInfo.eCharacter))
			
		ENDIF
	ENDIF
	
	IF bStateChanged OR sData.bUpdateSavehouseBlip
		Update_Savehouse_Respawn_Blip(sData.sWardrobeInfo.eSavehouse)
	ENDIF
ENDPROC

FUNC STRING GET_HEADER_GRAPHIC_FOR_WARDROBE()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL	RETURN "ShopUI_Title_Graphics_Michael" 	BREAK
		CASE CHAR_FRANKLIN	RETURN "ShopUI_Title_Graphics_Franklin"	BREAK
		CASE CHAR_TREVOR	RETURN "ShopUI_Title_Graphics_Trevor"	BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL REQUEST_HEADER_GRAPHIC_FOR_WARDROBE()
	IF NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)
		REQUEST_STREAMED_TEXTURE_DICT(sGraphicHeader)
		bHeaderGraphicRequested = TRUE
		RETURN HAS_STREAMED_TEXTURE_DICT_LOADED(sGraphicHeader)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_HEADER_GRAPHIC_FOR_WARDROBE()
	IF bHeaderGraphicRequested
		IF NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sGraphicHeader)
		ENDIF
		bHeaderGraphicRequested = FALSE
	ENDIF
ENDPROC

FUNC BOOL GET_CHARACTER_COLOUR_WARDROBE(INT &iHeadRed, INT &iHeadGreen, INT &iHeadBlue, INT &iFootRed, INT &iFootGreen, INT &iFootBlue)

	BOOL bColourSet = FALSE
	INT iAlphsDummy
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			GET_HUD_COLOUR(HUD_COLOUR_MICHAEL, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
			iHeadRed = 15
			iHeadGreen = 27
			iHeadBlue = 32
			bColourSet = TRUE
		BREAK
		CASE CHAR_FRANKLIN
			GET_HUD_COLOUR(HUD_COLOUR_FRANKLIN, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
			iHeadRed = 25
			iHeadGreen = 35
			iHeadBlue = 25
			bColourSet = TRUE
		BREAK
		CASE CHAR_TREVOR
			GET_HUD_COLOUR(HUD_COLOUR_TREVOR, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
			iHeadRed = 38
			iHeadGreen = 24
			iHeadBlue = 13
			bColourSet = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bColourSet
ENDFUNC

/// PURPOSE: Cleans up any assets that were created
PROC CLEANUP_WARDROBE(WARDROBE_DATA_STRUCT &sData)
	IF sData.iContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sData.iContextID)
	ENDIF
	
	IF sData.sWardrobeInfo.bActive		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// Restore clothes - to avoid issues caused by Sp to MP force cleanup
		RESTORE_CURRENT_PED_COMPONENTS(sData)
		CPRINTLN(DEBUG_PED_COMP, "Wardrobe cleanup: restoring current clothes.")
		
		// Return to gameplay camera
		IF DOES_CAM_EXIST(sData.sWardrobeInfo.camID)
			DESTROY_CAM(sData.sWardrobeInfo.camID)
		ENDIF
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)

		SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
		CLEANUP_MENU_ASSETS()
		CLEANUP_HEADER_GRAPHIC_FOR_WARDROBE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			CLEANUP_PIM_GLARE(GlareData)
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		UPDATE_PED_WEAPON_ANIMATION_OVERRIDE(PLAYER_PED_ID())
		
		// buddies
		sData.sBuddyHideData.buddyHide = FALSE
		UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
	ENDIF
	
	// clean up audio
	IF sData.bSoundBankRequested
		RELEASE_SCRIPT_AUDIO_BANK()
		CPRINTLN(DEBUG_PED_COMP, "Wardrobe releasing audio bank now.")
		sData.bSoundBankRequested = FALSE
	ENDIF
	
	IF sData.bWaitingForPreload
		RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		sData.bWaitingForPreload = FALSE
	ENDIF
	
	// turn off lights + unload light models
	ACTIVATE_WARDROBE_LIGHT(sData, FALSE)
	ACTIVATE_WARDROBE_HEAD_LIGHT(sData, FALSE)
	UNLOAD_WARDROBE_LIGHT(sData.sWardrobeInfo)
	RESET_ADAPTATION()
	
	PROCESS_WARDROBE_BLIP(sData, TRUE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CLOTHES)
			// shop script will take care of save.
		ELSE
			PROCESS_END_SHOPPING_SAVE(TRUE)
		ENDIF
	ENDIF
	
	Update_Savehouse_Respawn_Blip(sData.sWardrobeInfo.eSavehouse)
	
	g_bWardrobeScriptRunning[ENUM_TO_INT(sData.sWardrobeLauncherData.eWardrobe)] = FALSE
	PRINTLN("Wardrobe[", ENUM_TO_INT(sData.sWardrobeLauncherData.eWardrobe), "] terminating")
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE: Resets the wardrobe if the character changes
PROC CHECK_WARDROBE_RESET(WARDROBE_DATA_STRUCT &sData)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF sData.sWardrobeInfo.eStage = WAIT_FOR_TRIGGER
			IF sData.ePedModel != GET_ENTITY_MODEL(PLAYER_PED_ID())
			OR sData.sBrowseInfo.bReset
			OR (sData.sWardrobeInfo.eCharacter <> NO_CHARACTER AND NOT IS_PED_THE_CURRENT_PLAYER_PED(sData.sWardrobeInfo.eCharacter))
				sData.sWardrobeInfo.eStage = INITIALISE
				sData.iInitStage = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the camera from being rotated past the camera roation limits.
///    Should be called whenever the current camera changes, or the camera is rotated
PROC ENFORCE_CAM_LIMITS(WARDROBE_DATA_STRUCT &sData)
	IF sData.sWardrobeInfo.vCameraRot.z < sData.fLowerCamLimit
		sData.sWardrobeInfo.vCameraRot.z = sData.fLowerCamLimit
	ENDIF
	
	IF sData.sWardrobeInfo.vCameraRot.z > sData.fUpperCamLimit 
		sData.sWardrobeInfo.vCameraRot.z = sData.fUpperCamLimit
	ENDIF
ENDPROC

/// PURPOSE:
///    Calculates the camera rotation limits
///    Needs to be called whenever the current camera changes.
///    Also forces the current rotation to be within the new limits.
PROC CALCULATE_CAM_LIMITS(WARDROBE_DATA_STRUCT &sData)

	FLOAT fDefaultRotation = sData.sWardrobeInfo.fPlayerHead
	
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		fDefaultRotation = 180.0
	ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
		fDefaultRotation = -115.0	
	ENDIF
	
	WHILE fDefaultRotation < -180.0
		fDefaultRotation += 360.0
	ENDWHILE
	WHILE fDefaultRotation > 180.0
		fDefaultRotation -= 360.0
	ENDWHILE
	
	sData.fUpperCamLimit = fDefaultRotation + sData.sWardrobeInfo.fCameraRotMax
	sData.fLowerCamLimit = fDefaultRotation - sData.sWardrobeInfo.fCameraRotMax
	
	CPRINTLN(DEBUG_PED_COMP, "CALCULATE_CAM_LIMITS: fDefaultRotation =", fDefaultRotation,
			", fUpperCamLimit =", sData.fUpperCamLimit,
			", fLowerCamLimit =", sData.fLowerCamLimit)
	CPRINTLN(DEBUG_PED_COMP, "CALCULATE_CAM_LIMITS:   fCameraRotMax =", sData.sWardrobeInfo.fCameraRotMax)
	
	ENFORCE_CAM_LIMITS(sData)
ENDPROC

/// PURPOSE:
///    Sets which camera is being used depending on the menu item being browsed.
PROC SET_CAMERA_FOR_MENU_ITEM(WARDROBE_DATA_STRUCT &sData)

	BOOL bUsingHeadCam = FALSE


	IF NETWORK_IS_GAME_IN_PROGRESS()
		// MP uses clothes shop enums
		IF IS_MP_UPPER_MENU(sData.sBrowseInfo.eMenu)
			SET_WARDROBE_CAM_DATA_TORSO(sData)
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				sData.sWardrobeInfo.fCameraHeightOffset += 0.09
			ENDIF
		ELIF IS_MP_LOWER_MENU(sData.sBrowseInfo.eMenu)
			SET_WARDROBE_CAM_DATA_LEGS(sData)
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				sData.sWardrobeInfo.fCameraFOV += 5.0
			ENDIF
		ELIF IS_MP_FEET_MENU(sData.sBrowseInfo.eMenu)
			SET_WARDROBE_CAM_DATA_FEET(sData)
		ELIF IS_MP_HATS_MENU(sData.sBrowseInfo.eMenu)
		OR IS_MP_MASKS_MENU(sData.sBrowseInfo.eMenu)
		OR IS_MP_GLASSES_MENU(sData.sBrowseInfo.eMenu)
			SET_WARDROBE_CAM_DATA_HEAD(sData)
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				sData.sWardrobeInfo.fCameraHeightOffset += 0.08
			ENDIF
			bUsingHeadCam = TRUE
		ELSE
			// Full view
			SET_WARDROBE_CAM_DATA_OUTFIT(sData)
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				sData.sWardrobeInfo.fCameraHeightOffset += 0.08
			ENDIF
		ENDIF
	ELSE
		// Singleplayer uses wardrobe specifc enums
		SWITCH sData.sBrowseInfo.eMenu
		
			//Uppers
			CASE CLO_MENU_TORSO_W
			CASE CLO_MENU_JACKETS_W
			CASE CLO_MENU_CASUAL_JACKETS_W
			CASE CLO_MENU_CAS_JACKET_TOPS_W
			CASE CLO_MENU_CAS_JACKET_JKTS_W
			CASE CLO_MENU_HOODIES_W 
			CASE CLO_MENU_SHIRTS_W
			CASE CLO_MENU_TSHIRTS_W
			CASE CLO_MENU_POLOSHIRT_W
			CASE CLO_MENU_TANKTOPS_W
			CASE CLO_MENU_SUITVESTS_W
			CASE CLO_MENU_VESTS_W
			CASE CLO_MENU_SUITJACKETS_W
			CASE CLO_MENU_SWEATERS_W
			CASE CLO_MENU_TIES_W
			CASE CLO_MENU_OPENSHIRTS_W
			CASE CLO_MENU_UNDERSHIRTS_W
			CASE CLO_MENU_OPENSHIRTSB_W
				SET_WARDROBE_CAM_DATA_TORSO(sData)
			BREAK
			
			// Lowers
			CASE CLO_MENU_SHORTS_W
			CASE CLO_MENU_LEGS_W
			CASE CLO_MENU_SUITPANTS_W
				SET_WARDROBE_CAM_DATA_LEGS(sData)
			BREAK
			
			// Feet
			CASE CLO_MENU_FEET_W
			CASE CLO_MENU_SUITSHOES_W
				SET_WARDROBE_CAM_DATA_FEET(sData)
			BREAK
			
			// Head
			CASE CLO_MENU_MASKS_W
			CASE CLO_MENU_GLASSES_W
			CASE CLO_MENU_GLASSES_SUB_W
			CASE CLO_MENU_SPORTSHADES_W
			CASE CLO_MENU_STREETSHADES_W
			CASE CLO_MENU_EARRINGS_W
			CASE CLO_MENU_HATS_W
			CASE CLO_MENU_FORWARDSCAPS_W
			CASE CLO_MENU_BACKWARDSCAPS_W
				SET_WARDROBE_CAM_DATA_HEAD(sData)
				bUsingHeadCam = TRUE
			BREAK
			
			// Full view
			DEFAULT
				SET_WARDROBE_CAM_DATA_OUTFIT(sData)
			BREAK
		ENDSWITCH
	ENDIF
	
	// TEMP FIX FOR 1501467 - DISABLE CAMERA MOVEMENT IN SHOP WARDROBE
	IF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_ANY_SHOP()
	AND NOT IS_PROPERTY_STILT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	AND NOT IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) 
	AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())	
	AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())	
	#IF FEATURE_FIXER
		AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
	
		sData.sWardrobeInfo.fCameraRotMax = 0
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PROPERTY_STILT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		CONST_FLOAT fSTILT_WARDROBE_MAX_ROT	25.0
		IF sData.sWardrobeInfo.fCameraRotMax > fSTILT_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fSTILT_WARDROBE_MAX_ROT
		ENDIF
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		CONST_FLOAT fCLUBHOUSE_WARDROBE_MIN_ROT	-20.0
		CONST_FLOAT fCLUBHOUSE_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fCLUBHOUSE_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fCLUBHOUSE_WARDROBE_MAX_ROT
		ENDIF
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		CONST_FLOAT fARMORY_TRUCK_WARDROBE_MAX_ROT	25.0
		IF sData.sWardrobeInfo.fCameraRotMax > fARMORY_TRUCK_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fARMORY_TRUCK_WARDROBE_MAX_ROT
		ENDIF
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_BUNKER(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_HANGAR(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
	
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
		
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
	
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		CONST_FLOAT fBUNKER_WARDROBE_MAX_ROT	15.0
		IF sData.sWardrobeInfo.fCameraRotMax > fBUNKER_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fBUNKER_WARDROBE_MAX_ROT
		ENDIF
	
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		CONST_FLOAT fAUTO_SHOP_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fAUTO_SHOP_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fAUTO_SHOP_WARDROBE_MAX_ROT
		ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	ELIF sData.sWardrobeInfo.eWardrobe = PW_FREEMODE
	AND IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		CONST_FLOAT fFIXER_HQ_WARDROBE_MAX_ROT	20.0
		IF sData.sWardrobeInfo.fCameraRotMax > fFIXER_HQ_WARDROBE_MAX_ROT
			sData.sWardrobeInfo.fCameraRotMax = fFIXER_HQ_WARDROBE_MAX_ROT
		ENDIF
	#ENDIF
	
	ENDIF
	
	// Update the camera limits, and force current rotation to be within the new limits
	CALCULATE_CAM_LIMITS(sData)
	
	// (de)Activate the head light when needed
	CPRINTLN(DEBUG_PED_COMP, "bUsingHeadCam = ", bUsingHeadCam)
	
	IF DOES_ENTITY_EXIST(sData.mWardrobeHeadLight)
		CPRINTLN(DEBUG_PED_COMP, "mWardrobeHeadLight exists")
		// we were previously using the wardrobe head light
		IF bUsingHeadCam = FALSE
			// no longer need head cam
			ACTIVATE_WARDROBE_HEAD_LIGHT(sData, FALSE)
			ACTIVATE_WARDROBE_LIGHT(sData, TRUE)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_PED_COMP, "mWardrobeHeadLight does not exist")
		// we were previously using the normal wardrobe light
		IF bUsingHeadCam = TRUE
			// activate the head cam
			ACTIVATE_WARDROBE_LIGHT(sData, FALSE)
			ACTIVATE_WARDROBE_HEAD_LIGHT(sData, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC BUILD_WARDROBE_MENU(WARDROBE_DATA_STRUCT &sData)
	
	INT iRed, iGreen, iBlue
	INT iRedF, iGreenF, iBlueF
	INT iItemCount, iMenuItem, iFirstMenuitemAvailable
	TEXT_LABEL_15 tlTempLabel
	PED_COMP_NAME_ENUM eCompName
	PED_COMP_TYPE_ENUM eCompType
	PED_COMP_ITEM_DATA_STRUCT sItemData
	
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	CLEAR_MENU_DATA()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INIT_PIM_MENU_HEADER(VHeaderData)
	ENDIF
	
	IF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
		SET_MENU_TITLE("CSHOP_SAVE_OUT")
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			INT iHudR, iHudG, iHudB, iHudA
//			GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
//			SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
//		ENDIF
		
		INT iOutfitSlot
		TEXT_LABEL_31 tlOutfitLabel
		REPEAT MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER iOutfitSlot
			IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
				IF GET_MP_OUTFIT_NAME_IN_SLOT(iOutfitSlot, tlOutfitLabel)
					ADD_MENU_ITEM_TEXT(iOutfitSlot, "PIM_DNAME", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tlOutfitLabel)
				ELSE
					ADD_MENU_ITEM_TEXT(iOutfitSlot, "CSHOP_EMPTY")
				ENDIF
			ELSE
				ADD_MENU_ITEM_TEXT(iOutfitSlot, "CSHOP_EMPTY")
			ENDIF
		ENDREPEAT
		
		SET_CURRENT_MENU_ITEM(sData.iSavedOutfitsItem)
		
		IF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescDelete)
			CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescDelete)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_REMOVED", 4000)
		ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescRename)
			CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescRename)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_RENAMED", 4000)
		ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDesc)
			CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDesc)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_PICKED", 4000)
		ENDIF
		
		// Confirm slot selection
		IF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_SLOT_CONF")
		// Confirm slot delete
		ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_SLOT_REM")
		ENDIF
		
		IF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
		OR IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_YES")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_NO")
		ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
			// Nothing to add.
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SAVE")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF IS_MP_OUTFIT_STORED_IN_SLOT(sData.iSavedOutfitsItem)
			AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_DEL")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_REN")
			ENDIF
		ENDIF
		
		// B*2235469
		IF NOT IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF		
		
		EXIT
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu)
	AND NOT g_bShopMenuDisplayMaskOptions
	
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
		SET_MENU_TITLE(GET_CLOTHES_MENU_NAME_FOR_WARDROBE(sData.sBrowseInfo.eMenu, TRUE))
		
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			INT iHudR, iHudG, iHudB, iHudA
//			GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
//			SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
//		ENDIF
		
		iItemCount = 0
		iFirstMenuitemAvailable = -1
		REPEAT GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu) iMenuItem
			IF IS_BIT_SET(g_iShopMenuMaskMenuOptions[iMenuItem/32], iMenuItem%32)
				IF iFirstMenuitemAvailable = -1
					iFirstMenuitemAvailable = iMenuItem
				ENDIF
				GET_SHOP_SUB_GROUP_LABEL(sData.sBrowseInfo.eMenu, iMenuItem, tlTempLabel)
				IF IS_BIT_SET(g_iShopMenuMaskMenuNewOptions[iMenuItem/32], iMenuItem%32)
					ADD_MENU_ITEM_TEXT(iMenuItem, tlTempLabel, 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				ELSE
					ADD_MENU_ITEM_TEXT(iMenuItem, tlTempLabel)
				ENDIF
				iItemCount++
			ELSE
				IF sData.sBrowseInfo.iCurrentItem = iMenuItem
					sData.sBrowseInfo.iCurrentItem = -1
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF sData.sBrowseInfo.iCurrentItem = -1
			sData.sBrowseInfo.iCurrentItem = iFirstMenuitemAvailable
		ENDIF
		
		g_iShopMenuMaskType = sData.sBrowseInfo.iCurrentItem
		
		IF iItemCount = 0
			ADD_MENU_ITEM_TEXT(0, "WARD_NONE")
		ENDIF
		
		SET_TOP_MENU_ITEM(sData.iMaskMenuTopItem)
		SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_SAVED")
		ENDIF
		
		EXIT
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
	AND sData.sBrowseInfo.iMainMenuGroup = -1
		
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
		SET_MENU_TITLE(GET_CLOTHES_MENU_NAME_FOR_WARDROBE(sData.sBrowseInfo.eMenu, TRUE))
		
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			INT iHudR, iHudG, iHudB, iHudA
//			GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
//			SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
//		ENDIF
		
		iItemCount = 0
		REPEAT 8 iMenuItem
			IF iFirstMenuitemAvailable = -1
				iFirstMenuitemAvailable = iMenuItem
			ENDIF
			tlTempLabel = "CSHOP_WARD_"
			tlTempLabel += iMenuItem
			ADD_MENU_ITEM_TEXT(iMenuItem, tlTempLabel)
			iItemCount++
		ENDREPEAT
		
		IF sData.sBrowseInfo.iCurrentItem = -1
			sData.sBrowseInfo.iCurrentItem = 0
		ENDIF
		
		SET_TOP_MENU_ITEM(0)
		SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
		
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_SAVED")
		ENDIF
		
		EXIT
	ENDIF
	
	// Temporarily switch the menu for MP.
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS
		sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS_TEMP
	ENDIF
	
	SWITCH sData.sBrowseInfo.eMenu
	
		CASE CLO_MENU_MP_OUTFITS_TEMP
		
			sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS
		
			SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
			SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
			SET_MENU_TITLE(GET_CLOTHES_MENU_NAME(CLO_MENU_MP_OUTFITS, TRUE, DEFAULT, TRUE))
			
			SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
			IF GET_CHARACTER_COLOUR_WARDROBE(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
				SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
			ELSE
				SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
			ENDIF
			
			INT iFirstOutfit
			INT iCurrentOutfit
			INT iOutfitSlot
			TEXT_LABEL_31 tlOutfitLabel
			
			iFirstOutfit = -1
			iCurrentOutfit = -1
			
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER iOutfitSlot
				IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
					IF GET_MP_OUTFIT_NAME_IN_SLOT(iOutfitSlot, tlOutfitLabel)
						ADD_MENU_ITEM_TEXT(iOutfitSlot, "PIM_DNAME", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tlOutfitLabel)
					ELSE
						ADD_MENU_ITEM_TEXT(iOutfitSlot, "CSHOP_EMPTY")
					ENDIF
					
					IF iFirstOutfit = -1
						iFirstOutfit = iOutfitSlot
					ENDIF
					
					IF iCurrentOutfit = -1
					AND IS_MP_OUTFIT_STORED_IN_SLOT_CURRENT(PLAYER_PED_ID(), iOutfitSlot)
						ADD_MENU_ITEM_TEXT(iOutfitSlot, "", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TSHIRT)
						iCurrentOutfit = iOutfitSlot
					ELSE
						ADD_MENU_ITEM_TEXT(iOutfitSlot, "")
					ENDIF
					
					iItemCount++
				ENDIF
			ENDREPEAT
			
			IF iItemCount = 0
				ADD_MENU_ITEM_TEXT(0, "CSHOP_NONE")
			ELIF sData.sBrowseInfo.iCurrentItem = -1
				IF iCurrentOutfit != -1
					sData.sBrowseInfo.iCurrentItem = iCurrentOutfit
				ELSE
					sData.sBrowseInfo.iCurrentItem = iFirstOutfit
				ENDIF
			ENDIF
			
			SET_TOP_MENU_ITEM(sData.iTopItem_Sub)
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			
			IF iItemCount > 0
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF iItemCount > 1
				//ADD_MENU_HELP_KEY(ICON_GENERIC_UPDOWN_ARROWS, "ITEM_SCROLL")
			ENDIF
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
			//ADD_MENU_HELP_KEY(ICON_BUTTON_RT, "ITEM_FSCROLL")
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_SAVED")
			ENDIF
		BREAK
		
		CASE CLO_MENU_MAIN
		
			SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
			SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tlTempLabel = "CSHOP_WARDT_"
				tlTempLabel += sData.sBrowseInfo.iMainMenuGroup
				SET_MENU_TITLE(tlTempLabel)
				
//				INT iHudR, iHudG, iHudB, iHudA
//				GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
//				SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
			ELSE
				SET_MENU_TITLE(GET_CLOTHES_MENU_NAME_FOR_WARDROBE(sData.sBrowseInfo.eMenu, TRUE))
			ENDIF
			SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
			IF GET_CHARACTER_COLOUR_WARDROBE(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
				//SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
				SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
			ELSE
				//SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
				SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
			ENDIF
			
			// update the shoes options, as this changes based on current jbib / tie / legs
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				CLEAR_BIT(g_sShopCompItemData.iSubMenusWithItems[(ENUM_TO_INT(CLO_MENU_FEET_W)/32)], (ENUM_TO_INT(CLO_MENU_FEET_W)%32))
				CLEAR_BIT(g_sShopCompItemData.iSubMenusWithItems[(ENUM_TO_INT(CLO_MENU_SUITSHOES_W)/32)], (ENUM_TO_INT(CLO_MENU_SUITSHOES_W)%32))
				SETUP_SP_CLOTHES_FEET(sData.fpSetupClothingItemForShop, g_sShopCompItemData, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_MAIN)
				SETUP_SP_CLOTHES_SUITSHOES(sData.fpSetupClothingItemForShop, g_sShopCompItemData, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_MAIN)
			ENDIF
			
			INT iStartItem, iEndItem
			
			IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
				iStartItem = ENUM_TO_INT(CLO_MENU_FIRST_SP_WARDROBE_MENU)
				iEndItem = ENUM_TO_INT(CLO_MENU_LAST_SP_WARDROBE_MENU)
			ELSE
				// will be changed to include outfits in DLC
				iStartItem = ENUM_TO_INT(CLO_MENU_MP_OUTFITS)
				iEndItem = ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
			ENDIF
			
			// Clear out the array.
			REPEAT COUNT_OF(g_iClothingMenuLookup) iMenuItem
				g_iClothingMenuLookup[iMenuItem] = 0
				g_iClothingMenuIndex[iMenuItem] = 0
			ENDREPEAT
			g_iClothingMenuItemCount = 0
			
			// Configure the order index for each menu item
			FOR iMenuItem = iStartItem TO iEndItem
				IF DOES_PLAYER_HAVE_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, iMenuItem))
					ADD_CLOTHES_MENU_TO_LIST(INT_TO_ENUM(CLOTHES_MENU_ENUM, iMenuItem))
				ENDIF
			ENDFOR
			
			// Now build the menu
			REPEAT COUNT_OF(g_iClothingMenuLookup) iMenuItem
				IF g_iClothingMenuLookup[iMenuItem] != 0
					IF INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[iMenuItem]) != CLO_MENU_CREW_LOGO
					AND DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[iMenuItem]))
						ADD_MENU_ITEM_TEXT(iItemCount, GET_CLOTHES_MENU_NAME_FOR_WARDROBE(INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[iMenuItem]), FALSE), 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					ELSE
						ADD_MENU_ITEM_TEXT(iItemCount, GET_CLOTHES_MENU_NAME_FOR_WARDROBE(INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[iMenuItem]), FALSE))
					ENDIF
					
					IF sData.sBrowseInfo.iCurrentItem = -1
						sData.sBrowseInfo.iCurrentItem = iItemCount
					ENDIF
					
					iItemCount++
				ENDIF
			ENDREPEAT
			
			IF iItemCount = 0
				ADD_MENU_ITEM_TEXT(0, "CSHOP_NONE")
				sData.sBrowseInfo.iCurrentItem = -1
			ENDIF
			
			SET_TOP_MENU_ITEM(sData.iTopItem_Main)
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			
			IF iItemCount > 0
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF iItemCount > 1
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
			ENDIF
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_SAVED")
			ENDIF

			SET_CAMERA_FOR_MENU_ITEM(sData)
			
			// add help text for stars
			IF sData.sBrowseInfo.iCurrentItem != -1
				IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentItem]))
					SET_CURRENT_MENU_ITEM_DESCRIPTION("WARD_STAR", 4000)
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
		
			// update the tie / waistcoat jbib / as this changes based on current jbib / tie / legs
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF sData.sBrowseInfo.eMenu = CLO_MENU_SUITS_W
					CPRINTLN(DEBUG_PED_COMP, "Refreshing tie + suitvest menus")
					CLEAR_BIT(g_sShopCompItemData.iSubMenusWithItems[(ENUM_TO_INT(CLO_MENU_TIES_W)/32)], (ENUM_TO_INT(CLO_MENU_TIES_W)%32))
					CLEAR_BIT(g_sShopCompItemData.iSubMenusWithItems[(ENUM_TO_INT(CLO_MENU_SUITVESTS_W)/32)], (ENUM_TO_INT(CLO_MENU_SUITVESTS_W)%32))
					SETUP_SP_CLOTHES_TIES(sData.fpSetupClothingItemForShop, g_sShopCompItemData, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_SUITS_W)
					SETUP_SP_CLOTHES_SUITVESTS(sData.fpSetupClothingItemForShop, g_sShopCompItemData, GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID())), CLO_MENU_SUITS_W)
				ENDIF
			ENDIF
			
			// if rebuild was triggered by equipping / removing jacket- restore correct current item
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF sData.iCurrentItemBeforeJacketChange <> -1
					CPRINTLN(DEBUG_PED_COMP, "Wardrobe rebuild caused by jacket change: restore current item as: ", sData.iCurrentItemBeforeJacketChange)
					sData.sBrowseInfo.iCurrentItem = sData.iCurrentItemBeforeJacketChange
					sData.iCurrentItemBeforeJacketChange = -1
				ENDIF
			ENDIF
		
			SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
			SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu)
				GET_SHOP_SUB_GROUP_TITLE(sData.sBrowseInfo.eMenu, g_iShopMenuMaskType, tlTempLabel)
				SET_MENU_TITLE(tlTempLabel)
//				INT iHudR, iHudG, iHudB, iHudA
//				GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
//				SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
			ELSE
				// MP menu title stays same for all items in the menu
				SET_MENU_TITLE(GET_CLOTHES_MENU_NAME_FOR_WARDROBE(sData.sBrowseInfo.eMenu, TRUE))
			ENDIF
			
			SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
			IF GET_CHARACTER_COLOUR_WARDROBE(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
				//SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
				SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
			ELSE
				//SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
				SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
				SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
				SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
			ENDIF
			
			SET_CAMERA_FOR_MENU_ITEM(sData)
			
			sData.ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			
			PED_COMP_NAME_ENUM eCurrentSpecialItem, eJbibFromCurrentSpecial
			IF NETWORK_IS_GAME_IN_PROGRESS()
				eCurrentSpecialItem = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
				eJbibFromCurrentSpecial = GET_JBIB_FROM_SPECIAL(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentSpecialItem, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
			ENDIF
			
			BOOL bTopForcedByDecl
			IF NETWORK_IS_GAME_IN_PROGRESS()
				INT iDLCDeclHash
				iDLCDeclHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_DECL), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL))
				IF iDLCDeclHash != 0 AND iDLCDeclHash != -1
					IF DOES_DLC_ITEM_FORCE_PED_COMP_TYPE(iDLCDeclHash, PED_COMP_JBIB)
						bTopForcedByDecl = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF g_sShopCompItemData.iItemCount = 0
				ADD_MENU_ITEM_TEXT(0, "WARD_NONE")
			ELSE
				INT iAltItem
				scrShopPedComponent componentItem
				REPEAT g_sShopCompItemData.iItemCount iMenuItem
				
					// Multi decal items use the same top so no need to keep grabbing data.
					IF iMenuItem > 0
					AND eCompName = g_sShopCompItemData.eItems[iMenuItem]
					AND eCompType = g_sShopCompItemData.eTypes[iMenuItem]
					AND g_iMultiDecalTattooHash_Preset[iMenuItem] != 0
						// Same item...
					ELSE
						eCompName = g_sShopCompItemData.eItems[iMenuItem]
						eCompType = g_sShopCompItemData.eTypes[iMenuItem]
						sItemData = CALL sData.fpGetPedCompDataForItem(sData.ePedModel, eCompType, eCompName)
					ENDIF
					
					IF GET_CLOTHES_SHOP_MENU_LABEL_OVERRIDE(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]), tlTempLabel)
						sItemData.sLabel = tlTempLabel
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_DLC_BIT)
					AND eCompType < COMP_TYPE_OUTFIT
					AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
					AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NONE
						// get tattoo data and use label.
						IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[iMenuItem]), eFaction, PLAYER_PED_ID())
							sItemData.sLabel = sTattooData.sLabel
						ENDIF
						
//						IF NOT IS_MP_TATTOO_VIEWED(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[iMenuItem]))
//							SET_BIT(sItemData.iProperties, PED_COMPONENT_IS_NEW_BIT)
//						ELSE
							CLEAR_BIT(sItemData.iProperties, PED_COMPONENT_IS_NEW_BIT)
//						ENDIF
						
					ELIF eCompType != COMP_TYPE_OUTFIT
						// Clear the tattoo hash if we do not require it
						g_iMultiDecalTattooHash_Preset[iMenuItem] = 0
						g_iMultiDecalTattooHash_Collection[iMenuItem] = 0
					ENDIF
					
					IF IS_CLOTHES_MENU_LABEL_OVERRIDE_A_SUB_MENU(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]))
						IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, (GET_CLOTHES_SUB_MENU_FOR_LABEL_OVERRIDE(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]))))
							ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel, 1, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
							ADD_MENU_ITEM_TEXT(iMenuItem, "")
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel, 0, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
							ADD_MENU_ITEM_TEXT(iMenuItem, "")
						ENDIF
					ELSE
						IF (IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_NEW_BIT))
						AND (INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NONE)
							ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel, 1, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel, 0, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
						ENDIF
						
						
						// no jacket option
						IF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_JACKET
							BOOL bIsSpecialJacket
							bIsSpecialJacket = FALSE
							
							IF IS_PED_WEARING_JACKET_MP(PLAYER_PED_ID(), bIsSpecialJacket)
								// wearing a jacket- not current
								ADD_MENU_ITEM_TEXT(iMenuItem, "", 0, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
							ELSE
								// no jacket- current
								ADD_MENU_ITEM_TEXT(iMenuItem, "", 1, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TSHIRT)
							ENDIF
						
						// others
						ELSE
						
							BOOL bCurrent // is this item current (needed for special case mask check)
							bCurrent = FALSE
							
							PED_COMP_NAME_ENUM eCurrentTeeth
							PED_COMP_NAME_ENUM eCurrentPropEars 
							PED_COMP_NAME_ENUM eCurrentPropRightWrist
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
							AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_HAT
								IF NOT IS_PED_WEARING_A_HAT(PLAYER_PED_ID())
									bCurrent = TRUE
								ENDIF
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_BODY_WARMER
								IF NOT IS_PED_WEARING_A_BODY_WARMER(PLAYER_PED_ID())
									bCurrent = TRUE
								ENDIF
								
							ELIF NETWORK_IS_GAME_IN_PROGRESS() AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_EARRINGS
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								eCurrentPropEars = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_EARS))
								bCurrent = (NOT IS_ITEM_EARRINGS(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth) AND NOT IS_ITEM_EARRINGS(sData.ePedModel, COMP_TYPE_PROPS, eCurrentPropEars))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_TIE
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_TIE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_CHAIN
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_CHAIN(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_SCARF
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_SCARF(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_SWEATBAND
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_SWEATBAND(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_BANGLES
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_BANGLE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_STRAPS
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_STRAP(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_NECKLACE
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_A_NECKLACE(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_CUFFS
								eCurrentPropRightWrist = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_RIGHT_WRIST))
								bCurrent = (NOT IS_ITEM_A_CUFF(sData.ePedModel, COMP_TYPE_PROPS, eCurrentPropRightWrist))
							ELIF INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NO_BRACES
								eCurrentTeeth = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
								bCurrent = (NOT IS_ITEM_BRACES(sData.ePedModel, COMP_TYPE_TEETH, eCurrentTeeth))

							ELIF CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName)
								bCurrent = TRUE
							
							// Check accs/jbib variant
							ELIF NETWORK_IS_GAME_IN_PROGRESS()
							AND eCompType = COMP_TYPE_JBIB
							AND eCompName = eJbibFromCurrentSpecial
							AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) != CLO_LBL_NO_JACKET
							AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) != CLO_LBL_NO_TOP
								IF eCompName >= GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_PED_COMPONENT_FROM_TYPE(eCompType))
								AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
									bCurrent = (GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH) = g_iMultiDecalTattooHash_Preset[iMenuItem])
								ELSE
									bCurrent = TRUE
								ENDIF
							ENDIF
							
							
							// Check alt feet.
							IF NETWORK_IS_GAME_IN_PROGRESS()
							AND eCompType = COMP_TYPE_FEET
								IF eCompName >= GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_PED_COMPONENT_FROM_TYPE(eCompType))
								AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, DLC_RESTRICTION_TAG_HAS_ALT_VERSION, ENUM_TO_INT(SHOP_PED_COMPONENT))
									
									IF GET_ALT_DLC_PED_COMP_OF_TYPE(g_iLastDLCItemNameHash, PED_COMP_FEET, iAltItem)
										GET_SHOP_PED_COMPONENT(iAltItem, componentItem)
										IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET) = componentItem.m_drawableIndex
										AND GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET) = componentItem.m_textureIndex
											bCurrent = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
							IF bCurrent
								// SP no mask option not current if a comptype special mask is equipped
								IF NOT NETWORK_IS_GAME_IN_PROGRESS()
								AND eCompType = COMP_TYPE_PROPS
								AND eCompName = PROPS_HEAD_NONE
									CPRINTLN(DEBUG_PED_COMP, "Doing No Mask check")
									IF IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
										bCurrent = FALSE
									ENDIF
								ENDIF
								
								// MP multi decal check
								IF NETWORK_IS_GAME_IN_PROGRESS()
								AND eCompType < COMP_TYPE_OUTFIT
								AND g_iMultiDecalTattooHash_Preset[iMenuItem] != 0
								AND INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]) = CLO_LBL_NONE
									bCurrent = IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[iMenuItem]))
								ENDIF
								
								// if the current decl component forces this jbib, return false for this jbib state.
								IF NETWORK_IS_GAME_IN_PROGRESS()
								AND (eCompType = COMP_TYPE_JBIB OR eCompType = COMP_TYPE_SPECIAL)
								AND bTopForcedByDecl
									bCurrent = FALSE
								ENDIF
								
								IF bCurrent = TRUE
									ADD_MENU_ITEM_TEXT(iMenuItem, "", 1, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TSHIRT)
								ELSE
									// no mask option not current, show as acquired
									ADD_MENU_ITEM_TEXT(iMenuItem, "", 0, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
								ENDIF
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuItem, "", 0, (NOT g_bShopMenuOptionDisabled[iMenuItem]))
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_TEXT_LABEL_EXIST(sItemData.sLabel)
						CPRINTLN(DEBUG_PED_COMP, "Rebuilding wardrobe[", iItemCount, "] - \"", GET_STRING_FROM_TEXT_FILE(sItemData.sLabel), "\".")
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "Rebuilding wardrobe[", iItemCount, "] - \"", sItemData.sLabel, "\".")
					ENDIF
					
					iItemCount++
				ENDREPEAT
			ENDIF
			
			IF sData.iDepth > 1
				SET_TOP_MENU_ITEM(sData.iTopItem_Sub2)
			ELSE
				SET_TOP_MENU_ITEM(sData.iTopItem_Sub)
			ENDIF
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sData.sItemDesc)
				SET_CURRENT_MENU_ITEM_DESCRIPTION(sData.sItemDesc, 4000)
				sData.sItemDesc = ""
			ENDIF
			
			IF iItemCount > 0
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF iItemCount > 1
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
			ENDIF
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_SAVED")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Ensure the camera is in a suitable location
PROC UPDATE_WARDROBE_CAMERA(WARDROBE_DATA_STRUCT &sData)
		
	IF IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF

	sData.vCurrentBrowseCamLookAt = sData.sWardrobeInfo.vPlayerCoords + <<0.0, 0.0, sData.sWardrobeInfo.fCameraHeightOffset>>

	// -----------Create ----------------------------------------
	IF NOT DOES_CAM_EXIST(sData.sWardrobeInfo.camID)
		sData.sWardrobeInfo.camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	
	IF NOT IS_CAM_ACTIVE(sData.sWardrobeInfo.camID)
		CALCULATE_CAM_LIMITS(sData)
		sData.sWardrobeInfo.vCameraCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.sWardrobeInfo.vPlayerCoords, sData.sWardrobeInfo.vCameraRot.z, sData.sWardrobeInfo.vCameraOffset)
		CPRINTLN(DEBUG_PED_COMP, "vCameraCoords: ", sData.sWardrobeInfo.vCameraCoords, "vPlayerCoords: ", sData.sWardrobeInfo.vPlayerCoords)
		sData.bUpdateWardrobeCamera = TRUE
	ENDIF
	
	// -------------Rotate--------------------------
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
	
	// Add a deadlock but keep values from 0.
	IF ((iRightX  < 32) AND (iRightX > -32)) AND ((iRightY  < 32) AND (iRightY > -32))
	AND NOT IS_PAUSE_MENU_ACTIVE()
	
		IF ((iRightX  < 32) AND (iRightX > -32))
			iRightX = 0
		ELSE
			IF (iRightX  < 0)
				iRightX -= 32
			ELSE
				iRightX += 32
			ENDIF
		ENDIF
		
		IF ((iRightY  < 32) AND (iRightY > -32))
			iRightY = 0
		ELSE
			IF (iRightY  < 0)
				iRightY -= 32
			ELSE
				iRightY += 32
			ENDIF
		ENDIF
	ENDIF
	iRightX *= -1
	IF IS_LOOK_INVERTED()
		iRightY	*= -1
	ENDIF

	// ROTATION
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF HANDLE_MENU_CURSOR(NOT IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename))
			sData.sWardrobeInfo.vCameraRot.z -= g_fMenuCursorXMoveDistance * 400
		ENDIF
	ELSE
	sData.sWardrobeInfo.vCameraRot.z += (iRightX * 0.05)
	ENDIF

	// make sure we're not letting the camera go out of bounds before adjusting its position.
	ENFORCE_CAM_LIMITS(sData)
	
	// POSITION
	VECTOR vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.sWardrobeInfo.vPlayerCoords, sData.sWardrobeInfo.vCameraRot.z, sData.sWardrobeInfo.vCameraOffset)

	sData.sWardrobeInfo.vCameraCoords.x = vCamPos.x
	sData.sWardrobeInfo.vCameraCoords.y = vCamPos.y
	sData.sWardrobeInfo.vCameraCoords.z = vCamPos.z
	
	// ZOOM
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, caZoomInput)
		bZoomPCToggle = !bZoomPCToggle
	ENDIF
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, caZoomInput) // PC Keyboard uses a different input for zoom
	OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND (IS_CONTROL_PRESSED(FRONTEND_CONTROL, caZoomInput) or bZoomPCToggle))
	
		IF sData.fCamZoomAlpha < 1
			sData.fCamZoomAlpha = sData.fCamZoomAlpha + 0.05
		ENDIF
		IF sData.fCamZoomAlpha > 1
			sData.fCamZoomAlpha = 1
		ENDIF
	ELSE
		IF sData.fCamZoomAlpha > 0
			sData.fCamZoomAlpha = sData.fCamZoomAlpha - 0.05
		ENDIF
		IF sData.fCamZoomAlpha < 0
			sData.fCamZoomAlpha = 0
		ENDIF
	ENDIF
	
	FLOAT fZoom = 33
	FLOAT fFOV = sData.sWardrobeInfo.fCameraFOV
	IF sData.fCamZoomAlpha <> 0	
	AND sData.sWardrobeInfo.eStage = BROWSE_WARDROBE
		fFOV = COSINE_INTERP_FLOAT(sData.sWardrobeInfo.fCameraFOV, fZoom, sData.fCamZoomAlpha)
		PRINTLN("UPDATE_WARDROBE_CAMERA - IF sData.fCamZoomAlpha <> 0 fFOV=",fFOV)
	ELSE
		fFOV = sData.sWardrobeInfo.fCameraFOV
		PRINTLN("UPDATE_WARDROBE_CAMERA - IF sData.fCamZoomAlpha = 0 fFOV=",fFOV)
	ENDIF

	// --------------Update-------------------------------------
	SET_CAM_COORD(sData.sWardrobeInfo.camID, sData.sWardrobeInfo.vCameraCoords)
	SET_CAM_FOV(sData.sWardrobeInfo.camID, fFOV)
	POINT_CAM_AT_COORD(sData.sWardrobeInfo.camID, sData.vCurrentBrowseCamLookAt)
	
	IF sData.bUpdateWardrobeCamera
		SET_CAM_ACTIVE(sData.sWardrobeInfo.camID, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		sData.bUpdateWardrobeCamera = FALSE
	ENDIF
ENDPROC

PROC RESTORE_PED_COMPONENTS_AND_TATTOOS(WARDROBE_DATA_STRUCT &sData)
	RESTORE_CURRENT_PED_COMPONENTS(sData)	
	CPRINTLN(DEBUG_PED_COMP, "RTNP 1.")
	REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
ENDPROC

PROC WARDROBE_PRELOAD_FINISHED(WARDROBE_DATA_STRUCT &sData)
	RESTORE_PED_COMPONENTS_AND_TATTOOS(sData)
	BUILD_WARDROBE_MENU(sData)
	TRY_ON_SELECTED_CLOTHING_ITEM(sData)
ENDPROC

PROC HIDE_REMOTE_PLAYERS_WHILE_USING_WARDROBE()
	INT iPlayer
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerId)	 
		AND PLAYER_ID() != PlayerId
			SET_ENTITY_LOCALLY_INVISIBLE(GET_PLAYER_PED(PlayerId))
		ENDIF	
	ENDREPEAT
ENDPROC

/// PURPOSE: Displays the Scaleform UI and handles user input
PROC BROWSE_WARDROBE_ITEMS_AND_DRAW_HUD(WARDROBE_DATA_STRUCT &sData)

	BOOL bMenuRebuilt = FALSE
	BOOL bCursorAccept = FALSE // Mouse and keyboard cursor
	BOOL bJustLeftSubMenu = FALSE

	// Make sure player control is disabled, as sometimes other scripts seem to be turning it back on.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, TRUE)
	ELSE
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffScubaGear, TRUE)
	ENDIF
	
	// PC uses a different frontend input for zooming
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		caZoomInput = INPUT_FRONTEND_LS
	ELSE
		caZoomInput = INPUT_FRONTEND_LT
	ENDIF
	
	// Rebuild menu if controls have changed to ensure instructional buttons are correct for the current control scheme.
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		sData.bRebuildMenu = TRUE
	ENDIF
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	
	// Disable input if we have a message on screen
	IF g_bResultScreenDisplaying
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	ENDIF
	
	HIDE_REMOTE_PLAYERS_WHILE_USING_WARDROBE()

	// handle input
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	PED_COMP_NAME_ENUM eCompName
	PED_COMP_TYPE_ENUM eCompType
	
	IF sData.bRebuildMenu
		TEXT_LABEL strMenuName = GET_CLOTHES_MENU_NAME(sData.sBrowseInfo.eMenu, FALSE, DEFAULT, TRUE)
		IF DOES_TEXT_LABEL_EXIST(strMenuName)
			CPRINTLN(DEBUG_PED_COMP, "Rebuilding \"", GET_STRING_FROM_TEXT_FILE(strMenuName), "\" wardrobe menu now.")
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "Rebuilding \"", strMenuName, "\" wardrobe menu now.")
		ENDIF
		
		IF sData.bTryOnClothesAfterRebuild
		AND NETWORK_IS_GAME_IN_PROGRESS()
		AND(sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS)
			CPRINTLN(DEBUG_PED_COMP, "Don't call RESTORE_CURRENT_PED_COMPONENTS as we're doing preload in masks / hats for MP.")
		ELSE
			RESTORE_PED_COMPONENTS_AND_TATTOOS(sData)
			BUILD_WARDROBE_MENU(sData)
		ENDIF
		
		IF sData.bTryOnClothesAfterRebuild
		
			// preview hats and masks in MP so we dont see hair shrink
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND(sData.sBrowseInfo.eMenu >=  CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS)
			
				IF sData.bWaitingForPreload = FALSE
				
					IF sData.sBrowseInfo.iCurrentItem >= 0
						CPRINTLN(DEBUG_PED_COMP, "Preloading hats / masks for mp.")
						
						eCompName = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
						eCompType = g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem]
						PED_COMP_ITEM_DATA_STRUCT sItemInfo
						
						sItemInfo = CALL sData.fpGetPedCompDataForItem(sData.ePedModel, eCompType, eCompName)
						IF eCompType = COMP_TYPE_PROPS
							IF sItemInfo.iDrawable <>-1
							AND sItemInfo.iDrawable <> 255
								
								CPRINTLN(DEBUG_PED_COMP, "SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), ", sItemInfo.ePropPos, ", ", sItemInfo.iDrawable, ", ", sItemInfo.iTexture, ")")
								
								SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), sItemInfo.ePropPos, sItemInfo.iDrawable, sItemInfo.iTexture)
								sData.bWaitingForPreload = TRUE
							ELSE
								CPRINTLN(DEBUG_PED_COMP, "No item to preload: exit and exit and equip item.")
								WARDROBE_PRELOAD_FINISHED(sData)
							ENDIF
						ELSE
							IF sItemInfo.iDrawable <>-1
							AND sItemInfo.iTexture <> -1
								SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType), sItemInfo.iDrawable, sItemInfo.iTexture)
								sData.bWaitingForPreload = TRUE
							ELSE
								CPRINTLN(DEBUG_PED_COMP, "No item to preload: exit and equip item.")
								WARDROBE_PRELOAD_FINISHED(sData)
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "No item to preload: exit and equip item.")
						WARDROBE_PRELOAD_FINISHED(sData)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_PED_COMP, "Waiting for preload: hats / masks for mp.")
					IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
					AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "Preload finished: hats / masks for mp.")
						WARDROBE_PRELOAD_FINISHED(sData)
						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
						sData.bWaitingForPreload = FALSE
					ENDIF
				ENDIF
			ELSE
				TRY_ON_SELECTED_CLOTHING_ITEM(sData)
			ENDIF
		ENDIF
		
		IF sData.bWaitingForPreload = FALSE
			sData.bTryOnClothesAfterRebuild = FALSE
			sData.bRebuildMenu = FALSE
		ENDIF
		
		bMenuRebuilt = TRUE
	ENDIF
	
	BOOL bOutfitsMenu = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu))
	BOOL bRenameOutfit = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) AND NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu))
	BOOL bDeleteOutfit = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
	
	BOOL bSafeToBrowse = FALSE
	IF (NOT IS_PAUSE_MENU_ACTIVE() AND sData.sBrowseInfo.eStage != BROWSE_CLEANUP AND NOT bMenuRebuilt AND NOT sData.bRebuildMenu AND NOT sData.bTryOnClothesAfterRebuild AND NOT NETWORK_TEXT_CHAT_IS_TYPING())
		bSafeToBrowse = TRUE
	ENDIF
	
	IF (NETWORK_IS_GAME_IN_PROGRESS() AND NOT bMainMenuInitialised)
		bSafeToBrowse = FALSE
	ENDIF
	
	RESET_MOUSE_POINTER_FOR_PAUSE_OR_ALERT() // Resets the mouse pointer if a pause screen or alert screen occurs.
	
	IF bSafeToBrowse
	AND ((bOutfitsMenu AND NOT IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)) OR (IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)))
	
		// SAVE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				
				// highlight should move while in the outfit name editor
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS(DEFAULT, DEFAULT, DEFAULT, NOT IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename))
				
				// for some reason - cursor accept was being assigned well below this section
				bCursorAccept = FALSE
				IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					IF (g_sMenuData.iCurrentItem != g_iMenuCursorItem)
						SET_CURRENT_MENU_ITEM(g_iMenuCursorItem)
					ELSE
						sData.iSavedOutfitsItem = g_iMenuCursorItem
						bCursorAccept = TRUE
					ENDIF
				ENDIF
				
			
				// Build slot selection menu
				IF NOT IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)
					sData.iSavedOutfitsItem = 0
					sData.iSaveOutfitFlags = 0
					SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)
					sData.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
				// Confirm slot selection
				ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
					
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR bCursorAccept = TRUE
						// SAVE
						STORE_PED_CLOTHES_IN_MP_OUTFIT_SLOT(PLAYER_PED_ID(), sData.iSavedOutfitsItem)
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
						sData.bRebuildMenu = TRUE
						//SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDesc)
						//SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_PICKED", 4000)
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
						// Force player to rename outfit
						SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
						SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRenameSave)
						tlNewOutfitName = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.tlSavedOutfit_Name[sData.iSavedOutfitsItem]
						
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
					OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
					
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
					OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
						sData.bRebuildMenu = TRUE
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
					ENDIF
					
				// Confirm slot delete
				ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
				
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR bCursorAccept = TRUE
						// REMOVE
						REMOVE_MP_OUTFIT_IN_SLOT(sData.iSavedOutfitsItem)
						sData.bRebuildMenu = TRUE
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
						SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescDelete)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_REMOVED", 4000)
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR IS_MENU_CURSOR_CANCEL_PRESSED()
					
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
					OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
					
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
					OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
					
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
						sData.bRebuildMenu = TRUE
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
					ENDIF
					
				// Process slot rename
				ELIF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
					
					IF RUN_ON_SCREEN_KEYBOARD(sKeyBoardData.eStatus, sKeyBoardData.iProfanityToken, sKeyBoardData.iCurrentStatus, FALSE, FALSE, tlNewOutfitName, FALSE, 0, FALSE, FALSE, DEFAULT, FALSE, TRUE)
						tlNewOutfitName = GET_ONSCREEN_KEYBOARD_RESULT()
						sKeyBoardData.eStatus = OSK_PENDING
						sKeyBoardData.iCurrentstatus	= OSK_STAGE_SET_UP					
						sKeyBoardData.iProfanityToken = 0
						
						// RENAME
						IF NOT IS_STRING_NULL_OR_EMPTY(tlNewOutfitName)
							SET_MP_OUTFIT_NAME_IN_SLOT(sData.iSavedOutfitsItem, tlNewOutfitName)
							sData.bRebuildMenu = TRUE
							
							IF IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRenameSave)
								SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDesc)
							ELSE
								SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDescRename)
							ENDIF
							
							CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
							CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRenameSave)
							
							PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
							
						ELSE
							// CANCEL
							CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
							IF IS_STRING_NULL_OR_EMPTY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.tlSavedOutfit_Name[sData.iSavedOutfitsItem])
								REMOVE_MP_OUTFIT_IN_SLOT(sData.iSavedOutfitsItem)
							ENDIF
							
							sData.bRebuildMenu = TRUE
							PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
							
						ENDIF
						
						tlNewOutfitName = ""
					ENDIF
					
					IF sKeyBoardData.eStatus = OSK_CANCELLED
						sKeyBoardData.eStatus = OSK_PENDING
						sKeyBoardData.iCurrentstatus	= OSK_STAGE_SET_UP					
						sKeyBoardData.iProfanityToken = 0
						
						// CANCEL
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
						IF IS_STRING_NULL_OR_EMPTY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.tlSavedOutfit_Name[sData.iSavedOutfitsItem])
							REMOVE_MP_OUTFIT_IN_SLOT(sData.iSavedOutfitsItem)
						ENDIF
						sData.bRebuildMenu = TRUE
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
					ENDIF
					
				// Make slot selection
				ELSE
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR bCursorAccept = TRUE
						IF IS_MP_OUTFIT_STORED_IN_SLOT(sData.iSavedOutfitsItem)
							SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirm)
							sData.bRebuildMenu = TRUE
						ELSE
							// SAVE
							STORE_PED_CLOTHES_IN_MP_OUTFIT_SLOT(PLAYER_PED_ID(), sData.iSavedOutfitsItem)
							//CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)
							sData.bRebuildMenu = TRUE
							//SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitDesc)
							//SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_PICKED", 4000)
							
							// Force player to rename outfit
							SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
							SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRenameSave)
							tlNewOutfitName = GET_FILENAME_FOR_AUDIO_CONVERSATION("ITEM_NEW_OUT")
							tlNewOutfitName += " "
							tlNewOutfitName += sData.iSavedOutfitsItem
						ENDIF
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						bCursorAccept = FALSE
					ELIF bDeleteOutfit
						IF IS_MP_OUTFIT_STORED_IN_SLOT(sData.iSavedOutfitsItem)
							SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitConfirmDelete)
							sData.bRebuildMenu = TRUE
						ENDIF
						
					ELIF bRenameOutfit
						IF IS_MP_OUTFIT_STORED_IN_SLOT(sData.iSavedOutfitsItem)
							SET_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename)
							tlNewOutfitName = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.tlSavedOutfit_Name[sData.iSavedOutfitsItem]
							sData.bRebuildMenu = TRUE
						ENDIF
						
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR IS_MENU_CURSOR_CANCEL_PRESSED()
						CLEAR_BIT(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitTriggered)
						sData.bRebuildMenu = TRUE
						sData.bTryOnClothesAfterRebuild = TRUE
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
					OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
					
						sData.sInputData.bDPADDOWNReset = FALSE
						sData.sInputData.bLeftStickUDReset = FALSE
						sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
						sData.iSavedOutfitsItem++
						IF sData.iSavedOutfitsItem >= MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER
							sData.iSavedOutfitsItem = 0
						ENDIF
						sData.bRebuildMenu = TRUE
					
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
					OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
					
						sData.sInputData.bDPADUPReset = FALSE
						sData.sInputData.bLeftStickUDReset = FALSE
						sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
						ENDIF	
						
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
						sData.iSavedOutfitsItem--
						IF sData.iSavedOutfitsItem < 0
							sData.iSavedOutfitsItem = MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER-1
						ENDIF
						sData.bRebuildMenu = TRUE
					ENDIF
				ENDIF
			ENDIF
		
	ELIF bSafeToBrowse
	
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		
		BOOL bCancelPressed 
		bCancelPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							  OR IS_MENU_CURSOR_CANCEL_PRESSED() OR SHOULD_BACK_OUT_OF_SHOP_MENU()				
		RESET_BACK_OUT_OF_SHOP_MENU_GLOBAL()
		
		// Temporarily switch the menu for MP.
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS
			sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS_TEMP
		ENDIF
		
		SWITCH sData.sBrowseInfo.eMenu
			
			CASE CLO_MENU_MP_OUTFITS_TEMP
					
				sData.sBrowseInfo.eMenu = CLO_MENU_MP_OUTFITS
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
					
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
					IF sData.sBrowseInfo.iCurrentItem != -1
					
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
					 	INT iOutfitSlot
						INT iStartItem
						BOOL bFoundItem
						
						iStartItem = sData.sBrowseInfo.iCurrentItem-1
						FOR iOutfitSlot = iStartItem TO 0 STEP -1
							IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
								sData.sBrowseInfo.iCurrentItem = iOutfitSlot
								iOutfitSlot = -1//Bail
								bFoundItem = TRUE
							ENDIF
						ENDFOR
						
						IF NOT bFoundItem
							FOR iOutfitSlot = MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER-1 TO iStartItem STEP -1
								IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
									sData.sBrowseInfo.iCurrentItem = iOutfitSlot
									iOutfitSlot = iStartItem//Bail
									bFoundItem = TRUE
								ENDIF
							ENDFOR
						ENDIF
						
						sData.bTryOnClothesAfterRebuild = TRUE
						sData.bPreviewing = TRUE
						sData.bRebuildMenu = TRUE
						sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
					ENDIF
					
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
				
					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
					IF sData.sBrowseInfo.iCurrentItem != -1
					
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
					 	INT iOutfitSlot
						INT iStartItem
						BOOL bFoundItem
						
						iStartItem = sData.sBrowseInfo.iCurrentItem+1
						FOR iOutfitSlot = iStartItem TO MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER-1
							IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
								sData.sBrowseInfo.iCurrentItem = iOutfitSlot
								iOutfitSlot = MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER//Bail
								bFoundItem = TRUE
							ENDIF
						ENDFOR
						
						IF NOT bFoundItem
							FOR iOutfitSlot = 0 TO iStartItem
								IF IS_MP_OUTFIT_STORED_IN_SLOT(iOutfitSlot)
									sData.sBrowseInfo.iCurrentItem = iOutfitSlot
									iOutfitSlot = iStartItem+1//Bail
									bFoundItem = TRUE
								ENDIF
							ENDFOR
						ENDIF
						
						sData.bTryOnClothesAfterRebuild = TRUE
						sData.bPreviewing = TRUE
						sData.bRebuildMenu = TRUE
						sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
					ENDIF
				
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR (bCursorAccept = TRUE))
				AND sData.sBrowseInfo.iCurrentItem != -1
						
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
					sData.bPreviewing = FALSE
					TRY_ON_SELECTED_CLOTHING_ITEM(sData)
					STORE_CURRENT_PED_COMPONENTS(sData)
					
					sData.bClothesChanges = TRUE
					
					sData.bRebuildMenu = TRUE
					sData.bTryOnClothesAfterRebuild = TRUE
					
				// Return to main menu
				ELIF bCancelPressed
				
					sData.iDepth--
					sData.bPreviewing = FALSE
					sData.bRebuildMenu = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iCurrentGroup
					
					sData.sBrowseInfo.eMenu = CLO_MENU_MAIN		
					
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
				ENDIF
			BREAK
			
			CASE CLO_MENU_MAIN
				
				// Handle Menu mouse support
				IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						
					// Selecting again the currently selected item = buy
					IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
						bCursorAccept= TRUE
					ELSE
					
						sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
					
						sData.bUpdateWardrobeCamera = TRUE
							
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
							
						PRINTLN("current item is now ", sData.sBrowseInfo.iCurrentItem)
							
						sData.iTopItem_Main = GET_TOP_MENU_ITEM()
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						
						// add help text for stars
						IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, sData.sBrowseInfo.iCurrentItem))
							SET_CURRENT_MENU_ITEM_DESCRIPTION("WARD_STAR", 4000)
						ENDIF
					
					ENDIF
			
				ENDIF
				
									
				// -------------Group Selection Menu ---------------------------------- 		
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE)
				AND sData.sBrowseInfo.iCurrentItem != -1
				
					// Main menu group selection
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.iMainMenuGroup = -1
						sData.sBrowseInfo.iMainMenuGroup = sData.sBrowseInfo.iCurrentItem
						
						// Fast trask to mask menu
						IF sData.sBrowseInfo.iMainMenuGroup = 5 // MASKS
							sData.sBrowseInfo.eMenu = CLO_MENU_MASKS
							sData.sBrowseInfo.iCurrentGroup = g_iClothingMenuLookup[ENUM_TO_INT(CLO_MENU_MASKS)]
							sData.sBrowseInfo.iCurrentItem = 0
							g_bShopMenuDisplayMaskOptions = FALSE
							g_iShopMenuMaskType = 0
							sData.iMaskMenuTopItem = 0
						ENDIF
						
						sData.bRebuildMenu = TRUE
						sData.bUpdateWardrobeCamera = TRUE
						sData.sBrowseInfo.iCurrentItem = 0
						// Build the array required to search the items 
						CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
						GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ELSE
						sData.iDepth++
						sData.bRebuildMenu = TRUE
						sData.bTryOnClothesAfterRebuild = TRUE
						sData.bUpdateWardrobeCamera = TRUE
						sData.sBrowseInfo.iCurrentGroup = sData.sBrowseInfo.iCurrentItem
						sData.sBrowseInfo.eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentItem])
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
						AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu)
							PRINTLN("kr_test: mask menu selected")
							g_bShopMenuDisplayMaskOptions = FALSE
							g_iShopMenuMaskType = 0
						ENDIF
						
						// Build the array required to search the items 
						CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
						
						sData.sBrowseInfo.iCurrentItem = -1
						GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						sData.bPreviewing = TRUE // 1st item tried on in menu will be a preview
						bCursorAccept = FALSE
						
					ENDIF
					
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
					OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
					
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF	
					IF sData.bWaitingForPreload
						PRINTLN("release preload on FRONTEND UP (CLO_MENU_MAIN)")
			
						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
						sData.bWaitingForPreload = FALSE
					ENDIF

					// Main menu group selection
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.iMainMenuGroup = -1
						sData.sBrowseInfo.iCurrentItem--
						IF sData.sBrowseInfo.iCurrentItem < 0
							sData.sBrowseInfo.iCurrentItem = 7
						ENDIF
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ELSE
											
						IF GET_PREV_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
							sData.bUpdateWardrobeCamera = TRUE
							SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
							
							PRINTLN("current item is now ", sData.sBrowseInfo.iCurrentItem)
							
							
							sData.iTopItem_Main = GET_TOP_MENU_ITEM()
							PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						ENDIF
						
						// add help text for stars
						IF sData.sBrowseInfo.iCurrentItem != -1
							IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentItem]))
								SET_CURRENT_MENU_ITEM_DESCRIPTION("WARD_STAR", 4000)
							ENDIF
						ENDIF
					
					ENDIF
					
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
					OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
				
					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					IF sData.bWaitingForPreload
						PRINTLN("release preload on FRONTEND DOWN (CLO_MENU_MAIN)")
			
						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
						sData.bWaitingForPreload = FALSE
					ENDIF
					
					// Main menu group selection
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.iMainMenuGroup = -1
						sData.sBrowseInfo.iCurrentItem++
						IF sData.sBrowseInfo.iCurrentItem > 7
							sData.sBrowseInfo.iCurrentItem = 0
						ENDIF
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ELSE
					
					IF GET_NEXT_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
						sData.bUpdateWardrobeCamera = TRUE
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						
						PRINTLN("current item is now ", sData.sBrowseInfo.iCurrentItem)
						
						sData.iTopItem_Main = GET_TOP_MENU_ITEM()
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ENDIF
					
					// add help text for stars
					IF sData.sBrowseInfo.iCurrentItem != -1
						IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentItem]))
							SET_CURRENT_MENU_ITEM_DESCRIPTION("WARD_STAR", 4000)
						ENDIF
					ENDIF
					
					ENDIF
					
				// Check for exit
				ELIF bCancelPressed
					
					// Main menu group selection
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.iMainMenuGroup != -1
						sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iMainMenuGroup
						sData.sBrowseInfo.iMainMenuGroup = -1
						sData.bRebuildMenu = TRUE
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ELSE
					
					sData.bDoFinaliseHeadBlend = TRUE
					// Reset the clothes to the current
					IF NETWORK_IS_GAME_IN_PROGRESS()	
						RESTORE_CURRENT_PED_COMPONENTS(sData)  
						CPRINTLN(DEBUG_PED_COMP, "RTNP 5.")
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())  //Has to be done here, not in cleanup - all alterations to the ped have to happen before the head blend is finalized
					ENDIF
					sData.sBrowseInfo.eStage = BROWSE_CLEANUP
					
					ENDIF
				ENDIF
			BREAK
			
			DEFAULT
			
				// Handle Menu mouse support
//				IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
//					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
//				ENDIF

				IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						
					// Selecting again the currently selected item = buy
					IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
						bCursorAccept= TRUE
					ELSE
					
						// Cancel any previously started preloads
						IF sData.bWaitingForPreload
							PRINTLN("release preload on mouse selection (CLO_MENU_", sData.sBrowseInfo.eMenu, ")")
							
							RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
							RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
							sData.bWaitingForPreload = FALSE
						ENDIF
				
						// If new item is selectable
						IF g_sShopCompItemData.eItems[g_iMenuCursorItem] != DUMMY_PED_COMP	// GET_NEXT_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)

							sData.bPreviewing = TRUE
							
							sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
							
							IF sData.bCurrentItemIsNew
							OR (NETWORK_IS_GAME_IN_PROGRESS()
							AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
								sData.bRebuildMenu = TRUE
								sData.bTryOnClothesAfterRebuild = TRUE
							ELSE
								RESTORE_CURRENT_PED_COMPONENTS(sData)
								SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
								
								// do before try on here, so we get the previewed decal
								CPRINTLN(DEBUG_PED_COMP, "RTNP 3/4.")
								REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
								TRY_ON_SELECTED_CLOTHING_ITEM(sData)
							ENDIF
							
							IF sData.iDepth > 1
								sData.iTopItem_Sub2 = GET_TOP_MENU_ITEM()
							ELSE
								sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
							ENDIF
							PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")

							// add help text for stars
							IF DOES_PLAYER_HAVE_NEW_OPTIONS_IN_MENU(g_sShopCompItemData, INT_TO_ENUM(CLOTHES_MENU_ENUM, sData.sBrowseInfo.iCurrentItem))
								SET_CURRENT_MENU_ITEM_DESCRIPTION("WARD_STAR", 4000)
							ENDIF
						ENDIF
					
					ENDIF
			
				ENDIF
				
				// Mask sub menu selection
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu) AND NOT g_bShopMenuDisplayMaskOptions)
				AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bCursorAccept = TRUE)
				AND sData.sBrowseInfo.iCurrentItem != -1
				
					g_bShopMenuDisplayMaskOptions = TRUE
					
					sData.iDepth++
					sData.bRebuildMenu = TRUE
					sData.bTryOnClothesAfterRebuild = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentItem = 0
					sData.sBrowseInfo.iCurrentSubItem = g_iShopMenuMaskType
					
					// Build the array required to search the items 
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
				// Back out of mask sub menu
				ELIF (NETWORK_IS_GAME_IN_PROGRESS() AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu) AND g_bShopMenuDisplayMaskOptions)
				AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))
				AND sData.sBrowseInfo.iCurrentSubItem != -1
				
					g_bShopMenuDisplayMaskOptions = FALSE
					
					sData.iDepth--
					sData.bRebuildMenu = TRUE
					sData.bTryOnClothesAfterRebuild = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iCurrentSubItem
					sData.sBrowseInfo.iCurrentSubItem = -1
					
					// Build the array required to search the items 
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					//GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
				// Browse mask menus
				ELIF (NETWORK_IS_GAME_IN_PROGRESS() AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu) AND NOT g_bShopMenuDisplayMaskOptions)
				AND (sData.sBrowseInfo.iCurrentItem != -1) AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
						OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
						OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset))
						
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
					g_iShopMenuMaskType--
					WHILE (g_iShopMenuMaskType >= 0 AND NOT IS_BIT_SET(g_iShopMenuMaskMenuOptions[g_iShopMenuMaskType/32], g_iShopMenuMaskType%32))
						g_iShopMenuMaskType--
					ENDWHILE
					IF g_iShopMenuMaskType < 0
						g_iShopMenuMaskType = GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu)-1
					ENDIF
					WHILE (g_iShopMenuMaskType >= 0 AND NOT IS_BIT_SET(g_iShopMenuMaskMenuOptions[g_iShopMenuMaskType/32], g_iShopMenuMaskType%32))
						g_iShopMenuMaskType--
					ENDWHILE
					IF g_iShopMenuMaskType < 0
						g_iShopMenuMaskType = 0
					ENDIF
					
					sData.sBrowseInfo.iCurrentItem = g_iShopMenuMaskType
					sData.iMaskMenuTopItem = GET_TOP_MENU_ITEM()
					
					sData.bPreviewing = TRUE
					
					IF sData.bCurrentItemIsNew
					OR (NETWORK_IS_GAME_IN_PROGRESS()
					AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
						sData.bRebuildMenu = TRUE
						sData.bTryOnClothesAfterRebuild = TRUE
					ELSE
						RESTORE_CURRENT_PED_COMPONENTS(sData)
						CPRINTLN(DEBUG_PED_COMP, "RTNP 3.")
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						TRY_ON_SELECTED_CLOTHING_ITEM(sData)
					ENDIF
					
					IF sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
						sData.iTopItem_Main = GET_TOP_MENU_ITEM()
					ELSE
						IF sData.iDepth > 1
							sData.iTopItem_Sub2 = GET_TOP_MENU_ITEM()
						ELSE
							sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
						ENDIF
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
				
				
				ELIF (NETWORK_IS_GAME_IN_PROGRESS() AND DOES_SHOP_MENU_HAVE_SUB_GROUPS(sData.sBrowseInfo.eMenu) AND NOT g_bShopMenuDisplayMaskOptions)
				AND (sData.sBrowseInfo.iCurrentItem != -1) AND 
					(IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD > 100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset))

					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
					g_iShopMenuMaskType++
					WHILE (g_iShopMenuMaskType < GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu) AND NOT IS_BIT_SET(g_iShopMenuMaskMenuOptions[g_iShopMenuMaskType/32], g_iShopMenuMaskType%32))
						g_iShopMenuMaskType++
					ENDWHILE
					IF g_iShopMenuMaskType >= GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu)
						g_iShopMenuMaskType = 0
					ENDIF
					WHILE (g_iShopMenuMaskType < GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu) AND NOT IS_BIT_SET(g_iShopMenuMaskMenuOptions[g_iShopMenuMaskType/32], g_iShopMenuMaskType%32))
						g_iShopMenuMaskType++
					ENDWHILE
					IF g_iShopMenuMaskType >= GET_SHOP_SUB_GROUP_COUNT(sData.sBrowseInfo.eMenu)
						g_iShopMenuMaskType = 0
					ENDIF
					
					sData.sBrowseInfo.iCurrentItem = g_iShopMenuMaskType
					sData.iMaskMenuTopItem = GET_TOP_MENU_ITEM()
					
					sData.bPreviewing = TRUE
					
					IF sData.bCurrentItemIsNew
					OR (NETWORK_IS_GAME_IN_PROGRESS()
					AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
						sData.bRebuildMenu = TRUE
						sData.bTryOnClothesAfterRebuild = TRUE
					ELSE
						RESTORE_CURRENT_PED_COMPONENTS(sData)
						CPRINTLN(DEBUG_PED_COMP, "RTNP 3.")
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						TRY_ON_SELECTED_CLOTHING_ITEM(sData)
					ENDIF
					
					IF sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
						sData.iTopItem_Main = GET_TOP_MENU_ITEM()
					ELSE
						IF sData.iDepth > 1
							sData.iTopItem_Sub2 = GET_TOP_MENU_ITEM()
						ELSE
							sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
						ENDIF
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
					// We know the mouse is inside the menu now...
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
							
						// Selecting again the currently selected item = buy
						IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
							bCursorAccept= TRUE
						ELSE
							sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
								
							SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
							
							IF GET_CURRENT_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
								sData.bPreviewing = TRUE
						
								IF sData.bCurrentItemIsNew
								OR (NETWORK_IS_GAME_IN_PROGRESS()
								AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
									sData.bRebuildMenu = TRUE
									sData.bTryOnClothesAfterRebuild = TRUE
								ELSE
									RESTORE_CURRENT_PED_COMPONENTS(sData)
									CPRINTLN(DEBUG_PED_COMP, "RTNP 3.")
									REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
									SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
									TRY_ON_SELECTED_CLOTHING_ITEM(sData)
								ENDIF

								PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
							
							ENDIF
							
						ENDIF
											
					ENDIF
			
				//------------------Sub Menu Selection ---------------------------------     
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bCursorAccept = TRUE)
				AND sData.sBrowseInfo.iCurrentItem != -1
				AND IS_CLOTHES_MENU_LABEL_OVERRIDE_A_SUB_MENU(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]))
					sData.iDepth++
					sData.bRebuildMenu = TRUE
					sData.bTryOnClothesAfterRebuild = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentSubItem = sData.sBrowseInfo.iCurrentItem
					bCursorAccept = FALSE
					
					sData.sBrowseInfo.eMenu = GET_CLOTHES_SUB_MENU_FOR_LABEL_OVERRIDE(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[sData.sBrowseInfo.iCurrentItem]))
					
					// Build the array required to search the items 
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					sData.sBrowseInfo.iCurrentItem = -1
					GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
				// Back out of sub menu
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))
				AND sData.sBrowseInfo.iCurrentSubItem != -1
					
					bJustLeftSubMenu = TRUE
					sData.iDepth--
					sData.bRebuildMenu = TRUE
					sData.bTryOnClothesAfterRebuild = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iCurrentSubItem
					sData.sBrowseInfo.eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentGroup])
					sData.sBrowseInfo.iCurrentSubItem = -1
					
					// Build the array required to search the items 
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					//GET_FIRST_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem)
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					
				//------------------Item Selection Menu ---------------------------------     
				// Main browsing stuff
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bCursorAccept = TRUE)
				AND sData.sBrowseInfo.iCurrentItem != -1
				AND NOT g_bShopMenuOptionDisabled[sData.sBrowseInfo.iCurrentItem]
				
				
					sData.bRebuildMenu = TRUE
					sData.bPreviewing = FALSE
					
					// Remove items that are not stored
					//RESTORE_CURRENT_PED_COMPONENTS(sData.sCurrentClothes)
					
					eCompName = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
					eCompType = g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem]			
					
					// Update stored tattoo hash
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.iCurrentItem != -1
					AND g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem] != 0
						TATTOO_NAME_ENUM eTshirtDecal
						eTshirtDecal = GET_TATTOO_ENUM_FROM_DLC_HASH(GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH), GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()))
						IF (eTshirtDecal != INVALID_TATTOO)
							SET_MP_TATTOO_CURRENT(eTshirtDecal, FALSE)
						ELSE
							CERRORLN(DEBUG_PED_COMP, "<", GET_THIS_SCRIPT_NAME(), "> Trying to apply a null tshirt decal tattoo index (hash:", GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH), ")")
						ENDIF
						IF INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem]) != INVALID_TATTOO
							SET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH, g_iMultiDecalTattooHash_Preset[sData.sBrowseInfo.iCurrentItem])
							SET_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem]), TRUE)
						ELSE
							CERRORLN(DEBUG_PED_COMP, "<", GET_THIS_SCRIPT_NAME(), "> Trying to apply a null multidecal tattoo index (hash:", g_iMultiDecalTattooIndex[sData.sBrowseInfo.iCurrentItem], ")")
						ENDIF
					ENDIF
					
					// Try on an item
					IF NOT sData.bSelectedItemIsCurrent
						IF TRY_ON_SELECTED_CLOTHING_ITEM(sData)
							
							sData.bClothesChanges = TRUE
							
							STORE_CURRENT_PED_COMPONENTS(sData)
							
							IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
								SET_PLAYER_HAS_JUST_CHANGED_CLOTHES()
								RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(GET_CURRENT_PLAYER_PED_ENUM(), FALSE)
							ENDIF
							
							sData.sItemDesc = "CSHOP_SET" // Current outfit updated.
							
							// update playstats as the player has changed outfit
							IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
								PED_COMP_ITEM_DATA_STRUCT sItemDetails
								sItemDetails = CALL sData.fpGetPedCompDataForItem(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, eCompName)
								
								IF eCompType = COMP_TYPE_PROPS
									STORE_PROP_CHANGE_PLAYSTATS(PLAYER_PED_ID(), sItemDetails.ePropPos, sItemDetails.iDrawable, sItemDetails.iTexture)
								ELSE
									IF eCompType = COMP_TYPE_OUTFIT
										STORE_CLOTHES_CHANGE_PLAYSTATS_FOR_OUTFIT(sData.fpGetPedCompDataForItem, PLAYER_PED_ID(), eCompName)
									ELSE
										PED_COMPONENT ePedComponent
										ePedComponent=  GET_PED_COMPONENT_FROM_TYPE(eCompType)
										STORE_CLOTHES_CHANGE_PLAYSTATS(PLAYER_PED_ID(), ePedComponent, sItemDetails.iDrawable, sItemDetails.iTexture, GET_PED_PALETTE_VARIATION(PLAYER_PED_ID(), ePedComponent))
									ENDIF
								ENDIF
							ENDIF
							PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						ELIF g_sShopCompItemData.iItemCount > 0
							sData.sItemDesc = "WARD_NOMIX"
							PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
						ENDIF

					// Store as current
					ELSE
						STORE_CURRENT_PED_COMPONENTS(sData)
						RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(GET_CURRENT_PLAYER_PED_ENUM(), FALSE)
						sData.sItemDesc = "WARD_ALRDY" // You are already wearing this item.
						PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ENDIF
					
					
					CPRINTLN(DEBUG_PED_COMP, "RTNP 2.")
					REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
					
					HANDLE_JACKET_CHANGE_MP(sData) // cehck if player has changed into / out of jacket
					
					
					// Rebuild menu for body warmers.
					#IF USE_TU_CHANGES
					IF sData.sBrowseInfo.eMenu = CLO_MENU_MP_BODY_WARMERS
						CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)		
					ENDIf
					#ENDIF
					
					
				ELIF (sData.sBrowseInfo.iCurrentItem != -1) AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset))
					OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
					
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
					IF sData.bWaitingForPreload
						PRINTLN("release preload on FRONTEND UP (CLO_MENU_", sData.sBrowseInfo.eMenu, ")")

						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
						sData.bWaitingForPreload = FALSE
					ENDIF
					
					IF GET_PREV_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
						sData.bPreviewing = TRUE
						
						IF sData.bCurrentItemIsNew
						OR (NETWORK_IS_GAME_IN_PROGRESS()
						AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
							sData.bRebuildMenu = TRUE
							sData.bTryOnClothesAfterRebuild = TRUE
						ELSE
							RESTORE_CURRENT_PED_COMPONENTS(sData)
							CPRINTLN(DEBUG_PED_COMP, "RTNP 3.")
							REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
							SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
							TRY_ON_SELECTED_CLOTHING_ITEM(sData)
						ENDIF
						
						IF sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
							sData.iTopItem_Main = GET_TOP_MENU_ITEM()
						ELSE
							IF sData.iDepth > 1
								sData.iTopItem_Sub2 = GET_TOP_MENU_ITEM()
							ELSE
								sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
							ENDIF
						ENDIF
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ENDIF
				
				ELIF (sData.sBrowseInfo.iCurrentItem != -1) AND 
					(IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR (sData.sInputData.leftStickUD > 100 AND sData.sInputData.bLeftStickUDReset)
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset))
					OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)

					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
			
					IF sData.bWaitingForPreload
						PRINTLN("release preload on FRONTEND DOWN (CLO_MENU_", sData.sBrowseInfo.eMenu, ")")
						
						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
						sData.bWaitingForPreload = FALSE
					ENDIF
			
					IF GET_NEXT_SHOP_MENU_ITEM(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
						sData.bPreviewing = TRUE
						
						IF sData.bCurrentItemIsNew
						OR (NETWORK_IS_GAME_IN_PROGRESS()
						AND (sData.sBrowseInfo.eMenu >= CLO_MENU_HATS AND sData.sBrowseInfo.eMenu <= CLO_MENU_MASKS))
							sData.bRebuildMenu = TRUE
							sData.bTryOnClothesAfterRebuild = TRUE
						ELSE
							RESTORE_CURRENT_PED_COMPONENTS(sData)
							SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
							// do before try on here, so we get the previewed decal
							CPRINTLN(DEBUG_PED_COMP, "RTNP 4.")
							REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
							TRY_ON_SELECTED_CLOTHING_ITEM(sData)
						ENDIF
						
						IF sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
							sData.iTopItem_Main = GET_TOP_MENU_ITEM()
						ELSE
							IF sData.iDepth > 1
								sData.iTopItem_Sub2 = GET_TOP_MENU_ITEM()
							ELSE
								sData.iTopItem_Sub = GET_TOP_MENU_ITEM()
							ENDIF
						ENDIF
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
					ENDIF
					
				// Return to main menu
				ELIF bCancelPressed AND NOT bJustLeftSubMenu
				
					sData.iDepth--
					sData.bPreviewing = FALSE
					sData.bRebuildMenu = TRUE
					sData.bUpdateWardrobeCamera = TRUE
					sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iCurrentGroup
					
					// Main menu group selection
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND sData.sBrowseInfo.eMenu = CLO_MENU_MASKS
						sData.sBrowseInfo.iCurrentItem = sData.sBrowseInfo.iMainMenuGroup
						sData.sBrowseInfo.iMainMenuGroup = -1
					ENDIF
					
					sData.sBrowseInfo.eMenu = CLO_MENU_MAIN		

					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_CLOTHESSHOP_SOUNDSET")
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF IS_PAUSE_MENU_ACTIVE()
			AND sData.bPreviewing
				RESTORE_CURRENT_PED_COMPONENTS(sData)
			ENDIF
		ENDIF	
	ENDIF
	
	IF NOT sData.bTryOnClothesAfterRebuild
	AND NOT sData.bRebuildMenu
	//AND NOT bMenuRebuilt
		UPDATE_WARDROBE_CAMERA(sData)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT sData.bRebuildMenu
	AND sData.sBrowseInfo.eMenu != CLO_MENU_MAIN
	AND sData.sBrowseInfo.iCurrentItem >= 0
	AND g_bShopMenuOptionDisabled[sData.sBrowseInfo.iCurrentItem]
		SWITCH sData.sBrowseInfo.eMenu
			CASE CLO_MENU_BUSINESS_SHIRT
			CASE CLO_MENU_SUIT_VESTS
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK1") // You must be wearing a suitable jacket to view this item.
			BREAK
			CASE CLO_MENU_VEST_SHIRTS
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK2") // You must be wearing a vest to view this item.
			BREAK
			CASE CLO_MENU_HATS
			CASE CLO_MENU_BEANIES
			CASE CLO_MENU_CANVAS_HATS
			CASE CLO_MENU_CAPS
			CASE CLO_MENU_CAPS_BACKWARD
			CASE CLO_MENU_FLAT_CAPS
			CASE CLO_MENU_MILITARY_CAPS
			CASE CLO_MENU_TRILBIES
			CASE CLO_MENU_PORKPIE_HATS
			CASE CLO_MENU_FEDORAS
			CASE CLO_MENU_COWBOY_HATS
			CASE CLO_MENU_STRAW_HATS
			CASE CLO_MENU_BERETS
			CASE CLO_MENU_BANDANAS
			CASE CLO_MENU_HEADPHONES
			CASE CLO_MENU_EARDEFENDERS
			CASE CLO_MENU_HELMETS
			#IF USE_TU_CHANGES
			CASE CLO_MENU_MP_INDI_HATS
			CASE CLO_MENU_MP_PILOT_HATS
			CASE CLO_MENU_MP_LTS_HELMETS
			CASE CLO_MENU_MP_XMAS_HATS
			CASE CLO_MENU_MP_BIKER_HELMETS
			#ENDIF
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK3") // This item is not compatible with your current hairstyle.
			BREAK
			CASE CLO_MENU_MP_OUTFITS_STD
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK4") // This item is not compatible with your current hairstyle.
			BREAK
			CASE CLO_MENU_TIES
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK5") // You must be wearing a suitable shirt to view this item.
			BREAK
			CASE CLO_MENU_SCARVES
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK9") // This scarf is not compatible with your current outfit.
			BREAK
			CASE CLO_MENU_CHAINS
				
				SWITCH g_iShopMenuOptionDisabledInfo[sData.sBrowseInfo.iCurrentItem]
					CASE iShopmenu_Disabled_2_TRUE_chain
						SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK11") // This chain is not compatible with your current top.
					BREAK
					
					DEFAULT
						SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK99") // This item is not compatible with your current outfit.
					BREAK
				ENDSWITCH
			BREAK
			DEFAULT
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_BLOCK99") // This item is not compatible with your current outfit.
			BREAK
		ENDSWITCH
		
	ELIF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT sData.bRebuildMenu
	AND sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
	AND sData.sBrowseInfo.iCurrentItem != -1
	AND sData.sBrowseInfo.iMainMenuGroup != -1
	AND INT_TO_ENUM(CLOTHES_MENU_ENUM, g_iClothingMenuLookup[sData.sBrowseInfo.iCurrentItem]) = CLO_MENU_MP_OUTFITS
		IF NOT IS_EDIT_SAVED_OUTFIT_OPTION_BLOCKED(sData.sBrowseInfo.eMenu)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_OUTFIT_MP")
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_OUTFIT_BL")
		ENDIF
	ENDIF
	
	IF (IS_IME_IN_PROGRESS() AND IS_BIT_SET(sData.iSaveOutfitFlags, SHOPFLAG_bSaveOutfitRename))
		CPRINTLN(DEBUG_PED_COMP,"NOT drawing the wardrobe menu as outfit naming is in progress while using an IME")
	ELSE
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_CUSTOM_MENU_SAFE_TO_DRAW(FALSE, FALSE)
		DRAW_PIM_MENU_HEADER(VHeaderData)
		DRAW_PIM_GLARE(GlareData)
	ENDIF
	DRAW_MENU()
	DRAW_CURSOR_SCROLL_HIGHLIGHT(TRUE)
	ENDIF
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS() //1616741 - DRAW_MENU hides help text anyway, but this issue was never bugged in SP, so let's just do this check if it's not MP
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Loads the required assets and sets up browse info
PROC DO_INITIALISE(WARDROBE_DATA_STRUCT &sData)

	SWITCH sData.iInitStage
		CASE 0
			CPRINTLN(DEBUG_PED_COMP, "Initialising wardrobe data ")
			
			// Take note of the current player character so we can tell if the controller should be reset
			sData.ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			
			sData.sBrowseInfo.bMustLeaveLocate = FALSE
			sData.sBrowseInfo.bReset = FALSE
			
			// Setup the wardrobe information based on the coords that were passed into the script
			IF GET_WARDROBE_DATA(sData, sData.sWardrobeLauncherData)
				CLEANUP_HEADER_GRAPHIC_FOR_WARDROBE()
				sGraphicHeader = GET_HEADER_GRAPHIC_FOR_WARDROBE()
				sData.iInitStage++
			ELSE
				CLEANUP_WARDROBE(sData)
			ENDIF
		BREAK
		CASE 1
			// Wait for correct character
			IF (sData.sWardrobeInfo.eCharacter = NO_CHARACTER OR IS_PED_THE_CURRENT_PLAYER_PED(sData.sWardrobeInfo.eCharacter))
				sData.sWardrobeInfo.eStage = WAIT_FOR_TRIGGER
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles monitoring the player entering the wardrobe dispays context 
/// PARAMS:
///    sData - wardrobe data
///    bPrintHelp - handles releasing the context help
PROC DO_WARDROBE_CONTEXT_ENTRY(WARDROBE_DATA_STRUCT &sData, BOOL &bPrintHelp)
	IF sData.iContextID = NEW_CONTEXT_INTENTION
		REGISTER_CONTEXT_INTENTION(sData.iContextID, CP_MEDIUM_PRIORITY, sData.sHelpTrig)
	ENDIF
	SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
    IF HAS_CONTEXT_BUTTON_TRIGGERED(sData.iContextID)
		bPrintHelp = FALSE
		sData.sBrowseInfo.eStage = BROWSE_INIT
		sData.sWardrobeInfo.eStage = BROWSE_WARDROBE
		
		// Take a note of the current items
		STORE_CURRENT_PED_COMPONENTS(sData)
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
    ENDIF
ENDPROC

/// PURPOSE: Checks for the player entering the wardrobe area
PROC DO_CHECK_WARDROBE_ENTRY(WARDROBE_DATA_STRUCT &sData)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		BOOL bPrintHelp = FALSE
		FLOAT fDistToLaunch = 1.5
		FLOAT fDistLimit = 0
		IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_MP)
			IF GET_PROPERTY_SIZE_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROP_SIZE_TYPE_LARGE_APT
				fDistToLaunch = 3.0
//			#IF FEATURE_EXECUTIVE
//			ELIF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)	
//				fDistToLaunch = 0.5
//			#ENDIF
			ENDIF
		ENDIF
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF GET_BASE_PROPERTY_FROM_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROPERTY_STILT_APT_1_BASE_B
			OR GET_BASE_PROPERTY_FROM_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROPERTY_STILT_APT_5_BASE_A
			
				fDistLimit = 1
			ELIF GET_BASE_PROPERTY_FROM_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
			OR GET_BASE_PROPERTY_FROM_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
				fDistLimit = 0.45
			ELIF  IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(sData.eArmoryTruckSection, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_COMMAND_CENTER
					fDistLimit = 1.4
				ELSE
					fDistLimit = 1.2
				ENDIF
			ELIF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
			OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) 
			OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
			OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
			OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			#IF FEATURE_FIXER
			OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			#ENDIF
				fDistLimit = 2.0
			ELIF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				fDistLimit = 1.2
			ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
				fDistLimit = 2.5
			ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
				fDistLimit = 1.0			
			ELSE 
		 		fDistLimit = 5
			ENDIF
		ELSE 
			fDistLimit = 5
		ENDIF
		IF sData.fDistToPlayer <= fDistLimit
			IF (ALLOW_ENTRY_FOR_THE_END_OF_ARMENIAN1(sData))
			OR (IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_MP) AND sData.fDistToPlayer <= fDistToLaunch)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sData.sWardrobeInfo.vAngledAreaCoords[0], sData.sWardrobeInfo.vAngledAreaCoords[1], sData.sWardrobeInfo.fAngledAreaWidth)
				
				BOOL bOccupiedCheck = FALSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND (sData.sWardrobeInfo.eWardrobe = PW_FREEMODE AND IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty))
				AND (sData.iContextID = NEW_CONTEXT_INTENTION)
					bOccupiedCheck = TRUE
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF (IS_PLAYER_IN_CASINO(PLAYER_ID())
					OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID()))
					AND (sData.iContextID = NEW_CONTEXT_INTENTION)
						bOccupiedCheck = TRUE
					ENDIF	
				ENDIF
				
				IF IS_WARDROBE_SAFE_TO_BROWSE(sData.sWardrobeInfo, TRUE, TRUE, TRUE, bOccupiedCheck)//BOOL bCheckPlayerControl = TRUE, BOOL bCheckCutsceneFlag = TRUE, BOOL bEntryCheck = FALSE, BOOL bOccupiedCheck = FALSE)
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						bPrintHelp = TRUE
					ENDIF	
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF GET_PROPERTY_SIZE_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = PROP_SIZE_TYPE_SMALL_APT
							IF IS_PLAYER_FACING_WARDROBE(sData.sWardrobeInfo.vWardrobeCoords)
								DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
							ELSE
								bPrintHelp = FALSE
							ENDIF
						ELIF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
						OR IS_PLAYER_IN_BUNKER(PLAYER_ID())
						OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
						OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) 
						OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
						OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
						OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
						OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
						OR IS_PLAYER_IN_CASINO(PLAYER_ID())
						OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
							VECTOR vWardrobeCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.sWardrobeInfo.vPlayerCoords, sData.sWardrobeInfo.fPlayerHead, <<0,-1,0>>)
							
							IF IS_PLAYER_FACING_WARDROBE(vWardrobeCoords)
							OR IS_PLAYER_IN_CASINO(PLAYER_ID())
							OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
								DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
							ELSE
								bPrintHelp = FALSE
							ENDIF
						ELIF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
						AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
							IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(sData.eArmoryTruckSection, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_COMMAND_CENTER
								IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 90.0, 35.0)
								OR IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 270.0, 35.0)
									DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
								ELSE
									bPrintHelp = FALSE
								ENDIF
							ELSE
								IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 270.0, 35.0)
									DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
								ELSE
									bPrintHelp = FALSE
								ENDIF
							ENDIF
						ELIF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
							IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 249.7065, 30.0)
								DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
							ELSE
								bPrintHelp = FALSE
							ENDIF
						ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
							VECTOR vWardrobeCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.sWardrobeInfo.vPlayerCoords, sData.sWardrobeInfo.fPlayerHead, <<0,-1,0>>)
							IF IS_PLAYER_FACING_WARDROBE(vWardrobeCoords)
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1357.934082,144.473999,-96.109329>>, <<-1357.938477,145.667816,-95.109329>>, 0.900000)
								DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
							ELSE
								bPrintHelp = FALSE
							ENDIF
						#IF FEATURE_FIXER
						ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
							SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
							
							VECTOR boxA = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 8.54119, 11.2968, 8.60002 >>, eSimpleInterior)	// Original value at Hawick location <<392.508301,-72.616844,110.963020>>
							VECTOR boxB = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 4.92226, 11.3157, 10.6 >>, eSimpleInterior)	// Original value at Hawick location <<393.763763,-69.222610,112.963020>>
								
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), boxA, boxB, 2.500000)
								DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
							ELSE
								bPrintHelp = FALSE
							ENDIF
						#ENDIF
						ELSE
							DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
						ENDIF
					ELSE
						DO_WARDROBE_CONTEXT_ENTRY(sData, bPrintHelp)
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
						AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(TRUE)
								PRINT_HELP("OFF_BLOCK_WAR3")
							ELIF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								PRINT_HELP("OFF_BLOCK_WAR2")
							ELSE
								PRINT_HELP("OFF_BLOCK_WAR")
							ENDIF
						ENDIF
						
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
						AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(TRUE)
								PRINT_HELP("OFF_BLOCK_WAR3")
							ELIF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								PRINT_HELP("OFF_BLOCK_WAR2")
							ELSE
								PRINT_HELP("OFF_BLOCK_WAR")
							ENDIF
						ENDIF
						
						IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
							PRINT_HELP("WARD_BLOCK_JUGG")
						ENDIF
						
						IF IS_PED_WEARING_A_DUFFEL_BAG(PLAYER_PED_ID())
							PRINT_HELP("WARD_BLOCK_DBAG")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bPrintHelp
			IF sData.iContextID != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(sData.iContextID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the player into postion and displays the wardrobe hud
PROC DO_BROWSE_WARDROBE(WARDROBE_DATA_STRUCT &sData)

	IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF
	ENDIF

	// Check for the player leaving the area
	IF sData.sWardrobeInfo.bActive
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF sData.fDistToPlayer > 5.0
			OR NOT IS_WARDROBE_SAFE_TO_BROWSE(sData.sWardrobeInfo, FALSE, FALSE, FALSE)
				sData.sBrowseInfo.eStage = BROWSE_CLEANUP
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH sData.sBrowseInfo.eStage
		CASE BROWSE_INIT
		
			sData.sBuddyHideData.buddyHide = FALSE
			sData.sBuddyHideData.buddyInit = FALSE
			
			// request audio for menu
			IF sData.bSoundBankRequested = FALSE
			AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
				REQUEST_SCRIPT_AUDIO_BANK("CLOTHES_STORE", FALSE)
				sData.bSoundBankRequested = TRUE
			ENDIF
		
			// load all other assets 
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF LOAD_MENU_ASSETS("CLO_MNU")
				AND REQUEST_HEADER_GRAPHIC_FOR_WARDROBE()
					SET_PLAYER_IS_CHANGING_CLOTHES(TRUE)
					
					// Reset everything
					sData.iTopItem_Main = 0
					sData.iTopItem_Sub = 0
					sData.iTopItem_Sub2 = 0
					sData.iDepth = 0
					sData.sBrowseInfo.iMainMenuGroup = -1
					sData.sBrowseInfo.eMenu = CLO_MENU_MAIN
					sData.sBrowseInfo.iCurrentItem = -1
					sData.sBrowseInfo.iCurrentSubItem = -1
					sData.sWardrobeInfo.bActive = TRUE
					g_eCurrentWardrobe = sData.sWardrobeInfo.eWardrobe // Set this before we build menu
					
					g_bShopMenuDisplayMaskOptions = FALSE
					g_iShopMenuMaskType = 0
					
					// Make sure we have all the clothes that come with this wardrobe.
					CALL sData.fpUnlockPlayerPedClothesForSavehouse(sData.sWardrobeInfo.eSavehouse)
									
					// Setup the items so we can query if we have sub items
					CALL sData.fpBuildBrowseList(g_sShopCompItemData, sData.sBrowseInfo.eMenu, sData.sBrowseInfo.iMainMenuGroup, sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite)
					
					BUILD_WARDROBE_MENU(sData)
					SET_CURSOR_POSITION_FOR_MENU()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
									
					// Tell the player to get in the browse position		
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						OR NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
							REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						ENDIF
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						sData.sBrowseInfo.eStage = BROWSE_INTRO
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE BROWSE_INTRO
			// Check if the player has managed get in position
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND LOAD_WARDROBE_LIGHT(sData.sWardrobeInfo)
				
				// setup player
				CLEAR_AREA_OF_PROJECTILES(sData.sWardrobeInfo.vPlayerCoords, 5.0)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
					// pervent player form spawning inside the ground 
					SET_ENTITY_COORDS(PLAYER_PED_ID(),sData.sWardrobeInfo.vPlayerCoords, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sWardrobeInfo.fPlayerHead)
					// use this for camera coord
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
					MP_PROP_OFFSET_STRUCT tempOffset
					IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(sData.eArmoryTruckSection, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_COMMAND_CENTER
					OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(sData.eArmoryTruckSection, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
						GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_WARDROBE_PED_COORD, sData.eArmoryTruckSection, tempOffset)
					ENDIF
					SET_ENTITY_COORDS(PLAYER_PED_ID(),tempOffset.vLoc, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 176.7132 )
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				AND g_OwnerOfHangarPropertyIAmIn = PLAYER_ID()
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1234.5427, -2981.0283, -42.2636>>, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 180.2719 )
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
					
				ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
				AND g_ownerOfBasePropertyIAmIn = PLAYER_ID()
				
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<371.1484, 4819.4702, -59.9884>>, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 0 )

					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				AND g_OwnerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
				
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<508.6073, 4749.2729, -69.9960>>, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 1.0154 )

					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
				AND g_OwnerOfHackerTruckPropertyIAmIn= PLAYER_ID()
				
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1419.9253, -3014.3342, -79.9999>>, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.9705 )

					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1619.6835, -3020.2756, -76.2050>>, TRUE,TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 348.4922 )

					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<210.4235, 5162.0776, -90.1981>>, TRUE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 87.1186)
					
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1096.0431, 201.3330, -50.4402>>, TRUE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 255.8197)
					
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
					IF IS_WARDROBE_IN_CASINO_APARTMENT_MASTER_BEDROOM()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<976.0278, 64.5337, 115.1641>>, TRUE, TRUE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 68.0287)
					ELIF IS_WARDROBE_IN_CASINO_APARTMENT_GEUST_BEDROOM()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<984.0971, 59.9940, 115.1642>>, TRUE, TRUE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 281.6031)
					ENDIF
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2722.4922, -370.1368, -56.3809>>, TRUE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 343.0345)
					
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())							
				ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1557.9646, 381.1740, -54.2844>>, TRUE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 341.1055)
					
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1358.1615, 145.0377, -96.1093>>, TRUE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 26.3923)
					
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				#IF FEATURE_FIXER
				ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.22816, 10.8279, 8.6 >>, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())), TRUE, TRUE)	// Original value at Hawick location <<392.5167, -71.2226, 110.9630>>
					SET_ENTITY_HEADING(PLAYER_PED_ID(), TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(80.000000, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())))	// Original value at Hawick location -30.0
										
					sData.sWardrobeInfo.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				#ENDIF
				
				ELSE		
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), sData.sWardrobeInfo.vPlayerCoords, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sWardrobeInfo.fPlayerHead)
				ENDIF
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				sData.sBuddyHideData.buddyHide = TRUE
				
				// setup camera
				IF NOT DOES_CAM_EXIST(sData.sWardrobeInfo.camID)
					sData.sWardrobeInfo.camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				ENDIF
				SET_CAMERA_FOR_MENU_ITEM(sData)
				sData.bUpdateWardrobeCamera = TRUE
				UPDATE_WARDROBE_CAMERA(sData)
				
				// set up lighting for wardrobe
				ACTIVATE_WARDROBE_LIGHT(sData, TRUE)
				
				// Clear small area around Michael's wardrobe 
				// Can't do this for all wardrobes, because this would clear Franklin's wardrobe doors too, which are props.
				
				OBJECT_INDEX objBall
				IF sData.sWardrobeInfo.eWardrobe = PW_MICHAEL_MANSION
					objBall = GET_CLOSEST_OBJECT_OF_TYPE(sData.sWardrobeInfo.vPlayerCoords, 3.0, V_ILEV_EXBALL_BLUE, FALSE)
					IF DOES_ENTITY_EXIST( objBall )
						SET_ENTITY_COORDS(objBall, <<-815.3440, 176.9611, 75.7407>>)
					ENDIF
					
					//CLEAR_AREA_OF_OBJECTS(sData.sWardrobeInfo.vPlayerCoords, 3.0)
				ENDIF
				
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				BROWSE_WARDROBE_ITEMS_AND_DRAW_HUD(sData)
				
				// store if player is wearing jacket in MP
				IF NETWORK_IS_GAME_IN_PROGRESS()
					BOOL bIsSpecialJacket
					bIsSpecialJacket = FALSE
					sData.bWearingJacket = IS_PED_WEARING_JACKET_MP(PLAYER_PED_ID(), bIsSpecialJacket)
					CPRINTLN(DEBUG_PED_COMP, "Wardrobe: On entry bWearingJacket= ", sData.bWearingJacket)
					sData.iCurrentItemBeforeJacketChange = -1
				ENDIF

				// reset preload variable
				
				IF sData.bWaitingForPreload
					RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				sData.bWaitingForPreload = FALSE
				ENDIF
				
				sData.sBrowseInfo.eStage = BROWSE_BROWSING
			ENDIF
		BREAK
		
		CASE BROWSE_BROWSING
			BROWSE_WARDROBE_ITEMS_AND_DRAW_HUD(sData)
		BREAK
		
		CASE BROWSE_CLEANUP
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				IF IS_PLAYER_IN_BUNKER(PLAYER_ID())	// reset this coord to default value
					sData.sWardrobeInfo.vPlayerCoords = <<903.4413, -3199.4375, -98.1878>>
				ELIF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())	
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_COMMAND_CENTER
						OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
						OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
						OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
							MP_PROP_OFFSET_STRUCT tempOffset
							GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_WARDROBE, sData.eArmoryTruckSection, tempOffset)
							
							sData.sWardrobeInfo.vPlayerCoords = tempOffset.vLoc
							PRINTLN("DO_BROWSE_WARDROBE truck sData.sWardrobeInfo.vPlayerCoords: ", sData.sWardrobeInfo.vPlayerCoords)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT sData.bDoFinaliseHeadBlend
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					CLEAR_HELP()
					CLEANUP_MENU_ASSETS()
					CLEANUP_HEADER_GRAPHIC_FOR_WARDROBE()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						CLEANUP_PIM_GLARE(GlareData)
					ENDIF
					sData.sBuddyHideData.buddyHide = FALSE
					sData.sWardrobeInfo.bActive = FALSE
					
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, FALSE)
					ENDIF
					
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
						RESTORE_CURRENT_PED_COMPONENTS(sData)  
						CPRINTLN(DEBUG_PED_COMP, "RTNP 5.")
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
					ENDIF
					
					// Update stored globals
					STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
					
					// Return player control
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
					// turn the wardrobe lights off + remove their models
					ACTIVATE_WARDROBE_LIGHT(sData, FALSE)
					ACTIVATE_WARDROBE_HEAD_LIGHT(sData, FALSE)
					UNLOAD_WARDROBE_LIGHT(sData.sWardrobeInfo)
					RESET_ADAPTATION()
					
					// Return to gameplay camera
					IF DOES_CAM_EXIST(sData.sWardrobeInfo.camID)
						DESTROY_CAM(sData.sWardrobeInfo.camID)
					ENDIF
					SET_WIDESCREEN_BORDERS(FALSE, 0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
					
					UPDATE_PED_WEAPON_ANIMATION_OVERRIDE(PLAYER_PED_ID())
					
					// Update stats incase we changed clothes
					IF sData.bClothesChanges
						IF NETWORK_IS_GAME_IN_PROGRESS()
						AND NOT IS_PED_INJURED(PLAYER_PED_ID())
							SAVE_PLAYERS_CLOTHES(PLAYER_PED_ID())
							g_bTriggerShopSave = TRUE
						ENDIF
						sData.bClothesChanges = FALSE
					ENDIF
					
					// clean up audio
					IF sData.bSoundBankRequested
						RELEASE_SCRIPT_AUDIO_BANK()
						CPRINTLN(DEBUG_PED_COMP, "Wardrobe releasing audio bank now.")
						sData.bSoundBankRequested = FALSE
					ENDIF
					
					// block any inputs that conflict with exiting the wardrobe
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sBrowseInfo.tdInputBlockTimer = GET_NETWORK_TIME()
					ELSE
						sData.sBrowseInfo.iInputBlockTimer = GET_GAME_TIMER()
					ENDIF
					sData.sBrowseInfo.bInputBlockTimerSet = TRUE
					CPRINTLN(DEBUG_PED_COMP, "Wardrobe setting input block timer now.")
					
					// change state
					sData.sBrowseInfo.eStage 		 = BROWSE_INIT
					sData.sWardrobeInfo.eStage 		 = WAIT_FOR_TRIGGER
				ELSE
					// Keep menu on screen while we fix up the ped.
					BROWSE_WARDROBE_ITEMS_AND_DRAW_HUD(sData)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	// Keep players buddies out the way whilst we browse clothes
	IF IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
		UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if we should force a cleanup
FUNC BOOL SHOULD_WARDROBE_SCRIPT_CLEANUP(WARDROBE_DATA_STRUCT &sData)

	IF SHOULD_KICK_PLAYER_OUT_OF_SHOP()
		RESET_KICK_PLAYER_OUT_OF_SHOP_GLOBAL()
		RETURN TRUE
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		IF sData.bKillScript
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF sData.sWardrobeInfo.bDataSet
		IF sData.fDistToPlayer > 30.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_bInMultiplayer
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->CARMOD: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE    ----------     ","")
			RETURN TRUE
		ENDIF
		
		IF NOT IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_MP)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_PED_COMP, "\n FREEMODE WARDROBE FORCE CLEANUP: Switching from SP to MP")
			#ENDIF
			RETURN TRUE
		ENDIF
		IF NOT IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
		AND NOT IS_LOCAL_PLAYER_IN_IE_WAREHOUSE_THEY_OWN()
		AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()) 
		AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		AND NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
		AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		#IF FEATURE_FIXER
		AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#ENDIF
		
			IF IS_PLAYER_IN_ANY_SHOP() 
				IF sData.fDistToPlayer > 3.0
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_PED_COMP, "\n FREEMODE WARDROBE FORCE CLEANUP: Player not in changing room area of the shop.")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			IF !IS_WARDROBE_IN_CASINO_APARTMENT_GEUST_BEDROOM()
			AND !IS_WARDROBE_IN_CASINO_APARTMENT_MASTER_BEDROOM()
				IF sData.fDistToPlayer > 3.0
					RETURN TRUE
				ENDIF	
			ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF (GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) != AT_ST_COMMAND_CENTER
			AND GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) != AT_ST_LIVING_ROOM
			AND GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) != AT_ST_LIVING_ROOM
			AND GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) != AT_ST_LIVING_ROOM)
//			OR g_bRefreshTruckActivities
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_PED_COMP, "\n WARDROBE FORCE CLEANUP: player doesn't own living room/command center")
				#ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			IF sData.eArmoryTruckSection != ATS_INVALID_SECTION
				CPRINTLN(DEBUG_PED_COMP, "\n WARDROBE FORCE CLEANUP: player has left the armory truck")
			ENDIF
		ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID()
				IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				OR NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_PED_COMP, "\n FREEMODE WARDROBE FORCE CLEANUP: player in submarine and not in same gang as owner")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
		
		IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_PED_COMP, "\n FREEMODE WARDROBE FORCE CLEANUP: criminal starter pack browser launched.")
			#ENDIF
			RETURN TRUE
		ENDIF
		
	ELSE
		IF NOT IS_BIT_SET(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_PED_COMP, "\n SINGLEPLAYER WARDROBE FORCE CLEANUP: Switching from MP to SP")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Make a call to finalise head blend when ready
PROC PROCESS_FINALISE_HEAD_BLEND(WARDROBE_DATA_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF sData.bDoFinaliseHeadBlend
				IF sData.sWardrobeInfo.eStage != BROWSE_WARDROBE
				OR sData.sBrowseInfo.eStage > BROWSE_BROWSING
					IF HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
						CPRINTLN(DEBUG_SHOPS, "HAS_PED_HEAD_BLEND_FINISHED = TRUE")
						FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
						sData.bDoFinaliseHeadBlend = FALSE
						ELSE
							PRINTLN("PROCESS_FINALISE_HEAD_BLEND - waiting for streaming requests to complete")
						ENDIF
					ELSE
						PRINTLN("PROCESS_FINALISE_HEAD_BLEND - waiting for head blend to finish")
					ENDIF
				ENDIF
			ELSE
				IF sData.sWardrobeInfo.eStage = BROWSE_WARDROBE
				AND sData.sBrowseInfo.eStage <= BROWSE_BROWSING
					sData.bDoFinaliseHeadBlend = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		sData.bDoFinaliseHeadBlend = FALSE
	ENDIF
ENDPROC

INT iMainMenuInitStage = -1
INT iMainMenuToProcess = 0

#IF IS_DEBUG_BUILD
INT iInstructionCount
INT iInstructionCountTotal
INT iFrameCount
#ENDIF

#IF IS_DEBUG_BUILD
	CONST_INT WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME 12
#ENDIF
#IF NOT IS_DEBUG_BUILD
	CONST_INT WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME 200
#ENDIF

PROC PROCESS_BUILD_MAIN_MP_MENU(WARDROBE_DATA_STRUCT &sData)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	INT iStartItem, iEndItem, i
	CLOTHES_MENU_ENUM eTempMenu
	
	IF sData.sWardrobeInfo.eStage != BROWSE_WARDROBE
	OR sData.sBrowseInfo.eStage != BROWSE_BROWSING
		// Reset data when we are not in the wardrobe menu
		IF iMainMenuInitStage != 0
			iMainMenuInitStage = 0
			bMainMenuInitialised = FALSE
			iMainMenuToProcess = ENUM_TO_INT(CLO_MENU_MP_OUTFITS)
			
			// reset global data
			g_sShopCompItemData.iItemCount = 0
			REPEAT COUNT_OF(g_sShopCompItemData.iSubMenusWithItems) i
				g_sShopCompItemData.iSubMenusWithItems[i] = 0
				g_sShopCompItemData.iSubMenusWithNEWItems[i] = 0
				g_iSubMenusWithItems[i] = 0
			ENDREPEAT
			
			REPEAT COUNT_OF(g_sShopCompItemData.eItems) i
				g_sShopCompItemData.eItems[i] = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
				g_sShopCompItemData.eTypes[i] = INT_TO_ENUM(PED_COMP_TYPE_ENUM, 0)
				g_sShopCompItemData.iItemLabelOverride[i] = 0
			ENDREPEAT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			iFrameCount = GET_FRAME_COUNT()
			iInstructionCount = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
			iInstructionCountTotal = 0
		#ENDIF
		
		EXIT
	ENDIF
	
	SWITCH iMainMenuInitStage
		CASE 99
			// Sit idle and wait for reset.
		BREAK
		
		DEFAULT
		
			#IF IS_DEBUG_BUILD
				iInstructionCount = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
				PRINTLN("PROCESS_BUILD_MAIN_MP_MENU : Instructions at start of update = ", GET_NUMBER_OF_INSTRUCTIONS_EXECUTED())
			#ENDIF
		
			INT iCurrentPed
			MODEL_NAMES ePedModel
			ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			IF ePedModel = MP_M_FREEMODE_01
				iCurrentPed = 3
			ELIF ePedModel = MP_F_FREEMODE_01
				iCurrentPed = 4
			ENDIF
			
			INT iEarlyBail
			
			// First frame init
			IF iMainMenuToProcess = ENUM_TO_INT(CLO_MENU_MP_OUTFITS)
				g_eCached_CurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				g_eCached_CurrentSpecial = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
				g_eCached_CurrentTeeth = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_TEETH)
				g_bCached_WearingJacket = IS_PED_WEARING_JACKET_MP(PLAYER_PED_ID(), g_bCached_IsSpecialJacket, g_eCached_CurrentJbib)
				g_bCached_WearingOpenCheckShirt = IS_JBIB_COMPONENT_AN_OPEN_CHECK_SHIRT(ePedModel, g_eCached_CurrentJbib)
				g_bCached_WearingJacketOnlyItem = IS_JBIB_COMPONENT_A_JACKET_ONLY_ITEM(ePedModel, g_eCached_CurrentJbib)
				g_bCached_WearingOpenShortShirt = IS_JBIB_COMPONENT_AN_OPEN_SHORT_SHIRT(ePedModel, g_eCached_CurrentJbib)
			ENDIF
			
			g_bBuidlingMainWardrobeMenu = TRUE
			
			// Split the main loop up.
			iStartItem = iMainMenuToProcess
			iEndItem = iMainMenuToProcess+WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME-1
			
			iMainMenuToProcess += WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME
			
			IF iEndItem > ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
				iEndItem = ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
			ENDIF
			
			FOR i = iStartItem TO iEndItem
				eTempMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, i)
				
				IF eTempMenu = CLO_MENU_MP_AWARD_SHIRTS
				OR eTempMenu = CLO_MENU_MP_ARENA_WAR_TOPS
				OR eTempMenu = CLO_MENU_MASKS
				OR eTempMenu = CLO_MENU_MP_OUTFITS
					// skip expensive menus and just add them.
					SET_BIT(g_sShopCompItemData.iSubMenusWithItems[(ENUM_TO_INT(eTempMenu)/32)], (ENUM_TO_INT(eTempMenu)%32))
					SET_BIT(g_iSubMenusWithItems[(ENUM_TO_INT(eTempMenu)/32)], (ENUM_TO_INT(eTempMenu)%32))
					
				ELIF IS_CLOTHES_MENU_SUITABLE_FOR_MP_PLAYER_WARDROBE(eTempMenu, ePedModel)
				
					IF IS_MP_OUTFIT_MENU(eTempMenu)
						SETUP_MP_CLOTHES_OUTFITS(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ELIF IS_MP_UPPER_MENU(eTempMenu)
						SETUP_MP_CLOTHES_UPPER(sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME
					ELIF IS_MP_LOWER_MENU(eTempMenu)
						SETUP_MP_CLOTHES_LOWER(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ELIF IS_MP_FEET_MENU(eTempMenu)
						SETUP_MP_CLOTHES_FEET(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ELIF IS_MP_HATS_MENU(eTempMenu)
						SETUP_MP_CLOTHES_HATS(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ELIF IS_MP_MASKS_MENU(eTempMenu)
						SETUP_MP_CLOTHES_MASKS(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME
					ELIF IS_MP_GLASSES_MENU(eTempMenu)
						SETUP_MP_CLOTHES_GLASSES(sData.fpSetupClothingItemForShop, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ELIF IS_MP_ACCESSORIES_MENU(eTempMenu)
						SETUP_MP_CLOTHES_ACCESSORIES(sData.fpSetupClothingItemForShop, sData.fpGetPedComponentItemRequisite, g_sShopCompItemData, iCurrentPed, eTempMenu)
						iEarlyBail += 1
					ENDIF
					
					PRINTLN("PROCESS_BUILD_MAIN_MP_MENU : Instructions after building menu = ", GET_NUMBER_OF_INSTRUCTIONS_EXECUTED())
				ENDIF
				
				IF (iEarlyBail >= WARDROBE_MENUS_TO_PROCESS_IN_1_FRAME)
					iMainMenuToProcess = i+1
					i = iEndItem+1
				ENDIF
			ENDFOR
			
			#IF IS_DEBUG_BUILD
				iInstructionCountTotal += (GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()-iInstructionCount)
			#ENDIF
			
			IF iMainMenuToProcess > ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU)
			
				iMainMenuToProcess = 0
				g_bBuidlingMainWardrobeMenu = FALSE
				iMainMenuInitStage = 99
				bMainMenuInitialised = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN("PROCESS_BUILD_MAIN_MP_MENU : Frames to build menu = ", GET_FRAME_COUNT()-iFrameCount)
					PRINTLN("PROCESS_BUILD_MAIN_MP_MENU : Accumulated instructions building menus = ", iInstructionCountTotal)
				#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC WARDROBE_MAIN_UPDATE(WARDROBE_DATA_STRUCT &sData)
		
	#IF IS_DEBUG_BUILD UPDATE_WARDROBE_DEBUG_WIDGETS(sData) #ENDIF
	
	IF sData.sWardrobeInfo.bDataSet
		IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			sData.fDistToPlayer = GET_CLOSEST_ARMORY_TRUCK_WARDROBE(g_ownerOfArmoryTruckPropertyIAmIn, sData.eArmoryTruckSection)
		ELSE
			sData.fDistToPlayer = GET_DISTANCE_BETWEEN_COORDS(sData.sWardrobeInfo.vPlayerCoords, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		ENDIF
	ELSE
		sData.fDistToPlayer = -1.0
	ENDIF
	
	IF NOT SHOULD_WARDROBE_SCRIPT_CLEANUP(sData)
	
			
		PROCESS_FINALISE_HEAD_BLEND(sData)
	
		PROCESS_WARDROBE_BLIP(sData)
		
		UPDATE_WARDROBE_INPUT_BLOCKS(sData.sBrowseInfo)
		
		PROCESS_BUILD_MAIN_MP_MENU(sData)
		
		SWITCH sData.sWardrobeInfo.eStage
			CASE INITIALISE
				DO_INITIALISE(sData)
			BREAK
		
			CASE WAIT_FOR_TRIGGER
				DO_CHECK_WARDROBE_ENTRY(sData)					
			BREAK
			
			CASE BROWSE_WARDROBE
				DO_BROWSE_WARDROBE(sData)
			BREAK		
		ENDSWITCH
		
		CHECK_WARDROBE_RESET(sData)
	ELSE
		CLEANUP_WARDROBE(sData)
	ENDIF
ENDPROC
