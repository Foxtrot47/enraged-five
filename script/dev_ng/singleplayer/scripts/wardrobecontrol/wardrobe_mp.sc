//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	wardrobe_mp.sc 												//
//		AUTHOR			:	Kenneth Ross / Andrew Minghella								//
//		DESCRIPTION		:	Allows the player to select from a list of clothes that 	//
//							have been acquired and are available at the current stage.	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "wardrobe_core.sch"

WARDROBE_DATA_STRUCT sData

SCRIPT(WARDROBE_LAUNCHER_STRUCT sInData)
	
	sData.sWardrobeLauncherData = sInData
	
	CPRINTLN(DEBUG_PED_COMP, "Wardrobe[", ENUM_TO_INT(sData.sWardrobeLauncherData.eWardrobe), "] started in MP")
	
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	SET_BIT(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_MP)
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	#IF IS_DEBUG_BUILD SETUP_WARDROBE_DEBUG_WIDGETS(sData) #ENDIF
	
	sData.fpGetPedCompDataForItem = &GET_PED_COMP_DATA_FOR_ITEM_MP
	sData.fpGetPedCompItemCurrent = &GET_PED_COMP_ITEM_CURRENT_MP
	sData.fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_MP
	sData.fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_MP
	sData.fpIsPedCompItemCurrent = &IS_PED_COMP_ITEM_CURRENT_MP
	sData.fpIsPedCompItemNew = &IS_PED_COMPONENT_ITEM_NEW_MP
	sData.fpForceValidPedCompComboForItem = &FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP
	sData.fpBuildBrowseList = &BUILD_BROWSE_LIST_FOR_WARDROBE_MP
	sData.fpUnlockPlayerPedClothesForSavehouse = &UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_MP
	sData.fpGetPedComponentItemRequisite = &GET_PED_COMPONENT_ITEM_REQUISITE_MP
	sData.fpSetPedCompItemIsNew = &SET_PED_COMP_ITEM_IS_NEW_MP
	sData.fpIsAnyVariationOfItemAcquired = &IS_ANY_VARIATION_OF_ITEM_ACQUIRED_MP
	sData.sHelpTrig = "WARD_TRIG"
	sData.fpDressFreemodePlayerAtStartTorso = &DRESS_FREEMODE_PLAYER_AT_START_TORSO
	sData.fpCanPedComponentItemMixWithItem = &CAN_PED_COMPONENT_ITEM_MIX_WITH_ITEM_MP
	
	// Main loop
	WHILE TRUE
	
		WAIT(0)
		
		WARDROBE_MAIN_UPDATE(sData)

	ENDWHILE
ENDSCRIPT
