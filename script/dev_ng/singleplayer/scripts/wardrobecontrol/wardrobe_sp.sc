//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	wardrobe_sp.sc 												//
//		AUTHOR			:	Kenneth Ross / Andrew Minghella								//
//		DESCRIPTION		:	Allows the player to select from a list of clothes that 	//
//							have been acquired and are available at the current stage.	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "wardrobe_core.sch"

WARDROBE_DATA_STRUCT sData

SCRIPT(WARDROBE_LAUNCHER_STRUCT sInData)
	
	sData.sWardrobeLauncherData = sInData
	
	CPRINTLN(DEBUG_PED_COMP, "Wardrobe[", ENUM_TO_INT(sData.sWardrobeLauncherData.eWardrobe), "] started in SP")
	
	SET_BIT(sData.iWardrobeBitset, WARDROBE_BIT_LAUCNHED_IN_SP)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN(DEBUG_PED_COMP, "...wardrobe.sc has been forced to cleanup (SP to MP)")
		CLEANUP_WARDROBE(sData)
	ENDIF

	//Load spPacks verions of the savegame_bed
	IF g_bLoadedClifford 
		REQUEST_SCRIPT_WITH_NAME_HASH(HASH("wardrobe_spCLF"))
		WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("wardrobe_spCLF"))
			WAIT(0)
		ENDWHILE
		START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("wardrobe_spCLF"),sInData, SIZE_OF(sInData),DEFAULT_STACK_SIZE)
	ENDIF
	
	#IF IS_DEBUG_BUILD SETUP_WARDROBE_DEBUG_WIDGETS(sData) #ENDIF
	
	sData.fpGetPedCompDataForItem = &GET_PED_COMP_DATA_FOR_ITEM_SP
	sData.fpGetPedCompItemCurrent = &GET_PED_COMP_ITEM_CURRENT_SP
	sData.fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_SP
	sData.fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_SP
	sData.fpIsPedCompItemCurrent = &IS_PED_COMP_ITEM_CURRENT_SP
	sData.fpIsPedCompItemNew = &IS_PED_COMPONENT_ITEM_NEW_SP
	sData.fpForceValidPedCompComboForItem = &FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_SP
	sData.fpBuildBrowseList = &BUILD_BROWSE_LIST_FOR_WARDROBE_SP
	sData.fpUnlockPlayerPedClothesForSavehouse = &UNLOCK_PLAYER_PED_CLOTHES_FOR_SAVEHOUSE_SP
	sData.fpGetPedComponentItemRequisite = &GET_PED_COMPONENT_ITEM_REQUISITE_SP
	sData.fpSetPedCompItemIsNew = &SET_PED_COMP_ITEM_IS_NEW_SP
	sData.fpIsAnyVariationOfItemAcquired = &IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP
	sData.sHelpTrig = "WARD_TRIG"
	sData.fpDressFreemodePlayerAtStartTorso = &DUMMY_FUNCTION_POINTER_1
	sData.fpCanPedComponentItemMixWithItem = &CAN_PED_COMPONENT_ITEM_MIX_WITH_ITEM_SP
	
	// Main loop
	WHILE TRUE
	
		WAIT(0)
		
		WARDROBE_MAIN_UPDATE(sData)

	ENDWHILE
ENDSCRIPT
