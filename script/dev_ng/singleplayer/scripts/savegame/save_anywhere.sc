//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	save_anywhere.sc											//
//		AUTHOR			:	Andy Minghella												//
//		DESCRIPTION		:	Allows player to save the game anywhere via their phone.	//
//							Also handles restoring s "save anywhere" save. 				//
//														 								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1


USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "cutscene_public.sch"
USING "savegame_private.sch"
USING "debug_channels_structs.sch"
USING "snapshot_private.sch"

ENUM SA_STATE
	SAS_PRE_SAVE,
	SAS_POST_SAVE
ENDENUM

SA_STATE eState

/// PURPOSE: Terminates the thread
PROC CleanupScript()
	CPRINTLN(DEBUG_REPEAT, "Save_anywhere.sc is terminating.")
	TERMINATE_THIS_THREAD()
ENDPROC


/// PURPOSE:
///    handles displaying the help text for the first time the player tries to use save anywhere whilst on mission
PROC HANDLE_ONE_TIME_ONE_MISSION_HELP()
	IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SAVE_ANYWHERE_ON_MISSION)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			// wait for no help to be onscreen then display
			PRINT_HELP("AM_H_SAVEAONM")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SAVE_ANYWHERE_ON_MISSION)
			CPRINTLN(DEBUG_REPEAT, "SaveAnywhere: On mission help text displayed.")
		ENDIF
#IF IS_DEBUG_BUILD
	ELSE
		// we've already shown the help, clear flag
		CPRINTLN(DEBUG_REPEAT, "SaveAnywhere: On mission help text has already been displayed.")
#ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
PROC DO_SAVEGAME()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH eState
			CASE SAS_PRE_SAVE
				IF SAFE_TO_SAVE_GAME()
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					HANG_UP_AND_PUT_AWAY_PHONE()
					WAIT(0)

					// Remove any HUD elements
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)				
							
					// save the snapshot so save system knows this is a "save anywhere" save
					Store_RepeatPlay_Snapshot()
					
					PERFORM_PRE_SAVEGAME_ROUTINE(FALSE, TRUE)

					// this pauses the scripts until the save menu exits
					eState = SAS_POST_SAVE
					
					CPRINTLN(DEBUG_REPEAT, "Save_anywhere.sc Entering save menu.")
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					
					//Final check before saving: is the player safe? Is the respawn controller running?
					//copied from 6618038 ng fix
					IF (NOT IS_PED_INJURED(PLAYER_PED_ID())) 
					AND NOT (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("respawn_controller")) = 0
						SET_SAVE_MENU_ACTIVE(TRUE)
					ELSE
						CPRINTLN(DEBUG_REPEAT, "Player injured just before saving, stopping the save process")
					ENDIF
				ELSE
					// not safe to save the game, exit
					IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
					AND g_OnMissionState != MISSION_TYPE_RANDOM_EVENT
						HANDLE_ONE_TIME_ONE_MISSION_HELP()
					ENDIF
					
					g_HasCellphoneRequestedSave = FALSE
					CPRINTLN(DEBUG_REPEAT, "Save_anywhere.sc Not safe to save the game.")
				ENDIF
			BREAK
			
			CASE SAS_POST_SAVE
				// clear the snapshot- 
				// otherwise any normal saves from now on would be treated as "save anywhere" saves
				Reset_RepeatPlay_Snapshot()
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				g_HasCellphoneRequestedSave = FALSE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


SCRIPT
	CPRINTLN(DEBUG_REPEAT, "Save_anywhere.sc is launching.")
	
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		CPRINTLN(DEBUG_REPEAT, "Save_anywhere.sc force cleanup triggered.")
		CleanupScript()
	ENDIF
	
	WHILE g_HasCellphoneRequestedSave
		DO_SAVEGAME()
		WAIT(0)
	ENDWHILE
	
	//Now terminates after servicing a save request.
	//Relaunched by DO_SCRIPT_LAUNCH_CHECKS() when g_HasCellphoneRequestedSave = TRUE
	CleanupScript()
ENDSCRIPT
