//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_controller.sc										//
//		AUTHOR			:	Kenneth Ross & Alwyn Roberts								//
//		DESCRIPTION		:	Initialises the global data for each player character. 		//
//							Keeps track of all the player data such as weapons, health, //
//							armour, last time active, and last known coords.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "player_ped_scenes.sch"
USING "selector_public.sch"
USING "cellphone_public.sch"
USING "stats_private.sch"
USING "commands_camera.sch"
USING "comms_control_public.sch"
USING "shop_public.sch"
USING "code_control_data_gta5.sch"
USING "beast_secret_hunt.sch"

#IF IS_DEBUG_BUILD
USING "commands_debug.sch"
//USING "player_controller_debug.sch"
#ENDIF


ENUM PLAYER_CONTROL_STAGE_ENUM
	PC_STAGE_INIT = 0,
	PC_STAGE_WAIT,
	PC_STAGE_SWITCH,
	PC_STAGE_END
ENDENUM
PLAYER_CONTROL_STAGE_ENUM eStage = PC_STAGE_INIT

//SELECTOR_SLOTS_ENUM eSelectorPed
//SELECTOR_CAM_STRUCT sSelectorCam

INT iTimeNextPlayerMissionCheck
INT iTimeNextSwitchDeniedCheck
INT iPlayerCharToCheckThisFrame = 0
INT iTimeNextGarageVehicleCheck
//BOOL bRunwayBlipsActive = TRUE
BOOL bWantedRelationshipsSet = FALSE

PED_SCENE_STRUCT 	sSceneData // The struct used for the new scene system

//Purposefully "hidden" in amongst this script. We want it to be hard
//for hackers to spot these scripts in decompiled output. -BenR
#IF FEATURE_SP_DLC_BEAST_SECRET
#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastHuntVars sBeastHuntVars
#ENDIF
#ENDIF

// Cutscene data
BOOL bCutscenePed_VarsSet[NUM_OF_PLAYABLE_PEDS]
BOOL bCutscenePed_DataSet[NUM_OF_PLAYABLE_PEDS]
BOOL bCutsceneVeh_DataSet[NUM_OF_PLAYABLE_PEDS]

VECTOR vLastPlayerVehicleCoords[NUM_PLAYER_VEHICLE_IDS]
FLOAT fLastPlayerVehicleHeading[NUM_PLAYER_VEHICLE_IDS]
BOOL bLastPlayerVehicleInGarage[NUM_PLAYER_VEHICLE_IDS]

MODEL_NAMES eAutoOrderModel
BOOL bSendCarAppAutoProcessEmail = FALSE
BOOL bCarAppOrderFree = FALSE
BOOL bPlayerInCarInGarage = FALSE

INT iMartin1Stage
BOOL bMartin1RecordingRequested
VEHICLE_INDEX viMartin1Veh

INT 	iContentCheckStage		
INT 	iContentCheckTimer
BOOL 	bDisplayContentFeedMessage
BOOL	bDisplayContentRemovedWarning
TEXT_LABEL_31 tlContentLabel
INT 	iContentTickerINT

INT					iProcessAmbientQueue		= 0
SELECTOR_SLOTS_ENUM	eAmbientPlayerSelectorChar	= INT_TO_ENUM(SELECTOR_SLOTS_ENUM, 0)
INT iTempCol1, iTempCol2, iTempCol3, iTempCol4

CONST_INT	iCONTENT_CHECK_0_SPECIAL_EDITION	NCU_SPECIAL_EDITION
CONST_INT	iCONTENT_CHECK_1_COLLECTORS_EDITION	NCU_COLLECTORS_EDITION
CONST_INT	iCONTENT_CHECK_2_SOCIAL_CLUB		NCU_SOCIAL_CLUB
CONST_INT	iCONTENT_CHECK_3_BLIMP				NCU_BLIMP
CONST_INT	iCONTENT_CHECK_4_WEAPONS			4
CONST_INT	iCONTENT_CHECK_5_VEHICLES			5
CONST_INT	iCONTENT_CHECK_6_CLOTHES			6
CONST_INT	iCONTENT_CHECK_7_HAIRDOS			7
CONST_INT	iCONTENT_CHECK_8_TATTOOS			8
CONST_INT	iCONTENT_CHECK_9_STUNT_PLANES		9
CONST_INT	iCONTENT_CHECK_10_JAPANESE_SE		10
CONST_INT	iCONTENT_CHECK_12_NEXT_GEN_PREORDER	11
CONST_INT	iCONTENT_CHECK_13_CG_TO_NG			12
CONST_INT	iCONTENT_CHECK_14_HEISTS			13
CONST_INT	iCONTENT_CHECK_CASH_GIFT			14
CONST_INT	iMAX_LAST_ITEMS						15

INT iCacheShopData

CONST_INT CACHE_SHOP_VEHICLE_DATA_BS			0
CONST_INT CACHE_SHOP_WEAPON_DATA_BS				1
CONST_INT CACHE_SHOP_CLOTHES_DATA_BS			2
CONST_INT CACHE_SHOP_TATTOO_DATA_BS				3
CONST_INT CACHE_SHOP_HAIRD_DATA_BS				4

#IF IS_DEBUG_BUILD
	//Mission list debug variables
	BOOL bMissionCountHasChanged[3]

	WIDGET_GROUP_ID widgetID

	//WIDGET_GROUP_ID wPlayerControllerWidget
	//BOOL bGetAverageOfMissionArray
	BOOL bCreateVehicle[NUM_OF_PLAYABLE_PEDS]
	BOOL bCreateNPCVehicle
	//INT iTempRecentlySelectedScene
	INT iVehicleTypeDebug
	//BOOL bUpdateLocations
	//TEXT_WIDGET_ID wTestWidget
	BOOL bRandomisePlayerVariations
	BOOL bSetDefaultOutfit
	BOOL bOpenTrevorsDoor = FALSE
	BOOL bSetRandomoutfitOnStartup

	MODEL_NAMES eCompLookupPed
	INT iCompLookupType, iCompLookupItem, iCompLookupBitset, iCompLookupDummyItems
	INT iLastDraw, iLastTex
	BOOL bCheckMissingPedComponents
	TEXT_WIDGET_ID text_widgetID
	PED_INDEX MissingComponentsPed

	BOOL bTestTimelapseCutscene, bTestDefaultPlayerSwitchState
	BOOL bDrawSelectorShuit
	BOOL bPrintCurrentPedCompInfo, bPrintBitsetInfo
	
	BOOL debug_store_weapons, debug_restore_weapons, debug_remove_weapons, debug_remove_ammo, debug_store_snapshot, debug_restore_snapshot, debug_restore_snapshot_keeper
	BOOL debug_do_mask_test

	PROC SETUP_PLAYER_CONTROL_DEBUG_WIDGETS()
		
		widgetID = START_WIDGET_GROUP("Player Controller")
		
			ADD_WIDGET_BOOL("Draw debug display", g_bDrawPlayerControlDebugDisplay)
		
			ADD_WIDGET_BOOL("Draw Literal Scene String", g_bDrawLiteralSceneString)
			
			ADD_WIDGET_BOOL("Set player clothes in cutscenes", g_bSetPlayerClothesInCutscenes)
			ADD_WIDGET_BOOL("Set player weapons in cutscenes", g_bSetPlayerWeaponsInCutscenes)
			
			ADD_WIDGET_BOOL("Do mask test", debug_do_mask_test)
			
			START_WIDGET_GROUP("Player Ped Weapons")
			
				ADD_WIDGET_BOOL("Store weapons", debug_store_weapons)
				ADD_WIDGET_BOOL("Restore weapons", debug_restore_weapons)
				ADD_WIDGET_BOOL("Remove weapons", debug_remove_weapons)
				ADD_WIDGET_BOOL("Remove ammo", debug_remove_ammo)
				
				ADD_WIDGET_BOOL("Store snapshot", debug_store_snapshot)
				ADD_WIDGET_BOOL("Restore snapshot", debug_restore_snapshot)
				ADD_WIDGET_BOOL("Restore snapshot and keep", debug_restore_snapshot_keeper)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Player Ped Clothes")
			
				text_widgetID = ADD_TEXT_WIDGET("Model to check")
				SET_CONTENTS_OF_TEXT_WIDGET(text_widgetID, "player_zero")
				ADD_WIDGET_BOOL("Check missing components", bCheckMissingPedComponents)
			
				ADD_WIDGET_BOOL("Player is changing clothes (READ ONLY)", g_bPlayerIsChangingClothes)
				
				ADD_WIDGET_BOOL("Randomise Player Variations", bRandomisePlayerVariations)
				
				ADD_WIDGET_BOOL("Set random start outfit", bSetRandomoutfitOnStartup)
				
				ADD_WIDGET_BOOL("Set default outfit", bSetDefaultOutfit)
				
				ADD_WIDGET_BOOL("Print player's current ped component info", bPrintCurrentPedCompInfo)
				
				ADD_WIDGET_BOOL("Print bitset totals", bPrintBitsetInfo)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Player Ped Globals")
				ADD_WIDGET_BOOL("Can change clothes on mission", g_bPlayerCanChangeClothesOnMission)
				
				ADD_WIDGET_BOOL("Michael available", g_savedGlobals.sPlayerData.sInfo.bPedAvailable[CHAR_MICHAEL])
				ADD_WIDGET_BOOL("Franklin available", g_savedGlobals.sPlayerData.sInfo.bPedAvailable[CHAR_FRANKLIN])
				ADD_WIDGET_BOOL("Trevor available", g_savedGlobals.sPlayerData.sInfo.bPedAvailable[CHAR_TREVOR])
				
				
				ADD_WIDGET_INT_READ_ONLY("Michael missions", g_iAvailablePlayerMissions[CHAR_MICHAEL])
				ADD_WIDGET_INT_READ_ONLY("Franklin missions", g_iAvailablePlayerMissions[CHAR_FRANKLIN])
				ADD_WIDGET_INT_READ_ONLY("Trevor missions", g_iAvailablePlayerMissions[CHAR_TREVOR])
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Player Vehicles")
				ADD_WIDGET_INT_SLIDER("Type", iVehicleTypeDebug, 0, NUM_OF_PLAYER_PED_VEHICLE_TYPES-1, 1)
				ADD_WIDGET_BOOL("Create Michael's vehicle", bCreateVehicle[0])
				ADD_WIDGET_BOOL("Create Franklin's vehicle", bCreateVehicle[1])
				ADD_WIDGET_BOOL("Create Trevor's vehicle", bCreateVehicle[2])
				ADD_WIDGET_BOOL("Create NPC's vehicle", bCreateNPCVehicle)
			STOP_WIDGET_GROUP()
			
	//		START_WIDGET_GROUP("Player Locations")
	//		
	//			ADD_WIDGET_BOOL("Update locations", bUpdateLocations)
	//			ADD_WIDGET_BOOL("Debug Print: Scene Schedule Info", g_bDebugPrint_SceneScheduleInfo)
	//			
	//			PED_REQUEST_SCENE_ENUM eScene
	//			
	//			START_NEW_WIDGET_COMBO()
	//				REPEAT (ENUM_TO_INT(NUM_OF_PED_REQUEST_SCENES)+2) eScene
	//					ADD_TO_WIDGET_COMBO(Get_String_From_Ped_Request_Scene_Enum(eScene))
	//				ENDREPEAT
	//			STOP_WIDGET_COMBO("Recent Location", iTempRecentlySelectedScene)
	//			
	//			g_iSelectedDebugPlayerCharScene = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(NUM_OF_PED_REQUEST_SCENES))
	//			
	//			START_NEW_WIDGET_COMBO()
	//				REPEAT NUM_OF_PED_REQUEST_SCENES eScene
	//					ADD_TO_WIDGET_COMBO(Get_String_From_Ped_Request_Scene_Enum(eScene))
	//				ENDREPEAT
	//			STOP_WIDGET_COMBO("Locations", g_iSelectedDebugPlayerCharScene)
	//			
	//			ADD_WIDGET_INT_SLIDER("Location num", g_iSelectedDebugPlayerCharScene, 0, ENUM_TO_INT(NUM_OF_PED_REQUEST_SCENES)-1, 1)
	//			wTestWidget = ADD_TEXT_WIDGET("Location name")
	//			SET_CONTENTS_OF_TEXT_WIDGET(wTestWidget, Get_String_From_Ped_Request_Scene_Enum(INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, g_iSelectedDebugPlayerCharScene)))
	//			
	//			ADD_WIDGET_BOOL("Warp to selected location", g_bWarpDebugPlayerCharScene)
	//			ADD_WIDGET_BOOL("Perform Scene Demo", bPerformDemo)
	//			
	//			ADD_WIDGET_BOOL("Get Average Of Mission Array", bGetAverageOfMissionArray)
	//		STOP_WIDGET_GROUP()
			
			
			START_WIDGET_GROUP("Player Switch Values")
	//			ADD_WIDGET_BOOL("g_bUseProtoTypeCamSpline", g_bUseProtoTypeCamSpline)
	//			ADD_WIDGET_BOOL("g_bUseProtoTypeCamStreaming", g_bUseProtoTypeCamStreaming)
				
				ADD_WIDGET_BOOL("bOpenTrevorsDoor", bOpenTrevorsDoor)
	//			START_WIDGET_GROUP("Camera Positions")
	//				START_WIDGET_GROUP("Max Height")
	//					ADD_WIDGET_FLOAT_SLIDER("fMaxCamZ", fMaxCamZ, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("Duration", fSkyDuration, 0.0, 200000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("Start Ped 1")
	//					ADD_WIDGET_FLOAT_SLIDER("From Gamplay to first cam", fGameplayToSpline, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("Start Ped Height1", fFromPedHeight1, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("Start Duration1", fFromPedDuration1, 0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("Start Ped 2")
	//					ADD_WIDGET_FLOAT_SLIDER("Start Ped Height2", fFromPedHeight2, 0.0,20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("Start Duration2", fFromPedDuration2, 0.0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("Start Ped 3")
	//					ADD_WIDGET_FLOAT_SLIDER("Start Ped Height3", fFromPedHeight3, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("Start Duration3", fFromPedDuration3, 0.0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("End Ped 1")
	//					ADD_WIDGET_FLOAT_SLIDER("End Ped Height1", fTOPedHeight1, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("End Duration1", fTOPedDuration1, 0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("End Ped 2")
	//					ADD_WIDGET_FLOAT_SLIDER("End Ped Height2", fTOPedHeight2, 0.0,20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("End Duration2", fTOPedDuration2, 0.0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				START_WIDGET_GROUP("End Ped 3")
	//					ADD_WIDGET_FLOAT_SLIDER("End Ped Height3", fTOPedHeight3, 0.0, 20000, 1.0)
	//					ADD_WIDGET_FLOAT_SLIDER("End Duration3", fTOPedDuration3, 0.0, 20000, 1.0)
	//				STOP_WIDGET_GROUP()
	//				ADD_WIDGET_FLOAT_SLIDER("Motion Blur (at highest level)", fSkyMotionBlur, 0.0, 100.0, 0.1)
	//			STOP_WIDGET_GROUP()
	//			START_WIDGET_GROUP("Geo Loading")
	//
	////				ADD_WIDGET_INT_SLIDER("Camera Node to start loading", iLoadCamTo , 0, 20, 1)
	////				ADD_WIDGET_INT_SLIDER("Camera Node to Force Low LOD", iForceLOWlod , 0, 20, 1)
	////				ADD_WIDGET_INT_SLIDER("Camera Node to Disable Force Low LOD", iDisableForcedLOWlod, 0, 20, 1)
	//				
	//			STOP_WIDGET_GROUP()
	//			START_WIDGET_GROUP("Flash Effect")
	//				ADD_WIDGET_FLOAT_SLIDER("Minimum Exposure", fMinExposure, 0.0, 32.0, 0.1)
	//				ADD_WIDGET_FLOAT_SLIDER("Maximum Exposure", fMaxExposure, 0.0, 32.0, 0.1)
	//				ADD_WIDGET_INT_SLIDER("Ramp Up duration (msec)", iRampUpDuration, 0, 5000, 25)
	//				ADD_WIDGET_INT_SLIDER("Hold duration (msec)", iRampUpDuration, 0, 5000, 25)
	//				ADD_WIDGET_INT_SLIDER("Ramp Down duration (msec)", iRampUpDuration, 0, 5000, 25)
	//			STOP_WIDGET_GROUP()

			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("bTestTimelapseCutscene", bTestTimelapseCutscene)
			ADD_WIDGET_BOOL("bTestDefaultPlayerSwitchState", bTestDefaultPlayerSwitchState)
			ADD_WIDGET_BOOL("bDrawSelectorShuit", bDrawSelectorShuit)
			
		STOP_WIDGET_GROUP()
	ENDPROC

	FUNC STRING GET_PED_COMP_STRING(PED_COMPONENT eType)
		SWITCH eType
			CASE PED_COMP_HEAD 		RETURN "HEAD" 		BREAK
			CASE PED_COMP_HAIR 		RETURN "HAIR" 		BREAK
			CASE PED_COMP_TORSO 	RETURN "TORSO" 		BREAK
			CASE PED_COMP_LEG 		RETURN "LEG" 		BREAK
			CASE PED_COMP_FEET 		RETURN "FEET"		BREAK
			CASE PED_COMP_HAND 		RETURN "HAND" 		BREAK
			CASE PED_COMP_SPECIAL 	RETURN "SPECIAL" 	BREAK
			CASE PED_COMP_SPECIAL2 	RETURN "SPECIAL2"	BREAK
			CASE PED_COMP_DECL 		RETURN "DECL" 		BREAK
			CASE PED_COMP_BERD 		RETURN "BERD" 		BREAK
			CASE PED_COMP_TEETH 	RETURN "TEETH" 		BREAK
			CASE PED_COMP_JBIB 		RETURN "JBIB" 		BREAK
		ENDSWITCH
		RETURN ""
	ENDFUNC

	// Check what ped components we have yet to set up
	FUNC BOOL CHECK_MISSING_PED_COMPONENTS()

		IF NOT DOES_ENTITY_EXIST(MissingComponentsPed)
		OR IS_PED_INJURED(MissingComponentsPed)
			RETURN TRUE
		ENDIF
		
		PED_COMP_ITEM_DATA_STRUCT sCompData
		
		INT iBit, iTempItem, iStartLoop
		REPEAT 32 iBit
			// Get data for this item
			sCompData = GET_PED_COMP_DATA_FOR_ITEM_SP(eCompLookupPed, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)), INT_TO_ENUM(PED_COMP_NAME_ENUM, (iCompLookupBitset*32)+iBit))
			
			IF sCompData.iItemBit > -1

				IF (sCompData.iDrawable = iLastDraw AND sCompData.iTexture = iLastTex)
					CPRINTLN(DEBUG_PED_COMP, "...*** Duplicate ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)),   " [", sCompData.iDrawable, ", ", sCompData.iTexture, "] ***")
				ELIF (sCompData.iTexture < iLastTex AND sCompData.iDrawable <= iLastDraw)			
					CPRINTLN(DEBUG_PED_COMP, "...*** Order messed up ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)), " ***")
				ELSE
					// Fill in any gaps...
					WHILE (iLastDraw > -1 AND sCompData.iDrawable > iLastDraw)
					OR    (sCompData.iTexture > iLastTex+1)
					
						// Textures for previous drawables
						IF sCompData.iDrawable > iLastDraw
						
							IF sCompData.iDrawable > iLastDraw+1
							AND iLastTex = -1
								CPRINTLN(DEBUG_PED_COMP, "...Item missing ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)),   " [", iLastDraw, ", ", 0, "]")
								iCompLookupItem++
								iCompLookupDummyItems++
								iLastTex = 0
							ENDIF
							
							iStartLoop = iLastTex+1
							FOR iTempItem = iStartLoop TO GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(MissingComponentsPed, INT_TO_ENUM(PED_COMPONENT, iCompLookupType), iLastDraw)-1
								
								CPRINTLN(DEBUG_PED_COMP, "...Item missing ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)),   " [", iLastDraw, ", ", iTempItem, "]")
								
								iCompLookupItem++
								iCompLookupDummyItems++
								iLastTex = iTempItem
							ENDFOR
							iLastTex=-1
							iLastDraw++
						
						// Textures for current drawable
						ELSE
							iStartLoop = iLastTex+1
							FOR iTempItem = iStartLoop TO sCompData.iTexture-1
							
								CPRINTLN(DEBUG_PED_COMP, "...Item missing ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)),   " [", iLastDraw, ", ", iTempItem, "]")
								
								iCompLookupItem++
								iCompLookupDummyItems++
								iLastTex = iTempItem
							ENDFOR
						ENDIF
					ENDWHILE
					
					// Add current item
					//PRINTLN("...Adding item ", GET_PED_COMP_STRING(INT_TO_ENUM(PED_COMPONENT, iCompLookupType)),   " [", sCompData.iDrawable, ", ", sCompData.iTexture, "]")
					iCompLookupItem++
				ENDIF
				
				iLastDraw = sCompData.iDrawable
				iLastTex = sCompData.iTexture
			ENDIF
		ENDREPEAT
		
		// Process the next bitset on the next frame
		iCompLookupBitset++
		
		// Process the next item type when we have processed these bitsets
		IF iCompLookupBitset >= PED_COMPONENT_BITSETS
			iCompLookupBitset = 0
			iCompLookupType++
			iLastDraw = -1
			iLastTex = -1
		ENDIF
		
		// Finish processing when we have processed the types
		IF iCompLookupType >= NUM_PED_COMPONENTS
			CPRINTLN(DEBUG_PED_COMP, "...Finished")
			CPRINTLN(DEBUG_PED_COMP,"...Total items = ", iCompLookupItem)
			CPRINTLN(DEBUG_PED_COMP,"...Total items setup = ", iCompLookupItem-iCompLookupDummyItems)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDFUNC
	
	PROC MAINTAIN_PLAYER_CONTROL_DEBUG_WIDGETS()
	
		IF debug_do_mask_test
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_HAIR, HAIR_P0_3_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_P0_4_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_WRESTLER_MASK_3, FALSE)
				STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
			ENDIF
			debug_do_mask_test = FALSE
		ENDIF
	
		IF debug_store_weapons
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
			ENDIF
			debug_store_weapons = FALSE
		ENDIF
		
		IF debug_restore_weapons
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
			ENDIF
			debug_restore_weapons = FALSE
		ENDIF
		
		IF debug_remove_weapons
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			ENDIF
			debug_remove_weapons = FALSE
		ENDIF
		
		IF debug_remove_ammo
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				INT iSlot
				WEAPON_TYPE eWeapon
				WEAPON_SLOT eSlot
				REPEAT NUM_PLAYER_PED_WEAPON_SLOTS iSlot
					eSlot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iSlot)
					IF eSlot != WEAPONSLOT_INVALID
					AND eSlot != WEAPONSLOT_UNARMED
						eWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iSlot))
						IF eWeapon != WEAPONTYPE_INVALID
						AND eWeapon != WEAPONTYPE_UNARMED
						AND IS_WEAPON_AVAILABLE_FOR_GAME(eWeapon)
							SET_PED_AMMO(PLAYER_PED_ID(), eWeapon, 0)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			debug_remove_ammo = FALSE
		ENDIF
		
		IF debug_store_snapshot
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
			ENDIF
			debug_store_snapshot = FALSE
		ENDIF
		IF debug_restore_snapshot
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID(), TRUE)
			ENDIF
			debug_restore_snapshot = FALSE
		ENDIF
		IF debug_restore_snapshot_keeper
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID(), FALSE)
			ENDIF
			debug_restore_snapshot_keeper = FALSE
		ENDIF
		
		IF bCheckMissingPedComponents
			
			eCompLookupPed = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(text_widgetID)))
			
			IF IS_MODEL_A_PED(eCompLookupPed)
				IF DOES_ENTITY_EXIST(MissingComponentsPed)
					DELETE_PED(MissingComponentsPed)
				ENDIF
				
				REQUEST_MODEL(eCompLookupPed)
				WHILE NOT HAS_MODEL_LOADED(eCompLookupPed)
					WAIT(0)
				ENDWHILE
				
				MissingComponentsPed = CREATE_PED(PEDTYPE_MISSION, eCompLookupPed, <<100, 100, 100>>)
				SET_ENTITY_COLLISION(MissingComponentsPed, FALSE)
				SET_ENTITY_VISIBLE(MissingComponentsPed, FALSE)
				FREEZE_ENTITY_POSITION(MissingComponentsPed, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(eCompLookupPed)
				
				iCompLookupType = 0
				iCompLookupBitset = 0
				iCompLookupItem = 0
				iCompLookupDummyItems = 0
				iLastDraw = -1
				iLastTex = -1
				
				CPRINTLN(DEBUG_PED_COMP, "****************************************")
				CPRINTLN(DEBUG_PED_COMP, "CHECK_MISSING_PED_COMPONENTS")
				CPRINTLN(DEBUG_PED_COMP, "...Model = ", GET_CONTENTS_OF_TEXT_WIDGET(text_widgetID))
			
				WHILE NOT CHECK_MISSING_PED_COMPONENTS()
					WAIT(0)
				ENDWHILE
				CPRINTLN(DEBUG_PED_COMP, "****************************************")
				
				IF DOES_ENTITY_EXIST(MissingComponentsPed)
					DELETE_PED(MissingComponentsPed)
				ENDIF
			ENDIF
			bCheckMissingPedComponents = FALSE
		ENDIF
		
		IF bSetDefaultOutfit
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
			bSetDefaultOutfit = FALSE
		ENDIF
		
		IF bRandomisePlayerVariations
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_RANDOM_COMPONENT_VARIATION(PLAYER_PED_ID())
			ENDIF
			bRandomisePlayerVariations = FALSE
		ENDIF
		
		// print the ped comp details of all of the player's current clothes
		IF bPrintCurrentPedCompInfo = TRUE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				PRINT_CURRENT_PED_COMP_INFO_SP(PLAYER_PED_ID())
			ENDIF
			bPrintCurrentPedCompInfo = FALSE
		ENDIF
		
		// print the ped comp details of all of the player's current clothes
		IF bPrintBitsetInfo = TRUE
			PRINT_PED_COMP_BITSETS_REQUIRED()
			bPrintBitsetInfo = FALSE
		ENDIF
		
		
	//	IF bUpdateLocations
	//		iTempRecentlySelectedScene = ENUM_TO_INT(g_eRecentlySelectedScene)
	//	ENDIF
		
	//	SET_CONTENTS_OF_TEXT_WIDGET(wTestWidget, Get_String_From_Ped_Request_Scene_Enum(INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, g_iSelectedDebugPlayerCharScene)))
		
	//	IF g_bWarpDebugPlayerCharScene
	//	//for bug 884228
	//		sSceneData.sSelectorPeds.ePreviousSelectorPed = SELECTOR_PED_MICHAEL
	//		
	//
	//		sSceneData.iStage = 0
	//		sSceneData.eScene = INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, g_iSelectedDebugPlayerCharScene)
	//		sSceneData.sSelectorCam.bSplineCreated = FALSE
	//		
	//		
	//		
	//		GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sSceneData.eScene, sSceneData.ePed)
	//		
	//		IF (sSceneData.ePed = CHAR_MULTIPLAYER)
	//			
	//			IF (sSceneData.eScene = PR_SCENE_FTa_FRANKLIN1a
	//			OR sSceneData.eScene = PR_SCENE_FTa_FRANKLIN1b
	//			OR sSceneData.eScene = PR_SCENE_FTa_FRANKLIN1c
	//			OR sSceneData.eScene = PR_SCENE_FTa_FRANKLIN1d
	//			OR sSceneData.eScene = PR_SCENE_FTa_FRANKLIN1e)
	//			
	//				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	//					CASE CHAR_MICHAEL
	//						IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
	//							sSceneData.ePed = CHAR_FRANKLIN
	//						ELSE
	//							sSceneData.ePed = CHAR_TREVOR
	//						ENDIF
	//					BREAK
	//					CASE CHAR_FRANKLIN
	//						sSceneData.ePed = CHAR_TREVOR
	//					BREAK
	//					CASE CHAR_TREVOR
	//						sSceneData.ePed = CHAR_FRANKLIN
	//					BREAK
	//				ENDSWITCH
	//			ENDIF
	//		ENDIF
	//		
	//		
	//		FLOAT fScenePercent = -1
	//		PED_REQUEST_SCENE_ENUM eDebugSelectedScene = PR_SCENE_INVALID
	//		IF NOT GET_PLAYER_PED_POSITION_FOR_SCENE(sSceneData.eScene, sSceneData.vCreateCoords, sSceneData.fCreateHead, sSceneData.tCreateRoom)
	//			PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	//			eDebugSelectedScene = sSceneData.eScene
	//			GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(sSceneData.ePed, sSceneData.eScene, fScenePercent, sSceneData, sPassedScene)
	//			
	//		ENDIF
	//		
	////		PED_INFO_STRUCT sInfo
	////		IF GetLastKnownPedInfoPostMission(sSceneData.eScene,
	////				sInfo.vLastKnownCoords[sSceneData.ePed],
	////				sInfo.fLastKnownHead[sSceneData.ePed])
	////			TestSelectedMissionSceneCoords(sSceneData.eScene, sSceneData.ePed, sInfo)
	////		ENDIF
	//		
	//		IF IS_PED_THE_CURRENT_PLAYER_PED(sSceneData.ePed)
	//			SELECTOR_SLOTS_ENUM newSelectorPed
	//			SWITCH sSceneData.ePed
	//				CASE CHAR_MICHAEL
	//					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
	//						newSelectorPed = SELECTOR_PED_FRANKLIN
	//					ELSE
	//						newSelectorPed = SELECTOR_PED_TREVOR
	//					ENDIF
	//				BREAK
	//				CASE CHAR_FRANKLIN
	//					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
	//						newSelectorPed = SELECTOR_PED_MICHAEL
	//					ELSE
	//						newSelectorPed = SELECTOR_PED_TREVOR
	//					ENDIF
	//				BREAK
	//				CASE CHAR_TREVOR
	//					IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
	//						newSelectorPed = SELECTOR_PED_MICHAEL
	//					ELSE
	//						newSelectorPed = SELECTOR_PED_FRANKLIN
	//					ENDIF
	//				BREAK
	//			ENDSWITCH
	//			
	//			WHILE NOT SET_CURRENT_SELECTOR_PED(newSelectorPed)
	//				WAIT(0)
	//			ENDWHILE
	//		ENDIF
	//		
	//		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	//			CASE CHAR_MICHAEL
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
	//			BREAK
	//			CASE CHAR_FRANKLIN
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
	//			BREAK
	//			CASE CHAR_TREVOR
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
	//			BREAK
	//		ENDSWITCH
	//		
	//		// // // #949607
	//		SELECTOR_SLOTS_ENUM ePedSlot = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(sSceneData.ePed)
	//		PRINTSTRING("slot ")
	//		PRINTSTRING(GET_PLAYER_PED_STRING(sSceneData.ePed))
	//		PRINTSTRING(" has ")
	//		IF DOES_ENTITY_EXIST(sSceneData.sSelectorPeds.pedID[ePedSlot])
	//			PRINTSTRING("sSceneData exists")
	//			
	//			VECTOR vCreateCoords
	//			FLOAT fCreateHead
	//			TEXT_LABEL_31 tRoom
	//			IF GET_PLAYER_PED_POSITION_FOR_SCENE(sSceneData.eScene, vCreateCoords, fCreateHead, tRoom)
	//				
	//				VECTOR vPedSlotCoord = GET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[ePedSlot], FALSE)
	//				
	//				IF NOT ARE_VECTORS_ALMOST_EQUAL(vCreateCoords, vPedSlotCoord, 5.0)
	//					PRINTSTRING("	pedTo(")
	//					PRINTVECTOR(vPedSlotCoord)
	//					PRINTSTRING(") nowhere near vCreateCoords(")
	//					PRINTVECTOR(vCreateCoords)
	//					PRINTSTRING("): dist:")
	//					PRINTFLOAT(VDIST(vCreateCoords, vPedSlotCoord))
	//					
	//					SET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[ePedSlot], vCreateCoords)
	//					SET_ENTITY_HEADING(sSceneData.sSelectorPeds.pedID[ePedSlot], fCreateHead)
	//				ENDIF
	//			ENDIF
	//		ELSE
	//			PRINTSTRING("sSceneData DOESNT exist")
	//		ENDIF
	//		PRINTNl()
	//		// // //
	//		
	//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, TRUE)
	//			CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
	//		ENDIF
	//		
	//		BOOL bPerformingGenericPedScene = TRUE
	//		WHILE IS_PLAYER_PLAYING(PLAYER_ID())
	//		AND bPerformingGenericPedScene
	//			IF NOT GENERIC_PED_SCENE(sSceneData,
	//					&SCENE_CUSTOM_SCRIPT_REQUEST,
	//					&SCENE_CUSTOM_SCRIPT_SETUP)
	//				
	//				
	//				IF (eDebugSelectedScene <> PR_SCENE_INVALID)
	//				
	//					
	//					#IF IS_DEBUG_BUILD
	//					IF g_bDrawLiteralSceneString
	//					
	//					STRING sScene
	//					sScene = Get_String_From_Ped_Request_Scene_Enum(eDebugSelectedScene)
	//					HUD_COLOURS eHudColour
	//					eHudColour = HUD_COLOUR_BLUELIGHT
	//			
	//					TEXT_LABEL_63 sLiteral
	//					sLiteral  = ("eDebugSelectedScene =  \"")
	//					sLiteral += GET_STRING_FROM_STRING(sScene,
	//							GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
	//							GET_LENGTH_OF_LITERAL_STRING(sScene))
	//					sLiteral += ("\" , percent: ")
	//					sLiteral += GET_STRING_FROM_FLOAT(fScenePercent)
	//					
	//					SET_TEXT_SCALE(0.4, 0.4)
	//					SET_TEXT_HUD_COLOUR(eHudColour, 127)
	//					SET_TEXT_JUSTIFICATION(FONT_CENTRE)
	//					SET_TEXT_OUTLINE()
	//					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.925 - 0.03 - 0.03, "STRING", sLiteral)
	//					ENDIF
	//					#ENDIF
	//					
	//				ENDIF
	//				
	//				iTempRecentlySelectedScene = ENUM_TO_INT(g_eRecentlySelectedScene)
	//			ELSE
	//				bPerformingGenericPedScene = FALSE
	//			ENDIF
	//			WAIT(0)
	//		ENDWHILE
	//		
	//		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	//			CASE CHAR_MICHAEL
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
	//			BREAK
	//			CASE CHAR_FRANKLIN
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
	//			BREAK
	//			CASE CHAR_TREVOR
	//				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
	//				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
	//			BREAK
	//		ENDSWITCH
	//		
	//		g_bWarpDebugPlayerCharScene = FALSE
	//	ENDIF
		
	//	IF bPerformDemo
	//		PED_REQUEST_SCENE_ENUM sceneArray[NUM_OF_PED_REQUEST_SCENES]
	//		INT iSceneWait[NUM_OF_PED_REQUEST_SCENES]
	//		
	//		Perform_Switch_Demo(sceneArray, iSceneWait,
	//				bPerformDemo, iTempRecentlySelectedScene)
	//		bPerformDemo = FALSE
	//	ENDIF
		
		
	//	IF bTestSelectedMissionSceneCoords
	//		
	//		PED_REQUEST_SCENE_ENUM eReqScene
	//		REPEAT NUM_OF_PED_REQUEST_SCENES eReqScene
	//			IF (eReqScene <> PR_SCENE_DEAD)
	//				enumCharacterList ePed = NO_CHARACTER
	//				GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(eReqScene, ePed)
	//				
	//				IF TestSelectedMissionSceneCoords(eReqScene)
	//					
	//					PRINTSTRING("TestSelectedMissionSceneCoords(")
	//					PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eReqScene))
	//					PRINTSTRING("): TRUE")
	//					
	//				ELSE
	//					
	//					PRINTSTRING("TestSelectedMissionSceneCoords(")
	//					PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(eReqScene))
	//					PRINTSTRING("): FALSE")
	//					
	//				ENDIF
	//				
	//				PRINTNL()
	//			ENDIF
	//		ENDREPEAT
	//		
	//		bTestSelectedMissionSceneCoords = FALSE
	//	ENDIF
	//	IF bGetAverageOfMissionArray
	//		Get_Average_Of_Mission_Array(wPlayerControllerWidget, bGetAverageOfMissionArray)
	//		bGetAverageOfMissionArray = FALSE
	//	ENDIF
		
		// Vehicles
		INT iPed
		VEHICLE_INDEX vehTemp
		VECTOR vCoords
		FLOAT fHeading
		REPEAT NUM_OF_PLAYABLE_PEDS iPed
			IF bCreateVehicle[iPed]
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
					vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 2.5, 0.0>>)
					fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					
					WHILE NOT CREATE_PLAYER_VEHICLE(vehTemp, INT_TO_ENUM(enumCharacterList, iPed), vCoords, fHeading, TRUE, INT_TO_ENUM(VEHICLE_CREATE_TYPE_ENUM, iVehicleTypeDebug))
						WAIT(0)
					ENDWHILE
					
					IF DOES_ENTITY_EXIST(vehTemp)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTemp)
					ENDIF
				ENDIF
				
				bCreateVehicle[iPed] = FALSE
			ENDIF
		ENDREPEAT
		
		IF bCreateNPCVehicle
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
				vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 2.5, 0.0>>)
				fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
				
				WHILE NOT CREATE_NPC_VEHICLE(vehTemp, CHAR_AMANDA, vCoords, fHeading, TRUE, INT_TO_ENUM(VEHICLE_CREATE_TYPE_ENUM, iVehicleTypeDebug))
					WAIT(0)
				ENDWHILE
				
				IF DOES_ENTITY_EXIST(vehTemp)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTemp)
				ENDIF
			ENDIF
			bCreateNPCVehicle = FALSE
		ENDIF
		
		IF bOpenTrevorsDoor = TRUE
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_trevtraildr, (<< 1973.0, 3815.0, 34.0 >>), TRUE, 1.0, TRUE	)
			bOpenTrevorsDoor = FALSE
		ENDIF
		
		IF bSetRandomoutfitOnStartup
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		
			PRINTLN("Setting random outfit on the player")
		
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 27)
						CASE 0	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE) BREAK
						CASE 1	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_NAVY_JANITOR, FALSE) BREAK
						CASE 2	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE) BREAK
						CASE 3	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_GOLF, FALSE) BREAK
						CASE 4	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA, FALSE) BREAK
						CASE 5	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE) BREAK
						CASE 6	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_TENNIS, FALSE) BREAK
						CASE 7	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE) BREAK
						CASE 8	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_TRIATHLON, FALSE) BREAK
						CASE 9	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SECURITY, FALSE) BREAK
						CASE 10	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EXTERMINATOR, FALSE) BREAK
						CASE 11	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO, FALSE) BREAK
						CASE 12	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF, FALSE) BREAK
						CASE 13	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BLUE_BOILER_SUIT, FALSE) BREAK
						CASE 14	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1, FALSE) BREAK
						CASE 15	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2, FALSE) BREAK
						CASE 16	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3, FALSE) BREAK
						CASE 17	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_PROLOGUE, FALSE) BREAK
						CASE 18	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_COMMANDO, FALSE) BREAK
						CASE 19	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE) BREAK
						CASE 20	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_LEATHER_AND_JEANS, FALSE) BREAK
						CASE 21	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, FALSE) BREAK
						CASE 22	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DARK_GRAY_SUIT, FALSE) BREAK
						CASE 23	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_AND_PANTS, FALSE) BREAK
						CASE 24	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA_FLIP_FLOPS, FALSE) BREAK
						CASE 25	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_POLOSHIRT_PANTS, FALSE) BREAK
						CASE 26	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BED, FALSE) BREAK
					ENDSWITCH
				BREAK
				
				CASE CHAR_FRANKLIN
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 34)
						CASE 0	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE) BREAK
						CASE 1	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO, FALSE) BREAK
						CASE 2	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_GOLF, FALSE) BREAK
						CASE 3	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH, FALSE) BREAK
						CASE 4	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRIATHLON, FALSE) BREAK
						CASE 5	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_EXTERMINATOR, FALSE) BREAK
						CASE 6	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_BLACK_BOILER, FALSE) BREAK
						CASE 7	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SKYDIVING, FALSE) BREAK
						CASE 8	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TUXEDO, FALSE) BREAK
						CASE 9	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_BLUE_BOILER_SUIT, FALSE) BREAK
						CASE 10	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1, FALSE) BREAK
						CASE 11	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_2, FALSE) BREAK
						CASE 12	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_3, FALSE) BREAK
						CASE 13	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_GREEN_SHIRT_JEANS, FALSE) BREAK
						CASE 14	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SUIT_1, FALSE) BREAK
						CASE 15	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_HOODIE_AND_JEANS_1, FALSE) BREAK
						CASE 16	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRACKSUIT_JEANS, FALSE) BREAK
						CASE 17	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_SHIRT_JEANS, FALSE) BREAK
						CASE 18	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_0, FALSE) BREAK
						CASE 19	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_1, FALSE) BREAK
						CASE 20	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_2, FALSE) BREAK
						CASE 21	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_3, FALSE) BREAK
						CASE 22	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_4, FALSE) BREAK
						CASE 23	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_5, FALSE) BREAK
						CASE 24	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_6, FALSE) BREAK
						CASE 25	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_7, FALSE) BREAK
						CASE 26	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_8, FALSE) BREAK
						CASE 27	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_9, FALSE) BREAK
						CASE 28	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_10, FALSE) BREAK
						CASE 29	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_11, FALSE) BREAK
						CASE 30	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_12, FALSE) BREAK
						CASE 31	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_13, FALSE) BREAK
						CASE 32	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_14, FALSE) BREAK
						CASE 33	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_3PC_SUIT_15, FALSE) BREAK
					ENDSWITCH
				BREAK
				CASE CHAR_TREVOR
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 39)
						CASE 0	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE) BREAK
						CASE 1	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DOCK_WORKER, FALSE) BREAK
						CASE 2	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE) BREAK
						CASE 3	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, FALSE) BREAK
						CASE 4	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TENNIS, FALSE) BREAK
						CASE 5	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH, FALSE) BREAK
						CASE 6	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TRIATHLON, FALSE) BREAK
						CASE 7	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_UNDERWEAR, FALSE) BREAK
						CASE 8	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_SECURITY, FALSE) BREAK
						CASE 9	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE) BREAK
						CASE 10	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_PROLOGUE, FALSE) BREAK
						CASE 11	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TUXEDO, FALSE) BREAK
						CASE 12	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_LADIES, FALSE) BREAK
						CASE 13	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_BLUE_BOILER_SUIT, FALSE) BREAK
						CASE 14	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_1, FALSE) BREAK
						CASE 15	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_2, FALSE) BREAK
						CASE 16	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3, FALSE) BREAK
						CASE 17	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_HUNTING, FALSE) BREAK
						CASE 18	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_1, FALSE) BREAK
						CASE 19	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DENIM, FALSE) BREAK
						CASE 20	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_LUDENDORFF, FALSE) BREAK
						CASE 21	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_0, FALSE) BREAK
						CASE 22	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_1, FALSE) BREAK
						CASE 23	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_2, FALSE) BREAK
						CASE 24	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_3, FALSE) BREAK
						CASE 25	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_4, FALSE) BREAK
						CASE 26	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_5, FALSE) BREAK
						CASE 27	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_6, FALSE) BREAK
						CASE 28	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_7, FALSE) BREAK
						CASE 29	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_8, FALSE) BREAK
						CASE 30	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_CHEAPSUIT_9, FALSE) BREAK
						CASE 31	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_0, FALSE) BREAK
						CASE 32	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_1, FALSE) BREAK
						CASE 33	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_2, FALSE) BREAK
						CASE 34	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_3, FALSE) BREAK
						CASE 35	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_4, FALSE) BREAK
						CASE 36	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_5, FALSE) BREAK
						CASE 37	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_6, FALSE) BREAK
						CASE 38	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_2, FALSE) BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			bSetRandomoutfitOnStartup = FALSE
		ENDIF
		
		IF bTestTimelapseCutscene
			
			INT iMissionEnum = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(SP_MISSION_MAX)), iSavedMissionEnum
			structTimelapse sTimelapse
			structTimelapseSettings sTimelapseSettings

			BOOL bMF_DO_TRIGGER_TOD_M, bMF_DO_TRIGGER_TOD_F, bMF_DO_TRIGGER_TOD_T
			
			BOOL bTriggerTODRegardlessOfTimeForClothes = TRUE, bClearTheArea = TRUE
			BOOL bDoTimelapse
			
			TEXT_WIDGET_ID wTestWidget
			
			SET_CURRENT_WIDGET_GROUP(widgetID)
			WIDGET_GROUP_ID timelapseWidgetID = START_WIDGET_GROUP("bTestTimelapseCutscene")
				ADD_WIDGET_BOOL("bTestTimelapseCutscene", bTestTimelapseCutscene)
				
				ADD_WIDGET_INT_SLIDER("missionEnum", iMissionEnum, 0, ENUM_TO_INT(SP_MISSION_MAX)-1, 1)
				wTestWidget = ADD_TEXT_WIDGET("missionEnum name")
				SET_CONTENTS_OF_TEXT_WIDGET(wTestWidget, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionEnum)))
				
				ADD_WIDGET_BOOL("bMF_DO_TRIGGER_TOD_M", bMF_DO_TRIGGER_TOD_M)
				ADD_WIDGET_BOOL("bMF_DO_TRIGGER_TOD_F", bMF_DO_TRIGGER_TOD_F)
				ADD_WIDGET_BOOL("bMF_DO_TRIGGER_TOD_T", bMF_DO_TRIGGER_TOD_T)
				
				ADD_WIDGET_BOOL("bDoTimelapse", bDoTimelapse)
				
				START_WIDGET_GROUP("GET_TOD_CUTSCENE_ATTRIBUTES")
					ADD_WIDGET_VECTOR_SLIDER("camNode1Pos", sTimelapseSettings.vCamPos1 , -5000, 5000, 0.001)
					ADD_WIDGET_VECTOR_SLIDER("camNode1Rot", sTimelapseSettings.vCamRot1, -360, 360, 0.001)
					
					ADD_WIDGET_VECTOR_SLIDER("camNode2Pos", sTimelapseSettings.vCamPos2, -5000, 5000, 0.001)
					ADD_WIDGET_VECTOR_SLIDER("camNode2Rot", sTimelapseSettings.vCamRot2, -360, 360, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("fCamFov", sTimelapseSettings.fCamFOV, -0, 100, 0.1)
					
					ADD_WIDGET_INT_SLIDER("iSplineCamTime", sTimelapseSettings.iCamTime, 0, 15000, 0500)
					ADD_WIDGET_STRING("tWeatherType")	//, tWeatherType, -5000, 5000, 0.001)
					ADD_WIDGET_STRING("tCloudHat")	//, tCloudHat, -5000, 5000, 0.001)
					ADD_WIDGET_BOOL("bSkipToNightTime", sTimelapse.bSkipToNightTime)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("GET_TOD_WINDOW_TIME")
					ADD_WIDGET_INT_SLIDER("iTimeWindowStart", sTimelapse.iTimeWindowStart, -1, 25, 1)
					ADD_WIDGET_INT_SLIDER("iTimeWindowEnd", sTimelapse.iTimeWindowEnd, -1, 25, 1)
					ADD_WIDGET_BOOL("bTriggerTODRegardlessOfTimeForClothes", bTriggerTODRegardlessOfTimeForClothes)
					ADD_WIDGET_BOOL("bClearTheArea", bClearTheArea)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetID)
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			WHILE bTestTimelapseCutscene
				SET_CONTENTS_OF_TEXT_WIDGET(wTestWidget, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionEnum)))
				
				IF (iSavedMissionEnum <> iMissionEnum)
					
	//				CONST_INT   MF_INDEX_DO_TRIGGER_TOD_M   8   // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Michael.
	//				CONST_INT   MF_INDEX_DO_TRIGGER_TOD_F   9   // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Franklin.
	//				CONST_INT   MF_INDEX_DO_TRIGGER_TOD_T   10  // This mission will time of day skip to meet time restrictions before any lead-in scene is created for Trevor.
					
					bMF_DO_TRIGGER_TOD_M = IS_BIT_SET(g_sMissionStaticData[iMissionEnum].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_M)
					bMF_DO_TRIGGER_TOD_F = IS_BIT_SET(g_sMissionStaticData[iMissionEnum].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_F)
					bMF_DO_TRIGGER_TOD_T = IS_BIT_SET(g_sMissionStaticData[iMissionEnum].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_T)
					
					SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionEnum)
					GET_SP_MISSION_TOD_CUTSCENE_ATTRIBUTES(eMissionID, sTimelapseSettings)
					GET_SP_MISSION_TOD_WINDOW_TIME(eMissionID, sTimelapseSettings.iLapseStartHour, sTimelapseSettings.iLapseEndHour)

					sTimelapse.iTimeWindowStart = sTimelapseSettings.iLapseStartHour
					sTimelapse.iTimeWindowEnd = sTimelapseSettings.iLapseEndHour
					
					iSavedMissionEnum = iMissionEnum
				ENDIF
				
				IF INT_TO_ENUM(SP_MISSIONS, iMissionEnum) < SP_MISSION_MAX
					STATIC_BLIP_NAME_ENUM eBluip = g_sMissionStaticData[iMissionEnum].blip
					IF ENUM_TO_INT(eBluip) < g_iTotalStaticBlips
						VECTOR vBlipPosition = g_GameBlips[eBluip].vCoords[0]
						DRAW_DEBUG_SPHERE(vBlipPosition, 1.0, 000, 255, 128, 255)
					ENDIF
				ENDIF
				
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_SHIFT, "store camNode1")
					sTimelapseSettings.vCamPos1 = GET_CAM_COORD(GET_DEBUG_CAM())
					sTimelapseSettings.vCamRot1 = GET_CAM_ROT(GET_DEBUG_CAM())
					sTimelapseSettings.fCamFOV = GET_CAM_FOV(GET_DEBUG_CAM())
				ENDIF
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_CTRL, "store camNode2")
					sTimelapseSettings.vCamPos2 = GET_CAM_COORD(GET_DEBUG_CAM())
					sTimelapseSettings.vCamRot2 = GET_CAM_ROT(GET_DEBUG_CAM())
					sTimelapseSettings.fCamFOV = GET_CAM_FOV(GET_DEBUG_CAM())
				ENDIF
				
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_NONE, "save camNode1 & camNode2")
					SAVE_STRING_TO_DEBUG_FILE("iMissionEnum = ")SAVE_STRING_TO_DEBUG_FILE(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionEnum)))SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("camNode1Pos = ")SAVE_VECTOR_TO_DEBUG_FILE(sTimelapseSettings.vCamPos1)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("camNode1Rot = ")SAVE_VECTOR_TO_DEBUG_FILE(sTimelapseSettings.vCamRot1)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("camNode2Pos = ")SAVE_VECTOR_TO_DEBUG_FILE(sTimelapseSettings.vCamPos2)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("camNode2Rot = ")SAVE_VECTOR_TO_DEBUG_FILE(sTimelapseSettings.vCamRot2)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("fCamFov = ")SAVE_FLOAT_TO_DEBUG_FILE(sTimelapseSettings.fCamFOV)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF bDoTimelapse
					LOAD_SCENE(sTimelapseSettings.vCamPos1)
					
					VECTOR vMISSION_TOD_TRIGGER_POSITION
					
					IF INT_TO_ENUM(SP_MISSIONS, iMissionEnum) < SP_MISSION_MAX
						STATIC_BLIP_NAME_ENUM eBluip = g_sMissionStaticData[iMissionEnum].blip
						IF ENUM_TO_INT(eBluip) < g_iTotalStaticBlips
							vMISSION_TOD_TRIGGER_POSITION = g_GameBlips[eBluip].vCoords[0]
						ENDIF
					ENDIF
					
					IF ARE_VECTORS_EQUAL(vMISSION_TOD_TRIGGER_POSITION, <<0,0,0>>)
						vMISSION_TOD_TRIGGER_POSITION = GET_MISSION_TOD_END_POSITION(INT_TO_ENUM(SP_MISSIONS, iMissionEnum))
					ENDIF
					IF ARE_VECTORS_EQUAL(vMISSION_TOD_TRIGGER_POSITION, <<0,0,0>>)
						vMISSION_TOD_TRIGGER_POSITION = sTimelapseSettings.vCamPos1
					ENDIF
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vMISSION_TOD_TRIGGER_POSITION)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_MISSION_TOD_END_HEADING(INT_TO_ENUM(SP_MISSIONS, iMissionEnum)))
					
					// //
					WHILE bDoTimelapse

						CONST_INT iTIMELAPSE_0_request		0
						CONST_INT iTIMELAPSE_1_create		1
						CONST_INT iTIMELAPSE_22_run			22		//custom
						CONST_INT iTIMELAPSE_2_run			2
						CONST_INT iTIMELAPSE_3_hold			3
						CONST_INT iTIMELAPSE_4_end			4
						
						CONST_INT iTIMELAPSE_null			-1
						
						#IF IS_DEBUG_BUILD
						CONST_INT iDrawLiteralSceneStringRow	0
				//		TEXT_LABEL_63 str
						DrawLiteralSceneStringInt("iTimelapseCut:", sTimelapse.iTimelapseCut, iDrawLiteralSceneStringRow+0)
						#ENDIF
						
						CONST_FLOAT fCONST_timelapseCut		0.5
						
						// Playing mission normally
						SWITCH sTimelapse.iTimelapseCut

							CASE iTIMELAPSE_0_request
								
								CPRINTLN(DEBUG_SYSTEM, "<TOD> waiting on request AUDIO BANK \"TIME_LAPSE\".")
								
								IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
									sTimelapse.iTimelapseCut = iTIMELAPSE_1_create
								ENDIF
							BREAK
							
							CASE iTIMELAPSE_1_create
								IF bTriggerTODRegardlessOfTimeForClothes
									#IF IS_DEBUG_BUILD
										IF sTimelapse.bSkipToNightTime
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating FORCED NIGHT timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window end hour ", sTimelapse.iTimeWindowEnd, ".")
										ELSE
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating FORCED DAY timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window start hour ", sTimelapse.iTimeWindowStart, ".")
										ENDIF
									#ENDIF
									
									DESTROY_CAM(sTimelapse.splineCamera)
									sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
									
									ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, sTimelapseSettings.iCamTime)
							
									IF (sTimelapseSettings.iCustomBehaviour < 0)
										ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sTimelapseSettings.vCamPos2, sTimelapseSettings.vCamRot2, sTimelapseSettings.iCamTime)
									ELSE
										ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, ROUND(TO_FLOAT(sTimelapseSettings.iCamTime) * fCONST_timelapseCut))
									ENDIF
									
									SET_CAM_FOV(sTimelapse.splineCamera, sTimelapseSettings.fCamFOV)
									SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
									
									CLEAR_ROOM_FOR_GAME_VIEWPORT()
									CLEAR_AREA_OF_COPS(sTimelapseSettings.vCamPos1, GET_CAM_FAR_CLIP(sTimelapse.splineCamera))	//#1012871
									
									SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 0, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
									SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
									
									CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
									CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
									
									SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
										
									sTimelapse.iSplineStageSound = GET_SOUND_ID()
									PLAY_SOUND_FRONTEND(sTimelapse.iSplineStageSound, "TIME_LAPSE_MASTER")
									
									sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
									CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
								ELIF NOT IS_TIME_BETWEEN_THESE_HOURS(sTimelapse.iTimeWindowStart, sTimelapse.iTimeWindowEnd) //IS_DAYLIGHT_HOURS()
								 	IF sTimelapse.bSkipToNightTime
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating NIGHT timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window end hour ", sTimelapse.iTimeWindowEnd, ".")
									
										DESTROY_CAM(sTimelapse.splineCamera)
										sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
										
										ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, sTimelapseSettings.iCamTime)
										
										IF (sTimelapseSettings.iCustomBehaviour < 0)
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos2, sTimelapseSettings.vCamRot2, sTimelapseSettings.iCamTime)
										ELSE
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, ROUND(TO_FLOAT(sTimelapseSettings.iCamTime) * fCONST_timelapseCut))
										ENDIF
										
										SET_CAM_FOV(sTimelapse.splineCamera, sTimelapseSettings.fCamFOV)
										SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player as TOD camera deactivates.")
											SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
										ENDIF
										VEHICLE_INDEX vehPlayerLast
										vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
										IF DOES_ENTITY_EXIST(vehPlayerLast)
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player's last vehicle as TOD camera deactivates.")
											SET_ENTITY_VISIBLE(vehPlayerLast, FALSE)
										ENDIF
										
										CLEAR_ROOM_FOR_GAME_VIEWPORT()
										CLEAR_AREA_OF_COPS(sTimelapseSettings.vCamPos1, GET_CAM_FAR_CLIP(sTimelapse.splineCamera))	//#1012871
										
										SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 8, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
										SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
										
										CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
										CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
										
										SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
										
										sTimelapse.iSplineStageSound = GET_SOUND_ID()
										PLAY_SOUND_FRONTEND(sTimelapse.iSplineStageSound, "TIME_LAPSE_MASTER")
										
										sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
									ELSE
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Night timelapse not running as current hour ", GET_CLOCK_HOURS(), " is already in valid window [", sTimelapse.iTimeWindowEnd, "->", sTimelapse.iTimeWindowStart, "].")
										RELEASE_AMBIENT_AUDIO_BANK()
										bDoTimelapse = NOT TRUE
									ENDIF
								ELSE							
									IF NOT sTimelapse.bSkipToNightTime
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Creating DAY timelapse. Starting from current hour ", GET_CLOCK_HOURS(), ". Blending to window start hour ", sTimelapse.iTimeWindowStart, ".")
								
										DESTROY_CAM(sTimelapse.splineCamera)
										sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
										
										ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, sTimelapseSettings.iCamTime)
										
										IF (sTimelapseSettings.iCustomBehaviour < 0)
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos2, sTimelapseSettings.vCamRot2, sTimelapseSettings.iCamTime)
										ELSE
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, ROUND(TO_FLOAT(sTimelapseSettings.iCamTime) * fCONST_timelapseCut))
										ENDIF
										
										SET_CAM_FOV(sTimelapse.splineCamera, sTimelapseSettings.fCamFOV)
										SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
										SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player as TOD camera deactivates.")
											SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
										ENDIF
										VEHICLE_INDEX vehPlayerLast
										vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
										IF DOES_ENTITY_EXIST(vehPlayerLast)
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Hiding player's last vehicle as TOD camera deactivates.")
											SET_ENTITY_VISIBLE(vehPlayerLast, FALSE)
										ENDIF
										
										CLEAR_ROOM_FOR_GAME_VIEWPORT()
										CLEAR_AREA_OF_COPS(sTimelapseSettings.vCamPos1, GET_CAM_FAR_CLIP(sTimelapse.splineCamera))	//#1012871
										
										SET_TIMEOFDAY(sTimelapse.currentTimeOfDay, 0, 0, 0, GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
										SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
										
										CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
										CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
										
										SET_PLAYER_VEH_RADIO_OFF_FOR_TIMELAPSE()
										
										sTimelapse.iSplineStageSound = GET_SOUND_ID()
										PLAY_SOUND_FRONTEND(sTimelapse.iSplineStageSound, "TIME_LAPSE_MASTER")
										
										sTimelapse.iTimelapseCut = iTIMELAPSE_2_run
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse running...")
									ELSE
										CPRINTLN(DEBUG_SYSTEM, "<TOD> Day timelapse not running as current hour ", GET_CLOCK_HOURS(), " is already in valid window [", sTimelapse.iTimeWindowStart, "->", sTimelapse.iTimeWindowEnd, "].")
										RELEASE_AMBIENT_AUDIO_BANK()
										bDoTimelapse = NOT TRUE
									ENDIF
								ENDIF
							BREAK
						
							CASE iTIMELAPSE_2_run
								IF bClearTheArea
									CLEAR_AREA(sTimelapseSettings.vCamPos1, 300.0, TRUE, TRUE)
								ENDIF
								
								IF NOT sTimelapse.bSkipToNightTime
									IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(sTimelapse.iTimeWindowEnd, 0, sTimelapseSettings.tWeatherType, sTimelapseSettings.tCloudHat, sTimelapse)
										IF (sTimelapseSettings.iCustomBehaviour < 0)
											sTimelapse.iGameTimeHold = GET_GAME_TIMER()
											sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
										ELSE
											sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, sTimelapseSettings.iCamTime)
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos2, sTimelapseSettings.vCamRot2, ROUND(TO_FLOAT(sTimelapseSettings.iCamTime) * (1-fCONST_timelapseCut)))
											SET_CAM_FOV(sTimelapse.splineCamera, sTimelapseSettings.fCamFOV)
											SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
											SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
											
											sTimelapse.iGameTimeHold = GET_GAME_TIMER()
											sTimelapse.iTimelapseCut = iTIMELAPSE_22_run
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Custom timelapse running...")
										ENDIF
									ENDIF
								ELSE
									IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(sTimelapse.iTimeWindowStart, 0, sTimelapseSettings.tWeatherType, sTimelapseSettings.tCloudHat, sTimelapse)
										IF (sTimelapseSettings.iCustomBehaviour < 0)
											sTimelapse.iGameTimeHold = GET_GAME_TIMER()
											sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
										ELSE
											sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos1, sTimelapseSettings.vCamRot1, sTimelapseSettings.iCamTime)
											ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, sTimelapseSettings.vCamPos2, sTimelapseSettings.vCamRot2, ROUND(TO_FLOAT(sTimelapseSettings.iCamTime) * (1-fCONST_timelapseCut)))
											SET_CAM_FOV(sTimelapse.splineCamera, sTimelapseSettings.fCamFOV)
											SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
											SET_CAM_ACTIVE(sTimelapse.splineCamera,TRUE)
											
											sTimelapse.iGameTimeHold = GET_GAME_TIMER()
											sTimelapse.iTimelapseCut = iTIMELAPSE_22_run
											CPRINTLN(DEBUG_SYSTEM, "<TOD> Custom timelapse running...")
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
									CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse skipped by user input.")
									DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
									WHILE NOT IS_SCREEN_FADED_OUT()
										HIDE_HUD_AND_RADAR_THIS_FRAME()
										HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)		
										WAIT(0)
									ENDWHILE
									
									SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
									sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
								ENDIF
							BREAK
						
							CASE iTIMELAPSE_22_run
								IF (DOES_CAM_EXIST(sTimelapse.splineCamera)
								AND IS_CAM_INTERPOLATING(sTimelapse.splineCamera))
									//
								ELSE
									sTimelapse.iGameTimeHold = GET_GAME_TIMER()
									sTimelapse.iTimelapseCut = iTIMELAPSE_3_hold
									CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse holding...")
								ENDIF
								
								IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
									CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse skipped by user input.")
									DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
									WHILE NOT IS_SCREEN_FADED_OUT()
										HIDE_HUD_AND_RADAR_THIS_FRAME()
										HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)		
										WAIT(0)
									ENDWHILE
									
									SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
									sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
								ENDIF
							BREAK
							
							CASE iTIMELAPSE_3_hold
								#IF IS_DEBUG_BUILD
								DrawLiteralSceneStringInt("iTimelapseCut:", GET_GAME_TIMER()-sTimelapse.iTimelapseCut, iDrawLiteralSceneStringRow+1)
								#ENDIF
								
								IF (sTimelapse.iGameTimeHold+1000) > GET_GAME_TIMER()
								

	PRINTLN("near dof: ", GET_CAM_NEAR_DOF(sTimelapse.splineCamera))
	PRINTLN("far dof: ", GET_CAM_FAR_DOF(sTimelapse.splineCamera))
	PRINTLN("near clip: ", GET_CAM_NEAR_CLIP(sTimelapse.splineCamera))
	PRINTLN("far clip: ", GET_CAM_FAR_CLIP(sTimelapse.splineCamera))
	PRINTLN("dof strength: ", GET_CAM_DOF_STRENGTH(sTimelapse.splineCamera))
	PRINTNL()

								
									sTimelapse.iTimelapseCut = iTIMELAPSE_4_end
									CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse ending...")
								ENDIF
							BREAK
							
							CASE iTIMELAPSE_4_end
								CASCADE_SHADOWS_INIT_SESSION()
								
								STOP_SOUND(sTimelapse.iSplineStageSound)
								RELEASE_AMBIENT_AUDIO_BANK()
								
								sTimelapse.iTimelapseCut = iTIMELAPSE_null
								CPRINTLN(DEBUG_SYSTEM, "<TOD> Timelapse ended. Waiting for script to trigger cleanup.")
								bDoTimelapse = NOT TRUE
							BREAK
							
							CASE iTIMELAPSE_null
								bDoTimelapse = NOT TRUE
							BREAK
						ENDSWITCH
			
						WAIT(0)
					ENDWHILE
					
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					
					sTimelapse.iTimelapseCut = 0
					sTimelapse.iTimeSkipStage = 0
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			DELETE_WIDGET_GROUP(timelapseWidgetID)
		ENDIF
		
		IF bTestDefaultPlayerSwitchState
			
			STORE_DEFAULT_PLAYER_SWITCH_STATE(PLAYER_PED_ID())
			enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
			DEFAULT_PLAYER_SWITCH_STATE_STRUCT sDefaultPlayerSwitchState = g_sDefaultPlayerSwitchState[ePed]
			
			BOOL bStoreDefaultPlayerSwitchState
			
			
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			SET_CURRENT_WIDGET_GROUP(widgetID)
			WIDGET_GROUP_ID switchstateWidgetID = START_WIDGET_GROUP("bTestDefaultPlayerSwitchState")
				ADD_WIDGET_BOOL("bTestDefaultPlayerSwitchState", bTestDefaultPlayerSwitchState)
				ADD_WIDGET_BOOL("bStoreDefaultPlayerSwitchState", bStoreDefaultPlayerSwitchState)
				
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetID)
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			WHILE bTestDefaultPlayerSwitchState
				
				IF bStoreDefaultPlayerSwitchState
					vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
					
					STORE_DEFAULT_PLAYER_SWITCH_STATE(PLAYER_PED_ID())
					sDefaultPlayerSwitchState = g_sDefaultPlayerSwitchState[ePed]
					
					bStoreDefaultPlayerSwitchState = FALSE
				ENDIF
				
				//
				DrawDefaultPlayerSwitchState(ePed, "vVehicleCoord_a", sDefaultPlayerSwitchState.vVehicleCoord_a,	 vPlayerCoord,								sDefaultPlayerSwitchState.fVehicleHead_a,	sDefaultPlayerSwitchState.fVehicleSpeed_a,	0.5, HUD_COLOUR_BLUE, FALSE)
				DrawDefaultPlayerSwitchState(ePed, "vVehicleCoord_b", sDefaultPlayerSwitchState.vVehicleCoord_b,	sDefaultPlayerSwitchState.vVehicleCoord_a,	sDefaultPlayerSwitchState.fVehicleHead_b,	sDefaultPlayerSwitchState.fVehicleSpeed_b,	0.5, HUD_COLOUR_BLUE, FALSE)
				DrawDefaultPlayerSwitchState(ePed, "vVehicleCoord_c", sDefaultPlayerSwitchState.vVehicleCoord_c,	sDefaultPlayerSwitchState.vVehicleCoord_b,	sDefaultPlayerSwitchState.fVehicleHead_c,	sDefaultPlayerSwitchState.fVehicleSpeed_c,	0.5, HUD_COLOUR_BLUE, FALSE)
				
				//
				DrawDefaultPlayerSwitchState(ePed, "vWalkCoord_a", sDefaultPlayerSwitchState.vWalkCoord_a,	vPlayerCoord,							sDefaultPlayerSwitchState.fWalkHead_a, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				DrawDefaultPlayerSwitchState(ePed, "vWalkCoord_b", sDefaultPlayerSwitchState.vWalkCoord_b,	sDefaultPlayerSwitchState.vWalkCoord_a,	sDefaultPlayerSwitchState.fWalkHead_b, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				DrawDefaultPlayerSwitchState(ePed, "vWalkCoord_c", sDefaultPlayerSwitchState.vWalkCoord_c,	sDefaultPlayerSwitchState.vWalkCoord_b,	sDefaultPlayerSwitchState.fWalkHead_c, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				
				WAIT(0)
			ENDWHILE
			
			DELETE_WIDGET_GROUP(switchstateWidgetID)
		ENDIF
		
		IF bDrawSelectorShuit
			SELECTOR_SLOTS_ENUM eSelectorChar
			REPEAT NUM_OF_PLAYABLE_PEDS eSelectorChar
				
				INT iPedIDNative1 = NATIVE_TO_INT(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorChar])
				INT iPedIDNative2 = NATIVE_TO_INT(sSceneData.sSelectorPeds.pedID[eSelectorChar])
				
				TEXT_LABEL_63 paramText
				paramText  = "SELECTOR_"
				paramText += GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eSelectorChar))
				paramText += ": "
				paramText += iPedIDNative1
				paramText += "/"
				paramText += iPedIDNative2
				
				
				IF (g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorChar] <> NULL)
					STRING InstanceScript = ""
					INT InstanceId = 0
					InstanceScript = GET_ENTITY_SCRIPT(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorChar], InstanceId)
					
					paramText += ":"
					paramText += InstanceScript
				ENDIF
				
				// //
				FLOAT paramXPos = 0.205
				FLOAT paramYPos = 0.075 + (0.02 * ENUM_TO_INT(eSelectorChar))
				
				INT theR = 0, theG = 0, theB = 0, theA = 0
				IF (g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorChar] <> NULL)
					theR = 255	theG = 255	theB = 255
					theA = 255
				ELSE
					IF IS_PED_THE_CURRENT_PLAYER_PED(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eSelectorChar))
						theR = 128	theG = 128	theB = 255
					ELSE
						theR = 255	theG = 128	theB = 128
					ENDIF
					theA = 128
				ENDIF
				SET_TEXT_COLOUR(theR, theG, theB, theA)
				
				SET_TEXT_SCALE(0.25, 0.3)
				SET_TEXT_WRAP(0.0, 1.0)
				SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)
				// //
			ENDREPEAT
		ENDIF
	ENDPROC

#ENDIF


// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()

	//Unregister player peds from the automatic doors.
	PED_INDEX playerPed = PLAYER_PED_ID()
	UNREGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(GET_CURRENT_PLAYER_PED_ENUM(), playerPed)
	
	// Remove all references to the player vehicles
	INT i
	REPEAT NUM_PLAYER_VEHICLE_IDS i
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			PRINTLN("PLAYER VEHICLE ID GETTING SET TO NULL")
		ENDIF
		vLastPlayerVehicleCoords[i] = <<0,0,0>>
		bLastPlayerVehicleInGarage[i] = FALSE
		g_viCreatedPlayerVehicleIDs[i] = NULL
		g_eCreatedPlayerVehiclePed[i] = NO_CHARACTER
		g_eCreatedPlayerVehicleModel[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	
	TERMINATE_THIS_THREAD()
	
ENDPROC



///// PURPOSE: Checks if a request has been made to switch the player character
//PROC CHECK_PLAYER_PED_REQUEST()	
//	IF g_sPlayerPedRequest.eState = PR_STATE_PROCESSING
//		eStage = PC_STAGE_SWITCH
//	ENDIF
//ENDPROC


/// PURPOSE: Performs the player ped switch using the ped request data
//PROC MAINTAIN_PLAYER_PED_REQUEST()
//	//////////////////////////////////////////////////////////////////////////////////////////////////
//	/// Process the ped switch
//	SWITCH g_sPlayerPedRequest.eState
//		CASE PR_STATE_PROCESSING
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				SWITCH g_sPlayerPedRequest.eType
//				
//					//////////////////////////////////////////////////////////////////////////////////////////////////
//					/// Player has entered mission blip with the wrong character
//					///    	
//					CASE PR_TYPE_MISSION
//						SWITCH g_sPlayerPedRequest.iStage
//							CASE 0
//								g_sPlayerPedRequest.sSelectorPeds.bAmbient = TRUE
//								eSelectorPed = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(g_sPlayerPedRequest.ePed)
//								
//								// Make a dummy ped so we can run cam spline to them
//								IF CREATE_PLAYER_PED_ON_FOOT(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed], g_sPlayerPedRequest.ePed, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
//								
//									// Set invisible to avoid any popping issues
//									SET_ENTITY_VISIBLE(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed], FALSE)
//									
//									MAKE_SELECTOR_PED_SELECTION(g_sPlayerPedRequest.sSelectorPeds, eSelectorPed)
//									sSelectorCam.pedTo = g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed]
//									g_sPlayerPedRequest.iStage++
//								ENDIF
//							BREAK
//							
//							CASE 1
//								// Use a long spline so we can delete the current character and dont see the new character pop into place
//								IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sSelectorCam, 0.0, 0.0, SELECTOR_CAM_LONG_SPLINE)	// Returns FALSE when the camera spline is complete
//									IF sSelectorCam.bOKToSwitchPed
//										IF NOT sSelectorCam.bPedSwitched
//											IF TAKE_CONTROL_OF_SELECTOR_PED(g_sPlayerPedRequest.sSelectorPeds, FALSE)
//												sSelectorCam.bPedSwitched = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[g_sPlayerPedRequest.sSelectorPeds.ePreviousSelectorPed])
//										SET_PED_AS_NO_LONGER_NEEDED(g_sPlayerPedRequest.sSelectorPeds.pedID[g_sPlayerPedRequest.sSelectorPeds.ePreviousSelectorPed])
//									ENDIF
//									DISABLE_CELLPHONE(FALSE)
//									g_sPlayerPedRequest.eState = PR_STATE_COMPLETE
//								ENDIF
//							BREAK
//						ENDSWITCH
//					BREAK
//					
//					//////////////////////////////////////////////////////////////////////////////////////////////////
//					///	Player has selected character from ambient hotswap HUD
//					///    	
//					CASE PR_TYPE_AMBIENT
//					
//						SWITCH g_sPlayerPedRequest.iStage
//							CASE 0
//								// Fix for bug #107444 - Add a communication delay so the current character
//								// cannot contact you straight after you switch to the selected character.
//								enumCharacterList eCurrentPed
//								eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
//								IF eCurrentPed <> NO_CHARACTER
//									ADD_COMMUNICATION_DELAY_FOR_CHARACTER(eCurrentPed)
//								ENDIF
//								
//								// Set the global communication delay to a short timer when switching
//								ADD_GLOBAL_COMMUNICATION_DELAY(6000)
//								
//								// Setup the scene data
//								sSceneData.iStage = 0
//								sSceneData.eScene = PR_SCENE_INVALID
//								sSceneData.ePed = g_sPlayerPedRequest.ePed
//								
//								FLOAT fScenePercent
//								
//								PED_SCENE_STRUCT sPedScene
//								PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
//								
//								IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_SWITCH)
//								AND GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(sSceneData.ePed, sSceneData.eScene, fScenePercent, sPedScene, sPassedScene)
//									
//									#IF IS_DEBUG_BUILD
//									PRINTSTRING("<player_controller> GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME[\"")
//									PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(sSceneData.eScene))
//									PRINTSTRING("\"] fScenePercent:")
//									PRINTFLOAT(fScenePercent)
//									PRINTSTRING("%")
//									PRINTNL()
//									#ENDIF
//									
//									g_sPlayerPedRequest.iStage++
//								ELSE
//									// Unable to get a suitable scene so bail out
//									g_sPlayerPedRequest.eState = PR_STATE_COMPLETE
//								ENDIF
//							BREAK
//							CASE 1
//								
//								IF PROCESS_PLAYER_PED_SCENE(sSceneData)
//									g_sPlayerPedRequest.eState = PR_STATE_COMPLETE
//								ENDIF
//							BREAK
//						ENDSWITCH
//					BREAK
//				ENDSWITCH
//			ENDIF
//		BREAK
//		
//		// Process complete so finish up
//		CASE PR_STATE_COMPLETE
//			
//			//Update the player's air vehicle parachute state after switching based on flowflag. #839147
//			IF IS_PLAYER_PLAYING(PLAYER_ID())
//				SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(PLAYER_ID(), GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_AIR_VEHICLE_PARACHUTE_UNLOCKED))
//			ENDIF
//			IF IS_PLAYER_PLAYING(PLAYER_ID())
//				SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(), GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_WATER_VEHICLE_SCUBA_GEAR_UNLOCKED))
//			ENDIF
//		
//			eStage = PC_STAGE_WAIT
//		BREAK
//	ENDSWITCH
//	
//ENDPROC

/// PURPOSE: Checks to see if the player controller should be re-initialised
PROC CHECK_CONTROLLER_RESET()
	// Jump back to the initialisation stage
	IF NOT HAS_DEFAULT_INFO_BEEN_SET()
		eStage = PC_STAGE_INIT
		EXIT
	ENDIF
ENDPROC

PROC ProcessAmbientSelectorPedQueue()
	IF (g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue] = PLAYER_PED_ID())
		g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue] = NULL
	ENDIF

	IF NOT IS_ENTITY_DEAD(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
		#IF IS_DEBUG_BUILD
		enumCharacterList eCharList = GET_PLAYER_PED_ENUM(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
		#ENDIF
		
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
			
			IF IS_ENTITY_ON_SCREEN(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
				IF NOT IS_ENTITY_DEAD(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
					IF (GET_SCRIPT_TASK_STATUS(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
					AND (GET_SCRIPT_TASK_STATUS(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], SCRIPT_TASK_SMART_FLEE_POINT) <> PERFORMING_TASK)
						TASK_WANDER_STANDARD(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "<player_controller.DeleteQueuedPlayerChars> task player ped \"", GET_PLAYER_PED_STRING(eCharList), "\" to wander...")
						#ENDIF
						
					ENDIF
				ENDIF
			ELSE
				
				DELETE_PED(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_controller.DeleteQueuedPlayerChars> delete player ped \"", GET_PLAYER_PED_STRING(eCharList), "\"")
				#ENDIF
			ENDIF
			
		ELSE
			STRING InstanceScript = ""
			INT InstanceId = 0
			InstanceScript = GET_ENTITY_SCRIPT(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], InstanceId)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(InstanceScript)
				IF ARE_STRINGS_EQUAL(InstanceScript, "spy_vehicle_system")
					EXIT
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_controller.DeleteQueuedPlayerChars> doesnt belong to this script \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue])), "\", belongs to \"", InstanceScript, "\" [", InstanceId, "]")
			#ENDIF
			
			IF IS_STRING_NULL_OR_EMPTY(InstanceScript)
				SET_ENTITY_AS_MISSION_ENTITY(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], TRUE, TRUE)
				EXIT
			ENDIF
			
			IF ARE_STRINGS_EQUAL(InstanceScript, "player_controller_b")
				SET_ENTITY_AS_MISSION_ENTITY(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], TRUE, TRUE)
				EXIT
			ENDIF
			
			IF ARE_STRINGS_EQUAL(InstanceScript, "friends_controller")
				SET_ENTITY_AS_MISSION_ENTITY(g_ambientSelectorPedDeleteQueue[iProcessAmbientQueue], TRUE, TRUE)
				EXIT
			ENDIF
			
	//		SET_ENTITY_AS_MISSION_ENTITY(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE, TRUE)
	//		CASSERTLN(DEBUG_SWITCH, "SET_ENTITY_AS_MISSION_ENTITY!!!")
			
		ENDIF
	ENDIF
	
	iProcessAmbientQueue++
	IF iProcessAmbientQueue >= COUNT_OF(g_ambientSelectorPedDeleteQueue)
		iProcessAmbientQueue = 0
	ENDIF
ENDPROC

FUNC BOOL CheckForPlayerInDestroyedVehicle(enumCharacterList eCharList)
	IF (g_sPlayerLastVeh[eCharList].model = DUMMY_MODEL_FOR_SCRIPT)
		CPRINTLN(DEBUG_SWITCH, "CheckForPlayerInDestroyedVehicle(", GET_PLAYER_PED_STRING(eCharList), "): model: dummy")
		
		RETURN FALSE
	ENDIF
	
	IF (g_sPlayerLastVeh[eCharList].fHealth > 0)
		CPRINTLN(DEBUG_SWITCH, "CheckForPlayerInDestroyedVehicle(", GET_PLAYER_PED_STRING(eCharList), "): health: ", g_sPlayerLastVeh[eCharList].fHealth)
		
		RETURN FALSE
	ENDIF
	
	IF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eCharList] = PR_SCENE_DEAD
		CPRINTLN(DEBUG_SWITCH, "CheckForPlayerInDestroyedVehicle(", GET_PLAYER_PED_STRING(eCharList), "): already dead")
		
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_SWITCH, "CheckForPlayerInDestroyedVehicle(", GET_PLAYER_PED_STRING(eCharList), "): vehicle \"", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[eCharList].model), "\" destroyed, player is dead")
	CERRORLN(DEBUG_SWITCH, "CheckForPlayerInDestroyedVehicle(", GET_PLAYER_PED_STRING(eCharList), "): vehicle \"", GET_MODEL_NAME_FOR_DEBUG(g_sPlayerLastVeh[eCharList].model), "\" destroyed, player is dead")
	
	g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eCharList] = PR_SCENE_DEAD
	g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList] = GET_CURRENT_TIMEOFDAY()
	
	g_sPlayerLastVeh[eCharList].model = DUMMY_MODEL_FOR_SCRIPT
	g_vPlayerVeh[eCharList] = NULL
	
	RETURN TRUE
ENDFUNC

PROC CreateAmbientPlayerChars()

	enumCharacterList eCharList = GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eAmbientPlayerSelectorChar)
	
	IF NOT IS_PED_THE_CURRENT_PLAYER_PED(eCharList)
	
		IF (sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] <> g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])

			IF DOES_ENTITY_EXIST(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				IF NOT DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					//#1308553
					g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar]
					
					CPRINTLN(DEBUG_SWITCH, "update backwards \"", GET_PLAYER_PED_STRING(eCharList), "\"")
				ENDIF
			ENDIF
			sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar]
			
			IF NOT IS_ENTITY_DEAD(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				SAFE_AMBIENT_STORE_PLAYER_PED_INFO(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList] = GET_CURRENT_TIMEOFDAY()
				IF IS_PED_IN_ANY_VEHICLE(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					VEHICLE_INDEX selectorChar_veh = GET_VEHICLE_PED_IS_IN(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					
					IF DOES_ENTITY_EXIST(selectorChar_veh)
						
						
						SET_ENTITY_AS_MISSION_ENTITY(selectorChar_veh, TRUE, TRUE)
						
						STORE_VEH_DATA_FROM_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar],
								g_sPlayerLastVeh[eCharList],
								g_vPlayerLastVehCoord[eCharList],
								g_fPlayerLastVehHead[eCharList],
								g_ePlayerLastVehState[eCharList],
								g_ePlayerLastVehGen[eCharList])
						CheckForPlayerInDestroyedVehicle(eCharList)
						
						SET_VEHICLE_AS_NO_LONGER_NEEDED(selectorChar_veh)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> store player b \"", GET_PLAYER_PED_STRING(eCharList), "\"")	//CASSERTLN(DEBUG_SWITCH, "test one - store player b")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(eCharList)
			IF g_sPlayerLastVeh[eCharList].model <> DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(g_sPlayerLastVeh[eCharList].model)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			
			IF NOT DOES_ENTITY_EXIST(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> update sSceneData.sSelectorPeds.pedID[\"", GET_PLAYER_PED_STRING(eCharList), "\"] ", "model:", "NULL")
				
				/* */
				CPRINTLN(DEBUG_SWITCH, "	g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[\"", GET_PLAYER_PED_STRING(eCharList), "\"]: ", g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[eCharList])
				CPRINTLN(DEBUG_SWITCH, "	g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[\"", GET_PLAYER_PED_STRING(eCharList), "\"]: ", Get_String_From_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList]))
				/* */
				
			ELSE
				CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> update sSceneData.sSelectorPeds.pedID[\"", GET_PLAYER_PED_STRING(eCharList), "\"] ", "model:", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])))
			ENDIF
			
			
			DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3))
			DrawLiteralSceneString("<> ", 9+(ENUM_TO_INT(eCharList)*3))
			#ENDIF
			
			EXIT
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3), HUD_COLOUR_GREYLIGHT, 0.25)
			DrawLiteralSceneString("NOT DOES_ENTITY_EXIST", 9+(ENUM_TO_INT(eCharList)*3), HUD_COLOUR_GREYLIGHT, 0.25)
			#ENDIF
			
			//
			enumCreateState eCreateState = eCS_MAX
			IF NOT SafeToCreatePlayerAmbientPedAtLastKnownLocation(eCharList, eCreateState)
				IF (g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] <> NULL)
					g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = NULL
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "nonexistant peds global pedID[", GET_PLAYER_PED_STRING(eCharList), "] should be NULL")
					#ENDIF
				ENDIF
				
			ELSE
				PED_INDEX playerLastKnownPedID = NULL
				IF CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATION(playerLastKnownPedID,
						eCharList,
						g_sPlayerLastVeh[eCharList],
						TRUE)
					g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = playerLastKnownPedID
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> create player ped \"", GET_PLAYER_PED_STRING(eCharList), "\" at last coord...")
					#ENDIF
					
					PRIVATE_SetDefaultPlayerPedAttributes(g_sPlayerPedRequest.sSelectorPeds)
				ENDIF
			ENDIF
			
		ELSE
			//Is this ambient player chars wait timer at 0?
			INT iGameTime = GET_GAME_TIMER()
			IF g_iCharWaitTime[eCharList] < iGameTime+1000
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> incrament global communication delay \"", GET_PLAYER_PED_STRING(eCharList), "\"")
				#ENDIF
				
				ADD_COMMUNICATION_DELAY_FOR_CHARACTER(eCharList)
			ENDIF
			
			IF IS_ENTITY_A_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
			
				IF NOT IS_ENTITY_DEAD(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					
					enumCreateState eDeleteState = eCS_MAX
					IF SafeToDeletePlayerAmbientPed(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], eCharList, eAmbientPlayerSelectorChar, eDeleteState)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3))
						DrawLiteralSceneString("safe to delete", 9+(ENUM_TO_INT(eCharList)*3))
						#ENDIF
						
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							
							SAFE_AMBIENT_STORE_PLAYER_PED_INFO(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList] = GET_CURRENT_TIMEOFDAY()
							
							VEHICLE_INDEX selectorChar_veh
							
							IF IS_PED_IN_ANY_VEHICLE(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
								selectorChar_veh = GET_VEHICLE_PED_IS_IN(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							ELIF DOES_ENTITY_EXIST(g_vPlayerVeh[eCharList])
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_vPlayerVeh[eCharList])
									selectorChar_veh = g_vPlayerVeh[eCharList]
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(selectorChar_veh)
								SET_ENTITY_AS_MISSION_ENTITY(selectorChar_veh, TRUE, TRUE)
								
								STORE_VEH_DATA_FROM_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar],
										g_sPlayerLastVeh[eCharList],
										g_vPlayerLastVehCoord[eCharList],
										g_fPlayerLastVehHead[eCharList],
										g_ePlayerLastVehState[eCharList],
										g_ePlayerLastVehGen[eCharList])
								
								CheckForPlayerInDestroyedVehicle(eCharList)
								
								SET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], GET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar]))
								DELETE_VEHICLE(selectorChar_veh)
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> delete player vehicle \"", GET_PLAYER_PED_STRING(eCharList), "\"")
								#ENDIF
							ENDIF
							
							DELETE_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
								CERRORLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> global g_sPlayerPedRequest existed after deleting sSceneData??")
								DELETE_PED(g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							ENDIF
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> safe to delete player ped \"", GET_PLAYER_PED_STRING(eCharList), "\"")
							#ENDIF
						ELSE
							STRING InstanceScript = ""
							INT InstanceId = 0
							InstanceScript = GET_ENTITY_SCRIPT(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], InstanceId)
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> doesnt belong to this script \"", GET_PLAYER_PED_STRING(eCharList), "\", belongs to \"", InstanceScript, "\" [", InstanceId, "]")
							#ENDIF
							
							IF IS_STRING_NULL_OR_EMPTY(InstanceScript)
								SET_ENTITY_AS_MISSION_ENTITY(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE, TRUE)
								EXIT
							ENDIF
							
							IF ARE_STRINGS_EQUAL(InstanceScript, "player_controller_b")
								SET_ENTITY_AS_MISSION_ENTITY(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE, TRUE)
								EXIT
							ENDIF
							
							IF ARE_STRINGS_EQUAL(InstanceScript, "friends_controller")
								EXIT
							ENDIF

							IF ARE_STRINGS_EQUAL(InstanceScript, "FriendActivity")
							OR ARE_STRINGS_EQUAL(InstanceScript, "family_scene_t0")
								EXIT
							ENDIF
							
							IF ARE_STRINGS_EQUAL(InstanceScript, "mission_triggerer_a")
							OR ARE_STRINGS_EQUAL(InstanceScript, "mission_triggerer_b")
							OR ARE_STRINGS_EQUAL(InstanceScript, "mission_triggerer_c")
							OR ARE_STRINGS_EQUAL(InstanceScript, "mission_triggerer_d")
								EXIT
							ENDIF
							
							IF ARE_STRINGS_EQUAL(InstanceScript, "selector_example")
								EXIT
							ENDIF
							
						//	SET_ENTITY_AS_MISSION_ENTITY(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE, TRUE)
							CERRORLN(DEBUG_SWITCH, "<CreateAmbientPlayerChars> DON'T set ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])), " as mission entity[InstanceScript: \"", InstanceScript, "\"]!!!")		//#1583178
							
						ENDIF
					
					ELSE
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 tStr1, tStr2
						
						tStr1  = ("not safe to delete \"")
						tStr1 += (GET_PLAYER_PED_STRING(eCharList))
						tStr1 += ("\" ")
						
						tStr2 = ""
						SWITCH eDeleteState
							CASE eDS_1_PLAYER_NOT_PLAYING
								tStr2 += ("1_PLAYER_NOT_PLAYING")
							BREAK
							CASE eDS_2_PLAYER_LAST_SCENE_DEAD
								tStr2 += ("2_PLAYER_LAST_SCENE_DEAD")
							BREAK
							CASE eDS_3_CURRENT_SELECTOR_PED
								tStr2 += ("3_CURRENT_SELECTOR_PED")
							BREAK
							CASE eDS_4_NEW_SELECTOR_PED
								tStr2 += ("4_NEW_SELECTOR_PED")
							BREAK
							CASE eDS_5_SWITCH_IN_PROGRESS
								tStr2 += ("5_SWITCH_IN_PROGRESS")
							BREAK
							CASE eDS_6_TOO_CLOSE_TO_DELETE
								tStr2 += ("6 too close to delete[")
								
								VECTOR vFriendCharLastKnown, vPlayerCharCurrent
								vFriendCharLastKnown	= GET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], FALSE)
								vPlayerCharCurrent		= GET_ENTITY_COORDS(PLAYER_PED_ID())
								
								tStr2 += ROUND(VDIST(vFriendCharLastKnown, vPlayerCharCurrent))
								tStr2 += ("/")
								tStr2 += ROUND(fCONST_FRIEND_TOO_CLOSE_TO_DELETE)
								tStr2 += ("m]")
								
								DrawDebugSceneSphere(vFriendCharLastKnown, fCONST_FRIEND_TOO_CLOSE_TO_DELETE, HUD_COLOUR_PURE_WHITE, 0.15)
							BREAK
							CASE eDS_7_ON_SCREEN
								tStr2 += ("7_ON_SCREEN")
							BREAK
//							CASE eDS_8_SWITCH_TYPE_MISSION
//								tStr2 += ("8_SWITCH_TYPE_MISSION")
//							BREAK
							CASE eDS_9_SWITCH_STATE_PROCESSING
								tStr2 += ("9_SWITCH_STATE_PROCESSING")
							BREAK
							CASE eDS_10_LEADIN
								tStr2 += ("10_LEADIN")
							BREAK
							CASE eDS_11_DIRECTOR_MODE
								tStr2 += ("xx_DIRECTOR_MODE")
							BREAK
							
							DEFAULT
								tStr2 += ("")
								tStr2 += (ENUM_TO_INT(eDeleteState))
								tStr2 += ("_unknown")
							BREAK
						ENDSWITCH
						
						DrawLiteralSceneString(tStr1, 7+(ENUM_TO_INT(eCharList)*3))
						DrawLiteralSceneString(tStr2, 9+(ENUM_TO_INT(eCharList)*3))
						#ENDIF
						
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							STORE_VEH_DATA_FROM_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar],
									g_sPlayerLastVeh[eCharList],
									g_vPlayerLastVehCoord[eCharList],
									g_fPlayerLastVehHead[eCharList],
									g_ePlayerLastVehState[eCharList],
									g_ePlayerLastVehGen[eCharList])
							CheckForPlayerInDestroyedVehicle(eCharList)
						ENDIF
						
						IF (sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] <> PLAYER_PED_ID())
							IF IS_PED_FALLING(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
							AND eDeleteState != eDS_7_ON_SCREEN
							AND eDeleteState != eDS_6_TOO_CLOSE_TO_DELETE
								SET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[eAmbientPlayerSelectorChar])
								SET_ENTITY_HEADING(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[eAmbientPlayerSelectorChar])
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE)
								
								CPRINTLN(DEBUG_SWITCH, "reset ", GET_PLAYER_PED_STRING(eCharList), " coords for collision")
								
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3))
					DrawLiteralSceneString("IS_ENTITY_DEAD", 9+(ENUM_TO_INT(eCharList)*3))
					#ENDIF
					
					IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eCharList] <> PR_SCENE_DEAD)
					
						g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eCharList] = PR_SCENE_DEAD
						SAFE_AMBIENT_STORE_PLAYER_PED_INFO(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
						g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList] = GET_CURRENT_TIMEOFDAY()
						
						SET_ENTITY_AS_MISSION_ENTITY(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], TRUE, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
						
						sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = NULL
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "set g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[", GET_PLAYER_PED_STRING(eCharList), "] to PR_SCENE_DEAD")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3))
				DrawLiteralSceneString("NOT IS_ENTITY_A_PED", 9+(ENUM_TO_INT(eCharList)*3))
				
				CPRINTLN(DEBUG_SWITCH, "sSceneData.sSelectorPeds.pedID[", GET_PLAYER_PED_STRING(eCharList), "] isn't a PED??? \"", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])), "\"")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString(GET_PLAYER_PED_STRING(eCharList), 7+(ENUM_TO_INT(eCharList)*3), HUD_COLOUR_BLUELIGHT, 0.25)
		DrawLiteralSceneString("IS_PED_THE_CURRENT_PLAYER_PED", 9+(ENUM_TO_INT(eCharList)*3), HUD_COLOUR_BLUELIGHT, 0.25)
		#ENDIF
		
		IF (g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] <> NULL)
			g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] = NULL
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "players global pedID[", GET_PLAYER_PED_STRING(eCharList), "] should be NULL")
			#ENDIF
		ENDIF
	ENDIF
	
	INT iAmbientPlayerSelectorChar = ENUM_TO_INT(eAmbientPlayerSelectorChar)
	iAmbientPlayerSelectorChar++
	IF iAmbientPlayerSelectorChar >= ENUM_TO_INT(NUM_OF_PLAYABLE_PEDS)
		iAmbientPlayerSelectorChar = 0
	ENDIF
	eAmbientPlayerSelectorChar = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, iAmbientPlayerSelectorChar)
ENDPROC
PROC DeleteAmbientPlayerChars()
	
	enumCharacterList eCharList = GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eAmbientPlayerSelectorChar)
	
	IF NOT IS_ENTITY_DEAD(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
//			AND (sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar] <> FRIEND_A_PED_ID())
			
			IF IS_ENTITY_ON_SCREEN(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				IF (GET_SCRIPT_TASK_STATUS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
					TASK_WANDER_STANDARD(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "<DeleteAmbientPlayerChars> task player ped \"", GET_PLAYER_PED_STRING(eCharList), "\" to wander...")
					#ENDIF
					
				ENDIF
			ELSE
				SAFE_AMBIENT_STORE_PLAYER_PED_INFO(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[eCharList] = GET_CURRENT_TIMEOFDAY()
				 
				IF IS_PED_IN_ANY_VEHICLE(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					VEHICLE_INDEX selectorChar_veh = GET_VEHICLE_PED_IS_IN(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					
					IF DOES_ENTITY_EXIST(selectorChar_veh)
						
						
						SET_ENTITY_AS_MISSION_ENTITY(selectorChar_veh, TRUE, TRUE)
						
						STORE_VEH_DATA_FROM_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar],
								g_sPlayerLastVeh[eCharList],
								g_vPlayerLastVehCoord[eCharList],
								g_fPlayerLastVehHead[eCharList],
								g_ePlayerLastVehState[eCharList],
								g_ePlayerLastVehGen[eCharList])
						
						SET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar], GET_ENTITY_COORDS(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar]))
						DELETE_VEHICLE(selectorChar_veh)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "<DeleteAmbientPlayerChars> delete player vehicle \"", GET_PLAYER_PED_STRING(eCharList), "\"")
						#ENDIF
					ENDIF
				ENDIF
				
				DELETE_PED(sSceneData.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				
				 IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
					CERRORLN(DEBUG_SWITCH, "<DeleteAmbientPlayerChars> global g_sPlayerPedRequest existed after deleting sSceneData??")
					DELETE_PED(g_sPlayerPedRequest.sSelectorPeds.pedID[eAmbientPlayerSelectorChar])
				ENDIF

				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<DeleteAmbientPlayerChars> delete player ped \"", GET_PLAYER_PED_STRING(eCharList), "\"")
				#ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	INT iAmbientPlayerSelectorChar = ENUM_TO_INT(eAmbientPlayerSelectorChar)
	iAmbientPlayerSelectorChar++
	IF iAmbientPlayerSelectorChar >= ENUM_TO_INT(NUM_OF_PLAYABLE_PEDS)
		iAmbientPlayerSelectorChar = 0
	ENDIF
	eAmbientPlayerSelectorChar = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, iAmbientPlayerSelectorChar)
ENDPROC

/// PURPOSE: Checks to see if any of the player chars have been killed. If they have block switching to them for 6 hours
PROC MONITOR_AMBIENT_PLAYER_CHARS()
	
	ProcessAmbientSelectorPedQueue()
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	// don't moniter ambient player characters if a mission is ongoing...
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) //NB. Isn't considered on mission while an RE or switch scene is playing.
	OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		DeleteAmbientPlayerChars()
		EXIT
	ENDIF
	
	// don't monitor ambient player characters if in multiplayer...
	IF g_bInMultiplayer
		DeleteAmbientPlayerChars()
		EXIT
	ENDIF
	
	CreateAmbientPlayerChars()
	
ENDPROC

/// PURPOSE: Tracks the players last vehicle saved in a garage.
PROC MONITOR_STORED_PLAYER_VEHICLES()

	INT i, j, k
	TEXT_LABEL_31 tlGarageName
	VEHICLE_INDEX nearbyVehs[5]
	SAVEHOUSE_NAME_ENUM eSavehouse, eClosestSavehouse
	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	BOOL bResetSpawnCoords
	BOOL bInGarage
	INT iGarage
				
	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		EXIT
	ENDIF
	
	//////////////////////////////////////////////////////
	///     PLAYER GARAGE VEHICLES
	///     
	IF g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPending
	AND g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderReceivedOnBoot
	AND NOT g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderForPlayerVehicle
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		i = 0
		eClosestSavehouse = g_savedGlobals.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed]
		WHILE GET_PLAYER_GARAGE_DATA(ePed, i, tlGarageName, eSavehouse)
			IF eSavehouse = eClosestSavehouse
				j = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
				REPEAT j k
					IF DOES_ENTITY_EXIST(nearbyVehs[k])
					AND IS_VEHICLE_DRIVEABLE(nearbyVehs[k])
					AND IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, nearbyVehs[k])
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(nearbyVehs[k])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(nearbyVehs[k])
					AND g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel = GET_ENTITY_MODEL(nearbyVehs[k])
						//IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP1", TXTMSG_LOCKED)
						
							PRINTLN("\nMONITOR_STORED_PLAYER_VEHICLES - Processing car app order immediately on garage vehicle!")
							
							IF NOT g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor
								eAutoOrderModel = g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel
								bSendCarAppAutoProcessEmail = TRUE
								bCarAppOrderFree = (NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed] AND ePed = CHAR_FRANKLIN )
							
								IF NOT bCarAppOrderFree
									DEBIT_BANK_ACCOUNT(ePed, BAAC_CARMOD_SHOP_01_AP, g_savedGlobals.sSocialData.sCarAppOrder[ePed].iCost)
								ELSE
									PRINTLN("\nMONITOR_STORED_PLAYER_VEHICLES - App order is on the house!")
								ENDIF
								
								g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor = TRUE
							ENDIF
							
							SET_VEHICLE_MODS_FROM_CARAPP_ORDER(nearbyVehs[k], g_savedGlobals.sSocialData.sCarAppOrder[ePed])
							SET_CAR_APP_ORDER_HAS_BEEN_PROCESSED(g_savedGlobals.sSocialData.sCarAppOrder[ePed], ePed)
							
							#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
								ADD_NUMBER_PLATE_TEXT_TO_PLAYERS_SC_ACCOUNT(g_savedGlobals.sSocialData.sCarAppOrder[ePed].tlPlateText_pending)
							#ENDIF
							
							// Re-apply the mod colour so the app knows which group to work with...
							INT iCurrentColourGroupP = -1
							INT iCurrentColourGroupS = -1
							GET_VEHICLE_COLOURS(nearbyVehs[k], iTempCol1, iTempCol2)
							GET_VEHICLE_EXTRA_COLOURS(nearbyVehs[k], iTempCol3, iTempCol4)
							SWITCH g_savedGlobals.sSocialData.sCarAppOrder[ePed].iColour1Group
								CASE 0 iCurrentColourGroupP = ENUM_TO_INT(MCT_METALLIC) BREAK
								CASE 1 iCurrentColourGroupP = ENUM_TO_INT(MCT_CLASSIC) BREAK
								CASE 2 iCurrentColourGroupP = ENUM_TO_INT(MCT_MATTE) BREAK
								CASE 3 iCurrentColourGroupP = ENUM_TO_INT(MCT_METALS) BREAK
								CASE 4 iCurrentColourGroupP = ENUM_TO_INT(MCT_CHROME) BREAK
							ENDSWITCH
							SWITCH g_savedGlobals.sSocialData.sCarAppOrder[ePed].iColour2Group
								CASE 0 iCurrentColourGroupS = ENUM_TO_INT(MCT_METALLIC) BREAK
								CASE 1 iCurrentColourGroupS = ENUM_TO_INT(MCT_CLASSIC) BREAK
								CASE 2 iCurrentColourGroupS = ENUM_TO_INT(MCT_MATTE) BREAK
								CASE 3 iCurrentColourGroupS = ENUM_TO_INT(MCT_METALS) BREAK
								CASE 4 iCurrentColourGroupS = ENUM_TO_INT(MCT_CHROME) BREAK
							ENDSWITCH
							IF iCurrentColourGroupP != ENUM_TO_INT(MCT_NONE)
							AND iCurrentColourGroupP != ENUM_TO_INT(MCT_PEARLESCENT)
								SET_VEHICLE_MOD_COLOR_1(nearbyVehs[k], INT_TO_ENUM(MOD_COLOR_TYPE, iCurrentColourGroupP), 0, 0)
							ENDIF
							IF iCurrentColourGroupS != ENUM_TO_INT(MCT_NONE)
							AND iCurrentColourGroupS != ENUM_TO_INT(MCT_PEARLESCENT)
								SET_VEHICLE_MOD_COLOR_2(nearbyVehs[k], INT_TO_ENUM(MOD_COLOR_TYPE, iCurrentColourGroupS), 0)
							ENDIF
							SET_VEHICLE_COLOURS(nearbyVehs[k], iTempCol1, iTempCol2)
							SET_VEHICLE_EXTRA_COLOURS(nearbyVehs[k], iTempCol3, iTempCol4)
							
							//VEHICLE_GEN_NAME_ENUM eVehicleGen
							
							IF IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(nearbyVehs[k])
							
								INT iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
								IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(nearbyVehs[k]))
									iVehicleSlot = SAVED_VEHICLE_SLOT_BIKE
								ENDIF
								
								UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, nearbyVehs[k], iVehicleSlot, TRUE)
							
							//ELIF IS_VEHICLE_STORED_VEHGEN_GARAGE_VEHICLE(nearbyVehs[k], eVehicleGen)
								// Update the stored garage vehicle
								//VEHICLE_SETUP_STRUCT sDefaultVehState
								//GET_VEHICLE_SETUP(nearbyVehs[k], sDefaultVehState)
								//UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eVehicleGen, sDefaultVehState, <<0,0,0>>, -1.0)
							ELSE
								// Update the last modded vehicle instead
								UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, nearbyVehs[k], SAVED_VEHICLE_SLOT_MODDED, TRUE)
							ENDIF
						//ENDIF
						k=100 //Bail
					ENDIF
				ENDREPEAT
				i=100 //Bail
			ENDIF
			i++
		ENDWHILE
	ENDIF
	
	//////////////////////////////////////////////////////
	///     PLAYER CREATED VEHICELS
	///     
	REPEAT NUM_PLAYER_VEHICLE_IDS i
		IF g_eCreatedPlayerVehiclePed[i] != NO_CHARACTER
		AND g_eCreatedPlayerVehicleModel[i] != DUMMY_MODEL_FOR_SCRIPT
		
			// Apply car app orders instantly if processed on boot.
			IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
			
				// Track vehicles state
				vLastPlayerVehicleCoords[i] = GET_ENTITY_COORDS(g_viCreatedPlayerVehicleIDs[i])
				fLastPlayerVehicleHeading[i] = GET_ENTITY_HEADING(g_viCreatedPlayerVehicleIDs[i])
				
				bInGarage = FALSE
				WHILE NOT bInGarage
				AND GET_PLAYER_GARAGE_DATA(g_eCreatedPlayerVehiclePed[i], iGarage, tlGarageName, eSavehouse)
					IF IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, g_viCreatedPlayerVehicleIDs[i])
						bInGarage = TRUE
						
						// We need to force the z-coord so that the vehicle doesnt appear on the roof when we use
						// the SET_VEHICLE_ON_GROUND_PROPERLY command.
						UPDATE_VEHICLE_COORDS_FOR_PLAYERS_GARGE(g_eCreatedPlayerVehiclePed[i], iGarage, vLastPlayerVehicleCoords[i])
					ENDIF
					iGarage++
				ENDWHILE
				bLastPlayerVehicleInGarage[i] = bInGarage
				
				// Process CarApp
				IF g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPending
				AND g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderReceivedOnBoot
				AND g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel = g_eCreatedPlayerVehicleModel[i]
					//IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP1", TXTMSG_LOCKED, GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel)), -99, "NULL", STRING_COMPONENT)
						
						PRINTLN("\nMONITOR_STORED_PLAYER_VEHICLES - Processing car app order immediately on script vehicle!")
						
						IF NOT g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor
							eAutoOrderModel = g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel
							bSendCarAppAutoProcessEmail = TRUE
							bCarAppOrderFree = (NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed] AND ePed = CHAR_FRANKLIN)
							
							IF NOT bCarAppOrderFree
								DEBIT_BANK_ACCOUNT(ePed, BAAC_CARMOD_SHOP_01_AP, g_savedGlobals.sSocialData.sCarAppOrder[ePed].iCost)
							ELSE
								PRINTLN("\nMONITOR_STORED_PLAYER_VEHICLES - App order is on the house!")
							ENDIF
							g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor = TRUE
						ENDIF
						
						SET_VEHICLE_MODS_FROM_CARAPP_ORDER(g_viCreatedPlayerVehicleIDs[i], g_savedGlobals.sSocialData.sCarAppOrder[ePed])
						SET_CAR_APP_ORDER_HAS_BEEN_PROCESSED(g_savedGlobals.sSocialData.sCarAppOrder[ePed], ePed)
						
						#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
							ADD_NUMBER_PLATE_TEXT_TO_PLAYERS_SC_ACCOUNT(g_savedGlobals.sSocialData.sCarAppOrder[ePed].tlPlateText_pending)
						#ENDIF
						
						// Re-apply the mod colour so the app knows which group to work with...
						INT iCurrentColourGroupP = -1
						INT iCurrentColourGroupS = -1
						GET_VEHICLE_COLOURS(g_viCreatedPlayerVehicleIDs[i], iTempCol1, iTempCol2)
						GET_VEHICLE_EXTRA_COLOURS(g_viCreatedPlayerVehicleIDs[i], iTempCol3, iTempCol4)
						SWITCH g_savedGlobals.sSocialData.sCarAppOrder[ePed].iColour1Group
							CASE 0 iCurrentColourGroupP = ENUM_TO_INT(MCT_METALLIC) BREAK
							CASE 1 iCurrentColourGroupP = ENUM_TO_INT(MCT_CLASSIC) BREAK
							CASE 2 iCurrentColourGroupP = ENUM_TO_INT(MCT_MATTE) BREAK
							CASE 3 iCurrentColourGroupP = ENUM_TO_INT(MCT_METALS) BREAK
							CASE 4 iCurrentColourGroupP = ENUM_TO_INT(MCT_CHROME) BREAK
						ENDSWITCH
						SWITCH g_savedGlobals.sSocialData.sCarAppOrder[ePed].iColour2Group
							CASE 0 iCurrentColourGroupS = ENUM_TO_INT(MCT_METALLIC) BREAK
							CASE 1 iCurrentColourGroupS = ENUM_TO_INT(MCT_CLASSIC) BREAK
							CASE 2 iCurrentColourGroupS = ENUM_TO_INT(MCT_MATTE) BREAK
							CASE 3 iCurrentColourGroupS = ENUM_TO_INT(MCT_METALS) BREAK
							CASE 4 iCurrentColourGroupS = ENUM_TO_INT(MCT_CHROME) BREAK
						ENDSWITCH
						IF iCurrentColourGroupP != ENUM_TO_INT(MCT_NONE)
						AND iCurrentColourGroupP != ENUM_TO_INT(MCT_PEARLESCENT)
							SET_VEHICLE_MOD_COLOR_1(g_viCreatedPlayerVehicleIDs[i], INT_TO_ENUM(MOD_COLOR_TYPE, iCurrentColourGroupP), 0, 0)
						ENDIF
						IF iCurrentColourGroupS != ENUM_TO_INT(MCT_NONE)
						AND iCurrentColourGroupS != ENUM_TO_INT(MCT_PEARLESCENT)
							SET_VEHICLE_MOD_COLOR_2(g_viCreatedPlayerVehicleIDs[i], INT_TO_ENUM(MOD_COLOR_TYPE, iCurrentColourGroupS), 0)
						ENDIF
						SET_VEHICLE_COLOURS(g_viCreatedPlayerVehicleIDs[i], iTempCol1, iTempCol2)
						SET_VEHICLE_EXTRA_COLOURS(g_viCreatedPlayerVehicleIDs[i], iTempCol3, iTempCol4)
						
						INT iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i]))
							iVehicleSlot = SAVED_VEHICLE_SLOT_BIKE
						ENDIF
						
						UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, g_viCreatedPlayerVehicleIDs[i], iVehicleSlot, TRUE)
						
					//ENDIF
				ENDIF
				
			// Remove reference to destroyed player vehicles
			ELIF NOT DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			OR NOT IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
				PRINTLN("Removing reference to player vehicleID ", NATIVE_TO_INT(g_viCreatedPlayerVehicleIDs[i]), " in slot ", i)
				
				bResetSpawnCoords = FALSE
				
				// Make sure the vehicle doesnt spawn at the savehouse straight away if it was left at a distance.
				eClosestSaveHouse = GET_CLOSEST_SAVEHOUSE(vLastPlayerVehicleCoords[i], g_eCreatedPlayerVehiclePed[i], TRUE)
				IF eClosestSaveHouse != NUMBER_OF_SAVEHOUSE_LOCATIONS
				AND GET_DISTANCE_BETWEEN_COORDS(vLastPlayerVehicleCoords[i], g_sSavehouses[eClosestSaveHouse].vSpawnCoords) > 200
					bResetSpawnCoords = TRUE
					IF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
						g_iCreatedPlayerVehicleCleanupTimer[g_eCreatedPlayerVehiclePed[i]][SAVED_VEHICLE_SLOT_BIKE] = GET_GAME_TIMER()
					ELSE
						g_iCreatedPlayerVehicleCleanupTimer[g_eCreatedPlayerVehiclePed[i]][SAVED_VEHICLE_SLOT_CAR] = GET_GAME_TIMER()
					ENDIF
				ENDIF
				
				// Always reset spawn coords for these vehicles.
				IF eClosestSaveHouse = SAVEHOUSE_TREVOR_VB
				OR eClosestSaveHouse = SAVEHOUSE_TREVOR_SC
					bResetSpawnCoords = TRUE
				ENDIF
				
				// Reset the default spawn position if we didnt park it in the garage
				IF NOT bLastPlayerVehicleInGarage[i]
					bResetSpawnCoords = TRUE
				ENDIF
				
				IF bResetSpawnCoords
					IF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
						PRINTLN("VEHGEN: Resetting default spawn position for BIKE")
						g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					ELSE
						PRINTLN("VEHGEN: Resetting default spawn position for CAR")
						g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = <<0,0,0>>
					ENDIF
					
					IF g_eCreatedPlayerVehiclePed[i] = GET_CURRENT_PLAYER_PED_ENUM()
						PRINTLN("[PVB] MONITOR_STORED_PLAYER_VEHICLES - Clearing cached coords as the vehicle position has reset")
						g_sVehicleGenNSData.vPlayerVehicleCoords = <<0,0,0>>
						g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
					ENDIF
				ELSE
					IF IS_THIS_MODEL_A_BIKE(g_eCreatedPlayerVehicleModel[i])
						PRINTLN("VEHGEN: Updating default spawn position for BIKE - ", GET_STRING_FROM_VECTOR(vLastPlayerVehicleCoords[i]))
						g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = vLastPlayerVehicleCoords[i]
						g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_BIKE][g_eCreatedPlayerVehiclePed[i]] = fLastPlayerVehicleHeading[i]
					ELSE
						PRINTLN("VEHGEN: Updating default spawn position for CAR - ", GET_STRING_FROM_VECTOR(vLastPlayerVehicleCoords[i]))
						g_savedGlobals.sPlayerData.sInfo.vPlayerVehicleCoords[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = vLastPlayerVehicleCoords[i]
						g_savedGlobals.sPlayerData.sInfo.fPlayerVehicleHeading[SAVED_VEHICLE_SLOT_CAR][g_eCreatedPlayerVehiclePed[i]] = fLastPlayerVehicleHeading[i]
					ENDIF
					
					IF g_eCreatedPlayerVehiclePed[i] = GET_CURRENT_PLAYER_PED_ENUM()
						PRINTLN("[PVB] MONITOR_STORED_PLAYER_VEHICLES - Clearing cached coords as the vehicle position has changed")
						g_sVehicleGenNSData.vPlayerVehicleCoords = <<0,0,0>>
						g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
					ENDIF
				ENDIF
				
				vLastPlayerVehicleCoords[i] = <<0,0,0>>
				bLastPlayerVehicleInGarage[i] = FALSE
				g_viCreatedPlayerVehicleIDs[i] = NULL
				g_eCreatedPlayerVehiclePed[i] = NO_CHARACTER
				g_eCreatedPlayerVehicleModel[i] = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		// Update the global ID for players last vehicle
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			g_vPlayerVeh[ePed] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ELSE
			bPlayerInCarInGarage = FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_vPlayerVeh[ePed])
		AND IS_VEHICLE_DRIVEABLE(g_vPlayerVeh[ePed])
		
			//Do savehouse garage / impound checks
			IF NOT g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle
			AND GET_GAME_TIMER() > iTimeNextGarageVehicleCheck
				IF IS_VEHICLE_IN_PLAYERS_GARAGE(g_vPlayerVeh[ePed], ePed,TRUE)
					bPlayerInCarInGarage = TRUE
				ELSE
					//Send to impound if just out of the garage
					IF bPlayerInCarInGarage
						TRACK_VEHICLE_FOR_IMPOUND(g_vPlayerVeh[ePed],ePed)
					ENDIF
					bPlayerInCarInGarage = FALSE
				ENDIF
				iTimeNextGarageVehicleCheck = GET_GAME_TIMER() + 1000
			ENDIF
				
		
			// Update cloud vehicle
			IF g_vPlayerVeh[ePed] != g_viLastVehicleSentToCloud[ePed]
			
				// Send the vehicle data to the cloud if it is a player vehicle and the model/plate dont match data in globals
				IF NOT g_savedGlobals.sSocialData.sCarAppData[ePed].bSendDataToCloud
				
					IF g_savedGlobals.sSocialData.sCarAppData[ePed].eModel != GET_ENTITY_MODEL(g_vPlayerVeh[ePed])
					OR NOT ARE_STRINGS_EQUAL(g_savedGlobals.sSocialData.sCarAppData[ePed].tlPlateText, GET_VEHICLE_NUMBER_PLATE_TEXT(g_vPlayerVeh[ePed]))
					
						////////////////////////////////
						///    	 SAVED VEHICLE
						IF IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_vPlayerVeh[ePed])
							BOOL bSafeToUpdate = TRUE
							
							// Do not send other player vehicles.
							IF ePed != GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(g_vPlayerVeh[ePed])
								bSafeToUpdate = FALSE
							
							// Do not send bagger until we own it.
							ELIF ePed = CHAR_FRANKLIN
							AND GET_ENTITY_MODEL(g_vPlayerVeh[ePed]) = BAGGER
							AND NOT (g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE])
								bSafeToUpdate = FALSE
							
							// Do not send default vehicle until we mod it.
							ELIF NOT g_savedGlobals.sSocialData.bFirstVehicleSentToCloud[ePed]
							AND GET_ENTITY_MODEL(g_vPlayerVeh[ePed]) = GET_PLAYER_VEH_MODEL(ePed, VEHICLE_TYPE_CAR)
								bSafeToUpdate = FALSE
							ENDIF
							
							IF bSafeToUpdate
								PRINTLN("MONITOR_STORED_PLAYER_VEHICLES() - Attempting to save player vehicle in Car App globals")
								
								INT iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
								IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_vPlayerVeh[ePed]))
									iVehicleSlot = SAVED_VEHICLE_SLOT_BIKE
								ENDIF
								
								UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, g_vPlayerVeh[ePed], iVehicleSlot, TRUE)
								g_viLastVehicleSentToCloud[ePed] = g_vPlayerVeh[ePed]
							ENDIF
						
						////////////////////////////////
						///    	 GARAGE VEHICLE
						ELIF IS_VEHICLE_IN_PLAYERS_GARAGE(g_vPlayerVeh[ePed], ePed, TRUE)
							PRINTLN("MONITOR_STORED_PLAYER_VEHICLES() - Attempting to save garage vehicle in Car App globals")
							UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, g_vPlayerVeh[ePed], SAVED_VEHICLE_SLOT_GARAGE, TRUE)
							g_viLastVehicleSentToCloud[ePed] = g_vPlayerVeh[ePed]
							g_savedGlobals.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed] = GET_CLOSEST_SAVEHOUSE(GET_ENTITY_COORDS(g_viLastVehicleSentToCloud[ePed]), ePed, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	// If we have an instant order we should send the text straight away.
	IF g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPending
	AND g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderReceivedOnBoot
	AND NOT g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	
		eAutoOrderModel = g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel
		bSendCarAppAutoProcessEmail = TRUE
		bCarAppOrderFree = (NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed] AND ePed = CHAR_FRANKLIN )
	
		IF NOT bCarAppOrderFree
			DEBIT_BANK_ACCOUNT(ePed, BAAC_CARMOD_SHOP_01_AP, g_savedGlobals.sSocialData.sCarAppOrder[ePed].iCost)
		ELSE
			PRINTLN("\nMONITOR_STORED_PLAYER_VEHICLES - App order is on the house!")
		ENDIF
		
		g_savedGlobals.sSocialData.sCarAppOrder[ePed].bOrderPaidFor = TRUE
	
	ELIF bSendCarAppAutoProcessEmail
	
		// Do not send text on mission
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
			
			TEXT_LABEL_15 tlLabel = "SOCIAL_CARAPP1"
			IF bCarAppOrderFree
				tlLabel = "SOCIAL_FREE1"
			ENDIF
			
			IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_LS_CUSTOMS, tlLabel, TXTMSG_LOCKED, GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(eAutoOrderModel)), -99, "NULL", STRING_COMPONENT)
				bSendCarAppAutoProcessEmail = FALSE
			ENDIF
		ELSE
//			// Cancel the text unless it's for the first order.
//			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
//				bSendCarAppAutoProcessEmail = FALSE
//			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE:
///    Prints mission names for available missions for each character, only if their number has changed from the last check.
/// PARAMS:
///    iChar - Char ID (0-2)
///    sName - Name of the mission, use GET_SP_MISSION_NAME_LABEL or GET_RC_MISSION_NAME_LABEL
///    bClearArea - Is the mission waiting on a clear area?
///    bIsRC - Is the mission an RC one?
///   
PROC PRINT_AVAILABLE_MISSION(INT iChar, TEXT_LABEL_7 sName, BOOL bLeaveArea = FALSE, BOOL bIsRC = FALSE, BOOL bIsPrep = FALSE)
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(iCHar)
	UNUSED_PARAMETER(sName)
	UNUSED_PARAMETER(bLeaveArea)
	UNUSED_PARAMETER(bIsRc)
	UNUSED_PARAMETER(bIsPrep)
	#ENDIF
	#IF IS_DEBUG_BUILD
		STRING tlName
		IF iChar = 0
			tlName = "Michael"
		ELIF iChar = 1
			tlName = "Franklin"
		ELIF iChar = 2
			tlName = "Trevor"
		ENDIF
		
		IF bMissionCountHasChanged[iChar]
			IF bIsRC
				CDEBUG1LN(debug_ped_comp,tlName,": available RC mission: ",sName)
			ELIF bLeaveArea
				CDEBUG1LN(debug_ped_comp,tlName,": available mission waiting on LEAVE AREA: ",sName)
			ELIF bIsPrep
				CDEBUG1LN(debug_ped_comp,tlName,": available vehicle prep for mission: ",sName)
			ELSE
				CDEBUG1LN(debug_ped_comp,tlName,": available mission: ",sName)
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE: Checks to see if there are any missions available for each player character
PROC MONITOR_AVAILABLE_PLAYER_MISSIONS()

	IF GET_GAME_TIMER() > iTimeNextPlayerMissionCheck
		INT tempLoop = 0
		INT arrayPos = 0
		INT triggerIndex = 0
		
		//Save the current available missions for debugging
		INT iLastMissionCount = g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]
		//Reset the current mission count for the character we're checking this frame.
		g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame] = 0
		g_iQueuedCalls[iPlayerCharToCheckThisFrame] = 0
		g_iQueuedTxts[iPlayerCharToCheckThisFrame] = 0
		
		//Check all available missions for the character we're checking this frame.
		REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		
			arrayPos = g_availableMissions[tempLoop].index
			triggerIndex = g_availableMissions[tempLoop].missionTriggerID
			
			IF NOT (arrayPos = ILLEGAL_ARRAY_POSITION)
				IF NOT (triggerIndex = ILLEGAL_ARRAY_POSITION)
					//Ignore missions that are currently blocked by their trigger logic.
					IF NOT g_TriggerableMissions[triggerIndex].bBlocked
					
						SP_MISSIONS eMission = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[arrayPos].iValue1)
						
						//Has this mission been flagged to not show up on the switch HUD?
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_NO_SWITCH_COUNT)
						
							// Get triggerable character bitset for this mission.
							INT triggerCharBitset = Get_Mission_Triggerable_Characters_Bitset(eMission)
							
							// Check if the playable characters is allowed to trigger this mission.
							IF Is_Mission_Triggerable_By_Character(triggerCharBitset, INT_TO_ENUM(enumCharacterList, iPlayerCharToCheckThisFrame))
								
								//Check if this mission is time critical.
								IF triggerIndex != NO_CANDIDATE_ID
									g_eFirstAvailablePlayerMission[iPlayerCharToCheckThisFrame] = eMission
									g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
									PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(eMission))
								ELSE
									//mission probably available, waiting for leave area
									g_eFirstAvailablePlayerMission[iPlayerCharToCheckThisFrame] = eMission
									g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
									PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(eMission),TRUE)
								ENDIF
							ELSE
								
								//Special case ensuring Franklin has Exile2 as an available mission.
								IF (INT_TO_ENUM(enumCharacterList, iPlayerCharToCheckThisFrame) = CHAR_FRANKLIN)
									IF (eMission = SP_MISSION_EXILE_2)
										g_eFirstAvailablePlayerMission[iPlayerCharToCheckThisFrame] = eMission
										g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
										PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(eMission))
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check communication queues for mission triggering or unlocking communications.
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls tempLoop
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
			OR IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iSettings, COMM_BIT_TRIGGERS_MISSION)
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, iPlayerCharToCheckThisFrame)
					g_iQueuedCalls[iPlayerCharToCheckThisFrame]++
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts tempLoop
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
			OR IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iSettings, COMM_BIT_TRIGGERS_MISSION)
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, iPlayerCharToCheckThisFrame)
					g_iQueuedTxts[iPlayerCharToCheckThisFrame]++
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails tempLoop
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
			OR IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iSettings, COMM_BIT_TRIGGERS_MISSION)
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iPlayerCharBitset, iPlayerCharToCheckThisFrame)
					g_iQueuedTxts[iPlayerCharToCheckThisFrame]++
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		//Check for the getaway vehicle missions being available.
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FBI4_Prep3Amb")) > 0
				g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
				PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(SP_MISSION_FBI_4_PREP_3),DEFAULT,DEFAULT,TRUE)
			ENDIF
		ENDIF
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PREP_2_DONE)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_prep2Amb")) > 0
				IF INT_TO_ENUM(enumCharacterList, iPlayerCharToCheckThisFrame) != CHAR_TREVOR
					g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
					PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(SP_HEIST_AGENCY_2),DEFAULT,DEFAULT,TRUE)
				ENDIF
			ENDIF
		ENDIF
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_DONE)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist_prepEAmb")) > 0
				g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
				PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_SP_MISSION_NAME_LABEL(SP_HEIST_FINALE_PREP_D),DEFAULT,DEFAULT,TRUE)
			ENDIF
		ENDIF
		
		
		//Check for RC missions that are revealed in the FOW being available.
		g_eRC_MissionIDs eRCMissionID
		g_structRCMissionsStatic sRCMissionDetails
		
		REPEAT MAX_RC_MISSIONS tempLoop
			eRCMissionID = INT_TO_ENUM(g_eRC_MissionIDs, tempLoop)

			IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
			AND IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
			AND IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
			AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
				INT iFOWBit = tempLoop
				INT iFOWBitset = 0
				WHILE iFOWBit > 31
					iFOWBit -= 32
					iFOWBitset++
				ENDWHILE
				IF IS_BIT_SET(g_savedGlobals.sRandomChars.g_iVisibleInFOWBitset[iFOWBitset], iFOWBit)
					Retrieve_Random_Character_Static_Mission_Details(eRCMissionID, sRCMissionDetails)
					IF IS_BIT_SET(sRCMissionDetails.rcPlayableChars, iPlayerCharToCheckThisFrame)
						PRINT_AVAILABLE_MISSION(iPlayerCharToCheckThisFrame,GET_RC_MISSION_NAME_LABEL(eRCMissionID),FALSE,TRUE)
						g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check if previous mission count has changed
		
		IF iLastMissionCount != g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame]
		#IF IS_DEBUG_BUILD
			bMissionCountHasChanged[iPlayerCharToCheckThisFrame] = TRUE
		ELSE
			//Reset the bool if it hasn't changed
			bMissionCountHasChanged[iPlayerCharToCheckThisFrame] = FALSE
		#ENDIF
		ENDIF
		
			
		//Step to the next playable character index for the next frame.
		iPlayerCharToCheckThisFrame++
		IF iPlayerCharToCheckThisFrame >= NUM_OF_PLAYABLE_PEDS
			iPlayerCharToCheckThisFrame = 0
			//If we've checked all playable characters wait 2 seconds before checking again.
			iTimeNextPlayerMissionCheck = GET_GAME_TIMER() + 2000
		ENDIF
	ENDIF
	
	BOOL bOnMissionOfAnyType = IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
	
	// Check for displaying of various pieces of selector HUD help text.
	// No characters to swap to help text.
	IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_A)
	OR NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_B)
	OR NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_C)
		IF NOT bOnMissionOfAnyType
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_3)
					//Trevor 3 complete. This help message no longer needed. Flag it as shown.
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_A)
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_B)
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_C)
				ELSE
					IF IS_SELECTOR_ONSCREEN()
						IF iTimeNextSwitchDeniedCheck != -1
							IF GET_GAME_TIMER() > iTimeNextSwitchDeniedCheck
								SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SWTCH4")
									CASE FHS_EXPIRED
										ADD_HELP_TO_FLOW_QUEUE("AM_H_SWTCH4", FHP_MEDIUM, 0, 1000)
									BREAK
									CASE FHS_DISPLAYED
										IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_A)
											SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_A)
											iTimeNextSwitchDeniedCheck = -1
											g_txtFlowHelpLastDisplayed = ""
										ELIF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_B)
											SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_B)
											iTimeNextSwitchDeniedCheck = -1
											g_txtFlowHelpLastDisplayed = ""
										ELIF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_C)
											SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_C)
											iTimeNextSwitchDeniedCheck = -1
											g_txtFlowHelpLastDisplayed = ""
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ELIF iTimeNextSwitchDeniedCheck = -1
						iTimeNextSwitchDeniedCheck = GET_GAME_TIMER() + 15000
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_EXILE)
		IF NOT bOnMissionOfAnyType
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MARTIN_1)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
					//exile 1 complete. This help message no longer needed. Flag it as shown.
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_EXILE)
				ELSE
					IF IS_SELECTOR_ONSCREEN()
						IF iTimeNextSwitchDeniedCheck != -1
							IF GET_GAME_TIMER() > iTimeNextSwitchDeniedCheck
								SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SWTCH8")
									CASE FHS_EXPIRED
										ADD_HELP_TO_FLOW_QUEUE("AM_H_SWTCH8", FHP_MEDIUM, 0, 1000)
									BREAK
									CASE FHS_DISPLAYED
										IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_NO_SWAP_EXILE)
											SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_EXILE)
											iTimeNextSwitchDeniedCheck = -1
											g_txtFlowHelpLastDisplayed = ""
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ELIF iTimeNextSwitchDeniedCheck = -1
						iTimeNextSwitchDeniedCheck = GET_GAME_TIMER() + 15000
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bOnMissionOfAnyType
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				// Player is wanted. Inform them they can't switch while wanted.
				IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_WANTED_SWAP)
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3)
							SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SWTCH7")
								CASE FHS_EXPIRED
									IF IS_SELECTOR_ONSCREEN()
										ADD_HELP_TO_FLOW_QUEUE("AM_H_SWTCH7", FHP_MEDIUM, 0, 1000, DEFAULT_HELP_TEXT_TIME ,GET_CURRENT_PLAYER_PED_BIT())
									ENDIF
								BREAK
								CASE FHS_DISPLAYED
									SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_WANTED_SWAP)
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// First selector mission help text.
				IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_MISSIONS_A)
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3)
						BOOL bHasMission = FALSE
						INT iCharIndex
						REPEAT 3 iCharIndex
							IF g_iAvailablePlayerMissions[iCharIndex] > 0
								bHasMission = TRUE
							ENDIF
						ENDREPEAT
						IF bHasMission
							IF IS_SELECTOR_ONSCREEN()							
								REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_SWTCH0")
								SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SWTCH1")
									CASE FHS_EXPIRED
										ADD_HELP_TO_FLOW_QUEUE("AM_H_SWTCH1", FHP_MEDIUM, 0, 1000, 4000)
										CLEAR_FLOW_HELP_MESSAGE_DELAY()
									BREAK
									CASE FHS_DISPLAYED
										SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_MISSIONS_A)
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
					
				// Second selector mission help text.
				ELIF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_MISSIONS_B)
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3)
						BOOL bHasMission = FALSE
						INT iCharIndex
						REPEAT 3 iCharIndex
							IF g_iAvailablePlayerMissions[iCharIndex] > 0
								bHasMission = TRUE
							ENDIF
						ENDREPEAT
						IF bHasMission
							SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SWTCH9")//"AM_H_SWTCH2")
								CASE FHS_EXPIRED
									ADD_HELP_TO_FLOW_QUEUE("AM_H_SWTCH9", FHP_MEDIUM, 0, 1000)//"AM_H_SWTCH2", FHP_MEDIUM, 0, 1000)
								BREAK
								CASE FHS_DISPLAYED
									SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_MISSIONS_B)
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("g_iAvailablePlayerMissions[",iPlayerCharToCheckThisFrame,"] = ", g_iAvailablePlayerMissions[iPlayerCharToCheckThisFrame],"  ")
ENDPROC

FUNC BOOL GET_PLAYER_PED_CUTSCENE_HANDLE(enumCharacterList ePed, BOOL bVehicle, INT iVariation, TEXT_LABEL_31 &tlSceneHandle)

	tlSceneHandle = ""
	
	/////////////////////
	///     PEDS
	IF NOT bVehicle
		SWITCH iVariation
			CASE 0
				SWITCH ePed
					CASE CHAR_MICHAEL 	tlSceneHandle = "Michael"		BREAK
					CASE CHAR_FRANKLIN 	tlSceneHandle = "Franklin"		BREAK
					CASE CHAR_TREVOR 	tlSceneHandle = "Trevor"		BREAK
				ENDSWITCH
			BREAK
			CASE 1
				SWITCH ePed
					CASE CHAR_MICHAEL 	tlSceneHandle = "player_zero"	BREAK
					CASE CHAR_FRANKLIN 	tlSceneHandle = "player_one"	BREAK
					CASE CHAR_TREVOR 	tlSceneHandle = "player_two"	BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	
	/////////////////////
	///     VEHICLES
	ELSE
		SWITCH iVariation
			CASE 0
				SWITCH ePed
					CASE CHAR_MICHAEL 	tlSceneHandle = "tailgater"		BREAK
					CASE CHAR_FRANKLIN 	tlSceneHandle = "buffalo"		BREAK
					CASE CHAR_TREVOR 	tlSceneHandle = "bodhi2"		BREAK
				ENDSWITCH
			BREAK
			CASE 1
				SWITCH ePed
					CASE CHAR_MICHAEL 	tlSceneHandle = "michaels_car"		BREAK
					CASE CHAR_FRANKLIN 	tlSceneHandle = "franklins_car"		BREAK
					CASE CHAR_TREVOR 	tlSceneHandle = "trevors_car"		BREAK
				ENDSWITCH
			BREAK
			CASE 2
				SWITCH ePed
					CASE CHAR_MICHAEL 	tlSceneHandle = "EXL_3_Michaels_car"	BREAK
					CASE CHAR_FRANKLIN 	tlSceneHandle = "Franklin_Bike"			BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN (GET_HASH_KEY(tlSceneHandle) != 0)
ENDFUNC

FUNC BOOL GET_ENTITY_ID_FOR_CUTSCENE_VEHICLE(enumCharacterList ePed, MODEL_NAMES eModel, ENTITY_INDEX &entityID)
	
	TEXT_LABEL_31 tlSceneHandle
	INT iHandleVariation
	
	IF eModel != DUMMY_MODEL_FOR_SCRIPT
		WHILE GET_PLAYER_PED_CUTSCENE_HANDLE(ePed, TRUE, iHandleVariation, tlSceneHandle)
			IF DOES_CUTSCENE_ENTITY_EXIST(tlSceneHandle, eModel)
			AND GET_ENTITY_MODEL(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tlSceneHandle, eModel)) = eModel
				entityID = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tlSceneHandle, eModel)
				RETURN TRUE
			ENDIF
			iHandleVariation++
		ENDWHILE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE: Checks to see if any player ped or player vehicles exist in a cutscene.
///   		 This proc will ensure that the correct variations/weapons/tattoos/vehicle states are all up to date with stored data
PROC MONITOR_PLAYER_CUTSCENE_ENTITIES()

	INT iPlayerPed
	enumCharacterList ePed
	ENTITY_INDEX entityID
	TEXT_LABEL_31 tlSceneHandle
	
	// Ped data
	INT iHandle
	PED_INDEX pedID
	BOOL bPedDataSet[NUM_OF_PLAYABLE_PEDS]
	
	// Vehicle data
	VEHICLE_INDEX vehID
	INT iPlateBack
	TEXT_LABEL_15 tlPlateText
	BOOL bVehDataSet[NUM_OF_PLAYABLE_PEDS]
	PED_VEH_DATA_STRUCT sVehData
	INT iExtra
	INT iVehicleSlot
	VEHICLE_CREATE_TYPE_ENUM eVehicleType
	
	
	//////////////////////////////////////////////////////
	///     SET PED VARIATIONS
	///     
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
	
		// Ok to set variations?
		IF NOT IS_CUTSCENE_PLAYBACK_FLAG_SET(CUTSCENE_REQUESTED_FROM_WIDGET)
		AND g_bSetPlayerClothesInCutscenes
		
			// Go through all player characters
			REPEAT NUM_OF_PLAYABLE_PEDS iPlayerPed
				ePed = INT_TO_ENUM(enumCharacterList, iPlayerPed)
				IF NOT bCutscenePed_VarsSet[iPlayerPed]
					PRINTLN("MONITOR_PLAYER_CUTSCENE_ENTITIES()  - Attempting to set variations for ", GET_PLAYER_PED_STRING(ePed))
					iHandle = 0
					WHILE iHandle < 2 AND NOT bCutscenePed_VarsSet[iPlayerPed]
						IF GET_PLAYER_PED_CUTSCENE_HANDLE(ePed, FALSE, iHandle, tlSceneHandle)
							SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS(ePed, tlSceneHandle)
							bCutscenePed_VarsSet[iPlayerPed] = TRUE
						ENDIF
						iHandle++
					ENDWHILE
					bCutscenePed_VarsSet[iPlayerPed] = TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		// Reset all player character set flags
		REPEAT NUM_OF_PLAYABLE_PEDS iPlayerPed
			bCutscenePed_VarsSet[iPlayerPed] = FALSE
		ENDREPEAT
	ENDIF
	
	
	//////////////////////////////////////////////////////
	///     SET PED AND VEH DATA
	///     
	REPEAT NUM_OF_PLAYABLE_PEDS iPlayerPed
	
		IF IS_CUTSCENE_ACTIVE()
		
			ePed = INT_TO_ENUM(enumCharacterList, iPlayerPed)
			
			///////////////////////////////////////////////
			///     PEDS
			///     
			pedID = NULL
			entityID = NULL
			iHandle = 0
			
			WHILE iHandle < 2
			AND NOT DOES_ENTITY_EXIST(entityID)
				IF GET_PLAYER_PED_CUTSCENE_HANDLE(ePed, FALSE, iHandle, tlSceneHandle)
					IF DOES_CUTSCENE_ENTITY_EXIST(tlSceneHandle, GET_PLAYER_PED_MODEL(ePed))
						entityID = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tlSceneHandle, GET_PLAYER_PED_MODEL(ePed))
					ENDIF
				ENDIF
				iHandle++
			ENDWHILE
			
			IF DOES_ENTITY_EXIST(entityID)
				IF IS_ENTITY_A_PED(entityID)
					pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(entityID)
					
					IF NOT IS_PED_INJURED(pedID)
						IF NOT bCutscenePed_DataSet[iPlayerPed]
							// Only set data on fresh cutscene peds
							IF NOT IS_PED_IN_TEMP_PLAYER_PED_ID_LIST(pedID)
							AND pedID != PLAYER_PED_ID()
								#IF IS_DEBUG_BUILD
									PRINTLN("\n\nMONITOR_PLAYER_CUTSCENE_ENTITIES() Setting player data for ", GET_PLAYER_PED_STRING(ePed), " ped")
								#ENDIF
								
								IF g_bSetPlayerWeaponsInCutscenes
							    	RESTORE_PLAYER_PED_WEAPONS(pedID)
								ENDIF
							    RESTORE_PLAYER_PED_ARMOUR(pedID)
								RESTORE_PLAYER_PED_TATTOOS(pedID)
								
								// Store in temp list so we don't set the data again
								STORE_TEMP_PLAYER_PED_ID(pedID)
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("\n\nMONITOR_PLAYER_CUTSCENE_ENTITIES() Player data already set for ", GET_PLAYER_PED_STRING(ePed), " ped")
								#ENDIF
							ENDIF
							bCutscenePed_DataSet[iPlayerPed] = TRUE
						ENDIF
						bPedDataSet[iPlayerPed] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
			///////////////////////////////////////////////
			///     VEHICLES
			///     
			vehID = NULL
			entityID = NULL
			iVehicleSlot = -1
			eVehicleType = VEHICLE_TYPE_DEFAULT
			
			// Stored CAR data
			IF GET_ENTITY_ID_FOR_CUTSCENE_VEHICLE(ePed, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model, entityID)
				iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
				
			// Stored BIKE data
			ELIF GET_ENTITY_ID_FOR_CUTSCENE_VEHICLE(ePed, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model, entityID)
				iVehicleSlot = SAVED_VEHICLE_SLOT_BIKE
				
			// Default CAR data
			ELIF GET_ENTITY_ID_FOR_CUTSCENE_VEHICLE(ePed, GET_PLAYER_VEH_MODEL(ePed, VEHICLE_TYPE_CAR), entityID)
				eVehicleType = VEHICLE_TYPE_CAR
			
			// Default BIKE data
			ELIF GET_ENTITY_ID_FOR_CUTSCENE_VEHICLE(ePed, GET_PLAYER_VEH_MODEL(ePed, VEHICLE_TYPE_BIKE), entityID)
				eVehicleType = VEHICLE_TYPE_BIKE
			ENDIF
			
			IF DOES_ENTITY_EXIST(entityID)
				IF IS_ENTITY_A_VEHICLE(entityID)
					vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityID)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehID)
			AND IS_VEHICLE_DRIVEABLE(vehID)
				IF NOT bCutsceneVeh_DataSet[iPlayerPed]
					// Only set data on fresh cutscene vehicle
					IF NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(vehID)
					
						// Default data
						IF iVehicleSlot = -1
						
							#IF IS_DEBUG_BUILD
								PRINTLN("\n\nMONITOR_PLAYER_CUTSCENE_ENTITIES() Setting default vehicle data for ", GET_PLAYER_PED_STRING(ePed))
							#ENDIF
							
							// Grab the vehicle data for the specified ped
				       		GET_PLAYER_VEH_DATA(ePed, sVehData, eVehicleType)
							PRINTLN("...plate = ", sVehData.tlNumberPlate)
							PRINTLN("...type = ", ENUM_TO_INT(sVehData.eType))
							PRINTLN("...model = ", GET_MODEL_NAME_FOR_DEBUG(sVehData.model))
							
							
							IF sVehData.model = GET_ENTITY_MODEL(vehID)
							
					            SET_VEH_DATA_FROM_STRUCT(vehID, sVehData)
								
								SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehID, FALSE)
								SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehID, FALSE)
								SET_VEHICLE_HAS_STRONG_AXLES(vehID, TRUE)
								
								// Attach Mr Raspberry Jam to Trevors truck
								IF ePed = CHAR_TREVOR
									IF GET_ENTITY_MODEL(vehID) = BODHI2
										TURN_ON_MR_RASPBERRY_JAM(vehID)
									ENDIF
								ENDIF
								
								// Store in temp list so we don't set the data again
								STORE_TEMP_PLAYER_VEHICLE_ID(vehID, ePed)
							ENDIF
						
						// Stored data
						ELSE
							
							#IF IS_DEBUG_BUILD
								PRINTLN("\n\nMONITOR_PLAYER_CUTSCENE_ENTITIES() Setting stored vehicle data for ", GET_PLAYER_PED_STRING(ePed))
							#ENDIF
							
							SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehID, FALSE)
							SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehID, FALSE)
							SET_VEHICLE_HAS_STRONG_AXLES(vehID, TRUE)
							
							// Colours
							SET_VEHICLE_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour2)
							SET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra2)
							
							// Extras
							REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
								SET_VEHICLE_EXTRA(vehID, iExtra+1, (NOT g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[iExtra]))
							ENDREPEAT
							
							// Number plate
							// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
							IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
								SET_VEHICLE_NUMBER_PLATE_TEXT(vehID, tlPlateText)
								SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID, iPlateBack)
							ELSE
								SET_VEHICLE_NUMBER_PLATE_TEXT(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].tlNumberPlate)
								IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack >= 0
								AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
									SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack)
								ENDIF
							ENDIF
							
							// Tyre smoke
							SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreB)
							
							// Window tint
							SET_VEHICLE_WINDOW_TINT(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWindowTintColour)
							
							// Neon lights
							SET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonB)
							SET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
							SET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
							SET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
							SET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
							
							// Livery
							IF GET_VEHICLE_LIVERY_COUNT(vehID) > 1
							AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iLivery >= 0
								SET_VEHICLE_LIVERY(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iLivery)
							ENDIF
							
							// Wheel type
							IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType > -1
								IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehID))
									IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehID))
										IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType) = MWT_BIKE)
											SET_VEHICLE_WHEEL_TYPE(vehID, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType))
										ENDIF
									ELSE
										SET_VEHICLE_WHEEL_TYPE(vehID, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType))
									ENDIF
								ENDIF
							ENDIF
							
							// Mods
							SET_VEHICLE_MOD_DATA(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModVariation)
							
							// Attach Mr Raspberry Jam to Trevors truck
							IF ePed = CHAR_TREVOR
								IF GET_ENTITY_MODEL(vehID) = BODHI2
									TURN_ON_MR_RASPBERRY_JAM(vehID)
								ENDIF
							ENDIF
							
							// Store in temp list so we don't set the data again
							STORE_TEMP_PLAYER_VEHICLE_ID(vehID, ePed)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("\n\nMONITOR_PLAYER_CUTSCENE_ENTITIES() Player data already set for ", GET_PLAYER_PED_STRING(ePed), " vehicle")
						#ENDIF
					ENDIF
					
					bCutsceneVeh_DataSet[iPlayerPed] = TRUE
				ENDIF
				bVehDataSet[iPlayerPed] = TRUE
			ENDIF
		ENDIF
		
		IF NOT bPedDataSet[iPlayerPed]
			bCutscenePed_DataSet[iPlayerPed] = FALSE
		ENDIF
		IF NOT bVehDataSet[iPlayerPed]
			bCutsceneVeh_DataSet[iPlayerPed] = FALSE
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: This proc gets called when the player controller script starts up.
///    		 One of the main tasks is to set the player as one of the main chracters.
PROC INITIALISE_PLAYER_PED_STATE()

	// Make sure all the defaults have been set up
	IF NOT HAS_DEFAULT_INFO_BEEN_SET()
		SETUP_DEFAULT_PLAYER_INFO()

		//Setup default relationship groups.
		SETUP_DEFAULT_PLAYER_RELATIONSHIPS()
		
		// Ensure we update cutscene data
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
		
		// Set random player character on startup
		#IF IS_DEBUG_BUILD
			IF NOT g_bRandomPlayerCharacterSet
			AND NOT g_bMagDemoActive
			
				SELECTOR_SLOTS_ENUM eCharacterSelection = SELECTOR_PED_MICHAEL
				
				//Allow randomised start character to be overriden from the command line. -BenR
				BOOL bUseRandom = TRUE
				IF g_eDebugForceStartingCharacter != NO_CHARACTER
					IF IS_PLAYER_PED_PLAYABLE(g_eDebugForceStartingCharacter)
						bUseRandom = FALSE
						eCharacterSelection = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(g_eDebugForceStartingCharacter)
						PRINTLN("Starting character set to ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCharacterSelection)), " by custom_config script.")
					ELSE
						SCRIPT_ASSERT("Starting character defined in custom_config.sc is not playable. Using randomised starting char instead.")
					ENDIF
				ENDIF
			
				IF bUseRandom
					eCharacterSelection = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, GET_RANDOM_INT_IN_RANGE(0, 3))
					PRINTLN("Starting character randomised as ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCharacterSelection)), ".")
				ENDIF
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(eCharacterSelection)
					WAIT(0)
				ENDWHILE
				g_bRandomPlayerCharacterSet = TRUE
				
				// Get a random outfit set
				bSetRandomOutfitOnStartup = TRUE
			ENDIF
			
			// Reset last known ped info - for a clean slate when switching/creating ambient player peds
			ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
		#ENDIF
		
		
		// Max out all special abilities when not running the actual flow
		#IF IS_DEBUG_BUILD
			IF NOT g_savedGlobals.sFlow.isGameflowActive
			OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
				g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_MICHAEL] = TRUE
				g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_FRANKLIN] = TRUE
				g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[CHAR_TREVOR] = TRUE
				
				// Override the stat value
				STAT_SET_INT(SP0_SPECIAL_ABILITY, 100)
				STAT_SET_INT(SP1_SPECIAL_ABILITY, 100)
				STAT_SET_INT(SP2_SPECIAL_ABILITY, 100)
				
				// Unable to override current character so fill on ped
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("\nPlayer controller - initial player ped state [ePed: ")
	PRINTSTRING(GET_CURRENT_PLAYER_PED_STRING())
	PRINTSTRING(", eState: ")
	PRINTINT(ENUM_TO_INT(g_sPlayerPedRequest.eState))
	PRINTSTRING(", in progress:  ")
	PRINTBOOL(IS_PLAYER_SWITCH_IN_PROGRESS())
	PRINTSTRING("]")
	PRINTNL()
	#ENDIF
	
	// If we are in the middle of switching then jump straight to the
	// switch stage - this is most likely to happen when returning
	// from MP
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTSTRING("\nPlayer controller - returning from MP")PRINTNL()
//		
//		// Fix for bug #170990 - Removing player control so we cannot activate sp script with invalid character
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		eStage = PC_STAGE_WAIT
	ELSE
	
		// If we are not a valid player character then make a request to be one.
		IF NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		
			PRINTSTRING("\nPlayer controller - invalid character, attempting to make a request")PRINTNL()
			
			// Fix for bug #170990 - Removing player control so we cannot activate sp script with invalid character
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			// Attempt to set the last known player ped
			enumCharacterList ePed = GET_LAST_KNOWN_PLAYER_PED_ENUM()
			IF IS_PLAYER_PED_PLAYABLE(ePed)
				MAKE_PLAYER_PED_SWITCH_REQUEST(ePed)	//, PR_TYPE_AMBIENT)
				
			// Unable to get the last known player ped so try and get a ped that has been introduced.
			ELIF IS_SELECTOR_PED_AVAILABLE_IN_FLOW(SELECTOR_PED_MICHAEL) AND IS_SELECTOR_PED_INTRODUCED_IN_FLOW(SELECTOR_PED_MICHAEL)
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_MICHAEL)	//, PR_TYPE_AMBIENT)
			ELIF IS_SELECTOR_PED_AVAILABLE_IN_FLOW(SELECTOR_PED_FRANKLIN) AND IS_SELECTOR_PED_INTRODUCED_IN_FLOW(SELECTOR_PED_FRANKLIN)
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_FRANKLIN)	//, PR_TYPE_AMBIENT)
			ELIF IS_SELECTOR_PED_AVAILABLE_IN_FLOW(SELECTOR_PED_TREVOR) AND IS_SELECTOR_PED_INTRODUCED_IN_FLOW(SELECTOR_PED_TREVOR)
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_TREVOR)	//, PR_TYPE_AMBIENT)
			ELSE
			
				// Unable to find any suitable character so just select Michael.
				PRINTSTRING("\nPlayer controller - no suitable characters available, making a request for Michael")PRINTNL()
				MAKE_PLAYER_PED_SWITCH_REQUEST(CHAR_MICHAEL)	//, PR_TYPE_AMBIENT)
			ENDIF
		
		// Valid player character so set default info
		ELSE
			IF g_bPlayerSetupByQuickSave = FALSE
				PED_INDEX pedID = PLAYER_PED_ID()
				RESTORE_PLAYER_PED_WEAPONS(pedID)
				RESTORE_PLAYER_PED_ARMOUR(pedID)
				RESTORE_PLAYER_PED_TATTOOS(pedID)
				RESTORE_PLAYER_PED_VARIATIONS(pedID)
				SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(pedID, FALSE)
			ELSE
				CPRINTLN(DEBUG_PED_COMP,"Player controller skipping variations setup. Already done for loading a quicksave.")
			ENDIF
			g_bPlayerSetupByQuickSave = FALSE
		ENDIF
		
		// Jump to the wait stage so we can process any requestst
		eStage = PC_STAGE_WAIT
	ENDIF
	
	// Re-instate special ability
	INT i
	REPEAT NUM_OF_PLAYABLE_PEDS i
		IF g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[i]
			SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(INT_TO_ENUM(enumCharacterList, i)))
		ELSE
			SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(INT_TO_ENUM(enumCharacterList, i)))
		ENDIF
	ENDREPEAT
	
	// Refresh the last stored clothes struct to match saved globals
	g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
	g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
	g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
	
	// Set the front end player blip name
	UPDATE_PLAYER_PED_BLIP_NAME()
	// Set the correct timecycle modifier
	UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
	// Update the last known player info to stats
	UPDATE_PLAYER_PED_VARIATION_STATS()
	// Register this player with their automatic doors.
	PED_INDEX playerPed = PLAYER_PED_ID()
	REGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(GET_CURRENT_PLAYER_PED_ENUM(), playerPed)
	
//	// Check for respawning Player away from mission blips at safehouse	#939375
//	IF SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD()
//		
//		// Setup the scene data
//		sSceneData.iStage = 0
//		sSceneData.eScene = PR_SCENE_INVALID
//		sSceneData.ePed = GET_CURRENT_PLAYER_PED_ENUM()	//g_sPlayerPedRequest.ePed
//		
//		PED_SCENE_STRUCT sPedScene
//		PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
//		
//		FLOAT fScenePercent
//		SWITCH sSceneData.ePed
//			CASE CHAR_MICHAEL
//				GetMichaelSceneForCurrentTime(sSceneData.eScene, fScenePercent, sPedScene, sPassedScene)
//			BREAK
//			
//			CASE CHAR_FRANKLIN
//				GetFranklinSceneForCurrentTime(sSceneData.eScene, fScenePercent, sPedScene, sPassedScene)
//			BREAK
//			
//			CASE CHAR_TREVOR
//				GetTrevorSceneForCurrentTime(sSceneData.eScene, fScenePercent, sPedScene, sPassedScene)
//			BREAK
//		ENDSWITCH
//			
//		#IF IS_DEBUG_BUILD
//		PRINTSTRING("<player_controller> GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME[\"")
//		PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(sSceneData.eScene))
//		PRINTSTRING("\"] fScenePercent:")
//		PRINTFLOAT(fScenePercent)
//		PRINTSTRING("%")
//		
//		PRINTSTRING(" - perform post-load switch (url:bugstar:939375)")
//		PRINTNL()
//		#ENDIF
//		
//		WHILE NOT PROCESS_PLAYER_PED_SCENE(sSceneData, ENUM_TO_INT(SWITCH_FLAG_DESCENT_ONLY))
//			
//			#IF IS_DEBUG_BUILD
//			PRINTSTRING("<player_controller> PROCESS_PLAYER_PED_SCENE[\"")
//			PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(sSceneData.eScene))
//			PRINTSTRING("\"]")
//			PRINTNL()
//			#ENDIF
//			
//			WAIT(0)
//		ENDWHILE
//	
//	ELSE
//		#IF IS_DEBUG_BUILD
//		PRINTSTRING("<player_controller> SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD()")
//		
//		PRINTSTRING(" - perform post-load switch (url:bugstar:939375)")
//		PRINTNL()
//		#ENDIF
//	ENDIF
	
ENDPROC


PROC MONITOR_PLAYER_WANTED_RELATIONSHIPS()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			IF NOT bWantedRelationshipsSet
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_COP)
					bWantedRelationshipsSet = TRUE
				ENDIF
			ELSE
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_COP)
					bWantedRelationshipsSet = FALSE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//// PURPOSE: Handle vehicle recording for end of martin1.
PROC HANDLE_END_OF_MISSION_MARTIN_1()

	// Bail checks
	IF iMartin1Stage > 0
		IF IS_PED_INJURED(PLAYER_PED_ID())
		OR NOT g_bHandleEndOfMission_Martin1
			iMartin1Stage = 99
			PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - bail.")
		ENDIF
	ENDIF

	SWITCH iMartin1Stage
		CASE 0
			// Wait for mission script to set flag
			IF g_bHandleEndOfMission_Martin1
				iMartin1Stage++
				PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - waiting for cutscene to end.")
			ENDIF
		BREAK
		CASE 1
			REQUEST_VEHICLE_RECORDING(000, "END_MARTIN_1") bMartin1RecordingRequested = TRUE
			IF NOT IS_CUTSCENE_PLAYING()
			AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Martin1")) = 0
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(000, "END_MARTIN_1")
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						viMartin1Veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
					
					IF DOES_ENTITY_EXIST(viMartin1Veh)
					AND IS_VEHICLE_DRIVEABLE(viMartin1Veh)
						START_PLAYBACK_RECORDED_VEHICLE(viMartin1Veh, 000, "END_MARTIN_1")
						iMartin1Stage++
						
						PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - starting playback.")
						
					ELSE
						iMartin1Stage = 99
						
						PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - couldnt find vehicle.")
					ENDIF
				ELSE
					PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - waiting for carrec to load.")
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT DOES_ENTITY_EXIST(viMartin1Veh)
				iMartin1Stage++
				PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - vehicle dead.")
			ELIF NOT IS_VEHICLE_DRIVEABLE(viMartin1Veh)
				iMartin1Stage++
				PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - vehicle fucked.")
			ELIF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viMartin1Veh)
				iMartin1Stage++
				PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - playback complete.")
			ELSE
				PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - playback ongoing.")
			ENDIF
		BREAK
		CASE 3
			PRINTLN("HANDLE_END_OF_MISSION_MARTIN_1 - finished.")
			IF bMartin1RecordingRequested
				REMOVE_VEHICLE_RECORDING(000, "END_MARTIN_1")
			ENDIF
			viMartin1Veh = NULL
			iMartin1Stage = 0
			g_bHandleEndOfMission_Martin1 = FALSE
		BREAK
		
		// Bail
		CASE 99
			IF bMartin1RecordingRequested
				REMOVE_VEHICLE_RECORDING(000, "END_MARTIN_1")
			ENDIF
			viMartin1Veh = NULL
			iMartin1Stage = 0
			g_bHandleEndOfMission_Martin1 = FALSE
		BREAK
	ENDSWITCH
ENDPROC

INT iFrameAutodoorIndexDockWorker = 0
/// PURPOSE: Ensures Trevor is allowed into docks if he's in dock worker uniform.
PROC CHECK_FOR_TREVOR_AS_DOCK_WORKER_FOR_AUTODOORS()
	AUTOMATIC_DOOR_ENUM doorEnumLimit = AUTODOOR_DOCKS_BACK_GATE_OUT
	IF TIMERB() > 250
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF g_bDocksBackGatesIgnoreTrevorsUniform
				doorEnumLimit = AUTODOOR_DOCKS_FRONT_GATE_OUT
			ENDIF
			IF GET_MISSION_FLAG()
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_setup")) > 0 // Added for 468042 - Will only unlock front dock gates for Trevor in uniform while on docks_setup
					doorEnumLimit = AUTODOOR_DOCKS_FRONT_GATE_OUT
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF VDIST2(g_sAutoDoorData[iFrameAutodoorIndexDockWorker].coords, GET_ENTITY_COORDS(PLAYER_PED_ID())) < g_sAutoDoorData[iFrameAutodoorIndexDockWorker].checkRange
					IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(INT_TO_ENUM(AUTOMATIC_DOOR_ENUM, iFrameAutodoorIndexDockWorker), PLAYER_PED_ID())
						IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DOCK_WORKER)
							CPRINTLN(DEBUG_DOOR, "Trevor is NOT in dock outfit, Unregistered for dock door ", GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_INDEX(iFrameAutodoorIndexDockWorker), ".")
							UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(INT_TO_ENUM(AUTOMATIC_DOOR_ENUM, iFrameAutodoorIndexDockWorker), PLAYER_PED_ID())
						ENDIF
					ELSE
						IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DOCK_WORKER)
							CPRINTLN(DEBUG_DOOR, "Trevor is in dock outfit, registered for dock door ", GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_INDEX(iFrameAutodoorIndexDockWorker), ".")
							REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(INT_TO_ENUM(AUTOMATIC_DOOR_ENUM, iFrameAutodoorIndexDockWorker), PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
				iFrameAutodoorIndexDockWorker++
				IF iFrameAutodoorIndexDockWorker > ENUM_TO_INT(doorEnumLimit)
					iFrameAutodoorIndexDockWorker = ENUM_TO_INT(AUTODOOR_DOCKS_FRONT_GATE_IN)
				ENDIF
			ENDIF
		ENDIF
		SETTIMERB(0)
	ENDIF
ENDPROC

FUNC STRING GET_LABEL_FROM_STRING(STRING sLabel)
	RETURN sLabel
ENDFUNC

PROC SETUP_CONTENT_CHANGE_WITH_COUNTER(INT &iCounter, STRING sLabel)
	iCounter++
	tlContentLabel = GET_LABEL_FROM_STRING(sLabel)
	bDisplayContentFeedMessage = TRUE
ENDPROC

PROC SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(INT &iBitset, INT iBit, STRING sLabel)
	SET_BIT(iBitset, iBit)
	tlContentLabel = GET_LABEL_FROM_STRING(sLabel)
	bDisplayContentFeedMessage = TRUE
ENDPROC


PROC UPDATE_CONTENT_STATE_TATTOOS(BOOL bUnlocked)
	SET_PLAYER_PED_TATTOO_UNLOCKED(CHAR_MICHAEL, TATTOO_SP_MICHAEL_12, bUnlocked)
	SET_PLAYER_PED_TATTOO_UNLOCKED(CHAR_TREVOR, TATTOO_SP_TREVOR_6, bUnlocked)
	SET_PLAYER_PED_TATTOO_UNLOCKED(CHAR_FRANKLIN, TATTOO_SP_FRANKLIN_1, bUnlocked)
ENDPROC

PROC UPDATE_CONTENT_STATE_CLOTHES(BOOL bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_HEAVY_JACKET, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_03, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_3, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_BLACK_SUIT, bUnlocked, bUnlocked, bUnlocked)
	
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_DENIM_JACKET, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_TORSO, TORSO_P2_BOWLING_SHIRT_4, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_DENIM, bUnlocked, bUnlocked, bUnlocked)
	
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_TORSO, TORSO_P1_HOODIE_10, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_TORSO, TORSO_P1_HOODIE_12, bUnlocked, bUnlocked, bUnlocked)
	SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_HOODIE_AND_SWEATS, bUnlocked, bUnlocked, bUnlocked)
ENDPROC

PROC DO_INITIAL_CONTENT_CHECKS()

	#IF IS_JAPANESE_BUILD
		IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
			IF NOT IS_JAPANESE_SPECIAL_EDITION_GAME()
				CPRINTLN(DEBUG_PED_COMP, "DO_INITIAL_CONTENT_CHECKS: Removing Japanese Special Edition content")
				CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
			ENDIF
		ENDIF
	#ENDIF

	#IF NOT IS_JAPANESE_BUILD
		// Remove shared SE/CE edition content if no longer available
		IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
		OR IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION)
			IF NOT IS_SPECIAL_EDITION_GAME()
			AND NOT IS_COLLECTORS_EDITION_GAME()
			
				CPRINTLN(DEBUG_PED_COMP, "DO_INITIAL_CONTENT_CHECKS: Removing Special and Collectors Edition content")
				CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
				CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION)
				
				// Lock tattoos
				UPDATE_CONTENT_STATE_TATTOOS(FALSE)
				
				// Lock clothes
				UPDATE_CONTENT_STATE_CLOTHES(FALSE)
			ENDIF
		ENDIF
	#ENDIF

ENDPROC

FUNC BOOL SHOULD_PROCESS_NEW_CONTENT_CHECK()
	#IF IS_DEBUG_BUILD
	BOOL bPrintDebugInfo = FALSE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_printPlayerControllerLogs")
		bPrintDebugInfo = TRUE
	ENDIF
	#ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_PED_INJURED(PLAYER_PED_ID()) TRUE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_PLAYER_SWITCH_IN_PROGRESS( TRUE)")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_PLAYER_PED_SWITCH_IN_PROGRESS() TRUE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_CUTSCENE_PLAYING() true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_PLAYER_CONTROL_ON(PLAYER_ID()) false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bScriptsSetSafeForCutscene
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK g_bScriptsSetSafeForCutscene")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF iContentCheckStage > iMAX_LAST_ITEMS	// last item
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK iContentCheckStage > iMAX_LAST_ITEMS")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() 
	OR IS_SCREEN_FADED_OUT()				// #1382357
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_SCREEN_FADING_OUT() true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)			// #1382357
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY) true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)	// #1382357
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS) true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)	// #1382357
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR) true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_REPEAT_PLAY_ACTIVE() true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_INTERIOR_SCENE()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_INTERIOR_SCENE() true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_BROWSER_OPEN true")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()	
		#IF IS_DEBUG_BUILD
		IF bPrintDebugInfo
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS// SHOULD_PROCESS_NEW_CONTENT_CHECK IS_SKYSWOOP_AT_GROUND false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DO_NEW_CONTENT_CHECKS()

	//Display DLC removed warning screen (platform specific).
	IF bDisplayContentRemovedWarning
		IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM()
			SET_WARNING_MESSAGE("CONT_REM_PS", FE_WARNING_CONTINUE)
		ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
			SET_WARNING_MESSAGE("CONT_REM_XB", FE_WARNING_CONTINUE)
		ELSE
			SET_WARNING_MESSAGE("CONT_REM_PC", FE_WARNING_CONTINUE)
		ENDIF
		
		IF IS_AUTOSAVE_REQUEST_IN_PROGRESS()
			CLEAR_AUTOSAVE_REQUESTS()
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			bDisplayContentRemovedWarning = FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS 1")
	#ENDIF	

	IF GET_COMMANDLINE_PARAM_EXISTS("sc_printPlayerControllerLogs")
		CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS/iContentCheckTimer: ", GET_GAME_TIMER()-iContentCheckTimer, "s, iContentCheckStage: ", iContentCheckStage)
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	AND (GET_GAME_TIMER() - iContentCheckTimer) > 10000
	//B* 2352887: Don't do new content checks when in Director or Animal Mode
	AND NOT IS_DIRECTOR_MODE_RUNNING(TRUE)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)

		// Make sure we can display to the feed
		IF NOT SHOULD_PROCESS_NEW_CONTENT_CHECK()
			iContentCheckStage = iCONTENT_CHECK_12_NEXT_GEN_PREORDER
			iContentCheckTimer = GET_GAME_TIMER()
			iCacheShopData = 0
		ELSE
		
			INT iDLCClothes, iDLCItem, iDLCTops, iDLCHairdo, iPed
			scrShopPedComponent componentItem
			
			SWITCH iContentCheckStage
				CASE iCONTENT_CHECK_0_SPECIAL_EDITION // Special Edition
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
						IF IS_SPECIAL_EDITION_GAME()
							CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS: Unlocking Special Edition")
							SET_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION) 	// No longer calling SETUP_CONTENT_CHANGE_WITH_BIT_FLAG. 
																										// Just set the bit so we don't load the text label with 
																										// a message we don't want to display.
							// Unlock tattoos
							UPDATE_CONTENT_STATE_TATTOOS(TRUE)
							
							// Unlock clothes
							UPDATE_CONTENT_STATE_CLOTHES(TRUE)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_0_SPECIAL_EDITION")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_1_COLLECTORS_EDITION // Collectors Edition
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION)
						IF IS_COLLECTORS_EDITION_GAME()
							CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS: Unlocking Collectors edition")
							SET_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION) 	// In NG no longer calling SETUP_CONTENT_CHANGE_WITH_BIT_FLAG. 
																											// Just set the bit so we don't load the text label with 
																											// a message we don't want to display.
							// Unlock tattoos
							UPDATE_CONTENT_STATE_TATTOOS(TRUE)
							
							// Unlock clothes
							UPDATE_CONTENT_STATE_CLOTHES(TRUE)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_1_COLLECTORS_EDITION")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_2_SOCIAL_CLUB // Social Club
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SOCIAL_CLUB)
						
						CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//SocialClub - NETWORK_HAS_SOCIAL_CLUB_ACCOUNT():", NETWORK_HAS_SOCIAL_CLUB_ACCOUNT(),
																				   ", NETWORK_IS_SIGNED_ONLINE():", NETWORK_IS_SIGNED_ONLINE(),
																				   ", IS_PS3_VERSION():", IS_PS3_VERSION(),
																				   ", IS_PLAYSTATION_PLATFORM():", IS_PLAYSTATION_PLATFORM(),
																				   ", IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE):", IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE))
						
						IF IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE)
						
						// Fix for 1647136 - Social club content not unlocks for silver accounts.
						OR (NETWORK_HAS_SOCIAL_CLUB_ACCOUNT() AND NETWORK_IS_SIGNED_ONLINE() AND NOT IS_PS3_VERSION() AND NOT IS_PLAYSTATION_PLATFORM())
						
							SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SOCIAL_CLUB, "CONTENT_NEW_SC")
							
							// Unlock hairdos
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_HAIR, HAIR_P0_1_0, TRUE, TRUE)
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_HAIR, HAIR_P1_2_0, TRUE, TRUE)
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_HAIR, HAIR_P2_CURLS, TRUE, TRUE)
							
							// Unlock beards
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_BERD, BERD_P0_4_0, TRUE, TRUE)
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), COMP_TYPE_BERD, BERD_P1_1_0, TRUE, TRUE)
							SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_BERD, BERD_P2_4_0, TRUE, TRUE)
						ENDIF
					
					ELSE
						CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//SocialClub - IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SOCIAL_CLUB)")
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_2_SOCIAL_CLUB")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_3_BLIMP // Blimp
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_BLIMP)
						IF IS_PREORDER_GAME()
						OR IS_COLLECTORS_EDITION_GAME()
						OR IS_SPECIAL_EDITION_GAME()
						OR IS_JAPANESE_SPECIAL_EDITION_GAME()
						OR IS_LAST_GEN_PLAYER()
							IF NOT IS_SCENARIO_GROUP_ENABLED("BLIMP")
								CPRINTLN(DEBUG_INIT, "shop_controller.sc - preorder, CE or SE game - enable Scenario Group \"BLIMP\".")
						    	SET_SCENARIO_GROUP_ENABLED("BLIMP", TRUE)
							ENDIF
							
							SET_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_BLIMP) 	// In NG no longer calling SETUP_CONTENT_CHANGE_WITH_BIT_FLAG. 
																								// Just set the bit so we don't load the text label with 
																								// a message we don't want to display.					
						ENDIF			
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_3_BLIMP")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_4_WEAPONS // Weapons
					IF NOT IS_BIT_SET(iCacheShopData, CACHE_SHOP_WEAPON_DATA_BS)
						INT iDLCWeapons
						INT iWeaponCount, iWeapon
						scrShopWeaponData weaponData
						WEAPON_TYPE eWeaponType
						
						iDLCWeapons = 0
						iWeaponCount = GET_NUM_DLC_WEAPONS()
						REPEAT iWeaponCount iWeapon
							IF GET_DLC_WEAPON_DATA(iWeapon, weaponData)
								IF NOT IS_CONTENT_ITEM_LOCKED(weaponData.m_lockHash)
								AND NOT IS_DLC_WEAPON_LOCKED_BY_SCRIPT(INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash))
									eWeaponType = INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash)
								
									IF eWeaponType != WEAPONTYPE_HAMMER					// Part of the collectors edition content update
									AND eWeaponType != WEAPONTYPE_DLC_PISTOL50			// Part of the collectors edition content update
									AND eWeaponType != WEAPONTYPE_DLC_BULLPUPSHOTGUN	// Part of the collectors edition content update
									AND eWeaponType != WEAPONTYPE_DLC_ASSAULTSMG		// Part of the social club content update
										iDLCWeapons++
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF iDLCWeapons != g_savedGlobals.sShopData.iContentChecks_Weapons
						
							// NOTE !!!!
							
							// - This block of script runs in startup.sc too
							
							// NOTE !!!!
						
							PED_WEAPONS_STRUCT sTempPedWeapons
							PATCH_DLC_WEAPON_LAYOUT(sTempPedWeapons, weaponData)
							
							IF iDLCWeapons > g_savedGlobals.sShopData.iContentChecks_Weapons
								IF (iDLCWeapons - g_savedGlobals.sShopData.iContentChecks_Weapons) = 1
									IF NOT NETWORK_IS_GAME_IN_PROGRESS()
										tlContentLabel = "CONTENT_NEW_W0"
									ELSE
										tlContentLabel = "CONTENT_NEW_WM0"
									ENDIF
								ELSE
									IF NOT NETWORK_IS_GAME_IN_PROGRESS()
										tlContentLabel = "CONTENT_NEW_W1"
									ELSE
										tlContentLabel = "CONTENT_NEW_WM1"
									ENDIF
								ENDIF
							ENDIF
							g_savedGlobals.sShopData.iContentChecks_Weapons = iDLCWeapons
							
							#IF IS_NEXTGEN_BUILD
							bDisplayContentFeedMessage = TRUE
							IF IS_PC_VERSION()
								bDisplayContentFeedMessage = FALSE
							ENDIF
							#ENDIF
						ELIF iDLCWeapons < g_savedGlobals.sShopData.iContentChecks_Weapons
							g_savedGlobals.sShopData.iContentChecks_Weapons = iDLCWeapons
							bDisplayContentRemovedWarning = TRUE
						ENDIF
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_4_WEAPONS")
						#ENDIF	
						#ENDIF
						
						SET_BIT(iCacheShopData, CACHE_SHOP_WEAPON_DATA_BS)
						PRINTLN("DO_NEW_CONTENT_CHECKS CACHE_SHOP_WEAPON_DATA_BS TRUE")
					ENDIF	
				BREAK
				
				CASE iCONTENT_CHECK_5_VEHICLES // Vehicles
					IF NOT IS_BIT_SET(iCacheShopData, CACHE_SHOP_VEHICLE_DATA_BS)
						INT iDLCGarageVehicles, iDLCHangarVehicles, iDLCMarinaVehicles, iDLCHeliVehicles
						INT iVeh
						scrShopVehicleData vehicleData
						iDLCGarageVehicles = 0
						iDLCHangarVehicles = 0
						iDLCMarinaVehicles = 0
						iDLCHeliVehicles = 0
						REPEAT GET_NUM_DLC_VEHICLES() iVeh
							IF GET_DLC_VEHICLE_DATA(iVeh, vehicleData)
								IF INT_TO_ENUM(MODEL_NAMES, vehicleData.m_nameHash) != HOTKNIFE // Part of the collectors edition content update
								AND INT_TO_ENUM(MODEL_NAMES, vehicleData.m_nameHash) != CARBONRS // Part of the collectors edition content update
								AND INT_TO_ENUM(MODEL_NAMES, vehicleData.m_nameHash) != ELEGY2 // Part of the social club content update
									IF NOT IS_CONTENT_ITEM_LOCKED(vehicleData.m_lockHash)
									AND NOT IS_DLC_VEHICLE_LOCKED_BY_SCRIPT(INT_TO_ENUM(MODEL_NAMES, vehicleData.m_nameHash))
										
										VEHICLE_STORAGE_TYPE eVehStorageType
										eVehStorageType = GET_VEHICLE_STORAGE_TYPE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(INT_TO_ENUM(MODEL_NAMES, vehicleData.m_nameHash)))
										
										SWITCH eVehStorageType
											CASE VST_GARAGE	iDLCGarageVehicles++ BREAK
											CASE VST_HANGAR	iDLCHangarVehicles++ BREAK
											CASE VST_MARINA	iDLCMarinaVehicles++ BREAK
											CASE VST_HELI	iDLCHeliVehicles++ BREAK
										ENDSWITCH
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						INT iContentChecks_HangarVehicles, iContentChecks_MarinaVehicles, iContentChecks_HeliVehicles
						STAT_GET_INT(CONTENT_HANGER_VEH, iContentChecks_HangarVehicles)
						STAT_GET_INT(CONTENT_MARINA_VEH, iContentChecks_MarinaVehicles)
						STAT_GET_INT(CONTENT_HELI_VEH, iContentChecks_HeliVehicles)
						
						#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//Vehicles - GarageVehs: ", iDLCGarageVehicles, ", SavedTotal: ", g_savedGlobals.sShopData.iContentChecks_Vehicles)
						CDEBUG3LN(DEBUG_AMBIENT, "                                  HangarVehs: ", iDLCHangarVehicles, ", SavedTotal: ", iContentChecks_HangarVehicles)
						CDEBUG3LN(DEBUG_AMBIENT, "                                  MarinaVehicles: ", iDLCMarinaVehicles, ", SavedTotal: ", iContentChecks_MarinaVehicles)
						CDEBUG3LN(DEBUG_AMBIENT, "                                  HeliVehicles: ", iDLCHeliVehicles, ", SavedTotal: ", iContentChecks_HeliVehicles)
						#ENDIF
						
						IF iContentChecks_HangarVehicles < 0	
						OR iContentChecks_MarinaVehicles < 0
						OR iContentChecks_HeliVehicles < 0
							STAT_SET_INT(CONTENT_HANGER_VEH, iDLCHangarVehicles)	iContentChecks_HangarVehicles = iDLCHangarVehicles
							STAT_SET_INT(CONTENT_MARINA_VEH, iDLCMarinaVehicles)	iContentChecks_MarinaVehicles = iDLCMarinaVehicles
							STAT_SET_INT(CONTENT_HELI_VEH, iDLCHeliVehicles)		iContentChecks_HeliVehicles = iDLCHeliVehicles
							
							g_savedGlobals.sShopData.iContentChecks_Vehicles = iDLCGarageVehicles - (iDLCHangarVehicles+iDLCMarinaVehicles+iDLCHeliVehicles)
							
							CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//Vehicles - unininitialised, update garage: ", g_savedGlobals.sShopData.iContentChecks_Vehicles, ", hangar: ", iContentChecks_HangarVehicles, ", marina: ", iContentChecks_MarinaVehicles, ", heli: ", iContentChecks_HeliVehicles)
							MAKE_AUTOSAVE_REQUEST()
						ENDIF
						
						IF (iDLCGarageVehicles+iDLCHangarVehicles+iDLCMarinaVehicles+iDLCHeliVehicles) > g_savedGlobals.sShopData.iContentChecks_Vehicles+iContentChecks_HangarVehicles+iContentChecks_MarinaVehicles+iContentChecks_HeliVehicles
							IF ((iDLCGarageVehicles+iDLCHangarVehicles+iDLCMarinaVehicles+iDLCHeliVehicles) - g_savedGlobals.sShopData.iContentChecks_Vehicles+iContentChecks_HangarVehicles+iContentChecks_MarinaVehicles+iContentChecks_HeliVehicles) = 1
								IF NOT NETWORK_IS_GAME_IN_PROGRESS()
									IF iDLCGarageVehicles = 1
										tlContentLabel = "CONT_NEW_V"	//A new vehicle has been added to your Garage property.
									ELIF iDLCHangarVehicles = 1
										tlContentLabel = "CONT_NEW_VH"	//A new vehicle has been added to your Hangar property.
									ELIF iDLCMarinaVehicles = 1
										tlContentLabel = "CONT_NEW_VM"	//A new vehicle has been added to your Marina property.
									ELIF iDLCHeliVehicles = 1
										tlContentLabel = "CONT_NEW_VC"	//A new vehicle has been added to your Helipad property.
									ELSE
										SCRIPT_ASSERT("invalid vehicle content check!!!")
										tlContentLabel = "CONT_NEW_V"	//A new vehicle has been added to your Garage property.
									ENDIF
								ELSE
									tlContentLabel = "CONTENT_NEW_VW1"	//A new vehicle has been added for purchase from the Travel and Transport websites.
								ENDIF
							ELSE
								IF NOT NETWORK_IS_GAME_IN_PROGRESS()
									tlContentLabel  = "CONT_NEW_V"
									IF (iDLCGarageVehicles) > g_savedGlobals.sShopData.iContentChecks_Vehicles
										tlContentLabel += "G"
									ENDIF
									IF (iDLCHangarVehicles) > iContentChecks_HangarVehicles
										tlContentLabel += "H"
									ENDIF
									IF (iDLCMarinaVehicles) > iContentChecks_MarinaVehicles
										tlContentLabel += "M"
									ENDIF
									IF (iDLCHeliVehicles) > iContentChecks_HeliVehicles
										tlContentLabel += "C"
									ENDIF
									
									tlContentLabel += "1"
								ELSE
									tlContentLabel = "CONTENT_NEW_VW2"		//New vehicles have been added for purchase from the Travel and Transport websites.
								ENDIF
							ENDIF
							
							CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//Vehicles - tlContentLabel: \"", tlContentLabel, "\"")
							
							g_savedGlobals.sShopData.iContentChecks_Vehicles = iDLCGarageVehicles
							STAT_SET_INT(CONTENT_HANGER_VEH, iDLCHangarVehicles)
							STAT_SET_INT(CONTENT_MARINA_VEH, iDLCMarinaVehicles)
							STAT_SET_INT(CONTENT_HELI_VEH, iDLCHeliVehicles)

							//Fix for 2819142: Disabling vehicle content checks.
							bDisplayContentFeedMessage = FALSE
							
							MAKE_AUTOSAVE_REQUEST()
						ELIF (iDLCGarageVehicles+iDLCHangarVehicles+iDLCMarinaVehicles+iDLCHeliVehicles) < g_savedGlobals.sShopData.iContentChecks_Vehicles+iContentChecks_HangarVehicles+iContentChecks_MarinaVehicles+iContentChecks_HeliVehicles
							CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//Vehicles - tlContentLabel: not used")
							
							g_savedGlobals.sShopData.iContentChecks_Vehicles = iDLCGarageVehicles
							STAT_SET_INT(CONTENT_HANGER_VEH, iDLCHangarVehicles)
							STAT_SET_INT(CONTENT_MARINA_VEH, iDLCMarinaVehicles)
							STAT_SET_INT(CONTENT_HELI_VEH, iDLCHeliVehicles)
							MAKE_AUTOSAVE_REQUEST()
						ELSE
							CDEBUG3LN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS//Vehicles - saved and current stats are equal")
						ENDIF
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_5_VEHICLES")
						#ENDIF	
						#ENDIF
						
						SET_BIT(iCacheShopData, CACHE_SHOP_VEHICLE_DATA_BS)
						PRINTLN("DO_NEW_CONTENT_CHECKS CACHE_SHOP_VEHICLE_DATA_BS TRUE")
					ENDIF	
				BREAK
				
				CASE iCONTENT_CHECK_6_CLOTHES // Clothes
					IF NOT IS_BIT_SET(iCacheShopData, CACHE_SHOP_CLOTHES_DATA_BS)
						INIT_SHOP_PED_COMPONENT(componentItem)
						iDLCTops = 0
						iDLCItem = 0
						iPed = ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM())
						iDLCClothes = SETUP_SHOP_PED_APPAREL_QUERY(iPed, 0, 11, ENUM_TO_INT(SHOP_PED_COMPONENT))
						REPEAT iDLCClothes iDLCItem
							GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
							IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
								IF componentItem.m_eCompType = ENUM_TO_INT(PED_COMP_TORSO)
									iDLCTops++
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF iDLCTops > g_savedGlobals.sShopData.iContentChecks_Clothes
							g_savedGlobals.sShopData.iContentChecks_Clothes = iDLCTops
							tlContentLabel = "CONTENT_NEW_C"
							#IF IS_NEXTGEN_BUILD
							bDisplayContentFeedMessage = TRUE
							#ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_6_CLOTHES")
						#ENDIF	
						#ENDIF
						
						SET_BIT(iCacheShopData, CACHE_SHOP_CLOTHES_DATA_BS)
						PRINTLN("DO_NEW_CONTENT_CHECKS CACHE_SHOP_CLOTHES_DATA_BS TRUE")
					ENDIF	
				BREAK
				
				CASE iCONTENT_CHECK_7_HAIRDOS // Hairdos
					IF NOT IS_BIT_SET(iCacheShopData, CACHE_SHOP_HAIRD_DATA_BS)
					
						INIT_SHOP_PED_COMPONENT(componentItem)
						iDLCHairdo = 0
						iDLCItem = 0
						iPed = ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM())
						iDLCClothes = SETUP_SHOP_PED_APPAREL_QUERY(iPed, 0, 11, ENUM_TO_INT(SHOP_PED_COMPONENT))
						REPEAT iDLCClothes iDLCItem
							GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
							IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
								IF componentItem.m_eCompType = ENUM_TO_INT(PED_COMP_HAIR)
									iDLCHairdo++
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF iDLCHairdo > g_savedGlobals.sShopData.iContentChecks_Hairdos
							g_savedGlobals.sShopData.iContentChecks_Hairdos = iDLCHairdo
							tlContentLabel = "CONTENT_NEW_H"
							#IF IS_NEXTGEN_BUILD
							bDisplayContentFeedMessage = TRUE
							#ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_7_HAIRDOS")
						#ENDIF	
						#ENDIF
						SET_BIT(iCacheShopData, CACHE_SHOP_HAIRD_DATA_BS)
						PRINTLN("DO_NEW_CONTENT_CHECKS CACHE_SHOP_HAIRD_DATA_BS TRUE")
					ENDIF	
				BREAK
				
				CASE iCONTENT_CHECK_8_TATTOOS // Tattoos
					IF NOT IS_BIT_SET(iCacheShopData, CACHE_SHOP_TATTOO_DATA_BS)
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								// Test
								IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Tattoos, 0)
									TATTOO_DATA_STRUCT sTattooData
									IF GET_TATTOO_DATA(sTattooData, TATTOO_SP_MICHAEL_DLC, TATTOO_SP_MICHAEL, PLAYER_PED_ID())
										SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Tattoos, 0, "CONTENT_NEW_T")
									ENDIF
								ENDIF
							BREAK
							CASE CHAR_FRANKLIN
								// Test
								IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Tattoos, 1)
									TATTOO_DATA_STRUCT sTattooData
									IF GET_TATTOO_DATA(sTattooData, TATTOO_SP_FRANKLIN_DLC, TATTOO_SP_FRANKLIN, PLAYER_PED_ID())
										SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Tattoos, 1, "CONTENT_NEW_T")
									ENDIF
								ENDIF
							BREAK
							CASE CHAR_TREVOR
								// Test
								IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Tattoos, 2)
									TATTOO_DATA_STRUCT sTattooData
									IF GET_TATTOO_DATA(sTattooData, TATTOO_SP_TREVOR_DLC, TATTOO_SP_TREVOR, PLAYER_PED_ID())
										SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Tattoos, 2, "CONTENT_NEW_T")
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_8_TATTOOS")
						#ENDIF	
						#ENDIF
						
						SET_BIT(iCacheShopData, CACHE_SHOP_TATTOO_DATA_BS)
						PRINTLN("DO_NEW_CONTENT_CHECKS CACHE_SHOP_TATTOO_DATA_BS TRUE")
					ENDIF	
				BREAK
				
				CASE iCONTENT_CHECK_9_STUNT_PLANES // Stunt Planes minigame
					IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES))
						IF IS_SPECIAL_EDITION_GAME()
						OR IS_COLLECTORS_EDITION_GAME()
						OR IS_JAPANESE_SPECIAL_EDITION_GAME()
							IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
								Execute_Code_ID(CID_ACTIVATE_MINIGAME_STUNT_PLANES)
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_9_STUNT_PLANES")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_10_JAPANESE_SE // Japanese Special Edition
					#IF IS_JAPANESE_BUILD
						IF IS_JAPANESE_SPECIAL_EDITION_GAME()
							//Display the special edition content unlock.
							IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
								CPRINTLN(DEBUG_AMBIENT, "DO_NEW_CONTENT_CHECKS: Unlocking Special Edition")
								SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION, "CONTENT_NEW_SE")
							ENDIF
						ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_10_JAPANESE_SE")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_12_NEXT_GEN_PREORDER	// Next Gen Preorder
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_NG_PREORDER)
						IF IS_NEXT_GEN_PREORDER_GAME()
							SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Game, NCU_NG_PREORDER, "CONT_NG_PREOR")
							Execute_Code_ID(CID_GIVE_NG_PREORDER_REWARD, 0)
							iContentCheckTimer = GET_GAME_TIMER()
						ENDIF			
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_12_NEXT_GEN_PREORDER")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_13_CG_TO_NG	// Current Gen to Next Gen player.
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_CG_TO_NG)
						IF IS_LAST_GEN_PLAYER()
							// url:bugstar:7163505 - Gen9 - Players receive a ticker feed message about exclusive content for returning players although all CG to NG content will soon be unlocked by default for all Gen9 players.
							//SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Game, NCU_CG_TO_NG, "CONT_CG_TO_NG")
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_13_CG_TO_NG")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_14_HEISTS
					IF NOT IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_HEISTS)
						IF IS_MP_HEIST_PACK_PRESENT()
							SETUP_CONTENT_CHANGE_WITH_BIT_FLAG(g_savedGlobals.sShopData.iContentChecks_Game, NCU_HEISTS, "CONT_HEISTS")
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_14_HEISTS")
					#ENDIF	
					#ENDIF
				BREAK
				
				CASE iCONTENT_CHECK_CASH_GIFT
					
					#IF IS_DEBUG_BUILD
						IF g_b_IsFakePCGiftsWidgetActive = TRUE
							BREAK
						ENDIF
					#ENDIF
					
					IF g_i_SCAdminCashGiftScreenType > 0
						BREAK
					ENDIF
					
					
					IF g_b_skipCashGiftMessage = FALSE
					
						IF g_b_DoneScFeedCashGiftMessage = FALSE
						
							PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftMessageAmount = ", g_i_DoCashGiftMessageAmount)
							PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftACTUALAmount = ", g_i_DoCashGiftACTUALAmount)	
							IF g_i_DoCashGiftMessageAmount = 99 //Crazy Numbers
								//Rockstar Game Services have corrected your GTA Dollars by $~1~.
								tlContentLabel = "HUD_1_CASHGIFT"
								iContentCheckStage = NCU_SOCIAL_CLUB
								iContentTickerINT = g_i_DoCashGiftACTUALAmount
								bDisplayContentFeedMessage = TRUE
								g_b_skipCashGiftMessage = FALSE
								PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftMessageAmount = 99 reset everything")
								g_b_DoneScFeedCashGiftMessage = TRUE
								
							ELIF g_i_DoCashGiftMessageAmount = -99
								//Rockstar Game Services have corrected your GTA Dollars.
								tlContentLabel = "HUD_2_CASHGIFT"
								iContentCheckStage = NCU_SOCIAL_CLUB
								bDisplayContentFeedMessage = TRUE
								g_b_skipCashGiftMessage = FALSE
								PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftACTUALAmount = -99 Display non numbered message")
								g_b_DoneScFeedCashGiftMessage = TRUE
								
							ELIF g_i_DoCashGiftMessageAmount > 0
								//Rockstar Game Services have corrected your GTA Dollars by $~1~.
								tlContentLabel = "HUD_1_CASHGIFT"
								iContentCheckStage = NCU_SOCIAL_CLUB
								iContentTickerINT = g_i_DoCashGiftACTUALAmount
								bDisplayContentFeedMessage = TRUE
								g_b_skipCashGiftMessage = FALSE
								PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftMessageAmount > 0 reset everything")
								g_b_DoneScFeedCashGiftMessage = TRUE
							
								
							ELIF g_i_DoCashGiftMessageAmount < 0
								//Rockstar Game Services have corrected your GTA Dollars by -$~1~.
								tlContentLabel = "HUD_1_CASHGIFTN"
								iContentCheckStage = NCU_SOCIAL_CLUB
								iContentTickerINT = g_i_DoCashGiftACTUALAmount
								IF iContentTickerINT < 0
									iContentTickerINT *= -1
								ENDIF
								bDisplayContentFeedMessage = TRUE
								g_b_skipCashGiftMessage = FALSE
								PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_i_DoCashGiftMessageAmount < 0 reset everything")
								g_b_DoneScFeedCashGiftMessage = TRUE
							
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[GIFTTOOL] iCONTENT_CHECK_CASH_GIFT g_b_skipCashGiftMessage = TRUE reset everything")
						g_b_skipCashGiftMessage = FALSE
						g_i_DoCashGiftMessageAmount = 0
						g_i_DoCashGiftACTUALAmount = 0
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iCONTENT_CHECK_CASH_GIFT")
					#ENDIF	
					#ENDIF
				BREAK
				
				
				CASE iMAX_LAST_ITEMS
					iContentCheckStage = iCONTENT_CHECK_0_SPECIAL_EDITION
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH iContentCheckStage iMAX_LAST_ITEMS")
					#ENDIF	
					#ENDIF
					EXIT
				BREAK
			ENDSWITCH
			
			IF bDisplayContentFeedMessage
				IF (iContentCheckStage = NCU_SOCIAL_CLUB)
//					BEGIN_TEXT_COMMAND_THEFEED_POST("MARSTON_TICK") 	//Social Club ~n~~a~ 
//						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlContentLabel) 
//					END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "")
					IF iContentTickerINT = 0
						BEGIN_TEXT_COMMAND_THEFEED_POST(tlContentLabel) 
						END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
					ELSE
						BEGIN_TEXT_COMMAND_THEFEED_POST(tlContentLabel) 
						ADD_TEXT_COMPONENT_INTEGER(iContentTickerINT)
						END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
					ENDIF
				ELSE
					BEGIN_TEXT_COMMAND_THEFEED_POST(tlContentLabel)
					END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
				ENDIF
				
				bDisplayContentFeedMessage = FALSE
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS SWITCH 2")
				#ENDIF	
				#ENDIF
			ENDIF
			
			iContentCheckStage++
		ENDIF
	ENDIF
ENDPROC

SCRIPT

	PRINTSTRING("\nStarting player controller")PRINTNL()

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		PRINTSTRING("...player_controller.sc has been forced to cleanup (SP to MP)")
		PRINTNL()
		
		Script_Cleanup()
	ENDIF

	// Setup some debug widgets
	#IF IS_DEBUG_BUILD
		SETUP_PLAYER_CONTROL_DEBUG_WIDGETS()
	#ENDIF
	
	
	DO_INITIAL_CONTENT_CHECKS()
	
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
		Reset_Beast_Hunt_Variables(sBeastHuntVars)
		//No longer creating Beast Hunt widgets here, this is managed by Update_Widgets
	#ENDIF
	#ENDIF
	
	WHILE (TRUE)
	
		#IF IS_DEBUG_BUILD
			MAINTAIN_PLAYER_CONTROL_DEBUG_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME() 
			#ENDIF
		#ENDIF
		
		// This MUST be called each frame.
		
	
		SWITCH eStage
			// Set any defaults and make sure we are a valid character
			CASE PC_STAGE_INIT
				INITIALISE_PLAYER_PED_STATE()
			BREAK
			
			// The main control logic takes place here
			CASE PC_STAGE_WAIT
//				CHECK_PLAYER_PED_REQUEST()
				
				MONITOR_PLAYER_CUTSCENE_ENTITIES()
		
				IF NOT g_bMagDemoActive
					MONITOR_AMBIENT_PLAYER_CHARS()
					MONITOR_STORED_PLAYER_VEHICLES()
					MONITOR_AVAILABLE_PLAYER_MISSIONS()
					MONITOR_PLAYER_WANTED_RELATIONSHIPS()
				ENDIF
				
				// Moved this block of calls to the wait stage as we were hitting
				// the instruction limit per frame assert.
			BREAK
			
//			// Switching to another ped
//			CASE PC_STAGE_SWITCH
//				MAINTAIN_PLAYER_PED_REQUEST()
//			BREAK
//			
//			CASE PC_STAGE_END
//				// Perform any cleanup routines here
//				//		- Save player data
//			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SWITCH eStage")
		#ENDIF	
		#ENDIF
		
		//Purposefully "hidden" in amongst this script. We want it to be hard
		//for hackers to spot these scripts in decompiled output. -BenR
		#IF FEATURE_SP_DLC_BEAST_SECRET
		#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
			Maintain_Beast_Hunt(sBeastHuntVars)
			
			#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("Maintain_Beast_Hunt")
				#ENDIF	
				Update_Beast_Hunt_Widgets(sBeastHuntVars, widgetID)
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("Update_Beast_Hunt_Widgets")
				#ENDIF	
			#ENDIF
		#ENDIF
		#ENDIF
		
		HANDLE_END_OF_MISSION_MARTIN_1()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("HANDLE_END_OF_MISSION_MARTIN_1")
		#ENDIF	
		#ENDIF	
		
		CHECK_FOR_TREVOR_AS_DOCK_WORKER_FOR_AUTODOORS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CHECK_FOR_TREVOR_AS_DOCK_WORKER_FOR_AUTODOORS")
		#ENDIF	
		#ENDIF	
		
		DO_NEW_CONTENT_CHECKS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("DO_NEW_CONTENT_CHECKS")
		#ENDIF	
		#ENDIF	
		
		CHECK_CONTROLLER_RESET()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CHECK_CONTROLLER_RESET")
		#ENDIF	
		#ENDIF	
		
		// See if we need to set up the defaul clothes info
		IF g_savedGlobals.sPlayerData.sInfo.bDefaultClothesInfoSet = FALSE
			SETUP_DEFAULT_PLAYER_CLOTHES_INFO()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		WAIT(0)

	ENDWHILE
	
ENDSCRIPT

