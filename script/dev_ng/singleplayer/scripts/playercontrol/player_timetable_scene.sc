// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		  - Player_Timetable_Scene file for use – player_timetable_scene.sc			 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_water.sch"

//- script headers	-//
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "dialogue_public.sch"
USING "drunk_public.sch"
USING "player_ped_public.sch"
USING "selector_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"
USING "vehicle_gen_public.sch"
USING "building_control_public.sch"
USING "tv_control_public.sch"
USING "shrink_stat_tracking.sch"
USING "fake_cellphone_public.sch"

//-	private headers	-//
USING "family_private.sch"
USING "family_scene_private.sch"
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
	USING "player_controller_debug.sch"
#ENDIF

USING "push_in_public.sch"

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
//PROC SET_PLAYER_VEHICLE_ALARM_AUDIO_ACTIVE(VEHICLE_INDEX vehID, bool bbb)		//temp for #1549453
//	vehID = vehID
//	bbb = bbb
//ENDPROC

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PT_SCENE_STAGE_ENUM
	WAIT_FOR_PLAYER_PED_TO_UPDATE,
	CONTROL_PLAYER_DURING_SWITCH,
	PLAYER_SCENE_STARTED,
	PLAYER_EXITING_SWITCH,
	PLAYER_ON_PHONE_ANIM,
	PLAYER_HAS_WARDROBE_ANIM,
	
	FINISHED_TIMETABLED_SCENE
ENDENUM
PT_SCENE_STAGE_ENUM	current_player_timetable_scene_stage							= WAIT_FOR_PLAYER_PED_TO_UPDATE

//enumPlayerSceneAnimProgress eSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
PLAYER_TIMETABLE_SCENE_STRUCT	sTimetableScene
structPedsForConversation		MyLocalPedStruct
FAKE_CELLPHONE_DATA				sDaughterFakeCellphoneData
PUSH_IN_DATA					sPushInData
BOOL							bFilledPushInData = FALSE

TEXT_LABEL tSpeechBlock, tSpeechLabel
BOOL bHave_Created_Estab_Shot_Conversation = FALSE
BOOL bHave_Preloaded_Conversation = FALSE
BOOL bHave_Created_Switch_Scene_Conversation = FALSE
BOOL bHave_Played_Switch_Scene_Speech_Facial = FALSE
BOOL bPlayed_Switch_Scene_Context = FALSE
BOOL bShortCircuitedPushin = FALSE // rob

BOOL bStartPreloadPlayerComponentForScene = FALSE

BOOL bKeepCellphoneDisabled = TRUE
BOOL bTriggeredFade = FALSE

//BOOL bHave_Triggered_Music_Event = FALSE

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL				bPlayer_timetable_scene_in_progress									= TRUE
BOOL				bInitialised_timetable_scene										= FALSE
BOOL				bSwitchForcedStartAmbientTV											= FALSE
BOOL				bSwitchSkippedPlaybackTimeDescent, bSwitchSkippedPlaybackTimeHold	= FALSE
BOOL				bSetTanishaFightDoorVisible											= FALSE

INT					iPlayerOutfitPreloadStage											= 0
INT					iLeadinRequestStage													= 0
INT					iBenchcallHangupStage												= 0

BOOL				bRetaskToWander														= FALSE
BOOL				bUpdatedWalkTasks													= FALSE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX			scene_buddy
BOOL				bBuddyPostOutTasksPerformed										= FALSE

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX		scene_veh
VECTOR				vSceneVeh_driveOffset
FLOAT				fSceneVeh_driveSpeed

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX		player_prop, player_extra_prop
OBJECT_INDEX		player_door_l, player_door_r

FLOAT				fPlayerDoorOverridePhase										= 0.95

COVERPOINT_INDEX	CoverIndex

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//
//PTFX_ID				player_ptfx

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL tRecordingName
INT iRecordingNum
FLOAT fRecordingStart = -1, fRecordingSkip = -1, fRecordingStop = -1
FLOAT fSpeedSwitch = -1, fSpeedExit = -1

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//
SCENARIO_BLOCKING_INDEX scenarioBlockID

	//- Cameras(CAMERA_INDEX AND INT) -//
CAMERA_INDEX LoopCamIndex, ExitCamIndex

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//
	
#IF NOT IS_NEXTGEN_BUILD
CONST_INT		iFramesToLockCloth 			240
#ENDIF
#IF IS_NEXTGEN_BUILD
CONST_INT		iFramesToLockCloth 			240
#ENDIF

CONST_INT		iCONST_DRUNK_secDuration	20000	//30000	//10sec
CONST_INT		iCONST_SOBER_secDuration	 2500			//2.5sec
structTimer		tLieInTimer
INT				iShakeTimer = -1

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_timetable_scene_widget
INT				iCurrent_player_timetable_scene_stage
BOOL			bTestWarp
FLOAT			fDebugCamInfoDisplayX = 0.875
FLOAT			fDebugCamInfoDisplayY = 0.875
FLOAT			fDebugCamInfoOffsetY = 0.030

BOOL			bObjectReleasePhaseReset = FALSE
#ENDIF

	//- other Ints(INT) -//
	
	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL PRIVATE_SetRoadsSwitchArea(PED_REQUEST_SCENE_ENUM eScene, BOOL bAreaActive)
	
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	VECTOR vRoadAreaOffset
	
	SWITCH eScene
		CASE PR_SCENE_M_DEFAULT
		CASE PR_SCENE_F_DEFAULT
		CASE PR_SCENE_T_DEFAULT
			IF (sTimetableScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
			OR (sTimetableScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
				IF VMAG(vSceneVeh_driveOffset) > 0.0
					vRoadAreaOffset = <<VMAG(vSceneVeh_driveOffset),VMAG(vSceneVeh_driveOffset),5>>*1.5
				ENDIF
			ENDIF
		BREAK
		
		CASE PR_SCENE_M7_BIKINGJIMMY	vRoadAreaOffset = <<15, 15, 10>> BREAK
		CASE PR_SCENE_M7_TALKTOGUARD	vRoadAreaOffset = <<25, 25, 10>> BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY	vRoadAreaOffset = <<25, 80, 10>> BREAK
		
		CASE PR_SCENE_F_MD_FRANKLIN2	vRoadAreaOffset = <<25, 25, 10>> BREAK
		CASE PR_SCENE_M_MD_FBI2			vRoadAreaOffset = <<25, 25, 10>> BREAK

		CASE PR_SCENE_F_S_EXILE2		RETURN FALSE BREAK
		
		CASE PR_SCENE_Ma_FBI4intro		vRoadAreaOffset = <<25, 25, 10>> BREAK
		
		CASE PR_SCENE_F1_BYETAXI		vRoadAreaOffset = <<20, 20, 10>> BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_NIGHT	vRoadAreaOffset = <<08, 08, 10>> BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	vRoadAreaOffset = <<15, 15, 10>> BREAK
		
		CASE PR_SCENE_T_CR_POLICE_a		vRoadAreaOffset = <<40, 10, 10>> BREAK
		CASE PR_SCENE_T_CN_POLICE_b		vRoadAreaOffset = <<10, 60, 10>> BREAK
		CASE PR_SCENE_T_CN_POLICE_c		vRoadAreaOffset = <<60, 10, 10>> BREAK
		
		CASE PR_SCENE_T_CR_CHASECAR_a	vRoadAreaOffset = <<30, 50, 10>> BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b	vRoadAreaOffset = <<60, 20, 10>> BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE	vRoadAreaOffset = <<20, 50, 10>> BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER	vRoadAreaOffset = <<25, 12, 10>> BREAK
		
		CASE PR_SCENE_M2_CYCLING_a		vRoadAreaOffset = <<15, 15, 10>> BREAK
		CASE PR_SCENE_M2_CYCLING_b		vRoadAreaOffset = <<15, 15, 10>> BREAK
		CASE PR_SCENE_M2_CYCLING_c		vRoadAreaOffset = <<15, 15, 10>> BREAK
		
		CASE PR_SCENE_M_TRAFFIC_a		vRoadAreaOffset = <<60, 60, 20>> BREAK
		CASE PR_SCENE_M_TRAFFIC_b		vRoadAreaOffset = <<60, 60, 20>> BREAK
		CASE PR_SCENE_M_TRAFFIC_c		vRoadAreaOffset = <<60, 60, 20>> BREAK
		
		CASE PR_SCENE_F_TRAFFIC_a		vRoadAreaOffset = <<60, 60, 20>> BREAK
		CASE PR_SCENE_Fa_AGENCY1		RETURN PRIVATE_SetRoadsSwitchArea(PR_SCENE_F_TRAFFIC_a, bAreaActive) BREAK
		CASE PR_SCENE_F_TRAFFIC_b		vRoadAreaOffset = <<60, 60, 20>> BREAK
		CASE PR_SCENE_F_TRAFFIC_c		vRoadAreaOffset = <<60, 60, 20>> BREAK
		
		CASE PR_SCENE_Ta_CARSTEAL2
			BUILDING_NAME_ENUM eBuildingName
			eBuildingName = BUILDINGNAME_IPL_CARGEN_BLOCK_POLMAV_DT1_17
			
			IF NOT bAreaActive
				SET_BUILDING_STATE(eBuildingName, BUILDINGSTATE_NORMAL)
			ELSE
				SET_BUILDING_STATE(eBuildingName, BUILDINGSTATE_DESTROYED)
			ENDIF
			
			vRoadAreaOffset = <<50, 50, 50>>
		BREAK
		CASE PR_SCENE_Ta_FBI2
		CASE PR_SCENE_T_FLYING_PLANE
			vRoadAreaOffset = <<50, 50, 50>>
		BREAK
		CASE PR_SCENE_Ma_FBI1end
		CASE PR_SCENE_Ma_FBI3
		CASE PR_SCENE_Ma_FINALE2B
		CASE PR_SCENE_Ta_FINALEC
		CASE PR_SCENE_Fa_MICHAEL3
		CASE PR_SCENE_Ta_FAMILY4
		CASE PR_SCENE_Ta_FRANKLIN2
		CASE PR_SCENE_Fa_AGENCYprep1
		CASE PR_SCENE_Fa_FBI4intro
		CASE PR_SCENE_Fa_FBI4
		CASE PR_SCENE_Fa_EXILE3
		CASE PR_SCENE_Fa_EXILE2
		CASE PR_SCENE_Fa_FAMILY3
			vRoadAreaOffset = <<15, 15, 5>>
		BREAK
		CASE PR_SCENE_Ma_DOCKS2A 
		CASE PR_SCENE_Ma_DOCKS2B
		CASE PR_SCENE_Fa_DOCKS2A
		CASE PR_SCENE_M6_BOATING
		CASE PR_SCENE_Fa_FINALE2intro
		CASE PR_SCENE_Ma_FINALE1
		CASE PR_SCENE_Fa_AGENCY3A
			vRoadAreaOffset = <<20, 20, 10>>
		BREAK
		CASE PR_SCENE_Ma_FRANKLIN2
		CASE PR_SCENE_Fa_FBI1
		CASE PR_SCENE_Fa_RURAL2A
		CASE PR_SCENE_Fa_FBI5
		CASE PR_SCENE_M2_DRIVING_a	
		CASE PR_SCENE_M2_DRIVING_b	
		CASE PR_SCENE_M6_DRIVING_a	
		CASE PR_SCENE_M6_DRIVING_b	
		CASE PR_SCENE_M6_DRIVING_c	
		CASE PR_SCENE_M6_DRIVING_d	
		CASE PR_SCENE_M6_DRIVING_e	
		CASE PR_SCENE_M6_DRIVING_f	
		CASE PR_SCENE_M6_DRIVING_g	
		CASE PR_SCENE_M6_DRIVING_h
			vRoadAreaOffset = <<25, 25, 10>>
		BREAK
		
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum > 0)
				
				VECTOR vRecordingEnd
				vRecordingEnd = <<10,10,10>>
				
				REQUEST_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum, tRecordingName)
					vRecordingEnd = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum, fRecordingStop, tRecordingName)
				ENDIF
				
				vRoadAreaOffset = vRecordingEnd-vCreateCoords
				
				vRoadAreaOffset.x = ABSF(vRoadAreaOffset.x)
				vRoadAreaOffset.y = ABSF(vRoadAreaOffset.y)
				vRoadAreaOffset.z = ABSF(vRoadAreaOffset.z)
				
				IF vRoadAreaOffset.x < 10.0	vRoadAreaOffset.x = 10.0 ENDIF
				IF vRoadAreaOffset.y < 10.0	vRoadAreaOffset.y = 10.0 ENDIF
				IF vRoadAreaOffset.z < 10.0	vRoadAreaOffset.z = 10.0 ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("CASE ")
				SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				SAVE_STRING_TO_DEBUG_FILE("		vRoadAreaOffset = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vRoadAreaOffset)
				SAVE_STRING_TO_DEBUG_FILE(" BREAK")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				TEXT_LABEL_63 sScene, sInvalid
				sScene = Get_String_From_Ped_Request_Scene_Enum(eScene)
				sInvalid  = "SetRoadsSwitchArea() required for eScene: "
				sInvalid += GET_STRING_FROM_STRING(sScene,
						GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
						GET_LENGTH_OF_LITERAL_STRING(sScene))
				
				CPRINTLN(DEBUG_SWITCH, sInvalid)
				CASSERTLN(DEBUG_SWITCH, sInvalid)
				
				IF NOT bAreaActive
					SET_ROADS_IN_AREA(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset, bAreaActive)
				ELSE
					SET_ROADS_BACK_TO_ORIGINAL(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset)
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			IF (sTimetableScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
			AND (sTimetableScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
				IF VMAG(vSceneVeh_driveOffset) > 0.0
					vRoadAreaOffset = <<VMAG(vSceneVeh_driveOffset),VMAG(vSceneVeh_driveOffset),5>>
					
					IF NOT bAreaActive
						SAVE_STRING_TO_DEBUG_FILE("CASE ")
						SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
						SAVE_STRING_TO_DEBUG_FILE("		vRoadAreaOffset = ")
						SAVE_VECTOR_TO_DEBUG_FILE(vRoadAreaOffset)
						SAVE_STRING_TO_DEBUG_FILE(" BREAK")
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						TEXT_LABEL_63 sScene, sInvalid
						sScene = Get_String_From_Ped_Request_Scene_Enum(eScene)
						sInvalid  = "SetRoadsSwitchArea() required for eScene: "
						sInvalid += GET_STRING_FROM_STRING(sScene,
								GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
								GET_LENGTH_OF_LITERAL_STRING(sScene))
						
						CPRINTLN(DEBUG_SWITCH, sInvalid)
						CASSERTLN(DEBUG_SWITCH, sInvalid)
					ENDIF
					
					IF NOT bAreaActive
						SET_ROADS_IN_AREA(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset, bAreaActive)
					ELSE
						SET_ROADS_BACK_TO_ORIGINAL(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			#ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF NOT bAreaActive
		SET_ROADS_IN_AREA(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset, bAreaActive)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset, bAreaActive)
	ELSE
		SET_ROADS_BACK_TO_ORIGINAL(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vCreateCoords-vRoadAreaOffset, vCreateCoords+vRoadAreaOffset, bAreaActive)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "PRIVATE_SetRoadsSwitchArea(", Get_String_From_Ped_Request_Scene_Enum(eScene), ", ", vCreateCoords-vRoadAreaOffset, ", ", vCreateCoords+vRoadAreaOffset, ", ", bAreaActive, ")")
	#ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL PRIVATE_SetPedInSwitchArea(PED_REQUEST_SCENE_ENUM eScene, BOOL bAreaActive)
	
	VECTOR vPedAreaOffset
	
	SWITCH eScene
		CASE PR_SCENE_M_DEFAULT			RETURN FALSE BREAK
		CASE PR_SCENE_F_DEFAULT			RETURN FALSE BREAK
		CASE PR_SCENE_T_DEFAULT			RETURN FALSE BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	vPedAreaOffset = <<3,3,4>>		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_Fa_STRIPCLUB_ARM3, bAreaActive)
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3	RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_Fa_STRIPCLUB_ARM3, bAreaActive)
		
		CASE PR_SCENE_M_VWOODPARK_a		vPedAreaOffset = <<4,4,4>>		BREAK
		CASE PR_SCENE_M_VWOODPARK_b		RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_M_VWOODPARK_a, bAreaActive)
		
		CASE PR_SCENE_M_BENCHCALL_b		vPedAreaOffset = <<12,12,4>>		BREAK
		
		CASE PR_SCENE_F_CLUB			vPedAreaOffset = <<10,10,4>>	BREAK
		
		CASE PR_SCENE_F_LAMTAUNT_P5		vPedAreaOffset = <<15,15,4>>	BREAK
		
		CASE PR_SCENE_T_SC_BAR			vPedAreaOffset = <<4,4,4>>		BREAK
		
		CASE PR_SCENE_M_COFFEE_a		vPedAreaOffset = <<2.5,2,4>>	BREAK
		CASE PR_SCENE_M_COFFEE_b		vPedAreaOffset = <<4,4,4>>		BREAK
		CASE PR_SCENE_M_COFFEE_c		RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_M_COFFEE_a, bAreaActive)
		CASE PR_SCENE_M4_CINEMA			RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_M_COFFEE_a, bAreaActive)
		CASE PR_SCENE_M7_COFFEE			vPedAreaOffset = <<4,4,4>>		BREAK
		CASE PR_SCENE_M4_LUNCH_b		vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_M7_REJECTENTRY	vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO
			SET_SCENARIO_GROUP_ENABLED("MOVIE_STUDIO_SECURITY", bAreaActive)
			vPedAreaOffset = <<6,6,4>>
		BREAK
		CASE PR_SCENE_T_FIGHTBBUILD		vPedAreaOffset = <<5,5,4>>		BREAK
		CASE PR_SCENE_T_FIGHTBAR_a		vPedAreaOffset = <<8,8,4>>		BREAK
		CASE PR_SCENE_T_FIGHTBAR_b		vPedAreaOffset = <<8,8,4>>		BREAK
		CASE PR_SCENE_T_FIGHTBAR_c		vPedAreaOffset = <<8,8,4>>		BREAK	//1260251
		CASE PR_SCENE_T_YELLATDOORMAN	vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP	vPedAreaOffset = <<15,15,10>>	BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b	vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_T_FIGHTCASINO		vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT	vPedAreaOffset = <<6,6,4>>		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA	vPedAreaOffset = <<14,6,4>>		BREAK
		
		CASE PR_SCENE_T6_HUNTING1		vPedAreaOffset = <<25,25,4>>	BREAK
		CASE PR_SCENE_T6_HUNTING2		RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_T6_HUNTING1, bAreaActive)
		CASE PR_SCENE_T6_HUNTING3		RETURN PRIVATE_SetPedInSwitchArea(PR_SCENE_T6_HUNTING1, bAreaActive)
		
		DEFAULT
			vPedAreaOffset = <<4,4,4>>
		BREAK
	ENDSWITCH
	
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	SET_PED_PATHS_IN_AREA(vCreateCoords-vPedAreaOffset, vCreateCoords+vPedAreaOffset, bAreaActive)
	IF NOT bAreaActive
		SET_PED_NON_CREATION_AREA(vCreateCoords-vPedAreaOffset, vCreateCoords+vPedAreaOffset)
		scenarioBlockID = ADD_SCENARIO_BLOCKING_AREA(vCreateCoords-vPedAreaOffset, vCreateCoords+vPedAreaOffset)
		
		CLEAR_AREA(vCreateCoords, VMAG(vPedAreaOffset), TRUE)
	ELSE
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockID)
		CLEAR_PED_NON_CREATION_AREA()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "PRIVATE_SetPedInSwitchArea(", Get_String_From_Ped_Request_Scene_Enum(eScene), ", ", vCreateCoords-vPedAreaOffset, ", ", vCreateCoords+vPedAreaOffset, ", ", bAreaActive, ")")
	#ENDIF
	
	RETURN TRUE
ENDFUNC

PROC ar_SET_VEHICLE_FORWARD_SPEED(VEHICLE_INDEX VehicleIndex, FLOAT CarSpeed)

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "ar_SET_", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(VehicleIndex)), "_FORWARD_SPEED[", CarSpeed, "]")
	#ENDIF
	
	IF CarSpeed < 0.1
		CPRINTLN(DEBUG_SWITCH, "setting the forward speed < 0.1??")
		EXIT
	ENDIF
	
	SET_VEHICLE_FORWARD_SPEED(VehicleIndex, CarSpeed)
ENDPROC


FUNC BOOL IS_THIS_MODEL_A_PLANE_OR_BLIMP(MODEL_NAMES VehicleModelHashKey)
	IF IS_THIS_MODEL_A_PLANE(VehicleModelHashKey)
		RETURN TRUE
	ENDIF
	
	IF (VehicleModelHashKey = BLIMP OR vehicleModelHashKey = BLIMP2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ar_SET_VEHICLE_ON_GROUND_PROPERLY(VEHICLE_INDEX VehicleIndex)
	MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(VehicleIndex)
	
	IF IS_THIS_MODEL_A_PLANE_OR_BLIMP(eVehicleModel)
	OR IS_THIS_MODEL_A_HELI(eVehicleModel)
//		IF IS_ENTITY_IN_AIR(VehicleIndex)
			EXIT
//		ENDIF
	ENDIF
	
	//B* 2334165: If it's a beached boat, don't try to position safely on the ground - it WILL get stuck
	VECTOR vPos
	FLOAT fGroundZ, fWaterZ
	vPos = GET_ENTITY_COORDS(vehicleIndex)
	GET_GROUND_Z_FOR_3D_COORD(vPos,fGroundZ)
	GET_WATER_HEIGHT_NO_WAVES(vPos,fWaterZ)
	
	IF NOT (IS_THIS_MODEL_A_BOAT(eVehicleModel) OR eVehicleModel = SUBMERSIBLE)
		SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
	ELSE
		IF fGroundZ <> 0 AND IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF vPos.z > fGroundZ AND fGroundZ > fWaterZ
				FREEZE_ENTITY_POSITION(VehicleIndex,FALSE)
				SET_ENTITY_DYNAMIC(VehicleIndex, TRUE)
				APPLY_FORCE_TO_ENTITY(VehicleIndex,APPLY_TYPE_FORCE,<<0,0,0.1>>,<<0,0,0>>,0,FALSE,TRUE,TRUE)
			ELSE
				SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ar_FREEZE_ENTITY_POSITION(ENTITY_INDEX EntityIndex, BOOL FrozenByScriptFlag, STRING FrozenByScriptTitle)
	
	IF IS_ENTITY_DEAD(EntityIndex)
		EXIT
	ENDIF
	
	MODEL_NAMES EntityModel = GET_ENTITY_MODEL(EntityIndex)
	
	IF (EntityModel = V_ILEV_M_DINECHAIR)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	STRING sFrozenByScriptFlag
	IF FrozenByScriptFlag
		sFrozenByScriptFlag = "TRUE"
	ELSE
		sFrozenByScriptFlag = "FALSE"
	ENDIF
	VECTOR v = GET_ENTITY_COORDS(EntityIndex)
	IF IS_ENTITY_A_PED(EntityIndex)
		CPRINTLN(DEBUG_SWITCH, "ar_FREEZE_PED_", SAFE_GET_MODEL_NAME_FOR_DEBUG(EntityModel), "_POSITION[", sFrozenByScriptFlag, "], \"", FrozenByScriptTitle, "\" ", v)
	ELIF IS_ENTITY_A_VEHICLE(EntityIndex)
		CPRINTLN(DEBUG_SWITCH, "ar_FREEZE_VEH_", SAFE_GET_MODEL_NAME_FOR_DEBUG(EntityModel), "_POSITION[", sFrozenByScriptFlag, "], \"", FrozenByScriptTitle, "\" ", v)
	ELSE
		CPRINTLN(DEBUG_SWITCH, "ar_FREEZE_ENTITY_", SAFE_GET_MODEL_NAME_FOR_DEBUG(EntityModel), "_POSITION[", sFrozenByScriptFlag, "], \"", FrozenByScriptTitle, "\" ", v)
	ENDIF
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	FrozenByScriptTitle = FrozenByScriptTitle
	#ENDIF
	
	IF EntityModel = V_RES_TT_FLUSHER		//PR_SCENE_T6_FLUSHESFOOT
	OR EntityModel = V_ILEV_FH_DINEEAMESA	//PR_SCENE_F1_ONLAPTOP
		EXIT
	ENDIF
	
	FREEZE_ENTITY_POSITION(EntityIndex, FrozenByScriptFlag)
	
	IF IS_ENTITY_A_VEHICLE(EntityIndex)
		g_bVehFreezingUnderSwitchControl = TRUE
		IF NOT FrozenByScriptFlag
			ar_SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntityIndex))
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Timetable_Scene_Cleanup(BOOL bCleanupForMP, BOOL bCleanupForRepeatPlay)

	CPRINTLN(DEBUG_SWITCH, "Player_Timetable_Scene_Cleanup(bCleanupForMP:", GET_STRING_FROM_BOOL(bCleanupForMP), ", bCleanupForRepeatPlay:", GET_STRING_FROM_BOOL(bCleanupForRepeatPlay), ")")
	
	//- destroy all scripted cams -//
	IF DOES_CAM_EXIST(LoopCamIndex)
		DESTROY_CAM(LoopCamIndex)
	ENDIF
	IF DOES_CAM_EXIST(ExitCamIndex)
		DESTROY_CAM(ExitCamIndex)
	ENDIF
	RESET_PUSH_IN(sPushInData)
	
	//- mark peds as no longer needed -//
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF (sTimetableScene.eOutTask <> SCRIPT_TASK_USE_MOBILE_PHONE)
			IF NOT bCleanupForMP			//#1673304
			AND NOT bCleanupForRepeatPlay
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				CPRINTLN(DEBUG_SWITCH, "not bCleanupForMP")
			ELSE
				CPRINTLN(DEBUG_SWITCH, "is bCleanupForMP = ignore cleanup tasks")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_SWITCH, "sTimetableScene.eOutTask = SCRIPT_TASK_USE_MOBILE_PHONE = ignore cleanup tasks")
		ENDIF
	ENDIF
	
	//- mark vehicle as no longer needed -//
	g_bVehFreezingUnderSwitchControl = FALSE
	g_vPlayerVeh[sTimetableScene.sScene.ePed] = scene_veh
	IF DOES_ENTITY_EXIST(scene_veh)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scene_veh,FALSE)
		STORE_VEH_DATA_FROM_VEH(PLAYER_PED_ID(), scene_veh,
				g_sPlayerLastVeh[sTimetableScene.sScene.ePed],
				g_vPlayerLastVehCoord[sTimetableScene.sScene.ePed],
				g_fPlayerLastVehHead[sTimetableScene.sScene.ePed],
				g_ePlayerLastVehState[sTimetableScene.sScene.ePed],
				g_ePlayerLastVehGen[sTimetableScene.sScene.ePed])
		
		IF NOT IS_ENTITY_DEAD(scene_veh)
//			IF g_ePlayerLastVehState[sTimetableScene.sScene.ePed] = PTVS_1_playerWithVehicle
//				IF vdist(GET_ENTITY_COORDS(scene_veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 10.0
//					SET_MISSION_VEHICLE_GEN_VEHICLE(scene_veh, GET_ENTITY_COORDS(scene_veh), GET_ENTITY_HEADING(scene_veh))
//				ENDIF
//			ENDIF
//			IF g_ePlayerLastVehState[sTimetableScene.sScene.ePed] = PTVS_2_playerInVehicle
//				SET_MISSION_VEHICLE_GEN_VEHICLE(scene_veh, GET_ENTITY_COORDS(scene_veh), GET_ENTITY_HEADING(scene_veh))
//			ENDIF
			SET_VEHICLE_LIGHTS(scene_veh, NO_VEHICLE_LIGHT_OVERRIDE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, FALSE)
		ENDIF
		
		/*
		FREEZE_ENTITY_POSITION(scene_veh, FALSE)
		*/
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
			STOP_PLAYBACK_RECORDED_VEHICLE(scene_veh)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene.STOP_PLAYBACK_RECORDED_VEHICLE(scene_veh)")
		ENDIF
		
		SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(scene_veh, FALSE)
		SET_SCENE_VEH_RADIO_STATION(scene_veh, sTimetableScene.sScene.eScene, FALSE)
		
		IF bCleanupForRepeatPlay
			DELETE_VEHICLE(scene_veh)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SWITCH, "player_timetable_scene vehicle does not belong to script ignoring")
	ENDIF
//	IF DOES_ENTITY_EXIST(scene_extra_veh)
//		IF NOT IS_ENTITY_DEAD(scene_extra_veh)
//			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_extra_veh, FALSE)
//		ENDIF
//		
//		FREEZE_ENTITY_POSITION(scene_extra_veh, FALSE)
//	ENDIF
	
	//- mark objects as no longer needed -//
	
	IF NOT CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
		IF DOES_ENTITY_EXIST(player_prop)
			MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
			VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
			PED_BONETAG eBonetag = BONETAG_NULL
			FLOAT fDetachAnimPhase = -1.0
			enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
			
			IF GET_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
						eObjectModel, vecOffset, vecRotation, eBonetag,
						fDetachAnimPhase, thisSceneObjectAction)
				SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
			ENDIF
			
			IF bCleanupForRepeatPlay
			AND DOES_ENTITY_EXIST(player_prop)
				DELETE_OBJECT(player_prop)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(player_extra_prop)
			MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
			VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
			PED_BONETAG eBonetag = BONETAG_NULL
			FLOAT fDetachAnimPhase = -1.0
			enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
			
			BOOL bAttachedToBuddy
			IF GET_EXTRA_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
						eObjectModel, vecOffset, vecRotation, eBonetag,
						fDetachAnimPhase, thisSceneObjectAction, bAttachedToBuddy)
				SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_extra_prop, thisSceneObjectAction)
			ENDIF
			
			IF bCleanupForRepeatPlay
			AND DOES_ENTITY_EXIST(player_extra_prop)
				DELETE_OBJECT(player_extra_prop)
			ENDIF
		ENDIF
	ENDIF
	
	MODEL_NAMES eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES eDoorModel_R = DUMMY_MODEL_FOR_SCRIPT
	
	VECTOR vDoorOffset_L = <<0,0,0>>, vDoorOffset_R= <<0,0,0>>
	DOOR_HASH_ENUM eDoorHash_L, eDoorHash_R
	BOOL bReplaceDoor = TRUE
	FLOAT fHideDoorRadius = -1.0
	
	IF GET_DOORS_FOR_SCENE(sTimetableScene.sScene.eScene,
			eDoorModel_L, eDoorModel_R, vDoorOffset_L, vDoorOffset_R, eDoorHash_L, eDoorHash_R,
			bReplaceDoor, fHideDoorRadius)
		IF DOES_ENTITY_EXIST(player_door_l)
			IF bReplaceDoor
			OR bCleanupForRepeatPlay
				IF (fHideDoorRadius > 0)
				OR bCleanupForRepeatPlay
					DELETE_OBJECT(player_door_l)
					REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_L, fHideDoorRadius, eDoorModel_L)
				ENDIF
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_l)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(player_door_r)
			IF bReplaceDoor
			OR bCleanupForRepeatPlay
				IF (fHideDoorRadius > 0)
				OR bCleanupForRepeatPlay
					DELETE_OBJECT(player_door_r)
					REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_R, fHideDoorRadius, eDoorModel_r)
				ENDIF
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_r)
			ENDIF
		ENDIF
		
		IF (eDoorHash_L <> DUMMY_DOORHASH)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_L))
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L), 0.0, default, TRUE)
			ENDIF
		ENDIF
		IF (eDoorHash_R <> DUMMY_DOORHASH)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_R))
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R), 0.0, default, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	INT RoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
	IF (RoomKey = GET_HASH_KEY("V_35_SweatlRmEMPTY")
	OR RoomKey = GET_HASH_KEY("V_35_OfficeRm")
	OR RoomKey = GET_HASH_KEY("V_35_HallRm")
	OR RoomKey = GET_HASH_KEY("V_35_SweatlRm"))		//-1906407714
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		DOOR_NAME_ENUM eDoor
		REPEAT NUMBER_OF_DOORS eDoor
			DOOR_DATA_STRUCT sData = GET_DOOR_DATA(eDoor)
			
			IF VDIST2(vPlayerCoord, sData.coords) < (50.0*50.0)
				SET_DOOR_STATE(eDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
				
				CPRINTLN(DEBUG_SWITCH, "unlock doors ", ENUM_TO_INT(eDoor))
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_SWITCH, "CLEANUP - unlock all doors for this switch!")
		
	ELSE
		#IF IS_DEBUG_BUILD
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		CPRINTLN(DEBUG_SWITCH, "CLEANUP - dont touch the doors - room hash key: \"", RoomKey, "\" ", vPlayerCoord.x, ", ", vPlayerCoord.y, ", ", vPlayerCoord.z, "!")
		#ENDIF
	ENDIF
	
	TEXT_LABEL_63 entitySetName
	BOOL bActivateSet
	IF SETUP_INTERIOR_ENTITY_SET_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, entitySetName, bActivateSet)
	ENDIF
	
	TEXT_LABEL TCmod
	IF SETUP_TIMECYCLE_MOD_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, TCmod)
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.5)
	ENDIF
	
//	TEXT_LABEL_63 fxName
//	IF SETUP_PTFX_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, fxName)
//		STOP_PARTICLE_FX_LOOPED(player_ptfx)
//		player_ptfx = NULL
//	ENDIF
	
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	IF bInitialised_timetable_scene
		TEXT_LABEL_63 tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
		ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
		//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
		
		IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
				tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
				playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
			REMOVE_ANIM_DICT(tPlayerSceneAnimDict)
		ENDIF
			
		//this is not ideal but it will assert if she is deleted earlier
		IF (sTimetableScene.sScene.eScene =  PR_SCENE_F0_TANISHAFIGHT)
//		OR (sTimetableScene.sScene.eScene =  PR_SCENE_F_CLUB)
			
			
			TEXT_LABEL_31 sPlayerTimetableAdditional_script
			IF NOT SETUP_TIMETABLE_SCRIPT_FOR_SCENE(sTimetableScene.sScene.eScene, sPlayerTimetableAdditional_script)
			OR bCleanupForRepeatPlay
				IF DOES_ENTITY_EXIST(scene_buddy)
					DELETE_PED(scene_buddy)
				ENDIF
			ENDIF
			
			//TODO 1052639: Chat call to Lamar after the Tanisha switch scene.
			IF (sTimetableScene.sScene.eScene =  PR_SCENE_F0_TANISHAFIGHT)
				REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CHAT_LAM09, BIT_FRANKLIN, CHAR_LAMAR, 3, 60000)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(scene_buddy)
				SET_PED_LOD_MULTIPLIER(scene_buddy, 1.0)	//#1510547
				
				IF (sTimetableScene.eSceneBuddy <> CHAR_CHOP)
					SET_PED_CONFIG_FLAG(scene_buddy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
					
					CPRINTLN(DEBUG_SWITCH, "SET_PED_CONFIG_FLAG(scene_buddy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)")
				ENDIF
			ENDIF
			
			g_pScene_buddy = scene_buddy
		ENDIF
		
		IF (sTimetableScene.eSceneBuddy <> NO_CHARACTER)
			IF (sTimetableScene.eBuddyLoopTask = script_task_play_anim)
			OR (sTimetableScene.eBuddyOutTask = script_task_play_anim)
				TEXT_LABEL_63 tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut
				ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
				IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
						buddyAnimLoopFlag, buddyAnimOutFlag)
					REMOVE_ANIM_DICT(tSceneBuddyAnimDict)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//- remove vehicle recordings from the memory -//

	IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
	AND (iRecordingNum > 0)
		REMOVE_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   * set wanted level to '", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()), "' on cleanup")
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
	ENDIF
	
	//- clear sequences -//
	ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Player_Timetable_Scene_Cleanup")
	
	TEXT_LABEL_63 tBankName = "", tBankBName = ""
	IF SETUP_AUDIO_BANK_FOR_SCENE(sTimetableScene.sScene.eScene, tBankName, tBankBName)
		RELEASE_AMBIENT_AUDIO_BANK()
		UNREGISTER_SCRIPT_WITH_AUDIO()
	ENDIF
	
	TEXT_LABEL_63 tAudioEvent = ""
	INT iStartOffsetMs = 0
	IF SETUP_SYNCH_AUDIO_EVENT_FOR_SCENE(sTimetableScene.sScene.eScene, tAudioEvent, iStartOffsetMs)
		IF g_iPlayer_Timetable_Exit_SynchSceneID != -1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
				STOP_SYNCHRONIZED_AUDIO_EVENT(g_iPlayer_Timetable_Exit_SynchSceneID)
			ENDIF
		ENDIF
	ENDIF
	
	IF SETUP_AUDIO_SCENE_FOR_SCENE(sTimetableScene.sScene.eScene, tAudioEvent)
		//doNothing
	ENDIF
	
	INT Duration, MinFrequency, MaxFrequency
	IF SETUP_SYNCH_SHAKE_EVENT_FOR_SCENE(sTimetableScene.sScene.eScene, Duration, MinFrequency, MaxFrequency)
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	ENDIF
	IF SETUP_FORCE_STEP_TYPE_FOR_SCENE(sTimetableScene.sScene.eScene)
		SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), FALSE, 0,0)
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	SET_RADAR_ZOOM(0)		//712364
	
	RELEASE_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
	
	IF bSwitchForcedStartAmbientTV
		TEXT_LABEL_31 RoomName = ""
		FLOAT fTurnOffTVPhase = 0.0
		TV_LOCATION eRoomTVLocation
		TVCHANNELTYPE eTVChannelType
		TV_CHANNEL_PLAYLIST eTVPlaylist

		IF CONTROL_PLAYER_WATCHING_TV(sTimetableScene.sScene.eScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
		
			STOP_AMBIENT_TV_PLAYBACK(eRoomTVLocation)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> STOP_AMBIENT_TV_PLAYBACK()")
			#ENDIF
		ENDIF
	ENDIF
	
	IF (sTimetableScene.sScene.eScene = PR_SCENE_Fa_PHONECALL_ARM3)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_Fa_PHONECALL_FAM1)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_Fa_PHONECALL_FAM3)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_ARM3)
//	OR (sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_FAM1)
//	OR (sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_FAM3)
		IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TENNIS))
			EXECUTE_CODE_ID(CID_ACTIVATE_MINIGAME_POST_ARM3_BLOCK)
		ENDIF
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE, FALSE)
		
		SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
		
		
		IF (g_sPlayerLastVeh[CHAR_MICHAEL].model = BJXL)
		AND (g_ePlayerLastVehState[CHAR_MICHAEL] = PTVS_1_playerWithVehicle)
			g_sPlayerLastVeh[CHAR_MICHAEL].model = DUMMY_MODEL_FOR_SCRIPT
			g_ePlayerLastVehState[CHAR_MICHAEL] = PTVS_0_noVehicle
			CPRINTLN(DEBUG_SWITCH, "reset michaels BJXL")
		ENDIF
		IF (g_sPlayerLastVeh[CHAR_FRANKLIN].model = BJXL)
		AND (g_ePlayerLastVehState[CHAR_FRANKLIN] = PTVS_1_playerWithVehicle)
			g_sPlayerLastVeh[CHAR_FRANKLIN].model = DUMMY_MODEL_FOR_SCRIPT
			g_ePlayerLastVehState[CHAR_FRANKLIN] = PTVS_0_noVehicle
			CPRINTLN(DEBUG_SWITCH, "reset franklins BJXL")
		ENDIF
	ENDIF
	
	SWITCH sTimetableScene.sScene.eScene
		CASE PR_SCENE_M_PIER_a
		CASE PR_SCENE_M7_FAKEYOGA
		CASE PR_SCENE_M_CANAL_a
		CASE PR_SCENE_M_CANAL_b
		CASE PR_SCENE_M_CANAL_c
		CASE PR_SCENE_M_PIER_b
		CASE PR_SCENE_M2_SMOKINGGOLF
		CASE PR_SCENE_M_VWOODPARK_a
		CASE PR_SCENE_M_VWOODPARK_b
		CASE PR_SCENE_M6_MORNING_a
			BAWSAQ_INCREMENT_MODIFIER(BSMF_BSM_SMOKED_FOR_RWC)		//#1514495 - stockmarket
		BREAK
		CASE PR_SCENE_F_GYM
		CASE PR_SCENE_F0_SH_PUSHUP_a
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_GYM)					//#1514495 - stockmarket
		BREAK
	ENDSWITCH
	
	IF (sTimetableScene.sScene.eScene = PR_SCENE_M_HOOKERMOTEL)
		SHRINK_ADD_SEX_TIMESTAMP()
	ENDIF
	
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	PRIVATE_SetRoadsSwitchArea(sTimetableScene.sScene.eScene, TRUE)
	PRIVATE_SetPedInSwitchArea(sTimetableScene.sScene.eScene, TRUE)
	
	ENABLE_PROCOBJ_CREATION()
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_timetable_scene_widget)
		DELETE_WIDGET_GROUP(player_timetable_scene_widget)
	ENDIF
	
	IF NOT g_savedGlobals.sFlow.isGameflowActive
	OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		IF NOT (sTimetableScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			DOOR_NAME_ENUM eDoor
			REPEAT NUMBER_OF_DOORS eDoor
				DOOR_DATA_STRUCT sData = GET_DOOR_DATA(eDoor)
				
				IF VDIST2(vPlayerCoord, sData.coords) < (100.0*100.0)
					SET_DOOR_STATE(eDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
					
					CPRINTLN(DEBUG_SWITCH, "unlock door ", ENUM_TO_INT(eDoor), " - ", sData.dbg_name)
				ENDIF
			ENDREPEAT
			
			CPRINTLN(DEBUG_SWITCH, "DEBUG - unlock all doors for this switch!")
		ELSE
			CPRINTLN(DEBUG_SWITCH, "DEBUG - dont touch the doors - tanishafight!!")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SWITCH, "DEBUG - dont touch the doors - in flow!!")
	ENDIF
	
	#ENDIF
	
	// Reset the global communication delay to a short time after a switch scene.
	ADD_GLOBAL_COMMUNICATION_DELAY(CC_DELAY_POST_AMBIENT_SWITCH)
	
	HIDE_PHONE_FOR_HOTSWAP(FALSE)
	BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL(FALSE)
	
	IF (sTimetableScene.sScene.eScene != PR_SCENE_M_S_FAMILY4)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
	ENDIF
				
//	SAVE_STRING_TO_DEBUG_FILE("	[")
//	SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
//	SAVE_STRING_TO_DEBUG_FILE(" g_iPauseOnOutro:")
//	SAVE_INT_TO_DEBUG_FILE(g_iPauseOnOutro)
//	SAVE_STRING_TO_DEBUG_FILE("]")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
	
		
		
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 		
//		IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//			WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
//				FLOAT gameplayCamHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				FLOAT gameplayCamPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
//				
//				SAVE_STRING_TO_DEBUG_FILE("			CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))SAVE_STRING_TO_DEBUG_FILE("		//SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE()")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("				gpCamHead = ")SAVE_FLOAT_TO_DEBUG_FILE(gameplayCamHeading)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("				gpCamPitch = ")SAVE_FLOAT_TO_DEBUG_FILE(gameplayCamPitch)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("				")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("				RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("			BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				WAIT(0)
//			ENDWHILE
//		ENDIF
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
	
	//- kill mission text link -//
	THEFEED_RESUME()
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC


BOOL bWaitingForPlayerLieInInput = false, bDisableWhileWaiting = TRUE	//, bFirstPersonCamQuit = FALSE
PROC DisableWhileWaiting(STRING s)
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	IF NOT bDisableWhileWaiting
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		str  = ("EnableWhileWaiting(\"")
		str += (s)
		str += ("\")")
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, HUD_COLOUR_RED)
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	str  = ("DisableWhileWaiting(\"")
	str += (s)
	str += ("\")")
	DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, HUD_COLOUR_blue)
	#ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	
	IF NOT bWaitingForPlayerLieInInput
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
		
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
	ELSE
	
		#IF IS_DEBUG_BUILD
		str  = ("	bWaitingForPlayerLieInInput = true...")
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 1.0, HUD_COLOUR_blue)
		#ENDIF
		
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)		//1530825)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_X)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)	//2080815
	
	// Hide the HUD
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	
	IF (g_Cellphone.PhoneDS <> PDS_DISABLED)
		IF bKeepCellphoneDisabled
			DISABLE_CELLPHONE(TRUE)
		ENDIF
	ENDIF
	
	//Is the global wait timer at 0?
	INT iGameTime = GET_GAME_TIMER()
	IF iGameTime >= g_iGlobalWaitTime - 0500
		ADD_GLOBAL_COMMUNICATION_DELAY(CC_DELAY_POST_AMBIENT_SWITCH / 2)
		s = s
	ENDIF
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Timetable_Scene_Finished()
	
	IF DOES_ENTITY_EXIST(player_prop)
		MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
		PED_BONETAG eBonetag = BONETAG_NULL
		FLOAT fDetachAnimPhase = -1.0
		enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
		
		IF GET_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
					eObjectModel, vecOffset, vecRotation, eBonetag,
					fDetachAnimPhase, thisSceneObjectAction)
			SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(player_extra_prop)
		MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
		PED_BONETAG eBonetag = BONETAG_NULL
		FLOAT fDetachAnimPhase = -1.0
		enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
		BOOL bAttachedToBuddy
		IF GET_EXTRA_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
					eObjectModel, vecOffset, vecRotation, eBonetag,
					fDetachAnimPhase, thisSceneObjectAction, bAttachedToBuddy)
			SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_extra_prop, thisSceneObjectAction)
		ENDIF
		
	ENDIF
	
//	BOOL bLocalUnleashCodeCamStreaming = TRUE
//	#IF IS_DEBUG_BUILD
//		bLocalUnleashCodeCamStreaming = TRUE
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bFilledPushInData
		DrawLiteralSceneString("bFilledPushInData",
				1+1, HUD_COLOUR_RED)
	ELSE
		DrawLiteralSceneString("NOT bFilledPushInData",
				1+1, HUD_COLOUR_BLUE)
	ENDIF
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		DrawLiteralSceneString("switchSplineCamInProgress",
				2+1, HUD_COLOUR_RED)
	ELSE
													  //
		DrawLiteralSceneString("NOT switchSplineCamInProgress",
				2+1, HUD_COLOUR_BLUE)
	ENDIF
	IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			DrawLiteralSceneString("OFF interping from script cam",
					4+1, HUD_COLOUR_REDDARK)
		ELSE
			DrawLiteralSceneString("ON interping from script cam",
					4+1, HUD_COLOUR_REDLIGHT)
		ENDIF
	ELSE
		DrawLiteralSceneString("NOT interping from script cam",
				4+1, HUD_COLOUR_BLUE)
	ENDIF
	#ENDIF
	
	IF bFilledPushInData
		DisableWhileWaiting("bFilledPushInData")
		EXIT
	ENDIF

	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		DisableWhileWaiting("IS_PLAYER_SWITCH_IN_PROGRESS")
		EXIT
	ENDIF

	IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		DisableWhileWaiting("Player_Timetable_Scene_Finished")
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
		
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
		
		EXIT
	ENDIF
	
	bPlayer_timetable_scene_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Timetable_Scene_Variables(PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene)
	
	//- vectors -//
	//- floats -//
	//- ints -//
	//-- structs : PED_SCENE_STRUCT --//
	sTimetableScene = sPassedScene
	bInitialised_timetable_scene = TRUE
	
	bShortCircuitedPushin = FALSE // rob
	
	IF sTimetableScene.eSceneBuddy <> NO_CHARACTER
		SWITCH sTimetableScene.eSceneBuddy
			CASE CHAR_AMANDA
				g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FAMILY_MEMBER_BUSY
			BREAK
			CASE CHAR_JIMMY
				g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FAMILY_MEMBER_BUSY
			BREAK
			CASE CHAR_TRACEY
				g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER] = FAMILY_MEMBER_BUSY
			BREAK
			
			CASE CHAR_LAMAR
				g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR] = FAMILY_MEMBER_BUSY
				g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH] = FAMILY_MEMBER_BUSY
			BREAK
			
			CASE CHAR_RON
				g_eCurrentFamilyEvent[FM_TREVOR_0_RON] = FAMILY_MEMBER_BUSY
			BREAK
			
			CASE CHAR_FLOYD
				g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD] = FAMILY_MEMBER_BUSY
			BREAK
			
			CASE CHAR_DAVE		//used in 'PR_SCENE_M_MD_FBI2'
			CASE CHAR_TANISHA	//used in 'PR_SCENE_F0_TANISHAFIGHT'
			CASE CHAR_WADE		//used in 'PR_SCENE_T_SC_DRUNKHOWLING'
				//
			BREAK
			
			CASE CHAR_BLANK_ENTRY
				//used for non story char buddies
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("unknown sTimetableScene.eSceneBuddy")
			BREAK
		ENDSWITCH
	ENDIF
	
	GET_PLAYER_VEH_RECORDING_FOR_SCENE(sTimetableScene.sScene.eScene, tRecordingName, iRecordingNum,
			fRecordingStart, fRecordingSkip, fRecordingStop,
			fSpeedSwitch, fSpeedExit)
	
	IF ((sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT))
	AND (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
	
		IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
			
			FLOAT fWaterHeight
			IF GET_WATER_HEIGHT_NO_WAVES(sTimetableScene.sScene.vCreateCoords, fWaterHeight)
			
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: fWaterHeight: ", fWaterHeight, "	//vCreateCoords.z: ", sTimetableScene.sScene.vCreateCoords.z)
				#ENDIF
				
				IF (sTimetableScene.sScene.vCreateCoords.z < fWaterHeight)
				
					PED_VEH_DATA_STRUCT sVehData    //MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
					VECTOR vVehCoordOffset = <<0,0,0>>
					FLOAT fVehHeadOffset = 0
					
					GET_PLAYER_VEH_POSITION_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene,
								sVehData, vVehCoordOffset, fVehHeadOffset,
								vSceneVeh_driveOffset, fSceneVeh_driveSpeed)
					
					IF NOT IS_THIS_MODEL_A_BOAT(sVehData.model)
					AND NOT (sVehData.model = SUBMERSIBLE OR sVehData.model = SUBMERSIBLE2)
						sTimetableScene.eVehState = PTVS_1_playerWithVehicle
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: sTimetableScene.eVehState = PTVS_1_playerWithVehicle", " [", SAFE_GET_MODEL_NAME_FOR_DEBUG(sVehData.model), "]")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: ignore water vehicle stuff", " [", SAFE_GET_MODEL_NAME_FOR_DEBUG(sVehData.model))PRINTLN(DEBUG_SWITCH, "]")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			vSceneVeh_driveOffset = <<0,g_sDefaultPlayerSwitchState[sTimetableScene.sScene.ePed].fVehicleSpeed_a*1.5,0>>
			fSceneVeh_driveSpeed = g_sDefaultPlayerSwitchState[sTimetableScene.sScene.ePed].fVehicleSpeed_a
			
			CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: (fSceneVeh_driveSpeed:", fSceneVeh_driveSpeed, ")")
			
			sTimetableScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sTimetableScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
		ELSE
		
//			IF NOT IS_COLLISION_MARKED_OUTSIDE(sTimetableScene.sScene.vCreateCoords)
				sTimetableScene.eLoopTask = SCRIPT_TASK_STAND_STILL
				sTimetableScene.eOutTask = SCRIPT_TASK_STAND_STILL
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: vCreateCoords", sTimetableScene.sScene.vCreateCoords, "marked inside - stand still")
				#ENDIF
//			ELSE
//				sTimetableScene.eLoopTask = SCRIPT_TASK_WANDER_STANDARD	//SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD	//SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//				sTimetableScene.eOutTask = SCRIPT_TASK_WANDER_STANDARD	//SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
//				
//				bUpdatedWalkTasks = TRUE
//				
//				#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_SWITCH, "Initialise_Player_Timetable_Scene_Variables: vCreateCoords", sTimetableScene.sScene.vCreateCoords, "marked outside - walk to coord")
//				#ENDIF
//			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL SafeDetachSynchronizedScene(INT &sceneID)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "SafeDetachSynchronizedScene(", sceneID, ")")
	#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
		DETACH_SYNCHRONIZED_SCENE(sceneID)
		sceneID = -1
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CheckForPassengersToClear(VEHICLE_INDEX vehID)

	IF g_eMissionRunningOnMPSwitchStart = SP_MISSION_NONE
	AND g_eRCMRunningOnMPSwitchStart = NO_RC_MISSION
		CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear() : func EXIT early as no SP/RC mission was running when switch occured")
		EXIT
#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear(): last SP = ", g_eMissionRunningOnMPSwitchStart, " / last RC = ", g_eRCMRunningOnMPSwitchStart)
#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_eMissionRunningOnMPSwitchStart != SP_MISSION_NONE
			SCRIPT_ASSERT("g_eMissionRunningOnMPSwitchStart != SP_MISSION_NONE")
		ENDIF
	#ENDIF
	
	PED_INDEX pedTemp
	
	IF NOT IS_VEHICLE_SEAT_FREE(vehID, VS_FRONT_RIGHT)
		pedTemp = GET_PED_IN_VEHICLE_SEAT(vehID, VS_FRONT_RIGHT)
		IF NOT IS_PED_INJURED(pedTemp)
			CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
			CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear() : call clear ped tasks immediately on VS_FRONT_RIGHT ped")
		ENDIF
	ENDIF
	IF NOT IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_LEFT)
		pedTemp = GET_PED_IN_VEHICLE_SEAT(vehID, VS_BACK_LEFT)
		IF NOT IS_PED_INJURED(pedTemp)
			CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
			CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear() : call clear ped tasks immediately on VS_BACK_LEFT ped")
		ENDIF
	ENDIF
	IF NOT IS_VEHICLE_SEAT_FREE(vehID, VS_BACK_RIGHT)
		pedTemp = GET_PED_IN_VEHICLE_SEAT(vehID, VS_BACK_RIGHT)
		IF NOT IS_PED_INJURED(pedTemp)
			CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
			CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear() : call clear ped tasks immediately on VS_BACK_RIGHT ped")
		ENDIF
	ENDIF
	IF IS_ENTITY_ATTACHED(vehID)
		CPRINTLN(DEBUG_SWITCH, "CheckForPassengersToClear() : detach vehicle")
		DETACH_ENTITY(vehID)
	ENDIF
	
	CLEAR_AREA(GET_ENTITY_COORDS(vehID, FALSE), 2.5, FALSE)
	
ENDPROC

FUNC BOOL is_player_already_on_vehicle(PED_VEH_DATA_STRUCT sVehData, VEHICLE_INDEX &vehID)
	SELECTOR_SLOTS_ENUM eNewSelectorPed = sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed
	IF eNewSelectorPed >= NUMBER_OF_SELECTOR_PEDS
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, " # new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\"  > NUMBER_OF_SELECTOR_PEDS")
		

		SELECTOR_SLOTS_ENUM ePreviousSelectorPed = sTimetableScene.sScene.sSelectorPeds.ePreviousSelectorPed
		SELECTOR_SLOTS_ENUM eCurrentSelectorPed = sTimetableScene.sScene.sSelectorPeds.eCurrentSelectorPed

		CPRINTLN(DEBUG_SWITCH, " ## prev ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(ePreviousSelectorPed)), "\"")
		CPRINTLN(DEBUG_SWITCH, " ## current ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCurrentSelectorPed)), "\"")
		#ENDIF
		
		eNewSelectorPed = sTimetableScene.sScene.sSelectorPeds.eCurrentSelectorPed	//RETURN TRUE
	ENDIF
	
	
	PED_INDEX scenePedID = sTimetableScene.sScene.sSelectorPeds.pedID[eNewSelectorPed]
	
	IF NOT DOES_ENTITY_EXIST(scenePedID)
		IF IS_PED_THE_CURRENT_PLAYER_PED(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed))
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, " ### ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" doesnt exist, but is the player ped")
			#ENDIF
			
			scenePedID = PLAYER_PED_ID()
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(scenePedID)
		IF IS_PED_IN_ANY_VEHICLE(scenePedID, TRUE)
			VEHICLE_INDEX player_vehID = GET_VEHICLE_PED_IS_IN(scenePedID, TRUE)
			IF DOES_ENTITY_EXIST(player_vehID)
				IF GET_ENTITY_MODEL(player_vehID) = sVehData.model
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, " # new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" is already in a vehicle...")
					#ENDIF
					
					vehID = player_vehID
					
					IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
						SET_ENTITY_AS_MISSION_ENTITY(vehID, TRUE, TRUE)
					ENDIF
					RETURN TRUE
				
				#IF IS_DEBUG_BUILD
				else
					CPRINTLN(DEBUG_SWITCH, " # vehicle new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" is in doesnt match")
				#ENDIF
					
				ENDIF
				
			#IF IS_DEBUG_BUILD
			else
				CPRINTLN(DEBUG_SWITCH, " # vehicle new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" is in does not exist")
				return false
			#ENDIF
			
			ENDIF
			
		#IF IS_DEBUG_BUILD
		else
			CPRINTLN(DEBUG_SWITCH, " # new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" is not in any vehicle")
		#ENDIF
		
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_vPlayerVeh[sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed])
			
			IF NOT IS_ENTITY_A_MISSION_ENTITY(g_vPlayerVeh[sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed])
				SET_ENTITY_AS_MISSION_ENTITY(g_vPlayerVeh[sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed], TRUE, TRUE)
			ENDIF
			
			IF GET_ENTITY_MODEL(g_vPlayerVeh[sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed]) = sVehData.model
					
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, " # new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" has last known vehicle alive somewhere...")
				#ENDIF
				
				vehID = g_vPlayerVeh[sTimetableScene.sScene.sSelectorPeds.eNewSelectorPed]
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
					SET_ENTITY_AS_MISSION_ENTITY(vehID, TRUE, TRUE)
				ENDIF
				
				CheckForPassengersToClear(vehID)
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		INT SearchFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES
		IF (sTimetableScene.eVehState = PTVS_1_playerWithVehicle)
			VEHICLE_INDEX player_vehID = GET_RANDOM_VEHICLE_IN_SPHERE(sTimetableScene.sScene.vCreateCoords, 10.0, sVehData.model, SearchFlags)
			
			IF DOES_ENTITY_EXIST(player_vehID)
//				SCRIPT_ASSERT("# ped vehicle found ambiently (1_playerWithVehicle)!")
				
				CPRINTLN(DEBUG_SWITCH, " # ped vehicle \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" found ambiently (1_playerWithVehicle)!")
				
				vehID = player_vehID
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
					SET_ENTITY_AS_MISSION_ENTITY(vehID, TRUE, TRUE)
				ENDIF
				
				CheckForPassengersToClear(vehID)
				
				RETURN TRUE
			ENDIF
			
		ENDIF
		
		IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
			VEHICLE_INDEX player_vehID = GET_RANDOM_VEHICLE_IN_SPHERE(sTimetableScene.sScene.vCreateCoords, 10.0, sVehData.model, SearchFlags)

					
			IF DOES_ENTITY_EXIST(player_vehID)
//				SCRIPT_ASSERT("# ped vehicle found ambiently (2_playerInVehicle)!")
				
				CPRINTLN(DEBUG_SWITCH, " # ped vehicle \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" found ambiently (2_playerInVehicle)!")
				
				vehID = player_vehID
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehID)
					SET_ENTITY_AS_MISSION_ENTITY(vehID, TRUE, TRUE)
				ENDIF
				
				CheckForPassengersToClear(vehID)
				
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	#IF IS_DEBUG_BUILD
	else
		CPRINTLN(DEBUG_SWITCH, " # new ped \"", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eNewSelectorPed)), "\" is injured")
	#ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PRIVATE_SetDefaultSceneBuddyAttributes(PED_INDEX &pedIndex, enumCharacterList eSceneBuddy)
	
	REL_GROUP_HASH eRelGroup
	
	SWITCH eSceneBuddy
		CASE CHAR_JIMMY
		CASE CHAR_TRACEY
		CASE CHAR_AMANDA
		CASE CHAR_DAVE			//for scene PRM_MD_FBI2 (needs removing one day)
			eRelGroup = RELGROUPHASH_FAMILY_M
		BREAK
		CASE CHAR_TANISHA
		CASE CHAR_LAMAR
		CASE CHAR_CHOP
			eRelGroup = RELGROUPHASH_FAMILY_F
		BREAK
		CASE CHAR_FLOYD
		CASE CHAR_RON
		CASE CHAR_WADE
			eRelGroup = RELGROUPHASH_FAMILY_T
		BREAK
		
		CASE CHAR_BLANK_ENTRY
			MODEL_NAMES model
			model = GET_ENTITY_MODEL(pedIndex)
			
			IF (model = GET_CHOP_MODEL())
				eRelGroup = RELGROUPHASH_FAMILY_F
			ELSE
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str
				str  = ("invalid blank entry model \"")
				str += SAFE_GET_MODEL_NAME_FOR_DEBUG(model)
				str += ("\" for SetDefaultSceneBuddyAttributes()")
				SCRIPT_ASSERT(str)
				#ENDIF
				
				g_pScene_buddy = pedIndex
				EXIT
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str  = ("invalid scene buddy \"")
			str += GET_PLAYER_PED_STRING(eSceneBuddy)
			str += ("\" for SetDefaultSceneBuddyAttributes()")
			SCRIPT_ASSERT(str)
			#ENDIF
			
			g_pScene_buddy = pedIndex
			EXIT
		BREAK
	ENDSWITCH
	
	SET_ENTITY_LOD_DIST(pedIndex, 100)		//#1444135
	SET_PED_LOD_MULTIPLIER(pedIndex, 2.0)	//#1510547
	PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
	g_pScene_buddy = pedIndex
ENDPROC



/// PURPOSE:
///    url:bugstar:742018
/// PARAMS:
///    ePedScene - 
/// RETURNS:
///    
FUNC BOOL FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(ENTITY_INDEX EntityIndex, PED_REQUEST_SCENE_ENUM ePedScene)
	
	INTERIOR_INSTANCE_INDEX InteriorInstanceIndex
	INT RoomKey
	
	SWITCH ePedScene
//		CASE PR_SCENE_F1_ONCELL
//			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<<2.1047, 526.6067, 174.9013>>, "v_franklinshouse")
//			RoomKey = GET_HASH_KEY("loungeB")
//		BREAK
//		CASE PR_SCENE_F1_GETTINGREADY
//			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<<9.0561, 528.0162, 169.6401>>, "v_franklinshouse")
//			RoomKey = GET_HASH_KEY("Wardrobe")
//		BREAK
//		CASE PR_SCENE_T_SC_BAR
//			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<<126.0881, -1284.4879, 28.2834>>, "v_strip3")
//			RoomKey = GET_HASH_KEY("strp3mainrm")
//		BREAK
		CASE PR_SCENE_T_SC_CHASE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "CLEAR_ROOM_FOR_ENTITY(EntityIndex, ", ")")
			#ENDIF
			
			CLEAR_ROOM_FOR_ENTITY(EntityIndex)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "CLEAR_ROOM_FOR_ENTITY(EntityIndex, ", ")")
			#ENDIF
			
			CLEAR_ROOM_FOR_ENTITY(EntityIndex)
			
			RETURN TRUE
		BREAK
		
		DEFAULT
			InteriorInstanceIndex = NULL
			RoomKey = -1
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF (sTimetableScene.sScene.roomInterior <> InteriorInstanceIndex)
		CPRINTLN(DEBUG_SWITCH, "sTimetableScene.sScene.roomInterior[", NATIVE_TO_INT(sTimetableScene.sScene.roomInterior), "] <> InteriorInstanceIndex[", NATIVE_TO_INT(InteriorInstanceIndex), "])")
		
		SCRIPT_ASSERT("sTimetableScene.sScene.roomInterior <> InteriorInstanceIndex")
	ELSE
		TEXT_LABEL_63 sInvalid
		sInvalid  = Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
		sInvalid += ".roomInterior = InteriorInstanceIndex"
		
		CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
		SCRIPT_ASSERT(sInvalid)
	ENDIF
	#ENDIF
	
	FORCE_ROOM_FOR_ENTITY(EntityIndex, InteriorInstanceIndex, RoomKey)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(EntityIndex, ", NATIVE_TO_INT(InteriorInstanceIndex), ", ", RoomKey, ")")
	#ENDIF
	
	RETURN TRUE
ENDFUNC



PROC ar_ADD_PED_FOR_DIALOGUE(TEXT_LABEL tPlayerSceneVoiceID, TEXT_LABEL tBuddySceneVoiceID)

	IF (sTimetableScene.sScene.eScene = PR_SCENE_F_S_AGENCY_2A_a)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_S_AGENCY_2A_b)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, NULL, "")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, NULL, "")
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "MICHAEL")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 3, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
			#ENDIF
		ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_T_SHIT)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, NULL, "")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,
					3,
					PLAYER_PED_ID(), tPlayerSceneVoiceID)
			
			IF NOT g_bMagDemoActive
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 3, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				#ENDIF
			ENDIF
		ENDIF
	#ENDIF 
	
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M6_RONBORING)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 0)
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 3)
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 4)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), tPlayerSceneVoiceID)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, scene_buddy, tBuddySceneVoiceID)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 3, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
			#ENDIF
		ENDIF
		
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_Ta_CARSTEAL4)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 0)
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 1)
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct, 4)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), tPlayerSceneVoiceID)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, tBuddySceneVoiceID)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 3, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
			#ENDIF
		ENDIF
		
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M6_ONPHONE)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_M6_SUNBATHING)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_M6_DEPRESSED)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, NULL, "")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,
					2,
					PLAYER_PED_ID(), tPlayerSceneVoiceID)
			
			IF NOT g_bMagDemoActive
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 2, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				#ENDIF
			ENDIF
		ENDIF
	
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M7_HOOKERS)
		
		enumCharacterList ePed
		IF GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTimetableScene.sScene.eScene, ePed)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), tPlayerSceneVoiceID)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, scene_buddy, tBuddySceneVoiceID)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ", 0, ", PLAYER_PED_ID(), \"", tPlayerSceneVoiceID, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
			#ENDIF
		ENDIF
		
	ELSE
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			enumCharacterList ePlayerPed = GET_CURRENT_PLAYER_PED_ENUM()
			INT iPlayerSceneVoiceNum = ENUM_TO_INT(ePlayerPed)
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPlayerSceneVoiceNum, PLAYER_PED_ID(), tPlayerSceneVoiceID)
		ENDIF
		INT iBuddySceneVoiceNum = 4
		IF NOT IS_PED_INJURED(scene_buddy)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iBuddySceneVoiceNum, scene_buddy, tBuddySceneVoiceID)
		ELIF NOT IS_STRING_NULL_OR_EMPTY(tBuddySceneVoiceID)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iBuddySceneVoiceNum, NULL, tBuddySceneVoiceID)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WaitingForPhonecallToStartAndPause( #IF IS_DEBUG_BUILD INT &iDrawSceneRot #ENDIF )
	
	IF NOT IS_CELLPHONE_CONVERSATION_PAUSED()
		TEXT_LABEL tPlayerSceneVoiceID, tBuddySceneVoiceID
		FLOAT fSceneSpeechPhase
		IF SETUP_TIMETABLE_SPEECH_FOR_SCENE(sTimetableScene.sScene.eScene,
				tPlayerSceneVoiceID, tBuddySceneVoiceID, tSpeechBlock, tSpeechLabel, fSceneSpeechPhase)

			
			BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL(TRUE)
			HIDE_PHONE_FOR_HOTSWAP(TRUE)
			IF (g_Cellphone.PhoneDS = PDS_DISABLED)
				DISABLE_CELLPHONE(FALSE)
				bKeepCellphoneDisabled = FALSE
			ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), sTimetableScene.sScene.vCreateCoords)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), sTimetableScene.sScene.fCreateHead)
			
			ar_ADD_PED_FOR_DIALOGUE(tPlayerSceneVoiceID, tBuddySceneVoiceID)
			
			INT iWaitTimer = GET_GAME_TIMER() + 7500
			
			WHILE NOT PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LAMAR, tSpeechBlock, tSpeechLabel, CONV_PRIORITY_AMBIENT_HIGH)
			AND (iWaitTimer > GET_GAME_TIMER())
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("not PLAYER_CALL ")
				str += iWaitTimer - GET_GAME_TIMER()
				DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
				#ENDIF
				
				IF SET_PED_PRESET_OUTFIT_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene, PLAYER_PED_ID(), iPlayerOutfitPreloadStage)
					
					#IF IS_DEBUG_BUILD
					str = ("preload outfit ")
					str += (iPlayerOutfitPreloadStage)
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
					#ENDIF
				ELIF NOT (HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
				AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID()))
					
					#IF IS_DEBUG_BUILD
					str = ("prestreamed outfit ")
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
					#ENDIF
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			HIDE_PHONE_FOR_HOTSWAP(TRUE)
			
			BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL(TRUE)
			SET_BIT(BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START)

			iWaitTimer = GET_GAME_TIMER() + 12500
			
			WHILE NOT IS_CELLPHONE_CONVERSATION_PLAYING()
			AND (iWaitTimer > GET_GAME_TIMER())
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("not IS_CALL_ONGOING ")
				str += iWaitTimer - GET_GAME_TIMER()
				DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
				#ENDIF
				
				IF SET_PED_PRESET_OUTFIT_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene, PLAYER_PED_ID(), iPlayerOutfitPreloadStage)
					
					#IF IS_DEBUG_BUILD
					str = ("preload outfit ")
					str += (iPlayerOutfitPreloadStage)
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
					#ENDIF
				ELIF NOT (HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
				AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID()))
					
					#IF IS_DEBUG_BUILD
					str = ("prestreamed outfit ")
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
					#ENDIF
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<WaitingForPhonecallToStartAndPause> PAUSE_PLAYER_CALL_CHAR_CELLPHONE(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
			#ENDIF
			
			PAUSE_CELLPHONE_CONVERSATION(TRUE)
			HIDE_PHONE_FOR_HOTSWAP(TRUE)
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("wait for call")
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
			iDrawSceneRot += 1
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ("call PAUSED")
	DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
	iDrawSceneRot += 1
	#ENDIF
	
	RETURN FALSE
ENDFUNC


#IF NOT IS_NEXTGEN_BUILD
#IF IS_DEBUG_BUILD
CONST_INT iCONST_WAIT_FOR_BUDDY_MAX_FRAMES	250
#ENDIF
#IF NOT IS_DEBUG_BUILD
CONST_INT iCONST_WAIT_FOR_BUDDY_MAX_FRAMES	500
#ENDIF
#ENDIF

#IF IS_NEXTGEN_BUILD
#IF IS_DEBUG_BUILD
CONST_INT iCONST_WAIT_FOR_BUDDY_MAX_FRAMES	750
#ENDIF
#IF NOT IS_DEBUG_BUILD
CONST_INT iCONST_WAIT_FOR_BUDDY_MAX_FRAMES	1500
#ENDIF
#ENDIF

#IF IS_DEBUG_BUILD
PROC DrawLiteralSceneStringWithPrint(INT iWaitForBuddyFrames, STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
	DrawLiteralSceneString(sLiteral, iColumn, eColour, fAlphaMult)
	
	IF iWaitForBuddyFrames >= (iCONST_WAIT_FOR_BUDDY_MAX_FRAMES-5)
		CWARNINGLN(DEBUG_SWITCH, "EMERGENCY BAIL: \"", sLiteral, "\"")
	ENDIF
ENDPROC
#ENDIF

PROC Setup_Player_In_Switch_Vehicle()		//SETUPVEHSHIZZ
	
	DEBUG_PRINTCALLSTACK()
	
	IF sTimetableScene.eVehState <> PTVS_2_playerInVehicle
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** sTimetableScene.eVehState <> PTVS_2_playerInVehicle [", sTimetableScene.eVehState, "]")
		
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** IS_ENTITY_DEAD(PLAYER_PED_ID())")
		
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(scene_veh)
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** IS_ENTITY_DEAD(scene_veh)")
		
		EXIT
	ENDIF
	
	IF sTimetableScene.sScene.ePed <> GET_CURRENT_PLAYER_PED_ENUM()	//
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** sTimetableScene.sScene.ePed <> GET_CURRENT_PLAYER_PED_ENUM()	")
		
		EXIT
	ENDIF
	
	IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)")
	ELSE
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   *** NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)")
		
		VEHICLE_SEAT vsdriver = VS_DRIVER
		IF NOT IS_ENTITY_DEAD(scene_buddy)
			IF (sTimetableScene.eOutTask = SCRIPT_TASK_LEAVE_VEHICLE)
				IF (sTimetableScene.eBuddyOutTask = SCRIPT_TASK_STAND_STILL)
				
					IF NOT IS_VEHICLE_SEAT_FREE(scene_veh, VS_DRIVER)
						PED_INDEX scene_veh_driver_ped
						scene_veh_driver_ped = GET_PED_IN_VEHICLE_SEAT(scene_veh, VS_DRIVER)
						SET_ENTITY_COORDS(scene_veh_driver_ped,
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, <<-5.0, -5.0, 0.0>>), FALSE)
					ENDIF
					SET_PED_INTO_VEHICLE(scene_buddy, scene_veh, VS_DRIVER)
					
					CPRINTLN(DEBUG_SWITCH, "set player in passenger right seat")
					vsdriver = VS_BACK_RIGHT
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_SEAT_FREE(scene_veh, vsdriver)
			PED_INDEX scene_veh_driver_ped
			scene_veh_driver_ped = GET_PED_IN_VEHICLE_SEAT(scene_veh, vsdriver)
			IF (scene_veh_driver_ped <> scene_buddy)
				SET_ENTITY_COORDS(scene_veh_driver_ped,
						GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, <<-5.0, -5.0, 0.0>>), FALSE)
			ENDIF
		ENDIF
		
		ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Setup_Player_In_Switch_Vehicle")
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, TRUE)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), scene_veh, vsdriver)
		
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(scene_veh))
			SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		SET_PLAYER_CLOTH_PIN_FRAMES( PLAYER_ID(), 1 )
//		SET_PLAYER_CLOTH_LOCK_COUNTER(iFramesToLockCloth)
		ar_FREEZE_ENTITY_POSITION(scene_veh, TRUE, "Setup_Player_In_Switch_Vehicle")
		
		IF (sTimetableScene.sScene.eScene <> PR_SCENE_M6_CARSLEEP)
		AND (sTimetableScene.sScene.eScene <> PR_SCENE_M6_CARSLEEP)
		AND (sTimetableScene.sScene.eScene <> PR_SCENE_M6_CARSLEEP)
			SET_VEHICLE_ENGINE_ON(scene_veh, TRUE, TRUE)
			SET_VEHICLE_LIGHTS(scene_veh, FORCE_VEHICLE_LIGHTS_ON)
			
			IF (fSceneVeh_driveSpeed > 5.0)
				IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
					SET_HELI_BLADES_FULL_SPEED(scene_veh)
					
					IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
					OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
						CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
					ENDIF
				ENDIF
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
					SET_HELI_BLADES_FULL_SPEED(scene_veh)
				ENDIF
			ENDIF
			
		ENDIF
		
		SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(scene_veh, TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)

	ENDIF
ENDPROC

//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Timetable_Scene()
	THEFEED_FLUSH_QUEUE()
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	IF bKeepCellphoneDisabled
		DISABLE_CELLPHONE(TRUE)
	ENDIF
	
	PRIVATE_SetRoadsSwitchArea(sTimetableScene.sScene.eScene, FALSE)
	PRIVATE_SetPedInSwitchArea(sTimetableScene.sScene.eScene, FALSE)
	
	DISABLE_PROCOBJ_CREATION()
	
	Quit_Drunk_Camera_Immediately()
	
	//- wait for assets to load -//
	BOOL bWaitForBuddyAssets = FALSE
	PED_VEH_DATA_STRUCT sVehData    //MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vVehCoordOffset = <<0,0,0>>
	FLOAT fVehHeadOffset = 0
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
	
	BOOL bFreezePlayer = FALSE
	
	INT iWaitForCharChangeFrames = 0, iWaitForBuddyFrames = 0
	
	VECTOR vVehCoordSafehouse
	FLOAT fVehHeadSafehouse
	
	INT iPed
	PED_INDEX temp_ped[5]
	
	VECTOR vSafehouseParkingSpots[6]			
	FLOAT fSafehouseParkingSpots[6]
	
	WHILE NOT bWaitForBuddyAssets
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iDrawSceneRot = 2
		TEXT_LABEL_63 sWait = ("iWaitForBuddyFrames: ")
		sWait += (iWaitForBuddyFrames)
		DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, sWait, iDrawSceneRot, HUD_COLOUR_RED)
		iDrawSceneRot++
		#ENDIF
		
		TEXT_LABEL_63 tPlayerSceneAnimDict = "null", tPlayerSceneAnimLoop = "null", tPlayerSceneAnimOut = "null"
		ANIMATION_FLAGS playerAnimLoopFlag = AF_DEFAULT, playerAnimOutFlag = AF_DEFAULT
		//ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
		IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
				tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
				playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
			
			#IF IS_DEBUG_BUILD
			IF (sTimetableScene.eLoopTask <> SCRIPT_TASK_PLAY_ANIM
			AND sTimetableScene.eOutTask <> SCRIPT_TASK_PLAY_ANIM
			AND sTimetableScene.eLoopTask <> SCRIPT_TASK_SYNCHRONIZED_SCENE
			AND sTimetableScene.eOutTask <> SCRIPT_TASK_SYNCHRONIZED_SCENE)
				TEXT_LABEL_63 sInvalid
				sInvalid  = "player animation needed for "
				sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
				sInvalid += "?"
				
				CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
				SCRIPT_ASSERT(sInvalid)
			ENDIF
			#ENDIF
			
			REQUEST_ANIM_DICT(tPlayerSceneAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(tPlayerSceneAnimDict)
				bWaitForBuddyAssets = FALSE
				REQUEST_ANIM_DICT(tPlayerSceneAnimDict)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("request anim ")
				str += (tPlayerSceneAnimDict)
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot++
				#ENDIF
			ENDIF
		ENDIF
		
		IF sTimetableScene.eVehState <> PTVS_0_noVehicle
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum > 0)
				REQUEST_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum, tRecordingName)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("request veh rec ")
					str += tRecordingName
					IF (iRecordingNum < 100)	str += ("0") ENDIF
					IF (iRecordingNum < 10)		str += ("0") ENDIF
					str += iRecordingNum
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(scene_veh)
				BOOL bCreateVehicle = FALSE

				IF GET_PLAYER_VEH_POSITION_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene,
						sVehData, vVehCoordOffset, fVehHeadOffset,
						vSceneVeh_driveOffset, fSceneVeh_driveSpeed)
					bCreateVehicle = TRUE
				ELSE
					
					// #1562150
					IF sTimetableScene.eVehState = PTVS_1_playerWithVehicle
						
						GetPostMissionLastVeh(sTimetableScene.sScene.ePed, sVehData)
						
						
						IF sVehData.model <> DUMMY_MODEL_FOR_SCRIPT
							
							//Michaels mansion
							vSafehouseParkingSpots[0] = <<-859.0566, 144.3844, 60.6915>>
							fSafehouseParkingSpots[0] = 2.3933
							
							//Franklins City house
							vSafehouseParkingSpots[1] = <<-19.3897, -1458.4712, 29.5455>>
							fSafehouseParkingSpots[1] = 97.3963
							
							//Franklins Hills house
							vSafehouseParkingSpots[2] = <<1.8447, 545.8348, 173.3900>>
							fSafehouseParkingSpots[2] = 293.7715
							
							//Trevor Trailer
							vSafehouseParkingSpots[3] = <<1991.8527, 3826.9084, 31.2135>>
							fSafehouseParkingSpots[3] = 211.1618
							
							//Trevor City apartment
							vSafehouseParkingSpots[4] = <<-1176.4854, -1490.9407, 3.3797>>
							fSafehouseParkingSpots[4] = 303.3079
							
							//Trevor Stripclub
							vSafehouseParkingSpots[5] = <<154.8832, -1304.4706, 28.2026>>
							fSafehouseParkingSpots[5] = 240.9780
							
							INT iClosestPark = -1
							FLOAT fMinParkDist = 9999999.0
							
							INT ePark = 0
							REPEAT COUNT_OF(vSafehouseParkingSpots) ePark
								VECTOR vParkCoords = vSafehouseParkingSpots[ePark]
								FLOAT fDistToPark = VDIST(sTimetableScene.sScene.vCreateCoords, vParkCoords)
								
							//	IF fDistToPark < 150.0
									IF fDistToPark < fMinParkDist
										iClosestPark	= ePark
										fMinParkDist	= fDistToPark
									ENDIF
							//	ENDIF
							ENDREPEAT
							
							IF iClosestPark >= 0
								vVehCoordSafehouse = vSafehouseParkingSpots[iClosestPark]
								fVehHeadSafehouse = fSafehouseParkingSpots[iClosestPark]
								
								vVehCoordOffset = vVehCoordSafehouse - sTimetableScene.sScene.vCreateCoords
								fVehHeadOffset =  fVehHeadSafehouse - sTimetableScene.sScene.fCreateHead
								
								bCreateVehicle = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bCreateVehicle
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("request veh model ")
					IF (sVehData.model <> DUMMY_MODEL_FOR_SCRIPT)
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(sVehData.model)
					ELSE
						str += "dummy"
					ENDIF
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
					
					VECTOR vVehCoords = sTimetableScene.sScene.vCreateCoords+vVehCoordOffset
					VECTOR vSpeed
					FLOAT fVehHead = sTimetableScene.sScene.fCreateHead+fVehHeadOffset
					
					IF NOT is_player_already_on_vehicle(sVehData, scene_veh)
						
						CLEAR_AREA_OF_VEHICLES(vVehCoords, 2.0)
						
						// Override the vehicle type so we can create the real player vehicle.
						IF sVehData.bIsPlayerVehicle
							IF IS_THIS_MODEL_A_BIKE(sVehData.model)
								sVehData.eType = VEHICLE_TYPE_BIKE
							ELSE
								sVehData.eType = VEHICLE_TYPE_CAR
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						INT fHealth = sVehData.fHealth
						#ENDIF
						
						SWITCH sVehData.eType
							CASE VEHICLE_TYPE_CAR
							CASE VEHICLE_TYPE_BIKE
								
								SWITCH sTimetableScene.sScene.ePed
									CASE CHAR_MICHAEL
										DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
									BREAK
									CASE CHAR_FRANKLIN
										IF (sVehData.eType = VEHICLE_TYPE_CAR)
											DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_FRANKLIN_SAVEHOUSE_CAR)
											DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR)
										ELIF (sVehData.eType = VEHICLE_TYPE_BIKE)	
											DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_FRANKLIN_SAVEHOUSE_BIKE)
											DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_BIKE)
										ENDIF
									BREAK
									CASE CHAR_TREVOR
										DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY)
										DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY)
									BREAK
								ENDSWITCH
								
								DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(sTimetableScene.sScene.ePed, sVehData.eType)
								
								CLEAR_AREA(vVehCoords, 2, TRUE)
								IF CREATE_PLAYER_VEHICLE(scene_veh, sTimetableScene.sScene.ePed,
										vVehCoords, fVehHead, TRUE,
										sVehData.eType)
									
									ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed*0.5)
									IF svehData.model = BLIMP OR svehData.model = BLIMP2
										//B* 2099783: Apply vertical speed to keep it floating, it would be falling down quite fast otherwise
										vspeed = GET_ENTITY_VELOCITY(scene_veh)
										IF IS_ENTITY_IN_AIR(scene_veh)
											SET_ENTITY_VELOCITY(scene_veh,<<vspeed.x,vspeed.y,10>>)
										ENDIF
									ENDIF
									
									SET_LAST_DRIVEN_VEHICLE(scene_veh)	//733525
									
									ar_FREEZE_ENTITY_POSITION(scene_veh, TRUE, "Setup_Player_Timetable_Scene.PLAYER")
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
									
									IF sTimetableScene.eOutTask <> SCRIPT_TASK_LEAVE_VEHICLE
										Setup_Player_In_Switch_Vehicle()
									ENDIF
									
									IF (fSceneVeh_driveSpeed > 5.0)
										IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
											SET_HELI_BLADES_FULL_SPEED(scene_veh)
											
											IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
											OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
												CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
											ENDIF
										ENDIF
										IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
											SET_HELI_BLADES_FULL_SPEED(scene_veh)
										ENDIF
									ENDIF
									
	//								SET_VEHICLE_DAMAGE()
	//								GET_VEHICLE_DEFORMATION_AT_POS()
									
									SET_SCENE_VEH_RADIO_STATION(scene_veh, sTimetableScene.sScene.eScene, TRUE)
									SET_PLAYER_VEHICLE_ALARM_AUDIO_ACTIVE(scene_veh, TRUE) 	//#1549453
									
									IF g_ePlayerLastVehGen[sTimetableScene.sScene.ePed] != VEHGEN_NONE
										CPRINTLN(DEBUG_SWITCH, "SWITCH VEH - setting switch vehicle as veh gen vehicle ", g_ePlayerLastVehGen[sTimetableScene.sScene.ePed])
										SET_VEHICLE_GEN_VEHICLE(g_ePlayerLastVehGen[sTimetableScene.sScene.ePed], scene_veh, FALSE)
									ENDIF
									
									IF (sTimetableScene.sScene.eScene = PR_SCENE_F0_CLEANCAR)
									OR (sTimetableScene.sScene.eScene = PR_SCENE_F0_BIKE)
									OR (sTimetableScene.sScene.eScene = PR_SCENE_F1_CLEANCAR)
									OR (sTimetableScene.sScene.eScene = PR_SCENE_F1_BIKE)
										RESET_VEHICLE_GEN_LOADED_CHECKS()
									ENDIF
								ENDIF
							BREAK
							
							DEFAULT
								
								//#1487390
								IF (sVehData.model = FROGGER2)
								OR (sVehData.model = CARGOBOB)
								OR (sVehData.model = CARGOBOB2)
									DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY) 
								ENDIF
								
								REQUEST_MODEL(sVehData.model)
								IF HAS_MODEL_LOADED(sVehData.model)
									CLEAR_AREA(vVehCoords, 5.0, TRUE)
									
									scene_veh = CREATE_VEHICLE(sVehData.model,
											vVehCoords, fVehHead, FALSE, FALSE)
									ar_SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
									
									SET_LAST_DRIVEN_VEHICLE(scene_veh)	//733525
									
									SET_VEH_DATA_FROM_STRUCT(scene_veh, sVehData)
									
									ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed*0.5)
									IF svehData.model = BLIMP OR svehData.model = BLIMP2
										//B* 2099783: Apply vertical speed to keep it floating, it would be falling down quite fast otherwise
										vspeed = GET_ENTITY_VELOCITY(scene_veh)
										IF IS_ENTITY_IN_AIR(scene_veh)
											SET_HELI_BLADES_FULL_SPEED(scene_veh)
											SET_ENTITY_VELOCITY(scene_veh,<<vspeed.x,vspeed.y,10>>)
										ENDIF
									ENDIF
									
									ar_FREEZE_ENTITY_POSITION(scene_veh, TRUE, "Setup_Player_Timetable_Scene.DEFAULT")
									
									IF sTimetableScene.eOutTask <> SCRIPT_TASK_LEAVE_VEHICLE
										Setup_Player_In_Switch_Vehicle()
									ENDIF
									
									IF (fSceneVeh_driveSpeed > 5.0)
										IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
											SET_HELI_BLADES_FULL_SPEED(scene_veh)
											
											IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
											OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
												CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
											ENDIF
										ENDIF
										IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
											SET_HELI_BLADES_FULL_SPEED(scene_veh)
										ENDIF
									ENDIF
									
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
									
									IF (sVehData.model = DINGHY)
										SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(PLAYER_ID(), TRUE)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SWITCH, "SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE ", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
										#ENDIF
									ENDIF
									
									SET_SCENE_VEH_RADIO_STATION(scene_veh, sTimetableScene.sScene.eScene, TRUE)
									
									
									IF g_ePlayerLastVehGen[sTimetableScene.sScene.ePed] != VEHGEN_NONE
										CPRINTLN(DEBUG_SWITCH, "SWITCH VEH - setting switch vehicle as veh gen vehicle ", g_ePlayerLastVehGen[sTimetableScene.sScene.ePed])
										SET_VEHICLE_GEN_VEHICLE(g_ePlayerLastVehGen[sTimetableScene.sScene.ePed], scene_veh, FALSE)
									ENDIF
									
									// Kenneth R.
									// To prevent duplicate vehicles we need to ensure this is not a vehicle gen vehicle.
									IF GET_VEHICLE_GEN_DATA(g_sVehicleGenNSData.sRuntimeStruct[0], VEHGEN_MISSION_VEH)
										IF GET_ENTITY_MODEL(scene_veh) = g_savedGlobals.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].eModel
										AND ARE_STRINGS_JUST_EQUAL(GET_VEHICLE_NUMBER_PLATE_TEXT(scene_veh), g_savedGlobals.sVehicleGenData.sDynamicData[g_sVehicleGenNSData.sRuntimeStruct[0].dynamicSlotIndex].tlPlateText)
											
											CPRINTLN(DEBUG_SWITCH, "SWITCH VEH - cleaning up vehicle gen as it is the same as the vehicle we are creating")
											
											DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
											SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MISSION_VEH, FALSE)
											
											CPRINTLN(DEBUG_SWITCH, "IMPOUND FIX 2 - switch vehicle has created the mission vehgen so clear cached vehicle data")
											g_sPreviousMissionVehGenData.eModel = DUMMY_MODEL_FOR_SCRIPT
											sTempImpoundVehData.eModel = DUMMY_MODEL_FOR_SCRIPT
										ENDIF
									ENDIF
									// Remove this vehicle from the impound if it's in someone slot already.
									INT i, j
									REPEAT 3 i // Players
										REPEAT 2 j // Slots
											// Vehicle already in impound - remove.
											IF GET_ENTITY_MODEL(scene_veh) = g_savedGlobals.sVehicleGenData.sImpoundVehicles[i][j].eModel
												IF NOT IS_STRING_NULL_OR_EMPTY(g_savedGlobals.sVehicleGenData.sImpoundVehicles[i][j].tlPlateText)
													IF ARE_STRINGS_EQUAL(GET_VEHICLE_NUMBER_PLATE_TEXT(scene_veh), g_savedGlobals.sVehicleGenData.sImpoundVehicles[i][j].tlPlateText)
														
														CPRINTLN(DEBUG_SWITCH, "SWITCH VEH - cleaning up impound vehicle as it is the same as the vehicle we are creating")
														
														g_savedGlobals.sVehicleGenData.sImpoundVehicles[i][j].eModel = DUMMY_MODEL_FOR_SCRIPT
														g_savedGlobals.sVehicleGenData.iImpoundVehicleSlot[i] = j
													ENDIF
												ENDIF
											ENDIF
										ENDREPEAT
									ENDREPEAT
									
									// Start tracking it for impound if this matches the vehicle we stopped tracking.
									IF g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel = sVehData.model
									AND ARE_STRINGS_EQUAL(g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].tlPlateText, sVehData.tlNumberPlate)
										CPRINTLN(DEBUG_SWITCH, "KR TEST - re-tracking vehicle for impound")
										g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel = DUMMY_MODEL_FOR_SCRIPT
										TRACK_VEHICLE_FOR_IMPOUND(scene_veh, sTimetableScene.sScene.ePed)
									ENDIF
									
									//#1487390
									IF (sVehData.model = FROGGER2)
										CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
									ENDIF
								ENDIF
								
							BREAK
						ENDSWITCH
						
						IF DOES_ENTITY_EXIST(scene_veh)
						AND (sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT
						OR sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT
						OR sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT)
							
							#IF IS_DEBUG_BUILD
							SAVE_STRING_TO_DEBUG_FILE("fHealth: ")
							SAVE_INT_TO_DEBUG_FILE(fHealth)
							
							SAVE_STRING_TO_DEBUG_FILE(", sVehData.fHealth: ")
							SAVE_INT_TO_DEBUG_FILE(sVehData.fHealth)
							
							SAVE_STRING_TO_DEBUG_FILE(", GET_ENTITY_HEALTH: ")
							SAVE_INT_TO_DEBUG_FILE(GET_ENTITY_HEALTH(scene_veh))
							
							SAVE_STRING_TO_DEBUG_FILE(", GET_ENTITY_MAX_HEALTH: ")
							SAVE_INT_TO_DEBUG_FILE(GET_ENTITY_MAX_HEALTH(scene_veh))
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
							
						//	VECTOR vFrontLeftWheelOffset	= << -1.0, 1.5, -0.5 >>
						//	VECTOR vFrontRightWheelOffset	= << 1.0, 1.5, -0.5 >>
						//	VECTOR vRearLeftWheelOffset		= << -1.0, -1.5, -0.5 >>
						//	VECTOR vRearRightWheelOffset	= << 1.0, -1.5, -0.5 >>
							
							VECTOR vFrontLeftDamageOffset	= << -1.0, 1.0, 0.2 >>
							VECTOR vFrontRightDamageOffset	= << 1.0, 1.0, 0.2 >>
							VECTOR vRearLeftDamageOffset	= << -1.0, -1.0, 0.2 >>
							VECTOR vRearRightDamageOffset	= << 1.0, -1.0, 0.2 >>
							
							FLOAT fDamage					= (1.0 - (GET_ENTITY_HEALTH(scene_veh) / GET_ENTITY_MAX_HEALTH(scene_veh))) * 100.0
							FLOAT fDeformation				= (1.0 - (GET_ENTITY_HEALTH(scene_veh) / GET_ENTITY_MAX_HEALTH(scene_veh))) * 100.0
							
							SET_VEHICLE_DAMAGE(scene_veh, vFrontLeftDamageOffset, fDamage, fDeformation, TRUE)
							SET_VEHICLE_DAMAGE(scene_veh, vFrontRightDamageOffset, fDamage, fDeformation, TRUE)
							SET_VEHICLE_DAMAGE(scene_veh, vRearLeftDamageOffset, fDamage, fDeformation, TRUE)
							SET_VEHICLE_DAMAGE(scene_veh, vRearRightDamageOffset, fDamage, fDeformation, TRUE)
							
							CPRINTLN(DEBUG_SWITCH, "SET_VEHICLE_DAMAGE(scene_veh, ", fDamage, ", ", fDeformation, ", TRUE)")
							
						ENDIF
					ELSE
						//scene_veh
					ENDIF	
				ELSE
				
//					#IF IS_DEBUG_BUILD
//					TEXT_LABEL_63 sInvalid
//					sInvalid = "unrequired veh pos for eScene: "
//					sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
//					
//					CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
//					SCRIPT_ASSERT(sInvalid)
//					#ENDIF
				ENDIF
				
				
				IF g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel != DUMMY_MODEL_FOR_SCRIPT
					IF bCreateVehicle
						IF (sVehData.model != g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel)
						OR NOT ARE_STRINGS_EQUAL(sVehData.tlNumberPlate, g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].tlPlateText)
							CPRINTLN(DEBUG_SWITCH, "KR TEST - switch didnt create same vehicle so send stored data to impound")
							SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed], sTimetableScene.sScene.ePed)
							g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel = DUMMY_MODEL_FOR_SCRIPT
						ENDIF
					ELSE
						CPRINTLN(DEBUG_SWITCH, "KR TEST - switch didnt create a vehicle so send stored data to impound")
						SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed], sTimetableScene.sScene.ePed)
						g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[sTimetableScene.sScene.ePed].eModel = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
				ENDIF
			ELSE
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("vehicle_gen_controller")) > 0
					IF NOT HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER()
						bWaitForBuddyAssets = FALSE
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("veh gens not loaded near player ")
						str += g_sVehicleGenNSData.iCheckVehGensLoadedCounter
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
					ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("ignore veh gens loading...")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_REDLIGHT)
					iDrawSceneRot += 1
					#ENDIF

				ENDIF
			ENDIF
			
		ELSE
			//IF sTimetableScene.eVehState <> PTVS_0_noVehicle
		ENDIF
		
		//
		IF NOT DOES_ENTITY_EXIST(player_prop)
			MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
			VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
			PED_BONETAG eBonetag = BONETAG_NULL
			FLOAT fDetachAnimPhase = -1.0
			enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
			
			IF GET_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
						eObjectModel, vecOffset, vecRotation, eBonetag,
						fDetachAnimPhase, thisSceneObjectAction)
				bWaitForBuddyAssets = FALSE
				
				IF (thisSceneObjectAction = PSOA_3_world)
				AND (IS_PLAYER_SWITCH_IN_PROGRESS() AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT)
					thisSceneObjectAction = PSOA_0_detach
				ENDIF
				
				IF (thisSceneObjectAction <> PSOA_3_world)
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str2 = ("creating prop ")
					str2 += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str2, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
					
					REQUEST_MODEL(eObjectModel)
					
					IF (thisSceneObjectAction = PSOA_4_synchSwap)
					OR (thisSceneObjectAction = PSOA_9_synchAndPtfxSwap)

						MODEL_NAMES eObjectSwapModel
						eObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
						SWITCH eObjectModel
							CASE P_Laptop_02_S
								eObjectSwapModel = PROP_LAPTOP_02_CLOSED
							BREAK
							CASE V_ILEV_M_DINECHAIR
								eObjectSwapModel = P_DINECHAIR_01_S
							BREAK
							CASE PROP_ACC_GUITAR_01
								eObjectSwapModel = Prop_ACC_Guitar_01_D1
							BREAK
							CASE P_DEFILIED_RAGDOLL_01_S
								eObjectSwapModel = PROP_DEFILIED_RAGDOLL_01
							BREAK
							CASE P_HAND_TOILET_S
								eObjectSwapModel = Prop_ToiletFoot_Static
							BREAK
							DEFAULT
								eObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
								SCRIPT_ASSERT("invalid model")
								EXIT
							BREAK
						ENDSWITCH
					
						REQUEST_MODEL(eObjectSwapModel)
						IF NOT HAS_MODEL_LOADED(eObjectSwapModel)
							bWaitForBuddyAssets = FALSE
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = ("request swap model assets ")
							str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 1
							#ENDIF
						ENDIF
					ENDIF
					
					IF (thisSceneObjectAction = PSOA_8_ptfxSwap)
					OR (thisSceneObjectAction = PSOA_9_synchAndPtfxSwap)
						REQUEST_PTFX_ASSET()
						IF NOT HAS_PTFX_ASSET_LOADED()
							bWaitForBuddyAssets = FALSE
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = ("request ptfx asstes ")
							str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 1
							#ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_MODEL_LOADED(eObjectModel)
						REQUEST_MODEL(eObjectModel)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("request object model ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
					
					ELIF (sTimetableScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
					AND NOT DOES_ENTITY_EXIST(player_door_r)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("tanisha waiting for door clear ")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(player_extra_prop)
							IF ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(player_extra_prop), sTimetableScene.sScene.vCreateCoords+vecOffset)
								vecOffset += <<0.2,0.2,0.2>>
							ENDIF
						ENDIF
						
						player_prop = CREATE_OBJECT(eObjectModel, sTimetableScene.sScene.vCreateCoords+vecOffset)
						FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_prop, sTimetableScene.sScene.eScene)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("get closest object ")
					str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
					
					player_prop = GET_CLOSEST_OBJECT_OF_TYPE(sTimetableScene.sScene.vCreateCoords+vecOffset, 5.0, eObjectModel)
					IF (player_prop = player_extra_prop)
						player_prop = NULL
					ENDIF

					IF NOT DOES_ENTITY_EXIST(player_prop)
						REQUEST_MODEL(eObjectModel)
					ELSE
						
						#IF IS_DEBUG_BUILD	
						IF NOT ARE_VECTORS_EQUAL(vecOffset, <<0,0,0>>)
							IF (eBonetag <> BONETAG_NULL)
								VECTOR vRealOffset = GET_ENTITY_COORDS(player_prop)-sTimetableScene.sScene.vCreateCoords
								IF NOT ARE_VECTORS_ALMOST_EQUAL(vecOffset, vRealOffset, 0.1)
									SAVE_STRING_TO_DEBUG_FILE("ePropModel = ")
									SAVE_STRING_TO_DEBUG_FILE(SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel))
									SAVE_STRING_TO_DEBUG_FILE("	propOffset = ")
									SAVE_VECTOR_TO_DEBUG_FILE(vRealOffset)
									SAVE_STRING_TO_DEBUG_FILE("	//")
									SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
									SAVE_NEWLINE_TO_DEBUG_FILE()
									
									CPRINTLN(DEBUG_SWITCH, "	vecOffset[", vecOffset, "] <> vRealOffset[", vRealOffset, "]: ", VDIST(vecOffset, vRealOffset))
									SCRIPT_ASSERT("GET_OBJECTS_FOR_SCENE - update vecOffset")
								ENDIF
							ENDIF
						ENDIF
						#ENDIF
						
						IF (eBonetag <> BONETAG_NULL)
							//attached
						ELSE
							IF fDetachAnimPhase >= 0
								SET_ENTITY_COORDS(player_prop, sTimetableScene.sScene.vCreateCoords+vecOffset)
								IF eobjectmodel = PROP_CHATEAU_TABLE_01
									CLEAR_AREA_OF_OBJECTS(sTimetableScene.sScene.vCreateCoords+vecOffset,0.5)
									CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(sTimetableScene.sScene.vCreateCoords+vecOffset,0.5,eObjectModel,FALSE)
								ENDIF
							ELSE
								SET_ENTITY_COLLISION(player_prop, FALSE)
								SET_ENTITY_VISIBLE(player_prop, FALSE)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
				
			ENDIF
		ELSE
			TEXT_LABEL_63 tLoopSyncObjAnim = "", tLoopSyncExtraObjAnim = ""
			GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene,
					tLoopSyncObjAnim, tLoopSyncExtraObjAnim)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tLoopSyncObjAnim)
				IF NOT DOES_ENTITY_HAVE_DRAWABLE(player_prop)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("needs drawable ")
					str += SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(player_prop))
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT DOES_ENTITY_EXIST(player_extra_prop)
			MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
			VECTOR vecExtraOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
			PED_BONETAG eBonetag = BONETAG_NULL
			FLOAT fDetachAnimPhase = -1.0
			enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
			BOOL bAttachedToBuddy = FALSE
			
			IF GET_EXTRA_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
						eObjectModel, vecExtraOffset, vecRotation, eBonetag,
						fDetachAnimPhase, thisSceneObjectAction, bAttachedToBuddy)
				
				IF bAttachedToBuddy
					vecExtraOffset += <<0.1,0.1,0.1>>
				ENDIF
				
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str2 = ("creating extra ")
				str2 += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str2, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot += 2
				#ENDIF
				
				IF (thisSceneObjectAction <> PSOA_3_world)
					IF (eObjectModel = PROP_CS_IRONING_BOARD)
						
						MODEL_NAMES eTshirtObjectModel = Prop_T_Shirt_ironing
						
						REQUEST_MODEL(eObjectModel)
						REQUEST_MODEL(eTshirtObjectModel)
						
						IF (thisSceneObjectAction = PSOA_4_synchSwap)

							MODEL_NAMES eObjectSwapModel
							eObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
							SWITCH eObjectModel
								CASE P_Laptop_02_S
									eObjectSwapModel = PROP_LAPTOP_02_CLOSED
								BREAK
								CASE V_ILEV_M_DINECHAIR
									eObjectSwapModel = P_DINECHAIR_01_S
								BREAK
								DEFAULT
									eObjectSwapModel = DUMMY_MODEL_FOR_SCRIPT
									SCRIPT_ASSERT("invalid model")
									EXIT
								BREAK
							ENDSWITCH
						
							REQUEST_MODEL(eObjectSwapModel)
							IF NOT HAS_MODEL_LOADED(eObjectSwapModel)
								bWaitForBuddyAssets = FALSE
								
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 str = ("request swap model assets ")
								str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
								DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
								iDrawSceneRot += 1
								#ENDIF
							ENDIF
						ENDIF
						
						IF (thisSceneObjectAction = PSOA_8_ptfxSwap)
							REQUEST_PTFX_ASSET()
							IF NOT HAS_PTFX_ASSET_LOADED()
								bWaitForBuddyAssets = FALSE
								
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 str = ("request ptfx asstes ")
								str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
								DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
								iDrawSceneRot += 1
								#ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_MODEL_LOADED(eObjectModel)
							REQUEST_MODEL(eObjectModel)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 2
							#ENDIF
						ENDIF
						REQUEST_MODEL(eTshirtObjectModel)
						IF NOT HAS_MODEL_LOADED(eTshirtObjectModel)
							REQUEST_MODEL(eTshirtObjectModel)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = SAFE_GET_MODEL_NAME_FOR_DEBUG(eTshirtObjectModel)
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 2
							#ENDIF
						ENDIF
						
						IF HAS_MODEL_LOADED(eObjectModel)
						AND HAS_MODEL_LOADED(eTshirtObjectModel)
							IF DOES_ENTITY_EXIST(player_prop)
								IF ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(player_prop), sTimetableScene.sScene.vCreateCoords+vecExtraOffset)
									vecExtraOffset += <<0.1,0.1,0.1>>
								ENDIF
							ENDIF
							
							player_extra_prop = CREATE_OBJECT(eObjectModel, sTimetableScene.sScene.vCreateCoords+vecExtraOffset)
							OBJECT_INDEX tshirt_prop = CREATE_OBJECT(eTshirtObjectModel, sTimetableScene.sScene.vCreateCoords+vecExtraOffset+<<0,0,0.5>>)
							
							ATTACH_ENTITY_TO_ENTITY(tshirt_prop, player_extra_prop,
									0, <<0,0,0>>, <<0,0,0>>)
						ENDIF
					
					ELSE
						REQUEST_MODEL(eObjectModel)
						IF NOT HAS_MODEL_LOADED(eObjectModel)
							REQUEST_MODEL(eObjectModel)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = ("extra object model ")
							str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 2
							#ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(player_prop)
								IF ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(player_prop), sTimetableScene.sScene.vCreateCoords+vecExtraOffset)
									vecExtraOffset += <<0.2,0.2,0.2>>
								ENDIF
							ENDIF
							
							player_extra_prop = CREATE_OBJECT(eObjectModel, sTimetableScene.sScene.vCreateCoords+vecExtraOffset)
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_extra_prop, sTimetableScene.sScene.eScene)
							
							IF (eObjectModel = prop_yoga_mat_01)
								
								FLOAT lodDist
								lodDist = VDIST(<<-786.4108, 186.3641, 185.8040>>, <<-790.811,186.481,71.847>>)
								lodDist *= 1.25
								
								SET_ENTITY_LOD_DIST(player_extra_prop, ROUND(lodDist))
							ENDIF
							
						ENDIF
					
					ENDIF
				ELSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("get closest extra ")
					str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel)
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
					
					player_extra_prop = GET_CLOSEST_OBJECT_OF_TYPE(sTimetableScene.sScene.vCreateCoords+vecExtraOffset, 5.0, eObjectModel)
					IF (player_extra_prop = player_prop)
						player_extra_prop = NULL
					ENDIF
					IF DOES_ENTITY_EXIST(player_extra_prop)
						
						#IF IS_DEBUG_BUILD	
						IF NOT ARE_VECTORS_EQUAL(vecExtraOffset, <<0,0,0>>)
							VECTOR vRealExtraOffset = GET_ENTITY_COORDS(player_extra_prop)-sTimetableScene.sScene.vCreateCoords
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecExtraOffset, vRealExtraOffset, 0.1)
								SAVE_STRING_TO_DEBUG_FILE("eExtraObjectModel = ")
								SAVE_STRING_TO_DEBUG_FILE(SAFE_GET_MODEL_NAME_FOR_DEBUG(eObjectModel))
								SAVE_STRING_TO_DEBUG_FILE("	extraPropOffset = ")
								SAVE_VECTOR_TO_DEBUG_FILE(vRealExtraOffset)
								SAVE_STRING_TO_DEBUG_FILE("	//")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								CPRINTLN(DEBUG_SWITCH, "	vecExtraOffset[", vecExtraOffset, "] <> vRealExtraOffset[", vRealExtraOffset, "]: ", VDIST(vecExtraOffset, vRealExtraOffset))
								SCRIPT_ASSERT("GET_EXTRA_OBJECTS_FOR_SCENE - update vecExtraOffset")
							ENDIF
						ENDIF
						#ENDIF
						
						IF (eBonetag <> BONETAG_NULL)
							//attached
						ELSE
							IF fDetachAnimPhase >= 0
								SET_ENTITY_COORDS(player_extra_prop, sTimetableScene.sScene.vCreateCoords+vecExtraOffset)
							ELSE
								SET_ENTITY_COLLISION(player_extra_prop, FALSE)
								SET_ENTITY_VISIBLE(player_extra_prop, FALSE)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			TEXT_LABEL_63 tLoopSyncObjAnim = "", tLoopSyncExtraObjAnim = ""
			GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene,
					tLoopSyncObjAnim, tLoopSyncExtraObjAnim)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tLoopSyncExtraObjAnim)
				IF NOT DOES_ENTITY_HAVE_DRAWABLE(player_extra_prop)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("needs x-drawable ")
					str += SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(player_extra_prop))
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 2
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MODEL_NAMES eDoorModel_L = DUMMY_MODEL_FOR_SCRIPT
		MODEL_NAMES eDoorModel_R = DUMMY_MODEL_FOR_SCRIPT
		
		VECTOR vDoorOffset_L = <<0,0,0>>, vDoorOffset_R= <<0,0,0>>
		DOOR_HASH_ENUM eDoorHash_L = DUMMY_DOORHASH, eDoorHash_R = DUMMY_DOORHASH
		BOOL bReplaceDoor = TRUE
		FLOAT fHideDoorRadius = -1
		
		IF GET_DOORS_FOR_SCENE(sTimetableScene.sScene.eScene,
					eDoorModel_L, eDoorModel_R, vDoorOffset_L, vDoorOffset_R,
					eDoorHash_L, eDoorHash_R,
					bReplaceDoor, fHideDoorRadius)
			IF eDoorModel_L <> DUMMY_MODEL_FOR_SCRIPT
				
				IF NOT DOES_ENTITY_EXIST(player_door_l)
					bWaitForBuddyAssets = FALSE
				
					REQUEST_MODEL(eDoorModel_L)
					IF NOT HAS_MODEL_LOADED(eDoorModel_L)
						REQUEST_MODEL(eDoorModel_L)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("request object model ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_L)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
						
					ELSE
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("creating object model ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_L)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
						
						IF (eDoorHash_L <> DUMMY_DOORHASH)
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_L))
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L), 1.0, DEFAULT, TRUE)
								
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L), 1.0)")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_L)")
							ENDIF
						ENDIF
						
						IF (fHideDoorRadius > 0)
							VECTOR vDoorHide_L = sTimetableScene.sScene.vCreateCoords+vDoorOffset_L
							IF eDoorModel_L <> eDoorModel_r OR eDoorModel_L = PROP_CHATEAU_CHAIR_01 
							OR NOT DOES_ENTITY_EXIST(player_door_r)
								CREATE_MODEL_HIDE(vDoorHide_L, fHideDoorRadius, eDoorModel_L, FALSE)
								
								CPRINTLN(DEBUG_SWITCH, "CREATE_DOOR_L_HIDE(", vDoorHide_L, ", ", fHideDoorRadius, ", ", SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_L), ", FALSE)")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "WAIT_TO_CREATE_DOOR_L_HIDE(", vDoorHide_L, ", ", fHideDoorRadius, ", ", SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_L), ", FALSE)")
							ENDIF
						ENDIF
						
						IF bReplaceDoor
							player_door_l = CREATE_OBJECT(eDoorModel_L, sTimetableScene.sScene.vCreateCoords+vDoorOffset_L)
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_door_l, sTimetableScene.sScene.eScene)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(eDoorModel_L)
						ELSE
							player_door_l = GET_CLOSEST_OBJECT_OF_TYPE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_L, 5.0, eDoorModel_L)
							IF (player_door_l = player_door_r)
								player_door_l = NULL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF eDoorModel_R <> DUMMY_MODEL_FOR_SCRIPT
				IF NOT DOES_ENTITY_EXIST(player_door_r)
					bWaitForBuddyAssets = FALSE
					
					REQUEST_MODEL(eDoorModel_r)
					IF NOT HAS_MODEL_LOADED(eDoorModel_r)
						REQUEST_MODEL(eDoorModel_r)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("request object model ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_r)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
					ELSE
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("creating object model ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_r)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
						
						IF (eDoorHash_R <> DUMMY_DOORHASH)
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_R))
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R), 1.0, DEFAULT, TRUE)
								
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R), 1.0)")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_R)")
							ENDIF
						ENDIF
						
						IF (fHideDoorRadius > 0)
							VECTOR vDoorHide_R = sTimetableScene.sScene.vCreateCoords+vDoorOffset_r
							IF eDoorModel_r <> eDoorModel_L OR eDoorModel_r = PROP_CHATEAU_CHAIR_01 
							OR NOT DOES_ENTITY_EXIST(player_door_l)
								CREATE_MODEL_HIDE(vDoorHide_R, fHideDoorRadius, eDoorModel_r, FALSE)
							
								CPRINTLN(DEBUG_SWITCH, "CREATE_DOOR_R_HIDE(", vDoorHide_R, ", ", fHideDoorRadius, ", ", SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_r), ", FALSE)")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "WAIT_TO_CREATE_DOOR_R_HIDE(", vDoorHide_R, ", ", fHideDoorRadius, ", ", SAFE_GET_MODEL_NAME_FOR_DEBUG(eDoorModel_r), ", FALSE)")
							ENDIF
						ENDIF
						
						IF bReplaceDoor
							player_door_r = CREATE_OBJECT(eDoorModel_r, sTimetableScene.sScene.vCreateCoords+vDoorOffset_r)
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_door_r, sTimetableScene.sScene.eScene)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(eDoorModel_r)
						ELSE
							player_door_r = GET_CLOSEST_OBJECT_OF_TYPE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_r, 5.0, eDoorModel_r)
							IF (player_door_r = player_door_l)
								player_door_r = NULL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		TEXT_LABEL_63 entitySetName = ""
		BOOL bActivateSet = FALSE
		IF SETUP_INTERIOR_ENTITY_SET_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, entitySetName, bActivateSet)
			IF GET_HASH_KEY(entitySetName) = HASH("showhome_only")
				IF bActivateSet
					SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SHOWHOME, BUILDINGSTATE_DESTROYED)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_ES_FRANKLINS_HILLS_SAVEHOUSE_SHOWHOME, BUILDINGSTATE_NORMAL)
				ENDIF
			ELIF GET_HASH_KEY(entitySetName) = HASH("V_Michael_bed_messy")
				IF bActivateSet
					SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_BED, BUILDINGSTATE_DESTROYED)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_ES_MICHAEL_BED, BUILDINGSTATE_NORMAL)
				ENDIF
			ENDIF
		ENDIF
		
//		TEXT_LABEL TCmod = ""
//		IF SETUP_TIMECYCLE_MOD_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, TCmod)
//		ENDIF
		
//		TEXT_LABEL_63 fxName = ""
//		IF SETUP_PTFX_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, fxName)
//			REQUEST_PTFX_ASSET()
//			IF NOT HAS_PTFX_ASSET_LOADED()
//				bWaitForBuddyAssets = FALSE
//				REQUEST_PTFX_ASSET()
//				
//				#IF IS_DEBUG_BUILD
//				TEXT_LABEL_63 str = ("request ptfx assets ")
//				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
//				iDrawSceneRot += 1
//				#ENDIF
//			ENDIF
//			
//		ENDIF
		
		IF (sTimetableScene.eSceneBuddy <> NO_CHARACTER)
			IF (sTimetableScene.eBuddyLoopTask = script_task_play_anim)
			OR (sTimetableScene.eBuddyOutTask = script_task_play_anim)
				TEXT_LABEL_63 tSceneBuddyAnimDict = "null", tSceneBuddyAnimLoop = "null", tSceneBuddyAnimOut = "null"
				ANIMATION_FLAGS buddyAnimLoopFlag = AF_DEFAULT, buddyAnimOutFlag = AF_DEFAULT
				IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
						buddyAnimLoopFlag, buddyAnimOutFlag)
					
					#IF IS_DEBUG_BUILD
					IF (sTimetableScene.eBuddyLoopTask <> SCRIPT_TASK_PLAY_ANIM
					AND sTimetableScene.eBuddyOutTask <> SCRIPT_TASK_PLAY_ANIM
					AND sTimetableScene.eBuddyLoopTask <> SCRIPT_TASK_SYNCHRONIZED_SCENE
					AND sTimetableScene.eBuddyOutTask <> SCRIPT_TASK_SYNCHRONIZED_SCENE)
						TEXT_LABEL_63 sInvalid
						sInvalid  = "buddy animation needed for "
						sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
						sInvalid += "?"
						
						CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
						SCRIPT_ASSERT(sInvalid)
					ENDIF
					#ENDIF
					
					REQUEST_ANIM_DICT(tSceneBuddyAnimDict)
					IF NOT HAS_ANIM_DICT_LOADED(tSceneBuddyAnimDict)
						bWaitForBuddyAssets = FALSE
						REQUEST_ANIM_DICT(tSceneBuddyAnimDict)
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("request buddy anim ")
						str += (tSceneBuddyAnimDict)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 2
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(scene_buddy)
				
				IF (sTimetableScene.eSceneBuddy <> CHAR_BLANK_ENTRY)
					
					//buddy is a story char
					IF (sTimetableScene.eVehState <> PTVS_2_playerInVehicle)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REPEAT GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), temp_ped) iPed
								IF NOT IS_PED_INJURED(temp_ped[iPed])
									IF GET_ENTITY_MODEL(temp_ped[iPed]) = GET_NPC_PED_MODEL(sTimetableScene.eSceneBuddy)
										scene_buddy = temp_ped[iPed]
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
						
						IF NOT IS_PED_INJURED(scene_buddy)
							SET_ENTITY_COORDS(scene_buddy, sTimetableScene.sScene.vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset)
							SET_ENTITY_HEADING(scene_buddy, sTimetableScene.sScene.fCreateHead+sTimetableScene.fSceneBuddyHeadOffset)
							
							PRIVATE_SetDefaultSceneBuddyAttributes(scene_buddy, sTimetableScene.eSceneBuddy)
							PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
						ELSE
							
							IF NOT CREATE_NPC_PED_ON_FOOT(scene_buddy, sTimetableScene.eSceneBuddy,
									sTimetableScene.sScene.vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset,
									sTimetableScene.sScene.fCreateHead+sTimetableScene.fSceneBuddyHeadOffset)
								bWaitForBuddyAssets = FALSE
								
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 str = ("creating NPC on foot")
								DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
								iDrawSceneRot += 1
								#ENDIF
							ELSE
								PRIVATE_SetDefaultSceneBuddyAttributes(scene_buddy, sTimetableScene.eSceneBuddy)
								PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(scene_veh)
							IF NOT IS_VEHICLE_SEAT_FREE(scene_veh, VS_FRONT_RIGHT)
								
								PED_INDEX frontRightPed = GET_PED_IN_VEHICLE_SEAT(scene_veh, VS_FRONT_RIGHT)
								IF NOT IS_ENTITY_DEAD(frontRightPed)
									bWaitForBuddyAssets = FALSE
							
									#IF IS_DEBUG_BUILD
									TEXT_LABEL_63 str = ("creating ped in veh")
									DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
									iDrawSceneRot += 2
									#ENDIF
									
									IF (GET_ENTITY_MODEL(frontRightPed) = GET_NPC_PED_MODEL(sTimetableScene.eSceneBuddy))
										scene_buddy = frontRightPed
										PRIVATE_SetDefaultSceneBuddyAttributes(scene_buddy, sTimetableScene.eSceneBuddy)
										PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
									ENDIF
								ENDIF
								
							ELSE
								IF NOT CREATE_NPC_PED_INSIDE_VEHICLE(scene_buddy, sTimetableScene.eSceneBuddy,
										scene_veh, VS_FRONT_RIGHT)
									bWaitForBuddyAssets = FALSE
							
									#IF IS_DEBUG_BUILD
									TEXT_LABEL_63 str = ("creating NPC in veh")
									DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
									iDrawSceneRot += 1
									#ENDIF
								ELSE
									PRIVATE_SetDefaultSceneBuddyAttributes(scene_buddy, sTimetableScene.eSceneBuddy)
									PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//buddy ISNT a story char (e.g. stripper)
					
					PED_TYPE PedType = PEDTYPE_MISSION
					MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
					SETUP_BLANK_BUDDY_FOR_SCENE(sTimetableScene.sScene.eScene, PedType, model)
					
					IF (sTimetableScene.eVehState <> PTVS_2_playerInVehicle)
						
						bWaitForBuddyAssets = FALSE
							
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("creating ")
						str += SAFE_GET_MODEL_NAME_FOR_DEBUG(model)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
						
						IF (model = GET_CHOP_MODEL())
							IF CREATE_CHOP(scene_buddy,
									sTimetableScene.sScene.vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset,
									sTimetableScene.sScene.fCreateHead+sTimetableScene.fSceneBuddyHeadOffset)
								PRIVATE_SetDefaultSceneBuddyAttributes(scene_buddy, sTimetableScene.eSceneBuddy)
								REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")
							ENDIF
							
						ELSE
							REQUEST_MODEL(model)
					        IF HAS_MODEL_LOADED(model)
					            scene_buddy = CREATE_PED(PedType, model,
										sTimetableScene.sScene.vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset,
										sTimetableScene.sScene.fCreateHead+sTimetableScene.fSceneBuddyHeadOffset,
										FALSE, FALSE)
								g_pScene_buddy = scene_buddy
								PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
								
								
								SET_MODEL_AS_NO_LONGER_NEEDED(model)
					        ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(scene_veh)
							IF NOT IS_VEHICLE_SEAT_FREE(scene_veh, VS_FRONT_RIGHT)
								
								PED_INDEX frontRightPed = GET_PED_IN_VEHICLE_SEAT(scene_veh, VS_FRONT_RIGHT)
								IF NOT IS_ENTITY_DEAD(frontRightPed)
									bWaitForBuddyAssets = FALSE
							
									#IF IS_DEBUG_BUILD
									TEXT_LABEL_63 str = ("grabbing passenger")
									DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
									iDrawSceneRot += 1
									#ENDIF
									
									IF (GET_ENTITY_MODEL(frontRightPed) = model)
										scene_buddy = frontRightPed
										
										g_pScene_buddy = scene_buddy
									ENDIF
								ENDIF
							ELSE
								bWaitForBuddyAssets = FALSE
							
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 str = ("creating passenger")
								DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
								iDrawSceneRot += 1
								#ENDIF
								
								REQUEST_MODEL(model)
						        IF HAS_MODEL_LOADED(model)
						            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
						            // networked objects when it is not a net script. 
						            IF DOES_ENTITY_EXIST(scene_buddy)
						                DELETE_PED(scene_buddy)
						            ENDIF
						            
						            IF IS_VEHICLE_DRIVEABLE(scene_veh)
						                scene_buddy = CREATE_PED_INSIDE_VEHICLE(scene_veh,
												PedType, model, VS_FRONT_RIGHT)
						                
										g_pScene_buddy = scene_buddy
										PRIVATE_SetDefaultSceneBuddyCompVar(scene_buddy, sTimetableScene.eSceneBuddy, sTimetableScene.sScene.eScene)
										
										SET_MODEL_AS_NO_LONGER_NEEDED(model)
						            ENDIF
						        ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF NOT IS_ENTITY_DEAD(scene_buddy)
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(scene_buddy)
				AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(scene_buddy)
					//
				ELSE
					bWaitForBuddyAssets = FALSE
				
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("waiting for ped to preload")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 1
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		TEXT_LABEL_63 tBankName = "", tBankBName = ""
		IF SETUP_AUDIO_BANK_FOR_SCENE(sTimetableScene.sScene.eScene, tBankName, tBankBName)
			REGISTER_SCRIPT_WITH_AUDIO()
			IF NOT REQUEST_AMBIENT_AUDIO_BANK(tBankName)
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("request sfx ")
				str += (tBankName)
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot ++
				
//				SAVE_STRING_TO_DEBUG_FILE(str)
//				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				#ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tBankBName)
				IF NOT REQUEST_AMBIENT_AUDIO_BANK(tBankBName)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("request sfx ")
					str += (tBankBName)
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot ++
					
	//				SAVE_STRING_TO_DEBUG_FILE(str)
	//				SAVE_NEWLINE_TO_DEBUG_FILE()
					
					#ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF sTimetableScene.sScene.ePed <> GET_CURRENT_PLAYER_PED_ENUM()	//
			bWaitForBuddyAssets = FALSE
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 tScene = Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
		    TEXT_LABEL_63 str1 = GET_STRING_FROM_STRING(tScene, GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"), GET_LENGTH_OF_LITERAL_STRING(tScene))
			DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str1, iDrawSceneRot, HUD_COLOUR_RED)
			iDrawSceneRot++
			
			TEXT_LABEL_63 str2 = ("ePed ")
			str2 += GET_PLAYER_PED_STRING(sTimetableScene.sScene.ePed)
			DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str2, iDrawSceneRot, HUD_COLOUR_RED)
			iDrawSceneRot++
			
			TEXT_LABEL_63 str3 = "current "
			str3 += GET_PLAYER_PED_STRING(GET_CURRENT_PLAYER_PED_ENUM())
			DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str3, iDrawSceneRot, HUD_COLOUR_RED)
			iDrawSceneRot++
			#ENDIF
			
		ELIF IS_PLAYER_SWITCH_IN_PROGRESS() AND (GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT)
		AND (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT OR GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_PAN)
			bWaitForBuddyAssets = FALSE
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("ped state ")
			str += Get_String_From_Switch_State(GET_PLAYER_SWITCH_STATE())
			DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
			iDrawSceneRot++
			#ENDIF
			
		ELSE
			IF SET_PED_PRESET_OUTFIT_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene, PLAYER_PED_ID(), iPlayerOutfitPreloadStage)
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("preload outfit ")
				str += (iPlayerOutfitPreloadStage)
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot++
				#ENDIF
			ELIF NOT (HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
			AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID()))
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("prestreamed outfit ")
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot++
				#ENDIF
			ENDIF
		ENDIF
		
		TEXT_LABEL_63 tAudioEvent = ""
		INT iStartOffsetMs = 0
		IF SETUP_SYNCH_AUDIO_EVENT_FOR_SCENE(sTimetableScene.sScene.eScene, tAudioEvent, iStartOffsetMs)
			IF NOT PREPARE_SYNCHRONIZED_AUDIO_EVENT(tAudioEvent, iStartOffsetMs)
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("request event ")
				str += (tAudioEvent)
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot ++
				
				CPRINTLN(DEBUG_SWITCH, "PREPARE_SYNCHRONIZED_AUDIO_EVENT ", tAudioEvent)
				#ENDIF
			ENDIF
		ENDIF
		
		IF g_bMagDemoActive
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_FALLING(PLAYER_PED_ID())
					
					#IF IS_DEBUG_BUILD
					SAVE_STRING_TO_DEBUG_FILE("SETUP falling... ")
					#ENDIF
					
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE, "SETUP falling... ")
					bFreezePlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		TEXT_LABEL_31 RoomName = ""
		FLOAT fTurnOffTVPhase = 0.0
		TV_LOCATION eRoomTVLocation = TV_LOC_NONE
		TVCHANNELTYPE eTVChannelType = TVCHANNELTYPE_CHANNEL_NONE
		TV_CHANNEL_PLAYLIST eTVPlaylist = TV_PLAYLIST_NONE

		IF CONTROL_PLAYER_WATCHING_TV(sTimetableScene.sScene.eScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
			IF NOT IS_STRING_NULL_OR_EMPTY(RoomName)

//				//#1321543
//				REQUEST_MODELS_IN_ROOM(sTimetableScene.sScene.roomInterior, RoomName)
				
				IF IS_VALID_INTERIOR(sTimetableScene.sScene.roomInterior)
					IF IS_INTERIOR_READY(sTimetableScene.sScene.roomInterior)
						REQUEST_MODELS_IN_ROOM(sTimetableScene.sScene.roomInterior, RoomName)
						
						IF NOT IS_TV_SCRIPT_AVAILABLE_FOR_USE(eRoomTVLocation)
							bWaitForBuddyAssets = FALSE
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = ("tv[")
							str += ENUM_TO_INT(eRoomTVLocation)
							str += ("] not available for use...")
							DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
							iDrawSceneRot += 1
							#ENDIF
						ENDIF
						
						IF IS_THIS_TV_AVAILABLE_FOR_USE(eRoomTVLocation)
						AND NOT bSwitchForcedStartAmbientTV
							START_AMBIENT_TV_PLAYBACK(eRoomTVLocation, eTVChannelType, eTVPlaylist)
							bSwitchForcedStartAmbientTV = TRUE
						ENDIF
						
					ELSE
						bWaitForBuddyAssets = FALSE
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("tv interior[")
						str += (RoomName)
						str += ("] not ready...")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
					ENDIF
				
				ELSE
					IF iWaitForBuddyFrames < ROUND(TO_FLOAT(iCONST_WAIT_FOR_BUDDY_MAX_FRAMES) * 0.9)
						bWaitForBuddyAssets = FALSE
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("tv interior[")
						str += (RoomName)
						str += ("] not valid...")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("tv interior[")
						str += (RoomName)
						str += ("] not valid...")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_REDLIGHT)
						iDrawSceneRot += 1
						#ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		SP_MISSIONS eLeadInToCreate = SP_MISSION_NONE
		IF g_savedGlobals.sRepeatPlayData.eMission != SP_MISSION_NONE
		
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			#ENDIF
			
			eLeadInToCreate = g_savedGlobals.sRepeatPlayData.eMission //Store this value to use after the struct reset.
			IF IS_BIT_SET(g_sMissionStaticData[eLeadInToCreate].settingsBitset, MF_INDEX_HAS_LEADIN)
				SWITCH iLeadinRequestStage
					CASE 0
					//	CPRINTLN(DEBUG_SWITCH, "Waiting for SP scripts to intialise before preloading mission lead-in.")
					//	WHILE NOT g_isSPMainInitialised
					//		WAIT(0)
					//	ENDWHILE
					//	CPRINTLN(DEBUG_SWITCH, "SP scripts intialised.")
					//	MISSION_FLOW_PRELOAD_ASSETS_FOR_MISSION_TRIGGER(eLeadInToCreate)
						bWaitForBuddyAssets = FALSE
						
						#IF IS_DEBUG_BUILD
						str = ("Leadin - initialising SP scripts")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
						
						IF g_isSPMainInitialised
							CPRINTLN(DEBUG_SWITCH, "SP scripts intialised.")
							
							MISSION_FLOW_PRELOAD_ASSETS_FOR_MISSION_TRIGGER(eLeadInToCreate)
							iLeadinRequestStage = 1
						ENDIF
						
					BREAK
					CASE 1
					//	CPRINTLN(DEBUG_SWITCH, "Starting to wait for mission trigger to pick up preload request.")
					//	WHILE g_eMissionSceneToPreLoad != SP_MISSION_NONE
					//		WAIT(0)
					//	ENDWHILE
					//	WAIT(0)
					//	CPRINTLN(DEBUG_SWITCH, "Finished waiting for mission trigger to pick up preload request.")
						bWaitForBuddyAssets = FALSE
						
						#IF IS_DEBUG_BUILD
						str = ("Leadin - preloading")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
						
						IF NOT (g_eMissionSceneToPreLoad != SP_MISSION_NONE)
							CPRINTLN(DEBUG_SWITCH, "Finished waiting for mission trigger to pick up preload request.")
							iLeadinRequestStage = 2
						ENDIF
						
					BREAK
					CASE 2
					//	CPRINTLN(DEBUG_SWITCH, "Starting to wait for lead-in to finish loading assets.")
					//	WHILE g_bMissionTriggerLoading
					//		WAIT(0)
					//	ENDWHILE
					//	CPRINTLN(DEBUG_SWITCH, "Lead-in scene loaded. Unflagging leave area flag so the scene creates.")
					//	Set_Leave_Area_Flag_For_Mission(eLeadInToCreate, FALSE)
						bWaitForBuddyAssets = FALSE
						
						#IF IS_DEBUG_BUILD
						str = ("Leadin - loading ")
						str += GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eLeadInToCreate)
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
						
						IF NOT g_bMissionTriggerLoading
							CPRINTLN(DEBUG_SWITCH, "Lead-in scene loaded. Unflagging leave area flag so the scene creates.")
							Set_Leave_Area_Flag_For_Mission(eLeadInToCreate, FALSE)
							iLeadinRequestStage = 3
						ENDIF
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
		IF sTimetableScene.sScene.eScene = PR_SCENE_T6_TRAF_AIR
			IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_PlaneCreated)
				IF iWaitForBuddyFrames < ROUND(TO_FLOAT(iCONST_WAIT_FOR_BUDDY_MAX_FRAMES) * 0.9)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("controller_Trafficking")) > 0
						TEXT_LABEL_63 str = ("Plane Not Created - no controller")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
					ELSE
						TEXT_LABEL_63 str = ("Plane Not Created - controller")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
					ENDIF
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("Plane Created")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_REDLIGHT)
					iDrawSceneRot += 1
					#ENDIF
				ENDIF
			ENDIF
	/*	ELIF sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_ARM3
//		OR sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_FAM1
//		OR sTimetableScene.sScene.eScene = PR_SCENE_Fa_STRIPCLUB_FAM3
			IF NOT IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_IN_CLUB)
				IF iWaitForBuddyFrames < ROUND(TO_FLOAT(iCONST_WAIT_FOR_BUDDY_MAX_FRAMES) * 0.9)
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("Strippers Not Created")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 1
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("Strippers Created")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_REDLIGHT)
					iDrawSceneRot += 1
					#ENDIF
				ENDIF
			ENDIF	*/
		ELIF sTimetableScene.sScene.eScene = PR_SCENE_M2_SMOKINGGOLF
			IF IS_GOLF_CLUB_OPEN_AT_TIME_OF_DAY(GET_CLOCK_HOURS()) //golf club is open
			AND GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_GOLF))
				IF NOT IS_BITMASK_AS_ENUM_SET(g_sGolfGlobals.GlobalGolfControlFlag, GGCF_AMBIENT_GOLFERS_STREAMED_IN)
					IF iWaitForBuddyFrames < ROUND(TO_FLOAT(iCONST_WAIT_FOR_BUDDY_MAX_FRAMES) * 0.9)
						bWaitForBuddyAssets = FALSE
						
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("Golfer Not Created")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot += 1
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str = ("Golfer Created")
						DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_REDLIGHT)
						iDrawSceneRot += 1
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (sTimetableScene.eOutTask = SCRIPT_TASK_USE_MOBILE_PHONE)
			IF sTimetableScene.sScene.ePed <> GET_CURRENT_PLAYER_PED_ENUM()	//
				bWaitForBuddyAssets = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("wait for ePed for call")
				DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
				iDrawSceneRot += 1
				#ENDIF
			ELSE
				IF WaitingForPhonecallToStartAndPause( #IF IS_DEBUG_BUILD iDrawSceneRot #ENDIF )
					bWaitForBuddyAssets = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("wait for call to start")
					DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot += 1
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// // #1240234 // // // // // // // // // // // // // // //
		IF sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT
		OR sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT
		OR sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT
			SP_MISSIONS eTriggerMission = TRIGGER_SCENE_GET_SP_MISSION()
			IF (eTriggerMission != SP_MISSION_NONE)
				
//				SELECTOR_SLOTS_ENUM playerSelectorID = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(sTimetableScene.sScene.ePed)
//				
//				IF IS_BIT_SET(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(playerSelectorID))
//					
//					
//					IF NOT IS_BIT_SET(g_iSwitchTriggerStateBitset, SWITCH_TRIG_IN_PROGRESS)
//						IF NOT ARE_STRINGS_EQUAL(g_txtIntroMocapToLoad, "NONE")
//							IF HAS_THIS_CUTSCENE_LOADED(g_txtIntroMocapToLoad)
//						        //Continue switch.
//							ELSE
//								bWaitForBuddyAssets = FALSE
//								
//								#IF IS_DEBUG_BUILD
//								TEXT_LABEL_63 str = ("trigger CS ")
//								str += g_txtIntroMocapToLoad
//								DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
//								iDrawSceneRot += 1
//								#ENDIF
//						    ENDIF
//						ENDIF
//						
//						IF IS_PLAYER_SWITCH_IN_PROGRESS()
//							IF (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_MEDIUM)
//							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//								IF NOT HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
//									bWaitForBuddyAssets = FALSE
//									
//									#IF IS_DEBUG_BUILD
//									TEXT_LABEL_63 str = ("wait on player collision")
//									DrawLiteralSceneStringWithPrint(iWaitForBuddyFrames, str, iDrawSceneRot, HUD_COLOUR_RED)
//									iDrawSceneRot += 1
//									#ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//
//					IF IS_THIS_TV_AVAILABLE_FOR_USE(eRoomTVLocation)
//						START_AMBIENT_TV_PLAYBACK(eRoomTVLocation)
//					ENDIF
//				
//				
//				ENDIF
				
			ENDIF
		ENDIF
		// // // // // // // // // // // // // // // // // // // //
		
		
		
		IF NOT bWaitForBuddyAssets
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				IF (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
					IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
						IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
							IF iWaitForCharChangeFrames > 0
								iWaitForCharChangeFrames = 0
								iWaitForBuddyFrames = 0
							ENDIF
							
							iWaitForBuddyFrames++
						ENDIF
					ELIF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_JUMPCUT_DESCENT
					OR GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_PREP_DESCENT
						IF iWaitForCharChangeFrames > 0
							iWaitForCharChangeFrames = 0
							iWaitForBuddyFrames = 0
						ENDIF
						
						iWaitForBuddyFrames++
					ELIF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT
					AND sTimetableScene.sScene.ePed <> GET_CURRENT_PLAYER_PED_ENUM()
						iWaitForCharChangeFrames++
						iWaitForBuddyFrames++
					ENDIF
				ENDIF
			ELSE
				IF iWaitForCharChangeFrames > 0
					iWaitForCharChangeFrames = 0
					iWaitForBuddyFrames = 0
				ENDIF
				
				iWaitForBuddyFrames++
			ENDIF
			
			IF iWaitForBuddyFrames >= iCONST_WAIT_FOR_BUDDY_MAX_FRAMES
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 sWaitForBuddyFrames
				sWaitForBuddyFrames  = "iWaitForBuddyFrames > "
				sWaitForBuddyFrames += iWaitForBuddyFrames
				sWaitForBuddyFrames += ": "
				sWaitForBuddyFrames += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
				
				#IF NOT IS_NEXTGEN_BUILD
				PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sWaitForBuddyFrames)PRINTNL()
//				SCRIPT_ASSERT(sWaitForBuddyFrames)
				#ENDIF
				#IF IS_NEXTGEN_BUILD
				CERRORLN(DEBUG_SHOPS, GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sWaitForBuddyFrames)PRINTNL()
				#ENDIF
				#ENDIF
				
				bWaitForBuddyAssets = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				bTriggeredFade = TRUE
				CWARNINGLN(DEBUG_SWITCH, "<TRIGGER FADE> force fade out and set as trigger script to speed up assert streaming (removing chance of having gameplay returned but limited)")
			ENDIF
		ENDIF
		
		WAIT(0)
		DisableWhileWaiting("Setup_Player_Timetable_Scene")
	ENDWHILE
	
//	// If we're switching into a mission don't end the switch camera until the game is faded.
//	IF IS_BIT_SET(g_iSwitchTriggerStateBitset, SWITCH_TRIG_IN_PROGRESS)
//		INT iTimeoutTime = GET_GAME_TIMER() + 10000
//		WHILE NOT IS_SCREEN_FADED_OUT() AND GET_GAME_TIMER() < iTimeoutTime
//			WAIT(0)
//		ENDWHILE
//	ENDIF
	
	Request_SpecialMissionAssets(sTimetableScene.sScene)
	
	IF bFreezePlayer
		ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "SETUP falling... ")
	ENDIF
	
//	eSceneAnimProgress = ePlayerSceneAnimProgress
	
	//- create any script vehicles -//
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	SafeDetachSynchronizedScene(g_iPlayer_Timetable_Loop_SynchSceneID)
	SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
	
	IF bKeepCellphoneDisabled
		DISABLE_CELLPHONE(TRUE)
	ENDIF
	
	PRIVATE_SetRoadsSwitchArea(sTimetableScene.sScene.eScene, FALSE)
	PRIVATE_SetPedInSwitchArea(sTimetableScene.sScene.eScene, FALSE)
	
	iPlayerOutfitPreloadStage = 0
	
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Timetable_Scene_Stage(PT_SCENE_STAGE_ENUM this_player_timetable_scene_stage)
	SWITCH this_player_timetable_scene_stage
		CASE WAIT_FOR_PLAYER_PED_TO_UPDATE
			RETURN "WAIT_FOR_PLAYER_PED_TO_UPDATE"
		BREAK
		CASE CONTROL_PLAYER_DURING_SWITCH
			RETURN "CONTROL_PLAYER_DURING_SWITCH"
		BREAK
		CASE PLAYER_SCENE_STARTED
			RETURN "PLAYER_SCENE_STARTED"
		BREAK
		CASE PLAYER_EXITING_SWITCH
			RETURN "PLAYER_EXITING_SWITCH"
		BREAK
		CASE PLAYER_ON_PHONE_ANIM
			RETURN "PLAYER_ON_PHONE_ANIM"
		BREAK
		CASE PLAYER_HAS_WARDROBE_ANIM
			RETURN "PLAYER_HAS_WARDROBE_ANIM"
		BREAK
		
		CASE FINISHED_TIMETABLED_SCENE
			RETURN "FINISHED_TIMETABLED_SCENE"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Timetable_Scene_Stage(null) "
ENDFUNC
//PURPOSE:	Initialises the mission widget
PROC Create_Player_Timetable_Scene_widget()
	INT iWidget
	TEXT_LABEL_63 str = "Player_Timetable_Scene.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	player_timetable_scene_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PT_SCENE_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Timetable_Scene_Stage(INT_TO_ENUM(PT_SCENE_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_timetable_scene_stage", iCurrent_player_timetable_scene_stage)
		
		ADD_WIDGET_BOOL("bTestWarp", bTestWarp)
		
		ADD_WIDGET_FLOAT_SLIDER("fDebugCamInfoDisplayX", fDebugCamInfoDisplayX, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDebugCamInfoDisplayY", fDebugCamInfoDisplayY, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDebugCamInfoOffsetY", fDebugCamInfoOffsetY, 0.0, 1.0, 0.001)
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Timetable_Scene_Widget()
	iCurrent_player_timetable_scene_stage		= ENUM_TO_INT(current_player_timetable_scene_stage)
	
	IF (current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE)
		IF bTestWarp
			PED_SCENE_STRUCT sSceneData
			PLAYER_TIMETABLE_SCENE_STRUCT sWarpScene
			
			sSceneData.iStage = 0
			sSceneData.eScene = INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, g_iSelectedDebugPlayerCharScene)
			GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sSceneData.eScene, sSceneData.ePed)
			
			GET_PLAYER_PED_POSITION_FOR_SCENE(sSceneData.eScene, sSceneData.vCreateCoords, sSceneData.fCreateHead, sSceneData.tCreateRoom)
			SETUP_PLAYER_TIMETABLE_FOR_SCENE(sSceneData, sWarpScene)
			
			PED_VEH_DATA_STRUCT sData    //MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
			VECTOR vVehCoordOffset = <<0,0,0>>
			FLOAT fVehHeadOffset = 0
			
			VECTOR vDriveOffset = <<0,0,0>>
			FLOAT fDriveSpeed = 0
			
			IF sWarpScene.eVehState <> PTVS_0_noVehicle
				GET_PLAYER_VEH_POSITION_FOR_SCENE(sWarpScene.sScene.ePed, sWarpScene.sScene.eScene,
						sData, vVehCoordOffset, fVehHeadOffset,
						vDriveOffset, fDriveSpeed)
			ENDIF
			
			VECTOR vCamEndPos, vCamEndRot
			FLOAT vCamEndFov = 0.0
			IF NOT SETUP_PLAYER_CAMERA_FOR_SCENE(sWarpScene.sScene.eScene, vCamEndPos, vCamEndRot, vCamEndFov)
				vCamEndPos = sSceneData.vCreateCoords + <<0,5,1>>
				vCamEndRot = <<0,0,180>>
			ENDIF
			
			IF sWarpScene.sScene.ePed < INT_TO_ENUM(enumCharacterList, NUM_OF_PLAYABLE_PEDS)
				g_vPlayerVeh[sWarpScene.sScene.ePed] = scene_veh
			ELSE
				g_vPlayerVeh[GET_CURRENT_PLAYER_PED_ENUM()] = scene_veh
			ENDIF
			
			Test_Player_Ped_Scene(sWarpScene,
					sData.model, player_timetable_scene_widget, vVehCoordOffset, fVehHeadOffset,
					vDriveOffset,
					vCamEndPos, vCamEndRot,
					player_prop, player_extra_prop,
					player_door_l, player_door_r,
					scene_veh)
			
			bTestWarp = FALSE
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Timetable_Scene_Debug_Options()
ENDPROC

//PURPOSE:	#1218160 Add 'temporary camera' text to the build for the procedural switches
//Seeing as its going to be a while till we get the code support for this, could you add that text to them all so reviews dont flag these as being broken. Thanks
//FUNC BOOL DrawDebugSceneCamInfo()
//
//	TEXT_LABEL_63 sLiteralOne63 = "", sLiteralTwo63 = ""
//	HUD_COLOURS eHudColour = HUD_COLOUR_PURE_WHITE
//	
//	IF DOES_CAM_EXIST(ExitCamIndex)
//		
//		TEXT_LABEL_63 tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix
//		IF GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix)
//			sLiteralOne63  = ("playing animated switch cam")
//			
//			sLiteralTwo63  = (" \"")
//			sLiteralTwo63 += (tPlayerSceneSyncExitCam)
//			sLiteralTwo63 += ("\"")
//			
//			eHudColour = HUD_COLOUR_PURE_WHITE
//		ELSE
//			sLiteralOne63  = ("ExitCamIndex \"nameless???\" exists???")
//			
//			eHudColour = HUD_COLOUR_REDLIGHT
//		ENDIF
//	ELSE
//		
//		TEXT_LABEL_23 tPlayerSceneProceduralCam
//		IF SETUP_PROCEDURAL_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneProceduralCam)
//			sLiteralOne63  = ("playing procedural switch cam")
//			
//			sLiteralTwo63  = (" \"")
//			sLiteralTwo63 += (tPlayerSceneProceduralCam)
//			sLiteralTwo63 += ("\"")
//			
//			GET_PLAYER_SWITCH_INTERP_OUT_DURATION()
//			GET_PLAYER_SWITCH_INTERP_OUT_CURRENT_TIME()
//			
//			eHudColour = HUD_COLOUR_BLUELIGHT
//		ELSE
//			sLiteralOne63 += ("ExitCamIndex doesnt exist")
//			
//			eHudColour = HUD_COLOUR_REDLIGHT
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_STRING_NULL_OR_EMPTY(sLiteralOne63)
//		SET_TEXT_SCALE(0.4, 0.4)
//		SET_TEXT_HUD_COLOUR(eHudColour, 127)
//		SET_TEXT_JUSTIFICATION(FONT_CENTRE)
//		SET_TEXT_OUTLINE()
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fDebugCamInfoDisplayX, fDebugCamInfoDisplayY+(0*fDebugCamInfoOffsetY), "STRING", sLiteralOne63)
//	ENDIF
//	IF NOT IS_STRING_NULL_OR_EMPTY(sLiteralTwo63)
//		SET_TEXT_SCALE(0.4, 0.4)
//		SET_TEXT_HUD_COLOUR(eHudColour, 127)
//		SET_TEXT_JUSTIFICATION(FONT_CENTRE)
//		SET_TEXT_OUTLINE()
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fDebugCamInfoDisplayX, fDebugCamInfoDisplayY+(1*fDebugCamInfoOffsetY), "STRING", sLiteralTwo63)
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL Safe_Create_Synchronized_Scene(INT &iScene, STRING sCreateInfo, VECTOR scenePosition, VECTOR sceneOrientation, EULER_ROT_ORDER RotOrder = EULER_YXZ)
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
		iScene = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneOrientation, RotOrder)
		
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Safe_Create_Synchronized_Scene[", iScene, "] not running // ", sCreateInfo)
		
		RETURN TRUE
	ENDIF
	
	SET_SYNCHRONIZED_SCENE_ORIGIN(iScene, scenePosition, sceneOrientation, RotOrder)
	
	CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Safe_Create_Synchronized_Scene[", iScene, "] running // ", sCreateInfo)
	
	RETURN FALSE
ENDFUNC



FUNC BOOL Get_Ped_Anim_Or_SynchedScene_Phase(PED_INDEX PedIndex, STRING AnimDictName, STRING AnimName, FLOAT &AnimCurrentTime)
	
	IF IS_ENTITY_DEAD(PedIndex)
		AnimCurrentTime = -1.0
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_PLAYING_ANIM(PedIndex, AnimDictName, AnimName, ANIM_SCRIPT)
		AnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, AnimDictName, AnimName)
		RETURN TRUE
	ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, AnimDictName, AnimName, ANIM_SYNCED_SCENE)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK)
				AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID)
				RETURN TRUE
			ENDIF
			
			AnimCurrentTime = -1.0
			RETURN FALSE
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK)
				AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
				RETURN TRUE
			ENDIF
			
			AnimCurrentTime = -1.0
			RETURN FALSE
		ENDIF
	ENDIF
	
	AnimCurrentTime = -1.0
	RETURN FALSE
ENDFUNC

FUNC BOOL SetPedFromVehicleIntoSynchedScene(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)
	IF NOT IS_ENTITY_DEAD(VehicleIndex)
		IF IS_PED_IN_VEHICLE(PedIndex, VehicleIndex)
			SET_ENTITY_COORDS(PedIndex, GET_ENTITY_COORDS(VehicleIndex))
			SET_ENTITY_HEADING(PedIndex, GET_ENTITY_HEADING(VehicleIndex))
			
			RETURN TRUE
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC
FUNC BOOL SetPedFromSynchedSceneIntoVehicle(PED_REQUEST_SCENE_ENUM ePedScene)


//	IF NOT IS_ENTITY_DEAD(scene_veh)
//		TEXT_LABEL_63 tPlayerSceneSyncAnimVehLoop
//		IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(ePedScene, tPlayerSceneSyncAnimVehLoop)
//			PLAY_SYNCHRONIZED_ENTITY_ANIM(scene_veh,
//					g_iPlayer_Timetable_Loop_SynchSceneID,
//					tPlayerSceneSyncAnimVehLoop, tPlayerSceneSyncAnimDict,
//					NORMAL_BLEND_IN)
//					
//		ENDIF
//	ENDIF
	
	TEXT_LABEL_63 tPlayerSceneSyncAnimVehLoop, tPlayerSceneSyncAnimVehExit
	IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(ePedScene, tPlayerSceneSyncAnimVehLoop)
	OR GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(ePedScene, tPlayerSceneSyncAnimVehExit)
		IF (sTimetableScene.eVehState <> PTVS_0_noVehicle)
			
			IF NOT IS_ENTITY_DEAD(scene_veh)
				
//				#IF IS_DEBUG_BUILD
//				VECTOR vPreVehCoord = GET_ENTITY_COORDS(scene_veh)
//				VECTOR vPreVehRot = GET_ENTITY_ROTATION(scene_veh)
//				#ENDIF
				
				SET_RADIO_FRONTEND_FADE_TIME(3.5)
				
				STOP_SYNCHRONIZED_ENTITY_ANIM(scene_veh, NORMAL_BLEND_OUT, TRUE)
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
					IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, TRUE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), scene_veh, VS_DRIVER)
						SET_PLAYER_CLOTH_LOCK_COUNTER(iFramesToLockCloth)
					ELIF (sTimetableScene.eVehState = PTVS_1_playerWithVehicle)
						
						IF IS_STRING_NULL_OR_EMPTY(tPlayerSceneSyncAnimVehExit)
							GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(ePedScene, tPlayerSceneSyncAnimVehExit)
						ENDIF
						
						IF ARE_STRINGS_EQUAL(tPlayerSceneSyncAnimVehExit, "SITTING_ON_CAR_BONNET_EXIT_CAR")
						OR ARE_STRINGS_EQUAL(tPlayerSceneSyncAnimVehExit, "SITTING_ON_CAR_PREMIERE_EXIT_CAR")
							ar_SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
						ELSE
							#IF IS_DEBUG_BUILD
							SAVE_STRING_TO_DEBUG_FILE("NOT parkedhills scene \"")
							SAVE_STRING_TO_DEBUG_FILE(tPlayerSceneSyncAnimVehExit)
							SAVE_STRING_TO_DEBUG_FILE("\"")
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
//				#IF IS_DEBUG_BUILD
//				VECTOR vPostVehCoord = GET_ENTITY_COORDS(scene_veh)
//				VECTOR vPostVehRot = GET_ENTITY_ROTATION(scene_veh)
//				
//				OPEN_DEBUG_FILE()
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("pre-scene_veh coord: ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vPreVehCoord)
//				SAVE_STRING_TO_DEBUG_FILE(", ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vPreVehRot)
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				SAVE_STRING_TO_DEBUG_FILE("post-scene_veh coord: ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vPostVehCoord)
//				SAVE_STRING_TO_DEBUG_FILE(", ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vPostVehRot)
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				VECTOR vDiffVehCoord = vPostVehCoord-vPreVehCoord
//				VECTOR vDiffVehRot = vPostVehRot-vPreVehRot
//				
//				SAVE_STRING_TO_DEBUG_FILE("diff-scene_veh coord: ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vDiffVehCoord)
//				SAVE_STRING_TO_DEBUG_FILE(", ")
//				SAVE_VECTOR_TO_DEBUG_FILE(vDiffVehRot)
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				SAVE_STRING_TO_DEBUG_FILE("	vCreateCoords += ")SAVE_VECTOR_TO_DEBUG_FILE(vDiffVehCoord)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("	fCreateHead += ")SAVE_FLOAT_TO_DEBUG_FILE(vDiffVehRot.z)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				SAVE_STRING_TO_DEBUG_FILE("	vCreateRot += <<")SAVE_FLOAT_TO_DEBUG_FILE(vDiffVehRot.x)SAVE_STRING_TO_DEBUG_FILE(", ")SAVE_FLOAT_TO_DEBUG_FILE(vDiffVehRot.y)SAVE_STRING_TO_DEBUG_FILE(", 0.0>>")SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				CLOSE_DEBUG_FILE()
//				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SetPedFromSynchedSceneIntoObject(OBJECT_INDEX &ObjectIndex,
		PED_REQUEST_SCENE_ENUM ePedScene, INT &iDrawSceneRot, BOOL bExtraProp)
	IF NOT DOES_ENTITY_EXIST(ObjectIndex)
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_63 tExitSyncObjAnim, tExitSyncExtraObjAnim
	INT iExitFlags
	
	IF IS_ENTITY_ATTACHED(ObjectIndex)
	OR GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(ePedScene, tExitSyncObjAnim, tExitSyncExtraObjAnim, iExitFlags)
	OR (ePedScene = PR_SCENE_F0_TANISHAFIGHT)		//1294022
		MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
		PED_BONETAG eBonetag = BONETAG_NULL
		FLOAT fPropDetachPhase = -1.0
		enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
		
		BOOL bAttachedToBuddy = FALSE
		
		IF NOT bExtraProp
			IF GET_OBJECTS_FOR_SCENE(ePedScene,
					eObjectModel, vecOffset, vecRotation, eBonetag,
					fPropDetachPhase, thisSceneObjectAction)
			ENDIF
		ELSE
			
			IF GET_EXTRA_OBJECTS_FOR_SCENE(ePedScene,
					eObjectModel, vecOffset, vecRotation, eBonetag,
					fPropDetachPhase, thisSceneObjectAction, bAttachedToBuddy)
			ENDIF
		ENDIF
		
		TEXT_LABEL_63 tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut
		ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
		//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
		
		PED_INDEX animatedPed = NULL
		IF NOT bAttachedToBuddy
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(ePedScene,
					tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneAnimProgress)
				animatedPed = PLAYER_PED_ID()
			ENDIF
		ELSE
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(ePedScene,
					tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)
				animatedPed = scene_buddy
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(animatedPed)
			IF (eObjectModel <> DUMMY_MODEL_FOR_SCRIPT)
				FLOAT fPlayer_anim_current_time
				IF fPropDetachPhase >= 0
				AND Get_Ped_Anim_Or_SynchedScene_Phase(animatedPed, tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut, fPlayer_anim_current_time)
					
					
					#IF IS_DEBUG_BUILD
					STRING pEventName = "ObjectRelease"
					FLOAT ReturnStartPhase, ReturnEndPhase
					IF FIND_ANIM_EVENT_PHASE(tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut,
							pEventName, ReturnStartPhase, ReturnEndPhase)
						
						FLOAT fPropDetachPhaseDiff
						fPropDetachPhaseDiff = ABSF(fPropDetachPhase - ReturnStartPhase)
						
						IF (fPropDetachPhaseDiff > 0.0002)
							IF NOT bObjectReleasePhaseReset
							AND NOT bExtraProp
								SAVE_STRING_TO_DEBUG_FILE("FIND_ANIM_EVENT_PHASE(\"")
								SAVE_STRING_TO_DEBUG_FILE(tPlayerSceneSceneAnimDict)
								SAVE_STRING_TO_DEBUG_FILE("\", \"")
								SAVE_STRING_TO_DEBUG_FILE(tPlayerSceneSceneAnimOut)
								SAVE_STRING_TO_DEBUG_FILE("\", \"")
								SAVE_STRING_TO_DEBUG_FILE(pEventName)
								SAVE_STRING_TO_DEBUG_FILE("\",[")
								SAVE_FLOAT_TO_DEBUG_FILE(ReturnStartPhase)
									
								IF (ReturnStartPhase <> ReturnEndPhase)
									SAVE_STRING_TO_DEBUG_FILE(", ")
									SAVE_FLOAT_TO_DEBUG_FILE(ReturnEndPhase)
								ENDIF
									
								SAVE_STRING_TO_DEBUG_FILE("] <> ")
								SAVE_FLOAT_TO_DEBUG_FILE(fPropDetachPhase)
								SAVE_STRING_TO_DEBUG_FILE(")	//")
								
								SAVE_FLOAT_TO_DEBUG_FILE(fPropDetachPhaseDiff)
								
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								TEXT_LABEL_63 sEventNameAssert
								sEventNameAssert  = GET_STRING_FROM_STRING(Get_String_From_Ped_Request_Scene_Enum(ePedScene),
										GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
										GET_LENGTH_OF_LITERAL_STRING(Get_String_From_Ped_Request_Scene_Enum(ePedScene)))
								sEventNameAssert += " \""
								sEventNameAssert += pEventName
								sEventNameAssert += "\" differs: "
								sEventNameAssert += GET_STRING_FROM_FLOAT(ReturnStartPhase)
								
								SCRIPT_ASSERT(sEventNameAssert)
								bObjectReleasePhaseReset = TRUE
							ENDIF
							
							fPropDetachPhase = ReturnStartPhase
						ENDIF
					ENDIF
					#ENDIF
					
					IF fPlayer_anim_current_time >= fPropDetachPhase
						SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex, thisSceneObjectAction)
						
						IF DOES_ENTITY_EXIST(ObjectIndex)
						AND (thisSceneObjectAction = PSOA_9_synchAndPtfxSwap)
							TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
							INT iFlags
							IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject, iFlags)
								
								TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
								ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
								//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
								
								IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
										tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
										playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
									IF NOT IS_ENTITY_PLAYING_ANIM(ObjectIndex,
												tPlayerSceneSyncAnimDict,
												tPlayerSceneSyncObject, ANIM_SYNCED_SCENE)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(ObjectIndex,
												g_iPlayer_Timetable_Exit_SynchSceneID,
												tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
												INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iFlags)
										FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(ObjectIndex)
										
										CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM")
									ELSE
										CPRINTLN(DEBUG_SWITCH, "is playing anim")
									ENDIF
								ELSE
									CPRINTLN(DEBUG_SWITCH, "no anim dict")
								ENDIF
							ELSE
								CDEBUG3LN(DEBUG_SWITCH, "no object anim")
							ENDIF
						ENDIF
						
					else
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("playing anim: ", fPropDetachPhase, iDrawSceneRot+5, HUD_COLOUR_blue)
						iDrawSceneRot++
						
						CPRINTLN(DEBUG_SWITCH, "<switch prop> object anim: ", fPlayer_anim_current_time, " < ", fPropDetachPhase)
						#ENDIF
					ENDIF
					
//					#IF IS_DEBUG_BUILD
//					STRING pEventName = "ObjectRelease"
//					IF HasAnimEventPassed(pEventName, fPlayer_anim_current_time,
//							tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut,
//							iDrawSceneRot)
//						SCRIPT_ASSERT(pEventName)
//					ENDIF
//					#ENDIF
					
					
				ELSE
					IF NOT IS_ENTITY_DEAD(animatedPed)
						RETURN FALSE
					ENDIF
					
					IF fPropDetachPhase >= 0
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("entity anim: ", iDrawSceneRot+5, HUD_COLOUR_blue)
						iDrawSceneRot++
						#ENDIF
						
						IF (eObjectModel = Prop_phone_ING)
						OR (eObjectModel = Prop_phone_ING_03)
						OR (eObjectModel = Prop_phone_ING_02)
							IF NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sDaughterFakeCellphoneData)
								REQUEST_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
							ELSE
								DRAW_FAKE_CELLPHONE_SCREEN(sDaughterFakeCellphoneData, TRUE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
							ENDIF
						ENDIF
					ELIF IS_ENTITY_PLAYING_ANIM(animatedPed, tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut)
						#IF IS_DEBUG_BUILD
						fPlayer_anim_current_time = GET_ENTITY_ANIM_CURRENT_TIME(animatedPed, tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut)
						DrawLiteralSceneStringFloat("IS_ENTITY_PLAYING_ANIM: ", fPlayer_anim_current_time, iDrawSceneRot+5, HUD_COLOUR_blue)
						iDrawSceneRot++
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("other???: ", iDrawSceneRot+5, HUD_COLOUR_blue)
						iDrawSceneRot++
						#ENDIF
					ENDIF
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("no anim or scene for obj???", iDrawSceneRot+5, HUD_COLOUR_blue)
				iDrawSceneRot++
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF

	UNUSED_PARAMETER(iDrawSceneRot)
	RETURN FALSE
ENDFUNC

FUNC BOOL Get_Base_Speed_For_Drive_To_Coord(VECTOR &vSceneVehDriveOffset, FLOAT &fSceneVehDriveSpeed)
	
	IF NOT IS_VEHICLE_DRIVEABLE(scene_veh)
		RETURN FALSE
	ENDIF
	
	VECTOR scene_veh_coord = GET_ENTITY_COORDS(scene_veh)
	FLOAT scene_veh_ground_z
	
	IF (fSceneVehDriveSpeed <= 0.0)
	AND ARE_VECTORS_EQUAL(vSceneVehDriveOffset, <<0,0,0>>)
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Get_Base_Speed_For_Drive_To_Coord ", vSceneVehDriveOffset, ", ", fSceneVehDriveSpeed)
		
		IF NOT GET_GROUND_Z_FOR_3D_COORD(scene_veh_coord, scene_veh_ground_z)
			CPRINTLN(DEBUG_SWITCH, "	no ground for coord ", scene_veh_coord, ", super speed!")
		
			vSceneVehDriveOffset = <<0,5,0>>
			fSceneVehDriveSpeed = 25.0
			
			CPRINTLN(DEBUG_SWITCH, "	", "default base ", vSceneVehDriveOffset, ", ", fSceneVehDriveSpeed)
			
			RETURN TRUE
		ELSE
			
			FLOAT scene_veh_ground_z_diff = (scene_veh_coord.z - scene_veh_ground_z)-1.0
			
			IF scene_veh_ground_z_diff > 1.0
					
				MODEL_NAMES scene_veh_model = GET_ENTITY_MODEL(scene_veh)
				
				IF IS_THIS_MODEL_A_PLANE(scene_veh_model)
					CPRINTLN(DEBUG_SWITCH, "	ground for coord ", scene_veh_coord, " found at ", scene_veh_ground_z, ", calculate base speed [", scene_veh_ground_z_diff, "]", " - ", GET_MODEL_NAME_FOR_DEBUG(scene_veh_model), " -", " is a plane")
					
				ELIF IS_THIS_MODEL_A_HELI(scene_veh_model)
				OR (scene_veh_model = BLIMP)
					CPRINTLN(DEBUG_SWITCH, "	ground for coord ", scene_veh_coord, " found at ", scene_veh_ground_z, ", calculate base speed [", scene_veh_ground_z_diff, "]", " - ", GET_MODEL_NAME_FOR_DEBUG(scene_veh_model), " -", " is a heli/blimp")
					
					IF scene_veh_ground_z_diff < 5.0
					OR (sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT OR sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT OR sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT)
						vSceneVehDriveOffset = <<0,0,0>>
						fSceneVehDriveSpeed = 0.0
						
						PRINTLN("heli hovering (was calc'd)")
						
						RETURN FALSE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "	ground for coord ", scene_veh_coord, " found at ", scene_veh_ground_z, ", calculate base speed [", scene_veh_ground_z_diff, "]", " - ", GET_MODEL_NAME_FOR_DEBUG(scene_veh_model), " -", " not a plane or a heli")
					
					RETURN FALSE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SWITCH, "	ground for coord ", scene_veh_coord, " found at ", scene_veh_ground_z, ", calculate base speed [", scene_veh_ground_z_diff, "]")
			ENDIF
			
			CONST_FLOAT	iCONST_MAX_scene_veh_speed_height	50.0
			CONST_FLOAT	iCONST_MAX_scene_veh_speed_mult		5.0
			FLOAT		 scene_veh_speed_mult				= scene_veh_ground_z_diff
			scene_veh_speed_mult /= iCONST_MAX_scene_veh_speed_height
			
			IF (scene_veh_speed_mult < 0)
				scene_veh_speed_mult = 0
			ENDIF
			IF (scene_veh_speed_mult > 1)
				scene_veh_speed_mult = 1
			ENDIF
			scene_veh_speed_mult *= iCONST_MAX_scene_veh_speed_mult
			
			vSceneVehDriveOffset = <<0,scene_veh_speed_mult,0>>
			fSceneVehDriveSpeed = scene_veh_speed_mult * 5.0
			
			CPRINTLN(DEBUG_SWITCH, "	calculated base ", vSceneVehDriveOffset, ", ", fSceneVehDriveSpeed)
			
			RETURN TRUE
		ENDIF
		
	ELSE
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Get_Base_Speed_For_Drive_To_Coord ", vSceneVehDriveOffset, ", ", fSceneVehDriveSpeed, " OK")
		
		
		IF NOT GET_GROUND_Z_FOR_3D_COORD(scene_veh_coord, scene_veh_ground_z)
			//
		ELSE
			
			FLOAT scene_veh_ground_z_diff = (scene_veh_coord.z - scene_veh_ground_z)-1.0
			
			CPRINTLN(DEBUG_SWITCH, "	ground for coord ", scene_veh_coord, " found at ", scene_veh_ground_z, ", calculate base speed [", scene_veh_ground_z_diff, "]")
			
			IF scene_veh_ground_z_diff > 1.0
				MODEL_NAMES scene_veh_model = GET_ENTITY_MODEL(scene_veh)
				
				IF IS_THIS_MODEL_A_HELI(scene_veh_model)
					CPRINTLN(DEBUG_SWITCH, " is a heli")
					
					IF scene_veh_ground_z_diff < 5.0
					OR (sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT OR sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT OR sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT)
						vSceneVehDriveOffset = <<0,0,0>>
						fSceneVehDriveSpeed = 0.0
						
						PRINTLN("heli hovering (was OK)")
						
						RETURN FALSE
					ENDIF
				ELSE
					//
				ENDIF
			ENDIF
			
			
		ENDIF
		
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC ar_TASK_PUT_PED_DIRECTLY_INTO_COVER(PED_INDEX PedIndex, INT Time)
	
	IF IS_PED_IN_COVER(PedIndex)
		EXIT
	ENDIF
	
	VECTOR vCoverPos	= sTimetableScene.sScene.vCreateCoords
	FLOAT fCoverDir		= sTimetableScene.sScene.fCreateHead
	
	COVERPOINT_USAGE	coverpointUsage 	= COVUSE_WALLTORIGHT
	COVERPOINT_HEIGHT	coverpointHeight 	= COVHEIGHT_HIGH
	COVERPOINT_ARC		coverpointArc 		= COVARC_180
	
	BOOL CanPeekAndAim = FALSE
	FLOAT BlendInDuration = NORMAL_BLEND_DURATION	//0.0
	bool ForceInitialFacingDirection = FALSE
	bool bForceFaceLeft = FALSE
	
	//fix for B*1565359, treat post Franklin 1 switches as special case and specify cover points for each switch
	IF sTimetableScene.sScene.eScene = PR_SCENE_FTa_FRANKLIN1a
		
		vCoverPos			= <<798.5135, -2975.3286, 5.0208>>
		fCoverDir			= 355.0
		coverpointUsage		= COVUSE_WALLTORIGHT
		coverpointHeight 	= COVHEIGHT_TOOHIGH
		coverpointArc 		= COVARC_90
		BlendInDuration		= 0
		
	ELIF sTimetableScene.sScene.eScene = PR_SCENE_FTa_FRANKLIN1c
	
		vCoverPos			= <<708.9384, -2916.5771, 5.0480>>
		fCoverDir			= 268.1254
		coverpointUsage		= COVUSE_WALLTORIGHT
		coverpointHeight 	= COVHEIGHT_TOOHIGH
		coverpointArc 		= COVARC_90
		BlendInDuration		= 0
	
	ELIF sTimetableScene.sScene.eScene = PR_SCENE_FTa_FRANKLIN1d
	
		vCoverPos			= <<643.5130, -2917.2490, 5.1339>>
		fCoverDir			= 246.0
		coverpointUsage		= COVUSE_WALLTORIGHT
		coverpointHeight 	= COVHEIGHT_TOOHIGH
		coverpointArc 		= COVARC_90
		BlendInDuration		= 0
	
	ELIF sTimetableScene.sScene.eScene = PR_SCENE_FTa_FRANKLIN1e
	
		vCoverPos			= <<595.2472, -2819.2312, 5.0558>>
		fCoverDir			= 140.7381
		coverpointUsage		= COVUSE_WALLTOLEFT
		coverpointHeight 	= COVHEIGHT_TOOHIGH
		coverpointArc 		= COVARC_90
		BlendInDuration		= 0
	
	ENDIF
	
	IF CoverIndex = NULL		//NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(vCoverPos)
		CoverIndex = ADD_COVER_POINT(vCoverPos, fCoverDir,
				coverpointUsage, coverpointHeight, coverpointArc, TRUE)
	ENDIF
	
	TASK_PUT_PED_DIRECTLY_INTO_COVER(PedIndex, vCoverPos, Time, CanPeekAndAim, BlendInDuration, ForceInitialFacingDirection, bForceFaceLeft, CoverIndex)
	
	
//	TASK_SEEK_COVER_FROM_POS(PedIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<0,-5,0>>), -1)
ENDPROC



FUNC BOOL IsShortRangeDefaultSwitch(PED_SCENE_STRUCT sScene)
	
	IF sScene.eScene = PR_SCENE_M_DEFAULT
	OR sScene.eScene = PR_SCENE_F_DEFAULT
	OR sScene.eScene = PR_SCENE_T_DEFAULT
		
		SWITCH GET_PLAYER_SWITCH_TYPE()
			CASE SWITCH_TYPE_AUTO
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> IsShortRangeDefaultSwitch.", "SWITCH_TYPE_AUTO")
		
			BREAK
			CASE SWITCH_TYPE_LONG
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> IsShortRangeDefaultSwitch.", "SWITCH_TYPE_LONG")
			BREAK
			CASE SWITCH_TYPE_MEDIUM
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> IsShortRangeDefaultSwitch.", "SWITCH_TYPE_MEDIUM")
			BREAK
			
			CASE SWITCH_TYPE_SHORT
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> IsShortRangeDefaultSwitch.", "SWITCH_TYPE_SHORT")
				
				RETURN TRUE
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	CONTROL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL Control_Player_Loop_Tasks()
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
	
	SWITCH sTimetableScene.eLoopTask
		CASE SCRIPT_TASK_PLAY_ANIM
			TEXT_LABEL_63 tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
			ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
			//enumPlayerSceneAnimProgress ePlayerSceneLoopProgress
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneLoopProgress)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fCreateHead)
				
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_PLAY_ANIM \"", tPlayerSceneAnimDict, "\", \"", tPlayerSceneAnimLoop, "\" flag: ", ENUM_TO_INT(playerSceneLoopFlag))
				
				IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					IF IS_BITMASK_ENUM_AS_ENUM_SET(playerSceneLoopFlag, AF_OVERRIDE_PHYSICS)
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 sOverride_physics
						sOverride_physics = "overriding physics while player is attached: "
						sOverride_physics += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
						
						CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sOverride_physics)
						SCRIPT_ASSERT(sOverride_physics)
						#ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
				
				TASK_PLAY_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop,
						NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
						playerSceneLoopFlag)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
			TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
			ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
			//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
			
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
					playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_SYNCHRONIZED_SCENE ", tPlayerSceneSyncAnimDict, "\", \"", tPlayerSceneSyncAnimLoop, "\"")
				
				SetPedFromVehicleIntoSynchedScene(PLAYER_PED_ID(), scene_veh)
				
				VECTOR vCreateRot
				GET_PLAYER_PED_ROTATION_FOR_SCENE(sTimetableScene.sScene.eScene, fCreateHead, vCreateRot)
				
				Safe_Create_Synchronized_Scene(g_iPlayer_Timetable_Loop_SynchSceneID, "Player_Loop", vCreateCoords, vCreateRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Loop_SynchSceneID, TRUE)
				//BLEND_OUT_SYNCED_MOVEMENT_ANIMS(PLAYER_PED_ID())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_iPlayer_Timetable_Loop_SynchSceneID,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop,
						NORMAL_BLEND_IN, INSTANT_BLEND_OUT, 
						SYNCED_SCENE_DONT_INTERRUPT)
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				
				IF NOT IS_ENTITY_DEAD(scene_veh)
					TEXT_LABEL_63 tPlayerSceneSyncAnimVehLoop
					IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncAnimVehLoop)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(scene_veh,
								g_iPlayer_Timetable_Loop_SynchSceneID,
								tPlayerSceneSyncAnimVehLoop, tPlayerSceneSyncAnimDict,
								NORMAL_BLEND_IN)
								
					ENDIF
				ENDIF
				
				TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
				IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject)
				AND DOES_ENTITY_EXIST(player_prop)
					
					IF IS_ENTITY_ATTACHED(player_prop)
						DETACH_ENTITY(player_prop)
					ENDIF
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop,
							g_iPlayer_Timetable_Loop_SynchSceneID,
							tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
					CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop, \"", tPlayerSceneSyncObject, "\", \"", tPlayerSceneSyncAnimDict, "\")")
					
					IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerSceneSyncExtraObject)
					AND DOES_ENTITY_EXIST(player_extra_prop)
						IF IS_ENTITY_ATTACHED(player_extra_prop)
							DETACH_ENTITY(player_extra_prop)
						ENDIF
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_extra_prop,
								g_iPlayer_Timetable_Loop_SynchSceneID,
								tPlayerSceneSyncExtraObject, tPlayerSceneSyncAnimDict,
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						
						CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM(player_extra_prop, \"", tPlayerSceneSyncExtraObject, "\", \"", tPlayerSceneSyncAnimDict, "\")")
					ENDIF
				ENDIF

				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			Get_Base_Speed_For_Drive_To_Coord(vSceneVeh_driveOffset, fSceneVeh_driveSpeed)
			
			FLOAT fSceneVeh_driveSpeedLoop
			fSceneVeh_driveSpeedLoop = fSceneVeh_driveSpeed
			
			IF (sTimetableScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
				fSceneVeh_driveSpeedLoop *= 0.5
			ENDIF
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD ", vSceneVeh_driveOffset, ", ", fSceneVeh_driveSpeedLoop)
			
			IF DOES_ENTITY_EXIST(scene_veh)
				IF IS_VEHICLE_DRIVEABLE(scene_veh)
					ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Player_Loop.VEHICLE_DRIVE_TO_COORD")
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Player_Loop.VEHICLE_DRIVE_TO_COORD")
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
					
					SEQUENCE_INDEX siseq
					OPEN_SEQUENCE_TASK(siseq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, scene_veh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset*1.5), fSceneVeh_driveSpeedLoop, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS, 4.0, 30)
						TASK_VEHICLE_DRIVE_WANDER(NULL, scene_veh, fSceneVeh_driveSpeedLoop, DRIVINGMODE_AVOIDCARS)
					CLOSE_SEQUENCE_TASK(siseq)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siseq)
					CLEAR_SEQUENCE_TASK(siseq)
					
					ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeedLoop)
					
					IF (fSceneVeh_driveSpeed > 5.0)
						IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
							SET_HELI_BLADES_FULL_SPEED(scene_veh)
							
							IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
							OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
								CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
							ENDIF
						ENDIF
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
							SET_HELI_BLADES_FULL_SPEED(scene_veh)
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_SWITCH, "	tasked...")
					
					RETURN TRUE
				ELIF IS_ENTITY_DEAD(scene_veh)
					
					CPRINTLN(DEBUG_SWITCH, "	scene_veh is dead???")
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_PED_ON_MOUNT(PLAYER_PED_ID())
				PED_INDEX playersMount_ped
				playersMount_ped = GET_MOUNT(PLAYER_PED_ID())
				
				IF NOT IS_PED_INJURED(playersMount_ped)
					
					TASK_GO_STRAIGHT_TO_COORD(playersMount_ped,
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(playersMount_ped, vSceneVeh_driveOffset),
							fSceneVeh_driveSpeedLoop)	//PEDMOVE_WALK)
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDrive
			sDrive = "something wrong Control_Player_Loop_Tasks: "
			sDrive += "SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD  //"
			sDrive += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sDrive)
			SCRIPT_ASSERT(sDrive)
			#ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_31 tlRecordingName
			tlRecordingName = tRecordingName
			IF (iRecordingNum < 100)	tlRecordingName += "0" ENDIF
			IF (iRecordingNum < 10)		tlRecordingName += "0" ENDIF
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop VEHICLE_RECORDING", "(\"", tlRecordingName, "\") //", fRecordingStart)
			#ENDIF
			#IF NOT IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop VEHICLE_RECORDING")
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum > 0)
				IF DOES_ENTITY_EXIST(scene_veh)
				AND IS_VEHICLE_DRIVEABLE(scene_veh)
					
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
						ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Player_Loop.VEHICLE_RECORDING")
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, TRUE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), scene_veh, VS_DRIVER)
						SET_PLAYER_CLOTH_LOCK_COUNTER(iFramesToLockCloth)
					ENDIF
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
					ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Player_Loop.VEHICLE_RECORDING")

					START_PLAYBACK_RECORDED_VEHICLE(scene_veh, iRecordingNum, tRecordingName)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(scene_veh, fRecordingStart)
					
					#IF IS_DEBUG_BUILD
					ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(scene_veh, RDM_WHOLELINE)
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sRecording
			sRecording = "something wrong Control_Player_Loop_Tasks: "
			sRecording += "SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING  //"
			sRecording += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sRecording)
			SCRIPT_ASSERT(sRecording)
			#ENDIF
			
			RETURN FALSE
		BREAK
		CASE SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,5,0>>),
					PEDMOVE_WALK)

			IF (IS_ENTITY_IN_WATER(PLAYER_PED_ID()) OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_LAKE))
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_DIVING_SWIM)
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD")
			
			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,5,0>>),
					PEDMOVE_WALK)
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_WANDER_STANDARD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_WANDER_STANDARD")
			
			TASK_WANDER_STANDARD(PLAYER_PED_ID())
			
			
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ENTER_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_ENTER_VEHICLE")
			
			IF DOES_ENTITY_EXIST(scene_veh)
			AND IS_VEHICLE_DRIVEABLE(scene_veh)
				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), scene_veh,
						DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_STAND_STILL
			IF DOES_ENTITY_EXIST(scene_veh)
			AND IS_VEHICLE_DRIVEABLE(scene_veh)
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
					IF (GET_PED_IN_VEHICLE_SEAT(scene_veh, VS_DRIVER) = PLAYER_PED_ID())
						CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_VEHICLE_MISSION_COORS_TARGET (default switch to a vehicle)")
						
						VECTOR vGotoOffset
						FLOAT fGotoSpeed
						vGotoOffset	= <<0,0.1,0>>
						fGotoSpeed	= 0.1
						
						FLOAT fEntityHeight
						fEntityHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(scene_veh)
						vGotoOffset	*= fEntityHeight
						IF (vGotoOffset.y > 10)
							vGotoOffset.y = 10
						ENDIF
						fGotoSpeed	*= fEntityHeight
						IF (fGotoSpeed > 10)
							fGotoSpeed = 10
						ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "fEntityHeight: ", fEntityHeight, ", offset: ", vGotoOffset, ", speed: ", fGotoSpeed, "	//", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scene_veh)))
					
						#ENDIF
						
						TASK_VEHICLE_MISSION_COORS_TARGET(PLAYER_PED_ID(), scene_veh,
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vGotoOffset),
								MISSION_GOTO, fGotoSpeed, DRIVINGMODE_PLOUGHTHROUGH,
								-1, -1)
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
						//ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Player_Loop.STAND_STILL")
						ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fGotoSpeed)
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_STAND_STILL")
			
			
			
			IF (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING1)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING2)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING3)
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, TRUE)
				ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, TRUE)
				ENDIF
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_AIM_GUN_AT_ENTITY
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_AIM_GUN_AT_ENTITY")
			
			IF NOT IS_PED_INJURED(scene_buddy)
				
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 30)
				ENDIF
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
//				TASK_AIM_GUN_AT_ENTITY(PLAYER_PED_ID(), scene_buddy, -1)
//				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), scene_buddy, 1000)

				SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER")
			
			ar_TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), -1)
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_COWER
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_COWER")
			
			TASK_COWER(PLAYER_PED_ID())
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_USE_MOBILE_PHONE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Loop SCRIPT_TASK_USE_MOBILE_PHONE")
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ANY						FALLTHRU
		DEFAULT

			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sInvalid
			sInvalid = "invalid eTask for Control_Player_Loop_Tasks: "
			sInvalid += ENUM_TO_INT(sTimetableScene.eLoopTask)
			sInvalid += "  //"
			sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
			SCRIPT_ASSERT(sInvalid)
			#ENDIF
			
			IF (sTimetableScene.eLoopTask <> SCRIPT_TASK_ANY)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sUnknown
	sUnknown = "unknown eTask for Control_Player_Loop_Tasks: "
	sUnknown += ENUM_TO_INT(sTimetableScene.eLoopTask)
	sUnknown += "  //"
	sUnknown += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sUnknown)
	SCRIPT_ASSERT(sUnknown)
	#ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL Control_Buddy_Loop_Tasks()
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
	
	SWITCH sTimetableScene.eBuddyLoopTask
		CASE SCRIPT_TASK_PLAY_ANIM
			TEXT_LABEL_63 tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut
			ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
			
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
					buddyAnimLoopFlag, buddyAnimOutFlag)
				SET_PED_COORDS_KEEP_VEHICLE(scene_buddy, vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset)
				SET_ENTITY_HEADING(scene_buddy, fCreateHead+sTimetableScene.fSceneBuddyHeadOffset)
				
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_PLAY_ANIM \"", tSceneBuddyAnimDict, "\", \"", tSceneBuddyAnimLoop, "\" flag: ", ENUM_TO_INT(buddyAnimLoopFlag))
				
				TASK_PLAY_ANIM_ADVANCED(scene_buddy, tSceneBuddyAnimDict, tSceneBuddyAnimLoop,
						vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset, <<0,0,fCreateHead+sTimetableScene.fSceneBuddyHeadOffset>>,
						INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
						buddyAnimLoopFlag)
				SET_PED_KEEP_TASK(scene_buddy, TRUE)
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sAnim
			sAnim = "something wrong Control_Buddy_Loop_Tasks: "
			sAnim += "SCRIPT_TASK_PLAY_ANIM  //"
			sAnim += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sAnim)
			SCRIPT_ASSERT(sAnim)
			#ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
			TEXT_LABEL_63 tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut
			ANIMATION_FLAGS buddySyncLoopFlag, buddySyncOutFlag
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut,
					buddySyncLoopFlag, buddySyncOutFlag)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_SYNCHRONIZED_SCENE ", tBuddySceneSyncAnimDict, "\", \"", tBuddySceneSyncAnimLoop, "\"")
				
				SetPedFromVehicleIntoSynchedScene(scene_buddy, scene_veh)
				
				VECTOR vCreateRot
				GET_PLAYER_PED_ROTATION_FOR_SCENE(sTimetableScene.sScene.eScene, fCreateHead, vCreateRot)
				
				Safe_Create_Synchronized_Scene(g_iPlayer_Timetable_Loop_SynchSceneID, "Buddy_Loop", vCreateCoords, vCreateRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Loop_SynchSceneID, TRUE)
				//BLEND_OUT_SYNCED_MOVEMENT_ANIMS(scene_buddy)
				TASK_SYNCHRONIZED_SCENE(scene_buddy, g_iPlayer_Timetable_Loop_SynchSceneID,
						tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop,
						NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(scene_buddy, TRUE)
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD ", vSceneVeh_driveOffset, ", ", fSceneVeh_driveSpeed)
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				
				TASK_VEHICLE_DRIVE_TO_COORD(scene_buddy, scene_veh,
						GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset), fSceneVeh_driveSpeed,
						DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS,
						4.0, 30)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
				ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Buddy_Loop")
				ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed)
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDrive
			sDrive = "something wrong Control_Buddy_Loop_Tasks: "
			sDrive += "SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD  //"
			sDrive += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sDrive)
			SCRIPT_ASSERT(sDrive)
			#ENDIF
			
			RETURN FALSE
		BREAK
		CASE SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<Buddy_timetable_scene> Buddy_Loop SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
			
			TASK_GO_STRAIGHT_TO_COORD(scene_buddy,
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_buddy, <<0,5,0>>),
					PEDMOVE_WALK)
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_ENTER_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_ENTER_VEHICLE")
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				TASK_ENTER_VEHICLE(scene_buddy, scene_veh,
						DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_STAND_STILL
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_STAND_STILL")
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED")
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(scene_buddy, 25.0)
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_START_SCENARIO_AT_POSITION
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_START_SCENARIO_AT_POSITION")
			
			TEXT_LABEL_63 tBuddySceneScenarioDict, tBuddySceneScenarioLoop, tBuddySceneScenarioOut
			ANIMATION_FLAGS buddyScenarioLoopFlag, buddyScenarioOutFlag
			
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tBuddySceneScenarioDict, tBuddySceneScenarioLoop, tBuddySceneScenarioOut,
					buddyScenarioLoopFlag, buddyScenarioOutFlag)
				
				INT iTimeToLeave
				TASK_START_SCENARIO_AT_POSITION(scene_buddy, tBuddySceneScenarioLoop,
						vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset,
						fCreateHead+sTimetableScene.fSceneBuddyHeadOffset,
						iTimeToLeave, TRUE)
			
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_COWER
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Loop SCRIPT_TASK_COWER")
			
			TASK_COWER(scene_buddy)
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ANY						FALLTHRU
		DEFAULT

			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sInvalid
			sInvalid = "invalid eTask for Control_Buddy_Loop_Tasks: "
			sInvalid += ENUM_TO_INT(sTimetableScene.eBuddyLoopTask)
			sInvalid += "  //"
			sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
			SCRIPT_ASSERT(sInvalid)
			#ENDIF
			
			IF (sTimetableScene.eBuddyLoopTask <> SCRIPT_TASK_ANY)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sUnknown
	sUnknown = "unknown eTask for Control_Buddy_Loop_Tasks: "
	sUnknown += ENUM_TO_INT(sTimetableScene.eBuddyLoopTask)
	sUnknown += "  //"
	sUnknown += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sUnknown)
	SCRIPT_ASSERT(sUnknown)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Control_Player_Out_Tasks()
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
	
	ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Control_Player_Out_Tasks")
	
	SWITCH sTimetableScene.eOutTask
		CASE SCRIPT_TASK_PLAY_ANIM
			TEXT_LABEL_63 tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
			ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
//			enumPlayerSceneAnimProgress ePlayerSceneLoopProgress
			
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneLoopProgress)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_PLAY_ANIM \"", tPlayerSceneAnimDict, "\", \"", tPlayerSceneAnimOut, "\" flag:", ENUM_TO_INT(playerSceneOutFlag))
				#ENDIF
				
				IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					IF IS_BITMASK_ENUM_AS_ENUM_SET(playerSceneOutFlag, AF_OVERRIDE_PHYSICS)
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 sOverride_physics
						sOverride_physics = "overriding physics while player is attached: "
						sOverride_physics += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
						
						CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sOverride_physics)
						SCRIPT_ASSERT(sOverride_physics)
						#ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
				
				FLOAT fBlendOutDelta
				fBlendOutDelta = WALK_BLEND_OUT
				IF CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
					fBlendOutDelta = FAST_BLEND_OUT
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerSceneOutFlag, AF_HOLD_LAST_FRAME)
						SET_BITMASK_ENUM_AS_ENUM(playerSceneOutFlag, AF_HOLD_LAST_FRAME)
					ENDIF
				ENDIF
				
				TASK_PLAY_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut,
						NORMAL_BLEND_IN, fBlendOutDelta, -1,
						playerSceneOutFlag)
				
				FLOAT paramStrength, amplitudeScalar
				BOOL bCountsForPubClubVisit
				IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bCountsForPubClubVisit)
					Make_Ped_Drunk(PLAYER_PED_ID(), ROUND(iCONST_DRUNK_secDuration*1.5))
					Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
				ENDIF
				
				
				TEXT_LABEL_23 tPlayerSceneProceduralCam
				IF SETUP_PROCEDURAL_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneProceduralCam)
					
					IF DOES_CAM_EXIST(LoopCamIndex)
						SET_CAM_ACTIVE(LoopCamIndex, FALSE)
						DESTROY_CAM(LoopCamIndex)
					ENDIF
					
//					VECTOR vecCamEndPos, vecCamEndRot
//					FLOAT vecCamEndFov
//					IF SETUP_PLAYER_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene, vecCamEndPos, vecCamEndRot, vecCamEndFov)
//						ExitCamIndex = CREATE_CAM("DEFAULT_SPLINE_CAMERA",TRUE)
//						
//						ADD_CAM_SPLINE_NODE(ExitCamIndex,
//								GET_FINAL_RENDERED_CAM_COORD(),
//								GET_FINAL_RENDERED_CAM_ROT(),
//								2500)
//						ADD_CAM_SPLINE_NODE(ExitCamIndex, vecCamEndPos+<<0,0,5>>, vecCamEndRot, 1500)
//						ADD_CAM_SPLINE_NODE(ExitCamIndex, vecCamEndPos, vecCamEndRot, 1500)
//						SET_CAM_FOV(ExitCamIndex, vecCamEndFov)
//						SET_CAM_ACTIVE(ExitCamIndex,TRUE)
//						
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					ENDIF
					
				/*	#IF IS_DEBUG_BUILD
					VECTOR vecCamEndPos, vecCamEndRot
					FLOAT vecCamEndFov
					IF SETUP_PLAYER_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene,
							vecCamEndPos, vecCamEndRot, vecCamEndFov)
					OR (ARE_VECTORS_EQUAL(vecCamEndPos, <<0,0,0>>) AND ARE_VECTORS_EQUAL(vecCamEndRot, <<0,0,0>>))
					
						VECTOR vecMocapFramePos, vecMocapFrameRot
						FLOAT vecMocapFrameFov
						
						IF DEBUG_GetPlayerSwitchEstablishingShot(sTimetableScene.sScene.tCreateRoom, tPlayerSceneEstabShotSuffix,
								vecMocapFramePos, vecMocapFrameRot, vecMocapFrameFov)
							//match end coord with establishing shot
							
						ELSE
							
							//match end coord with first frame of mocap
							vecMocapFramePos = GET_CAM_COORD(ExitCamIndex)
							vecMocapFrameRot = GET_CAM_ROT(ExitCamIndex)
							vecMocapFrameFov = GET_CAM_FOV(ExitCamIndex)
						ENDIF
						
						CONST_FLOAT fPosTolerance	0.5
						CONST_FLOAT fRotTolerance	0.5
						CONST_FLOAT fFovTolerance	0.5
						IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
						OR NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
						OR NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
							TEXT_LABEL_63 tCamDiff
							tCamDiff  = "vecCamEnd <> vecMocapFrame: "
							tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
//							
							CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", tCamDiff)
							
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndPos[", vecCamEndPos, "] <> vecMocapFramePos[", vecMocapFramePos, "]: ", VDIST(vecCamEndPos, vecMocapFramePos))
							ENDIF
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndRot[", vecCamEndRot, "] <> vecMocapFrameRot[", vecMocapFrameRot, "]: ", VDIST(vecCamEndRot, vecMocapFrameRot))
							ENDIF
							IF NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndFov[", vecCamEndFov, "] <> vecMocapFrameFov[", vecMocapFrameFov, "]: ", ABSF(vecCamEndFov - vecMocapFrameFov))
							ENDIF
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("CASE ")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
							SAVE_STRING_TO_DEBUG_FILE("			vecCamEndPos = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFramePos)
							SAVE_STRING_TO_DEBUG_FILE("		vecCamEndRot = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFrameRot)
							
							IF (vecMocapFrameFov <> 40.0)
								SAVE_STRING_TO_DEBUG_FILE("		vecCamEndFov = ")
								SAVE_FLOAT_TO_DEBUG_FILE(vecMocapFrameFov)
								SAVE_STRING_TO_DEBUG_FILE(" RETURN TRUE BREAK")
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("		RETURN TRUE BREAK")
							ENDIF
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SCRIPT_ASSERT(tCamDiff)
						ENDIF
					ENDIF
					
					FLOAT gpCamPitch, gameplayCamSmoothRate, gpCamHead
					
					IF SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sTimetableScene.sScene.eScene,
							gpCamPitch, gameplayCamSmoothRate, gpCamHead)
						TEXT_LABEL_63 tCamDiff
						tCamDiff  = "remove RELATIVE_GAMEPLAY_CAM_PITCH for "
						tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
						
						SCRIPT_ASSERT(tCamDiff)
					ENDIF
					#ENDIF	*/
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sMissingAnim
			sMissingAnim = "no anim in Control_Player_Out_Tasks: "
			sMissingAnim += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sMissingAnim)
			SCRIPT_ASSERT(sMissingAnim)
			#ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
			TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
			ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
			//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
			
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
					playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_SYNCHRONIZED_SCENE ", tPlayerSceneSyncAnimDict, "\", \"", tPlayerSceneSyncAnimOut, "\"")
				
//				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
//					SafeDetachSynchronizedScene(g_iPlayer_Timetable_Loop_SynchSceneID)
//					g_iPlayer_Timetable_Loop_SynchSceneID = 0
//				ENDIF
				
				SetPedFromVehicleIntoSynchedScene(PLAYER_PED_ID(), scene_veh)
				
				VECTOR vCreateRot
				GET_PLAYER_PED_ROTATION_FOR_SCENE(sTimetableScene.sScene.eScene, fCreateHead, vCreateRot)
				
				Safe_Create_Synchronized_Scene(g_iPlayer_Timetable_Exit_SynchSceneID, "Player_Out", vCreateCoords, vCreateRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
				//BLEND_OUT_SYNCED_MOVEMENT_ANIMS(PLAYER_PED_ID())
				
				FLOAT fBlendin, fBlendOut
				SYNCED_SCENE_PLAYBACK_FLAGS scpf_Flags
				RAGDOLL_BLOCKING_FLAGS ragdollFlags
				IK_CONTROL_FLAGS ikFlags
				P_GET_PLAYER_SYNCHRONIZED_SCENE_DATA(sTimetableScene.sScene.eScene, scpf_Flags, fBlendin, fBlendOut, ragdollFlags, ikFlags)
				
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_iPlayer_Timetable_Exit_SynchSceneID,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimOut,
						fBlendin, fBlendOut, scpf_Flags, ragdollFlags, fBlendin, ikFlags)
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				
				FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
				
				

				TEXT_LABEL_63 tFacialClip
				IF GET_TASK_FACIAL_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene, tFacialClip)
				
					CPRINTLN(DEBUG_SWITCH, "	TASK_PLAY_FACIAL_ANIM(PLAYER_PED_ID(), \"", tPlayerSceneSyncAnimDict, "\", \"", tFacialClip, "\")")
					
					TASK_PLAY_ANIM(PLAYER_PED_ID(), tPlayerSceneSyncAnimDict, tFacialClip,
							NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
					bHave_Played_Switch_Scene_Speech_Facial = TRUE
				ENDIF
				
				TEXT_LABEL_63 tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix
				IF GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix)
					
					IF DOES_CAM_EXIST(LoopCamIndex)
						SET_CAM_ACTIVE(LoopCamIndex, FALSE)
						DESTROY_CAM(LoopCamIndex)
					ENDIF
					
					ExitCamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					PLAY_SYNCHRONIZED_CAM_ANIM(ExitCamIndex,
							g_iPlayer_Timetable_Exit_SynchSceneID,
							tPlayerSceneSyncExitCam, tPlayerSceneSyncAnimDict)
					
					#IF IS_DEBUG_BUILD
					VECTOR vecCamEndPos, vecCamEndRot
					FLOAT vecCamEndFov
					IF SETUP_PLAYER_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene,
							vecCamEndPos, vecCamEndRot, vecCamEndFov)
					OR (ARE_VECTORS_EQUAL(vecCamEndPos, <<0,0,0>>) AND ARE_VECTORS_EQUAL(vecCamEndRot, <<0,0,0>>))
					
						VECTOR vecMocapFramePos, vecMocapFrameRot
						FLOAT vecMocapFrameFov
						
						IF DEBUG_GetPlayerSwitchEstablishingShot(sTimetableScene.sScene.tCreateRoom, tPlayerSceneEstabShotSuffix,
								vecMocapFramePos, vecMocapFrameRot, vecMocapFrameFov)
							//match end coord with establishing shot
							
						ELSE
							
							//match end coord with first frame of mocap
							vecMocapFramePos = GET_CAM_COORD(ExitCamIndex)
							vecMocapFrameRot = GET_CAM_ROT(ExitCamIndex)
							vecMocapFrameFov = GET_CAM_FOV(ExitCamIndex)
						ENDIF
						
						#IF USE_TU_CHANGES
						IF (sTimetableScene.sScene.eScene = PR_SCENE_M7_EMPLOYEECONVO)
							vecMocapFramePos.z -= 30
						ENDIF
						#ENDIF
						
						CONST_FLOAT fPosTolerance	0.5
						CONST_FLOAT fRotTolerance	0.5
						CONST_FLOAT fFovTolerance	0.5
						IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
						OR NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
						OR NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
							TEXT_LABEL_63 tCamDiff
							tCamDiff  = "vecCamEnd <> vecMocapFrame: "
							tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
//							
							CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", tCamDiff)
							
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndPos[", vecCamEndPos.x, ", ", vecCamEndPos.y, ", ", vecCamEndPos.z, "] <> vecMocapFramePos[", vecMocapFramePos.x, ", ", vecMocapFramePos.y, ", ", vecMocapFramePos.z, "]: ", VDIST(vecCamEndPos, vecMocapFramePos))
							ENDIF
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndRot[", vecCamEndRot.x, ", ", vecCamEndRot.y, ", ", vecCamEndRot.z, "] <> vecMocapFrameRot[", vecMocapFrameRot.x, ", ", vecMocapFrameRot.y, ", ", vecMocapFrameRot.z, "]: ", VDIST(vecCamEndRot, vecMocapFrameRot))
							ENDIF
							IF NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
								CPRINTLN(DEBUG_SWITCH, "	vecCamEndFov[", vecCamEndFov, "] <> vecMocapFrameFov[", vecMocapFrameFov, "]: ", ABSF(vecCamEndFov - vecMocapFrameFov))
							ENDIF
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("CASE ")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
							SAVE_STRING_TO_DEBUG_FILE("			vecCamEndPos = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFramePos)
							SAVE_STRING_TO_DEBUG_FILE("		vecCamEndRot = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFrameRot)
							
							IF (vecMocapFrameFov <> 40.0)
								SAVE_STRING_TO_DEBUG_FILE("		vecCamEndFov = ")
								SAVE_FLOAT_TO_DEBUG_FILE(vecMocapFrameFov)
								SAVE_STRING_TO_DEBUG_FILE(" RETURN TRUE BREAK")
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("		RETURN TRUE BREAK")
							ENDIF
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SCRIPT_ASSERT(tCamDiff)
						ENDIF
					ENDIF
					
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
						FLOAT gpCamPitch, gameplayCamSmoothRate, gpCamHead
						IF SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sTimetableScene.sScene.eScene,
								gpCamPitch, gameplayCamSmoothRate, gpCamHead)
							FLOAT paramStrength, amplitudeScalar
							BOOL bBlankCountsForPubClubVisit
							IF NOT SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bBlankCountsForPubClubVisit)
							AND (sTimetableScene.sScene.eScene <> PR_SCENE_F_MD_KUSH_DOC) AND (sTimetableScene.sScene.eScene <> PR_SCENE_F_KUSH_DOC_a)
							AND (sTimetableScene.sScene.eScene <> PR_SCENE_M2_CARSLEEP_a) AND (sTimetableScene.sScene.eScene <> PR_SCENE_M2_CARSLEEP_b) AND (sTimetableScene.sScene.eScene <> PR_SCENE_M6_CARSLEEP)
				
								TEXT_LABEL_63 tCamDiff
								tCamDiff  = "remove RELATIVE_GAMEPLAY_CAM_PITCH for "
								tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
								
								SCRIPT_ASSERT(tCamDiff)
							ENDIF
						ELSE
							FLOAT paramStrength, amplitudeScalar
							BOOL bBlankCountsForPubClubVisit
							IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bBlankCountsForPubClubVisit)
								TEXT_LABEL_63 tCamDiff
								tCamDiff  = "add RELATIVE_GAMEPLAY_CAM_PITCH for "
								tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
								
								SCRIPT_ASSERT(tCamDiff)
							ENDIF
						ENDIF
					ENDIF
					#ENDIF
				ENDIF
				
				TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
				INT iFlags
				IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject, iFlags)
					
					IF IS_ENTITY_ATTACHED(player_prop)
						DETACH_ENTITY(player_prop)
					ENDIF
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop,
							g_iPlayer_Timetable_Exit_SynchSceneID,
							tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
							fBlendin, fBlendOut, iFlags)
							
					IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerSceneSyncExtraObject)
						IF IS_ENTITY_ATTACHED(player_extra_prop)
							DETACH_ENTITY(player_extra_prop)
						ENDIF
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_extra_prop,
								g_iPlayer_Timetable_Exit_SynchSceneID,
								tPlayerSceneSyncExtraObject, tPlayerSceneSyncAnimDict,
								fBlendin, fBlendOut, iFlags)
					ENDIF
				ENDIF
				
				TEXT_LABEL_63 tPlayerDoorSyncAnimL, tPlayerDoorSyncAnimR
				IF GET_SYNCHRONIZED_DOOR_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerDoorSyncAnimL, tPlayerDoorSyncAnimR)

					IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerDoorSyncAnimL)
						IF DOES_ENTITY_EXIST(player_door_l)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(player_door_l,
									g_iPlayer_Timetable_Exit_SynchSceneID,
									tPlayerDoorSyncAnimL, tPlayerSceneSyncAnimDict,
									fBlendin, fBlendOut)
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_door_l, sTimetableScene.sScene.eScene)
						ENDIF
					ENDIF
					IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerDoorSyncAnimR)
						IF DOES_ENTITY_EXIST(player_door_r)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(player_door_r,
									g_iPlayer_Timetable_Exit_SynchSceneID,
									tPlayerDoorSyncAnimR, tPlayerSceneSyncAnimDict,
									fBlendin, fBlendOut)
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(player_door_r, sTimetableScene.sScene.eScene)
						ENDIF
					ENDIF
				ENDIF
				
				TEXT_LABEL_63 entitySetName
				BOOL bActivateSet
				IF SETUP_INTERIOR_ENTITY_SET_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, entitySetName, bActivateSet)
				ENDIF
				
				IF SETUP_FORCE_STEP_TYPE_FOR_SCENE(sTimetableScene.sScene.eScene)
					SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), TRUE, 20, 0)
				ENDIF
				
				TEXT_LABEL TCmod
				IF SETUP_TIMECYCLE_MOD_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, TCmod)
					SET_TIMECYCLE_MODIFIER(TCmod)
				ENDIF
				
//				TEXT_LABEL_63 fxName
//				IF SETUP_PTFX_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, fxName)
//				/*	player_ptfx =*/ START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(fxName,
//							player_prop,
//							<<0,1,0>>, <<0,0,0>>)
//				ENDIF
				
//				TEXT_LABEL_63 tBankName, tBankBName
//				tBankName = ""
//				tBankBName = ""
//				IF SETUP_AUDIO_BANK_FOR_SCENE(sTimetableScene.sScene.eScene, tBankName, tBankBName)
//					PLAY_SOUND_FRONTEND(-1,tBankName)
//				ENDIF
				
				TEXT_LABEL_63 tAudioEvent
				INT iStartOffsetMs
				tAudioEvent = ""
				iStartOffsetMs = 0
				IF SETUP_SYNCH_AUDIO_EVENT_FOR_SCENE(sTimetableScene.sScene.eScene, tAudioEvent, iStartOffsetMs)
					PLAY_SYNCHRONIZED_AUDIO_EVENT(g_iPlayer_Timetable_Exit_SynchSceneID)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_AUDIO_EVENT ", tAudioEvent)
					#ENDIF
					
				ENDIF
				
				TEXT_LABEL_63 tSceneName
				tSceneName = ""
				IF SETUP_AUDIO_SCENE_FOR_SCENE(sTimetableScene.sScene.eScene, tSceneName)
					START_AUDIO_SCENE(tSceneName)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "START_AUDIO_SCENE ", tSceneName)
					#ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(scene_veh)
					TEXT_LABEL_63 tPlayerSceneSyncAnimVehExit
					IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncAnimVehExit)
						
						FLOAT BlendInDelta			// BlendInDelta - The speed at which the synchronized anim should blend in (e.g. NORMAL_BLEND_IN - see commands_task.sch)
						FLOAT BlendOutDelta			// BlendOutDelta - The speed at which the synchronized anim should blend out. (e.g. NORMAL_BLEND_OUT - see commands_task.sch)
						INT Flags					// Flags - The synced scene flags to use (see commands_task.sch - SYNCED_SCENE_PLAYBACK_FLAGS)
						FLOAT MoverBlendInDelta		// MoverBlendInDelta - the speed at which the mover blends in to the scene. This can be usefull for seamless entry onto a synced scene. (e.g. NORMAL_BLEND_IN - see commands_task.sch)
						
						BlendInDelta				= NORMAL_BLEND_IN					//
						BlendOutDelta				= NORMAL_BLEND_OUT					//8.0
						Flags						= ENUM_TO_INT(SYNCED_SCENE_NONE)	//0
						MoverBlendInDelta			= NORMAL_BLEND_IN					//1000.0
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(scene_veh)
							
							CPRINTLN(DEBUG_SWITCH, "Player_Out.synch scene.STOP_PLAYBACK_RECORDED_VEHICLE(scene_veh)")
							
						ENDIF
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(scene_veh,
								g_iPlayer_Timetable_Exit_SynchSceneID,
								tPlayerSceneSyncAnimVehExit, tPlayerSceneSyncAnimDict,
								BlendInDelta, BlendOutDelta, Flags, MoverBlendInDelta)
					ENDIF
				ENDIF
				
				FLOAT paramStrength, amplitudeScalar
				BOOL bCountsForPubClubVisit
				IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bCountsForPubClubVisit)
					Make_Ped_Drunk(PLAYER_PED_ID(), ROUND(iCONST_DRUNK_secDuration*1.5))
					Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sSync
			sSync = "no syncanim in Control_Player_Out_Tasks: "
			sSync += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sSync)
			SCRIPT_ASSERT(sSync)
			#ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			Get_Base_Speed_For_Drive_To_Coord(vSceneVeh_driveOffset, fSceneVeh_driveSpeed)
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD ", vSceneVeh_driveOffset, ", ", fSceneVeh_driveSpeed)
			
			IF DOES_ENTITY_EXIST(scene_veh)
				IF IS_VEHICLE_DRIVEABLE(scene_veh)
					
					
					IF IsShortRangeDefaultSwitch(sTimetableScene.sScene)
						CPRINTLN(DEBUG_SWITCH, "	bypassing...")
						
						RETURN TRUE
					ENDIF
					
					ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Player_Out.VEHICLE_DRIVE_TO_COORD")
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
					
					PED_VEH_DATA_STRUCT sVehData
					VECTOR vVehCoordOffset
					FLOAT fVehHeadOffset
					vVehCoordOffset = <<0,0,0>>
					fVehHeadOffset = 0
					FLOAT fSceneVehDriveSpeed
					IF GET_PLAYER_VEH_POSITION_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene,
							sVehData, vVehCoordOffset, fVehHeadOffset,
							vSceneVeh_driveOffset, fSceneVehDriveSpeed)
							
						SET_ENTITY_COORDS(scene_veh, sTimetableScene.sScene.vCreateCoords+vVehCoordOffset)
						SET_ENTITY_HEADING(scene_veh, sTimetableScene.sScene.fCreateHead+fVehHeadOffset)
						ar_SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
					ENDIF
					
					IF (fSceneVeh_driveSpeed <> GET_ENTITY_SPEED(scene_veh))
					AND fSceneVeh_driveSpeed < 0.1
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
						OR IS_THIS_MODEL_A_PLANE_OR_BLIMP(GET_ENTITY_MODEL(scene_veh))
							fSceneVeh_driveSpeed = GET_ENTITY_SPEED(scene_veh)
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_SWITCH, "Control_Player_Out_Tasks(fSceneVeh_driveSpeed:", fSceneVeh_driveSpeed, ")")
					
					TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), scene_veh,
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset), fSceneVeh_driveSpeed,
							DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS,
							4.0, 30)
					
					
					ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed)
					
					
					IF (fSceneVeh_driveSpeed > 5.0)
						IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
							SET_HELI_BLADES_FULL_SPEED(scene_veh)
							
							IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
							OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
								CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
							ENDIF
						ENDIF
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
							SET_HELI_BLADES_FULL_SPEED(scene_veh)
						ENDIF
					ENDIF
				
					CPRINTLN(DEBUG_SWITCH, "	tasking and destroying all cams...")
					DESTROY_ALL_CAMS(TRUE)
					
					RETURN TRUE
				ELIF IS_ENTITY_DEAD(scene_veh)
					
					CPRINTLN(DEBUG_SWITCH, "	scene_veh is dead???")
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_PED_ON_MOUNT(PLAYER_PED_ID())
				PED_INDEX playersMount_ped
				playersMount_ped = GET_MOUNT(PLAYER_PED_ID())
				
				IF NOT IS_PED_INJURED(playersMount_ped)
					
					TASK_GO_STRAIGHT_TO_COORD(playersMount_ped,
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(playersMount_ped, vSceneVeh_driveOffset),
							fSceneVeh_driveSpeed)	//PEDMOVE_WALK)
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDrive
			sDrive = "something wrong Control_Player_Out_Tasks: "
			sDrive += "SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD  //"
			sDrive += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sDrive)
			SCRIPT_ASSERT(sDrive)
			#ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_31 tlRecordingName
			tlRecordingName = tRecordingName
			IF (iRecordingNum < 100)	tlRecordingName += "0" ENDIF
			IF (iRecordingNum < 10)		tlRecordingName += "0" ENDIF
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out VEHICLE_RECORDING", "(\"", tlRecordingName, "\") //", fRecordingStart)
			#ENDIF
			#IF NOT IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out VEHICLE_RECORDING")
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum > 0)
			
				IF DOES_ENTITY_EXIST(scene_veh)
				AND IS_VEHICLE_DRIVEABLE(scene_veh)	
					
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
						ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Player_Out.VEHICLE_RECORDING")
						
						START_PLAYBACK_RECORDED_VEHICLE(scene_veh, iRecordingNum, tRecordingName)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(scene_veh, fRecordingStart)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sRecording
			sRecording = "something wrong Control_Player_Out_Tasks: "
			sRecording += "VEHICLE_RECORDING  //"
			sRecording += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sRecording)
			SCRIPT_ASSERT(sRecording)
			#ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,5,0>>),
					PEDMOVE_WALK)
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
			
			VECTOR vWalkOffset
			vWalkOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,5,0>>)
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD[", vWalkOffset, "]")
			
			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vWalkOffset, PEDMOVE_WALK)
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_WANDER_STANDARD
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_WANDER_STANDARD[", "]")
			
			IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_WANDER_STANDARD) != PERFORMING_TASK)
				TASK_WANDER_STANDARD(PLAYER_PED_ID())
			ENDIF
			
			RETURN TRUE
		BREAK
		
		
		CASE SCRIPT_TASK_ENTER_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_ENTER_VEHICLE")
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), scene_veh,
							DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_LEAVE_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_LEAVE_VEHICLE")
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), scene_veh)
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_STAND_STILL
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_STAND_STILL")
			
			IF (sTimetableScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> SCRIPT_TASK_STAND_STILL clears SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF (IS_ENTITY_IN_WATER(PLAYER_PED_ID()) OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_LAKE))
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "Player_Out - water")
//					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_DIVING_IDLE)
				ENDIF
			ENDIF
			
			IF (sTimetableScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> SCRIPT_TASK_STAND_STILL clears SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD")
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			IF (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING1)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING2)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_T6_HUNTING3)
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, TRUE)
				ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, TRUE)
				ENDIF
			ENDIF
			
//			IF IS_PLAYER_SWITCH_IN_PROGRESS()
//			AND GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_LONG
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//						IF bDisableWhileWaiting
//							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//						ENDIF
//					ENDIF
//				ELSE
//					IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//						IF bDisableWhileWaiting
//							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_ACHIEVE_HEADING
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_ACHIEVE_HEADING")
			
			TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), sTimetableScene.sScene.fCreateHead+180.0)
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER")
			
			ar_TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), -1)
			
			bDisableWhileWaiting = FALSE
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_AIM_GUN_AT_ENTITY
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_AIM_GUN_AT_ENTITY")
			
			IF NOT IS_PED_INJURED(scene_buddy)
				
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 30)
				ENDIF
				
//				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
//				TASK_AIM_GUN_AT_ENTITY(PLAYER_PED_ID(), scene_buddy, -1)
				
//				SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_USE_MOBILE_PHONE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Player_Out SCRIPT_TASK_USE_MOBILE_PHONE")
			
			bWaitingForPlayerLieInInput = TRUE
			
//			TASK_USE_MOBILE_PHONE_TIMED(PLAYER_PED_ID(), 28000)
//			ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(PLAYER_PED_ID(),
//					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,5,0>>),
//					PEDMOVE_WALK)
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ANY						FALLTHRU
		DEFAULT

			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sInvalid
			sInvalid = "invalid eTask for Control_Player_Out_Tasks: "
			sInvalid += ENUM_TO_INT(sTimetableScene.eOutTask)
			sInvalid += "  //"
			sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
			SCRIPT_ASSERT(sInvalid)
			#ENDIF
			
			IF (sTimetableScene.eOutTask <> SCRIPT_TASK_ANY)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sUnknown
	sUnknown = "unknown eTask for Control_Player_Out_Tasks: "
	sUnknown += ENUM_TO_INT(sTimetableScene.eOutTask)
	sUnknown += "  //"
	sUnknown += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sUnknown)
	SCRIPT_ASSERT(sUnknown)
	#ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL Control_Buddy_Out_Tasks()
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
	
	SWITCH sTimetableScene.eBuddyOutTask
		CASE SCRIPT_TASK_PLAY_ANIM
			TEXT_LABEL_63 tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut
			ANIMATION_FLAGS buddySceneLoopFlag, buddySceneOutFlag
			
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
					buddySceneLoopFlag, buddySceneOutFlag)
//				SET_PED_COORDS_KEEP_VEHICLE(scene_buddy, vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset)
//				SET_ENTITY_HEADING(scene_buddy, fCreateHead+sTimetableScene.fSceneBuddyHeadOffset)
				
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_PLAY_ANIM \"", tSceneBuddyAnimDict, "\", \"", tSceneBuddyAnimOut, "\" flag: ", ENUM_TO_INT(buddySceneOutFlag))
				
				IF (sTimetableScene.sScene.eScene = PR_SCENE_M_VWOODPARK_a)
				OR (sTimetableScene.sScene.eScene = PR_SCENE_M_VWOODPARK_b)
					TASK_LOOK_AT_ENTITY(scene_buddy, PLAYER_PED_ID(), -1)
				ENDIF
				
				TASK_PLAY_ANIM(scene_buddy, tSceneBuddyAnimDict, tSceneBuddyAnimOut,
						INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
						buddySceneOutFlag)
				SET_PED_KEEP_TASK(scene_buddy, TRUE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
			TEXT_LABEL_63 tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut
			ANIMATION_FLAGS buddySyncLoopFlag, buddySyncOutFlag
			
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut,
					buddySyncLoopFlag, buddySyncOutFlag)
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_SYNCHRONIZED_SCENE \"", tBuddySceneSyncAnimDict, "\", \"", tBuddySceneSyncAnimOut, "\"")
				
				SetPedFromVehicleIntoSynchedScene(PLAYER_PED_ID(), scene_veh)
				
				VECTOR vCreateRot
				GET_PLAYER_PED_ROTATION_FOR_SCENE(sTimetableScene.sScene.eScene, fCreateHead, vCreateRot)
				
				Safe_Create_Synchronized_Scene(g_iPlayer_Timetable_Exit_SynchSceneID, "Buddy_Out", vCreateCoords, vCreateRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
				
				SYNCED_SCENE_PLAYBACK_FLAGS scpf_Flags
				IK_CONTROL_FLAGS ikFlags
				F_GET_BUDDY_SYNCHRONIZED_SCENE_FLAG(sTimetableScene.sScene.eScene, scpf_Flags, ikFlags)
				
				TASK_SYNCHRONIZED_SCENE(scene_buddy, g_iPlayer_Timetable_Exit_SynchSceneID,
						tBuddySceneSyncAnimDict, tBuddySceneSyncAnimOut,
						NORMAL_BLEND_IN, WALK_BLEND_OUT,
						scpf_Flags, DEFAULT, DEFAULT, ikFlags)
				SET_FORCE_FOOTSTEP_UPDATE(scene_buddy, TRUE)
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD ", vSceneVeh_driveOffset, ", ", fSceneVeh_driveSpeed)
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				
				TASK_VEHICLE_DRIVE_TO_COORD(scene_buddy, scene_veh,
						GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset), fSceneVeh_driveSpeed,
						DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS,
						4.0, 30)
				
				ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Buddy_Out")
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, TRUE)
				
				ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed)
				
				
				IF (fSceneVeh_driveSpeed > 5.0)
					IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(scene_veh))
						SET_HELI_BLADES_FULL_SPEED(scene_veh)
						
						IF GET_LANDING_GEAR_STATE(scene_veh) != LGS_LOCKED_UP
						OR GET_LANDING_GEAR_STATE(scene_veh) != LGS_RETRACTING
							CONTROL_LANDING_GEAR(scene_veh, LGC_RETRACT_INSTANT)
						ENDIF
					ENDIF
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(scene_veh))
						SET_HELI_BLADES_FULL_SPEED(scene_veh)
					ENDIF
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> buddy_Out SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
			
			VECTOR vWalktoOffset
			vWalktoOffset = <<0,10,0>>
			IF sTimetableScene.sScene.eScene = PR_SCENE_T_CR_WAKEBEACH
				vWalktoOffset = <<0,15,0>>
			ENDIF
			IF sTimetableScene.sScene.eScene = PR_SCENE_T_CN_WAKEBARN
				vWalktoOffset = <<-7.5,5,0>>
			ENDIF
			
			SEQUENCE_INDEX siseq
			OPEN_SEQUENCE_TASK(siseq)
				TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_buddy, vWalktoOffset), PEDMOVE_WALK)
				TASK_WANDER_IN_AREA(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_buddy, vWalktoOffset*2.5), 10)
			CLOSE_SEQUENCE_TASK(siseq)
			TASK_PERFORM_SEQUENCE(scene_buddy, siseq)
			CLEAR_SEQUENCE_TASK(siseq)
			
			RETURN TRUE
		BREAK

		CASE SCRIPT_TASK_ENTER_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_ENTER_VEHICLE")
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				TASK_ENTER_VEHICLE(scene_buddy, scene_veh,
						DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_LEAVE_VEHICLE
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_LEAVE_VEHICLE")
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				TASK_LEAVE_VEHICLE(scene_buddy, scene_veh)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_STAND_STILL
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_STAND_STILL")
			
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_WANDER_STANDARD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_WANDER_STANDARD")
			
			TASK_WANDER_STANDARD(scene_buddy)
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED")
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(scene_buddy, 25.0)
			RETURN TRUE
		BREAK
		CASE SCRIPT_TASK_START_SCENARIO_AT_POSITION
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_START_SCENARIO_AT_POSITION")
			
			TEXT_LABEL_63 tBuddySceneScenarioDict, tBuddySceneScenarioLoop, tBuddySceneScenarioOut
			ANIMATION_FLAGS buddyScenarioLoopFlag, buddyScenarioOutFlag
			
			IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tBuddySceneScenarioDict, tBuddySceneScenarioLoop, tBuddySceneScenarioOut,
					buddyScenarioLoopFlag, buddyScenarioOutFlag)
				
				INT iTimeToLeave
				TASK_START_SCENARIO_AT_POSITION(scene_buddy, tBuddySceneScenarioOut,
						vCreateCoords+sTimetableScene.vSceneBuddyCoordOffset,
						fCreateHead+sTimetableScene.fSceneBuddyHeadOffset,
						iTimeToLeave, FALSE)
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_HANDS_UP
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Buddy_Out SCRIPT_TASK_HANDS_UP")
			
			TASK_HANDS_UP(scene_buddy, -1, PLAYER_PED_ID())
			SET_PED_KEEP_TASK(scene_buddy, TRUE)
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ANY						FALLTHRU
		DEFAULT

			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sInvalid
			sInvalid = "invalid eTask for Control_Buddy_Out_Tasks: "
			sInvalid += ENUM_TO_INT(sTimetableScene.eBuddyOutTask)
			sInvalid += "  //"
			sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
			SCRIPT_ASSERT(sInvalid)
			#ENDIF
			
			IF (sTimetableScene.eBuddyOutTask <> SCRIPT_TASK_ANY)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sUnknown
	sUnknown = "unknown eTask for Control_Buddy_Out_Tasks: "
	sUnknown += ENUM_TO_INT(sTimetableScene.eBuddyOutTask)
	sUnknown += "  //"
	sUnknown += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sUnknown)
	SCRIPT_ASSERT(sUnknown)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Play_Buddy_PostOut_SynchScene(PED_REQUEST_SCENE_ENUM ePedScene, PED_INDEX PedIndex,
		INT &iPostOutSceneID, STRING sPostOutAnimPostOut = NULL)
	TEXT_LABEL_63 tPostOutAnimDict, tPostOutAnimLoop, tPostOutAnimOut
	ANIMATION_FLAGS PostOutLoopFlag, PostOutOutFlag
	
	VECTOR vFloydSceneCoord
	FLOAT fFloydSceneRot
	TEXT_LABEL_31 sFloydRoom
	
	IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(ePedScene, tPostOutAnimDict, tPostOutAnimLoop, tPostOutAnimOut,
			PostOutLoopFlag, PostOutOutFlag)
	AND GET_PLAYER_PED_POSITION_FOR_SCENE(ePedScene, vFloydSceneCoord, fFloydSceneRot, sFloydRoom)
		
		TEXT_LABEL_63 tPostOutAnimPostOut = sPostOutAnimPostOut
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sPostOutAnimPostOut)
			tPostOutAnimPostOut = sPostOutAnimPostOut
		ELSE
			tPostOutAnimPostOut = tPostOutAnimLoop
		ENDIF
		
		REQUEST_ANIM_DICT(tPostOutAnimDict)
		WHILE NOT HAS_ANIM_DICT_LOADED(tPostOutAnimDict)
			WAIT(0)
			DisableWhileWaiting("Play_Buddy_PostOut_SynchScene")
		ENDWHILE
		IF NOT IS_PED_INJURED(PedIndex)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> Play_Buddy_PostOut_SynchScene \"", tPostOutAnimDict, "\", \"", tPostOutAnimPostOut, "\" ", Get_String_From_FamilyEvent(g_eSceneBuddyEvents))
			#ENDIF
			
			SYNCED_SCENE_PLAYBACK_FLAGS scpf_Flags
			IK_CONTROL_FLAGS ikFlags
			
			F_GET_BUDDY_SYNCHRONIZED_SCENE_FLAG(ePedScene, scpf_Flags, ikFlags)
			
			iPostOutSceneID = CREATE_SYNCHRONIZED_SCENE(vFloydSceneCoord, <<0.0, 0.0, fFloydSceneRot>>)
			TASK_SYNCHRONIZED_SCENE(PedIndex, iPostOutSceneID, tPostOutAnimDict, tPostOutAnimPostOut,
					INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
					scpf_Flags, DEFAULT, INSTANT_BLEND_IN, ikFlags)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iPostOutSceneID, TRUE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iPostOutSceneID, FALSE)
			SET_SYNCHRONIZED_SCENE_PHASE(iPostOutSceneID, 0.0)
			
			SET_PED_KEEP_TASK(PedIndex, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Control_Buddy_PostOut_Tasks()
//	TEXT_LABEL_63 tPostOutAnimDict, tPostOutAnimLoop, tPostOutAnimOut
//	ANIMATION_FLAGS PostOutLoopFlag, PostOutOutFlag
	INT iPostOutSceneID
	
	SWITCH sTimetableScene.sScene.eScene
		CASE PR_SCENE_Ma_FAMILY1
			IF NOT IS_PED_INJURED(scene_buddy)
				
				vSceneVeh_driveOffset = <<0,5,0>>
				fSceneVeh_driveSpeed = 10.0
				
				SEQUENCE_INDEX siseq
				OPEN_SEQUENCE_TASK(siseq)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, scene_veh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset*1.5), fSceneVeh_driveSpeed, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS, 4.0, 30)
					TASK_VEHICLE_DRIVE_WANDER(NULL, scene_veh, fSceneVeh_driveSpeed, DRIVINGMODE_AVOIDCARS)
				CLOSE_SEQUENCE_TASK(siseq)
				TASK_PERFORM_SEQUENCE(scene_buddy, siseq)
				CLEAR_SEQUENCE_TASK(siseq)
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_F_CLUB
			WAIT(0)
			
			IF NOT IS_PED_INJURED(scene_buddy)
				
				FORCE_PED_MOTION_STATE(scene_buddy, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
			//	FORCE_PED_AI_AND_ANIMATION_UPDATE(scene_buddy)	//1422634
				
				SEQUENCE_INDEX siseq
				OPEN_SEQUENCE_TASK(siseq)
					TASK_GO_STRAIGHT_TO_COORD(NULL, <<-510.3902, -21.7973, 45.6141>>, PEDMOVE_WALK)
					TASK_WANDER_IN_AREA(NULL, <<-510.3902, -21.7973, 45.6141>>, 5)
				CLOSE_SEQUENCE_TASK(siseq)
				TASK_PERFORM_SEQUENCE(scene_buddy, siseq)
				CLEAR_SEQUENCE_TASK(siseq)
				WAIT(0)
				
				RETURN TRUE
			ENDIF
		BREAK

		CASE PR_SCENE_M2_BEDROOM
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID,
					"BED_LOOP_02_Amanda")
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy,
					iPostOutSceneID)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE PR_SCENE_M7_LOUNGECHAIRS
//			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy,
//					iPostOutSceneID)
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID)
				TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
				ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
				//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
						playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
					TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
					IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene,
							tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop, iPostOutSceneID,
								tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
								INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
								ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS),
								INSTANT_BLEND_IN)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_extra_prop, iPostOutSceneID,
								tPlayerSceneSyncExtraObject, tPlayerSceneSyncAnimDict,
								INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
								ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS),
								INSTANT_BLEND_IN)
						SET_ENTITY_COLLISION(player_extra_prop, FALSE)
					ENDIF
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M6_RONBORING
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID)
				TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
				ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
				//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
						playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
					TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
					IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject)
//						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop, iPostOutSceneID,
//								tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
//								INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
//								ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_LOOP_WITHIN_SCENE),
//								NORMAL_BLEND_IN)
//						SET_ENTITY_COLLISION(player_prop, FALSE)
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(player_extra_prop, iPostOutSceneID,
								tPlayerSceneSyncExtraObject, tPlayerSceneSyncAnimDict,
								INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
								ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_LOOP_WITHIN_SCENE),
								INSTANT_BLEND_IN)
						SET_ENTITY_COLLISION(player_extra_prop, FALSE)
					ENDIF
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_GETSREADY
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID,
					"AROUND_THE_TABLE_SELFISH_EXIT_LOOP_Jimmy")
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M2_KIDS_TV
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID,
					"Console_end_LOOP_FLOYD")
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID,
					"bear_floyds_face_smell_exit_loop_floyd")
				
				TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
				ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
				//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
						playerSyncLoopFlag, playerSyncOutFlag)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop, iPostOutSceneID,
							"BEAR_FLOYDS_FACE_SMELL_EXIT_LOOP_Doll", tPlayerSceneSyncAnimDict,
							INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
							ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_LOOP_WITHIN_SCENE),
							INSTANT_BLEND_IN)
//					PLAY_ENTITY_ANIM(player_prop,
//							"BEAR_FLOYDS_FACE_SMELL_EXIT_LOOP_Doll", tPlayerSceneSyncAnimDict,
//							INSTANT_BLEND_IN, TRUE, TRUE)
					SET_ENTITY_COLLISION(player_prop, FALSE)
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID,
					"Pineapple_EXIT_LOOP_FLOYD")
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_A2	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B2	
			IF Play_Buddy_PostOut_SynchScene(sTimetableScene.sScene.eScene, scene_buddy, iPostOutSceneID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
			IF NOT IS_PED_INJURED(scene_buddy)
				RETURN PRIVATE_Update_Family_Find_Event(scene_buddy,
						FM_MICHAEL_WIFE, << -812.0607, 179.5117, 71.1531 >>, DEFAULT, FALSE)
			ENDIF
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			IF NOT IS_PED_INJURED(scene_buddy)
				RETURN PRIVATE_Update_Family_Find_Event(scene_buddy,
						FM_MICHAEL_WIFE, << -812.0607, 179.5117, 71.1531 >>, DEFAULT, FALSE)
			ENDIF
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Control_Buddy_PostOut_FriendEvents()
	
	
	SWITCH sTimetableScene.sScene.eScene
		CASE PR_SCENE_M2_BEDROOM		FALLTHRU
		CASE PR_SCENE_M2_SAVEHOUSE0_b	
			g_eSceneBuddyEvents = FE_M2_WIFE_sleeping
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_GETSREADY
			g_eSceneBuddyEvents = FE_M7_WIFE_sleeping
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE
			g_eSceneBuddyEvents = FE_M_WIFE_gets_drink_in_kitchen
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
		CASE PR_SCENE_M7_WIFETENNIS
			g_eSceneBuddyEvents = FE_ANY_find_family_event
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			g_eSceneBuddyEvents = FE_M7_SON_gaming
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			g_eSceneBuddyEvents = FE_M7_SON_on_laptop_looking_for_jobs
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			g_eSceneBuddyEvents = FE_T1_FLOYD_cleaning
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_A2	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B	FALLTHRU
		CASE PR_SCENE_T_FLOYDSPOON_B2	
			g_eSceneBuddyEvents = FE_T1_FLOYD_cries_in_foetal_position
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E0	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E1	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E2	FALLTHRU
		CASE PR_SCENE_T_FLOYDCRYING_E3	
			g_eSceneBuddyEvents = FE_T1_FLOYD_cries_on_sofa
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			g_eSceneBuddyEvents = FE_T1_FLOYD_pineapple
			RETURN TRUE
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Has_Current_Player_Ped_Updated()
	BOOL bPlayer_Ped_Updated = FALSE
	
	IF sTimetableScene.sScene.ePed = GET_CURRENT_PLAYER_PED_ENUM()	//
	
		IF NOT sTimetableScene.bDescentOnly
			bPlayer_Ped_Updated = TRUE
		ELSE
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_PREP_DESCENT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "bDescentOnly ", Get_String_From_Switch_State(GET_PLAYER_SWITCH_STATE()))
					#ENDIF
				ELSE
					IF NOT SET_PED_PRESET_OUTFIT_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene, PLAYER_PED_ID(), iPlayerOutfitPreloadStage)
						bPlayer_Ped_Updated = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT SET_PED_PRESET_OUTFIT_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene, PLAYER_PED_ID(), iPlayerOutfitPreloadStage)
					bPlayer_Ped_Updated = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bPlayer_Ped_Updated
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			//attach the prop to the player?
			IF DOES_ENTITY_EXIST(player_prop)
				MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
				VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
				PED_BONETAG eBonetag = BONETAG_NULL
				FLOAT fDetachAnimPhase = -1.0
				enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
				
				IF GET_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
							eObjectModel, vecOffset, vecRotation, eBonetag, fDetachAnimPhase, thisSceneObjectAction)
					IF (eBonetag <> BONETAG_NULL)
						SET_ENTITY_RECORDS_COLLISIONS(player_prop, TRUE)
						ar_FREEZE_ENTITY_POSITION(player_prop, FALSE, "player_prop one")
						
						ATTACH_ENTITY_TO_ENTITY(player_prop, PLAYER_PED_ID(),
								GET_PED_BONE_INDEX(PLAYER_PED_ID(), eBonetag),
								vecOffset, vecRotation)
					ELSE
						VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
						FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
						
						SET_ENTITY_COORDS(player_prop, vCreateCoords+vecOffset)
						SET_ENTITY_ROTATION(player_prop, <<0,0,fCreateHead>>+vecRotation)
						
						ar_FREEZE_ENTITY_POSITION(player_prop, TRUE, "player_prop two")
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(player_extra_prop)
				MODEL_NAMES eObjectModel = DUMMY_MODEL_FOR_SCRIPT
				VECTOR vecOffset = <<0,0,0>>, vecRotation = <<0,0,0>>
				PED_BONETAG eBonetag = BONETAG_NULL
				FLOAT fDetachAnimPhase = -1.0
				enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
				BOOL bAttachedToBuddy
				
				IF GET_EXTRA_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
							eObjectModel, vecOffset, vecRotation, eBonetag, fDetachAnimPhase,
							thisSceneObjectAction, bAttachedToBuddy)
					IF (eBonetag <> BONETAG_NULL)
						SET_ENTITY_RECORDS_COLLISIONS(player_extra_prop, TRUE)
						ar_FREEZE_ENTITY_POSITION(player_extra_prop, FALSE, "player_extra_prop one")
						
						IF NOT bAttachedToBuddy
							ATTACH_ENTITY_TO_ENTITY(player_extra_prop, PLAYER_PED_ID(),
									GET_PED_BONE_INDEX(PLAYER_PED_ID(), eBonetag),
									vecOffset, vecRotation)
						ELSE
							IF NOT IS_ENTITY_DEAD(g_pScene_buddy)
								ATTACH_ENTITY_TO_ENTITY(player_extra_prop, g_pScene_buddy,
										GET_PED_BONE_INDEX(g_pScene_buddy, eBonetag),
										vecOffset, vecRotation)
							ELSE
								SCRIPT_ASSERT("IS_ENTITY_DEAD(g_pScene_buddy)")
							ENDIF
						ENDIF
									
					ELSE
						VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
						FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
						
						SET_ENTITY_COORDS(player_extra_prop, vCreateCoords+vecOffset)
						SET_ENTITY_ROTATION(player_extra_prop, <<0,0,fCreateHead>>+vecRotation)
						
						ar_FREEZE_ENTITY_POSITION(player_extra_prop, TRUE, "player_extra_prop two")
					ENDIF
				ENDIF
			ENDIF
			
			TEXT_LABEL_31 RoomName
			FLOAT fTurnOffTVPhase
			TV_LOCATION eRoomTVLocation
			TVCHANNELTYPE eTVChannelType
			TV_CHANNEL_PLAYLIST eTVPlaylist

			IF CONTROL_PLAYER_WATCHING_TV(sTimetableScene.sScene.eScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
				IF IS_THIS_TV_AVAILABLE_FOR_USE(eRoomTVLocation)
					IF NOT IS_THIS_TV_FORCED_ON(eRoomTVLocation)
					AND NOT bSwitchForcedStartAmbientTV
						START_AMBIENT_TV_PLAYBACK(eRoomTVLocation)
						bSwitchForcedStartAmbientTV = TRUE
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "<", GET_THIS_SCRIPT_NAME(), "> START_AMBIENT_TV_PLAYBACK()")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			TEXT_LABEL_63 EmitterName
			FLOAT fTurnOffRadioPhase
			IF SET_SCENE_RADIO_EMITTER_ENABLED(sTimetableScene.sScene.eScene, EmitterName, fTurnOffRadioPhase)
				SET_STATIC_EMITTER_ENABLED(EmitterName, FALSE)
			ENDIF
			
			SET_PED_DAMAGE_FOR_SCENE(sTimetableScene.sScene.eScene, PLAYER_PED_ID())
			CDEBUG3LN(DEBUG_SWITCH, " - player_timetable_scene - Has_Current_Player_Ped_Updated - Checking scuba state for new ped.")
			SET_PED_SCUBA_FOR_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.ePed)
			
			CLEAR_HELP()		//url:bugstar:331736
			RETURN TRUE
		ENDIF
	ENDIF
	
//	SP_MISSIONS eTriggerMission
//	eTriggerMission = MISSION_FLOW_GET_ACTIVE_TRIGGER_SCENE()
//	IF (eTriggerMission != SP_MISSION_NONE)
//		
//		SELECTOR_SLOTS_ENUM playerSelectorID
//		playerSelectorID = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(sTimetableScene.sScene.ePed)
//		
//		IF IS_BIT_SET(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(playerSelectorID))
//			
//			IF (sTimetableScene.sScene.eScene = PR_SCENE_M_DEFAULT AND playerSelectorID = SELECTOR_PED_MICHAEL)
//			OR (sTimetableScene.sScene.eScene = PR_SCENE_F_DEFAULT AND playerSelectorID = SELECTOR_PED_FRANKLIN)
//			OR (sTimetableScene.sScene.eScene = PR_SCENE_T_DEFAULT AND playerSelectorID = SELECTOR_PED_TREVOR)
//				//
//				
//				CPRINTLN(DEBUG_SWITCH, "player timetable scene:sTimetableScene.eScene = MISSION_FLOW_FORCE_TRIGGER_MISSION()")
//				
////				Player_Timetable_Scene_Cleanup()
////				RETURN FALSE
//				
//				sTimetableScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//				sTimetableScene.eOutTask = SCRIPT_TASK_STAND_STILL
//				RETURN TRUE
//			ELSE
//				CPRINTLN(DEBUG_SWITCH, "player timetable scene:sTimetableScene.eScene = playerSelectorID")
//			ENDIF
//		ELSE
//			
//			CPRINTLN(DEBUG_SWITCH, "player timetable scene:IS_BIT_NOT_SET(SELECTOR_")
//			#IF IS_DEBUG_BUILD 
//			CPRINTLN(DEBUG_SWITCH, GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(playerSelectorID)))
//			#ENDIF
//			CPRINTLN(DEBUG_SWITCH, ")")
//		ENDIF
//	ELSE
////		CPRINTLN(DEBUG_SWITCH, "special assets:eTriggerMission = SP_MISSION_NONE")
//	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL StopPlaybackPlayersRecordedVehicle(INT &iDrawSceneRot, FLOAT fTimePosition)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 strStop
	strStop  = "fRecordingStop:"
	strStop += GET_STRING_FROM_FLOAT(fRecordingStop)
	
	DrawLiteralSceneString(strStop, iDrawSceneRot, HUD_COLOUR_BLUEDARK)
	#ENDIF
	iDrawSceneRot++
	
	IF fTimePosition >= fRecordingStop
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL Has_Player_Finished_Switching()
	VECTOR vCreateCoords = sTimetableScene.sScene.vCreateCoords
	FLOAT fCreateHead = sTimetableScene.sScene.fCreateHead
	
	IF NOT IsShortRangeDefaultSwitch(sTimetableScene.sScene)
		IF (sTimetableScene.eOutTask <> SCRIPT_TASK_USE_MOBILE_PHONE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
	ENDIF
	
	IF sTimetableScene.eVehState <> PTVS_2_playerInVehicle
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   * sTimetableScene.eVehState <> PTVS_2_playerInVehicle")
		
		IF NOT IsShortRangeDefaultSwitch(sTimetableScene.sScene)
			IF (sTimetableScene.eOutTask <> SCRIPT_TASK_USE_MOBILE_PHONE)
			AND (sTimetableScene.sScene.eScene <> PR_SCENE_F_S_EXILE2)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fCreateHead)
				SET_PED_CLOTH_PIN_FRAMES(PLAYER_PED_ID(), 1)
			ENDIF
		ENDIF
		
		/*IF NOT IS_PED_INJURED(scene_buddy)
			IF (GET_ENTITY_MODEL(scene_buddy) = A_C_HORSE)
				SET_PED_ONTO_MOUNT(PLAYER_PED_ID(), scene_buddy)
			ENDIF
		ENDIF*/
	ELSE
		
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   * IF sTimetableScene.eVehState = PTVS_2_playerInVehicle")
		
		IF IS_VEHICLE_DRIVEABLE(scene_veh)
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   ** IF IS_VEHICLE_DRIVEABLE(scene_veh)")
			Setup_Player_In_Switch_Vehicle()
			
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   * set wanted level on players vehicle")
				
				SET_VEHICLE_IS_WANTED(scene_veh, TRUE)
				
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
			
			// Radio
			enumCharacterList eCurrentPlayerPedEnum = GET_CURRENT_PLAYER_PED_ENUM()
			INT iRadioIndex = g_sPlayerLastVeh[eCurrentPlayerPedEnum].iRadioIndex
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   **** SET_RADIO_TO_STATION_INDEX(", iRadioIndex, ")	//", GET_PLAYER_PED_STRING(eCurrentPlayerPedEnum))
			
			enumCharacterList ePlayer
			REPEAT NUM_OF_PLAYABLE_PEDS ePlayer
				CPRINTLN(DEBUG_SWITCH, "		player ", GET_PLAYER_PED_STRING(ePlayer), " radio: ", g_sPlayerLastVeh[ePlayer].iRadioIndex)
			ENDREPEAT
			
			
			#ENDIF
			
			SET_RADIO_TO_STATION_INDEX(iRadioIndex)
		ELSE
			
			CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene>   ** IF NOT IS_VEHICLE_DRIVEABLE(scene_veh)")
			
			IF NOT IsShortRangeDefaultSwitch(sTimetableScene.sScene)
				IF NOT IS_ENTITY_DEAD(scene_veh)
					SET_VEHICLE_FIXED(scene_veh)
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	Control_Player_Loop_Tasks()
	IF NOT IS_PED_INJURED(scene_buddy)
		Control_Buddy_Loop_Tasks()
	ENDIF
	
	FLOAT gameplayCamPitch, gameplayCamSmoothRate
	FLOAT gameplayCamHeading
	IF SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sTimetableScene.sScene.eScene,
			gameplayCamPitch, gameplayCamSmoothRate, gameplayCamHeading)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 tPlayerSceneProceduralCam
		IF SETUP_PROCEDURAL_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneProceduralCam)
		AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			TEXT_LABEL_63 sProc = "remove gameplay pitch for proc cam : "
			sProc += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sProc)
			SCRIPT_ASSERT(sProc)
			
			gameplayCamPitch = 999.000
			gameplayCamHeading = 999.000
		ENDIF
		#ENDIF
		
		IF (gameplayCamPitch <> 999.000)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(gameplayCamPitch, gameplayCamSmoothRate)
		ENDIF
		IF (gameplayCamHeading <> 999.000)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(gameplayCamHeading)
		ENDIF
	
	ENDIF
	
	//	#1327351
	IF NOT DOES_CAM_EXIST(ExitCamIndex)
		VECTOR vSceneCoord
		FLOAT fSceneRot
		TEXT_LABEL_31 tCreateRoom
		
		IF GET_PLAYER_PED_POSITION_FOR_SCENE(sTimetableScene.sScene.eScene, vSceneCoord, fSceneRot, tCreateRoom)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tCreateRoom)
				TEXT_LABEL_23 tPlayerSceneProceduralCam
				IF SETUP_PROCEDURAL_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneProceduralCam)
					VECTOR vecCamEndPos, vecCamEndRot
					FLOAT vecCamEndFov
					
					IF SETUP_PLAYER_CAMERA_FOR_SCENE(sTimetableScene.sScene.eScene, vecCamEndPos, vecCamEndRot, vecCamEndFov)
						
					#IF IS_DEBUG_BUILD
					OR NOT IS_STRING_NULL_OR_EMPTY(tPlayerSceneProceduralCam)
						
						VECTOR vecMocapFramePos, vecMocapFrameRot
						FLOAT vecMocapFrameFov
						IF DEBUG_GetPlayerSwitchEstablishingShot(tPlayerSceneProceduralCam, "",
								vecMocapFramePos, vecMocapFrameRot, vecMocapFrameFov)
							
							#IF USE_TU_CHANGES
							IF (sTimetableScene.sScene.eScene = PR_SCENE_M7_EMPLOYEECONVO)
								vecMocapFramePos.z -= 30
							ENDIF
							#ENDIF
							
							CONST_FLOAT fPosTolerance	0.5
							CONST_FLOAT fRotTolerance	0.5
							CONST_FLOAT fFovTolerance	0.5
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
							OR NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
							OR NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
								TEXT_LABEL_63 tCamDiff
								tCamDiff  = "vecCamEnd <> vecMocapFrame: "
								tCamDiff += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	//							
								CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", tCamDiff)
								
								IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndPos, vecMocapFramePos, fPosTolerance)
									CPRINTLN(DEBUG_SWITCH, "	vecCamEndPos[", vecCamEndPos.x, ", ", vecCamEndPos.y, ", ", vecCamEndPos.z, "] <> vecMocapFramePos[", vecMocapFramePos.x, ", ", vecMocapFramePos.y, ", ", vecMocapFramePos.z, "]: ", VDIST(vecCamEndPos, vecMocapFramePos))
								ENDIF
								IF NOT ARE_VECTORS_ALMOST_EQUAL(vecCamEndRot, vecMocapFrameRot, fRotTolerance)
									CPRINTLN(DEBUG_SWITCH, "	vecCamEndRot[", vecCamEndRot.x, ", ", vecCamEndRot.y, ", ", vecCamEndRot.z, "] <> vecMocapFrameRot[", vecMocapFrameRot.x, ", ", vecMocapFrameRot.y, ", ", vecMocapFrameRot.z, "]: ", VDIST(vecCamEndRot, vecMocapFrameRot))
								ENDIF
								IF NOT (ABSF(vecCamEndFov - vecMocapFrameFov) <= fFovTolerance)
									CPRINTLN(DEBUG_SWITCH, "	vecCamEndFov[", vecCamEndFov, "] <> vecMocapFrameFov[", vecMocapFrameFov, "]: ", ABSF(vecCamEndFov - vecMocapFrameFov))
								ENDIF
								
								SAVE_STRING_TO_DEBUG_FILE("CASE ")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
								SAVE_STRING_TO_DEBUG_FILE("			vecCamEndPos = ")
								SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFramePos)
								SAVE_STRING_TO_DEBUG_FILE("		vecCamEndRot = ")
								SAVE_VECTOR_TO_DEBUG_FILE(vecMocapFrameRot)
								
								IF (vecMocapFrameFov <> 40.0)
									SAVE_STRING_TO_DEBUG_FILE("		vecCamEndFov = ")
									SAVE_FLOAT_TO_DEBUG_FILE(vecMocapFrameFov)
									SAVE_STRING_TO_DEBUG_FILE(" RETURN TRUE BREAK	//")
								ELSE
									SAVE_STRING_TO_DEBUG_FILE("		RETURN TRUE BREAK	//")
								ENDIF
								
								SAVE_STRING_TO_DEBUG_FILE(tPlayerSceneProceduralCam)
								
								SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								vecCamEndPos = vecMocapFramePos
								vecCamEndRot = vecMocapFrameRot
								vecCamEndFov = vecMocapFrameFov
								
								SCRIPT_ASSERT(tCamDiff)
							ENDIF
							
						ENDIF
						#ENDIF
						
						ExitCamIndex = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						SET_CAM_COORD(ExitCamIndex, vecCamEndPos)
						SET_CAM_ROT(ExitCamIndex, vecCamEndRot)
						SET_CAM_FOV(ExitCamIndex, vecCamEndFov)
						
						SET_CAM_ACTIVE(ExitCamIndex,TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Create_Switch_Scene_Conversation()
	
	IF bHave_Created_Estab_Shot_Conversation
		KILL_ANY_CONVERSATION()
		bHave_Created_Estab_Shot_Conversation = FALSE
	ENDIF
	
	TEXT_LABEL tPlayerSceneVoiceID, tBuddySceneVoiceID
	FLOAT fSceneSpeechPhase
	IF SETUP_TIMETABLE_SPEECH_FOR_SCENE(sTimetableScene.sScene.eScene,
			tPlayerSceneVoiceID, tBuddySceneVoiceID, tSpeechBlock, tSpeechLabel, fSceneSpeechPhase)
		
		IF (fSceneSpeechPhase <> 0.0)
			IF (fSceneSpeechPhase > 0.0)
				
				TEXT_LABEL_63 tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut
				ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
				//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut,
						playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneAnimProgress)
					FLOAT fPlayer_anim_current_time
					IF Get_Ped_Anim_Or_SynchedScene_Phase(PLAYER_PED_ID(), tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut, fPlayer_anim_current_time)
						//
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> fSceneSpeechPhase: ", fSceneSpeechPhase, ", fPlayer_anim_current_time: ", fPlayer_anim_current_time)
						#ENDIF
						
						IF fPlayer_anim_current_time < 0.0
							RETURN FALSE
						ENDIF
						IF fPlayer_anim_current_time < fSceneSpeechPhase
							RETURN FALSE
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		ar_ADD_PED_FOR_DIALOGUE(tPlayerSceneVoiceID, tBuddySceneVoiceID)
		
		IF (sTimetableScene.eOutTask <> SCRIPT_TASK_USE_MOBILE_PHONE)
			
			IF bHave_Preloaded_Conversation
				BEGIN_PRELOADED_CONVERSATION()
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> BEGIN_PRELOADED_CONVERSATION(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				#ENDIF
				
				RETURN TRUE
			ELIF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				
				START_PRELOADED_CONVERSATION()
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> UNPAUSE_FACE_TO_FACE_CONVERSATION(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				#ENDIF
				
				RETURN TRUE
			ELSE
				IF CREATE_CONVERSATION(MyLocalPedStruct, tSpeechBlock, tSpeechLabel, CONV_PRIORITY_AMBIENT_HIGH)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> CREATE_CONVERSATION(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
					#ENDIF
					
					IF sTimetableScene.sScene.eScene = PR_SCENE_T_FIGHTBAR_a
					OR sTimetableScene.sScene.eScene = PR_SCENE_T_FIGHTYAUCLUB_b
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,1,0>>),
								PEDMOVE_WALK)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_CELLPHONE_CONVERSATION_PAUSED()
				PAUSE_CELLPHONE_CONVERSATION(FALSE)
				
				START_PRELOADED_CONVERSATION()
				bDisableWhileWaiting = FALSE
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> UNPAUSE_PLAYER_CALL_CHAR_CELLPHONE(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
				#ENDIF
				
				RETURN TRUE
			ELSE
				IF (g_Cellphone.PhoneDS = PDS_DISABLED)
					DISABLE_CELLPHONE(FALSE)
					bKeepCellphoneDisabled = FALSE
				ENDIF
				IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LAMAR, tSpeechBlock, tSpeechLabel, CONV_PRIORITY_AMBIENT_HIGH)
					bDisableWhileWaiting = FALSE
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> PLAYER_CALL_CHAR_CELLPHONE(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "<player_timetable_scene> failed to create conversation(\"", tSpeechBlock, "\", \"", tSpeechLabel, "\")	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
		#ENDIF
		
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL NOT_ProgressOutOfEstabShotEarly(PED_REQUEST_SCENE_ENUM eScene)
	
	FLOAT fDistToProgress = 0.0
	
	SWITCH eScene
		CASE PR_SCENE_M2_SAVEHOUSE1_b		fDistToProgress = 6.5 BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a		fDistToProgress = 5.0 BREAK
		CASE PR_SCENE_T_FIGHTBAR_a			fDistToProgress = 5.0 BREAK
		CASE PR_SCENE_T_FIGHTBAR_b			fDistToProgress = 5.0 BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b		fDistToProgress = 20.0 BREAK
		CASE PR_SCENE_T_FIGHTCASINO			fDistToProgress = 10.0 BREAK
		DEFAULT
			RETURN get_NOT_PlayerHasStartedNewScene()
		BREAK
	ENDSWITCH
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	OR GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
	
		CPRINTLN(DEBUG_SWITCH, "switch not running/short???")
		
		RETURN FALSE
	ENDIF
	
	IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT)
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
		
		FLOAT fCamDist2 = VDIST2(vPlayerCoords, vCamCoords)
		IF fCamDist2 < (fDistToProgress*fDistToProgress)
		
			CPRINTLN(DEBUG_SWITCH, "dist[", SQRT(fCamDist2), "m] < fDistToProgress[", fDistToProgress, "m]")
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			bDisableWhileWaiting = FALSE
			
			RETURN FALSE
		ENDIF
		
		CPRINTLN(DEBUG_SWITCH, "dist[", SQRT(fCamDist2), "m] >= fDistToProgress[", fDistToProgress, "m]")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_SWITCH, "state not estab shot???")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Has_Player_Started_New_Scene()
	INT iDrawSceneRot = 2
	
	IF NOT NOT_ProgressOutOfEstabShotEarly(sTimetableScene.sScene.eScene)
		
		IF DOES_CAM_EXIST(LoopCamIndex)
			IF NOT IS_CAM_RENDERING(LoopCamIndex)
				SET_CAM_ACTIVE(LoopCamIndex, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("LoopCamIndex rendering", iDrawSceneRot, HUD_COLOUR_PURPLE)
				iDrawSceneRot += 1
				#ENDIF
			ENDIF
		ENDIF
		
		
		IF NOT sTimetableScene.bDescentOnly
		
			IF CONTROL_PLAYER_HAVING_A_LIE_IN(sTimetableScene.sScene.eScene)
			
				IF NOT IS_TIMER_STARTED(tLieInTimer)
					START_TIMER_NOW(tLieInTimer)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("player having a lie in", iDrawSceneRot+0, HUD_COLOUR_GREEN)
				DrawLiteralSceneStringTimer("tLieInTimer: ", tLieInTimer, iDrawSceneRot+1, HUD_COLOUR_REDDARK)
				iDrawSceneRot += 2
				#ENDIF
				
				IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
					
					CONST_FLOAT fLIE_IN_TIME_OUT_seconds	7.5		//15.0		//30.0
					IF NOT TIMER_DO_WHEN_READY(tLieInTimer, fLIE_IN_TIME_OUT_seconds)
						
						bWaitingForPlayerLieInInput = TRUE
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF CONTROL_PLAYER_PROCEDURAL_CONV_INSTANT(sTimetableScene.sScene.eScene)
			
			IF NOT bHave_Created_Switch_Scene_Conversation
				IF NOT Create_Switch_Scene_Conversation()
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("waiting to play conversation", iDrawSceneRot+0, HUD_COLOUR_GREEN)
					iDrawSceneRot += 1
					#ENDIF
					
					RETURN FALSE
				ELSE
					bHave_Created_Switch_Scene_Conversation = TRUE
				ENDIF
			ENDIF
			
//			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			
//				#IF IS_DEBUG_BUILD
//				DrawLiteralSceneString("player on the phone", iDrawSceneRot+0, HUD_COLOUR_GREEN)
//				iDrawSceneRot += 1
//				#ENDIF
//				
//				bDisableWhileWaiting = FALSE
//				RETURN FALSE
//			ELSE
//				//
//			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camID)
			DrawLiteralSceneStringFloat("camID phase:",
					GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camID), iDrawSceneRot, HUD_COLOUR_GREEN)
			iDrawSceneRot++
		ELSE
			DrawLiteralSceneString("camID phase off", iDrawSceneRot, HUD_COLOUR_REDLIGHT)
			iDrawSceneRot++
		ENDIF
		
//		IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camTo)
//			DrawLiteralSceneStringFloat("camTo phase:",
//					GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camTo), iDrawSceneRot, HUD_COLOUR_GREEN)
//			iDrawSceneRot++
//		ELSE
//			DrawLiteralSceneString("camTo phase off", iDrawSceneRot, HUD_COLOUR_REDLIGHT)
//			iDrawSceneRot++
//		ENDIF
		#ENDIF
		
//		IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camTo)
//			CPRINTLN(DEBUG_SWITCH, "camTo phase:")
//			CPRINTLN(DEBUG_SWITCH, GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camTo))
//			
//			IF GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camTo) < 0.8
//				
//				#IF IS_DEBUG_BUILD
//				DrawLiteralSceneString("camTo phase off", iDrawSceneRot, HUD_COLOUR_REDLIGHT)
//				iDrawSceneRot++
//				#ENDIF
//				
//				//
//				RETURN FALSE
//			ENDIF
//			
//		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			IF bUpdatedWalkTasks
				VECTOR vOccupiedBounds = <<0.5,0.5,1.0>>
				IF IS_AREA_OCCUPIED(sTimetableScene.sScene.vCreateCoords-vOccupiedBounds,
						sTimetableScene.sScene.vCreateCoords+vOccupiedBounds,
						FALSE, TRUE, FALSE, TRUE, FALSE, PLAYER_PED_ID())
					VECTOR vSafeCoord
					IF GET_SAFE_COORD_FOR_PED(sTimetableScene.sScene.vCreateCoords, FALSE, vSafeCoord)
						sTimetableScene.sScene.vCreateCoords = vSafeCoord
						
						CPRINTLN(DEBUG_SWITCH, "GET_SAFE_COORD_FOR_PED")
					ENDIF
				ENDIF
			ENDIF
			
			Control_Player_Out_Tasks()
			//Reset savehouse blips
			INT iSavehouse
      		REPEAT ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS) iSavehouse
            	Update_Savehouse_Respawn_Garage(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
				Update_Savehouse_Respawn_Blip(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
			ENDREPEAT
		ENDIF
		
		IF NOT IS_PED_INJURED(scene_buddy)
			Control_Buddy_Out_Tasks()
		ENDIF
		
		//removed for #470174
//		FLOAT gameplayCamPitch, gameplayCamSmoothRate
//		FLOAT gameplayCamHeading
//		IF SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sTimetableScene.sScene.eScene,
//				gameplayCamPitch, gameplayCamSmoothRate, gameplayCamHeading)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(gameplayCamPitch, gameplayCamSmoothRate)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(gameplayCamHeading)
//		ENDIF
		
		IF NOT IS_ENTITY_DEAD(scene_veh)
			ar_FREEZE_ENTITY_POSITION(scene_veh, FALSE, "Has_Player_Started_New_Scene")
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(scene_veh, FALSE)
			
			IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(scene_veh))
				IF (fSceneVeh_driveSpeed = 0.0)
				
					SET_ENTITY_DYNAMIC(scene_veh, TRUE)
				
					IF CAN_ANCHOR_BOAT_HERE(scene_veh)
						SET_BOAT_ANCHOR(scene_veh, TRUE)
						CPRINTLN(DEBUG_SWITCH, "SET_BOAT_ANCHOR(scene_veh, TRUE)")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		RETURN TRUE
	ELSE
		
		IF (sTimetableScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
			SET_ENTITY_VISIBLE(player_door_r, FALSE)
		ENDIF
		
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
		
		FLOAT fCamDist2 = VDIST2(vPlayerCoords, vCamCoords)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("PlayerHasNotStartedNewScene",
				iDrawSceneRot, HUD_COLOUR_GREEN)
		iDrawSceneRot++
		
		DrawLiteralSceneStringFloat("fCamDist: ", SQRT(fCamDist2),
				iDrawSceneRot, HUD_COLOUR_GREENLIGHT)
		iDrawSceneRot++
		
		IF GET_PLAYER_SWITCH_INTERP_OUT_DURATION() > 0
			TEXT_LABEL_63 str = "duration: "
			str += GET_PLAYER_SWITCH_INTERP_OUT_DURATION()
			str += ", time: "
			str += GET_PLAYER_SWITCH_INTERP_OUT_CURRENT_TIME()
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_GREEN)
			iDrawSceneRot++
		ENDIF
		
		DrawLiteralSceneString(Get_String_From_Script_Task_Name(sTimetableScene.eLoopTask),
				iDrawSceneRot, HUD_COLOUR_GREENLIGHT)
		iDrawSceneRot += 2
		#ENDIF
		
		IF (sTimetableScene.eLoopTask = SCRIPT_TASK_STAND_STILL)
			IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
				IF fCamDist2 < (15*15)
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "descent-goto")
				ENDIF
			ENDIF
			IF IS_PED_FALLING(PLAYER_PED_ID())
				CPRINTLN(DEBUG_SWITCH, "player is falling while trying to stand still!!!")
				//SCRIPT_ASSERT("player is falling while trying to stand still!!!")
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), sTimetableScene.sScene.vCreateCoords)
			ELIF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID()) 
				CPRINTLN(DEBUG_SWITCH, "player in parachute freefall falling while trying to stand still!!!")
				//SCRIPT_ASSERT("player in parachute freefall falling while trying to stand still!!!")
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), sTimetableScene.sScene.vCreateCoords)
			ELSE
				CDEBUG3LN(DEBUG_SWITCH, "player isn't falling while standing still")
			ENDIF
		ENDIF
		IF (sTimetableScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD)	//1367736
			IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
				IF fCamDist2 < (15*15)
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "descent-goto")
				ENDIF
			ENDIF
		ENDIF
		IF (sTimetableScene.eLoopTask = SCRIPT_TASK_WANDER_STANDARD)		//1578716
			IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
				IF NOT bRetaskToWander
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "descent-wander")
					TASK_WANDER_STANDARD(PLAYER_PED_ID())
					bRetaskToWander = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF (sTimetableScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
		AND IS_VEHICLE_DRIVEABLE(scene_veh)
			IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
				IF NOT bRetaskToWander
					Get_Base_Speed_For_Drive_To_Coord(vSceneVeh_driveOffset, fSceneVeh_driveSpeed)
				
					FLOAT fSceneVeh_driveSpeedLoop
					fSceneVeh_driveSpeedLoop = fSceneVeh_driveSpeed
					
					IF (sTimetableScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
						fSceneVeh_driveSpeedLoop *= 0.5
					ENDIF
					
					CPRINTLN(DEBUG_SWITCH, "Has_Player_Started_New_Scene(fSceneVeh_driveSpeedLoop:", fSceneVeh_driveSpeedLoop, ")")
					
					ar_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE, "descent-wander")
					
					SEQUENCE_INDEX siseq
					OPEN_SEQUENCE_TASK(siseq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, scene_veh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset*1.5), fSceneVeh_driveSpeedLoop, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(scene_veh), DRIVINGMODE_AVOIDCARS, 4.0, 30)
						TASK_VEHICLE_DRIVE_WANDER(NULL, scene_veh, fSceneVeh_driveSpeedLoop, DRIVINGMODE_AVOIDCARS)
					CLOSE_SEQUENCE_TASK(siseq)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siseq)
					CLEAR_SEQUENCE_TASK(siseq)
					
					ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeedLoop)
					
					bRetaskToWander = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camID)
		DrawLiteralSceneStringFloat("spline cam phase:",
				GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camID),
				iDrawSceneRot, HUD_COLOUR_GREEN)
		iDrawSceneRot++
		
		
	ENDIF
	#ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(scene_veh)
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		AND GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
		AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
			IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() > 0
			//	CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(scene_veh), 10.0)
			ELSE
				IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
					IF NOT bSwitchSkippedPlaybackTimeDescent
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
							IF (fRecordingSkip >= 0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(scene_veh, fRecordingSkip - GET_TIME_POSITION_IN_RECORDING(scene_veh))
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(scene_veh)
								
								#IF IS_DEBUG_BUILD
								SAVE_STRING_TO_DEBUG_FILE("position in recording \"")
								SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
								
								IF iRecordingNum < 100	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
								IF iRecordingNum < 10	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
								SAVE_INT_TO_DEBUG_FILE(iRecordingNum)
								
								SAVE_STRING_TO_DEBUG_FILE("\" at time ")
								SAVE_FLOAT_TO_DEBUG_FILE(fRecordingSkip)
								SAVE_STRING_TO_DEBUG_FILE(" / ")
								SAVE_FLOAT_TO_DEBUG_FILE(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNum, tRecordingName))
								SAVE_STRING_TO_DEBUG_FILE(": ")
								SAVE_VECTOR_TO_DEBUG_FILE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum, fRecordingSkip, tRecordingName))
								SAVE_STRING_TO_DEBUG_FILE("	//")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								#ENDIF
							ENDIF
						ELSE
							//
							
							PED_VEH_DATA_STRUCT sVehData    //MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
							VECTOR vVehCoordOffset = <<0,0,0>>
							FLOAT fVehHeadOffset = 0
							
							IF GET_PLAYER_VEH_POSITION_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene,
									sVehData, vVehCoordOffset, fVehHeadOffset,
									vSceneVeh_driveOffset, fSceneVeh_driveSpeed)

								TEXT_LABEL_63 tPlayerSceneSyncAnimVehExit
								IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncAnimVehExit)
									//dont reposition - synch scene anim played back
								ELSE
									SET_ENTITY_COORDS(scene_veh, sTimetableScene.sScene.vCreateCoords+vVehCoordOffset)
									SET_ENTITY_HEADING(scene_veh, sTimetableScene.sScene.fCreateHead+fVehHeadOffset)
									ar_SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
									
									ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed)
									
									#IF IS_DEBUG_BUILD
									VECTOR vRepos = sTimetableScene.sScene.vCreateCoords+vVehCoordOffset
									CPRINTLN(DEBUG_SWITCH, "reposition scene_veh \"", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scene_veh)), "\"	to ", vRepos, "	// ", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						bSwitchSkippedPlaybackTimeDescent = TRUE
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
						SET_PLAYBACK_SPEED(scene_veh, fSpeedSwitch)
					ENDIF
				ELSE
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
						// Restore full playback speed
						SET_PLAYBACK_SPEED(scene_veh, fSpeedExit)
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
		
		IF bSwitchSkippedPlaybackTimeDescent
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
			AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_HOLD
				
				IF NOT bSwitchSkippedPlaybackTimeHold
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
						IF (fRecordingSkip >= 0)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(scene_veh, fRecordingSkip - GET_TIME_POSITION_IN_RECORDING(scene_veh))
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(scene_veh)
							
							#IF IS_DEBUG_BUILD
							SAVE_STRING_TO_DEBUG_FILE("position in recording \"")
							SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
							
							IF iRecordingNum < 100	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
							IF iRecordingNum < 10	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
							SAVE_INT_TO_DEBUG_FILE(iRecordingNum)
							
							SAVE_STRING_TO_DEBUG_FILE("\" at time ")
							SAVE_FLOAT_TO_DEBUG_FILE(fRecordingSkip)
							SAVE_STRING_TO_DEBUG_FILE(" / ")
							SAVE_FLOAT_TO_DEBUG_FILE(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNum, tRecordingName))
							SAVE_STRING_TO_DEBUG_FILE(": ")
							SAVE_VECTOR_TO_DEBUG_FILE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum, fRecordingSkip, tRecordingName))
							SAVE_STRING_TO_DEBUG_FILE("	//")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
						ENDIF
					ELSE
						//
						
						PED_VEH_DATA_STRUCT sVehData    //MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
						VECTOR vVehCoordOffset = <<0,0,0>>
						FLOAT fVehHeadOffset = 0
						
						IF GET_PLAYER_VEH_POSITION_FOR_SCENE(sTimetableScene.sScene.ePed, sTimetableScene.sScene.eScene,
								sVehData, vVehCoordOffset, fVehHeadOffset,
								vSceneVeh_driveOffset, fSceneVeh_driveSpeed)

							TEXT_LABEL_63 tPlayerSceneSyncAnimVehExit
							IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncAnimVehExit)
								//dont reposition - synch scene anim played back
							ELSE
								SET_ENTITY_COORDS(scene_veh, sTimetableScene.sScene.vCreateCoords+vVehCoordOffset)
								SET_ENTITY_HEADING(scene_veh, sTimetableScene.sScene.fCreateHead+fVehHeadOffset)
								ar_SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
								
								ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, fSceneVeh_driveSpeed)
								
								#IF IS_DEBUG_BUILD
								VECTOR vRepos = sTimetableScene.sScene.vCreateCoords+vVehCoordOffset
								CPRINTLN(DEBUG_SWITCH, "reposition scene_veh \"", SAFE_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scene_veh)), "\"	to ", vRepos, "	// ", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					bSwitchSkippedPlaybackTimeHold = TRUE
				ENDIF
			ENDIF
		
		ENDIF
			
			
		
		
		TEXT_LABEL_63 tPlayerSceneSyncAnimVehLoop
		IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(sTimetableScene.sScene.eScene,
				tPlayerSceneSyncAnimVehLoop)
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(scene_veh)
			REQUEST_VEHICLE_DIAL(scene_veh)
		ENDIF
	ENDIF
	
	IF (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_CHECKSHOE)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_WIPERIGHT)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_WIPEHANDS)
//		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F))
//			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F), prop_com_gar_door_01, << 483.56, -1316.08, 32.18 >>)
//			
//			CPRINTLN(DEBUG_SWITCH, "ADD_DOOR_TO_SYSTEM...")
//			
//		ELSE
//			
//			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F), 1.0, DEFAULT, TRUE)
//			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F), DOORSTATE_FORCE_OPEN_THIS_FRAME, DEFAULT, TRUE)
//			
//			CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_DOOR_STATE(CARSTEAL_GARAGE_F)")
//			
//		ENDIF
//		
//		
//		IF NOT IS_BIT_SET(g_sAutoDoorData[AUTODOOR_DEVIN_GATE_L].settingsBitset, BIT_AUTODOOR_LOCK_OPEN)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_DEVIN_GATE_L, TRUE)
//			
//			CPRINTLN(DEBUG_SWITCH, "FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(L)...")
//		ENDIF
//		IF NOT IS_BIT_SET(g_sAutoDoorData[AUTODOOR_DEVIN_GATE_R].settingsBitset, BIT_AUTODOOR_LOCK_OPEN)
//			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_DEVIN_GATE_R, TRUE)
//			
//			CPRINTLN(DEBUG_SWITCH, "FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(R)...")
//		ENDIF
//		
//		IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_HAYES_GARAGE, PLAYER_PED_ID())
//			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_HAYES_GARAGE, PLAYER_PED_ID())
//			
//			CPRINTLN(DEBUG_SWITCH, "REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR...")
//		ELSE
//			
//			CPRINTLN(DEBUG_SWITCH, "PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR...")
//			
//		ENDIF
		
		
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M7_KIDS_GAMING)
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.8,174.0,76.9>>)
			
			CPRINTLN(DEBUG_SWITCH, "ADD_DOOR_TO_SYSTEM...")
			
		ELSE
			
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1, DEFAULT, TRUE)
			
			CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_DOOR_STATE(M_MANSION_SON)")
			
		ENDIF
//	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_Ma_MICHAEL2)
//		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM))
//			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), V_ILEV_MM_DOORW, <<-805.0,171.9,76.9>>)
//			
//			CPRINTLN(DEBUG_SWITCH, "ADD_DOOR_TO_SYSTEM...")
//			
//		ELSE
//			
//			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
//			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), 1, DEFAULT, TRUE)
//			
//			CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_""SET_DOOR_STATE...")
//			
//		ENDIF
//	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_F_MD_KUSH_DOC)
//		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM))
//			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), vb_30_shutters, <<-1168.404,-1567.186,4.843>>)
//			
//			CPRINTLN(DEBUG_SWITCH, "ADD_DOOR_TO_SYSTEM...")
//			
//		ELSE
//			
//			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
//			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM), 1, DEFAULT, TRUE)
//			
//			CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_""SET_DOOR_STATE...")
//			
//		ENDIF
		
		
	ENDIF
	
	IF (sTimetableScene.sScene.eScene = PR_SCENE_M2_ARGUEWITHWIFE)	//#1524857
		
		IF NOT bHave_Created_Estab_Shot_Conversation
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
		
			FLOAT fCamDist2 = VDIST2(vPlayerCoords, vCamCoords)
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT
			AND (fCamDist2 < (40*40))
				/*
				
				Hey Alwyn,

				I've updated these roots:

				MICS1_IG_18a
				MICS1_IG_18b
				MICS1_IG_18c

				Thanks. 
				
				*/
				
				TEXT_LABEL tPlayerSceneVoiceID, tBuddySceneVoiceID
				FLOAT fSceneSpeechPhase
				IF SETUP_TIMETABLE_SPEECH_FOR_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneVoiceID, tBuddySceneVoiceID, tSpeechBlock, tSpeechLabel, fSceneSpeechPhase)
						
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						enumCharacterList ePlayerPed = GET_CURRENT_PLAYER_PED_ENUM()
						INT iPlayerSceneVoiceNum = ENUM_TO_INT(ePlayerPed)
						
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iPlayerSceneVoiceNum, PLAYER_PED_ID(), tPlayerSceneVoiceID)
					ENDIF
					INT iBuddySceneVoiceNum = 4
					IF NOT IS_PED_INJURED(scene_buddy)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iBuddySceneVoiceNum, scene_buddy, tBuddySceneVoiceID)
					ELIF NOT IS_STRING_NULL_OR_EMPTY(tBuddySceneVoiceID)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, iBuddySceneVoiceNum, NULL, tBuddySceneVoiceID)
					ENDIF
					
					IF CREATE_CONVERSATION(MyLocalPedStruct, tSpeechBlock, "MICS1_IG_18R", CONV_PRIORITY_AMBIENT_HIGH)
						bHave_Created_Estab_Shot_Conversation = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
		
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M7_ROUNDTABLE)		//done elsewhere...
	
	ELIF (sTimetableScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE)	//#1553855
		IF NOT bHave_Preloaded_Conversation
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
		
			FLOAT fCamDist2 = VDIST2(vPlayerCoords, vCamCoords)
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT
			AND (fCamDist2 < (40*40))
			
				
				TEXT_LABEL tPlayerSceneVoiceID, tBuddySceneVoiceID
				FLOAT fSceneSpeechPhase
				IF SETUP_TIMETABLE_SPEECH_FOR_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneVoiceID, tBuddySceneVoiceID, tSpeechBlock, tSpeechLabel, fSceneSpeechPhase)
					
					ar_ADD_PED_FOR_DIALOGUE(tPlayerSceneVoiceID, tBuddySceneVoiceID)
					IF PRELOAD_CONVERSATION(MyLocalPedStruct, tSpeechBlock, tSpeechLabel, CONV_PRIORITY_AMBIENT_HIGH)
						
//						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						
						CPRINTLN(DEBUG_SWITCH, "PRELOAD_CONVERSATION(", tSpeechBlock, ", ", tSpeechLabel, ")")
						
						bHave_Preloaded_Conversation = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL InterpBackFromSynchCam(CAMERA_INDEX &camIndex, BOOL bDestroyCamIndex)
	IF DOES_CAM_EXIST(camIndex)
		
		IF IS_CAM_RENDERING(camIndex)
			
			
			FLOAT gameplayCamPitch, gameplayCamSmoothRate
			FLOAT gameplayCamHeading
			IF SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sTimetableScene.sScene.eScene,
					gameplayCamPitch, gameplayCamSmoothRate, gameplayCamHeading)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(gameplayCamPitch, gameplayCamSmoothRate)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(gameplayCamHeading)
			ENDIF
			
		
			FLOAT paramStrength, amplitudeScalar
			BOOL bBlankCountsForPubClubVisit
			IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bBlankCountsForPubClubVisit)
			OR (((sTimetableScene.sScene.eScene = PR_SCENE_F_MD_KUSH_DOC)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_M2_CARSLEEP_a)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_M2_CARSLEEP_b)
			OR (sTimetableScene.sScene.eScene = PR_SCENE_M6_CARSLEEP)) AND (GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON))
				
				VECTOR camIndexCoord = GET_CAM_COORD(camIndex)
				VECTOR gamcamIndexCoord = GET_GAMEPLAY_CAM_COORD()
				FLOAT fCamInterpDist = GET_DISTANCE_BETWEEN_COORDS(camIndexCoord, gamcamIndexCoord)
				
				INT iDrawSceneRot
				IF IsPlayerMovingLeftStick(iDrawSceneRot)
					fCamInterpDist *= 0.2
				ENDIF
				
				INT iCamInterpTime = ROUND(fCamInterpDist*1000)
				
				
				IF (sTimetableScene.sScene.eScene = PR_SCENE_M2_CARSLEEP_a)
				OR (sTimetableScene.sScene.eScene = PR_SCENE_M2_CARSLEEP_b)
				OR (sTimetableScene.sScene.eScene = PR_SCENE_M6_CARSLEEP)
					iCamInterpTime = 0
				ENDIF
				
				RENDER_SCRIPT_CAMS(FALSE, iCamInterpTime != 0, iCamInterpTime, FALSE)
				bDestroyCamIndex = FALSE
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "InterpBackFromSynchCam: RENDER_SCRIPT_CAMS(FALSE, ", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene), ", ", iCamInterpTime, ")")
				#ENDIF
				
				DisableWhileWaiting("interp...")
			ELIF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				
				IF GET_PLAYER_WARDROBE_ANIM_FOR_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
				AND current_player_timetable_scene_stage != PLAYER_HAS_WARDROBE_ANIM
					CPRINTLN(DEBUG_SWITCH, "DON'T InterpBackFromSynchCam: RENDER_SCRIPT_CAMS(", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene), ")		//CAM_VIEW_MODE_FIRST_PERSON, ", Get_String_From_Player_Timetable_Scene_Stage(current_player_timetable_scene_stage))
					RETURN FALSE
				ELSE
					FLOAT fPushinDist = PUSH_IN_DISTANCE
					VECTOR vCamCoord = GET_FINAL_RENDERED_CAM_COORD()
					VECTOR vPlayerCoord = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
					FLOAT fCoordDist3d = GET_DISTANCE_BETWEEN_COORDS(vCamCoord, vPlayerCoord)
					
					CONST_FLOAT fPUSHIN_DIST_BUFFER	0.75
					CONST_FLOAT fPUSHIN_DIST_MIN	0.001
					fPushinDist = CLAMP(fPushinDist, fPUSHIN_DIST_MIN, fCoordDist3d-fPUSHIN_DIST_BUFFER)
					IF fPushinDist < 0
						CWARNINGLN(DEBUG_SWITCH, "fPushinDist is less than zero!! ", fPushinDist)
						fPushinDist  = fPUSHIN_DIST_MIN
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF fPushinDist = PUSH_IN_DISTANCE
						CPRINTLN(DEBUG_SWITCH, "PUSH_IN fCoordDist3d: ", fCoordDist3d, ", fPushinDist: PUSH_IN_DISTANCE	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
					ELIF fPushinDist = fPUSHIN_DIST_MIN
						CPRINTLN(DEBUG_SWITCH, "PUSH_IN fCoordDist3d: ", fCoordDist3d, ", fPushinDist: fPUSHIN_DIST_MIN	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
					ELSE
						CPRINTLN(DEBUG_SWITCH, "PUSH_IN fCoordDist3d: ", fCoordDist3d, ", fPushinDist: ", fPushinDist, "	//", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene))
					ENDIF
					#ENDIF
					FILL_PUSH_IN_DATA(sPushInData, PLAYER_PED_ID(), sTimetableScene.sScene.ePed,
							0.000001,			//FLOAT fDistance = PUSH_IN_DISTANCE
							300,					//INT iInterpTime = PUSH_IN_INTERP_TIME
							300,					//INT iCutTime = PUSH_IN_CUT_TIME
							000,					//INT iPostFXTime = PUSH_IN_POSTFX_TIME
							000,					//INT iSpeedUpTime = 0
							DEFAULT)				// FLOAT fSpeedUpProportion = PUSH_IN_SPEED_UP_PROPORTION)
					bFilledPushInData = TRUE
					bDestroyCamIndex = FALSE
					
					// rob - short-circuit the creation of push-in cams to keep the original cam moving round till the cut
					IF NOT bShortCircuitedPushin
						sPushInData.state = PUSH_IN_RUNNING
						sPushInData.iCreateTime = GET_GAME_TIMER()
						sPushInData.bDonePostFX = FALSE		
						bShortCircuitedPushin = TRUE
					ENDIF
					
					VECTOR vDirectionMod
					IF SET_PUSH_IN_DIRECTION_MODIFIER_FOR_SCENE(sTimetableScene.sScene.eScene, vDirectionMod)
						CPRINTLN(DEBUG_SWITCH, "SET_PUSH_IN_DIRECTION_MODIFIER(", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene), ", ", vDirectionMod, ")")
						
						SET_PUSH_IN_DIRECTION_MODIFIER(sPushInData, vDirectionMod)
					ENDIF
					
					CPRINTLN(DEBUG_SWITCH, "InterpBackFromSynchCam: FILL_PUSH_IN_DATA(", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene), ")		//CAM_VIEW_MODE_FIRST_PERSON, ", Get_String_From_Player_Timetable_Scene_Stage(current_player_timetable_scene_stage))
				ENDIF
			ELSE
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()	
				CPRINTLN(DEBUG_SWITCH, "InterpBackFromSynchCam: STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(", Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene), ")")
			ENDIF
			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		IF bDestroyCamIndex
			DESTROY_CAM(camIndex)
		ENDIF		
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL BlendOutOfSynchTask(PED_REQUEST_SCENE_ENUM ePedScene, STRING sReason)
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK
		
		bWaitingForPlayerLieInInput = TRUE
		sReason = sReason
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "BlendOutOfSynchTask(", Get_String_From_Ped_Request_Scene_Enum(ePedScene), ", \"", sReason, "\")")
	#ENDIF
	
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	
	INT iSIMULATE_PLAYER_INPUT_GAIT = -1
	IF BLENDOUT_OF_SYNCH_TASK_FOR_TIMETABLE_SCENE(ePedScene, iSIMULATE_PLAYER_INPUT_GAIT)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "GET_SYNCHRONIZED_SCENE_PHASE(player_ped_id exit) = ")
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			CPRINTLN(DEBUG_SWITCH, GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID))
		ELSE
			CPRINTLN(DEBUG_SWITCH, "FINISHED")
		ENDIF
		#ENDIF
		
		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
		SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, iSIMULATE_PLAYER_INPUT_GAIT)
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	ELSE
		//
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(scene_buddy)
		SWITCH sTimetableScene.sScene.eScene
			CASE PR_SCENE_F_LAMTAUNT_P1
			CASE PR_SCENE_F_LAMTAUNT_P3
			CASE PR_SCENE_F_LAMTAUNT_P5
			CASE PR_SCENE_F_LAMTAUNT_NIGHT
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), scene_buddy, 2500)
			BREAK
		ENDSWITCH
		
	ENDIF
	
	SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
	bDisableWhileWaiting = FALSE
	
	WAIT(0)
	sReason = sReason
	RETURN TRUE
ENDFUNC

FUNC BOOL Has_Player_Finished_Out_Tasks_Finished()
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	INT iDrawSceneRot = 2
	
	#IF IS_DEBUG_BUILD
	IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camID)
//		DrawLiteralSceneStringFloat("camID phase:",
//				GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camID), iDrawSceneRot, HUD_COLOUR_GREEN)
//		iDrawSceneRot++
	ELSE
//		DrawLiteralSceneString("camID phase off", iDrawSceneRot, HUD_COLOUR_REDLIGHT)
//		iDrawSceneRot++
	ENDIF
	
	IF DOES_CAM_EXIST(sTimetableScene.sScene.sSelectorCam.camTo)
//		DrawLiteralSceneStringFloat("camTo phase:",
//				GET_CAM_SPLINE_PHASE(sTimetableScene.sScene.sSelectorCam.camTo), iDrawSceneRot, HUD_COLOUR_GREEN)
//		iDrawSceneRot++
	ELSE
//		DrawLiteralSceneString("camTo phase off", iDrawSceneRot, HUD_COLOUR_REDLIGHT)
//		iDrawSceneRot++
	ENDIF
	#ENDIF
	
	PED_COMPONENT ComponentID[3]
	INT DrawableID[3], TextureID[3]
	
	
			
	IF IS_TIMER_STARTED(tLieInTimer)
		IF TIMER_DO_WHEN_READY(tLieInTimer, 90.0)
			CPRINTLN(DEBUG_SWITCH,"super duper emergency exit")
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	SWITCH sTimetableScene.eOutTask
		CASE SCRIPT_TASK_PLAY_ANIM
			
			
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				START_TIMER_NOW(tLieInTimer)
			ENDIF
			
			TEXT_LABEL_63 tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
			ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
			//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneAnimProgress)
				#IF IS_DEBUG_BUILD
				str = ("finishOut PLAY_ANIM \"")
				
				INT iPlayerSceneAnimOut
				iPlayerSceneAnimOut = GET_LENGTH_OF_LITERAL_STRING(tPlayerSceneAnimOut)
				
				CONST_INT iMAX_ANIM_OUT_WIDGET_LENGTH	32
				IF (iPlayerSceneAnimOut) <= iMAX_ANIM_OUT_WIDGET_LENGTH
					str += (tPlayerSceneAnimOut)
				ELSE
					str += GET_STRING_FROM_STRING(tPlayerSceneAnimOut,
							(iPlayerSceneAnimOut) - iMAX_ANIM_OUT_WIDGET_LENGTH,
							GET_LENGTH_OF_LITERAL_STRING(tPlayerSceneAnimOut))
				ENDIF
				
				
				str += ("\"")
							
				#ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
					FLOAT fPlayerSceneAnimOutTime
					fPlayerSceneAnimOutTime = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
					
					#IF IS_DEBUG_BUILD
					str += (": ")
					str += GET_STRING_FROM_FLOAT(fPlayerSceneAnimOutTime)
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
					iDrawSceneRot += 2
					#ENDIF
					
					FLOAT fPlayerSceneAnimUpdateComponentTime
					IF GetPlayerAnimComponentForTimetableScene(sTimetableScene.sScene.eScene, fPlayerSceneAnimUpdateComponentTime,
							ComponentID, DrawableID, TextureID)
						
						IF (GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ComponentID[0]) <> DrawableID[0])
						OR (GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ComponentID[0]) <> TextureID[0])
							
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneString("update ped variation", iDrawSceneRot, HUD_COLOUR_blue)
							iDrawSceneRot += 1
							#ENDIF
							
							IF NOT bStartPreloadPlayerComponentForScene
								SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[0], DrawableID[0], TextureID[0])
								SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[1], DrawableID[1], TextureID[1])
								SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[2], DrawableID[2], TextureID[2])
								bStartPreloadPlayerComponentForScene = TRUE
							ENDIF
							
							IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene, "TrousersUp",
									fPlayerSceneAnimOutTime,
									tPlayerSceneAnimDict, tPlayerSceneAnimOut,
									iDrawSceneRot)
							OR fPlayerSceneAnimOutTime > fPlayerSceneAnimUpdateComponentTime
								IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())

									SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[0], DrawableID[0], TextureID[0])
									SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[1], DrawableID[1], TextureID[1])
									SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[2], DrawableID[2], TextureID[2])
									RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene,
							"WalkInterruptible", fPlayerSceneAnimOutTime,
							tPlayerSceneAnimDict, tPlayerSceneAnimOut,
							iDrawSceneRot)
					OR HasAnimEventPassed("WalkInterruptible", fPlayerSceneAnimOutTime,
							tPlayerSceneAnimDict, tPlayerSceneAnimOut,
							iDrawSceneRot)
						IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
							//
						ELSE
							
							IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = PERFORMING_TASK)
								STOP_ANIM_PLAYBACK(PLAYER_PED_ID())
							ENDIF
							
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							RETURN TRUE
						ENDIF
						
					ENDIF
					
					IF CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
						IF fPlayerSceneAnimOutTime > 0.95
//							STOP_ANIM_PLAYBACK(PLAYER_PED_ID())
							
//							CLEAR_PED_TASKS(PLAYER_PED_ID())
							CPRINTLN(DEBUG_SWITCH,"crossfade phonecall anims")
							
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					
					#IF IS_DEBUG_BUILD
					str += (" FINISHED")
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_RED)
					iDrawSceneRot++
					#ENDIF
					
					
				ENDIF	
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str2
					str2 += (" * ")
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						str2 += ("PLAYER IS IN ANY VEHICLE, ")
					else
						str2 += ("player isn't in any vehicle, ")
					endif
					if is_ped_sitting_in_any_vehicle(player_ped_id())
						str2 += ("IS SITTING IN ANY VEHICLE")
					else
						str2 += ("isn't sitting in any vehicle, ")
					endif
					
					iDrawSceneRot++
					DrawLiteralSceneString(str2, iDrawSceneRot, HUD_COLOUR_BLUELIGHT)
					iDrawSceneRot++
					#ENDIF
					
					IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
					AND NOT IsPlayerUsingAccelerateOrBrake(scene_veh, iDrawSceneRot)
						//
					ELSE
						IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = PERFORMING_TASK)
							STOP_ANIM_PLAYBACK(PLAYER_PED_ID())
						ENDIF
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						RETURN TRUE
					ENDIF
					
				ENDIF
				
				IF DOES_CAM_EXIST(ExitCamIndex)
					IF NOT IS_CAM_RENDERING(ExitCamIndex)
						#IF IS_DEBUG_BUILD
						iDrawSceneRot++
						DrawLiteralSceneString("ExitCamIndex exists", iDrawSceneRot, HUD_COLOUR_PURPLE)
						iDrawSceneRot++
						#ENDIF
					ELSE
						
//						FLOAT ExitCamSplinePhase
//						ExitCamSplinePhase = GET_CAM_SPLINE_PHASE(ExitCamIndex)
						
						FLOAT paramStrength, amplitudeScalar
						BOOL bBlankCountsForPubClubVisit
						IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bBlankCountsForPubClubVisit)
							
							SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(amplitudeScalar)
							SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(amplitudeScalar)
							
							SET_CAM_MOTION_BLUR_STRENGTH(ExitCamIndex, paramStrength)
						
							IF NOT (g_drunkCameraActive)
								Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
							ELSE
								Update_Drunk_Camera_ParamTime(iCONST_DRUNK_secDuration)
								Update_Drunk_Camera_ParamStrength(paramStrength*3.0)
								Update_Drunk_Camera_ParamDesiredAmplitude(amplitudeScalar*3.0)
							ENDIF
							
							#IF IS_DEBUG_BUILD
							IF g_bDrawLiteralSceneString
							g_bDrawDebugDrunkInfo = TRUE
							ENDIF
							
							iDrawSceneRot++
							DrawLiteralSceneString("ExitCamIndex drunk", /*ExitCamSplinePhase,*/ iDrawSceneRot, HUD_COLOUR_PURPLE)
							iDrawSceneRot++
							#ENDIF
							
						ELSE
							
							#IF IS_DEBUG_BUILD
							iDrawSceneRot++
							DrawLiteralSceneString("ExitCamIndex rendering", /*ExitCamSplinePhase,*/ iDrawSceneRot, HUD_COLOUR_PURPLE)
							iDrawSceneRot++
							#ENDIF
						ENDIF
						
//						IF ExitCamSplinePhase <= 0.99
//							
//						ELSE
//							InterpBackFromSynchCam(ExitCamIndex, TRUE)
//							RETURN TRUE
//						ENDIF
					ENDIF
				ELSE
					
					#IF IS_DEBUG_BUILD
					iDrawSceneRot++
					DrawLiteralSceneString("ExitCamIndex DOESNT exist", iDrawSceneRot, HUD_COLOUR_PURPLE, 0.5)
					iDrawSceneRot++
					#ENDIF
				ENDIF
				FLOAT paramStrength, amplitudeScalar
				BOOL bCountsForPubClubVisit
				IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bCountsForPubClubVisit)

				
		//			SAVE_STRING_TO_DEBUG_FILE("SHAKE_PLAYER_TIMETABLE_DRUNK_CAM D")
		//			SAVE_NEWLINE_TO_DEBUG_FILE()
					
					#IF IS_DEBUG_BUILD
					IF g_bDrawLiteralSceneString
					g_bDrawDebugDrunkInfo = TRUE
					ENDIF
					#ENDIF
					
					IF NOT (g_drunkCameraActive)
						Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
					ELSE
						Update_Drunk_Camera_ParamTime(iCONST_DRUNK_secDuration)
						Update_Drunk_Camera_ParamStrength(paramStrength*3.0)
						Update_Drunk_Camera_ParamDesiredAmplitude(amplitudeScalar*3.0)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
//					STOP_ANIM_PLAYBACK(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					IF DOES_CAM_EXIST(ExitCamIndex)
						IF IS_CAM_RENDERING(ExitCamIndex)
							InterpBackFromSynchCam(ExitCamIndex, TRUE)
						ENDIF
					ENDIF
					
					RETURN TRUE
				ENDIF
			
				
			ENDIF
			
			IF TIMER_DO_WHEN_READY(tLieInTimer, 30.0)
				CPRINTLN(DEBUG_SWITCH, "emergency exit for play anim")
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
		
			FLOAT fPlayerSceneAnimOutTime
			
			#IF IS_DEBUG_BUILD
			str = ("finishOut SYNC")
			#ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
				fPlayerSceneAnimOutTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
				
				#IF IS_DEBUG_BUILD
				str += (": ")
				str += GET_STRING_FROM_FLOAT(fPlayerSceneAnimOutTime)
				DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
				iDrawSceneRot++
				#ENDIF
				
			ELSE
				fPlayerSceneAnimOutTime = 0.0
				
				#IF IS_DEBUG_BUILD
				str += (" FINISHED")
				DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
				iDrawSceneRot++
				#ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(ExitCamIndex)
				IF NOT IS_CAM_RENDERING(ExitCamIndex)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("ExitCamIndex exists", iDrawSceneRot, HUD_COLOUR_PURPLE)
					iDrawSceneRot++
					#ENDIF
				ELSE
					
					FLOAT paramStrength, amplitudeScalar
					BOOL bBlankCountsForPubClubVisit
					IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bBlankCountsForPubClubVisit)
						
						SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(amplitudeScalar)
						SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(amplitudeScalar)
						
						SET_CAM_MOTION_BLUR_STRENGTH(ExitCamIndex, paramStrength)
						
						IF NOT (g_drunkCameraActive)
							Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
						ELSE
							Update_Drunk_Camera_ParamTime(iCONST_DRUNK_secDuration)
							Update_Drunk_Camera_ParamStrength(paramStrength*3.0)
							Update_Drunk_Camera_ParamDesiredAmplitude(amplitudeScalar*3.0)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF g_bDrawLiteralSceneString
						g_bDrawDebugDrunkInfo = TRUE
						ENDIF
						
						DrawLiteralSceneString("ExitCamIndex drunk", iDrawSceneRot, HUD_COLOUR_PURPLE)
						iDrawSceneRot++
						#ENDIF
						
					ELSE
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("ExitCamIndex rendering", iDrawSceneRot, HUD_COLOUR_PURPLE)
						iDrawSceneRot++
						#ENDIF
					ENDIF
					
					
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("ExitCamIndex DOESNT exist", iDrawSceneRot, HUD_COLOUR_PURPLE)
				iDrawSceneRot++
				#ENDIF
			ENDIF
			
//			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					IF bDisableWhileWaiting
//					AND NOT bFirstPersonCamQuit
//						#IF IS_DEBUG_BUILD
//						DrawLiteralSceneString("Disable 1st ped!", iDrawSceneRot, HUD_COLOUR_YELLOW)
//						iDrawSceneRot++
//						#ENDIF
//					
//						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//					ELSE
//						#IF IS_DEBUG_BUILD
//						DrawLiteralSceneString("Don't disable 1st ped!", iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
//						iDrawSceneRot++
//						#ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					IF bDisableWhileWaiting
//					AND NOT bFirstPersonCamQuit
//						#IF IS_DEBUG_BUILD
//						DrawLiteralSceneString("Disable 1st veh!", iDrawSceneRot, HUD_COLOUR_YELLOW)
//						iDrawSceneRot++
//						#ENDIF
//					
//						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//					ELSE
//						#IF IS_DEBUG_BUILD
//						DrawLiteralSceneString("Don't disable 1st veh!", iDrawSceneRot, HUD_COLOUR_YELLOWDARK)
//						iDrawSceneRot++
//						#ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			TEXT_LABEL_63 tPlayerSyncSceneDict, tPlayerSyncSceneLoop, tPlayerSyncSceneOut
			ANIMATION_FLAGS playerSyncSceneLoopFlag, playerSyncSceneOutFlag
			//enumPlayerSceneAnimProgress ePlayerSceneSyncProgress
			
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSyncSceneDict, tPlayerSyncSceneLoop, tPlayerSyncSceneOut,
					playerSyncSceneLoopFlag, playerSyncSceneOutFlag)	//, ePlayerSceneSyncProgress)
				
				FLOAT fPlayerSceneAnimUpdateComponentTime
				IF GetPlayerAnimComponentForTimetableScene(sTimetableScene.sScene.eScene,
						fPlayerSceneAnimUpdateComponentTime,
						ComponentID, DrawableID, TextureID)
					
					IF (GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ComponentID[0]) <> DrawableID[0])
					OR (GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ComponentID[0]) <> TextureID[0])
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("update ped variation ", fPlayerSceneAnimUpdateComponentTime,
								iDrawSceneRot, HUD_COLOUR_blue)
						iDrawSceneRot += 1
						#ENDIF
						
						IF NOT bStartPreloadPlayerComponentForScene
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[0], DrawableID[0], TextureID[0])
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[1], DrawableID[1], TextureID[1])
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), ComponentID[2], DrawableID[2], TextureID[2])
							bStartPreloadPlayerComponentForScene = TRUE
						ENDIF
						
						IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene, "TrousersUp",
								fPlayerSceneAnimOutTime,
								tPlayerSyncSceneDict, tPlayerSyncSceneOut,
								iDrawSceneRot)
						OR fPlayerSceneAnimOutTime > fPlayerSceneAnimUpdateComponentTime
							IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[0], DrawableID[0], TextureID[0])
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[1], DrawableID[1], TextureID[1])
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ComponentID[2], DrawableID[2], TextureID[2])
								RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
				IF HasQuitCamPhasePassed(sTimetableScene.sScene.eScene, ExitCamIndex,
						fPlayerSceneAnimOutTime,
						iDrawSceneRot)
					InterpBackFromSynchCam(ExitCamIndex, FALSE)
				ENDIF
				
				IF HasFirstPersonQuitCamPhasePassed(sTimetableScene.sScene.eScene, ExitCamIndex,
						fPlayerSceneAnimOutTime,
						iDrawSceneRot)
//					bFirstPersonCamQuit = TRUE
					InterpBackFromSynchCam(ExitCamIndex, FALSE)
				ENDIF
				
				IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene,
						"WalkInterruptible", fPlayerSceneAnimOutTime,
						tPlayerSyncSceneDict, tPlayerSyncSceneOut,
						iDrawSceneRot)
				OR HasAnimEventPassed("WalkInterruptible", fPlayerSceneAnimOutTime,
						tPlayerSyncSceneDict, tPlayerSyncSceneOut,
						iDrawSceneRot)
					IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
					AND NOT HasQuitAnimPhasePassed(sTimetableScene.sScene.eScene,
							fPlayerSceneAnimOutTime,
							iDrawSceneRot)
						//
					ELSE
						BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "exit scene quit/stick")
						
						IF (sTimetableScene.eVehState <> PTVS_0_noVehicle)
							SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
						ENDIF
						
						IF DOES_ENTITY_EXIST(player_prop)
						AND IS_ENTITY_ATTACHED_TO_ENTITY(player_prop, PLAYER_PED_ID())
							SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(player_extra_prop)
						AND IS_ENTITY_ATTACHED_TO_ENTITY(player_extra_prop, PLAYER_PED_ID())
							SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
						ENDIF
						
//						SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
						
						InterpBackFromSynchCam(ExitCamIndex, TRUE)
						
//						FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
//						
//						RETURN TRUE
					ENDIF
					
					IF GET_PLAYER_WARDROBE_ANIM_FOR_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
						
//						BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "wardrobe")
//						
//						IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
//							SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
//						ENDIF
						
//						IF DOES_ENTITY_EXIST(player_prop)
//						AND IS_ENTITY_ATTACHED_TO_ENTITY(player_prop, PLAYER_PED_ID())
//							SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
//						ENDIF
//						IF DOES_ENTITY_EXIST(player_extra_prop)
//						AND IS_ENTITY_ATTACHED_TO_ENTITY(player_extra_prop, PLAYER_PED_ID())
//							SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
//						ENDIF
						
//						SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
//						
//						InterpBackFromSynchCam(ExitCamIndex, TRUE)
//						
//						FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
						
						RETURN TRUE
					ENDIF
					
					IF (sTimetableScene.sScene.eScene = PR_SCENE_F1_NEWHOUSE)
						FLOW_BITSET_IDS eFlowBitsetToQuery 
						INT iOffsetBitIndex 
						
						// Franklin's appartment        
						//Is this bit in our first or second bitset? 
						IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) <= 31 
							eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
							iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) 
						ELSE 
							eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
							iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) - 31 
						ENDIF 

						IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
							BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "wardrobe")
							
							IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
								SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
							ENDIF
							
							IF DOES_ENTITY_EXIST(player_prop)
							AND IS_ENTITY_ATTACHED_TO_ENTITY(player_prop, PLAYER_PED_ID())
								SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(player_extra_prop)
							AND IS_ENTITY_ATTACHED_TO_ENTITY(player_extra_prop, PLAYER_PED_ID())
								SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
							ENDIF
							
							SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
							
							InterpBackFromSynchCam(ExitCamIndex, TRUE)
							
							FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
							
							RETURN TRUE
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				//
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("no anim for scene", iDrawSceneRot)
				iDrawSceneRot++
				#ENDIF
				
			ENDIF
			
			TEXT_LABEL_31 RoomName
			FLOAT fTurnOffTVPhase
			TV_LOCATION eRoomTVLocation
			TVCHANNELTYPE eTVChannelType
			TV_CHANNEL_PLAYLIST eTVPlaylist
			
			IF CONTROL_PLAYER_WATCHING_TV(sTimetableScene.sScene.eScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
				IF (IS_THIS_TV_FORCED_ON(eRoomTVLocation))
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("NOT ForceAmbientTV: ", fTurnOffTVPhase, iDrawSceneRot, HUD_COLOUR_GREENDARK)
					iDrawSceneRot++
					#ENDIF
					
					IF fTurnOffTVPhase > 0
						IF (fPlayerSceneAnimOutTime > fTurnOffTVPhase)
							STOP_AMBIENT_TV_PLAYBACK(eRoomTVLocation)
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("ForceAmbientTV: ", fTurnOffTVPhase, iDrawSceneRot, HUD_COLOUR_GREENLIGHT)
					iDrawSceneRot++
					#ENDIF
				ENDIF
			ENDIF
			
			TEXT_LABEL_63 EmitterName
			FLOAT fTurnOffRadioPhase
			IF SET_SCENE_RADIO_EMITTER_ENABLED(sTimetableScene.sScene.eScene, EmitterName, fTurnOffRadioPhase)
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("NOT ForceAmbientRadio: ", fTurnOffRadioPhase, iDrawSceneRot, HUD_COLOUR_GREENDARK)
				iDrawSceneRot++
				#ENDIF
				
				IF (fPlayerSceneAnimOutTime > fTurnOffRadioPhase)
					SET_STATIC_EMITTER_ENABLED(EmitterName, FALSE)
				ENDIF
			ENDIF
			
			INT Duration, MinFrequency, MaxFrequency
			IF SETUP_SYNCH_SHAKE_EVENT_FOR_SCENE(sTimetableScene.sScene.eScene, Duration, MinFrequency, MaxFrequency)
				
				FLOAT ReturnStartPhase, ReturnEndPhase
				IF FIND_ANIM_EVENT_PHASE(tPlayerSyncSceneDict, tPlayerSyncSceneOut,
						"PadShake",
						ReturnStartPhase, ReturnEndPhase)
					
					IF fPlayerSceneAnimOutTime >= ReturnStartPhase
					AND fPlayerSceneAnimOutTime <= ReturnEndPhase
					
						FLOAT fPadShakeMult
						fPadShakeMult =(fPlayerSceneAnimOutTime-ReturnStartPhase)/ReturnStartPhase
						
						FLOAT fFrequency
						fFrequency = ((MaxFrequency-MinFrequency)*fPadShakeMult)+MinFrequency
						INT Frequency
						Frequency = ROUND(fFrequency)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "SET_CONTROL_SHAKE(PLAYER_CONTROL, ", Duration, ", ", Frequency, ") //", fPadShakeMult, " [", iShakeTimer - GET_GAME_TIMER(), "]")
						#ENDIF
						
						IF iShakeTimer < GET_GAME_TIMER()
							SET_CONTROL_SHAKE(PLAYER_CONTROL, Duration, Frequency)
							iShakeTimer = GET_GAME_TIMER() + (Duration*2)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SWITCH, "wait for shake ", ReturnStartPhase, " <= ", fPlayerSceneAnimOutTime, " <= ", ReturnEndPhase)
						#ENDIF
						
					ENDIF
				ELSE
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SWITCH, "no \"PadShake\"")
					#ENDIF
				ENDIF
				
			ENDIF
			
			IF (sTimetableScene.eBuddyOutTask = sTimetableScene.eOutTask)
				TEXT_LABEL_63 tBuddySyncSceneDict, tBuddySyncSceneLoop, tBuddySyncSceneOut
				ANIMATION_FLAGS buddySyncSceneLoopFlag, buddySyncSceneOutFlag
				IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tBuddySyncSceneDict, tBuddySyncSceneLoop, tBuddySyncSceneOut,
						buddySyncSceneLoopFlag, buddySyncSceneOutFlag)
					IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene,
							"victim_fall", fPlayerSceneAnimOutTime,
							tPlayerSyncSceneDict, tPlayerSyncSceneOut,
							iDrawSceneRot)
					OR HasAnimEventPassed("victim_fall", fPlayerSceneAnimOutTime,
							tPlayerSyncSceneDict, tPlayerSyncSceneOut,
							iDrawSceneRot)
							
						IF NOT IS_ENTITY_DEAD(scene_buddy)
							IF NOT IS_PED_RUNNING_RAGDOLL_TASK(scene_buddy)
//								CLEAR_PED_TASKS(scene_buddy)
//								SET_PED_TO_RAGDOLL_WITH_FALL(scene_buddy, 1000, 3000,
//										TYPE_DIE_FROM_HIGH,
//										GET_ENTITY_FORWARD_VECTOR(scene_buddy), 
//										100.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								SET_HIGH_FALL_TASK(scene_buddy, 1000, 3000, HIGHFALL_IN_AIR)
								
								ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_INJURED_PED, scene_buddy)
							
								SET_PED_MONEY(scene_buddy, 0)		// so he doesnt drop money when dead
								SET_ENTITY_HEALTH(scene_buddy, 0)
							ENDIF
						ENDIF
					ENDIF
					IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene,
							"victim_die", fPlayerSceneAnimOutTime,
							tPlayerSyncSceneDict, tPlayerSyncSceneOut,
							iDrawSceneRot)
					OR HasAnimEventPassed("victim_die", fPlayerSceneAnimOutTime,
							tBuddySyncSceneDict, tBuddySyncSceneOut,
							iDrawSceneRot)
						IF NOT IS_ENTITY_DEAD(scene_buddy)
							ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_INJURED_PED, scene_buddy)
							
							SET_PED_MONEY(scene_buddy, 0)		// so he doesnt drop money when dead
							SET_ENTITY_HEALTH(scene_buddy, 0)
						ENDIF
					ENDIF
					
					BOOL bCheckForBuddyWalkInterruptible
					bCheckForBuddyWalkInterruptible = TRUE
					TEXT_LABEL_31 sPlayerTimetableAdditional_script
					IF SETUP_TIMETABLE_SCRIPT_FOR_SCENE(sTimetableScene.sScene.eScene, sPlayerTimetableAdditional_script)
						IF ARE_STRINGS_EQUAL(sPlayerTimetableAdditional_script, "player_scene_m_shopping")
							bCheckForBuddyWalkInterruptible = FALSE
						ENDIF
					ENDIF
					
					IF bCheckForBuddyWalkInterruptible
					AND NOT IS_ENTITY_DEAD(scene_buddy)
						IF HasAnimEventPassed("END_IN_WALK", fPlayerSceneAnimOutTime,		//#1453336
								tBuddySyncSceneDict, tBuddySyncSceneOut, iDrawSceneRot)
						OR HasAnimEventPassed("WalkInterruptible", fPlayerSceneAnimOutTime,	//#1453336
								tBuddySyncSceneDict, tBuddySyncSceneOut, iDrawSceneRot)
							
							IF (GET_SCRIPT_TASK_STATUS(scene_buddy, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK)
								
								VECTOR VecOffset
								VecOffset = (GET_ENTITY_COORDS(scene_buddy)-sTimetableScene.sScene.vCreateCoords)
								
								//#1533821
								IF sTimetableScene.sScene.eScene = PR_SCENE_F_LAMTAUNT_NIGHT
									VecOffset = (<<168.6380, -1666.2222, 30.2691>>-GET_ENTITY_COORDS(scene_buddy))
								ENDIF
								
								CPRINTLN(DEBUG_SWITCH, "buddy END_IN_WALK, VecOffset: ")
								CPRINTLN(DEBUG_SWITCH, vecOffset)
								
								TASK_GO_STRAIGHT_TO_COORD(scene_buddy,
										 GET_ENTITY_COORDS(scene_buddy) + VecOffset,
										 PEDMOVE_WALK, -1)
								FORCE_PED_MOTION_STATE(scene_buddy, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
		//						FORCE_PED_AI_AND_ANIMATION_UPDATE(scene_buddy)
							ELSE
								CPRINTLN(DEBUG_SWITCH, "buddy SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
							ENDIF
						ELSE 
							CPRINTLN(DEBUG_SWITCH, "buddy no WalkInterruptible???")
						ENDIF
					ENDIF
					
					IF (sTimetableScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
						IF fPlayerSceneAnimOutTime >= 0.883
						AND NOT IS_ENTITY_DEAD(scene_buddy)
//							IF IS_ENTITY_VISIBLE(scene_buddy)
//								SET_ENTITY_VISIBLE(scene_buddy, FALSE)
								
								
								fPlayerDoorOverridePhase = fPlayerSceneAnimOutTime
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF (sTimetableScene.eBuddyOutTask = SCRIPT_TASK_STAND_STILL)		//PR_SCENE_M7_FAKEYOGA
//			OR (sTimetableScene.eBuddyOutTask = SCRIPT_TASK_PLAY_ANIM)			//PR_SCENE_M_VWOODPARK_a/b
				IF NOT bBuddyPostOutTasksPerformed
					FLOAT fBuddyExitPhase
					
					IF IS_BUDDY_QUITING_EXIT_TIMETABLE_SCENE(sTimetableScene.sScene.eScene, fBuddyExitPhase)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("fBuddyExitPhase: ", fBuddyExitPhase, iDrawSceneRot, HUD_COLOUR_ORANGE)
						iDrawSceneRot++
						#ENDIF
						
						IF (fPlayerSceneAnimOutTime > fBuddyExitPhase)
							Control_Buddy_PostOut_Tasks()
							Control_Buddy_PostOut_FriendEvents()
							bBuddyPostOutTasksPerformed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			TEXT_LABEL_63 tPlayerSceneSyncAnimVehExit
			IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneSyncAnimVehExit)
				IF NOT IS_ENTITY_DEAD(scene_veh)
					
					SET_VEH_FORCED_RADIO_THIS_FRAME(scene_veh)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(scene_veh)
					REQUEST_VEHICLE_DIAL(scene_veh)
					
					IF HasAnimPhasePassed_SCRIPT(sTimetableScene.sScene.eScene,
							"close_veh_doors", fPlayerSceneAnimOutTime,
							tPlayerSyncSceneDict, tPlayerSceneSyncAnimVehExit,
							iDrawSceneRot)
					OR (sTimetableScene.sScene.eScene != PR_SCENE_M7_OPENDOORFORAMA
					AND HasAnimEventPassed("close_veh_doors", fPlayerSceneAnimOutTime,
							tPlayerSyncSceneDict, tPlayerSceneSyncAnimVehExit,
							iDrawSceneRot))
						SET_VEHICLE_DOORS_SHUT(scene_veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
				
				BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "exit scene not running")
				SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
				SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
				SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
				
//				SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)	//not required #605445
				
				InterpBackFromSynchCam(ExitCamIndex, TRUE)
				
				FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
				
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(player_prop)
					TEXT_LABEL_63 tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject
					INT iFlags
					IF GET_SYNCHRONIZED_OBJ_FOR_TIMETABLE_EXIT_SCENE(sTimetableScene.sScene.eScene, tPlayerSceneSyncObject, tPlayerSceneSyncExtraObject, iFlags)
					
					
						TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
						ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
						//enumPlayerSceneAnimProgress ePlayerSyncLoopProgress
						
						IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
								tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
								playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSyncLoopProgress)
							IF NOT IS_ENTITY_PLAYING_ANIM(player_prop,
										tPlayerSceneSyncAnimDict,
										tPlayerSceneSyncObject, ANIM_SYNCED_SCENE)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(player_prop,
										g_iPlayer_Timetable_Exit_SynchSceneID,
										tPlayerSceneSyncObject, tPlayerSceneSyncAnimDict,
										INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iFlags)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(player_prop)
								
								CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "is playing anim")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_SWITCH, "no anim dict")
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_SWITCH, "no object anim")
					ENDIF
//				ELSE
//					CPRINTLN(DEBUG_SWITCH, "doesnt exist")
				ENDIF
				
				FLOAT fBlendOutOfSynchPhase
				fBlendOutOfSynchPhase = 1.0
				
				FLOAT fBlendin, fBlendOut
				SYNCED_SCENE_PLAYBACK_FLAGS scpf_Flags
				RAGDOLL_BLOCKING_FLAGS ragdollFlags
				IK_CONTROL_FLAGS ikFlags
				P_GET_PLAYER_SYNCHRONIZED_SCENE_DATA(sTimetableScene.sScene.eScene, scpf_Flags, fBlendin, fBlendOut, ragdollFlags, ikFlags)
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(scpf_Flags, SYNCED_SCENE_TAG_SYNC_OUT)
					fBlendOutOfSynchPhase = 0.95
				ENDIF
				
				IF CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
					fBlendOutOfSynchPhase = 0.85
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) >= fBlendOutOfSynchPhase
					
					TEXT_LABEL_63 strBlend
					strBlend  = "exit scene >= "
					#IF IS_DEBUG_BUILD
					strBlend += GET_STRING_FROM_FLOAT(fBlendOutOfSynchPhase)
					#ENDIF
					#IF NOT IS_DEBUG_BUILD
					strBlend += ROUND(fBlendOutOfSynchPhase)
					#ENDIF
					
					IF NOT CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
						BlendOutOfSynchTask(sTimetableScene.sScene.eScene, strBlend)
					ELSE
						TASK_PLAY_ANIM(PLAYER_PED_ID(),
								"SWITCH@MICHAEL@BENCH",
								"CELLPHONE_CALL_LISTEN_BASE",
								INSTANT_BLEND_IN, FAST_BLEND_OUT, -1,
								AF_SECONDARY|AF_LOOPING|AF_UPPERBODY|AF_HOLD_LAST_FRAME)
						//SET_ANIM_FILTER(PLAYER_PED_ID(), "BONEMASK_HEAD_NECK_AND_R_ARM")		//#1556528
					ENDIF
					
					SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
					SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
					SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
					
					SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
					
					InterpBackFromSynchCam(ExitCamIndex, TRUE)
					
					FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
					
					RETURN TRUE
				ENDIF
				
				
			
				MODEL_NAMES eDoorModel_L, eDoorModel_R
				VECTOR vDoorOffset_L, vDoorOffset_R
				DOOR_HASH_ENUM eDoorHash_L, eDoorHash_R
				BOOL bReplaceDoor
				FLOAT fHideDoorRadius
				IF GET_DOORS_FOR_SCENE(sTimetableScene.sScene.eScene,
						eDoorModel_L, eDoorModel_R, vDoorOffset_L, vDoorOffset_R, eDoorHash_L, eDoorHash_R,
						bReplaceDoor, fHideDoorRadius)
					IF DOES_ENTITY_EXIST(player_door_l)
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) < fPlayerDoorOverridePhase
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_L))
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_L), DOORSTATE_FORCE_OPEN_THIS_FRAME, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L), 1.0, DEFAULT, TRUE)
								
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(R:", ENUM_TO_INT(eDoorHash_L), ", 1.0)")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(R:", ENUM_TO_INT(eDoorHash_L), ")")
							ENDIF

						ELSE
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_L))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L), 0.0, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
								
								FLOAT fDoorRatio
								fDoorRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_L))
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(R:", ENUM_TO_INT(eDoorHash_L), ", 0.0)		//", fDoorRatio, " d:", ABSF(fDoorRatio) < 0.0005, " b:", bReplaceDoor, " r:", fHideDoorRadius > 0)
								#ENDIF
								
								IF ABSF(fDoorRatio) < 0.0005
									
									DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_L), DOORSTATE_FORCE_CLOSED_THIS_FRAME, DEFAULT, TRUE)
									
									IF bReplaceDoor
										IF (fHideDoorRadius > 0)
											DELETE_OBJECT(player_door_l)
											REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_L, fHideDoorRadius, eDoorModel_L)
										ENDIF
									ELSE
										SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_l)
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(R:", ENUM_TO_INT(eDoorHash_L), ")")
								
								IF bReplaceDoor
									IF (fHideDoorRadius > 0)
										DELETE_OBJECT(player_door_l)
										REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_L, fHideDoorRadius, eDoorModel_L)
									ENDIF
								ELSE
									SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_l)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(player_door_r)
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) < fPlayerDoorOverridePhase
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_R))
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_R), DOORSTATE_FORCE_OPEN_THIS_FRAME, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R), 1.0, DEFAULT, TRUE)
								
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(R:", ENUM_TO_INT(eDoorHash_R), ", 1.0) - open")
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(R:", ENUM_TO_INT(eDoorHash_R), ")")
							ENDIF

						ELSE
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash_R))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R), 0.0, DEFAULT, TRUE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
								
								FLOAT fDoorRatio
								fDoorRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(eDoorHash_R))
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_OPEN_RATIO(R:", ENUM_TO_INT(eDoorHash_R), ", 0.0)		//", fDoorRatio, " d:", ABSF(fDoorRatio) < 0.0005, " b:", bReplaceDoor, " r:", fHideDoorRadius > 0)
								#ENDIF
								
								
								IF ABSF(fDoorRatio) < 0.0005
								
									IF bReplaceDoor
										IF (fHideDoorRadius > 0)
											DELETE_OBJECT(player_door_r)
											REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_R, fHideDoorRadius, eDoorModel_r)
										ENDIF
									ELSE
										SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_r)
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_DOOR_REGISTERED_WITH_SYSTEM(R:", ENUM_TO_INT(eDoorHash_R), ")")
								
								IF bReplaceDoor
									IF (fHideDoorRadius > 0)
										DELETE_OBJECT(player_door_r)
										REMOVE_MODEL_HIDE(sTimetableScene.sScene.vCreateCoords+vDoorOffset_R, fHideDoorRadius, eDoorModel_r)
									ENDIF
								ELSE
									SET_OBJECT_AS_NO_LONGER_NEEDED(player_door_r)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			#IF IS_DEBUG_BUILD
			str = ("finishOut VEHICLE_DRIVE_TO_COORD")
			str += (" = ")
			SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
				CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")		BREAK
				CASE PERFORMING_TASK				str += ("PERFORMING")				BREAK
				CASE DORMANT_TASK					str += ("DORMANT")					BREAK
				CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
				CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
				CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
				CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
				CASE FINISHED_TASK					str += ("FINISHED")				BREAK
				
				DEFAULT
					str += ("???invalid task???")
				BREAK
			ENDSWITCH
			
			str += " time:"
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				str += "off"
			ELSE
				str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(tLieInTimer))
			ENDIF
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF DOES_ENTITY_EXIST(scene_veh)
				IF NOT IS_VEHICLE_DRIVEABLE(scene_veh)
					CPRINTLN(DEBUG_SWITCH,"emergency exit for undrivable vehicle")
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
			AND NOT IsPlayerUsingAccelerateOrBrake(scene_veh, iDrawSceneRot)
				//
			ELSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF IS_PED_ON_MOUNT(PLAYER_PED_ID())
					PED_INDEX playersMount_ped
					playersMount_ped = GET_MOUNT(PLAYER_PED_ID())
					
					IF NOT IS_PED_INJURED(playersMount_ped)
						CLEAR_PED_TASKS(playersMount_ped)
					ENDIF
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				START_TIMER_NOW(tLieInTimer)
			ENDIF
			
			IF TIMER_DO_WHEN_READY(tLieInTimer, 5.0)
				CPRINTLN(DEBUG_SWITCH, "emergency exit for driving tasks")
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			
			//#467997 - dont do anything if the buddy is exitting
			IF NOT IS_PED_INJURED(scene_buddy)
			AND IS_VEHICLE_DRIVEABLE(scene_veh)
				IF IS_PED_IN_VEHICLE(scene_buddy, scene_veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE(scene_buddy, scene_veh)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("buddy leaving veh", iDrawSceneRot, HUD_COLOUR_RED)
						iDrawSceneRot++
						#ENDIF
						
//						ar_SET_VEHICLE_FORWARD_SPEED(scene_veh, 0.0)
						TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(),scene_veh,
								TEMPACT_HANDBRAKESTRAIGHT, 0050)
						
						RETURN FALSE
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT IS_PED_ON_MOUNT(PLAYER_PED_ID())
				
				IF bDisableWhileWaiting
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = PERFORMING_TASK
						bDisableWhileWaiting = FALSE
					ENDIF
				ENDIF
				
				RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = FINISHED_TASK
			ELSE
				PED_INDEX playersMount_ped
				playersMount_ped = GET_MOUNT(PLAYER_PED_ID())
				
				IF NOT IS_PED_INJURED(playersMount_ped)
					RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
				ENDIF
				
				RETURN FALSE
			ENDIF
			
			
		BREAK
		
		CASE SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			#IF IS_DEBUG_BUILD
			str = ("finishOut VEHICLE_RECORDING")
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
					
					FLOAT fTimePositionInSceneVehRecording
					fTimePositionInSceneVehRecording = GET_TIME_POSITION_IN_RECORDING(scene_veh)
					
					#IF IS_DEBUG_BUILD
					str  = ("rec time: ")
					str += GET_STRING_FROM_FLOAT(fTimePositionInSceneVehRecording)
					
					DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
					iDrawSceneRot++
					
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(scene_veh), 1.0, HUD_COLOUR_blue)
					
					str = "rec: "
					str += tRecordingName
					str += iRecordingNum
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(scene_veh), 2.0, HUD_COLOUR_blue)
					
					FLOAT fPlaybackPhase
					fPlaybackPhase = GET_TIME_POSITION_IN_RECORDING(scene_veh) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
					
					str  = "fPlaybackPhase: "
					str += GET_STRING_FROM_FLOAT(fPlaybackPhase)
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(scene_veh), 3.0, HUD_COLOUR_blue)
					
					IF g_bDrawLiteralSceneString
						ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(scene_veh, RDM_WHOLELINE)
					ENDIF
					
					#ENDIF
					
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					OR GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
					OR GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_OUTRO_HOLD
						// Restore full playback speed, now that we are passed the final jump cut
						SET_PLAYBACK_SPEED(scene_veh, 1.0)
					ENDIF
					
					IF IsPlayerMovingLeftStick(iDrawSceneRot)
					OR IsPlayerUsingAccelerateOrBrake(scene_veh, iDrawSceneRot)
					OR StopPlaybackPlayersRecordedVehicle(iDrawSceneRot, fTimePositionInSceneVehRecording)
						STOP_PLAYBACK_RECORDED_VEHICLE(scene_veh)
					ENDIF
				ENDIF
				
				/* let code do it
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0)
				*/
				
				RETURN NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPT_TASK_GO_STRAIGHT_TO_COORD
		CASE SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
		CASE SCRIPT_TASK_WANDER_STANDARD
			#IF IS_DEBUG_BUILD
			str  = ("finishOut ")
			IF (sTimetableScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
				str += "GO_STRAIGHT_TO_COORD"
			ELIF (sTimetableScene.eOutTask = SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				str += "FOLLOW_NAV_MESH_TO_COORD"
			ELIF (sTimetableScene.eOutTask = SCRIPT_TASK_WANDER_STANDARD)
				str += "WANDER_STANDARD"
			ENDIF
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot += 2
			
			str  = " time:"
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				str += "off"
			ELSE
				str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(tLieInTimer))
			ENDIF
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot += 2
			#ENDIF
			
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				START_TIMER_NOW(tLieInTimer)
			ENDIF
			
			IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
				//
			ELSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF IS_PED_ON_MOUNT(PLAYER_PED_ID())
					PED_INDEX playersMount_ped
					playersMount_ped = GET_MOUNT(PLAYER_PED_ID())
					
					IF NOT IS_PED_INJURED(playersMount_ped)
						CLEAR_PED_TASKS(playersMount_ped)
					ENDIF
				ENDIF
				
				bDisableWhileWaiting = FALSE
				RETURN TRUE
			ENDIF
			
			IF TIMER_DO_WHEN_READY(tLieInTimer, 5.0)
				CPRINTLN(DEBUG_SWITCH, "emergency exit for goto coord tasks")
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			
			IF bDisableWhileWaiting
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = PERFORMING_TASK
					bDisableWhileWaiting = FALSE
				ENDIF
			ENDIF
			
			IF sTimetableScene.eOutTask = SCRIPT_TASK_WANDER_STANDARD
			
				IF TIMER_DO_WHEN_READY(tLieInTimer, 2.5)
					CPRINTLN(DEBUG_SWITCH, "emergency exit for goto coord tasks")
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
				
				NAVMESH_ROUTE_RESULT enavmesh_route_result
				FLOAT fOut_DistanceRemaining
				INT iOut_ThisIsLastRouteSection
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PLAYER_PED_ID(), fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				IF enavmesh_route_result = NAVMESHROUTE_ROUTE_NOT_FOUND
					TASK_WANDER_STANDARD(PLAYER_PED_ID())
					
					CPRINTLN(DEBUG_SWITCH, "wander again??")
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = FINISHED_TASK
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		
		CASE SCRIPT_TASK_ENTER_VEHICLE
			#IF IS_DEBUG_BUILD
			str = ("finishOut ENTER_VEHICLE")
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				RETURN IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_LEAVE_VEHICLE
			#IF IS_DEBUG_BUILD
			str  = ("finishOut LEAVE_VEHICLE")
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				str += (" timer")
			ENDIF
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh, TRUE)
					IF NOT IS_TIMER_STARTED(tLieInTimer)
						START_TIMER_NOW(tLieInTimer)
					ENDIF
					
					IF TIMER_DO_WHEN_READY(tLieInTimer, 1.5)
						RETURN TRUE
					ENDIF
				ENDIF
				
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPT_TASK_STAND_STILL
			IF IS_VEHICLE_DRIVEABLE(scene_veh)
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
					
					FLOAT fScene_veh_speed
					fScene_veh_speed = GET_ENTITY_SPEED(scene_veh)
					
					#IF IS_DEBUG_BUILD
					str = ("finishOut COORS_TARGET ")
					DrawLiteralSceneStringFloat(str, fScene_veh_speed, iDrawSceneRot, HUD_COLOUR_blue)
					iDrawSceneRot++
					#ENDIF
			
					IF fScene_veh_speed <= 1.0
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
					
					IF IsPlayerMovingLeftStick(iDrawSceneRot)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str = ("finishOut STAND_STILL")
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE SCRIPT_TASK_ACHIEVE_HEADING
			#IF IS_DEBUG_BUILD
			str = ("finishOut ACHIEVE_HEADING")
			str += (" = ")
			SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING)
				CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")				BREAK
				CASE PERFORMING_TASK				str += ("PERFORMING")					BREAK
				CASE DORMANT_TASK					str += ("DORMANT")						BREAK
				CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
				CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
				CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
				CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
				CASE FINISHED_TASK					str += ("FINISHED")						BREAK
				
				DEFAULT
					str += ("???invalid task???")
				BREAK
			ENDSWITCH
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING) = FINISHED_TASK
		BREAK
		CASE SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER
			#IF IS_DEBUG_BUILD
			str = ("finishOut PUT_PED_DIRECTLY_INTO_COVER")
			str += (" = ")
			SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")				BREAK
				CASE PERFORMING_TASK				str += ("PERFORMING")					BREAK
				CASE DORMANT_TASK					str += ("DORMANT")						BREAK
				CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
				CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
				CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
				CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
				CASE FINISHED_TASK					str += ("FINISHED")						BREAK
				
				DEFAULT
					str += ("???invalid task???")
				BREAK
			ENDSWITCH
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF NOT IS_TIMER_STARTED(tLieInTimer)
				START_TIMER_NOW(tLieInTimer)
			ENDIF
			IF TIMER_DO_WHEN_READY(tLieInTimer, 10.0)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				CANCEL_TIMER(tLieInTimer)
			ENDIF
			
			//fix for B*1565359, treat post Franklin 1 switches as special case and end script when player moves away from coverpoint
			RETURN (GET_DISTANCE_BETWEEN_COORDS(sTimetableScene.sScene.vCreateCoords, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE) > 3.0)
			//RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) = FINISHED_TASK
			
		BREAK
		CASE SCRIPT_TASK_AIM_GUN_AT_ENTITY
			#IF IS_DEBUG_BUILD
			str = ("finishOut AIM_GUN_AT_ENTITY")
			str += (" = ")
			SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_AT_ENTITY)
				CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")		BREAK
				CASE PERFORMING_TASK				str += ("PERFORMING")				BREAK
				CASE DORMANT_TASK					str += ("DORMANT")					BREAK
				CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
				CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
				CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
				CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
				CASE FINISHED_TASK					str += ("FINISHED")				BREAK
				
				DEFAULT
					str += ("???invalid task???")
				BREAK
			ENDSWITCH
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
			IF IsPlayerMovingLeftStick(iDrawSceneRot)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			
			RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_AT_ENTITY) = FINISHED_TASK
		BREAK
		CASE SCRIPT_TASK_USE_MOBILE_PHONE
			#IF IS_DEBUG_BUILD
			str = ("finishOut USE_MOBILE_PHONE")
			str += (" = ")
			SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_USE_MOBILE_PHONE)
				CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")		BREAK
				CASE PERFORMING_TASK				str += ("PERFORMING")				BREAK
				CASE DORMANT_TASK					str += ("DORMANT")					BREAK
				CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
				CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
				CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
				CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
				CASE FINISHED_TASK					str += ("FINISHED")				BREAK
				
				DEFAULT
					str += ("???invalid task???")
				BREAK
			ENDSWITCH
			
			DrawLiteralSceneString(str, iDrawSceneRot, HUD_COLOUR_blue)
			iDrawSceneRot++
			#ENDIF
			
//			IF IsPlayerMovingLeftStick(iDrawSceneRot)
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
//				RETURN TRUE
//			ENDIF
//			
//			RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_USE_MOBILE_PHONE) = FINISHED_TASK
			
			RETURN HAS_CELLPHONE_CALL_FINISHED()
		BREAK
		
		CASE SCRIPT_TASK_ANY						FALLTHRU
		DEFAULT

			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sInvalid
			sInvalid = "invalid eTask for Control_player_finishing_out_Tasks: "
			sInvalid += ENUM_TO_INT(sTimetableScene.eOutTask)
			sInvalid += "  //"
			sInvalid += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
			
			CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
			SCRIPT_ASSERT(sInvalid)
			#ENDIF
			
			IF (sTimetableScene.eOutTask <> SCRIPT_TASK_ANY)
				RETURN GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), sTimetableScene.eOutTask) = FINISHED_TASK
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sUnknown
	sUnknown = "unknown eTask for Control_player_finishing_out_Tasks: "
	sUnknown += ENUM_TO_INT(sTimetableScene.eOutTask)
	sUnknown += "  //"
	sUnknown += Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
	
	CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sUnknown)
	SCRIPT_ASSERT(sUnknown)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Has_Player_Finished_Timetabled_Scene()
	INT iDrawSceneRot
	
	IF NOT bHave_Created_Switch_Scene_Conversation
		IF Create_Switch_Scene_Conversation()
			bHave_Created_Switch_Scene_Conversation = TRUE
		ENDIF
		
	ELSE
		IF NOT bHave_Played_Switch_Scene_Speech_Facial
			TEXT_LABEL_63 tFacialClip
			IF GET_SPEECH_FACIAL_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene, tFacialClip)
			
				TEXT_LABEL_63 tPlayerSceneFacialAnimDict, tPlayerSceneFacialAnimLoop, tPlayerSceneFacialAnimOut
				ANIMATION_FLAGS playerFacialLoopFlag, playerFacialOutFlag
				
				IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
						tPlayerSceneFacialAnimDict, tPlayerSceneFacialAnimLoop, tPlayerSceneFacialAnimOut,
						playerFacialLoopFlag, playerFacialOutFlag)
				
					CPRINTLN(DEBUG_SWITCH, "	TASK_PLAY_FACIAL_ANIM(PLAYER_PED_ID(), \"", tPlayerSceneFacialAnimDict, "\", \"", tFacialClip, "\")")
					
					TASK_PLAY_ANIM(PLAYER_PED_ID(), tPlayerSceneFacialAnimDict, tFacialClip,
							NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
					bHave_Played_Switch_Scene_Speech_Facial = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	TEXT_LABEL_31 tContext
	FLOAT fSceneContextPhase = -1
	BOOL bPlayerSpeaks
	IF SETUP_TIMETABLE_CONTEXT_FOR_SCENE(sTimetableScene.sScene.eScene, tContext, fSceneContextPhase, bPlayerSpeaks)
		IF (fSceneContextPhase >= 0)
		AND NOT (bPlayed_Switch_Scene_Context)
		
			TEXT_LABEL_63 tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut
			ANIMATION_FLAGS playerSceneLoopFlag, playerSceneOutFlag
			//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
			
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene,
					tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimLoop, tPlayerSceneSceneAnimOut,
					playerSceneLoopFlag, playerSceneOutFlag)	//, ePlayerSceneAnimProgress)
				FLOAT fPlayer_anim_current_time
				IF Get_Ped_Anim_Or_SynchedScene_Phase(PLAYER_PED_ID(), tPlayerSceneSceneAnimDict, tPlayerSceneSceneAnimOut, fPlayer_anim_current_time)
					//
					
					
					IF (fPlayer_anim_current_time >= fSceneContextPhase)
						
						#IF IS_DEBUG_BUILD
						IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
						OR (NOT IS_PED_INJURED(scene_buddy) AND IS_ANY_SPEECH_PLAYING(scene_buddy))
							fSceneContextPhase *= GET_RANDOM_FLOAT_IN_RANGE(1.0100, 1.0900)
							
							SAVE_STRING_TO_DEBUG_FILE("fSceneContextPhase = ")
							SAVE_FLOAT_TO_DEBUG_FILE(fSceneContextPhase)
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							CPRINTLN(DEBUG_SWITCH, "fSceneContextPhase = ", fSceneContextPhase)
							
							TEXT_LABEL_63 sScene, sInvalid
							sScene = Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
							sInvalid  = "speech going during fSceneContextPhase "
							sInvalid += GET_STRING_FROM_FLOAT(fSceneContextPhase)
							sInvalid += ": "
							sInvalid += GET_STRING_FROM_STRING(sScene,
									GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
									GET_LENGTH_OF_LITERAL_STRING(sScene))
							
							CPRINTLN(DEBUG_SWITCH, "player_timetable_scene: ", sInvalid)
							
							RETURN FALSE
						ENDIF
						#ENDIF
						
						IF bPlayerSpeaks
							PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), tContext)
						ELSE
							PLAY_PED_AMBIENT_SPEECH(scene_buddy, tContext)
						ENDIF
						bPlayed_Switch_Scene_Context = TRUE
					ENDIF
					
				ENDIF
			ENDIF
		
		
		ENDIF
	ENDIF
	
	
	
	IF IS_BUDDY_ASLEEP_FOR_TIMETABLE_SCENE(sTimetableScene.sScene.eScene)
		IF NOT IS_PED_INJURED(scene_buddy)
			PRIVATE_Update_Family_Sleeping(scene_buddy)
		ENDIF
	ENDIF
	
//	IF NOT bHave_Triggered_Music_Event
//		IF (sTimetableScene.sScene.eScene = PR_SCENE_M_MD_FBI2)
//			IF TRIGGER_MUSIC_EVENT("FIB2_CAR_TO_CS")
//				bHave_Triggered_Music_Event = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF Has_Player_Finished_Out_Tasks_Finished()
		IF NOT IS_PED_INJURED(scene_buddy)
			IF NOT bBuddyPostOutTasksPerformed
				Control_Buddy_PostOut_Tasks()
				Control_Buddy_PostOut_FriendEvents()
				bBuddyPostOutTasksPerformed = TRUE
			ENDIF
		ENDIF
		
		FLOAT paramStrength, amplitudeScalar
		BOOL bCountsForPubClubVisit
		IF SHAKE_PLAYER_TIMETABLE_DRUNK_CAM(sTimetableScene.sScene.eScene, paramStrength, amplitudeScalar, bCountsForPubClubVisit)

		
//			SAVE_STRING_TO_DEBUG_FILE("SHAKE_PLAYER_TIMETABLE_DRUNK_CAM D")
//			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			#IF IS_DEBUG_BUILD
			IF g_bDrawLiteralSceneString
			g_bDrawDebugDrunkInfo = TRUE
			ENDIF
			#ENDIF
			
			IF NOT (g_drunkCameraActive)
				Activate_Drunk_Camera(iCONST_DRUNK_secDuration, iCONST_SOBER_secDuration, paramStrength, amplitudeScalar, ExitCamIndex)
			ELSE
				Update_Drunk_Camera_ParamTime(iCONST_DRUNK_secDuration)
				Update_Drunk_Camera_ParamStrength(paramStrength*3.0)
				Update_Drunk_Camera_ParamDesiredAmplitude(amplitudeScalar*3.0)
			ENDIF
			
			IF NOT Is_Ped_Drunk(PLAYER_PED_ID())
				Make_Ped_Drunk(PLAYER_PED_ID(), iCONST_DRUNK_secDuration)
			ELSE
				Extend_Overall_Drunk_Time(PLAYER_PED_ID(), iCONST_DRUNK_secDuration)
			ENDIF
			
			IF bCountsForPubClubVisit
				BawsaqIncrementDrunkModifier_PUBCLUBVISIT()
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_CHECKSHOE)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_WIPEHANDS)
	OR (sTimetableScene.sScene.eScene = PR_SCENE_F_CS_WIPERIGHT)
		IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_HAYES_GARAGE, PLAYER_PED_ID())
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_HAYES_GARAGE, PLAYER_PED_ID())
			
			CPRINTLN(DEBUG_SWITCH, "REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR...")
		ELSE
			
			CPRINTLN(DEBUG_SWITCH, "PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR...")
		ENDIF
	ELIF (sTimetableScene.sScene.eScene = PR_SCENE_M7_KIDS_GAMING)
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.8,174.0,76.9>>)
			
			CPRINTLN(DEBUG_SWITCH, "ADD_DOOR_TO_SYSTEM...")
			
		ELSE
			
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_FORCE_LOCKED_THIS_FRAME, DEFAULT, TRUE)
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1, DEFAULT, TRUE)
			
			CPRINTLN(DEBUG_SWITCH, "DOOR_SYSTEM_SET_DOOR_STATE(M_MANSION_SON)")
			
		ENDIF
	ENDIF
	
	//detach prop when requested
	SetPedFromSynchedSceneIntoObject(player_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, FALSE)
	SetPedFromSynchedSceneIntoObject(player_extra_prop, sTimetableScene.sScene.eScene, iDrawSceneRot, TRUE)
	
//	#IF IS_DEBUG_BUILD
//	//DrawDebugSceneCamInfo()	//1218160
//	#ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL Has_Player_Finished_Wardrobe_Scene()
	
	VECTOR vFloydSceneCoord = <<-812.003,173.700,75.741>>
	VECTOR vFloydSceneRot = <<0,0,-74.00+90.00>>
	
	TEXT_LABEL_31 sWardrobeDict = "switch@michael@closet"
	TEXT_LABEL sWardrobeClip = ""
	
	CAMERA_INDEX wardrobeCamIndex
	
	CONST_INT iWardrobe_Stage_0		0
	CONST_INT iWardrobe_Stage_1		1
	CONST_INT iWardrobe_Stage_2		2
	CONST_INT iWardrobe_Stage_null	-1
	
	#IF IS_DEBUG_BUILD
	BOOL bMoveScene
	FLOAT fScenePhase
	SET_CURRENT_WIDGET_GROUP(player_timetable_scene_widget)
	START_WIDGET_GROUP(sWardrobeDict)
		ADD_WIDGET_BOOL("bMoveScene", bMoveScene)
		ADD_WIDGET_VECTOR_SLIDER("vFloydSceneCoord", vFloydSceneCoord, -1000, 1000, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("vFloydSceneRot", vFloydSceneRot, -360, 360, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fScenePhase", fScenePhase, 0, 1, 0.001)
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(player_timetable_scene_widget)
	#ENDIF
	
	INT iWardrobeStage = iWardrobe_Stage_0, iWardrobeSceneID = -1, iWardrobeGameTimer = -1
	WHILE (iWardrobeStage > iWardrobe_Stage_null)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringInt("iWardrobeStage ", iWardrobeStage, 0, HUD_COLOUR_PURPLELIGHT)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iWardrobeSceneID)
			DrawLiteralSceneStringFloat("phase ", fScenePhase, 1, HUD_COLOUR_PURPLELIGHT)
		ELSE
			DrawLiteralSceneString("phase NULL ", 1, HUD_COLOUR_PURPLELIGHT)
		ENDIF
		#ENDIF
		
		SWITCH iWardrobeStage
			CASE iWardrobe_Stage_0
				bDisableWhileWaiting = TRUE
				
				REQUEST_ANIM_DICT(sWardrobeDict)
			
				IF HAS_ANIM_DICT_LOADED(sWardrobeDict)
					
					sWardrobeClip = "closet_c"
					
					//
					BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "wardrobe")
					
					IF (sTimetableScene.eVehState = PTVS_2_playerInVehicle)
						SetPedFromSynchedSceneIntoVehicle(sTimetableScene.sScene.eScene)
					ENDIF
					
					IF DOES_ENTITY_EXIST(player_prop)
						MODEL_NAMES eObjectModel 
						VECTOR vecOffset , vecRotation 
						PED_BONETAG eBonetag
						FLOAT fDetachAnimPhase 
						enumPlayerSceneObjectAction thisSceneObjectAction
						
						eObjectModel = DUMMY_MODEL_FOR_SCRIPT
						vecOffset = <<0,0,0>>
						vecRotation = <<0,0,0>>
						eBonetag = BONETAG_NULL
						fDetachAnimPhase = -1.0
						thisSceneObjectAction = PSOA_NULL
						
						IF GET_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
									eObjectModel, vecOffset, vecRotation, eBonetag,
									fDetachAnimPhase, thisSceneObjectAction)
							SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(player_extra_prop)
						MODEL_NAMES eObjectModel 
						VECTOR vecOffset , vecRotation 
						PED_BONETAG eBonetag
						FLOAT fDetachAnimPhase 
						enumPlayerSceneObjectAction thisSceneObjectAction
						BOOL bAttachedToBuddy
						
						eObjectModel = DUMMY_MODEL_FOR_SCRIPT
						vecOffset = <<0,0,0>>
						vecRotation = <<0,0,0>>
						eBonetag = BONETAG_NULL
						fDetachAnimPhase = -1.0
						thisSceneObjectAction = PSOA_NULL
						bAttachedToBuddy = FALSE
						
						IF GET_EXTRA_OBJECTS_FOR_SCENE(sTimetableScene.sScene.eScene,
									eObjectModel, vecOffset, vecRotation, eBonetag,
									fDetachAnimPhase, thisSceneObjectAction, bAttachedToBuddy)
							SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_extra_prop, thisSceneObjectAction)
						ENDIF
					ENDIF
					
					SafeDetachSynchronizedScene(g_iPlayer_Timetable_Exit_SynchSceneID)
					
					InterpBackFromSynchCam(ExitCamIndex, TRUE)
					
					FORCE_ROOM_FOR_PLAYER_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
					
					IF NOT SET_PED_COMPONENT_FOR_MISSION(PLAYER_PED_ID())
						SET_RANDOM_CLOTHES_COMBO(PLAYER_PED_ID(), TRUE, TRUE, TRUE)
					ENDIF
					
					IF SETUP_FORCE_STEP_TYPE_FOR_SCENE(sTimetableScene.sScene.eScene)
						SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), TRUE, 0, 1)
					ENDIF
					
					//
					
					iWardrobeSceneID = CREATE_SYNCHRONIZED_SCENE(vFloydSceneCoord, vFloydSceneRot)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iWardrobeSceneID,
							sWardrobeDict, sWardrobeClip,
							INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					TEXT_LABEL sWardrobeCamClip
					sWardrobeCamClip = sWardrobeClip
					sWardrobeCamClip += "_cam"
					wardrobeCamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
					PLAY_SYNCHRONIZED_CAM_ANIM(wardrobeCamIndex, iWardrobeSceneID,
							sWardrobeCamClip, sWardrobeDict)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(iWardrobeSceneID, FALSE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iWardrobeSceneID, TRUE)
					SET_SYNCHRONIZED_SCENE_PHASE(iWardrobeSceneID, 0.0)
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
					
					iWardrobeGameTimer = GET_GAME_TIMER() + 1000
					iWardrobeStage = iWardrobe_Stage_1
				ENDIF
			BREAK
			CASE iWardrobe_Stage_1
				
				SET_SYNCHRONIZED_SCENE_PHASE(iWardrobeSceneID, 0.0)
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("iWardrobeGameTimer ", GET_GAME_TIMER() - iWardrobeGameTimer, 2, HUD_COLOUR_PURPLEDARK)
				#ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
					AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID())
						iWardrobeStage = iWardrobe_Stage_2
					ENDIF
				ENDIF
				
				IF iWardrobeStage != iWardrobe_Stage_2
					IF GET_GAME_TIMER() > iWardrobeGameTimer
						SCRIPT_ASSERT("emergency wardrobe bail")
						iWardrobeStage = iWardrobe_Stage_2
					ENDIF
				ENDIF
			BREAK
			CASE iWardrobe_Stage_2
				
				BOOL bBlendoutFromScene
				bBlendoutFromScene = FALSE
				
				FLOAT fWardrobeScenePhase
				fWardrobeScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iWardrobeSceneID)
				
				INT iDrawSceneRot
				iDrawSceneRot = 2
				
				IF HasAnimEventPassed("WalkInterruptible", fWardrobeScenePhase,
						sWardrobeDict, sWardrobeClip,
						iDrawSceneRot)
					IF NOT IsPlayerMovingLeftStick(iDrawSceneRot)
						//
					ELSE
						bBlendoutFromScene = TRUE
					ENDIF
				ENDIF
				
				IF NOT bFilledPushInData
					IF HasFirstPersonQuitCamPhasePassed(sTimetableScene.sScene.eScene, wardrobeCamIndex,
							fWardrobeScenePhase,
							iDrawSceneRot)
						InterpBackFromSynchCam(wardrobeCamIndex, TRUE)
					ENDIF
				ENDIF
				
				IF fWardrobeScenePhase >= 0.99
					bBlendoutFromScene = TRUE
				ENDIF
				
				IF bBlendoutFromScene
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					BlendOutOfSynchTask(sTimetableScene.sScene.eScene, "wardrobe scene done")
					
					InterpBackFromSynchCam(wardrobeCamIndex, TRUE)
					
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.1103)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-0.0921)
					ENDIF
					
					iWardrobeStage = iWardrobe_Stage_null
				ENDIF
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		IF NOT bMoveScene
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iWardrobeSceneID)
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iWardrobeSceneID)
			ENDIF
		ELSE
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iWardrobeSceneID)
				SET_SYNCHRONIZED_SCENE_PHASE(iWardrobeSceneID, fScenePhase)
				SET_SYNCHRONIZED_SCENE_ORIGIN(iWardrobeSceneID, vFloydSceneCoord, vFloydSceneRot)
			ENDIF
		ENDIF
		#ENDIF
		
		DisableWhileWaiting("Has_Player_Finished_Wardrobe_Scene")
		
		WAIT(0)
	ENDWHILE
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT(PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene)
	
	INT ForceCleanupBitField = FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY	//FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED|FORCE_CLEANUP_FLAG_SP_TO_MP
	IF HAS_FORCE_CLEANUP_OCCURRED(ForceCleanupBitField)
		IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() != FORCE_CLEANUP_FLAG_SP_TO_MP
			Player_Timetable_Scene_Cleanup(TRUE, FALSE)
		ELIF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() != FORCE_CLEANUP_FLAG_REPEAT_PLAY
			Player_Timetable_Scene_Cleanup(TRUE, TRUE)
		ELSE
			Player_Timetable_Scene_Cleanup(FALSE, FALSE)
		ENDIF
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Timetable_Scene_Variables(sPassedScene)
	Setup_Player_Timetable_Scene()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Timetable_Scene_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	IF bTriggeredFade
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			SET_THIS_IS_A_TRIGGER_SCRIPT(FALSE)
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			bTriggeredFade = FALSE
			CPRINTLN(DEBUG_SWITCH, "<TRIGGER FADE> force fade in after trying to speed up assert streaming")
		ENDIF
	ENDIF
	ANIM_DATA LocalAnimData, EmptyAnimData
	
	WHILE bPlayer_timetable_scene_in_progress
		IF Should_Switch_Cleanup_Be_Forced()
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					Control_Player_Out_Tasks()
					//Reset savehouse blips
					INT iSavehouse
		      		REPEAT ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS) iSavehouse
		            	Update_Savehouse_Respawn_Garage(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
						Update_Savehouse_Respawn_Blip(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
					ENDREPEAT
				ENDIF
				IF NOT IS_PED_INJURED(scene_buddy)
					Control_Buddy_Out_Tasks()
				ENDIF
				
				Player_Timetable_Scene_Cleanup(FALSE, FALSE)
			ENDIF
		ENDIF
		
		WAIT(0)
		
//		IF (current_player_timetable_scene_stage <> PLAYER_HAS_WARDROBE_ANIM)
			DisableWhileWaiting("bPlayer_timetable_scene_in_progress")
//		ENDIF
		
		//url:bugstar:974545
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE , false)
		SUPRESS_RANDOM_EVENT_THIS_FRAME(RC_COP_PURSUE_VEHICLE_FLEE_SPAWNED , false)
		
		SWITCH current_player_timetable_scene_stage
			CASE WAIT_FOR_PLAYER_PED_TO_UPDATE
				IF Has_Current_Player_Ped_Updated()
					current_player_timetable_scene_stage = CONTROL_PLAYER_DURING_SWITCH
				ENDIF
			BREAK
			CASE CONTROL_PLAYER_DURING_SWITCH
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF Has_Player_Finished_Switching()
						current_player_timetable_scene_stage = PLAYER_SCENE_STARTED
					ENDIF
				ENDIF
			BREAK
			CASE PLAYER_SCENE_STARTED
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF Has_Player_Started_New_Scene()
						current_player_timetable_scene_stage = PLAYER_EXITING_SWITCH
					ENDIF
				ENDIF
			BREAK
			CASE PLAYER_EXITING_SWITCH
				IF (sTimetableScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
				AND NOT bSetTanishaFightDoorVisible
					SET_ENTITY_VISIBLE(player_door_r, TRUE)
					bSetTanishaFightDoorVisible = TRUE
				ENDIF

				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF Has_Player_Finished_Timetabled_Scene()
						IF GET_PLAYER_WARDROBE_ANIM_FOR_TIMETABLE_SCENE(PLAYER_PED_ID(), sTimetableScene.sScene.eScene)
							bDisableWhileWaiting = TRUE
							current_player_timetable_scene_stage = PLAYER_HAS_WARDROBE_ANIM
						ELIF CONTROL_PLAYER_IS_ON_THE_PHONE(sTimetableScene.sScene.eScene)
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								TASK_PLAY_ANIM(PLAYER_PED_ID(),
//										"SWITCH@MICHAEL@BENCH",
//										"CELLPHONE_CALL_LISTEN_BASE",
//										SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
//										AF_SECONDARY|AF_LOOPING|AF_UPPERBODY|AF_HOLD_LAST_FRAME)
//								SET_ANIM_FILTER(PLAYER_PED_ID(), "BONEMASK_HEAD_NECK_AND_R_ARM")		//#1556528
//							ENDIF
							
							iBenchcallHangupStage = 0
							current_player_timetable_scene_stage = PLAYER_ON_PHONE_ANIM
						ELSE
							current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PLAYER_ON_PHONE_ANIM
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SWITCH iBenchcallHangupStage
						CASE 0
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								#IF IS_DEBUG_BUILD
								DrawLiteralSceneString("player on the phone", 2, HUD_COLOUR_GREEN)
								#ENDIF
								
							ELSE
								#IF IS_DEBUG_BUILD
								DrawLiteralSceneString("player NOT the phone", 2, HUD_COLOUR_RED)
								#ENDIF
								
								//TASK_PLAY_ANIM(PLAYER_PED_ID(),
								//		"SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT",
								//		FAST_BLEND_IN, SLOW_BLEND_OUT, -1,
								//		AF_SECONDARY|AF_UPPERBODY)
								//SET_ANIM_FILTER(PLAYER_PED_ID(), "BONEMASK_HEAD_NECK_AND_R_ARM")		//#1556528
								
								LocalAnimData.type = APT_SINGLE_ANIM
								LocalAnimData.dictionary0 = "switch@michael@bench"
								LocalAnimData.anim0 = "cellphone_call_out"

								LocalAnimData.flags = AF_SECONDARY

								LocalAnimData.blendInDelta = WALK_BLEND_IN
								LocalAnimData.blendOutDelta = WALK_BLEND_OUT
								LocalAnimData.filter = GET_HASH_KEY("bonemask_head_neck_and_r_arm")

								TASK_SCRIPTED_ANIMATION(PLAYER_PED_ID(), LocalAnimData, EmptyAnimData, EmptyAnimData, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								
								iBenchcallHangupStage = 1
							ENDIF
						BREAK
						
						CASE 1
							BOOL bHangedUpDeletedPhone
							bHangedUpDeletedPhone = FALSE
							
							IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT")
								FLOAT fCELLPHONE_CALL_OUT_time
								fCELLPHONE_CALL_OUT_time = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT")
								
								#IF IS_DEBUG_BUILD
								DrawLiteralSceneStringFloat("CALL_OUT:", fCELLPHONE_CALL_OUT_time, 3, HUD_COLOUR_RED)
								#ENDIF
								
								IF fCELLPHONE_CALL_OUT_time >= 0.443
									bHangedUpDeletedPhone = TRUE
								ENDIF
							ELSE
								bHangedUpDeletedPhone = TRUE
							ENDIF
							
							IF bHangedUpDeletedPhone
								IF DOES_ENTITY_EXIST(player_prop)
									DELETE_OBJECT(player_prop)
								ENDIF
								
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								
								iBenchcallHangupStage = 2
								current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE
							ENDIF
						BREAK
						
						DEFAULT
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneString("unknown iBenchcallHangupStage?", 3, HUD_COLOUR_RED)
							#ENDIF
								
							current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE
						BREAK
					ENDSWITCH
				ELSE
					IF DOES_ENTITY_EXIST(player_prop)
						DELETE_OBJECT(player_prop)
					ENDIF
					
					iBenchcallHangupStage = 2
					current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE
				ENDIF
			BREAK
			CASE PLAYER_HAS_WARDROBE_ANIM
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF Has_Player_Finished_Wardrobe_Scene()
						current_player_timetable_scene_stage = FINISHED_TIMETABLED_SCENE
					ENDIF
				ENDIF
			BREAK
			
			CASE FINISHED_TIMETABLED_SCENE
				Player_Timetable_Scene_Finished()
			BREAK
		ENDSWITCH
		
		IF bFilledPushInData
			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
			IF NOT HANDLE_PUSH_IN(sPushInData,
					TRUE,		//BOOL bCamsAttached = FALSE,
					TRUE,		//BOOL bDestroyCamsAtEnd = TRUE,
					DEFAULT,	//BOOL bDoPostFX = TRUE,
					DEFAULT,	//BOOL bOnlyAttachStartIfCamsAttached = FALSE,
					FALSE)	//BOOL bDoColouredFlash = TRUE)
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				CPRINTLN(DEBUG_SWITCH, "HANDLE_PUSH_IN FALSE state: ", sPushInData.state)
			ELSE
				CPRINTLN(DEBUG_SWITCH, "HANDLE_PUSH_IN TRUE state: ", sPushInData.state)
				bFilledPushInData = FALSE
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_SWITCH, "bFilledPushInData: FALSE!!")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Timetable_Scene_Widget()
		Player_Timetable_Scene_Debug_Options()
		
		TEXT_LABEL_63 tScene = Get_String_From_Ped_Request_Scene_Enum(sTimetableScene.sScene.eScene)
		HUD_COLOURS eHudColour = HUD_COLOUR_PURPLE
		
//		SWITCH eSceneAnimProgress
//			CASE PAP_0_default
//				tScene += " [m]"
//				eHudColour = HUD_COLOUR_REDLIGHT
//			BREAK
//			CASE PAP_1_placeholder
//				tScene += " [p]"
//				eHudColour = HUD_COLOUR_GREYLIGHT
//			BREAK
//			CASE PAP_2_variation
//				tScene += " [v]"
//				eHudColour = HUD_COLOUR_GREYDARK
//			BREAK
//			
//			DEFAULT
//				//
//			BREAK
//		ENDSWITCH
	    
		DrawLiteralSceneString(GET_STRING_FROM_STRING(tScene,
				GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
				GET_LENGTH_OF_LITERAL_STRING(tScene)),
				0, eHudColour)
		DrawLiteralSceneString(
				Get_String_From_Player_Timetable_Scene_Stage(current_player_timetable_scene_stage),
				1, HUD_COLOUR_PURPLELIGHT)
		
		#ENDIF
		#IF NOT IS_DEBUG_BUILD
//		eSceneAnimProgress = eSceneAnimProgress
		#ENDIF
		
	ENDWHILE
	
	Player_Timetable_Scene_Cleanup(FALSE, FALSE)
ENDSCRIPT
