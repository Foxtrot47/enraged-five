// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene T_PARK file for use – player_scene_t_park.sc			 ___
// ___ 																					 ___
// _________________________________________________________________________________________



USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//

USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T_PARK_STAGE_ENUM
	PS_T_PARK_switchInProgress,
	PS_T_PARK_STAGE_TWO,
	
	FINISHED_T_PARK
ENDENUM
PS_T_PARK_STAGE_ENUM		current_player_scene_t_park_stage						= PS_T_PARK_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_t_park_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_T_Park_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX		player_prop
MODEL_NAMES			ePropModel = DUMMY_MODEL_FOR_SCRIPT
VECTOR				propOffset, propRotation
PED_BONETAG			eAttachBonetag
FLOAT				fDetachAnimPhase
enumPlayerSceneObjectAction thisSceneObjectAction

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_t_park_coord
FLOAT				fPlayer_scene_t_park_head

	//- Pickups(INT) -//
PTFX_ID				ptLoop, ptSpray

PED_BONETAG			ePtfxBonetag
TEXT_LABEL_63		tPlayerParkLoopPtfx, tPlayerParkPtfx, tPlayerParkEndPtfx
VECTOR vParkEndPtfxOffset

FLOAT fStartEffectTime_A = -1, fStartEffectTime_B = -1

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tPlayerParkAnimDict, tPlayerParkAnimOut

TEXT_LABEL_63		tPlayerParkSfxBank, tPlayerParkSfxSet, tPlayerParkSfxSound
INT					iPlayerParkSfxID

INT					iSprayStage = 0

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_scene_t_park_widget
INT				iCurrent_player_scene_t_park_stage
BOOL			bMoveProp, bLoopieLoopLoop
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_T_Park_Cleanup()
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	
	IF DOES_ENTITY_EXIST(player_prop)
		IF (thisSceneObjectAction <> PSOA_NULL)
			SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
		ENDIF
		
	ENDIF
	
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	IF (ptSpray <> NULL)
		STOP_PARTICLE_FX_LOOPED(ptSpray)
		ptSpray = NULL
		
		STOP_SOUND(iPlayerParkSfxID)
		RELEASE_SOUND_ID(iPlayerParkSfxID)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_t_park_widget)
		DELETE_WIDGET_GROUP(player_scene_t_park_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
//	IF iPlayer_scene_t_park_cutscene_stage < 10
//		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		ENDIF
//	ENDIF
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_T_Park_Finished()
	//CLEAR_CprintS()
	bPlayer_Scene_T_Park_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_T_Park_Variables()
	
	selected_player_scene_t_park_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_t_park_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_t_park_scene,
			vPlayer_scene_t_park_coord, fPlayer_scene_t_park_head, tPlayer_scene_t_park_room)
	
	//- vectors -//
	//- floats -//
	//- ints -//
	
	
	SWITCH selected_player_scene_t_park_scene
		CASE PR_SCENE_T_PUKEINTOFOUNT
		CASE PR_SCENE_T_CN_PARK_b
//		CASE PR_SCENE_T_CN_PARK_c
			tPlayerParkSfxBank = "TREVOR_PUKEINTOFOUNT"
			tPlayerParkSfxSet = "TREVOR_PUKEINTOFOUNT_SOUNDS"
			tPlayerParkSfxSound = "Puke"
			
			fStartEffectTime_A = 0.19
			fStartEffectTime_B = 0.33
			
			ePtfxBonetag = BONETAG_HEAD
			tPlayerParkLoopPtfx = ""
			tPlayerParkPtfx = "scr_trev_amb_puke"
			
			tPlayerParkEndPtfx = "scr_pts_vomit_water"
			
			IF (selected_player_scene_t_park_scene = PR_SCENE_T_PUKEINTOFOUNT)
				vParkEndPtfxOffset = <<-118.32176, -441.76620, 34.97325+0.25>>-vPlayer_scene_t_park_coord
			ELIF (selected_player_scene_t_park_scene = PR_SCENE_T_CN_PARK_b)
				vParkEndPtfxOffset = <<0.1250, -1.1486, 0.0588+0.1>>
				
				#IF IS_DEBUG_BUILD
				SAVE_STRING_TO_DEBUG_FILE("vParkEndPtfxOffset = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_t_park_coord+vParkEndPtfxOffset)
				SAVE_STRING_TO_DEBUG_FILE("-vPlayer_scene_t_park_coord")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				#ENDIF
				
			ELSE
				vParkEndPtfxOffset = <<-0.1250, 1.1486, -0.0588>>
			ENDIF
			
		BREAK
		CASE PR_SCENE_T_SMOKEMETH
		CASE PR_SCENE_Ta_RC_MRSP2
			tPlayerParkSfxBank = ""
			tPlayerParkSfxSet = ""
			tPlayerParkSfxSound = ""
			
			fStartEffectTime_A = 0.19
			fStartEffectTime_B = 0.47
			
			ePtfxBonetag = BONETAG_HEAD
			tPlayerParkLoopPtfx = ""
			tPlayerParkPtfx = "scr_meth_pipe_smoke"
			tPlayerParkEndPtfx = ""
		BREAK
		CASE PR_SCENE_T6_DIGGING
			tPlayerParkSfxBank = ""
			tPlayerParkSfxSet = ""
			tPlayerParkSfxSound = ""
			
			fStartEffectTime_A = 0.22
			fStartEffectTime_B = 0.24
			
			ePtfxBonetag = BONETAG_NULL
			tPlayerParkLoopPtfx = ""
			tPlayerParkPtfx = "scr_pts_digging"
			tPlayerParkEndPtfx = ""
			
			ePropModel = PROP_TOOL_SHOVEL
			propOffset = <<0,0,-0.045>>		propRotation= <<0, 180,0>>
			eAttachBonetag = BONETAG_PH_R_HAND
			fDetachAnimPhase = 0.70
			thisSceneObjectAction = PSOA_0_detach
			
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			tPlayerParkSfxBank = ""
			tPlayerParkSfxSet = ""
			tPlayerParkSfxSound = ""
			
			fStartEffectTime_A = 0.50
			fStartEffectTime_B = 0.55
			
			ePtfxBonetag = BONETAG_R_FOOT
			tPlayerParkLoopPtfx = "scr_pts_flush"
			tPlayerParkPtfx = "scr_pts_footsplash"
			tPlayerParkEndPtfx = ""
			
//			ePropModel = PROP_TOOL_SHOVEL
//			propOffset = <<0,0,-0.045>>		propRotation= <<0, 180,0>>
//			eAttachBonetag = BONETAG_PH_R_HAND
//			fDetachAnimPhase = 0.70
//			thisSceneObjectAction = PSOA_0_detach
			
		BREAK
		CASE PR_SCENE_T_HEADINSINK
			tPlayerParkSfxBank = ""
			tPlayerParkSfxSet = ""
			tPlayerParkSfxSound = ""
			
			fStartEffectTime_A = 0.15
			fStartEffectTime_B = -1.0
			
			ePtfxBonetag = BONETAG_HEAD
			tPlayerParkLoopPtfx = ""
			tPlayerParkPtfx = "scr_pts_headsplash_trev"		//"scr_pts_headsplash"
			tPlayerParkEndPtfx = ""
			
			ePropModel = Prop_CS_Sink_Filler_02
			propOffset= <<-0.540,0.080,-0.130>>		//<<2.2270, -0.5880, 1.3750>>
			propRotation= <<0,0, -12>>
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.70
			thisSceneObjectAction = PSOA_NULL
		BREAK
		CASE PR_SCENE_M4_WASHFACE
			tPlayerParkSfxBank = ""
			tPlayerParkSfxSet = ""
			tPlayerParkSfxSound = ""
			
			fStartEffectTime_A = 0.07
			fStartEffectTime_B = -1.0
			
			ePtfxBonetag = BONETAG_PH_L_HAND
			tPlayerParkLoopPtfx = ""
			tPlayerParkPtfx = "scr_pts_headsplash"
			tPlayerParkEndPtfx = ""
			
			ePropModel = Prop_CS_Sink_Filler_03
			propOffset= <<-0.720,0.900,0.210>>
			propRotation= <<0,0, 9.000>>
			eAttachBonetag = BONETAG_NULL
			fDetachAnimPhase = 0.70
			thisSceneObjectAction = PSOA_NULL
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			STRING sScene
			TEXT_LABEL_63 str
			sScene = Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_park_scene)
			str = "invalid selected_player_scene_t_park_scene: "
			str += (GET_STRING_FROM_STRING(sScene,
					GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
					GET_LENGTH_OF_LITERAL_STRING(sScene)))
			
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
		BREAK
	ENDSWITCH
	
	//-- structs : PS_T_PARK_STRUCT --//
	
	PED_SCENE_STRUCT sPedScene
	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	
	sPedScene.iStage = 0
	sPedScene.eScene = selected_player_scene_t_park_scene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
	
	TEXT_LABEL_63 tPlayerAnimLoop
	ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
	GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_t_park_scene,
			tPlayerParkAnimDict, tPlayerAnimLoop, tPlayerParkAnimOut,
			playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_T_Park()
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	REQUEST_PTFX_ASSET()
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	#IF IS_DEBUG_BUILD
	INT iLoadingAssets = 0
	#ENDIF
	
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		AND (GET_PLAYER_SWITCH_STATE() <> SWITCH_STATE_JUMPCUT_DESCENT
		OR GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_PREP_DESCENT)
			bAllAssetsLoaded = FALSE
				
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("wait for descent")
			DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
			#ENDIF
		ENDIF

		
		REQUEST_PTFX_ASSET()
		IF NOT HAS_PTFX_ASSET_LOADED()
			bAllAssetsLoaded = FALSE
				
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("HAS_PTFX_ASSET_LOADED()")
			DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
			#ENDIF
			
		ENDIF
		
		//
		IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerParkSfxBank)
			IF NOT REQUEST_AMBIENT_AUDIO_BANK(tPlayerParkSfxBank)
				bAllAssetsLoaded = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("REQUEST_AMBIENT_AUDIO_BANK(")
				str += (tPlayerParkSfxBank)
				str += (")")
				DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
			ENDIF
		ENDIF
		
		//
		IF NOT (ePropModel = DUMMY_MODEL_FOR_SCRIPT)
			IF NOT DOES_ENTITY_EXIST(player_prop)

				REQUEST_MODEL(ePropModel)
				IF NOT HAS_MODEL_LOADED(ePropModel)
					bAllAssetsLoaded = FALSE
					REQUEST_MODEL(ePropModel)
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("REQUEST_MODEL(")
					str += (GET_MODEL_NAME_FOR_DEBUG(ePropModel))
					str += (")")
					DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
				ELSE
					IF (eAttachBonetag <> BONETAG_NULL)
						IF IS_PLAYER_SWITCH_IN_PROGRESS()
						AND (GET_PLAYER_SWITCH_STATE() <> SWITCH_STATE_JUMPCUT_DESCENT
						OR GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_PREP_DESCENT)
				
							bAllAssetsLoaded = FALSE
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str = ("wait for descent")
							DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
							iLoadingAssets++
							#ENDIF
						ELSE
							player_prop = CREATE_OBJECT(ePropModel, vPlayer_scene_t_park_coord)
							ATTACH_ENTITY_TO_ENTITY(player_prop, PLAYER_PED_ID(),
									GET_PED_BONE_INDEX(PLAYER_PED_ID(), eAttachBonetag),
									propOffset, propRotation)
						ENDIF
					ELSE
						player_prop = CREATE_OBJECT(ePropModel, vPlayer_scene_t_park_coord+propOffset)
						SET_ENTITY_COORDS(player_prop, vPlayer_scene_t_park_coord+propOffset)
						SET_ENTITY_ROTATION(player_prop, <<0,0,fPlayer_scene_t_park_head>>+propRotation)
						
						FREEZE_ENTITY_POSITION(player_prop, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bAllAssetsLoaded
		
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("iWaitForBuddyAssets: ")
			str += iWaitForBuddyAssets
			DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
			#ENDIF
			
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_T_Park_Stage(PS_T_PARK_STAGE_ENUM this_player_scene_t_park_stage)
	SWITCH this_player_scene_t_park_stage
		CASE PS_T_PARK_switchInProgress
			RETURN "PS_T_PARK_switchInProgress"
		BREAK
		CASE PS_T_PARK_STAGE_TWO
			RETURN "PS_T_PARK_STAGE_TWO"
		BREAK
		
		CASE FINISHED_T_PARK
			RETURN "FINISHED_T_PARK"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_T_Park_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_T_Park_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_t_park.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_park_scene)
	
	player_scene_t_park_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_T_PARK_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_T_Park_Stage(INT_TO_ENUM(PS_T_PARK_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_t_park_stage", iCurrent_player_scene_t_park_stage)
		
		IF NOT (ePropModel = DUMMY_MODEL_FOR_SCRIPT)
			ADD_WIDGET_VECTOR_SLIDER("propOffset", propOffset, -5, 5, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("propRotation", propRotation, -180, 180, 1.0)
			ADD_WIDGET_BOOL("bMoveProp", bMoveProp)
		ENDIF
		
		ADD_WIDGET_BOOL("bLoopieLoopLoop", bLoopieLoopLoop)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_Park_Widget()
	iCurrent_player_scene_t_park_stage = ENUM_TO_INT(current_player_scene_t_park_stage)
	
	IF bMoveProp
		WHILE bMoveProp
			
			IF DOES_ENTITY_EXIST(player_prop)
		
				IF (eAttachBonetag <> BONETAG_NULL)
					ATTACH_ENTITY_TO_ENTITY(player_prop, PLAYER_PED_ID(),
							GET_PED_BONE_INDEX(PLAYER_PED_ID(), eAttachBonetag),
							propOffset, propRotation)
				ELSE
					SET_ENTITY_COORDS(player_prop, vPlayer_scene_t_park_coord+propOffset)
					SET_ENTITY_ROTATION(player_prop, <<0,0,fPlayer_scene_t_park_head>>+propRotation)
				ENDIF
			ELSE
				bMoveProp = FALSE
			ENDIF
		
			WAIT(0)
		ENDWHILE
		
		
		SAVE_STRING_TO_DEBUG_FILE("propOffset: ")
		SAVE_VECTOR_TO_DEBUG_FILE(propOffset)
		SAVE_STRING_TO_DEBUG_FILE(", propRotation: ")
		SAVE_VECTOR_TO_DEBUG_FILE(propRotation)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_T_Park_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_t_park_stage = FINISHED_T_PARK
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	CUTSCENE FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_T_PARK_switchInProgress()
	
	IF NOT (ePropModel = DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(player_prop)
			REQUEST_MODEL(ePropModel)
			IF NOT HAS_MODEL_LOADED(ePropModel)
				REQUEST_MODEL(ePropModel)
			ELSE
				IF (eAttachBonetag <> BONETAG_NULL)
					player_prop = CREATE_OBJECT(ePropModel, vPlayer_scene_t_park_coord)
					ATTACH_ENTITY_TO_ENTITY(player_prop, PLAYER_PED_ID(),
							GET_PED_BONE_INDEX(PLAYER_PED_ID(), eAttachBonetag),
							propOffset, propRotation)
				ELSE
					player_prop = CREATE_OBJECT(ePropModel, vPlayer_scene_t_park_coord+propOffset)
					SET_ENTITY_COORDS(player_prop, vPlayer_scene_t_park_coord+propOffset)
					SET_ENTITY_ROTATION(player_prop, <<0,0,fPlayer_scene_t_park_head>>+propRotation)
					
					FREEZE_ENTITY_POSITION(player_prop, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
	
		IF DOES_ENTITY_EXIST(player_prop)
			IF (eAttachBonetag <> BONETAG_NULL)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(player_prop, PLAYER_PED_ID())
					ATTACH_ENTITY_TO_ENTITY(player_prop, PLAYER_PED_ID(),
							GET_PED_BONE_INDEX(PLAYER_PED_ID(), eAttachBonetag),
							propOffset, propRotation)
				ENDIF
			ENDIF
		ENDIF
		
		
		RETURN TRUE
	ELSE
		//
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_T_PARK_STAGE_TWO()
	
	#IF IS_DEBUG_BUILD
	CONST_INT iCONST_PARK_literal_row	8
	#ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerParkLoopPtfx)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptLoop)
			VECTOR vParkLoopPtfxCoord
			vParkLoopPtfxCoord = <<1971.620, 3819.650, 32.856>>-vPlayer_scene_t_park_coord
			ptLoop = START_PARTICLE_FX_LOOPED_AT_COORD(tPlayerParkLoopPtfx,
									vPlayer_scene_t_park_coord+vParkLoopPtfxCoord,
									<<0,0,0>>, 1)
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("ptLoop exists", 1+iCONST_PARK_literal_row, HUD_COLOUR_BLUELIGHT)
			#ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerParkAnimDict, tPlayerParkAnimOut, ANIM_SYNCED_SCENE)
			
			FLOAT fPlayerParkAnimOutTime
		//	fPlayerParkAnimOutTime = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), tPlayerParkAnimDict, tPlayerParkAnimOut)
			fPlayerParkAnimOutTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("playing anim: ", fPlayerParkAnimOutTime, 2+iCONST_PARK_literal_row, HUD_COLOUR_BLUELIGHT)
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			FLOAT ReturnStartPhase, ReturnEndPhase
			IF FIND_ANIM_EVENT_PHASE(tPlayerParkAnimDict, tPlayerParkAnimOut, "puke_a",
					ReturnStartPhase, ReturnEndPhase)
			OR FIND_ANIM_EVENT_PHASE(tPlayerParkAnimDict, tPlayerParkAnimOut, "puke_b",
					ReturnStartPhase, ReturnEndPhase)
				SCRIPT_ASSERT("puke anim events exist!")
			ENDIF
			#ENDIF
			
			SWITCH iSprayStage
				CASE 0
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString(tPlayerParkPtfx, 3+iCONST_PARK_literal_row, HUD_COLOUR_GREEN)
					DrawLiteralSceneStringFloat("fVOM_A: ", fStartEffectTime_A, 4+iCONST_PARK_literal_row, HUD_COLOUR_GREEN)
					#ENDIF
					
					IF (fStartEffectTime_A < 0)
						iSprayStage++
						RETURN FALSE
					ENDIF
					
					IF (fPlayerParkAnimOutTime > fStartEffectTime_A)
						
						IF NOT DOES_ENTITY_EXIST(player_prop)
						OR (ePtfxBonetag <> BONETAG_NULL)
							IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(tPlayerParkPtfx,
									PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, ePtfxBonetag, 1)
								
								IF ARE_STRINGS_EQUAL(tPlayerParkPtfx, "scr_meth_pipe_smoke")
									Player_Takes_Weed_Hit(PLAYER_PED_ID())
								ENDIF
								
								iPlayerParkSfxID = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iPlayerParkSfxID, tPlayerParkSfxSound, PLAYER_PED_ID(), tPlayerParkSfxSet)
								iSprayStage++
							ENDIF
						ELSE
							IF START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(tPlayerParkPtfx,
									player_prop, <<0,0,-1>>, <<0,0,0>>)
								
								IF ARE_STRINGS_EQUAL(tPlayerParkPtfx, "scr_meth_pipe_smoke")
									Player_Takes_Weed_Hit(PLAYER_PED_ID())
								ENDIF
								
								iPlayerParkSfxID = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iPlayerParkSfxID, tPlayerParkSfxSound, PLAYER_PED_ID(), tPlayerParkSfxSet)
								iSprayStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString(tPlayerParkPtfx, 3+iCONST_PARK_literal_row, HUD_COLOUR_ORANGE)
					DrawLiteralSceneStringFloat("fVOM_B: ", fStartEffectTime_B, 4+iCONST_PARK_literal_row, HUD_COLOUR_ORANGE)
					#ENDIF
					
					IF (fStartEffectTime_B < 0)
						iSprayStage++
						RETURN FALSE
					ENDIF
					
					IF (fPlayerParkAnimOutTime > fStartEffectTime_B)
						
						IF NOT DOES_ENTITY_EXIST(player_prop)
						OR (ePtfxBonetag <> BONETAG_NULL)
							IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(tPlayerParkPtfx,
									PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, ePtfxBonetag, 1)
								
								IF ARE_STRINGS_EQUAL(tPlayerParkPtfx, "scr_meth_pipe_smoke")
									Player_Takes_Weed_Hit(PLAYER_PED_ID())
								ENDIF
								
								iPlayerParkSfxID = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iPlayerParkSfxID, tPlayerParkSfxSound, PLAYER_PED_ID(), tPlayerParkSfxSet)
								iSprayStage++
							ENDIF
						ELSE
							IF START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(tPlayerParkPtfx,
									player_prop, <<0,0,-1>>, <<0,0,0>>)
								
								IF ARE_STRINGS_EQUAL(tPlayerParkPtfx, "scr_meth_pipe_smoke")
									Player_Takes_Weed_Hit(PLAYER_PED_ID())
								ENDIF
								
								iPlayerParkSfxID = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iPlayerParkSfxID, tPlayerParkSfxSound, PLAYER_PED_ID(), tPlayerParkSfxSet)
								iSprayStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("fVOM_END (extra ptfx): ", 4+iCONST_PARK_literal_row, HUD_COLOUR_BLUELIGHT)
					#ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerParkEndPtfx)
						IF START_PARTICLE_FX_NON_LOOPED_AT_COORD(tPlayerParkEndPtfx,
								vPlayer_scene_t_park_coord+vParkEndPtfxOffset,
								<<0,0,0>>, 1)
							
							#IF IS_DEBUG_BUILD
							SAVE_STRING_TO_DEBUG_FILE("START_PARTICLE_FX_NON_LOOPED_AT_COORD(\"")
							SAVE_STRING_TO_DEBUG_FILE(tPlayerParkEndPtfx)
							SAVE_STRING_TO_DEBUG_FILE("\", vPlayer_scene_t_park_coord+")
							SAVE_VECTOR_TO_DEBUG_FILE(vParkEndPtfxOffset)
							SAVE_STRING_TO_DEBUG_FILE(", <<0,0,0>>, 1)")
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
							
							iSprayStage++
						ENDIF
					ELSE
						iSprayStage++
					ENDIF
				BREAK
				
				CASE 3
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("fVOM_END", 4+iCONST_PARK_literal_row, HUD_COLOUR_REDDARK, 0.75)
					#ENDIF
					
					
					#IF IS_DEBUG_BUILD
					IF bLoopieLoopLoop
						IF (fPlayerParkAnimOutTime >= 0.99)
						OR (fPlayerParkAnimOutTime < fStartEffectTime_A)
							SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Exit_SynchSceneID, TRUE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
							
							iSprayStage = 0
							RETURN FALSE
						ENDIF
					ENDIF
					#ENDIF
					
				BREAK
			ENDSWITCH
			
			
			IF DOES_ENTITY_EXIST(player_prop)
				IF IS_ENTITY_ATTACHED(player_prop)
					IF (thisSceneObjectAction <> PSOA_NULL)
						IF (fPlayerParkAnimOutTime > fDetachAnimPhase)
							SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("player_prop (attached)", 5+iCONST_PARK_literal_row, HUD_COLOUR_GREENLIGHT)
					DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), player_prop, HUD_COLOUR_GREENLIGHT)
				ELSE
					DrawLiteralSceneString("player_prop (detached)", 5+iCONST_PARK_literal_row, HUD_COLOUR_REDLIGHT)
					DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), player_prop, HUD_COLOUR_REDLIGHT)
					#ENDIF
					
				ENDIF
				
				
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("player_prop (none)", 5+iCONST_PARK_literal_row, HUD_COLOUR_REDDARK)
				#ENDIF
				
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("NOT playing anim: ", iCONST_PARK_literal_row+2, HUD_COLOUR_REDLIGHT)
			#ENDIF
			
			IF (ptSpray <> NULL)
				STOP_PARTICLE_FX_LOOPED(ptSpray)
				ptSpray = NULL
				
				STOP_SOUND(iPlayerParkSfxID)
				RELEASE_SOUND_ID(iPlayerParkSfxID)
			ENDIF
			
			IF DOES_ENTITY_EXIST(player_prop)
				IF IS_ENTITY_ATTACHED(player_prop)
					IF (thisSceneObjectAction <> PSOA_NULL)
						SET_SCENE_OBJECT_AS_NO_LONGER_NEEDED(player_prop, thisSceneObjectAction)
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bLoopieLoopLoop
				
				iSprayStage = 0
				RETURN FALSE
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("player dead: ", iCONST_PARK_literal_row+2, HUD_COLOUR_REDLIGHT)
		#ENDIF
		
		Player_Scene_T_Park_Cleanup()
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_T_Park_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_T_Park_Variables()
	Setup_Player_Scene_T_Park()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_T_Park_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_T_Park_in_progress
	AND ProgressScene(BIT_MICHAEL|BIT_TREVOR, NULL)
		WAIT(0)
		
//		Control_Player_Scene_T_Park_Cutscene()
		
		SWITCH current_player_scene_t_park_stage
			CASE PS_T_PARK_switchInProgress
				IF Do_PS_T_PARK_switchInProgress()
					current_player_scene_t_park_stage = PS_T_PARK_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_T_PARK_STAGE_TWO
				IF Do_PS_T_PARK_STAGE_TWO()
					current_player_scene_t_park_stage = FINISHED_T_PARK
				ENDIF
			BREAK
			
			CASE FINISHED_T_PARK
				Player_Scene_T_Park_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_T_Park_Widget()
		Player_Scene_T_Park_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_t_park_scene,
				5+2, HUD_COLOUR_GREENLIGHT)
		STRING sStage = Get_String_From_Player_Scene_T_Park_Stage(current_player_scene_t_park_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_T_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				6+2, HUD_COLOUR_BLUELIGHT)
		#ENDIF
	ENDWHILE
	
	Player_Scene_T_Park_Cleanup()
ENDSCRIPT
