// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene M_SHOP file for use – player_scene_m_shopping.sc		 ___
// ___ 																					 ___
// _________________________________________________________________________________________



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSforshopping			5

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_M_SHOP_STAGE_ENUM
	PS_M_SHOP_switchInProgress,
	PS_M_SHOP_STAGE_TWO,
	
	FINISHED_M_SHOPPING
ENDENUM
PS_M_SHOP_STAGE_ENUM		current_player_scene_m_shopping_stage						= PS_M_SHOP_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_m_shopping_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation MyLocalPedStruct
TEXT_LABEL tCreatedConvLabels[5]
structTimer speechTimer

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_M_Shop_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
INT						iShopperStage

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
MODEL_NAMES			VehicleModelHashKey

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX		shopper_obj
MODEL_NAMES			shopperObj_model
VECTOR				vShopperObj_offset, vShopperObj_rotation

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_m_shop_coord
FLOAT				fPlayer_scene_m_shop_head

VECTOR				vShopper_wanderOffset
FLOAT				fShopper_wanderRadius

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tShopperSceneAnimDict, tShopperSceneAnimLoop, tShopperSceneAnimOut

TEXT_LABEL_63		tShopperScenario = ""
VECTOR				vShopperScenarioOffset
FLOAT				fShopperScenarioHead

FLOAT				fDismissMichaelPhase = -1

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//
SCENARIO_BLOCKING_INDEX ScenarioBlockingIndex

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fMicShopAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_m_shopping_widget
INT				iCurrent_player_scene_m_shopping_stage
BOOL			bMovePeds = FALSE, bUpdateScenario = FALSE
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_M_Shop_Cleanup()
//	INT iPed
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	IF (VehicleModelHashKey <> DUMMY_MODEL_FOR_SCRIPT)
		REMOVE_VEHICLE_ASSET(VehicleModelHashKey)
	ENDIF
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
//	REPEAT COUNT_OF(eShopperModel) iPed
//		IF (eShopperModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
//			SET_MODEL_AS_NO_LONGER_NEEDED(eShopperModel[iPed])
//		ENDIF
//	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_m_shopping_widget)
		DELETE_WIDGET_GROUP(player_scene_m_shopping_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_M_Shop_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_M_Shop_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_M_Shop_Variables()
	selected_player_scene_m_shopping_scene	= g_eRecentlySelectedScene	//PR_SCENE_m_BBSHOP_YARD	//PR_SCENE_m_BBSHOP_ALLEY
	
	TEXT_LABEL_31 tPlayer_scene_m_shop_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_m_shopping_scene, vPlayer_scene_m_shop_coord, fPlayer_scene_m_shop_head, tPlayer_scene_m_shop_room)
	
	SWITCH selected_player_scene_m_shopping_scene
		CASE PR_SCENE_M2_WIFEEXITSCAR
			//- vectors -//
			vShopper_wanderOffset = <<-25,-35,0>>		fShopper_wanderRadius = 30
			
			fDismissMichaelPhase = 0.73
			
			//- enums -//
			
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a
			//- vectors -//
			vShopper_wanderOffset = <<-1306.8362, -700.7733, 24.3224>> - vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 40
			
			//- enums -//
			iShopperStage = 2
			
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b
			//- vectors -//
			vShopper_wanderOffset = <<-566.2225, 272.6104, 82.0206+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 30
			
			//- enums -//
			iShopperStage = 2
			
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a
			//- vectors -//
			vShopper_wanderOffset = <<77.2686, -217.3210, 53.6418+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 25
			
			//- enums -//
			iShopperStage = 2
			
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b
			//- vectors -//
			vShopper_wanderOffset = <<412.1519, 313.2252, 102.0191+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 15
			
			//- enums -//
			iShopperStage = 2
			
		BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA
			//- vectors -//
			vShopper_wanderOffset = <<-616.7041, -275.1926, 37.8417+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 30
			
			//- enums -//
//			iShopperStage = 2
			
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			//- vectors -//
			vShopper_wanderOffset = <<-106.3491, -125.1416, 56.8469+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 25
			
			//- enums -//
			
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
			//- vectors -//
			vShopper_wanderOffset = <<-106.3491, -125.1416, 56.8469+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 25
			
			//- enums -//
			
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
			//- vectors -//
			vShopper_wanderOffset = <<-16.8925, 328.3010, 112.1615+1>>-vPlayer_scene_m_shop_coord
			fShopper_wanderRadius = 25
			
			//- enums -//
			
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			//- vectors -//
			vShopper_wanderOffset = <<0,0,0>>
			fShopper_wanderRadius = 0
			
			//- enums -//
			
			tShopperScenario = "PROP_HUMAN_SEAT_SUNLOUNGER"
			vShopperScenarioOffset = <<1.358,2.385,-0.608>>		//<<-1352.733,357.810,63.072>>-vPlayer_scene_m_shop_coord+<<0,0,0.5>>
			fShopperScenarioHead = 160.000						//135.0
			
			shopperObj_model = PROP_PATIO_LOUNGER1
			vShopperObj_offset = <<-1352.644,357.697,63.082>>	- <<-1353.7910, 355.1845, 64.0800>>
			vShopperObj_rotation = <<0.0000, 0.0000, 54.35		- 72.0000>>
			
		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			//- vectors -//
			vShopper_wanderOffset = <<0,0,0>>
			fShopper_wanderRadius = 0
			
			//- enums -//
			
			shopperObj_model = PROP_CHATEAU_CHAIR_01
			vShopperObj_offset = <<0.8706, 2.0176, -0.4948>>	//<<-0.7526, -0.9163, -0.4948>>
			vShopperObj_rotation = <<0.0000, 0.0000, 0.0000>>
			
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "IGNORABLE: invalid m_shopping_scene: "
			
			STRING sScene
			sScene = Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_shopping_scene)
			str += GET_STRING_FROM_STRING(sScene,
					GET_LENGTH_OF_LITERAL_STRING("PR_SCENE_"),
					GET_LENGTH_OF_LITERAL_STRING(sScene))
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			vShopper_wanderOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-50, 50),GET_RANDOM_FLOAT_IN_RANGE(-50, 50),0>>
			
			//- floats -//
			fShopper_wanderRadius = VMAG(vShopper_wanderOffset)*0.75
			
			//- enums -//
			
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_M_SHOP_STRUCT --//
	
	IF (selected_player_scene_m_shopping_scene <> PR_SCENE_M2_LUNCH_a)
	AND (selected_player_scene_m_shopping_scene <> PR_SCENE_M7_LOUNGECHAIRS)
	AND (selected_player_scene_m_shopping_scene <> PR_SCENE_M7_RESTAURANT)
		PED_VEH_DATA_STRUCT sVehData
		VECTOR vVehCoordsOffset
		FLOAT fVehHeadOffset
		VECTOR vDriveOffset
		FLOAT fDriveSpeed
		
		IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_MICHAEL, selected_player_scene_m_shopping_scene,
				sVehData, vVehCoordsOffset, fVehHeadOffset,
				vDriveOffset, fDriveSpeed)
			VehicleModelHashKey = sVehData.model
		ENDIF
	ELSE
		VehicleModelHashKey = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	
	IF (iShopperStage = 0)
		ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
		GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_m_shopping_scene,
				tShopperSceneAnimDict,
				tShopperSceneAnimLoop, tShopperSceneAnimOut,
				buddyAnimLoopFlag, buddyAnimOutFlag)
		iShopperStage = 0		//iCONST_SHOPPER_0_waitForSynchScene
	ELSE
		tShopperSceneAnimDict	= ""
		tShopperSceneAnimLoop	= ""
		tShopperSceneAnimOut	= ""
		
		iShopperStage = 2		//iCONST_SHOPPER_2_exitVehicle
	ENDIF
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_M_Shop()
//	INT iPed
	
	//- request models - peds -//
	//- request models - vehicles -//
	IF (VehicleModelHashKey <> DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_VEHICLE_ASSET(VehicleModelHashKey)
	ENDIF
	
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	BOOL bWaitForBuddyAssets = FALSE
	WHILE NOT bWaitForBuddyAssets
	AND (iWaitForBuddyAssets < 400)
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iRow = 1
		#ENDIF
		
		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str  = ("g_pScene_buddy doesnt exist")
			DrawLiteralSceneString(str, iRow+iCONSTANTSforshopping, HUD_COLOUR_RED)
			iRow++
			#ENDIF
			
			bWaitForBuddyAssets = FALSE
		ENDIF
		
		IF (VehicleModelHashKey <> DUMMY_MODEL_FOR_SCRIPT)
			IF NOT HAS_VEHICLE_ASSET_LOADED(VehicleModelHashKey)
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str  = ("VehicleModelHashKey not loaded")
				DrawLiteralSceneString(str, iRow+iCONSTANTSforshopping, HUD_COLOUR_RED)
				iRow++
				#ENDIF
				
				bWaitForBuddyAssets = FALSE
			ENDIF
		ENDIF
		
		IF NOT bWaitForBuddyAssets
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
	PRIVATE_SetDefaultFamilyMemberAttributes(g_pScene_buddy, RELGROUPHASH_FAMILY_M)
	SET_PED_CONFIG_FLAG(g_pScene_buddy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	SET_PED_CONFIG_FLAG(g_pScene_buddy, PCF_CanSayFollowedByPlayerAudio, TRUE)
	
//	ADD_RELATIONSHIP_GROUP("SHOPPERS", shopperGroup)
//	
//	REPEAT COUNT_OF(shopper_ped) iPed
//		IF (eShopperModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
//			shopper_ped[iPed] = CREATE_PED(PEDTYPE_MISSION, eShopperModel[iPed], vPlayer_scene_m_shop_coord+vShopper_coordOffset[iPed])
//			SET_ENTITY_HEADING(shopper_ped[iPed], fPlayer_scene_m_shop_head+fShopper_headOffset[iPed])
//			SET_PED_RANDOM_COMPONENT_VARIATION(shopper_ped[iPed])
//			
//			SET_PED_RELATIONSHIP_GROUP_HASH(shopper_ped[iPed], shopperGroup)
//			
//		ENDIF
//	ENDREPEAT
	
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, shopperGroup, RELGROUPHASH_PLAYER)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, shopperGroup)
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
//	IF (selected_player_scene_m_shopping_scene = PR_SCENE_M2_DROPOFFDAU_b)
//		SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_UNLOCKED)
//		SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
//	ENDIF
	
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_M_Shop_Stage(PS_M_SHOP_STAGE_ENUM this_player_scene_m_shopping_stage)
	SWITCH this_player_scene_m_shopping_stage
		CASE PS_M_SHOP_switchInProgress
			RETURN "PS_M_SHOP_switchInProgress"
		BREAK
		CASE PS_M_SHOP_STAGE_TWO
			RETURN "PS_M_SHOP_STAGE_TWO"
		BREAK
		
		CASE FINISHED_M_SHOPPING
			RETURN "FINISHED_M_SHOPPING"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_M_Shop_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_M_Shop_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_m_shopping.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_shopping_scene)
	
	player_scene_m_shopping_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_M_SHOP_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_M_Shop_Stage(INT_TO_ENUM(PS_M_SHOP_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_m_shopping_stage", iCurrent_player_scene_m_shopping_stage)
		
		START_WIDGET_GROUP("Do_PS_M_SHOP_STAGE_TWO")
			ADD_WIDGET_VECTOR_SLIDER("vPlayer_scene_m_shop_coord", vPlayer_scene_m_shop_coord, -3000.0, 3000.0, 0.0)
			ADD_WIDGET_FLOAT_SLIDER("fPlayer_scene_m_shop_head", fPlayer_scene_m_shop_head, -360.0, 360.0, 0.0)
			
//			REPEAT COUNT_OF(shopper_ped) iWidget
//				str = "shopper_ped "
//				str += iWidget
//				ADD_WIDGET_STRING(str)
//				ADD_WIDGET_VECTOR_SLIDER("vShopper_coordOffset", vShopper_coordOffset[iWidget], -30.0, 30.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fShopper_headOffset", fShopper_headOffset[iWidget], -180.0, 180.0, 1.0)
//				ADD_WIDGET_VECTOR_SLIDER("vShopper_wanderOffset", vShopper_wanderOffset[iWidget], -150.0, 150.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fShopper_wanderRadius", fShopper_wanderRadius[iWidget], 0.0, 50.0, 1.0)
//			ENDREPEAT
			
			ADD_WIDGET_STRING("buddy")
//				ADD_WIDGET_VECTOR_SLIDER("vShopper_coordOffset", vShopper_coordOffset[COUNT_OF(shopper_ped)], -30.0, 30.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fShopper_headOffset", fShopper_headOffset[COUNT_OF(shopper_ped)], -180.0, 180.0, 1.0)
			ADD_WIDGET_VECTOR_SLIDER("vShopper_wanderOffset",
					vShopper_wanderOffset,	//COUNT_OF(shopper_ped)],
					-150.0, 150.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fShopper_wanderRadius",
					fShopper_wanderRadius,	//COUNT_OF(shopper_ped)],
					0.0, 50.0, 1.0)
			
			ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
			
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tShopperScenario)
				ADD_WIDGET_STRING(tShopperScenario)
				ADD_WIDGET_VECTOR_SLIDER("vShopperScenarioOffset", vShopperScenarioOffset, -10.0, 10.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fShopperScenarioHead", fShopperScenarioHead, -180.0, 180.0, 1.0)
				
				ADD_WIDGET_BOOL("bUpdateScenario", bUpdateScenario)
			ENDIF
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_M_Shop_Widget()
//	INT iPed
	iCurrent_player_scene_m_shopping_stage = ENUM_TO_INT(current_player_scene_m_shopping_stage)
	
	IF NOT bMovePeds
	ELSE

		IF NOT (g_iDebugSelectedFriendConnDisplay > 0)
			g_iDebugSelectedFriendConnDisplay = 1
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		
		
			IF NOT IS_PED_INJURED(g_pScene_buddy)
				IF NOT IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tShopperSceneAnimDict, tShopperSceneAnimLoop, ANIM_SYNCED_SCENE)
					TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, g_iPlayer_Timetable_Loop_SynchSceneID,
							tShopperSceneAnimDict, tShopperSceneAnimLoop,
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				
					iShopperStage = 0
				ENDIF
				
//				vShopper_coordOffset = GET_ENTITY_COORDS(g_pScene_buddy) - vPlayer_scene_t_fight_coord + <<0,0,-1>>
//				fShopper_headOffset = GET_ENTITY_HEADING(g_pScene_buddy) - fPlayer_scene_t_fight_head
//
//				IF fShopper_headOffset > 180.0
//					fShopper_headOffset -= 360.0
//				ENDIF
//				IF fShopper_headOffset < -180.0
//					fShopper_headOffset += 360.0
//				ENDIF
			ENDIF
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			IF NOT IS_PED_INJURED(g_pScene_buddy)
				IF NOT IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tShopperSceneAnimDict, tShopperSceneAnimOut, ANIM_SYNCED_SCENE)
					TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, g_iPlayer_Timetable_exit_SynchSceneID,
							tShopperSceneAnimDict, tShopperSceneAnimOut,
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				ENDIF
			ENDIF
			
		ELSE
			IF NOT IS_PED_INJURED(g_pScene_buddy)
	//			CLEAR_PED_TASKS(g_pScene_buddy)
				
//				SET_ENTITY_HEADING(g_pScene_buddy, fPlayer_scene_t_fight_head+fShopper_headOffset)
//				SET_ENTITY_COORDS(g_pScene_buddy, vPlayer_scene_t_fight_coord+vShopper_coordOffset)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bUpdateScenario
		IF NOT IS_PED_INJURED(g_pScene_buddy)
//			CLEAR_PED_TASKS_IMMEDIATELY(g_pScene_buddy)
			SET_ENTITY_COORDS(g_pScene_buddy, vPlayer_scene_m_shop_coord + vShopperScenarioOffset)
			SET_ENTITY_HEADING(g_pScene_buddy, fPlayer_scene_m_shop_head + fShopperScenarioHead)
			
			TASK_START_SCENARIO_AT_POSITION(g_pScene_buddy, tShopperScenario,
					vPlayer_scene_m_shop_coord + vShopperScenarioOffset,
					fPlayer_scene_m_shop_head + fShopperScenarioHead, 0)
					
			DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopperScenarioOffset, 0.25)
			
			VECTOR VHEADOFFSET = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS( vPlayer_scene_m_shop_coord + vShopperScenarioOffset, fPlayer_scene_m_shop_head + fShopperScenarioHead, <<0,1,0>>)
			
			DrawDebugSceneLineBetweenCoords(vPlayer_scene_m_shop_coord + vShopperScenarioOffset, VHEADOFFSET)
			DrawDebugSceneSphere(VHEADOFFSET, 0.25, HUD_COLOUR_BLACK)
		ELSE
			bUpdateScenario = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_M_Shop_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_m_shopping_stage = FINISHED_M_SHOPPING
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL ShouldShopperFleeFromPlayer(PED_INDEX thisShopperPed, BOOL bCheckPlayerFuckingAbout = TRUE)
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisShopperPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_COMBAT(thisShopperPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF bCheckPlayerFuckingAbout
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			RETURN TRUE
		ENDIF
		
		IF HAS_PED_RECEIVED_EVENT(thisShopperPed, EVENT_SHOT_FIRED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
BOOL bGeneraredShopperWander = FALSE
#ENDIF


FUNC BOOL CanShopperBeDeleted(PED_INDEX &thisShopperPed)
	
	CONST_FLOAT fCONST_DONT_DELETE_DIST	75.0
	
	VECTOR thisShopperPed_coord = GET_ENTITY_COORDS(thisShopperPed)
	VECTOR playerPed_coord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF VDIST2(thisShopperPed_coord, playerPed_coord) < (fCONST_DONT_DELETE_DIST*fCONST_DONT_DELETE_DIST)	
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ON_SCREEN(thisShopperPed)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "CanShopperBeDeleted(", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(thisShopperPed)), "): TRUE")
	#ENDIF
	
	SET_ENTITY_AS_MISSION_ENTITY(thisShopperPed, TRUE, TRUE)
	DELETE_PED(thisShopperPed)
	RETURN TRUE
ENDFUNC

PROC UpdateMicShopPedStage(INT iNewShopperStage, STRING sNewStageString = NULL)
	
	#IF IS_DEBUG_BUILD
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sNewStageString)
		CPRINTLN(DEBUG_SWITCH, "iShopperStage[", GET_PLAYER_PED_STRING(GET_NPC_PED_ENUM(g_pScene_buddy)), "] = ", iShopperStage, " -> ", iNewShopperStage, ": ", sNewStageString)
	ELSE
		CPRINTLN(DEBUG_SWITCH, "iShopperStage[", GET_PLAYER_PED_STRING(GET_NPC_PED_ENUM(g_pScene_buddy)), "] = ", iShopperStage, " -> ", iNewShopperStage)
	ENDIF
	#ENDIF
	
	sNewStageString = sNewStageString
	iShopperStage = iNewShopperStage
ENDPROC

INT iHassleTimer = -1, iHassleCount = 0
BOOL bSentGriefText

PROC PlayMicShopPedGreet(PED_INDEX thisShopperPed)
	IF IS_PED_INJURED(thisShopperPed)
		
		IF NOT bSentGriefText
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisShopperPed, PLAYER_PED_ID())
			// Should the player get an angry text message from the hospital?
			
			enumCharacterList	eChar = GET_NPC_PED_ENUM(thisShopperPed)
			enumFriend			eFriend = GET_FRIEND_FROM_CHAR(eChar)
			CC_CommID			eCommID = GET_COMM_ID_FOR_FRIEND_FAIL(eFriend)

			IF eCommID != COMM_NONE
			    CC_CodeID eHospitalCID = Private_GetHospitalChargeCID(thisShopperPed)
			    
			    // Send angry text message, if allowed, and not already an angry message queued
			    IF eHospitalCID != CID_BLANK
					IF NOT IS_COMMUNICATION_REGISTERED(eCommID)
					OR GET_COMMUNICATION_STATUS(eCommID) = CS_ERROR
						
						Store_Vector_ID_Data(VID_DYNAMIC_FRIEND_HOSPITAL, GET_ENTITY_COORDS(thisShopperPed, FALSE), 250.0)
						IF REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(g_eDefaultPlayerChar, eChar, FTM_FRIEND_HOSPITAL, FALSE, eHospitalCID, eCommID, VID_DYNAMIC_FRIEND_HOSPITAL)
							bSentGriefText = TRUE
						ENDIF
					ENDIF
			    ENDIF
			ENDIF
			
		ENDIF
		
		EXIT
	ENDIF
	
	Play_Greet_Player_Family_Context(thisShopperPed, FE_ANY_wander_family_event,
			MyLocalPedStruct, "FMMAUD", tCreatedConvLabels,
			speechTimer, CONV_PRIORITY_AMBIENT_MEDIUM)
	
	
	// #1529975
	CONST_FLOAT fMIN_HASSLE_DIST	8.0
	FLOAT fHassleDist2 = VDIST2(GET_ENTITY_COORDS(thisShopperPed), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	IF fHassleDist2 > (fMIN_HASSLE_DIST*fMIN_HASSLE_DIST)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringFloat("fHassleDist: ", SQRT(fHassleDist2), 7+iCONSTANTSforshopping, HUD_COLOUR_PURPLE, fMicShopAlphaMult)
		#ENDIF
		
		iHassleTimer = -1
	ELIF IS_ANY_SPEECH_PLAYING(thisShopperPed)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("speech playing", 7+iCONSTANTSforshopping, HUD_COLOUR_PURPLE, fMicShopAlphaMult)
		#ENDIF
		
		iHassleTimer = -1
	ELSE
		IF iHassleTimer <= 0
			iHassleTimer = GET_GAME_TIMER()
		ENDIF
		
		CONST_INT iMIN_FIRST_HASSLE_TIME	15000	//15s
		CONST_INT iMIN_EXTRA_HASSLE_TIME	10000	//10s
		
		IF iHassleTimer > 0
			
			IF iHassleCount = 0
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("iFirstHassle: ", GET_GAME_TIMER()-iHassleTimer, 7+iCONSTANTSforshopping, HUD_COLOUR_ORANGELIGHT, fMicShopAlphaMult)
				#ENDIF
				
				IF (iHassleTimer+iMIN_FIRST_HASSLE_TIME) < GET_GAME_TIMER()
					PLAY_PED_AMBIENT_SPEECH(thisShopperPed, "FRIEND_FOLLOWED_BY_PLAYER")
					
					iHassleTimer = -1
					iHassleCount++
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("iMultHassle: ", GET_GAME_TIMER()-iHassleTimer, 7+iCONSTANTSforshopping, HUD_COLOUR_ORANGEDARK, fMicShopAlphaMult)
				#ENDIF
				
				IF (iHassleTimer+iMIN_EXTRA_HASSLE_TIME) < GET_GAME_TIMER()
					PLAY_PED_AMBIENT_SPEECH(thisShopperPed, "HIT_BY_PLAYER")
					
					iHassleTimer = -1
					iHassleCount++
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL ControlMicShopPedStage(PED_INDEX &thisShopperPed)
	
	CONST_INT	iCONST_SHOPPER_0_waitForSynchScene		0
	CONST_INT	iCONST_SHOPPER_1_synchSceneRunning		1
					
	CONST_INT	iCONST_SHOPPER_2_exitVehicle			2
	CONST_INT	iCONST_SHOPPER_3_followNavmesh			3
	CONST_INT	iCONST_SHOPPER_4_wanderInArea			4
	CONST_INT	iCONST_SHOPPER_4_useNearScenario		5
	CONST_INT	iCONST_SHOPPER_6_useScenarioAtPos		6
	
	CONST_INT	iCONST_SHOPPER_7_keepAnimation			7
	
	CONST_INT	iCONST_SHOPPER_8_fleePlayer				8
					
	CONST_INT	iCONST_SHOPPER_ON_FIRE					-1
	CONST_INT	iCONST_SHOPPER_DEAD						-2
	
	#IF IS_DEBUG_BUILD
	INT			iShopperPedRow							= 0+4+iCONSTANTSforshopping
	#ENDIF
	
	IF NOT IS_PED_INJURED(thisShopperPed)
		
		IF NOT ProgressScene(BIT_MICHAEL, thisShopperPed)
			SET_ENTITY_AS_MISSION_ENTITY(thisShopperPed, TRUE, TRUE)
			DELETE_PED(thisShopperPed)
			
			CPRINTLN(DEBUG_SWITCH, "ProgressScene - false")
			
			UpdateMicShopPedStage(iCONST_SHOPPER_DEAD, "ped dead baby, ped dead")
			RETURN FALSE
		ENDIF
		
		IF (iShopperStage >= iCONST_SHOPPER_0_waitForSynchScene)
			IF IS_ENTITY_ON_FIRE(thisShopperPed)
				IF (GET_SCRIPT_TASK_STATUS(thisShopperPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
					STOP_ANIM_PLAYBACK(thisShopperPed)
				ENDIF
				CLEAR_PED_TASKS(thisShopperPed)
				
				UpdateMicShopPedStage(iCONST_SHOPPER_ON_FIRE, "ped on fire")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SWITCH iShopperStage
			CASE iCONST_SHOPPER_0_waitForSynchScene		//wait for synch scene to run
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
				OR IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  synch scene running???",
							iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					#ENDIF
					
					UpdateMicShopPedStage(iCONST_SHOPPER_1_synchSceneRunning, "loop or exit synch started")
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  wait for synch scene to run",
							iShopperPedRow, HUD_COLOUR_REDLIGHT, fMicShopAlphaMult)
					#ENDIF
					
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_1_synchSceneRunning	//synch scene running
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  loop synch scene...",
							iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					#ENDIF
					
				ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
					TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
					
					IF NOT IS_ENTITY_PLAYING_ANIM(thisShopperPed, tShopperSceneAnimDict, tShopperSceneAnimOut, ANIM_SYNCED_SCENE)
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  exit synch scene...",
								iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
						#ENDIF
						
						CLEAR_PED_TASKS(thisShopperPed)
						STOP_SYNCHRONIZED_ENTITY_ANIM(thisShopperPed, FAST_BLEND_OUT, TRUE)
						
						SET_PED_MOVE_ANIMS_BLEND_OUT(thisShopperPed)
						
//						VECTOR vShopper_wanderNormal
//						vShopper_wanderNormal = vShopper_wanderOffset / VMAG(vShopper_wanderOffset)
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
								vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
								PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
						FORCE_PED_MOTION_STATE(thisShopperPed, MS_ON_FOOT_WALK)
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(thisShopperPed)
						
						CLEAR_RAGDOLL_BLOCKING_FLAGS(thisShopperPed, RBF_VEHICLE_IMPACT)
						
						UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "not playing exit synch anim")
						RETURN TRUE
					ELSE
						FLOAT CurrentExitPhase
						CurrentExitPhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
						
						FLOAT ReturnStartPhase, ReturnEndPhase
						IF NOT FIND_ANIM_EVENT_PHASE(tShopperSceneAnimDict, tShopperSceneAnimOut,
								"WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneStringFloat("  exit synch ", CurrentExitPhase,
									iShopperPedRow, HUD_COLOUR_YELLOWLIGHT, fMicShopAlphaMult)
							#ENDIF
						ELSE
							
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneStringFloat("  Interrupt ", ReturnStartPhase,
									iShopperPedRow, HUD_COLOUR_YELLOWDARK, fMicShopAlphaMult)
							#ENDIF
							
							IF (fDismissMichaelPhase >= 0)
								IF (CurrentExitPhase >= fDismissMichaelPhase)
									
									#IF IS_DEBUG_BUILD
									IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
									OR IS_ANY_SPEECH_PLAYING(thisShopperPed)
										fDismissMichaelPhase *= GET_RANDOM_FLOAT_IN_RANGE(1.0100, 1.0900)
										
										SAVE_STRING_TO_DEBUG_FILE("fDismissMichaelPhase = ")
										SAVE_FLOAT_TO_DEBUG_FILE(fDismissMichaelPhase)
										SAVE_NEWLINE_TO_DEBUG_FILE()
										
										CPRINTLN(DEBUG_SWITCH, "fDismissMichaelPhase = ", fDismissMichaelPhase)
										
										SCRIPT_ASSERT("speech going during fDismissMichaelPhase")
										RETURN FALSE
									ENDIF
									#ENDIF
									
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(thisShopperPed, "DISMISS_MICHAEL", "AMANDA_NORMAL")
									fDismissMichaelPhase = -1
								ENDIF
							ENDIF
							
							IF (CurrentExitPhase >= ReturnStartPhase)
								CLEAR_PED_TASKS(thisShopperPed)
								STOP_SYNCHRONIZED_ENTITY_ANIM(thisShopperPed, FAST_BLEND_OUT, TRUE)
								
								SET_PED_MOVE_ANIMS_BLEND_OUT(thisShopperPed)
								
//								VECTOR vShopper_wanderNormal
//								vShopper_wanderNormal = vShopper_wanderOffset / VMAG(vShopper_wanderOffset)
								
								TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
										vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
										PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
								FORCE_PED_MOTION_STATE(thisShopperPed, MS_ON_FOOT_WALK)
								
								CLEAR_RAGDOLL_BLOCKING_FLAGS(thisShopperPed, RBF_VEHICLE_IMPACT)
								
								UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "WalkInterruptible phase reached")
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  synch scene NOT running???",
							iShopperPedRow, HUD_COLOUR_REDLIGHT, fMicShopAlphaMult)
					#ENDIF
					
					IF (selected_player_scene_m_shopping_scene <> PR_SCENE_M7_RESTAURANT)
//					AND (selected_player_scene_m_shopping_scene <> PR_SCENE_M2_LUNCH_a)
//					AND (selected_player_scene_m_shopping_scene <> PR_SCENE_M7_LOUNGECHAIRS)
						CLEAR_PED_TASKS(thisShopperPed)
						
						IF IS_STRING_NULL_OR_EMPTY(tShopperScenario)
							TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
									vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
									PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
							FORCE_PED_MOTION_STATE(thisShopperPed, MS_ON_FOOT_WALK)
							
							CLEAR_RAGDOLL_BLOCKING_FLAGS(thisShopperPed, RBF_VEHICLE_IMPACT)
							
							UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "not playing synch scene (navmesh)")
							RETURN TRUE
						ELSE
							TASK_START_SCENARIO_AT_POSITION(thisShopperPed, tShopperScenario,
									vPlayer_scene_m_shop_coord + vShopperScenarioOffset,
									fPlayer_scene_m_shop_head + fShopperScenarioHead, 0, FALSE)
							
							UpdateMicShopPedStage(iCONST_SHOPPER_6_useScenarioAtPos, "not playing synch scene (scenario)")
							RETURN TRUE
						ENDIF
					ELSE
						UpdateMicShopPedStage(iCONST_SHOPPER_7_keepAnimation, "not playing synch scene (anim)")
						RETURN TRUE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE iCONST_SHOPPER_2_exitVehicle	//
				IF IS_PED_IN_ANY_VEHICLE(thisShopperPed)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//							
//							#IF IS_DEBUG_BUILD
//							TEXT_LABEL_63 str
//							str  = "  sitting in vehicle (dis)"
//							DrawLiteralSceneString(str,
//									iShopperPedRow, HUD_COLOUR_PURPLEDARK, fMicShopAlphaMult)
//							#ENDIF
//						ELSE
//							#IF IS_DEBUG_BUILD
//							TEXT_LABEL_63 str
//							str  = "  sitting in vehicle"
//							DrawLiteralSceneString(str,
//									iShopperPedRow, HUD_COLOUR_PURPLELIGHT, fMicShopAlphaMult)
//							#ENDIF
//						ENDIF
//					ELSE
//						IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//							#IF IS_DEBUG_BUILD
//							TEXT_LABEL_63 str
//							str  = "  sitting in vehicle (dis)"
//							DrawLiteralSceneString(str,
//									iShopperPedRow, HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
//							#ENDIF
//						ELSE
//							#IF IS_DEBUG_BUILD
//							TEXT_LABEL_63 str
//							str  = "  sitting in vehicle"
//							DrawLiteralSceneString(str,
//									iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
//							#ENDIF
//						ENDIF
//					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  NOT playing out anim", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					#ENDIF
					
					IF IS_PED_RAGDOLL(thisShopperPed) OR IS_PED_GETTING_UP(thisShopperPed)
						RETURN FALSE
					ENDIF
					
					CLEAR_PED_TASKS(thisShopperPed)
					
					IF ARE_VECTORS_EQUAL(vShopper_wanderOffset, <<0,0,0>>)
						VECTOR vShopper_ped_coord
						vShopper_ped_coord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<5, 0, 0>>)
						vShopper_wanderOffset = (vShopper_ped_coord - vPlayer_scene_m_shop_coord) * 4.0
						
						#IF IS_DEBUG_BUILD
						
						IF NOT bGeneraredShopperWander
							
							SAVE_STRING_TO_DEBUG_FILE("missing shopper wander for [")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_shopping_scene))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SCRIPT_ASSERT("missing shopper wander")
							
							bGeneraredShopperWander = TRUE
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE("vShopper_wanderOffset = ")
						SAVE_VECTOR_TO_DEBUG_FILE(vShopper_wanderOffset)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						#ENDIF
					ENDIF
					
					CLEAR_PED_TASKS(thisShopperPed)
					TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
							 vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
							 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
					UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "not in vehicle")
					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_3_followNavmesh
				
				IF ShouldShopperFleeFromPlayer(thisShopperPed)
					UpdateMicShopPedStage(iCONST_SHOPPER_8_fleePlayer)
					RETURN FALSE
				ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  navmesh...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
						GET_ENTITY_COORDS(thisShopperPed),
						HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
						vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
						HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				#ENDIF
				
				IF IS_PED_RAGDOLL(thisShopperPed) OR IS_PED_GETTING_UP(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				PlayMicShopPedGreet(thisShopperPed)
				
				IF (GET_SCRIPT_TASK_STATUS(thisShopperPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
					IF VDIST2(GET_ENTITY_COORDS(thisShopperPed), vPlayer_scene_m_shop_coord + vShopper_wanderOffset) < (fShopper_wanderRadius*fShopper_wanderRadius)	
						IF (fShopper_wanderRadius = 0)
							fShopper_wanderRadius = VMAG(vShopper_wanderOffset)*0.75
							
							#IF IS_DEBUG_BUILD
							
							IF NOT bGeneraredShopperWander
								
								SAVE_STRING_TO_DEBUG_FILE("missing shopper wander for [")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_shopping_scene))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								SCRIPT_ASSERT("missing shopper wander")
								
								bGeneraredShopperWander = TRUE
							ENDIF
							
							SAVE_STRING_TO_DEBUG_FILE("fShopper_wanderRadius = ")
							SAVE_FLOAT_TO_DEBUG_FILE(fShopper_wanderRadius)
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
						ENDIF
						
						TASK_WANDER_IN_AREA(thisShopperPed,
								vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
								fShopper_wanderRadius)
						UpdateMicShopPedStage(iCONST_SHOPPER_4_wanderInArea)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_4_wanderInArea
				
				IF ShouldShopperFleeFromPlayer(thisShopperPed)
					UpdateMicShopPedStage(iCONST_SHOPPER_8_fleePlayer)
					RETURN FALSE
				ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  wander...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
						GET_ENTITY_COORDS(thisShopperPed),
						HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
						vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
						HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
						fShopper_wanderRadius,
						HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult*0.5)
				#ENDIF
				
				IF IS_PED_RAGDOLL(thisShopperPed) OR IS_PED_GETTING_UP(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				PlayMicShopPedGreet(thisShopperPed)
				
				IF VDIST2(GET_ENTITY_COORDS(thisShopperPed), vPlayer_scene_m_shop_coord + vShopper_wanderOffset) < (fShopper_wanderRadius*fShopper_wanderRadius)
					IF DOES_SCENARIO_EXIST_IN_AREA(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
								fShopper_wanderRadius, TRUE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(thisShopperPed,
								vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
								fShopper_wanderRadius, 0)
						REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlockingIndex)
								
						UpdateMicShopPedStage(iCONST_SHOPPER_4_useNearScenario)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_4_useNearScenario
				
				IF ShouldShopperFleeFromPlayer(thisShopperPed)
					UpdateMicShopPedStage(iCONST_SHOPPER_8_fleePlayer)
					RETURN FALSE
				ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				PlayMicShopPedGreet(thisShopperPed)
				
				IF PED_HAS_USE_SCENARIO_TASK(thisShopperPed)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  using near scenario", iShopperPedRow, HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
							GET_ENTITY_COORDS(thisShopperPed),
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
							vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							fShopper_wanderRadius,
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult*0.5)
					#ENDIF
					
					//
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  NO scenario...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
							GET_ENTITY_COORDS(thisShopperPed),
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
							vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							fShopper_wanderRadius,
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult*0.5)
					#ENDIF
				
					IF IS_PED_RAGDOLL(thisShopperPed) OR IS_PED_GETTING_UP(thisShopperPed)
						RETURN FALSE
					ENDIF
					
					VECTOR vRandomWanderPoint, vRandomWanderCoord
					vRandomWanderPoint = GET_RANDOM_POINT_IN_DISC_UNIFORM(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							fShopper_wanderRadius*0.9, 0.0)
					
					IF GET_SAFE_COORD_FOR_PED(vRandomWanderPoint, FALSE, vRandomWanderCoord, GSC_FLAG_NOT_ISOLATED | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
						IF VDIST2(GET_ENTITY_COORDS(thisShopperPed), vRandomWanderCoord) > (10*10)
							TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
									 vRandomWanderCoord,
									 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							
							VECTOR vMin, vMax
							vMin = GET_ENTITY_COORDS(thisShopperPed)-<<2,2,2>>
							vMax = GET_ENTITY_COORDS(thisShopperPed)+<<2,2,2>>
							ScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax, DEFAULT, FALSE)
							
							UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "not using scenario")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_6_useScenarioAtPos
				
				IF ShouldShopperFleeFromPlayer(thisShopperPed)
					UpdateMicShopPedStage(iCONST_SHOPPER_8_fleePlayer)
					RETURN FALSE
				ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				PlayMicShopPedGreet(thisShopperPed)
				
				IF PED_HAS_USE_SCENARIO_TASK(thisShopperPed)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  using scenario", iShopperPedRow, HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
							GET_ENTITY_COORDS(thisShopperPed),
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
							vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							fShopper_wanderRadius,
							HUD_COLOUR_BLUEDARK, fMicShopAlphaMult*0.5)
					#ENDIF
					
					//
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  NO scenario...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
							GET_ENTITY_COORDS(thisShopperPed),
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisShopperPed),
							vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
							fShopper_wanderRadius,
							HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult*0.5)
					#ENDIF
					
					PlayMicShopPedGreet(thisShopperPed)
					
//					IF IS_PED_RAGDOLL(thisShopperPed) OR IS_PED_GETTING_UP(thisShopperPed)
//						RETURN FALSE
//					ENDIF
//					
//					VECTOR vRandomWanderPoint, vRandomWanderCoord
//					vRandomWanderPoint = GET_RANDOM_POINT_IN_DISC_UNIFORM(vPlayer_scene_m_shop_coord + vShopper_wanderOffset,
//							fShopper_wanderRadius*0.9, 0.0)
//					
//					IF GET_SAFE_COORD_FOR_PED(vRandomWanderPoint, FALSE, vRandomWanderCoord, GSC_FLAG_NOT_ISOLATED | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
//						IF VDIST2(GET_ENTITY_COORDS(thisShopperPed), vRandomWanderCoord) > (10*10)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
//									 vRandomWanderCoord,
//									 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//							
//							VECTOR vMin, vMax
//							vMin = GET_ENTITY_COORDS(thisShopperPed)-<<2,2,2>>
//							vMax = GET_ENTITY_COORDS(thisShopperPed)+<<2,2,2>>
//							ScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax, DEFAULT, FALSE)
//							
//							UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "not using scenario")
//							RETURN TRUE
//						ENDIF
//					ENDIF
				ENDIF
			BREAK
			
			CASE iCONST_SHOPPER_7_keepAnimation
				
				IF ShouldShopperFleeFromPlayer(thisShopperPed, FALSE)
					UpdateMicShopPedStage(iCONST_SHOPPER_8_fleePlayer)
					RETURN FALSE
				ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				PlayMicShopPedGreet(thisShopperPed)
				
				IF IS_ENTITY_PLAYING_ANIM(thisShopperPed, tShopperSceneAnimDict, tShopperSceneAnimLoop, ANIM_SYNCED_SCENE)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  loop synch...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					
					DrawDebugSceneTextWithOffset("loop synch", GET_ENTITY_COORDS(thisShopperPed), 0)
					DrawDebugSceneTextWithOffset("loop synch", GET_ENTITY_COORDS(shopper_obj), 0)
					#ENDIF
					
				ELIF IS_ENTITY_PLAYING_ANIM(thisShopperPed, tShopperSceneAnimDict, tShopperSceneAnimOut, ANIM_SYNCED_SCENE)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  exit synch...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  other anim...", iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
					#ENDIF
					
//					CLEAR_RAGDOLL_BLOCKING_FLAGS(thisShopperPed, RBF_PLAYER_IMPACT)
//					TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
//							 vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
//							 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
//					UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "stage""stage")
//					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_SHOPPER_8_fleePlayer
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  fleePlayer...", iShopperPedRow, HUD_COLOUR_ORANGELIGHT, fMicShopAlphaMult)
				#ENDIF
				
				IF CanShopperBeDeleted(thisShopperPed)
					RETURN FALSE
				ENDIF
				
				CONST_FLOAT	fCONST_SHOPPER_flee_dist		50.0
				
				IF (GET_SCRIPT_TASK_STATUS(thisShopperPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
					TASK_SMART_FLEE_PED(thisShopperPed, PLAYER_PED_ID(), fCONST_SHOPPER_flee_dist, 20000, TRUE)
					RETURN TRUE
				ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(thisShopperPed), GET_ENTITY_COORDS(thisShopperPed)) > (fCONST_SHOPPER_flee_dist * 1.25)
					TASK_FOLLOW_NAV_MESH_TO_COORD(thisShopperPed,
							 vPlayer_scene_m_shop_coord+vShopper_wanderOffset,
							 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fShopper_wanderRadius * 0.1)
					UpdateMicShopPedStage(iCONST_SHOPPER_3_followNavmesh, "flee to navmesh")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE iCONST_SHOPPER_ON_FIRE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  ON FIRE", iShopperPedRow, HUD_COLOUR_ORANGELIGHT, fMicShopAlphaMult)
				#ENDIF
			BREAK
			
			DEFAULT
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("STAGE DEFAULT ", iShopperStage, iShopperPedRow, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
				#ENDIF
				
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ELSE
		IF (iShopperStage <> iCONST_SHOPPER_DEAD)
			UpdateMicShopPedStage(iCONST_SHOPPER_DEAD)
		ENDIF
		
		IF DOES_ENTITY_EXIST(thisShopperPed)
			PlayMicShopPedGreet(thisShopperPed)
			
		#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("dead", iShopperPedRow, HUD_COLOUR_RED, fMicShopAlphaMult)
		ELSE
			DrawLiteralSceneString("non-existant", iShopperPedRow, HUD_COLOUR_GREY, fMicShopAlphaMult*0.5)
		#ENDIF
		
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_M_SHOP_switchInProgress()
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
	
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		
		SWITCH GET_enumFamilyMember_from_ped(g_pScene_buddy)
			CASE FM_MICHAEL_SON
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, g_pScene_buddy, Get_VoiceID_From_FamilyMember(FM_MICHAEL_SON))
			BREAK
			CASE FM_MICHAEL_DAUGHTER
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, g_pScene_buddy, Get_VoiceID_From_FamilyMember(FM_MICHAEL_DAUGHTER))
			BREAK
			CASE FM_MICHAEL_WIFE
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, g_pScene_buddy, Get_VoiceID_From_FamilyMember(FM_MICHAEL_WIFE))
			BREAK
			DEFAULT
				SCRIPT_ASSERT("invalid famMember ID for voice")
			BREAK
		ENDSWITCH
		
		ControlMicShopPedStage(g_pScene_buddy)
		
		RETURN TRUE
	ELSE
		//CPRINTLN(DEBUG_SWITCH, "IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")
	ENDIF
	
	ControlMicShopPedStage(g_pScene_buddy)
	
	IF (shopperObj_model <> DUMMY_MODEL_FOR_SCRIPT)
		
		IF NOT DOES_ENTITY_EXIST(shopper_obj)
			shopper_obj = GET_CLOSEST_OBJECT_OF_TYPE(vPlayer_scene_m_shop_coord+vShopperObj_offset, 1.0, shopperObj_model)
			
			IF DOES_ENTITY_EXIST(shopper_obj)
				SET_ENTITY_COORDS(shopper_obj, vPlayer_scene_m_shop_coord+vShopperObj_offset)
				SET_ENTITY_ROTATION(shopper_obj, <<0,0,fPlayer_scene_m_shop_head>>+vShopperObj_rotation)
				
				FREEZE_ENTITY_POSITION(shopper_obj, TRUE)
			ENDIF
		ELSE
			//
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_M_SHOP_STAGE_TWO()
	
//	#IF IS_DEBUG_BUILD
//	FLOAT AnimCurrentTime = -1.0
//	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
//		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID)
//		DrawLiteralSceneStringFloat("Loop_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSforshopping, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
//	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
//		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
//		DrawLiteralSceneStringFloat("Exit_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSforshopping, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
//	ENDIF
//	IF AnimCurrentTime < 0
//		DrawLiteralSceneString("SynchSceneID OFF??", 0+4+iCONSTANTSforshopping, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
//	ENDIF
//	#ENDIF
	
//	INT iPed
//	REPEAT COUNT_OF(shopper_ped) iPed
//		ControlMicShopPedStage(shopper_ped[iPed], iPed)
//	ENDREPEAT
	
	ControlMicShopPedStage(g_pScene_buddy)
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_M_Shop_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_M_Shop_Variables()
	Setup_Player_Scene_M_Shop()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_M_Shop_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_M_Shop_in_progress
	AND ProgressScene(BIT_MICHAEL, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_m_shopping_stage
			CASE PS_M_SHOP_switchInProgress
				IF Do_PS_M_SHOP_switchInProgress()
					current_player_scene_m_shopping_stage = PS_M_SHOP_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_M_SHOP_STAGE_TWO
				IF Do_PS_M_SHOP_STAGE_TWO()
					current_player_scene_m_shopping_stage = FINISHED_M_SHOPPING
				ENDIF
			BREAK
			
			CASE FINISHED_M_SHOPPING
				Player_Scene_M_Shop_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_M_Shop_Widget()
		Player_Scene_M_Shop_Debug_Options()
		
		fMicShopAlphaMult = 1.0
		IF current_player_scene_m_shopping_stage = PS_M_SHOP_STAGE_TWO
			
			CONST_FLOAT fMAX_PLAYER_MIC_SHOP_DIST	20.0
			FLOAT fPlayerMicShopDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_m_shop_coord)
			IF DOES_ENTITY_EXIST(g_pScene_buddy)
				FLOAT fShopPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_pScene_buddy, FALSE))
				IF (fPlayerMicShopDist > fShopPedDist)
					fPlayerMicShopDist = fShopPedDist
				ENDIF
			ENDIF
		
			fMicShopAlphaMult = 1.0 - (fPlayerMicShopDist/fMAX_PLAYER_MIC_SHOP_DIST)
			IF fMicShopAlphaMult < 0
				fMicShopAlphaMult = 0
			ENDIF
			
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_m_shopping_scene,
				2+iCONSTANTSforshopping, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_M_Shop_Stage(current_player_scene_m_shopping_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_M_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSforshopping, HUD_COLOUR_BLUELIGHT, fMicShopAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_M_Shop_Cleanup()
ENDSCRIPT
