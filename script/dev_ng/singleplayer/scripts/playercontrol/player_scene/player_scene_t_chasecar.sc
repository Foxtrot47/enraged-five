// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene T_CHASECAR file for use – player_scene_t_chasecar.sc	 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//USING "traffic_default_values.sch"
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					1	//150
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					1	//50	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				1	//25	
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK		1	//8   
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		1	//4					
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		1	//6

USING "traffic.sch"
//

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
CONST_INT		iCONSTANTSforchasecar			6

CONST_INT		iNUM_OF_CHASED					4

FLOAT			CruiseSpeed						= 15.0
DRIVINGMODE		Mode							= DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS

CONST_FLOAT		fCONST_LOST_CHASED_PED_DIST		150.0

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T_CHASECAR_STAGE_ENUM
	PS_T_CHASECAR_STAGE_ZERO,
	PS_T_CHASECAR_switchInProgress,
	PS_T_CHASECAR_STAGE_TWO,
	
	FINISHED_PLAYER_SCENE_T_CHASECAR
ENDENUM
PS_T_CHASECAR_STAGE_ENUM	current_player_scene_t_chasecar_stage						= PS_T_CHASECAR_STAGE_ZERO
PED_REQUEST_SCENE_ENUM		selected_player_scene_t_chasecar_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL				bPlayer_Scene_T_ChaseCar_in_progress								= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX			t_chasecar_ped[iNUM_OF_CHASED]
MODEL_NAMES			t_chasecar_ped_model[iNUM_OF_CHASED]
PED_TYPE			t_chasecar_pedtype = PEDTYPE_CIVMALE

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX		t_chasecar_veh[iNUM_OF_CHASED]
MODEL_NAMES			t_chasecar_veh_model[iNUM_OF_CHASED]
VECTOR 				vTChaseCar_veh_offset[iNUM_OF_CHASED]
FLOAT 				fTChaseCar_veh_offset[iNUM_OF_CHASED]
INT					iTChaseCar_veh_colour = 0

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//
REL_GROUP_HASH		chasecargroup

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_t_chasecar_coord
FLOAT				fPlayer_scene_t_chasecar_head

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL	tRecordingName	= "null"
INT			iRecordingNum[iNUM_OF_CHASED]
FLOAT		fPlayerRecordingStart, fPlayerRecordingSkip

TEXT_LABEL	tChaseHintCamLabel		= "null"

TEXT_LABEL	tChaseRandomSpeechLabel	= "null"
INT			tChaseRandomCount = -1
INT			iCONST_MAX_Chase_car_conv = 5, tChaseSpeechCount= 0

TEXT_LABEL	tChaseGiveupSpeechLabel	= "null"
TEXT_LABEL	tChaseKillSpeechLabel	= "null"
structTimer	speechTimer

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fTrevChaseAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_t_chasecar_widget
INT				iCurrent_player_scene_t_chasecar_stage
BOOL			bMove_vehs, bRecord_vehs, bSave_vehs
#ENDIF

	//- other Ints(INT) -//
INT iPlaybackStage[iNUM_OF_CHASED]
FLOAT fPlaybackSpeed[iNUM_OF_CHASED]

	//- other Floats(FLOAT) -//
FLOAT			fRecordingQuitPhase = -1

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_T_ChaseCar_Cleanup()
	INT iChaseCount
	
	REPEAT iNUM_OF_CHASED iChaseCount
		//- mark peds as no longer needed -//
		SET_PED_AS_NO_LONGER_NEEDED(t_chasecar_ped[iChaseCount])
		
		//- mark vehicle as no longer needed -//
		SET_VEHICLE_AS_NO_LONGER_NEEDED(t_chasecar_veh[iChaseCount])
		
		//- mark objects as no longer needed -//
		//- mark models as no longer needed -//
		IF (t_chasecar_ped_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(t_chasecar_ped_model[iChaseCount])
		ENDIF
		IF (t_chasecar_veh_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(t_chasecar_veh_model[iChaseCount])
		ENDIF
		
		//- remove anims from the memory -//
		//- remove vehicle waypordings from the memory -//
		IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
		AND (iRecordingNum[iChaseCount] > 0)
			REMOVE_VEHICLE_RECORDING(iRecordingNum[iChaseCount], tRecordingName)
		ENDIF
		
	ENDREPEAT
	
	//- clear sequences -//
	//- destroy all scripted cams -//
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_t_chasecar_widget)
		DELETE_WIDGET_GROUP(player_scene_t_chasecar_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_T_ChaseCar_Finished()
	//CLEAR_CPRINTS()
	bPlayer_Scene_T_ChaseCar_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_T_ChaseCar_Variables()
	selected_player_scene_t_chasecar_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_t_chasecar_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_t_chasecar_scene, vPlayer_scene_t_chasecar_coord, fPlayer_scene_t_chasecar_head, tPlayer_scene_t_chasecar_room)
	
	INT iPlayerRecordingNum
	FLOAT fPlayerRecordingStop
	FLOAT fPlayerSpeedSwitch, fPlayerSpeedExit
	
	GET_PLAYER_VEH_RECORDING_FOR_SCENE(selected_player_scene_t_chasecar_scene,
			tRecordingName, iPlayerRecordingNum,
			fPlayerRecordingStart, fPlayerRecordingSkip, fPlayerRecordingStop,
			fPlayerSpeedSwitch, fPlayerSpeedExit)
	
	SWITCH selected_player_scene_t_chasecar_scene
		CASE PR_SCENE_T_CR_CHASECAR_a		// Chasing a car along river.
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<5.5437, 30.4559, 0.0000>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = -4.6273
			
			//- ints -//
			iTChaseCar_veh_colour = 7
			
			t_chasecar_ped_model[0] = A_M_Y_GenStreet_01
			t_chasecar_ped_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_pedtype = PEDTYPE_CIVMALE
			
			t_chasecar_veh_model[0] = Penumbra
			t_chasecar_veh_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			Mode = DRIVINGMODE_AVOIDCARS_RECKLESS | DF_PreventBackgroundPathfinding
			
			//tRecordingName = "scene_t_chaseA"
			iRecordingNum[0] = 002
			iRecordingNum[1] = -1
			iRecordingNum[2] = -1
			iRecordingNum[3] = -1
			
			tChaseHintCamLabel		= "CMN_HINT" //"PST_HINTCAR"
			
			tChaseRandomSpeechLabel	= "PST_CHSCRaR"		//"PST_CHSCRaR1", "PST_CHSCRaR2", "PST_CHSCRa3", "PST_CHSCRa4"
			tChaseGiveupSpeechLabel	= "PST_CHSCRaG"
			tChaseKillSpeechLabel	= "PST_CHSCRaK"
			
			iCONST_MAX_Chase_car_conv = 4
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CN_CHASECAR_b		// Trevor chasing car in countryside
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<-21.2110, 7.3930, 0.0000>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = 0.7266
			
			//- ints -//
			iTChaseCar_veh_colour = 0
			
			t_chasecar_ped_model[0] = A_M_Y_GenStreet_01
			t_chasecar_ped_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[2] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_pedtype = PEDTYPE_CIVMALE
			
			t_chasecar_veh_model[0] = Penumbra
			t_chasecar_veh_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[2] = DUMMY_MODEL_FOR_SCRIPT
			
			Mode = DRIVINGMODE_AVOIDCARS_RECKLESS
			
			//tRecordingName = "scene_t_chaseB"
			iRecordingNum[0] = 002
			iRecordingNum[1] = -1
			iRecordingNum[2] = -1
			iRecordingNum[3] = -1
			
			tChaseHintCamLabel		= "CMN_HINT" //"PST_HINTCAR"
			
			tChaseRandomSpeechLabel	= "PST_CHSCRbR"		//"PST_CHSCRbR1", "PST_CHSCRbR2", "PST_CHSCRbR3"
			tChaseGiveupSpeechLabel	= "PST_CHSCRbG"
			tChaseKillSpeechLabel	= "PST_CHSCRbK"
			
			iCONST_MAX_Chase_car_conv = 3
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CR_CHASEBIKE		// Trevor running over 3 cyclist in the road - cycle rage
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<-7.9514, -28.1131	, 1.0940>>
			vTChaseCar_veh_offset[1] = <<-10.8760, -26.4702	, 1.1106>>
			
			
			vTChaseCar_veh_offset[2] = <<-13.8006, -24.8273, 1.1272>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = 10.9603
			fTChaseCar_veh_offset[1] = -0.8648
			fTChaseCar_veh_offset[2] = -12.6899
			
			//- ints -//
			iTChaseCar_veh_colour = 0
			
			t_chasecar_ped_model[0] = A_M_Y_ROADCYC_01
			t_chasecar_ped_model[1] = A_M_Y_ROADCYC_01
			t_chasecar_ped_model[2] = A_M_Y_ROADCYC_01
			t_chasecar_ped_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_pedtype = PEDTYPE_CIVMALE
			
			t_chasecar_veh_model[0] = TRIBIKE
			t_chasecar_veh_model[1] = TRIBIKE
			t_chasecar_veh_model[2] = TRIBIKE
			t_chasecar_veh_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			//tRecordingName = "scene_t_chaseC"
			iRecordingNum[0] = 002
			iRecordingNum[1] = 003
			iRecordingNum[2] = 004
			iRecordingNum[3] = -1
			
			tChaseHintCamLabel		= "CMN_HINT" //"PST_HINTBIKE"
			
			tChaseRandomSpeechLabel	= "PST_CHSCRcR"		//"PST_CHSCRcR1", //"PST_CHSCRcR2", //"PST_CHSCRcR3"
			tChaseGiveupSpeechLabel	= ""
			tChaseKillSpeechLabel	= "PST_CHSCRcK"		//"PST_CHSCRcK1", //"PST_CHSCRcK2", //"PST_CHSCRcK3"
			
			iCONST_MAX_Chase_car_conv = 3
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CR_CHASESCOOTER		// Trevor chasing a guy on a scooter and screaming 'Scooter Brother!'
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<12.7813, 6.7387, 0.0000>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = -2.6961
			
			//- ints -//
			iTChaseCar_veh_colour = 0
			
			t_chasecar_ped_model[0] = A_M_Y_GenStreet_01
			t_chasecar_ped_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_pedtype = PEDTYPE_CIVMALE
			
			t_chasecar_veh_model[0] = FAGGIO2
			t_chasecar_veh_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			//tRecordingName = "scene_t_chaseD"		//"scene_t_chase_d"
			iRecordingNum[0] = 002
			iRecordingNum[1] = -1
			iRecordingNum[2] = -1
			iRecordingNum[3] = -1
			
			tChaseHintCamLabel		= "CMN_HINT" //"PST_HINTSCOOT"
			
			tChaseRandomSpeechLabel	= "PST_CHSCRdR"
			tChaseGiveupSpeechLabel	= "PST_CHSCRdG"
			tChaseKillSpeechLabel	= "PST_CHSCRdK"
			
			iCONST_MAX_Chase_car_conv = 8
			tChaseRandomCount = -1
			fRecordingQuitPhase = 0.95
			
			CruiseSpeed			= 10.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CR_POLICE_a
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<-9.1510, -1.3167, 0.9699>>		//<<895.3365, -1199.5375, 45.9852>>
			vTChaseCar_veh_offset[1] = <<-10.4908, 3.3092, 0.0>>
			vTChaseCar_veh_offset[2] = <<-23.4117, -0.3117, 0.0>>
			vTChaseCar_veh_offset[3] = <<-20.2394, -5.2394, 0.0>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = -0.3188		//-82.3870
			fTChaseCar_veh_offset[1] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			
			//- ints -//
			t_chasecar_ped_model[0] = S_M_Y_COP_01
			t_chasecar_ped_model[1] = S_M_Y_COP_01
			t_chasecar_ped_model[2] = S_M_Y_COP_01
			t_chasecar_ped_model[3] = S_M_Y_COP_01
			
			t_chasecar_pedtype = PEDTYPE_COP
			
			t_chasecar_veh_model[0] = POLICE3
			t_chasecar_veh_model[1] = POLICE3
			t_chasecar_veh_model[2] = POLICE3
			t_chasecar_veh_model[3] = POLICE3
			
			//tRecordingName = "scene_t_policeA"
			iRecordingNum[0] = 002
			iRecordingNum[1] = 003
			iRecordingNum[2] = 004
			iRecordingNum[3] = 005
			
			tChaseHintCamLabel		= ""
			
			tChaseRandomSpeechLabel	= "PST_POLaR"
			tChaseGiveupSpeechLabel	= ""
			tChaseKillSpeechLabel	= ""
			
			iCONST_MAX_Chase_car_conv = 3
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CN_POLICE_b
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<1.5168, -20.0085, 0.1667>>		//<<1881.9980, 2225.0313, 54.0000>>
			vTChaseCar_veh_offset[1] = <<-4.9822, -20.3650, 0.1911>>	//<<1881.9980, 2225.0313, 54.0000>>
			vTChaseCar_veh_offset[2] = <<-7.8196, -18.8446, 0.0>>
			vTChaseCar_veh_offset[3] = <<-4.3313, -28.5564, 0.0>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = -3.4484							//-4.7459
			fTChaseCar_veh_offset[1] = -12.2569							//-4.7459
			fTChaseCar_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			
			//- ints -//
			t_chasecar_ped_model[0] = S_M_Y_RANGER_01
			t_chasecar_ped_model[1] = S_M_Y_RANGER_01
			t_chasecar_ped_model[2] = S_M_Y_RANGER_01
			t_chasecar_ped_model[3] = S_M_Y_RANGER_01
			
			t_chasecar_pedtype = PEDTYPE_COP
			
			t_chasecar_veh_model[0] = SHERIFF
			t_chasecar_veh_model[1] = SHERIFF
			t_chasecar_veh_model[2] = PRANGER
			t_chasecar_veh_model[3] = PRANGER
			
			//tRecordingName = "scene_t_policeB"
			iRecordingNum[0] = 002
			iRecordingNum[1] = 003
			iRecordingNum[2] = 004
			iRecordingNum[3] = 005
			
			tChaseHintCamLabel		= ""
			
			tChaseRandomSpeechLabel	= "PST_POLbR"
			tChaseGiveupSpeechLabel	= ""
			tChaseKillSpeechLabel	= ""
			
			iCONST_MAX_Chase_car_conv = 5
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		CASE PR_SCENE_T_CN_POLICE_c
			//- vectors -//
			vTChaseCar_veh_offset[0] = <<-14.3569, 0.1057, 0.0>>
			vTChaseCar_veh_offset[1] = <<-14.8079, 6.2547, 0.0>>
			vTChaseCar_veh_offset[2] = <<-25.6028, 6.8598, 0.0>>
			vTChaseCar_veh_offset[3] = <<-27.9848, 0.2778, 0.0>>
			
			//- floats -//
			fTChaseCar_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[1] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[2] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			fTChaseCar_veh_offset[3] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			
			//- ints -//
			t_chasecar_ped_model[0] = S_M_Y_RANGER_01
			t_chasecar_ped_model[1] = S_M_Y_RANGER_01
			t_chasecar_ped_model[2] = S_M_Y_RANGER_01
			t_chasecar_ped_model[3] = S_M_Y_RANGER_01
			
			t_chasecar_pedtype = PEDTYPE_COP
			
			t_chasecar_veh_model[0] = SHERIFF
			t_chasecar_veh_model[1] = SHERIFF
			t_chasecar_veh_model[2] = PRANGER
			t_chasecar_veh_model[3] = PRANGER
			
			//tRecordingName = "scene_t_policeC"
			iRecordingNum[0] = 002
			iRecordingNum[1] = 003
			iRecordingNum[2] = 004
			iRecordingNum[3] = 005
			
			tChaseHintCamLabel		= ""
			
			tChaseRandomSpeechLabel	= "PST_POLcR"
			tChaseGiveupSpeechLabel	= ""
			tChaseKillSpeechLabel	= ""
			
			iCONST_MAX_Chase_car_conv = 5
			tChaseRandomCount = 1
			fRecordingQuitPhase = 1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str  = "invalid selected_player_scene_t_chasecar_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_chasecar_scene)
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			FLOAT fDebugJumpAngle
			fDebugJumpAngle = fPlayer_scene_t_chasecar_head+90+GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			vTChaseCar_veh_offset[0] = <<SIN(fDebugJumpAngle), COS(fDebugJumpAngle), 0.0>> * GET_RANDOM_FLOAT_IN_RANGE(15, 25)
			
			//- floats -//
			fTChaseCar_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-5, 5)
			
			//- ints -//
			iTChaseCar_veh_colour = 0
			
			//- enums -//
			t_chasecar_ped_model[0] = A_M_Y_GenStreet_01
			t_chasecar_ped_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_ped_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_veh_model[0] = Penumbra
			t_chasecar_veh_model[1] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[2] = DUMMY_MODEL_FOR_SCRIPT
			t_chasecar_veh_model[3] = DUMMY_MODEL_FOR_SCRIPT
			
			t_chasecar_pedtype = PEDTYPE_CIVMALE
			
			tRecordingName = "null"
			iRecordingNum[0] = -1
			iRecordingNum[1] = -1
			iRecordingNum[2] = -1
			iRecordingNum[3] = -1
			
			tChaseHintCamLabel		= "null"
			
			tChaseRandomSpeechLabel	= "null"
			tChaseGiveupSpeechLabel	= "null"
			tChaseKillSpeechLabel	= "null"
			
			iCONST_MAX_Chase_car_conv = -1
			tChaseRandomCount = -1
			fRecordingQuitPhase = -1.0
			
			//-- structs : PS_T_CHASECAR_STRUCT --//
		BREAK
	ENDSWITCH
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_T_ChaseCar()
	INT iChaseCount
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle waypordings -//
	//- request interior models -//
	//- wait for assets to load -//
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iRow = 0
		#ENDIF
		
		REPEAT iNUM_OF_CHASED iChaseCount
			IF (t_chasecar_ped_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(t_chasecar_ped_model[iChaseCount])
				IF NOT HAS_MODEL_LOADED(t_chasecar_ped_model[iChaseCount])
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("ped model ")
					str += GET_MODEL_NAME_FOR_DEBUG(t_chasecar_ped_model[iChaseCount])
					DrawLiteralSceneString(str, iRow+1+iCONSTANTSforchasecar, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					REQUEST_MODEL(t_chasecar_ped_model[iChaseCount])
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
			IF (t_chasecar_veh_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(t_chasecar_veh_model[iChaseCount])
				IF NOT HAS_MODEL_LOADED(t_chasecar_veh_model[iChaseCount])
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("veh model ")
					str += GET_MODEL_NAME_FOR_DEBUG(t_chasecar_veh_model[iChaseCount])
					DrawLiteralSceneString(str, iRow+1+iCONSTANTSforchasecar, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					REQUEST_MODEL(t_chasecar_veh_model[iChaseCount])
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum[iChaseCount] > 0)
				REQUEST_VEHICLE_RECORDING(iRecordingNum[iChaseCount], tRecordingName)
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iChaseCount], tRecordingName)
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("veh rec ")
					str += tRecordingName
					IF (iRecordingNum[iChaseCount] < 100)	str += ("0") ENDIF
					IF (iRecordingNum[iChaseCount] < 10)	str += ("0") ENDIF
					str += iRecordingNum[iChaseCount]
					DrawLiteralSceneString(str, iRow+1+iCONSTANTSforchasecar, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					REQUEST_VEHICLE_RECORDING(iRecordingNum[iChaseCount], tRecordingName)
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any sequence tasks -//
	ADD_RELATIONSHIP_GROUP("chasecar", chasecargroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,chasecargroup,RELGROUPHASH_PLAYER )
	
	REPEAT iNUM_OF_CHASED iChaseCount
		
		//- create any script vehicles -//
		IF (t_chasecar_veh_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
			CLEAR_AREA(vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iChaseCount], 20.0, TRUE)
			
			t_chasecar_veh[iChaseCount] = CREATE_VEHICLE(t_chasecar_veh_model[iChaseCount],
					vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iChaseCount],
					fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iChaseCount])
			SET_VEHICLE_COLOUR_COMBINATION(t_chasecar_veh[iChaseCount], iTChaseCar_veh_colour)
			SET_MODEL_AS_NO_LONGER_NEEDED(t_chasecar_veh_model[iChaseCount])
			
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(t_chasecar_veh[iChaseCount], TRUE)
		ENDIF
		
		//- create any script peds -//
		IF (t_chasecar_ped_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
			t_chasecar_ped[iChaseCount] = CREATE_PED_INSIDE_VEHICLE(t_chasecar_veh[iChaseCount],
					t_chasecar_pedtype, t_chasecar_ped_model[iChaseCount], VS_DRIVER)
			SET_PED_RANDOM_COMPONENT_VARIATION(t_chasecar_ped[iChaseCount])
			SET_MODEL_AS_NO_LONGER_NEEDED(t_chasecar_ped_model[iChaseCount])
			
			SET_PED_RELATIONSHIP_GROUP_HASH(t_chasecar_ped[iChaseCount], chasecargroup)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(t_chasecar_ped[iChaseCount], TRUE) 
			
			IF (t_chasecar_ped_model[iChaseCount] = A_M_Y_ROADCYC_01)
				SET_PED_HELMET(t_chasecar_ped[iChaseCount], TRUE)
			ENDIF
			
			IF (t_chasecar_pedtype <> PEDTYPE_COP)
			
				SET_PED_COMBAT_MOVEMENT(t_chasecar_ped[iChaseCount], CM_WILLRETREAT)
				SET_PED_COMBAT_ATTRIBUTES(t_chasecar_ped[iChaseCount], CA_ALWAYS_FLEE,TRUE)
				
				SET_PED_FLEE_ATTRIBUTES(t_chasecar_ped[iChaseCount], FA_USE_COVER, TRUE)
				SET_PED_FLEE_ATTRIBUTES(t_chasecar_ped[iChaseCount], FA_USE_VEHICLE, TRUE)
			ELSE
//				SET_PED_COMBAT_MOVEMENT(t_chasecar_ped[iChaseCount], CM_WILLRETREAT)
//				SET_PED_COMBAT_ATTRIBUTES(t_chasecar_ped[iChaseCount], CA_ALWAYS_FLEE,TRUE)
				
//				SET_PED_FLEE_ATTRIBUTES(t_chasecar_ped[iChaseCount], FA_USE_COVER, TRUE)
//				SET_PED_FLEE_ATTRIBUTES(t_chasecar_ped[iChaseCount], FA_USE_VEHICLE, TRUE)
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_T_ChaseCar_Stage(PS_T_CHASECAR_STAGE_ENUM this_player_scene_t_chasecar_stage)
	SWITCH this_player_scene_t_chasecar_stage
		CASE PS_T_CHASECAR_STAGE_ZERO
			RETURN "PS_T_CHASECAR_STAGE_ZERO"
		BREAK
		CASE PS_T_CHASECAR_switchInProgress
			RETURN "PS_T_CHASECAR_switchInProgress"
		BREAK
		CASE PS_T_CHASECAR_STAGE_TWO
			RETURN "PS_T_CHASECAR_STAGE_TWO"
		BREAK
		
		CASE FINISHED_PLAYER_SCENE_T_CHASECAR
			RETURN "FINISHED_PLAYER_SCENE_T_CHASECAR"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_T_ChaseCar_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_T_ChaseCar_widget()
	INT iWidget
	TEXT_LABEL_63 tWidget
	
	player_scene_t_chasecar_widget = START_WIDGET_GROUP("player_scene_t_chasecar.sc - Player scene T_CHASECAR")
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_T_CHASECAR_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_T_ChaseCar_Stage(INT_TO_ENUM(PS_T_CHASECAR_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_t_chasecar_stage", iCurrent_player_scene_t_chasecar_stage)
		
		START_WIDGET_GROUP("t_chasecar_veh")	
			
			REPEAT iNUM_OF_CHASED iWidget
				
				IF (t_chasecar_ped_model[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
				AND (t_chasecar_veh_model[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
					
					tWidget  = "t_chasecar_veh["
					tWidget += iWidget
					tWidget += "]"
					
					START_WIDGET_GROUP(tWidget)
						ADD_WIDGET_VECTOR_SLIDER("vTChaseCar_veh_offset", vTChaseCar_veh_offset[iWidget], -100.0, 100.0, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fTChaseCar_veh_offset", fTChaseCar_veh_offset[iWidget], -180.0, 180.0, 0.1)
						
						ADD_WIDGET_STRING(GET_MODEL_NAME_FOR_DEBUG(t_chasecar_ped_model[iWidget]))
						ADD_WIDGET_STRING(GET_MODEL_NAME_FOR_DEBUG(t_chasecar_veh_model[iWidget]))
					STOP_WIDGET_GROUP()
				ENDIF
			ENDREPEAT
			
			ADD_WIDGET_STRING("iTChaseCar_veh_colour")
			ADD_WIDGET_INT_SLIDER("iTChaseCar_veh_colour", iTChaseCar_veh_colour, -1, 15, 1)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("bMove_vehs", bMove_vehs)
		ADD_WIDGET_BOOL("bRecord_vehs", bRecord_vehs)
		ADD_WIDGET_BOOL("bSave_vehs", bSave_vehs)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_ChaseCar_Record()
	INT iWidget
	INT iThisRecording = 0
	INT iThisRecordingOffset = 100
	VEHICLE_INDEX player_scene_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	
	CLEAR_AREA(vPlayer_scene_t_chasecar_coord, 50*2, TRUE)
	SET_ROADS_IN_AREA(vPlayer_scene_t_chasecar_coord-<<50,50,50>>,
			vPlayer_scene_t_chasecar_coord+<<50,50,50>>, FALSE)
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	
	IF NOT IS_ENTITY_DEAD(player_scene_veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
			STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
		ENDIF
		SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_t_chasecar_coord)
		SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_t_chasecar_head)
		SET_VEHICLE_FIXED(player_scene_veh)
	ENDIF
	
	REPEAT iNUM_OF_CHASED iWidget
		IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iWidget])
				STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iWidget])
				
				SET_ENTITY_COORDS(t_chasecar_veh[iWidget], vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iWidget])
				SET_ENTITY_HEADING(t_chasecar_veh[iWidget], fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iWidget])
				SET_VEHICLE_FIXED(t_chasecar_veh[iWidget])
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(t_chasecar_ped[iWidget])
			CLEAR_PED_TASKS(t_chasecar_ped[iWidget])
		ENDIF
	ENDREPEAT
	
	WHILE bRecord_vehs
		
		IF NOT IS_ENTITY_DEAD(player_scene_veh)
		AND NOT (DOES_ENTITY_EXIST(t_chasecar_veh[0]) AND IS_ENTITY_DEAD(t_chasecar_veh[0]))
		AND NOT (DOES_ENTITY_EXIST(t_chasecar_veh[1]) AND IS_ENTITY_DEAD(t_chasecar_veh[1]))
		AND NOT (DOES_ENTITY_EXIST(t_chasecar_veh[2]) AND IS_ENTITY_DEAD(t_chasecar_veh[2]))
		AND NOT (DOES_ENTITY_EXIST(t_chasecar_veh[3]) AND IS_ENTITY_DEAD(t_chasecar_veh[3]))
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
					
					SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_t_chasecar_coord)
					SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_t_chasecar_head)
					SET_VEHICLE_FIXED(player_scene_veh)
				ENDIF
				
				REPEAT iNUM_OF_CHASED iWidget
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iWidget])
						STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iWidget])
						
						SET_ENTITY_COORDS(t_chasecar_veh[iWidget], vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iWidget])
						SET_ENTITY_HEADING(t_chasecar_veh[iWidget], fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iWidget])
						SET_VEHICLE_FIXED(t_chasecar_veh[iWidget])
					ENDIF
				ENDREPEAT
				
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), player_scene_veh, VS_DRIVER)
			ELSE
				//
				
				VEHICLE_INDEX player_record_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				
				IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(player_record_veh)
					
					INT iPlayerVehInt = -1
					IF (player_record_veh = player_scene_veh)
						iPlayerVehInt = COUNT_OF(t_chasecar_veh)
					ELSE
						REPEAT iNUM_OF_CHASED iWidget
							IF (player_record_veh = t_chasecar_veh[iWidget])
								iPlayerVehInt = iWidget
							ENDIF
						ENDREPEAT
					ENDIF
					
					DrawLiteralSceneStringInt("press 'UP' to record ", iPlayerVehInt, 0, HUD_COLOUR_BLUE)
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
						
						IF (player_record_veh <> player_scene_veh)
							IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
							AND (iRecordingNum[0]-1 > 0)
								REQUEST_VEHICLE_RECORDING(iRecordingNum[0]-1, tRecordingName)
								IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[0]-1, tRecordingName)
									REQUEST_VEHICLE_RECORDING(iRecordingNum[0]-1, tRecordingName)
									WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[0]-1, tRecordingName)
										DrawLiteralSceneString("HAS_VEHICLE_RECORDING_BEEN_LOADED", 0, HUD_COLOUR_BLUELIGHT)
										WAIT(0)
									ENDWHILE
								ENDIF
								
								IF NOT IS_ENTITY_DEAD(player_scene_veh)
									START_PLAYBACK_RECORDED_VEHICLE(player_scene_veh, iRecordingNum[0]-1, tRecordingName)
								ENDIF
							ENDIF
						ELSE
							iThisRecording = iRecordingNum[0]-1
						ENDIF
						
						REPEAT iNUM_OF_CHASED iWidget
							IF (player_record_veh <> t_chasecar_veh[iWidget])
								IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
								AND (iRecordingNum[iWidget] > 0)
									REQUEST_VEHICLE_RECORDING(iRecordingNum[iWidget], tRecordingName)
									IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iWidget], tRecordingName)
										REQUEST_VEHICLE_RECORDING(iRecordingNum[iWidget], tRecordingName)
										WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iWidget], tRecordingName)
											DrawLiteralSceneString("HAS_VEHICLE_RECORDING_BEEN_LOADED", 0, HUD_COLOUR_BLUELIGHT)
											WAIT(0)
										ENDWHILE
									ENDIF
									
									IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
										START_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iWidget], iRecordingNum[iWidget], tRecordingName)
										
										FLOAT fTime = 0.0
										
//										VECTOR vPlayer_scene_t_chasecar_coord
//										FLOAT				fPlayer_scene_t_chasecar_head
										
										VECTOR vRecStartPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iWidget], fTime, tRecordingName)
										VECTOR vRecStartRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iWidget], fTime, tRecordingName)
										
										IF NOT ARE_VECTORS_EQUAL(vRecStartPos, vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iWidget])
										OR NOT (vRecStartRot.z = fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iWidget])
											SAVE_STRING_TO_DEBUG_FILE("FAKE ")
											SAVE_STRING_TO_DEBUG_FILE("pos and rot of ")
											SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
											SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iWidget])
											SAVE_STRING_TO_DEBUG_FILE(": ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos)
											SAVE_STRING_TO_DEBUG_FILE(", ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartRot)
											SAVE_NEWLINE_TO_DEBUG_FILE()
											

SAVE_STRING_TO_DEBUG_FILE("	vTChaseCar_veh_offset[")
SAVE_INT_TO_DEBUG_FILE(iWidget)
SAVE_STRING_TO_DEBUG_FILE("] = ")
SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos-vPlayer_scene_t_chasecar_coord)
SAVE_STRING_TO_DEBUG_FILE("		//")
SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_t_chasecar_coord)
SAVE_NEWLINE_TO_DEBUG_FILE()

SAVE_STRING_TO_DEBUG_FILE("	fTChaseCar_veh_offset[")
SAVE_INT_TO_DEBUG_FILE(iWidget)
SAVE_STRING_TO_DEBUG_FILE("] = ")
SAVE_FLOAT_TO_DEBUG_FILE(vRecStartRot.z-fPlayer_scene_t_chasecar_head)
SAVE_STRING_TO_DEBUG_FILE("		//")
SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_t_chasecar_head)
SAVE_NEWLINE_TO_DEBUG_FILE()
											
											
										ELSE
											SAVE_STRING_TO_DEBUG_FILE("real ")
											SAVE_STRING_TO_DEBUG_FILE("pos and rot of ")
											SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
											SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iWidget])
											SAVE_STRING_TO_DEBUG_FILE(": ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos)
											SAVE_STRING_TO_DEBUG_FILE(", ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartRot)
											SAVE_NEWLINE_TO_DEBUG_FILE()
										ENDIF
										
										
									ENDIF
								ENDIF
							ELSE
								iThisRecording = iRecordingNum[iWidget]
							ENDIF
						ENDREPEAT
						
						IF NOT IS_ENTITY_DEAD(player_record_veh)
							START_RECORDING_VEHICLE(player_record_veh,
									iThisRecording+iThisRecordingOffset,
									tRecordingName, TRUE)
							
							CPRINTLN(DEBUG_SWITCH, "START_RECORDING_VEHICLE(player_record_veh, ", iThisRecording+iThisRecordingOffset, ", \"", tRecordingName, "\")")
						ENDIF
					ENDIF
					
					INT iCHASECAR_VEH_MAX = 0
					REPEAT iNUM_OF_CHASED iWidget
						IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
							iCHASECAR_VEH_MAX++
						ENDIF
					ENDREPEAT
					
					
					DrawLiteralSceneStringInt("iCHASECAR_VEH_MAX:", iCHASECAR_VEH_MAX, 1, HUD_COLOUR_RED)
					
					DrawLiteralSceneString("press 'LEFT' to change", 2, HUD_COLOUR_BLUELIGHT)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
						
						
						SAVE_STRING_TO_DEBUG_FILE("iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						iPlayerVehInt--
						
						SAVE_STRING_TO_DEBUG_FILE("--: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						IF iPlayerVehInt < 0
							
							SAVE_STRING_TO_DEBUG_FILE(" < ")
							SAVE_INT_TO_DEBUG_FILE(0)
							SAVE_STRING_TO_DEBUG_FILE("zet to max")
							
							iPlayerVehInt = iCHASECAR_VEH_MAX
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE(", iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
					ENDIF
					DrawLiteralSceneString("press 'RIGHT' to change", 3, HUD_COLOUR_BLUELIGHT)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
						
						
						SAVE_STRING_TO_DEBUG_FILE("iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						iPlayerVehInt++
						
						SAVE_STRING_TO_DEBUG_FILE("++: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						IF iPlayerVehInt > iCHASECAR_VEH_MAX
							
							SAVE_STRING_TO_DEBUG_FILE(" > ")
							SAVE_INT_TO_DEBUG_FILE(iCHASECAR_VEH_MAX)
							SAVE_STRING_TO_DEBUG_FILE("zet to zero")
							
							iPlayerVehInt = 0
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE(", iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
					ENDIF
					
					VEHICLE_INDEX new_record_veh
					IF (iPlayerVehInt = iCHASECAR_VEH_MAX)
					OR (iPlayerVehInt = COUNT_OF(t_chasecar_veh))
						
//						SAVE_STRING_TO_DEBUG_FILE("new_record_veh = player_scene_veh")
//						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						new_record_veh = player_scene_veh
					ELSE
						
//						SAVE_STRING_TO_DEBUG_FILE("new_record_veh = t_chasecar_veh[")
//						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
//						SAVE_STRING_TO_DEBUG_FILE("]")
//						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						new_record_veh = t_chasecar_veh[iPlayerVehInt]
					ENDIF
					
					IF (new_record_veh <> player_record_veh)
						
						CLEAR_AREA(vPlayer_scene_t_chasecar_coord, 100.0, TRUE)
						
						PED_INDEX player_scene_ped = GET_PED_IN_VEHICLE_SEAT(new_record_veh, VS_DRIVER)
						
						SET_ENTITY_COORDS(player_scene_ped, GET_ENTITY_COORDS(player_record_veh)+<<1,1,1>>)
						
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), new_record_veh, VS_DRIVER)
						SET_PED_INTO_VEHICLE(player_scene_ped, player_record_veh, VS_DRIVER)
						
						IF NOT IS_ENTITY_DEAD(player_scene_veh)
							SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_t_chasecar_coord)
							SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_t_chasecar_head)
							SET_VEHICLE_FIXED(player_scene_veh)
						ENDIF
						
						REPEAT iNUM_OF_CHASED iWidget
							IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
								SET_ENTITY_COORDS(t_chasecar_veh[iWidget], vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iWidget])
								SET_ENTITY_HEADING(t_chasecar_veh[iWidget], fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iWidget])
								SET_VEHICLE_FIXED(t_chasecar_veh[iWidget])
							ENDIF
							IF NOT IS_ENTITY_DEAD(t_chasecar_ped[iWidget])
								CLEAR_PED_TASKS(t_chasecar_ped[iWidget])
							ENDIF
						ENDREPEAT
						
					ENDIF
					
				ELSE
					DrawLiteralSceneString("RECORDING!", 0, HUD_COLOUR_RED)
					DrawLiteralSceneStringInt(tRecordingName, iThisRecording+iThisRecordingOffset, 1, HUD_COLOUR_RED)
					DrawLiteralSceneStringFloat("recording: ", GET_TIME_POSITION_IN_RECORDED_RECORDING(player_record_veh), 2, HUD_COLOUR_RED)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
						ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(player_scene_veh, RDM_WHOLELINE)
					ENDIF
					
					REPEAT iNUM_OF_CHASED iWidget
						IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iWidget])
								ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iWidget], RDM_WHOLELINE)
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
						STOP_RECORDING_VEHICLE(player_record_veh)
						
						CPRINTLN(DEBUG_SWITCH, "STOP_RECORDING_VEHICLE(player_record_veh, ", iThisRecording+iThisRecordingOffset, ", \"", tRecordingName, "\")")
						
						iThisRecordingOffset += 10
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
						ENDIF
						SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_t_chasecar_coord)
						SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_t_chasecar_head)
						SET_VEHICLE_FIXED(player_scene_veh)
						
						REPEAT iNUM_OF_CHASED iWidget
							IF NOT IS_ENTITY_DEAD(t_chasecar_veh[iWidget])
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iWidget])
									STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iWidget])
								ENDIF
								SET_ENTITY_COORDS(t_chasecar_veh[iWidget], vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[iWidget])
								SET_ENTITY_HEADING(t_chasecar_veh[iWidget], fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[iWidget])
								SET_VEHICLE_FIXED(t_chasecar_veh[iWidget])
							ENDIF
						ENDREPEAT
						
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	SET_ROADS_BACK_TO_ORIGINAL(vPlayer_scene_t_chasecar_coord-<<50,50,50>>,
			vPlayer_scene_t_chasecar_coord+<<50,50,50>>)
	bRecord_vehs = FALSE
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_ChaseCar_Widget()
	INT iChaseCount
	
	iCurrent_player_scene_t_chasecar_stage = ENUM_TO_INT(current_player_scene_t_chasecar_stage)
	
	IF bMove_vehs
		PED_VEH_DATA_STRUCT sData    //MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vCoordOffset = <<0,0,0>>
		FLOAT fHead = 0
		VECTOR vDriveOffset = <<0,0,0>>
		FLOAT fDriveSpeed = 0
		
		WHILE bMove_vehs
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX player_veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(player_veh)
					IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_TREVOR, selected_player_scene_t_chasecar_scene,
							sData, vCoordOffset, fHead,
							vDriveOffset, fDriveSpeed)
						SET_ENTITY_COORDS(player_veh, vPlayer_scene_t_chasecar_coord+vCoordOffset)
						SET_ENTITY_HEADING(player_veh, fPlayer_scene_t_chasecar_head+fHead)
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CLEAR_AREA(vPlayer_scene_t_chasecar_coord, 5, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(t_chasecar_veh[0])
				SET_ENTITY_COORDS(t_chasecar_veh[0], vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[0])
				SET_ENTITY_HEADING(t_chasecar_veh[0], fPlayer_scene_t_chasecar_head+fTChaseCar_veh_offset[0])
				
				SET_VEHICLE_COLOUR_COMBINATION(t_chasecar_veh[0], iTChaseCar_veh_colour)
				
				IF NOT IS_PED_INJURED(t_chasecar_ped[0])
					CLEAR_PED_TASKS(t_chasecar_ped[0])
				ENDIF
				
				CLEAR_AREA(vPlayer_scene_t_chasecar_coord+vTChaseCar_veh_offset[0], 5, TRUE)
			ENDIF
		
			
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF bRecord_vehs
		Watch_Player_Scene_T_ChaseCar_Record()
	ENDIF
	IF bSave_vehs
//		INT iSave
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Initialise_Player_Scene_T_ChaseCar_Variables()")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_CHASED iChaseCount
				IF NOT ARE_VECTORS_EQUAL(vTChaseCar_veh_offset[iChaseCount], <<0,0,0>>)
					SAVE_STRING_TO_DEBUG_FILE("	vTChaseCar_veh_offset[")
					SAVE_INT_TO_DEBUG_FILE(iChaseCount)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_VECTOR_TO_DEBUG_FILE(vTChaseCar_veh_offset[iChaseCount])
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_CHASED iChaseCount
				IF (fTChaseCar_veh_offset[iChaseCount] <> 0.0)
					SAVE_STRING_TO_DEBUG_FILE("	fTChaseCar_veh_offset[")
					SAVE_INT_TO_DEBUG_FILE(iChaseCount)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_FLOAT_TO_DEBUG_FILE(fTChaseCar_veh_offset[iChaseCount])
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	//- enums -//")SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_CHASED iChaseCount
				SAVE_STRING_TO_DEBUG_FILE("	t_chasecar_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iChaseCount)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				IF (t_chasecar_ped_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
					SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(t_chasecar_veh_model[iChaseCount]))
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("DUMMY_MODEL_FOR_SCRIPT")
				ENDIF
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			REPEAT iNUM_OF_CHASED iChaseCount
				SAVE_STRING_TO_DEBUG_FILE("	t_chasecar_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iChaseCount)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				IF (t_chasecar_veh_model[iChaseCount] <> DUMMY_MODEL_FOR_SCRIPT)
					SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(t_chasecar_veh_model[iChaseCount]))
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("DUMMY_MODEL_FOR_SCRIPT")
				ENDIF
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
		
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		
		bSave_vehs = FALSE
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_T_ChaseCar_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_t_chasecar_stage = FINISHED_PLAYER_SCENE_T_CHASECAR
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
		bSave_vehs = TRUE
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Control_PS_T_Chasecar_Speech(TEXT_LABEL &tChaseLabel, structTimer &sTimer, INT &iClosestCarID)
	IF IS_STRING_NULL_OR_EMPTY(tChaseLabel)
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL tThisChaseLabel = tChaseLabel
	
	IF get_NOT_PlayerHasStartedNewScene()
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("get_NOT_PlayerHasStartedNewScene",
				iCONSTANTSforchasecar+iNUM_OF_CHASED+2,
				HUD_COLOUR_GREEN, fTrevChaseAlphaMult)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TIMER_STARTED(sTimer)
		START_TIMER_NOW(sTimer)
		
		RETURN FALSE
	ENDIF
	
	IF (tChaseSpeechCount >= iCONST_MAX_Chase_car_conv)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringInt("tChaseSpeechCount:", tChaseSpeechCount,
				iCONSTANTSforchasecar+iNUM_OF_CHASED+2,
				HUD_COLOUR_GREEN, fTrevChaseAlphaMult)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCONST_DELAY_Chase_car_conv	10.0
	FLOAT fDELAY_Chase_car_conv = fCONST_DELAY_Chase_car_conv+((TO_FLOAT(tChaseSpeechCount)/TO_FLOAT(iCONST_MAX_Chase_car_conv))*fCONST_DELAY_Chase_car_conv)
	IF NOT TIMER_DO_WHEN_READY(sTimer, fDELAY_Chase_car_conv)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringFloat("sTimer: ", GET_TIMER_IN_SECONDS(sTimer),
				iCONSTANTSforchasecar+iNUM_OF_CHASED+2,
				HUD_COLOUR_GREEN, fTrevChaseAlphaMult)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(t_chasecar_ped[iClosestCarID])
		iClosestCarID = -1
		RETURN FALSE
	ENDIF
	
	FLOAT fChasePedDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(t_chasecar_ped[iClosestCarID], FALSE))
	
	CONST_FLOAT fDIST_Chase_car_conv	30.0
	CONST_FLOAT fDIST_Chase_car_hint	fCONST_LOST_CHASED_PED_DIST
	IF fChasePedDist2 > (fDIST_Chase_car_conv*fDIST_Chase_car_conv)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringFloat("fChasePedDist:", SQRT(fChasePedDist2),
				iCONSTANTSforchasecar+iNUM_OF_CHASED+2,
				HUD_COLOUR_GREEN, fTrevChaseAlphaMult)
		#ENDIF
		
		IF fChasePedDist2 > (fDIST_Chase_car_hint*fDIST_Chase_car_hint)
			IF IS_GAMEPLAY_HINT_ACTIVE()
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			ENDIF
			iClosestCarID = -1
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (tChaseRandomCount > 0)
		tThisChaseLabel += tChaseRandomCount
		
		#IF IS_DEBUG_BUILD
		IF NOT DOES_TEXT_LABEL_EXIST(tThisChaseLabel)
			
			TEXT_LABEL tThisOtherChaseLabel = tThisChaseLabel
			tThisOtherChaseLabel += "_1"
			IF NOT DOES_TEXT_LABEL_EXIST(tThisOtherChaseLabel)
				TEXT_LABEL_63 tChaseLabelAssert = "\""
				tChaseLabelAssert += tThisChaseLabel
				tChaseLabelAssert += "\" doesnt exist, consider nulling tChaseLabel..."
				
				CPRINTLN(DEBUG_SWITCH, tChaseLabelAssert)
				
				tChaseLabel = ""
				RETURN FALSE
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
//	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, g_pScene_buddy, "LAMAR")

	IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tThisChaseLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
		
		IF (tChaseRandomCount > 0)
			tChaseRandomCount++
		ENDIF
		
		RESTART_TIMER_NOW(sTimer)
		tChaseSpeechCount++
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IsChasePedOk(PED_INDEX PedIndex, VEHICLE_INDEX VehIndex, INT &iStage,
		INT	iCONST_CHASE_PED_DEAD, INT	iCONST_CHASE_VEH_DEAD)
	
	IF NOT DOES_ENTITY_EXIST(PedIndex)
		iStage = iCONST_CHASE_PED_DEAD
		RETURN FALSE
	ENDIF
	
/*
	IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tChaseGiveupSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
		RESTART_TIMER_NOW(speechTimer)
		RETURN TRUE
	ENDIF
*/
	
	IF IS_PED_INJURED(PedIndex)
	
		IF NOT CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tChaseKillSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
			RETURN FALSE
		ELSE
			iStage = iCONST_CHASE_PED_DEAD
			RESTART_TIMER_NOW(speechTimer)
			RETURN FALSE
		ENDIF
		
		tChaseGiveupSpeechLabel = tChaseGiveupSpeechLabel
		
		iStage = iCONST_CHASE_PED_DEAD
		RETURN FALSE
	ENDIF
	IF NOT IS_VEHICLE_DRIVEABLE(VehIndex)
		CLEAR_PED_TASKS(PedIndex)
		iStage = iCONST_CHASE_VEH_DEAD
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
	

FUNC BOOL UpdateClosestCar(INT iChaseCount, INT &iClosestCar)
	
//	FLOAT		fTOO_FAR_TO_CHASE_DIST		= (fCONST_LOST_CHASED_PED_DIST / 3.0)
//	
//	VECTOR player_ped_coord = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	VECTOR t_chasecar_ped_coord = GET_ENTITY_COORDS(t_chasecar_ped[iChaseCount])
//	
//	FLOAT t_chasecar_ped_dist2 = VDIST2(player_ped_coord, t_chasecar_ped_coord)
//	
	IF iClosestCar < 0
//		IF t_chasecar_ped_dist2 < (fTOO_FAR_TO_CHASE_DIST*fTOO_FAR_TO_CHASE_DIST)
			iClosestCar  = iChaseCount
			RETURN TRUE
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ControlChase(INT iChaseCount, INT &iClosestCar)
	
	CONST_INT	iCONST_CHASE_0_waiting					0
	CONST_INT	iCONST_CHASE_1_playback					1
	CONST_INT	iCONST_CHASE_2_driveWander				2
	CONST_INT	iCONST_CHASE_3_followPlayer				3
	CONST_INT	iCONST_CHASE_4_findVehicle				4
	
	CONST_INT	iCONST_CHASE_PED_DEAD					-1
	CONST_INT	iCONST_CHASE_VEH_DEAD					-2
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	SWITCH iPlaybackStage[iChaseCount] 
		CASE iCONST_CHASE_0_waiting
			
			IF NOT IsChasePedOk(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount],
					iPlaybackStage[iChaseCount],
					iCONST_CHASE_PED_DEAD, iCONST_CHASE_VEH_DEAD)
				RETURN FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("waiting... ", iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_PURPLE, fTrevChaseAlphaMult)
			#ENDIF
			
			UpdateClosestCar(iChaseCount, iClosestCar)
			
			IF (fPlayerRecordingSkip >= 0)
				IF IS_VEHICLE_DRIVEABLE(t_chasecar_veh[iChaseCount])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iChaseCount])
						START_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], iRecordingNum[iChaseCount], tRecordingName)
					ELSE
						// Check if we are rendering the final jump cut of a Switch transition
						IF IS_PLAYER_SWITCH_IN_PROGRESS()
							IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
							AND GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], fPlayerRecordingSkip - GET_TIME_POSITION_IN_RECORDING(t_chasecar_veh[iChaseCount]))
	//							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(t_chasecar_veh[iChaseCount])
								
								#IF IS_DEBUG_BUILD
								SAVE_STRING_TO_DEBUG_FILE("skip: position in recording \"")
								SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
								
								IF (iRecordingNum[iChaseCount] < 100)	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
								IF (iRecordingNum[iChaseCount] < 10)	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
								SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iChaseCount])
								
								SAVE_STRING_TO_DEBUG_FILE("\" at time ")
								SAVE_FLOAT_TO_DEBUG_FILE(fPlayerRecordingSkip)
								SAVE_STRING_TO_DEBUG_FILE(": ")
								SAVE_VECTOR_TO_DEBUG_FILE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iChaseCount], fPlayerRecordingSkip, tRecordingName))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								#ENDIF
								
								iPlaybackStage[iChaseCount] = iCONST_CHASE_1_playback
							ENDIF
						ELSE
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], fPlayerRecordingSkip - GET_TIME_POSITION_IN_RECORDING(t_chasecar_veh[iChaseCount]))
//							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(t_chasecar_veh[iChaseCount])
							
							#IF IS_DEBUG_BUILD
							SAVE_STRING_TO_DEBUG_FILE("end: position in recording \"")
							SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
							
							IF (iRecordingNum[iChaseCount] < 100)	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
							IF (iRecordingNum[iChaseCount] < 10)	SAVE_STRING_TO_DEBUG_FILE("0") ENDIF
							SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iChaseCount])
							
							SAVE_STRING_TO_DEBUG_FILE("\" at time ")
							SAVE_FLOAT_TO_DEBUG_FILE(fPlayerRecordingSkip)
							SAVE_STRING_TO_DEBUG_FILE(": ")
							SAVE_VECTOR_TO_DEBUG_FILE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iChaseCount], fPlayerRecordingSkip, tRecordingName))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
							
							iPlaybackStage[iChaseCount] = iCONST_CHASE_1_playback
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		CASE iCONST_CHASE_1_playback
			IF NOT IsChasePedOk(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount],
					iPlaybackStage[iChaseCount],
					iCONST_CHASE_PED_DEAD, iCONST_CHASE_VEH_DEAD)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_PED_SITTING_IN_VEHICLE(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount])
				iPlaybackStage[iChaseCount] = iCONST_CHASE_4_findVehicle
				RETURN FALSE
			ENDIF
			
			UpdateClosestCar(iChaseCount, iClosestCar)
			
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iChaseCount])
			
				VEHICLE_INDEX playerChasecarVeh
				playerChasecarVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(playerChasecarVeh)
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(playerChasecarVeh)
					fPlaybackSpeed[iChaseCount] = 1.0
				ELSE
					CONST_FLOAT fIdealChaseDist			17.5
					CONST_FLOAT fMinChaseDist			7.5
					CONST_FLOAT fMaxChaseDist			30.0
					CONST_FLOAT fMinPlaybackSpeed		0.7
					CONST_FLOAT fMaxPlaybackSpeed		1.2
					CONST_FLOAT fDefaultPlaybackSpeed	1.0
					BOOL bUseAirResistance
					bUseAirResistance					= FALSE	//TRUE
					
					IF NOT (GET_PED_TYPE(t_chasecar_ped[iChaseCount]) = PEDTYPE_COP)
						CALCULATE_PLAYBACK_SPEED_FROM_CHAR(t_chasecar_veh[iChaseCount], PLAYER_PED_ID(), fPlaybackSpeed[iChaseCount],
								fIdealChaseDist, fMinChaseDist, fMaxChaseDist, 
								fMinPlaybackSpeed, fMaxPlaybackSpeed,
								fDefaultPlaybackSpeed,
								bUseAirResistance)
					ELSE
						fPlaybackSpeed[iChaseCount] = 1.0
						
						FLOAT fPlayerSpeed, fChasecarSpeed
						
						fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
						fChasecarSpeed = GET_ENTITY_SPEED(t_chasecar_veh[iChaseCount])
						
	//					SAVE_STRING_TO_DEBUG_FILE("fPlayerSpeed: ")
	//					SAVE_FLOAT_TO_DEBUG_FILE(fPlayerSpeed)
	//					SAVE_STRING_TO_DEBUG_FILE(", fChasecar[")
	//					SAVE_INT_TO_DEBUG_FILE(iChaseCount)
	//					SAVE_STRING_TO_DEBUG_FILE("]Speed: ")
	//					SAVE_FLOAT_TO_DEBUG_FILE(fChasecarSpeed)
						
						fPlaybackSpeed[iChaseCount] = 1.0
						IF(fChasecarSpeed>fPlayerSpeed)
							fPlaybackSpeed[iChaseCount] = fPlayerSpeed/fChasecarSpeed
						ENDIF
						
	//					SAVE_STRING_TO_DEBUG_FILE(", fPlaybackSpeed:")
	//					SAVE_FLOAT_TO_DEBUG_FILE(fPlaybackSpeed[iChaseCount])
	//					SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				
				IF fPlaybackSpeed[iChaseCount] <= 0.05
					STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount])
					CPRINTLN(DEBUG_SWITCH, "recording[", iChaseCount, "] is too slow, stop playback")
					
					RETURN FALSE
				ENDIF
				
				SET_PLAYBACK_SPEED(t_chasecar_veh[iChaseCount], fPlaybackSpeed[iChaseCount])
				
				FLOAT fPlaybackPhase
				fPlaybackPhase = GET_TIME_POSITION_IN_RECORDING(t_chasecar_veh[iChaseCount]) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNum[iChaseCount], tRecordingName)
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("fPlaybackSpeed: ", fPlaybackSpeed[iChaseCount], iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_PURPLE, fTrevChaseAlphaMult)
				
				
				str  = "rec: "
				str += tRecordingName
				
				IF (iRecordingNum[iChaseCount] < 100)	str += "0"	ENDIF
				IF (iRecordingNum[iChaseCount] < 10)	str += "0"	ENDIF
				str += iRecordingNum[iChaseCount]
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 0.0, HUD_COLOUR_PURPLE)
				
				str  = "fPlaybackTime: "
				str += GET_STRING_FROM_FLOAT(GET_TIME_POSITION_IN_RECORDING(t_chasecar_veh[iChaseCount]))
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 1.0, HUD_COLOUR_PURPLE)
				
				str  = "fPlaybackSpeed: "
				str += GET_STRING_FROM_FLOAT(fPlaybackSpeed[iChaseCount])
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 2.0, HUD_COLOUR_GREEN)
				
				str  = "fPlaybackPhase: "
				str += GET_STRING_FROM_FLOAT(fPlaybackPhase)
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 3.0, HUD_COLOUR_GREEN)
				
				IF g_bDrawLiteralSceneString
					ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], RDM_WHOLELINE)
				ENDIF
				#ENDIF
				
				IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(t_chasecar_veh[iChaseCount])
					
					#IF IS_DEBUG_BUILD
					str  = "not using ai"
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 4.0, HUD_COLOUR_PURPLE)
					#ENDIF
					
					IF NOT (GET_PED_TYPE(t_chasecar_ped[iChaseCount]) = PEDTYPE_COP)
						IF IS_AREA_OCCUPIED(
								GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]) - <<1,1,1>>, 
								GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]) + <<1,1,1>>, 
								FALSE, TRUE, FALSE, FALSE, FALSE)
							SET_PLAYBACK_TO_USE_AI(t_chasecar_veh[iChaseCount], Mode)
//							SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(t_chasecar_veh[iChaseCount], 4000, Mode) 
						ENDIF
					ELSE
						REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
						UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
					ENDIF
				ELSE
					
					#IF IS_DEBUG_BUILD
					str  = "USING AI"
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 4.0, HUD_COLOUR_PURPLE)
					#ENDIF
				ENDIF
				
				//recording has reached its end, stop playback
				IF (fPlaybackPhase >= fRecordingQuitPhase)
					STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount])
					CPRINTLN(DEBUG_SWITCH, "recording[", iChaseCount, "] has reached its end, stop playback")
					
					RETURN FALSE
				ENDIF
				
				//recording has been disrupted by the player, stop playback
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(t_chasecar_ped[iChaseCount], PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(t_chasecar_veh[iChaseCount], PLAYER_PED_ID())
					STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount])
					CPRINTLN(DEBUG_SWITCH, "recording[", iChaseCount, "] has been disrupted by the player, stop playback")
					
					RETURN FALSE
				ENDIF
				
				//recording has been disrupted by a vehicle, stop playback
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(t_chasecar_veh[iChaseCount])
					STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount])
					CPRINTLN(DEBUG_SWITCH, "recording[", iChaseCount, "] has been disrupted by a vehicle, stop playback")
					
					RETURN FALSE
				ENDIF
				
				IF (GET_PED_TYPE(t_chasecar_ped[iChaseCount]) = PEDTYPE_COP)
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							STOP_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount])
							CPRINTLN(DEBUG_SWITCH, "recording[", iChaseCount, "] has been disrupted by a vehicle, stop playback")
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//recording has finished, escape with AI
	//			SET_PED_COMBAT_ATTRIBUTES(t_chasecar_ped[iChaseCount], CA_USE_VEHICLE, TRUE)
				
				CLEAR_PED_TASKS(t_chasecar_ped[iChaseCount])
				
				IF NOT (GET_PED_TYPE(t_chasecar_ped[iChaseCount]) = PEDTYPE_COP)
					TASK_VEHICLE_DRIVE_WANDER(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount], CruiseSpeed, Mode)
					SET_PED_KEEP_TASK(t_chasecar_ped[iChaseCount], TRUE)
					
					
					IF (selected_player_scene_t_chasecar_scene = PR_SCENE_T_CR_CHASECAR_a)
						SET_VEHICLE_ENGINE_HEALTH(t_chasecar_veh[iChaseCount], 0.0)
					ENDIF
					
					iPlaybackStage[iChaseCount] = iCONST_CHASE_2_driveWander
				ELSE
					TASK_COMBAT_PED(t_chasecar_ped[iChaseCount], PLAYER_PED_ID())
					SET_PED_COMBAT_ATTRIBUTES(t_chasecar_ped[iChaseCount], CA_USE_VEHICLE, TRUE)
					
					SET_PED_KEEP_TASK(t_chasecar_ped[iChaseCount], TRUE)
					
					REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
					UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
					
					iPlaybackStage[iChaseCount] = iCONST_CHASE_3_followPlayer
				ENDIF
				
				RETURN FALSE
			ENDIF
		BREAK
		CASE iCONST_CHASE_2_driveWander
			IF NOT IsChasePedOk(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount],
					iPlaybackStage[iChaseCount],
					iCONST_CHASE_PED_DEAD, iCONST_CHASE_VEH_DEAD)
				RETURN FALSE
			ENDIF
			
//			IF NOT IS_PED_SITTING_IN_VEHICLE(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount])
//				iPlaybackStage[iChaseCount] = iCONST_CHASE_4_findVehicle
//				RETURN FALSE
//			ENDIF
			
			UpdateClosestCar(iChaseCount, iClosestCar)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("driveWander: ",
					VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(t_chasecar_ped[iChaseCount])),
					iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_YELLOWDARK, fTrevChaseAlphaMult)
			
			str  = "fPlaybackSpeed: "
			str += GET_STRING_FROM_FLOAT(fPlaybackSpeed[iChaseCount])
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 0.0, HUD_COLOUR_YELLOW)
			
			str  = "vdist: "
			str += GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(t_chasecar_ped[iChaseCount])))
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 1.0, HUD_COLOUR_YELLOW)
			
			str  = "fspeed: "
			str += GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(t_chasecar_veh[iChaseCount]))
			str += " / "
			str += GET_STRING_FROM_FLOAT(CruiseSpeed)
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 2.0, HUD_COLOUR_YELLOW)
			
			#ENDIF
			
			IF IS_PED_SITTING_IN_VEHICLE(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount])
				IF (GET_SCRIPT_TASK_STATUS(t_chasecar_ped[iChaseCount], SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK)
					TASK_VEHICLE_DRIVE_WANDER(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount], CruiseSpeed, Mode)
					
					CPRINTLN(DEBUG_SWITCH, "TASK_VEHICLE_DRIVE_WANDER...")
				ELSE
					SET_DRIVE_TASK_CRUISE_SPEED(t_chasecar_ped[iChaseCount], CruiseSpeed)
				ENDIF
			ELSE
				iPlaybackStage[iChaseCount] = iCONST_CHASE_4_findVehicle
				RETURN FALSE
			ENDIF
		BREAK
		CASE iCONST_CHASE_3_followPlayer
			IF NOT IsChasePedOk(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount],
					iPlaybackStage[iChaseCount],
					iCONST_CHASE_PED_DEAD, iCONST_CHASE_VEH_DEAD)
				RETURN FALSE
			ENDIF
			
			UpdateClosestCar(iChaseCount, iClosestCar)
			
			
			//#945904
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
				IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
				AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					CPRINTLN(DEBUG_SWITCH, "t_chasecar_ped[", iChaseCount, "] no longer needed - player not wanted")
					
//					#IF IS_DEBUG_BUILD
//					SAVE_STRING_TO_DEBUG_FILE("iPlaybackStage[")
//					SAVE_INT_TO_DEBUG_FILE(iChaseCount)
//					SAVE_STRING_TO_DEBUG_FILE("]: ")
//					SAVE_INT_TO_DEBUG_FILE(iPlaybackStage[iChaseCount] )
//					SAVE_NEWLINE_TO_DEBUG_FILE()
//
//					SCRIPT_ASSERT("GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0")
//					#ENDIF
					
					CLEAR_PED_TASKS(t_chasecar_ped[iChaseCount])
					SET_PED_AS_NO_LONGER_NEEDED(t_chasecar_ped[iChaseCount])
					RETURN FALSE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("missionFollow: ",
					VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(t_chasecar_ped[iChaseCount])),
					iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_YELLOWDARK, fTrevChaseAlphaMult)
			DrawDebugSceneLineBetweenEntities(PLAYER_PED_ID(), t_chasecar_ped[iChaseCount], HUD_COLOUR_YELLOWDARK)
			
			str  = "fPlaybackSpeed: "
			str += GET_STRING_FROM_FLOAT(fPlaybackSpeed[iChaseCount])
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(t_chasecar_veh[iChaseCount]), 2.0, HUD_COLOUR_YELLOW)
			
			#ENDIF
		BREAK
		
		CASE iCONST_CHASE_4_findVehicle
			IF IS_PED_INJURED(t_chasecar_ped[iChaseCount])
				iPlaybackStage[iChaseCount] = iCONST_CHASE_PED_DEAD
				RETURN FALSE
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE(t_chasecar_veh[iChaseCount])
				iPlaybackStage[iChaseCount] = iCONST_CHASE_VEH_DEAD
				RETURN FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(t_chasecar_ped[iChaseCount])
				DrawLiteralSceneString("findVehicle...", iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_RED, fTrevChaseAlphaMult)
			ENDIF
			
			#ENDIF
			
			IF NOT IS_PED_SITTING_IN_VEHICLE(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount])
				IF NOT IS_PED_GETTING_INTO_A_VEHICLE(t_chasecar_ped[iChaseCount])
					
					CPRINTLN(DEBUG_SWITCH, "TASK_ENTER_VEHICLE(t_chasecar_ped[", iChaseCount, "], t_chasecar_veh[", iChaseCount, "])")
					
					TASK_ENTER_VEHICLE(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount])
				ENDIF
			ELSE
				iPlaybackStage[iChaseCount] = iCONST_CHASE_1_playback
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE iCONST_CHASE_PED_DEAD
			#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(t_chasecar_ped[iChaseCount])
				DrawLiteralSceneString("ped dead...", iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_RED, fTrevChaseAlphaMult)
			ENDIF
			#ENDIF
		BREAK
		CASE iCONST_CHASE_VEH_DEAD
			#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(t_chasecar_veh[iChaseCount])
				DrawLiteralSceneString("veh dead...", iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_RED, fTrevChaseAlphaMult)
			ENDIF
			#ENDIF
			
			IF NOT IS_PED_INJURED(t_chasecar_ped[iChaseCount])
				IF (GET_SCRIPT_TASK_STATUS(t_chasecar_ped[iChaseCount], SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
					SET_PED_CONFIG_FLAG(t_chasecar_ped[iChaseCount], PCF_GetOutUndriveableVehicle, TRUE)
					TASK_SMART_FLEE_PED(t_chasecar_ped[iChaseCount], PLAYER_PED_ID(), 150, -1, FALSE, FALSE)
				ENDIF
			ELSE
				iPlaybackStage[iChaseCount] = iCONST_CHASE_PED_DEAD
				RETURN FALSE
			ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("unknown iPlaybackStage ", iPlaybackStage[iChaseCount], iChaseCount+2+iCONSTANTSforchasecar, HUD_COLOUR_RED)
			#ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_T_CHASECAR_STAGE_ZERO()
	
	VEHICLE_INDEX playerChasecarVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	IF NOT IS_ENTITY_DEAD(playerChasecarVeh)
	
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(playerChasecarVeh)
		
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("playback not going on for player vehicle", 2+iCONSTANTSforchasecar, HUD_COLOUR_PURPLE)
			#ENDIF
		
		ELSE
			
			INT iChaseCount
			REPEAT iNUM_OF_CHASED iChaseCount
				IF IS_VEHICLE_DRIVEABLE(t_chasecar_veh[iChaseCount])
				AND NOT IS_PED_INJURED(t_chasecar_ped[iChaseCount])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
					AND (iRecordingNum[iChaseCount] > 0)
						START_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], iRecordingNum[iChaseCount], tRecordingName)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(t_chasecar_veh[iChaseCount], fPlayerRecordingStart)
						
						TASK_VEHICLE_MISSION(t_chasecar_ped[iChaseCount], t_chasecar_veh[iChaseCount], NULL, MISSION_FOLLOW_RECORDING, 20, Mode, 0.5, 2)
						
						IF (GET_PED_TYPE(t_chasecar_ped[iChaseCount]) = PEDTYPE_COP)
							SET_VEHICLE_SIREN(t_chasecar_veh[iChaseCount], TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_T_CHASECAR_switchInProgress()
	
	INT iChaseCount, iClosestCar = -1
	REPEAT iNUM_OF_CHASED iChaseCount
		ControlChase(iChaseCount, iClosestCar)
	ENDREPEAT
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ELSE
		//CPRINTLN(DEBUG_SWITCH, "IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT		iPlayerChasingSomeone	= -1
FUNC BOOL Do_PS_T_CHASECAR_STAGE_TWO()
		
	INT iChaseCount, iClosestCar = -1
	REPEAT iNUM_OF_CHASED iChaseCount
		ControlChase(iChaseCount, iClosestCar)
	ENDREPEAT
	
	IF (iClosestCar < 0)
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			INT iAlive
			REPEAT iNUM_OF_CHASED iAlive
				IF DOES_ENTITY_EXIST(t_chasecar_ped[iAlive])
					IF NOT IS_PED_INJURED(t_chasecar_ped[iAlive])
						IF IS_GAMEPLAY_HINT_ACTIVE()
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
							iPlayerChasingSomeone = iClosestCar
						ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				iPlayerChasingSomeone = iClosestCar
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			iPlayerChasingSomeone = iClosestCar
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tChaseHintCamLabel)
		CONST_FLOAT	fMAX_Chasing_DIST		150.0	//100.0
		
		
		IF (iPlayerChasingSomeone <> iClosestCar)
		OR (iClosestCar < -1)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			iPlayerChasingSomeone	= iClosestCar
			
			RETURN FALSE
		ENDIF
		
		FLOAT fPlayerDistFromChaseCar = VDIST2(GET_ENTITY_COORDS(t_chasecar_ped[iClosestCar], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		IF (fPlayerDistFromChaseCar < (fMAX_Chasing_DIST*fMAX_Chasing_DIST))
		OR IS_GAMEPLAY_HINT_ACTIVE()
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, t_chasecar_ped[iClosestCar], tChaseHintCamLabel)
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				iPlayerChasingSomeone	= iClosestCar
			ENDIF
		ENDIF
	ENDIF
	
	Control_PS_T_Chasecar_Speech(tChaseRandomSpeechLabel, speechTimer, iClosestCar)
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_T_ChaseCar_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_T_ChaseCar_Variables()
	Setup_Player_Scene_T_ChaseCar()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_T_ChaseCar_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_T_ChaseCar_in_progress
	AND ProgressScene(BIT_TREVOR, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_t_chasecar_stage
			CASE PS_T_CHASECAR_STAGE_ZERO
				IF Do_PS_T_CHASECAR_STAGE_ZERO()
					current_player_scene_t_chasecar_stage = PS_T_CHASECAR_switchInProgress
				ENDIF
			BREAK
			CASE PS_T_CHASECAR_switchInProgress
				IF Do_PS_T_CHASECAR_switchInProgress()
					current_player_scene_t_chasecar_stage = PS_T_CHASECAR_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_T_CHASECAR_STAGE_TWO
				IF Do_PS_T_CHASECAR_STAGE_TWO()
					current_player_scene_t_chasecar_stage = FINISHED_PLAYER_SCENE_T_CHASECAR
				ENDIF
			BREAK
			
			CASE FINISHED_PLAYER_SCENE_T_CHASECAR
				Player_Scene_T_ChaseCar_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_T_ChaseCar_Widget()
		Player_Scene_T_ChaseCar_Debug_Options()
		
		fTrevChaseAlphaMult = 1.0
		IF current_player_scene_t_chasecar_stage = PS_T_CHASECAR_STAGE_TWO
			FLOAT fPlayerTrevChaseDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_t_chasecar_coord)
			
			INT iPed
			REPEAT COUNT_OF(t_chasecar_ped) iPed
				IF DOES_ENTITY_EXIST(t_chasecar_ped[iPed])
					FLOAT fChasePedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(t_chasecar_ped[iPed], FALSE))
					
					IF (fPlayerTrevChaseDist > fChasePedDist)
						fPlayerTrevChaseDist = fChasePedDist
					ENDIF
				ENDIF
			ENDREPEAT
			
			fTrevChaseAlphaMult = 1.0 - (fPlayerTrevChaseDist/fCONST_LOST_CHASED_PED_DIST)
			IF fTrevChaseAlphaMult < 0
				fTrevChaseAlphaMult = 0
			ENDIF
			fTrevChaseAlphaMult = (fTrevChaseAlphaMult*0.8)+0.2
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_t_chasecar_scene,
				iCONSTANTSforchasecar, HUD_COLOUR_BLUELIGHT, fTrevChaseAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_T_ChaseCar_Stage(current_player_scene_t_chasecar_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_T_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				1+iCONSTANTSforchasecar, HUD_COLOUR_BLUELIGHT, fTrevChaseAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_T_ChaseCar_Cleanup()
ENDSCRIPT
