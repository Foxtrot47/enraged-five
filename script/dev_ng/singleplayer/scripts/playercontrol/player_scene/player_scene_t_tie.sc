// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene T_TIE file for use – player_scene_t_tie.sc				 ___
// ___ 																					 ___
// _________________________________________________________________________________________



USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T_TIE_STAGE_ENUM
	PS_T_TIE_switchInProgress,
	PS_T_TIE_STAGE_TWO,
	
	FINISHED_T_TIE
ENDENUM
PS_T_TIE_STAGE_ENUM			current_player_scene_t_tie_stage						= PS_T_TIE_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_t_tie_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_T_Tie_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
PED_INDEX				tieHostage_ped[4]
MODEL_NAMES				eTieHostageModel[4]

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_t_tie_coord
FLOAT				fPlayer_scene_t_tie_head

VECTOR				vTieHostage_coordOffset[4]
VECTOR				vTieHostage_rotOffset[4]

	//- Pickups(INT) -//
INT					iTieHostageStage[4]

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_31		sTieAnimDict
TEXT_LABEL_31		sTieAnimLoop[4]
ANIMATION_FLAGS		eTieAnimFlag = AF_DEFAULT

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//
DECAL_ID tieDecal[6]
DECAL_RENDERSETTING_ID renderSettingsId[6]
VECTOR vTieDecal_coordOffset[6]

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_scene_t_tie_widget
INT				iCurrent_player_scene_t_tie_stage
BOOL			bMove_peds, bSave_peds
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_T_Tie_Cleanup()
	INT iCleanup
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eTieHostageModel) iCleanup
		IF (eTieHostageModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eTieHostageModel[iCleanup])
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	IF NOT IS_STRING_NULL_OR_EMPTY(sTieAnimDict)
		REMOVE_ANIM_DICT(sTieAnimDict)
	ENDIF
	
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	REPEAT COUNT_OF(tieDecal) iCleanup
		IF IS_DECAL_ALIVE(tieDecal[iCleanup])
			//
		ENDIF
	ENDREPEAT
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_t_tie_widget)
		DELETE_WIDGET_GROUP(player_scene_t_tie_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_T_Tie_Finished()
	//CLEAR_CprintS()
	bPlayer_Scene_T_Tie_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_T_Tie_Variables()
	
	selected_player_scene_t_tie_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_t_tie_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_t_tie_scene, vPlayer_scene_t_tie_coord, fPlayer_scene_t_tie_head, tPlayer_scene_t_tie_room)
	
	PED_SCENE_STRUCT sPedScene
	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	
	sPedScene.iStage = 0
	sPedScene.eScene = selected_player_scene_t_tie_scene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
	
	SWITCH selected_player_scene_t_tie_scene
		CASE PR_SCENE_T_NAKED_ISLAND
			//- text labels -//
		/*	sTieAnimDict = "misslamar1dead_body"
			sTieAnimLoop[0] = "dead_idle"
			sTieAnimLoop[1] = "dead_idle"
			sTieAnimLoop[2] = "dead_idle"
			sTieAnimLoop[3] = "dead_idle"	*/
			
			sTieAnimDict = "DEAD"
			sTieAnimLoop[0] = "Dead_A"
			sTieAnimLoop[1] = "Dead_D"
			sTieAnimLoop[2] = "Dead_E"
			sTieAnimLoop[3] = "Dead_G"	
			
			//- vectors -//
			vTieHostage_coordOffset[0] = <<14.6130, 8.4563, 0.9751>>					//<<2804.4580, -1445.2747, 1.5270>> - vPlayer_scene_t_tie_coord
			vTieHostage_coordOffset[1] = <<10.6489, 8.6708, 0.7701>>					//<<2800.4939, -1445.0602, 1.3220>> - vPlayer_scene_t_tie_coord
			vTieHostage_coordOffset[2] = <<6.3982, 1.4604, 1.1000>>						//<<2796.2432, -1452.2705, 1.6519>> - vPlayer_scene_t_tie_coord
			vTieHostage_coordOffset[3] = <<4.2327, 5.8640, 0.9000		- 0.15>>		//<<2794.0776, -1447.8669, 1.4519>> - vPlayer_scene_t_tie_coord

			//- floats -//
			vTieHostage_rotOffset[0] = <<0.0000, 0.0000, 23.0470>>		//<<0.0000, 0.0000, -26.5130>> - fPlayer_scene_t_tie_head
			vTieHostage_rotOffset[1] = <<0.0000, 0.0000, 97.2582>>		//<<0.0000, 0.0000, 47.6982>> - fPlayer_scene_t_tie_head
			vTieHostage_rotOffset[2] = <<0.0000, 0.0000, 34.2158>>		//<<0.0000, 0.0000, -15.3442>> - fPlayer_scene_t_tie_head
			vTieHostage_rotOffset[3] = <<0.0000, 0.0000, 61.2158>>		//<<0.0000, 0.0000, 11.6558>> - fPlayer_scene_t_tie_head

			//- enums -//
			eTieHostageModel[0] = G_M_Y_LOST_01
			eTieHostageModel[1] = G_M_Y_LOST_01
			eTieHostageModel[2] = G_M_Y_LOST_02
			eTieHostageModel[3] = G_M_Y_LOST_03
			
			eTieAnimFlag = AF_ENDS_IN_DEAD_POSE | AF_USE_MOVER_EXTRACTION
			
			//- ints -//
			iTieHostageStage[0] = 1
			iTieHostageStage[1] = 1
			iTieHostageStage[2] = 1
			iTieHostageStage[3] = 1
			
			renderSettingsId[0] = DECAL_RSID_BLOOD_SPLATTER
			renderSettingsId[1] = DECAL_RSID_BLOOD_SPLATTER
			renderSettingsId[2] = DECAL_RSID_BLOOD_SPLATTER
			renderSettingsId[3] = DECAL_RSID_BLOOD_SPLATTER
			
			vTieDecal_coordOffset[0] = <<2803.76025, -1445.53442, 0.37364>>-vPlayer_scene_t_tie_coord+<<0,0,0.1>>				//vTieHostage_coordOffset[0]+<<GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), 0>>
			vTieDecal_coordOffset[1] = <<2800.02197, -1444.86328, 0.28778>>-vPlayer_scene_t_tie_coord+<<0,0,0.1>>				//vTieHostage_coordOffset[1]+<<GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), 0>>
			vTieDecal_coordOffset[2] = <<2795.93359, -1452.40430, 0.60450>>-vPlayer_scene_t_tie_coord+<<0,0,0.1>>				//vTieHostage_coordOffset[2]+<<GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), 0>>
			vTieDecal_coordOffset[3] = <<2794.30054, -1448.03308, 0.28483>>-vPlayer_scene_t_tie_coord+<<0,0,0.1>>				//vTieHostage_coordOffset[3]+<<GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), GET_RANDOM_FLOAT_IN_RANGE(0.25,0.25), 0>>
			
			renderSettingsId[4] = DECAL_RSID_BLOOD_DIRECTIONAL
			renderSettingsId[5] = DECAL_RSID_BLOOD_DIRECTIONAL
			vTieDecal_coordOffset[4] = ((vTieDecal_coordOffset[0]+vTieDecal_coordOffset[2]) / 2.0)+<<GET_RANDOM_FLOAT_IN_RANGE(0.5,0.5), GET_RANDOM_FLOAT_IN_RANGE(0.5,0.5), 0>>
			vTieDecal_coordOffset[5] = ((vTieDecal_coordOffset[1]+vTieDecal_coordOffset[2]) / 2.0)+<<GET_RANDOM_FLOAT_IN_RANGE(0.5,0.5), GET_RANDOM_FLOAT_IN_RANGE(0.5,0.5), 0>>
			
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SWITCH, "invalid selected_player_scene_t_tie_scene: ", Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_tie_scene))
			#ENDIF
			
			//- text labels -//
			sTieAnimDict = ""
			sTieAnimLoop[0] = ""
			sTieAnimLoop[1] = ""
			sTieAnimLoop[2] = ""
			sTieAnimLoop[3] = ""
			
			//- vectors -//
			eTieHostageModel[0] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[0] = <<0,0,0>>		vTieHostage_rotOffset[0] = <<0,0,0>>
			eTieHostageModel[1] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[1] = <<0,0,0>>		vTieHostage_rotOffset[1] = <<0,0,0>>
			eTieHostageModel[2] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[2] = <<0,0,0>>		vTieHostage_rotOffset[2] = <<0,0,0>>
			eTieHostageModel[3] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[3] = <<0,0,0>>		vTieHostage_rotOffset[3] = <<0,0,0>>
			
			eTieAnimFlag = AF_DEFAULT
			
			//- ints -//
			iTieHostageStage[0] = -1
			iTieHostageStage[1] = -1
			iTieHostageStage[2] = -1
			iTieHostageStage[3] = -1
			
			renderSettingsId[0] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			renderSettingsId[1] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			renderSettingsId[2] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			renderSettingsId[3] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			renderSettingsId[4] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			renderSettingsId[5] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_T_TIE_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_T_Tie()
	INT iSetup
	
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		//- request models - peds -//
		
		REPEAT COUNT_OF(eTieHostageModel) iSetup
			IF (eTieHostageModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(eTieHostageModel[iSetup])
				IF NOT HAS_MODEL_LOADED(eTieHostageModel[iSetup])
					REQUEST_MODEL(eTieHostageModel[iSetup])
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		//- request models - vehicles -//
		//- request models - objects -//
		//- request models - weapons -//
		//- request anims and ptfx --//
		IF NOT IS_STRING_NULL_OR_EMPTY(sTieAnimDict)
			REQUEST_ANIM_DICT(sTieAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sTieAnimDict)
				REQUEST_ANIM_DICT(sTieAnimDict)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		
		//- request vehicle recordings -//
		//- request interior models -//
		//- wait for assets to load -//
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	
	REPEAT COUNT_OF(tieHostage_ped) iSetup
		IF (eTieHostageModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
			tieHostage_ped[iSetup] = CREATE_PED(PEDTYPE_MISSION, eTieHostageModel[iSetup], vPlayer_scene_t_tie_coord+vTieHostage_coordOffset[iSetup])
			SET_ENTITY_ROTATION(tieHostage_ped[iSetup], <<0,0,fPlayer_scene_t_tie_head>>+vTieHostage_rotOffset[iSetup])
			SET_PED_RANDOM_COMPONENT_VARIATION(tieHostage_ped[iSetup])
			
			
			IF (selected_player_scene_t_tie_scene = PR_SCENE_T_UNDERPIER)
				SET_PED_COMPONENT_VARIATION(tieHostage_ped[iSetup], PED_COMP_TORSO, 1, 0)
				SET_PED_COMPONENT_VARIATION(tieHostage_ped[iSetup], PED_COMP_LEG, 0,0)
			ELSE
				SET_PED_RANDOM_COMPONENT_VARIATION(tieHostage_ped[iSetup])
			ENDIF
			
			SET_PED_COMBAT_ATTRIBUTES(tieHostage_ped[iSetup], CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tieHostage_ped[iSetup], CA_ALWAYS_FLEE, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tieHostage_ped[iSetup], TRUE)
//			SET_PED_CAN_RAGDOLL(tieHostage_ped[iSetup], FALSE)
			
			SET_PED_CAN_BE_TARGETTED(tieHostage_ped[iSetup], FALSE)
			
			CPRINTLN(DEBUG_SWITCH, "TASK_PLAY_ANIM(tieHostage_ped[", iSetup, "], \"", sTieAnimDict, "\", \"", sTieAnimLoop[iSetup], "\")")
			TASK_PLAY_ANIM_ADVANCED(tieHostage_ped[iSetup], sTieAnimDict, sTieAnimLoop[iSetup],
					vPlayer_scene_t_tie_coord+vTieHostage_coordOffset[iSetup],
					<<0,0,fPlayer_scene_t_tie_head>>+vTieHostage_rotOffset[iSetup],
					INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
					eTieAnimFlag)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(tieHostage_ped[iSetup])
			
			SET_PED_KEEP_TASK(tieHostage_ped[iSetup], TRUE)
			
			SET_PED_RESET_FLAG(tieHostage_ped[iSetup], PRF_AllowUpdateIfNoCollisionLoaded, TRUE)
			
			/*
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tieHostage_ped[iSetup], TRUE)
			*/
			
			IF ENUM_TO_INT(renderSettingsId[iSetup]) > 0
				APPLY_PED_BLOOD_BY_ZONE(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_HEAD),			0.360, 0.710, "ShotgunSmall")
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_HEAD, 				0.810, 0.733, BDT_STAB)
				
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_TORSO, 				0.940, 0.590, BDT_SHOTGUN_LARGE)

				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_RIGHT_ARM,			0.240, 0.620, BDT_STAB)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_LEFT_ARM,			0.000, 0.150, BDT_SHOTGUN_SMALL)

				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_RIGHT_LEG,			0.460, 0.853, BDT_SHOTGUN_LARGE)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(tieHostage_ped[iSetup], PDZ_LEFT_LEG,			0.308, 0.786, BDT_SHOTGUN_LARGE)
				
				APPLY_PED_BLOOD_SPECIFIC(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_RIGHT_ARM),	0.375, 0.398, 000, 1.0, -1, 0.0, "BasicSlash")
				APPLY_PED_BLOOD_SPECIFIC(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_RIGHT_ARM),	0.500, 0.600, 000, 1.0, -1, 0.0, "BasicSlash")
				
				APPLY_PED_BLOOD_SPECIFIC(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_RIGHT_ARM),	0.600, 0.250, 050, 0.5, -1, 0.0, "BasicSlash")
				APPLY_PED_BLOOD_SPECIFIC(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_RIGHT_ARM),	0.650, 0.325, 050, 0.5, -1, 0.0, "BasicSlash")
				
				APPLY_PED_BLOOD_SPECIFIC(tieHostage_ped[iSetup], ENUM_TO_INT(PDZ_TORSO),		0.580, 0.704, 000, 1.0, -1, 0.0, "ShotgunLarge")
			ENDIF						
			
		ENDIF
	ENDREPEAT
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	REPEAT COUNT_OF(tieDecal) iSetup
			
		IF ENUM_TO_INT(renderSettingsId[iSetup]) > 0
			VECTOR dir = <<0.0, 0.0, -1.0>>
			VECTOR side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
			
			FLOAT width = 1.5
			FLOAT height = 1.5
			
			FLOAT colR = 0.196
			FLOAT colG = 0
			FLOAT colB = 0
			FLOAT colA = 1.0
			
			FLOAT life = -1
			
			tieDecal[iSetup] = ADD_DECAL(renderSettingsId[iSetup],
					vPlayer_scene_t_tie_coord+vTieDecal_coordOffset[iSetup], dir, side,
					width, height,
					colR, colG, colB, colA,
					life)
			
			WAIT(0)
		ENDIF
	ENDREPEAT
	
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_T_Tie_Stage(PS_T_TIE_STAGE_ENUM this_player_scene_t_tie_stage)
	SWITCH this_player_scene_t_tie_stage
		CASE PS_T_TIE_switchInProgress
			RETURN "PS_T_TIE_switchInProgress"
		BREAK
		CASE PS_T_TIE_STAGE_TWO
			RETURN "PS_T_TIE_STAGE_TWO"
		BREAK
		
		CASE FINISHED_T_TIE
			RETURN "FINISHED_T_TIE"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_T_Tie_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_T_Tie_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_t_tie.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_tie_scene)
	
	player_scene_t_tie_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_T_TIE_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_T_Tie_Stage(INT_TO_ENUM(PS_T_TIE_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_t_tie_stage", iCurrent_player_scene_t_tie_stage)
		
		START_WIDGET_GROUP("TieHostage_createOffset")
			REPEAT COUNT_OF(tieHostage_ped) iWidget
				IF (eTieHostageModel[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
					ADD_WIDGET_STRING("tieHostage_ped")
					
					FLOAT fTieHostageCoordOffsetMag
					fTieHostageCoordOffsetMag = VMAG(vTieHostage_coordOffset[iWidget])
					
					IF (fTieHostageCoordOffsetMag < ABSF(vTieHostage_coordOffset[iWidget].x))
						fTieHostageCoordOffsetMag = ABSF(vTieHostage_coordOffset[iWidget].x)
					ENDIF
					IF (fTieHostageCoordOffsetMag < ABSF(vTieHostage_coordOffset[iWidget].y))
						fTieHostageCoordOffsetMag = ABSF(vTieHostage_coordOffset[iWidget].y)
					ENDIF
					IF (fTieHostageCoordOffsetMag < ABSF(vTieHostage_coordOffset[iWidget].z))
						fTieHostageCoordOffsetMag = ABSF(vTieHostage_coordOffset[iWidget].z)
					ENDIF
					
					fTieHostageCoordOffsetMag *= 2
					IF (fTieHostageCoordOffsetMag < 20.0)
						fTieHostageCoordOffsetMag = 20.0
					ENDIF
					
					ADD_WIDGET_VECTOR_SLIDER("vTieHostage_coordOffset", vTieHostage_coordOffset[iWidget],
						-fTieHostageCoordOffsetMag, fTieHostageCoordOffsetMag,
						fTieHostageCoordOffsetMag * 0.005)
					ADD_WIDGET_VECTOR_SLIDER("vTieHostage_rotOffset", vTieHostage_rotOffset[iWidget], -180, 180, 1.0)
				ENDIF
			ENDREPEAT
			
			ADD_WIDGET_BOOL("bMove_peds", bMove_peds)
			ADD_WIDGET_BOOL("bSave_peds", bSave_peds)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_Tie_Widget()
	iCurrent_player_scene_t_tie_stage = ENUM_TO_INT(current_player_scene_t_tie_stage)
	
	INT iWidget
	IF bMove_peds
		
		IF NOT (g_iDebugSelectedFriendConnDisplay > 0)
			g_iDebugSelectedFriendConnDisplay = 1
		ENDIF
		
		REPEAT COUNT_OF(tieHostage_ped) iWidget
			IF IS_PED_INJURED(tieHostage_ped[iWidget])
				SET_ENTITY_HEALTH(tieHostage_ped[iWidget], 200)
				REVIVE_INJURED_PED(tieHostage_ped[iWidget])
			ELSE
//				CLEAR_PED_TASKS(tieHostage_ped[iWidget])
				
				TASK_PLAY_ANIM_ADVANCED(tieHostage_ped[iWidget], sTieAnimDict, sTieAnimLoop[iWidget],
						vPlayer_scene_t_tie_coord+vTieHostage_coordOffset[iWidget],
						<<0,0,fPlayer_scene_t_tie_head>>+vTieHostage_rotOffset[iWidget],
						INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
						eTieAnimFlag)
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	IF bSave_peds
		
		SAVE_STRING_TO_DEBUG_FILE("	\"player_scene_t_tie.sc\" - bSave_peds")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(tieHostage_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vTieHostage_coordOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTieHostage_coordOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_t_tie_coord+vTieHostage_coordOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - vPlayer_scene_t_tie_coord")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(tieHostage_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vTieHostage_rotOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTieHostage_rotOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(<<0,0,fPlayer_scene_t_tie_head>>+vTieHostage_rotOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - fPlayer_scene_t_tie_head")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bSave_peds = FALSE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_T_Tie_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_t_tie_stage = FINISHED_T_TIE
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
		bSave_peds = TRUE
	ENDIF
	
//	IF IS_KEYBOARD_KEY_PRESSED(KEY_W)
//		IF SET_PED_VARIATIONS_FOR_SCENE(selected_player_scene_t_tie_scene, PLAYER_PED_ID())
//			STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
//		ENDIF
//	ENDIF
	
	
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL ControlHostagePeds(PED_INDEX &hostagePedArray[], INT &hostageStage[])
	
	BOOL bIsHostageAlive = FALSE
	
	INT iHostage
	REPEAT COUNT_OF(hostagePedArray) iHostage
		IF NOT IS_PED_INJURED(hostagePedArray[iHostage])
			
			#IF IS_DEBUG_BUILD
			IF bMove_peds
				DrawLiteralSceneStringInt("moving... ", iHostage, iHostage+7, HUD_COLOUR_GREENLIGHT)
				RETURN FALSE
			ENDIF
			#ENDIF
			
			SWITCH hostageStage[iHostage]
				CASE 0	//
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("tied ", iHostage, iHostage+7, HUD_COLOUR_GREENLIGHT)
					#ENDIF
				BREAK
				CASE 1	//
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("dying... ", iHostage, iHostage+7, HUD_COLOUR_GREENLIGHT)
					#ENDIF
					
					/*
					SET_PED_CAN_RAGDOLL(tieHostage_ped[iHostage], TRUE)
//					SET_PED_TO_RAGDOLL_WITH_FALL(tieHostage_ped[iHostage], 1000, 3000,
//							TYPE_DIE_FROM_HIGH,
//							GET_ENTITY_FORWARD_VECTOR(tieHostage_ped[iHostage]), 
//							100.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					SET_PED_TO_RAGDOLL(tieHostage_ped[iHostage], 1000, 3000,
							TASK_RELAX, FALSE, FALSE)
					
					SET_ENTITY_HEALTH(tieHostage_ped[iHostage], 0)
					*/
					
//					WAIT(0)
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("unknown ", iHostage, iHostage+7, HUD_COLOUR_PURE_WHITE)
					#ENDIF
					
					SCRIPT_ASSERT("ControlHostagePeds")
				BREAK
			ENDSWITCH
			
			bIsHostageAlive = TRUE
		ELIF DOES_ENTITY_EXIST(hostagePedArray[iHostage])
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("dead ", iHostage, iHostage+7, HUD_COLOUR_REDLIGHT)
			
			TEXT_LABEL_63 str  = "dead["
			str += iHostage
			str += "]"
			DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(hostagePedArray[iHostage], FALSE), 0.0, HUD_COLOUR_REDLIGHT)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sTieAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(sTieAnimLoop[iHostage])
				str  = "\""
				str += sTieAnimDict
				str += "\\"
				str += sTieAnimLoop[iHostage]
				str += "\""
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(hostagePedArray[iHostage], FALSE), 1.0, HUD_COLOUR_REDLIGHT)
			ENDIF
			#ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("doesnt exist ", iHostage, iHostage+7, HUD_COLOUR_RED, 0.5)
			#ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN bIsHostageAlive
ENDFUNC
// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_T_TIE_switchInProgress()
	
	ControlHostagePeds(tieHostage_ped, iTieHostageStage)
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ELSE
		//CPRINTLN(DEBUG_SWITCH, "IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_T_TIE_STAGE_TWO()
	ControlHostagePeds(tieHostage_ped, iTieHostageStage)
	
	FLOAT fPlayerDist2FromCoord = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_t_tie_coord)
	
	#IF IS_DEBUG_BUILD
	DrawLiteralSceneStringFloat("vdist: ", SQRT(fPlayerDist2FromCoord),
			COUNT_OF(tieHostage_ped)+7, HUD_COLOUR_BLUELIGHT)
	#ENDIF
	
	IF (fPlayerDist2FromCoord > (100.0*100.0))
		CPRINTLN(DEBUG_SWITCH, "player is 100m+ from the chateau")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_T_Tie_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_T_Tie_Variables()
	Setup_Player_Scene_T_Tie()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_T_Tie_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_T_Tie_in_progress
	AND ProgressScene(BIT_TREVOR, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_t_tie_stage
			CASE PS_T_TIE_switchInProgress
				IF Do_PS_T_TIE_switchInProgress()
					current_player_scene_t_tie_stage = PS_T_TIE_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_T_TIE_STAGE_TWO
				IF Do_PS_T_TIE_STAGE_TWO()
					current_player_scene_t_tie_stage = FINISHED_T_TIE
				ENDIF
			BREAK
			
			CASE FINISHED_T_TIE
				Player_Scene_T_Tie_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_T_Tie_Widget()
		Player_Scene_T_Tie_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_t_tie_scene,
				5, HUD_COLOUR_BLUELIGHT)
		STRING sStage = Get_String_From_Player_Scene_T_tie_Stage(current_player_scene_t_tie_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_T_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				6, HUD_COLOUR_BLUELIGHT)
		#ENDIF
	ENDWHILE
	
	Player_Scene_T_Tie_Cleanup()
ENDSCRIPT
