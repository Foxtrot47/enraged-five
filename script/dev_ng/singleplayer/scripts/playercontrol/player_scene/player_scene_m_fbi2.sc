// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene M_FBI2 file for use – player_scene_m_fbi2.sc			 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSfortaunting			3


// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_M_FBI2_STAGE_ENUM
	PS_M_FBI2_switchInProgress,
	PS_M_FBI2_STAGE_TWO,
	PS_M_FBI2_STAGE_THREE,
	
	FINISHED_PLAYER_SCENE_M_FBI2
ENDENUM
PS_M_FBI2_STAGE_ENUM	current_player_scene_m_fbi2_stage						= PS_M_FBI2_switchInProgress
PED_REQUEST_SCENE_ENUM	selected_player_scene_m_fbi2_scene				= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

/*
// for mag demo - struct to pass over entities into FBI2
STRUCT MAG_DEMO_FBI2_ENTITIES_STRUCT
	BLIP_INDEX blip
	BOOL bBlipActive
	
	VEHICLE_INDEX heliVehicle
	VEHICLE_INDEX bikeVehicle
	
	VEHICLE_INDEX truck01Vehicle
	VEHICLE_INDEX truck02Vehicle
	VEHICLE_INDEX fibVehicle
	
	PED_INDEX trevorPed
	PED_INDEX franklinPed
	PED_INDEX fibPed
	BOOL bCreated
ENDSTRUCT
MAG_DEMO_FBI2_ENTITIES_STRUCT g_sMagDemoFBI2Entities
*/

structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_M_Fbi2_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
enumCharacterList	eFbi2_trevor_pedID = CHAR_TREVOR
VECTOR 				vFbi2_trevor_coord = <<1373.39, -2072.44, 51.38>>
FLOAT 				fFbi2_trevor_heading = 137.37

enumCharacterList	eFbi2_Franklin_pedID = CHAR_FRANKLIN

MODEL_NAMES			eFbi2_fbiAgent_model = CS_FBISUIT_01	//S_M_M_FIBOFFICE_02
VECTOR 				vFbi2_fbiAgent_coord = <<1372.20, -2073.76, 51.12>>
FLOAT 				fFbi2_fbiAgent_heading = -38.38

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
MODEL_NAMES			eFbi2_heli_model = FROGGER
VECTOR 				vFbi2_heli_coord = <<1368.5, -2076.7, 51.00>>
FLOAT 				fFbi2_heli_heading = -15.08

enumCharacterList	eFbi2_bike_vehID = CHAR_FRANKLIN
VECTOR 				vFbi2_bike_coord = << 1373.42, -2079.67, 51.00>>
FLOAT 				fFbi2_bike_heading = -78.9

MODEL_NAMES			eFbi2_truck01_model = UTILLITRUCK3
VECTOR 				vFbi2_truck01_coord = <<1390.31, -2050.63, 51.00>>
FLOAT 				fFbi2_truck01_heading = 74.2
FLOAT 				fFbi2_truck01_dirt = 0.19

MODEL_NAMES			eFbi2_truck02_model = UTILLITRUCK2
VECTOR 				vFbi2_truck02_coord = <<1400.29, -2053.63, 51.00>>
FLOAT 				fFbi2_truck02_heading = 166.8
FLOAT 				fFbi2_truck02_dirt = 0.37

MODEL_NAMES			eFbi2_fbiVeh_model = FBI2
VECTOR 				vFbi2_fbiVeh_coord = <<1365.29, -2065.67, 51.00>>
FLOAT 				fFbi2_fbiVeh_heading = -83.30
FLOAT 				fFbi2_fbiVeh_dirt = 0.0

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//
INT					iSyncSceneA, iSyncSceneB
VECTOR 				vSyncScenePos = <<1372.64, -2073.13, 51.00>>
STRING				sSyncSceneAnimDict	= "MISSFBI2_intro"
STRING				sSyncSceneFIBClip	= "TrevFIB_talkLoop_FIB"
STRING				sSyncSceneTrevClip	= "TrevFIB_talkLoop_trev"
STRING				sSyncSceneFranClip	= "FR_byBike"

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_m_fbi2_coord, vPlayer_scene_m_fbi2_trigger
FLOAT				fPlayer_scene_m_fbi2_head

//VECTOR				vRoadsOffCentre = << 1360.8590, -2008.7632, 51.1790 >>
//VECTOR				vRoadsOffMin = vRoadsOffCentre - <<10,10,5>>
//VECTOR				vRoadsOffMax = vRoadsOffCentre + <<10,10,5>>

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
INT					iRecordingNum = -1
TEXT_LABEL			tRecordingName = ""

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_scene_m_fbi2_widget
INT				iCurrent_player_scene_m_fbi2_stage
BOOL			bRecord_vehs
INT				iDrawLiteralSceneRow = 0
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_M_Fbi2_Cleanup()
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	IF (eFbi2_trevor_pedID <> NO_CHARACTER)
		SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(eFbi2_trevor_pedID)
	ENDIF
	IF (eFbi2_Franklin_pedID <> NO_CHARACTER)
		SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(eFbi2_Franklin_pedID)
	ENDIF
	IF (eFbi2_fbiAgent_model <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eFbi2_fbiAgent_model)
	ENDIF
	
	IF (eFbi2_bike_vehID <> NO_CHARACTER)
		SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(eFbi2_bike_vehID, VEHICLE_TYPE_BIKE)
	ENDIF
	IF (eFbi2_heli_model <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eFbi2_heli_model)
	ENDIF
	IF (eFbi2_truck01_model <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eFbi2_truck01_model)
	ENDIF
	IF (eFbi2_truck02_model <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eFbi2_truck02_model)
	ENDIF
	IF (eFbi2_fbiVeh_model <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eFbi2_fbiVeh_model)
	ENDIF
	
	//- remove anims from the memory -//
	IF NOT IS_STRING_NULL_OR_EMPTY(sSyncSceneAnimDict)
		REMOVE_ANIM_DICT(sSyncSceneAnimDict)
	ENDIF
	
	//- remove vehicle recordings from the memory -//
	IF  (iRecordingNum >= 0)
	AND NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
		REMOVE_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
	ENDIF
	
	//- clear sequences -//
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
//	SET_ROADS_BACK_TO_ORIGINAL(vRoadsOffMin, vRoadsOffMax)
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_m_fbi2_widget)
		DELETE_WIDGET_GROUP(player_scene_m_fbi2_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_M_Fbi2_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_M_Fbi2_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_M_Fbi2_Variables()
	selected_player_scene_m_fbi2_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_m_fbi2_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_m_fbi2_scene, vPlayer_scene_m_fbi2_coord, fPlayer_scene_m_fbi2_head, tPlayer_scene_m_fbi2_room)
	
	FLOAT fRecording_start, fRecording_skip, fRecording_stop
	FLOAT fSpeed_switch, fSpeed_exit
	
	GET_PLAYER_VEH_RECORDING_FOR_SCENE(selected_player_scene_m_fbi2_scene,
			tRecordingName, iRecordingNum,
			fRecording_start, fRecording_skip, fRecording_stop,
			fSpeed_switch, fSpeed_exit)
	
	SWITCH selected_player_scene_m_fbi2_scene
		CASE PR_SCENE_M_MD_FBI2
			vPlayer_scene_m_fbi2_trigger =  <<1370, -2034, 50>>
			
			eFbi2_trevor_pedID = CHAR_TREVOR 		vFbi2_trevor_coord = <<1373.39, -2072.44, 51.38>>		fFbi2_trevor_heading = 137.37
			eFbi2_Franklin_pedID = CHAR_FRANKLIN
			eFbi2_fbiAgent_model = CS_FBISUIT_01	vFbi2_fbiAgent_coord = <<1372.20, -2073.76, 51.12>>		fFbi2_fbiAgent_heading = -38.38
			
			eFbi2_heli_model = FROGGER 				vFbi2_heli_coord = <<1368.5, -2076.7, 51.00>>			fFbi2_heli_heading = -15.08
			eFbi2_bike_vehID = CHAR_FRANKLIN 		vFbi2_bike_coord = << 1373.42, -2079.67, 51.00>> 		fFbi2_bike_heading = -78.9
			eFbi2_truck01_model = UTILLITRUCK3 		vFbi2_truck01_coord = <<1390.31, -2050.63, 51.00>> 		fFbi2_truck01_heading = 74.2		fFbi2_truck01_dirt = 0.19
			eFbi2_truck02_model = UTILLITRUCK2 		vFbi2_truck02_coord = <<1400.29, -2053.63, 51.00>> 		fFbi2_truck02_heading = 166.8		fFbi2_truck02_dirt = 0.37
			eFbi2_fbiVeh_model = FBI2 				vFbi2_fbiVeh_coord = <<1365.29, -2065.67, 51.00>>		fFbi2_fbiVeh_heading = -83.30		fFbi2_fbiVeh_dirt = 0.0
			
			vSyncScenePos = <<1372.64, -2073.13, 51.00>>
			sSyncSceneAnimDict	= "MISSFBI2_intro"
			sSyncSceneFIBClip	= "TrevFIB_talkLoop_FIB"
			sSyncSceneTrevClip	= "TrevFIB_talkLoop_trev"
			sSyncSceneFranClip	= "FR_byBike"
			
		BREAK
		CASE PR_SCENE_F_MD_FRANKLIN2
			vPlayer_scene_m_fbi2_trigger =  <<-710.0626, 5310.5493, 70.6050>>
			
			eFbi2_trevor_pedID = NO_CHARACTER				vFbi2_trevor_coord = <<0,0,0>>					fFbi2_trevor_heading = 0
			eFbi2_Franklin_pedID = NO_CHARACTER
			eFbi2_fbiAgent_model = DUMMY_MODEL_FOR_SCRIPT	vFbi2_fbiAgent_coord = <<0,0,0>>				fFbi2_fbiAgent_heading = 0
			
			eFbi2_heli_model = DUMMY_MODEL_FOR_SCRIPT		vFbi2_heli_coord = <<0,0,0>>					fFbi2_heli_heading = 0
			eFbi2_bike_vehID = NO_CHARACTER 				vFbi2_bike_coord = << 0,0,000>> 				fFbi2_bike_heading = 0
			eFbi2_truck01_model = DUMMY_MODEL_FOR_SCRIPT 	vFbi2_truck01_coord = <<0,0,0>> 				fFbi2_truck01_heading = 0			fFbi2_truck01_dirt = 0.19
			eFbi2_truck02_model = DUMMY_MODEL_FOR_SCRIPT 	vFbi2_truck02_coord = <<0,0,0>> 				fFbi2_truck02_heading = 0			fFbi2_truck02_dirt = 0.37
			eFbi2_fbiVeh_model = DUMMY_MODEL_FOR_SCRIPT 	vFbi2_fbiVeh_coord = <<0,0,0>>					fFbi2_fbiVeh_heading = 0			fFbi2_fbiVeh_dirt = 0.0
			
			vSyncScenePos = <<0,0,0>>
			sSyncSceneAnimDict	= ""
			sSyncSceneFIBClip	= ""
			sSyncSceneTrevClip	= ""
			sSyncSceneFranClip	= ""
			
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_m_fbi2_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_fbi2_scene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(str)PRINTNL()
			SCRIPT_ASSERT(str)
			#ENDIF
			
		BREAK
	ENDSWITCH
	
	
	//- vectors -//
	//- floats -//
	//- ints -//
	//-- structs : MAG_DEMO_FBI2_ENTITIES_STRUCT --//
	g_sMagDemoFBI2Entities.bCreated = FALSE
	
//	#IF IS_DEBUG_BUILD
//	g_bDrawLiteralSceneString = FALSE
//	#ENDIF
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_M_Fbi2()
	
	//- request interior models -//
	//- wait for assets to load -//
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iRow = 0
		#ENDIF
		
		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
			IF (selected_player_scene_m_fbi2_scene <> PR_SCENE_F_MD_FRANKLIN2)
			
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str  = ("g_pScene_buddy doesnt exist")
				DrawLiteralSceneString(str, iRow+1+iCONSTANTSfortaunting, HUD_COLOUR_RED)
				iRow++
				#ENDIF
				
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		
		//- request models - peds -//
		IF (eFbi2_trevor_pedID <> NO_CHARACTER)
			REQUEST_PLAYER_PED_MODEL(eFbi2_trevor_pedID)
			IF NOT HAS_PLAYER_PED_MODEL_LOADED(eFbi2_trevor_pedID)
				REQUEST_PLAYER_PED_MODEL(eFbi2_trevor_pedID)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_Franklin_pedID <> NO_CHARACTER)
			REQUEST_PLAYER_PED_MODEL(eFbi2_Franklin_pedID)
			IF NOT HAS_PLAYER_PED_MODEL_LOADED(eFbi2_Franklin_pedID)
				REQUEST_PLAYER_PED_MODEL(eFbi2_Franklin_pedID)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_fbiAgent_model <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(eFbi2_fbiAgent_model)
			IF NOT HAS_MODEL_LOADED(eFbi2_fbiAgent_model)
				REQUEST_MODEL(eFbi2_fbiAgent_model)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		
		//- request models - vehicles -//
		IF (eFbi2_bike_vehID <> NO_CHARACTER)
			REQUEST_PLAYER_VEH_MODEL(eFbi2_bike_vehID, VEHICLE_TYPE_BIKE)
			IF NOT HAS_PLAYER_VEH_MODEL_LOADED(eFbi2_bike_vehID, VEHICLE_TYPE_BIKE)
				REQUEST_PLAYER_VEH_MODEL(eFbi2_bike_vehID, VEHICLE_TYPE_BIKE)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_heli_model <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(eFbi2_heli_model)
			IF NOT HAS_MODEL_LOADED(eFbi2_heli_model)
				REQUEST_MODEL(eFbi2_heli_model)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_truck01_model <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(eFbi2_truck01_model)
			IF NOT HAS_MODEL_LOADED(eFbi2_truck01_model)
				REQUEST_MODEL(eFbi2_truck01_model)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_truck02_model <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(eFbi2_truck02_model)
			IF NOT HAS_MODEL_LOADED(eFbi2_truck02_model)
				REQUEST_MODEL(eFbi2_truck02_model)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		IF (eFbi2_fbiVeh_model <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(eFbi2_fbiVeh_model)
			IF NOT HAS_MODEL_LOADED(eFbi2_fbiVeh_model)
				REQUEST_MODEL(eFbi2_fbiVeh_model)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
			
		//- request models - objects -//
		//- request models - weapons -//
		//- request anims and ptfx --//
		IF NOT IS_STRING_NULL_OR_EMPTY(sSyncSceneAnimDict)
			REQUEST_ANIM_DICT(sSyncSceneAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sSyncSceneAnimDict)
				REQUEST_ANIM_DICT(sSyncSceneAnimDict)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
			
		//- request vehicle recordings -//
		IF  (iRecordingNum >= 0)
		AND NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			REQUEST_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum, tRecordingName)
				
				#IF IS_DEBUG_BUILD
				PRINTSTRING("request veh rec \"")
				PRINTSTRING(tRecordingName)
				PRINTSTRING("\" ")
				PRINTINT(iRecordingNum)
				PRINTNL()
				#ENDIF
				
				REQUEST_VEHICLE_RECORDING(iRecordingNum, tRecordingName)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	INT j
	//- create any script vehicles -//
	IF (eFbi2_bike_vehID <> NO_CHARACTER)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.bikeVehicle)
			CREATE_PLAYER_VEHICLE(g_sMagDemoFBI2Entities.bikeVehicle, eFbi2_bike_vehID,
					vFbi2_bike_coord, fFbi2_bike_heading, TRUE, VEHICLE_TYPE_BIKE)
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sMagDemoFBI2Entities.bikeVehicle)
			SET_ENTITY_INVINCIBLE(g_sMagDemoFBI2Entities.bikeVehicle, TRUE)
		ENDIF
	ENDIF
	IF (eFbi2_heli_model <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.heliVehicle)
			g_sMagDemoFBI2Entities.heliVehicle = CREATE_VEHICLE(eFbi2_heli_model,
					vFbi2_heli_coord, fFbi2_heli_heading)
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sMagDemoFBI2Entities.heliVehicle)
			SET_ENTITY_INVINCIBLE(g_sMagDemoFBI2Entities.heliVehicle, TRUE)
			
			SET_VEHICLE_COLOUR_COMBINATION(g_sMagDemoFBI2Entities.heliVehicle, 1)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sMagDemoFBI2Entities.heliVehicle, TRUE)
		ENDIF
	ENDIF
	IF (eFbi2_truck01_model <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.truck01Vehicle)
			g_sMagDemoFBI2Entities.truck01Vehicle = CREATE_VEHICLE(eFbi2_truck01_model,
					vFbi2_truck01_coord, fFbi2_truck01_heading)
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sMagDemoFBI2Entities.truck01Vehicle)
			SET_ENTITY_INVINCIBLE(g_sMagDemoFBI2Entities.truck01Vehicle, TRUE)
			FOR j=1 TO 8
				SET_VEHICLE_EXTRA(g_sMagDemoFBI2Entities.truck01Vehicle, j, TRUE)
			ENDFOR		
			SET_VEHICLE_EXTRA(g_sMagDemoFBI2Entities.truck01Vehicle, 1, FALSE)
			SET_VEHICLE_EXTRA(g_sMagDemoFBI2Entities.truck01Vehicle, 3, FALSE)

			SET_VEHICLE_DIRT_LEVEL(g_sMagDemoFBI2Entities.truck01Vehicle, fFbi2_truck01_dirt)
		ENDIF
	ENDIF
	IF (eFbi2_truck02_model <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.truck02Vehicle)
			g_sMagDemoFBI2Entities.truck02Vehicle = CREATE_VEHICLE(eFbi2_truck02_model,
					vFbi2_truck02_coord, fFbi2_truck02_heading)
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sMagDemoFBI2Entities.truck02Vehicle)
			SET_ENTITY_INVINCIBLE(g_sMagDemoFBI2Entities.truck02Vehicle, TRUE)
			FOR j=1 TO 8
				SET_VEHICLE_EXTRA(g_sMagDemoFBI2Entities.truck02Vehicle, j, TRUE)
			ENDFOR	
			SET_VEHICLE_EXTRA(g_sMagDemoFBI2Entities.truck02Vehicle, 3, FALSE)
			
			SET_VEHICLE_DIRT_LEVEL(g_sMagDemoFBI2Entities.truck02Vehicle, fFbi2_truck02_dirt)
		ENDIF
	ENDIF
	IF (eFbi2_fbiVeh_model <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.fibVehicle)
			g_sMagDemoFBI2Entities.fibVehicle = CREATE_VEHICLE(eFbi2_fbiVeh_model,
					vFbi2_fbiVeh_coord, fFbi2_fbiVeh_heading)
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sMagDemoFBI2Entities.fibVehicle)
			SET_ENTITY_INVINCIBLE(g_sMagDemoFBI2Entities.fibVehicle, TRUE)
			
			SET_VEHICLE_DIRT_LEVEL(g_sMagDemoFBI2Entities.fibVehicle, fFbi2_fbiVeh_dirt)
		ENDIF
	ENDIF
	
	//- create any script peds -//
	IF (eFbi2_fbiAgent_model <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.fibPed)
			g_sMagDemoFBI2Entities.fibPed = CREATE_PED(PEDTYPE_MISSION, eFbi2_fbiAgent_model,
					vFbi2_fbiAgent_coord, fFbi2_fbiAgent_heading)
		//	SET_PED_COMPONENT_VARIATION(g_sMagDemoFBI2Entities.fibPed, PED_COMP_HEAD, 1, 0)
		//	SET_PED_COMPONENT_VARIATION(g_sMagDemoFBI2Entities.fibPed, PED_COMP_TORSO, 0, 2)
		//	SET_PED_COMPONENT_VARIATION(g_sMagDemoFBI2Entities.fibPed, PED_COMP_LEG, 0, 2)
		ENDIF
	ENDIF
	
	//- create the peds for hot-swap -//
	IF (eFbi2_trevor_pedID <> NO_CHARACTER)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.TrevorPed)
			CREATE_PLAYER_PED_ON_FOOT(g_sMagDemoFBI2Entities.TrevorPed, eFbi2_trevor_pedID,
					vFbi2_trevor_coord, fFbi2_trevor_heading)
			SET_PED_COMP_ITEM_CURRENT_SP(g_sMagDemoFBI2Entities.TrevorPed, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
			SET_PED_COMP_ITEM_CURRENT_SP(g_sMagDemoFBI2Entities.TrevorPed, COMP_TYPE_LEGS, LEGS_P2_SWEAT_PANTS, FALSE)
			SET_PED_COMPONENT_VARIATION(g_sMagDemoFBI2Entities.TrevorPed, PED_COMP_FEET, 10, 0)
		ENDIF
	ENDIF
	IF (eFbi2_Franklin_pedID <> NO_CHARACTER)
		IF NOT DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.FranklinPed)
			CREATE_PLAYER_PED_ON_FOOT(g_sMagDemoFBI2Entities.FranklinPed, eFbi2_Franklin_pedID,
					vFbi2_bike_coord, fFbi2_heli_heading)
			SET_PED_COMP_ITEM_CURRENT_SP(g_sMagDemoFBI2Entities.FranklinPed, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
		ENDIF
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sSyncSceneAnimDict)
		iSyncSceneA = CREATE_SYNCHRONIZED_SCENE(vSyncScenePos, <<0,0,0>>)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneA, TRUE)
		TASK_SYNCHRONIZED_SCENE(g_sMagDemoFBI2Entities.fibPed,	iSyncSceneA,
				sSyncSceneAnimDict, sSyncSceneFIBClip,
				NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		TASK_SYNCHRONIZED_SCENE(g_sMagDemoFBI2Entities.trevorPed, iSyncSceneA,
				sSyncSceneAnimDict, sSyncSceneTrevClip,
				NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		
		iSyncSceneB = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncSceneB, g_sMagDemoFBI2Entities.bikeVehicle,
				GET_ENTITY_BONE_INDEX_BY_NAME(g_sMagDemoFBI2Entities.bikeVehicle, "seat_f"))
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneB, TRUE)	
		TASK_SYNCHRONIZED_SCENE(g_sMagDemoFBI2Entities.franklinPed, iSyncSceneB,
				sSyncSceneAnimDict, sSyncSceneFranClip,
				NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
	ENDIF
	
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
//	SET_ROADS_IN_AREA(vRoadsOffMin, vRoadsOffMax, FALSE)
	
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
		SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, g_pScene_buddy, "Dave")
	ENDIF
	
	g_sMagDemoFBI2Entities.bCreated = TRUE
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_M_Fbi2_Stage(PS_M_FBI2_STAGE_ENUM this_player_scene_m_fbi2_stage)
	SWITCH this_player_scene_m_fbi2_stage
		CASE PS_M_FBI2_switchInProgress
			RETURN "PS_M_FBI2_switchInProgress"
		BREAK
		CASE PS_M_FBI2_STAGE_TWO
			RETURN "PS_M_FBI2_STAGE_TWO"
		BREAK
		CASE PS_M_FBI2_STAGE_THREE
			RETURN "PS_M_FBI2_STAGE_THREE"
		BREAK
		
		CASE FINISHED_PLAYER_SCENE_M_FBI2
			RETURN "FINISHED_PLAYER_SCENE_M_FBI2"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_M_Fbi2_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_M_Fbi2_widget()
	INT iWidget
	
	player_scene_m_fbi2_widget = START_WIDGET_GROUP("player_scene_m_fbi2.sc - Player scene M_FBI2")
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_M_FBI2_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_M_Fbi2_Stage(INT_TO_ENUM(PS_M_FBI2_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_m_fbi2_stage", iCurrent_player_scene_m_fbi2_stage)
		
		START_WIDGET_GROUP("g_sMagDemoFBI2Entities")
			START_WIDGET_GROUP("heliVehicle")
				ADD_WIDGET_VECTOR_SLIDER("vFbi2_heli_coord", vFbi2_heli_coord, -5000.0, 5000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fFbi2_heli_heading", fFbi2_heli_heading, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("bikeVehicle")
				ADD_WIDGET_VECTOR_SLIDER("vFbi2_bike_coord", vFbi2_bike_coord, -5000.0, 5000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fFbi2_bike_heading", fFbi2_bike_heading, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("TrevorPed")
				ADD_WIDGET_VECTOR_SLIDER("vFbi2_trevor_coord", vFbi2_trevor_coord, -5000.0, 5000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fFbi2_trevor_heading", fFbi2_trevor_heading, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("FranklinPed")
//				ADD_WIDGET_VECTOR_SLIDER("vFbi2_Franklin_coord", vFbi2_Franklin_coord, -5000.0, 5000.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fFbi2_Franklin_heading", fFbi2_Franklin_heading, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("fibPed")
				ADD_WIDGET_VECTOR_SLIDER("vFbi2_fbiAgent_coord", vFbi2_fbiAgent_coord, -5000.0, 5000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fFbi2_fbiAgent_heading", fFbi2_fbiAgent_heading, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("bRecord_vehs", bRecord_vehs)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_M_Fbi2_Widget()
	iCurrent_player_scene_m_fbi2_stage = ENUM_TO_INT(current_player_scene_m_fbi2_stage)
	
	IF bRecord_vehs
		
		INT iRecording = iRecordingNum+1
		
		WHILE bRecord_vehs
			VEHICLE_INDEX record_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			
			IF NOT IS_ENTITY_DEAD(record_veh)
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), record_veh)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(record_veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(record_veh)
						
						SET_ENTITY_COORDS(record_veh, vPlayer_scene_m_fbi2_coord)
						SET_ENTITY_HEADING(record_veh, fPlayer_scene_m_fbi2_head)
					ENDIF
					
//					IF NOT IS_PED_INJURED(t_chasecar_ped)
//						SET_ENTITY_COORDS(t_chasecar_ped, <<0,0,0>>)
//					ENDIF
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), record_veh, VS_DRIVER)
				ELSE
					//
					
					IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(record_veh)
						DrawLiteralSceneString("press J to record", 0, HUD_COLOUR_BLUELIGHT)
						DRAW_DEBUG_SPHERE(vPlayer_scene_m_fbi2_trigger, 1.5, 128, 128, 255, 128)
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
							START_RECORDING_VEHICLE(record_veh, iRecording, tRecordingName, TRUE)
							
							PRINTSTRING("START_RECORDING_VEHICLE(record_veh, ")
							PRINTINT(iRecording)
							PRINTSTRING(", \"")
							PRINTSTRING(tRecordingName)
							PRINTSTRING("\")")
							PRINTNL()
							
						ENDIF
					ELSE
						DrawLiteralSceneStringInt("RECORDING ", iRecording, 0, HUD_COLOUR_RED)
						DRAW_DEBUG_SPHERE(vPlayer_scene_m_fbi2_trigger, 1.0, 255, 0, 0, 128)
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
							STOP_RECORDING_VEHICLE(record_veh)
							
							PRINTSTRING("STOP_RECORDING_VEHICLE(record_veh, ")
							PRINTINT(iRecording)
							PRINTSTRING(", \"")
							PRINTSTRING(tRecordingName)
							PRINTSTRING("\")")
							PRINTNL()
							
							iRecording++
							
							SET_ENTITY_COORDS(record_veh, vPlayer_scene_m_fbi2_coord)
							SET_ENTITY_HEADING(record_veh, fPlayer_scene_m_fbi2_head)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			WAIT(0)
		ENDWHILE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_M_Fbi2_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_m_fbi2_stage = FINISHED_PLAYER_SCENE_M_FBI2
	ENDIF
	
ENDPROC

PROC DrawLiteralSceneVeh(VEHICLE_INDEX record_veh, INT iColumn, FLOAT fAlphaMult = 1.0)
	
	
	TEXT_LABEL_63 tStr = ""
	HUD_COLOURS eColour = HUD_COLOUR_BLUELIGHT
	
	IF NOT IS_ENTITY_DEAD(record_veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(record_veh)
			tStr  = "veh PLAYBACK "
			tStr += GET_STRING_FROM_FLOAT(GET_TIME_POSITION_IN_RECORDING(record_veh) / 1000.0)
			eColour = HUD_COLOUR_BLUELIGHT
			
			
//			IF g_bDrawLiteralSceneString
//				ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(record_veh, RDM_WHOLELINE)
//			ELSE
//				ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(record_veh, RDM_NONE)
//			ENDIF
		ELSE
			tStr = "veh no playback"
			eColour = HUD_COLOUR_GREENLIGHT
		ENDIF
	ELSE
		tStr = "veh DEAD"
		eColour = HUD_COLOUR_RED
	ENDIF
	
	DrawLiteralSceneString(tStr, iColumn, eColour, fAlphaMult)
	
ENDPROC

#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_M_FBI2_switchInProgress()
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ELSE
		PRINTSTRING("IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")PRINTNL()
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_M_FBI2_STAGE_TWO()
	
	
	#IF IS_DEBUG_BUILD
	INT iRow = 1
	VEHICLE_INDEX record_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	DrawLiteralSceneVeh(record_veh, iDrawLiteralSceneRow+iRow)
	iRow++
	#ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		PRINTSTRING("IS_PLAYER_SWITCH_IN_PROGRESS()")PRINTNL()
		
//		//removed for #835753
//		TEXT_LABEL tSceneSpeechBlock	= "FBI2AUD"		//"PRSAUD"
//		TEXT_LABEL tSceneSpeechLabel	= "FBI2_CHAT"	//"PSM_FBI2"
//		
//		IF CREATE_CONVERSATION(MyLocalPedStruct, tSceneSpeechBlock, tSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
//			RETURN TRUE
//		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_M_FBI2_STAGE_THREE()
	
	VECTOR vPlayerPedCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	CONST_FLOAT fMAX_DISTANCE_FROM_FBI2_TRIGGER		500.0	//200.0	//100.0
	FLOAT fDistanceFromFbi2Trigger = GET_DISTANCE_BETWEEN_COORDS(vPlayerPedCoord, vPlayer_scene_m_fbi2_trigger)
	
	VEHICLE_INDEX player_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	
	#IF IS_DEBUG_BUILD
	INT iRow = 1
	DrawLiteralSceneVeh(player_veh, iDrawLiteralSceneRow+iRow)
	iRow++
	
	TEXT_LABEL_63 tStr = "distance: "		//186
	tStr += ROUND(fDistanceFromFbi2Trigger)
	tStr += "m"
	DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_BLUELIGHT)
	iRow++
	#ENDIF
	
	//slow down cars while the player is playing a recording
	IF NOT IS_ENTITY_DEAD(player_veh)
		
		CONST_FLOAT fDONT_STOP_TIME_POS		13000.0
		CONST_FLOAT fDONT_STOP_TIME_DIST	8.0
		
		CONST_FLOAT fDISTANCE_FOR_TRAF_GRAB	30.0
		
		VECTOR vDontStopCoord = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum, fDONT_STOP_TIME_POS, tRecordingName)
		
		#IF IS_DEBUG_BUILD
		DrawDebugSceneSphere(vDontStopCoord, fDISTANCE_FOR_TRAF_GRAB, HUD_COLOUR_BLUELIGHT, 0.1)
		DrawDebugSceneSphere(vDontStopCoord, fDONT_STOP_TIME_DIST, HUD_COLOUR_REDLIGHT, 0.1)
		#ENDIF
		VEHICLE_INDEX vRandomTrafVeh = GET_RANDOM_VEHICLE_IN_SPHERE(vDontStopCoord, fDISTANCE_FOR_TRAF_GRAB, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		IF (NOT IS_ENTITY_DEAD(vRandomTrafVeh))
		AND (vRandomTrafVeh <> player_veh)
			PRINTSTRING("vRandomTrafVeh NOT dead, ")
			#IF IS_DEBUG_BUILD
			PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vRandomTrafVeh)))
			#ENDIF
			
			PRINTSTRING(", ")
			PRINTFLOAT(GET_ENTITY_SPEED(vRandomTrafVeh))
			PRINTNL()
			
			IF GET_ENTITY_SPEED(vRandomTrafVeh) > 5.0
				PED_INDEX pRandomTrafPed = GET_PED_IN_VEHICLE_SEAT(vRandomTrafVeh, VS_DRIVER)
				IF (NOT IS_PED_INJURED(pRandomTrafPed))
					
					IF VDIST2(GET_ENTITY_COORDS(vRandomTrafVeh), vDontStopCoord) > (fDONT_STOP_TIME_DIST*fDONT_STOP_TIME_DIST)
					
						TASK_VEHICLE_TEMP_ACTION(pRandomTrafPed, vRandomTrafVeh, TEMPACT_BRAKE, 500)
						
						PRINTSTRING("	vRandomTrafVeh hits the brakes!!!")
						PRINTNL()
					ELSE
						PRINTSTRING("	vRandomTrafVeh DONT STOP here!!!")
						PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			PRINTSTRING("vRandomTrafVeh dead / players veh")
			PRINTNL()
		ENDIF
	ENDIF
	
	IF fDistanceFromFbi2Trigger > fMAX_DISTANCE_FROM_FBI2_TRIGGER
		PRINTSTRING("player is ")
		PRINTFLOAT(fDistanceFromFbi2Trigger)
		PRINTSTRING("m ( > ")
		PRINTFLOAT(fMAX_DISTANCE_FROM_FBI2_TRIGGER)
		PRINTSTRING("m) from the mission trigger")
		PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.bikeVehicle)
		#IF IS_DEBUG_BUILD
		tStr  = "bikeVehicle"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "bikeVehicle"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.heliVehicle)
		#IF IS_DEBUG_BUILD
		tStr  = "heliVehicle"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "heliVehicle"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.truck01Vehicle)
		#IF IS_DEBUG_BUILD
		tStr  = "truck01Vehicle"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "truck01Vehicle"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.truck02Vehicle)
		#IF IS_DEBUG_BUILD
		tStr  = "truck02Vehicle"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "truck02Vehicle"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.fibVehicle)
		#IF IS_DEBUG_BUILD
		tStr  = "fibVehicle"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "fibVehicle"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.fibPed)
		#IF IS_DEBUG_BUILD
		tStr  = "fibPed"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "fibPed"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.TrevorPed)
		#IF IS_DEBUG_BUILD
		tStr  = "TrevorPed"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "TrevorPed"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sMagDemoFBI2Entities.FranklinPed)
		#IF IS_DEBUG_BUILD
		tStr  = "FranklinPed"
		tStr += " exists"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_GREENLIGHT)
		iRow++
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		tStr  = "FranklinPed"
		tStr += " non-existent"
		DrawLiteralSceneString(tStr, iDrawLiteralSceneRow+iRow, HUD_COLOUR_REDLIGHT, 0.25)
		iRow++
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_M_Fbi2_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_M_Fbi2_Variables()
	Setup_Player_Scene_M_Fbi2()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_M_Fbi2_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_M_Fbi2_in_progress
	AND (TRUE OR ProgressScene(BIT_MICHAEL|BIT_FRANKLIN, NULL))
		WAIT(0)
		
		//SET_VEHICLE_DENSITY_MULTIPLIER(0.3)
		
		#IF IS_DEBUG_BUILD
		IF Is_Player_Timetable_Scene_In_Progress()
			iDrawLiteralSceneRow = 5
		ELSE
			iDrawLiteralSceneRow = 0
		ENDIF
		#ENDIF
		
		// setup dave to stay in car
		IF NOT IS_PED_INJURED(g_pScene_buddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_pScene_buddy, TRUE)
			SET_PED_KEEP_TASK(g_pScene_buddy, TRUE)
		ENDIF
			
		SWITCH current_player_scene_m_fbi2_stage
			CASE PS_M_FBI2_switchInProgress
				IF Do_PS_M_FBI2_switchInProgress()
					current_player_scene_m_fbi2_stage = PS_M_FBI2_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_M_FBI2_STAGE_TWO
				IF Do_PS_M_FBI2_STAGE_TWO()
					current_player_scene_m_fbi2_stage = PS_M_FBI2_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_M_FBI2_STAGE_THREE
				IF Do_PS_M_FBI2_STAGE_THREE()
					current_player_scene_m_fbi2_stage = FINISHED_PLAYER_SCENE_M_FBI2
				ENDIF
			BREAK
			
			CASE FINISHED_PLAYER_SCENE_M_FBI2
				Player_Scene_M_Fbi2_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_M_Fbi2_Widget()
		Player_Scene_M_Fbi2_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_m_fbi2_scene,
				 iDrawLiteralSceneRow+5, HUD_COLOUR_BLUELIGHT)
		STRING sStage = Get_String_From_Player_Scene_m_fbi2_Stage(current_player_scene_m_fbi2_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_M_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				iDrawLiteralSceneRow+6, HUD_COLOUR_BLUELIGHT)
		#ENDIF
	ENDWHILE
	
	Player_Scene_M_Fbi2_Cleanup()
ENDSCRIPT
