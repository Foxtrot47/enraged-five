// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 	(A1)  - Player scene FT_FRANKLIN1 file for use – player_scene_ft_franklin1.sc	 ___
// ___ 																					 ___
// _________________________________________________________________________________________



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "RC_threat_public.sch" 
USING "player_ped_scenes.sch"
USING "cheat_controller_public.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
CONST_INT				iNUM_OF_PS_FT_FRANKLIN1_VEHICLES								1

#IF IS_DEBUG_BUILD
CONST_INT iDrawLiteralScene_row	5
#ENDIF

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_FT_FRANKLIN1_STAGE_ENUM
	PS_FT_FRANKLIN1_switchInProgress,
	PS_FT_FRANKLIN1_STAGE_TWO_POLICE,
	
	PS_FT_FRANKLIN1_STAGE_THREE_POLICE,
	PS_FT_FRANKLIN1_STAGE_FOUR_POLICE,
	
	PS_FT_FRANKLIN1_STAGE_TWO_TRAIN,
	PS_FT_FRANKLIN1_STAGE_THREE_TRAIN,
	
	FINISHED_PLAYER_SCENE_FT_FRANKLIN1
ENDENUM
PS_FT_FRANKLIN1_STAGE_ENUM	current_player_scene_ft_franklin1_stage						= PS_FT_FRANKLIN1_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_ft_franklin1_scene					= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL				bPlayer_Scene_FT_Franklin1_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX			ft_franklin1_ped[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]
MODEL_NAMES			ePolice_ped_model

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX		ft_franklin1_veh[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]
MODEL_NAMES			police_veh_model[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]
VECTOR 				vTPolice_veh_offset[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]
FLOAT 				fTPolice_veh_offset[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]

FLOAT				fTrainCruiseSpeed = -1.0
INT					iPoliceWantedCheckTime = -1

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_ft_franklin1_coord
FLOAT				fPlayer_scene_ft_franklin1_head

VECTOR				vTPolice_goto_offset[iNUM_OF_PS_FT_FRANKLIN1_VEHICLES]

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL tVoiceID, tSceneSpeechLabel
TEXT_LABEL tSoundName, tSetName
INT iSoundGameTime = -1

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//
COVERPOINT_INDEX	franklin1_coverPoint
VECTOR				vCoverOffset			//coverpoint position
FLOAT				fCoverHead				//coverpoint heading
COVERPOINT_USAGE	coverpointUsage			//coverpoint usage
COVERPOINT_HEIGHT	coverpointHeight		//coverpoint height
COVERPOINT_ARC		coverpointArc			//coverpoint arc

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_scene_ft_franklin1_widget
INT				iCurrent_player_scene_ft_franklin1_stage
BOOL			bMove_vehs, bTask_vehs, bSave_vehs
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_FT_Franklin1_Cleanup()
	INT iVeh
	
	//- mark peds as no longer needed -//
	REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
		SET_PED_AS_NO_LONGER_NEEDED(ft_franklin1_ped[iVeh])
	ENDREPEAT
	
	//- mark vehicle as no longer needed -//
	REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
		SET_VEHICLE_AS_NO_LONGER_NEEDED(ft_franklin1_veh[iVeh])
	ENDREPEAT
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	SET_MODEL_AS_NO_LONGER_NEEDED(ePolice_ped_model)
	REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(police_veh_model[iVeh])
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	REMOVE_COVER_POINT(franklin1_coverPoint)
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	IF (police_veh_model[0] = FREIGHT)
		SET_RANDOM_TRAINS(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_ft_franklin1_widget)
		DELETE_WIDGET_GROUP(player_scene_ft_franklin1_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_FT_Franklin1_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_FT_Franklin1_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_FT_Franklin1_Variables()
	selected_player_scene_ft_franklin1_scene	= g_eRecentlySelectedScene	//PR_SCENE_FT_FRANKLIN1
	
	TEXT_LABEL_31 tPlayer_scene_ft_franklin1_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_ft_franklin1_scene, vPlayer_scene_ft_franklin1_coord, fPlayer_scene_ft_franklin1_head, tPlayer_scene_ft_franklin1_room)
	
	vCoverOffset 		= <<0,0,0>>
	fCoverHead 			= 0.0
	
	SWITCH selected_player_scene_ft_franklin1_scene
		CASE PR_SCENE_FTa_FRANKLIN1a	FALLTHRU
//		CASE PR_SCENE_FTa_FRANKLIN1b	FALLTHRU
		CASE PR_SCENE_FTa_FRANKLIN1c	FALLTHRU
		CASE PR_SCENE_FTa_FRANKLIN1d	FALLTHRU
		CASE PR_SCENE_FTa_FRANKLIN1e
			
			SWITCH selected_player_scene_ft_franklin1_scene
				CASE PR_SCENE_FTa_FRANKLIN1a
					vTPolice_veh_offset[0] = <<775.4436, -2967.5979, 4.8237>>	- (vPlayer_scene_ft_franklin1_coord)
					fTPolice_veh_offset[0] = 174.1408							- 90.0000
					
					vTPolice_goto_offset[0] = <<776.4791, -2985.1741, 4.8169>>	- (vPlayer_scene_ft_franklin1_coord)
					
					coverpointUsage 	= COVUSE_WALLTORIGHT
					coverpointHeight 	= COVHEIGHT_HIGH
					coverpointArc 		= COVARC_180
				BREAK
//				CASE PR_SCENE_FTa_FRANKLIN1b
//					vTPolice_veh_offset[0] =  <<762.1586, -3047.9634, 5.0379>>	- (vPlayer_scene_ft_franklin1_coord)
//					fTPolice_veh_offset[0] = 5.7183								- 114.8398
//					
//					vTPolice_goto_offset[0] = <<762.6600, -3021.0066, 4.8269>>	- (vPlayer_scene_ft_franklin1_coord)
//					
//					coverpointUsage 	= COVUSE_WALLTORIGHT
//					coverpointHeight 	= COVHEIGHT_HIGH
//					coverpointArc 		= COVARC_180
//				BREAK
				CASE PR_SCENE_FTa_FRANKLIN1c
					vTPolice_veh_offset[0] = <<30.4341, -20.6057, 0.0429>>
					fTPolice_veh_offset[0] = 2.1600
					
					vTPolice_goto_offset[0] = <<29.4510, 37.9000, 0.9107>>
					
//					vCoverOffset 		= <<708.7761, -2916.4907, 5.0997>>		- vPlayer_scene_ft_franklin1_coord
//					fCoverHead 			= 0.0
//					coverpointUsage 	= COVUSE_WALLTOLEFT
//					coverpointHeight 	= COVHEIGHT_LOW
//					coverpointArc 		= COVARC_180
				
					vCoverOffset 		= <<0,0,0>>
					fCoverHead 			= 0.0
					coverpointUsage 	= INT_TO_ENUM(COVERPOINT_USAGE, -1)
					coverpointHeight 	= INT_TO_ENUM(COVERPOINT_HEIGHT, -1)
					coverpointArc 		= INT_TO_ENUM(COVERPOINT_ARC, -1)
					
				BREAK
				CASE PR_SCENE_FTa_FRANKLIN1d
					vTPolice_veh_offset[0] = <<28.5848, 1.3633, 0.1963>>
					fTPolice_veh_offset[0] = 25.2910
					
					vTPolice_goto_offset[0] = <<30.0000, 30.0000, 1.1960>>
				
					vCoverOffset 		= <<643.5049, -2917.2471, 5.1339>>		- vPlayer_scene_ft_franklin1_coord
					fCoverHead 			= 0.0
					coverpointUsage 	= COVUSE_WALLTORIGHT
					coverpointHeight 	= COVHEIGHT_HIGH
					coverpointArc 		= COVARC_180
				BREAK
				CASE PR_SCENE_FTa_FRANKLIN1e
					vTPolice_veh_offset[0] = <<571.7438, -2820.1094, 5.0585>>	- (vPlayer_scene_ft_franklin1_coord)
					fTPolice_veh_offset[0] = 233.5577							- 299.0519
					
					fTPolice_veh_offset[0] = -171.000
					
					vTPolice_goto_offset[0] = <<590.6221, -2859.8169, 5.0585>>	- (vPlayer_scene_ft_franklin1_coord)
					
					coverpointUsage 	= COVUSE_WALLTOLEFT
					coverpointHeight 	= COVHEIGHT_HIGH
					coverpointArc 		= COVARC_180
				BREAK
				DEFAULT
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str
					str = "invalid selected_player_scene_ft_franklin1_scene: "
					str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_ft_franklin1_scene)
					
					PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(str)PRINTNL()
					SCRIPT_ASSERT(str)
					#ENDIF
				BREAK
			ENDSWITCH
			
			//- vectors -//
			//- floats -//
			//- ints -//
			ePolice_ped_model = S_M_Y_COP_01
			police_veh_model[0] = POLICE
			
		BREAK
		CASE PR_SCENE_T_CN_WAKETRAIN
			//- vectors -//
			vTPolice_veh_offset[0] = <<1466.7709, 6400.6021, 33.8759>> - vPlayer_scene_ft_franklin1_coord
			
			vTPolice_goto_offset[0] = <<0,0,0>>
			
			//- floats -//
			fTPolice_veh_offset[0] = 0
			
			fTrainCruiseSpeed = 20.0
			
			//- ints -//
			ePolice_ped_model = S_M_M_LSMetro_01	//This was a police officer model, why one would driver a train I don't kno
			
			police_veh_model[0] = FREIGHT
			
			tVoiceID = ""
			tSceneSpeechLabel = ""
			
			tSoundName = "Warning_Once"
			tSetName = "TRAIN_HORN"
			
			coverpointUsage 	= INT_TO_ENUM(COVERPOINT_USAGE, -1)
			coverpointHeight 	= INT_TO_ENUM(COVERPOINT_HEIGHT, -1)
			coverpointArc 		= INT_TO_ENUM(COVERPOINT_ARC, -1)
		BREAK
//		CASE PR_SCENE_F_S_FBI1end
//			//- vectors -//
//			vTPolice_veh_offset[0] = <<1548.6085, -1905.7190, 79.9723>> - vPlayer_scene_ft_franklin1_coord
//			vTPolice_veh_offset[0] += 0.4
//			
//			vTPolice_goto_offset[0] = <<1602.1941, -1929.4343, 99.3424>> - vPlayer_scene_ft_franklin1_coord
//			
//			//- floats -//
//			fTPolice_veh_offset[0] = (59.1233-180) - fPlayer_scene_ft_franklin1_head
//			
//			fTrainCruiseSpeed = 15.0
//			
//			//- ints -//
//			ePolice_ped_model = GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
//			
//			police_veh_model[0] = GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
//			
//			tVoiceID = ""
//			tSceneSpeechLabel = ""
//			
//			coverpointUsage 	= INT_TO_ENUM(COVERPOINT_USAGE, -1)
//			coverpointHeight 	= INT_TO_ENUM(COVERPOINT_HEIGHT, -1)
//			coverpointArc 		= INT_TO_ENUM(COVERPOINT_ARC, -1)
//		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_ft_franklin1_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_ft_franklin1_scene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(str)PRINTNL()
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			vTPolice_veh_offset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			vTPolice_goto_offset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			//- floats -//
			fTPolice_veh_offset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			ePolice_ped_model = A_M_Y_MUSCLBEAC_01
			
			police_veh_model[0] = TAILGATER
		BREAK
	ENDSWITCH

	
	
					vCoverOffset 		= <<0,0,0>>
					fCoverHead 			= 0.0
					coverpointUsage 	= INT_TO_ENUM(COVERPOINT_USAGE, -1)
					coverpointHeight 	= INT_TO_ENUM(COVERPOINT_HEIGHT, -1)
					coverpointArc 		= INT_TO_ENUM(COVERPOINT_ARC, -1)
	//-- structs : PS_FT_FRANKLIN1_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_FT_Franklin1()
	INT iVeh
	
	//- request models - peds -//
	REQUEST_MODEL(ePolice_ped_model)
	
	//- request models - vehicles -//
	REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
		REQUEST_MODEL(police_veh_model[iVeh])
	ENDREPEAT
	
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iLoadingAssets = 0
		#ENDIF
		
		IF NOT HAS_MODEL_LOADED(ePolice_ped_model)
			REQUEST_MODEL(ePolice_ped_model)
			bAllAssetsLoaded = FALSE
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str = ("ePolice_ped_model")
			DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
			#ENDIF
		ENDIF
		REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
			IF NOT HAS_MODEL_LOADED(police_veh_model[iVeh])
				REQUEST_MODEL(police_veh_model[iVeh])
				bAllAssetsLoaded = FALSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("police_veh_model")
				DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
			ENDIF
			
			IF (police_veh_model[iVeh] = FREIGHT)
				REQUEST_MODEL(FREIGHTCAR)
				REQUEST_MODEL(FREIGHTGRAIN)
				REQUEST_MODEL(FREIGHTCONT1)
				
				IF NOT HAS_MODEL_LOADED(FREIGHTCAR)
					REQUEST_MODEL(FREIGHTCAR)
					bAllAssetsLoaded = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("FREIGHTCAR")
					DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
				ENDIF
				IF NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
					REQUEST_MODEL(FREIGHTGRAIN)
					bAllAssetsLoaded = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("FREIGHTGRAIN")
					DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
				ENDIF
				IF NOT HAS_MODEL_LOADED(FREIGHTCONT1)
					REQUEST_MODEL(FREIGHTCONT1)
					bAllAssetsLoaded = FALSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("FREIGHTCONT1")
					DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iVeh
		
		IF (police_veh_model[iVeh] <> FREIGHT)
		
		
			IF (police_veh_model[iVeh] <> GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
				ft_franklin1_veh[iVeh] = CREATE_VEHICLE(police_veh_model[iVeh],
						vPlayer_scene_ft_franklin1_coord+vTPolice_veh_offset[iVeh],
						fPlayer_scene_ft_franklin1_head+fTPolice_veh_offset[iVeh])
				SET_MODEL_AS_NO_LONGER_NEEDED(police_veh_model[iVeh])
				
				SET_VEHICLE_SIREN(ft_franklin1_veh[iVeh], TRUE)
			
			ELSE
			
				CREATE_PLAYER_VEHICLE(ft_franklin1_veh[iVeh], CHAR_MICHAEL,
						vPlayer_scene_ft_franklin1_coord+vTPolice_veh_offset[iVeh],
						fPlayer_scene_ft_franklin1_head+fTPolice_veh_offset[iVeh], TRUE, VEHICLE_TYPE_CAR)
			ENDIF
		ELSE
			SET_RANDOM_TRAINS(FALSE)
			DELETE_ALL_TRAINS()
			ft_franklin1_veh[iVeh] = CREATE_MISSION_TRAIN(0,
					vPlayer_scene_ft_franklin1_coord+vTPolice_veh_offset[iVeh],
					TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
			SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTGRAIN)
			SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
		ENDIF
		
		IF (ePolice_ped_model <> GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			ft_franklin1_ped[iVeh] = CREATE_PED_INSIDE_VEHICLE(ft_franklin1_veh[iVeh], PEDTYPE_COP, ePolice_ped_model, VS_DRIVER)
			SET_PED_RANDOM_COMPONENT_VARIATION(ft_franklin1_ped[iVeh])
			GIVE_WEAPON_TO_PED(ft_franklin1_ped[iVeh], WEAPONTYPE_PISTOL, 1500, TRUE)
		ELSE
			CREATE_PLAYER_PED_INSIDE_VEHICLE(ft_franklin1_ped[iVeh], CHAR_MICHAEL,
					ft_franklin1_veh[iVeh], VS_DRIVER, TRUE)
		ENDIF
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(ePolice_ped_model)
	
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	
	IF (coverpointUsage <> INT_TO_ENUM(COVERPOINT_USAGE, -1))
		franklin1_coverPoint = ADD_COVER_POINT(vPlayer_scene_ft_franklin1_coord+vCoverOffset,
				fPlayer_scene_ft_franklin1_head+fCoverHead,
				coverpointUsage, coverpointHeight, coverpointArc, TRUE)
	ENDIF
	
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_FT_Franklin1_Stage(PS_FT_FRANKLIN1_STAGE_ENUM this_player_scene_ft_franklin1_stage)
	SWITCH this_player_scene_ft_franklin1_stage
		CASE PS_FT_FRANKLIN1_switchInProgress
			RETURN "PS_FT_FRANKLIN1_switchInProgress"
		BREAK
		CASE PS_FT_FRANKLIN1_STAGE_TWO_POLICE
			RETURN "PS_FT_FRANKLIN1_STAGE_TWO_POLICE"
		BREAK
		
		CASE PS_FT_FRANKLIN1_STAGE_THREE_POLICE
			RETURN "PS_FT_FRANKLIN1_STAGE_THREE_POLICE"
		BREAK
		CASE PS_FT_FRANKLIN1_STAGE_FOUR_POLICE
			RETURN "PS_FT_FRANKLIN1_STAGE_FOUR_POLICE"
		BREAK
		
		CASE PS_FT_FRANKLIN1_STAGE_TWO_TRAIN
			RETURN "PS_FT_FRANKLIN1_STAGE_TWO_TRAIN"
		BREAK
		CASE PS_FT_FRANKLIN1_STAGE_THREE_TRAIN
			RETURN "PS_FT_FRANKLIN1_STAGE_THREE_TRAIN"
		BREAK
		
		CASE FINISHED_PLAYER_SCENE_FT_FRANKLIN1
			RETURN "FINISHED_PLAYER_SCENE_FT_FRANKLIN1"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_FT_Franklin1_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_FT_Franklin1_widget()
	INT iWidget
	TEXT_LABEL_31 sWidget
	
	player_scene_ft_franklin1_widget = START_WIDGET_GROUP("player_scene_ft_franklin1.sc - Player scene FT_FRANKLIN1")
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_FT_FRANKLIN1_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_FT_Franklin1_Stage(INT_TO_ENUM(PS_FT_FRANKLIN1_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_ft_franklin1_stage", iCurrent_player_scene_ft_franklin1_stage)
		
		
		REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iWidget
			sWidget = "ft_franklin1_veh "
			sWidget += iWidget
			START_WIDGET_GROUP(sWidget)
				ADD_WIDGET_VECTOR_SLIDER("vTPolice_veh_offset",
						vTPolice_veh_offset[iWidget],
						-VMAG(vTPolice_veh_offset[iWidget]) * 1.5,		//-333.260
						VMAG(vTPolice_veh_offset[iWidget]) * 1.5,
						0.1)
					
				ADD_WIDGET_FLOAT_SLIDER("fTPolice_veh_offset", fTPolice_veh_offset[iWidget], -180.0, 180.0, 1.0)
				
				IF fTrainCruiseSpeed >= 0
					ADD_WIDGET_FLOAT_SLIDER("fTrainCruiseSpeed", fTrainCruiseSpeed, 0.0, 50.0, 0.5)
				ENDIF
				
				ADD_WIDGET_STRING("vTPolice_goto_offset")
				ADD_WIDGET_VECTOR_SLIDER("vTPolice_goto_offset", vTPolice_goto_offset[iWidget], -100.0, 100.0, 0.1)
				
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
		
		IF NOT ARE_VECTORS_EQUAL(vCoverOffset, <<0,0,0>>)
			START_WIDGET_GROUP("vCoverOffset")
				ADD_WIDGET_VECTOR_SLIDER("vCoverOffset", vCoverOffset, -5, 5, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fCoverHead", fCoverHead, -180.0, 180.0, 1.0)
			STOP_WIDGET_GROUP()
		ENDIF
		
		ADD_WIDGET_BOOL("bMove_vehs", bMove_vehs)
		ADD_WIDGET_BOOL("bTask_vehs", bTask_vehs)
		ADD_WIDGET_BOOL("bSave_vehs", bSave_vehs)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_FT_Franklin1_Widget()
	iCurrent_player_scene_ft_franklin1_stage = ENUM_TO_INT(current_player_scene_ft_franklin1_stage)
	
	INT iMove
	IF bMove_vehs
		PED_VEH_DATA_STRUCT sData    //MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vCoordOffset = <<0,0,0>>
		FLOAT fHead = 0
		VECTOR vDriveOffset = <<0,0,0>>
		FLOAT fDriveSpeed = 0
		
		WHILE bMove_vehs
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX player_veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(player_veh)
					IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_MICHAEL, selected_player_scene_ft_franklin1_scene,
							sData, vCoordOffset, fHead,
							vDriveOffset, fDriveSpeed)
						SET_ENTITY_COORDS(player_veh, vPlayer_scene_ft_franklin1_coord+vCoordOffset)
						SET_ENTITY_HEADING(player_veh, fPlayer_scene_ft_franklin1_head+fHead)
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CLEAR_AREA(vPlayer_scene_ft_franklin1_coord, 5, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iMove
				IF NOT IS_ENTITY_DEAD(ft_franklin1_veh[iMove])
					current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_TWO_POLICE
					
					SET_ENTITY_COORDS(ft_franklin1_veh[iMove], vPlayer_scene_ft_franklin1_coord+vTPolice_veh_offset[iMove])
					SET_ENTITY_HEADING(ft_franklin1_veh[iMove], fPlayer_scene_ft_franklin1_head+fTPolice_veh_offset[iMove])
					
					IF NOT IS_PED_INJURED(ft_franklin1_ped[iMove])
						CLEAR_PED_TASKS(ft_franklin1_ped[iMove])
					ENDIF
					
					CLEAR_AREA(vPlayer_scene_ft_franklin1_coord+vTPolice_veh_offset[iMove], 5, TRUE)
				ENDIF
				
				DrawDebugSceneLineBetweenCoords(
						vTPolice_goto_offset[iMove]+vPlayer_scene_ft_franklin1_coord,
						vTPolice_veh_offset[iMove]+vPlayer_scene_ft_franklin1_coord,
						HUD_COLOUR_YELLOW)
				DrawDebugSceneSphere(vTPolice_goto_offset[iMove]+vPlayer_scene_ft_franklin1_coord, 1.0, HUD_COLOUR_YELLOW)
			
			ENDREPEAT
			
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF bTask_vehs
		INT iTask
		REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iTask
			IF NOT IS_ENTITY_DEAD(ft_franklin1_veh[iTask])
				IF NOT IS_PED_INJURED(ft_franklin1_ped[iTask])
					TASK_VEHICLE_MISSION_PED_TARGET(ft_franklin1_ped[iTask], ft_franklin1_veh[iTask], PLAYER_PED_ID(), MISSION_FOLLOW, 60.0, DRIVINGMODE_AVOIDCARS, 1, 1)
					SET_PED_KEEP_TASK(ft_franklin1_ped[iTask], TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		bTask_vehs = FALSE
	ENDIF
	IF bSave_vehs
		INT iSave
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_ft_franklin1_scene))SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("				vTPolice_veh_offset[")SAVE_INT_TO_DEBUG_FILE(iSave)SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_VECTOR_TO_DEBUG_FILE(vTPolice_veh_offset[iSave])SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("				fTPolice_veh_offset[")SAVE_INT_TO_DEBUG_FILE(iSave)SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_FLOAT_TO_DEBUG_FILE(fTPolice_veh_offset[iSave])SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("				")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			REPEAT iNUM_OF_PS_FT_FRANKLIN1_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("				vTPolice_goto_offset[")SAVE_INT_TO_DEBUG_FILE(iSave)SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_VECTOR_TO_DEBUG_FILE(vTPolice_goto_offset[iSave])SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("				")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				coverpointUsage 	= COVUSE_???")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				coverpointHeight 	= COVHEIGHT_???")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				coverpointArc 		= COVARC_???")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		
		bSave_vehs = FALSE
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_FT_Franklin1_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
		bSave_vehs = TRUE
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_FT_FRANKLIN1_switchInProgress()
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			SWITCH ePed
				CASE CHAR_FRANKLIN	tVoiceID = "FRANKLIN"	tSceneSpeechLabel = "PSF_FRA1" BREAK
				CASE CHAR_TREVOR	tVoiceID = "TREVOR"		tSceneSpeechLabel = "PST_FRA1" BREAK
				DEFAULT
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str
					str = "invalid GET_CURRENT_PLAYER_PED_ENUM: "
					str += GET_PLAYER_PED_STRING(GET_CURRENT_PLAYER_PED_ENUM())
					
					PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(str)PRINTNL()
					SCRIPT_ASSERT(str)
					#ENDIF
				BREAK
			ENDSWITCH
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, GET_CURRENT_PLAYER_PED_INT(), PLAYER_PED_ID(), tVoiceID)
			
			RETURN TRUE
		ENDIF
	ELSE
		//
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_FT_FRANKLIN1_STAGE_TWO_POLICE()
	

	IF IS_VEHICLE_DRIVEABLE(ft_franklin1_veh[0])
		IF NOT IS_PED_INJURED(ft_franklin1_ped[0])
			
			IF IS_ENTITY_ON_SCREEN(ft_franklin1_veh[0])
				BOOL bCREATE_CONVERSATION = FALSE
				IF IS_STRING_NULL_OR_EMPTY(tSceneSpeechLabel)
					bCREATE_CONVERSATION = TRUE
				ELSE
					IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
						bCREATE_CONVERSATION = TRUE
					ENDIF
				ENDIF
				
				IF bCREATE_CONVERSATION
					TASK_VEHICLE_DRIVE_TO_COORD(ft_franklin1_ped[0], ft_franklin1_veh[0],
							vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
							10.0, DRIVINGSTYLE_NORMAL, police_veh_model[0],
							DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 4.0, -1)
					
					RETURN TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenCoords(
					vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
					vTPolice_veh_offset[0]+vPlayer_scene_ft_franklin1_coord,
					HUD_COLOUR_YELLOW)
			DrawDebugSceneSphere(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, 1.0, HUD_COLOUR_YELLOW)
			#ENDIF
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				RETURN FALSE
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
	RETURN FALSE
ENDFUNC


FUNC BOOL Do_PS_FT_FRANKLIN1_STAGE_THREE_POLICE()
	IF IS_VEHICLE_DRIVEABLE(ft_franklin1_veh[0])
		IF NOT IS_PED_INJURED(ft_franklin1_ped[0])
			
			VECTOR ft_franklin1_ped0_coord = GET_ENTITY_COORDS(ft_franklin1_ped[0], FALSE)
			FLOAT ft_franklin1_ped0_dist2 = VDIST2(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, ft_franklin1_ped0_coord)
			
			IF ft_franklin1_ped0_dist2 < (4.5*4.5)
				TASK_VEHICLE_DRIVE_WANDER(ft_franklin1_ped[0], ft_franklin1_veh[0], 10.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				iPoliceWantedCheckTime = GET_GAME_TIMER()
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			HUD_COLOURS eHUD_COLOUR_YELLOW = HUD_COLOUR_YELLOW
			#ENDIF
			
			IF (GET_PED_TYPE(ft_franklin1_ped[0]) = PEDTYPE_COP)
				IF NOT IS_CHEAT_ACTIVE(CHEAT_TYPE_WANTED_LEVEL_DOWN)
					IF CAN_PED_SEE_PLAYER(ft_franklin1_ped[0], 135)
						#IF IS_DEBUG_BUILD
						eHUD_COLOUR_YELLOW = HUD_COLOUR_RED
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						eHUD_COLOUR_YELLOW = HUD_COLOUR_BLUE
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					eHUD_COLOUR_YELLOW = HUD_COLOUR_GREEN
					#ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenCoords(
					vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
					ft_franklin1_ped0_coord,
					eHUD_COLOUR_YELLOW)
			DrawDebugSceneSphere(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, 1.0, eHUD_COLOUR_YELLOW)
			DrawLiteralSceneStringFloat(
					"dist: ", SQRT(ft_franklin1_ped0_dist2),
					7-4+iDrawLiteralScene_row, eHUD_COLOUR_YELLOW)
			#ENDIF
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				RETURN FALSE
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_FT_FRANKLIN1_STAGE_FOUR_POLICE()
	IF IS_VEHICLE_DRIVEABLE(ft_franklin1_veh[0])
		IF NOT IS_PED_INJURED(ft_franklin1_ped[0])
			
			#IF IS_DEBUG_BUILD
			HUD_COLOURS eHUD_COLOUR_YELLOW = HUD_COLOUR_YELLOW
			#ENDIF
			
			IF (GET_PED_TYPE(ft_franklin1_ped[0]) = PEDTYPE_COP)
				IF NOT IS_CHEAT_ACTIVE(CHEAT_TYPE_WANTED_LEVEL_DOWN)
					IF CAN_PED_SEE_PLAYER(ft_franklin1_ped[0], 135)
						#IF IS_DEBUG_BUILD
						eHUD_COLOUR_YELLOW = HUD_COLOUR_RED
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						eHUD_COLOUR_YELLOW = HUD_COLOUR_BLUE
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					eHUD_COLOUR_YELLOW = HUD_COLOUR_GREEN
					#ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenCoords(
					GET_ENTITY_COORDS(PLAYER_PED_ID()),
					GET_ENTITY_COORDS(ft_franklin1_ped[0]),
					eHUD_COLOUR_YELLOW)
			#ENDIF
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				iPoliceWantedCheckTime = -1
				RETURN FALSE
			ENDIF
			
			IF (iPoliceWantedCheckTime < 0)
				iPoliceWantedCheckTime = GET_GAME_TIMER()
				RETURN FALSE
			ENDIF
			
			IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ft_franklin1_ped[0])) > (250.0*250.0))
				TASK_VEHICLE_DRIVE_WANDER(ft_franklin1_ped[0], ft_franklin1_veh[0], 10.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				SET_PED_KEEP_TASK(ft_franklin1_ped[0], TRUE)
				RETURN TRUE
			ENDIF
			
			IF (iPoliceWantedCheckTime+60000 < GET_GAME_TIMER())
				TASK_VEHICLE_DRIVE_WANDER(ft_franklin1_ped[0], ft_franklin1_veh[0], 10.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				SET_PED_KEEP_TASK(ft_franklin1_ped[0], TRUE)
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat(
					"dist: ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ft_franklin1_ped[0])),
					7-4+iDrawLiteralScene_row, eHUD_COLOUR_YELLOW)
			DrawLiteralSceneStringFloat(
					"time: ", TO_FLOAT(GET_GAME_TIMER() - iPoliceWantedCheckTime) / 1000.0,
					7-5+iDrawLiteralScene_row, eHUD_COLOUR_YELLOW)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL Do_PS_FT_FRANKLIN1_STAGE_TWO_TRAIN()
	
	IF IS_VEHICLE_DRIVEABLE(ft_franklin1_veh[0])
		IF NOT IS_PED_INJURED(ft_franklin1_ped[0])
			
			VECTOR ft_franklin1_ped0_coord = GET_ENTITY_COORDS(ft_franklin1_ped[0], FALSE)
			FLOAT ft_franklin1_ped0_dist = VDIST(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, ft_franklin1_ped0_coord)
			
//			IF ft_franklin1_ped0_dist2 < (4.5*4.5)
//				TASK_VEHICLE_DRIVE_WANDER(ft_franklin1_ped[0], ft_franklin1_veh[0], 10.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
//				RETURN TRUE
//			ENDIF
			
			CONST_FLOAT fMaxTrainSpeed	17.5
			CONST_FLOAT fMinTrainSpeed	0.0
			
			CONST_FLOAT fMaxTrainDist	60.0
			CONST_FLOAT fMinTrainDist	11.5
			
			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenCoords(
					vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
					ft_franklin1_ped0_coord,
					HUD_COLOUR_ORANGE)
			DrawDebugSceneSphere(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, 1.0, HUD_COLOUR_ORANGE, 0.5)
			DrawLiteralSceneStringFloat(
					"dist: ", ft_franklin1_ped0_dist,
					4+iDrawLiteralScene_row, HUD_COLOUR_ORANGE)
			#ENDIF
			
			TEXT_LABEL_63 tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut
			ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
			//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
			GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_ft_franklin1_scene,
					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
					playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerAnimDict, tPlayerAnimLoop)
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("loop anim", 3+iDrawLiteralScene_row, HUD_COLOUR_ORANGE)
				#ENDIF
				
				FLOAT m = (fMinTrainSpeed - fMaxTrainSpeed) / (fMinTrainDist - fMaxTrainDist)
				FLOAT c = ((fMaxTrainSpeed-(m*fMaxTrainDist))+ (fMinTrainSpeed-(m*fMinTrainDist)))/2
				
				fTrainCruiseSpeed = (m * ft_franklin1_ped0_dist) + c
				
				IF (fTrainCruiseSpeed > fMaxTrainSpeed)
					fTrainCruiseSpeed = fMaxTrainSpeed
				ENDIF
				IF (fTrainCruiseSpeed < fMinTrainSpeed)
					fTrainCruiseSpeed = fMinTrainSpeed
				ENDIF
			ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerAnimDict, tPlayerAnimOut)
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("exit anim", 3+iDrawLiteralScene_row, HUD_COLOUR_ORANGE)
				#ENDIF
				
				FLOAT m = (fMinTrainSpeed - fMaxTrainSpeed) / (fMinTrainDist - fMaxTrainDist)
				FLOAT c = ((fMaxTrainSpeed-(m*fMaxTrainDist))+ (fMinTrainSpeed-(m*fMinTrainDist)))/2
				
				fTrainCruiseSpeed = (m * ft_franklin1_ped0_dist) + c
				
				IF (fTrainCruiseSpeed > fMaxTrainSpeed)
					fTrainCruiseSpeed = fMaxTrainSpeed
				ENDIF
				IF (fTrainCruiseSpeed < fMinTrainSpeed)
					fTrainCruiseSpeed = fMinTrainSpeed
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("no anim??", 3+iDrawLiteralScene_row, HUD_COLOUR_ORANGE)
				#ENDIF
				
				SET_TRAIN_CRUISE_SPEED(ft_franklin1_veh[0], fMaxTrainSpeed)
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("speed: ", fTrainCruiseSpeed, 5+iDrawLiteralScene_row, HUD_COLOUR_ORANGE)
			#ENDIF
			
			
			SET_TRAIN_CRUISE_SPEED(ft_franklin1_veh[0], fTrainCruiseSpeed)
			SET_TRAIN_SPEED(ft_franklin1_veh[0], fTrainCruiseSpeed)
			
			IF (iSoundGameTime <= 0)
				iSoundGameTime = GET_GAME_TIMER()
			ELSE
				IF (GET_GAME_TIMER() > iSoundGameTime+2500)
					IF REQUEST_SCRIPT_AUDIO_BANK(tSetName)
						PLAY_SOUND_FROM_ENTITY(-1, tSoundName, ft_franklin1_veh[0], tSetName)
						iSoundGameTime = GET_GAME_TIMER()
						
						PRINTSTRING("PLAY_SOUND_FROM_ENTITY(\"")PRINTSTRING(tSoundName)PRINTSTRING("\", ")PRINTSTRING(tSetName)PRINTSTRING("\")")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_FT_FRANKLIN1_STAGE_THREE_TRAIN()
	

	IF IS_VEHICLE_DRIVEABLE(ft_franklin1_veh[0])
		IF NOT IS_PED_INJURED(ft_franklin1_ped[0])
			
//			IF IS_ENTITY_ON_SCREEN(ft_franklin1_veh[0])
//				
//				TASK_VEHICLE_DRIVE_TO_COORD(ft_franklin1_ped[0], ft_franklin1_veh[0],
//						vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
//						10.0, DRIVINGSTYLE_NORMAL, TRAIN_veh_model[0],
//						DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 4.0, -1)
//				
//				RETURN TRUE
//			ENDIF

			#IF IS_DEBUG_BUILD
			DrawDebugSceneLineBetweenCoords(
					vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord,
					vTPolice_veh_offset[0]+vPlayer_scene_ft_franklin1_coord,
					HUD_COLOUR_ORANGE)
			DrawDebugSceneSphere(vTPolice_goto_offset[0]+vPlayer_scene_ft_franklin1_coord, 1.0, HUD_COLOUR_ORANGE)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_FT_Franklin1_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_FT_Franklin1_Variables()
	Setup_Player_Scene_FT_Franklin1()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_FT_Franklin1_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_FT_Franklin1_in_progress
	AND ProgressScene(BIT_FRANKLIN|BIT_TREVOR, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_ft_franklin1_stage
			CASE PS_FT_FRANKLIN1_switchInProgress
				IF Do_PS_FT_FRANKLIN1_switchInProgress()
					IF (police_veh_model[0] <> FREIGHT)
						
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						FORCE_START_HIDDEN_EVASION(PLAYER_ID())
						
						current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_TWO_POLICE
					ELSE
						current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_TWO_TRAIN
					ENDIF
				ENDIF
			BREAK
			
			CASE PS_FT_FRANKLIN1_STAGE_TWO_POLICE
				IF Do_PS_FT_FRANKLIN1_STAGE_TWO_POLICE()
					current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_THREE_POLICE
				ENDIF
			BREAK
			CASE PS_FT_FRANKLIN1_STAGE_THREE_POLICE
				IF Do_PS_FT_FRANKLIN1_STAGE_THREE_POLICE()
					current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_FOUR_POLICE
				ENDIF
			BREAK
			CASE PS_FT_FRANKLIN1_STAGE_FOUR_POLICE
				IF Do_PS_FT_FRANKLIN1_STAGE_FOUR_POLICE()
					current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
				ENDIF
			BREAK
			
			CASE PS_FT_FRANKLIN1_STAGE_TWO_TRAIN
				IF Do_PS_FT_FRANKLIN1_STAGE_TWO_TRAIN()
					current_player_scene_ft_franklin1_stage = PS_FT_FRANKLIN1_STAGE_THREE_TRAIN
				ENDIF
			BREAK
			CASE PS_FT_FRANKLIN1_STAGE_THREE_TRAIN
				IF Do_PS_FT_FRANKLIN1_STAGE_THREE_TRAIN()
					current_player_scene_ft_franklin1_stage = FINISHED_PLAYER_SCENE_FT_FRANKLIN1
				ENDIF
			BREAK
			
			CASE FINISHED_PLAYER_SCENE_FT_FRANKLIN1
				Player_Scene_FT_Franklin1_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_FT_Franklin1_Widget()
		Player_Scene_FT_Franklin1_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_ft_franklin1_scene,
				iDrawLiteralScene_row+0, HUD_COLOUR_GREENLIGHT)
		STRING sStage = Get_String_From_Player_Scene_FT_Franklin1_Stage(current_player_scene_ft_franklin1_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_FT_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				iDrawLiteralScene_row+1, HUD_COLOUR_BLUELIGHT)
		#ENDIF
	ENDWHILE
	
	Player_Scene_FT_Franklin1_Cleanup()
ENDSCRIPT
