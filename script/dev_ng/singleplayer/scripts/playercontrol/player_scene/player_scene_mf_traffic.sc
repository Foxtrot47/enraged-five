// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 	(A1)  - Player scene MF_TRAFFIC file for use - player_scene_mf_traffic.sc		 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"
USING "player_scene_m_traffic_data.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
CONST_INT				iNUM_OF_PS_MF_TRAFFIC_VEHICLES								12
CONST_INT				iNUM_OF_PS_MF_TRUCK_VEHICLES								4
CONST_INT				iNUM_OF_PS_MF_AMBIENT_VEHICLES								4
CONST_INT				iNUM_OF_PS_MF_NEXTGEN_VEHICLES								4

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_MF_TRAFFIC_STAGE_ENUM
	PS_MF_TRAFFIC_switchInProgress,
	PS_MF_TRAFFIC_STAGE_TWO,
	
	FINISHED_PLAYER_SCENE_MF_TRAFFIC
ENDENUM
PS_MF_TRAFFIC_STAGE_ENUM		current_player_scene_mf_traffic_stage						= PS_MF_TRAFFIC_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_mf_traffic_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_MF_Traffic_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
MODEL_NAMES			eTraffic_ped_model[4]

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX		mf_traffic_veh[iNUM_OF_PS_MF_TRAFFIC_VEHICLES]
MODEL_NAMES			mf_traffic_veh_model[iNUM_OF_PS_MF_TRAFFIC_VEHICLES]
VECTOR 				vMTraffic_veh_offset[iNUM_OF_PS_MF_TRAFFIC_VEHICLES]
FLOAT 				fMTraffic_veh_offset[iNUM_OF_PS_MF_TRAFFIC_VEHICLES]

VEHICLE_INDEX		m_truck_veh[iNUM_OF_PS_MF_TRUCK_VEHICLES]
MODEL_NAMES			m_truck_veh_model[iNUM_OF_PS_MF_TRUCK_VEHICLES]
VECTOR 				vMTruck_veh_offset[iNUM_OF_PS_MF_TRUCK_VEHICLES]
FLOAT 				fMTruck_veh_offset[iNUM_OF_PS_MF_TRUCK_VEHICLES]

VEHICLE_INDEX		m_ambient_veh[iNUM_OF_PS_MF_AMBIENT_VEHICLES]
MODEL_NAMES			m_ambient_veh_model[iNUM_OF_PS_MF_AMBIENT_VEHICLES]
VECTOR 				vMAmbient_veh_offset[iNUM_OF_PS_MF_AMBIENT_VEHICLES]
FLOAT 				fMAmbient_veh_offset[iNUM_OF_PS_MF_AMBIENT_VEHICLES]

VEHICLE_INDEX		m_nextgen_veh[iNUM_OF_PS_MF_NEXTGEN_VEHICLES]
MODEL_NAMES			m_nextgen_veh_model[iNUM_OF_PS_MF_NEXTGEN_VEHICLES]
VECTOR 				vMNextgen_veh_offset[iNUM_OF_PS_MF_NEXTGEN_VEHICLES]
FLOAT 				fMNextgen_veh_offset[iNUM_OF_PS_MF_NEXTGEN_VEHICLES]

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_mf_traffic_coord
FLOAT				fPlayer_scene_mf_traffic_head

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//
structTimer			sHornTimer

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	player_scene_mf_traffic_widget
INT				iCurrent_player_scene_mf_traffic_stage
BOOL			bDraw_vehs, bMove_vehs, bTask_vehs, bSave_vehs
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_MF_Traffic_Cleanup()
	INT iVeh
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	REPEAT COUNT_OF(mf_traffic_veh) iVeh
		SET_VEHICLE_AS_NO_LONGER_NEEDED(mf_traffic_veh[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_truck_veh) iVeh
		SET_VEHICLE_AS_NO_LONGER_NEEDED(m_truck_veh[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_ambient_veh) iVeh
		SET_VEHICLE_AS_NO_LONGER_NEEDED(m_ambient_veh[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_nextgen_veh) iVeh
		SET_VEHICLE_AS_NO_LONGER_NEEDED(m_nextgen_veh[iVeh])
	ENDREPEAT
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eTraffic_ped_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(eTraffic_ped_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(mf_traffic_veh_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(mf_traffic_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_truck_veh_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(m_truck_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_ambient_veh_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(m_ambient_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_nextgen_veh_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(m_nextgen_veh_model[iVeh])
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	SET_VEHICLE_POPULATION_BUDGET(3)	//reset
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_mf_traffic_widget)
		DELETE_WIDGET_GROUP(player_scene_mf_traffic_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_MF_Traffic_Finished()
	//CLEAR_CprintS()
	bPlayer_Scene_MF_Traffic_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_MF_Traffic_Variables()
	TEXT_LABEL_31 tPlayer_scene_mf_traffic_room
	selected_player_scene_mf_traffic_scene	= g_eRecentlySelectedScene
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_mf_traffic_scene, vPlayer_scene_mf_traffic_coord, fPlayer_scene_mf_traffic_head, tPlayer_scene_mf_traffic_room)
	
	//- vectors -//
	
	//-- structs : PS_MF_TRAFFIC_STRUCT --//
	
	Initialise_This_Traffic_Scene_Variables(selected_player_scene_mf_traffic_scene, eTraffic_ped_model,
			mf_traffic_veh_model,	vMTraffic_veh_offset,	fMTraffic_veh_offset,
			m_truck_veh_model,		vMTruck_veh_offset,		fMTruck_veh_offset,
			m_ambient_veh_model,	vMAmbient_veh_offset,	fMAmbient_veh_offset,
			m_nextgen_veh_model,	vMNextgen_veh_offset,	fMNextgen_veh_offset)
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************

PROC SET_PED_PRUDISH_RANDOM_COMPONENT_VARIATION(PED_INDEX PedIndex)
	SET_PED_RANDOM_COMPONENT_VARIATION(PedIndex)
	
	//1543662
	IF (GET_ENTITY_MODEL(PedIndex) = A_M_M_SALTON_01)
		IF GET_PED_DRAWABLE_VARIATION(PedIndex, PED_COMP_TORSO) = 1
			IF GET_RANDOM_BOOL()
				SET_PED_COMPONENT_VARIATION(PedIndex, PED_COMP_TORSO, 0, GET_PED_TEXTURE_VARIATION(PedIndex, PED_COMP_TORSO))
				CPRINTLN(DEBUG_SWITCH, "prudish salton traffic dude 0")
			ELSE
				SET_PED_COMPONENT_VARIATION(PedIndex, PED_COMP_TORSO, 2, GET_PED_TEXTURE_VARIATION(PedIndex, PED_COMP_TORSO))
				CPRINTLN(DEBUG_SWITCH, "prudish salton traffic dude 2")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_RANDOM_PED_MODEL_ID_IN_RANGE()
	INT iPedId = GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(eTraffic_ped_model))
	
	IF HAS_MODEL_LOADED(eTraffic_ped_model[iPedId])
		RETURN iPedId
	ENDIF
	
	// for some reason there is a pedmodel which hasn't been loaded... (1720217)
	MODEL_NAMES eSafe_traffic_ped_model[4]
	INT i, iPedCount = 0
	REPEAT COUNT_OF(eTraffic_ped_model) i
		IF HAS_MODEL_LOADED(eTraffic_ped_model[i])
			eSafe_traffic_ped_model[iPedCount] = eTraffic_ped_model[i]
			iPedCount++
		ENDIF
	ENDREPEAT
	
	IF iPedCount <= 0
		SCRIPT_ASSERT("badness happening in GET_RANDOM_PED_MODEL_ID_IN_RANGE(iPedCount <= 0)!!!")
		RETURN 0
	ENDIF
	
	iPedId = GET_RANDOM_INT_IN_RANGE(0, iPedCount)
	REPEAT COUNT_OF(eTraffic_ped_model) i
		IF eSafe_traffic_ped_model[iPedId] = eTraffic_ped_model[i]
			RETURN i
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("worseness happening in GET_RANDOM_PED_MODEL_ID_IN_RANGE()!!!")
	RETURN iPedId
ENDFUNC

//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_MF_Traffic()
	INT iVeh
	
	#IF NOT IS_NEXTGEN_BUILD
	SET_VEHICLE_POPULATION_BUDGET(0)		//#1569574
	#ENDIF
	#IF IS_NEXTGEN_BUILD
	SET_VEHICLE_POPULATION_BUDGET(1)
	#ENDIF
	
	//- request models - peds -//
	REPEAT COUNT_OF(eTraffic_ped_model) iVeh
		REQUEST_MODEL(eTraffic_ped_model[iVeh])
	ENDREPEAT
	
	//- request models - vehicles -//
	REPEAT COUNT_OF(mf_traffic_veh_model) iVeh
		REQUEST_MODEL(mf_traffic_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_truck_veh_model) iVeh
		REQUEST_MODEL(m_truck_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_ambient_veh_model) iVeh
		REQUEST_MODEL(m_ambient_veh_model[iVeh])
	ENDREPEAT
	REPEAT COUNT_OF(m_nextgen_veh_model) iVeh
		REQUEST_MODEL(m_nextgen_veh_model[iVeh])
	ENDREPEAT
	
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	#IF IS_DEBUG_BUILD
	INT iLoadingAssets = 0
	#ENDIF
	
	CONST_INT 	lodDist		250	//ROUND(fLodDist)
	CONST_FLOAT	lodMult		2.0
	
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		REPEAT COUNT_OF(eTraffic_ped_model) iVeh
			IF NOT HAS_MODEL_LOADED(eTraffic_ped_model[iVeh])
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = ("eTraffic_ped_model[")
				str += (GET_MODEL_NAME_FOR_DEBUG(eTraffic_ped_model[iVeh]))
				str += ("]")
				DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
				REQUEST_MODEL(eTraffic_ped_model[iVeh])
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDREPEAT
		
		REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iVeh
			IF NOT DOES_ENTITY_EXIST(mf_traffic_veh[iVeh])
				IF NOT HAS_MODEL_LOADED(mf_traffic_veh_model[iVeh])
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("mf_traffic_veh_model[")
					str += (GET_MODEL_NAME_FOR_DEBUG(mf_traffic_veh_model[iVeh]))
					str += ("]")
					DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
					REQUEST_MODEL(mf_traffic_veh_model[iVeh])
					bAllAssetsLoaded = FALSE
				ELSE
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMTraffic_veh_offset[iVeh], 10.0, TRUE)
					mf_traffic_veh[iVeh] = CREATE_VEHICLE(mf_traffic_veh_model[iVeh],
							vPlayer_scene_mf_traffic_coord+vMTraffic_veh_offset[iVeh],
							fPlayer_scene_mf_traffic_head+fMTraffic_veh_offset[iVeh])
					SET_ENTITY_LOD_DIST(mf_traffic_veh[iVeh], lodDist)
					SET_VEHICLE_LOD_MULTIPLIER(mf_traffic_veh[iVeh], lodMult)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mf_traffic_veh[iVeh], TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iVeh
			IF NOT DOES_ENTITY_EXIST(m_truck_veh[iVeh])
				IF NOT HAS_MODEL_LOADED(m_truck_veh_model[iVeh])
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("m_truck_veh_model[")
					str += (GET_MODEL_NAME_FOR_DEBUG(m_truck_veh_model[iVeh]))
					str += ("]")
					DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
					REQUEST_MODEL(m_truck_veh_model[iVeh])
					bAllAssetsLoaded = FALSE
				ELSE
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMTruck_veh_offset[iVeh], 10.0, TRUE)
					m_truck_veh[iVeh] = CREATE_VEHICLE(m_truck_veh_model[iVeh],
							vPlayer_scene_mf_traffic_coord+vMTruck_veh_offset[iVeh],
							fPlayer_scene_mf_traffic_head+fMTruck_veh_offset[iVeh])
					SET_ENTITY_LOD_DIST(m_truck_veh[iVeh], lodDist)
					SET_VEHICLE_LOD_MULTIPLIER(m_truck_veh[iVeh], lodMult)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(m_truck_veh[iVeh], TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iVeh
			IF NOT DOES_ENTITY_EXIST(m_ambient_veh[iVeh])
				IF NOT HAS_MODEL_LOADED(m_ambient_veh_model[iVeh])
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("m_ambient_veh_model[")
					str += (GET_MODEL_NAME_FOR_DEBUG(m_ambient_veh_model[iVeh]))
					str += ("]")
					DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
					REQUEST_MODEL(m_ambient_veh_model[iVeh])
					bAllAssetsLoaded = FALSE
				ELSE
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMAmbient_veh_offset[iVeh], 10.0, TRUE)
					m_ambient_veh[iVeh] = CREATE_VEHICLE(m_ambient_veh_model[iVeh],
							vPlayer_scene_mf_traffic_coord+vMAmbient_veh_offset[iVeh],
							fPlayer_scene_mf_traffic_head+fMAmbient_veh_offset[iVeh])
					SET_ENTITY_LOD_DIST(m_ambient_veh[iVeh], lodDist)
					SET_VEHICLE_LOD_MULTIPLIER(m_ambient_veh[iVeh], lodMult)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(m_ambient_veh[iVeh], TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iVeh
			IF NOT DOES_ENTITY_EXIST(m_nextgen_veh[iVeh])
				IF NOT HAS_MODEL_LOADED(m_nextgen_veh_model[iVeh])
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = ("m_ambient_veh_model[")
					str += (GET_MODEL_NAME_FOR_DEBUG(m_nextgen_veh_model[iVeh]))
					str += ("]")
					DrawLiteralSceneString(str, 5+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
					REQUEST_MODEL(m_nextgen_veh_model[iVeh])
					bAllAssetsLoaded = FALSE
				ELSE
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMNextgen_veh_offset[iVeh], 10.0, TRUE)
					m_nextgen_veh[iVeh] = CREATE_VEHICLE(m_nextgen_veh_model[iVeh],
							vPlayer_scene_mf_traffic_coord+vMNextgen_veh_offset[iVeh],
							fPlayer_scene_mf_traffic_head+fMNextgen_veh_offset[iVeh])
					SET_ENTITY_LOD_DIST(m_nextgen_veh[iVeh], lodDist)
					SET_VEHICLE_LOD_MULTIPLIER(m_nextgen_veh[iVeh], lodMult)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(m_nextgen_veh[iVeh], TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	
	//- create any script vehicles -//
	REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iVeh
		IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iVeh])
			PED_INDEX traffic_ped
			traffic_ped = CREATE_PED_INSIDE_VEHICLE(mf_traffic_veh[iVeh], PEDTYPE_CIVMALE,
					eTraffic_ped_model[GET_RANDOM_PED_MODEL_ID_IN_RANGE()],
					VS_DRIVER)
			
			SET_PED_PRUDISH_RANDOM_COMPONENT_VARIATION(traffic_ped)
			
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mf_traffic_veh[iVeh], TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(traffic_ped, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mf_traffic_veh_model[iVeh])
		ENDIF
	ENDREPEAT
	
	REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iVeh
		IF NOT IS_ENTITY_DEAD(m_truck_veh[iVeh])
			IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_truck_veh[iVeh]) > 0)
				PED_INDEX truck_ped
				truck_ped = CREATE_PED_INSIDE_VEHICLE(m_truck_veh[iVeh], PEDTYPE_CIVMALE,
						eTraffic_ped_model[GET_RANDOM_PED_MODEL_ID_IN_RANGE()],
						VS_DRIVER)
			
				SET_PED_PRUDISH_RANDOM_COMPONENT_VARIATION(truck_ped)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mf_traffic_veh[iVeh], TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(truck_ped, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_truck_veh_model[iVeh])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_truck_veh[2]) > 0)
		ATTACH_VEHICLE_TO_TRAILER(m_truck_veh[0], m_truck_veh[2])
	ENDIF
	IF NOT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_truck_veh[3]) > 0)
		ATTACH_VEHICLE_TO_TRAILER(m_truck_veh[1], m_truck_veh[3])
	ENDIF
	
	REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iVeh
		IF NOT IS_ENTITY_DEAD(m_ambient_veh[iVeh])
			IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_ambient_veh[iVeh]) > 0)
				PED_INDEX ambient_ped
				ambient_ped = CREATE_PED_INSIDE_VEHICLE(m_ambient_veh[iVeh], PEDTYPE_CIVMALE,
						eTraffic_ped_model[GET_RANDOM_PED_MODEL_ID_IN_RANGE()],
						VS_DRIVER)
			
				SET_PED_PRUDISH_RANDOM_COMPONENT_VARIATION(ambient_ped)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mf_traffic_veh[iVeh], TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ambient_ped, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_ambient_veh_model[iVeh])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iVeh
		IF NOT IS_ENTITY_DEAD(m_nextgen_veh[iVeh])
			IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_nextgen_veh[iVeh]) > 0)
				PED_INDEX ambient_ped
				ambient_ped = CREATE_PED_INSIDE_VEHICLE(m_nextgen_veh[iVeh], PEDTYPE_CIVMALE,
						eTraffic_ped_model[GET_RANDOM_PED_MODEL_ID_IN_RANGE()],
						VS_DRIVER)
			
				SET_PED_PRUDISH_RANDOM_COMPONENT_VARIATION(ambient_ped)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mf_traffic_veh[iVeh], TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ambient_ped, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_nextgen_veh_model[iVeh])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(eTraffic_ped_model) iVeh
		SET_MODEL_AS_NO_LONGER_NEEDED(eTraffic_ped_model[iVeh])
	ENDREPEAT
	
//	INSTANTLY_FILL_PED_POPULATION()
//	INSTANTLY_FILL_VEHICLE_POPULATION()
	
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC


PROC Task_Vehicle_Drive_To_Offset_And_Wander(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, FLOAT CruiseSpeed, DRIVINGMODE Mode)
	/* old
	TASK_VEHICLE_DRIVE_WANDER(PedIndex, VehicleIndex, CruiseSpeed, Mode)
	*/
	
	VECTOR VecOffset = <<0,CruiseSpeed,0>>
	VECTOR VecCoors = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, VecOffset)
	
	SEQUENCE_INDEX drive_sequence
	OPEN_SEQUENCE_TASK(drive_sequence)
		TASK_VEHICLE_DRIVE_TO_COORD(NULL, VehicleIndex, VecCoors, CruiseSpeed,
				DRIVINGSTYLE_NORMAL,
				GET_ENTITY_MODEL(VehicleIndex),
				Mode, 2.0,5.0)
		TASK_VEHICLE_DRIVE_WANDER(NULL, VehicleIndex, CruiseSpeed, Mode)
	CLOSE_SEQUENCE_TASK(drive_sequence)
	TASK_PERFORM_SEQUENCE(PedIndex, drive_sequence)
	CLEAR_SEQUENCE_TASK(drive_sequence)
	
ENDPROC


#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_MF_Traffic_Stage(PS_MF_TRAFFIC_STAGE_ENUM this_player_scene_mf_traffic_stage)
	SWITCH this_player_scene_mf_traffic_stage
		CASE PS_MF_TRAFFIC_switchInProgress
			RETURN "PS_MF_TRAFFIC_switchInProgress"
		BREAK
		CASE PS_MF_TRAFFIC_STAGE_TWO
			RETURN "PS_MF_TRAFFIC_STAGE_TWO"
		BREAK
		
		CASE FINISHED_PLAYER_SCENE_MF_TRAFFIC
			RETURN "FINISHED_PLAYER_SCENE_MF_TRAFFIC"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_MF_Traffic_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_MF_Traffic_widget()
	INT iWidget
	TEXT_LABEL_63 sWidget
	
	sWidget = "player_scene_mf_traffic.sc - "
	sWidget += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_mf_traffic_scene)
	player_scene_mf_traffic_widget = START_WIDGET_GROUP(sWidget)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_MF_TRAFFIC_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_MF_Traffic_Stage(INT_TO_ENUM(PS_MF_TRAFFIC_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_mf_traffic_stage", iCurrent_player_scene_mf_traffic_stage)
		
		START_WIDGET_GROUP("mf_traffic_veh")
			REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iWidget
				sWidget  = "mf_traffic_veh "
				sWidget += iWidget
				sWidget += " "
				sWidget += GET_MODEL_NAME_FOR_DEBUG(mf_traffic_veh_model[iWidget])
				START_WIDGET_GROUP(sWidget)
					ADD_WIDGET_VECTOR_SLIDER("vMTraffic_veh_offset", vMTraffic_veh_offset[iWidget], -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fMTraffic_veh_offset", fMTraffic_veh_offset[iWidget], -360.0, 360.0, 0.1)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("m_truck_veh")
			REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iWidget
				sWidget  = "m_truck_veh "
				sWidget += iWidget
				sWidget += " "
				sWidget += GET_MODEL_NAME_FOR_DEBUG(m_truck_veh_model[iWidget])
				START_WIDGET_GROUP(sWidget)
					ADD_WIDGET_VECTOR_SLIDER("vMTruck_veh_offset", vMTruck_veh_offset[iWidget], -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fMTruck_veh_offset", fMTruck_veh_offset[iWidget], -360.0, 360.0, 0.1)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("m_ambient_veh")
			REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iWidget
				sWidget  = "m_ambient_veh "
				sWidget += iWidget
				sWidget += " "
				sWidget += GET_MODEL_NAME_FOR_DEBUG(m_ambient_veh_model[iWidget])
				START_WIDGET_GROUP(sWidget)
					ADD_WIDGET_VECTOR_SLIDER("vMAmbient_veh_offset", vMAmbient_veh_offset[iWidget], -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fMAmbient_veh_offset", fMAmbient_veh_offset[iWidget], -360.0, 360.0, 0.1)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("m_nextgen_veh")
			REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iWidget
				sWidget  = "m_nextgen_veh "
				sWidget += iWidget
				sWidget += " "
				sWidget += GET_MODEL_NAME_FOR_DEBUG(m_nextgen_veh_model[iWidget])
				START_WIDGET_GROUP(sWidget)
					ADD_WIDGET_VECTOR_SLIDER("vMNextgen_veh_offset", vMNextgen_veh_offset[iWidget], -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fMNextgen_veh_offset", fMNextgen_veh_offset[iWidget], -360.0, 360.0, 0.1)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("bDraw_vehs", bDraw_vehs)
		ADD_WIDGET_BOOL("bMove_vehs", bMove_vehs)
		ADD_WIDGET_BOOL("bTask_vehs", bTask_vehs)
		ADD_WIDGET_BOOL("bSave_vehs", bSave_vehs)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_MF_Traffic_Widget()
	iCurrent_player_scene_mf_traffic_stage = ENUM_TO_INT(current_player_scene_mf_traffic_stage)
	
	
	IF bDraw_vehs
		
		IF NOT (g_iDebugSelectedFriendConnDisplay > 0)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_iDebugSelectedFriendConnDisplay = 1
		ENDIF
		IF NOT (g_bDrawLiteralSceneString)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDrawLiteralSceneString = TRUE
		ENDIF
		
		INT iDraw
		TEXT_LABEL str
		REPEAT COUNT_OF(mf_traffic_veh) iDraw
			IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iDraw])
				str = "traffic "
				str += iDraw
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(mf_traffic_veh[iDraw]), 0.0,
						HUD_COLOUR_RED)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(m_truck_veh) iDraw
			IF NOT IS_ENTITY_DEAD(m_truck_veh[iDraw])
				str = "truck "
				str += iDraw
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(m_truck_veh[iDraw]), 0.0,
						HUD_COLOUR_GREEN)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(m_ambient_veh) iDraw
			IF NOT IS_ENTITY_DEAD(m_ambient_veh[iDraw])
				str = "ambient "
				str += iDraw
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(m_ambient_veh[iDraw]), 0.0,
						HUD_COLOUR_BLUE)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(m_nextgen_veh) iDraw
			IF NOT IS_ENTITY_DEAD(m_nextgen_veh[iDraw])
				str = "nextgen "
				str += iDraw
				DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(m_nextgen_veh[iDraw]), 0.0,
						HUD_COLOUR_ORANGE)
			ENDIF
		ENDREPEAT
	ENDIF
	IF bMove_vehs
		INT iMove
		PED_VEH_DATA_STRUCT sData    //MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
		VECTOR vCoordOffset = <<0,0,0>>
		FLOAT fHead = 0
		VECTOR vDriveOffset = <<0,0,0>>
		FLOAT fDriveSpeed = 0
		
//		WHILE bMove_vehs
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX player_veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(player_veh)
					IF GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_MICHAEL, selected_player_scene_mf_traffic_scene,
							sData, vCoordOffset, fHead,
							vDriveOffset, fDriveSpeed)
						SET_ENTITY_COORDS(player_veh, vPlayer_scene_mf_traffic_coord+vCoordOffset)
						SET_ENTITY_HEADING(player_veh, fPlayer_scene_mf_traffic_head+fHead)
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CLEAR_AREA(vPlayer_scene_mf_traffic_coord, 5, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iMove
				IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iMove])
					SET_ENTITY_COORDS(mf_traffic_veh[iMove], vPlayer_scene_mf_traffic_coord+vMTraffic_veh_offset[iMove])
					SET_ENTITY_HEADING(mf_traffic_veh[iMove], fPlayer_scene_mf_traffic_head+fMTraffic_veh_offset[iMove])
					
					PED_INDEX mf_traffic_ped = GET_PED_IN_VEHICLE_SEAT(mf_traffic_veh[iMove], VS_DRIVER)
					IF NOT IS_PED_INJURED(mf_traffic_ped)
						CLEAR_PED_TASKS(mf_traffic_ped)
					ENDIF
					
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMTraffic_veh_offset[iMove], 5, TRUE)
				ENDIF
			ENDREPEAT
			REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iMove
				IF NOT IS_ENTITY_DEAD(m_truck_veh[iMove])
					IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_truck_veh[iMove]) > 0)
						SET_ENTITY_COORDS(m_truck_veh[iMove], vPlayer_scene_mf_traffic_coord+vMTruck_veh_offset[iMove])
						SET_ENTITY_HEADING(m_truck_veh[iMove], fPlayer_scene_mf_traffic_head+fMTruck_veh_offset[iMove])
					ENDIF
					
					PED_INDEX m_truck_ped = GET_PED_IN_VEHICLE_SEAT(m_truck_veh[iMove], VS_DRIVER)
					IF NOT IS_PED_INJURED(m_truck_ped)
						CLEAR_PED_TASKS(m_truck_ped)
					ENDIF
					
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMTruck_veh_offset[iMove], 5, TRUE)
				ENDIF
			ENDREPEAT
			REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iMove
				IF NOT IS_ENTITY_DEAD(m_ambient_veh[iMove])
					IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_ambient_veh[iMove]) > 0)
						SET_ENTITY_COORDS(m_ambient_veh[iMove], vPlayer_scene_mf_traffic_coord+vMAmbient_veh_offset[iMove])
						SET_ENTITY_HEADING(m_ambient_veh[iMove], fPlayer_scene_mf_traffic_head+fMAmbient_veh_offset[iMove])
					ENDIF
					
					PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_ambient_veh[iMove], VS_DRIVER)
					IF NOT IS_PED_INJURED(m_ambient_ped)
						CLEAR_PED_TASKS(m_ambient_ped)
					ENDIF
					
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMAmbient_veh_offset[iMove], 5, TRUE)
				ENDIF
			ENDREPEAT
			REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iMove
				IF NOT IS_ENTITY_DEAD(m_nextgen_veh[iMove])
					IF (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(m_nextgen_veh[iMove]) > 0)
						SET_ENTITY_COORDS(m_nextgen_veh[iMove], vPlayer_scene_mf_traffic_coord+vMNextgen_veh_offset[iMove])
						SET_ENTITY_HEADING(m_nextgen_veh[iMove], fPlayer_scene_mf_traffic_head+fMNextgen_veh_offset[iMove])
					ENDIF
					
					PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_nextgen_veh[iMove], VS_DRIVER)
					IF NOT IS_PED_INJURED(m_ambient_ped)
						CLEAR_PED_TASKS(m_ambient_ped)
					ENDIF
					
					CLEAR_AREA(vPlayer_scene_mf_traffic_coord+vMNextgen_veh_offset[iMove], 5, TRUE)
				ENDIF
			ENDREPEAT
			
//			WAIT(0)
//		ENDWHILE
	ENDIF
	
	IF bTask_vehs
		INT iTask
		REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iTask
			IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iTask])
				PED_INDEX mf_traffic_ped = GET_PED_IN_VEHICLE_SEAT(mf_traffic_veh[iTask], VS_DRIVER)
				IF NOT IS_PED_INJURED(mf_traffic_ped)
					Task_Vehicle_Drive_To_Offset_And_Wander(mf_traffic_ped, mf_traffic_veh[iTask], 10, DRIVINGMODE_STOPFORCARS)
					SET_PED_KEEP_TASK(mf_traffic_ped, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iTask
			IF NOT IS_ENTITY_DEAD(m_truck_veh[iTask])
				PED_INDEX m_truck_ped = GET_PED_IN_VEHICLE_SEAT(m_truck_veh[iTask], VS_DRIVER)
				IF NOT IS_PED_INJURED(m_truck_ped)
					Task_Vehicle_Drive_To_Offset_And_Wander(m_truck_ped, m_truck_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
					SET_PED_KEEP_TASK(m_truck_ped, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iTask
			IF NOT IS_ENTITY_DEAD(m_ambient_veh[iTask])
				PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_ambient_veh[iTask], VS_DRIVER)
				IF NOT IS_PED_INJURED(m_ambient_ped)
					Task_Vehicle_Drive_To_Offset_And_Wander(m_ambient_ped, m_ambient_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
					SET_PED_KEEP_TASK(m_ambient_ped, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iTask
			IF NOT IS_ENTITY_DEAD(m_nextgen_veh[iTask])
				PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_nextgen_veh[iTask], VS_DRIVER)
				IF NOT IS_PED_INJURED(m_ambient_ped)
					Task_Vehicle_Drive_To_Offset_And_Wander(m_ambient_ped, m_nextgen_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
					SET_PED_KEEP_TASK(m_ambient_ped, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		bTask_vehs = FALSE
	ENDIF
	IF bSave_vehs
		INT iSave
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Initialise_Player_Scene_MF_Traffic_Variables()")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	vTraffic_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vMTraffic_veh_offset[iSave])
				
				SAVE_STRING_TO_DEBUG_FILE("		fTraffic_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fMTraffic_veh_offset[iSave])
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	vTruck_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vMTruck_veh_offset[iSave])
				
				SAVE_STRING_TO_DEBUG_FILE("			fTruck_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fMTruck_veh_offset[iSave])
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	vAmbient_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vMAmbient_veh_offset[iSave])
				
				SAVE_STRING_TO_DEBUG_FILE("			fAmbient_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fMAmbient_veh_offset[iSave])
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	vNextgen_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vMNextgen_veh_offset[iSave])
				
				SAVE_STRING_TO_DEBUG_FILE("			fNextgen_veh_offset[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fMNextgen_veh_offset[iSave])
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			
			SAVE_STRING_TO_DEBUG_FILE("	//- ints -//")SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	mf_traffic_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(mf_traffic_veh_model[iSave]))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	m_truck_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(m_truck_veh_model[iSave]))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	m_ambient_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(m_ambient_veh_model[iSave]))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iSave
				SAVE_STRING_TO_DEBUG_FILE("	m_nextgen_veh_model[")
				SAVE_INT_TO_DEBUG_FILE(iSave)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(m_nextgen_veh_model[iSave]))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			
			SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		
		bSave_vehs = FALSE
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_MF_Traffic_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_mf_traffic_stage = FINISHED_PLAYER_SCENE_MF_TRAFFIC
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
		bSave_vehs = TRUE
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************
PROC TASK_TRAFFIC_VEHICLES_DRIVE_WANDER()
	
	INT iTask
	REPEAT iNUM_OF_PS_MF_TRAFFIC_VEHICLES iTask
		IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iTask])
			PED_INDEX mf_traffic_ped = GET_PED_IN_VEHICLE_SEAT(mf_traffic_veh[iTask], VS_DRIVER)
			IF NOT IS_PED_INJURED(mf_traffic_ped)
				Task_Vehicle_Drive_To_Offset_And_Wander(mf_traffic_ped, mf_traffic_veh[iTask], 10, DRIVINGMODE_STOPFORCARS)
				SET_PED_KEEP_TASK(mf_traffic_ped, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT iNUM_OF_PS_MF_TRUCK_VEHICLES iTask
		IF NOT IS_ENTITY_DEAD(m_truck_veh[iTask])
			PED_INDEX m_truck_ped = GET_PED_IN_VEHICLE_SEAT(m_truck_veh[iTask], VS_DRIVER)
			IF NOT IS_PED_INJURED(m_truck_ped)
				Task_Vehicle_Drive_To_Offset_And_Wander(m_truck_ped, m_truck_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
				SET_PED_KEEP_TASK(m_truck_ped, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT iNUM_OF_PS_MF_AMBIENT_VEHICLES iTask
		IF NOT IS_ENTITY_DEAD(m_ambient_veh[iTask])
			PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_ambient_veh[iTask], VS_DRIVER)
			IF NOT IS_PED_INJURED(m_ambient_ped)
				Task_Vehicle_Drive_To_Offset_And_Wander(m_ambient_ped, m_ambient_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
				SET_PED_KEEP_TASK(m_ambient_ped, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT iNUM_OF_PS_MF_NEXTGEN_VEHICLES iTask
		IF NOT IS_ENTITY_DEAD(m_nextgen_veh[iTask])
			PED_INDEX m_ambient_ped = GET_PED_IN_VEHICLE_SEAT(m_nextgen_veh[iTask], VS_DRIVER)
			IF NOT IS_PED_INJURED(m_ambient_ped)
				Task_Vehicle_Drive_To_Offset_And_Wander(m_ambient_ped, m_nextgen_veh[iTask], 5, DRIVINGMODE_STOPFORCARS)
				SET_PED_KEEP_TASK(m_ambient_ped, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_MF_TRAFFIC_switchInProgress()
	
	
	
	IF COUNTDOWNTIMER_DO_WHEN_READY(sHornTimer, 0.0)
		INT iVeh = GET_RANDOM_INT_IN_RANGE(0, iNUM_OF_PS_MF_TRAFFIC_VEHICLES)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringInt("iVeh: ", iVeh, 8)
		#ENDIF
		
		IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iVeh])
			PED_INDEX mf_traffic_ped = GET_PED_IN_VEHICLE_SEAT(mf_traffic_veh[iVeh], VS_DRIVER)
			IF NOT IS_PED_INJURED(mf_traffic_ped)
				FLOAT fRandomHornTime = GET_RANDOM_FLOAT_IN_RANGE(0.5, 1.0)*2.0
				FLOAT fRandomHornWait = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.5)*2.0
				
				CPRINTLN(DEBUG_SWITCH, "START_VEHICLE_HORN(mf_traffic_veh[", iVeh, "],", fRandomHornTime, ")	//", fRandomHornWait)
				
				START_VEHICLE_HORN(mf_traffic_veh[iVeh], ROUND(fRandomHornTime*1000.0))
				RESTART_COUNTDOWNTIMER_AT(sHornTimer, fRandomHornWait)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		TASK_TRAFFIC_VEHICLES_DRIVE_WANDER()
		START_COUNTDOWNTIMER_AT(sHornTimer, GET_RANDOM_FLOAT_IN_RANGE(1.75, 2.25))
		
		RETURN TRUE
	ELSE
		//CPRINTLN(DEBUG_SWITCH, "IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")
	ENDIF
	
	IF NOT IS_COUNTDOWNTIMER_STARTED(sHornTimer)
		START_COUNTDOWNTIMER_AT(sHornTimer, GET_RANDOM_FLOAT_IN_RANGE(1.75, 2.25)*2.0)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_MF_TRAFFIC_STAGE_TWO()

	
	#IF IS_DEBUG_BUILD
	DrawLiteralSceneStringFloat("sHornTimer: ", GET_COUNTDOWNTIMER_IN_SECONDS(sHornTimer), 7)
	#ENDIF

	IF COUNTDOWNTIMER_DO_WHEN_READY(sHornTimer, 0.0)
		INT iVeh = GET_RANDOM_INT_IN_RANGE(0, iNUM_OF_PS_MF_TRAFFIC_VEHICLES)
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringInt("iVeh: ", iVeh, 8)
		#ENDIF
		
		IF NOT IS_ENTITY_DEAD(mf_traffic_veh[iVeh])
			PED_INDEX mf_traffic_ped = GET_PED_IN_VEHICLE_SEAT(mf_traffic_veh[iVeh], VS_DRIVER)
			IF NOT IS_PED_INJURED(mf_traffic_ped)
				FLOAT fRandomHornTime = GET_RANDOM_FLOAT_IN_RANGE(0.5, 1.0)
				FLOAT fRandomHornWait = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.5)
				
				CPRINTLN(DEBUG_SWITCH, "START_VEHICLE_HORN(mf_traffic_veh[", iVeh, "],", fRandomHornTime, ")	//", fRandomHornWait)
				
				START_VEHICLE_HORN(mf_traffic_veh[iVeh], ROUND(fRandomHornTime*1000.0))
				RESTART_COUNTDOWNTIMER_AT(sHornTimer, fRandomHornWait)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_mf_traffic_coord) > 100.0
		CPRINTLN(DEBUG_SWITCH, "player is 100m+ from the chateau")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_MF_Traffic_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_MF_Traffic_Variables()
	Setup_Player_Scene_MF_Traffic()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_MF_Traffic_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_MF_Traffic_in_progress
	AND ProgressScene(BIT_MICHAEL|BIT_FRANKLIN, NULL)
		IF Should_Switch_Cleanup_Be_Forced()
			Player_Scene_MF_Traffic_Cleanup()
		ENDIF
		
		WAIT(0)
		
		SWITCH current_player_scene_mf_traffic_stage
			CASE PS_MF_TRAFFIC_switchInProgress
				IF Do_PS_MF_TRAFFIC_switchInProgress()
					SET_VEHICLE_POPULATION_BUDGET(2)
		
					current_player_scene_mf_traffic_stage = PS_MF_TRAFFIC_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_MF_TRAFFIC_STAGE_TWO
				IF Do_PS_MF_TRAFFIC_STAGE_TWO()
					current_player_scene_mf_traffic_stage = FINISHED_PLAYER_SCENE_MF_TRAFFIC
				ENDIF
			BREAK
			
			CASE FINISHED_PLAYER_SCENE_MF_TRAFFIC
				Player_Scene_MF_Traffic_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_MF_Traffic_Widget()
		Player_Scene_MF_Traffic_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_mf_traffic_scene,
				5, HUD_COLOUR_BLUELIGHT)
		STRING sStage = Get_String_From_Player_Scene_mf_traffic_Stage(current_player_scene_mf_traffic_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_MF_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				6, HUD_COLOUR_BLUELIGHT)
		#ENDIF
	ENDWHILE
	
	Player_Scene_MF_Traffic_Cleanup()
ENDSCRIPT
