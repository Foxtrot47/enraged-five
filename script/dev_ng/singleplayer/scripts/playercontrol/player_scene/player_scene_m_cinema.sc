// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene M_CINEMA file for use - player_scene_m_cinema.sc		 ___
// ___ 																					 ___
// _________________________________________________________________________________________



USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSforcinema			3

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_M_CINEMA_STAGE_ENUM
	PS_M_CINEMA_switchInProgress,
	PS_M_CINEMA_STAGE_TWO,
	
	FINISHED_M_CINEMA
ENDENUM
PS_M_CINEMA_STAGE_ENUM		current_player_scene_m_cinema_stage						= PS_M_CINEMA_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_m_cinema_scene					= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_M_Cinema_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
PED_INDEX				cinema_ped[4]
MODEL_NAMES				eCinemaModel[4]
INT						iCinemaStage[4]
TEXT_LABEL				tCinemaScript[4]

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_m_cinema_coord
FLOAT				fPlayer_scene_m_cinema_head

VECTOR				vCinema_coordOffset[4]
FLOAT				fCinema_headOffset[4]

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fMicCinAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_m_cinema_widget
INT				iCurrent_player_scene_m_cinema_stage
BOOL			bMove_peds, bSave_peds
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_M_Cinema_Cleanup()
	INT iPed
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eCinemaModel) iPed
		IF (eCinemaModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eCinemaModel[iPed])
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_m_cinema_widget)
		DELETE_WIDGET_GROUP(player_scene_m_cinema_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_M_Cinema_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_M_Cinema_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_M_Cinema_Variables()
	
	selected_player_scene_m_cinema_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_m_cinema_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_m_cinema_scene, vPlayer_scene_m_cinema_coord, fPlayer_scene_m_cinema_head, tPlayer_scene_m_cinema_room)
	
	PED_SCENE_STRUCT sPedScene
	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	
	sPedScene.iStage = 0
	sPedScene.eScene = selected_player_scene_m_cinema_scene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
	
	SWITCH selected_player_scene_m_cinema_scene
		CASE PR_SCENE_M4_CINEMA
			//- text labels -//
			//- vectors -//
			//- vectors -//
			vCinema_coordOffset[0] = <<-1416.5402, -205.8167, 45.5004>> - vPlayer_scene_m_cinema_coord
			vCinema_coordOffset[1] = <<-1423.8984, -207.5375, 45.5004>> - vPlayer_scene_m_cinema_coord
			vCinema_coordOffset[2] = <<-1428.5013, -210.4254, 45.5004>> - vPlayer_scene_m_cinema_coord
			
			vCinema_coordOffset[3] = <<GET_RANDOM_FLOAT_IN_RANGE(-1416.5402, -1428.5013), GET_RANDOM_FLOAT_IN_RANGE(-205.8167, -210.4254), 45.5004>> - vPlayer_scene_m_cinema_coord
			

			//- floats -//
			fCinema_headOffset[0] =	5.8017 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[1] =	2.5820 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[2] =	-47.7025 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[3] =	65.2818 - fPlayer_scene_m_cinema_head
			
			//- enums -//
			eCinemaModel[0] = A_F_M_TOURIST_01		tCinemaScript[0] = ""
			eCinemaModel[1] = A_M_Y_HIPSTER_01		tCinemaScript[1] = ""
			eCinemaModel[2] = A_M_M_BEVHILLS_01		tCinemaScript[2] = ""
			eCinemaModel[3] = A_M_M_GENFAT_01		tCinemaScript[3] = ""
			
			//- enums -//
			iCinemaStage[0] = 0
			iCinemaStage[1] = 0
			iCinemaStage[2] = 0
			iCinemaStage[3] = 0
		BREAK
		CASE PR_SCENE_M7_HOOKERS
			//- text labels -//
			//- vectors -//
			vCinema_coordOffset[0] = <<1.2712, 6.0710, -0.7500>>		//<<534.4589, 115.0843, 95.7207>> - vPlayer_scene_m_cinema_coord
			vCinema_coordOffset[1] = <<3.8061, 5.7599, -0.5000>>		//<<536.9938, 114.7732, 95.9707>> - vPlayer_scene_m_cinema_coord
			vCinema_coordOffset[2] = <<4.5410, 7.8487, -0.4000>>		//<<537.7287, 116.8620, 96.0707>> - vPlayer_scene_m_cinema_coord
			vCinema_coordOffset[3] = <<3.1051, 13.7474, -0.3463>>		//<<536.2928, 122.7607, 96.1244>> - vPlayer_scene_m_cinema_coord

			//- floats -//
			fCinema_headOffset[0] = 123.0590							//109.2437 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[1] = 134.0498							//120.2345 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[2] = 58.6630								//44.8477 - fPlayer_scene_m_cinema_head
			fCinema_headOffset[3] = 89.5592								//75.7439 - fPlayer_scene_m_cinema_head
			
			//- enums -//
			eCinemaModel[0] = S_F_Y_HOOKER_01		tCinemaScript[0] = "pb_prostitute"
			eCinemaModel[1] = S_F_Y_HOOKER_01		tCinemaScript[1] = "pb_prostitute"
			eCinemaModel[2] = S_F_Y_HOOKER_01		tCinemaScript[2] = "pb_prostitute"
			eCinemaModel[3] = S_F_Y_HOOKER_01		tCinemaScript[3] = "pb_prostitute"
			
			//- enums -//
			iCinemaStage[0] = 1
			iCinemaStage[1] = 1
			iCinemaStage[2] = 1
			iCinemaStage[3] = 1
		BREAK
		
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_m_cinema_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_cinema_scene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(str)PRINTNL()
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- text labels -//
			//- vectors -//
			vCinema_coordOffset[0] = <<0,0,0>>
			vCinema_coordOffset[1] = <<0,0,0>>
			vCinema_coordOffset[2] = <<0,0,0>>
			vCinema_coordOffset[3] = <<0,0,0>>
			
			//- floats -//
			fCinema_headOffset[0] = 0
			fCinema_headOffset[1] = 0
			fCinema_headOffset[2] = 0
			fCinema_headOffset[3] = 0
			
			//- enums -//
			eCinemaModel[0] = DUMMY_MODEL_FOR_SCRIPT
			eCinemaModel[1] = DUMMY_MODEL_FOR_SCRIPT
			eCinemaModel[2] = DUMMY_MODEL_FOR_SCRIPT
			eCinemaModel[3] = DUMMY_MODEL_FOR_SCRIPT
			
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_M_CINEMA_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_M_Cinema()
	INT iPed
	
	INT iWaitForBuddyAssets = 0
	BOOL bAllAssetsLoaded = FALSE
	WHILE NOT bAllAssetsLoaded
	AND (iWaitForBuddyAssets < 400)
		bAllAssetsLoaded = TRUE
		
		//- request models - peds -//
		
		REPEAT COUNT_OF(eCinemaModel) iPed
			IF (eCinemaModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(eCinemaModel[iPed])
				IF NOT HAS_MODEL_LOADED(eCinemaModel[iPed])
					REQUEST_MODEL(eCinemaModel[iPed])
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(tCinemaScript[iPed])
				REQUEST_SCRIPT(tCinemaScript[iPed])
				IF NOT HAS_SCRIPT_LOADED(tCinemaScript[iPed])
					bAllAssetsLoaded = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		//- request models - vehicles -//
		//- request models - objects -//
		//- request models - weapons -//
		//- request anims and ptfx --//
		//- request vehicle recordings -//
		//- request interior models -//
		//- wait for assets to load -//
		
		IF NOT bAllAssetsLoaded
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	
	REPEAT COUNT_OF(cinema_ped) iPed
		IF (eCinemaModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			cinema_ped[iPed] = CREATE_PED(PEDTYPE_CIVMALE, eCinemaModel[iPed], vPlayer_scene_m_cinema_coord+vCinema_coordOffset[iPed])
			
			SET_ENTITY_HEADING(cinema_ped[iPed], fPlayer_scene_m_cinema_head+fCinema_headOffset[iPed])
			SET_PED_RANDOM_COMPONENT_VARIATION(cinema_ped[iPed])
			
			SET_PED_COMBAT_ATTRIBUTES(cinema_ped[iPed], CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(cinema_ped[iPed], CA_ALWAYS_FLEE, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cinema_ped[iPed], TRUE)
			SET_PED_CAN_RAGDOLL(cinema_ped[iPed], FALSE)
		ENDIF
	ENDREPEAT
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_M_Cinema_Stage(PS_M_CINEMA_STAGE_ENUM this_player_scene_m_cinema_stage)
	SWITCH this_player_scene_m_cinema_stage
		CASE PS_M_CINEMA_switchInProgress
			RETURN "PS_M_CINEMA_switchInProgress"
		BREAK
		CASE PS_M_CINEMA_STAGE_TWO
			RETURN "PS_M_CINEMA_STAGE_TWO"
		BREAK
		
		CASE FINISHED_M_CINEMA
			RETURN "FINISHED_M_CINEMA"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_M_Cinema_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_M_Cinema_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_m_cinema.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_cinema_scene)
	
	player_scene_m_cinema_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_M_CINEMA_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_M_Cinema_Stage(INT_TO_ENUM(PS_M_CINEMA_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_m_cinema_stage", iCurrent_player_scene_m_cinema_stage)
		
		START_WIDGET_GROUP("Cinema_createOffset")
			REPEAT COUNT_OF(cinema_ped) iWidget
				IF (eCinemaModel[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
					ADD_WIDGET_STRING("cinema_ped")
					
					FLOAT fCinemaCoordOffsetMag
					fCinemaCoordOffsetMag = VMAG(vCinema_coordOffset[iWidget])
					
					IF (fCinemaCoordOffsetMag < ABSF(vCinema_coordOffset[iWidget].x))
						fCinemaCoordOffsetMag = ABSF(vCinema_coordOffset[iWidget].x)
					ENDIF
					IF (fCinemaCoordOffsetMag < ABSF(vCinema_coordOffset[iWidget].y))
						fCinemaCoordOffsetMag = ABSF(vCinema_coordOffset[iWidget].y)
					ENDIF
					IF (fCinemaCoordOffsetMag < ABSF(vCinema_coordOffset[iWidget].z))
						fCinemaCoordOffsetMag = ABSF(vCinema_coordOffset[iWidget].z)
					ENDIF
					
					fCinemaCoordOffsetMag *= 2
					IF (fCinemaCoordOffsetMag < 20.0)
						fCinemaCoordOffsetMag = 20.0
					ENDIF
					
					ADD_WIDGET_VECTOR_SLIDER("vCinema_coordOffset", vCinema_coordOffset[iWidget],
						-fCinemaCoordOffsetMag, fCinemaCoordOffsetMag,
						fCinemaCoordOffsetMag * 0.005)
					ADD_WIDGET_FLOAT_SLIDER("fCinema_headOffset", fCinema_headOffset[iWidget], -180, 180, 1.0)
				ENDIF
			ENDREPEAT
			
			ADD_WIDGET_BOOL("bMove_peds", bMove_peds)
			ADD_WIDGET_BOOL("bSave_peds", bSave_peds)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_M_Cinema_Widget()
	iCurrent_player_scene_m_cinema_stage = ENUM_TO_INT(current_player_scene_m_cinema_stage)
	
	INT iWidget
	IF bMove_peds
		
		IF NOT (g_iDebugSelectedFriendConnDisplay > 0)
			g_iDebugSelectedFriendConnDisplay = 1
		ENDIF
		
		REPEAT COUNT_OF(cinema_ped) iWidget
			IF NOT IS_PED_INJURED(cinema_ped[iWidget])
//				CLEAR_PED_TASKS(cinema_ped[iWidget])
				
				SET_ENTITY_HEADING(cinema_ped[iWidget], fPlayer_scene_m_cinema_head+fCinema_headOffset[iWidget])
				SET_ENTITY_COORDS(cinema_ped[iWidget], vPlayer_scene_m_cinema_coord+vCinema_coordOffset[iWidget])
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	IF bSave_peds
		
		SAVE_STRING_TO_DEBUG_FILE("	\"player_scene_m_cinema.sc\" - bSave_peds")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(cinema_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vCinema_coordOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vCinema_coordOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_m_cinema_coord+vCinema_coordOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - vPlayer_scene_m_cinema_coord")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(cinema_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fCinema_headOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fCinema_headOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_m_cinema_head+fCinema_headOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - fPlayer_scene_m_cinema_head")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bSave_peds = FALSE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_M_Cinema_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_m_cinema_stage = FINISHED_M_CINEMA
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
		bSave_peds = TRUE
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL ShouldShopperFleeFromPlayer(PED_INDEX thisCinemaPed)
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisCinemaPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisCinemaPed)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_COMBAT(thisCinemaPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(cinema_ped) iPed
		IF (thisCinemaPed <> cinema_ped[iPed])
			IF (iCinemaStage[iPed] < 0)			//is dead
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC BOOL ControlMicCinemaPedStage(PED_INDEX thisCinemaPed, INT iPed)
	
	CONST_INT	iCONST_CINEMA_0_wanderStandard			0
	CONST_INT	iCONST_CINEMA_1_playScenario			1
	
	CONST_INT	iCONST_CINEMA_2_startflee				2
	CONST_INT	iCONST_CINEMA_3_fleePlayer				3
	
	CONST_INT	iCONST_CINEMA_4_script					4
					
	CONST_INT	iCONST_CINEMA_DEAD						-1
	
	IF DOES_ENTITY_EXIST(thisCinemaPed)
		
		SWITCH iCinemaStage[iPed]
			CASE iCONST_CINEMA_0_wanderStandard
				
				IF get_NOT_PlayerHasStartedNewScene()
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  wait for wander", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_GREENLIGHT, fMicCinAlphaMult)
					#ENDIF
					
					RETURN FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  wander...", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
				#ENDIF
				
				IF IS_PED_INJURED(thisCinemaPed)
					iCinemaStage[iPed] = iCONST_CINEMA_DEAD
					RETURN FALSE
				ENDIF
				
				IF ShouldShopperFleeFromPlayer(thisCinemaPed)
					iCinemaStage[iPed] = iCONST_CINEMA_2_startflee
					RETURN FALSE
				ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(thisCinemaPed, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
					TASK_WANDER_STANDARD(thisCinemaPed)
					SET_PED_KEEP_TASK(cinema_ped[iPed], TRUE)
				ENDIF
			BREAK
			CASE iCONST_CINEMA_1_playScenario
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  scenario...", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
				#ENDIF
				
				IF IS_PED_INJURED(thisCinemaPed)
					iCinemaStage[iPed] = iCONST_CINEMA_DEAD
					RETURN FALSE
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tCinemaScript[iPed])
					START_NEW_SCRIPT_WITH_ARGS(tCinemaScript[iPed], cinema_ped[iPed], SIZE_OF(cinema_ped[iPed]), DEFAULT_STACK_SIZE)
					
					iCinemaStage[iPed] = iCONST_CINEMA_4_script
					RETURN FALSE
				ENDIF
				
				IF ShouldShopperFleeFromPlayer(thisCinemaPed)
					iCinemaStage[iPed] = iCONST_CINEMA_2_startflee
					RETURN FALSE
				ENDIF
				
				STRING ScenarioName
				ScenarioName = "WORLD_HUMAN_PROSTITUTE_LOW_CLASS"
				
				IF NOT PED_HAS_USE_SCENARIO_TASK(thisCinemaPed)
					TASK_START_SCENARIO_IN_PLACE(thisCinemaPed, ScenarioName,
							0,		//int TimeToLeave = 0,
							TRUE)	//BOOL bPlayIntroClip = false)
					SET_PED_KEEP_TASK(cinema_ped[iPed], TRUE)
				ENDIF
			BREAK
			
			CASE iCONST_CINEMA_2_startflee
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  start flee...", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
				#ENDIF
				
				INT iPed2
				REPEAT COUNT_OF(iCinemaStage) iPed2
					IF (iPed <> iPed2)
						IF (iCinemaStage[iPed2] < iCONST_CINEMA_2_startflee)
						AND (iCinemaStage[iPed] >= 0)			//is NOT dead
							iCinemaStage[iPed2] = iCONST_CINEMA_2_startflee
						ENDIF
					ENDIF
				ENDREPEAT
				
				iCinemaStage[iPed] = iCONST_CINEMA_3_fleePlayer
				RETURN FALSE
			BREAK
			CASE iCONST_CINEMA_3_fleePlayer
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  flee...", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
				#ENDIF
				
				IF IS_PED_INJURED(thisCinemaPed)
					iCinemaStage[iPed] = iCONST_CINEMA_DEAD
					RETURN FALSE
				ENDIF
				
				CONST_FLOAT	fCONST_SHOPPER_flee_dist		50.0
				
				IF (GET_SCRIPT_TASK_STATUS(thisCinemaPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
					CLEAR_PED_TASKS(thisCinemaPed)
					TASK_SMART_FLEE_PED(thisCinemaPed, PLAYER_PED_ID(), fCONST_SHOPPER_flee_dist, 20000, TRUE)
					RETURN TRUE
				ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(thisCinemaPed), GET_ENTITY_COORDS(PLAYER_PED_ID())) > (fCONST_SHOPPER_flee_dist * fCONST_SHOPPER_flee_dist)
					iCinemaStage[iPed] = iCONST_CINEMA_0_wanderStandard
					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_CINEMA_4_script
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("script", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_RED, fMicCinAlphaMult)
				#ENDIF
				
				RETURN FALSE
			BREAK
			
			CASE iCONST_CINEMA_DEAD
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("dead", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_RED, fMicCinAlphaMult)
				#ENDIF
				
				RETURN FALSE
			BREAK
			
			DEFAULT
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("STAGE DEFAULT ", iCinemaStage[iPed], iPed+4+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
				#ENDIF
				
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("non-existant", iPed+4+iCONSTANTSforcinema, HUD_COLOUR_GREY, fMicCinAlphaMult*0.5)
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_M_CINEMA_switchInProgress()
	
	INT iPed
	REPEAT COUNT_OF(cinema_ped) iPed
		ControlMicCinemaPedStage(cinema_ped[iPed], iPed)
	ENDREPEAT
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ELSE
//		PRINTSTRING("IS_PLAYER_SWITCH_IN_PROGRESS() = NOT NOT FALSE")PRINTNL()
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_M_CINEMA_STAGE_TWO()
	
	INT iPed
	REPEAT COUNT_OF(cinema_ped) iPed
		ControlMicCinemaPedStage(cinema_ped[iPed], iPed)
	ENDREPEAT
	
	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_m_cinema_coord) > 100.0
		PRINTSTRING("player is 100m+ from the chateau")PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_M_Cinema_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_M_Cinema_Variables()
	Setup_Player_Scene_M_Cinema()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_M_Cinema_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_M_Cinema_in_progress
	AND ProgressScene(BIT_MICHAEL, cinema_ped[0])
		WAIT(0)
		
		SWITCH current_player_scene_m_cinema_stage
			CASE PS_M_CINEMA_switchInProgress
				IF Do_PS_M_CINEMA_switchInProgress()
					current_player_scene_m_cinema_stage = PS_M_CINEMA_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_M_CINEMA_STAGE_TWO
				IF Do_PS_M_CINEMA_STAGE_TWO()
					current_player_scene_m_cinema_stage = FINISHED_M_CINEMA
				ENDIF
			BREAK
			
			CASE FINISHED_M_CINEMA
				Player_Scene_M_Cinema_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_M_Cinema_Widget()
		Player_Scene_M_Cinema_Debug_Options()
		
		fMicCinAlphaMult = 1.0
		IF current_player_scene_m_cinema_stage = PS_M_CINEMA_STAGE_TWO
			
			CONST_FLOAT fMAX_PLAYER_MIC_CINEMA_DIST	20.0
			FLOAT fPlayerMicCinemaDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_m_cinema_coord)
			
			INT iPed
			REPEAT COUNT_OF(cinema_ped) iPed
				
				IF DOES_ENTITY_EXIST(cinema_ped[iPed])
					FLOAT fCinemaPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(cinema_ped[iPed], FALSE))
					
					IF (fPlayerMicCinemaDist > fCinemaPedDist)
						fPlayerMicCinemaDist = fCinemaPedDist
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			fMicCinAlphaMult = 1.0 - (fPlayerMicCinemaDist/fMAX_PLAYER_MIC_CINEMA_DIST)
			IF fMicCinAlphaMult < 0
				fMicCinAlphaMult = 0
			ENDIF
			
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_m_cinema_scene,
				2+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_M_Cinema_Stage(current_player_scene_m_cinema_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_M_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSforcinema, HUD_COLOUR_BLUELIGHT, fMicCinAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_M_Cinema_Cleanup()
ENDSCRIPT
