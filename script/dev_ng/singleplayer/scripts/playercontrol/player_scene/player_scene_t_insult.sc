// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene T_INSULT file for use - player_scene_t_insult.sc		 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
CONST_INT iCONSTANTSforinsulting			3

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T_INSULT_STAGE_ENUM
	PS_T_INSULT_switchInProgress,
	PS_T_INSULT_STAGE_TWO,
	PS_T_INSULT_STAGE_THREE,
	
	FINISHED_T_INSULT
ENDENUM
PS_T_INSULT_STAGE_ENUM		current_player_scene_t_insult_stage						= PS_T_INSULT_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_t_insult_scene					= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_T_Insult_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				insulted_ped[2]
INT						iInsultedStage[2]
MODEL_NAMES				eInsultedPedModel[2]
INT						iInsultedPedLookatStage[2], iInsultedPedLookatGameTime[2]
GROUP_INDEX				insultedGroup

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			insulted_obj[2]
MODEL_NAMES				eInsultedObjModel[2]
VECTOR					vInsultedObjPos[2], vInsultedObjRot[2]

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_t_insult_coord
FLOAT				fPlayer_scene_t_insult_head

VECTOR				vInsulted_coordOffset[2], vInsulted_wanderOffset[2]
FLOAT				fInsulted_headOffset[2], fInsulted_wanderRadius[2]

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut

TEXT_LABEL_63		tInsultAnimDict
TEXT_LABEL_63		tInsultAnimLoop[2], tInsultAnimOut[2], tInsultAnimPost[2], tInsultAnimPostDead[2], tInsultAnimPostDeadLoop[2]
TEXT_LABEL_63		tInsultPropLoop[2], tInsultPropOut[2], tInsultPropPost[2], tInsultPropPostDead[2], tInsultPropPostDeadLoop[2]
TEXT_LABEL_63		tInsultClipset[2]

INT					iInsult_AnimPost_SynchSceneID = -1

TEXT_LABEL			tTaunterSceneSpeechLabel	= ""
FLOAT				fTaunterSceneSpeechLabel	= 0.0
BOOL				bTaunterSceneSpeechLabel	= FALSE

INT					iStrugGameTime				= -1

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fInsultAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_t_insult_widget
INT				iCurrent_player_scene_t_insult_stage
BOOL			bMovePeds, bSavePeds
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_T_Insult_Cleanup()
	INT iCleanup
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eInsultedPedModel) iCleanup
		IF (eInsultedPedModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eInsultedPedModel[iCleanup])
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(eInsultedObjModel) iCleanup
		IF (eInsultedObjModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eInsultedObjModel[iCleanup])
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	REMOVE_ANIM_DICT(tInsultAnimDict)
	
	IF DOES_GROUP_EXIST(insultedGroup)
		REMOVE_GROUP(insultedGroup)
	ENDIF
	
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_t_insult_widget)
		DELETE_WIDGET_GROUP(player_scene_t_insult_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_T_Insult_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_T_Insult_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_T_Insult_Variables()
	
	selected_player_scene_t_insult_scene	= g_eRecentlySelectedScene	//PR_SCENE_T_BBINSULT_YARD	//PR_SCENE_T_BBINSULT_ALLEY
	
	TEXT_LABEL_31 tPlayer_scene_t_insult_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_t_insult_scene, vPlayer_scene_t_insult_coord, fPlayer_scene_t_insult_head, tPlayer_scene_t_insult_room)
	
	ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
	
	GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_t_insult_scene,
			tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
			playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
	
	tInsultClipset[0] = ""
	tInsultClipset[1] = ""
	
	eInsultedObjModel[0] = dummy_model_for_script
	eInsultedObjModel[1] = dummy_model_for_script
			
	SWITCH selected_player_scene_t_insult_scene
		
		CASE PR_SCENE_T_SC_CHASE
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@CHASE_STRIPPERS"
			
			tInsultAnimLoop[0] = "LOOP_STRIPPER_01"
			tInsultAnimOut[0] = "EXIT_STRIPPER_01"
			tInsultAnimPost[0] = ""
			
			tInsultAnimLoop[1] = "LOOP_STRIPPER_02"
			tInsultAnimOut[1] = "EXIT_STRIPPER_02"
			tInsultAnimPost[1] = ""
			
			tTaunterSceneSpeechLabel = "TRVS_IG_35"
			fTaunterSceneSpeechLabel = 0.15	//0.25
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vInsulted_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			vInsulted_wanderOffset[0] = <<8.2284, -35.8652, -1.0334>>
			vInsulted_wanderOffset[1] = <<28.4066, -23.8848, -0.7887>>
			
			//- floats -//
			fInsulted_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fInsulted_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fInsulted_wanderRadius[0] = 50.0
			fInsulted_wanderRadius[1] = 50.0
			
			//- enums -//
			eInsultedPedModel[0]		=  S_F_Y_STRIPPER_01
			eInsultedPedModel[1]		=  S_F_Y_STRIPPER_01
			
			iInsultedPedLookatStage[0] = -1
			iInsultedPedLookatStage[1] = -1
			
		BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@MOCKS_LAPDANCE"
			
			tInsultAnimLoop[0] = "001443_01_TRVS_28_IDLE_STRIPPER"
			tInsultAnimOut[0] = "001443_01_TRVS_28_EXIT_STRIPPER"
			tInsultAnimPost[0] = "001443_01_TRVS_28_IDLE_STRIPPER"
			
			tInsultAnimLoop[1] = "001443_01_TRVS_28_IDLE_MAN"
			tInsultAnimOut[1] = "001443_01_TRVS_28_EXIT_MAN"
			tInsultAnimPost[1] = "001443_01_TRVS_28_IDLE_MAN"
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<-1.5421, 0.8274, -1.0288>>		//<<115.4148, -1286.8466, 28.2362>> - vPlayer_scene_t_insult_coord
			vInsulted_coordOffset[1] = <<-0.9636, 1.1757, -1.0243>>		//<<115.9933, -1286.4983, 28.2407>> - vPlayer_scene_t_insult_coord
			
			vInsulted_wanderOffset[0]	= <<0,0,0>>
			vInsulted_wanderOffset[1]	= <<0,0,0>>
			
			//- floats -//
			fInsulted_headOffset[0] = -130.0000		//-239.0000 - fPlayer_scene_t_insult_head
			fInsulted_headOffset[1] = -130.0000		//-239.0000 - fPlayer_scene_t_insult_head
			
			fInsulted_wanderRadius[0] = 0.0
			fInsulted_wanderRadius[1] = 0.0
			
			//- enums -//
		/*	Hey man, FuFu only spawns in the stripclub from 8pm to 8am.
			Can you change the variation of the model you create during that time?
			You can give her black hair to look like Chasity who works the other shift.
			Thanks man.
			*/
			INT iHour
			iHour = GET_CLOCK_HOURS()
			IF iHour < 8 OR iHour > 20
				eInsultedPedModel[0]	=  S_F_Y_STRIPPER_02
			ELSE
				eInsultedPedModel[0]	=  S_F_Y_STRIPPER_01
			ENDIF
			
			eInsultedPedModel[1]		=  A_M_Y_HIPSTER_01		//A_M_M_BUSINESS_01
			
			iInsultedPedLookatStage[0] = -1
			iInsultedPedLookatStage[1] = 0
		BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@ANNOYS_SUNBATHERS"
			
			tInsultAnimLoop[0] = "trev_annoys_sunbathers_loop_girl"
			tInsultAnimOut[0] = "trev_annoys_sunbathers_exit_girl"
			tInsultAnimPost[0] = ""
			tInsultClipset[0] = "move_f@scared"
			
			tInsultAnimLoop[1] = "trev_annoys_sunbathers_loop_guy"
			tInsultAnimOut[1] = "trev_annoys_sunbathers_exit_guy"
			tInsultAnimPost[1] = ""
			tInsultClipset[1] = "move_m@hurry_Butch@A"
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<-0.3866, 0.1726, 1.0000>>		//<<-1326.2666, -1667.3174, 2.5795>> - vPlayer_scene_t_insult_coord
			vInsulted_coordOffset[1] = <<-0.0726, -0.4742, 1.0000>>		//<<-1325.9526, -1667.9642, 2.5795>> - vPlayer_scene_t_insult_coord
			
			vInsulted_wanderOffset[0] = <<-1291.4749, -1719.6064, 1.5806>> - vPlayer_scene_t_insult_coord
			vInsulted_wanderOffset[1] = vInsulted_wanderOffset[0]
			
			//- floats -//
			fInsulted_headOffset[0] = 0.0
			fInsulted_headOffset[1] = 0.0
			
			fInsulted_wanderRadius[0] = 20.000
			fInsulted_wanderRadius[1] = fInsulted_wanderRadius[0]
			
			//- enums -//
			eInsultedPedModel[0] =  A_F_Y_BEACH_01
			eInsultedPedModel[1] =  A_M_Y_BEACH_01
			
			iInsultedPedLookatStage[0] = -1
			iInsultedPedLookatStage[1] = -1
			
			eInsultedObjModel[0] = PROP_CS_BEACHTOWEL_01	vInsultedObjPos[0] = <<0,0,-1>> vInsultedObjRot[0] = <<0,0,2.160>>
			eInsultedObjModel[1] = PROP_CS_BEACHTOWEL_01	vInsultedObjPos[1] = <<0,0,-1>> vInsultedObjRot[1] = <<0,0,-4.320>>
			
		BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@RUDE_AT_CAFE"
			
			tInsultAnimLoop[0] = "001218_03_TRVS_23_RUDE_AT_CAFE_IDLE_FEMALE"
			tInsultAnimOut[0] = "001218_03_TRVS_23_RUDE_AT_CAFE_EXIT_FEMALE"
			tInsultAnimPost[0] = ""
			
			//- vectors -//
			vInsulted_coordOffset[0]	= <<4.261, -2.388, 0.5000>>
			vInsulted_wanderOffset[0]	= <<10.5918, -34.5146, 0.0>>
			
			//- floats -//
			fInsulted_headOffset[0]	= 57.695
			fInsulted_wanderRadius[0] = 30.000
			
			//- enums -//
			eInsultedPedModel[0]		= A_F_Y_Hipster_03
			
			iInsultedPedLookatStage[0] = -1
			
			tInsultAnimLoop[1] = ""
			tInsultAnimOut[1] = ""
			tInsultAnimPost[1] = ""
			vInsulted_wanderOffset[1] = <<0,0,0>>
			fInsulted_wanderRadius[1] = 0
			eInsultedPedModel[1] = dummy_model_for_script
			iInsultedPedLookatStage[1] = -1
			
		BREAK
		CASE PR_SCENE_T_SCARETRAMP
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@SCARES_TRAMP"
			
			tInsultAnimLoop[0] = "trev_scares_tramp_idle_tramp"
			tInsultAnimOut[0] = "trev_scares_tramp_exit_tramp"
			tInsultAnimPost[0] = ""
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<2.0921, -1.5872, 0.0000>>
			
			vInsulted_wanderOffset[0] = <<-605.8271, -1419.5369, 10.9545>> - vPlayer_scene_t_insult_coord
			
			//- floats -//
			fInsulted_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fInsulted_wanderRadius[0] = 40.000
			
			//- enums -//
			eInsultedPedModel[0] = A_M_O_Tramp_01
			
			iInsultedPedLookatStage[0] = -1
			
			tInsultAnimLoop[1] = ""
			tInsultAnimOut[1] = ""
			tInsultAnimPost[1] = ""
			vInsulted_wanderOffset[1] = <<0,0,0>>
			fInsulted_wanderRadius[1] = 0
			eInsultedPedModel[1] = dummy_model_for_script
			iInsultedPedLookatStage[1] = -1
			
		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@THROW_FOOD"
			
			tInsultAnimLoop[0] = "LOOP_Ped"
			tInsultAnimOut[0] = "EXIT_Ped"
			tInsultAnimPost[0] = ""
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-5.0, 5.0), GET_RANDOM_FLOAT_IN_RANGE(-5.0, 5.0), 0.0000>>
			
			vInsulted_wanderOffset[0] = <<507.6885, -1451.7279, 28.2925>> - vPlayer_scene_t_insult_coord
			
			//- floats -//
			fInsulted_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fInsulted_wanderRadius[0] = 40.000
			
			//- enums -//
			eInsultedPedModel[0] = A_M_Y_STLAT_01
			
			iInsultedPedLookatStage[0] = -1
			
			tInsultAnimLoop[1] = ""
			tInsultAnimOut[1] = ""
			tInsultAnimPost[1] = ""
			vInsulted_wanderOffset[1] = <<0,0,0>>
			fInsulted_wanderRadius[1] = 0
			eInsultedPedModel[1] = dummy_model_for_script
			iInsultedPedLookatStage[1] = -1
			
		BREAK
		
		
		CASE PR_SCENE_T_UNDERPIER
		
			//- text labels -//
			tInsultAnimDict = "SWITCH@TREVOR@UNDER_PIER"
			tInsultAnimLoop[0] = "LOOP_Ped"
			tInsultAnimOut[0] = "EXIT_Ped"
			tInsultAnimPost[0] = "EXIT_LOOP_Ped"
			tInsultAnimPostDead[0] = "DEATH_Ped"
			tInsultAnimPostDeadLoop[0] = "DEATH_LOOP_Ped"
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			vInsulted_wanderOffset[0]	= <<0,0,0>>
			
			//- floats -//
			fInsulted_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fInsulted_wanderRadius[0] = 0.0
			
			//- enums -//
			eInsultedPedModel[0] = A_M_M_Business_01
			
			iInsultedPedLookatStage[0] = -1
			
			eInsultedObjModel[0] = P_TREV_ROPE_01_S
			vInsultedObjPos[0] = <<0,0,0>>
			tInsultPropLoop[0] = "LOOP_Rope"
			tInsultPropOut[0] = "EXIT_Rope"
			tInsultPropPost[0] = "EXIT_LOOP_Rope"
			tInsultPropPostDead[0] = "DEATH_Rope"
			tInsultPropPostDeadLoop[0] = "DEATH_LOOP_Rope"

			eInsultedObjModel[1] = Prop_Rolled_sock_01
			vInsultedObjPos[1] = <<0,0,0>>
			tInsultPropLoop[1] = "LOOP_Sock"
			tInsultPropOut[1] = "EXIT_Sock"
			tInsultPropPost[1] = "EXIT_LOOP_Sock"
			tInsultPropPostDead[1] = "DEATH_Sock"
			tInsultPropPostDeadLoop[1] = "DEATH_LOOP_Sock"
			
			tInsultAnimLoop[1] = ""
			tInsultAnimOut[1] = ""
			tInsultAnimPost[1] = ""
			vInsulted_wanderOffset[1] = <<0,0,0>>
			fInsulted_wanderRadius[1] = 0
			eInsultedPedModel[1] = dummy_model_for_script
			iInsultedPedLookatStage[1] = -1
		BREAK
		
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_t_insult_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_insult_scene)
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- text labels -//
			tInsultAnimDict = ""
			tInsultAnimLoop[0] = ""
			tInsultAnimOut[0] = ""
			tInsultAnimPost[0] = ""
			
			tInsultAnimLoop[1] = ""
			tInsultAnimOut[1] = ""
			tInsultAnimPost[1] = ""
			
			//- vectors -//
			vInsulted_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vInsulted_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			//- floats -//
			fInsulted_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fInsulted_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eInsultedPedModel[0] = A_M_Y_MUSCLBEAC_01
			eInsultedPedModel[1] = A_M_Y_MUSCLBEAC_01
			
			iInsultedPedLookatStage[0] = -1
			iInsultedPedLookatStage[1] = -1
			
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_T_INSULT_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_T_Insult()
	INT iSetup
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	BOOL bWaitForBuddyAssets = FALSE
	WHILE NOT bWaitForBuddyAssets
	AND (iWaitForBuddyAssets < 400)
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iLoadingAssets = 5
		#ENDIF
		
		REPEAT COUNT_OF(eInsultedPedModel) iSetup
			IF (eInsultedPedModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = (GET_MODEL_NAME_FOR_DEBUG(eInsultedPedModel[iSetup]))
				DrawLiteralSceneString(str, iCONSTANTSforinsulting+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
				REQUEST_MODEL(eInsultedPedModel[iSetup])
				IF NOT HAS_MODEL_LOADED(eInsultedPedModel[iSetup])
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eInsultedPedModel[iSetup])
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tInsultClipset[iSetup])
				REQUEST_CLIP_SET(tInsultClipset[iSetup])
				IF NOT HAS_CLIP_SET_LOADED(tInsultClipset[iSetup])
					bWaitForBuddyAssets = FALSE
				
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = (GET_MODEL_NAME_FOR_DEBUG(eInsultedPedModel[iSetup]))
					DrawLiteralSceneString(str, iCONSTANTSforinsulting+iLoadingAssets, HUD_COLOUR_REDLIGHT)
					iLoadingAssets++
					#ENDIF
					
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		REPEAT COUNT_OF(eInsultedObjModel) iSetup
			IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = (GET_MODEL_NAME_FOR_DEBUG(eInsultedObjModel[iSetup]))
				DrawLiteralSceneString(str, iCONSTANTSforinsulting+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
				REQUEST_MODEL(eInsultedObjModel[iSetup])
				IF NOT HAS_MODEL_LOADED(eInsultedObjModel[iSetup])
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eInsultedObjModel[iSetup])
				ENDIF
			ENDIF
		ENDREPEAT
		
		REQUEST_ANIM_DICT(tInsultAnimDict)
		IF NOT HAS_ANIM_DICT_LOADED(tInsultAnimDict)
			bWaitForBuddyAssets = FALSE
			REQUEST_ANIM_DICT(tInsultAnimDict)
		ENDIF
		
		IF NOT bWaitForBuddyAssets
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	
	//- create any script vehicles -//
	//- create any script peds -//
	REPEAT COUNT_OF(insulted_ped) iSetup
		IF (eInsultedPedModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
			insulted_ped[iSetup] = CREATE_PED(PEDTYPE_MISSION, eInsultedPedModel[iSetup], vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iSetup])
			SET_ENTITY_HEADING(insulted_ped[iSetup], fPlayer_scene_t_insult_head+fInsulted_headOffset[iSetup])
			SET_PED_RANDOM_COMPONENT_VARIATION(insulted_ped[iSetup])
			
			IF (eInsultedPedModel[iSetup] = A_F_Y_BEACH_01)
				IF (selected_player_scene_t_insult_scene = PR_SCENE_T_UNDERPIER)
					SET_PED_COMPONENT_VARIATION(insulted_ped[iSetup], PED_COMP_SPECIAL, 1, 0)
				ENDIF
			ENDIF
			
			SET_PED_COMBAT_ATTRIBUTES(insulted_ped[iSetup], CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(insulted_ped[iSetup], CA_ALWAYS_FLEE, TRUE)
			
			TASK_PLAY_ANIM(insulted_ped[iSetup], tInsultAnimDict, tInsultAnimLoop[iSetup],
					NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
					AF_LOOPING, 0)
		ENDIF
	ENDREPEAT
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	REPEAT COUNT_OF(insulted_obj) iSetup
		IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
			insulted_obj[iSetup] = CREATE_OBJECT(eInsultedObjModel[iSetup],
					vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iSetup]+vInsultedObjPos[iSetup])
			SET_ENTITY_ROTATION(insulted_obj[iSetup],
					<<0,0,fPlayer_scene_t_insult_head+fInsulted_headOffset[iSetup]>>+vInsultedObjRot[iSetup])
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropLoop[iSetup])
				PLAY_ENTITY_ANIM(insulted_obj[iSetup], tInsultPropLoop[iSetup], tInsultAnimDict,
						NORMAL_BLEND_IN, TRUE, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_T_Insult_Stage(PS_T_INSULT_STAGE_ENUM this_player_scene_t_insult_stage)
	SWITCH this_player_scene_t_insult_stage
		CASE PS_T_INSULT_switchInProgress
			RETURN "PS_T_INSULT_switchInProgress"
		BREAK
		CASE PS_T_INSULT_STAGE_TWO
			RETURN "PS_T_INSULT_STAGE_TWO"
		BREAK
		CASE PS_T_INSULT_STAGE_THREE
			RETURN "PS_T_INSULT_STAGE_THREE"
		BREAK
		
		CASE FINISHED_T_INSULT
			RETURN "FINISHED_T_INSULT"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_T_Insult_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_T_Insult_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_t_insult.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_insult_scene)
	
	player_scene_t_insult_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_T_INSULT_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_T_Insult_Stage(INT_TO_ENUM(PS_T_INSULT_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_t_insult_stage", iCurrent_player_scene_t_insult_stage)
		
		START_WIDGET_GROUP("Do_PS_T_INSULT_STAGE_TWO")
			ADD_WIDGET_VECTOR_SLIDER("vPlayer_scene_t_insult_coord", vPlayer_scene_t_insult_coord, -3000.0, 3000.0, 0.0)
			ADD_WIDGET_FLOAT_SLIDER("fPlayer_scene_t_insult_head", fPlayer_scene_t_insult_head, -360.0, 360.0, 0.0)
			
			REPEAT COUNT_OF(insulted_ped) iWidget
				IF (eInsultedPedModel[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
					str = "insulted_ped "
					str += iWidget
					ADD_WIDGET_STRING(str)
					ADD_WIDGET_VECTOR_SLIDER("vInsulted_coordOffset", vInsulted_coordOffset[iWidget], -30.0, 30.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fInsulted_headOffset", fInsulted_headOffset[iWidget], -180.0, 180.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("vInsulted_wanderOffset", vInsulted_wanderOffset[iWidget], -200.0, 200.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fInsulted_wanderRadius", fInsulted_wanderRadius[iWidget], 0.0, 50.0, 1.0)
				ENDIF
				
				IF NOT ARE_VECTORS_EQUAL(vInsultedObjPos[iWidget], <<0,0,0>>)
				OR NOT ARE_VECTORS_EQUAL(vInsultedObjRot[iWidget], <<0,0,0>>)
				
					str = "insulted_obj "
					str += iWidget
					ADD_WIDGET_STRING(str)
					ADD_WIDGET_VECTOR_SLIDER("vInsultedObjPos", vInsultedObjPos[iWidget], -30.0, 30.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vInsultedObjRot", vInsultedObjRot[iWidget], -180.0, 180.0, 1.0)
				ENDIF
			ENDREPEAT
			
			ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
			ADD_WIDGET_BOOL("bSavePeds", bSavePeds)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_Insult_Widget()
	INT iWidget
	iCurrent_player_scene_t_insult_stage = ENUM_TO_INT(current_player_scene_t_insult_stage)
	
	IF NOT bMovePeds
	ELSE
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		
			REPEAT COUNT_OF(insulted_ped) iWidget
				IF NOT IS_PED_INJURED(insulted_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(insulted_ped[iWidget], tInsultAnimDict, tInsultAnimLoop[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(insulted_ped[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
								tInsultAnimDict, tInsultAnimLoop[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
						iInsultedStage[iWidget] = 0
					ENDIF
					
					vInsulted_coordOffset[iWidget] = GET_ENTITY_COORDS(insulted_ped[iWidget]) - vPlayer_scene_t_insult_coord
					fInsulted_headOffset[iWidget] = GET_ENTITY_HEADING(insulted_ped[iWidget]) - fPlayer_scene_t_insult_head

					IF fInsulted_headOffset[iWidget] > 180.0
						fInsulted_headOffset[iWidget] -= 360.0
					ENDIF
					IF fInsulted_headOffset[iWidget] < -180.0
						fInsulted_headOffset[iWidget] += 360.0
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(insulted_ped[iWidget])
						CPRINTLN(DEBUG_SWITCH, "insulted_ped ", iWidget, " injured??")
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(insulted_obj) iWidget
				IF DOES_ENTITY_EXIST(insulted_obj[iWidget])
					IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPostDeadLoop[iWidget])
						IF NOT IS_ENTITY_PLAYING_ANIM(insulted_obj[iWidget], tInsultAnimDict, tInsultPropLoop[iWidget], ANIM_SYNCED_SCENE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
									tInsultPropLoop[iWidget], tInsultAnimDict,
									NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						ENDIF
					ELSE
						SET_ENTITY_COORDS(insulted_obj[iWidget], vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iWidget]+vInsultedObjPos[iWidget])
						SET_ENTITY_ROTATION(insulted_obj[iWidget], <<0,0,fPlayer_scene_t_insult_head+fInsulted_headOffset[iWidget]>>+vInsultedObjRot[iWidget])
					ENDIF
				ENDIF
			ENDREPEAT
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		
			REPEAT COUNT_OF(insulted_ped) iWidget
				IF NOT IS_PED_INJURED(insulted_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(insulted_ped[iWidget], tInsultAnimDict, tInsultAnimOut[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(insulted_ped[iWidget], g_iPlayer_Timetable_Exit_SynchSceneID,
								tInsultAnimDict, tInsultAnimOut[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(insulted_ped[iWidget])
						CPRINTLN(DEBUG_SWITCH, "insulted_ped ", iWidget, " injured??")
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(insulted_obj) iWidget
				IF DOES_ENTITY_EXIST(insulted_obj[iWidget])
					IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPostDeadLoop[iWidget])
						IF NOT IS_ENTITY_PLAYING_ANIM(insulted_obj[iWidget], tInsultAnimDict, tInsultPropOut[iWidget], ANIM_SYNCED_SCENE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iWidget], g_iPlayer_Timetable_Exit_SynchSceneID,
									tInsultPropOut[iWidget], tInsultAnimDict,
									NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						ENDIF
					ELSE
						SET_ENTITY_COORDS(insulted_obj[iWidget], vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iWidget]+vInsultedObjPos[iWidget])
						SET_ENTITY_ROTATION(insulted_obj[iWidget], <<0,0,fPlayer_scene_t_insult_head+fInsulted_headOffset[iWidget]>>+vInsultedObjRot[iWidget])
					ENDIF
				ENDIF
			ENDREPEAT
			
		ELSE
		
			REPEAT COUNT_OF(insulted_ped) iWidget
				IF NOT IS_PED_INJURED(insulted_ped[iWidget])
	//				CLEAR_PED_TASKS(insulted_ped[iWidget])
					
					SET_ENTITY_HEADING(insulted_ped[iWidget], fPlayer_scene_t_insult_head+fInsulted_headOffset[iWidget])
					SET_ENTITY_COORDS(insulted_ped[iWidget], vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iWidget])
				ENDIF
			ENDREPEAT
			
		ENDIF
		
	ENDIF
	
	
	IF bSavePeds
		
		SAVE_STRING_TO_DEBUG_FILE("	\"player_scene_t_insult.sc\" - bSavePeds")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(vInsulted_coordOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vInsulted_coordOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vInsulted_coordOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_t_insult_coord+vInsulted_coordOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - vPlayer_scene_t_insult_coord")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT COUNT_OF(vInsulted_wanderOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vInsulted_wanderOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vInsulted_wanderOffset[iWidget])
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT COUNT_OF(vInsulted_wanderOffset) iWidget
			IF NOT ARE_VECTORS_EQUAL(vInsultedObjPos[iWidget], <<0,0,0>>)
			OR NOT ARE_VECTORS_EQUAL(vInsultedObjRot[iWidget], <<0,0,0>>)
			
				SAVE_STRING_TO_DEBUG_FILE("	vInsultedObjPos[")
				SAVE_INT_TO_DEBUG_FILE(iWidget)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vInsultedObjPos[iWidget])
				SAVE_STRING_TO_DEBUG_FILE("	vInsultedObjRot[")
				SAVE_INT_TO_DEBUG_FILE(iWidget)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vInsultedObjRot[iWidget])
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		
		SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(fInsulted_headOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fInsulted_headOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fInsulted_headOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_t_insult_head+fInsulted_headOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - fPlayer_scene_t_insult_head")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT COUNT_OF(fInsulted_wanderRadius) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fInsulted_wanderRadius[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fInsulted_wanderRadius[iWidget])
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bSavePeds = FALSE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_T_Insult_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_t_insult_stage = FINISHED_T_INSULT
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************


FUNC BOOL HasPlayerScaredInsulted(PED_INDEX insultedPed)
	
	#IF IS_DEBUG_BUILD
	IF bMovePeds
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF eInsultedObjModel[0] = P_TREV_ROPE_01_S
	OR eInsultedObjModel[1] = P_TREV_ROPE_01_S
		RETURN FALSE
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(insultedPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), insultedPed)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_COMBAT(insultedPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(insulted_ped) iPed
		IF (insultedPed <> insulted_ped[iPed])
			IF (iInsultedStage[iPed] < 0)			//is dead
			AND DOES_ENTITY_EXIST(insulted_ped[iPed])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
//	INT iZone
//	REPEAT iOFF_LIMIT_COUNT iZone
//		IF NOT ARE_VECTORS_EQUAL(vOffLimitBounds[iZone], <<0,0,0>>)
//			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
//					vPlayer_scene_t_fight_coord+vOffLimitOffset[iZone],
//					vOffLimitBounds[iZone],
//					fPlayer_scene_t_fight_head+fOffLimitAngle[iZone])
//				
//				FLOAT Degrees = 50.0
//				IF IS_PED_FACING_PED(insultedPed, PLAYER_PED_ID(), Degrees)
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC BOOL Fake_Anim_Event_Phase(STRING pAnimDictName, STRING pAnimName, STRING pEventName, FLOAT& ReturnStartPhase, FLOAT& ReturnEndPhase )
	#IF IS_DEBUG_BUILD
	IF FIND_ANIM_EVENT_PHASE(pAnimDictName, pAnimName, pEventName, ReturnStartPhase, ReturnEndPhase)
		SCRIPT_ASSERT("FIND_ANIM_EVENT_PHASE exists!!!")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF ARE_STRINGS_EQUAL(pEventName, "WalkInterruptible")
		IF ARE_STRINGS_EQUAL(pAnimDictName, "SWITCH@TREVOR@SCARES_TRAMP")
			IF ARE_STRINGS_EQUAL(pAnimName, "trev_scares_tramp_exit_tramp")
				ReturnStartPhase	= 0.8
				ReturnEndPhase		= 1.0
				
				RETURN TRUE
			ENDIF
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "SWITCH@TREVOR@CHASE_STRIPPERS")
			IF ARE_STRINGS_EQUAL(pAnimName, "EXIT_STRIPPER_01")
			OR ARE_STRINGS_EQUAL(pAnimName, "EXIT_STRIPPER_02")
				ReturnStartPhase	= 0.4
				ReturnEndPhase		= 1.0
				
				RETURN TRUE
			ENDIF
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "SWITCH@TREVOR@THROW_FOOD")
			IF ARE_STRINGS_EQUAL(pAnimName, "EXIT_Ped")
				ReturnStartPhase	= 0.7
				ReturnEndPhase		= 1.0
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_T_INSULT_switchInProgress()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
//	CPRINTLN(DEBUG_SWITCH, "<switchanim> ")
	IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerSceneAnimDict)
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
			DrawLiteralSceneString("loop", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			bPlayerPlayingOutAnim = TRUE
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
			IF bPlayerPlayingOutAnim
				DrawLiteralSceneString("loop, out", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			ELSE
				DrawLiteralSceneString("out", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			ENDIF
			
			bPlayerPlayingOutAnim = TRUE
		ENDIF
	ENDIF
	
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
	ENDIF
	#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("IS_SYNCHRONIZED_SCENE_RUNNING", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		#ENDIF
		
		INT iPed
		REPEAT COUNT_OF(insulted_ped) iPed
			IF NOT IS_PED_INJURED(insulted_ped[iPed])
				TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
						tInsultAnimDict, tInsultAnimLoop[iPed],
						NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(insulted_obj) iPed
			IF DOES_ENTITY_EXIST(insulted_obj[iPed])
				IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropLoop[iPed])
					PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
							tInsultPropLoop[iPed], tInsultAnimDict,
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
					CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM(", GET_MODEL_NAME_FOR_DEBUG(eInsultedObjModel[iPed]), ", ", g_iPlayer_Timetable_Loop_SynchSceneID, ", ", tInsultPropLoop[iPed], ", ", tInsultAnimDict, ")	//Loop")
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tTaunterSceneSpeechLabel)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
		//	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, g_pScene_buddy, "LAMAR")
			
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 7, insulted_ped[0], "STRIPPER1")
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 8, insulted_ped[1], "STRIPPER2")
		ENDIF
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_T_INSULT_STAGE_TWO()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	CPRINTLN(DEBUG_SWITCH, "<switchanim> ")
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
	ENDIF
	#ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			INT iPed
			REPEAT COUNT_OF(insulted_ped) iPed
				IF NOT IS_PED_INJURED(insulted_ped[iPed])
					
					FLOAT blendOutDelta = INSTANT_BLEND_OUT
					SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_NONE
					RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_NONE
					FLOAT moverBlendInDelta = INSTANT_BLEND_IN
					IK_CONTROL_FLAGS ikFlags = AIK_NONE
					
					FLOAT ReturnStartPhase = -1, ReturnEndPhase = -1
					IF FIND_ANIM_EVENT_PHASE(tInsultAnimDict, tInsultAnimOut[iPed], "WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
					OR Fake_ANIM_EVENT_PHASE(tInsultAnimDict, tInsultAnimOut[iPed], "WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
						blendOutDelta = NORMAL_BLEND_OUT
						flags = SYNCED_SCENE_TAG_SYNC_OUT
						ragdollFlags = RBF_NONE
						moverBlendInDelta = NORMAL_BLEND_IN
						ikFlags = AIK_NONE
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
							tInsultAnimDict, tInsultAnimOut[iPed],
							INSTANT_BLEND_IN, blendOutDelta,
							flags, ragdollFlags, moverBlendInDelta, ikFlags)
					iInsultedStage[iPed] = 0
				ELSE
					//
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(insulted_obj) iPed
				IF DOES_ENTITY_EXIST(insulted_obj[iPed])
					IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropOut[iPed])
						PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
								tInsultPropOut[iPed], tInsultAnimDict,
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
										
								
						CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM(", GET_MODEL_NAME_FOR_DEBUG(eInsultedObjModel[iPed]), ", ", g_iPlayer_Timetable_Exit_SynchSceneID, ", ", tInsultPropOut[iPed], ", ", tInsultAnimDict, ")	//Out")
					ENDIF
				ELSE
					//
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("IS_SYNCHRONIZED_SCENE_RUNNING", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			#ENDIF
			
			TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			#ENDIF
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("switch Spline Cam In Progress", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		#ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
BOOL bGeneraredInsultedWander = FALSE
#ENDIF


FUNC BOOL HasPlayerScaredPed(PED_INDEX thisInsultedPed)
	
	#IF IS_DEBUG_BUILD
	IF bMovePeds
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisInsultedPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisInsultedPed)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_COMBAT(thisInsultedPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(insulted_ped) iPed
		IF (thisInsultedPed <> insulted_ped[iPed])
			IF (iInsultedStage[iPed] < 0)			//is dead
			AND DOES_ENTITY_EXIST(insulted_ped[iPed])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (selected_player_scene_t_insult_scene = PR_SCENE_T_SC_CHASE)
		
		FLOAT fChasedStripperDist2	= VDIST2(GET_ENTITY_COORDS(thisInsultedPed, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		CONST_FLOAT fCHASED_STRIPPER_TOO_CLOSE	10.0
		
		IF (fChasedStripperDist2 < (fCHASED_STRIPPER_TOO_CLOSE*fCHASED_STRIPPER_TOO_CLOSE))
			
			CPRINTLN(DEBUG_SWITCH, "chased strippers flee")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ControlInsultPedStage(PED_INDEX thisInsultedPed, INT iPed)
	CONST_INT	iCONST_INSULT_0_outAnim					0
	CONST_INT	iCONST_INSULT_1_navmesh					1
	CONST_INT	iCONST_INSULT_2_wander					2
	
	CONST_INT	iCONST_INSULT_3_fleeing					3
	CONST_INT	iCONST_INSULT_4_flee					4
	
	CONST_INT	iCONST_INSULT_10_postSynchScene			10
	
	CONST_INT	iCONST_INSULT_11_postSynchDead			11
	CONST_INT	iCONST_INSULT_12_postSynchDeadLoop		12
	
	CONST_INT	iCONST_INSULT_DEAD						-1
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	IF NOT IS_PED_INJURED(thisInsultedPed)
		SWITCH iInsultedStage[iPed]
			CASE iCONST_INSULT_0_outAnim	//
				
				IF IS_ENTITY_PLAYING_ANIM(thisInsultedPed, tInsultAnimDict, tInsultAnimOut[iPed], ANIM_SYNCED_SCENE)
					
					FLOAT ReturnStartPhase, ReturnEndPhase
					ReturnStartPhase = -1
					ReturnEndPhase = -1
					IF FIND_ANIM_EVENT_PHASE(tInsultAnimDict, tInsultAnimOut[iPed],
							"WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
					OR Fake_ANIM_EVENT_PHASE(tInsultAnimDict, tInsultAnimOut[iPed],
							"WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
						FLOAT AnimCurrentTime
						AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
						
						IF (AnimCurrentTime > ReturnStartPhase)
//							CLEAR_PED_TASKS(thisInsultedPed)
//							FORCE_PED_MOTION_STATE(thisInsultedPed, MS_ON_FOOT_WALK)
							
							CLEAR_PED_TASKS(thisInsultedPed)
							STOP_SYNCHRONIZED_ENTITY_ANIM(thisInsultedPed, NORMAL_BLEND_OUT, TRUE)
							
							SET_PED_MOVE_ANIMS_BLEND_OUT(thisInsultedPed)
							
							FLOAT PEDMOVE_INTERRUPT
							PED_MOTION_STATE MS_ON_FOOT_INTERRUPT
							PEDMOVE_INTERRUPT = PEDMOVE_WALK
							MS_ON_FOOT_INTERRUPT = MS_ON_FOOT_WALK
							
							IF (selected_player_scene_t_insult_scene = PR_SCENE_T_SCARETRAMP)
								PEDMOVE_INTERRUPT = PEDMOVE_RUN
								MS_ON_FOOT_INTERRUPT = MS_ON_FOOT_RUN
								
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(thisInsultedPed, "GENERIC_FRIGHTENED_HIGH", "A_M_O_TRAMP_01_BLACK_FULL_01")
							ENDIF
							IF (selected_player_scene_t_insult_scene = PR_SCENE_T_THROW_FOOD)
								PEDMOVE_INTERRUPT = PEDMOVE_RUN
								MS_ON_FOOT_INTERRUPT = MS_ON_FOOT_RUN
							ENDIF
							
							TASK_FOLLOW_NAV_MESH_TO_COORD(thisInsultedPed,
									vPlayer_scene_T_INSULT_coord+vInsulted_wanderOffset[iPed],
									PEDMOVE_INTERRUPT, DEFAULT_TIME_NEVER_WARP, fInsulted_wanderRadius[iPed] * 0.1)
							FORCE_PED_MOTION_STATE(thisInsultedPed, MS_ON_FOOT_INTERRUPT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(thisInsultedPed)
							
							iInsultedStage[iPed] = iCONST_INSULT_1_navmesh
							EXIT
						ENDIF
						
						#IF IS_DEBUG_BUILD
						str  = "walkInt \""
						
						INT iTaunterAnimOutLength
						iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tInsultAnimOut[iPed])
						
						IF iTaunterAnimOutLength >= 10
							str += GET_STRING_FROM_STRING(tInsultAnimOut[iPed],
									iTaunterAnimOutLength - 10,
									iTaunterAnimOutLength)
						ELSE
							str += tInsultAnimOut[iPed]
						ENDIF
						str += "\" "
						str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
						
						DrawLiteralSceneString(str,
								iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUEDARK, fInsultAlphaMult)
						#ENDIF
						
					ELSE
						
						#IF IS_DEBUG_BUILD
						str  = "outAnim \""
						
						INT iTaunterAnimOutLength
						iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tInsultAnimOut[iPed])
						
						IF iTaunterAnimOutLength >= 10
							str += GET_STRING_FROM_STRING(tInsultAnimOut[iPed],
									iTaunterAnimOutLength - 10,
									iTaunterAnimOutLength)
						ELSE
							str += tInsultAnimOut[iPed]
						ENDIF
						str += "\""
						
						DrawLiteralSceneString(str,
								iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
						#ENDIF
						
					ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  NOT playing out anim", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
					#ENDIF
					
					CLEAR_PED_TASKS(thisInsultedPed)
					
					
					IF IS_STRING_NULL_OR_EMPTY(tInsultAnimPost[iPed])
						
						IF ARE_VECTORS_EQUAL(vInsulted_wanderOffset[iPed], <<0,0,0>>)
							VECTOR vinsulted_ped_coord
							vinsulted_ped_coord = GET_ENTITY_COORDS(thisInsultedPed)
							vInsulted_wanderOffset[iPed] = (vinsulted_ped_coord - vPlayer_scene_T_INSULT_coord) * 4.0
							
							#IF IS_DEBUG_BUILD
							
							IF NOT bGeneraredInsultedWander
								
								SCRIPT_ASSERT("missing Insulted wander")
								
								SAVE_STRING_TO_DEBUG_FILE("missing Insulted wander for \"")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_insult_scene))
								SAVE_STRING_TO_DEBUG_FILE("\"")
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								bGeneraredInsultedWander = TRUE
							ENDIF
							
							SAVE_STRING_TO_DEBUG_FILE("vInsulted_wanderOffset[")
							SAVE_INT_TO_DEBUG_FILE(iPed)
							SAVE_STRING_TO_DEBUG_FILE("] = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vInsulted_wanderOffset[iPed])
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
						ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(tInsultClipset[iPed])
							SET_PED_MOVEMENT_CLIPSET(thisInsultedPed, tInsultClipset[iPed])
						ENDIF
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(thisInsultedPed,
								 vPlayer_scene_T_INSULT_coord+vInsulted_wanderOffset[iPed],
								 PEDMOVE_WALK, -1)
						iInsultedStage[iPed] = iCONST_INSULT_1_navmesh
					ELSE
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
							iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_t_insult_coord, <<0,0,fPlayer_scene_t_insult_head>>)
							SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
						ENDIF
						
						SYNCED_SCENE_PLAYBACK_FLAGS sspFlags
						RAGDOLL_BLOCKING_FLAGS ragdollFlags
						IK_CONTROL_FLAGS ikFlags
						sspFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
						ragdollFlags = RBF_PLAYER_IMPACT | RBF_MELEE
						ikFlags = AIK_NONE
						
						INT iSetup
						
						IF (eInsultedObjModel[0] = P_TREV_ROPE_01_S)	//1129377
						OR (eInsultedObjModel[1] = P_TREV_ROPE_01_S)
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sspFlags, SYNCED_SCENE_DONT_INTERRUPT)	SET_BITMASK_ENUM_AS_ENUM(sspFlags, SYNCED_SCENE_DONT_INTERRUPT) ENDIF
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sspFlags, SYNCED_SCENE_USE_PHYSICS)		SET_BITMASK_ENUM_AS_ENUM(sspFlags, SYNCED_SCENE_USE_PHYSICS) ENDIF
							
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, RBF_PLAYER_IMPACT)			SET_BITMASK_ENUM_AS_ENUM(ragdollFlags, RBF_PLAYER_IMPACT) ENDIF
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, RBF_MELEE)					SET_BITMASK_ENUM_AS_ENUM(ragdollFlags, RBF_MELEE) ENDIF
							
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ikFlags, AIK_NONE)						SET_BITMASK_ENUM_AS_ENUM(ikFlags, AIK_NONE) ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(insulted_ped[iPed], TRUE)
							SET_PED_CAN_BE_TARGETTED(insulted_ped[iPed], FALSE)
							
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(insulted_ped[iPed], FALSE)
							
							SET_ENTITY_MAX_HEALTH(insulted_ped[iPed], 1000)
							SET_ENTITY_HEALTH(insulted_ped[iPed], GET_ENTITY_MAX_HEALTH(insulted_ped[iPed]))
							
							SET_PED_SUFFERS_CRITICAL_HITS(insulted_ped[iPed], FALSE)
							SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_BlockWeaponReactionsUnlessDead, TRUE)
							SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisablePotentialBlastReactions, TRUE)
							SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisableMeleeHitReactions, TRUE)
							
//							SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(insulted_ped[iPed], FALSE, RELGROUPHASH_PLAYER)
							
							REPEAT COUNT_OF(insulted_obj) iSetup
								SET_ENTITY_CAN_BE_DAMAGED(insulted_obj[iSetup], FALSE)
								SET_OBJECT_TARGETTABLE(insulted_obj[iSetup], FALSE)
								
//								SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(insulted_obj[iSetup], FALSE, RELGROUPHASH_PLAYER)
							ENDREPEAT
						ENDIF
						
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, RBF_PLAYER_IMPACT)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(insulted_ped[iPed], FALSE)
						ENDIF
						
						TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], iInsult_AnimPost_SynchSceneID,
								tInsultAnimDict, tInsultAnimPost[iPed],
								INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
								sspFlags, ragdollFlags, INSTANT_BLEND_IN, ikFlags)
						
						REPEAT COUNT_OF(insulted_obj) iSetup
							IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
								IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPost[iSetup])
									PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iSetup], iInsult_AnimPost_SynchSceneID,
											tInsultPropPost[iSetup], tInsultAnimDict,
											NORMAL_BLEND_IN)
								ENDIF
							ENDIF
						ENDREPEAT
								
						vInsulted_wanderOffset[iPed] = vInsulted_coordOffset[iPed]
						iInsultedStage[iPed] = iCONST_INSULT_10_postSynchScene
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_INSULT_1_navmesh
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  navmesh...", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisInsultedPed),
						vPlayer_scene_T_INSULT_coord + vInsulted_wanderOffset[iPed],
						HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
				
				IF NOT IS_PED_GROUP_MEMBER(insulted_ped[iPed], insultedGroup)
					IF NOT DOES_GROUP_EXIST(insultedGroup)
						insultedGroup = CREATE_GROUP(DEFAULT_TASK_ALLOCATOR_FOLLOW_ANY_MEANS)
					ENDIF
					
					IF (iPed = 0)
						SET_PED_AS_GROUP_LEADER(insulted_ped[iPed], insultedGroup)
					ELSE
						SET_PED_AS_GROUP_MEMBER(insulted_ped[iPed], insultedGroup)
					ENDIF
				ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(thisInsultedPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
					
					IF (fInsulted_wanderRadius[iPed] = 0)
						fInsulted_wanderRadius[iPed] = VMAG(vInsulted_wanderOffset[iPed])*0.75
						
						#IF IS_DEBUG_BUILD
						
						IF NOT bGeneraredInsultedWander
							
							SCRIPT_ASSERT("missing Insulted wander")
							
							SAVE_STRING_TO_DEBUG_FILE("missing Insulted wander for \"")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_insult_scene))
							SAVE_STRING_TO_DEBUG_FILE("\"")
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							bGeneraredInsultedWander = TRUE
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE("fInsulted_wanderRadius[")
						SAVE_INT_TO_DEBUG_FILE(iPed)
						SAVE_STRING_TO_DEBUG_FILE("] = ")
						SAVE_FLOAT_TO_DEBUG_FILE(fInsulted_wanderRadius[iPed])
						SAVE_NEWLINE_TO_DEBUG_FILE()
						#ENDIF
					ENDIF
					
					//TASK_WANDER_STANDARD(thisInsultedPed)
					TASK_WANDER_IN_AREA(thisInsultedPed,
							vPlayer_scene_T_INSULT_coord + vInsulted_wanderOffset[iPed],
							fInsulted_wanderRadius[iPed])
					iInsultedStage[iPed] = iCONST_INSULT_2_wander
					BREAK
				ENDIF
				
				IF HasPlayerScaredPed(thisInsultedPed)
					iInsultedStage[iPed] = iCONST_INSULT_3_fleeing
					BREAK
				ENDIF
			BREAK
			CASE iCONST_INSULT_2_wander
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  wander...", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				DrawDebugSceneSphere(vPlayer_scene_T_INSULT_coord + vInsulted_wanderOffset[iPed],
						fInsulted_wanderRadius[iPed],
						HUD_COLOUR_BLUELIGHT, fInsultAlphaMult*0.5)
				#ENDIF
				
				IF HasPlayerScaredPed(thisInsultedPed)
					iInsultedStage[iPed] = iCONST_INSULT_3_fleeing
					BREAK
				ENDIF
				
			BREAK
			
			CASE iCONST_INSULT_3_fleeing
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  fleeing...", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
					
				INT iPed2
				REPEAT COUNT_OF(iInsultedStage) iPed2
					IF (iPed <> iPed2)
						IF (iInsultedStage[iPed2] <> iCONST_INSULT_3_fleeing)	//not already fleeing
						AND (iInsultedStage[iPed2] <> iCONST_INSULT_4_flee)		//not already fleeing
							iInsultedStage[iPed2] = iCONST_INSULT_3_fleeing
						ENDIF
					ENDIF
				ENDREPEAT
				
				TASK_SMART_FLEE_PED(thisInsultedPed, PLAYER_PED_ID(), 100.0, -1)
				SET_PED_KEEP_TASK(thisInsultedPed, TRUE)
				iInsultedStage[iPed] = iCONST_INSULT_4_flee
			BREAK
			CASE iCONST_INSULT_4_flee
				#IF IS_DEBUG_BUILD
				str  = "  flee["
				str += GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(thisInsultedPed), GET_ENTITY_COORDS(PLAYER_PED_ID())))
				str += "]"
				DrawLiteralSceneString(str, iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(thisInsultedPed, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
					iInsultedStage[iPed] = iCONST_INSULT_1_navmesh
				ENDIF
			BREAK
			
			CASE iCONST_INSULT_10_postSynchScene
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  postSynchScene...", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
					
				IF HasPlayerScaredInsulted(thisInsultedPed)
					iInsultedStage[iPed] = iCONST_INSULT_1_navmesh
					BREAK
				ENDIF
				
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_BlockWeaponReactionsUnlessDead, TRUE)
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisablePotentialBlastReactions, TRUE)
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisableMeleeHitReactions, TRUE)
				
				IF IS_ENTITY_PLAYING_ANIM(insulted_ped[iPed], tInsultAnimDict, tInsultAnimPost[iPed], ANIM_SYNCED_SCENE)
					
					IF (selected_player_scene_t_insult_scene = PR_SCENE_T_UNDERPIER)
						IF iStrugGameTime <= 0
							iStrugGameTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
						ENDIF
						
						IF IS_ANY_SPEECH_PLAYING(thisInsultedPed)
							iStrugGameTime = -1
						ENDIF
						
						
						IF iStrugGameTime > 0
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneStringInt("  iStrugGameTime: ", iStrugGameTime-GET_GAME_TIMER(), iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
							#ENDIF

							IF GET_GAME_TIMER() > iStrugGameTime
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, insulted_ped[iPed], "PIERWOMAN")
								IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", "TRVS_STRUG", CONV_PRIORITY_AMBIENT_MEDIUM)
									iStrugGameTime = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					SYNCED_SCENE_PLAYBACK_FLAGS sspFlags
					RAGDOLL_BLOCKING_FLAGS ragdollFlags
					IK_CONTROL_FLAGS ikFlags
					sspFlags = SYNCED_SCENE_NONE
					ragdollFlags = RBF_NONE
					ikFlags = AIK_NONE
					
					IF (eInsultedObjModel[0] = P_TREV_ROPE_01_S)	//1129377
					OR (eInsultedObjModel[1] = P_TREV_ROPE_01_S)
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sspFlags, SYNCED_SCENE_DONT_INTERRUPT)	SET_BITMASK_ENUM_AS_ENUM(sspFlags, SYNCED_SCENE_DONT_INTERRUPT) ENDIF
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sspFlags, SYNCED_SCENE_USE_PHYSICS)		SET_BITMASK_ENUM_AS_ENUM(sspFlags, SYNCED_SCENE_USE_PHYSICS) ENDIF
						
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, RBF_PLAYER_IMPACT)			SET_BITMASK_ENUM_AS_ENUM(ragdollFlags, RBF_PLAYER_IMPACT) ENDIF
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, RBF_MELEE)					SET_BITMASK_ENUM_AS_ENUM(ragdollFlags, RBF_MELEE) ENDIF
						
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(ikFlags, AIK_NONE)						SET_BITMASK_ENUM_AS_ENUM(ikFlags, AIK_NONE) ENDIF
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(insulted_ped[iPed], TRUE)
						SET_PED_CAN_BE_TARGETTED(insulted_ped[iPed], FALSE)
						
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(insulted_ped[iPed], FALSE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], iInsult_AnimPost_SynchSceneID,
							tInsultAnimDict, tInsultAnimPost[iPed],
							INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
							sspFlags, ragdollFlags, INSTANT_BLEND_IN, ikFlags)
					
					INT iSetup
					REPEAT COUNT_OF(insulted_obj) iSetup
						IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
							IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPost[iSetup])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iSetup], iInsult_AnimPost_SynchSceneID,
										tInsultPropPost[iSetup], tInsultAnimDict,
										NORMAL_BLEND_IN)
										
										
								CPRINTLN(DEBUG_SWITCH, "PLAY_SYNCHRONIZED_ENTITY_ANIM(", GET_MODEL_NAME_FOR_DEBUG(eInsultedObjModel[iSetup]), ", ", iInsult_AnimPost_SynchSceneID, ", ", tInsultPropPost[iSetup], ", ", tInsultAnimDict, ")	//Post")
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				SWITCH iInsultedPedLookatStage[iPed]
					CASE -1
						#IF IS_DEBUG_BUILD
						DrawDebugSceneTextWithOffset("lookat -1", GET_ENTITY_COORDS(insulted_ped[iPed], FALSE), 0, HUD_COLOUR_BLACK, 0.25)
						#ENDIF
					BREAK
					CASE 0
						#IF IS_DEBUG_BUILD
						DrawDebugSceneTextWithOffset("lookat 0", GET_ENTITY_COORDS(insulted_ped[iPed], FALSE), 0, HUD_COLOUR_RED, 1.0)
						#ENDIF
						
						INT iRandom
						iRandom = GET_RANDOM_INT_IN_RANGE(1500, 4000)
						
						TASK_LOOK_AT_ENTITY(insulted_ped[iPed], PLAYER_PED_ID(), iRandom)
						iInsultedPedLookatGameTime[iPed] = GET_GAME_TIMER() + iRandom + GET_RANDOM_INT_IN_RANGE(0500, 2000)
						
						iInsultedPedLookatStage[iPed] = 1
						
					BREAK
					CASE 1
						#IF IS_DEBUG_BUILD
						str  = "lookat ["
						str += (iInsultedPedLookatGameTime[iPed] - GET_GAME_TIMER())
						str += "]"
						DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(insulted_ped[iPed], FALSE), 0, HUD_COLOUR_GREEN, 1.0)
						#ENDIF
						
//						IF GET_SCRIPT_TASK_STATUS(insulted_ped[iPed], SCRIPT_TASK_LOOK_AT_ENTITY) = FINISHED_TASK
//							iInsultedPedLookatStage[iPed] = 0
//						ENDIF
						
						IF (GET_GAME_TIMER() > iInsultedPedLookatGameTime[iPed])
							iInsultedPedLookatGameTime[iPed] = 0
							iInsultedPedLookatStage[iPed] = 0
						ENDIF
					BREAK
				ENDSWITCH
				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(insulted_ped[iPed], PLAYER_PED_ID())
					CPRINTLN(DEBUG_SWITCH, "has_entity_been_damaged_by_entity")
				ELSE
					CPRINTLN(DEBUG_SWITCH, "NOT has_entity_been_damaged_by_entity")
				ENDIF
				
				BOOL bTriggerPostSynchDead
				bTriggerPostSynchDead = FALSE
				
				IF (GET_ENTITY_HEALTH(insulted_ped[iPed]) < GET_ENTITY_MAX_HEALTH(insulted_ped[iPed]))
					bTriggerPostSynchDead = TRUE
				ENDIF
				
				IF (IS_ENTITY_ON_FIRE(insulted_ped[iPed]))
					bTriggerPostSynchDead = TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_J)
					bTriggerPostSynchDead = TRUE
				ENDIF
				#ENDIF
				
				IF bTriggerPostSynchDead
					SET_SYNCHRONIZED_SCENE_PHASE(iInsult_AnimPost_SynchSceneID, 0.0)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iInsult_AnimPost_SynchSceneID, TRUE)
					SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, FALSE)
					
					SYNCED_SCENE_PLAYBACK_FLAGS spFlags
					RAGDOLL_BLOCKING_FLAGS ragdollFlags
					IK_CONTROL_FLAGS ikFlags
					spFlags = SYNCED_SCENE_DONT_INTERRUPT
					ragdollFlags = RBF_FIRE|RBF_EXPLOSION|RBF_PLAYER_IMPACT|RBF_MELEE|RBF_ALLOW_BLOCK_DEAD_PED	//RBF_NONE
					ikFlags = AIK_NONE
					
					TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], iInsult_AnimPost_SynchSceneID,
							tInsultAnimDict, tInsultAnimPostDead[iPed],
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT,
							spFlags, ragdollFlags, NORMAL_BLEND_IN, ikFlags)
					
					SET_ENTITY_CAN_BE_DAMAGED(insulted_ped[iPed], FALSE)
					SET_RAGDOLL_BLOCKING_FLAGS(insulted_ped[iPed], ragdollFlags)
					
					SET_PED_CONFIG_FLAG(insulted_ped[iPed], PCF_DisableExplosionReactions, TRUE)
					
					INT iSetup
					REPEAT COUNT_OF(insulted_obj) iSetup
						IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
							IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPostDead[iSetup])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iSetup], iInsult_AnimPost_SynchSceneID,
										tInsultPropPostDead[iSetup], tInsultAnimDict,
										NORMAL_BLEND_IN)
								SET_ENTITY_CAN_BE_DAMAGED(insulted_obj[iSetup], FALSE)
							ENDIF
						ENDIF
					ENDREPEAT
					
					iInsultedStage[iPed] = iCONST_INSULT_11_postSynchDead
					BREAK
				ENDIF
			BREAK
			
			CASE iCONST_INSULT_11_postSynchDead
				BOOL bPerformingPostSynchDead
				bPerformingPostSynchDead = TRUE
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  postSynchDead: no synchID", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_PURPLELIGHT, fInsultAlphaMult)
					#ENDIF
					
					bPerformingPostSynchDead = FALSE
				ELSE
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("  postSynchDead: ", GET_SYNCHRONIZED_SCENE_PHASE(iInsult_AnimPost_SynchSceneID), iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_PURPLELIGHT, fInsultAlphaMult)
					#ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iInsult_AnimPost_SynchSceneID) >= 0.99
						bPerformingPostSynchDead = FALSE
					ENDIF
				ENDIF
				
				IF NOT bPerformingPostSynchDead
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
						iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_t_insult_coord, <<0,0,fPlayer_scene_t_insult_head>>)
					ENDIF
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iInsult_AnimPost_SynchSceneID, FALSE)
					SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
					
					SYNCED_SCENE_PLAYBACK_FLAGS spFlags
					RAGDOLL_BLOCKING_FLAGS ragdollFlags
					IK_CONTROL_FLAGS ikFlags
					spFlags = SYNCED_SCENE_NONE
					ragdollFlags = RBF_FIRE|RBF_EXPLOSION|RBF_PLAYER_IMPACT|RBF_MELEE	//RBF_NONE
					ikFlags = AIK_NONE
					
					TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], iInsult_AnimPost_SynchSceneID,
							tInsultAnimDict, tInsultAnimPostDeadLoop[iPed],
							INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
							spFlags, ragdollFlags, INSTANT_BLEND_IN, ikFlags)
					
					INT iSetup
					REPEAT COUNT_OF(insulted_obj) iSetup
						IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
							IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPostDeadLoop[iSetup])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iSetup], iInsult_AnimPost_SynchSceneID,
										tInsultPropPostDeadLoop[iSetup], tInsultAnimDict,
										NORMAL_BLEND_IN)
							ENDIF
						ENDIF
					ENDREPEAT
					
					iInsultedStage[iPed] = iCONST_INSULT_12_postSynchDeadLoop
					BREAK
				ENDIF
			BREAK
			CASE iCONST_INSULT_12_postSynchDeadLoop
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  postSynchDeadLoop...", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_PURPLELIGHT, fInsultAlphaMult)
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				IF bMovePeds
					iInsultedStage[iPed] = iCONST_INSULT_0_outAnim
					BREAK
				ENDIF
				#ENDIF
				
				
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_BlockWeaponReactionsUnlessDead, TRUE)
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisablePotentialBlastReactions, TRUE)
				SET_PED_RESET_FLAG(insulted_ped[iPed], PRF_DisableMeleeHitReactions, TRUE)
				
				IF NOT IS_ENTITY_PLAYING_ANIM(insulted_ped[iPed], tInsultAnimDict, tInsultAnimPostDeadLoop[iPed], ANIM_SYNCED_SCENE)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
						iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_t_insult_coord, <<0,0,fPlayer_scene_t_insult_head>>)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(insulted_ped[iPed], iInsult_AnimPost_SynchSceneID,
							tInsultAnimDict, tInsultAnimPostDeadLoop[iPed],
							INSTANT_BLEND_IN, INSTANT_BLEND_OUT
						//	, spFlags, ragdollFlags, INSTANT_BLEND_IN, ikFlags
							)
					
					INT iSetup
					REPEAT COUNT_OF(insulted_obj) iSetup
						IF (eInsultedObjModel[iSetup] <> DUMMY_MODEL_FOR_SCRIPT)
							IF NOT IS_STRING_NULL_OR_EMPTY(tInsultPropPostDeadLoop[iSetup])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iSetup], iInsult_AnimPost_SynchSceneID,
										tInsultPropPostDeadLoop[iSetup], tInsultAnimDict,
										NORMAL_BLEND_IN)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			BREAK
			
			DEFAULT
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("STAGE DEFAULT ", iInsultedStage[iPed], iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
				
			BREAK
		ENDSWITCH
	ELSE
		iInsultedStage[iPed] = iCONST_INSULT_DEAD
		
		IF DOES_ENTITY_EXIST(thisInsultedPed)
			INT iObj
			REPEAT COUNT_OF(insulted_obj) iObj
				IF DOES_ENTITY_EXIST(insulted_obj[iObj])
					STOP_SYNCHRONIZED_ENTITY_ANIM(insulted_obj[iObj], NORMAL_BLEND_OUT, TRUE)
					
					FREEZE_ENTITY_POSITION(insulted_obj[iObj], FALSE)
					SET_ENTITY_DYNAMIC(insulted_obj[iObj], TRUE)
					
					SET_OBJECT_AS_NO_LONGER_NEEDED(insulted_obj[iObj])
					
					CPRINTLN(DEBUG_SWITCH, "insulted_obj[", iObj, "] no longer needed???")
				ENDIF
			ENDREPEAT
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(thisInsultedPed)
			DrawLiteralSceneString("dead", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_RED, fInsultAlphaMult)
		ELSE
			DrawLiteralSceneString("non-existant", iPed+5+iCONSTANTSforinsulting, HUD_COLOUR_GREY, fInsultAlphaMult*0.5)
		ENDIF
		
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL Do_PS_T_INSULT_STAGE_THREE()
	
	#IF IS_DEBUG_BUILD
	FLOAT AnimCurrentTime = -1.0
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID)
		DrawLiteralSceneStringFloat("Loop_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
		DrawLiteralSceneStringFloat("Exit_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
	ENDIF
	IF AnimCurrentTime < 0
		DrawLiteralSceneString("SynchSceneID OFF??", 0+4+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
	ENDIF
	#ENDIF
	
	INT iPed
	REPEAT COUNT_OF(insulted_ped) iPed
		ControlInsultPedStage(insulted_ped[iPed], iPed)
	ENDREPEAT
	
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		FLOAT fExitSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tTaunterSceneSpeechLabel)
			IF NOT bTaunterSceneSpeechLabel
				IF (fExitSynchScenePhase < fTaunterSceneSpeechLabel)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("play dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
					#ENDIF
					
					IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tTaunterSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
						bTaunterSceneSpeechLabel = TRUE
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("dialogue heard ", fExitSynchScenePhase, 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("no dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
			#ENDIF
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
//		DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
//		#ENDIF
//		
//		RETURN TRUE
	ENDIF

	
//	ControlInsultPedStage(g_pScene_buddy, COUNT_OF(insulted_ped))
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF VDIST(vPlayerCoord, vPlayer_scene_t_insult_coord) < 100.0
		RETURN FALSE
	ENDIF
	
	REPEAT COUNT_OF(insulted_ped) iPed
		IF NOT IS_PED_INJURED(insulted_ped[iPed])
			VECTOR vInsultedPedCoord = GET_ENTITY_COORDS(insulted_ped[iPed])
			
			IF VDIST(vPlayerCoord, vInsultedPedCoord) < 100.0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_SWITCH, "player is 100m+ from the chateau")
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_T_Insult_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_T_Insult_Variables()
	Setup_Player_Scene_T_Insult()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_T_Insult_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_T_Insult_in_progress
	AND ProgressScene(BIT_TREVOR, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_t_insult_stage
			CASE PS_T_INSULT_switchInProgress
				IF Do_PS_T_INSULT_switchInProgress()
					current_player_scene_t_insult_stage = PS_T_INSULT_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_T_INSULT_STAGE_TWO
				IF Do_PS_T_INSULT_STAGE_TWO()
					current_player_scene_t_insult_stage = PS_T_INSULT_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_T_INSULT_STAGE_THREE
				IF Do_PS_T_INSULT_STAGE_THREE()
					current_player_scene_t_insult_stage = FINISHED_T_INSULT
				ENDIF
			BREAK
			
			CASE FINISHED_T_INSULT
				Player_Scene_T_Insult_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_T_Insult_Widget()
		Player_Scene_T_Insult_Debug_Options()
		
		DrawLiteralSceneTitle(selected_player_scene_t_insult_scene,
				2+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_T_Insult_Stage(current_player_scene_t_insult_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_F_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSforinsulting, HUD_COLOUR_BLUELIGHT, fInsultAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_T_Insult_Cleanup()
ENDSCRIPT
