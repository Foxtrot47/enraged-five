// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene T_FIGHT file for use – player_scene_t_bbfight.sc		 ___
// ___ 																					 ___
// _________________________________________________________________________________________



USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"
USING "rc_threat_public.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSforfighting		4

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T_FIGHT_STAGE_ENUM
	PS_T_FIGHT_switchInProgress,
	PS_T_FIGHT_STAGE_TWO,
	
	FINISHED_T_FIGHT
ENDENUM
PS_T_FIGHT_STAGE_ENUM		current_player_scene_t_fight_stage						= PS_T_FIGHT_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_t_fight_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_T_Fight_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				fighter_ped[2]
INT						iFighter_stage[2]
MODEL_NAMES				eFighterModel[2]
REL_GROUP_HASH			fighterGroup

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			fighter_obj[2]
MODEL_NAMES				eFighterObjModel[2]
PED_BONETAG				eFighterObjAttach[2]

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_t_fight_coord
FLOAT				fPlayer_scene_t_fight_head

VECTOR				vFighter_coordOffset[2]
FLOAT				fFighter_headOffset[2]


CONST_INT iOFF_LIMIT_COUNT 1
VECTOR vOffLimitOffset[iOFF_LIMIT_COUNT], vOffLimitBounds[iOFF_LIMIT_COUNT]
FLOAT fOffLimitAngle[iOFF_LIMIT_COUNT]

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
TEXT_LABEL_63		tBouncerSceneAnimDict, tBouncerSceneAnimLoop[2], tBouncerSceneAnimOut[2]
TEXT_LABEL_63		tBouncerSceneScenario[2]
INT					iBouncerSceneScenarioGameTime[2]

TEXT_LABEL			tFighterSceneVoicePlayer = ""
TEXT_LABEL			tFighterSceneVoiceVictim = ""
TEXT_LABEL			tFighterSceneSpeechLabel = ""
FLOAT				fFighterSceneSpeechLabel = 0.0
BOOL				bFighterSceneSpeechLabel = FALSE

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fFightAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_t_fight_widget
INT				iCurrent_player_scene_t_fight_stage
BOOL			bMovePeds, bSavePeds
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_T_Fight_Cleanup()
	INT iPed
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eFighterModel) iPed
		IF (eFighterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eFighterModel[iPed])
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	REMOVE_RELATIONSHIP_GROUP(fighterGroup)
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_t_fight_widget)
		DELETE_WIDGET_GROUP(player_scene_t_fight_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_T_Fight_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_T_Fight_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_T_Fight_Variables()
	selected_player_scene_t_fight_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_t_fight_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_t_fight_scene, vPlayer_scene_t_fight_coord, fPlayer_scene_t_fight_head, tPlayer_scene_t_fight_room)
	
	ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
	
	GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_t_fight_scene,
			tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
			playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
	
	vOffLimitOffset[0] = <<0,0,0>>
	vOffLimitBounds[0] = <<0,0,0>>
	fOffLimitAngle[0] = 0
	
	CONST_INT	iCONST_BOUNCER_100_synchLoop			100
	CONST_INT	iCONST_BOUNCER_200_animLoop				200
	CONST_INT	iCONST_BOUNCER_0_idle					0
	
	SWITCH selected_player_scene_t_fight_scene
		CASE PR_SCENE_M_VWOODPARK_a
			//- vectors -//
			vFighter_coordOffset[0] = <<-0.8800, 0.8016, -1.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = -150.0000
			
			//- enums -//
			eFighterModel[0] = S_M_Y_Ranger_01
			
			iFighter_stage[0] = iCONST_BOUNCER_200_animLoop
			
			tBouncerSceneAnimDict		= "SWITCH@MICHAEL@PARKBENCH_SMOKE_RANGER"
			tBouncerSceneAnimLoop[0]	= "Ranger_Nervous_Loop"
			tBouncerSceneAnimOut[0]		= "PARKBENCH_SMOKE_RANGER_EXIT_RANGER"
			
			tBouncerSceneScenario[0]	= "any"
		BREAK
		CASE PR_SCENE_M_VWOODPARK_b
			//- vectors -//
			vFighter_coordOffset[0] = <<-1.1832, -0.3745, -1.0000>>
			
			//- floats -//
			fFighter_headOffset[0] =  -147.0000
			
			//- enums -//
			eFighterModel[0] = S_M_Y_Baywatch_01
			
			iFighter_stage[0] = iCONST_BOUNCER_200_animLoop
			
			tBouncerSceneAnimDict		= "SWITCH@MICHAEL@PARKBENCH_SMOKE_RANGER"
			tBouncerSceneAnimLoop[0]	= "Ranger_Nervous_Loop"
			tBouncerSceneAnimOut[0]		= "PARKBENCH_SMOKE_RANGER_EXIT_RANGER"
			
			tBouncerSceneScenario[0]	= "any"
		BREAK
		
		CASE PR_SCENE_M7_REJECTENTRY
			//- vectors -//
			vFighter_coordOffset[0] = <<-0.8296, 1.1365, -0.9999>>		//<<-718.8607, 256.0654, 78.7802>> - vPlayer_scene_t_fight_coord
			
			//- floats -//
			fFighter_headOffset[0] = -176.3014							//-145.0314 - fPlayer_scene_t_fight_head
			
			//- enums -//
			eFighterModel[0] = S_M_M_BOUNCER_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@MICHAEL@REJECTED_ENTRY"
			tBouncerSceneAnimLoop[0]	= "001396_01_MICS3_6_REJECTED_ENTRY_IDLE_BOUNCER"
			tBouncerSceneAnimOut[0]		= "001396_01_MICS3_6_REJECTED_ENTRY_EXIT_BOUNCER"
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_GUARD_STAND"
					
		BREAK
		
		
		CASE PR_SCENE_M7_TALKTOGUARD
			//- vectors -//
			vFighter_coordOffset[0] = <<-1050.724,-476.663,36.037>> - vPlayer_scene_t_fight_coord
			
			//- floats -//
			fFighter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eFighterModel[0] = S_M_M_SECURITY_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@MICHAEL@TALKS_TO_GUARD"
			tBouncerSceneAnimLoop[0]	= "001393_02_MICS3_3_TALKS_TO_GUARD_IDLE_GUARD"
			tBouncerSceneAnimOut[0]		= "001393_02_MICS3_3_TALKS_TO_GUARD_EXIT_GUARD"
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_CLIPBOARD"
			
//			eFighterObjModel[0] = P_AMB_CLIPBOARD_01
//			eFighterObjAttach[0] = BONETAG_PH_L_HAND

		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO
			//- vectors -//
			vFighter_coordOffset[0] = <<0.1366, -1.2192, -1.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = 85.6329
			
			//- enums -//
			eFighterModel[0] = S_M_Y_GRIP_01	//S_M_M_SECURITY_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@MICHAEL@BAR_EMPLOYEE_CONVO"
			tBouncerSceneAnimLoop[0]	= "001387_03_MICS3_2_BAR_EMPLOYEE_CONVO_IDLE_STAFF"
			tBouncerSceneAnimOut[0]		= "001387_03_MICS3_2_BAR_EMPLOYEE_CONVO_EXIT_STAFF"
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_CLIPBOARD"
			
			eFighterObjModel[0] = P_CS_CLIPBOARD	//P_AMB_CLIPBOARD_01
			eFighterObjAttach[0] = BONETAG_PH_L_HAND
			
			tFighterSceneVoicePlayer = "MICHAEL"
			tFighterSceneVoiceVictim = "PREMPLOYEE"
			tFighterSceneSpeechLabel = CreateRandomSpeechLabel("MICS3_IG_2")
			fFighterSceneSpeechLabel = 0.20
			
		BREAK
		
		CASE PR_SCENE_T_FIGHTBBUILD
			//- vectors -//
			vFighter_coordOffset[0] = <<-1194.7703, -1570.9237, 4.60>> - vPlayer_scene_t_fight_coord+<<0,0,-1>>
			fFighter_headOffset[0] = 0.61 - fPlayer_scene_t_fight_head
			
			vFighter_coordOffset[1] = <<-1.3617, 6.7273, -1.0000>>		//<<-1200.9117, -1562.9607, 4.6120>> - vPlayer_scene_t_fight_coord
			fFighter_headOffset[1] = -3.6142							//-168.6142 - fPlayer_scene_t_fight_head
			
			//- enums -//
			eFighterModel[0] = A_M_Y_MUSCLBEAC_01
			eFighterModel[1] = A_M_Y_MUSCLBEAC_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			iFighter_stage[1] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@TREVOR@PUSHES_BODYBUILDER"
			tBouncerSceneAnimLoop[0]	= "001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_BB1"
			tBouncerSceneAnimLoop[1]	= "001426_03_TRVS_5_PUSHES_BODYBUILDER_IDLE_BB2"
			tBouncerSceneAnimOut[0]		= "001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_BB1"
			tBouncerSceneAnimOut[1]		= "001426_03_TRVS_5_PUSHES_BODYBUILDER_EXIT_BB2"
			
			tBouncerSceneScenario[0]	= "any"								//"PROP_HUMAN_MUSCLE_CHIN_UPS"
			tBouncerSceneScenario[1]	= "WORLD_HUMAN_MUSCLE_FREE_WEIGHTS"	//"PROP_HUMAN_MUSCLE_BENCH_PRESS"
			
		BREAK
		CASE PR_SCENE_T_FIGHTBAR_a
			//- vectors -//
			vFighter_coordOffset[0] = <<-4.7000, -0.2000, 0.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = 143.0000
			
			//- enums -//
			eFighterModel[0] = A_M_M_Business_01
			
			iFighter_stage[0] = iCONST_BOUNCER_0_idle
			iFighter_stage[1] = iCONST_BOUNCER_0_idle
			
			vOffLimitOffset[0] = <<-3.65, -1.20, 1.50>>
			vOffLimitBounds[0] = <<2.50, 1.25, 2.00>>
			fOffLimitAngle[0] = -12.00
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_GUARD_STAND"
		BREAK
		CASE PR_SCENE_T_FIGHTBAR_b
			//- vectors -//
			vFighter_coordOffset[0] = <<-1666.3738, -978.6192, 7.1578>> - vPlayer_scene_t_fight_coord
			
			//- floats -//
			fFighter_headOffset[0] = -149.0587
			
			//- enums -//
			eFighterModel[0] = A_M_M_Business_01
			
			iFighter_stage[0] = iCONST_BOUNCER_0_idle
			iFighter_stage[1] = iCONST_BOUNCER_0_idle
			
			vOffLimitOffset[0] = <<0.400,-4.100,1.800>>
			vOffLimitBounds[0] = <<2.500, 1.500, 2.00>>
			fOffLimitAngle[0] = 57.000
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_GUARD_STAND"
		BREAK
		CASE PR_SCENE_T_FIGHTBAR_c
			//- vectors -//
			vFighter_coordOffset[0] = <<1.5000, 5.7035, 0.0000>>
			vFighter_coordOffset[1] = <<4.2393, 4.1781, 0.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = 145.8000
			fFighter_headOffset[1] = 130.320
			
			//- enums -//
			eFighterModel[0] = A_M_M_SALTON_01
			eFighterModel[1] = A_M_M_SALTON_01
			
			iFighter_stage[0] = iCONST_BOUNCER_0_idle
			iFighter_stage[1] = iCONST_BOUNCER_0_idle
			
			vOffLimitOffset[0] = <<2.100,4.300,2.000>>
			vOffLimitBounds[0] = <<2.500, 1.750, 2.000>>
			fOffLimitAngle[0] = 33.000
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_GUARD_STAND"
			tBouncerSceneScenario[1]	= "WORLD_HUMAN_GUARD_STAND"
		BREAK
		CASE PR_SCENE_T_YELLATDOORMAN
			//- vectors -//
			vFighter_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vFighter_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fFighter_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eFighterModel[0] = A_M_M_Malibu_01
//			eFighterModel[1] = A_M_M_Malibu_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			iFighter_stage[1] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@TREVOR@YELLS_AT_DOORMAN"
			tBouncerSceneAnimLoop[0]	= "001430_01_TRVS_21_YELLS_AT_DOORMAN_IDLE_DMAN1"
			tBouncerSceneAnimLoop[1]	= "001430_01_TRVS_21_YELLS_AT_DOORMAN_IDLE_DMAN2"
			tBouncerSceneAnimOut[0]		= "001430_01_TRVS_21_YELLS_AT_DOORMAN_EXIT_DMAN1"
			tBouncerSceneAnimOut[1]		= "001430_01_TRVS_21_YELLS_AT_DOORMAN_EXIT_DMAN2"
			
			tBouncerSceneScenario[0]	= ""
			tBouncerSceneScenario[1]	= ""
			
		BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP
//			//- text labels -//
//			sTieAnimDict = ""
//			sTieAnimLoop[0] = ""
//			sTieAnimLoop[1] = ""
//			sTieAnimLoop[2] = ""
//			sTieAnimLoop[3] = ""
//			
//			//- vectors -//
//			eTieHostageModel[0] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[0] = <<0,0,0>>		vTieHostage_rotOffset[0] = <<0,0,0>>
//			eTieHostageModel[1] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[1] = <<0,0,0>>		vTieHostage_rotOffset[1] = <<0,0,0>>
//			eTieHostageModel[2] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[2] = <<0,0,0>>		vTieHostage_rotOffset[2] = <<0,0,0>>
//			eTieHostageModel[3] = DUMMY_MODEL_FOR_SCRIPT	vTieHostage_coordOffset[3] = <<0,0,0>>		vTieHostage_rotOffset[3] = <<0,0,0>>
//			
//			eTieAnimFlag = AF_DEFAULT
//			
//			//- ints -//
//			iTieHostageStage[0] = -1
//			iTieHostageStage[1] = -1
//			iTieHostageStage[2] = -1
//			iTieHostageStage[3] = -1
//			
//			renderSettingsId[0] = DECAL_RSID_BLOOD_SPLATTER
//			renderSettingsId[1] = DECAL_RSID_BLOOD_DIRECTIONAL
//			vTieDecal_coordOffset[0] = <<50.72744, -2059.79395, 0.49728>> - vPlayer_scene_t_tie_coord
//			vTieDecal_coordOffset[1] = <<47.84937, -2060.95142, 1.73777>> - vPlayer_scene_t_tie_coord
//			
//			renderSettingsId[2] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
//			renderSettingsId[3] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
//			renderSettingsId[4] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
//			renderSettingsId[5] = INT_TO_ENUM(DECAL_RENDERSETTING_ID, -1)
			
			
		
			//- vectors -//
			vFighter_coordOffset[0] = <<-3.2640, -3.7895, 14.1815>>
			
			//- floats -//
			fFighter_headOffset[0] = -0.0
			
			//- enums -//
			eFighterModel[0] = G_M_Y_LOST_01
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@TREVOR@BRIDGE"
			tBouncerSceneAnimLoop[0]	= "HOLD_LOOP_victim"
			tBouncerSceneAnimOut[0]		= "THROW_EXIT_victim"
			
			tBouncerSceneScenario[0]	= ""
			
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_HEAD, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_BERD, 1, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_HAIR, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_TORSO, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_LEG, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_HAND, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_FEET, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_TEETH, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_SPECIAL, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_SPECIAL2, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_DECL, 0, 0)
//	                                    SET_PED_COMPONENT_VARIATION(scene_buddy, PED_COMP_JBIB, 0, 0)
//										
//										//url:bugstar:812879
//										APPLY_PED_BLOOD_BY_ZONE(scene_buddy, ENUM_TO_INT(PDZ_HEAD), 0.360,0.710, "ShotgunSmall")
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_HEAD, 0.810,0.733, BDT_STAB)
//										
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_TORSO, 0.940, 0.590, BDT_SHOTGUN_LARGE)
//
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_RIGHT_ARM, 0.240, 0.620, BDT_STAB)
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_LEFT_ARM, 0.000,0.150, BDT_SHOTGUN_SMALL)
//
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_RIGHT_LEG, 0.460,0.853, BDT_SHOTGUN_LARGE)
//										APPLY_PED_BLOOD_DAMAGE_BY_ZONE(scene_buddy, PDZ_LEFT_LEG, 0.308,0.786, BDT_SHOTGUN_LARGE)
//										APPLY_PED_BLOOD_SPECIFIC(scene_buddy, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.375, 0.398, 0.0, 1.0, -1, 0.0, "BasicSlash")
//										APPLY_PED_BLOOD_SPECIFIC(scene_buddy, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.5, 0.6, 0.0, 1.0, -1, 0.0, "BasicSlash")
//										
//										APPLY_PED_BLOOD_SPECIFIC(scene_buddy, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.6, 0.25, 50.0, 0.5, -1, 0.0, "BasicSlash")
//										APPLY_PED_BLOOD_SPECIFIC(scene_buddy, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.65, 0.325, 50.0, 0.5, -1, 0.0, "BasicSlash")
//										
//										APPLY_PED_BLOOD_SPECIFIC(scene_buddy, ENUM_TO_INT(PDZ_TORSO), 0.58, 0.704, 0.0, 1.0, -1, 0.0, "ShotgunLarge")
//										
			
			
			tFighterSceneVoicePlayer = "TREVOR"
			tFighterSceneVoiceVictim = "DropGuy"
			tFighterSceneSpeechLabel = "TRVS_IG_29G"
			fFighterSceneSpeechLabel = 0.75
			
		BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b
			//- vectors -//
			vFighter_coordOffset[0] = <<0.2190, 5.8091, 0.6000>>		//<<-1279.8350, 309.7326, 64.5553>> - vPlayer_scene_t_fight_coord
			vFighter_coordOffset[1] = <<2.4000, 4.6336, 0.6000>>		//<<-1277.6539, 308.5571, 64.5553>> - vPlayer_scene_t_fight_coord

			//- floats -//
			fFighter_headOffset[0] = -164.1600							//-193.2530 - fPlayer_scene_t_fight_head
			fFighter_headOffset[1] = 166.3200							//137.2270 - fPlayer_scene_t_fight_head
			
			//- enums -//
			eFighterModel[0] = A_M_M_Business_01
			eFighterModel[1] = A_M_M_Business_01
			
			iFighter_stage[0] = iCONST_BOUNCER_0_idle
			iFighter_stage[1] = iCONST_BOUNCER_0_idle
			
			vOffLimitOffset[0] = << 4.0, 8.0, 2.0>>
			vOffLimitBounds[0] = <<10.5, 5.5, 2.0>>
			fOffLimitAngle[0] = 0.0
			
		BREAK
		CASE PR_SCENE_T_FIGHTCASINO
			//- vectors -//
			vFighter_coordOffset[0] = <<3.2998, -1.8338, 0.1366>>		//<<927.4286, 46.1710, 79.9010>> - vPlayer_scene_t_fight_coord
			vFighter_coordOffset[1] = <<2.0737, -3.6041, 0.1348>>		//<<926.2025, 44.4007, 79.8992>> - vPlayer_scene_t_fight_coord

			//- floats -//
			fFighter_headOffset[0] = -169.9514							//59.6571 - fPlayer_scene_t_fight_head
			fFighter_headOffset[1] = 156.7190							//386.3275 - fPlayer_scene_t_fight_head
			
			iFighter_stage[0] = iCONST_BOUNCER_0_idle
			iFighter_stage[1] = iCONST_BOUNCER_0_idle
			
			//- enums -//
			eFighterModel[0] = S_M_M_BOUNCER_01
			eFighterModel[1] = S_M_M_BOUNCER_01
			
			vOffLimitOffset[0] = <<3.0, -3.0, 2.0>>
			vOffLimitBounds[0] = <<4.5, 2.5, 2.0>>
			fOffLimitAngle[0] = 9.0
			
		BREAK
//		CASE PR_SCENE_T_KONEIGHBOUR
//			//- vectors -//
//			vFighter_coordOffset[0] = <<8.8000, 1.9000, 0.6000>>
//			
//			//- floats -//
//			fFighter_headOffset[0] = -159.000
//			
//			//- enums -//
//			eFighterModel[0] = A_M_Y_BevHills_02
//			eFighterModel[1] = DUMMY_MODEL_FOR_SCRIPT
//		BREAK
		
		
		CASE PR_SCENE_T_ESCORTED_OUT
			//- vectors -//
			vFighter_coordOffset[0] = <<-52.20,354.39,113.05>> - vPlayer_scene_t_fight_coord + <<0,0,-1>>
			vFighter_coordOffset[1] = <<-53.18,354.08,113.05>> - vPlayer_scene_t_fight_coord + <<0,0,-1>>
			
			//- floats -//
			fFighter_headOffset[0] = -100
			fFighter_headOffset[1] = -100
			
			//- enums -//
			eFighterModel[0] = A_M_M_Business_01
			eFighterModel[1] = A_M_M_Business_01
			
			vOffLimitOffset[0] = <<1.6187, -0.8200, 1.0000>>
			vOffLimitBounds[0] = <<2.5000+0.5, 8.0000, 2.0000>>
			fOffLimitAngle[0] = 6.0000
			
			iFighter_stage[0] = iCONST_BOUNCER_100_synchLoop
			iFighter_stage[1] = iCONST_BOUNCER_100_synchLoop
			
			tBouncerSceneAnimDict		= "SWITCH@TREVOR@ESCORTED_OUT"
			tBouncerSceneAnimLoop[0]	= "001215_02_TRVS_12_ESCORTED_OUT_IDLE_GUARD1"
			tBouncerSceneAnimLoop[1]	= "001215_02_TRVS_12_ESCORTED_OUT_IDLE_GUARD2"
			tBouncerSceneAnimOut[0]		= "001215_02_TRVS_12_ESCORTED_OUT_EXIT_GUARD1"
			tBouncerSceneAnimOut[1]		= "001215_02_TRVS_12_ESCORTED_OUT_EXIT_GUARD2"
			
			tBouncerSceneScenario[0]	= "WORLD_HUMAN_GUARD_STAND"
			tBouncerSceneScenario[1]	= "WORLD_HUMAN_GUARD_STAND"
			
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_t_fight_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_fight_scene)
			
			CPRINTLN(DEBUG_SWITCH, str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			vFighter_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vFighter_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			//- floats -//
			fFighter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fFighter_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eFighterModel[0] = A_M_Y_MUSCLBEAC_01
			eFighterModel[1] = A_M_Y_MUSCLBEAC_01
			
			vOffLimitOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vOffLimitBounds[0] = <<1,1,1>>
			fOffLimitAngle[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_T_FIGHT_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_T_Fight()
	INT iPed
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	BOOL bWaitForBuddyAssets = FALSE
	WHILE NOT bWaitForBuddyAssets
	AND (iWaitForBuddyAssets < 400)
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iLoadingAssets = 0
		#ENDIF
		
		REPEAT COUNT_OF(fighter_ped) iPed
			IF (eFighterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = (GET_MODEL_NAME_FOR_DEBUG(eFighterModel[iPed]))
				DrawLiteralSceneString(str, iCONSTANTSforfighting+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
				REQUEST_MODEL(eFighterModel[iPed])
				IF NOT HAS_MODEL_LOADED(eFighterModel[iPed])
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eFighterModel[iPed])
				ENDIF
			ENDIF
			IF (eFighterObjModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str = (GET_MODEL_NAME_FOR_DEBUG(eFighterObjModel[iPed]))
				DrawLiteralSceneString(str, iCONSTANTSforfighting+iLoadingAssets, HUD_COLOUR_REDLIGHT)
				iLoadingAssets++
				#ENDIF
				
				REQUEST_MODEL(eFighterObjModel[iPed])
				IF NOT HAS_MODEL_LOADED(eFighterObjModel[iPed])
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eFighterObjModel[iPed])
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		IF NOT bWaitForBuddyAssets
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	ADD_RELATIONSHIP_GROUP("FIGHTERS", fighterGroup)
	
	REPEAT COUNT_OF(fighter_ped) iPed
		IF (eFighterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			fighter_ped[iPed] = CREATE_PED(PEDTYPE_MISSION, eFighterModel[iPed], vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed])
			SET_ENTITY_HEADING(fighter_ped[iPed], fPlayer_scene_t_fight_head+fFighter_headOffset[iPed])
			SET_PED_RANDOM_COMPONENT_VARIATION(fighter_ped[iPed])
			
			IF (eFighterModel[iPed] = S_M_Y_Baywatch_01)

				
				IF (GET_PED_DRAWABLE_VARIATION(fighter_ped[iPed], PED_COMP_TORSO) = 0)		//topless
					IF (GET_PED_DRAWABLE_VARIATION(fighter_ped[iPed], PED_COMP_DECL) <> 1)
						SET_PED_COMPONENT_VARIATION(fighter_ped[iPed], PED_COMP_DECL, 1, 0)
					ENDIF
				ELIF (GET_PED_DRAWABLE_VARIATION(fighter_ped[iPed], PED_COMP_DECL)  = 1)	//tshirt
					IF (GET_PED_DRAWABLE_VARIATION(fighter_ped[iPed], PED_COMP_DECL) <> 0)
						SET_PED_COMPONENT_VARIATION(fighter_ped[iPed], PED_COMP_DECL, 0, 0)
					ENDIF
				ENDIF
			ENDIF
			
			SET_PED_RELATIONSHIP_GROUP_HASH(fighter_ped[iPed], fighterGroup)
			TASK_STAND_STILL(fighter_ped[iPed], -1)
			
			IF (selected_player_scene_t_fight_scene = PR_SCENE_T_CR_BRIDGEDROP)
				SET_ENTITY_MAX_HEALTH(fighter_ped[iPed], 105)
				SET_ENTITY_HEALTH(fighter_ped[iPed], 105)
			ENDIF
		ENDIF
		IF (eFighterObjModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			fighter_obj[iPed] = CREATE_OBJECT(eFighterObjModel[iPed], vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed])
			SET_ENTITY_HEADING(fighter_obj[iPed], fPlayer_scene_t_fight_head+fFighter_headOffset[iPed])
			
			ATTACH_ENTITY_TO_ENTITY(fighter_obj[iPed], fighter_ped[iPed],
					GET_PED_BONE_INDEX(fighter_ped[iPed], eFighterObjAttach[iPed]),
					<<0,0,0>>, <<0,0,0>>)
		ENDIF
		
	ENDREPEAT
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, fighterGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, fighterGroup)
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_T_Fight_Stage(PS_T_FIGHT_STAGE_ENUM this_player_scene_t_fight_stage)
	SWITCH this_player_scene_t_fight_stage
		CASE PS_T_FIGHT_switchInProgress
			RETURN "PS_T_FIGHT_switchInProgress"
		BREAK
		CASE PS_T_FIGHT_STAGE_TWO
			RETURN "PS_T_FIGHT_STAGE_TWO"
		BREAK
		
		CASE FINISHED_T_FIGHT
			RETURN "FINISHED_T_FIGHT"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_T_Fight_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_T_Fight_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_t_fight.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_t_fight_scene)
	
	player_scene_t_fight_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_T_FIGHT_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_T_Fight_Stage(INT_TO_ENUM(PS_T_FIGHT_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_t_fight_stage", iCurrent_player_scene_t_fight_stage)
		
		START_WIDGET_GROUP("Do_PS_T_FIGHT_STAGE_TWO")
			ADD_WIDGET_VECTOR_SLIDER("vPlayer_scene_t_fight_coord", vPlayer_scene_t_fight_coord, -3000.0, 3000.0, 0.0)
			ADD_WIDGET_FLOAT_SLIDER("fPlayer_scene_t_fight_head", fPlayer_scene_t_fight_head, -360.0, 360.0, 0.0)
			
			REPEAT COUNT_OF(fighter_ped) iWidget
				str = "fighter_ped "
				str += iWidget
				ADD_WIDGET_STRING(str)
				ADD_WIDGET_VECTOR_SLIDER("vFighter_coordOffset", vFighter_coordOffset[iWidget], -30.0, 30.0, 0.1)
				
				IF (fFighter_headOffset[iWidget] < -180.0)
					fFighter_headOffset[iWidget] += 360.0
				ENDIF
				IF (fFighter_headOffset[iWidget] >  180.0)
					fFighter_headOffset[iWidget] -= 360.0
				ENDIF
				
				ADD_WIDGET_FLOAT_SLIDER("fFighter_headOffset", fFighter_headOffset[iWidget], -180.0, 180.0, 1.0)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("vOffLimitOffset")
			REPEAT iOFF_LIMIT_COUNT iWidget
				str = "vOffLimitOffset "
				str += iWidget
				ADD_WIDGET_STRING(str)
				ADD_WIDGET_VECTOR_SLIDER("vOffLimitOffset", vOffLimitOffset[iWidget], -30.0, 30.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vOffLimitBounds", vOffLimitBounds[iWidget], -0.0, 30.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("fOffLimitAngle", fOffLimitAngle[iWidget], -180.0, 180.0, 1.0)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
		ADD_WIDGET_BOOL("bSavePeds", bSavePeds)
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_T_Fight_Widget()
	iCurrent_player_scene_t_fight_stage = ENUM_TO_INT(current_player_scene_t_fight_stage)
	
	INT iWidget
	IF bMovePeds
		
		IF NOT (g_iDebugSelectedFriendConnDisplay > 0)
			g_iDebugSelectedFriendConnDisplay = 1
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		
			REPEAT COUNT_OF(fighter_ped) iWidget
				IF NOT IS_PED_INJURED(fighter_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(fighter_ped[iWidget], tBouncerSceneAnimDict, tBouncerSceneAnimLoop[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(fighter_ped[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
								tBouncerSceneAnimDict, tBouncerSceneAnimLoop[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
						iFighter_stage[iWidget] = 100
					ENDIF
					
					vFighter_coordOffset[iWidget] = GET_ENTITY_COORDS(fighter_ped[iWidget]) - vPlayer_scene_t_fight_coord + <<0,0,-1>>
					fFighter_headOffset[iWidget] = GET_ENTITY_HEADING(fighter_ped[iWidget]) - fPlayer_scene_t_fight_head

					IF fFighter_headOffset[iWidget] > 180.0
						fFighter_headOffset[iWidget] -= 360.0
					ENDIF
					IF fFighter_headOffset[iWidget] < -180.0
						fFighter_headOffset[iWidget] += 360.0
					ENDIF
//				ELSE
//					CPRINTLN(DEBUG_SWITCH, "fighter_ped ", iWidget)
//					CPRINTLN(DEBUG_SWITCH, " doesnt exist??")
				ENDIF
			ENDREPEAT
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		
			REPEAT COUNT_OF(fighter_ped) iWidget
				IF NOT IS_PED_INJURED(fighter_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(fighter_ped[iWidget], tBouncerSceneAnimDict, tBouncerSceneAnimOut[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(fighter_ped[iWidget], g_iPlayer_Timetable_exit_SynchSceneID,
								tBouncerSceneAnimDict, tBouncerSceneAnimOut[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
//				ELSE
//					CPRINTLN(DEBUG_SWITCH, "fighter_ped ", iWidget)
//					CPRINTLN(DEBUG_SWITCH, " doesnt exist??")
				ENDIF
			ENDREPEAT
			
		ELSE
		
			REPEAT COUNT_OF(fighter_ped) iWidget
				IF NOT IS_PED_INJURED(fighter_ped[iWidget])
	//				CLEAR_PED_TASKS(fighter_ped[iWidget])
					
					SET_ENTITY_HEADING(fighter_ped[iWidget], fPlayer_scene_t_fight_head+fFighter_headOffset[iWidget])
					SET_ENTITY_COORDS(fighter_ped[iWidget], vPlayer_scene_t_fight_coord+vFighter_coordOffset[iWidget])
				ENDIF
			ENDREPEAT
			
		ENDIF
		
	ENDIF
	
	IF bSavePeds
		
		SAVE_STRING_TO_DEBUG_FILE("	\"player_scene_m_fighter.sc\" - bSavePeds")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(fighter_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vFighter_coordOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vFighter_coordOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_t_fight_coord+vFighter_coordOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - vPlayer_scene_t_fight_coord")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(fighter_ped) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fFighter_headOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fFighter_headOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("							//")
			SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_t_fight_head+fFighter_headOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - fPlayer_scene_t_fight_head")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		
		REPEAT iOFF_LIMIT_COUNT iWidget
			IF NOT ARE_VECTORS_EQUAL(vOffLimitBounds[iWidget], <<0,0,0>>)
				SAVE_STRING_TO_DEBUG_FILE("	vOffLimitOffset[")
				SAVE_INT_TO_DEBUG_FILE(iWidget)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vOffLimitOffset[iWidget])
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("	vOffLimitBounds[")
				SAVE_INT_TO_DEBUG_FILE(iWidget)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vOffLimitBounds[iWidget])
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("	fOffLimitAngle[")
				SAVE_INT_TO_DEBUG_FILE(iWidget)
				SAVE_STRING_TO_DEBUG_FILE("] = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fOffLimitAngle[iWidget])
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bSavePeds = FALSE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_T_Fight_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_t_fight_stage = FINISHED_T_FIGHT
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************


FUNC BOOL HasPlayerIntimidatedBouncer(PED_INDEX bouncerPed)
	
	#IF IS_DEBUG_BUILD
	IF bMovePeds
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(bouncerPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), bouncerPed)
	OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), bouncerPed)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_COMBAT(bouncerPed, PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SHOOTING_NEAR_PED(bouncerPed)
		RETURN TRUE
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(bouncerPed), 15)
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(bouncerPed, EVENT_RESPONDED_TO_THREAT)
		RETURN TRUE
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(fighter_ped) iPed
		IF (bouncerPed <> fighter_ped[iPed])
			IF (iFighter_stage[iPed] < 0)			//is dead
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDREPEAT
	
	INT iZone
	REPEAT iOFF_LIMIT_COUNT iZone
		IF NOT ARE_VECTORS_EQUAL(vOffLimitBounds[iZone], <<0,0,0>>)
			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					vPlayer_scene_t_fight_coord+vOffLimitOffset[iZone],
					vOffLimitBounds[iZone],
					fPlayer_scene_t_fight_head+fOffLimitAngle[iZone])
				
				FLOAT Degrees = 50.0
				IF IS_PED_FACING_PED(bouncerPed, PLAYER_PED_ID(), Degrees)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HasBouncerGottenTooCloseToMissionBlip(PED_INDEX bouncerPed, FLOAT fMin_dist_from_blip)
	
	#IF IS_DEBUG_BUILD
	IF bMovePeds
		RETURN FALSE
	ENDIF
	#ENDIF

	STATIC_BLIP_NAME_ENUM eBlip
	REPEAT g_iTotalStaticBlips eBlip
		IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_ACTIVE)
			
			IF g_GameBlips[eBlip].eCategory = STATIC_BLIP_CATEGORY_PROPERTY		//#1500784
				//
			ELSE

				VECTOR vBlipCoord = GET_STATIC_BLIP_POSITION(eBlip)
				
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vBlipCoord, fMin_dist_from_blip)
				#ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(bouncerPed), vBlipCoord) < (fMin_dist_from_blip*fMin_dist_from_blip)
					RETURN TRUE
				ENDIF
				
				//for multi-char missions
				IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eBlip].bMultiCoordAndSprite
					INT iMultiBlip, iMULTI_BLIP_COUNT = 3
					REPEAT iMULTI_BLIP_COUNT iMultiBlip
						
						VECTOR vMultiBlipCoord = GET_STATIC_BLIP_POSITION(eBlip, iMultiBlip)
						IF NOT IS_VECTOR_ZERO(vMultiBlipCoord)
						
							#IF IS_DEBUG_BUILD
							DrawDebugSceneSphere(vMultiBlipCoord, fMin_dist_from_blip)
							#ENDIF

							IF VDIST2(GET_ENTITY_COORDS(bouncerPed), vMultiBlipCoord) < (fMin_dist_from_blip*fMin_dist_from_blip)
								RETURN TRUE
							ENDIF
						ENDIF
						
					ENDREPEAT
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ControlBouncerPeds(PED_INDEX &bouncerPeds[], INT &iFighterStage[])
	
	CONST_INT	iCONST_BOUNCER_100_synchLoop			100
	CONST_INT	iCONST_BOUNCER_101_synchExit			101
	CONST_INT	iCONST_BOUNCER_102_synchFinished		102
	
	CONST_INT	iCONST_BOUNCER_200_animLoop				200
	CONST_INT	iCONST_BOUNCER_201_animExit				201
	CONST_INT	iCONST_BOUNCER_202_animFinished			202
	
	CONST_INT	iCONST_BOUNCER_0_idle					0
	CONST_INT	iCONST_BOUNCER_1_return					1
	
	CONST_INT	iCONST_BOUNCER_5_scenario				5
	
	CONST_INT	iCONST_BOUNCER_10_fightSetup			10
	CONST_INT	iCONST_BOUNCER_11_fight					11
	
	CONST_INT	iCONST_BOUNCER_DEAD						-1
	
	#IF IS_DEBUG_BUILD
	CONST_INT iControlBouncerPeds						5+1
	#ENDIF
	
	INT iPed
	REPEAT COUNT_OF(bouncerPeds) iPed
		IF NOT IS_PED_INJURED(bouncerPeds[iPed])
			
			SWITCH iFighterStage[iPed]
				CASE iCONST_BOUNCER_100_synchLoop	//
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
						TASK_SYNCHRONIZED_SCENE(bouncerPeds[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
							tBouncerSceneAnimDict, tBouncerSceneAnimLoop[iPed],
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("IS_LOOP_SYNCH_SCENE_RUNNING", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						iFighterStage[iPed] = iCONST_BOUNCER_101_synchExit
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("loop waiting", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
							TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
							
							iFighterStage[iPed] = iCONST_BOUNCER_101_synchExit
						ENDIF
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_101_synchExit	//
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
						
						TASK_SYNCHRONIZED_SCENE(bouncerPeds[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
								tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("IS_EXIT_SYNCH_SCENE_RUNNING", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						REQUEST_ANIM_DICT(tBouncerSceneAnimDict)
						
						iFighterStage[iPed] = iCONST_BOUNCER_102_synchFinished
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
							DrawLiteralSceneString("loop running", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						ELSE
							DrawLiteralSceneString("loop NOT running", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						ENDIF
						#ENDIF
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_102_synchFinished	//
					
					IF HAS_ANIM_DICT_LOADED(tBouncerSceneAnimDict)
					AND IS_ENTITY_PLAYING_ANIM(bouncerPeds[iPed], tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed], ANIM_SYNCED_SCENE)
						
						STRING pEventName
						pEventName = "victim_fall"
						FLOAT ReturnStartPhase, ReturnEndPhase
						ReturnStartPhase = -1
						ReturnEndPhase = -1
						IF FIND_ANIM_EVENT_PHASE(tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed],
								pEventName, ReturnStartPhase, ReturnEndPhase)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str
							str  = "fallsync \""
							str += GET_STRING_FROM_STRING(tBouncerSceneAnimOut[iPed],
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]) - 15,
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]))
							str += "\""
							DrawLiteralSceneString(str, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENDARK, fFightAlphaMult)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							
							str  = "fallPhase: "
							str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 1.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							#ENDIF
							
							IF (ReturnStartPhase < GET_SYNCHRONIZED_SCENE_PHASE (g_iPlayer_Timetable_Exit_SynchSceneID))
								IF NOT IS_PED_RUNNING_RAGDOLL_TASK(bouncerPeds[iPed])
									CLEAR_PED_TASKS(bouncerPeds[iPed])
									SET_PED_TO_RAGDOLL_WITH_FALL(bouncerPeds[iPed], 1000, 3000,
											TYPE_DIE_FROM_HIGH,
											GET_ENTITY_FORWARD_VECTOR(bouncerPeds[iPed]), 
											100.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									SET_PED_CONFIG_FLAG(bouncerPeds[iPed], PCF_RemoveDeadExtraFarAway, TRUE)
								ENDIF
							ENDIF
						
						ELSE
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str
							str  = "outsync \""
							str += GET_STRING_FROM_STRING(tBouncerSceneAnimOut[iPed],
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]) - 15,
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]))
							str += "\""
							DrawLiteralSceneString(str,
									iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							#ENDIF
						ENDIF
						
						
						IF (selected_player_scene_t_fight_scene = PR_SCENE_M7_EMPLOYEECONVO)
							IF NOT IS_STRING_NULL_OR_EMPTY(tBouncerSceneScenario[iPed])
								STRING tBouncerSceneScenarioDict
								tBouncerSceneScenarioDict = "AMB@WORLD_HUMAN_CLIPBOARD@MALE@BASE"
								
								REQUEST_ANIM_DICT(tBouncerSceneScenarioDict)
								
								IF (0.95 < GET_SYNCHRONIZED_SCENE_PHASE (g_iPlayer_Timetable_Exit_SynchSceneID))
									IF HAS_ANIM_DICT_LOADED(tBouncerSceneScenarioDict)
										TASK_START_SCENARIO_IN_PLACE(bouncerPeds[iPed], tBouncerSceneScenario[iPed], 0, FALSE)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(bouncerPeds[iPed])
										WAIT(0)
										
										iBouncerSceneScenarioGameTime[iPed] = -1
										iFighterStage[iPed] = iCONST_BOUNCER_5_scenario
										
										BREAK
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF (selected_player_scene_t_fight_scene = PR_SCENE_T_YELLATDOORMAN)
//						OR (selected_player_scene_t_fight_scene = PR_SCENE_T_FIGHTBBUILD)
							IF (0.95 < GET_SYNCHRONIZED_SCENE_PHASE (g_iPlayer_Timetable_Exit_SynchSceneID))
								TASK_PUT_PED_DIRECTLY_INTO_MELEE(bouncerPeds[iPed], PLAYER_PED_ID(), 0.0, -1.0, 0.0)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(bouncerPeds[iPed])
								WAIT(0)
								
								iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
								BREAK
							ENDIF
						ENDIF
						
						IF (selected_player_scene_t_fight_scene = PR_SCENE_T_FIGHTBBUILD)
						AND (iPed = 0)
							IF (0.95 < GET_SYNCHRONIZED_SCENE_PHASE (g_iPlayer_Timetable_Exit_SynchSceneID))
								TASK_PUT_PED_DIRECTLY_INTO_MELEE(bouncerPeds[iPed], PLAYER_PED_ID(), 0.0, -1.0, 0.0)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(bouncerPeds[iPed])
								
								WAIT(0)
								
								IF NOT IS_PED_INJURED(bouncerPeds[iPed])
									TASK_PUT_PED_DIRECTLY_INTO_MELEE(PLAYER_PED_ID(), bouncerPeds[iPed], 0.0, -1.0, 0.0)
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									WAIT(0)
								ENDIF
								
								iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
								BREAK
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  NOT playing out sync", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						//CLEAR_PED_TASKS(bouncerPeds[iPed])
						iFighterStage[iPed] = iCONST_BOUNCER_1_return
					ENDIF
				BREAK
				
				CASE iCONST_BOUNCER_200_animLoop	//
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
//						TASK_ANIMRONIZED_SCENE(bouncerPeds[iPed], g_iPlayer_Timetable_Loop_SynchSceneID, tBouncerSceneAnimDict, tBouncerSceneAnimLoop[iPed], NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_PLAY_ANIM(bouncerPeds[iPed], tBouncerSceneAnimDict, tBouncerSceneAnimLoop[iPed], NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("IS_LOOP_anim_SCENE_RUNNING", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						iFighterStage[iPed] = iCONST_BOUNCER_201_animExit
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("loop waiting", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
							iFighterStage[iPed] = iCONST_BOUNCER_201_animExit
						ENDIF
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_201_animExit	//
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
//						TASK_ANIMRONIZED_SCENE(bouncerPeds[iPed], g_iPlayer_Timetable_Exit_SynchSceneID, tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed], NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_PLAY_ANIM(bouncerPeds[iPed],
								tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed],
								NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_TAG_SYNC_OUT)
						
						TASK_LOOK_AT_ENTITY(bouncerPeds[iPed], PLAYER_PED_ID(), -1)
						
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("IS_EXIT_ANIM_SCENE_RUNNING", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						REQUEST_ANIM_DICT(tBouncerSceneAnimDict)
						
						iFighterStage[iPed] = iCONST_BOUNCER_202_animFinished
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
							DrawLiteralSceneString("loop running", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						ELSE
							DrawLiteralSceneString("loop NOT running", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						ENDIF
						#ENDIF
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_202_animFinished	//
					
					IF HAS_ANIM_DICT_LOADED(tBouncerSceneAnimDict)
					AND IS_ENTITY_PLAYING_ANIM(bouncerPeds[iPed], tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed], ANIM_SCRIPT)
						
						STRING pEventName
						pEventName = "victim_fall"
						FLOAT ReturnStartPhase, ReturnEndPhase
						ReturnStartPhase = -1
						ReturnEndPhase = -1
						IF FIND_ANIM_EVENT_PHASE(tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed],
								pEventName, ReturnStartPhase, ReturnEndPhase)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str
							str  = "fallAnim \""
							str += GET_STRING_FROM_STRING(tBouncerSceneAnimOut[iPed],
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]) - 15,
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]))
							str += "\""
							DrawLiteralSceneString(str, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENDARK, fFightAlphaMult)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							
							str  = "fallPhase: "
							str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 1.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							#ENDIF
							
							IF (ReturnStartPhase < GET_ENTITY_ANIM_CURRENT_TIME(bouncerPeds[iPed], tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed]))
								IF NOT IS_PED_RUNNING_RAGDOLL_TASK(bouncerPeds[iPed])
									CLEAR_PED_TASKS(bouncerPeds[iPed])
									SET_PED_TO_RAGDOLL_WITH_FALL(bouncerPeds[iPed], 1000, 3000,
											TYPE_DIE_FROM_HIGH,
											GET_ENTITY_FORWARD_VECTOR(bouncerPeds[iPed]), 
											100.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									SET_PED_CONFIG_FLAG(bouncerPeds[iPed], PCF_RemoveDeadExtraFarAway, TRUE)
								ENDIF
							ENDIF
						
						ELSE
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 str
							str  = "outAnim \""
							str += GET_STRING_FROM_STRING(tBouncerSceneAnimOut[iPed],
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]) - 15,
									GET_LENGTH_OF_LITERAL_STRING(tBouncerSceneAnimOut[iPed]))
							str += "\""
							DrawLiteralSceneString(str,
									iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
							#ENDIF
						ENDIF
						
						IF (selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_a
						OR selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_b)
							IF (GET_ENTITY_ANIM_CURRENT_TIME(bouncerPeds[iPed], tBouncerSceneAnimDict, tBouncerSceneAnimOut[iPed]) > 0.954)
								IF (selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_a)
									vFighter_coordOffset[iPed] = <<199.6328, 1213.4664, 224.5948>> - vPlayer_scene_t_fight_coord
								ELIF (selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_b)
									vFighter_coordOffset[iPed] = <<-1908.2874, -717.1404, 5.9049>> - vPlayer_scene_t_fight_coord
								ELSE
		//							vFighter_coordOffset[iPed] = <<50,50,0>>
								ENDIF
								
								TASK_FOLLOW_NAV_MESH_TO_COORD(bouncerPeds[iPed],
										vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed],
										PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
								FORCE_PED_MOTION_STATE(bouncerPeds[iPed], MS_ON_FOOT_WALK)
								
								//CLEAR_PED_TASKS(bouncerPeds[iPed])
								TASK_CLEAR_LOOK_AT(bouncerPeds[iPed])
								
								WAIT(0)
								iFighterStage[iPed] = iCONST_BOUNCER_1_return
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  NOT playing out anim", iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						IF (selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_a)
							vFighter_coordOffset[iPed] = <<199.6328, 1213.4664, 224.5948>> - vPlayer_scene_t_fight_coord
						ELIF (selected_player_scene_t_fight_scene = PR_SCENE_M_VWOODPARK_b)
							vFighter_coordOffset[iPed] = <<-1908.2874, -717.1404, 5.9049>> - vPlayer_scene_t_fight_coord
						ELSE
//							vFighter_coordOffset[iPed] = <<50,50,0>>
						ENDIF
						
						//CLEAR_PED_TASKS(bouncerPeds[iPed])
						TASK_CLEAR_LOOK_AT(bouncerPeds[iPed])
						
						iFighterStage[iPed] = iCONST_BOUNCER_1_return
					ENDIF
				BREAK
				
				CASE iCONST_BOUNCER_0_idle
					VECTOR vBouncerIdleCoord, vBouncerIdleTarget
					vBouncerIdleCoord		= GET_ENTITY_COORDS(bouncerPeds[iPed])
					vBouncerIdleTarget		= vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed] + <<0,0,1>>
					
					FLOAT fBouncerIdleTargetDist
					fBouncerIdleTargetDist = VDIST(vBouncerIdleCoord, vBouncerIdleTarget)
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("idle ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
					DrawDebugSceneTextWithOffset("idle", GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
					DrawDebugSceneLineBetweenCoords(vBouncerIdleCoord, vBouncerIdleTarget, HUD_COLOUR_GREENLIGHT)
					#ENDIF
					
					IF HasPlayerIntimidatedBouncer(bouncerPeds[iPed])
						iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
						BREAK
					ENDIF
					
					IF fBouncerIdleTargetDist >= 1.0
						iFighterStage[iPed] = iCONST_BOUNCER_1_return
						BREAK
					ENDIF
					
					FLOAT fBouncerIdleHead, fBouncerIdleDir
					fBouncerIdleHead		= GET_ENTITY_HEADING(bouncerPeds[iPed])
					fBouncerIdleDir			= fPlayer_scene_t_fight_head+fFighter_headOffset[iPed]
					IF (fBouncerIdleDir < 0.0) fBouncerIdleDir += 360.0
					ELIF (fBouncerIdleDir > 360.0) fBouncerIdleDir -= 360.0 ENDIF
					
					FLOAT fBouncerIdleDirDist
					fBouncerIdleDirDist		= ABSF(fBouncerIdleHead - fBouncerIdleDir)
					
					
					CPRINTLN(DEBUG_SWITCH, "fBouncerIdleDirDist: ", fBouncerIdleDirDist, "		//(", fBouncerIdleHead, " - ", fBouncerIdleDir, ")")
					
					IF fBouncerIdleDirDist >  45
						IF NOT (GET_SCRIPT_TASK_STATUS(bouncerPeds[iPed], SCRIPT_TASK_ACHIEVE_HEADING) = PERFORMING_TASK)
							
							CPRINTLN(DEBUG_SWITCH, "TASK_ACHIEVE_HEADING(bouncerPeds[", iPed, "], ", fBouncerIdleDir, ")	//", fBouncerIdleDirDist)
							
							TASK_ACHIEVE_HEADING(bouncerPeds[iPed], fBouncerIdleDir)
						ENDIF
						
						BREAK
					ENDIF
					
//					IF NOT IS_ENTITY_PLAYING_ANIM(bouncerPeds[iPed], sChateauAnimDict, sChateauAnim)
//						TASK_PLAY_ANIM(fighter_ped[iPed], sChateauAnimDict, sChateauAnim,		// TASK_STAND_STILL(fighter_ped[iPed], -1)
//								NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0)
//						BREAK
//					ENDIF
					
//					IF HasPlayerTresspassedBouncer(bouncerPeds[iPed])
//						iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
//						BREAK
//					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(tBouncerSceneScenario[iPed])
						iBouncerSceneScenarioGameTime[iPed] = -1
						iFighterStage[iPed] = iCONST_BOUNCER_5_scenario
						BREAK
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_1_return
					VECTOR vBouncerReturnCoord, vBouncerReturnTarget
					vBouncerReturnCoord		= GET_ENTITY_COORDS(bouncerPeds[iPed])
					vBouncerReturnTarget	= vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed] + <<0,0,1>>
					
					FLOAT fBouncerReturnTargetDist
					fBouncerReturnTargetDist = VDIST(vBouncerReturnCoord, vBouncerReturnTarget)
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("return ", fBouncerReturnTargetDist, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					DrawDebugSceneTextWithOffset("return", GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					DrawDebugSceneLineBetweenCoords(vBouncerReturnCoord, vBouncerReturnTarget, HUD_COLOUR_ORANGELIGHT)
					#ENDIF
					
					IF fBouncerReturnTargetDist <= 0.5
						iFighterStage[iPed] = iCONST_BOUNCER_0_idle
						BREAK
					ENDIF
					IF HasPlayerIntimidatedBouncer(bouncerPeds[iPed])
					
						iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
						BREAK
					ENDIF
					
					IF NOT (GET_SCRIPT_TASK_STATUS(bouncerPeds[iPed], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)
					AND NOT (GET_SCRIPT_TASK_STATUS(bouncerPeds[iPed], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = WAITING_TO_START_TASK)
						
//						FLOAT fBouncerTargetDir
//						fBouncerTargetDir	= fPlayer_scene_t_fight_head+fFighter_headOffset[iPed]
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(bouncerPeds[iPed], vBouncerReturnTarget, 0.5, DEFAULT_TIME_NEVER_WARP )
						BREAK
					ENDIF
				BREAK
				
				CASE iCONST_BOUNCER_10_fightSetup
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("fightSetup ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					DrawDebugSceneTextWithOffset("fightSetup", GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					
					TEXT_LABEL_63 str
					str  = "PUT_PED_DIRECTLY_INTO_MELEE:"
					SWITCH GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE)
						CASE WAITING_TO_START_TASK			str += ("WAITING_TO_START")				BREAK
						CASE PERFORMING_TASK				str += ("PERFORMING")					BREAK
						CASE DORMANT_TASK					str += ("DORMANT")						BREAK
						CASE VACANT_STAGE					str += ("VACANT_STAGE")					BREAK
						CASE GROUP_TASK_STAGE				str += ("GROUP_TASK_STAGE")				BREAK
						CASE ATTRACTOR_SCRIPT_TASK_STAGE	str += ("ATTRACTOR_SCRIPT_TASK_STAGE")	BREAK
						CASE SECONDARY_TASK_STAGE			str += ("SECONDARY_TASK_STAGE")			BREAK
						CASE FINISHED_TASK					str += ("FINISHED")						BREAK
						
						DEFAULT
							str += ("???invalid task???")
						BREAK
					ENDSWITCH
					
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(bouncerPeds[iPed]), 1.0, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					
					#ENDIF
					
					IF (GET_SCRIPT_TASK_STATUS(bouncerPeds[iPed], SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE) = FINISHED_TASK)
					OR (GET_SCRIPT_TASK_STATUS(bouncerPeds[iPed], SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE) = PERFORMING_TASK)
					
						INT iPed2
						REPEAT COUNT_OF(bouncerPeds) iPed2
							IF (iPed <> iPed2)
								IF (iFighterStage[iPed2] < iCONST_BOUNCER_10_fightSetup)	//not already fighting
									iFighterStage[iPed2] = iCONST_BOUNCER_10_fightSetup
								ENDIF
							ENDIF
						ENDREPEAT
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, fighterGroup, RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, fighterGroup)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED(bouncerPeds[iPed], 25.0, -1)
						SET_PED_KEEP_TASK(bouncerPeds[iPed], TRUE)
						
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(bouncerPeds[iPed])
						
						iFighterStage[iPed] = iCONST_BOUNCER_11_fight
					ENDIF
				BREAK
				CASE iCONST_BOUNCER_11_fight
					
					CONST_FLOAT fCONST_t_fight_coord_protect_radius	50.0
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("fight ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
					DrawDebugSceneTextWithOffset("fight", GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_t_fight_coord, fCONST_t_fight_coord_protect_radius, HUD_COLOUR_ORANGELIGHT, fFightAlphaMult)
					#ENDIF
					
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_t_fight_coord) > fCONST_t_fight_coord_protect_radius
					AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(bouncerPeds[iPed])) > fCONST_t_fight_coord_protect_radius
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, fighterGroup, RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, fighterGroup)
						
						CLEAR_PED_TASKS(bouncerPeds[iPed])
						TASK_FOLLOW_NAV_MESH_TO_COORD(bouncerPeds[iPed], vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed], 0.5, DEFAULT_TIME_NEVER_WARP )
						
						iFighterStage[iPed] = iCONST_BOUNCER_1_return
					ENDIF
					
					IF HasBouncerGottenTooCloseToMissionBlip(bouncerPeds[iPed], 15.0)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, fighterGroup, RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, fighterGroup)
						
						CLEAR_PED_TASKS(bouncerPeds[iPed])
						TASK_FOLLOW_NAV_MESH_TO_COORD(bouncerPeds[iPed], vPlayer_scene_t_fight_coord+vFighter_coordOffset[iPed], 0.5, DEFAULT_TIME_NEVER_WARP )
						
						iFighterStage[iPed] = iCONST_BOUNCER_1_return
					ENDIF
				BREAK
				
				CASE iCONST_BOUNCER_5_scenario
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("scenario ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_PURPLELIGHT, fFightAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(bouncerPeds[iPed]), vPlayer_scene_t_fight_coord + vFighter_coordOffset[iPed], HUD_COLOUR_PURPLELIGHT, fFightAlphaMult)
					
					DrawDebugSceneTextWithOffset("scenario", GET_ENTITY_COORDS(bouncerPeds[iPed]), 0.0, HUD_COLOUR_PURPLELIGHT, fFightAlphaMult)
					IF NOT IS_STRING_NULL_OR_EMPTY(tBouncerSceneScenario[iPed])
						DrawDebugSceneTextWithOffset(tBouncerSceneScenario[iPed], GET_ENTITY_COORDS(bouncerPeds[iPed]), 1.0, HUD_COLOUR_PURPLELIGHT, fFightAlphaMult)
					ELSE
						DrawDebugSceneTextWithOffset("null", GET_ENTITY_COORDS(bouncerPeds[iPed]), 1.0, HUD_COLOUR_PURPLELIGHT, fFightAlphaMult)
					ENDIF
					#ENDIF
					
					IF HasPlayerIntimidatedBouncer(bouncerPeds[iPed])
					
						IF DOES_ENTITY_EXIST(fighter_obj[iPed])
							DELETE_OBJECT(fighter_obj[iPed])
							SET_OBJECT_AS_NO_LONGER_NEEDED(fighter_obj[iPed])
						ENDIF
						
						CLEAR_PED_TASKS(bouncerPeds[iPed])
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(bouncerPeds[iPed])
						iFighterStage[iPed] = iCONST_BOUNCER_10_fightSetup
						BREAK
					ENDIF
					
					IF NOT PED_HAS_USE_SCENARIO_TASK(bouncerPeds[iPed])
						
						IF ARE_STRINGS_EQUAL(tBouncerSceneScenario[iPed], "any")
		
							IF DOES_SCENARIO_EXIST_IN_AREA(vPlayer_scene_t_fight_coord + vFighter_coordOffset[iPed], 20.0, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(bouncerPeds[iPed],
										vPlayer_scene_t_fight_coord + vFighter_coordOffset[iPed], 20.0)
								iBouncerSceneScenarioGameTime[iPed] = GET_GAME_TIMER()
							ENDIF
						ELSE
							BOOL bPlayIntroClip
							bPlayIntroClip = TRUE
							IF (selected_player_scene_t_fight_scene = PR_SCENE_M7_EMPLOYEECONVO)
								bPlayIntroClip = FALSE
							ENDIF
							
							TASK_START_SCENARIO_IN_PLACE(bouncerPeds[iPed],
									tBouncerSceneScenario[iPed], 0, bPlayIntroClip)
							iBouncerSceneScenarioGameTime[iPed] = GET_GAME_TIMER()
						ENDIF
							
						CPRINTLN(DEBUG_SWITCH, "doesnt have task...")
					ELSE
						//
						
						IF DOES_ENTITY_EXIST(fighter_obj[iPed])
								
							CPRINTLN(DEBUG_SWITCH, "has task, prop exists [", iBouncerSceneScenarioGameTime[iPed], "]...")
							
							IF iBouncerSceneScenarioGameTime[iPed] > 0
								 IF (GET_GAME_TIMER() > (iBouncerSceneScenarioGameTime[iPed]+1000))
//									OBJECT_INDEX dupe_fighter_obj
//									dupe_fighter_obj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(fighter_obj[iPed]), 0.5, eFighterObjModel[iPed], FALSE)
//									
//									IF (fighter_obj[iPed] <> dupe_fighter_obj)
										DELETE_OBJECT(fighter_obj[iPed])
										SET_OBJECT_AS_NO_LONGER_NEEDED(fighter_obj[iPed])
//									ENDIF
								ENDIF
							ENDIF
						ELSE
							//
								
//							CPRINTLN(DEBUG_SWITCH, "has task, no prop...")
							
						ENDIF
						
					ENDIF
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringInt("unknown ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_GREENLIGHT, fFightAlphaMult)
					#ENDIF
					
					SCRIPT_ASSERT("ControlBouncerPeds")
				BREAK
			ENDSWITCH
			
		ELIF DOES_ENTITY_EXIST(bouncerPeds[iPed])
		
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(bouncerPeds[iPed], FALSE), vPlayer_scene_t_fight_coord)
			DrawLiteralSceneStringInt("dead ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_REDLIGHT, fFightAlphaMult)
			#ENDIF
			
			IF (iFighterStage[iPed] <> iCONST_BOUNCER_DEAD)
				
				INT iPed2
				REPEAT COUNT_OF(bouncerPeds) iPed2
					IF (iPed <> iPed2)
						IF (iFighterStage[iPed2] <> iCONST_BOUNCER_DEAD)			//not dead
						AND (iFighterStage[iPed2] < iCONST_BOUNCER_10_fightSetup)	//not already fighting
							iFighterStage[iPed2] = iCONST_BOUNCER_10_fightSetup
						ENDIF
					ENDIF
				ENDREPEAT
				
				iFighterStage[iPed] = iCONST_BOUNCER_DEAD
			ENDIF
			
			IF DOES_ENTITY_EXIST(fighter_obj[iPed])
				IF iBouncerSceneScenarioGameTime[iPed] > 0
					DELETE_OBJECT(fighter_obj[iPed])
					SET_OBJECT_AS_NO_LONGER_NEEDED(fighter_obj[iPed])
				ENDIF
			ENDIF
		ELSE
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringInt("doesnt exist ", iPed, iPed+iControlBouncerPeds+iCONSTANTSforfighting, HUD_COLOUR_RED, fFightAlphaMult*0.25)
			#ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_T_FIGHT_switchInProgress()
	
	ControlBouncerPeds(fighter_ped, iFighter_stage)
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_T_FIGHT_STAGE_TWO()
	
	ControlBouncerPeds(fighter_ped, iFighter_stage)
	
	
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		FLOAT fExitSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
		
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			IF NOT IS_STRING_NULL_OR_EMPTY(tFighterSceneSpeechLabel)
				IF NOT bFighterSceneSpeechLabel
					IF (fExitSynchScenePhase < fFighterSceneSpeechLabel)
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("wait for dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("play dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
						#ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(tFighterSceneVoicePlayer)
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, GET_CURRENT_PLAYER_PED_INT(), PLAYER_PED_ID(), tFighterSceneVoicePlayer)
						ENDIF
						IF NOT IS_STRING_NULL_OR_EMPTY(tFighterSceneVoiceVictim)
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, fighter_ped[0], tFighterSceneVoiceVictim)
						ENDIF
						
						IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tFighterSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
							bFighterSceneSpeechLabel = TRUE
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("dialogue heard ", fExitSynchScenePhase, 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("no dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
				#ENDIF
			ENDIF
		
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("no dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
			#ENDIF
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
//		DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
//		#ENDIF
//		
//		RETURN TRUE
	ENDIF
	
	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_t_fight_coord) > 100.0
		CPRINTLN(DEBUG_SWITCH, "player is 100m+ from the fight coord ", vPlayer_scene_t_fight_coord)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_T_Fight_Cleanup()
	ENDIF

	WAIT(0)
	
	Initialise_Player_Scene_T_Fight_Variables()
	Setup_Player_Scene_T_Fight()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_T_Fight_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_T_Fight_in_progress
	AND ProgressScene(BIT_MICHAEL|BIT_TREVOR, NULL)
		IF Should_Switch_Cleanup_Be_Forced()
			Player_Scene_T_Fight_Cleanup()
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			IF selected_player_scene_t_fight_scene = PR_SCENE_T_FIGHTBAR_c
			AND GET_CURRENT_PROPERTY_OWNER(PROPERTY_BAR_HEN_HOUSE) = CHAR_TREVOR
				Player_Scene_T_Fight_Cleanup()
			ENDIF
		ENDIF
		
		WAIT(0)
		
		SWITCH current_player_scene_t_fight_stage
			CASE PS_T_FIGHT_switchInProgress
				IF Do_PS_T_FIGHT_switchInProgress()
					current_player_scene_t_fight_stage = PS_T_FIGHT_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_T_FIGHT_STAGE_TWO
				IF Do_PS_T_FIGHT_STAGE_TWO()
					current_player_scene_t_fight_stage = FINISHED_T_FIGHT
				ENDIF
			BREAK
			
			CASE FINISHED_T_FIGHT
				Player_Scene_T_Fight_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_T_Fight_Widget()
		Player_Scene_T_Fight_Debug_Options()
		
		fFightAlphaMult = 1.0
		IF current_player_scene_t_fight_stage = PS_T_FIGHT_STAGE_TWO
			
			CONST_FLOAT fMAX_PLAYER_FIGHT_DIST	20.0
			FLOAT fPlayerFightDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_t_fight_coord)
			
			INT iPed
			REPEAT COUNT_OF(fighter_ped) iPed
				IF DOES_ENTITY_EXIST(fighter_ped[iPed])
					FLOAT fChasePedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(fighter_ped[iPed], FALSE))
					
					IF (fPlayerFightDist > fChasePedDist)
						fPlayerFightDist = fChasePedDist
					ENDIF
				ENDIF
			ENDREPEAT
			
			fFightAlphaMult = 1.0 - (fPlayerFightDist/fMAX_PLAYER_FIGHT_DIST)
			IF fFightAlphaMult < 0
				fFightAlphaMult = 0
			ENDIF
			
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_t_fight_scene,
				2+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_t_fight_Stage(current_player_scene_t_fight_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_F_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSforfighting, HUD_COLOUR_BLUELIGHT, fFightAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_T_Fight_Cleanup()
ENDSCRIPT
