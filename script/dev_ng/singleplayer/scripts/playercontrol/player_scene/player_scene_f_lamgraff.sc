// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene F_LAM_GRAFF file for use – player_scene_f_lamgraff.sc	 ___
// ___ 																					 ___
// _________________________________________________________________________________________


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"
USING "family_public.sch"

//-	private headers	-//
USING "player_scene_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
#IF IS_DEBUG_BUILD
CONST_INT iDrawLiteralScene_row	6
#ENDIF

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_F_LAM_GRAFF_STAGE_ENUM
	PS_F_LAM_GRAFF_switchInProgress,
	PS_F_LAM_GRAFF_STAGE_TWO,
	PS_F_LAM_GRAFF_STAGE_THREE,
	
	FINISHED_F_LAM_GRAFF
ENDENUM
PS_F_LAM_GRAFF_STAGE_ENUM	current_player_scene_f_lam_graff_stage						= PS_F_LAM_GRAFF_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_f_lam_graff_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_F_Lam_Graff_in_progress							= TRUE
BOOL					bCreate_conversation											= FALSE

	//- People(PED_INDEX and MODEL_NAMES) -//

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			oSprayCan
MODEL_NAMES				eSprayCanModel

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_f_lam_graff_coord
FLOAT				fPlayer_scene_f_lam_graff_head

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tBuddyGraffAnimDict, tBuddyGraffAnimOut, tBuddyGraffAnimPostOut

INT					iInsult_AnimPost_SynchSceneID = -1

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//
PTFX_ID ptSpray
FLOAT fSprayColR = 0.0, fSprayColG = 1.0, fSprayColB = 1.0

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fLamGraffAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_f_lam_graff_widget
INT				iCurrent_player_scene_f_lam_graff_stage
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_F_Lam_Graff_Cleanup()
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	IF (ptSpray <> NULL)
		STOP_PARTICLE_FX_LOOPED(ptSpray)
		ptSpray = NULL
	ENDIF
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_f_lam_graff_widget)
		DELETE_WIDGET_GROUP(player_scene_f_lam_graff_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_F_Lam_Graff_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_F_Lam_Graff_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_F_Lam_Graff_Variables()
	
	selected_player_scene_f_lam_graff_scene	= g_eRecentlySelectedScene	//PR_SCENE_F_LAMGRAFF_YARD	//PR_SCENE_F_LAMGRAFF_ALLEY
	
	TEXT_LABEL_31 tPlayer_scene_x_WHATEVER_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_f_lam_graff_scene, vPlayer_scene_f_lam_graff_coord, fPlayer_scene_f_lam_graff_head, tPlayer_scene_x_WHATEVER_room)
	
	//- vectors -//
	//- floats -//
	//- ints -//
	
	eSprayCanModel = Prop_CS_Spray_Can
	
	//-- structs : PS_F_LAM_GRAFF_STRUCT --//
	
	PED_SCENE_STRUCT sPedScene
	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	
	sPedScene.iStage = 0
	sPedScene.eScene = selected_player_scene_f_lam_graff_scene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
	
	TEXT_LABEL_63		tBuddyGraffAnimLoop
	ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
	GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_f_lam_graff_scene,
			tBuddyGraffAnimDict, tBuddyGraffAnimLoop, tBuddyGraffAnimOut,
			buddyAnimLoopFlag, buddyAnimOutFlag)
	tBuddyGraffAnimPostOut = "Lamar_tagging_EXIT_LOOP_LAMAR"
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_F_Lam_Graff()
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	REQUEST_MODEL(eSprayCanModel)
	
	//- request models - weapons -//
	//- request anims and ptfx --//
	REQUEST_PTFX_ASSET()
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	WHILE NOT DOES_ENTITY_EXIST(g_pScene_buddy)
	OR NOT HAS_MODEL_LOADED(eSprayCanModel)
		
		#IF IS_DEBUG_BUILD
		INT iRow = 0
		
		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str  = ("g_pScene_buddy doesnt exist")
			DrawLiteralSceneString(str, iRow+iDrawLiteralScene_row, HUD_COLOUR_RED)
			iRow++
			#ENDIF
		ENDIF
		IF NOT HAS_MODEL_LOADED(eSprayCanModel)
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str  = ("prop model ")
			str += GET_MODEL_NAME_FOR_DEBUG(eSprayCanModel)
			DrawLiteralSceneString(str, iRow+iDrawLiteralScene_row, HUD_COLOUR_RED)
			iRow++
			#ENDIF
		ENDIF
		
		
		#ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
	
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
		IF NOT IS_PED_INJURED(g_pScene_buddy)
			TASK_STAND_STILL(g_pScene_buddy, -1)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(g_pScene_buddy, RELGROUPHASH_PLAYER)
	ENDIF
	
	SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
	SET_PED_CONFIG_FLAG(g_pScene_buddy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	SET_RAGDOLL_BLOCKING_FLAGS(g_pScene_buddy, RBF_PLAYER_IMPACT)
	PRIVATE_SetDefaultFamilyMemberAttributes(g_pScene_buddy, RELGROUPHASH_FAMILY_F)
	
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	
	oSprayCan = CREATE_OBJECT(eSprayCanModel, GET_ENTITY_COORDS(g_pScene_buddy))
	ATTACH_ENTITY_TO_ENTITY(oSprayCan, g_pScene_buddy,
			GET_PED_BONE_INDEX(g_pScene_buddy, BONETAG_PH_R_HAND),
			<<0,0,0>>,	//widget_object_pos,
			<<0,0,0>>)	//widget_object_rot)
	
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_F_Lam_Graff_Stage(PS_F_LAM_GRAFF_STAGE_ENUM this_player_scene_f_lam_graff_stage)
	SWITCH this_player_scene_f_lam_graff_stage
		CASE PS_F_LAM_GRAFF_switchInProgress
			RETURN "PS_F_LAM_GRAFF_switchInProgress"
		BREAK
		CASE PS_F_LAM_GRAFF_STAGE_TWO
			RETURN "PS_F_LAM_GRAFF_STAGE_TWO"
		BREAK
		CASE PS_F_LAM_GRAFF_STAGE_THREE
			RETURN "PS_F_LAM_GRAFF_STAGE_THREE"
		BREAK
		
		CASE FINISHED_F_LAM_GRAFF
			RETURN "FINISHED_F_LAM_GRAFF"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_F_Lam_Graff_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_F_Lam_Graff_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_f_lam_graff.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_lam_graff_scene)
	
	player_scene_f_lam_graff_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_F_LAM_GRAFF_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_F_Lam_Graff_Stage(INT_TO_ENUM(PS_F_LAM_GRAFF_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_f_lam_graff_stage", iCurrent_player_scene_f_lam_graff_stage)
		
		START_WIDGET_GROUP("fSprayCol")
			ADD_WIDGET_FLOAT_SLIDER("fSprayColR", fSprayColR, 0.0, 255.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fSprayColG", fSprayColG, 0.0, 255.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fSprayColB", fSprayColB, 0.0, 255.0, 0.001)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_F_Lam_Graff_Widget()
	iCurrent_player_scene_f_lam_graff_stage = ENUM_TO_INT(current_player_scene_f_lam_graff_stage)
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_F_Lam_Graff_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_f_lam_graff_stage = FINISHED_F_LAM_GRAFF
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_F_LAM_GRAFF_switchInProgress()
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ELSE
		//
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_LAM_GRAFF_STAGE_TWO()
	
	IF NOT IS_PED_INJURED(g_pScene_buddy)
		IF DOES_ENTITY_EXIST(oSprayCan)
			IF IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tBuddyGraffAnimDict, tBuddyGraffAnimOut)	
				
				IF IS_ENTITY_ON_FIRE(g_pScene_buddy)
					STOP_ANIM_PLAYBACK(g_pScene_buddy)
					CLEAR_PED_TASKS(g_pScene_buddy)
					RETURN FALSE
				ENDIF
				
		//		FLOAT fBuddyGraffAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(g_pScene_buddy, tBuddyGraffAnimDict, tBuddyGraffAnimOut)
				FLOAT fBuddyGraffAnimTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("anim ", fBuddyGraffAnimTime, 1+iDrawLiteralScene_row, HUD_COLOUR_GREENLIGHT, fLamGraffAlphaMult)
				#ENDIF
				
				CONST_FLOAT fSPRAY_ANIM_A_START	0.3812		//0.272		//0.15
				CONST_FLOAT fSPRAY_ANIM_A_STOP	0.4778		//0.425		//0.30
				
				CONST_FLOAT fSPRAY_ANIM_B_START	0.5745		//0.897		//0.48
				CONST_FLOAT fSPRAY_ANIM_B_STOP	0.6712		//0.999		//0.73
				
				IF (fBuddyGraffAnimTime < fSPRAY_ANIM_A_START)
					//wait for start A
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for start A:", fSPRAY_ANIM_A_START, 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_A_STOP)
					//wait for stop A
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for stop A:", fSPRAY_ANIM_A_STOP, 2+iDrawLiteralScene_row, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray = NULL)
						ptSpray = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_lamgraff_paint_spray",
								oSprayCan, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_COLOUR(ptSpray, fSprayColR, fSprayColG, fSprayColB)
						
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_B_START)
					//wait for start B
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for start B:", fSPRAY_ANIM_B_START, 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_B_STOP)
					//wait for stop B
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for stop B:", fSPRAY_ANIM_B_STOP, 2+iDrawLiteralScene_row, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray = NULL)
						ptSpray = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_lamgraff_paint_spray",
								oSprayCan, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_COLOUR(ptSpray, fSprayColR, fSprayColG, fSprayColB)
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < 1.0)
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("wait for end", 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("ending...", 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					CLEAR_PED_TASKS(g_pScene_buddy)
				
					//CLEAR_RAGDOLL_BLOCKING_FLAGS(g_pScene_buddy, RBF_PLAYER_IMPACT)
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
						iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_f_lam_graff_coord, <<0,0,fPlayer_scene_f_lam_graff_head>>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, iInsult_AnimPost_SynchSceneID,
							tBuddyGraffAnimDict, tBuddyGraffAnimPostOut,
							NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
							SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_USE_PHYSICS,
							RBF_NONE, NORMAL_BLEND_IN, AIK_NONE)
					
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("anim stopped", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
				#ENDIF
				
				IF (ptSpray <> NULL)
					STOP_PARTICLE_FX_LOOPED(ptSpray)
					ptSpray = NULL
				ENDIF
				
				IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", "FRAS_IG_7x", CONV_PRIORITY_AMBIENT_MEDIUM)
			
					CLEAR_PED_TASKS(g_pScene_buddy)
				
					CLEAR_RAGDOLL_BLOCKING_FLAGS(g_pScene_buddy, RBF_PLAYER_IMPACT)
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
						iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_f_lam_graff_coord, <<0,0,fPlayer_scene_f_lam_graff_head>>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, iInsult_AnimPost_SynchSceneID,
							tBuddyGraffAnimDict, tBuddyGraffAnimPostOut,
							NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
							SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_USE_PHYSICS,
							RBF_NONE, NORMAL_BLEND_IN, AIK_NONE)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("oSprayCan dead", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
			PRINTSTRING("oSprayCan dead")PRINTNL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("g_pScene_buddy dead", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
		PRINTSTRING("g_pScene_buddy dead")PRINTNL()
		#ENDIF
		
		Player_Scene_F_Lam_Graff_Cleanup()
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_LAM_GRAFF_STAGE_THREE()
	
	IF NOT IS_PED_INJURED(g_pScene_buddy)
		IF DOES_ENTITY_EXIST(oSprayCan)
			IF IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tBuddyGraffAnimDict, tBuddyGraffAnimPostOut)	
				
				IF IS_ENTITY_ON_FIRE(g_pScene_buddy)
					STOP_ANIM_PLAYBACK(g_pScene_buddy)
					CLEAR_PED_TASKS(g_pScene_buddy)
					RETURN FALSE
				ENDIF
				
		//		FLOAT fBuddyGraffAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(g_pScene_buddy, tBuddyGraffAnimDict, tBuddyGraffAnimPostOut)
				FLOAT fBuddyGraffAnimTime = GET_SYNCHRONIZED_SCENE_PHASE(iInsult_AnimPost_SynchSceneID)
			
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("anim ", fBuddyGraffAnimTime, 1+iDrawLiteralScene_row, HUD_COLOUR_GREENLIGHT, fLamGraffAlphaMult)
				#ENDIF
				
				CONST_FLOAT fSPRAY_ANIM_A_START	0.2812		//0.272		//0.15
				CONST_FLOAT fSPRAY_ANIM_A_STOP	0.4778		//0.425		//0.30
				
				CONST_FLOAT fSPRAY_ANIM_B_START	0.5745		//0.897		//0.48
				CONST_FLOAT fSPRAY_ANIM_B_STOP	0.7712		//0.999		//0.73
				
				IF NOT bCreate_conversation
					IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", "FRAS_IG_7x", CONV_PRIORITY_AMBIENT_MEDIUM)
						bCreate_conversation = TRUE
					ENDIF
				ENDIF
				
				IF (fBuddyGraffAnimTime < fSPRAY_ANIM_A_START)
					//wait for start A
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for start A:", fSPRAY_ANIM_A_START, 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					
					DrawDebugSceneTextWithOffset("wait for start A", GET_ENTITY_COORDS(oSprayCan), -0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(fSPRAY_ANIM_A_START), GET_ENTITY_COORDS(oSprayCan),  0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_A_STOP)
					//wait for stop A
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for stop A:", fSPRAY_ANIM_A_STOP, 2+iDrawLiteralScene_row, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					
					DrawDebugSceneTextWithOffset("wait for stop A", GET_ENTITY_COORDS(oSprayCan), -0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(fSPRAY_ANIM_A_STOP), GET_ENTITY_COORDS(oSprayCan),  0.5, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray = NULL)
						ptSpray = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_lamgraff_paint_spray",
								oSprayCan, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_COLOUR(ptSpray, fSprayColR, fSprayColG, fSprayColB)
						
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_B_START)
					//wait for start B
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for start B:", fSPRAY_ANIM_B_START, 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					
					DrawDebugSceneTextWithOffset("wait for start B", GET_ENTITY_COORDS(oSprayCan), -0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(fSPRAY_ANIM_B_START), GET_ENTITY_COORDS(oSprayCan),  0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
					
				ELIF (fBuddyGraffAnimTime < fSPRAY_ANIM_B_STOP)
					//wait for stop B
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for stop B:", fSPRAY_ANIM_B_STOP, 2+iDrawLiteralScene_row, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					
					DrawDebugSceneTextWithOffset("wait for stop B", GET_ENTITY_COORDS(oSprayCan), -0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(fSPRAY_ANIM_B_STOP), GET_ENTITY_COORDS(oSprayCan),  0.5, HUD_COLOUR_GREEN, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray = NULL)
						ptSpray = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_lamgraff_paint_spray",
								oSprayCan, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_COLOUR(ptSpray, fSprayColR, fSprayColG, fSprayColB)
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("wait for end", 2+iDrawLiteralScene_row, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_RED, fLamGraffAlphaMult)
					
					DrawDebugSceneTextWithOffset("wait for end", GET_ENTITY_COORDS(oSprayCan), -0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(1.0), GET_ENTITY_COORDS(oSprayCan),  0.5, HUD_COLOUR_RED, fLamGraffAlphaMult)
					#ENDIF
					
					IF (ptSpray <> NULL)
						STOP_PARTICLE_FX_LOOPED(ptSpray)
						ptSpray = NULL
					ENDIF
//					IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", "FRAS_IG_7x", CONV_PRIORITY_AMBIENT_MEDIUM)
//				
//						CLEAR_PED_TASKS(g_pScene_buddy)
//					
//						CLEAR_RAGDOLL_BLOCKING_FLAGS(g_pScene_buddy, RBF_PLAYER_IMPACT)
//					
//						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
//							iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_f_lam_graff_coord, <<0,0,fPlayer_scene_f_lam_graff_head>>)
//							SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
//						ENDIF
//						
//						TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, iInsult_AnimPost_SynchSceneID,
//								tBuddyGraffAnimDict, tBuddyGraffAnimPostpostout,
//								NORMAL_BLEND_IN, NORMAL_BLEND_postout)
//						
//						RETURN TRUE
//					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("anim stopped", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
					DrawDebugSceneSphere(GET_ENTITY_COORDS(oSprayCan), 0.1, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
					#ENDIF
				
				IF (ptSpray <> NULL)
					STOP_PARTICLE_FX_LOOPED(ptSpray)
					ptSpray = NULL
				ENDIF
				
//				IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", "FRAS_IG_7x", CONV_PRIORITY_AMBIENT_MEDIUM)
//			
//					CLEAR_PED_TASKS(g_pScene_buddy)
//				
//					CLEAR_RAGDOLL_BLOCKING_FLAGS(g_pScene_buddy, RBF_PLAYER_IMPACT)
//				
//					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iInsult_AnimPost_SynchSceneID)
//						iInsult_AnimPost_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPlayer_scene_f_lam_graff_coord, <<0,0,fPlayer_scene_f_lam_graff_head>>)
//						SET_SYNCHRONIZED_SCENE_LOOPED(iInsult_AnimPost_SynchSceneID, TRUE)
//					ENDIF
//					
//					TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, iInsult_AnimPost_SynchSceneID,
//							tBuddyGraffAnimDict, tBuddyGraffAnimPostpostout,
//							NORMAL_BLEND_IN, NORMAL_BLEND_postout,
//							SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,
//							RBF_NONE, NORMAL_BLEND_IN, AIK_NONE)
//					
//					RETURN TRUE
//				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("oSprayCan dead", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
			PRINTSTRING("oSprayCan dead")PRINTNL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("g_pScene_buddy dead", 1+iDrawLiteralScene_row, HUD_COLOUR_REDLIGHT, fLamGraffAlphaMult)
		PRINTSTRING("g_pScene_buddy dead")PRINTNL()
		#ENDIF
		
		Player_Scene_F_Lam_Graff_Cleanup()
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_F_Lam_Graff_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_F_Lam_Graff_Variables()
	Setup_Player_Scene_F_Lam_Graff()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_F_Lam_Graff_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_F_Lam_Graff_in_progress
	AND ProgressScene(BIT_FRANKLIN, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_f_lam_graff_stage
			CASE PS_F_LAM_GRAFF_switchInProgress
				IF Do_PS_F_LAM_GRAFF_switchInProgress()
					current_player_scene_f_lam_graff_stage = PS_F_LAM_GRAFF_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_F_LAM_GRAFF_STAGE_TWO
				IF Do_PS_F_LAM_GRAFF_STAGE_TWO()
					current_player_scene_f_lam_graff_stage = PS_F_LAM_GRAFF_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_F_LAM_GRAFF_STAGE_THREE
				IF Do_PS_F_LAM_GRAFF_STAGE_THREE()
					current_player_scene_f_lam_graff_stage = FINISHED_F_LAM_GRAFF
				ENDIF
			BREAK
			
			CASE FINISHED_F_LAM_GRAFF
				Player_Scene_F_Lam_Graff_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_F_Lam_Graff_Widget()
		Player_Scene_F_Lam_Graff_Debug_Options()
		
		fLamGraffAlphaMult = 1.0
		IF current_player_scene_f_lam_graff_stage = PS_F_LAM_GRAFF_STAGE_THREE
			
			CONST_FLOAT fMAX_PLAYER_LAM_GRAFF_DIST	20.0
			FLOAT fPlayerLamGraffDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_f_lam_graff_coord)
			
			IF DOES_ENTITY_EXIST(g_pScene_buddy)
				FLOAT fTaunterPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_pScene_buddy, FALSE))
				
				IF (fPlayerLamGraffDist > fTaunterPedDist)
					fPlayerLamGraffDist = fTaunterPedDist
				ENDIF
			ENDIF
			
			fLamGraffAlphaMult = 1.0 - (fPlayerLamGraffDist/fMAX_PLAYER_LAM_GRAFF_DIST)
			IF fLamGraffAlphaMult < 0
				fLamGraffAlphaMult = 0
			ENDIF
		ENDIF
		
		STRING sStage = Get_String_From_Player_Scene_F_Lam_Graff_Stage(current_player_scene_f_lam_graff_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_F_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				5, HUD_COLOUR_BLUELIGHT, fLamGraffAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_F_Lam_Graff_Cleanup()
ENDSCRIPT
