// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene F_TAXI file for use – player_scene_f_taxi.sc			 ___
// ___ 																					 ___
// _________________________________________________________________________________________


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
#IF IS_DEBUG_BUILD
CONST_INT iDrawLiteralScene_row	5
#ENDIF

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_F_TAXI_STAGE_ENUM
	PS_F_TAXI_switchInProgress,
	PS_F_TAXI_STAGE_TWO,
	PS_F_TAXI_STAGE_THREE,
	
	FINISHED_F_TAXI
ENDENUM
PS_F_TAXI_STAGE_ENUM		current_player_scene_f_taxi_stage						= PS_F_TAXI_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_f_taxi_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_F_Taxi_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX			f_taxi_ped
MODEL_NAMES			f_taxi_ped_model

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX		f_taxi_veh
PED_VEH_DATA_STRUCT s_taxi_data
VECTOR				vTaxiCoordsOffset
VECTOR				vTaxiDriveOffset
FLOAT				fTaxiDriveSpeed
BOOL				bUnfrozen_taxi_position = FALSE


	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_f_taxi_coord
FLOAT				fPlayer_scene_f_taxi_head

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tBuddyTaxiAnimDict, tBuddyTaxiAnim

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fFranTaxiAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_f_taxi_widget
INT				iCurrent_player_scene_f_taxi_stage
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_F_Taxi_Cleanup()
	//- mark peds as no longer needed -//
	IF NOT IS_ENTITY_DEAD(f_taxi_ped)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(f_taxi_ped)
		SET_PED_AS_NO_LONGER_NEEDED(f_taxi_ped)
	ENDIF
	
	//- mark vehicle as no longer needed -//
	IF NOT IS_ENTITY_DEAD(f_taxi_veh)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(f_taxi_veh)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(f_taxi_veh)
	ENDIF
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	SET_VEHICLE_MODEL_IS_SUPPRESSED(f_taxi_ped_model, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(f_taxi_ped_model)
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_f_taxi_widget)
		DELETE_WIDGET_GROUP(player_scene_f_taxi_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_F_Taxi_Finished()
	//CLEAR_PRINTS()
	bPlayer_Scene_F_Taxi_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_F_Taxi_Variables()
	
	selected_player_scene_f_taxi_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_x_WHATEVER_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_f_taxi_scene, vPlayer_scene_f_taxi_coord, fPlayer_scene_f_taxi_head, tPlayer_scene_x_WHATEVER_room)
	
	//- vectors -//
	//- floats -//
	//- ints -//
	
	f_taxi_ped_model = A_M_M_EastSA_02
	
	FLOAT fVehHeadOffset
	GET_PLAYER_VEH_POSITION_FOR_SCENE(CHAR_FRANKLIN, selected_player_scene_f_taxi_scene,
			s_taxi_data, vTaxiCoordsOffset, fVehHeadOffset,
			vTaxiDriveOffset, fTaxiDriveSpeed)
	
	//-- structs : PS_F_TAXI_STRUCT --//
	
	PED_SCENE_STRUCT sPedScene
	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
	
	sPedScene.iStage = 0
	sPedScene.eScene = selected_player_scene_f_taxi_scene
	sPedScene.ePed = g_sPlayerPedRequest.ePed
	SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
	
	ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
	GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_f_taxi_scene,
			tBuddyTaxiAnimDict, tBuddyTaxiAnim, tBuddyTaxiAnim,
			buddyAnimLoopFlag, buddyAnimOutFlag)
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_F_Taxi()
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	REQUEST_MODEL(f_taxi_ped_model)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(f_taxi_ped_model, TRUE)
	
	f_taxi_veh = GET_RANDOM_VEHICLE_IN_SPHERE(vPlayer_scene_f_taxi_coord+vTaxiCoordsOffset,
			15.0, s_taxi_data.model,
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
	
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	
	WHILE NOT DOES_ENTITY_EXIST(g_pScene_buddy)
	OR NOT DOES_ENTITY_EXIST(f_taxi_veh)
	OR NOT HAS_MODEL_LOADED(f_taxi_ped_model)
	AND (iWaitForBuddyAssets < 400)
		
		IF NOT DOES_ENTITY_EXIST(f_taxi_veh)
			f_taxi_veh = GET_RANDOM_VEHICLE_IN_SPHERE(vPlayer_scene_f_taxi_coord+vTaxiCoordsOffset,
					15.0, s_taxi_data.model,
					VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		INT iLoadingAssets = 0
		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
			TEXT_LABEL_63 str = ("g_pScene_buddy")
			DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
		ENDIF
		IF NOT DOES_ENTITY_EXIST(f_taxi_veh)
			TEXT_LABEL_63 str = ("f_taxi_veh")
			DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
		ENDIF
		IF NOT HAS_MODEL_LOADED(f_taxi_ped_model)
			TEXT_LABEL_63 str = ("f_taxi_ped_model[")
			str += (GET_MODEL_NAME_FOR_DEBUG(f_taxi_ped_model))
			str += ("]")
			DrawLiteralSceneString(str, iDrawLiteralScene_row+iLoadingAssets, HUD_COLOUR_REDLIGHT)
			iLoadingAssets++
		ENDIF
		#ENDIF
		
		iWaitForBuddyAssets++
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
	
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
//		IF NOT IS_PED_INJURED(g_pScene_buddy)
//			TASK_STAND_STILL(g_pScene_buddy, -1)
//			SET_PED_KEEP_TASK(g_pScene_buddy, TRUE)
//		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(g_pScene_buddy, RELGROUPHASH_FAMILY_F)
		SET_AMBIENT_VOICE_NAME(g_pScene_buddy, "A_F_Y_BevHills_01_White_FULL_01")
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(f_taxi_veh)
		SET_VEHICLE_ENGINE_ON(f_taxi_veh, TRUE, TRUE)
		SET_VEHICLE_DOORS_LOCKED(f_taxi_veh, VEHICLELOCK_LOCKED)
		
		SET_VEHICLE_ON_GROUND_PROPERLY(f_taxi_veh)
		
		f_taxi_ped = CREATE_PED_INSIDE_VEHICLE(f_taxi_veh, PEDTYPE_CIVMALE, f_taxi_ped_model, VS_DRIVER)
		SET_PED_RANDOM_COMPONENT_VARIATION(f_taxi_ped)
		SET_MODEL_AS_NO_LONGER_NEEDED(f_taxi_ped_model)
	ENDIF
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_F_Taxi_Stage(PS_F_TAXI_STAGE_ENUM this_player_scene_f_taxi_stage)
	SWITCH this_player_scene_f_taxi_stage
		CASE PS_F_TAXI_switchInProgress
			RETURN "PS_F_TAXI_switchInProgress"
		BREAK
		CASE PS_F_TAXI_STAGE_TWO
			RETURN "PS_F_TAXI_STAGE_TWO"
		BREAK
		CASE PS_F_TAXI_STAGE_THREE
			RETURN "PS_F_TAXI_STAGE_THREE"
		BREAK
		
		CASE FINISHED_F_TAXI
			RETURN "FINISHED_F_TAXI"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_F_Taxi_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_F_Taxi_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_f_taxi.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_taxi_scene)
	
	player_scene_f_taxi_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_F_TAXI_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_F_Taxi_Stage(INT_TO_ENUM(PS_F_TAXI_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_f_taxi_stage", iCurrent_player_scene_f_taxi_stage)
		
		ADD_WIDGET_STRING("vTaxiDrive")
		ADD_WIDGET_VECTOR_SLIDER("vTaxiDriveOffset", vTaxiDriveOffset, -250, 250, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fTaxiDriveSpeed", fTaxiDriveSpeed, 0, 20, 0.1)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_F_Taxi_Widget()
	iCurrent_player_scene_f_taxi_stage = ENUM_TO_INT(current_player_scene_f_taxi_stage)
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_F_Taxi_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		SET_VEHICLE_ON_GROUND_PROPERLY(f_taxi_veh)
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		TASK_VEHICLE_DRIVE_TO_COORD(f_taxi_ped, f_taxi_veh,
				vTaxiDriveOffset+vPlayer_scene_f_taxi_coord,
				fTaxiDriveSpeed, DRIVINGSTYLE_NORMAL, s_taxi_data.model,
				DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, ((4.5) / 2) - 1, 10.0)
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL TaxiPeepsInjured(INT &iColumn)
	
	IF NOT IS_PED_INJURED(g_pScene_buddy)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("g_pScene_buddy ok", iColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("g_pScene_buddy dead", iColumn, HUD_COLOUR_REDLIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
		current_player_scene_f_taxi_stage = FINISHED_F_TAXI
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(f_taxi_ped)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("f_taxi_ped ok", iColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("f_taxi_ped dead", iColumn, HUD_COLOUR_REDLIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
		current_player_scene_f_taxi_stage = FINISHED_F_TAXI
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(f_taxi_veh)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("f_taxi_veh ok", iColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("f_taxi_veh dead", iColumn, HUD_COLOUR_REDLIGHT, fFranTaxiAlphaMult)
		iColumn++
		#ENDIF
		
		current_player_scene_f_taxi_stage = FINISHED_F_TAXI
		RETURN TRUE
	ENDIF
	
	UNUSED_PARAMETER(iColumn)
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_F_TAXI_switchInProgress()
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_TAXI_STAGE_TWO()
	
	INT iStageTwoColumn
	#IF IS_DEBUG_BUILD
	iStageTwoColumn = 1+iDrawLiteralScene_row
	#ENDIF
	
	IF NOT TaxiPeepsInjured(iStageTwoColumn)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("Loop_SynchSceneID:",
					GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID),
					iStageTwoColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
			iStageTwoColumn++
			#ENDIF
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			FLOAT fExitSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneStringFloat("Exit_SynchSceneID:",
					fExitSynchScenePhase,
					iStageTwoColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
			iStageTwoColumn++
			#ENDIF
		
			IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(g_pScene_buddy, f_taxi_veh, VS_BACK_RIGHT)
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("not in veh",
						iStageTwoColumn, HUD_COLOUR_redLIGHT, fFranTaxiAlphaMult)
				iStageTwoColumn++
				#ENDIF
				
				IF fExitSynchScenePhase >= 0.90
					SET_PED_INTO_VEHICLE(g_pScene_buddy, f_taxi_veh, VS_BACK_RIGHT)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("in veh",
						iStageTwoColumn, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
				iStageTwoColumn++
				#ENDIF
				
			ENDIF
			
			IF NOT bUnfrozen_taxi_position
				
				#IF IS_DEBUG_BUILD
				MODEL_NAMES EntityModel = GET_ENTITY_MODEL(f_taxi_ped)
				PRINTSTRING("ar_FREEZE_")
				
				IF IS_ENTITY_A_PED(f_taxi_ped)
					PRINTSTRING("PED_")
				ELIF IS_ENTITY_A_VEHICLE(f_taxi_ped)
					PRINTSTRING("VEH_")
				ELSE
					PRINTSTRING("ENTITY_")
				ENDIF
				
				PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(EntityModel))
				
				PRINTSTRING("_POSITION[")
				PRINTBOOL(FALSE)
				PRINTSTRING("], ")
				PRINTVECTOR(GET_ENTITY_COORDS(f_taxi_ped))
				PRINTNL()
				#ENDIF
				
				FREEZE_ENTITY_POSITION(f_taxi_ped, FALSE)
				bUnfrozen_taxi_position = TRUE
			ENDIF
			
			TASK_VEHICLE_TEMP_ACTION(f_taxi_ped, f_taxi_veh, TEMPACT_HANDBRAKESTRAIGHT, 0050)
		ELSE
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("Exit_SynchSceneID: END",
					iStageTwoColumn, HUD_COLOUR_REDLIGHT, fFranTaxiAlphaMult)
			iStageTwoColumn++
			#ENDIF
			
			IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(g_pScene_buddy, f_taxi_veh, VS_BACK_RIGHT)
				SET_PED_INTO_VEHICLE(g_pScene_buddy, f_taxi_veh, VS_BACK_RIGHT)
			ENDIF
			
			STOP_SYNCHRONIZED_ENTITY_ANIM(f_taxi_veh, NORMAL_BLEND_OUT, TRUE)
			WAIT(0100)
				
			IF NOT TaxiPeepsInjured(iStageTwoColumn)
				SEQUENCE_INDEX siseq
				OPEN_SEQUENCE_TASK(siseq)
					TASK_VEHICLE_TEMP_ACTION(NULL, f_taxi_veh, TEMPACT_BRAKE, 0050)
					TASK_PAUSE(NULL, 2000)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, f_taxi_veh,
							vTaxiDriveOffset+vPlayer_scene_f_taxi_coord,
							fTaxiDriveSpeed, DRIVINGSTYLE_NORMAL, s_taxi_data.model,
							DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, ((4.5) / 2) - 1, 10.0)
				CLOSE_SEQUENCE_TASK(siseq)
				TASK_PERFORM_SEQUENCE(f_taxi_ped, siseq)
				CLEAR_SEQUENCE_TASK(siseq)
				
				RETURN TRUE
	   		ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_TAXI_STAGE_THREE()
	
	INT iStageThreeColumn
	#IF IS_DEBUG_BUILD
	iStageThreeColumn = 2+iDrawLiteralScene_row
	#ENDIF
	
	IF NOT TaxiPeepsInjured(iStageThreeColumn)
		
		VECTOR f_taxi_ped_coord = GET_ENTITY_COORDS(f_taxi_ped, FALSE)
		FLOAT f_taxi_ped_dist2 = VDIST2(vTaxiDriveOffset+vPlayer_scene_f_taxi_coord, f_taxi_ped_coord)
		
		CONST_FLOAT fCONST_taxi_ped_reached 4.5
		IF f_taxi_ped_dist2 < (fCONST_taxi_ped_reached*fCONST_taxi_ped_reached)
			TASK_VEHICLE_DRIVE_WANDER(f_taxi_ped, f_taxi_veh, fTaxiDriveSpeed, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		DrawDebugSceneLineBetweenCoords(
				vTaxiDriveOffset+vPlayer_scene_f_taxi_coord,
				f_taxi_ped_coord,
				HUD_COLOUR_YELLOW)
		DrawDebugSceneSphere(vTaxiDriveOffset+vPlayer_scene_f_taxi_coord, fCONST_taxi_ped_reached, HUD_COLOUR_YELLOW, 0.25)
		DrawLiteralSceneStringFloat(
				"dist: ", SQRT(f_taxi_ped_dist2),
				iStageThreeColumn, HUD_COLOUR_YELLOW, fFranTaxiAlphaMult)
		iStageThreeColumn++
		#ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_F_Taxi_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_F_Taxi_Variables()
	Setup_Player_Scene_F_Taxi()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_F_Taxi_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_F_Taxi_in_progress
	AND ProgressScene(BIT_FRANKLIN, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_f_taxi_stage
			CASE PS_F_TAXI_switchInProgress
				IF Do_PS_F_TAXI_switchInProgress()
					current_player_scene_f_taxi_stage = PS_F_TAXI_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_F_TAXI_STAGE_TWO
				IF Do_PS_F_TAXI_STAGE_TWO()
					current_player_scene_f_taxi_stage = PS_F_TAXI_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_F_TAXI_STAGE_THREE
				IF Do_PS_F_TAXI_STAGE_THREE()
					current_player_scene_f_taxi_stage = FINISHED_F_TAXI
				ENDIF
			BREAK
			
			CASE FINISHED_F_TAXI
				Player_Scene_F_Taxi_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_F_Taxi_Widget()
		Player_Scene_F_Taxi_Debug_Options()
		
		fFranTaxiAlphaMult = 1.0
		IF current_player_scene_f_taxi_stage = PS_F_TAXI_STAGE_TWO
			
			CONST_FLOAT fMAX_PLAYER_LAM_SHOP_DIST	60.0
			FLOAT fPlayerFranTaxiDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_f_taxi_coord)
			
//			INT iPed
//			REPEAT COUNT_OF(F_TAXI_ped) iPed
//				IF DOES_ENTITY_EXIST(F_TAXI_ped[iPed])
//					FLOAT fChasePedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(F_TAXI_ped[iPed], FALSE))
//					
//					IF (fPlayerFranTaxiDist > fChasePedDist)
//						fPlayerFranTaxiDist = fChasePedDist
//					ENDIF
//				ENDIF
//			ENDREPEAT
			
			fFranTaxiAlphaMult = 1.0 - (fPlayerFranTaxiDist/fMAX_PLAYER_LAM_SHOP_DIST)
			IF fFranTaxiAlphaMult < 0
				fFranTaxiAlphaMult = 0
			ENDIF
			fFranTaxiAlphaMult = (fFranTaxiAlphaMult*0.8)+0.2
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_F_TAXI_scene,
				iDrawLiteralScene_row, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_F_TAXI_Stage(current_player_scene_f_taxi_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_T_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				1+iDrawLiteralScene_row, HUD_COLOUR_BLUELIGHT, fFranTaxiAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_F_Taxi_Cleanup()
ENDSCRIPT
