// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene M_KIDS file for use – player_scene_m_kids.sc			 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"
USING "family_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSforkids				3

CONST_INT iNUM_OF_KIDS					2

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_M_KIDS_STAGE_ENUM
	PS_M_KIDS_switchInProgress,
	PS_M_KIDS_STAGE_TWO,
	PS_M_KIDS_STAGE_THREE,
	PS_M_KIDS_STAGE_FOUR,
	
	FINISHED_M_KIDS
ENDENUM
PS_M_KIDS_STAGE_ENUM		current_player_scene_m_kids_stage						= PS_M_KIDS_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_m_kids_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_M_Kids_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				kids_ped[iNUM_OF_KIDS]
enumCharacterList		eKidsChar[iNUM_OF_KIDS]
INT						iKidsStage[iNUM_OF_KIDS]
enumFamilyMember		eKidsFamilyMember[iNUM_OF_KIDS]
enumFamilyEvents		eKidsFamilyEvent[iNUM_OF_KIDS]
REL_GROUP_HASH			kidsGroup

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX			kids_veh[iNUM_OF_KIDS]
MODEL_NAMES				eVehModel[iNUM_OF_KIDS]
VECTOR					vVeh_coordOffset[iNUM_OF_KIDS]
FLOAT					fVeh_headOffset[iNUM_OF_KIDS]
TEXT_LABEL_63			tVehAnimLoop[iNUM_OF_KIDS], tVehAnimOut[iNUM_OF_KIDS]

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_m_kids_coord
FLOAT				fPlayer_scene_m_kids_head

VECTOR				vKids_coordOffset[iNUM_OF_KIDS]
FLOAT				fKids_headOffset[iNUM_OF_KIDS]

TEXT_LABEL_63		tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
TEXT_LABEL_63		tKidsAnimLoop[iNUM_OF_KIDS], tKidsAnimOut[iNUM_OF_KIDS]

TEXT_LABEL			tKidsSceneSpeechLabel
FLOAT				fKidsSceneSpeechPhase
BOOL				bKidsSceneSpeechPreload, bKidsSceneSpeechHeard

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL	tRecordingName	= ""
INT			iRecordingNum[iNUM_OF_KIDS]
FLOAT		fPlayerRecordingStart, fPlayerRecordingSkip, fPlayerSpeedSwitch

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fKidsAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_m_kids_widget
INT				iCurrent_player_scene_m_kids_stage
BOOL			bMovePeds = FALSE, bRecord_vehs = FALSE
#ENDIF

	//- other Ints(INT) -//
INT				iFamily_synch_scene

	//- other Floats(FLOAT) -//
FLOAT fExitScene = 0.97

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_M_Kids_Cleanup()
	INT iPed
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eKidsChar) iPed
		IF (eKidsChar[iPed] <> NO_CHARACTER)
			SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(eKidsChar[iPed])
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
		AND (iRecordingNum[iPed] > 0)
			REMOVE_VEHICLE_RECORDING(iRecordingNum[iPed], tRecordingName)
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	REMOVE_RELATIONSHIP_GROUP(kidsGroup)
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_m_kids_widget)
		DELETE_WIDGET_GROUP(player_scene_m_kids_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_M_Kids_Finished()
	//CLEAR_CprintS()
	bPlayer_Scene_M_Kids_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_M_Kids_Variables()
	selected_player_scene_m_kids_scene	= g_eRecentlySelectedScene
	
	TEXT_LABEL_31 tPlayer_scene_m_kids_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_m_kids_scene, vPlayer_scene_m_kids_coord, fPlayer_scene_m_kids_head, tPlayer_scene_m_kids_room)
	
	ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
	
	GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_m_kids_scene,
			tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
			playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
	
	INT iPlayerRecordingNum
	FLOAT fPlayerRecordingStop
	FLOAT fPlayerSpeedExit
	
	GET_PLAYER_VEH_RECORDING_FOR_SCENE(selected_player_scene_m_kids_scene,
			tRecordingName, iPlayerRecordingNum,
			fPlayerRecordingStart, fPlayerRecordingSkip, fPlayerRecordingStop,
			fPlayerSpeedSwitch, fPlayerSpeedExit)
	
	SWITCH selected_player_scene_m_kids_scene
		CASE PR_SCENE_M2_KIDS_TV
			//- vectors -//
			vKids_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eKidsChar[0] = CHAR_JIMMY
			
			eKidsFamilyMember[0] = FM_MICHAEL_SON
			
			eKidsFamilyEvent[0] = FE_M2_SON_watching_TV
			
			g_eCurrentFamilyEvent[eKidsFamilyMember[0]] = FAMILY_MEMBER_BUSY
			
			//- text labels -//
			tKidsAnimLoop[0]	= "BASE_Jimmy"
			tKidsAnimOut[0]		= "EXIT_Jimmy"
			
			//
			vKids_coordOffset[1] = <<0,0,0>>
			fKids_headOffset[1] = 0
			eKidsChar[1] = NO_CHARACTER
			iRecordingNum[1] = -1
		BREAK
		BREAK
		CASE PR_SCENE_M7_KIDS_TV
			//- vectors -//
			vKids_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			vKids_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eKidsChar[0] = CHAR_JIMMY
			eKidsChar[1] = CHAR_TRACEY
			
			eKidsFamilyMember[0] = FM_MICHAEL_SON
			eKidsFamilyMember[1] = FM_MICHAEL_DAUGHTER
			
			eKidsFamilyEvent[0] = FE_M7_SON_watching_TV_with_tracey
			eKidsFamilyEvent[1] = FE_M7_SON_watching_TV_with_tracey
			
			g_eCurrentFamilyEvent[eKidsFamilyMember[0]] = FAMILY_MEMBER_BUSY
			g_eCurrentFamilyEvent[eKidsFamilyMember[1]] = FAMILY_MEMBER_BUSY
			
			//- text labels -//
			tKidsAnimLoop[0]	= "001520_02_MICS3_14_TV_W_KIDS_IDLE_JMY"
			tKidsAnimOut[0]		= "001520_02_MICS3_14_TV_W_KIDS_EXIT_JMY"
			
			tKidsAnimLoop[1]	= "001520_02_MICS3_14_TV_W_KIDS_IDLE_TRC"
			tKidsAnimOut[1]		= "001520_02_MICS3_14_TV_W_KIDS_EXIT_TRC"
			
//			tKidsSceneSpeechLabel = CreateRandomSpeechLabel("MICS3_IG_15")
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			//- vectors -//
			vKids_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			vKids_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eKidsChar[0] = CHAR_AMANDA
			eKidsChar[1] = CHAR_TRACEY
			
			eKidsFamilyMember[0] = FM_MICHAEL_WIFE
			eKidsFamilyMember[1] = FM_MICHAEL_DAUGHTER
			
			eKidsFamilyEvent[0] = FE_ANY_find_family_event	//FE_M7_FAMILY_finished_breakfast
			eKidsFamilyEvent[1] = FE_ANY_find_family_event	//FE_M7_FAMILY_finished_breakfast
			
			g_eCurrentFamilyEvent[eKidsFamilyMember[0]] = FAMILY_MEMBER_BUSY
			g_eCurrentFamilyEvent[eKidsFamilyMember[1]] = FAMILY_MEMBER_BUSY
			
/*
AROUND_THE_TABLE_SELFISH_BASE_Chair.anim
AROUND_THE_TABLE_SELFISH_Chair.anim
AROUND_THE_TABLE_SELFISH_EXIT_LOOP_Chair.anim

AROUND_THE_TABLE_SELFISH_BASE_Lap_Top.anim
AROUND_THE_TABLE_SELFISH_Lap_Top.anim
AROUND_THE_TABLE_SELFISH_EXIT_LOOP_Lap_Top.anim

AROUND_THE_TABLE_SELFISH_BASE_Amanda.anim
AROUND_THE_TABLE_SELFISH_Amanda.anim

AROUND_THE_TABLE_SELFISH_BASE_Jimmy.anim
AROUND_THE_TABLE_SELFISH_Jimmy.anim
AROUND_THE_TABLE_SELFISH_EXIT_LOOP_Jimmy.anim

AROUND_THE_TABLE_SELFISH_BASE_Michael.anim
AROUND_THE_TABLE_SELFISH_Michael.anim

AROUND_THE_TABLE_SELFISH_BASE_Tracy.anim
AROUND_THE_TABLE_SELFISH_Tracy.anim

*/
			
			//- text labels -//
			tKidsAnimLoop[0]	= "AROUND_THE_TABLE_SELFISH_BASE_Amanda"
			tKidsAnimOut[0]		= "AROUND_THE_TABLE_SELFISH_Amanda"
			
			tKidsAnimLoop[1]	= "AROUND_THE_TABLE_SELFISH_BASE_Tracy"
			tKidsAnimOut[1]		= "AROUND_THE_TABLE_SELFISH_Tracy"
			
			tKidsSceneSpeechLabel = CreateRandomSpeechLabel("MICS3_IG_12")
			fKidsSceneSpeechPhase = 0.0
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			//- vectors -//
			vKids_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fKids_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eKidsChar[0] = CHAR_JIMMY
			eKidsFamilyMember[0] = FM_MICHAEL_SON
			eKidsFamilyEvent[0] = FE_ANY_find_family_event
			
			g_eCurrentFamilyEvent[eKidsFamilyMember[0]] = FAMILY_MEMBER_BUSY
			
			iRecordingNum[0] = 002
			
			//- text labels -//
			tKidsAnimLoop[0]	= ""	//"LOOP_Jimmy"
			tKidsAnimOut[0]		= "EXIT_Jimmy"
			
			//- vehicle -//
			eVehModel[0]		= SCORCHER	//TRIBIKE
			vVeh_coordOffset[0]	= <<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>
			fVeh_headOffset[0]	= GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			tVehAnimLoop[0]		= ""	//"LOOP_TriBike"
			tVehAnimOut[0]		= "EXIT_TriBike"
			
			//
			vKids_coordOffset[1] = <<0,0,0>>
			fKids_headOffset[1] = 0
			eKidsChar[1] = NO_CHARACTER
			iRecordingNum[1] = -1
			
			tKidsSceneSpeechLabel = "MICS3_IG_10"
			fKidsSceneSpeechPhase = 0.2
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_m_kids_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_kids_scene)
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			vKids_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vKids_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			//- floats -//
			fKids_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fKids_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			//- enums -//
			eKidsChar[0] = NO_CHARACTER
			eKidsChar[1] = NO_CHARACTER
			
//			tKidsSceneSpeechLabel = ""
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_M_KIDS_STRUCT --//
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_M_Kids()
	INT iPed
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	BOOL bWaitForBuddyAssets = FALSE
	WHILE NOT bWaitForBuddyAssets
	AND (iWaitForBuddyAssets < 400)
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iRow = 1
		#ENDIF
		
//		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
//			
//			#IF IS_DEBUG_BUILD
//			TEXT_LABEL_63 str  = ("g_pScene_buddy doesnt exist")
//			DrawLiteralSceneString(str, iRow+1+iCONSTANTSforkids, HUD_COLOUR_RED)
//			iRow++
//			#ENDIF
//			
//			bWaitForBuddyAssets = FALSE
//		ENDIF
		
		REPEAT COUNT_OF(kids_ped) iPed
			IF (eKidsChar[iPed] <> NO_CHARACTER)
				REQUEST_NPC_PED_MODEL(eKidsChar[iPed])
				IF NOT HAS_NPC_PED_MODEL_LOADED(eKidsChar[iPed])
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("NPC \"")
					str += GET_PLAYER_PED_STRING(eKidsChar[iPed])
					str += "\""
					DrawLiteralSceneString(str, iRow+iCONSTANTSforkids, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					bWaitForBuddyAssets = FALSE
					REQUEST_NPC_PED_MODEL(eKidsChar[iPed])
				ENDIF
			ENDIF
			IF (eVehModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(eVehModel[iPed])
				IF NOT HAS_MODEL_LOADED(eVehModel[iPed])
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("veh \"")
					str += GET_MODEL_NAME_FOR_DEBUG(eVehModel[iPed])
					str += "\""
					DrawLiteralSceneString(str, iRow+iCONSTANTSforkids, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eVehModel[iPed])
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			AND (iRecordingNum[iPed] > 0)
				REQUEST_VEHICLE_RECORDING(iRecordingNum[iPed], tRecordingName)
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iPed], tRecordingName)
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("veh rec ")
					str += tRecordingName
					IF (iRecordingNum[iPed] < 100)	str += ("0") ENDIF
					IF (iRecordingNum[iPed] < 10)	str += ("0") ENDIF
					str += iRecordingNum[iPed]
					DrawLiteralSceneString(str, iRow+iCONSTANTSforkids, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					REQUEST_VEHICLE_RECORDING(iRecordingNum[iPed], tRecordingName)
					bWaitForBuddyAssets = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerSceneAnimDict)
			REQUEST_ANIM_DICT(tPlayerSceneAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(tPlayerSceneAnimDict)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str  = ("anim dict \"")
				str += tPlayerSceneAnimDict
				str += ("\"")
				DrawLiteralSceneString(str, iRow+iCONSTANTSforkids, HUD_COLOUR_RED)
				iRow++
				#ENDIF
				
				bWaitForBuddyAssets = FALSE
				REQUEST_ANIM_DICT(tPlayerSceneAnimDict)
			ENDIF
		ENDIF
		
		
		TEXT_LABEL_63 tFamilySynchSceneDict = "", tFamilySynchSceneClip = ""
		ANIMATION_FLAGS eFamilySynchSceneFlag = AF_DEFAULT
		enumFamilyAnimProgress eFamilySynchSceneProgress = FAP_0_default
		IF PRIVATE_Get_FamilyMember_Anim(eKidsFamilyMember[0], eKidsFamilyEvent[0],
				tFamilySynchSceneDict, tFamilySynchSceneClip, eFamilySynchSceneFlag, eFamilySynchSceneProgress)
			IF NOT IS_STRING_NULL_OR_EMPTY(tFamilySynchSceneDict)
				REQUEST_ANIM_DICT(tFamilySynchSceneDict)
				IF NOT HAS_ANIM_DICT_LOADED(tFamilySynchSceneDict)
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("anim dict \"")
					str += tFamilySynchSceneDict
					str += ("\"")
					DrawLiteralSceneString(str, iRow+iCONSTANTSforkids, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					bWaitForBuddyAssets = FALSE
					REQUEST_ANIM_DICT(tFamilySynchSceneDict)
				ENDIF
			ENDIF
				
		ENDIF
		
		IF NOT bWaitForBuddyAssets
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	REPEAT COUNT_OF(kids_veh) iPed
		IF (eVehModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			kids_veh[iPed] = CREATE_VEHICLE(eVehModel[iPed],
					vPlayer_scene_m_kids_coord+vVeh_coordOffset[iPed],
					fPlayer_scene_m_kids_head+fVeh_headOffset[iPed],
					FALSE, FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(kids_veh[iPed])
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tVehAnimLoop[iPed])
				PLAY_ENTITY_ANIM(kids_veh[iPed], tVehAnimLoop[iPed], tPlayerSceneAnimDict, NORMAL_BLEND_IN, TRUE, FALSE)
			ELSE
				START_PLAYBACK_RECORDED_VEHICLE(kids_veh[iPed], iRecordingNum[iPed], tRecordingName)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//- create any script peds -//
//	SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
	
	ADD_RELATIONSHIP_GROUP("KIDSS", kidsGroup)
	
	REPEAT COUNT_OF(kids_ped) iPed
		IF (eKidsChar[iPed] <> NO_CHARACTER)
			IF CREATE_NPC_PED_ON_FOOT(kids_ped[iPed], eKidsChar[iPed],
					vPlayer_scene_m_kids_coord+vKids_coordOffset[iPed],
					fPlayer_scene_m_kids_head+fKids_headOffset[iPed])
				PRIVATE_SetDefaultSceneBuddyCompVar(kids_ped[iPed], eKidsChar[iPed], selected_player_scene_m_kids_scene)
				
				SET_PED_RELATIONSHIP_GROUP_HASH(kids_ped[iPed], kidsGroup)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tKidsAnimLoop[iPed])
					TASK_PLAY_ANIM(kids_ped[iPed], tPlayerSceneAnimDict, tKidsAnimLoop[iPed],
							NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
							AF_LOOPING | AF_NOT_INTERRUPTABLE)
					iKidsStage[iPed] = 0
				ELIF IS_VEHICLE_DRIVEABLE(kids_veh[iPed])
					SET_PED_INTO_VEHICLE(kids_ped[iPed], kids_veh[iPed], VS_DRIVER)
					iKidsStage[iPed] = 20
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, kidsGroup, RELGROUPHASH_PLAYER)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, kidsGroup)
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_M_Kids_Stage(PS_M_KIDS_STAGE_ENUM this_player_scene_m_kids_stage)
	SWITCH this_player_scene_m_kids_stage
		CASE PS_M_KIDS_switchInProgress
			RETURN "PS_M_KIDS_switchInProgress"
		BREAK
		CASE PS_M_KIDS_STAGE_TWO
			RETURN "PS_M_KIDS_STAGE_TWO"
		BREAK
		CASE PS_M_KIDS_STAGE_THREE
			RETURN "PS_M_KIDS_STAGE_THREE"
		BREAK
		CASE PS_M_KIDS_STAGE_FOUR
			RETURN "PS_M_KIDS_STAGE_FOUR"
		BREAK
		
		CASE FINISHED_M_KIDS
			RETURN "FINISHED_M_KIDS"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_M_Kids_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_M_Kids_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_m_kids.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_m_kids_scene)
	
	player_scene_m_kids_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_M_KIDS_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_M_Kids_Stage(INT_TO_ENUM(PS_M_KIDS_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_m_kids_stage", iCurrent_player_scene_m_kids_stage)
		
		START_WIDGET_GROUP("Do_PS_M_KIDS_STAGE_TWO")
			ADD_WIDGET_VECTOR_SLIDER("vPlayer_scene_m_kids_coord", vPlayer_scene_m_kids_coord, -3000.0, 3000.0, 0.0)
			ADD_WIDGET_FLOAT_SLIDER("fPlayer_scene_m_kids_head", fPlayer_scene_m_kids_head, -360.0, 360.0, 0.0)
			
			REPEAT COUNT_OF(kids_ped) iWidget
				str = "kids_ped "
				str += iWidget
				ADD_WIDGET_STRING(str)
				ADD_WIDGET_VECTOR_SLIDER("vKids_coordOffset", vKids_coordOffset[iWidget], -30.0, 30.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fKids_headOffset", fKids_headOffset[iWidget], -180.0, 180.0, 1.0)
			ENDREPEAT
			
			ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
			ADD_WIDGET_FLOAT_SLIDER("fExitScene", fExitScene, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
			ADD_WIDGET_BOOL("bRecord_vehs", bRecord_vehs)
		ENDIF
	STOP_WIDGET_GROUP()
ENDPROC



//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_M_Kids_Record()
	INT iWidget
	INT iThisRecording = 0
	INT iThisRecordingOffset = 100
	VEHICLE_INDEX player_scene_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	IF IS_ENTITY_DEAD(player_scene_veh)
		player_scene_veh = g_vPlayerVeh[GET_CURRENT_PLAYER_PED_ENUM()]
	ENDIF
	IF IS_ENTITY_DEAD(player_scene_veh)
		player_scene_veh = GET_LAST_DRIVEN_VEHICLE()
	ENDIF
	
	CLEAR_AREA(vPlayer_scene_m_kids_coord, 50*2, TRUE)
	SET_ROADS_IN_AREA(vPlayer_scene_m_kids_coord-<<50,50,50>>,
			vPlayer_scene_m_kids_coord+<<50,50,50>>, FALSE)
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	
	IF NOT IS_ENTITY_DEAD(player_scene_veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
			STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
		ENDIF
		SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_m_kids_coord)
		SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_m_kids_head)
		SET_VEHICLE_FIXED(player_scene_veh)
	ENDIF
	
	REPEAT iNUM_OF_KIDS iWidget
		IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iWidget])
				STOP_PLAYBACK_RECORDED_VEHICLE(kids_veh[iWidget])
				
				SET_ENTITY_COORDS(kids_veh[iWidget], vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
				SET_ENTITY_HEADING(kids_veh[iWidget], fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
				SET_VEHICLE_FIXED(kids_veh[iWidget])
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(kids_ped[iWidget])
			CLEAR_PED_TASKS(kids_ped[iWidget])
		ENDIF
	ENDREPEAT
	
	WHILE bRecord_vehs
		
		IF NOT IS_ENTITY_DEAD(player_scene_veh)
		AND NOT (DOES_ENTITY_EXIST(kids_veh[0]) AND IS_ENTITY_DEAD(kids_veh[0]))
		AND NOT (DOES_ENTITY_EXIST(kids_veh[1]) AND IS_ENTITY_DEAD(kids_veh[1]))
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
					
					SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_m_kids_coord)
					SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_m_kids_head)
					SET_VEHICLE_FIXED(player_scene_veh)
				ENDIF
				
				REPEAT iNUM_OF_KIDS iWidget
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iWidget])
						STOP_PLAYBACK_RECORDED_VEHICLE(kids_veh[iWidget])
						
						SET_ENTITY_COORDS(kids_veh[iWidget], vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
						SET_ENTITY_HEADING(kids_veh[iWidget], fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
						SET_VEHICLE_FIXED(kids_veh[iWidget])
					ENDIF
				ENDREPEAT
				
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), player_scene_veh, VS_DRIVER)
			ELSE
				//
				
				VEHICLE_INDEX player_record_veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				
				IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(player_record_veh)
					
					INT iPlayerVehInt = -1
					IF (player_record_veh = player_scene_veh)
						iPlayerVehInt = COUNT_OF(kids_veh)
					ELSE
						REPEAT iNUM_OF_KIDS iWidget
							IF (player_record_veh = kids_veh[iWidget])
								iPlayerVehInt = iWidget
							ENDIF
						ENDREPEAT
					ENDIF
					
					DrawLiteralSceneStringInt("press 'UP' to record ", iPlayerVehInt, 0, HUD_COLOUR_BLUE)
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
						
						IF (player_record_veh <> player_scene_veh)
							IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
							AND (iRecordingNum[0]-1 > 0)
								REQUEST_VEHICLE_RECORDING(iRecordingNum[0]-1, tRecordingName)
								IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[0]-1, tRecordingName)
									REQUEST_VEHICLE_RECORDING(iRecordingNum[0]-1, tRecordingName)
									WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[0]-1, tRecordingName)
										DrawLiteralSceneString("HAS_VEHICLE_RECORDING_BEEN_LOADED", 0, HUD_COLOUR_BLUELIGHT)
										WAIT(0)
									ENDWHILE
								ENDIF
								
								IF NOT IS_ENTITY_DEAD(player_scene_veh)
									START_PLAYBACK_RECORDED_VEHICLE(player_scene_veh, iRecordingNum[0]-1, tRecordingName)
								ENDIF
							ENDIF
						ELSE
							iThisRecording = iRecordingNum[0]-1
						ENDIF
						
						REPEAT iNUM_OF_KIDS iWidget
							IF (player_record_veh <> kids_veh[iWidget])
								IF NOT IS_STRING_NULL_OR_EMPTY(tRecordingName)
								AND (iRecordingNum[iWidget] > 0)
									REQUEST_VEHICLE_RECORDING(iRecordingNum[iWidget], tRecordingName)
									IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iWidget], tRecordingName)
										REQUEST_VEHICLE_RECORDING(iRecordingNum[iWidget], tRecordingName)
										WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNum[iWidget], tRecordingName)
											DrawLiteralSceneString("HAS_VEHICLE_RECORDING_BEEN_LOADED", 0, HUD_COLOUR_BLUELIGHT)
											WAIT(0)
										ENDWHILE
									ENDIF
									
									IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
										START_PLAYBACK_RECORDED_VEHICLE(kids_veh[iWidget], iRecordingNum[iWidget], tRecordingName)
										
										FLOAT fTime = 0.0
										
//										VECTOR vPlayer_scene_m_kids_coord
//										FLOAT				fPlayer_scene_m_kids_head
										
										VECTOR vRecStartPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iWidget], fTime, tRecordingName)
										VECTOR vRecStartRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNum[iWidget], fTime, tRecordingName)
										
										IF NOT ARE_VECTORS_EQUAL(vRecStartPos, vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
										OR NOT (vRecStartRot.z = fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
											SAVE_STRING_TO_DEBUG_FILE("FAKE ")
											SAVE_STRING_TO_DEBUG_FILE("pos and rot of ")
											SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
											SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iWidget])
											SAVE_STRING_TO_DEBUG_FILE(": ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos)
											SAVE_STRING_TO_DEBUG_FILE(", ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartRot)
											SAVE_NEWLINE_TO_DEBUG_FILE()
											

SAVE_STRING_TO_DEBUG_FILE("	vVeh_coordOffset[")
SAVE_INT_TO_DEBUG_FILE(iWidget)
SAVE_STRING_TO_DEBUG_FILE("] = ")
SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos-vPlayer_scene_m_kids_coord)
SAVE_STRING_TO_DEBUG_FILE("		//")
SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_m_kids_coord)
SAVE_NEWLINE_TO_DEBUG_FILE()

SAVE_STRING_TO_DEBUG_FILE("	fVeh_headOffset[")
SAVE_INT_TO_DEBUG_FILE(iWidget)
SAVE_STRING_TO_DEBUG_FILE("] = ")
SAVE_FLOAT_TO_DEBUG_FILE(vRecStartRot.z-fPlayer_scene_m_kids_head)
SAVE_STRING_TO_DEBUG_FILE("		//")
SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_m_kids_head)
SAVE_NEWLINE_TO_DEBUG_FILE()
											
											
										ELSE
											SAVE_STRING_TO_DEBUG_FILE("real ")
											SAVE_STRING_TO_DEBUG_FILE("pos and rot of ")
											SAVE_STRING_TO_DEBUG_FILE(tRecordingName)
											SAVE_INT_TO_DEBUG_FILE(iRecordingNum[iWidget])
											SAVE_STRING_TO_DEBUG_FILE(": ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartPos)
											SAVE_STRING_TO_DEBUG_FILE(", ")
											SAVE_VECTOR_TO_DEBUG_FILE(vRecStartRot)
											SAVE_NEWLINE_TO_DEBUG_FILE()
										ENDIF
										
										
									ENDIF
								ENDIF
							ELSE
								iThisRecording = iRecordingNum[iWidget]
							ENDIF
						ENDREPEAT
						
						IF NOT IS_ENTITY_DEAD(player_record_veh)
							START_RECORDING_VEHICLE(player_record_veh,
									iThisRecording+iThisRecordingOffset,
									tRecordingName, TRUE)
							
							CPRINTLN(DEBUG_SWITCH, "START_RECORDING_VEHICLE(player_record_veh, ", iThisRecording+iThisRecordingOffset, ", \"", tRecordingName, "\")")
						ENDIF
					ENDIF
					
					INT iCHASECAR_VEH_MAX = 0
					REPEAT iNUM_OF_KIDS iWidget
						IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
							iCHASECAR_VEH_MAX++
						ENDIF
					ENDREPEAT
					
					
					DrawLiteralSceneStringInt("iCHASECAR_VEH_MAX:", iCHASECAR_VEH_MAX, 1, HUD_COLOUR_RED)
					
					DrawLiteralSceneString("press 'LEFT' to change", 2, HUD_COLOUR_BLUELIGHT)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
						
						
						SAVE_STRING_TO_DEBUG_FILE("iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						iPlayerVehInt--
						
						SAVE_STRING_TO_DEBUG_FILE("--: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						IF iPlayerVehInt < 0
							
							SAVE_STRING_TO_DEBUG_FILE(" < ")
							SAVE_INT_TO_DEBUG_FILE(0)
							SAVE_STRING_TO_DEBUG_FILE("zet to max")
							
							iPlayerVehInt = iCHASECAR_VEH_MAX
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE(", iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
					ENDIF
					DrawLiteralSceneString("press 'RIGHT' to change", 3, HUD_COLOUR_BLUELIGHT)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
						
						
						SAVE_STRING_TO_DEBUG_FILE("iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						iPlayerVehInt++
						
						SAVE_STRING_TO_DEBUG_FILE("++: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						
						IF iPlayerVehInt > iCHASECAR_VEH_MAX
							
							SAVE_STRING_TO_DEBUG_FILE(" > ")
							SAVE_INT_TO_DEBUG_FILE(iCHASECAR_VEH_MAX)
							SAVE_STRING_TO_DEBUG_FILE("zet to zero")
							
							iPlayerVehInt = 0
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE(", iPlayerVehInt: ")
						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
					ENDIF
					
					VEHICLE_INDEX new_record_veh
					IF (iPlayerVehInt = iCHASECAR_VEH_MAX)
					OR (iPlayerVehInt = COUNT_OF(kids_veh))
						
//						SAVE_STRING_TO_DEBUG_FILE("new_record_veh = player_scene_veh")
//						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						new_record_veh = player_scene_veh
					ELSE
						
//						SAVE_STRING_TO_DEBUG_FILE("new_record_veh = kids_veh[")
//						SAVE_INT_TO_DEBUG_FILE(iPlayerVehInt)
//						SAVE_STRING_TO_DEBUG_FILE("]")
//						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						new_record_veh = kids_veh[iPlayerVehInt]
					ENDIF
					
					IF (new_record_veh <> player_record_veh)
						
						CLEAR_AREA(vPlayer_scene_m_kids_coord, 100.0, TRUE)
						
						PED_INDEX player_scene_ped = GET_PED_IN_VEHICLE_SEAT(new_record_veh, VS_DRIVER)
						
						SET_ENTITY_COORDS(player_scene_ped, GET_ENTITY_COORDS(player_record_veh)+<<1,1,1>>)
						
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), new_record_veh, VS_DRIVER)
						SET_PED_INTO_VEHICLE(player_scene_ped, player_record_veh, VS_DRIVER)
						
						IF NOT IS_ENTITY_DEAD(player_scene_veh)
							SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_m_kids_coord)
							SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_m_kids_head)
							SET_VEHICLE_FIXED(player_scene_veh)
						ENDIF
						
						REPEAT iNUM_OF_KIDS iWidget
							IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
								SET_ENTITY_COORDS(kids_veh[iWidget], vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
								SET_ENTITY_HEADING(kids_veh[iWidget], fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
								SET_VEHICLE_FIXED(kids_veh[iWidget])
							ENDIF
							IF NOT IS_ENTITY_DEAD(kids_ped[iWidget])
								CLEAR_PED_TASKS(kids_ped[iWidget])
							ENDIF
						ENDREPEAT
						
					ENDIF
					
				ELSE
					DrawLiteralSceneString("RECORDING!", 0, HUD_COLOUR_RED)
					DrawLiteralSceneStringInt(tRecordingName, iThisRecording+iThisRecordingOffset, 1, HUD_COLOUR_RED)
					DrawLiteralSceneStringFloat("recording: ", GET_TIME_POSITION_IN_RECORDED_RECORDING(player_record_veh), 2, HUD_COLOUR_RED)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
						ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(player_scene_veh, RDM_WHOLELINE)
					ENDIF
					
					REPEAT iNUM_OF_KIDS iWidget
						IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iWidget])
								ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(kids_veh[iWidget], RDM_WHOLELINE)
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
						STOP_RECORDING_VEHICLE(player_record_veh)
						
						CPRINTLN(DEBUG_SWITCH, "STOP_RECORDING_VEHICLE(player_record_veh, ", iThisRecording+iThisRecordingOffset, ", \"", tRecordingName, "\")")
						
						iThisRecordingOffset += 10
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_scene_veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(player_scene_veh)
						ENDIF
						SET_ENTITY_COORDS(player_scene_veh, vPlayer_scene_m_kids_coord)
						SET_ENTITY_HEADING(player_scene_veh, fPlayer_scene_m_kids_head)
						SET_VEHICLE_FIXED(player_scene_veh)
						
						REPEAT iNUM_OF_KIDS iWidget
							IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iWidget])
									STOP_PLAYBACK_RECORDED_VEHICLE(kids_veh[iWidget])
								ENDIF
								SET_ENTITY_COORDS(kids_veh[iWidget], vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
								SET_ENTITY_HEADING(kids_veh[iWidget], fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
								SET_VEHICLE_FIXED(kids_veh[iWidget])
							ENDIF
						ENDREPEAT
						
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
			bRecord_vehs = FALSE
			
			IF IS_ENTITY_DEAD(player_scene_veh)
				SAVE_STRING_TO_DEBUG_FILE("IS_ENTITY_DEAD(player_scene_veh)")
				
				player_scene_veh = CREATE_VEHICLE(Cruiser,
						vPlayer_scene_m_kids_coord+<<GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0), 0.0000>>,
						fPlayer_scene_m_kids_head+GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0),
						FALSE, FALSE)
				
			ENDIF
			IF (DOES_ENTITY_EXIST(kids_veh[0]) AND IS_ENTITY_DEAD(kids_veh[0]))
				SAVE_STRING_TO_DEBUG_FILE("DOES_ENTITY_EXIST(kids_veh[0]) AND IS_ENTITY_DEAD(kids_veh[0])")
			ENDIF
			IF (DOES_ENTITY_EXIST(kids_veh[1]) AND IS_ENTITY_DEAD(kids_veh[1]))
				SAVE_STRING_TO_DEBUG_FILE("DOES_ENTITY_EXIST(kids_veh[1]) AND IS_ENTITY_DEAD(kids_veh[1])")
			ENDIF
			
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	SET_ROADS_BACK_TO_ORIGINAL(vPlayer_scene_m_kids_coord-<<50,50,50>>,
			vPlayer_scene_m_kids_coord+<<50,50,50>>)
	bRecord_vehs = FALSE
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_M_Kids_Widget()
	INT iWidget
	iCurrent_player_scene_m_kids_stage = ENUM_TO_INT(current_player_scene_m_kids_stage)
	
	IF NOT bMovePeds
	ELSE
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		
			REPEAT COUNT_OF(kids_ped) iWidget
				IF NOT IS_PED_INJURED(kids_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(kids_ped[iWidget], tPlayerSceneAnimDict, tKidsAnimLoop[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(kids_ped[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
								tPlayerSceneAnimDict, tKidsAnimLoop[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
						ikidsStage[iWidget] = 0
					ENDIF
					
					vkids_coordOffset[iWidget] = GET_ENTITY_COORDS(kids_ped[iWidget]) - vPlayer_scene_m_kids_coord
					fkids_headOffset[iWidget] = GET_ENTITY_HEADING(kids_ped[iWidget]) - fPlayer_scene_m_kids_head

					IF fkids_headOffset[iWidget] > 180.0
						fkids_headOffset[iWidget] -= 360.0
					ENDIF
					IF fkids_headOffset[iWidget] < -180.0
						fkids_headOffset[iWidget] += 360.0
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(kids_veh[iWidget], tPlayerSceneAnimDict, tVehAnimLoop[iWidget], ANIM_SYNCED_SCENE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(kids_veh[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
								tVehAnimLoop[iWidget], tPlayerSceneAnimDict, NORMAL_BLEND_IN)
					ENDIF
					
					vVeh_coordOffset[iWidget] = GET_ENTITY_COORDS(kids_veh[iWidget]) - vPlayer_scene_m_kids_coord
					fVeh_headOffset[iWidget] = GET_ENTITY_HEADING(kids_veh[iWidget]) - fPlayer_scene_m_kids_head

					IF fVeh_headOffset[iWidget] > 180.0
						fVeh_headOffset[iWidget] -= 360.0
					ENDIF
					IF fVeh_headOffset[iWidget] < -180.0
						fVeh_headOffset[iWidget] += 360.0
					ENDIF
				ENDIF
			ENDREPEAT
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		
			REPEAT COUNT_OF(kids_ped) iWidget
				IF NOT IS_PED_INJURED(kids_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(kids_ped[iWidget], tPlayerSceneAnimDict, tKidsAnimOut[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(kids_ped[iWidget], g_iPlayer_Timetable_exit_SynchSceneID,
								tPlayerSceneAnimDict, tKidsAnimOut[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(kids_veh[iWidget], tPlayerSceneAnimDict, tVehAnimOut[iWidget], ANIM_SYNCED_SCENE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(kids_veh[iWidget], g_iPlayer_Timetable_exit_SynchSceneID,
								tVehAnimOut[iWidget], tPlayerSceneAnimDict, NORMAL_BLEND_IN)
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
		
			REPEAT COUNT_OF(kids_ped) iWidget
				IF NOT IS_PED_INJURED(kids_ped[iWidget])
					SET_ENTITY_HEADING(kids_ped[iWidget], fPlayer_scene_m_kids_head+fkids_headOffset[iWidget])
					SET_ENTITY_COORDS(kids_ped[iWidget], vPlayer_scene_m_kids_coord+vkids_coordOffset[iWidget])
				ENDIF
				IF NOT IS_ENTITY_DEAD(kids_veh[iWidget])
					SET_ENTITY_HEADING(kids_veh[iWidget], fPlayer_scene_m_kids_head+fVeh_headOffset[iWidget])
					SET_ENTITY_COORDS(kids_veh[iWidget], vPlayer_scene_m_kids_coord+vVeh_coordOffset[iWidget])
				ENDIF
			ENDREPEAT
		ENDIF
		
	ENDIF
	IF bRecord_vehs
		Watch_Player_Scene_M_Kids_Record()
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_M_Kids_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_m_kids_stage = FINISHED_M_KIDS
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

PROC ControlKidsPedStage(PED_INDEX thisKidsPed, INT iPed)
	CONST_INT	iCONST_KIDS_0_waitForExit 				0
	CONST_INT	iCONST_KIDS_5_exitSynch 				5
	
	CONST_INT	iCONST_KIDS_20_vehRec					20
	
	CONST_INT	iCONST_KIDS_10_postExit 				10
	
	CONST_INT	iCONST_KIDS_1_release 					1
	CONST_INT	iCONST_KIDS_2_family 					2
	
	CONST_INT	iCONST_KIDS_DEAD						-1
	
	#IF IS_DEBUG_BUILD
	CONST_FLOAT Offset_y	0.0
	CONST_INT iCONSTANTSforkidsstage	5+iCONSTANTSforkids+1
	#ENDIF
	
	VECTOR vFamily_scene_coord_FS_MICHAEL = << -812.0607, 179.5117, 71.1531 >>
	FLOAT fFamily_scene_head_FS_MICHAEL = 222.8314
	
	IF NOT IS_PED_INJURED(thisKidsPed)
		
		SWITCH iKidsStage[iPed]
			CASE iCONST_KIDS_0_waitForExit	//
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  waitForExit...", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("waitForExit...", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
					IF IS_ENTITY_PLAYING_ANIM(thisKidsPed, tPlayerSceneAnimDict, tKidsAnimOut[iPed], ANIM_SYNCED_SCENE)
						iKidsStage[iPed] = iCONST_KIDS_5_exitSynch
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_KIDS_5_exitSynch	//
				
				IF IS_ENTITY_PLAYING_ANIM(thisKidsPed, tPlayerSceneAnimDict, tKidsAnimOut[iPed], ANIM_SYNCED_SCENE)
					
					#IF IS_DEBUG_BUILD
					
					INT iKidsAnimOutLength
					iKidsAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tKidsAnimOut[iPed])
					
					TEXT_LABEL_63 str
					str  = " outAnim \""
					
					IF (iKidsAnimOutLength >= 15)
						str += GET_STRING_FROM_STRING(tKidsAnimOut[iPed],
								iKidsAnimOutLength - 15,
								iKidsAnimOutLength)
					ELSE
						str += tKidsAnimOut[iPed]
					ENDIF
					str += "\""
					DrawLiteralSceneString(str,
							iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
					
					#ENDIF
					
					TEXT_LABEL_63 tFamilySynchSceneDict, tFamilySynchSceneClip
					ANIMATION_FLAGS eFamilySynchSceneFlag
					enumFamilyAnimProgress eFamilySynchSceneProgress
					
					IF PRIVATE_Get_FamilyMember_Anim(eKidsFamilyMember[iPed], eKidsFamilyEvent[iPed],
							tFamilySynchSceneDict, tFamilySynchSceneClip,
							eFamilySynchSceneFlag, eFamilySynchSceneProgress)
						REQUEST_ANIM_DICT(tFamilySynchSceneDict)
						IF NOT HAS_ANIM_DICT_LOADED(tFamilySynchSceneDict)
						ENDIF
					ENDIF
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
						IF NOT IS_ENTITY_DEAD(kids_veh[iPed])
							IF IS_ENTITY_PLAYING_ANIM(kids_veh[iPed], tPlayerSceneAnimDict, tVehAnimOut[iPed], ANIM_SYNCED_SCENE)
								IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) > 0.817
									
									CPRINTLN(DEBUG_SWITCH, "GET_SYNCHRONIZED_SCENE_PHASE")
									
									STOP_SYNCHRONIZED_ENTITY_ANIM(kids_veh[iPed], NORMAL_BLEND_OUT, TRUE)
									
									VECTOR vkids_veh_coord
									vkids_veh_coord = <<-824.5328, 175.0702, 69.8919>>		//(<<-825.2491, 173.5268, 69.6639>>	+ <<-825.2491, 173.5268, 69.6639>>	+ <<-824.3973, 175.3323, 69.9365>>) / 3.0
									
									FLOAT vkids_veh_head
									vkids_veh_head = 156.6901								//(156.6450							+ 156.6450							+ 157.2428)							/ 3.0
									
									vkids_veh_coord.x = -824.333
									vkids_veh_head = 140.490
									
									SET_ENTITY_COORDS(kids_veh[iPed],	vkids_veh_coord)
									SET_ENTITY_HEADING(kids_veh[iPed],	vkids_veh_head)
									
									SET_VEHICLE_ON_GROUND_PROPERLY(kids_veh[iPed])
									
									
									
//START_WIDGET_GROUP("iCONST_KIDS_5_exitSynch")
//	ADD_WIDGET_VECTOR_SLIDER("vkids_veh_coord", vkids_veh_coord, -1000, 1000, 0.01)
//	ADD_WIDGET_FLOAT_SLIDER("vkids_veh_head", vkids_veh_head, -1000, 1000, 0.1)
//STOP_WIDGET_GROUP()
//WHILE DOES_ENTITY_EXIST(kids_veh[iPed])
//	SET_ENTITY_COORDS(kids_veh[iPed], vkids_veh_coord)
//	SET_ENTITY_HEADING(kids_veh[iPed], vkids_veh_head)
//	
//	WAIT(0)
//ENDWHILE
									
								ELSE
									CPRINTLN(DEBUG_SWITCH, "NOT GET_SYNCHRONIZED_SCENE_PHASE???")
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SWITCH, "NOT IS_ENTITY_PLAYING_ANIM")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_SWITCH, "IS_ENTITY_DEAD")
						ENDIF
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) > fExitScene //0.97
							#IF IS_DEBUG_BUILD
							DrawLiteralSceneString("  NOT playing out anim", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
							DrawDebugSceneTextWithOffset("NOT playing out anim", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
							#ENDIF
							
							CPRINTLN(DEBUG_SWITCH, "SYNCH > fExitScene")
						
							iKidsStage[iPed] = iCONST_KIDS_10_postExit
						ENDIF
					ELSE
						CPRINTLN(DEBUG_SWITCH, "SYNCH not running")
					
						iKidsStage[iPed] = iCONST_KIDS_10_postExit
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "NOT IS_ENTITY_PLAYING_ANIM")
					
					iKidsStage[iPed] = iCONST_KIDS_10_postExit
				ENDIF
			BREAK
			
			CASE iCONST_KIDS_20_vehRec
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  vehRec...", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("vehRec...", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
				
				#IF IS_DEBUG_BUILD
				IF NOT IS_ENTITY_DEAD(kids_veh[iPed])
					ar_DISPLAY_PLAYBACK_RECORDED_VEHICLE(kids_veh[iPed], RDM_WHOLELINE)
				ENDIF
				#ENDIF
				
			BREAK
			
			CASE iCONST_KIDS_10_postExit
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  postExit...", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("postExit...", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
//				TEXT_LABEL_63 tAnimDict, tAnimClip
//				IF PRIVATE_Update_Family_SynchScene(thisKidsPed,
//						eKidsFamilyMember[iPed], eKidsFamilyEvent[iPed],
//						vFamily_scene_coord_FS_MICHAEL + <<0,0,1>>,
//						fFamily_scene_head_FS_MICHAEL,
//						iFamily_synch_scene,
//						TRUE, FALSE, tAnimDict, tAnimClip)
//					iKidsStage[iPed] = iCONST_KIDS_1_release
//				ENDIF
				
				
				IF (eKidsFamilyEvent[iPed] = FE_ANY_find_family_event)
					iKidsStage[iPed] = iCONST_KIDS_1_release
					BREAK
				ENDIF
				
				TEXT_LABEL_63 tFamilySynchSceneDict
				TEXT_LABEL_63 tFamilySynchSceneClip
				
				ANIMATION_FLAGS eFamilySynchSceneFlag
				enumFamilyAnimProgress eFamilySynchSceneProgress
				
				VECTOR vSynchSceneOffset
				FLOAT fSynchSceneHead
				
				IF PRIVATE_Get_FamilyMember_Anim(eKidsFamilyMember[iPed], eKidsFamilyEvent[iPed],
						tFamilySynchSceneDict, tFamilySynchSceneClip, eFamilySynchSceneFlag, eFamilySynchSceneProgress)
				AND PRIVATE_Get_FamilyMember_Init_Offset(eKidsFamilyMember[iPed], eKidsFamilyEvent[iPed],
						vSynchSceneOffset, fSynchSceneHead)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene)
						iFamily_synch_scene = CREATE_SYNCHRONIZED_SCENE(vFamily_scene_coord_FS_MICHAEL+vSynchSceneOffset, <<0,0,fFamily_scene_head_FS_MICHAEL+fSynchSceneHead>>)
						
						SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene, TRUE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iFamily_synch_scene, FALSE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(thisKidsPed, iFamily_synch_scene,
							tFamilySynchSceneDict, tFamilySynchSceneClip,
							INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
							SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_NONE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iFamily_synch_scene, TRUE)
					
					iKidsStage[iPed] = iCONST_KIDS_1_release
				ENDIF
			BREAK
			
			CASE iCONST_KIDS_1_release
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  release...", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("release...", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				
				IF DOES_ENTITY_EXIST(g_pScene_buddy)
					CPRINTLN(DEBUG_SWITCH, "g_pScene_buddy[", Get_String_From_FamilyMember(eKidsFamilyMember[iPed]), "] exist ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(g_pScene_buddy)), " & ", Get_String_From_FamilyMember(GET_enumFamilyMember_from_ped(g_pScene_buddy)), " ")
					CPRINTLN(DEBUG_SWITCH, "g_eSceneBuddyEvents ", Get_String_From_FamilyEvent(g_eSceneBuddyEvents), ", ", Get_String_From_FamilyEvent(eKidsFamilyEvent[iPed]), " <> ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eKidsFamilyMember[iPed]]))
					CPRINTLN(DEBUG_SWITCH, "")
				ELSE
					CPRINTLN(DEBUG_SWITCH, "g_pScene_buddy[", Get_String_From_FamilyMember(eKidsFamilyMember[iPed]), "] doesnt exist ")
					CPRINTLN(DEBUG_SWITCH, "g_eSceneBuddyEvents ", Get_String_From_FamilyEvent(g_eSceneBuddyEvents), ", ", Get_String_From_FamilyEvent(eKidsFamilyEvent[iPed]), " <> ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eKidsFamilyMember[iPed]]))
					CPRINTLN(DEBUG_SWITCH, "")
				ENDIF
				#ENDIF
				
				IF (eKidsFamilyEvent[iPed] <> g_eCurrentFamilyEvent[eKidsFamilyMember[iPed]])
					
					IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
						g_eCurrentFamilyEvent[eKidsFamilyMember[iPed]]	= FAMILY_MEMBER_BUSY
						g_eSceneBuddyEvents								= eKidsFamilyEvent[iPed]
						
						g_pScene_buddy = thisKidsPed
						EXIT
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_pScene_buddy)
					ENDIF
				ELSE
					iKidsStage[iPed] = iCONST_KIDS_2_family
				ENDIF
			BREAK
			CASE iCONST_KIDS_2_family
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  family...", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("family...", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
			BREAK
			
			DEFAULT
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("STAGE DEFAULT ", iKidsStage[iPed], iPed+iCONSTANTSforkidsstage, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				DrawDebugSceneTextWithOffset("STAGE DEFAULT", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
			BREAK
		ENDSWITCH
	ELSE
		iKidsStage[iPed] = iCONST_KIDS_DEAD
		
		#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(thisKidsPed)
			DrawLiteralSceneString("dead", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_RED, fKidsAlphaMult)
			DrawDebugSceneTextWithOffset("dead", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_RED, fKidsAlphaMult)
		ELSE
			DrawLiteralSceneString("non-existant", iPed+iCONSTANTSforkidsstage, HUD_COLOUR_GREY, fKidsAlphaMult*0.5)
//			DrawDebugSceneTextWithOffset("non-existant", GET_ENTITY_COORDS(thisKidsPed), Offset_y, HUD_COLOUR_GREY, fKidsAlphaMult*0.5)
		ENDIF
		
		#ENDIF
	ENDIF
ENDPROC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_M_KIDS_switchInProgress()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ENDIF
	#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("loop running", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		#ENDIF
		
		INT iPed
		REPEAT COUNT_OF(kids_ped) iPed
			IF NOT IS_PED_INJURED(kids_ped[iPed])
				TASK_SYNCHRONIZED_SCENE(kids_ped[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
						tPlayerSceneAnimDict, tKidsAnimLoop[iPed],
						NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
			IF NOT IS_ENTITY_DEAD(kids_veh[iPed])
						
				IF NOT IS_STRING_NULL_OR_EMPTY(tVehAnimLoop[iPed])
					PLAY_SYNCHRONIZED_ENTITY_ANIM(kids_veh[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
							tVehAnimLoop[iPed], tPlayerSceneAnimDict, NORMAL_BLEND_IN)
				ELSE
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iPed])
						START_PLAYBACK_RECORDED_VEHICLE(kids_veh[iPed], iRecordingNum[iPed], tRecordingName)
					ENDIF
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(kids_veh[iPed], fPlayerRecordingSkip - GET_TIME_POSITION_IN_RECORDING(kids_veh[iPed]))
					SET_PLAYBACK_SPEED(kids_veh[iPed], fPlayerSpeedSwitch)	
				ENDIF
				
			ENDIF
		ENDREPEAT
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tKidsSceneSpeechLabel)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
			
			IF (selected_player_scene_m_kids_scene = PR_SCENE_M7_ROUNDTABLE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, g_pScene_buddy, "JIMMY")
				
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, kids_ped[0], "AMANDA")
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, kids_ped[1], "TRACEY")
			ELIF (selected_player_scene_m_kids_scene = PR_SCENE_M7_BIKINGJIMMY)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, kids_ped[0], "JIMMY")
			ELSE
				SCRIPT_ASSERT("invalid scene for speech label")
			ENDIF
		ENDIF
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("loop stopped", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		#ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tKidsSceneSpeechLabel)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
				
				IF (selected_player_scene_m_kids_scene = PR_SCENE_M7_ROUNDTABLE)
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, g_pScene_buddy, "JIMMY")
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, kids_ped[0], "AMANDA")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, kids_ped[1], "TRACEY")
				ELIF (selected_player_scene_m_kids_scene = PR_SCENE_M7_BIKINGJIMMY)
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, kids_ped[0], "JIMMY")
				ELSE
					SCRIPT_ASSERT("invalid scene for speech label")
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(kids_ped) iPed
		ControlKidsPedStage(kids_ped[iPed], iPed)
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_M_KIDS_STAGE_TWO()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ENDIF
	#ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tKidsSceneSpeechLabel)
	AND NOT bKidsSceneSpeechPreload
		IF PRELOAD_CONVERSATION(MyLocalPedStruct, "PRSAUD", tKidsSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
			bKidsSceneSpeechPreload = TRUE
		ENDIF
	ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			INT iPed
			REPEAT COUNT_OF(kids_ped) iPed
				IF NOT IS_PED_INJURED(kids_ped[iPed])
					
					IF NOT IS_ENTITY_DEAD(kids_veh[iPed])
						IF IS_PED_SITTING_IN_VEHICLE(kids_ped[iPed], kids_veh[iPed])
							SET_ENTITY_COORDS(kids_ped[iPed], GET_ENTITY_COORDS(kids_ped[iPed]))
						ENDIF
					ENDIF
					TASK_SYNCHRONIZED_SCENE(kids_ped[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
							tPlayerSceneAnimDict, tKidsAnimOut[iPed],
							NORMAL_BLEND_IN, INSTANT_BLEND_OUT)	//, SYNCED_SCENE_TAG_SYNC_OUT)
					iKidsStage[iPed] = 0
				ELSE
					iKidsStage[iPed] = -1
				ENDIF
				IF NOT IS_ENTITY_DEAD(kids_veh[iPed])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(kids_veh[iPed])
						STOP_PLAYBACK_RECORDED_VEHICLE(kids_veh[iPed])
					ENDIF
					PLAY_SYNCHRONIZED_ENTITY_ANIM(kids_veh[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
							tVehAnimOut[iPed], tPlayerSceneAnimDict, NORMAL_BLEND_IN)
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("exit running", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
			#ENDIF
			
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tKidsSceneSpeechLabel)
			AND bKidsSceneSpeechPreload
				IF NOT bKidsSceneSpeechHeard
				AND (fKidsSceneSpeechPhase = 0)
					BEGIN_PRELOADED_CONVERSATION()
					bKidsSceneSpeechHeard = TRUE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
			#ENDIF
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("switch Spline Cam In Progress", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		#ENDIF
		
	ENDIF
	
	INT iPed
	REPEAT COUNT_OF(kids_ped) iPed
		ControlKidsPedStage(kids_ped[iPed], iPed)
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_M_KIDS_STAGE_THREE()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ENDIF
	#ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			
			IF IS_STRING_NULL_OR_EMPTY(tKidsSceneSpeechLabel)
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("exit running", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
			ELIF NOT bKidsSceneSpeechPreload
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("preloading dialogue", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
				#ENDIF
				
				IF PRELOAD_CONVERSATION(MyLocalPedStruct, "PRSAUD", tKidsSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
					bKidsSceneSpeechPreload = TRUE
				ENDIF
				
			ELSE
				FLOAT fExitSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
				
				IF NOT bKidsSceneSpeechHeard
					IF (fExitSynchScenePhase <= fKidsSceneSpeechPhase)
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("wait for dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneStringFloat("play dialogue ", fExitSynchScenePhase, 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
						#ENDIF
						
						BEGIN_PRELOADED_CONVERSATION()
						bKidsSceneSpeechHeard = TRUE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("dialogue heard ", fExitSynchScenePhase, 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
					#ENDIF
				ENDIF
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("scene stopped", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("switch Spline Cam In Progress", 5+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		#ENDIF
		
	ENDIF
	
	
	INT iPed
	REPEAT COUNT_OF(kids_ped) iPed
		ControlKidsPedStage(kids_ped[iPed], iPed)
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_M_KIDS_STAGE_FOUR()
	
	#IF IS_DEBUG_BUILD
	FLOAT AnimCurrentTime = -1.0
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID)
		DrawLiteralSceneStringFloat("Loop_SynchSceneID:", AnimCurrentTime, 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
		DrawLiteralSceneStringFloat("Exit_SynchSceneID:", AnimCurrentTime, 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ENDIF
	IF AnimCurrentTime < 0
		DrawLiteralSceneString("SynchSceneID OFF??", 4+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
	ENDIF
	#ENDIF
	
	INT iPed
	REPEAT COUNT_OF(kids_ped) iPed
		ControlKidsPedStage(kids_ped[iPed], iPed)
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_M_Kids_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_M_Kids_Variables()
	Setup_Player_Scene_M_Kids()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_M_Kids_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_M_Kids_in_progress
	AND ProgressScene(BIT_MICHAEL, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_m_kids_stage
			CASE PS_M_KIDS_switchInProgress
				IF Do_PS_M_KIDS_switchInProgress()
					current_player_scene_m_kids_stage = PS_M_KIDS_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_M_KIDS_STAGE_TWO
				IF Do_PS_M_KIDS_STAGE_TWO()
					current_player_scene_m_kids_stage = PS_M_KIDS_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_M_KIDS_STAGE_THREE
				IF Do_PS_M_KIDS_STAGE_THREE()
					current_player_scene_m_kids_stage = PS_M_KIDS_STAGE_FOUR
				ENDIF
			BREAK
			CASE PS_M_KIDS_STAGE_FOUR
				IF Do_PS_M_KIDS_STAGE_FOUR()
					current_player_scene_m_kids_stage = FINISHED_M_KIDS
				ENDIF
			BREAK
			
			CASE FINISHED_M_KIDS
				Player_Scene_M_Kids_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_M_Kids_Widget()
		Player_Scene_M_Kids_Debug_Options()
		
		fKidsAlphaMult = 1.0
		IF current_player_scene_m_kids_stage = PS_M_KIDS_STAGE_FOUR
			
			CONST_FLOAT fMAX_PLAYER_KIDS_DIST	20.0
			FLOAT fPlayerKidsDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_m_kids_coord)
			
			INT iPed
			REPEAT COUNT_OF(kids_ped) iPed
				
				IF DOES_ENTITY_EXIST(kids_ped[iPed])
					FLOAT fKidsPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(kids_ped[iPed], FALSE))
					
					IF (fPlayerKidsDist > fKidsPedDist)
						fPlayerKidsDist = fKidsPedDist
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			fKidsAlphaMult = 1.0 - (fPlayerKidsDist/fMAX_PLAYER_KIDS_DIST)
			IF fKidsAlphaMult < 0
				fKidsAlphaMult = 0
			ENDIF
			
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_m_kids_scene,
				2+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_M_Kids_Stage(current_player_scene_m_kids_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_F_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSforkids, HUD_COLOUR_BLUELIGHT, fKidsAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_M_Kids_Cleanup()
ENDSCRIPT
