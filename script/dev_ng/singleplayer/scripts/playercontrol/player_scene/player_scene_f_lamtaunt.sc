// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Player scene F_TAUNT file for use – player_scene_f_lamtaunter.sc	 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "chase_hint_cam.sch"
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "player_ped_scenes.sch"

//-	private headers	-//
USING "player_scene_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
#ENDIF

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

CONST_INT iCONSTANTSfortauntering			3

CONST_INT iNUM_OF_TAUNTERS					3
CONST_INT iNUM_OF_TAUNTERS_PLUS_LAMAR		iNUM_OF_TAUNTERS+1

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_F_TAUNT_STAGE_ENUM
	PS_F_TAUNT_switchInProgress,
	PS_F_TAUNT_STAGE_TWO,
	PS_F_TAUNT_STAGE_THREE,
	PS_F_TAUNT_STAGE_FOUR,
	
	FINISHED_F_TAUNT
ENDENUM
PS_F_TAUNT_STAGE_ENUM		current_player_scene_f_lamtaunter_stage						= PS_F_TAUNT_switchInProgress
PED_REQUEST_SCENE_ENUM		selected_player_scene_f_lamtaunter_scene						= PR_SCENE_INVALID

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structPedsForConversation	MyLocalPedStruct
TEXT_LABEL tCreatedConvLabels[5]
structTimer speechTimer

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bPlayer_Scene_F_Taunter_in_progress							= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				taunter_ped[iNUM_OF_TAUNTERS]
MODEL_NAMES				eTaunterModel[iNUM_OF_TAUNTERS]
INT						iTaunterStage[iNUM_OF_TAUNTERS_PLUS_LAMAR]
REL_GROUP_HASH			taunterGroup
PED_TYPE				taunterPedType = PEDTYPE_INVALID
	
	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vPlayer_scene_f_lamtaunt_coord
FLOAT				fPlayer_scene_f_lamtaunt_head

VECTOR				vTaunter_coordOffset[iNUM_OF_TAUNTERS], vTaunter_wanderOffset[iNUM_OF_TAUNTERS_PLUS_LAMAR]
FLOAT				fTaunter_headOffset[iNUM_OF_TAUNTERS], fTaunter_wanderRadius[iNUM_OF_TAUNTERS_PLUS_LAMAR]

TEXT_LABEL_63		tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut
TEXT_LABEL_63		tTaunterAnimDict, tTaunterAnimLoop[iNUM_OF_TAUNTERS_PLUS_LAMAR], tTaunterAnimOut[iNUM_OF_TAUNTERS_PLUS_LAMAR]

	//- Pickups(INT) -//

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL			tTaunterSceneSpeechLabel = ""
FLOAT				fTaunterSceneSpeechLabel = 0.0
BOOL				bTaunterSceneSpeechLabel = FALSE

STRING				scenarioTypeName = "WORLD_HUMAN_PROSTITUTE_LOW_CLASS"

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//

	//- Timers(structTimer) -//

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
FLOAT			fLamTaunterAlphaMult = 1.0
WIDGET_GROUP_ID	player_scene_f_lamtaunter_widget
INT				iCurrent_player_scene_f_lamtaunter_stage
BOOL			bMovePeds, bSavePeds
#ENDIF

	//- other Ints(INT) -//

	//- other Floats(FLOAT) -//
FLOAT fExitScene = 0.94

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Player_Scene_F_Taunter_Cleanup()
	INT iPed
	
	//- mark peds as no longer needed -//
	REPEAT COUNT_OF(eTaunterModel) iPed
		IF (eTaunterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_PED_AS_NO_LONGER_NEEDED(taunter_ped[iPed])
		ENDIF
	ENDREPEAT
	
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	REPEAT COUNT_OF(eTaunterModel) iPed
		IF (eTaunterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eTaunterModel[iPed])
		ENDIF
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	REMOVE_RELATIONSHIP_GROUP(taunterGroup)
	
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(player_scene_f_lamtaunter_widget)
		DELETE_WIDGET_GROUP(player_scene_f_lamtaunter_widget)
	ENDIF
	#ENDIF
					
	IF NOT IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
		SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, TRUE)
	ENDIF

	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Called on successful completion of the mission
PROC Player_Scene_F_Taunter_Finished()
	//CLEAR_CprintS()
	bPlayer_Scene_F_Taunter_in_progress = FALSE
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Player_Scene_F_Taunter_Variables()
	selected_player_scene_f_lamtaunter_scene	= g_eRecentlySelectedScene	//PR_SCENE_f_BBTAUNT_YARD	//PR_SCENE_f_BBTAUNT_ALLEY
	
	TEXT_LABEL_31 tPlayer_scene_f_lamtaunt_room
	GET_PLAYER_PED_POSITION_FOR_SCENE(selected_player_scene_f_lamtaunter_scene, vPlayer_scene_f_lamtaunt_coord, fPlayer_scene_f_lamtaunt_head, tPlayer_scene_f_lamtaunt_room)
	
	ANIMATION_FLAGS playerAnimLoopFlag, playerAnimOutFlag
	//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
	
	GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_f_lamtaunter_scene,
			tPlayerSceneAnimDict, tPlayerSceneAnimLoop, tPlayerSceneAnimOut,
			playerAnimLoopFlag, playerAnimOutFlag)	//, ePlayerSceneAnimProgress)
	
	SWITCH selected_player_scene_f_lamtaunter_scene
//		CASE PR_SCENE_F_TAUNT
//			//- vectors -//
//			vTaunter_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
//			vTaunter_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
//			vTaunter_coordOffset[2] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
//			
//			vTaunter_wanderOffset[0] = <<-77.3, -4.1,1>>
//			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
//			vTaunter_wanderOffset[2] = vTaunter_wanderOffset[0]
//			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = <<0,0,0>>
//			
//			//- floats -//
//			fTaunter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
//			fTaunter_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
//			fTaunter_headOffset[2] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
//			
//			fTaunter_wanderRadius[0] = 30.0
//			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
//			fTaunter_wanderRadius[2] = fTaunter_wanderRadius[0]
//			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 0
//			
//			fExitScene = 0.94
//			
//			//- enums -//
//			eTaunterModel[0] = G_M_Y_StrPunk_01
//			eTaunterModel[1] = G_M_Y_StrPunk_02
//			eTaunterModel[2] = G_M_Y_STRPUNK_01
//			taunterPedType = PEDTYPE_GANG1
//			
//			//- text labels -//
//			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P6"
//			tTaunterAnimLoop[0]		= "LOOP_GANG1"
//			tTaunterAnimLoop[1]		= "LOOP_GANG2"
//			tTaunterAnimLoop[2]		= "EXIT_GANG3"
//			tTaunterAnimOut[0]		= "EXIT_GANG1"
//			tTaunterAnimOut[1]		= "EXIT_GANG2"
//			tTaunterAnimOut[2]		= "EXIT_GANG3"
//			
//			tTaunterSceneSpeechLabel = CreateRandomSpeechLabel("FRAS_IG_14")
//			fTaunterSceneSpeechLabel	= 0.0
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1
			//- vectors -//
			vTaunter_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vTaunter_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			vTaunter_wanderOffset[0] = <<-77.3, -4.1,1>>
			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = << 66.7, -7.299, 1.0 >>
			
			//- floats -//
			fTaunter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fTaunter_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fTaunter_wanderRadius[0] = 30.0
			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 20.0
			
			fExitScene = 0.94
			
			//- enums -//
			eTaunterModel[0] = G_M_Y_StrPunk_01
			eTaunterModel[1] = G_M_Y_StrPunk_02
			taunterPedType = PEDTYPE_GANG1
			
			//- text labels -//
			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P1"
			tTaunterAnimLoop[0]		= "gang_taunt_loop_thug_01"
			tTaunterAnimLoop[1]		= "gang_taunt_loop_thug_02"
			tTaunterAnimOut[0]		= "gang_taunt_exit_thug_01"
			tTaunterAnimOut[1]		= "gang_taunt_exit_thug_02"
			
			tTaunterSceneSpeechLabel = "FRAS_IG_6_P1"
			fTaunterSceneSpeechLabel	= 0.0
		BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2
//			//- vectors -//
//			vTaunter_coordOffset[0] = <<-0.0886, -0.1866, 1.0000>>		//<<297.9926, -1617.3041, 30.5333>> - vPlayer_scene_f_lamtaunt_coord
//			vTaunter_coordOffset[1] = <<-0.2257, 0.8433, 1.0000>>		//<<297.8555, -1616.2742, 30.5333>> - vPlayer_scene_f_lamtaunt_coord
//			
//			vTaunter_wanderOffset[0] = <<390.3804, -1529.1986, 29.2748>> - vPlayer_scene_f_lamtaunt_coord
//			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
//			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = -vTaunter_wanderOffset[0]
//			
//			//- floats -//
//			fTaunter_headOffset[0] = 0.0
//			fTaunter_headOffset[1] = 0.0
//			
//			fTaunter_wanderRadius[0] = 40
//			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
//			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = fTaunter_wanderRadius[0]
//			
//			fExitScene = 0.94
//			
//			//- enums -//
//			eTaunterModel[0] = G_M_Y_StrPunk_01
//			eTaunterModel[1] = G_M_Y_StrPunk_02
//			taunterPedType = PEDTYPE_GANG1
//			
//			//- text labels -//
//			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P2"
//			tTaunterAnimLoop[0]		= "fras_ig_6_p2_loop_g1"
//			tTaunterAnimLoop[1]		= "fras_ig_6_p2_loop_g2"
//			tTaunterAnimOut[0]		= "fras_ig_6_p2_exit_g1"
//			tTaunterAnimOut[1]		= "fras_ig_6_p2_exit_g2"
//			
//			tTaunterSceneSpeechLabel	= "FRAS_IG_6_P2"
//			fTaunterSceneSpeechLabel	= 0.0
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			//- vectors -//
			vTaunter_coordOffset[0] = <<-0.2331, 0.0497, 0.0041>>
			vTaunter_coordOffset[1] = <<0.6908, -0.4534, 0.0042>>
			
			vTaunter_wanderOffset[0] = <<-0.600,-144.300,0>>
			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = <<112.2308, 43.1290, 3.1880>>
			
			//- floats -//
			fTaunter_headOffset[0] = 180.0000
			fTaunter_headOffset[1] = 180.0000
			
			fTaunter_wanderRadius[0] = 40
			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 40
			
			fExitScene = 0.94
			
			//- enums -//
			eTaunterModel[0] = G_M_Y_StrPunk_01
			eTaunterModel[1] = G_M_Y_StrPunk_02
			taunterPedType = PEDTYPE_GANG1
			
			//- text labels -//
			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tTaunterAnimLoop[0]		= "gang_taunt_with_lamar_loop_g1"
			tTaunterAnimLoop[1]		= "gang_taunt_with_lamar_loop_g2"
			tTaunterAnimOut[0]		= "gang_taunt_with_lamar_exit_g1"
			tTaunterAnimOut[1]		= "gang_taunt_with_lamar_exit_g2"
			
			tTaunterSceneSpeechLabel	= "FRAS_IG_6_P3"
			fTaunterSceneSpeechLabel	= 0.0
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
			//- vectors -//
			vTaunter_coordOffset[0] = <<1.7097, -1.8585, -0.0520>>		//<<-17.1795, -1825.7705, 25.7893>> - vPlayer_scene_f_lamtaunt_coord
			vTaunter_coordOffset[1] = <<2.7011, -1.5000, -0.0520>>		//<<-16.1881, -1825.4120, 25.7893>> - vPlayer_scene_f_lamtaunt_coord
	
			vTaunter_wanderOffset[0] = <<59.4395, -1890.5951, 21.5656>> - vPlayer_scene_f_lamtaunt_coord
			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
			
			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = <<67.5391, -54.7307, -4.6336>>*-1.0
			
			//- floats -//
			fTaunter_headOffset[0] = -0.7167		//52.9269 - fPlayer_scene_f_lamtaunt_head
			fTaunter_headOffset[1] = -0.7167		//52.9269 - fPlayer_scene_f_lamtaunt_head
			
			fTaunter_wanderRadius[0] = 40
			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 40
			
			fExitScene = 0.99
			
			//- enums -//
			eTaunterModel[0] = S_M_Y_COP_01	//G_M_Y_StrPunk_01
			eTaunterModel[1] = S_M_Y_COP_01	//G_M_Y_StrPunk_02
			taunterPedType = PEDTYPE_COP
			
			//- text labels -//
			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P5"
			tTaunterAnimLoop[0]		= "fras_ig_6_p5_loop_g1"
			tTaunterAnimLoop[1]		= "fras_ig_6_p5_loop_g2"
			tTaunterAnimOut[0]		= "fras_ig_6_p5_exit_g1"
			tTaunterAnimOut[1]		= "fras_ig_6_p5_exit_g2"
			
			tTaunterSceneSpeechLabel = "FRAS_IG_6_P4"
			fTaunterSceneSpeechLabel = 0.625
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			//- vectors -//
			vTaunter_coordOffset[0] = <<3.8209, 1.0394, 0.0000>>		//<<196.5719, -1671.6135, 29.8280>> - vPlayer_scene_f_lamtaunt_coord
			vTaunter_coordOffset[1] = <<4.0994, -0.0922, 0.0000>>		//<<196.8504, -1672.7451, 29.8280>> - vPlayer_scene_f_lamtaunt_coord
			
			vTaunter_wanderOffset[0] = <<278.2831, -1650.5173, 27.9565>> - vPlayer_scene_f_lamtaunt_coord
			vTaunter_wanderOffset[1] = vTaunter_wanderOffset[0]
			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = -<<54.673,62.700,0>>
			
			//- floats -//
			fTaunter_headOffset[0] = -23.9383		//-107.7383 - fPlayer_scene_f_lamtaunt_head
			fTaunter_headOffset[1] = 5.0908		//-78.7092 - fPlayer_scene_f_lamtaunt_head
			
			fTaunter_wanderRadius[0] = 40.000
			fTaunter_wanderRadius[1] = fTaunter_wanderRadius[0]
			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 40.000
			
			fExitScene = 0.94
			
			//- enums -//
			eTaunterModel[0] = G_M_Y_StrPunk_01
			eTaunterModel[1] = G_M_Y_StrPunk_02
			taunterPedType = PEDTYPE_GANG1
			
			//- text labels -//
			tTaunterAnimDict		= "SWITCH@FRANKLIN@GANG_TAUNT_P3"
			tTaunterAnimLoop[0]		= "fras_ig_10_p3_loop_g1"
			tTaunterAnimLoop[1]		= "fras_ig_10_p3_loop_g2"
			tTaunterAnimOut[0]		= "fras_ig_10_p3_exit_g1"
			tTaunterAnimOut[1]		= "fras_ig_10_p3_exit_g2"
			
			tTaunterSceneSpeechLabel = CreateRandomSpeechLabel("FRAS_IG_10")
			fTaunterSceneSpeechLabel = 0.0
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "invalid selected_player_scene_f_lamtaunter_scene: "
			str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_lamtaunter_scene)
			
			CPRINTLN(DEBUG_SWITCH, GET_THIS_SCRIPT_NAME(), ": ", str)
			SCRIPT_ASSERT(str)
			#ENDIF
			
			//- vectors -//
			vTaunter_coordOffset[0] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			vTaunter_coordOffset[1] = <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0.0000>>
			
			vTaunter_wanderOffset[0] = <<0,0,0>>
			vTaunter_wanderOffset[1] = <<0,0,0>>
			vTaunter_wanderOffset[COUNT_OF(taunter_ped)] = <<0,0,0>>
			
			//- floats -//
			fTaunter_headOffset[0] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			fTaunter_headOffset[1] = GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180.0)
			
			fTaunter_wanderRadius[0] = 0
			fTaunter_wanderRadius[1] = 0
			fTaunter_wanderRadius[COUNT_OF(taunter_ped)] = 0
			
			fExitScene = -1
			
			//- enums -//
			eTaunterModel[0] = G_M_Y_StrPunk_01
			eTaunterModel[1] = G_M_Y_StrPunk_02
			taunterPedType = PEDTYPE_INVALID
			
			tTaunterSceneSpeechLabel = ""
			fTaunterSceneSpeechLabel = 0.0
		BREAK
	ENDSWITCH
	
	//- ints -//
	//-- structs : PS_F_TAUNT_STRUCT --//
	
//	IF (selected_player_scene_f_lamtaunter_scene <> PR_SCENE_F_TAUNT)
//		TEXT_LABEL_63 tBuddyAnimDict
//		ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
//		GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(selected_player_scene_f_lamtaunter_scene, tBuddyAnimDict,
//				tTaunterAnimLoop[COUNT_OF(taunter_ped)],
//				tTaunterAnimOut[COUNT_OF(taunter_ped)],
//				buddyAnimLoopFlag, buddyAnimOutFlag)
//	ELSE
		tTaunterAnimLoop[COUNT_OF(taunter_ped)] = ""
		tTaunterAnimLoop[COUNT_OF(taunter_ped)] = ""
//	ENDIF
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Player_Scene_F_Taunter()
	INT iPed
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	
	INT iWaitForBuddyAssets = 0
	BOOL bWaitForBuddyAssets = FALSE
	WHILE NOT bWaitForBuddyAssets
	AND (iWaitForBuddyAssets < 400)
		bWaitForBuddyAssets = TRUE
		
		#IF IS_DEBUG_BUILD
		INT iRow = 0
		#ENDIF
		
//		IF (selected_player_scene_f_lamtaunter_scene <> PR_SCENE_F_TAUNT)
			IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str  = ("g_pScene_buddy doesnt exist")
				DrawLiteralSceneString(str, iRow+1+iCONSTANTSfortauntering, HUD_COLOUR_RED)
				iRow++
				#ENDIF
				
				bWaitForBuddyAssets = FALSE
			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//			TEXT_LABEL_63 str  = ("g_pScene_buddy not needed...")
//			DrawLiteralSceneString(str, iRow+1+iCONSTANTSfortauntering, HUD_COLOUR_REDLIGHT)
//			iRow++
//			#ENDIF
//		ENDIF
		
		REPEAT COUNT_OF(taunter_ped) iPed
			IF (eTaunterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(eTaunterModel[iPed])
				IF NOT HAS_MODEL_LOADED(eTaunterModel[iPed])
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str  = ("ped model ")
					str += GET_MODEL_NAME_FOR_DEBUG(eTaunterModel[iPed])
					DrawLiteralSceneString(str, iRow+1+iCONSTANTSfortauntering, HUD_COLOUR_RED)
					iRow++
					#ENDIF
					
					bWaitForBuddyAssets = FALSE
					REQUEST_MODEL(eTaunterModel[iPed])
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tTaunterAnimDict)
			REQUEST_ANIM_DICT(tTaunterAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(tTaunterAnimDict)
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str  = ("anim dict \"")
				str += tTaunterAnimDict
				str += ("\"")
				DrawLiteralSceneString(str, iRow+1+iCONSTANTSfortauntering, HUD_COLOUR_RED)
				iRow++
				#ENDIF
				
				bWaitForBuddyAssets = FALSE
				REQUEST_ANIM_DICT(tTaunterAnimDict)
			ENDIF
		ENDIF
		
		IF NOT bWaitForBuddyAssets
			iWaitForBuddyAssets++
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//- create any script vehicles -//
	//- create any script peds -//
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
		
		IF GET_NPC_PED_MODEL(CHAR_LAMAR) = GET_ENTITY_MODEL(g_pScene_buddy)
			SET_ENTITY_AS_MISSION_ENTITY(g_pScene_buddy, TRUE, TRUE)
		ELSE
			SCRIPT_ASSERT("cannot set Lamar as entity - not Lamar???")
		ENDIF
		
		PRIVATE_SetDefaultFamilyMemberAttributes(g_pScene_buddy, RELGROUPHASH_FAMILY_F)
		
		SET_PED_CONFIG_FLAG(g_pScene_buddy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(g_pScene_buddy, PCF_CanSayFollowedByPlayerAudio, TRUE)
		
		PRIVATE_SetDefaultFamilyMemberAttributes(g_pScene_buddy, RELGROUPHASH_FAMILY_F)
	ENDIF
	
	ADD_RELATIONSHIP_GROUP("TAUNTERS", taunterGroup)
	
	REPEAT COUNT_OF(taunter_ped) iPed
		IF (eTaunterModel[iPed] <> DUMMY_MODEL_FOR_SCRIPT)
			taunter_ped[iPed] = CREATE_PED(taunterPedType, eTaunterModel[iPed], vPlayer_scene_f_lamtaunt_coord+vTaunter_coordOffset[iPed])
			SET_ENTITY_HEADING(taunter_ped[iPed], fPlayer_scene_f_lamtaunt_head+fTaunter_headOffset[iPed])
			SET_PED_RANDOM_COMPONENT_VARIATION(taunter_ped[iPed])
			
			SET_PED_RELATIONSHIP_GROUP_HASH(taunter_ped[iPed], taunterGroup)
			TASK_PLAY_ANIM(taunter_ped[iPed], tTaunterAnimDict, tTaunterAnimLoop[iPed],
					NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
					AF_LOOPING | AF_NOT_INTERRUPTABLE)
			
			SET_PED_CONFIG_FLAG(taunter_ped[iPed], PCF_CanSayFollowedByPlayerAudio, TRUE)
			
			IF (taunterPedType = PEDTYPE_COP)
				GIVE_WEAPON_TO_PED(taunter_ped[iPed], WEAPONTYPE_STUNGUN, 1000)
			ENDIF
		ENDIF
	ENDREPEAT
	
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, taunterGroup, RELGROUPHASH_PLAYER)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, taunterGroup)
	
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for player -//
	
ENDPROC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_Player_Scene_F_Taunter_Stage(PS_F_TAUNT_STAGE_ENUM this_player_scene_f_lamtaunter_stage)
	SWITCH this_player_scene_f_lamtaunter_stage
		CASE PS_F_TAUNT_switchInProgress
			RETURN "PS_F_TAUNT_switchInProgress"
		BREAK
		CASE PS_F_TAUNT_STAGE_TWO
			RETURN "PS_F_TAUNT_STAGE_TWO"
		BREAK
		CASE PS_F_TAUNT_STAGE_THREE
			RETURN "PS_F_TAUNT_STAGE_THREE"
		BREAK
		CASE PS_F_TAUNT_STAGE_FOUR
			RETURN "PS_F_TAUNT_STAGE_FOUR"
		BREAK
		
		CASE FINISHED_F_TAUNT
			RETURN "FINISHED_F_TAUNT"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Player_Scene_F_Taunter_Stage(null) "
ENDFUNC

//PURPOSE:	Initialises the mission widget
PROC Create_Player_Scene_F_Taunter_widget()
	INT iWidget
	
	TEXT_LABEL_63 str = "player_scene_f_lamtaunter.sc - "
	str += Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_lamtaunter_scene)
	
	player_scene_f_lamtaunter_widget = START_WIDGET_GROUP(str)
		START_NEW_WIDGET_COMBO()
			REPEAT COUNT_OF(PS_F_TAUNT_STAGE_ENUM) iWidget
				ADD_TO_WIDGET_COMBO(Get_String_From_Player_Scene_F_Taunter_Stage(INT_TO_ENUM(PS_F_TAUNT_STAGE_ENUM, iWidget)))
			ENDREPEAT
		STOP_WIDGET_COMBO("iCurrent_player_scene_f_lamtaunter_stage", iCurrent_player_scene_f_lamtaunter_stage)
		
		START_WIDGET_GROUP("Do_PS_F_TAUNT_STAGE_TWO")
			ADD_WIDGET_VECTOR_SLIDER("vPlayer_scene_f_lamtaunt_coord", vPlayer_scene_f_lamtaunt_coord, -3000.0, 3000.0, 0.0)
			ADD_WIDGET_FLOAT_SLIDER("fPlayer_scene_f_lamtaunt_head", fPlayer_scene_f_lamtaunt_head, -360.0, 360.0, 0.0)
			
			REPEAT COUNT_OF(taunter_ped) iWidget
				IF (eTaunterModel[iWidget] <> DUMMY_MODEL_FOR_SCRIPT)
					str = "taunter_ped "
					str += iWidget
					ADD_WIDGET_STRING(str)
					ADD_WIDGET_VECTOR_SLIDER("vTaunter_coordOffset", vTaunter_coordOffset[iWidget], -30.0, 30.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fTaunter_headOffset", fTaunter_headOffset[iWidget], -180.0, 180.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("vTaunter_wanderOffset", vTaunter_wanderOffset[iWidget], -150.0, 150.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fTaunter_wanderRadius", fTaunter_wanderRadius[iWidget], 0.0, 50.0, 1.0)
				ENDIF
			ENDREPEAT
			
			ADD_WIDGET_STRING("buddy")
//				ADD_WIDGET_VECTOR_SLIDER("vTaunter_coordOffset", vTaunter_coordOffset[COUNT_OF(taunter_ped)], -30.0, 30.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fTaunter_headOffset", fTaunter_headOffset[COUNT_OF(taunter_ped)], -180.0, 180.0, 1.0)
			ADD_WIDGET_VECTOR_SLIDER("vTaunter_wanderOffset", vTaunter_wanderOffset[COUNT_OF(taunter_ped)], -150.0, 150.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fTaunter_wanderRadius", fTaunter_wanderRadius[COUNT_OF(taunter_ped)], 0.0, 50.0, 1.0)
			
			ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
			ADD_WIDGET_BOOL("bSavePeds", bSavePeds)
			
			ADD_WIDGET_FLOAT_SLIDER("fExitScene", fExitScene, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Player_Scene_F_Taunter_Widget()
	INT iWidget
	iCurrent_player_scene_f_lamtaunter_stage = ENUM_TO_INT(current_player_scene_f_lamtaunter_stage)
	
	IF NOT bMovePeds
	ELSE
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		
			REPEAT COUNT_OF(taunter_ped) iWidget
				IF NOT IS_PED_INJURED(taunter_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(taunter_ped[iWidget], tTaunterAnimDict, tTaunterAnimLoop[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(taunter_ped[iWidget], g_iPlayer_Timetable_Loop_SynchSceneID,
								tTaunterAnimDict, tTaunterAnimLoop[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
						iTaunterStage[iWidget] = 0
					ENDIF
					
					vTaunter_coordOffset[iWidget] = GET_ENTITY_COORDS(taunter_ped[iWidget]) - vPlayer_scene_f_lamtaunt_coord
					fTaunter_headOffset[iWidget] = GET_ENTITY_HEADING(taunter_ped[iWidget]) - fPlayer_scene_f_lamtaunt_head

					IF fTaunter_headOffset[iWidget] > 180.0
						fTaunter_headOffset[iWidget] -= 360.0
					ENDIF
					IF fTaunter_headOffset[iWidget] < -180.0
						fTaunter_headOffset[iWidget] += 360.0
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "taunter_ped ", iWidget, " doesnt exist??")
				ENDIF
			ENDREPEAT
			
		ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		
			REPEAT COUNT_OF(taunter_ped) iWidget
				IF NOT IS_PED_INJURED(taunter_ped[iWidget])
					IF NOT IS_ENTITY_PLAYING_ANIM(taunter_ped[iWidget], tTaunterAnimDict, tTaunterAnimOut[iWidget], ANIM_SYNCED_SCENE)
						TASK_SYNCHRONIZED_SCENE(taunter_ped[iWidget], g_iPlayer_Timetable_exit_SynchSceneID,
								tTaunterAnimDict, tTaunterAnimOut[iWidget],
								NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SWITCH, "taunter_ped ", iWidget, " doesnt exist??")
				ENDIF
			ENDREPEAT
			
		ELSE
		
			REPEAT COUNT_OF(taunter_ped) iWidget
				IF NOT IS_PED_INJURED(taunter_ped[iWidget])
	//				CLEAR_PED_TASKS(taunter_ped[iWidget])
					
					SET_ENTITY_HEADING(taunter_ped[iWidget], fPlayer_scene_f_lamtaunt_head+fTaunter_headOffset[iWidget])
					SET_ENTITY_COORDS(taunter_ped[iWidget], vPlayer_scene_f_lamtaunt_coord+vTaunter_coordOffset[iWidget])
				ENDIF
			ENDREPEAT
			
		ENDIF
		
	ENDIF
	
	IF bSavePeds
		
		SAVE_STRING_TO_DEBUG_FILE("	\"player_scene_f_lamtaunter.sc\" - bSavePeds")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- vectors -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(vTaunter_coordOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vTaunter_coordOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTaunter_coordOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_VECTOR_TO_DEBUG_FILE(vPlayer_scene_f_lamtaunt_coord+vTaunter_coordOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - vPlayer_scene_f_lamtaunt_coord")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT COUNT_OF(vTaunter_wanderOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	vTaunter_wanderOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vTaunter_wanderOffset[iWidget])
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("	//- floats -//")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT COUNT_OF(fTaunter_headOffset) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fTaunter_headOffset[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fTaunter_headOffset[iWidget])
			
			SAVE_STRING_TO_DEBUG_FILE("		//")
			SAVE_FLOAT_TO_DEBUG_FILE(fPlayer_scene_f_lamtaunt_head+fTaunter_headOffset[iWidget])
			SAVE_STRING_TO_DEBUG_FILE(" - fPlayer_scene_f_lamtaunt_head")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT COUNT_OF(fTaunter_wanderRadius) iWidget
			SAVE_STRING_TO_DEBUG_FILE("	fTaunter_wanderRadius[")
			SAVE_INT_TO_DEBUG_FILE(iWidget)
			SAVE_STRING_TO_DEBUG_FILE("] = ")
			SAVE_FLOAT_TO_DEBUG_FILE(fTaunter_wanderRadius[iWidget])
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bSavePeds = FALSE
	ENDIF
	
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Player_Scene_F_Taunter_Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		current_player_scene_f_lamtaunter_stage = FINISHED_F_TAUNT
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Do_PS_F_TAUNT_switchInProgress()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ENDIF
	#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("IS_SYNCHRONIZED_SCENE_RUNNING", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		#ENDIF
		
		INT iPed
		REPEAT COUNT_OF(taunter_ped) iPed
			IF NOT IS_PED_INJURED(taunter_ped[iPed])
				TASK_SYNCHRONIZED_SCENE(taunter_ped[iPed], g_iPlayer_Timetable_Loop_SynchSceneID,
						tTaunterAnimDict, tTaunterAnimLoop[iPed],
						NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		IF NOT IS_PED_INJURED(g_pScene_buddy)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, g_pScene_buddy, "LAMAR")
		ENDIF
		
		IF NOT IS_PED_INJURED(taunter_ped[0])
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, taunter_ped[0], "GangMember1")
		ENDIF
		IF NOT IS_PED_INJURED(taunter_ped[1])
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 6, taunter_ped[1], "GangMember2")
		ENDIF
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("scene stopped", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Do_PS_F_TAUNT_STAGE_TWO()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ENDIF
	#ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			INT iPed
			REPEAT COUNT_OF(taunter_ped) iPed
				IF NOT IS_PED_INJURED(taunter_ped[iPed])
					TASK_SYNCHRONIZED_SCENE(taunter_ped[iPed], g_iPlayer_Timetable_Exit_SynchSceneID,
							tTaunterAnimDict, tTaunterAnimOut[iPed],
							NORMAL_BLEND_IN, SLOW_BLEND_OUT,
							SYNCED_SCENE_TAG_SYNC_OUT | SYNCED_SCENE_USE_PHYSICS)
					iTaunterStage[iPed] = 0
				ELSE
					iTaunterStage[iPed] = -1
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("IS_SYNCHRONIZED_SCENE_RUNNING", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
			#ENDIF
			
			TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("scene stopped", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
			#ENDIF
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("switch Spline Cam In Progress", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		#ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
BOOL bGeneraredTaunterWander = FALSE
#ENDIF

INT iHassleTimer = -1, iHassleCount = 0
BOOL bSentGriefText

PROC PlayLamTauntPedGreet(PED_INDEX thisTaunterPed)
	IF NOT DOES_ENTITY_EXIST(thisTaunterPed)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(thisTaunterPed)
		
		IF NOT bSentGriefText
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisTaunterPed, PLAYER_PED_ID())
			// Should the player get an angry text message from the hospital?
			enumCharacterList	eChar = GET_NPC_PED_ENUM(thisTaunterPed)
			enumFriend  eFriend = GET_FRIEND_FROM_CHAR(eChar)
			CC_CommID   eCommID = GET_COMM_ID_FOR_FRIEND_FAIL(eFriend)

			IF eCommID != COMM_NONE
			    CC_CodeID eHospitalCID = Private_GetHospitalChargeCID(thisTaunterPed)
			    
			    // Send angry text message, if allowed, and not already an angry message queued
			    IF eHospitalCID != CID_BLANK
					IF NOT IS_COMMUNICATION_REGISTERED(eCommID)
					OR GET_COMMUNICATION_STATUS(eCommID) = CS_ERROR
						Store_Vector_ID_Data(VID_DYNAMIC_FRIEND_HOSPITAL, GET_ENTITY_COORDS(thisTaunterPed, FALSE), 250.0)
						IF REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(g_eDefaultPlayerChar, eChar, FTM_FRIEND_HOSPITAL, FALSE, eHospitalCID, eCommID, VID_DYNAMIC_FRIEND_HOSPITAL)
							bSentGriefText = TRUE
						ENDIF
					ENDIF
			    ENDIF
			ENDIF
			
		ENDIF
		
		EXIT
	ENDIF
	
	Play_Greet_Player_Family_Context(thisTaunterPed, FE_ANY_wander_family_event,
			MyLocalPedStruct, "FMFAUD", tCreatedConvLabels,
			speechTimer, CONV_PRIORITY_AMBIENT_MEDIUM)
	
	
	// #1529975
	CONST_FLOAT fMIN_HASSLE_DIST	8.0
	FLOAT fHassleDist2 = VDIST2(GET_ENTITY_COORDS(thisTaunterPed), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	IF fHassleDist2 > (fMIN_HASSLE_DIST*fMIN_HASSLE_DIST)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneStringFloat("fHassleDist: ", SQRT(fHassleDist2), 7+COUNT_OF(taunter_ped)+iCONSTANTSfortauntering, HUD_COLOUR_PURPLE, fLamTaunterAlphaMult)
		#ENDIF
		
		iHassleTimer = -1
	ELIF IS_ANY_SPEECH_PLAYING(thisTaunterPed)
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("speech playing", 7+COUNT_OF(taunter_ped)+iCONSTANTSfortauntering, HUD_COLOUR_PURPLE, fLamTaunterAlphaMult)
		#ENDIF
		
		iHassleTimer = -1
	ELSE
		IF iHassleTimer <= 0
			iHassleTimer = GET_GAME_TIMER()
		ENDIF
		
		CONST_INT iMIN_FIRST_HASSLE_TIME	15000	//15s
		CONST_INT iMIN_EXTRA_HASSLE_TIME	10000	//10s
		
		IF iHassleTimer > 0
			
			IF iHassleCount = 0
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("iFirstHassle: ", GET_GAME_TIMER()-iHassleTimer, 7+COUNT_OF(taunter_ped)+iCONSTANTSfortauntering, HUD_COLOUR_ORANGELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				IF (iHassleTimer+iMIN_FIRST_HASSLE_TIME) < GET_GAME_TIMER()
					PLAY_PED_AMBIENT_SPEECH(thisTaunterPed, "FRIEND_FOLLOWED_BY_PLAYER")
					
					iHassleTimer = -1
					iHassleCount++
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("iMultHassle: ", GET_GAME_TIMER()-iHassleTimer, 7+COUNT_OF(taunter_ped)+iCONSTANTSfortauntering, HUD_COLOUR_ORANGEDARK, fLamTaunterAlphaMult)
				#ENDIF
				
				IF (iHassleTimer+iMIN_EXTRA_HASSLE_TIME) < GET_GAME_TIMER()
					PLAY_PED_AMBIENT_SPEECH(thisTaunterPed, "HIT_BY_PLAYER")
					
					iHassleTimer = -1
					iHassleCount++
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL ControlLamTaunterPedStage(PED_INDEX &thisTaunterPed, INT iPed)
	
	CONST_INT	iCONST_LAMTAUNT_0						0
	CONST_INT	iCONST_LAMTAUNT_1_gotoCoord				1
	CONST_INT	iCONST_LAMTAUNT_2_navmesh				2
	CONST_INT	iCONST_LAMTAUNT_3_wander				3
	CONST_INT	iCONST_LAMTAUNT_4_useNearScenario		4
	
	CONST_INT	iCONST_LAMTAUNT_10_hating				10
	CONST_INT	iCONST_LAMTAUNT_11_hated				11
	
	CONST_INT	iCONST_LAMTAUNT_DEAD					-1
	
	#IF IS_DEBUG_BUILD
	CONST_FLOAT Offset_y	1.0
	#ENDIF
	
	ENAV_SCRIPT_FLAGS eLamtauntNavmeshFlag = ENAV_DEFAULT
	
	IF DOES_ENTITY_EXIST(thisTaunterPed)
		
		IF NOT ProgressScene(BIT_FRANKLIN, thisTaunterPed)
			SET_ENTITY_AS_MISSION_ENTITY(thisTaunterPed, TRUE, TRUE)
			
			DELETE_PED(thisTaunterPed)
			CPRINTLN(DEBUG_SWITCH, "ProgressScene - false")
			
			iTaunterStage[iPed] = iCONST_LAMTAUNT_DEAD
			
			RETURN FALSE
		ENDIF
		
		IF IS_PED_INJURED(thisTaunterPed)
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("dead", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_RED, fLamTaunterAlphaMult)
			DrawDebugSceneTextWithOffset("dead", GET_ENTITY_COORDS(thisTaunterPed, FALSE), Offset_y, HUD_COLOUR_RED, fLamTaunterAlphaMult)
			#ENDIF
			
			iTaunterStage[iPed] = iCONST_LAMTAUNT_DEAD
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		
		NAVMESH_ROUTE_RESULT enavmesh_route_result
		FLOAT fOut_DistanceRemaining
		INT iOut_ThisIsLastRouteSection
		#ENDIF
		
		SWITCH iTaunterStage[iPed]
			CASE iCONST_LAMTAUNT_0	//
				IF IS_ENTITY_PLAYING_ANIM(thisTaunterPed, tTaunterAnimDict, tTaunterAnimOut[iPed], ANIM_SYNCED_SCENE)
					
					#IF IS_DEBUG_BUILD
					HUD_COLOURS eHudColourWalkOut
					eHudColourWalkOut = HUD_COLOUR_BLUELIGHT
					#ENDIF
					
					FLOAT ffExitScene
					ffExitScene = fExitScene
					
					FLOAT ReturnStartPhase, ReturnEndPhase
					ReturnStartPhase = -1
					ReturnEndPhase = -1
					IF FIND_ANIM_EVENT_PHASE(tTaunterAnimDict, tTaunterAnimOut[iPed],
							"WalkInterruptible", ReturnStartPhase, ReturnEndPhase)
						#IF IS_DEBUG_BUILD
						str  = " outWalk \""
						eHudColourWalkOut = HUD_COLOUR_BLUEDARK
						#ENDIF
						
						ffExitScene = ReturnStartPhase
					ELSE
						#IF IS_DEBUG_BUILD
						str  = " outAnim \""
						eHudColourWalkOut = HUD_COLOUR_BLUELIGHT
						#ENDIF
						
						ffExitScene = fExitScene
					ENDIF
					
					#IF IS_DEBUG_BUILD
					INT iTaunterAnimOutLength
					iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tTaunterAnimOut[iPed])
					
					IF iTaunterAnimOutLength >= 16
						str += GET_STRING_FROM_STRING(tTaunterAnimOut[iPed],
								iTaunterAnimOutLength - 16,
								iTaunterAnimOutLength)
					ELSE
						str += tTaunterAnimOut[iPed]
					ENDIF
					
					str += "\""
					DrawLiteralSceneString(str,
							iPed+5+iCONSTANTSfortauntering, eHudColourWalkOut, fLamTaunterAlphaMult)
					DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(thisTaunterPed), Offset_y, eHudColourWalkOut, fLamTaunterAlphaMult)
					#ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID) > ffExitScene //0.97
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  NOT playing out anim", iPed+5+iCONSTANTSfortauntering, eHudColourWalkOut, fLamTaunterAlphaMult)
						DrawDebugSceneTextWithOffset("NOT playing out anim", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, eHudColourWalkOut, fLamTaunterAlphaMult)
						#ENDIF
						
						IF ARE_VECTORS_EQUAL(vTaunter_wanderOffset[iPed], <<0,0,0>>)
							VECTOR vTaunter_ped_coord
							vTaunter_ped_coord = GET_ENTITY_COORDS(thisTaunterPed)
							vTaunter_wanderOffset[iPed] = (vTaunter_ped_coord - vPlayer_scene_f_lamtaunt_coord) * 4.0
							vTaunter_wanderOffset[iPed].z = 0.0
							#IF IS_DEBUG_BUILD
							
							IF NOT bGeneraredTaunterWander
								
								SCRIPT_ASSERT("missing taunter wander")
								
								SAVE_STRING_TO_DEBUG_FILE("missing taunter wander for [")
								SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_lamtaunter_scene))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								
								bGeneraredTaunterWander = TRUE
							ENDIF
							
							SAVE_STRING_TO_DEBUG_FILE("vTaunter_wanderOffset[")
							SAVE_INT_TO_DEBUG_FILE(iPed)
							SAVE_STRING_TO_DEBUG_FILE("] = ")
							SAVE_VECTOR_TO_DEBUG_FILE(vTaunter_wanderOffset[iPed])
							SAVE_NEWLINE_TO_DEBUG_FILE()
							#ENDIF
						ENDIF
						
						WAIT(0)
						IF IS_PED_INJURED(thisTaunterPed)
							
							CLEAR_PED_TASKS(thisTaunterPed)
							
							TASK_GO_STRAIGHT_TO_COORD(thisTaunterPed,
									 vPlayer_scene_f_lamtaunt_coord+(vTaunter_wanderOffset[iPed]*0.1),
									 PEDMOVE_WALK, -1)
							FORCE_PED_MOTION_STATE(thisTaunterPed, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(thisTaunterPed)
							
							CLEAR_RAGDOLL_BLOCKING_FLAGS(thisTaunterPed, RBF_VEHICLE_IMPACT)
						ENDIF
						
						WAIT(0)
						
						iTaunterStage[iPed] = iCONST_LAMTAUNT_1_gotoCoord
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
					WAIT(0)
					IF IS_PED_INJURED(thisTaunterPed)
						
						CLEAR_PED_TASKS(thisTaunterPed)
					
						TASK_GO_STRAIGHT_TO_COORD(thisTaunterPed,
								 vPlayer_scene_f_lamtaunt_coord+(vTaunter_wanderOffset[iPed]*0.1),
								 PEDMOVE_WALK, -1)
						FORCE_PED_MOTION_STATE(thisTaunterPed, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(thisTaunterPed)
							
						CLEAR_RAGDOLL_BLOCKING_FLAGS(thisTaunterPed, RBF_VEHICLE_IMPACT)
					ENDIF
					
					WAIT(0)
					
					iTaunterStage[iPed] = iCONST_LAMTAUNT_1_gotoCoord
					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_LAMTAUNT_1_gotoCoord
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  gotoCoord...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneTextWithOffset("gotoCoord...", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisTaunterPed),
						vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
						HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(thisTaunterPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK)
					TASK_FOLLOW_NAV_MESH_TO_COORD(thisTaunterPed,
							 vPlayer_scene_f_lamtaunt_coord+vTaunter_wanderOffset[iPed],
							 PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, eLamtauntNavmeshFlag)
					
					iTaunterStage[iPed] = iCONST_LAMTAUNT_2_navmesh
					RETURN TRUE
				ENDIF
				
				FLOAT fGotoCoord_dist
				fGotoCoord_dist = VMAG(vTaunter_wanderOffset[iPed]*0.05)
				
				#IF IS_DEBUG_BUILD
				DrawDebugSceneSphere(vPlayer_scene_f_lamtaunt_coord, fGotoCoord_dist, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(thisTaunterPed),vPlayer_scene_f_lamtaunt_coord) >= fGotoCoord_dist
					IF NOT Is_Player_Timetable_Scene_In_Progress()
					AND NOT IS_ENTITY_ON_SCREEN(thisTaunterPed)
						TASK_FOLLOW_NAV_MESH_TO_COORD(thisTaunterPed,
								 vPlayer_scene_f_lamtaunt_coord+vTaunter_wanderOffset[iPed],
								 PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, eLamtauntNavmeshFlag)
						
						iTaunterStage[iPed] = iCONST_LAMTAUNT_2_navmesh
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisTaunterPed)
				OR IS_PED_IN_COMBAT(thisTaunterPed)
					iTaunterStage[iPed] = iCONST_LAMTAUNT_10_hating
					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_LAMTAUNT_2_navmesh
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  navmesh...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(thisTaunterPed, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				str  = "navmesh["
				SWITCH enavmesh_route_result
					CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
					DEFAULT
						str += ENUM_TO_INT(enavmesh_route_result)
					BREAK
				ENDSWITCH
				str += ", "
				str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
				str += "m, s:"
				str += (iOut_ThisIsLastRouteSection)
				str += "]"
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisTaunterPed),
						vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
						HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				IF IS_PED_RAGDOLL(thisTaunterPed) OR IS_PED_GETTING_UP(thisTaunterPed)
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisTaunterPed)
				OR IS_PED_IN_COMBAT(thisTaunterPed)
					iTaunterStage[iPed] = iCONST_LAMTAUNT_10_hating
					RETURN TRUE
				ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(thisTaunterPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
				
					IF (fTaunter_wanderRadius[iPed] = 0)
						fTaunter_wanderRadius[iPed] = VMAG(vTaunter_wanderOffset[iPed])*0.75
						
						#IF IS_DEBUG_BUILD
						
						IF NOT bGeneraredTaunterWander
							
							SCRIPT_ASSERT("missing taunter wander")
							
							SAVE_STRING_TO_DEBUG_FILE("missing taunter wander for [")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(selected_player_scene_f_lamtaunter_scene))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							bGeneraredTaunterWander = TRUE
						ENDIF
						
						SAVE_STRING_TO_DEBUG_FILE("fTaunter_wanderRadius[")
						SAVE_INT_TO_DEBUG_FILE(iPed)
						SAVE_STRING_TO_DEBUG_FILE("] = ")
						SAVE_FLOAT_TO_DEBUG_FILE(fTaunter_wanderRadius[iPed])
						SAVE_NEWLINE_TO_DEBUG_FILE()
						#ENDIF
					ENDIF
					
					//TASK_WANDER_STANDARD(thisTaunterPed)
					TASK_WANDER_IN_AREA(thisTaunterPed,
							vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
							fTaunter_wanderRadius[iPed])
							
					iTaunterStage[iPed] = iCONST_LAMTAUNT_3_wander
					RETURN TRUE
				ENDIF
				
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisTaunterPed)
				OR IS_PED_IN_COMBAT(thisTaunterPed)
					iTaunterStage[iPed] = iCONST_LAMTAUNT_10_hating
					RETURN TRUE
				ENDIF
			BREAK
			CASE iCONST_LAMTAUNT_3_wander
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  wander...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneTextWithOffset("wander...", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneSphere(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
						fTaunter_wanderRadius[iPed],
						HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult*0.5)
				#ENDIF
				
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisTaunterPed)
				OR IS_PED_IN_COMBAT(thisTaunterPed)
					iTaunterStage[iPed] = iCONST_LAMTAUNT_10_hating
					RETURN TRUE
				ENDIF
				
				IF IS_PED_RAGDOLL(thisTaunterPed) OR IS_PED_GETTING_UP(thisTaunterPed)
					RETURN FALSE
				ENDIF
				
				IF IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
					CPRINTLN(DEBUG_SWITCH, "disable \"", scenarioTypeName, "\"...")
					
					SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, FALSE)
				ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(thisTaunterPed), vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed]) < (fTaunter_wanderRadius[iPed]*fTaunter_wanderRadius[iPed])
					IF DOES_SCENARIO_EXIST_IN_AREA(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								fTaunter_wanderRadius[iPed], TRUE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(thisTaunterPed,
								vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								fTaunter_wanderRadius[iPed], 0)
//						REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlockingIndex)
								
						iTaunterStage[iPed] = iCONST_LAMTAUNT_4_useNearScenario
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_LAMTAUNT_4_useNearScenario
				
//				IF ShouldShopperFleeFromPlayer(thisTaunterPed)
//					UpdateLamTauntPedStage(iCONST_LAMTAUNT_8_fleePlayer)
//					RETURN FALSE
//				ENDIF
//				
//				IF CanShopperBeDeleted(thisTaunterPed)
//					RETURN FALSE
//				ENDIF
				
				IF PED_HAS_USE_SCENARIO_TASK(thisTaunterPed)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneString("  using near scenario", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUEDARK, fLamTaunterAlphaMult)
					DrawDebugSceneTextWithOffset("using near scenario", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUEDARK, fLamTaunterAlphaMult)
					
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
							GET_ENTITY_COORDS(thisTaunterPed),
							HUD_COLOUR_BLUEDARK, fLamTaunterAlphaMult)
					DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisTaunterPed),
							vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
							HUD_COLOUR_BLUEDARK, fLamTaunterAlphaMult)
					DrawDebugSceneSphere(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
							fTaunter_wanderRadius[iPed],
							HUD_COLOUR_BLUEDARK, fLamTaunterAlphaMult*0.5)
					#ENDIF
					
					//
				ELSE
					IF IS_PED_RAGDOLL(thisTaunterPed) OR IS_PED_GETTING_UP(thisTaunterPed)
						RETURN FALSE
					ENDIF
					
					VECTOR vRandomWanderPoint, vRandomWanderCoord
					vRandomWanderPoint = GET_RANDOM_POINT_IN_DISC_UNIFORM(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
							CLAMP(fTaunter_wanderRadius[iPed]*0.9, 10.0, 100.0), 0.0)
					
					IF (GET_SCRIPT_TASK_STATUS(thisTaunterPed, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) = PERFORMING_TASK)
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  GOTO scenario...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						
						enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(thisTaunterPed, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
						str  = "scenario["
						SWITCH enavmesh_route_result
							CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
							CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
							CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
							CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
							DEFAULT
								str += ENUM_TO_INT(enavmesh_route_result)
							BREAK
						ENDSWITCH
						str += ", "
						str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
						str += "m, s:"
						str += (iOut_ThisIsLastRouteSection)
						str += "]"
						
						DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						
						DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
								GET_ENTITY_COORDS(thisTaunterPed),
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisTaunterPed),
								vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						DrawDebugSceneSphere(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								fTaunter_wanderRadius[iPed],
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult*0.5)
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawLiteralSceneString("  NO scenario...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						DrawDebugSceneTextWithOffset("NO scenario...", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						
						DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(PLAYER_PED_ID()),
								GET_ENTITY_COORDS(thisTaunterPed),
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						DrawDebugSceneLineBetweenCoords(GET_ENTITY_COORDS(thisTaunterPed),
								vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
						DrawDebugSceneSphere(vPlayer_scene_f_lamtaunt_coord + vTaunter_wanderOffset[iPed],
								fTaunter_wanderRadius[iPed],
								HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult*0.5)
						#ENDIF
						
						IF GET_SAFE_COORD_FOR_PED(vRandomWanderPoint, FALSE, vRandomWanderCoord, GSC_FLAG_NOT_ISOLATED | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
							IF VDIST2(GET_ENTITY_COORDS(thisTaunterPed), vRandomWanderCoord) > (10*10)
								TASK_FOLLOW_NAV_MESH_TO_COORD(thisTaunterPed, vRandomWanderCoord,
										 PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP,
										 DEFAULT_NAVMESH_RADIUS, eLamtauntNavmeshFlag)
								
	//							VECTOR vMin, vMax
	//							vMin = GET_ENTITY_COORDS(thisTaunterPed)-<<2,2,2>>
	//							vMax = GET_ENTITY_COORDS(thisTaunterPed)+<<2,2,2>>
	//							ScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax, DEFAULT, FALSE)
								
								iTaunterStage[iPed] = iCONST_LAMTAUNT_1_gotoCoord
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE iCONST_LAMTAUNT_10_hating
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  hating...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneTextWithOffset("hating...", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				INT iHaterPed
				REPEAT iNUM_OF_TAUNTERS iHaterPed
					IF (iHaterPed <> iPed)
						IF (iTaunterStage[iHaterPed] = iCONST_LAMTAUNT_2_navmesh)
						OR (iTaunterStage[iHaterPed] = iCONST_LAMTAUNT_3_wander)
							iTaunterStage[iHaterPed] = iCONST_LAMTAUNT_10_hating
						ENDIF
					ENDIF
				ENDREPEAT
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, taunterGroup, RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, taunterGroup)
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, taunterGroup, RELGROUPHASH_FAMILY_F)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_FAMILY_F, taunterGroup)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(thisTaunterPed, 15.0)
				
				iTaunterStage[iPed] = iCONST_LAMTAUNT_11_hated
				RETURN TRUE
			BREAK
			CASE iCONST_LAMTAUNT_11_hated
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneString("  hated...", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneTextWithOffset("hated...", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
			BREAK
			
			DEFAULT
				
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringInt("STAGE DEFAULT ", iTaunterStage[iPed], iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				DrawDebugSceneTextWithOffset("STAGE DEFAULT", GET_ENTITY_COORDS(thisTaunterPed), Offset_y, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
				
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("non-existant", iPed+5+iCONSTANTSfortauntering, HUD_COLOUR_GREY, fLamTaunterAlphaMult*0.5)
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_TAUNT_STAGE_THREE()
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayerPlayingOutAnim = FALSE
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimLoop)
		DrawLiteralSceneString("loop", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneAnimDict, tPlayerSceneAnimOut)
		IF bPlayerPlayingOutAnim
			DrawLiteralSceneString("loop, out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ELSE
			DrawLiteralSceneString("out", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		ENDIF
		
		bPlayerPlayingOutAnim = TRUE
	ENDIF
	IF NOT bPlayerPlayingOutAnim
		DrawLiteralSceneString("none", 4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ENDIF
	#ENDIF
	
	IF NOT get_NOT_PlayerHasStartedNewScene()
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			FLOAT fExitSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
			
			IF NOT bTaunterSceneSpeechLabel
				IF (fExitSynchScenePhase < fTaunterSceneSpeechLabel)
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("wait for dialogue ", fExitSynchScenePhase, 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					DrawLiteralSceneStringFloat("play dialogue ", fExitSynchScenePhase, 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
					#ENDIF
					
					IF CREATE_CONVERSATION(MyLocalPedStruct, "PRSAUD", tTaunterSceneSpeechLabel, CONV_PRIORITY_AMBIENT_MEDIUM)
						bTaunterSceneSpeechLabel = TRUE
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawLiteralSceneStringFloat("dialogue heard ", fExitSynchScenePhase, 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawLiteralSceneString("scene stopped", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		DrawLiteralSceneString("switch Spline Cam In Progress", 5+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		#ENDIF
		
	ENDIF
	
	
	INT iPed
	REPEAT COUNT_OF(taunter_ped) iPed
		ControlLamTaunterPedStage(taunter_ped[iPed], iPed)
	ENDREPEAT
	
	ControlLamTaunterPedStage(g_pScene_buddy, COUNT_OF(taunter_ped))
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Do_PS_F_TAUNT_STAGE_FOUR()
	
	#IF IS_DEBUG_BUILD
	FLOAT AnimCurrentTime = -1.0
	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Loop_SynchSceneID)
		DrawLiteralSceneStringFloat("Loop_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
		AnimCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
		DrawLiteralSceneStringFloat("Exit_SynchSceneID:", AnimCurrentTime, 0+4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ENDIF
	IF AnimCurrentTime < 0
		DrawLiteralSceneString("SynchSceneID OFF??", 0+4+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
	ENDIF
	#ENDIF
	
	INT iPed
	REPEAT COUNT_OF(taunter_ped) iPed
		ControlLamTaunterPedStage(taunter_ped[iPed], iPed)
	ENDREPEAT
	
	ControlLamTaunterPedStage(g_pScene_buddy, COUNT_OF(taunter_ped))
	PlayLamTauntPedGreet(g_pScene_buddy)
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Player_Scene_F_Taunter_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Player_Scene_F_Taunter_Variables()
	Setup_Player_Scene_F_Taunter()
	
	#IF IS_DEBUG_BUILD
	Create_Player_Scene_F_Taunter_widget()
	#ENDIF
	
	ar_ALLOW_PLAYER_SWITCH_OUTRO()
	
	WHILE bPlayer_Scene_F_Taunter_in_progress
	AND ProgressScene(BIT_FRANKLIN, NULL)
		WAIT(0)
		
		SWITCH current_player_scene_f_lamtaunter_stage
			CASE PS_F_TAUNT_switchInProgress
				IF Do_PS_F_TAUNT_switchInProgress()
					current_player_scene_f_lamtaunter_stage = PS_F_TAUNT_STAGE_TWO
				ENDIF
			BREAK
			CASE PS_F_TAUNT_STAGE_TWO
				IF Do_PS_F_TAUNT_STAGE_TWO()
					current_player_scene_f_lamtaunter_stage = PS_F_TAUNT_STAGE_THREE
				ENDIF
			BREAK
			CASE PS_F_TAUNT_STAGE_THREE
				IF Do_PS_F_TAUNT_STAGE_THREE()
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, g_pScene_buddy, "LAMAR")
					
					current_player_scene_f_lamtaunter_stage = PS_F_TAUNT_STAGE_FOUR
				ENDIF
			BREAK
			CASE PS_F_TAUNT_STAGE_FOUR
				IF Do_PS_F_TAUNT_STAGE_FOUR()
					current_player_scene_f_lamtaunter_stage = FINISHED_F_TAUNT
				ENDIF
			BREAK
			
			CASE FINISHED_F_TAUNT
				Player_Scene_F_Taunter_Finished()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		Watch_Player_Scene_F_Taunter_Widget()
		Player_Scene_F_Taunter_Debug_Options()
		
		fLamTaunterAlphaMult = 1.0
		IF current_player_scene_f_lamtaunter_stage = PS_F_TAUNT_STAGE_FOUR
			
			CONST_FLOAT fMAX_PLAYER_LAM_TAUNT_DIST	20.0
			FLOAT fPlayerLamTaunterDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayer_scene_f_lamtaunt_coord)
			
			IF DOES_ENTITY_EXIST(g_pScene_buddy)
				FLOAT fTaunterPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_pScene_buddy, FALSE))
				
				IF (fPlayerLamTaunterDist > fTaunterPedDist)
					fPlayerLamTaunterDist = fTaunterPedDist
				ENDIF
			ENDIF
			
			INT iPed
			REPEAT COUNT_OF(taunter_ped) iPed
				
				IF DOES_ENTITY_EXIST(taunter_ped[iPed])
					FLOAT fTaunterPedDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(taunter_ped[iPed], FALSE))
					
					IF (fPlayerLamTaunterDist > fTaunterPedDist)
						fPlayerLamTaunterDist = fTaunterPedDist
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			fLamTaunterAlphaMult = 1.0 - (fPlayerLamTaunterDist/fMAX_PLAYER_LAM_TAUNT_DIST)
			IF fLamTaunterAlphaMult < 0
				fLamTaunterAlphaMult = 0
			ENDIF
			
		ENDIF
		
		DrawLiteralSceneTitle(selected_player_scene_f_lamtaunter_scene,
				2+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		STRING sStage = Get_String_From_Player_Scene_F_Taunter_Stage(current_player_scene_f_lamtaunter_stage)
		DrawLiteralSceneString(GET_STRING_FROM_STRING(sStage,
				GET_LENGTH_OF_LITERAL_STRING("PS_F_"),
				GET_LENGTH_OF_LITERAL_STRING(sStage)),
				3+iCONSTANTSfortauntering, HUD_COLOUR_BLUELIGHT, fLamTaunterAlphaMult)
		#ENDIF
	ENDWHILE
	
	Player_Scene_F_Taunter_Cleanup()
ENDSCRIPT
