

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT   USE_TU_CHANGES  0


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"



  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appVLSI.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Used to launch hacking app from cellphone whilst displaying a 
//                      :   a scaleform loading screen.    
//                            
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           

//Notes:
// AppDummyApp0 used as ENUM reference
// Launch currently voiced in Cellphone_FLASHHAND
/*
                    IF SelectedAppIndex = ENUM_TO_INT (AppDummyApp0) 
                        
                        b_AppLaunchVoided = TRUE

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - Circuit breaker has no actual script association. No script requested or launched.")
                            cdPrintstring("CELLPHONE_FH - Setting fake Circuit Breaker launch global g_HackingAppHasBeenLaunched to TRUE.")
                            cdPrintnl()   
                        #endif

                        g_HackingAppHasBeenLaunched = TRUE

                    ENDIF

 */

//Fill_App_Sheet (AppDummyApp0, "CELL_CIRCBREAK", 8, "DummyHackScrp", 54, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED) in Cellphone_PRIVATE sets up app sheet details.








BOOL b_InitialVLSISetup = FALSE




ENUM eHack_Stage

    HS_INITIALISE,
    HS_WAITING_FOR_ACTIVATE,
    HS_UNLOCKING,
    HS_POST_UNLOCK_PAUSE

ENDENUM



eHack_Stage HackStage




                                                                
                           


PROC Set_Up_Test_VLSI_App()

    /*
    //Just testing with trackify methods.                                                          
    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //System 



        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Skips loading screen.
        ELSE
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Show loading screen.
        ENDIF


        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip.
        ELSE
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Show arrow on target blip.
        ENDIF

    END_SCALEFORM_MOVIE_METHOD()
    */


    
    //Should be something like this:
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1)) 

        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SHOW_HACKING_APP")



        
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_101") 
        
        END_TEXT_COMMAND_SCALEFORM_STRING()



    
        END_SCALEFORM_MOVIE_METHOD()

                   
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))      
    */





    //Temp routine.


    IF HackStage = HS_INITIALISE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppVLSI - Requested DLC_MPHEIST/HEIST_HACK_SNAKE soundbank.")
            cdPrintnl()

        #endif



        REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")

        WHILE REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE") = FALSE

            WAIT(0)

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppVLSI - Waiting on DLC_MPHEIST/HEIST_HACK_SNAKE soundbank to load...")
                cdPrintnl()

            #endif

        ENDWHILE









        //New scaleform routine that uses Trackify Loading Screen enum to manipulate hacking screen.

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 23)


        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //System 

           
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (-99)  //Negative angle removes target.
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)

     
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

  
            //Updated because of Trackify changes which mean loading screens take an enum rather than a bool.
            //LOADING_SCREEN_TYPE_HACKING = 2
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (2)



            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip.
    

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (0.0)
            ENDIF


        END_SCALEFORM_MOVIE_METHOD()


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))      


        HackStage = HS_WAITING_FOR_ACTIVATE




    ELSE


        IF HackStage = HS_WAITING_FOR_ACTIVATE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 23)


            //New scaleform routine that uses Trackify Loading Screen enum to manipulate hacking screen.
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //System 

               
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (-99)  //Negative angle removes target.
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)

         
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

      
                //Updated because of Trackify changes which mean loading screens take an enum rather than a bool.
                //LOADING_SCREEN_TYPE_HACKING_ACTIVE = 3
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (3)



                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip.
        

                IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (0.0)
                ENDIF


            END_SCALEFORM_MOVIE_METHOD()


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))      



            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("AppVLSI - Updating Hack screen to UNLOCKING.")
                cdPrintnl()   
            #endif




            HackStage = HS_UNLOCKING



        ENDIF


    ENDIF


ENDPROC










PROC Set_Up_VLSI_Buttons()



    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"YES" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative


    ELSE 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


     
ENDPROC





PROC Set_Up_VLSI_Buttons_Unlocking()



    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"YES" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative


    ELSE 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


     
ENDPROC























PROC Cleanup_and_Terminate()


    RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("AppVLSI - appVLSI exiting normally and removing soundbank...")
        PRINTNL()

        cdPrintnl()
        cdPrintstring ("AppVLSI - S_P_D appVLSI exiting normally and removing soundbank...")
        cdPrintnl()

    #endif



    TERMINATE_THIS_THREAD()


ENDPROC 












SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    


    HackStage = HS_INITIALISE

    

    IF b_InitialVLSISetup = FALSE

        Set_Up_Test_VLSI_App()
        Set_Up_VLSI_Buttons()

        b_InitialVLSISetup = TRUE

    ENDIF

    
     
    WHILE TRUE

        WAIT(0)
        

     

        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        
            SWITCH g_Cellphone.PhoneDS
             
            

                CASE PDS_RUNNINGAPP
                                             
                    //We need to set this critical bool up:
                    //g_HackingAppHasBeenLaunched = TRUE  
                    //after an appVLSI loading screen, to let Colin C's stuff know that his minigame should be launched.
                    //Then CLEANUP_AND_TERMINATE?




                    IF HackStage = HS_WAITING_FOR_ACTIVATE

                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) 
                    
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("App VLSI - Positive button pressed during wait phase. Proceeding to UNLOCK stage.")
                                cdPrintnl()
                            #endif
                          
                            //Play_Select_Beep()


                            PLAY_SOUND_FRONTEND(-1, "Lester_Laugh_Phone", "DLC_HEIST_HACKING_SNAKE_SOUNDS")

                            g_InputButtonJustPressed = TRUE
                            
                            Set_Up_Test_VLSI_App()

                            Set_Up_VLSI_Buttons_Unlocking()

                            SETTIMERA(0)
                            SETTIMERB(0)

                        ENDIF

                    ENDIF





                    IF HackStage = HS_UNLOCKING
                        
                        //Just a little routine to play some beeps during the unlock pause.
                        IF TIMERB() > 499

                            //Play_Select_Beep()
                            SETTIMERB(0)
                        
                        ENDIF

                        IF TIMERA() > 2200

                            HackStage = HS_POST_UNLOCK_PAUSE

                        ENDIF

                    ENDIF





                    
                    IF HackStage = HS_POST_UNLOCK_PAUSE

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("App VLSI - setting g_HackingAppHasBeenLaunched to TRUE now pause has expired.")
                            cdPrintnl()
                        #endif
    
                        g_HackingAppHasBeenLaunched = TRUE

                    ENDIF






         
                BREAK

                                                                                                                             

                CASE PDS_COMPLEXAPP


                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

               

                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 23198. AppVLSI assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                    ENDIF

                BREAK
               


                DEFAULT

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("AppVLSI in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  


    

                                                                                                            


       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Repeats list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT


