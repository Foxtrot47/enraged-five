

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "organiser_public.sch"
USING "cellphone_private.sch"
USING "stack_sizes.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME    :    appOrganiser.sc
//      AUTHOR          :   Steve T, Ak
//      DESCRIPTION     :   Placeholder Organiser script, launched from phone
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


BOOL bActivated = FALSE
BOOL bViewSwitchedToOrganiser = FALSE

BOOL bButtonUp = FALSE
BOOL bButtonDown = FALSE
BOOL bButtonLeft = FALSE
BOOL bButtonRight = FALSE

PROC DO_GENERIC_DIRECTIONAL_INPUTS()
	//check for the directional buttons to feed the directly to scaleform
	//check for the action buttons to interact with the menu

	IF NOT (bButtonUp)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
			bButtonUp = TRUE
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_INPUT_EVENT",1)//up

		ENDIF
	ELSE //wait for release
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
			bButtonUp = FALSE
		ENDIF
	ENDIF
	
	IF NOT (bButtonDown)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
			bButtonDown = TRUE
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_INPUT_EVENT",3)//down
		ENDIF
	ELSE //wait for release
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
			bButtonDown = FALSE
		ENDIF
	ENDIF
	
	IF NOT (bButtonLeft)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
			bButtonLeft = TRUE
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_INPUT_EVENT",4)//left
			

		ENDIF
	ELSE //wait for release
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
			bButtonLeft = FALSE
		ENDIF
	ENDIF
	
	IF NOT (bButtonRight)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
			bButtonRight = TRUE
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_INPUT_EVENT",2)//right
			

		ENDIF
	ELSE //wait for release
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
			bButtonRight = FALSE
		ENDIF
	ENDIF
ENDPROC






PROC Cleanup_and_Terminate()


    TERMINATE_THIS_THREAD()


ENDPROC








SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Cleanup_and_Terminate()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	CHECK_FOR_ORGANISER_TIME_CHANGE()
	//PLACE_EVENT_IN_ORGANISER_TIMESLOT(ORG_EVT_DEBUG,3,0,1,TRUE)

	//PLACE_EVENT_IN_ORGANISER_TIMESLOT(ORG_EVT_DEBUG,12,1,1,TRUE)
	
	//PLACE_EVENT_IN_ORGANISER_TIMESLOT(ORG_EVT_DEBUG,17,2,1,TRUE)
	
	//PLACE_EVENT_IN_ORGANISER_TIMESLOT(ORG_EVT_DEBUG,20,3,1,TRUE)
	
	
	//PLACE_EVENT_IN_ABSOLUTE_ORGANISER_TIMESLOT(ORG_EVT_HOUSE_VIEWING, 7, GET_CLOCK_DAY_OF_WEEK(), 11)
	
    WHILE TRUE

        WAIT(0)
       

        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL   //Leave this in, Andrew.

			//call email update here
			
			IF NOT bActivated
				//perform first run action // attempt to get phone
				bActivated = TRUE
				
				
			ELSE
				IF g_B_Scaleform_Movies_Loaded //the phone movie is loaded
				//update phone
					IF NOT bViewSwitchedToOrganiser
						bViewSwitchedToOrganiser = TRUE
						LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",5)
						
						//do first data update						
						UPDATE_ORGANISER_SCALEFORM(SF_MovieIndex, TRUE)//force an update
						LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",5)
					ELSE
							//update view
							DO_GENERIC_DIRECTIONAL_INPUTS()
							
							//check for selection input

					ENDIF
				ENDIF
			ENDIF
			
			//When selected set g_Cellphone.PhoneDS = PDS_COMPLEXAPP
			
			//set back to  PDS_RUNNINGAPP  when at top level of menu

            
            
         
            IF CHECK_FOR_APPLICATION_EXIT()  //Leave this in, Andrew.

                Cleanup_and_Terminate()

            ENDIF

        ENDIF




        IF CHECK_FOR_ABNORMAL_EXIT()     //Leave this in, Andrew.

            Cleanup_and_Terminate()

        ENDIF



        
            
    ENDWHILE
    
    
ENDSCRIPT


