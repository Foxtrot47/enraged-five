USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "friends_public.sch"
USING "randomChar_public.sch"
USING "appMPPlayerListFunctions.sch"
USING "FMMC_keyboard.sch"
USING "cheat_controller_public.sch"
USING "cellphone_movement.sch"
USING "net_cash_transactions.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appContacts.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Create contacts list by referencing information held in character
//                          sheet and checking phonebook presence field. If the character is 
//                          included in a phonebook, add him/her to the contacts list.
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



//Moved to globals so that it can be interrogated by public functions.
//INT ContactListSlot[MaxNumberOfContactSlots]


BOOL b_VoidContactSelectionDueToForce = FALSE

BOOL b_TryToPlaceContact = FALSE

FLOAT local_alphafloat, top_local_alphafloat

INT i_Number_Of_Contacts_In_List = 0

INT FriendSlot_OriginIndex = 0
INT FriendSlot_StartingIndex = 9999 //This needs to be initialised as a large number so later comparisons with slot zero cannot return true #1245546

//For Friends at top format...
INT NPCSlot_StartingIndex = 9999


INT ListCursorIndex = 0
SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex
INT i_Selected_Slot_to_Restore


INT ContactNamesToDraw = 0

INT SF_SelectedContactInt, SF_SelectedChoiceInt //stores a temp reference to the selected contact for error checking purposes.

INT Phone_Dialling_SoundID



//String handling for numpad entry
INT SelectedNumpadIndex
INT NewStringIndex = 0

STRING NewStringToAppend[1] //Greatly reduced from 20. We don't need these and I'm trying to get the stack size down for #1641893

TEXT_LABEL NumpadEntryString
INT NumpadEntryStringLength



BOOL b_DoFriend_SecondaryCallRoutine = FALSE
BOOL b_DoCancelConfirmation = FALSE

BOOL b_DoHumanFriend_Routine = FALSE




BOOL b_EngagedBypassCheck = FALSE
BOOL b_TrevorEngagedCurseSetup = FALSE

BOOL BypassGameAssociation = FALSE

BOOL b_ThisIsLastGenBonusCheat = FALSE
BOOL b_ThisCheatBlocked = FALSE



//enumPhonebookPresence PersonalPhonebookToCheck //we need to check which character we are using and store which phonebook we should base the contacts list on.



//Temp
FLOAT listOriginX = 0.82
FLOAT listOriginY = 0.42


FLOAT drawItemX = 0.0
FLOAT drawItemY = 0.0

BOOL dpad_scroll_pause_cued = FALSE
//BOOL b_ContactForCamPicSelected = FALSE


ENUM eCalling_Contact_Progress

     CC_LAUNCH_TIMER_AND_CHECK_MESSAGE,
     CC_CHEAT_PAUSE,
     CC_CONNECTION_PAUSE,
     CC_PLAY_ANSWERPHONE,
     CC_ENGAGED

ENDENUM


eCalling_Contact_Progress cc_Progress = CC_LAUNCH_TIMER_AND_CHECK_MESSAGE


//Connection length and answerphone block - now moved to cellphone_globals.sch
/*
INT Connection_EngagedPauseLength = 4000
INT Connection_AnsMsgPauseLength = 7000
STRING answerPhoneBlock = "H1AUD"
*/




INT Connection_PauseLength



//Connection and Finale Choice Timers
INT Connection_AttemptStartTime, Connection_AttemptCurrentTime
INT FinaleChoice_StartTime, FinaleChoice_CurrentTime

BOOL b_DoFinaleConfirmation = FALSE



//Holder for answerphone label check
TEXT_LABEL Check_AnswerphoneLabel


structPedsForConversation AppContactPedStruct


//Holds copies for Finale Choice manipulation.
INT Master_LesterInt, Master_TrevorInt, Master_MichaelInt




//Gameworld Number temporary data storage

BOOL DialledNumberHasGameworldAssociation = FALSE
TEXT_LABEL GameworldAssociatedRoot
TEXT_LABEL_23 GameworldAssociatedVoice




TEXT_LABEL_3 singleCharacter[7] //This has to be a text label as a string address in memory can be overwritten. See Graeme.
//DecisionCharacter[2]

INT iBoolsBitSet
CONST_INT iBS_LAUNCH_STORE              0
CONST_INT iBS_STORE_ACTIVE              1

BOOL b_Secondary_Option_Available = FALSE
BOOL b_Friend_Option_Available = FALSE
INT i_Available_Option = 0
INT tempDynamicIndex = 0 //This cannot be declared in switch where it would ideally be...

INT iRandomNumber


//////// MP VARIABLES
m_structPlayerList  m_sPlayers

STRUCT KEYBOARD_DATA 
    OSK_STATUS eStatus
    INT iProfanityToken
    INT iCurrentStatus
ENDSTRUCT
KEYBOARD_DATA KeyBoardData
TEXT_LABEL_63 tlSendMessage
BOOL bCreatingSendMessage
INT iStringLengthInCharacters


FUNC BOOL Is_SinglePlayer_Climbing()

    IF g_bInMultiplayer

        RETURN FALSE //Player does not hold phone whilst in MP. We don't need to check climbing at all here.

    ELSE

        IF IS_PLAYER_PLAYING (PLAYER_ID())
            IF IS_PLAYER_CLIMBING(PLAYER_ID()) //See 843727 - Don't allow dial out while on ladder.
                

                #if IS_DEBUG_BUILD
                    cdPrintstring("appContacts - Singleplayer climbing - No selection action taken.")
                    cdPrintnl()
                #endif


                RETURN TRUE
            
            ELSE


                RETURN FALSE

            ENDIF

        ELSE
        
            RETURN FALSE

        ENDIF

    ENDIF


ENDFUNC




FUNC BOOL IS_THIS_CHARACTER_FRIEND_OF_CURRENT_PLAYER_CHAR (enumCharacterList charToQuery)

    
    SWITCH g_Cellphone.PhoneOwner           
            

            CASE CHAR_MICHAEL  


                IF ARE_CHARS_FRIENDS(CharToQuery, CHAR_MICHAEL)

                    RETURN TRUE

                ELSE

                    RETURN FALSE

                ENDIF

            BREAK


            CASE CHAR_FRANKLIN
                

                IF ARE_CHARS_FRIENDS(CharToQuery, CHAR_FRANKLIN)

                    RETURN TRUE

                ELSE

                    RETURN FALSE

                ENDIF

            
            BREAK


            CASE CHAR_TREVOR


                IF ARE_CHARS_FRIENDS(CharToQuery, CHAR_TREVOR)

            
                    RETURN TRUE
            
                ELSE

                    RETURN FALSE

                ENDIF


            BREAK
        

            DEFAULT
            
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PRIV - Abnormal owner decision, no is_friend result available")
                    cdPrintnl()

                #endif

                RETURN FALSE


            BREAK

    ENDSWITCH



    RETURN FALSE


ENDFUNC






PROC Set_Up_FinaleConfirmationScreen()

        FinaleChoice_StartTime = GET_GAME_TIMER() //Get launch time for the purpose of creating a brief pause to prevent button mashing selecting confirmation prematurely.


        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 7) //Quick change of view to make sure data is flushed from view state 2.


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 2)


        WAIT (0)
        
        PRINTNL()
        PRINTSTRING ("TESTING FLUSH!")
        PRINTNL()

        

        SWITCH g_TheContactInvolvedinCall

                                   
            CASE CHAR_TREVOR

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(0)), 0,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7001", 
                    
                     //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture)
            
            BREAK


            CASE CHAR_MICHAEL

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(0)), 0,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,"CELL_7002",
                    
                    //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture) 
            
            BREAK


            CASE CHAR_LESTER

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(0)), 0,
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7003",
                
                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                 g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                 
                "CELL_E_311") 
            
            BREAK


        ENDSWITCH







        //Set up yes / no soft keys for this confirmation screen.
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" - Positive
        ELSE 
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive
        ENDIF



        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative
        ELSE 
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
        ENDIF



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2)

        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_249") //"Are you sure?"



ENDPROC





/*
PROC Do_FinaleConfirmation()


        #if IS_DEBUG_BUILD 
            PRINTNL()
            PRINTSTRING ("Testing finale confirmation")
            PRINTNL()
            PRINTNL()
        #endif

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.


                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


                b_DoFinaleConfirmation = FALSE

                Play_Select_Beep()


                g_InputButtonJustPressed = TRUE


                    //Set bools to specify this is a story call here!
                g_b_Is_This_An_Activity_Call = FALSE
                g_b_Is_This_An_Activity_Cancellation_Call = FALSE


                Do_Call()


        ENDIF






        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full contact list display.
                    

            b_DoFinaleConfirmation = FALSE



            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

            

            Play_Back_Beep()

            g_InputButtonJustPressed = TRUE

          
            Place_Names_in_ContactSlots()

            WAIT(0) //make sure this JUST_PRESSED doesn't carry over by stopping for a game cycle.
            

            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 9672. appContacts assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            ENDIF

            NumpadEntryString ="" //Reset entry string.

        ENDIF





ENDPROC
*/






PROC Post_Aborted_String_Entry_Button_Creation()     //Safer to have this completely separate and not used as a common proc with Post_placement_setup even though it's the same at present.
                                                    //This is currently only used by the routine added for TODO 1580608 and was part of the solution to the later bug 1643709



 IF NOT (g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT)   //This script can be launched by "calling" text message senders. In that case we want to go directly    
    AND NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)                //to the calling screen specified in appTextMessage so we bypass the initial contacts list screen setup below.

    
        //Bug #1025518
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //Fix for flicking during photo send. Should be safe enough to have a wait in Port Heist mission.
            WAIT (750)
        ENDIF

        
        IF g_CellPhone.PhoneDS > PDS_MAXIMUM //make sure we are in running app state at least here...

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2) //Set enum state and header to CONTACTS

      

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_7000")
            
            ELSE

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_0")

            ENDIF


            ClockThrottleMins = 99 //trigger clock update.


            //AppContacts can be launched from the cellphone camera application. If the "about to send pic" bit is set we change the buttons accordingly

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)


                //Badger - Select Back combo
                IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                
                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)






            ELSE

 


                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) //don't display disabled soft keys...



                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                    ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                    ENDIF

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other





                ELSE


                     //Badger - Call, Keypad, Back combo
                    IF g_b_ToggleButtonLabels //prologue Cellphone will default to displaying button labels.

                        IF i_Number_Of_Contacts_In_List > 0 //Only display call button if there is at least one contact.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive

                        ENDIF

                        IF g_Use_Prologue_Cellphone = FALSE  //only display enter no. for modern phones...
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_207") //"ENTER NO." - Other
                            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
                            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                        ENDIF
                        

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                    ELSE

                        IF i_Number_Of_Contacts_In_List > 0 //Only display call button if there is at least one contact.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive

                        ENDIF


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                    ENDIF

                
                ENDIF

            ENDIF


    
        ENDIF


    ENDIF






ENDPROC








PROC Post_Placement_Setup_and_Checks()

    
    IF NOT (g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT)   //This script can be launched by "calling" text message senders. In that case we want to go directly    
    AND NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)                //to the calling screen specified in appTextMessage so we bypass the initial contacts list screen setup below.

    
        //Bug #1025518
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //Fix for flicking during photo send. Should be safe enough to have a wait in Port Heist mission.
            WAIT (750)
        ENDIF

        
        IF g_CellPhone.PhoneDS > PDS_MAXIMUM //make sure we are in running app state at least here...

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2) //Set enum state and header to CONTACTS

      

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_7000")
            
            ELSE

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_0")

            ENDIF


            ClockThrottleMins = 99 //trigger clock update.


            //AppContacts can be launched from the cellphone camera application. If the "about to send pic" bit is set we change the buttons accordingly

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

                
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"SEND PIC" Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
                */


                //Badger - Select Back combo
                IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                
                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)






            ELSE

                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_207") //"ENTER NO." - Other
                */




                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) //don't display disabled soft keys...



                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                    ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                    ENDIF

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other





                ELSE


                     //Badger - Call, Keypad, Back combo
                    IF g_b_ToggleButtonLabels //prologue Cellphone will default to displaying button labels.

                        IF i_Number_Of_Contacts_In_List > 0 //Only display call button if there is at least one contact.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive

                        ENDIF

                        IF g_Use_Prologue_Cellphone = FALSE  //only display enter no. for modern phones...
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_207") //"ENTER NO." - Other
                            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                        ELSE
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
                            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                        ENDIF
                        

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                    ELSE

                        IF i_Number_Of_Contacts_In_List > 0 //Only display call button if there is at least one contact.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive

                        ENDIF


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            9, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"ENTER NO." - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                    ENDIF

                
                ENDIF

            ENDIF


    
        ENDIF


    ENDIF

    //Critical! Restore Master alpha ints as Finale Choice contacts list may have altered them.
  	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_LESTER, Master_LesterInt)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_TREVOR, Master_TrevorInt)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MICHAEL, Master_MichaelInt)
ENDPROC







PROC ADD_FRIEND_TO_SLOT(STRING playerName)
    
    #if IS_DEBUG_BUILD

        PRINTSTRING("Populating friend slot ")
        PRINTINT(FriendSlot_OriginIndex)
        PRINTNL()

    #endif       
    
    

    
    
      
//   //Put a dummy friend in the first free slot after the Story Contacts have been added.
//    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(FriendSlot_OriginIndex)), 0,
//        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
//        DummyFriend[NumberOfDummyFriends], 
//        
//        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
//        "CELL_300",
//        
//        //Display temporary blank headshot.
//        "CELL_300")
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(2))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((TO_FLOAT(FriendSlot_OriginIndex)))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
        
        // The Player Name (literal string composed using the Text Commands)
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(playerName)
        END_TEXT_COMMAND_SCALEFORM_STRING()
        
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_MP_999")//("CELL_MP_999") //CHAR_HUMANDEFAULT
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_MP_999")//("CELL_MP_999") //CHAR_HUMANDEFAULT - This one sets the headshot to the left of the name in the contacts list.

        //Setting this with a null string will display no headshot at all...
        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("") //This one sets the contact pic.
            


    END_SCALEFORM_MOVIE_METHOD()

    FriendSlot_OriginIndex ++
    
     //Critical! Conor, this is needed for a navigation back up, so when something is added to the slot it needs incremented.    
    i_Number_Of_Contacts_In_List ++ 
    
     #if IS_DEBUG_BUILD 
       
        PRINTSTRING("September 2014 - NumContacts in list after friend additions is ")
        PRINTINT(i_Number_Of_Contacts_In_List)
        PRINTNL()

    #endif
ENDPROC








PROC ADD_MP_FRIEND_TO_SLOT(STRING playerName)
    
    #if IS_DEBUG_BUILD

        PRINTSTRING("Populating friend slot ")
        PRINTINT(FriendSlot_OriginIndex)
        PRINTNL()

    #endif    
    

    
         
//   //Put a dummy friend in the first free slot after the Story Contacts have been added.
//    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(FriendSlot_OriginIndex)), 0,
//        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
//        DummyFriend[NumberOfDummyFriends], 
//        
//        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
//        "CELL_300",
//        
//        //Display temporary blank headshot.
//        "CELL_300")
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(2))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((TO_FLOAT(FriendSlot_OriginIndex)))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
        
        // The Player Name (literal string composed using the Text Commands)
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(playerName)
        END_TEXT_COMMAND_SCALEFORM_STRING()
        
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_MP_999")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_MP_999") //("CELL_MP_999") //CHAR_HUMANDEFAULT - This one sets the headshot to the left of the name in the contacts list.

        //Setting this with a null string will display no headshot at all...
        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("") //This one sets the contact pic.



    
    END_SCALEFORM_MOVIE_METHOD()

    FriendSlot_OriginIndex ++
    
     //Critical! Conor, this is needed for a navigation back up, so when something is added to the slot it needs incremented.    
    i_Number_Of_Contacts_In_List ++ 
    
     #if IS_DEBUG_BUILD 
       
        PRINTSTRING("September 2014 - NumContacts in list after friend additions is ")
        PRINTINT(i_Number_Of_Contacts_In_List)
        PRINTNL()

    #endif
ENDPROC






















PROC Place_Friends_in_ContactSlots()

	#if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppContacts - 02 Sep 3038184. 18th May drastic action for bug 2828375 - Nuking any save game alphabetical ordering for male and female assistant contacts for Executive pack via Place_Friends")
        cdPrintnl()   
     #endif
	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_BIKER_CH1, 1320)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSISTF, 180)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSIST, 190)

    // Gather the details for all other players (ignoring current player)
    // KGM 3/8/11: For now, just create a list of players in the network game - unsorted, etc
    PLAYER_INDEX    thisPlayer  = NULL
    INT             thisSlot    = 0
    INT             tempLoop    = 0
    GAMER_HANDLE friendHandle
    
    
    //Critical.
    FriendSlot_OriginIndex = i_Number_Of_Contacts_In_List   //Marks the scaleform slot where the story contacts finished and the friends should start.


    //Critical, Conor, we need to keep this so we know where the friends slots start from when a slot is selected.
    FriendSlot_StartingIndex = FriendSlot_OriginIndex

    Clear_PlayerList_Details(m_sPlayers)


    BOOL Human_Contacts_Barred_During_MP_Tutorial = FALSE

    IF g_bInMultiplayer
        IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() = FALSE //#1545235 - function from #1601899

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintString("ST - Place_Friends_in_ContactSlots() reporting that the player is in MP but has not completed the tutorial, not placing friends or session players in list") 
                cdPrintnl()

            #endif

            Human_Contacts_Barred_During_MP_Tutorial = TRUE
            
        ENDIF
    ENDIF   

    
    
    REPEAT NUM_NETWORK_PLAYERS tempLoop
        thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
        IF IS_NET_PLAYER_OK(thisPlayer,FALSE)
            IF Human_Contacts_Barred_During_MP_Tutorial = FALSE //#1545235  
                IF NOT (thisPlayer = PLAYER_ID())

                    m_sPlayers.plPlayers[thisSlot] = thisPlayer
                    ADD_FRIEND_TO_SLOT(GET_PLAYER_NAME(thisPlayer))

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("AppContacts Place Normal - Adding Network Player:")
                        cdPrintstring(GET_PLAYER_NAME(thisPlayer))
                        cdPrintnl()
                    #endif



                    thisSlot++
                
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    m_sPlayers.plNumPlayers = thisSlot
    
    tempLoop = 0
    thisSlot = 0
    INT iMaxFriends    
    IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())

        iMaxFriends = NETWORK_GET_FRIEND_COUNT()
        IF iMaxFriends > 100
            iMaxFriends = 100 
        ENDIF
    
        FOR tempLoop = 0 TO iMaxFriends - 1
            IF NETWORK_IS_FRIEND_ONLINE(NETWORK_GET_FRIEND_NAME(tempLoop))
                IF NETWORK_IS_FRIEND_IN_SAME_TITLE(NETWORK_GET_FRIEND_NAME(tempLoop))
                    friendHandle = GET_GAMER_HANDLE_FRIEND(tempLoop)
                    IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(friendHandle)
                        IF Human_Contacts_Barred_During_MP_Tutorial = FALSE //#1545235  

                            IF thisSlot < 100
                                m_sPlayers.plFriends[thisSlot].Handle = friendHandle
                                IF IS_XBOX_PLATFORM()
                                    m_sPlayers.plFriends[thisSlot].Name = NETWORK_GET_FRIEND_DISPLAY_NAME(tempLoop)
                                ELSE
                                    m_sPlayers.plFriends[thisSlot].Name = NETWORK_GET_FRIEND_NAME(tempLoop)
                                ENDIF
                                ADD_FRIEND_TO_SLOT(m_sPlayers.plFriends[thisSlot].Name)

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("AppContacts Place Normal - Adding online friend who is not in session:")
                                    cdPrintstring(m_sPlayers.plFriends[thisSlot].Name)
                                    cdPrintnl()
                                #endif

                                thisSlot++

                            ENDIF
                    
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDFOR
    ENDIF
    m_sPlayers.plTotalNumContacts = m_sPlayers.plNumPlayers + thisSlot


ENDPROC





PROC Place_MP_Friends_in_ContactSlots()

	 #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppContacts - 02 September. 18th May drastic action for bug 2828375 - Nuking any save game alphabetical ordering for male and female assistant contacts for Executive pack via Place_MP_Friends")
        cdPrintnl()   
     #endif
	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_BIKER_CH1, 1320)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSISTF, 180)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSIST, 190)

    // Gather the details for all other players (ignoring current player)
    // KGM 3/8/11: For now, just create a list of players in the network game - unsorted, etc
    PLAYER_INDEX    thisPlayer  = NULL
    INT             thisSlot    = 0
    INT             tempLoop    = 0
    GAMER_HANDLE friendHandle
    
    
    //Critical.
    FriendSlot_OriginIndex = i_Number_Of_Contacts_In_List   //Marks the scaleform slot where the story contacts finished and the friends should start.


    //Critical, Conor, we need to keep this so we know where the friends slots start from when a slot is selected.
    FriendSlot_StartingIndex = FriendSlot_OriginIndex

    Clear_PlayerList_Details(m_sPlayers)


    BOOL Human_Contacts_Barred_During_MP_Tutorial = FALSE

    IF g_bInMultiplayer
        IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() = FALSE //#1545235 - function from #1601899

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintString("ST - Place_MP_Friends_in_ContactSlots() reporting that the player is in MP but has not completed the tutorial, not placing friends or session players in list") 
                cdPrintnl()

            #endif

            Human_Contacts_Barred_During_MP_Tutorial = TRUE
            
        ENDIF
    ENDIF   

    
    
    REPEAT NUM_NETWORK_PLAYERS tempLoop
        thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
        IF IS_NET_PLAYER_OK(thisPlayer,FALSE)
            IF Human_Contacts_Barred_During_MP_Tutorial = FALSE //#1545235  
                IF NOT (thisPlayer = PLAYER_ID())

                    m_sPlayers.plPlayers[thisSlot] = thisPlayer
                    ADD_MP_FRIEND_TO_SLOT(GET_PLAYER_NAME(thisPlayer))

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("AppContacts Place MP - Adding Network Player:")
                        cdPrintstring(GET_PLAYER_NAME(thisPlayer))
                        cdPrintnl()
                    #endif


                    thisSlot++
                
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    m_sPlayers.plNumPlayers = thisSlot
    
    
    thisSlot = 0
    tempLoop = 0
    INT iMaxFriends
    IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())

        iMaxFriends = NETWORK_GET_FRIEND_COUNT()
        IF iMaxFriends > 100
            iMaxFriends = 100 
        ENDIF

        FOR tempLoop = 0 TO iMaxFriends - 1
            IF NETWORK_IS_FRIEND_ONLINE(NETWORK_GET_FRIEND_NAME(tempLoop))
                IF NETWORK_IS_FRIEND_IN_SAME_TITLE(NETWORK_GET_FRIEND_NAME(tempLoop))
                    friendHandle = GET_GAMER_HANDLE_FRIEND(tempLoop)
                    IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(friendHandle)
                        IF Human_Contacts_Barred_During_MP_Tutorial = FALSE //#1545235  

                            IF thisSlot < 100
                                m_sPlayers.plFriends[thisSlot].Handle = friendHandle
                                IF IS_XBOX_PLATFORM()
                                    m_sPlayers.plFriends[thisSlot].Name = NETWORK_GET_FRIEND_DISPLAY_NAME(tempLoop)
                                ELSE
                                    m_sPlayers.plFriends[thisSlot].Name = NETWORK_GET_FRIEND_NAME(tempLoop)
                                ENDIF
                                
                                ADD_MP_FRIEND_TO_SLOT(m_sPlayers.plFriends[thisSlot].Name)

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("AppContacts Place MP - Adding online friend who is not in session:")
                                    cdPrintstring(m_sPlayers.plFriends[thisSlot].Name)
                                    cdPrintnl()
                                #endif

                                thisSlot++

                            ENDIF
                    
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDFOR
    ENDIF
    m_sPlayers.plTotalNumContacts = m_sPlayers.plNumPlayers + thisSlot


ENDPROC





PROC Place_Names_in_ContactSlots()

	#if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppContacts - 02 Sep. 18th May drastic action for bug 2828375 - Nuking any save game alphabetical ordering for male and female assistant contacts for Executive pack via Place_Names")
        cdPrintnl()   
     #endif
	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_BIKER_CH1, 1320)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSISTF, 180)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSIST, 190)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 2)

    WAIT (0)

    i_Number_Of_Contacts_In_List = 0 //reset number of contacts before filling list.


    b_DoFinaleConfirmation = FALSE //make sure this is false each time we run this proc.

    b_DoHumanFriend_Routine = FALSE


    //Display "no contacts" message in slot 1 in lieu of any contacts...
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(0)), 0,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_232", "CELL_233", "CELL_393") 



    

    BOOL contact_name_in_alpha_order[MAX_CHARACTERS_PLUS_DUMMY]

    INT slotIndex = 0

    WHILE slotIndex < (ENUM_TO_INT(MAX_CHARACTERS_MP))  

        
        //When testing ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKS() comment in this WAIT (0)
        //WAIT(0) // for testing all contacts.


        #IF IS_DEBUG_BUILD

            PRINTSTRING("Populating sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
    
        #endif         


        INT comparisonIndex = 0

        INT top_alpha_comparison = (ENUM_TO_INT(MAX_CHARACTERS_MP))

    	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), 5000)

        //Virtual Character handled by this interndal function now.
        Get_Cellphone_Owner()

        WHILE comparisonIndex < (ENUM_TO_INT(MAX_CHARACTERS_MP))

            /*
            PRINTSTRING("comparison index =")
            PRINTINT(comparisonIndex)
            PRINTNL()
            */

            
            
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                IF comparisonIndex = ENUM_TO_INT (CHAR_TREVOR)
                OR comparisonIndex = ENUM_TO_INT (CHAR_MICHAEL)
                OR comparisonIndex = ENUM_TO_INT (CHAR_LESTER)


              
                    //Critical! This alters the alphabetical order by manipulating the alphaints. Holders obtained the correct value on startup to re-establish old values at end of proc and
                    //before termination of this script.
                                       
                    //Ben, this stipulates the order in which the characters appear on the finale contact list.
                    
					GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_LESTER, 3)
					GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_TREVOR, 1)
                    GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MICHAEL, 2)                 


                    b_TryToPlaceContact = TRUE

                ELSE

                    b_TryToPlaceContact = FALSE

                ENDIF

            ELSE
                
                IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, comparisonIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED 

                    b_TryToPlaceContact = TRUE

                ELSE

                    b_TryToPlaceContact = FALSE

                ENDIF

            ENDIF
           									
			//This is the main SP Contact placement routine. For MP specific adjustments please see Place_MP_Names_in_ContactSlots() below.																				 
																									 
            IF b_TryToPlaceContact
                IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.
                    IF contact_name_in_alpha_order[comparisonIndex] = FALSE

                        //#if IS_DEBUG_BUILD
                            //cdPrintstring ("TAC ")
                            //cdPrintint(top_alpha_comparison)
                            //cdPrintnl()
                        //#endif
                       
                           
                        //If contact has missed call status, manipulate him to the top of the list by dividing his or her alphaint by 100.
                        //We can also manipulate the scheduled friend activity near the top also.
						
                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, comparisonIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, comparisonIndex)), g_Cellphone.PhoneOwner)

                            local_alphafloat = ((TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, comparisonIndex)))) / 100.0)

                        ELSE

                            local_alphafloat = TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, comparisonIndex)))

                        ENDIF


                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)

                            top_local_alphafloat = ((TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison)))) / 100.0)

                        ELSE

                            top_local_alphafloat = TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison)))

                        ENDIF
                    


                        IF local_alphafloat < top_local_alphafloat
                            
                        
                            top_alpha_comparison = comparisonIndex









                            
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


                                //Display Finale choice options. Kill Trevor, Kill Michael, Lester Deathwish.

                               
                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_TREVOR)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7001", 

                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                       	g_sCharacterSheetAll[top_alpha_comparison].label,
                                        g_sCharacterSheetAll[top_alpha_comparison].picture) 
                                
                                ENDIF

                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_MICHAEL)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,"CELL_7002",
                                        
                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                        g_sCharacterSheetAll[top_alpha_comparison].label,
                                        g_sCharacterSheetAll[top_alpha_comparison].picture)
                                

                                ENDIF


                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_LESTER)

                                    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,

                                
                                    
                                    //We manually replace the LESTER contact pic with the skull symbol for LESTER DEATHWISH. The call screen will still show LESTER by design.

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7003",
                                        
                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                       	 g_sCharacterSheetAll[top_alpha_comparison].label,
                                         
                                        "CELL_E_311") 
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
               ENDIF
            ENDIF

            comparisonIndex ++

        ENDWHILE

        


        ContactListSlot[slotIndex] = top_alpha_comparison
        contact_name_in_alpha_order[top_alpha_comparison] = TRUE
        
                                                                     





        IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))
        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED
        //AND NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))

            IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.


           
                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER


                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)


                            //Fourth parameter set to 1 draws icon beside contact name.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 1,
                                 INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_CharacterSheetNonSaved[top_alpha_comparison].phonebookNumberLabel, "CELL_195", "CELL_300") 

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0, 
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_CharacterSheetNonSaved[top_alpha_comparison].phonebookNumberLabel, "CELL_195", "CELL_300") 
                        
                        ENDIF
                            
                    ELSE

                        
                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)

                            //Fourth parameter set to 1 draws icon beside contact name.
                            
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 1,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_sCharacterSheetAll[top_alpha_comparison].label, 
                                
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                g_sCharacterSheetAll[top_alpha_comparison].label,
                                g_sCharacterSheetAll[top_alpha_comparison].picture)

                    
                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_sCharacterSheetAll[top_alpha_comparison].label, 
                                
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                g_sCharacterSheetAll[top_alpha_comparison].label,
                                g_sCharacterSheetAll[top_alpha_comparison].picture) 
                    
                   
                        ENDIF
                   
                    ENDIF
                            
                        

                ENDIF
           ENDIF
        ENDIF

        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, slotIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED       
        
            IF (INT_TO_ENUM (enumCharacterList, slotIndex )) <> g_Cellphone.PhoneOwner   //don't let the phonebook holder include himself in the total number of contacts.

            
                i_Number_Of_Contacts_In_List ++ 
            
            ENDIF   
            
            
            PRINTSTRING("NumContacts-")
            PRINTINT(i_Number_Of_Contacts_In_List)
            PRINTNL()

        ENDIF

        slotIndex ++
    ENDWHILE


    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))
        IF NETWORK_IS_SIGNED_ONLINE()
        
            IF g_Use_Prologue_Cellphone = FALSE //Don't allow human contacts in prologue phone.
                Place_Friends_in_ContactSlots() //Conor, this is your dummy proc for placing the friends in!  Reinstate this please.
            ENDIF

        ENDIF
    ENDIF

    Post_Placement_Setup_and_Checks()
ENDPROC

PROC Place_MP_Names_in_ContactSlots()


	#if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppContacts -  02 Sep 2016 - 18th May drastic action for bug 2828375 - Nuking any save game alphabetical ordering for male and female assistant contacts for Executive pack via Place_MP_Names")
        cdPrintnl()   
     #endif
	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_BIKER_CH1, 1320)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSISTF, 180)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSIST, 190)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 2)

    WAIT (0)



    //Display "no contacts" message in slot 1 in lieu of any contacts...
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(0)), 0,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_232", "CELL_233", "CELL_393") 



    i_Number_Of_Contacts_In_List = 0 //reset number of contacts before filling list. Critical this is done before filling the friends slots...


    
    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))
        IF NETWORK_IS_SIGNED_ONLINE()
        
            IF g_Use_Prologue_Cellphone = FALSE //Don't allow human contacts in prologue phone.
			#IF FEATURE_DLC_2_2022
			AND IS_CONTACT_IN_FAVOURITES_LIST_BY_INDEX(ciPHONE_CONTACT_FAVOURITE_PLAYERS)
			#ENDIF
                Place_MP_Friends_in_ContactSlots() //Conor, this is your dummy proc for placing the friends in!  Reinstate this please.
            ENDIF

        ENDIF
    ENDIF


    NPCSlot_StartingIndex = i_Number_Of_Contacts_In_List //Critical!

    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("AppContacts - Critical! NPCSlot_StartingIndex is ")
        cdPrintint(NPCSlot_StartingIndex)
        cdPrintnl()

    #endif
    


    b_DoFinaleConfirmation = FALSE //make sure this is false each time we run this proc.

    b_DoHumanFriend_Routine = FALSE


    

    BOOL contact_name_in_alpha_order[MAX_CHARACTERS_PLUS_DUMMY]

    INT slotIndex = i_Number_Of_Contacts_In_List //Critical!

    WHILE slotIndex < (ENUM_TO_INT(MAX_CHARACTERS_MP))  

        
        //When testing ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKS() comment in this WAIT (0)
        //WAIT(0) // for testing all contacts.


        #IF IS_DEBUG_BUILD

            PRINTSTRING("Populating MP sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
    
        #endif         


        INT comparisonIndex = 0

        INT top_alpha_comparison = (ENUM_TO_INT(MAX_CHARACTERS_MP))

		GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), 5000)

        /*
        PRINTSTRING("top_alpha_comparison =")
        PRINTINT(top_alpha_comparison)
        PRINTNL()
        */
   

     

        //Virtual Character handled by this interndal function now.
        Get_Cellphone_Owner()



        

        WHILE comparisonIndex < (ENUM_TO_INT(MAX_CHARACTERS_MP))

            /*
            PRINTSTRING("comparison index =")
            PRINTINT(comparisonIndex)
            PRINTNL()
            */

            
            
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                IF comparisonIndex = ENUM_TO_INT (CHAR_TREVOR)
                OR comparisonIndex = ENUM_TO_INT (CHAR_MICHAEL)
                OR comparisonIndex = ENUM_TO_INT (CHAR_LESTER)


              
                    //Critical! This alters the alphabetical order by manipulating the alphaints. Holders obtained the correct value on startup to re-establish old values at end of proc and
                    //before termination of this script.
                                       
                    //Ben, this stipulates the order in which the characters appear on the finale contact list.
					GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_LESTER, 3)
                    GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_TREVOR, 1)
					GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MICHAEL, 2)                

                    b_TryToPlaceContact = TRUE

                ELSE

                    b_TryToPlaceContact = FALSE

                ENDIF

            ELSE
                
                IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, comparisonIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED 

                    b_TryToPlaceContact = TRUE

                ELSE

                    b_TryToPlaceContact = FALSE

                ENDIF

            ENDIF



			//url:bugstar:7429893 - If the player is playing as Lamar in MP content, do not include himself in the contacts list.
			IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
				IF g_bUseLamarThemeInMP = TRUE AND (INT_TO_ENUM (enumCharacterList, comparisonIndex )) = CHAR_LAMAR
					
						#if IS_DEBUG_BUILD
        					cdPrintnl()
        					cdPrintstring("AppContacts - Nov, 2021 - Playing as Lamar, therefore not including Lamar in MP contacts list.")
        					cdPrintnl()   
     					#endif
						
                    	b_TryToPlaceContact = FALSE
					
				ENDIF
				
				
				IF g_bUseFranklinThemeInMP = TRUE AND (INT_TO_ENUM (enumCharacterList, comparisonIndex )) = CHAR_FIXER_FRANKLIN
					
						#if IS_DEBUG_BUILD
        					cdPrintnl()
        					cdPrintstring("AppContacts - Nov, 2021 - Playing as Franklin, therefore not including Franklin in MP contacts list.")
        					cdPrintnl()   
     					#endif
						
                    	b_TryToPlaceContact = FALSE
					
				ENDIF
			ENDIF
			
			#IF FEATURE_DLC_2_2022
			enumCharacterList eCharTemp = INT_TO_ENUM (enumCharacterList, comparisonIndex )
			
            IF IS_CONTACT_VALID_FOR_FAVOURITES_LIST(eCharTemp)
			AND NOT IS_CONTACT_IN_FAVOURITES_LIST_BY_CHAR(eCharTemp)
			AND g_AutoScrollContact != eCharTemp
				
				PRINTLN("AppContacts - [CONTACT_FAVOURITES] - Character ", enumCharacterList_TO_STRING(eCharTemp)," is not favourited, therefore not including in MP contacts list.")
				
            	b_TryToPlaceContact = FALSE
				
			ENDIF
			#ENDIF
																							 
            IF b_TryToPlaceContact
  
                IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.
                    IF contact_name_in_alpha_order[comparisonIndex] = FALSE

                        //#if IS_DEBUG_BUILD
                            //cdPrintstring ("TAC ")
                            //cdPrintint(top_alpha_comparison)
                            //cdPrintnl()
                        //#endif
                       
                           
                        //If contact has missed call status, manipulate him to the top of the list by dividing his or her alphaint by 100.
                        //We can also manipulate the scheduled friend activity near the top also.

                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, comparisonIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, comparisonIndex)), g_Cellphone.PhoneOwner)

                            local_alphafloat = ((TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, comparisonIndex)))) / 100.0)

                        ELSE

                            local_alphafloat = TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, comparisonIndex)))

                        ENDIF


                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)

                            top_local_alphafloat = ((TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison)))) / 100.0)

                        ELSE

                            top_local_alphafloat = TO_FLOAT (GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(INT_TO_ENUM(enumCharacterList, top_alpha_comparison)))

                        ENDIF

                        IF local_alphafloat < top_local_alphafloat
                            top_alpha_comparison = comparisonIndex
                            
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


                                //Display Finale choice options. Kill Trevor, Kill Michael, Lester Deathwish.

                               
                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_TREVOR)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7001", 
												
                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage. 
                                        g_sCharacterSheetAll[top_alpha_comparison].label,
                                        g_sCharacterSheetAll[top_alpha_comparison].picture) 
                                
                                ENDIF

                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_MICHAEL)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,"CELL_7002",
                                        
                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                       	g_sCharacterSheetAll[top_alpha_comparison].label,
                                        g_sCharacterSheetAll[top_alpha_comparison].picture) 
                                

                                ENDIF


                                IF top_alpha_comparison = ENUM_TO_INT (CHAR_LESTER)
                                    //We manually replace the LESTER contact pic with the skull symbol for LESTER DEATHWISH. The call screen will still show LESTER by design.

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_7003",
                                        
                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                        g_sCharacterSheetAll[top_alpha_comparison].label,
                                         
                                        "CELL_E_311") 



                                ENDIF

                        
                            ENDIF

                            
   
                        ENDIF

                    ENDIF
               ENDIF

            ENDIF

            comparisonIndex ++

        ENDWHILE

        


        ContactListSlot[slotIndex] = top_alpha_comparison
        contact_name_in_alpha_order[top_alpha_comparison] = TRUE
        
                                                                     





        IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))
        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED 
        //AND NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS))

            IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.


           
                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 

                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)


                            //Fourth parameter set to 1 draws icon beside contact name.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 1,
                                 INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_CharacterSheetNonSaved[top_alpha_comparison].phonebookNumberLabel, "CELL_195", "CELL_300") 

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0, 
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_CharacterSheetNonSaved[top_alpha_comparison].phonebookNumberLabel, "CELL_195", "CELL_300") 
                        
                        ENDIF
                            
                    ELSE

                        
                        IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, top_alpha_comparison), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                        OR HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY ((INT_TO_ENUM (enumCharacterList, top_alpha_comparison)), g_Cellphone.PhoneOwner)

                            //Fourth parameter set to 1 draws icon beside contact name.
                            
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 1,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_sCharacterSheetAll[top_alpha_comparison].label, 
                                
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                g_sCharacterSheetAll[top_alpha_comparison].label,
                                g_sCharacterSheetAll[top_alpha_comparison].picture) 
                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)), 0,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_sCharacterSheetAll[top_alpha_comparison].label, 
                                
                                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                g_sCharacterSheetAll[top_alpha_comparison].label,
                                g_sCharacterSheetAll[top_alpha_comparison].picture) 
                        ENDIF
                   
                    ENDIF
                            
                        

                ENDIF
           ENDIF
           ENDIF


        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, slotIndex), ENUM_TO_INT(g_Cellphone.PhoneOwner)) = LISTED       
        
            IF (INT_TO_ENUM (enumCharacterList, slotIndex )) <> g_Cellphone.PhoneOwner   //don't let the phonebook holder include himself in the total number of contacts.

            
                i_Number_Of_Contacts_In_List ++ 
            
            ENDIF   
            
            
            PRINTSTRING("MP_NumContacts-")
            PRINTINT(i_Number_Of_Contacts_In_List)
            PRINTNL()

        ENDIF


    
        slotIndex ++

  

    ENDWHILE

    

    Post_Placement_Setup_and_Checks()



     
ENDPROC










PROC Pass_Decision_Characters_Into_Algorithm (INT FirstInt, INT SecondInt)


    INT ResultInt


    ResultInt = FirstInt + SecondInt  //Minimum of 0 (0 + 0), maximum of 18 (9+9)
    ResultInt = 20 - Resultint  //Minium of two (20 - 18), maximum of 20 (20 - 0)
    Resultint = ResultInt + 5 //Minimum of 7 (2 + 5), maximum of 25 (20 + 5)


    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("Decision characters ")
        cdPrintint(FirstInt)
        cdPrintint(SecondInt)
        cdPrintstring(" gives algorithm result ")
        cdPrintint(Resultint)

    #endif



    SWITCH ResultInt


        CASE 7

            GameworldAssociatedRoot = "ANS_RAN1"
            GameworldAssociatedVoice = "ANS_M_USA1"

        BREAK


        CASE 8

            GameworldAssociatedRoot = "ANS_RAN2"
            GameworldAssociatedVoice = "ANS_F_USA1"

        BREAK
        

        CASE 9

            GameworldAssociatedRoot = "ANS_RAN3"
            GameworldAssociatedVoice = "ANS_M_ENG1"

        BREAK



        CASE 10

            GameworldAssociatedRoot = "ANS_RAN4"
            GameworldAssociatedVoice = "ANS_M_USA2"

        BREAK


        CASE 11

            GameworldAssociatedRoot = "ANS_RAN5"
            GameworldAssociatedVoice = "ANS_M_ASIAN"

        BREAK


        CASE 12

            GameworldAssociatedRoot = "ANS_RAN6"
            GameworldAssociatedVoice = "ANS_F_USA2"

        BREAK

        
        CASE 13

            GameworldAssociatedRoot = "ANS_RAN7"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK


        CASE 14

            GameworldAssociatedRoot = "ANS_RAN8"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK


        CASE 15

            GameworldAssociatedRoot = "ANS_RAN9"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK

        
        CASE 16

            GameworldAssociatedRoot = "ANS_RAN10"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK


        CASE 17

            GameworldAssociatedRoot = "ANS_RAN1"
            GameworldAssociatedVoice = "ANS_M_USA1"

        BREAK


        CASE 18

            GameworldAssociatedRoot = "ANS_RAN2"
            GameworldAssociatedVoice = "ANS_F_USA1"

        BREAK
        

        CASE 19

            GameworldAssociatedRoot = "ANS_RAN3"
            GameworldAssociatedVoice = "ANS_M_ENG1"

        BREAK



        CASE 20

            GameworldAssociatedRoot = "ANS_RAN4"
            GameworldAssociatedVoice = "ANS_M_USA2"

        BREAK


        CASE 21

            GameworldAssociatedRoot = "ANS_RAN5"
            GameworldAssociatedVoice = "ANS_M_ASIAN"

        BREAK


        CASE 22

            GameworldAssociatedRoot = "ANS_RAN6"
            GameworldAssociatedVoice = "ANS_F_USA2"

        BREAK

        
        CASE 23

            GameworldAssociatedRoot = "ANS_RAN7"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK


        CASE 24

            GameworldAssociatedRoot = "ANS_RAN8"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK


        CASE 25

            GameworldAssociatedRoot = "ANS_RAN9"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK

        


        DEFAULT

            GameworldAssociatedRoot = "ANS_RAN8"
            GameworldAssociatedVoice = "ANSWER_FX"

        BREAK



    ENDSWITCH


ENDPROC















PROC Display_Contacts_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


            
    ContactNamesToDraw = (i_Number_Of_Contacts_In_List) 

    DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    WHILE drawIndex < ContactNamesToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF

		
      
        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(ContactListSlot[drawIndex], g_Cellphone.PhoneOwner) <> NOT_LISTED // would need to check individual owner phonebooks if this is required.
            
            DISPLAY_TEXT (drawItemX, drawItemY, GLOBAL_CHARACTER_SHEET_GET_LABEL(ContactListSlot[drawIndex])) //draw the name of the contact by referring to the character's text label. 
      
            drawItemY = drawItemY + 0.03 //Draw the next name slightly down the y-axis
        
        ENDIF

   
        drawIndex ++
       



    ENDWHILE


    


ENDPROC




PROC Combi_PageUp()
               
    IF ListCursorIndex > 0
        
        ListCursorIndex --

    ELSE

        ListCursorIndex = (i_Number_Of_Contacts_In_List - 1)

    ENDIF   

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(1)))


ENDPROC




PROC Combi_PageDown()
       
    ListCursorIndex ++
           
    IF ListCursorIndex = i_Number_Of_Contacts_In_List 

        ListCursorIndex = 0

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_INPUT_EVENT", (TO_FLOAT(3)))


ENDPROC






//Temp - would be done in flash.
PROC Check_for_List_Navigation()

    /*                        
    DISPLAY_TEXT_WITH_NUMBER (0.20, 0.45, "CELL_500", ListCursorIndex)
    DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.5, "STRING", "LCI")



    DISPLAY_TEXT_WITH_NUMBER (0.40, 0.45, "CELL_500", i_Number_Of_Contacts_In_List)
    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.5, "STRING", "NumCon")
    */


    IF dpad_scroll_pause_cued
        IF TIMERA() > 150

            dpad_scroll_pause_cued = FALSE

        ENDIF
    ENDIF
    
    // PC Scrollwheel support
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
        
            IF ListCursorIndex > 0
                
                ListCursorIndex --

            ELSE

                ListCursorIndex = (i_Number_Of_Contacts_In_List - 1)

            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)
        
        ENDIF
        
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
        
            ListCursorIndex ++

                 
            IF ListCursorIndex = i_Number_Of_Contacts_In_List 

                ListCursorIndex = 0

            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

        ENDIF
    
    ENDIF


    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                
                ListCursorIndex --

            ELSE

                ListCursorIndex = (i_Number_Of_Contacts_In_List - 1)

            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++
                   
                 
            IF ListCursorIndex = i_Number_Of_Contacts_In_List 

                ListCursorIndex = 0

            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)
   
        ENDIF



        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            Combi_PageDown()
            Combi_PageDown()
            Combi_PageDown()
            Combi_PageDown()            
            Combi_PageDown()
            
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)
            
        ENDIF




        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))

            Combi_PageUp()
            Combi_PageUp()
            Combi_PageUp()
            Combi_PageUp()            
            Combi_PageUp()
            
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)
            
        ENDIF


    ENDIF

            
ENDPROC





PROC Auto_Scroll_to_Set_Contact()


    ListCursorIndex ++

                 
    IF ListCursorIndex = i_Number_Of_Contacts_In_List 

        ListCursorIndex = 0

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)


        #if IS_DEBUG_BUILD 
            PRINTNL()
            PRINTSTRING ("Auto scroll for contact was set but did not find contact in list. Clearing bit.")
            PRINTNL()
        #endif

    ENDIF

            
    Call_Scaleform_Input_Keypress_Down()



    //SF_SelectedContactInt = LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
    
    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


    WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
    
        WAIT (0)

        #if IS_DEBUG_BUILD
            cdPrintstring("appContacts - Waiting on scaleform return during autoscroll routine.")
            cdPrintnl()
        #endif

    ENDWHILE
    

    SF_SelectedContactInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)





    IF SF_SelectedContactInt < 0 //Scaleform can return -1 is the contact list hasn't been filled in time and the player really quickly presses X multiple times.

        SF_SelectedContactInt = 0   //Set to zero if this is the case to prevent array overrrun. See bug 65344 

    ENDIF

    If g_bInMultiplayer
	
		IF SF_SelectedContactInt > NPCSlot_StartingIndex //potential fix for 4209475. Disregard human players in multiplayer.
		OR SF_SelectedContactInt = NPCSlot_StartingIndex

		    IF g_AutoScrollContact = INT_TO_ENUM (enumCharacterList, (ContactListSlot[SF_SelectedContactInt]))
			                      
		        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

		        #if IS_DEBUG_BUILD 
		            PRINTNL()
					PRINTSTRING ("Auto scroll for contact multiplayer.  Match found at position:")
					PRINTINT(ContactListSlot[SF_SelectedContactInt])
		            PRINTSTRING ("Auto scroll for contact has found desired contact. Clearing bit.")
		            PRINTNL()
		        #endif

		    ENDIF
		
		ENDIF

	ELSE

		IF g_AutoScrollContact = INT_TO_ENUM (enumCharacterList, (ContactListSlot[SF_SelectedContactInt]))
		                      
	        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

	        #if IS_DEBUG_BUILD 
	            PRINTNL()
				PRINTSTRING ("Auto scroll for contact singleplayer. Match found at position:")
				PRINTINT(ContactListSlot[SF_SelectedContactInt])
	            PRINTSTRING ("Auto scroll for contact has found desired contact. Clearing bit.")
	            PRINTNL()
	        #endif

	    ENDIF

	ENDIF

ENDPROC









PROC Do_Call()

        //Set Phone Display State to "attempting to call contact" and update the view state enum with the required data.

        //See bug 316474
        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

            g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT

            LaunchTransitionMoveUpIfRequired()
         
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 15. appContacts assigns PDS_ATTEMPTING_TO_CALL_CONTACT and LaunchTransitionMoveUpIfRequired()")
                cdPrintnl()   
            #endif

            IF IS_PLAYER_PLAYING (PLAYER_ID())
                IF NOT IS_ENTITY_IN_WATER (PLAYER_PED_ID()) //Make sure player doesn't try to put the phone to his ear in SP, as the phone call gets aborted immediately in water. See bug 907295.
                    
                    IF IS_PED_IN_COVER(PLAYER_PED_ID())

                        IF NOT IsCurrentCamFirstPerson() 

                            Put_Cellphone_To_Ear()  //Having the cellphone beside the ear in cover in first person can cause clipping problems.

                        ENDIF

                    ELSE

                        IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE  //Added for 2038700, this delays first person mode from putting the phone to ear immediately.
                                                                                //It will go to ear on foot when the cellphone conversation starts from PlayCellphoneConversation()
                            Put_Cellphone_To_Ear()

                        ENDIF

                    ENDIF

                ENDIF
            ENDIF

            //If the player is in sleep mode, return to normal. See TODO 338963
            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.

                SET_CELLPHONE_PROFILE_TO_NORMAL()

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 0)

            ENDIF

         ENDIF


        
    
        IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                "CELL_300",
                "CELL_211", 
                "CELL_195")   

        ELSE


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
               g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                "CELL_211",

                //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)

        
        ENDIF
                       
        
        Update_Scaleform_VSE ()



        #if IS_DEBUG_BUILD 
    
            cdPrintnl()
            cdPrintstring ("appCONTACTS_1 - Attempting to call contact with label: ")
            cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
            cdPrintstring (" which is charsheet entry ")
            cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
            cdPrintnl()

        #endif

        IF g_bInMultiplayer

            INCREMENT_BY_MP_INT_CHARACTER_STAT (MP_STAT_CALLS_MADE_TO_NPC, 1) //1782892
        
            #if IS_DEBUG_BUILD 
                cdPrintstring("appContacts - Incrementing MP_STAT_CALLS_MADE_TO_NPC on dialling NPC")
                cdPrintnl()
            #endif
        
        ENDIF


ENDPROC










PROC Do_FinaleConfirmationProceed()



      
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.


                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


                b_DoFinaleConfirmation = FALSE

                Play_Select_Beep()


                g_InputButtonJustPressed = TRUE


                //Set bools to specify this is a story call here!
                g_b_Is_This_An_Activity_Call = FALSE
                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                g_b_Is_This_A_Secondary_Function_Call = FALSE


                Do_Call()


        ENDIF


ENDPROC



PROC Do_FinaleConfirmationGoBack()


        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full contact list display.
                    

            b_DoFinaleConfirmation = FALSE



            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 7) //Quick change of view to make sure data is flushed from view state 2.



            Play_Back_Beep()

            g_InputButtonJustPressed = TRUE

          

            IF g_bInMultiplayer

                Place_MP_Names_in_ContactSlots()

            ELSE

                Place_Names_in_ContactSlots()

            ENDIF

            WAIT(0) //make sure this JUST_PRESSED doesn't carry over by stopping for a game cycle.
            

            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 9672. appContacts assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            ENDIF

            NumpadEntryString ="" //Reset entry string.

        ENDIF


ENDPROC











PROC Set_Up_Cancellation_Confirmation_Menu()



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)

         //Badger - Select only.
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        ELSE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


   
        
        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_270") //Yes

               
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_271") //No
                                                                                                        

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18)


        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_269")

            

ENDPROC














 PROC Check_For_Friend_Secondary_Call_Decision()

    //Macros
    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.


        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

        b_DoCancelConfirmation = FALSE


        Play_Select_Beep()


        g_InputButtonJustPressed = TRUE


        
   
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

            Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
            
                WAIT (0)

                #if IS_DEBUG_BUILD
                    cdPrintstring("appContacts - Waiting on scaleform return during Check_For_Friend_Secondary_Call_Decision")
                    cdPrintnl()
                #endif

            ENDWHILE


            SF_SelectedChoiceInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)

            //Old display order for CALL_HANGOUT/CANCEL_SECONDARY. 1142222 requested change to SECONDARY_CALL_HANGOUT...
            /*

            IF SF_SelectedChoiceInt = 0    //Was 0 when order was CALL_HANGOUT_SECONDARY. 1142222 requested change to SECONDARY_CALL_HANGOUT...

                 //Set bools to specify this is a story call here!
                g_b_Is_This_An_Activity_Call = FALSE
                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                g_b_Is_This_A_Secondary_Function_Call = FALSE

                Do_Call()

            ENDIF
                

                #if IS_DEBUG_BUILD
                    cdPrintstring("AppCONTACTS - Checking second slot choice...")
                    cdPrintnl()
                #endif


                IF SF_SelectedChoiceInt = 1 //Was 1 when order was CALL_HANGOUT_SECONDARY. 1142222 requested change to SECONDARY_CALL_HANGOUT...


                
                    IF b_Friend_Option_Available //Friend option should always occupy slot 1 for just now...

                        //Changed at Sam's request (827347) - Section 5. If the two chars can cancel a friend activity then it's implicit
                        //that the second option must have been "Cancel activity" so we go to the cancel confirmation screen here.
                        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedInCall, g_Cellphone.PhoneOwner)


                            //Set up CANCEL yes or no menu here.
                            Set_Up_Cancellation_Confirmation_Menu()


                            b_DoCancelConfirmation = TRUE


                        ELSE //Otherwise, it must have been the "Hang out" option that was displayed.
                            

                            //Set bools for friend activity hang out call...
                            g_b_Is_This_An_Activity_Call = TRUE
                            g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                            g_b_Is_This_A_Secondary_Function_Call = FALSE

                            Do_Call()


                        ENDIF

                    ELSE   //If the friend option is unavailable, slot 1 must be filled by a secondary option.
                
                        //Set bools for secondary function call...
                        g_b_Is_This_An_Activity_Call = FALSE
                        g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                        g_b_Is_This_A_Secondary_Function_Call = TRUE

                        Do_Call()

                    ENDIF   


                ENDIF



                IF SF_SelectedChoiceInt = 2     //Was 2 when order was CALL_HANGOUT_SECONDARY. 1142222 requested change to SECONDARY_CALL_HANGOUT...

                //This should always hold the secondary function for just now...

                    //Set bools for secondary function call...
                    g_b_Is_This_An_Activity_Call = FALSE
                    g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                    g_b_Is_This_A_Secondary_Function_Call = TRUE

                    Do_Call()

                ENDIF
                */
                //End of old order.






                //Need to hook up #1033532  IS_CHAT_CALL_AVAILABLE_FOR_CHARACTER(enumCharacterList paramChar) branch here = DONE.

                IF IS_CHAT_CALL_AVAILABLE_FOR_CHARACTER(g_TheContactInvolvedinCall)
                //IF g_AllowUnderwaterDialogue// - swap in this for the above for easy testing.



                        IF SF_SelectedChoiceInt = 0 


                            IF b_Secondary_Option_Available

                                //Set bools for secondary function call...
                                g_b_Is_This_An_Activity_Call = FALSE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = TRUE

                                Do_Call()

                            ELSE


                                //Set bools to specify this is a story call here!
                                g_b_Is_This_An_Activity_Call = FALSE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = FALSE

                                Do_Call()
                        
                            ENDIF


                        ENDIF



                        IF SF_SelectedChoiceInt = 1


                            IF b_Secondary_Option_Available

                                //Set bools to specify this is a story call here!   If the secondary option was available in slot zero, then slot 1 will hold the story call.
                                g_b_Is_This_An_Activity_Call = FALSE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = FALSE

                                Do_Call()



                            ELSE  // No secondary option available mean slot zero will hold a story call, so friends actions will be in slot 1.


                                IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedInCall, g_Cellphone.PhoneOwner)


                                    //Set up CANCEL yes or no menu here.
                                    Set_Up_Cancellation_Confirmation_Menu()


                                    b_DoCancelConfirmation = TRUE


                                ELSE //Otherwise, it must have been the "Hang out" option that was displayed.
                                    

                                    //Set bools for friend activity hang out call...
                                    g_b_Is_This_An_Activity_Call = TRUE
                                    g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                    g_b_Is_This_A_Secondary_Function_Call = FALSE

                                    Do_Call()


                                ENDIF


                             ENDIF

                        ENDIF



                        IF SF_SelectedChoiceInt = 2 //If 0 or 1 haven't returned true then it follows that something must be in slot 2 and that can only be friends actions under this ordering system.

                            IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedInCall, g_Cellphone.PhoneOwner)


                                //Set up CANCEL yes or no menu here.
                                Set_Up_Cancellation_Confirmation_Menu()


                                b_DoCancelConfirmation = TRUE


                            ELSE //Otherwise, it must have been the "Hang out" option that was displayed.
                                

                                //Set bools for friend activity hang out call...
                                g_b_Is_This_An_Activity_Call = TRUE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = FALSE

                                Do_Call()


                            ENDIF

                        ENDIF






                ELSE


             


                        IF SF_SelectedChoiceInt = 0


                            IF b_Secondary_Option_Available

                                 //Set bools for secondary function call...
                                g_b_Is_This_An_Activity_Call = FALSE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = TRUE

                                Do_Call()



                            ELSE  // No secondary option available mean slot zero will store friends actions as no story chat is available...


                                IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedInCall, g_Cellphone.PhoneOwner)


                                    //Set up CANCEL yes or no menu here.
                                    Set_Up_Cancellation_Confirmation_Menu()


                                    b_DoCancelConfirmation = TRUE


                                ELSE //Otherwise, it must have been the "Hang out" option that was displayed.
                                    

                                    //Set bools for friend activity hang out call...
                                    g_b_Is_This_An_Activity_Call = TRUE
                                    g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                    g_b_Is_This_A_Secondary_Function_Call = FALSE

                                    Do_Call()


                                ENDIF


                             ENDIF

                        ENDIF



                        IF SF_SelectedChoiceInt = 1  //Must be a friend action if no story chat was present.

                            IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedInCall, g_Cellphone.PhoneOwner)


                                //Set up CANCEL yes or no menu here.
                                Set_Up_Cancellation_Confirmation_Menu()


                                b_DoCancelConfirmation = TRUE


                            ELSE //Otherwise, it must have been the "Hang out" option that was displayed.
                                

                                //Set bools for friend activity hang out call...
                                g_b_Is_This_An_Activity_Call = TRUE
                                g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                g_b_Is_This_A_Secondary_Function_Call = FALSE

                                Do_Call()


                            ENDIF

                        ENDIF

                




                ENDIF





    ENDIF
    ENDIF


ENDPROC



PROC Set_Up_Human_Friend_Menu()  //Conor, stick any other options in here.



        #if IS_DEBUG_BUILD
            
            cdPrintnl()
            cdPrintstring("AppCONTACTS - Set_Up_Human_Friend_Menu has been called. #2105815")
            cdPrintnl()

        #endif


     

        //See bug 832525
        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

            g_Cellphone.PhoneDS = PDS_COMPLEXAPP

            /*
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 171237. appContacts assigns PDS_COMPLEXAPP. This will get reassigned when dynamically updating.")
                cdPrintnl()   
            #endif
            */
        ENDIF


        //Badger - Select Back combo
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        ELSE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)




        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")  //Call...





        Fill_PlayerInteraction_Screen_Slots(m_sPlayers)


        IF g_Cellphone.PhoneDS > PDS_AWAY   //If the current selection returns invalid for bug #1608514, HANG_UP_AND_PUT_AWAY_PHONE() will be called as a precaution
                                            //from Fill_PlayerInteraction_Screen_Slots above in appMPPlayerListFunctions.sch


            //If phone was not signalled to be hung up from Fill_PlayerInteraction_Screen_Slots, go ahead with header and display view assignment.

            PRINTLN("Set_Up_Human_Friend_Menu: list cursor index = ",ListCursorIndex)
            PRINTLN("Set_Up_Human_Friend_Menu: i_Selected_Slot_to_Restore = ",i_Selected_Slot_to_Restore)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18,TO_FLOAT(i_Selected_Slot_to_Restore))
            i_Selected_Slot_to_Restore = 0
            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18)
           
            // Display the Header (using the player name string)
            STRING thePlayerName
            IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
                thePlayerName = GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            ELSE
                thePlayerName = MP_PL_TEXT_LABEL_TO_STRING(m_sPlayers.plFriends[GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)].Name)
            ENDIF


      

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_HEADER")
                //BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFNH") //<c>~a~</c>
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(thePlayerName)
                END_TEXT_COMMAND_SCALEFORM_STRING() 
            END_SCALEFORM_MOVIE_METHOD()


            //Trial - Steve T. Reset this to prevent call quit spamming.  1602347
            m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT //Reset to prevent Session - 8 spamming.


            b_DoHumanFriend_Routine = TRUE
    
        ELSE

            #if IS_DEBUG_BUILD
            
                cdPrintnl()
                cdPrintstring("AppCONTACTS - chosen contact has left the session whilst phone was up - aborting for safety.")
                cdPrintnl()

            #endif

        ENDIF


        
ENDPROC







PROC MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()

    IF g_bInMultiplayer

        Place_MP_Names_in_ContactSlots()
    
    ELSE

        Place_Names_in_ContactSlots()

    ENDIF

    m_sPlayers.bButtonReleased = FALSE
    m_sPlayers.bLocalNotOnline = FALSE
    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_Cellphone.PhoneDS = PDS_RUNNINGAPP
        
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 17123789. appContacts assigns PDS_RUNNINGAPP. This will get reassigned when dynamically updating.")
            cdPrintnl()   
        #endif



        //Added in for #1580608
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2) //, TO_FLOAT(SF_SelectedContactInt)) //Set enum state and header to CONTACTS



        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_7000")
        
        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_0")

        ENDIF   
        
       
        STOP_SOUND (Phone_Dialling_SoundID)
        cc_Progress = CC_LAUNCH_TIMER_AND_CHECK_MESSAGE



        
    ENDIF
ENDPROC


PROC Check_For_Human_Friend_Menu_Decision()  //Conor, this is where we check for the selection the user makes in the human friend menu.
    
    INT iScriptTransactionIndex
    INT iBankAmount, iWalletAmount
    INT iValidSelectionFailReason
    NETWORK_CLAN_DESC clanDesc
    FE_WARNING_FLAGS iFlags
    GAMER_HANDLE localHandle
    

                        //1526236 Dynamic handling of the "Highlight" option that can appear in the human interaction menu.
                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE)  //Set from appMPPlayerListFunctions
                        
                       
                            IF NOT (NETWORK_IS_GAMER_IN_MY_SESSION (m_sPlayers.selectedPlayerHandle)) //Player no longer in same session

                                CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE)

                            
                                #if IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("appContacts - Human Contact Highlight bit was set but selected player no longer in session, updating menu.")
                                    cdPrintnl()

                                #endif

                                //Can't really do this as the fellow "session player" becomes a friend and the original slot is invalid. We'd need to completely
                                //restore the list, then perhaps do an auto-selection by searching on a saved copy of the original PlayerHandle. A recipe for disaster.
                                
                                //b_DoHumanFriend_Routine = FALSE

                                //Set_Up_Human_Friend_Menu()  //This sets b_DoHumanFriend_Routine back to TRUE.

                                //EXIT

                                //If we really need to do this we could change  m_sPlayers.plPlayers[thisSlot[ to use handles which are persistent. Player IDs are not.

                                //Maybe put help here? "Player has left session!"
                                 

                                NumpadEntryString ="" //Reset entry string.

                                //For safety 20.09.13
                                #IF IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Stack size after NETWORK_IS_GAMER_IN_MY_SESSION prohibitive - closing phone.") NET_NL()
                                #ENDIF

                                g_Cellphone.PhoneDS = PDS_AWAY



                                /*
                                g_InputButtonJustPressed = TRUE

                                IF g_bInMultiplayer

                                    Place_MP_Names_in_ContactSlots()

                                ELSE

                                    Place_Names_in_ContactSlots()

                                ENDIF

                                WAIT(0) //make sure this JUST_PRESSED doesn't carry over by stopping for a game cycle.
                                

                                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("STATE ASSIGNMENT 8871. appContacts assigns PDS_RUNNINGAPP as player in session has left.")
                                        cdPrintnl()   
                                    #endif

                                ENDIF
                                */
                            

                            ENDIF


                        ENDIF



                        #if IS_DEBUG_BUILD
                        
                            IF g_Cellphone_Onscreen_State_Debug
                                //IF IS_NET_PLAYER_OK(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot], TRUE, FALSE)  //Check the status of the last selected player.
                                IF NETWORK_IS_GAMER_IN_MY_SESSION (m_sPlayers.selectedPlayerHandle)
                                  
                                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.50, "STRING", "OK - selected player in session")

                                ELSE
                    
                                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.50, "STRING", "Selected player NOT in session.")
                        
                                ENDIF 
                                
                            ENDIF

                        #endif


                      
                        //Was getting this using IS_NET_PLAYER_OK
                        //Error: [script_network] Verifyf(playerIndex>=0&&playerIndex<MAX_NUM_PHYSICAL_PLAYERS) FAILED: 
                        //SCRIPT: Script Name = appcontacts : Program Counter = 33962: NETWORK_IS_PLAYER_ACTIVE - Invalid player index (-1) (from Updating script appcontacts)


                        //NETWORK_IS_GAMER_IN_MY_SESSION(GAMER_HANDLE& gamerHandle)




    SWITCH m_sPlayers.iPlayerInteractionStage
        CASE PLAYER_INTERACTION_STAGE_SELECT
            IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
                IF NOT g_sShopSettings.bProcessStoreAlert
                    CLEAR_BIT(iBoolsBitSet, iBS_LAUNCH_STORE)
                    CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
                    //NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
                    PRINTLN("  --> APP CONTACTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
                ENDIF
                EXIT
            ENDIF
            IF m_sPlayers.bConfirmedInteraction
                IF m_sPlayers.bLocalNotOnline
                    iFlags = FE_WARNING_OK
                    SET_WARNING_MESSAGE_WITH_HEADER("HUD_CONNPROB","HUD_DISCON",iFlags)
                    IF m_sPlayers.bButtonReleased
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.
                        OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 1")
                                cdPrintnl()   
                            #endif

                            //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()

                            //For safety 20.09.13
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Stack size after m_sPlayers.bLocalNotOnline prohibitive - closing phone.") NET_NL()
                            #ENDIF

                            g_Cellphone.PhoneDS = PDS_AWAY



                        ENDIF
                    ELSE
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.
                        OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
                        ELSE
                            m_sPlayers.bButtonReleased = TRUE
                        ENDIF
                    ENDIF
                ELSE
                    //handle whatever interaction the player has just confirmed
                    i_Selected_Slot_to_Restore = m_sPlayers.m_selectedOptionSlot  //make a copy so this can be input into DISPLAY_VIEW to reselect last highlighted slot.
                    SWITCH m_sPlayers.optionList[m_sPlayers.m_selectedOptionSlot] 
                        CASE OPTION_CALL
                            //IF NOT NETWORK_IS_GAME_IN_PROGRESS() //Removed for #1633276
                            //OR GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) >= g_sMPTunables.iPhoneFriendCharge...Should include this!
                                                    
                                IF NOT voiceSession.bInVoiceSession
                                    //put back in when 
                                    IF NOT NETWORK_SESSION_IS_VOICE_SESSION_BUSY()
                                    AND NOT NETWORK_SESSION_IS_VOICE_SESSION_ACTIVE()
//                                        IF NOT IS_XBOX_PLATFORM()
//                                        OR (IS_XBOX_PLATFORM() AND GET_PLAYER_DISPLAY_NAME_FOR_PHONECALLS(m_sPlayers.selectedPlayerHandle))
                                            m_sPlayers.bConfirmedInteraction = FALSE
                                            
                                            SCRIPT_START_A_VOICE_SESSION()
                                            //MPGlobalsAmbience.playerBeingCalled = m_sPlayers.selectedPlayerHandle
                                            
                                            m_sPlayers.bCallRequestSent = FALSE
                                            STRING thePlayerName
                                            IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
                                                thePlayerName = GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                                            ELSE
                                                thePlayerName = MP_PL_TEXT_LABEL_TO_STRING(m_sPlayers.plFriends[GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)].Name)
                                            ENDIF
                                            //IF IS_XBOX_PLATFORM()
                                             //   LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL_FROM_APPCONTACTS(voiceSession.displayName[0]) 
                                           // ELSE
                                                LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL_FROM_APPCONTACTS(thePlayerName) 
                                            //ENDIF
                                            m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_IN_CALL
                                            
                                            RESET_NET_TIMER(m_sPlayers.voiceSessionTimer) //1602347
                                            SET_OUTGOING_CHAT_CALL_AS_ENGAGED (FALSE) //1602347

                                            
                                            #if IS_DEBUG_BUILD
                                            
                                                PRINTLN("...CDM MP: Player placing a call to : ",thePlayerName)
                                                PRINTLN("Subtracting call charge...")

                                            #endif


                                            IF g_bInMultiplayer //#1626685
												INT iDifference
												INT iCost
												iCost = g_sMPTunables.iPhoneFriendCharge
												//If the player does not have the tunable amount just subtract his remaining balance... #1633276
												//The fix for this ^ was just commented out... fixed *now* anyway.
												IF NOT NETWORK_CAN_SPEND_MONEY2(iCost, FALSE, TRUE, FALSE, iDifference)
													iCost -= iDifference
												ENDIF
                                                localHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
												
												IF iCost > 0
	                                                IF USE_SERVER_TRANSACTIONS()
	                                                    TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CALL_PLAYER, iCost, iScriptTransactionIndex, FALSE, TRUE)
	                                                    g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = localHandle
	                                                ELSE
	                                                    NETWORK_SPENT_CALL_PLAYER(iCost, localHandle, FALSE, TRUE)
	                                                ENDIF                           
												ENDIF
                                                INCREMENT_BY_MP_INT_CHARACTER_STAT (MP_STAT_CALLS_MADE_TO_PLAYERS, 1) //1782892
            
                                                #if IS_DEBUG_BUILD 
                                                    cdPrintstring("appContacts - Incrementing MP_STAT_CALLS_MADE_TO_PLAYERS on dialling human player.")
                                                    cdPrintnl()
                                                #endif

                                            ENDIF
//                                        #if IS_DEBUG_BUILD
//                                        ELSE
//                                            PRINTLN("...CDM MP:  waiting for GET_PLAYER_DISPLAY_NAME_FOR_PHONECALLS placing call")
//                                        #ENDIF
//                                        ENDIF

                                    ELSE    
                                        IF NETWORK_SESSION_IS_VOICE_SESSION_BUSY()
                                            PRINTLN("...CDM MP: waiting for NOT NETWORK_SESSION_IS_VOICE_SESSION_BUSY")
                                        ELIF NETWORK_SESSION_IS_VOICE_SESSION_ACTIVE()
                                            PRINTLN("...CDM MP: waiting for NOT NETWORK_SESSION_IS_VOICE_SESSION_ACTIVE")
                                            IF NOT voiceSession.bExitOnNewCall
                                                voiceSession.bRequestedVoiceSession = TRUE
                                                voiceSession.bClearVoiceSession = TRUE
                                                voiceSession.bExitOnNewCall = TRUE
                                                 PRINTLN("...CDM MP: killing active call")
                                            ENDIF
                                        ENDIF
                                    ENDIF
                                ELSE
                                    IF voiceSession.bAwaitingEndEvent
                                        PRINTLN("player ending a call ingoring input")
                                    ELSE
                                    #IF IS_DEBUG_BUILD
                                        NET_PRINT("...CDM MP: Player ended call- returning to player interaction screen") NET_NL()
                                    #ENDIF
                                    SCRIPT_END_A_VOICE_SESSION()
                                    PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 2")
                                        cdPrintnl()   
                                    #endif

                                    //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()


                                    IF voiceSession.bInVoiceSession  //Potential fix for #1623996
                
                                        CLEAR_VOICE_SESSION_STRUCT()
                
                                        #if IS_DEBUG_BUILD

                                            PRINTNL()
                                            PRINTSTRING("TRIAL AppContacts - performing Emergency CLEAR_VOICE_SESSION_STRUCT() as player should no longer be in voice session.")
                                            PRINTNL()
                            
                                        #endif

                                    ENDIF           
                                    
                                    
                                    //For safety 20.09.13
                                    #IF IS_DEBUG_BUILD
                                        NET_PRINT("...CDM MP: Stack size after selecting End Call prohibitive - closing phone.") NET_NL()
                                    #ENDIF

                                    g_Cellphone.PhoneDS = PDS_AWAY
                     
                                                             
                                    ENDIF
                                ENDIF

//                            ELSE   //Removed for #1633276
//                                IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
//                                    LAUNCH_STORE_CASH_ALERT(DEFAULT, DEFAULT, SPL_PHONE)
//                                    SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
//                                    NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
//                                    PRINTLN("  -->  APP CONTACTS - LAUNCH_STORE_CASH_ALERT - CALLED")
//                                ENDIF
//                                IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CALL_PLAY_MON")
//                                    PRINT_HELP("CALL_PLAY_MON")
//                                    PRINTLN("AppContacts: Player can't make a call to another player they don't have enough money in account.")
//                                ENDIF
//                            ENDIF

                            EXIT
                        BREAK
                        
                        CASE OPTION_SEND_MESSAGE
                            IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])
                                IF NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])], SEND_MESSAGE_COOLDOWN)
                                    IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)
                                        PRINT_HELP_WITH_PLAYER_NAME("CELL_MP_1009H", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]), GET_PLAYER_HUD_COLOUR(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) //There is a short wait before you can send another message to ~a~
                                    ENDIF
                                    m_sPlayers.bConfirmedInteraction = FALSE
                                    PRINTLN("     ---------->     APP - OPTION_SEND_MESSAGE - IN COOLDOWN - SEND TO ", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                                    EXIT
                                ENDIF
                            ENDIF
							
							GAMER_HANDLE gamerHandle
							gamerHandle = GET_GAMER_HANDLE_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            
                            //Double Check Player is still Valid
                            IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
                            OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
							OR NOT  NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle)
//                          OR NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            //OR NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            //OR NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                         	OR NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])



                                #if IS_DEBUG_BUILD //extra debug added by Steve T. to help diagnose bug # 2333703


                                    IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
                                        cdPrintstring("AppContacts - Double Check reports that NULL passed account over 17 for chat has returned false. Preventing text message reply.")
                                        cdPrintnl()
                                    ENDIF
                                

                                    IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                                        cdPrintstring("AppContacts - Double Check reports that target contact account is not over 17 for chat. Preventing text message reply.")
                                        cdPrintnl()
                                    ENDIF

				                    IF NOT NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle)
				                        cdPrintstring("AppContacts - Double Check reports that target contact as NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle) = FALSE")
				                        cdPrintnl()
				                    ENDIF

//                                    IF NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                                        cdPrintstring("AppContacts - Double Check reports that target contact is muted by the calling player. Preventing text message reply.")
//                                        cdPrintnl()
//                                    ENDIF


//                                    IF NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                                        cdPrintstring("AppContacts - Double Check reports that target contact has been blocked by the calling player. Preventing text message reply.")
//                                        cdPrintnl()
//                                    ENDIF


//                                    IF NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                                        cdPrintstring("Appcontacts - Double Check reports that target contact has muted the calling player at the target end. Preventing text message reply.")
//                                        cdPrintnl()
//                                    ENDIF

                                    
                                    IF NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                                        cdPrintstring("AppContacts - Double Check reports that target contact has blocked the calling player at the target end. Preventing text message reply.")
                                        cdPrintnl()
                                    ENDIF


                                #endif



                                IF IS_OK_TO_PRINT_FREEMODE_HELP()
                                    IF NOT ARE_STRINGS_EQUAL(GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]),"**Invalid**")
                                        PRINT_HELP_WITH_PLAYER_NAME("CELL_MP_1009M", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]), GET_PLAYER_HUD_COLOUR(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) //It is currently not possible to send a message to ~a~.
                                    ELSE
                                        PRINT_HELP("CELL_MP_1009N") //It is currently not possible to send a message.
                                    ENDIF
                                ENDIF
                                m_sPlayers.bConfirmedInteraction = FALSE
                                PRINTLN("     ---------->     APP - OPTION_SEND_MESSAGE - NO LONGER A VALID PLAYER (UNDERAGE, BLOCKED, OR MUTED) - SEND TO ", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                                EXIT
                            ENDIF
                            
                            m_sPlayers.bConfirmedInteraction = FALSE
                            m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SEND_MESSAGE
                            PRINTLN("     ---------->     APP - OPTION_SEND_MESSAGE - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SEND_MESSAGE - SEND TO ", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                            bCreatingSendMessage = TRUE
                            EXIT
                        BREAK
                        
    //                    CASE OPTION_PARTY
    //                        IF NOT HAS_NET_TIMER_STARTED(m_sPlayers.partyTimeOut)
    //                            IF NOT NETWORK_IS_PARTY_STATUS_PENDING()
    //                                IF NETWORK_IS_IN_PARTY()
    //                                    m_sPlayers.bHostingParty = TRUE
    //                                    PRINTLN("waiting for NETWORK_HOST_PARTY()")
    //                                ENDIF
    //                            ENDIF
    //                            START_NET_TIMER(m_sPlayers.partyTimeOut)
    //                        ELSE
    //                            IF HAS_NET_TIMER_EXPIRED(m_sPlayers.partyTimeOut,10000)
    //                                #IF IS_DEBUG_BUILD
    //                                SCRIPT_ASSERT("OPTION_PARTY FAILED. Notify CONOR M!")
    //                                #ENDIF
    //                                PRINTLN("OPTION_PARTY FAILED SEE CONOR!")
    //                                m_sPlayers.bConfirmedInteraction = FALSE
    //                                MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
    //                            ENDIF
    //                        ENDIF
    //                        //check player is not already in party with local player
    //                        IF NOT m_sPlayers.bHostingParty
    //                            IF NOT NETWORK_IS_IN_PARTY()
    //                                IF NETWORK_HOST_PARTY()
    //                                    m_sPlayers.bHostingParty = TRUE
    //                                ELSE
    //                                   PRINTLN("waiting for NETWORK_HOST_PARTY()")
    //                                ENDIF
    //                            ENDIF
    //                            EXIT
    //                        ELSE
    //                            IF NOT NETWORK_IS_PARTY_STATUS_PENDING()
    //                                IF NETWORK_IS_IN_PARTY()
    //                                    IF NOT NETWORK_IS_PARTY_MEMBER(m_sPlayers.selectedPlayerHandle)
    //                                        IF NETWORK_INVITE_GAMER_TO_PARTY(m_sPlayers.selectedPlayerHandle)
    //                                            m_sPlayers.bConfirmedInteraction = FALSE
    //                                            MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
    //                                            #IF IS_DEBUG_BUILD
    //                                                NET_PRINT("...CDM MP: player finished selected interaction moving back to interaction screen") NET_NL()
    //                                            #ENDIF
    //                                        ENDIF
    //                                        EXIT
    //                                    ENDIF
    //                                    m_sPlayers.bConfirmedInteraction = FALSE
    //                                ELSE
    //                                    PRINTLN("waiting for NETWORK_IS_IN_PARTY()")
    //                                ENDIF
    //                            ELSE
    //                                PRINTLN("waiting for NOT NETWORK_IS_PARTY_STATUS_PENDING()")
    //                            ENDIF
    //                        ENDIF
    //                    BREAK
                        CASE OPTION_FRIEND
                            IF NOT NETWORK_IS_ADDING_FRIEND()
                                IF IS_PS3_VERSION()
                                AND NOT NETWORK_IS_FRIEND(m_sPlayers.selectedPlayerHandle)
                                    NET_PRINT_TIME() NET_PRINT("   Process_Stage_Confirm_Interaction()   ---------->     FRIEND REQUEST - STARTED     <----------     ") NET_NL()
                                    IF NETWORK_ADD_FRIEND(m_sPlayers.selectedPlayerHandle, "")
                                        NET_PRINT_TIME() NET_PRINT("   Process_Stage_Confirm_Interaction()   ---------->     FRIEND REQUEST - DONE - A     <----------     ") NET_NL()
                                        m_sPlayers.bConfirmedInteraction = FALSE
                                        
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 3")
                                            cdPrintnl()   
                                        #endif

                                        //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
                                        //#IF IS_DEBUG_BUILD
                                            //NET_PRINT("...CDM MP: player finished selected interaction moving back to interaction screen") NET_NL()
                                        //#ENDIF 
                                        
                                        #IF IS_DEBUG_BUILD
                                            NET_PRINT("...CDM MP: Stack size after PS3 friend request prohibitive - closing phone.") NET_NL()
                                        #ENDIF

                                        g_Cellphone.PhoneDS = PDS_AWAY
                                         
                                    ENDIF
                                ELSE    

                                    NETWORK_SHOW_PROFILE_UI(m_sPlayers.selectedPlayerHandle)
                                    NET_PRINT_TIME() NET_PRINT("   Process_Stage_Confirm_Interaction()   ---------->     FRIEND REQUEST - OPEN PROFILE - DONE     <----------     ") NET_NL()
                                    m_sPlayers.bConfirmedInteraction = FALSE
                                    
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 4")
                                        cdPrintnl()   
                                    #endif

                                    //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
                                    //#IF IS_DEBUG_BUILD
                                        //NET_PRINT("...CDM MP: player finished selected interaction moving back to interaction screen") NET_NL()
                                    //#ENDIF

                                    #IF IS_DEBUG_BUILD
                                        NET_PRINT("...CDM MP: Stack size after OPTION FRIEND non-PS3 prohibitive - closing phone.") NET_NL()
                                    #ENDIF

                                    g_Cellphone.PhoneDS = PDS_AWAY

                                ENDIF
                            ENDIF
                        BREAK
                        CASE OPTION_CREW
                            IF NETWORK_CLAN_SERVICE_IS_VALID()
                                IF NETWORK_CLAN_PLAYER_IS_ACTIVE(m_sPlayers.selectedPlayerHandle)
                                    NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), m_sPlayers.selectedPlayerHandle)
                                ENDIF
                            ENDIF
                            IF clanDesc.Id != -1
                                IF clanDesc.IsOpenClan = 1
                                    IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(clanDesc.Id)
                                        IF NETWORK_CLAN_JOIN(clanDesc.Id)
                                            m_sPlayers.bConfirmedInteraction = FALSE

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 5")
                                                cdPrintnl()   
                                            #endif

                                            //1602475
                                            BEGIN_TEXT_COMMAND_THEFEED_POST("CREW_JOIN")
                                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(clanDesc.ClanName)
                                            END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE) 


                                            //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
                                            #IF IS_DEBUG_BUILD
                                                NET_PRINT("...CDM MP: Stack size after joining clan prohibitive - closing phone.") NET_NL()
                                            #ENDIF

                                            g_Cellphone.PhoneDS = PDS_AWAY


                                        ELSE

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 6")
                                                cdPrintnl()   
                                            #endif

                                            g_Cellphone.PhoneDS = PDS_AWAY
                                            //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
                                            #IF IS_DEBUG_BUILD
                                                NET_PRINT("......CDM MP: Stack size after clan operation prohibitive - closing phone.") NET_NL()
                                            #ENDIF
                                            m_sPlayers.bConfirmedInteraction = FALSE

                                            g_Cellphone.PhoneDS = PDS_AWAY


                                        ENDIF
                                        EXIT
                                    ENDIF
                                ENDIF
                            ENDIF
                            m_sPlayers.bConfirmedInteraction = FALSE
                        BREAK
            //          CASE OPTION_INVITE_CINEMA
            //              IF IS_NET_PLAYER_OK(PLAYER_ID())
            //                    IF IS_NET_PLAYER_OK(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],FALSE)
            //                      BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]),FMMC_TYPE_CINEMA,FMMC_ROCKSTAR_CREATOR_ID,0,MPGlobals.invitedPlayers.vInviteLoc)
            //                      DISPLAY_PLAYER_INTERACTION_SCREEN()
            //                      #IF IS_DEBUG_BUILD
            //                              NET_PRINT("...CDM MP: sent cinema invite to player from phone ") NET_NL()
            //                        #ENDIF
            //                      m_sPlayers.bConfirmedInteraction = FALSE
            //                  ENDIF
            //                ENDIF
            //          BREAK
    //                    CASE OPTION_REQUEST_PICKUP
    //                        IF IS_NET_PLAYER_OK(PLAYER_ID())
    //                            IF IS_NET_PLAYER_OK(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],FALSE)
    //                                REINIT_NET_TIMER(MPGlobalsAmbience.requestPickupDelay[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])
    //                                BROADCAST_SEND_BASIC_TEXT(SPECIFIC_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]),"MP_TXT_MSG0")
    //                                MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()
    //                                #IF IS_DEBUG_BUILD
    //                                    NET_PRINT("...CDM MP: sent request pickup from player ") NET_NL()
    //                                #ENDIF
    //                                m_sPlayers.bConfirmedInteraction = FALSE
    //                            ENDIF
    //                        ENDIF
    //                    BREAK
                        CASE OPTION_PROFILE
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: App Contact: showing profile for player ") NET_NL()
                            #ENDIF
                            m_sPlayers.bConfirmedInteraction = FALSE
                            Set_Up_Human_Friend_Menu()
                            NETWORK_SHOW_PROFILE_UI(m_sPlayers.selectedPlayerHandle)
                        BREAK
                        CASE OPTION_TAG_PLAYER
    //                      IF IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
    //                          CLEAR_BIT(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
    //                      ELSE
    //                          
    //                      ENDIF
                            IF IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                                CLEAR_BIT(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                            ELSE
                                MPGlobals.iTaggedPlayersBitSet= 0
                                SET_BIT(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                            ENDIF
                           Set_Up_Human_Friend_Menu()
                            m_sPlayers.bConfirmedInteraction = FALSE
                        BREAK
                    ENDSWITCH
                ENDIF
            ELSE
                // Check if the player responds NO and return to Player List screen
        //        IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        //            Place_Names_in_ContactSlots()
        //            #IF IS_DEBUG_BUILD
        //                NET_PRINT("...CDM MP: player exited confirmation moving back to interaction screen") NET_NL()
        //            #ENDIF
        //            EXIT
        //        ENDIF
        //        SWITCH m_sPlayers.optionList[m_sPlayers.m_selectedOptionSlot] 
        //          CASE OPTION_CREW
        //              SET_WARNING_MESSAGE_WITH_HEADER("","CRW_JOINCONFIRM",FE_WARNING_YESNO,default,FALSE,-1,m_sPlayers.M_selectedPlayerClanName)
        //          BREAK
        //      ENDSWITCH
        //        // Check if the player responds YES send a Private Chat request and move on to the 'Chat Status' screen
        //        IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        //            // The player list shouldn't update while the player is deeper into the app than the PlayerList screen to ensure player list details are preserved.
        //            
        //            /
        //            
        //            
        //    //      // Send the Private Voice Chat request to the specific player
        //    //      BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PRIVATE_VOICE_CHAT_REQUEST, SPECIFIC_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
        //    //      #IF IS_DEBUG_BUILD
        //    //          NET_PRINT("...CDM MP: Sent request for a private chat with another player") NET_NL()
        //    //      #ENDIF
        //    //      // Display the Chat Status screen
        //    //      Display_Chat_Status_Screen()
        //            
        //            EXIT
        //        ENDIF
        //      
                //Macros
                IF g_InputButtonJustPressed = FALSE
                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.
                        m_sPlayers.bButtonReleased = FALSE
                        m_sPlayers.bLocalNotOnline = FALSE

                        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                        b_DoCancelConfirmation = FALSE


                        Play_Select_Beep()


                        g_InputButtonJustPressed = TRUE

                        
                   
                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                        Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


                        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
                        
                            WAIT (0)

                            #if IS_DEBUG_BUILD
                                cdPrintstring("appContacts - Waiting on scaleform return during Check_For_Human_Friend_Menu_Decision.")
                                cdPrintnl()
                            #endif

                        ENDWHILE


                        m_sPlayers.m_selectedOptionSlot = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)

                        // Is the player still in the network game?
                        IF NOT IS_CURRENT_SELECTION_VALID(m_sPlayers,iValidSelectionFailReason)
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Attempting to request interaction with player, but player is no longer in network game - returning to PlayerList screen") NET_NL()
                            #ENDIF
                            SWITCH iValidSelectionFailReason
                                CASE FAIL_REASON_TARGET_PLAYER_NOT_ONLINE
                                    PRINT_NOW("PLAYER_N_ON", DEFAULT_GOD_TEXT_TIME, 1)
                                BREAK
                                
                                CASE FAIL_REASON_TARGET_PLAYER_NOT_IN_GAME
                                    PRINT_NOW("PLAYERLEFT", DEFAULT_GOD_TEXT_TIME, 1)
                                BREAK
                                
                                CASE FAIL_REASON_LOCAL_PLAYER_NOT_ONLINE
                                    voiceSession.bExitOnNewCall = FALSE
                                    m_sPlayers.bLocalNotOnline = TRUE
                                    m_sPlayers.bConfirmedInteraction = TRUE
                                    EXIT
                                BREAK
                            ENDSWITCH
                                                           
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 7")
                                cdPrintnl()   
                            #endif


                            //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()


                            //For safety 20.09.13
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Stack size after IS_CURRENT_SELECTION_VALID returned false prohibitive - closing phone.") NET_NL()
                            #ENDIF

                            g_Cellphone.PhoneDS = PDS_AWAY



                            EXIT
                        ENDIF
//                        IF m_sPlayers.optionList[m_sPlayers.m_selectedOptionSlot] = OPTION_RACE_TO_POINT
//                            IF IS_CURRENT_SELECTION_VALID(m_sPlayers)
//                                IF IS_NET_PLAYER_OK(PLAYER_ID())
//                                    IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_PLAYER_COORDS(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) <= R2P_INVITE_RADIUS
//                                        m_sPlayers.bConfirmedInteraction = TRUE    
//                                    ENDIF
//                                ENDIF
//                            ENDIF
                        //ELSE
                           // RESET_NET_TIMER(m_sPlayers.partyTimeOut)
                            voiceSession.bExitOnNewCall = FALSE
                            m_sPlayers.bConfirmedInteraction = TRUE
                            PRINTLN("...CDM MP: App Contact setting confirmation of interaction to true")
                       // ENDIF
                        
                            
//                        IF m_sPlayers.optionList[m_sPlayers.m_selectedOptionSlot] = OPTION_RACE_TO_POINT
//                            IF IS_CURRENT_SELECTION_VALID(m_sPlayers)
//                                IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_PLAYER_COORDS(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) <= R2P_INVITE_RADIUS
//                                    EXIT
//                                ENDIF
//                            ENDIF
//                            #IF IS_DEBUG_BUILD
//                                NET_PRINT("...CDM MP: moving back to player option screen") NET_NL()
//                            #ENDIF  
//                            MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()              
//                            EXIT
//                        ENDIF
                    ENDIF
                ENDIF
            ENDIF   
        BREAK
        
        CASE PLAYER_INTERACTION_STAGE_IN_CALL

            IF NOT IS_CURRENT_SELECTION_VALID(m_sPlayers, iValidSelectionFailReason)
                #IF IS_DEBUG_BUILD
                   PRINTLN("...CDM MP: selected player is no longer valid aborting for reason #", iValidSelectionFailReason)
                #ENDIF

                SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE)

                 #IF IS_DEBUG_BUILD
                    NET_PRINT("...CDM MP: Call failed showing busy.") NET_NL()
                #ENDIF
                SCRIPT_END_A_VOICE_SESSION()

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Script End A Voice Session - 7")
                                    cdPrintnl()   
                                #endif


         
                m_sPlayers.bBusy = TRUE
                m_sPlayers.bCallRequestSent = TRUE
                EXIT

            ELSE

                IF m_sPlayers.bCallRequestSent
                    IF NOT m_sPlayers.bBusy
                    
                        IF NOT voiceSession.bInVoiceSession
                        OR NOT NETWORK_SESSION_IS_IN_VOICE_SESSION()
                            PRINTLN("...CDM MP: voiceSession.bInVoiceSession = false while awaiting response bailing out")
                            /*
                            SCRIPT_END_A_VOICE_SESSION()
                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Script End A Voice Session - 8")
                                    cdPrintnl()   
                                #endif

                            m_sPlayers.bConfirmedInteraction = FALSE
                            Set_Up_Human_Friend_Menu()
                            EXIT
                            */

                            SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1602347


                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Call failed showing busy because of auto-rejection by other player probably.") NET_NL()
                            #ENDIF
                                    
                                SCRIPT_END_A_VOICE_SESSION()

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Script End A Voice Session - 8")
                                    cdPrintnl()   
                                #endif


         
                                m_sPlayers.bBusy = TRUE
                                m_sPlayers.bCallRequestSent = TRUE
                                EXIT

                        ENDIF




                        // Check if the player quits the private chat
                       // IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
                       IF NETWORK_IS_SIGNED_ONLINE()
                            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)) 
                                #if IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Player Quit the Private Chat - returning to player interaction screen 1") NET_NL()
                            
                                    cdPrintnl()
                                    cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 9")
                                    cdPrintnl()   
                                #endif


                                SCRIPT_END_A_VOICE_SESSION()
                                m_sPlayers.bConfirmedInteraction = FALSE
                                //Set_Up_Human_Friend_Menu()
                                EXIT
                            ENDIF
                            IF IS_GAMER_HANDLE_VALID(voiceSession.gamerHandle)
                                IF NETWORK_IS_FRIEND(voiceSession.gamerHandle)
                                    IF NOT NETWORK_IS_FRIEND_HANDLE_ONLINE(voiceSession.gamerHandle)
                                        PRINTLN("App contacts: killing voice call ", voiceSession.gamerName, " is not online")
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("Script End A Voice Session - 9")
                                            cdPrintnl()   
                                        #endif

                                        SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1602347

                                        SCRIPT_END_A_VOICE_SESSION()

                                        //Set_Up_Human_Friend_Menu()
                                        m_sPlayers.bBusy = TRUE
                                        m_sPlayers.bCallRequestSent = TRUE
                                        EXIT
                                    ENDIF
                                ENDIF
                            ELSE
                                PRINTLN("App contacts: killing voice call gamer handle not valid")
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("Script End A Voice Session - 10")
                                        cdPrintnl()   
                                    #endif

                                SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1602347

                                SCRIPT_END_A_VOICE_SESSION()

                                //Set_Up_Human_Friend_Menu()
                                m_sPlayers.bBusy = TRUE
                                m_sPlayers.bCallRequestSent = TRUE
                                EXIT
                            ENDIF
                        ELSE
                                #if IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Player Killing Private Chat NOT ONLINE! - returning to player interaction screen") NET_NL()
                                                        cdPrintnl()
                                    cdPrintstring("Script End A Voice Session - 11")
                                    cdPrintnl()   
                                #endif

                            SCRIPT_END_A_VOICE_SESSION()
                            m_sPlayers.bConfirmedInteraction = FALSE
                            Set_Up_Human_Friend_Menu()
                            EXIT
                        ENDIF
    //                  SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE)
    //                  SCRIPT_END_A_VOICE_SESSION()
    //                  #IF IS_DEBUG_BUILD
    //                      NET_PRINT("...CDM MP: Call receiving player is busy displaying engaged") NET_NL()
    //                  #ENDIF
    //                  EXIT    
                        IF NOT voiceSession.bConnectedCallOnGoing
                            IF voiceSession.iScriptResponse= -1
                                //IF HAS_NET_TIMER_EXPIRED(m_sPlayers.voiceSessionTimer, VOICE_SESSION_TIMEOUT, TRUE) 
                                IF HAS_NET_TIMER_EXPIRED(m_sPlayers.voiceSessionTimer, 40000, TRUE) //Halving this...

                                    #IF IS_DEBUG_BUILD
                                        NET_PRINT("...CDM MP: Call timed out - setting call to busy.") NET_NL()
                                    #ENDIF
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("Script End A Voice Session - 12")
                                            cdPrintnl()   
                                        #endif


                                    SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1602347


                                    SCRIPT_END_A_VOICE_SESSION()

                                    //Set_Up_Human_Friend_Menu()
                                    m_sPlayers.bBusy = TRUE
                                    m_sPlayers.bCallRequestSent = TRUE
                             

                                    EXIT
                                ENDIF
                            ELSE
                                SWITCH voiceSession.iScriptResponse
                                    CASE PLY_TO_PLY_CALL_RESPONSE_ACCEPTED
                                        #IF IS_DEBUG_BUILD
                                            NET_PRINT("...CDM MP: Call connected ") NET_NL()
                                        #ENDIF
                                        SET_PLAYER_CHAT_CALL_AS_CONNECTED()

                                        TitleUpdate_Post_Set_Player_Chat_Call_As_Connected()

                                        IF NETWORK_IS_GAME_IN_PROGRESS()


                                            //Commented out until I suss out 1604163
                                            /*
                                            IF GET_VC_CASH_SPLIT(10*g_sMPTunables.iPhoneFriendCharge, iBankAmount, iWalletAmount)
                                                NETWORK_SPENT_CALL_PLAYER(iBankAmount, voiceSession.gamerHandle,TRUE)
                                            ELSE
                                                NETWORK_SPENT_CALL_PLAYER(iBankAmount, voiceSession.gamerHandle,TRUE)
                                                NETWORK_SPENT_CALL_PLAYER(iWalletAmount, voiceSession.gamerHandle,FALSE)
                                            ENDIF
                                            */

                                            IF iBankAmount = iWalletAmount
                                            ENDIF

                                        ENDIF
                                        STOP_SOUND (Phone_Dialling_SoundID)
                                       // HANG_UP_AND_PUT_AWAY_PHONE()
                                       // TERMINATE_APPCONTACTS_FOR_CHAT_CALL()
                                        EXIT
                                   BREAK
                                   CASE PLY_TO_PLY_CALL_RESPONSE_ENGAGED
                                        SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE)
                                         #IF IS_DEBUG_BUILD
                                            NET_PRINT("...CDM MP: Call receiving player is busy displaying engaged") NET_NL()
                                        #ENDIF
                                        
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("Script End A Voice Session - 13")
                                            cdPrintnl()   
                                        #endif

                                        SCRIPT_END_A_VOICE_SESSION()
                                        m_sPlayers.bBusy = TRUE
                                        EXIT
                                   BREAK
                                   
                                   DEFAULT
                                        #IF IS_DEBUG_BUILD
                                            PRINTLN("...CDM MP: Call over for reason ", voiceSession.iScriptResponse, " - returning to player interaction screen") NET_NL()
                                        #ENDIF

                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("Script End A Voice Session - 14")
                                            cdPrintnl()   
                                        #endif

                                        SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1596679

                                        
                                        SCRIPT_END_A_VOICE_SESSION()
                                        m_sPlayers.bBusy = TRUE
                                      
                                        
                                        //HANG_UP_AND_PUT_AWAY_PHONE()
                                        //TERMINATE_APPCONTACTS_FOR_CHAT_CALL()
                                        EXIT    


                                   BREAK
                                ENDSWITCH
                            ENDIF
                        ENDIF
                    ENDIF
    //                IF voiceSession.iCallResponse = -1
    //                    IF HAS_NET_TIMER_EXPIRED(m_sPlayers.voiceSessionTimer, VOICE_SESSION_TIMEOUT)
    //                        #IF IS_DEBUG_BUILD
    //                            NET_PRINT("...CDM MP: Call timed out - returning to player interaction screen") NET_NL()
    //                        #ENDIF
    //                        SCRIPT_END_A_VOICE_SESSION()
    //                        Set_Up_Human_Friend_Menu()
    //                        EXIT
    //                    ENDIF
    //                ELSE
    //                    IF voiceSession.iCallResponse != ENUM_TO_INT(RESPONSE_ACCEPTED)
    //                        #IF IS_DEBUG_BUILD
    //                            PRINTLN("...CDM MP: Call over response was ",voiceSession.iCallResponse, " - returning to player interaction screen")
    //                        #ENDIF
    //                        SCRIPT_END_A_VOICE_SESSION()
    //                        HANG_UP_AND_PUT_AWAY_PHONE()
    //                        TERMINATE_APPCONTACTS_FOR_CHAT_CALL()
    //
    //                        EXIT
    //                    ELSE
    //                        #IF IS_DEBUG_BUILD
    //                            NET_PRINT("...CDM MP: Call connected - returning to player interaction screen") NET_NL()
    //                        #ENDIF
    //                        HANG_UP_AND_PUT_AWAY_PHONE()
    //                        TERMINATE_APPCONTACTS_FOR_CHAT_CALL()
    //
    //                        EXIT
    //                    ENDIF
    //                ENDIF
                ELSE

                    IF NOT HAS_NET_TIMER_STARTED(m_sPlayers.voiceSessionTimer)

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Resetting and starting Chat Caller timer.")
                            cdPrintnl()   
                        #endif



                        RESET_NET_TIMER(m_sPlayers.voiceSessionTimer) //1602347
                    
                        START_NET_TIMER(m_sPlayers.voiceSessionTimer, TRUE)
                    
                    ELSE


                        //This fires a lot! 1602347
                        IF HAS_NET_TIMER_EXPIRED(m_sPlayers.voiceSessionTimer, 15000, TRUE)

                            
                            // Possibly replace this section with engaged routine below for #1596679?
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Call timed out (before ever starting voice session ) - returning to player interaction screen") NET_NL()
                            #ENDIF
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("Script End A Voice Session - 15")
                                        cdPrintnl()   
                                    #endif

                            SCRIPT_END_A_VOICE_SESSION()

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 8")
                                cdPrintnl()   
                            #endif


                            //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()

                            
                            Play_Back_Beep()    //1602347



                            Remove_Cellphone_From_Ear()  //1602347

                            

                            SCRIPT_ASSERT("Didn't receive VOICE_SESSION_STARTED_EVENT after 15 seconds. Pass logs to Conor M.")


                            //For safety 20.09.13
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: Stack size after VOICE_SESSION_STARTED_EVENT wasn't received prohibitive - closing phone.") NET_NL()
                            #ENDIF

                            g_Cellphone.PhoneDS = PDS_AWAY




                            EXIT



                            /*
                            SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE) //1596679

                            #IF IS_DEBUG_BUILD
                                NET_PRINT("...CDM MP: ...CDM MP: Call timed out (before ever starting voice session ) - setting as busy as a failsafe.") NET_NL()
                            #ENDIF
                                    
                                SCRIPT_END_A_VOICE_SESSION()

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Script End A Voice Session - 18")
                                    cdPrintnl()   
                                #endif


         
                                m_sPlayers.bBusy = TRUE
                                m_sPlayers.bCallRequestSent = TRUE


                                SCRIPT_ASSERT("Didn't receive VOICE_SESSION_STARTED_EVENT after 10 seconds. Pass logs to Conor M.")


                                EXIT
                            */

                        ENDIF


                        // Check if the player quits the private chat
                        IF voiceSession.bInVoiceSession //Adding this on a trial basis to prevent quitting before PROCESS_VOICE_SESSION_STARTED_EVENT() has fired.
                        OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)
                            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))  
                                #if IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Player Quit the Private Chat - returning to player interaction screen 2") NET_NL()

                                    cdPrintnl()
                                    cdPrintstring("MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS call 9")
                                    cdPrintnl()   
                                #endif

                                SCRIPT_END_A_VOICE_SESSION()

                                //MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()

                                //For safety 20.09.13
                                #IF IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Stack size after inVoiceSession quit prohibitive - closing phone.") NET_NL()
                                #ENDIF

                                g_Cellphone.PhoneDS = PDS_AWAY

                                EXIT
                            ENDIF
                        ENDIF

                        IF voiceSession.bInVoiceSession

                            IF IS_INITIAL_CALL_PROCESSING_COMPLETE(m_sPlayers,m_sPlayers.selectedPlayerName)
                                
                                REINIT_NET_TIMER(m_sPlayers.voiceSessionTimer, TRUE)

                                PRINTLN("...CDM MP: Sent phone call event to player ")
                                m_sPlayers.bBusy = FALSE
                                m_sPlayers.bCallRequestSent = TRUE
                                IF g_bInMultiplayer
                                    SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_CALL_FRIEND, TRUE)
                                ENDIF

                            ELSE

                                PRINTLN("...CDM MP: voiceSession.bInVoiceSession TRUE in call false killing call ")
    //                            SCRIPT_END_A_VOICE_SESSION()
    //                            MP_PLAYER_INTERACTION_MOVE_BACK_TO_CONTACTS()

                                SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE)
    
    
    
    
                                #IF IS_DEBUG_BUILD
                                    NET_PRINT("...CDM MP: Call failed for some reason (started voice session but not in one??) showing busy.") NET_NL()
                                #ENDIF

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("Script End A Voice Session - 16")
                                        cdPrintnl()   
                                    #endif

                                SCRIPT_END_A_VOICE_SESSION()
                                m_sPlayers.bBusy = TRUE
                                m_sPlayers.bCallRequestSent = TRUE

                                EXIT
                            ENDIF
                        ELSE
                            PRINTLN("...CDM MP: waiting for voiceSession.bInVoiceSession")
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        BREAK
        
        CASE PLAYER_INTERACTION_STAGE_SEND_MESSAGE

            //Maintain Keyboard Use
            IF bCreatingSendMessage = TRUE
                IF RUN_ON_SCREEN_KEYBOARD(KeyBoardData.eStatus, KeyBoardData.iProfanityToken, KeyBoardData.iCurrentStatus, FALSE, FALSE, tlSendMessage, FALSE, 0, TRUE)
                    tlSendMessage = GET_ONSCREEN_KEYBOARD_RESULT()
                    NET_PRINT_TIME() NET_PRINT("     ---------->     APP - MAINTAIN_PERSISTENT_CHECKS - KEYBOARD RESULT = ") NET_PRINT(tlSendMessage) NET_NL()
                    KeyBoardData.eStatus = OSK_PENDING
                    KeyBoardData.iCurrentstatus = OSK_STAGE_SET_UP                  
                    KeyBoardData.iProfanityToken = 0
                    bCreatingSendMessage = FALSE
                    
                    iStringLengthInCharacters = GET_LENGTH_OF_LITERAL_STRING(tlSendMessage)
                    tlSendMessage = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(tlSendMessage, 0, iStringLengthInCharacters, 63)
                    
                    REINIT_NET_TIMER(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])

                    
                            
                    IF IS_STRING_NULL_OR_EMPTY (CONVERT_TEXT_LABEL_TO_STRING(tlSendMessage))

                        #if IS_DEBUG_BUILD

                            cdPrintstring ("AppContacts - NULL OR empty message from OPTION_SEND_MESSAGE keyboard result. Not sending. #1896032")
                            cdPrintnl()

                            PRINTLN("AppContacts - NULL OR empty message from OPTION_SEND_MESSAGE keyboard result. Not sending. #1896032")

                        #endif

                    ELSE
                    
                        GAMER_HANDLE thisHandle
                        thisHandle = GET_GAMER_HANDLE_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                        
                        //Send the Message to the Player
                        //REINIT_NET_TIMER(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])
                        NETWORK_SEND_TEXT_MESSAGE(tlSendMessage, thisHandle)    //BROADCAST_TEXT_MESSAGE_USING_LITERAL(tlSendMessage, SPECIFIC_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                        NET_PRINT_TIME() NET_PRINT("     ---------->     APP - ciPI_SUB_MENU_SM_PLAYER_LIST - MESSAGE SENT TO ") NET_PRINT(GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) NET_NL()
                        NET_PRINT_TIME() NET_PRINT("     ---------->     APP - ciPI_SUB_MENU_SM_PLAYER_LIST - MESSAGE = ") NET_PRINT(tlSendMessage) NET_NL()
                        
                        PRINT_TICKER_WITH_PLAYER_NAME("CP_TM_SENT", m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])  //Your message has been sent to ~a~
                        
                        INCREMENT_BY_MP_INT_CHARACTER_STAT (MP_STAT_TEXTS_SENT_TO_PLAYERS, 1) //1782892

                    ENDIF

                    m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT
                    PRINTLN("     ---------->     APP - PLAYER_INTERACTION_STAGE_SEND_MESSAGE - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT - B")
                ENDIF
                
                IF KeyBoardData.eStatus = OSK_CANCELLED
                    NET_PRINT_TIME() NET_PRINT("     ---------->     APP - MAINTAIN_PERSISTENT_CHECKS - KEYBOARD CLOSED - oskStatus = ") NET_PRINT_INT(ENUM_TO_INT(KeyBoardData.eStatus)) NET_NL()
                    KeyBoardData.eStatus = OSK_PENDING
                    KeyBoardData.iCurrentstatus = OSK_STAGE_SET_UP                  
                    KeyBoardData.iProfanityToken = 0
                    bCreatingSendMessage = FALSE
                    
                    m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT
                    PRINTLN("     ---------->     APP - PLAYER_INTERACTION_STAGE_SEND_MESSAGE - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SELECT - B")
                ENDIF
            ENDIF
            
            
        BREAK
    ENDSWITCH

ENDPROC




PROC Set_Up_Call_Hangout_Secondary_Cancel_Menu()


        SETTIMERA(0) //This will be SP only, so safe to use that single timer rather than net time paradigm.


        //See bug 832525
        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

            g_Cellphone.PhoneDS = PDS_COMPLEXAPP

            /*
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 171237. appContacts assigns PDS_COMPLEXAPP. This will get reassigned when dynamically updating.")
                cdPrintnl()   
            #endif
            */
        ENDIF







        //Badger - Select Back combo
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_210") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        ELSE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)





        b_DoFriend_SecondaryCallRoutine = TRUE




        //Set up menu and bool to do friend contact selection

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)


     

           
        //Old display order for CALL_HANGOUT/CANCEL_SECONDARY. 1142222 requested change to SECONDARY_CALL_HANGOUT...
        /*
        //Changed at Sam's request (827347) - Section 4. Checks Friend functions to decide which additional menu option, if any, to display in Scaleform slot 1.

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208") 



        IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_209") //Display Hang out option.

            #if IS_DEBUG_BUILD

                cdPrintstring ("AppCONTACTS - Can ARRANGE activity with Friend, displaying Hang Out option in SF Slot 1.")
                cdPrintnl()

            #endif

        ENDIF


        //Deliberately not using an ELSE here, a consecutive debug print will highlight a problem in the decision making of CAN_CHARS_ARRANGE or CAN_CHARS_CANCEL...
        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_222") //Display Cancel option.


            #if IS_DEBUG_BUILD

                cdPrintstring ("AppCONTACTS - Can CANCEL activity with this Friend, displaying Cancel option in SF Slot 1.")
                cdPrintnl()

            #endif

        ENDIF


        

        IF b_Secondary_Option_Available 

            IF b_Friend_Option_Available //Decide which slot to use based on the presence of a friend option.

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(2)),
                    (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_SpecialSPCharacters[i_Available_Option].SecondaryFunctionLabel_1) 

            
            ELSE


                IF g_bInMultiplayer

                      LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_SpecialMPCharacters[i_Available_Option].SecondaryFunctionLabel_1)

                ELSE

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_SpecialSPCharacters[i_Available_Option].SecondaryFunctionLabel_1)


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppCONTACTS - b_Friend_Option unavailable. Placing secondary option in slot 1.")
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF

        ENDIF
        */




        

        //Need to hook up #1033532  IS_CHAT_CALL_AVAILABLE_FOR_CHARACTER(enumCharacterList paramChar) branch here - DONE.

        IF IS_CHAT_CALL_AVAILABLE_FOR_CHARACTER(g_TheContactInvolvedinCall)
        //IF g_AllowUnderwaterDialogue// - swap in this for the above for easy testing.





                //New order SECONDARY_CALL_HANGOUT/CANCEL for bug 1142222 
                IF b_Secondary_Option_Available

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_SpecialSPCharacters[i_Available_Option].SecondaryFunctionLabel_1) 


                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")  //Normal Story Call...


                    IF b_Friend_Option_Available
                    

                        IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(2)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_209") //Display Hang out option.

                            /*
                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can ARRANGE activity with Friend, displaying Hang Out option in SF Slot 1.")
                                cdPrintnl()

                            #endif
                            */

                        ENDIF


                        //Deliberately not using an ELSE here, a consecutive debug print will highlight a problem in the decision making of CAN_CHARS_ARRANGE or CAN_CHARS_CANCEL...
                        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(2)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_222") //Display Cancel option.


                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can CANCEL activity with this Friend, displaying Cancel option in SF Slot 1.")
                                cdPrintnl()

                            #endif

                        ENDIF


                    ENDIF


                ELSE   //Slot zero will be free, so Normal story call and friend option all move up one slot.


                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")  //Normal Story Call...



                    IF b_Friend_Option_Available
                    

                        IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_209") //Display Hang out option.
                            /*
                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can ARRANGE activity with Friend, displaying Hang Out option in SF Slot 1.")
                                cdPrintnl()

                            #endif
                            */

                        ENDIF


                        //Deliberately not using an ELSE here, a consecutive debug print will highlight a problem in the decision making of CAN_CHARS_ARRANGE or CAN_CHARS_CANCEL...
                        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_222") //Display Cancel option.


                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can CANCEL activity with this Friend, displaying Cancel option in SF Slot 1.")
                                cdPrintnl()

                            #endif

                        ENDIF


                    ENDIF


                ENDIF







        ELSE







                 //New order SECONDARY_CALL_HANGOUT/CANCEL for bug 1142222 
                IF b_Secondary_Option_Available

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_SpecialSPCharacters[i_Available_Option].SecondaryFunctionLabel_1) 


      
                    IF b_Friend_Option_Available
                    

                        IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_209") //Display Hang out option.

                            /*
                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can ARRANGE activity with Friend, displaying Hang Out option in SF Slot 1.")
                                cdPrintnl()

                            #endif
                            */

                        ENDIF


                        //Deliberately not using an ELSE here, a consecutive debug print will highlight a problem in the decision making of CAN_CHARS_ARRANGE or CAN_CHARS_CANCEL...
                        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_222") //Display Cancel option.


                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can CANCEL activity with this Friend, displaying Cancel option in SF Slot 1.")
                                cdPrintnl()

                            #endif

                        ENDIF


                    ENDIF


                ELSE   //Slot zero will be free, so Normal story call and friend option all move up one slot.


                


                    IF b_Friend_Option_Available
                    

                        IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_209") //Display Hang out option.
                            /*
                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can ARRANGE activity with Friend, displaying Hang Out option in SF Slot 1.")
                                cdPrintnl()

                            #endif
                            */

                        ENDIF


                        //Deliberately not using an ELSE here, a consecutive debug print will highlight a problem in the decision making of CAN_CHARS_ARRANGE or CAN_CHARS_CANCEL...
                        IF CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) //Yes, can arrange actvity.

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_222") //Display Cancel option.


                            #if IS_DEBUG_BUILD

                                cdPrintstring ("AppCONTACTS - Can CANCEL activity with this Friend, displaying Cancel option in SF Slot 1.")
                                cdPrintnl()

                            #endif

                        ENDIF


                    ENDIF


                ENDIF





        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18, TO_FLOAT(i_Selected_Slot_to_Restore))
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
ENDPROC
















PROC CancellationConfirmation()



    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.


        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


        Play_Select_Beep()


        g_InputButtonJustPressed = TRUE


        
   
        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

        Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
        
            WAIT (0)

            #if IS_DEBUG_BUILD
                cdPrintstring("appContacts - Waiting on scaleform return during Check_For_Contact_Selection.")
                cdPrintnl()
            #endif

        ENDWHILE


        SF_SelectedChoiceInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)


        IF SF_SelectedChoiceInt = 0 //Selected YES - cancellation confirmed.

             
            This_Player_Request_Cancellation[g_Cellphone.PhoneOwner] = TRUE


            b_DoFriend_SecondaryCallRoutine = FALSE

            g_b_Is_This_An_Activity_Call = FALSE
            g_b_Is_This_An_Activity_Cancellation_Call = TRUE
            g_b_Is_This_A_Secondary_Function_Call = FALSE


            Do_Call()


        ELSE

            // Selected NO - Go back to previous screen

            b_DoCancelConfirmation = FALSE

            Set_Up_Call_Hangout_Secondary_Cancel_Menu()

        ENDIF


    ENDIF
    ENDIF



ENDPROC












PROC Check_For_Contact_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    //Macros
    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - contact selected.

        IF NOT Is_SinglePlayer_Climbing()


            //See bug 823426 - Removed as this was causing hangout menu to launch and will interfere with confirmation screen.
            //CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

            g_b_Is_This_An_Activity_Call = FALSE //reset as a failsafe.
            g_b_Is_This_An_Activity_Cancellation_Call = FALSE
            g_b_Is_This_A_Secondary_Function_Call = FALSE


            DialledNumberHasGameworldAssociation = FALSE


            Play_Select_Beep()


            g_InputButtonJustPressed = TRUE


            //Start
                IF i_Number_Of_Contacts_In_List > 0   //don't let the player call the "no contacts" entry! This means that to choose the Finale contact in debug, you need to make sure 
                                                //the player character has a normal contact in their list.

                
                    //g_TheContactInvolvedinCall = ContactListSlot[ListCursorIndex] // Point to contents of array rather than just the cursor index itself.
                    

                        

                    //SF_SelectedContactInt = LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()




                   

                    WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
                                        
                        WAIT (0)

                        #if IS_DEBUG_BUILD
                            cdPrintstring("appContacts - Waiting on scaleform return during Check_For_Contact_Selection.")
                            cdPrintnl()
                        #endif

                    ENDWHILE


                    SF_SelectedContactInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)



                    BOOL b_ShouldLaunchHumanFriendMenu = FALSE //Have a nice, clear check rather than ANDs , ORs.

                    IF NETWORK_IS_SIGNED_ONLINE()

                        //CL_TOP
                        IF g_bInMultiplayer //This caters for having the friends above the NPC contacts.

                            IF SF_SelectedContactInt < NPCSlot_StartingIndex

                                #if IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("AppContacts - Launching human friend menu as selected contact int is lower than NPCSlot_StartingIndex")
                                    cdPrintnl()

                                #endif

                                b_ShouldLaunchHumanFriendMenu = TRUE

                            ENDIF

                        ELSE
                                                
                            IF SF_SelectedContactInt = FriendSlot_StartingIndex //Story / Human delimiter check.
                            OR SF_SelectedContactInt > FriendSlot_StartingIndex

                                b_ShouldLaunchHumanFriendMenu = TRUE

                            ENDIF
                    
                        ENDIF

                    ENDIF


                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                        b_ShouldLaunchHumanFriendMenu = FALSE //Critical! Make sure we completely ignore human friends in finale list.

                    ENDIF

                    
                    IF b_ShouldLaunchHumanFriendMenu

                        //CL_TOP - MP list would just be normal....
                        m_sPlayers.m_selectedPlayerSlot = SF_SelectedContactInt - FriendSlot_StartingIndex
                        
                        #if IS_DEBUG_BUILD
                            cdPrintstring("appContacts - Human friend slot selected")
                            cdPrintnl()
                            PRINTLN("SF_SelectedContactInt: ", SF_SelectedContactInt)
                            PRINTLN("FriendSlot_StartingIndex: ", SF_SelectedContactInt)
                            PRINTLN("m_sPlayers.m_selectedPlayerSlot: ", m_sPlayers.m_selectedPlayerSlot)
                        #endif
                        

                        Set_Up_Human_Friend_Menu()

                    ELSE




                        IF SF_SelectedContactInt < 0 //Scaleform can return -1 is the contact list hasn't been filled in time and the player really quickly presses X multiple times.

                            SF_SelectedContactInt = 0   //Set to zero if this is the case to prevent array overrrun. See bug 65344 
                    
                        ENDIF





                       
                        g_TheContactInvolvedinCall = INT_TO_ENUM (enumCharacterList, (ContactListSlot[SF_SelectedContactInt]))

                        
                        IF g_ForcedContactSelection <> NO_CHARACTER   //A force is in place, that means no other contact bar the forced character can be selected...

                            #if IS_DEBUG_BUILD
                                PRINTNL()
                                PRINTSTRING ("AppContacts - A force selection of a contact is in place, will not take any action if selected contact does not match force.")
                                PRINTNL()
                            #endif


                            IF g_TheContactInvolvedinCall <> g_ForcedContactSelection

                                b_VoidContactSelectionDueToForce = TRUE //Selected character does not match force. Void further action.

                            ELSE

                                b_VoidContactSelectionDueToForce = FALSE
                            
                            ENDIF

                        ELSE

                            b_VoidContactSelectionDueToForce = FALSE

                        ENDIF
                       

                        

                        IF b_VoidContactSelectionDueToForce = FALSE
                            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)


                                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDPIC_CONTACT_SELECTED)

                                    #if IS_DEBUG_BUILD
                                        PRINTNL()
                                        PRINTSTRING ("AppContacts - Fudge sending pic to chosen contact")
                                        PRINTNL()
                                    #endif

                        			GLOBAL_CHARACTER_SHEET_SET_PICTURE_MESSAGE_STATUS(g_TheContactInvolvedinCall, HAS_PICMSG_STORED)

                                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 


                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(1)),
                                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_288",
                                            "CELL_300",
                                             g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phoneBookNumberLabel,
                                            "CELL_195" )
                                               

                                    ELSE
                                        
                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(1)),
                                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_288",
                                           	g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,

                                            //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                             g_sCharacterSheetAll[g_TheContactInvolvedinCall].label )

                                    ENDIF
                                    
                                        //Get rid of "send to" button
                                        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 0,
                                            //INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other

                                    


                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other




                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen which can we use as a sending screen for just now...

           

                                        #if IS_DEBUG_BUILD

                                            cdPrintstring ("Display View 4 - appContacts 1")
                                            cdPrintnl()

                                        #endif


                                     SET_BIT (BitSet_CellphoneDisplay, g_BS_SENDPIC_CONTACT_SELECTED)


                                 ENDIF




                               
                            ELSE
                           




                                //Decide if we should display any friend, secondary or tertiary function besides call or hangout.

                                b_Secondary_Option_Available = FALSE
                                b_Friend_Option_Available = FALSE


                                IF g_bInMultiplayer

                                        INT tempIndex = 0
                                        
                                        
                                        WHILE tempIndex < MAX_SPECIAL_MP_CHARACTERS

                                       
                                            IF ARE_STRINGS_EQUAL (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label, g_SpecialMPCharacters[tempIndex].Name_TextLabel)

                                                #if IS_DEBUG_BUILD
                                                    PRINTNL()
                                                    PRINTSTRING("Found special MP character at index ")
                                                    PRINTINT (tempIndex)
                                                    PRINTNL()
                                                #endif

                                                IF NOT ARE_STRINGS_EQUAL (g_SpecialMPCharacters[tempIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL") //A secondary function must have been defined if the contents are no longer the predefined null.

                                                    b_Secondary_Option_Available = TRUE //If we need to support a tertiary option add an extra int so we can distinguish between 
                                                                                        //secondary or tertiary calls. 

                                                    i_Available_Option = tempIndex //Get a copy so we can extract the text label when constructing the menu.

                                                ENDIF


                                                //Set up call by using a global int to specify if the secondary option label is zero, 1 or 2 before do call.
                                                //When Keith checks IS_CALLING_CONTACT (CHAR_EXAMPLE)
                                                //He can interrogate the global to see if it's a function call or a normal call (i.e the global int is zero)



                                            ENDIF

                                            tempIndex ++

                                        ENDWHILE

                                ELSE   //SP
                                        
                                        INT tempIndex = 0
                                        
                                        
                                        WHILE tempIndex < MAX_SPECIAL_SP_CHARACTERS

                                       
                                            IF ARE_STRINGS_EQUAL (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label, g_SpecialSPCharacters[tempIndex].Name_TextLabel)

                                                #if IS_DEBUG_BUILD
                                                    PRINTNL()
                                                    PRINTSTRING("Found special SP character at index ")
                                                    PRINTINT (tempIndex)
                                                    PRINTNL()
                                                #endif

                                                IF NOT ARE_STRINGS_EQUAL (g_SpecialSPCharacters[tempIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL") //A secondary function must have been defined if the contents are no longer the predefined null.

                                                    b_Secondary_Option_Available = TRUE

                                                    i_Available_Option = tempIndex //Get a copy so we can extract the text label when constructing the menu.


                                                    //quick unref hack.
                                                    IF i_Available_Option = i_Available_Option
                                                    
                                                        //do nothing.
                                                    
                                                    ENDIF

                                                ENDIF


                                            ENDIF

                                            tempIndex ++

                                        ENDWHILE


                                 ENDIF





                                IF IS_THIS_CHARACTER_FRIEND_OF_CURRENT_PLAYER_CHAR (g_TheContactInvolvedinCall)
                                AND (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) = FALSE) //See bug 823426 - No hangout option during finale choice list.

                                    IF (CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) = FALSE)
                                    AND (CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) = FALSE) 

                                        b_Friend_Option_Available = FALSE
                                                    
                                    ELSE

                                        b_Friend_Option_Available = TRUE

                                    ENDIF

                                ENDIF





                            
                                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                                    Set_Up_FinaleConfirmationScreen()

                                    b_DoFinaleConfirmation = TRUE

                                ELSE

                                    IF b_Friend_Option_Available = FALSE
                                    AND b_Secondary_Option_Available = FALSE

                                        //No other option available, just make the standard call.
                                        g_b_Is_This_An_Activity_Call = FALSE
                                        g_b_Is_This_An_Activity_Cancellation_Call = FALSE
                                        g_b_Is_This_A_Secondary_Function_Call = FALSE

                                                                                
                                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE)
                                        AND g_TheContactInvolvedinCall = CHAR_DETONATEPHONE // See bug 942408

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("AppCONTACTS - CHAR_DETONATEPHONE selected but further action blocked by bit set.")
                                                cdPrintnl()   
                                            #endif

                                        ELSE

                                            Do_Call()  

                                        ENDIF

                                    ELSE  //Hitting this clause means either a friend option or a secondary option is available.

                                        Set_Up_Call_Hangout_Secondary_Cancel_Menu()

                                    ENDIF

                                ENDIF


                                /*Commented out 11.12.12 - simplifying this section as the finale contact list is currently not needed but secondary functions now need support in list.
                                IF IS_THIS_CHARACTER_FRIEND_OF_CURRENT_PLAYER_CHAR (g_TheContactInvolvedinCall)
                                AND (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) = FALSE) //See bug 823426 - No hangout option during finale choice list.


                                   
                                      
                                    //IF (HAVE_CHARS_ARRANGED_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) = FALSE)
                                    //AND (HAS_CHAR_ARRANGED_FRIEND_ACTIVITY (g_Cellphone.PhoneOwner) = TRUE)
                                    
                                    //Changed at Sam's request (827347) - Section 1. If a current friend is selected and we can't arrange an activity and cannot cancel an activity either 
                                    //then we go to a normal call.
                                    IF (CAN_CHARS_ARRANGE_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) = FALSE)
                                    AND (CAN_CHARS_CANCEL_FRIEND_ACTIVITY (g_TheContactInvolvedinCall, g_Cellphone.PhoneOwner) = FALSE) 


                                        #if IS_DEBUG_BUILD
                                            cdPrintstring("AppCONTACTS - Unable to ARRANGE and unable to CANCEL activity. Proceeding with normal story call.")
                                            cdPrintnl()
                                        #endif

                                        g_b_Is_This_An_Activity_Call = FALSE
                                        g_b_Is_This_An_Activity_Cancellation_Call = FALSE


                                                        
                                        Do_Call()  

                                    ELSE

                                        //Changed at Sam's request (827347) - Section 2.
                                        //Commented section here was used to prevent any hangout or cancel menu appearing if the player was on a mission. 
                                        //This will be handled by Friend public function checks in Set_Up_Call_Hangout_Secondary_Cancel_Menu procedure .
                                        //IF IS_CURRENTLY_ON_MISSION_TO_TYPE() = FALSE


                                            Set_Up_Call_Hangout_Secondary_Cancel_Menu()


                                        //ELSE

                                            //g_b_Is_This_An_Activity_Call = FALSE
                                            //g_b_Is_This_An_Activity_Cancellation_Call = FALSE

                                                   
                                            //Do_Call()  

                                        //ENDIF

                                    ENDIF

                                ELSE


             

                                    //Finale Choice Confirmation cued if necessary...

                                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)

                               
                                        Set_Up_FinaleConfirmationScreen()

                                        b_DoFinaleConfirmation = TRUE


                                    ELSE



                                        //Set bools to specify this is a story call here!
                                        g_b_Is_This_An_Activity_Call = FALSE
                                        g_b_Is_This_An_Activity_Cancellation_Call = FALSE

                                        Do_Call()

                                    ENDIF


                                ENDIF
                                End of comment out 11.12.12 */




                                




                            ENDIF
                        ENDIF


                    ENDIF //Story / Human delimiter check.

                ENDIF //End.

        ENDIF
    
    ENDIF         
    ENDIF


ENDPROC









PROC Check_For_Numpad_Selection()


    

    IF NOT IS_PED_INJURED (PLAYER_PED_ID())

        //DISABLE_JUMP (PLAYER_ID()) //disable jump every frame so we can use PS3 square button.
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_JUMP)
        
    ENDIF



    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))


            Play_Select_Beep()


            g_InputButtonJustPressed = TRUE



            INT testReturnedSFInt

            //testReturnedSFInt = LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
        
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

            Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
    
                WAIT (0)

                #if IS_DEBUG_BUILD
                    cdPrintstring("appContacts - Waiting on scaleform return during numpad selection.")
                    cdPrintnl()
                #endif

            ENDWHILE

            


            testReturnedSFInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)

                #if IS_DEBUG_BUILD
                    cdPrintstring("RETURNED INT IS ")
                    cdPrintint (testReturnedSFInt)
                    cdPrintnl()
                #endif




            SelectedNumpadIndex = NumpadSlot[testReturnedSFInt]


            //NewStringToAppend[NewStringIndex] = GET_STRING_FROM_TEXT_FILE (NumpadList[SelectedNumpadIndex].NumpadLabel)

            

            NewStringToAppend[0] = GET_FILENAME_FOR_AUDIO_CONVERSATION (NumpadList[SelectedNumpadIndex].NumpadLabel)
            


             
            IF ARE_STRINGS_EQUAL (NewStringToAppend[0], "*") //Redial button has been pressed.
                
                NumpadEntryString = g_Redial_NumpadEntryString[g_Cellphone.PhoneOwner] //Numpad entry string now becomes whatever was previously stored the last time a manual call was made. 
                
                NewStringIndex = GET_LENGTH_OF_LITERAL_STRING (NumpadEntryString) //Update length of current working string to reflect that of newly copied contents.


            ELSE

                IF GET_LENGTH_OF_LITERAL_STRING (NumpadEntryString) < 14  //Maximum phone number length set to 12 digits - will change to a defined constant.

                    NumpadEntryString += NewStringToAppend[0] 

                    NewStringIndex ++

                ENDIF
           
            ENDIF


            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_LITERAL_STRING (SF_MovieIndex, "SET_HEADER", NumpadEntryString)

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_HEADER")
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                    ADD_TEXT_COMPONENT_SUBSTRING_PHONE_NUMBER(NumpadEntryString)
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()

            
      
    ENDIF



        

    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_CELLPHONE_OPTION)) //Delete character from number string
        //Potential conflict with car exit but as this is undocumented worthwhile leaving in for just now...

            Play_Back_Beep()


            g_InputButtonJustPressed = TRUE


            IF GET_LENGTH_OF_LITERAL_STRING (NumpadEntryString) > 0 //Don't truncate a string that's already of zero length...

                //NumpadEntryString = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING (NumpadEntryString, (NewStringIndex - 1))

                NumpadEntryString = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 0,  (NewStringIndex - 1))

                NewStringIndex --
            
            ENDIF

            //Graeme says fix!
            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_LITERAL_STRING (SF_MovieIndex, "SET_HEADER", NumpadEntryString)

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_HEADER")
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                    ADD_TEXT_COMPONENT_SUBSTRING_PHONE_NUMBER(NumpadEntryString)
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()

         

    ENDIF


      


    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Call entered number...

        IF NOT Is_SinglePlayer_Climbing() //See 843727 - Don't allow dial out while on ladder.



                Play_Select_Beep()


                //Copy newly entered string into redial var...
                g_Redial_NumpadEntryString[g_Cellphone.PhoneOwner] = NumpadEntryString


                //Assign TheContactInvolvedInCall to a dummy character in case no match is found to the entered number in the next section...
                //Do this before assigning state to prevent inadvertent return of residual contact. See bug 375586
                g_TheContactInvolvedinCall = CHAR_BLANK_ENTRY

                DialledNumberHasGameworldAssociation = FALSE


                //Set Phone Display State to "attempting to call contact" and update the view state enum with the required data.
                
                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.
            
                    g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT

                    LaunchTransitionMoveUpIfRequired()

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 16. appContacts assigns PDS_ATTEMPTING_TO_CALL_CONTACT and LaunchTransitionMoveUpIfRequired()")
                        cdPrintnl()   
                    #endif


                    
                    IF IS_PLAYER_PLAYING (PLAYER_ID())
                        IF NOT IS_ENTITY_IN_WATER (PLAYER_PED_ID()) //Make sure player doesn't try to put the phone to his ear in SP, as the phone call gets aborted immediately in water. See bug 907295.
                            IF IS_PED_IN_COVER(PLAYER_PED_ID())

                                IF NOT IsCurrentCamFirstPerson() 

                                    Put_Cellphone_To_Ear()  //Having the cellphone beside the ear in cover in first person can cause clipping problems.

                                ENDIF

                            ELSE

                                IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE  //Added for 2038700, this delays first person mode from putting the phone to ear immediately.
                                                                                        //It will go to ear on foot when the cellphone conversation starts from PlayCellphoneConversation()

                                    Put_Cellphone_To_Ear()


                                ENDIF

                            ENDIF
                        ENDIF
                    ENDIF


                ENDIF


      

                //Need to go through list of contacts currently in phonebook and assigned contact involved in call to that contact...
                INT contactListIndex = 0

                BypassGameAssociation = FALSE

                STRING TempComparisonString
               
				INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
                WHILE contactListIndex < iMaxCharacter
					enumCharacterList eContactListCharacter = INT_TO_ENUM (enumCharacterList, contactListIndex)
                    IF (eContactListCharacter) <> CHAR_DIRECTOR //For 2260082, we need to stop the manually dialled phone number picking up the associated contact.
                                                                                            //It needs to go through the DialledNumberHasGameworldAssociation branch to work correctly. 
                    
                        TempComparisonString = GET_FILENAME_FOR_AUDIO_CONVERSATION (g_CharacterSheetNonSaved[contactListIndex].phonebookNumberLabel)
                        
                        IF ARE_STRINGS_EQUAL (TempComparisonString, NumpadEntryString) 

							
                            IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(eContactListCharacter, ENUM_TO_INT (g_Cellphone.PhoneOwner)) = LISTED //make sure that we only search for contacts in a phonebook, not the entire character sheet.
                            OR GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(eContactListCharacter, ENUM_TO_INT (g_Cellphone.PhoneOwner)) = SPECIAL_NUMBERS 
                            OR ARE_STRINGS_EQUAL (TempComparisonString, GET_FILENAME_FOR_AUDIO_CONVERSATION (g_CharacterSheetNonSaved[CHAR_CALL911].phonebookNumberLabel))

                                g_TheContactInvolvedinCall = (eContactListCharacter)

                            ELSE

                                BypassGameAssociation = TRUE //This contact had a matching number to the entry string but is not listed. It may have an acceptable
                                                             //gameworld format, but we don't want to play any generic answerphone, just engaged.

                            ENDIF
                           

                        ENDIF


                    ENDIF


                    //ENDIF

                    contactListIndex ++


                ENDWHILE


                //If number not found... keep contact as blank.
                

                IF g_TheContactInvolvedinCall = CHAR_BLANK_ENTRY //CHAR_BLANK_ENTRY persists, we didn't find a match.
                AND DialledNumberHasGameworldAssociation = FALSE
                AND BypassGameAssociation = FALSE
        
                    //Bug 207898
                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)//(1)
                                  
                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                            ADD_TEXT_COMPONENT_SUBSTRING_PHONE_NUMBER(NumpadEntryString)
                            END_TEXT_COMMAND_SCALEFORM_STRING()
                                                  
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_211")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_195")
                            
                    END_SCALEFORM_MOVIE_METHOD()

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen which can we use as a sending screen for just now...
                    


                        #if IS_DEBUG_BUILD

                            cdPrintstring ("Display View 4 - appContacts 2")
                            cdPrintnl()

                        #endif

                    

                                  
                    //Check for gameworld answerphone message.
                    DialledNumberHasGameworldAssociation = FALSE


                    INT gwNumIndex = 0

                    WHILE gwNumIndex < (ENUM_TO_INT(MAX_GAMEWORLD_NUMBERS))  

                        STRING TempGWComparisonString

                        TempGWComparisonString = GET_FILENAME_FOR_AUDIO_CONVERSATION (g_GameworldNumber[gwNumIndex].Gameworld_DialledNumberLabel)
                        
                        IF ARE_STRINGS_EQUAL (TempGWComparisonString, NumpadEntryString) 

                            DialledNumberHasGameworldAssociation = TRUE
                            GameworldAssociatedRoot = g_GameworldNumber[gwNumIndex].Gameworld_AnsphoneLabelRoot                       
                            GameworldAssociatedVoice = g_GameworldNumber[gwNumIndex].Gameworld_VoiceID
                              
                        ENDIF
                    

                        gwNumIndex ++

                    ENDWHILE



                    //Also check for Numpad entry string formats here to play random messages if no gameworld asssociation has been found. See bug 913635.
                    IF DialledNumberHasGameworldAssociation = FALSE
                    
                        NumpadEntryStringLength = GET_LENGTH_OF_LITERAL_STRING (NumpadEntryString)

                        #if IS_DEBUG_BUILD
                        
                            PRINTSTRING("AppCONTACTS - Numpad Entry string length was...")
                            PRINTINT(NumpadEntryStringLength)
                            PRINTNL()
                            PRINTSTRING(NumpadEntryString)
                            PRINTNL()

                        #endif

                        IF NumpadEntryStringLength > 6 //only check for number format if string is seven characters or more...
                    
                        


                                singleCharacter[0] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 0, 1)
                                singleCharacter[1] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 1, 2)
                                singleCharacter[2] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 2, 3)
                                singleCharacter[3] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 3, 4)
                                
                                singleCharacter[4] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 4, 5)
                                singleCharacter[5] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 5, 6)
                                                             

                                    //Check for 1-999 format.
                                    IF ARE_STRINGS_EQUAL (singleCharacter[0], "1")
                                    AND ARE_STRINGS_EQUAL (singleCharacter[1], "9")
                                    AND ARE_STRINGS_EQUAL (singleCharacter[2], "9")
                                    AND ARE_STRINGS_EQUAL (singleCharacter[3], "9")
                                        
                                        
                                        #if IS_DEBUG_BUILD

                                      
                                            PRINTSTRING ("1-999 format identified. Grabbed two decision characters that occur after 9.")
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[4])
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[5])
                                            PRINTNL()
                                            
                                        #endif


                                        INT DecisionInt1, DecisionInt2

                                                                    
                                        //Converts the text labels into an by returnin an int from large string comparison lookup table.
                                        DecisionInt1 = ConvertSingleCharacter(singleCharacter[4])
                                        DecisionInt2 = ConvertSingleCharacter(singleCharacter[5])


                                        //Root and voices assigned via this.
                                        Pass_Decision_Characters_Into_Algorithm (DecisionInt1, DecisionInt2)

                                        DialledNumberHasGameworldAssociation = TRUE
                                      


                                    ENDIF //End of Check for 1-999





                                IF DialledNumberHasGameworldAssociation = FALSE //No 1999 number was found above...

                                //Check for format xxx_555_01xx        
                        
                                    IF NumpadEntryStringLength = 10
                                    OR NumpadEntryStringLength = 7




                                        IF NumpadEntryStringLength = 10

                                            singleCharacter[0] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 3, 4)
                                            singleCharacter[1] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 4, 5)
                                            singleCharacter[2] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 5, 6)
                                            singleCharacter[3] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 6, 7)
                                            singleCharacter[4] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 7, 8)
                                            
                                            singleCharacter[5] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 8, 9)
                                            singleCharacter[6] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 9, 10)

                                        ELSE //Must be 7.

                                            singleCharacter[0] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 0, 1)
                                            singleCharacter[1] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 1, 2)
                                            singleCharacter[2] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 2, 3)
                                            singleCharacter[3] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 3, 4)
                                            singleCharacter[4] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 4, 5)
                                            
                                            singleCharacter[5] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 5, 6)
                                            singleCharacter[6] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (NumpadEntryString, 6, 7)
            
                                        ENDIF    
                                            
                                                 
                                        #if IS_DEBUG_BUILD
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[0])
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[1])
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[2])
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[3])
                                            PRINTNL()
                                            PRINTSTRING (singleCharacter[4])
                                            PRINTNL()
                                        #endif
                                        

                                        IF ARE_STRINGS_EQUAL (singleCharacter[0], "5")
                                        AND ARE_STRINGS_EQUAL (singleCharacter[1], "5")
                                        AND ARE_STRINGS_EQUAL (singleCharacter[2], "5")
                                        AND ARE_STRINGS_EQUAL (singleCharacter[3], "0")
                                        AND ARE_STRINGS_EQUAL (singleCharacter[4], "1")


                                            #if IS_DEBUG_BUILD

                                                IF NumpadEntryStringLength = 10
                                                    PRINTNL()
                                                    PRINTSTRING ("xxx_555_01xx area code format identified. Grabbed last two decision characters")
                                                ELSE
                                                    PRINTSTRING ("555_01xx no area code format identified. Grabbed last two decision characters")
                                                ENDIF
                                                PRINTNL()
                                                PRINTSTRING (singleCharacter[5])
                                                PRINTNL()
                                                PRINTSTRING (singleCharacter[6])
                                                PRINTNL()
                                                
                                            #endif


                                            INT DecisionInt1, DecisionInt2

                                                                        
                                            //Converts the text labels into an by returnin an int from large string comparison lookup table.
                                            DecisionInt1 = ConvertSingleCharacter(singleCharacter[5])
                                            DecisionInt2 = ConvertSingleCharacter(singleCharacter[6])


                                            //Root and voices assigned via this.
                                            Pass_Decision_Characters_Into_Algorithm (DecisionInt1, DecisionInt2)

                                            DialledNumberHasGameworldAssociation = TRUE
                                          


                                        ENDIF //End of Check for format xxx_555_01xx, 555_01xx 

                                    ENDIF               

                            ENDIF   




                            





 
                        ENDIF

                    
                    ENDIF 







            

                ELSE

                    //We must have found a match for the number in the contacts list.

                          

                    //If the chosen contact is a special number, e.g 911, classify it as a known caller so as to display the full information nn the phone calling screen.
                    IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(g_TheContactInvolvedinCall, ENUM_TO_INT (g_Cellphone.PhoneOwner))= SPECIAL_NUMBERS 
                    OR g_TheContactInvolvedinCall = CHAR_CALL911
                                        
						GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner), KNOWN_CALLER)

                        IF g_TheContactInvolvedinCall = CHAR_CALL911

                            IF NOT (GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_CALL911, 0) = LISTED)    //If 911 is already listed for Michael then it has been added once already
                                
                                //IF g_bInMultiplayer 
                            
                                    //ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, ALL_OWNERS_BOOKS, FALSE) //Once you get through to 911, add this contact as Emergency Services # 1409265
                            
                                    //ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK, TRUE)

                                //ELSE

                                    ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, ALL_OWNERS_BOOKS) //Once you get through to 911, add this contact as Emergency Services # 1409265
                            
                                    ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK, FALSE)

                                //ENDIF                               

                            
                            ENDIF

                            
                            IF g_bInMultiplayer

                                IF NOT IS_CONTACT_IN_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK)

                                    ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK, TRUE)

                                ENDIF

                            ENDIF

                        ENDIF

                    
                    ENDIF   




                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, 
                            "CELL_300",
                            "CELL_211", 
                            "CELL_195")   

                    ELSE


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                            "CELL_211",

                            //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label) 


                    ENDIF



      



          
                ENDIF



                Update_Scaleform_VSE ()


                #if IS_DEBUG_BUILD 
            
                    cdPrintnl()
                    cdPrintstring ("appCONTACTS_2 - Attempting to call contact with label: ")
                    cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                    cdPrintstring (" which is charsheet entry ")
                    cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
                    cdPrintnl()

                #endif



          ENDIF


    
      ENDIF




ENDPROC







PROC PLAYER_CALL_CHAR_CELLPHONE_DECISION (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)


//    IF IS_CURRENT_MISSION_DLC()
//
//        #if IS_DEBUG_BUILD
//
//            cdPrintnl()
//            cdPrintstring ("AppContacts - making decision on which answerphone PLAYER_CALL routine to use... Ongoing DLC mission detected. Using V Content in DLC variant.")
//            cdPrintnl()
//
//        #endif
//
//        //If a dlc mission is ongoing we need to make sure that the answerphone still points to legacy contact dialogue in american.rpf rather americanDLC.rpf
//        PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)
//        
//    ELSE








        IF g_bInMultiplayer
        OR IS_FAKE_MULTIPLAYER_MODE_SET()


            IF g_TheContactInvolvedinCall = CHAR_BENNYS_OMW //Lowrider Benny Answerphone message    2547521


                IF g_bUse_MP_DLC_Dialogue = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("AppContacts - ANSWERPHONE g_TheContactInvolvedinCall 2 = CHAR_BENNYS_OMW but g_bUse_MP_DLC_Dialogue was FALSE. Setting to TRUE. It probably should be.")
                        cdPrintnl()

                    #endif

                    g_bUse_MP_DLC_Dialogue = TRUE //This needs to be true to point the dialogue handler to americanDLC.rpf

                ENDIF

                //Note that WhichBlockOfTextToLoad is used here as Benny does not use the standard answerphone block.
                PLAYER_CALL_CHAR_CELLPHONE (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)


            ELSE


                IF g_bUse_MP_DLC_Dialogue = TRUE
            
                
                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("AppContacts - making decision on which answerphone PLAYER_CALL routine to use... Ongoing MP DLC mission detected. Using V Content in DLC variant in order to play legacy answerphone message.")
                        cdPrintnl()

                    #endif


                    UNUSED_PARAMETER(WhichBlockOfTextToLoad)

                    //If a dlc mission is ongoing we need to make sure that the answerphone still points to legacy contact dialogue in american.rpf rather americanDLC.rpf
                    PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (YourPedStruct, WhichChar, answerPhoneBlock, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)
                

                ELSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("AppContacts - making decision on which MP answerphone PLAYER_CALL routine to use... Using legacy V Content.")
                        cdPrintnl()
                
                    #endif
                
                    UNUSED_PARAMETER(WhichBlockOfTextToLoad)

                    PLAYER_CALL_CHAR_CELLPHONE (YourPedStruct, WhichChar, answerPhoneBlock, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)

                ENDIF


            ENDIF

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("AppContacts - making decision on which answerphone PLAYER_CALL routine to use... Using legacy V Content.")
                cdPrintnl()
        
            #endif
        
            UNUSED_PARAMETER(WhichBlockOfTextToLoad)

            PLAYER_CALL_CHAR_CELLPHONE (YourPedStruct, WhichChar, answerPhoneBlock, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)


        ENDIF


//    ENDIF

ENDPROC











PROC Check_For_Answerphone_Timeout()


    SWITCH cc_Progress

    
    CASE CC_LAUNCH_TIMER_AND_CHECK_MESSAGE

        //check for answerphone message availability here...

        //launch timer and set pause length depending on whether or not an ans message is available.

        //If gameworld answerphone label existed, get that root data in here otherwise do below. Also need to get voice id for answerphone message in there too.



        IF g_TheContactInvolvedinCall = CHAR_BENNYS_OMW //Lowrider Benny Answerphone message    2547521
                           
            //Lowrider Benny Answerphone message    2547521
            /*
            [LOWCA_CALL10_1:LOWCAAU]
            ~z~This is Benny. You want something, come see me at the shop. 
            [LOWCA_CALL10_3:LOWCAAU]
            ~z~You don't want something, then why you calling?
            */

            IF g_bUse_MP_DLC_Dialogue = FALSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("AppContacts - ANSWERPHONE g_TheContactInvolvedinCall 1 = CHAR_BENNYS_OMW but g_bUse_MP_DLC_Dialogue was FALSE. Setting to TRUE. It probably should be.")
                    cdPrintnl()

                #endif

                g_bUse_MP_DLC_Dialogue = TRUE //This needs to be true to point the dialogue handler to americanDLC.rpf

            ENDIF

            IF DOES_TEXT_BLOCK_EXIST ("LOWCAAU")

                REQUEST_ADDITIONAL_TEXT_FOR_DLC ("LOWCAAU", DLC_AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (DLC_AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                    WAIT(0)
                ENDWHILE
        
            ENDIF

            Check_AnswerphoneLabel = "LOWCA_CALL10_1"   //Benny's answerphone message from the Lowrider pack's americanNetDialogue.txt which is constructed into an americandlc.rpf
                    
        ELSE

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("AppCONTACTS - Check_For_Answerphone_Timeout() - Requesting standard answerPhoneBlock.")
                cdPrintnl()   
            #endif

            IF DOES_TEXT_BLOCK_EXIST (answerPhoneBlock)
                
                REQUEST_ADDITIONAL_TEXT (answerPhoneBlock, PHONE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (PHONE_TEXT_SLOT)  //the permanent global from the temporary holder.
                    WAIT(0)
                ENDWHILE

            ENDIF
        
            Check_AnswerphoneLabel = g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].ansphone_labelroot
            Check_AnswerphoneLabel += "_1"

        ENDIF        


        IF DOES_TEXT_LABEL_EXIST (Check_AnswerPhoneLabel)
        OR DialledNumberHasGameworldAssociation = TRUE


            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("AppCONTACTS - Check_For_Answerphone_Timeout() - Found Answerphone text label or DialledNumber has GW association.")
                cdPrintnl()   
            #endif  



  
            Connection_PauseLength = Connection_AnsMsgPauseLength //Answerphone message does exist so we set the pause length to be whatever the AnsMsgPauseLength is.

            IF IS_CALLING_CONTACT (CHAR_DETONATEPHONE) //Special Case for Brenda's TVBankIntro.sc

                Connection_PauseLength = g_TVStockMarketIntro_CellphoneTimer //Brenda specifies this in singleplayer/include/globals/mission_globals.sch

            ENDIF


        ELSE 

            //It's possible to alter the pause between ringing and engaged here if need be..
            //Connection_EngagedPauseLength = 10000

            Connection_PauseLength = Connection_EngagedPauseLength  //No answerphone available so we will elect to play the engaged tone after the defined pause if no flow script grabs the attempt to call the contact. 

        ENDIF
      
        IF DialledNumberHasGameworldAssociation = TRUE
        AND ARE_STRINGS_EQUAL (GameworldAssociatedRoot, "CELLPHONE_CHEAT")
        AND g_bInMultiPlayer = FALSE
    

            //We need to check if this cheat is  a PS3 to PS4 / X360 to XBONE bonus.

            //Look up list, previously defined in charsheet_public.sch
            IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SEAPLANE")
            OR ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DUELC")
            OR ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_BUBBLE")

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("AppCONTACTS - b_ThisIsLastGenBonusCheat now TRUE - matched CHEAT type to list.")
                    cdPrintnl()   
                #endif  
        
                b_ThisIsLastGenBonusCheat = TRUE    

            ENDIF


            
            IF b_ThisIsLastGenBonusCheat

                IF IS_LAST_GEN_PLAYER()
                //AND g_DumpDisableEveryFrameCaller = FALSE // - Comment in to test.


                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("AppCONTACTS - b_ThisIsLastGenBonusCheat is TRUE - player has last gen privileges. Proceeding.")
                        cdPrintnl()   
                    #endif  
            
                    b_ThisCheatBlocked = FALSE

                ELSE
                    
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("AppCONTACTS - b_ThisIsLastGenBonusCheat is TRUE but this player does not have last gen privileges. Blocking cheat.")
                        cdPrintnl() 
                    #endif  

                    b_ThisCheatBlocked = TRUE

                ENDIF

            ENDIF


            
            IF b_ThisCheatBlocked = FALSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("AppCONTACTS - SP Initial check says GameworldAssociatedRoot was CELLPHONE_CHEAT - no block in place.")
                    cdPrintnl()   
                #endif

                Connection_AttemptStartTime = GET_GAME_TIMER()


                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)//(1)
                                  
                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                            ADD_TEXT_COMPONENT_SUBSTRING_PHONE_NUMBER(NumpadEntryString)
                            END_TEXT_COMMAND_SCALEFORM_STRING()
                                                  
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300")

                            IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_MONOC")

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_MONOSIGNAL")
                            
                            ELSE

                                IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DIRECTOR") //2172238  - Display "Director Mode" on cellphone call screen when 1-999-LSTALENT is entered.
                                    //IF IS_PC_VERSION() //Opening up director mode entry from phone for 2260082

                                        #IF FEATURE_SP_DLC_DIRECTOR_MODE
                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_DRCT") //Will display "Unknown" on other platforms if the cheat number is entered.
                                        #ENDIF

                                        #IF NOT FEATURE_SP_DLC_DIRECTOR_MODE
                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_CHEAT") 
                                        #ENDIF

                                    //ENDIF
                                ELSE
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_CHEAT")  //Display "Cheat Processing" on cellphone call screen.
                                ENDIF

                            ENDIF

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_195")
                            
                    END_SCALEFORM_MOVIE_METHOD()

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)


                cc_PROGRESS = CC_CHEAT_PAUSE



            ELSE
            

                cc_Progress = CC_CONNECTION_PAUSE

                Connection_AttemptStartTime = GET_GAME_TIMER()

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppCONTACTS - Aborting Cheat as cheat if blocked. Last gen privileges probably.")
                    cdPrintnl()  
                    PRINTSTRING("Last gen privilege fail. Waiting for call connection...")
                    PRINTNL()

                #endif

            ENDIF


      
        ELSE

            cc_Progress = CC_CONNECTION_PAUSE

            Connection_AttemptStartTime = GET_GAME_TIMER()

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppCONTACTS - Waiting for call connection...")
                cdPrintnl()  
                PRINTSTRING("Waiting for call connection...")
                PRINTNL()

            #endif

        ENDIF

    BREAK



    CASE CC_CHEAT_PAUSE


        Connection_AttemptCurrentTime = GET_GAME_TIMER()


        IF Connection_AttemptCurrentTime - Connection_AttemptStartTime > 1500 //

            cc_Progress = CC_PLAY_ANSWERPHONE  //Skip any connection delays and go straight to processing...

        ENDIF


    BREAK



    CASE CC_CONNECTION_PAUSE

        Connection_AttemptCurrentTime = GET_GAME_TIMER()
        
        //Loop remote connection tone...
        

        IF Connection_PauseLength <> Connection_EngagedPauseLength
       
            IF HAS_SOUND_FINISHED (Phone_Dialling_SoundID)
                IF voiceSession.bConnectedCallOnGoing = FALSE
                   IF g_Use_Prologue_Cellphone

                        PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "Dial_and_Remote_Ring", g_Owner_Soundset)
                    ELSE

                        //PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "PHONE_GENERIC_REMOTE_RING_01")
                        
                        IF Connection_AttemptCurrentTime - Connection_AttemptStartTime > 1700 //#1543287
                            PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "Remote_Ring", g_Owner_Soundset)
                        ENDIF

                    ENDIF
                ENDIF
            ENDIF

        ELSE
        
            IF IS_CALLING_CONTACT (CHAR_CALL911)   //Need to play the remote ring for the 911 call as a special case. It may be a few seconds before it is grabbed, so there's a danger of silence until a grab is made.
            OR IS_CALLING_CONTACT (CHAR_CHAT_CALL) //Same as above for a human to human chat call. The remote ring will never trigger as it isn't truly grabbed.
            OR IS_CALLING_CONTACT (CHAR_DETONATEBOMB)

                IF HAS_SOUND_FINISHED (Phone_Dialling_SoundID)
                    IF voiceSession.bConnectedCallOnGoing = FALSE                                                              
                        IF g_Use_Prologue_Cellphone

                            PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "Dial_and_Remote_Ring", g_Owner_Soundset)
                        ELSE
                                                                                                                             
                            //PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "PHONE_GENERIC_REMOTE_RING_01")
                            IF Connection_AttemptCurrentTime - Connection_AttemptStartTime > 1700 //#1543287
                                PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "Remote_Ring", g_Owner_Soundset)
                            ENDIF

                        ENDIF
                    ENDIF
                ENDIF

            ENDIF

        ENDIF


        IF (Connection_AttemptCurrentTime - Connection_AttemptStartTime) > Connection_PauseLength
     

            IF Connection_PauseLength = Connection_EngagedPauseLength //as the connection pause was equal to the defined engaged pause length then we know that it's an engaged tone we 
                                                                      //want to play if the "grab time" runs out. We didn't find an answerphone message above.


                b_EngagedBypassCheck = FALSE

                IF g_TheContactInvolvedinCall = CHAR_CALL911
                OR g_TheContactInvolvedinCall = CHAR_CHAT_CALL 
                OR g_TheContactInvolvedinCall = CHAR_DETONATEBOMB

                    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED))
                                    
                        b_EngagedBypassCheck = TRUE

                        //Trial addition 29.03.13 to make #1136714 bomb out after ten seconds if emergencycall script hasn't picked up the call.
                        IF ((Connection_AttemptCurrentTime - Connection_AttemptStartTime) > ( Connection_PauseLength  + 10000 ))

                            IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
                                IF g_TheContactInvolvedinCall = CHAR_CALL911

                                    #if IS_DEBUG_BUILD
                                        cdPrintstring("appContacts - STATE_ASSIGNMENT 78777 - Was in attempting to call contact state for 911 but timed out!")
                                        cdPrintstring("appContacts - STATE_ASSIGNMENT 78777 - Setting phone to PDS_AWAY.")
                                        cdPrintnl()
                                    #endif

                                    
                                    g_Cellphone.PhoneDS = PDS_AWAY

                                ENDIF
                            ENDIF

                        ENDIF //end of trial addition.

                    ENDIF

                ENDIF


                IF NOT b_EngagedBypassCheck //Gives the 911 call and the MP chat call unlimited time to grab. It doesn't have an answerphone message, but we don't want it to be engaged.
           
                    cc_Progress = CC_ENGAGED
                    
                    STOP_SOUND (Phone_Dialling_SoundID)

                    b_TrevorEngagedCurseSetup = FALSE 

                    SETTIMERB(0) //Initialise pause for Trevor curse to cue.

                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 

                        IF g_TheContactInvolvedinCall = CHAR_CHAT_CALL

                                 
                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
                                    
                                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~
                                        
                                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)
                                    
                                    END_TEXT_COMMAND_SCALEFORM_STRING()                        

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_220") //"BUSY"



                                

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //This field is invisible, so feel free to duplicate with previous text label.


                             END_SCALEFORM_MOVIE_METHOD ()

                        ELSE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(5)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, 
                                "CELL_300",
                                "CELL_220",
                                "CELL_195")   

                        ENDIF


                    ELSE
                    
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(5)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                            "CELL_220",

                            //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label) 

                    
                    ENDIF
                    
                    
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4) //Set enum state to call screen and update any changes




                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("Connection time elapsed. No grab from flow. No ansphone msg. Phone set to engaged...")
                        PRINTNL()
                        cdPrintstring ("Display View 4 - appContacts 3")
                        cdPrintnl()

                    #endif

                ENDIF

               
            ELSE
            
                cc_Progress = CC_PLAY_ANSWERPHONE    //not equal to the engaged pause length so we must have found a answerphone message in the above segment.

                #IF IS_DEBUG_BUILD

                    PRINTSTRING ("Playing answerphone call...")
                    PRINTNL()

                #endif




                 
            ENDIF
        
        ENDIF   

      
    BREAK


    CASE CC_PLAY_ANSWERPHONE
    
        #if IS_DEBUG_BUILD

            cdPRINTSTRING ("Cue answerphone message via dialogue handler. Terminating appContacts after call starts")
            cdPRINTNL()
            PRINTSTRING ("Cue answerphone message via dialogue handler. Terminating appContacts after call starts")
            PRINTNL()
            
        #endif
        
        //We need to get the correct label to display so we get the enum of the selected contact and pass that in along with the audio block and a reference to the 
        //answerphone message for that contact defined in charsheet_initial.sc
        //NB! Note that I'm not forcing the phone into hand. I'm altering the default behaviour since the player must already have the phone in hand as he has proactively called a contact.
        


        //Check if this a gameworld number answerphone message or a contact.

        IF DialledNumberHasGameworldAssociation = TRUE


            IF ARE_STRINGS_EQUAL (GameworldAssociatedRoot, "CELLPHONE_CHEAT")  //Check if this is a cheat type association that has been recognised from the number entry string.
            AND g_bInMultiPlayer = FALSE //No cellphone cheats in MP!

                //These are set up in Fill_Gameworld_Numbers within charsheet_public.
                //For cellphone_cheats, the root should always be set up as "CELLPHONE_CHEAT", then the voice as unique to that cheat.
               
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_MONOC")

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("Monochrome Test 1 recognised!")
                            cdPrintnl()

                        #endif
                        

                        IF g_IsMonochromeCheatActive = FALSE

                            g_IsMonochromeCheatActive = TRUE

                            //Set theme to grey, can be overridden by the user simply selecting a new theme from settings.
                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer = 5 //grey
                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer = 5 //grey
                            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer = 5 //grey

                            
                            IF IS_PLAYER_PLAYING (PLAYER_ID())

                                IF NOT (IS_PED_IN_ANY_VEHICLE (PLAYER_PED_ID(), TRUE))
                                    IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0)

                                        ADD_EXPLOSION(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, 2.0>>, EXP_TAG_ROCKET, 0.1)
                                
                                    ENDIF
                                ENDIF
                                
                            ENDIF
                            

                        ELSE

                            g_IsMonochromeCheatActive = FALSE

                        ENDIF


                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SUPJUM")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_SUPJUM phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SUPER_JUMP)     
                        
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_HEALA")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_HEALA phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_GIVE_HEALTH_ARMOR)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SLIDEYC")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_SLIDEYC phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SLIDEY_CARS)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_FASTR")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_FASTR phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_FAST_RUN)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_WANTDN")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_WANTDN phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_WANTED_LEVEL_DOWN)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_WANTUP")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_WANTUP phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_WANTED_LEVEL_UP)     
                    ENDIF

                    

                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_WEATHER")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_WEATHER phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_ADVANCE_WEATHER)     
                    ENDIF


                                  
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_FASTS")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_FASTS phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_FAST_SWIM)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_POWER")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_POWER phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_CHUTE")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_CHUTE phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_GIVE_PARACHUTE)     
                    ENDIF

                    

                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_BANG")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_BANG phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_BANG_BANG)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_FLAMB")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_FLAMB phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_FLAMING_BULLETS)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_EXPMEL")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_EXPMEL phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_EXPLOSIVE_MELEE)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_ZEROG")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_ZEROG phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_0_GRAVITY)     
                    ENDIF
                    

                    
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_INVINC")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_INVINC phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_INVINCIBILITY)     
                    ENDIF




                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SLOWMO")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_SLOWMO phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SLOWMO)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SKYFA")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_SKYFA phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SKYFALL)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DRUNK")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_DRUNK phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_DRUNK)     
                    ENDIF
                    

                    
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DEADEYE")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_DEADEYE phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_AIM_SLOWMO)     
                    ENDIF



                    //Added for 2066607
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_WEAPONS")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_WEAPONS phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_GIVE_WEAPONS)     
                    ENDIF





                    //Vehicle Spawning section.     
                    
                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_BUZZARD")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_BUZZARD phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, BUZZARD)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_COMET2")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_COMET2 phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, COMET2)     
                    ENDIF

                            


                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_BMX")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_BMX phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, BMX)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_CADDY")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_CADDY phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, CADDY)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DUSTER")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_DUSTER phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, DUSTER)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_PCJ")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_PCJ phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, PCJ)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_RAPIDGT")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_RAPIDGT phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, RAPIDGT)     
                    ENDIF




                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_STRETCH")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_STRETCH phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, STRETCH)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_STUNT")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_STUNT phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, STUNT)     
                    ENDIF



                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_TRASH")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_TRASH phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, TRASH)     
                    ENDIF


                    IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SANCHEZ")
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CHEAT_SANCHEZ phone cheat recognised!")
                            cdPrintnl()
                        #endif
                        //From cheat_controller_public.sch
                        TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, SANCHEZ)
                    ENDIF

                    //Current Gen to Next Gen bonus vehicle Spawning section.

                    IF IS_LAST_GEN_PLAYER() //
                    //AND g_DumpDisableEveryFrameCaller = FALSE // - Comment in to test.
                            
                        IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_SEAPLANE")
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("Last Gen Player CHEAT_SEAPLANE phone cheat recognised!")
                                cdPrintnl()
                            #endif
                            //From cheat_controller_public.sch
                            TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, DODO)     
                        ENDIF



                        IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DUELC")
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("Last Gen Player CHEAT_DUELC phone cheat recognised!")
                                cdPrintnl()
                            #endif
                            //From cheat_controller_public.sch
                            TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, DUKES2)     
                        ENDIF



                        IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_BUBBLE")
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("Last Gen Player CHEAT_BUBBLE phone cheat recognised!")
                                cdPrintnl()
                            #endif
                            //From cheat_controller_public.sch
                            TRIGGER_CHEAT (CHEAT_TYPE_SPAWN_VEHICLE, SUBMERSIBLE2)     
                        ENDIF

                    ENDIF


                    //2172238 - Enter Director Mode. Was PC only...
                    //IF IS_PC_VERSION() //Opening up director mode entry from phone for 2260082

                        IF ARE_STRINGS_EQUAL (GameworldAssociatedVoice, "CHEAT_DIRECTOR")

                            #IF FEATURE_SP_DLC_DIRECTOR_MODE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CHEAT_DIRECTOR phone cheat recognised on PC and Console. Entering director mode...")
                                    cdPrintnl()
                                #endif

                                FEATURE_BLOCK_REASON eBlockReason
                                eBlockReason = GET_FEATURE_BLOCKED_REASON(DEFAULT, DEFAULT, DEBUG_DIRECTOR)
                                IF eBlockReason != FBR_NONE
                                    CPRINTLN(DEBUG_DIRECTOR, "Director Mode could not launch with warning screen. Block condition ", DEBUG_GET_FEATURE_BLOCK_REASON_STRING(eBlockReason,DEBUG_DIRECTOR), " triggered during the process.")
                                    DO_DIRECTOR_MODE_WARNING_SCREEN_WAIT(eBlockReason)
                                ELSE
                                    DO_DIRECTOR_MODE_ACCEPT_SCREEN_WAIT() 
                                ENDIF
                                                                
                            #ENDIF

                        ENDIF
                        
                        
                        
                    //ENDIF

                                               
                    //Critical! Force the phone away after activating the cheat! Do not remove! Submit check 37 kookoko
              
                    HANG_UP_AND_PUT_AWAY_PHONE()




           

            ELSE

                IF ARE_STRINGS_EQUAL (GameworldAssociatedRoot, "CELLPHONE_CHEAT")

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("AppContacts - Cellpone Cheats are inactive in MP - hanging up phone as a precaution.")
                        cdPrintnl()

                    #endif


                    HANG_UP_AND_PUT_AWAY_PHONE()


                ELSE



                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("AppContacts - DialledNumberHasGameworldAssociation = TRUE answerphone message commencing.")
                        cdPrintnl()

                    #endif


                    ADD_PED_FOR_DIALOGUE (AppContactPedStruct, 1, NULL, GameworldAssociatedVoice)
            

                    //Pause answering time needs set to zero here as we've already waited a believable time for the answerphone to "pick up"...
                    g_Outgoing_Call_Pause_Answering_Time = 0
                
                
    //            IF IS_CURRENT_MISSION_DLC()
    //            
    //                PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, GameworldAssociatedRoot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
    //            
    //            ELSE
                
                    PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, GameworldAssociatedRoot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

    //            ENDIF
                      
                    

                ENDIF

            ENDIF


        ELSE





            TEXT_LABEL_23 VoiceID_Decision
                                            

            //Lowrider Benny Answerphone message    2547521
            /*
            [LOWCA_CALL10_1:LOWCAAU]
            ~z~This is Benny. You want something, come see me at the shop. 
            [LOWCA_CALL10_3:LOWCAAU]
            ~z~You don't want something, then why you calling?
            */
            



            SWITCH g_TheContactInvolvedinCall

                
                //Lowrider Benny Answerphone message    2547521
                CASE CHAR_BENNYS_OMW
                    VoiceID_Decision = "BENNY"
                BREAK


                CASE CHAR_MICHAEL
                    VoiceID_Decision = "MICHAEL"
                BREAK


                CASE CHAR_TREVOR
                    VoiceID_Decision = "TREVOR"
                BREAK


                CASE CHAR_FRANKLIN
                    VoiceID_Decision = "FRANKLIN"
                BREAK


                CASE CHAR_LESTER
                    VoiceID_Decision = "LESTER"
                BREAK


                CASE CHAR_LESTER_DEATHWISH //2023337 - Doesn't look like it's recorded.
                    VoiceID_Decision = "LESTER"
                BREAK

                                
                CASE CHAR_TRACEY
                    VoiceID_Decision = "TRACEY"
                BREAK

                
                CASE CHAR_LAMAR
                    VoiceID_Decision = "LAMAR"
                BREAK

           
                CASE CHAR_AMANDA
                    VoiceID_Decision = "AMANDA"
                BREAK


                CASE CHAR_MOLLY
                    VoiceID_Decision = "MALEEXECASSIST"
                BREAK


                CASE CHAR_DEVIN
                    VoiceID_Decision = "FEMEXECASSIST"
                BREAK



                
                CASE CHAR_CHENGSR
                    VoiceID_Decision = "CHENGSR"
                BREAK


                CASE CHAR_JIMMY
                    VoiceID_Decision = "JIMMY"
                BREAK


                CASE CHAR_TOW_TONYA
                    VoiceID_Decision = "TONYA"
                BREAK


                CASE CHAR_BEVERLY
                    VoiceID_Decision = "BEVERLY"
                BREAK


                CASE CHAR_NIGEL
                    VoiceID_Decision = "NIGEL"
                BREAK


                CASE CHAR_OSCAR
                    VoiceID_Decision = "OSCAR"
                BREAK

                
                CASE CHAR_BARRY 
                    VoiceID_Decision = "BARRY"
                BREAK

                
                CASE CHAR_GAYMILITARY
                    VoiceID_Decision = "GAYMILITARY"
                BREAK

                
                CASE CHAR_RON
                    VoiceID_Decision = "NervousRon"
                BREAK

                
                CASE CHAR_WADE 
                    VoiceID_Decision = "Wade"
                BREAK


                CASE CHAR_OMEGA
                    VoiceID_Decision = "OMEGA"
                BREAK


                CASE CHAR_MARNIE
                    VoiceID_Decision = "MARNIE"
                BREAK


                CASE CHAR_STRETCH
                    VoiceID_Decision = "STRETCH"
                BREAK


                CASE CHAR_STEVE
                    VoiceID_Decision = "STEVE"
                BREAK

                
                CASE CHAR_CRIS
                    VoiceID_Decision = "CRIS"
                BREAK


                CASE CHAR_DAVE
                    VoiceID_Decision = "Dave"
                BREAK

                
                CASE CHAR_JOSH
                    VoiceID_Decision = "JOSH"
                BREAK


                CASE CHAR_DR_FRIEDLANDER
                    VoiceID_Decision = "FRIEDLANDER"
                BREAK


                CASE CHAR_MAUDE
                    VoiceID_Decision = "MAUDE"
                BREAK

            
                CASE CHAR_JIMMY_BOSTON
                    VoiceID_Decision = "JIMMYBOSTON"
                BREAK


                CASE CHAR_JOE
                    VoiceID_Decision = "JOE"
                BREAK


                CASE CHAR_SOLOMON
                    VoiceID_Decision = "SOLOMON"
                BREAK


                CASE CHAR_HUNTER    
                    VoiceID_Decision = "CLETUS"
                BREAK


                CASE CHAR_TANISHA
                    VoiceID_Decision = "TANISHA"
                BREAK


                CASE CHAR_ABIGAIL
                    VoiceID_Decision = "ABIGAIL"
                BREAK
    

                CASE CHAR_DENISE
                    VoiceID_Decision = "DENISE"
                BREAK
                
                //CDM- Added 19/6/13
                CASE CHAR_MP_BRUCIE
                     VoiceID_Decision = "BRUCIE"
                BREAK
                
                CASE CHAR_MP_BIKER_BOSS
                     VoiceID_Decision = "BikerBoss"
                BREAK
                
                CASE CHAR_MP_MEX_BOSS
                     VoiceID_Decision = "MexicanBoss"
                BREAK
                
                CASE CHAR_MP_MECHANIC
                    VoiceID_Decision = "MECHANIC"
                BREAK
                

                CASE CHAR_MP_MORS_MUTUAL
                    VoiceID_Decision = "MorsMutual"
                BREAK

                CASE CHAR_MP_MERRYWEATHER
                    VoiceID_Decision = "FM_MERRYWEATHER"
                BREAK
                
                CASE CHAR_PEGASUS_DELIVERY
                    VoiceID_Decision = "PEGASUS"
                BREAK
                
                CASE CHAR_SIMEON
                    VoiceID_Decision = "SIMEON"
                BREAK
                
                CASE CHAR_MARTIN
                    VoiceID_Decision = "MARTIN"
                BREAK
                
                CASE CHAR_DOM
                    VoiceID_Decision = "DOM"
                BREAK

                /* Stub for Tom, the Epsilon guy, doesn't appear to get added to the phone.
                CHAR_TOM
                    VoiceID_Decision = "TOM"
                BREAK
                */
                
                CASE CHAR_MP_GERALD
                    VoiceID_Decision = "GERALD"
                BREAK

                DEFAULT

                    //Get generic answer tone for here...
                    VoiceID_Decision = "MICHAEL"

                BREAK


            ENDSWITCH

     
            ADD_PED_FOR_DIALOGUE (AppContactPedStruct, 1, NULL, VoiceID_Decision)
            

            //Pause answering time needs set to zero here as we've already waited a believable time for the answerphone to "pick up"...
            g_Outgoing_Call_Pause_Answering_Time = 0




            SWITCH g_TheContactInvolvedInCall



            //Lowrider Benny Answerphone message    2547521
            CASE CHAR_BENNYS_OMW
                //Need to make sure that PLAYER_CALL_CHAR_CELLPHONE_DECISION points to the DLC dialogue rpf for this particular answerphone message by use of the MP dialogue flag.
                PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, CHAR_BENNYS_OMW, "LOWCAAU", "LOWCA_CALL10", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN) 
            BREAK



            CASE CHAR_LESTER_DEATHWISH //2023337 - Default Lester Deathwish unique number to his normal answerphone messsage.

                PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, CHAR_LESTER, answerPhoneBlock, "LES_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
            
            BREAK   



            CASE CHAR_DR_FRIEDLANDER 

                INT i_tempClockHours

                i_tempClockHours  = GET_CLOCK_HOURS()

                IF i_tempClockHours > 17     // office closed from 6pm to 9am.
                OR i_tempClockHours < 9

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "SHR_APH3", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF

                
                IF i_tempClockHours > 8     //open, morning 9am - 1pm
                AND i_tempClockHours < 13

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "SHR_APH1", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF


                IF i_tempClockHours > 12    //open, afternoon 1pm - 6pm
                AND i_tempClockHours < 18
                
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "SHR_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF

            BREAK





            CASE CHAR_AMANDA

                IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
                AND (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6))

                    //Play "angry" message.
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "AMA_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                
                ELSE //Default Amanda answerphone message.
                
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].ansphone_labelroot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                
                ENDIF


            BREAK



            CASE CHAR_RON

                IF g_bInMultiplayer
                    
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_RBusy", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELSE

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].ansphone_labelroot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF

            BREAK




            CASE CHAR_DENISE

                IF GET_MISSION_FLOW_FLAG_STATE (FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)   //Franklin has his new house

                    //Play "Center for the arts" message.
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "DEN_APH3", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                
                ELSE //Default Denise answerphone message.
                
                    
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].ansphone_labelroot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                
                ENDIF


            BREAK


            
            CASE CHAR_TRACEY

                IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
                   //Use TRA_APH1
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "TRA_APH1", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
                   //Use TRA_APH2 
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "TRA_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                                  
                ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
                    //Use TRA_APH3
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "TRA_APH3", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELSE

                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "TRA_APH4", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                   
                ENDIF

            BREAK



            CASE CHAR_LAMAR


                IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
           
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "LAM_APH1", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
                 
                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "LAM_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ELSE

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "LAM_APH3", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF

            BREAK




            
            CASE CHAR_JIMMY

                IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
                   //Use TRA_APH1
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "JIM_APH1", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
                   //Use TRA_APH2 
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "JIM_APH3", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                                  
                ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
                    //Use TRA_APH3
                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "JIM_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)

                ELSE

                   PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "JIM_APH4", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                   
                ENDIF

            BREAK



            CASE CHAR_BARRY
   
                IF IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_BARRY_4)  //After the strand is completed, Barry becomes paranoid.

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "BAR_APH2", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ELSE //Play pre-completion stoned answerphone.

                    PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "BAR_APH1", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                    
                ENDIF

            BREAK

            CASE CHAR_MP_MERRYWEATHER
                iRandomNumber = GET_RANDOM_INT_IN_RANGE(0,100)
                IF iRandomNumber <= 50
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_MWBusy", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ELSE
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_MWBusy2" , CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ENDIF
            BREAK
            
            CASE CHAR_PEGASUS_DELIVERY
                iRandomNumber = GET_RANDOM_INT_IN_RANGE(0,100)
                IF iRandomNumber <= 50
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_PGbusy", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ELSE
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_PGbusy2" , CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ENDIF
            BREAK
            
            CASE CHAR_MP_MORS_MUTUAL
                iRandomNumber = GET_RANDOM_INT_IN_RANGE(0,100)
                IF iRandomNumber <= 50
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_INSbusy", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ELSE
                     PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_INSbusy2" , CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                ENDIF
            BREAK
            
            CASE CHAR_MP_GERALD
                //ADD_PED_FOR_DIALOGUE (AppContactPedStruct, 0, PLAYER_PED_ID(),"")
                PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, "ANS_GBusy", CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
            BREAK

            DEFAULT  //Use this for characters who only have one standard, unchanging answerphone message.

                //Play the default answerphone message tied to this contact as attached via charsheet_public. 
                PLAYER_CALL_CHAR_CELLPHONE_DECISION (AppContactPedStruct, (INT_TO_ENUM(enumCharacterList, g_TheContactInvolvedinCall)), answerPhoneBlock, g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].ansphone_labelroot, CONV_PRIORITY_NON_CRITICAL_CALL, FALSE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
                                
            ENDSWITCH
            


            //Critical! #1171319
            //Make sure the "is answerphone" message flag is set so that dialogue handler knows not to auto-hide the phone after it becomes connected.
            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_THIS_IS_AN_ANSWERPHONE_CALL)



        ENDIF

    BREAK


    CASE CC_ENGAGED


        //Loop engaged tone...
        
        IF HAS_SOUND_FINISHED (Phone_Dialling_SoundID)
    
            //PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "PHONE_GENERIC_ENGAGED")
            PLAY_SOUND_FRONTEND (Phone_Dialling_SoundID, "Remote_Engaged", g_Owner_Soundset)
        
        ENDIF

        IF TIMERB() > 1500
            IF b_TrevorEngagedCurseSetup = FALSE
                IF g_Cellphone.PhoneOwner = CHAR_TREVOR
       
                    IF NOT IS_PED_INJURED (PLAYER_PED_ID())

                        //ADD_PED_FOR_DIALOGUE (AppContactPedStruct, 1, PLAYER_PED_ID(), "TREVOR")


        
                        //CREATE_CONVERSATION (AppContactPedStruct, "ENGAUD", "TRE_EG1", CONV_PRIORITY_MEDIUM)

                        PLAY_PED_AMBIENT_SPEECH (PLAYER_PED_ID(), "PHONE_CALL_NOT_CONNECTED") //See bug 787566 

                        //PLAY_PED_AMBIENT_SPEECH (PLAYER_PED_ID(), "GENERIC_FUCK_YOU")


                        b_TrevorEngagedCurseSetup = TRUE

                    ENDIF

                ENDIF
            ENDIF
        ENDIF

                          
        //This will repeat until the player hangs up...

        #if IS_DEBUG_BUILD

            //PRINTSTRING ("phone engaged...")
            //PRINTNL()

        #endif

  


    BREAK


    
    ENDSWITCH






ENDPROC







PROC Cleanup_and_Terminate()


    STOP_SOUND (Phone_Dialling_SoundID)

    RELEASE_SOUND_ID (Phone_Dialling_SoundID)

    m_sPlayers.bBusy = FALSE
    //May need to reset rendertarget...


    //Can't have update_VSE or PDS_MAXIMUM set here due to calls...

	CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDPIC_CONTACT_SELECTED)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)

    //Clear finale contact list choice. Bit is set by flow calling specific LAUNCH FINALE public function.
    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)


    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL)


    //Critical! Restore Master alpha ints after Finale Choice contacts list may have altered them.	
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_LESTER, Master_LesterInt)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_TREVOR, Master_TrevorInt)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MICHAEL, Master_MichaelInt)


    #if IS_DEBUG_BUILD
        cdPrintstring("appContacts - TERMINATING thread.")
        cdPrintnl()
    #endif
    IF m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_IN_CALL
        IF NETWORK_IS_SIGNED_ONLINE()
            IF voiceSession.iScriptResponse = -1
                PRINTLN("appContacts - TERMINATING thread. and ending voice session")
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("Script End A Voice Session - 18")
                                        cdPrintnl()   
                                    #endif

                SCRIPT_END_A_VOICE_SESSION()
            ENDIF
        ENDIF
    ENDIF
        





    TERMINATE_THIS_THREAD()


ENDPROC 










SCRIPT


    //Numpad Entry tests.
    //g_NumpadEntryString = "teststring"



    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

    
    //Call this immediately to make sure "exit vehicle" action is disabled in cellphone flashhand right away.
    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


    //Make sure any rogue call to these aren't hanging around...
    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)


    //Get a copy of proper alphaInts before manipulating each character to specify order they appear on finale choice contacts list.

    Master_LesterInt = GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(CHAR_LESTER)
    Master_TrevorInt = GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(CHAR_TREVOR)
    Master_MichaelInt = GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(CHAR_MICHAEL)

    #if IS_DEBUG_BUILD

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)
 
            cdPrintstring ("AppCONTACTS - Launching Finale Contacts List.")
            cdPrintnl()
  
        ENDIF

    #endif



 
    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //transparent
    


    Phone_Dialling_SoundID = GET_SOUND_ID()

  
    IF g_bInMultiplayer

        Place_MP_Names_in_ContactSlots()

    ELSE

        Place_Names_in_ContactSlots()

    ENDIF

    IF NOT ((ENUM_TO_INT (g_Cellphone.PhoneDS)) = ENUM_TO_INT (PDS_ATTEMPTING_TO_CALL_CONTACT)) //if any other app has launched appContacts in order to access the call system, leave the state as is.
        
        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

            
            //See bug 375586
            IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //Prevent slow framerate opening up a window of opportunity when calling text sender during appContacts launch. 


                g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Failsafe - makes sure that the cellphone is initialised into running state where applicable.
        
             
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 1647. appContacts assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            ENDIF

        ENDIF

    
    ENDIF




    FinaleChoice_StartTime = GET_GAME_TIMER() //Get launch time for the purpose of creating a brief pause to prevent button mashing selecting Finale Choice prematurely.

	 #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppContacts - 2nd Sept 16th May Sanity check for bug 2828375 - Update alphabetical ordering for male and female assistant contacts for Executive pack.")
        cdPrintnl()   
     #endif

	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_BIKER_CH1, 1320)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSISTF, 180)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CHAR_MP_P_ASSIST, 190)
    
    WHILE TRUE

     


        WAIT(0)
        

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL)

  
            Remove_Cellphone_From_Ear()

            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.


                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppCONTACTS - appContacts has been requested to terminate by TERMINATE_APPCONTACTS_FOR_CHAT_CALL()")
                    cdPrintstring("STATE ASSIGNMENT 2337. appContacts assigns PDS_COMPLEXAPP - was in attempting to call state")
                    cdPrintnl() 

                #endif



                //This should be the state that Conor's player list app was in before it launched the outgoing chat call procedure.

                g_Cellphone.PhoneDS = PDS_COMPLEXAPP


            ENDIF

                        
            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL)

            Cleanup_and_Terminate()

        ENDIF

        IF b_DoHumanFriend_Routine = TRUE
        AND g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        AND m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_IN_CALL

   
            Check_For_Human_Friend_Menu_Decision()

        ENDIF

     



        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.

            SWITCH g_Cellphone.PhoneDS
             
            


                CASE PDS_RUNNINGAPP
                                                    
                                                    
                                                    
                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

                        Auto_Scroll_To_Set_Contact()                            
             
                    ELSE

                        Check_for_List_Navigation()                                      
                                             


                        //Brief pause during finale choice filter to prevent button mashing selecting Finale Choice prematurely.
                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)
                        
                            FinaleChoice_CurrentTime = GET_GAME_TIMER()

                            IF (FinaleChoice_CurrentTime - FinaleChoice_StartTime) > 1000


                                IF b_DoFinaleConfirmation

                                    Do_FinaleConfirmationProceed()

                                ELSE

                                    Check_For_Contact_Selection()
                                
                                ENDIF

                            ENDIF



                            IF b_DoFinaleConfirmation  //Separated go back routine from Finale confirmation proceed to allow user to go back to triple choice immediately. #1204849

                                Do_FinaleConfirmationGoBack()

                            ENDIF


                        
                        
                        ELSE
                                                       
                              Check_For_Contact_Selection()

                        ENDIF

                    ENDIF



                    
                    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //don't check for this button if we are sending a cellphone pic.
                       
                        
                                
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Enter new number

                        


                                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

                                    #if IS_DEBUG_BUILD

                                        PRINTSTRING("AppContacts in Finale Choice mode, won't launch number entry!")
                                        PRINTNL()

                                     #endif

                                ELSE

                                    IF g_Use_Prologue_Cellphone = FALSE //No enter number for prologue mission!
                                    
                                        IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE)) //No enter number during Lester 1B
                                        //This bit is cleared in CELLPHONE_FLASHHAND's terminate routine as a precaution.
                                   
                                            #if IS_DEBUG_BUILD

                                                PRINTSTRING("AppContacts detecting special option button input")
                                                PRINTNL()

                                            #endif

                                          
                                           
                                           
                                            Play_Select_Beep()
                               
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 11)


                                            NewStringIndex = 0 //Potential fix for bug 771768 


                                            NumpadEntryString ="" //Potential fix for 1589265


                                            Scaleform_Setup_and_Fill_Numpad()
                                            
                                            AppCursorIndex = 0 // Reset the app cursor to slot 0 for scrollwheel - cursor is automatically changed to key 1



                                            b_DoHumanFriend_Routine = FALSE //Test fix for 2105815. This should make sure that full left and right navigation is restored.



                                            //Set methods and have new state that says we are in entry mode...
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 11) //Set enum state to NUMPAD_SCREEN

                                            
                                            
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_221") //Should be replace by black or contents of literal string


                                            //Monochrome
                                            /*
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" Positive

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 1,
                                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Other
                                            */


                                            
                                            //Badger - Select, Call, Back keypad screen combo
                                            IF g_b_ToggleButtonLabels
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" - Positive

                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                                    5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Other
                                                SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

                                            ELSE
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                                    5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Other
                                                SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
                                            ENDIF

                                            //See bug 316474
                                            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                                g_Cellphone.PhoneDS = PDS_COMPLEXAPP

                                                #if IS_DEBUG_BUILD
                                                    cdPrintnl()
                                                    cdPrintstring("STATE ASSIGNMENT 17. appContacts assigns PDS_COMPLEXAPP")
                                                    cdPrintnl()   
                                                #endif

                                            ENDIF

                                        ELSE

                                            PRINTSTRING("AppCONTACTS - Cannot open numpad as g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE bit is set.")
                                            PRINTNL()

                                        ENDIF

                                ENDIF

                            ENDIF

                        ENDIF

                    ENDIF

          
                BREAK



                
                CASE PDS_COMPLEXAPP

                    IF b_DoHumanFriend_Routine = TRUE //Conor, stick anything in here you need to.

                     
                        Check_for_List_Navigation()      //Critical.

                        Check_For_Human_Friend_Menu_Decision()  //Critical.

  
                    ELSE


                        IF b_DoFriend_SecondaryCallRoutine = FALSE


                            Check_For_Navigation() //from cellphone_private.sch.

                            Check_For_Numpad_Selection()


                        ELSE


                            //Dynamically Update List here based on timer:
                            IF TIMERA() > 1500
                                
                                tempDynamicIndex = 0

                                WHILE tempDynamicIndex < MAX_SPECIAL_SP_CHARACTERS

                               
                                    IF ARE_STRINGS_EQUAL (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label , g_SpecialSPCharacters[tempDynamicIndex].Name_TextLabel)

                               
                                        IF NOT ARE_STRINGS_EQUAL (g_SpecialSPCharacters[tempDynamicIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL") //A secondary function must have been defined if the contents are no longer the predefined null.

                                            b_Secondary_Option_Available = TRUE

                                            i_Available_Option = tempDynamicIndex //Get a copy so we can extract the text label when constructing the menu.

                                        ELSE

                                            b_Secondary_Option_Available = FALSE

                            
                                        ENDIF


                                    ENDIF

                                    tempDynamicIndex ++

                                ENDWHILE

                                IF b_DoCancelConfirmation = FALSE


                                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
                                    
                                    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


                                    
                                    WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
                                    
                                        WAIT (0)

                                        #if IS_DEBUG_BUILD
                                            cdPrintstring("appContacts - Waiting on return value from scaleform before menu refresh. 301")
                                            cdPrintnl()
                                        #endif
                                        
                                    ENDWHILE


                                    i_Selected_Slot_to_Restore = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)  //make a copy so this can be input into DISPLAY_VIEW to reselect last highlighted slot.

                                  


                                    Set_Up_Call_Hangout_Secondary_Cancel_Menu() 
                                
                                ENDIF

                            ENDIF



                            Check_for_List_Navigation()  

                            // Using one time help display system instead as this is needed with other checks #bug:1472650
                            IF b_Friend_Option_Available = TRUE
                                IF NOT g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneFriend
                                    IF NOT (CAN_CHARS_ARRANGE_BATTLE_BUDDY (g_Cellphone.PhoneOwner, g_TheContactInvolvedinCall))                                       
                                        g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneFriend = TRUE                                        
                                    ENDIF
                                ENDIF
                            ENDIF


                            IF (CAN_CHARS_ARRANGE_BATTLE_BUDDY (g_Cellphone.PhoneOwner, g_TheContactInvolvedinCall))

                                IF NOT g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneBBuddy
                                    IF b_Secondary_Option_Available = TRUE
                                
                                        IF CAN_MISSION_ARRANGE_MULTIPLE_BATTLE_BUDDIES(g_eFriendMissionZoneID)

                                            SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_BBUDDY")   // #1143018 - Battle Buddy help text. (plural)
                                        
                                                CASE FHS_EXPIRED

                                                    ADD_HELP_TO_FLOW_QUEUE("AM_H_BBUDDY", FHP_HIGH, 500, DEFAULT_HELP_TEXT_TIME)
                                                
                                                BREAK
                                            
                                                CASE FHS_DISPLAYED

                                                    g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneBBuddy = TRUE
                                                    
                                                    #if IS_DEBUG_BUILD
                        
                                                        cdPrintstring("appCONTACTS - Cellphone Battle Buddy help has been displayed. Saved global set.")
                                                        cdPrintnl()

                                                    #endif

                                                BREAK
                                            
                                            ENDSWITCH
                                            
                                            IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_BBUDDY1") = FHS_QUEUED // (remove singular)
                                                REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_BBUDDY1")
                                            ENDIF
                                        
                                        ELSE
                                        
                                            SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_BBUDDY1")   // #1143018 - Battle Buddy help text. (singular)
                                        
                                                CASE FHS_EXPIRED

                                                    ADD_HELP_TO_FLOW_QUEUE("AM_H_BBUDDY1", FHP_HIGH, 500, DEFAULT_HELP_TEXT_TIME)
                                                
                                                BREAK
                                            
                                                CASE FHS_DISPLAYED

                                                    g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneBBuddy = TRUE
                                                    
                                                    #if IS_DEBUG_BUILD
                        
                                                        cdPrintstring("appCONTACTS - Cellphone Battle Buddy help has been displayed. Saved global set.")
                                                        cdPrintnl()

                                                    #endif

                                                BREAK
                                            
                                            ENDSWITCH
                                            
                                            IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_BBUDDY") = FHS_QUEUED // (remove plural)
                                                REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_BBUDDY")
                                            ENDIF

                                        ENDIF

                                    ENDIF
                                ENDIF
                            
                            ENDIF


                            IF b_DoCancelConfirmation

                               CancellationConfirmation()               
                                        
                            ELSE

                                Check_For_Friend_Secondary_Call_Decision()

                            ENDIF

                        ENDIF
    

                    ENDIF







                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full contact list display.
                    
                        //IF b_DoCancelConfirmation = FALSE

                            Play_Back_Beep()

                            g_InputButtonJustPressed = TRUE

                          
                            IF g_bInMultiplayer

                                Place_MP_Names_in_ContactSlots()

                            ELSE

                                Place_Names_in_ContactSlots()

                            ENDIF

                            WAIT(0) //make sure this JUST_PRESSED doesn't carry over by stopping for a game cycle.
                            

                            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("STATE ASSIGNMENT 18. appContacts assigns PDS_RUNNINGAPP")
                                    cdPrintnl()   
                                #endif

                            ENDIF

                            NumpadEntryString ="" //Reset entry string.

                            
                            IF b_DoFriend_SecondaryCallRoutine = TRUE

                                b_DoFriend_SecondaryCallRoutine = FALSE

                            ENDIF

                            IF b_DoCancelConfirmation = TRUE

                               b_DoCancelConfirmation = FALSE    

                            ENDIF

                            b_DoHumanFriend_Routine = FALSE

                        
                        //ENDIF

                    ENDIF


                BREAK









                CASE PDS_ATTEMPTING_TO_CALL_CONTACT //Keith could check for this in his flow stuff if we want to stop other calls getting to the player's phone when he is
                                                    //actively trying to call another contact.

           
                    //Check connection progress. If it has reached "engaged" display signifier...
                    IF cc_Progress = CC_ENGAGED


                        #if IS_DEBUG_BUILD

                            IF g_DoDebugTempDraw_Phone = TRUE

                                format_medium_ostext (255, 128, 64, 255) //Orange
                                DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.08), "CELL_220")

                            ENDIF

                        #endif

                    ENDIF
                    

                    Check_For_Answerphone_Timeout()


                BREAK






                CASE PDS_MAXIMUM

                     #IF IS_DEBUG_BUILD

                        PRINTSTRING("Appcontacts aborting...PDS set to Maximum from flashhandler")
                        PRINTNL()

                    #endif

                    Cleanup_and_Terminate() //Failsafe in case checks below are not picked up... 

                BREAK






                DEFAULT

                    #IF IS_DEBUG_BUILD

                        PRINTSTRING("AppContacts in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK




            ENDSWITCH

     
            

            //CRITICAL! It might seem that this could be put in the PDS_RUNNINGAPP switch clause. It can't because cellphone_flashhand resets the state to PDS_MAXIMUM
            //because of the call system, so this segment needs to run and check for a button press in MAXIMUM or RUNNINGAPP state. We can check that this script is
            //not in COMPLEXAPP state as an alternative.

            
            //CHECK_FOR_APPLICATION_EXIT() //can't use this as appContacts requires special case treatment for the player proactively calling a contact from the phonebook. 
            //Well, it did but this is not currently the case as we are returning to PDS_MAXIMUM from the calling screen at the moment. Leaving this segment in as it causes no ill effects.       
            //STATE ASSIGNMENT 53 from flashhand will take care of this exit beforehand usually, so this routine is more of a safety measure now.           


            IF NOT (g_Cellphone.PhoneDS = PDS_COMPLEXAPP)
            IF (g_App_Elapsed_SafetyBuffer - g_App_SafetyBuffer_StartTime) > g_App_Required_SafetyBuffer_Duration //make sure app has had a chance to launch correctly before exiting. See bug 25815.
                     

                IF IS_PLAYER_PLAYING (PLAYER_ID())
                    IF IS_PLAYER_CONTROL_ON (PLAYER_ID())

    
                        //This BACK_INPUT section won't get run as long as I'm returning to PDS_MAXIMUM via flashhand and using the terminate check above...
                        //It might have to go back in if "back" is to return us to some other state.
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back - quit this application

                           
                            #if IS_DEBUG_BUILD

                                PRINTSTRING("AppContacts detecting back button input.")
                                PRINTNL()

                            #endif

                           
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)  
                            
                            
                                #if IS_DEBUG_BUILD

                                    PRINTSTRING("Not allowing Contacts exit as this is the Finale Choice list!")
                                    PRINTNL()

                                #endif

        
                                Play_Back_Beep()


                            ELSE


                                Play_Back_Beep()
               

                                IF NOT (g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT)   //#1580608

                                    #if IS_DEBUG_BUILD

                                        PRINTSTRING("g_CellPhone.PhoneDS wasn't PDS_ATTEMPTING_TO_CALL_CONTACT so allowing appContacts to terminate")
                                        PRINTNL()

                                    #endif




                                    Cleanup_and_Terminate() //Quit appContacts. Could change this so the contacts thread doesn't completely terminate and swaps back to contact list.
                            
                                
                                ELSE
                                
                                    //Big new clause change for #1580608

                                    //Adding this on a trial basis to prevent quitting before PROCESS_VOICE_SESSION_STARTED_EVENT() has fired. 1602347

                                    IF g_TheContactInvolvedinCall = CHAR_CHAT_CALL
                                        
                                        IF voiceSession.bInVoiceSession
                                        OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)



                                            #if IS_DEBUG_BUILD

                                                PRINTSTRING("g_CellPhone.PhoneDS was in PDS_ATTEMPTING_TO_CALL_CONTACT for chat call - allowing manual exit as InVoiceSession is returning true.")
                                                PRINTNL()

                                            #endif


                                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2) //, TO_FLOAT(SF_SelectedContactInt)) //Set enum state and header to CONTACTS

          

                                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_7000")
                                        
                                            ELSE

                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_0")

                                            ENDIF   
                                        
                                       
                                            STOP_SOUND (Phone_Dialling_SoundID)
                                            cc_Progress = CC_LAUNCH_TIMER_AND_CHECK_MESSAGE


                                        ELSE

                                            #if IS_DEBUG_BUILD

                                                PRINTSTRING("g_CellPhone.PhoneDS was in PDS_ATTEMPTING_TO_CALL_CONTACT for chat call - but not allowing manual exit as primary voice session event has yet to fire after request.")
                                                PRINTNL()

                                            #endif

                                    
                                        ENDIF

                                    ELSE

                                        #if IS_DEBUG_BUILD
                                            //New for 2023083 - Hunting failsafe.
                                            cdPRINTSTRING("g_CellPhone.PhoneDS was in PDS_ATTEMPTING_TO_CALL_CONTACT - setting to RUNNING APP and re-establishing view #1580608")
                                            cdPRINTNL()

                                            
                                            cdPRINTSTRING("STATE_ASSIGNMENT - app Contacts RUNNING APP  #1580608")
                                            cdPRINTNL()

                                        #endif


                                        Remove_Cellphone_From_Ear() //New for 2023083 - Hunting failsafe.

                             
                                        g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                                        Post_Aborted_String_Entry_Button_Creation() //added to resolve bug 1643709   


                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 2) //, TO_FLOAT(SF_SelectedContactInt)) //Set enum state and header to CONTACTS

          

                                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) 

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_7000")
                                        
                                        ELSE

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_0")

                                        ENDIF   
                                        
                                       
                                        STOP_SOUND (Phone_Dialling_SoundID)
                                        cc_Progress = CC_LAUNCH_TIMER_AND_CHECK_MESSAGE
                                   
                                  ENDIF
                                                                 
                                ENDIF

                            ENDIF

                        ENDIF

                    ENDIF
                ENDIF
                
            ENDIF
            ENDIF
            
             

        ELSE



            //IMPORTANT:
            //Even though calls cannot come through to running apps now, appContacts needs this check because a call can be made from within it. It therefore needs to
            //exit when a call connects.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling contact" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the contacts list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the contacts list.


            //N.B BeforeCallPhoneDS state above, now superseded by BeforeCallStateHandler cued from dialogue handler.

            
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("APP_CONTACTS - AppContacts terminating due to status changing to ongoing call")
                PRINTNL()

            #endif



            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



    
        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()     

        ENDIF








        
        /*
        IF g_TerminateApp = TRUE


            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("APP_CONTACTS - AppContacts terminating due to g_TerminateApp = TRUE")
                PRINTNL()

            #endif


            
            Cleanup_and_Terminate() 

        ENDIF
        */




         #if IS_DEBUG_BUILD

            /*
            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_7)
                NETWORK_MUTE_GAMER(m_sPlayers.selectedPlayerHandle, TRUE)
            ENDIF

            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_8)
                NETWORK_MUTE_GAMER(m_sPlayers.selectedPlayerHandle, FALSE)
            ENDIF
            */

            //Show Graeme this issue.
            /*
            IF NOT IS_STRING_NULL_OR_EMPTY(singlecharacter[0])

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", singlecharacter[0])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.84, "STRING", singlecharacter[1])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.45, 0.84, "STRING", singlecharacter[2])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.5, 0.84, "STRING", singlecharacter[3])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.84, "STRING", singlecharacter[4])

            ENDIF
            */

            /*

            IF NOT IS_STRING_NULL_OR_EMPTY(NewStringToAppend[4])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", NewStringToAppend[4])
            ENDIF

            IF NOT IS_STRING_NULL_OR_EMPTY(NewStringToAppend[5])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.84, "STRING", NewStringToAppend[5])
            ENDIF

            IF NOT IS_STRING_NULL_OR_EMPTY(NewStringToAppend[6])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.45, 0.84, "STRING", NewStringToAppend[6])
            ENDIF

            IF NOT IS_STRING_NULL_OR_EMPTY(NewStringToAppend[7])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.5, 0.84, "STRING", NewStringToAppend[7])
            ENDIF

            IF NOT IS_STRING_NULL_OR_EMPTY(NewStringToAppend[8])
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.84, "STRING", NewStringToAppend[8])
            ENDIF



            IF ARE_STRINGS_EQUAL(NewStringtoAppend[4], "4")
               DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.64, "STRING", "Four")
            ENDIF
    
            */

            //draw_debug_screen_grid()

            //This command should be okay to use in release mode.
            //IF GET_LENGTH_OF_LITERAL_STRING (NumpadEntryString) > 5
            //  DISPLAY_TEXT_WITH_LITERAL_STRING (0.05, 0.5, "STRING", "TEST!")
            //ENDIF
         
        #endif



  
            
    ENDWHILE
    
    
ENDSCRIPT


