USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "cellphone_movement.sch"
USING "stack_sizes.sch"
USING "email_public.sch"
USING "selector_public.sch"
USING "website_public.sch"
  
 
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME    :    appEmail.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Placeholder Organiser or Email script, launched from phone
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//phone rotation stuff
VECTOR v_3dEmailOriginalPosition, v_3dEmailDesiredPosition





//Texture dictionary notes
//      IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SplashScreens")
//          REQUEST_STREAMED_TEXTURE_DICT("SplashScreens") //MichaelTSplash
//      ENDIF

//      SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SplashScreens")
/*
PROC LOAD_TEXTURE_DICTIONARY_AND_BLOCK()

    REQUEST_STREAMED_TEXTURE_DICT("placeholderattachment")//TODO, replace with emails
    PRINTLN("Email system enter wait for attachement texture dictionaries load")
    WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED("placeholderattachment")
        WAIT(10)
    ENDWHILE
    PRINTLN("Email system leaving wait for attachement texture dictionaries load")

ENDPROC

PROC RELEASE_TEXTURE_DICTIONARIES()
    PRINTLN("Email system releasing attachement texture dictionaries")
    SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("placeholderattachment")

ENDPROC
*/

//Replaced these with safety globals g_b_Rotate3dPhonePortrait and g_b_Rotate3dPhoneLandscape
//BOOL b_Rotate3dPhonePortrait = FALSE
//BOOL b_Rotate3dPhone = FALSE

SCALEFORM_RETURN_INDEX fudp
INT fudret = 0


PROC Cleanup_and_Terminate()
    IF(g_iEmailWithLoadedTextureIndex != -1)
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(g_iEmailWithLoadedTextureIndex))
        g_iEmailWithLoadedTextureIndex = -1
        PRINTLN("Releasing old attachment texture")
    ENDIF
    g_bEmailSystemPaused = FALSE


    //As this script is not checking CHECK_APPLICATION_EXIT() we need to have a failsafe that ensures the correct display state is selected and scaleform 
    //resets the display to the homescreen to avoid any timing issues. Pressing "Select" then "Back" in very quick succession for example.

    IF g_Cellphone.PhoneDS > PDS_TAKEOUT //i.e don't run this if the phone has just been put into disabled or away state
       

        g_Cellphone.PhoneDS = PDS_MAXIMUM


        Update_Scaleform_VSE ()



    ENDIF


    //Play_Back_Beep()
    //RELEASE_TEXTURE_DICTIONARIES()
    TERMINATE_THIS_THREAD()


ENDPROC

PROC RotatePhoneToWidescreen()
    FLOAT fComplete = MovePhoneToDestination( v_3dEmailOriginalPosition, v_3dEmailDesiredPosition, g_3dPhoneStartRotVec, <<-90, 0, 90.0>>, 350.0, FALSE)
    
    // scale the phone up just a bit so that email is readable
    // note that subtitles were already overlapping, so might as well go big (or go home)
    IF g_IsMonochromeCheatActive = FALSE
        
        IF IS_PC_VERSION()

            IF NOT IsCurrentCamFirstPerson() //2184278 - Enlarging the phone in 800 x 600 seems to display the corner of the phone in 1st person.
                
                SET_MOBILE_PHONE_SCALE(500 + 75*fComplete)

            ENDIF

        ELSE

            SET_MOBILE_PHONE_SCALE(500 + 75*fComplete)
    
        ENDIF

    ENDIF
    //MovePhoneToDestination should return 2.0 very quickly in First Person mode so the prop in hand should begin rotating to landscape on immediate launch of the app.

    
    IF fComplete >= 1.0
        
        CellphoneFirstPersonHorizontalModeToggle(TRUE)

        g_b_Rotate3dPhoneLandscape = FALSE
        s_iTimeStampStart = 0
    ENDIF
ENDPROC


PROC RotatePhoneToPortrait_and_Exit()
    FLOAT fComplete = MovePhoneToDestination( v_3dEmailDesiredPosition, v_3dEmailOriginalPosition, <<-90, 0, 90.0>>, g_3dPhoneStartRotVec, 350.0, FALSE)
    

    // scale the phone down again.
    IF g_IsMonochromeCheatActive = FALSE
        SET_MOBILE_PHONE_SCALE(500 + 75*(1.0-fComplete))
    ENDIF

    //MovePhoneToDestination should return 2.0 very quickly in First Person mode so the prop in hand should begin rotating back on termination of the app.

    IF fComplete >= 1.0
        g_b_Rotate3dPhonePortrait = FALSE

        //Embedded check in this for 2015070
        CellphoneFirstPersonHorizontalModeToggle(FALSE)
  
        Cleanup_and_Terminate()
        s_iTimeStampStart = 0
    ENDIF
ENDPROC


///Email system

enumCharacterList currPlayer

BOOL bActivated = FALSE
BOOL bViewSwitchedToEmail = FALSE
BOOL bUpdateRequired = FALSE
BOOL bResponseFiredSinceLastViewOfInbox = FALSE//

INT iSelectedEmail = -1
INT iSelectedResponse = -1

BOOL bPadCircle = FALSE
BOOL bPadCross = FALSE
BOOL bButtonUp = FALSE
BOOL bButtonDown = FALSE
BOOL bButtonLeft = FALSE
BOOL bButtonRight = FALSE

BOOL bURLPrimed = FALSE
STRING sURLlast = NULL

FLOAT fLastValidInboxIndexSelected = 0

BOOL bMovedAfterBack = TRUE // this is to cope with the fact scaleform returns are unreliable

PROC DO_GENERIC_DIRECTIONAL_INPUTS()



    IF g_ShouldForceSelectionOfLatestAppItem
        PRINTLN("appemail DO_GENERIC_DIRECTIONAL_INPUTS bailed due to autoselect")
        iSelectedEmail = 0
        fLastValidInboxIndexSelected = TO_FLOAT(iSelectedEmail)                
        //double check that there are actually emails to select
        IF EMAILS_IN_INBOX(currPlayer) = 0
            iSelectedEmail = -1
        ENDIF
        IF NOT (iSelectedEmail = -1)
            g_Cellphone.PhoneDS = PDS_COMPLEXAPP
            bUpdateRequired = TRUE
            SETTIMERB(0)
            Play_Select_Beep()
        ENDIF
        EXIT
    ENDIF
    IF IS_BROWSER_OPEN() //if the browser has been launched from the email link then ignore inputs
        PRINTLN("appemail DO_GENERIC_DIRECTIONAL_INPUTS bailed due to net being visible")
        EXIT
    ENDIF
    IF g_b_Rotate3dPhonePortrait
        PRINTLN("appemail DO_GENERIC_DIRECTIONAL_INPUTS bailed due to phone rotating back to close")
        EXIT
    ENDIF
    //PRINTLN("appemail DO_GENERIC_DIRECTIONAL_INPUTS  checking inputs ")
    //check for the directional buttons to feed the directly to scaleform
    //check for the action buttons to interact with the menu

    //,
    //,
    //,
    //,

    IF NOT (bButtonUp)
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
        OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
            bButtonUp = TRUE
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")//up
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
            END_SCALEFORM_MOVIE_METHOD()
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
            SETTIMERA(0)
            bMovedAfterBack = TRUE
        ENDIF
    ELSE //wait for release
    
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
        OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
            IF TIMERA() > 100 AND (iSelectedEmail != -1)
                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")//up
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                END_SCALEFORM_MOVIE_METHOD()
                //PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
                SETTIMERA(0)
                bMovedAfterBack = TRUE
            ENDIF
        ELSE //NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
            bButtonUp = FALSE
        ENDIF
    ENDIF
    
    IF NOT (bButtonDown)
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
        OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
            bButtonDown = TRUE

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
            END_SCALEFORM_MOVIE_METHOD()
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
            SETTIMERB(0)
            bMovedAfterBack = TRUE
        ENDIF
    ELSE //wait for release
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
        OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
            IF TIMERB() > 100 AND (iSelectedEmail != -1)
                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
                END_SCALEFORM_MOVIE_METHOD()
                //PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
                SETTIMERB(0)
                bMovedAfterBack = TRUE
            ENDIF
        ELSE //NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
            bButtonDown = FALSE
        ENDIF
    ENDIF
    
    IF NOT (bButtonLeft)
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
            bButtonLeft = TRUE
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
            END_SCALEFORM_MOVIE_METHOD()
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

        ENDIF
    ELSE //wait for release
        IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
            bButtonLeft = FALSE
        ENDIF
    ENDIF
    
    IF NOT (bButtonRight)
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)   
            bButtonRight = TRUE
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_INPUT_EVENT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
            END_SCALEFORM_MOVIE_METHOD()
            PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)
        ENDIF
    ELSE //wait for release
        IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)   
            bButtonRight = FALSE
        ENDIF
    ENDIF
    

    //INPUT_FRONTEND_ACCEPT,
    
    
    IF NOT bPadCross
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT) AND (TIMERB() > 100)
            PRINTLN("appemail Cellphone input select")
            //attempting to select a mail!
            IF iSelectedEmail = -1
                



    
                
                //iSelectedEmail = CALL_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"GET_CURRENT_SELECTION")
                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"GET_CURRENT_SELECTION")
                                        fudp = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE () 
                                        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(fudp)
                                            PRINTLN("INITIAL SELECTION appemail spinning its wheels waiting for current selection")
                                            //RENDER_BROWSER()
                                            WAIT(0)
                                        ENDWHILE
                                        fudret = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(fudp)
                
                iSelectedEmail = fudret

                #if USE_TU_CHANGES
                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintString("S_T Scaleform return value on selecting email is ")
                        cdPrintInt(fudret)
                        cdPrintnl()
                        cdPrintnl()

                    #endif
                #endif
    
    
                
    
    
    
                
                //double check that there are actually emails to select
                IF EMAILS_IN_INBOX(currPlayer) = 0
                    iSelectedEmail = -1
                ENDIF
                
                
                
                IF NOT (iSelectedEmail = -1)
                    //EMAIL_LIST = 8
                    //EMAIL_VIEW = 9
                    
                    IF bMovedAfterBack = TRUE
                    fLastValidInboxIndexSelected = TO_FLOAT(iSelectedEmail)
                     PRINTLN("Last valid email selected set to ", fLastValidInboxIndexSelected)
                        bMovedAfterBack = FALSE
                    ENDIF
                    
                    g_Cellphone.PhoneDS = PDS_COMPLEXAPP
                    
                    bUpdateRequired = TRUE
                    SETTIMERB(0)
                    
                    Play_Select_Beep()
                ENDIF
            ELSE
            
                //TODO go to picking a response! if the mail has one! Or if picking then pick response
                IF iSelectedResponse = -1
                
                    IF SCALEFORM_CHECK_FOR_RESPONSE_ALLOWED(currPlayer,iSelectedEmail) AND (NOT bResponseFiredSinceLastViewOfInbox)
                        //go to response picking
                        iSelectedResponse = 0

                        bUpdateRequired = TRUE
                        SETTIMERB(0)
                        
                        Play_Select_Beep()
                    ENDIF
                ELSE 
                    //response picked, fire it
                    
                    
                    
//                    //iSelectedResponse  = CALL_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"GET_CURRENT_SELECTION")
//                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"GET_CURRENT_SELECTION")
//                                        fudp = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE ()
//                                        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(fudp)
//                                            PRINTLN("THIS IS THE RESPONSE appemail spinning it's wheels waiting for current selection")
//                                            //RENDER_BROWSER()
//                                            WAIT(0)
//                                        ENDWHILE
//                                        fudret = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(fudp)
//                    
                    //Hard coding first responce as we never have any other responce other than RESPOND
                    iSelectedResponse = 0 //fudret
                   
                    
                    PRINTln("\nAppemail firing response: ",fudret,".\n")
                    SCALEFORM_FIRE_RESPONSE(currPlayer,iSelectedEmail,iSelectedResponse)                
                    bResponseFiredSinceLastViewOfInbox = TRUE
                    //return to mail
                    iSelectedResponse = -1
                    
                    bUpdateRequired = TRUE
                    
                    Play_Select_Beep()
                    SETTIMERB(0)
                ENDIF
            ENDIF
            
            bPadCross = TRUE
        ENDIF
    ELSE
        //IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)   
                bPadCross = FALSE
       // ENDIF
    ENDIF
    
    
    //,

        //PAD_CIRCLE = 17,
    //BOOL bPadCircle = FALSE
    IF  (TIMERB() > 100)
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_CANCEL) 
            PRINTLN("Appemail cancel button press detected")
            BOOL backCancel = FALSE
            
            IF bPadCircle = FALSE
                IF NOT (iSelectedEmail = -1)
                    IF (iSelectedResponse = -1) //selected mail but no response picking in progress
                        //cancel back to inbox from mail
                        iSelectedEmail = ROUND(fLastValidInboxIndexSelected)
                        bURLPrimed = FALSE
                        SETTIMERB(0)
                        backCancel = TRUE
                        Play_Back_Beep()
     
                    ELSE
                        //canceling out of a response pick
                        SETTIMERB(0)
                        iSelectedResponse = -1
                        Play_Back_Beep()
                        
                    ENDIF
                    bUpdateRequired = TRUE
                ELSE
                    //no mail selected, exit if not already exiting
                    
                    IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_FLINK") = FHS_QUEUED
                         SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_WITH_LINK)
                    ENDIF
                    
                    IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SCROLL") = FHS_QUEUED
                        SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_SCROLL)
                    ENDIF
                    
                    
                    IF g_b_Rotate3dPhoneLandscape = FALSE //Don't allow exit routine, if the phone is in the process of rotating to landscape.
                      AND NOT g_b_Rotate3dPhonePortrait

                        Play_Back_Beep()
                        g_b_Rotate3dPhonePortrait = TRUE //will cue rotate and exit routine from above...
                        g_bEmailSystemPaused = FALSE //Steve T note. Should be able to remove this...
                    ENDIF
                ENDIF
                bPadCircle = TRUE
            ENDIF
            
             IF backCancel
                 iSelectedEmail = -1
                 //g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Steve T Note 2. This relates to the weirdness - we'd need to take this out.
             ENDIF
             
            
        ELSE
        
            bPadCircle = FALSE
        ENDIF
    ENDIF

    IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_EXTRA_OPTION)
        PRINTLN("appemail url button detected")
        IF bURLPrimed
        
            //SCRIPT_ASSERT("Email url triggered") //do webpage
            
            //clear the help
            
           // REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_FLINK")
            IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_FLINK") = FHS_QUEUED
            
                 SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_WITH_LINK)
                
            ENDIF
            
            IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SCROLL") = FHS_QUEUED
            
                SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_SCROLL)
            
            ENDIF
            //sURLlast
            g_BrowserLinkToLaunch = sURLlast
            //GO_TO_WEBSITE(g_BrowserLinkToLaunch)
            START_BROWSER(SBSS_Launch_Link)
            bURLPrimed = FALSE
            
            Play_Select_Beep()
            /*
            g_b_Rotate3dPhonePortrait = TRUE //will cue rotate and exit routine from above...
            b_Rotate3dPhoneX = TRUE
            b_Rotate3dPhoneY = TRUE
            b_Rotate3dPhoneZ = TRUE
            g_bEmailSystemPaused = FALSE //Steve T note. Should be able to remove this...
            */
        ENDIF
        
        
    
    
    ENDIF
    
ENDPROC




SCRIPT


    IF g_bInMultiplayer
        //RELEASE_TEXTURE_DICTIONARIES()
        TERMINATE_THIS_THREAD()
    ENDIF
    

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

    

    currPlayer = GET_CURRENT_PLAYER_PED_ENUM()
    REQUEST_ADDITIONAL_TEXT("email", PHONE_TEXT_SLOT)
    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(PHONE_TEXT_SLOT)
        WAIT(0)
    ENDWHILE
           
    /*
    //SCRIPT_ASSERT("Email test")
    //pending system test
    ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD(TEST_DYNAMIC_THREAD)     
    PRIME_EMAIL_FOR_FIRING_INTO_DYNAMIC_THREAD_IN_HOURS(TEST_DYNAMIC_THREAD, CULT3, 1)
    OVERRIDE_CONTENT_FOR_PENDING_DYNAMIC_EMAIL(TEST_DYNAMIC_THREAD,CULT3, "EPS_BMAIL_G4")
    
    ADD_CONTENT_FOR_FOR_PENDING_DYNAMIC_EMAIL_SUBSTRING(TEST_DYNAMIC_THREAD,CULT3, "EPS_BUYROBBTN")
    ADD_CONTENT_FOR_FOR_PENDING_DYNAMIC_EMAIL_SUBSTRING(TEST_DYNAMIC_THREAD,CULT3, "EPS_BUYROBBTN")
    ADD_CONTENT_FOR_FOR_PENDING_DYNAMIC_EMAIL_SUBSTRING(TEST_DYNAMIC_THREAD,CULT3, "EPS_BUYROBBTN")
    ADD_CONTENT_FOR_FOR_PENDING_DYNAMIC_EMAIL_SUBSTRING(TEST_DYNAMIC_THREAD,CULT3, "EPS_BUYROBBTN")
    */

    //Default these to be on the safe side...
    g_b_Rotate3dPhonePortrait = FALSE
    g_b_Rotate3dPhoneLandscape = FALSE




    //Set up and cue initial rotation of the phone to landscape...

    g_Cellphone.PhoneDS = PDS_COMPLEXAPP //We're using a "rotate" so the phone needs to be in COMPLEX_APP state to prevent cellphone_flashhand allowing a phone "put away"
                                         //to run during the rotate back to portrait.

    g_b_Rotate3dPhoneLandscape = TRUE
    g_bEmailSystemPaused = TRUE

    
    IF g_B_Scaleform_Movies_Loaded //the phone movie is loaded
        CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
        CONFIGURE_EMAIL_SOFT_KEY_BACK(TRUE) //default back key to on
        CONFIGURE_EMAIL_SOFT_KEY_LINK(FALSE)
                    
    ENDIF
    
    SETTIMERB(0)
    //LOAD_TEXTURE_DICTIONARY_AND_BLOCK()
    




    //Added for A class bug #1550721 //Removed for 1998041
    //GET_MOBILE_PHONE_POSITION (v_3dEmailOriginalPosition)
    
    
    //Added for 1998041
    //Prevents the phone from disappearing completely if the player started in 1st person, brought up Email, went to 3rd, then hit exit.
    v_3dEmailOriginalPosition = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]


    v_3dEmailDesiredPosition = v_3dEmailOriginalPosition
    // original value was -6. Text-readable size is -10.
    v_3dEmailDesiredPosition.x -= 10.0 //Specifies the size of the left adjustment to make sure the entire body text is visible in landscape mode.

    v_3dEmailDesiredPosition.y += 20.0 //Fixes TRC bombshell with new anims by making sure the landscape phone lies above any long or four line subtitles - in German!

    WHILE TRUE

        WAIT(0)


        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL   //Leave this in, Andrew.



          

            IF g_b_Rotate3dPhoneLandscape AND NOT g_b_Rotate3dPhonePortrait //make sure phone can't get stuck trying to rotate forwards and back.
                RotatePhoneToWidescreen()
                
            ELIF g_b_Rotate3dPhonePortrait
                RotatePhoneToPortrait_and_Exit()
                
            ENDIF


            /* Andrew, I've left these here as examples of where your own organiser / email procs and functions should be called.
            Draw_Camera_Zoom()

            Check_For_Shutter_Press()

            Process_Shutter_Delay()
            */

            
            //call email update here
            
            IF NOT bActivated
                //perform first run action // attempt to get phone
                bActivated = TRUE
                
                
            ELSE
                IF g_B_Scaleform_Movies_Loaded //the phone movie is loaded
                    //CONFIGURE_EMAIL_SOFT_KEY_SELECT(TRUE)
                    
                //update phone
                    IF NOT bViewSwitchedToEmail
                        //switch view to email
                        bViewSwitchedToEmail = TRUE


                        //call the update on the inbox here
                        FILL_SCALEFORM_INBOX_FOR_PED(SF_MovieIndex, currPlayer)
                        
                        
                        //EMAIL_LIST = 8
                        //EMAIL_VIEW = 9
                        

                        #if USE_TU_CHANGES
                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintString("S_T Filled inbox... ")
                                cdPrintnl()
                                cdPrintnl()

                            #endif
                        #endif
    






                        
                        
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING(SF_MovieIndex,"SET_HEADER","EM_INBOX")

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",8)
                        
                    ELSE


                        IF g_bEmailSystemUpdated
                            g_bEmailSystemUpdated = FALSE
                            bUpdateRequired = TRUE
                        ENDIF
                    
                        //update view
                        IF bUpdateRequired
                            SETTIMERA(0)
                            SETTIMERB(0)
                            //add new data to the email/inbox
                            IF (iSelectedEmail = -1)//not picked an email to view yet
                            CPRINTLN(debug_email,"Not selected email")
                            
                                IF bMovedAfterBack
                                    bMovedAfterBack = FALSE
                                    fLastValidInboxIndexSelected = 0.0
                                ENDIF
                            
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",8)
                                
                                
                                
                                INT mails = FILL_SCALEFORM_INBOX_FOR_PED(SF_MovieIndex, currPlayer)
                                
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING(SF_MovieIndex,"SET_HEADER","EM_INBOX")
                                bResponseFiredSinceLastViewOfInbox = FALSE
                                
                                IF mails > 0
                                    CONFIGURE_EMAIL_SOFT_KEY_SELECT(TRUE)
                                ELSE
                                    CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
                                ENDIF
                                CPRINTLN(debug_email,"mails: ",mails)
                                
                                CONFIGURE_EMAIL_SOFT_KEY_BACK(TRUE)
                                CONFIGURE_EMAIL_SOFT_KEY_LINK(FALSE)
                                
                                PRINTLN("Last valid inbox index selected ", fLastValidInboxIndexSelected)
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",8, fLastValidInboxIndexSelected)
                        
                            ELSE //email is selected
                                CPRINTLN(debug_email,"Selected emails")

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",21)
                                    WAIT(0)
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",21)
                                    WAIT(0)
                                CONFIGURE_EMAIL_SOFT_KEY_BACK(TRUE)
                                
                                EMAIL_FEED_ID_BUFFER_PURGE() //clear any on screen feed notifications for email
                                IF(iSelectedResponse = -1)//not in picking a response mode
                                    CPRINTLN(debug_email,"Selected response: ",iselectedResponse)

                                    BLOCKING_PREP_TEXTURE_FOR_SCALEFORM_MOVIE_EMAIL(currPlayer,iSelectedEmail)
                                    
                                    FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED(SF_MovieIndex,currPlayer,iSelectedEmail)  
                                    
                                    g_ShouldForceSelectionOfLatestAppItem = FALSE
                                    
                                    bURLPrimed = DOES_INBOX_MAIL_HAVE_URL(currPlayer,iSelectedEmail)
                                    IF bURLPrimed
                                        CPRINTLN(debug_email,"URL Primed")
                                    
                                        sURLlast = GET_EMAIL_IN_INBOX_URL(currPlayer,iSelectedEmail)
                                        PRINTLN("Email attempting to trigger appEmail")
                                        //do first time hyperlink help
                                          IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_FIRST_EMAIL_WITH_LINK)
                                            
                                            SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_FLINK")
                                              CASE FHS_EXPIRED
                                                ADD_HELP_TO_FLOW_QUEUE("AM_H_FLINK", FHP_HIGH, 0, 1000)
                                              BREAK
                                              CASE FHS_DISPLAYED
                                                    SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_WITH_LINK)
                                                
                                              BREAK
                                            ENDSWITCH
                                            //SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_WITH_LINK)


                                          ELSE 


                                                //2149152 for PC version to reinforce hyperlink help every session.
                                                IF IS_PC_VERSION()
                                                    IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_MP_HYPERLINK_EMAIL_HELP)

                                                        SET_BIT (BitSet_CellphoneTU, g_BSTU_MP_HYPERLINK_EMAIL_HELP)
                                                        
                                                        PRINT_HELP ("AM_H_FLINK")

                                                        #if IS_DEBUG_BUILD
                                                            cdPrintnl()
                                                            cdPrintstring("AppEmail - Displaying Hyperlink control message for the second and final time. Setting bit.")
                                                            cdPrintnl()
                                                        #endif
                                                            
                                                    ENDIF
                                                ENDIF



                                          ENDIF
                                        //TODO SOFTKEYS
                                        CONFIGURE_EMAIL_SOFT_KEY_LINK(TRUE)
                                    ELSE

                                        //Steve T.
                                        IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_FIRST_EMAIL_SCROLL)
                                            SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SCROLL")
                                              CASE FHS_EXPIRED
                                                ADD_HELP_TO_FLOW_QUEUE("AM_H_SCROLL", FHP_HIGH, 0, 1000)
                                              BREAK
                                              CASE FHS_DISPLAYED
                                                    
                                                SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_SCROLL)
                                              BREAK
                                            ENDSWITCH
                                            //SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_EMAIL_SCROLL)
                                          ENDIF
                                     

                                        CONFIGURE_EMAIL_SOFT_KEY_LINK(FALSE)
                                    ENDIF
                                    
                                    

                                    
                                    
                                    
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",9)
                                
                                ELSE
                                    CPRINTLN(debug_email,"picking response:",iSelectedEmail)
                                    //in picking a response mode
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",21)
                                    WAIT(0)
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",21)
                                    WAIT(0)
                                   //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",21)
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY",9)
                                    FILL_SCALEFORM_RESPONSES_FOR_EMAIL(SF_MovieIndex,currPlayer,iSelectedEmail)
                                    
                                    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING(SF_MovieIndex,"SET_HEADER","EM_RESPONSE")
                                   // LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",21)
                                   LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"DISPLAY_VIEW",9)
                                ENDIF
                            ENDIF
                        
                            
                            bUpdateRequired = FALSE
                        ELSE
                            //check for update needing done
                            //do input
                            DO_GENERIC_DIRECTIONAL_INPUTS()
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
           
            
        
            IF NOT (currPlayer = GET_CURRENT_PLAYER_PED_ENUM())
                //PANIC BAIL
                g_bEmailSystemPaused = FALSE
                SCRIPT_ASSERT("appEmail panic bailed due to player changing during use!")

                #if IS_DEBUG_BUILD

                    PRINTSTRING ("appEmail exiting due to player character switch.")
                    PRINTNL ()

                #endif



                Cleanup_and_Terminate()
            ENDIF
         
            




        ENDIF


        
        
        
        
        
        

        
        //Finally check for abnormal exit
        IF CHECK_FOR_ABNORMAL_EXIT()   //Leave this in, Andrew.

            /* Legacy.
            b_Rotate3dPhonePortrait = TRUE
            b_Rotate3dPhoneX = TRUE
            b_Rotate3dPhoneY = TRUE
            b_Rotate3dPhoneZ = TRUE
            
            g_bEmailSystemPaused = FALSE
            
            WHILE b_Rotate3dPhonePortrait = TRUE
            
                RotatePhoneToPortrait_and_Exit()    //This will terminate the thread...

            ENDWHILE
            */
            

            #if IS_DEBUG_BUILD

                PRINTSTRING ("appEmail exiting abnormally!")
                PRINTNL ()

            #endif


            g_b_Rotate3dPhonePortrait = FALSE
            g_b_Rotate3dPhoneLandscape = FALSE 


            g_bEmailSystemPaused = FALSE
            //RELEASE_TEXTURE_DICTIONARIES()

            //Embedded check in this for 2015070
            CellphoneFirstPersonHorizontalModeToggle(FALSE)

            TERMINATE_THIS_THREAD()



        ENDIF





        
            
    ENDWHILE
    
    
ENDSCRIPT


