

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "completionPercentage_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"
USING "Flow_Mission_Data_Public.sch"



  
// ********************************************************************************************************
// ********************************************************************************************************
// ********************************************************************************************************
//
//      MISSION NAME    :   appChecklist.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   An app that displays a simple breakdown of the player's game progress            
//                             
//                          
//
//
// ********************************************************************************************************
// ********************************************************************************************************
// ********************************************************************************************************

                                           

//CONST_INT MaxNumberOfRepeatSlots    10
CONST_INT EARLIEST_FIRST            0
CONST_INT LATEST_FIRST              1



INT OpenListSlot[SP_MISSION_MAX]


INT NumberOfEntriesInList = 0

INT ListCursorIndex = 0


INT i_Selected_Checklist



BOOL dpad_scroll_pause_cued = FALSE














 /*
 ENUM enumGrouping

    CP_GROUP_NO_GROUP, //0
    CP_GROUP_MISSIONS, //1
    CP_GROUP_MINIGAMES, //2
    CP_GROUP_ODDJOBS,   //3
    CP_GROUP_RANDOMCHARS, 
    CP_GROUP_RANDOMEVENTS,
    CP_GROUP_MISCELLANEOUS,
    CP_GROUP_FRIENDS

ENDENUM
*/
                                                                



ENUM enumAppPhases

    IN_PRELIM_CHOICE,
    IN_OPEN_MISSIONS_PHASE,
    IN_CHECKLIST_PHASE1,
    IN_CHECKLIST_PHASE2

ENDENUM


enumAppPhases eCurrent_Stream



//The order of the enums directly correlates to the order they appear on an "earliest first" list on the phone. Was thinking of sorting it alphabetically but missions may be 
//more important than micro-collection stats.
ENUM enumCheckListEntry

    CLE_MISSIONS,
    //CLE_MINIGAMES, 
    //CLE_ODDJOBS,
    CLE_HOBBIES,
    CLE_RANDOMCHARS,
    CLE_RANDOMEVENTS,
    //CLE_FRIENDS, 
    CLE_MISCELLANEOUS,  //Now calculates friends and misc together to match social club.
    //CLE_TESTBLANK,

    MAX_CHECKLIST_ENTRIES //Do not remove!

ENDENUM


INT CheckListSlot[MAX_CHECKLIST_ENTRIES]




 //Grouping enum for reference. These are used in the 100 percent completion tracker but crossover nicely so we can extract total numbers of each group available and the number 
 //currently completed.

 /*
 ENUM enumGrouping

    CP_GROUP_NO_GROUP, //0
    CP_GROUP_MISSIONS, //1
    CP_GROUP_MINIGAMES, //2
    CP_GROUP_ODDJOBS,   //3
    CP_GROUP_RANDOMCHARS, 
    CP_GROUP_RANDOMEVENTS,
    CP_GROUP_MISCELLANEOUS,
    CP_GROUP_FRIENDS

ENDENUM
*/
      




//Might need to be a saved global so we can keep track of each apps availability for each owner.
STRUCT structCheckListEntry
    

    TEXT_LABEL      CLE_LABEL              //the label of the checklist entry for displaying on the phone
    enumGrouping    CLE_GROUP
    INT             CLE_NUM_CURRENT_COMPLETE

ENDSTRUCT


INT SF_PlugInt_1, SF_PlugInt_2, i_Selected_Choice

SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex

BOOL b_GroupingsAvailable = FALSE


structCheckListEntry CheckListEntry[MAX_CHECKLIST_ENTRIES]

BOOL This_Grouping_Has_Been_Included[MAX_CHECKLIST_ENTRIES]






 //Open up for public use if necessary.
PROC Fill_CheckListEntry (enumCheckListEntry whichEntry, STRING whichEntryLabel, enumGrouping whichGrouping, INT whichCurrentNumberOfGroupingCompleted)
 

    CheckListEntry[whichEntry].CLE_LABEL = whichEntryLabel

    CheckListEntry[whichEntry].CLE_GROUP = whichGrouping

    CheckListEntry[whichEntry].CLE_NUM_CURRENT_COMPLETE = whichCurrentNumberOfGroupingCompleted

    IF CheckListEntry[0].CLE_GROUP = CP_GROUP_MISSIONS
        //unref hack
    ENDIF


ENDPROC








PROC Fill_CheckListEntries()

    //RecalculateResultantPercentageTotal() //from CompletionPercentage_Private.sch  //need to get temp_groups to see if any of the groupings have completed members.

    Fill_CheckListEntry (CLE_MISSIONS, "CELL_3001", CP_GROUP_MISSIONS, temp_Group_Num_of_Missions)

    //Fill_CheckListEntry (CLE_MINIGAMES, "CELL_3002", CP_GROUP_MINIGAMES, temp_Group_Num_of_Minigames)
    //Fill_CheckListEntry (CLE_ODDJOBS, "CELL_3003", CP_GROUP_ODDJOBS, temp_Group_Num_of_Oddjobs)

    Fill_CheckListEntry (CLE_HOBBIES, "CELL_3008", CP_GROUP_MINIGAMES, temp_Group_Num_of_Missions) 



    Fill_CheckListEntry (CLE_RANDOMCHARS, "CELL_3004", CP_GROUP_RANDOMCHARS, temp_Group_Num_of_RandomChars) //referred to as Strangers and Freaks!
    Fill_CheckListEntry (CLE_RANDOMEVENTS, "CELL_3005", CP_GROUP_RANDOMEVENTS, temp_Group_Num_of_RandomEvents)
    
    //Fill_CheckListEntry (CLE_FRIENDS, "CELL_3006", CP_GROUP_FRIENDS, temp_Group_Num_of_Friends)
    Fill_CheckListEntry (CLE_MISCELLANEOUS, "CELL_3007", CP_GROUP_MISCELLANEOUS, temp_Group_Num_of_Miscellaneous) 

                                                                          

ENDPROC






PROC Setup_Preliminary_Choice()


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_3301")

                       
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_3302")


    //CELL_3302

    NumberOfEntriesInList = 2 



    //Set scalefrom movie view state enum to Repeats screen and display associated buttons.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) //Set enum state and header to Check List
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_23")



    //Badger - Select Back combo
    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_227") //"Error!" - Other  
         


    ELSE

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0, 
            15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM ) //"Error!" - Other
       


    ENDIF


    eCurrent_Stream = IN_PRELIM_CHOICE


ENDPROC







PROC Place_Open_Missions_in_Slots()


    NumberOfEntriesInList = 0 //reset number of Repeats before filling list.


    UPDATE_CHECKLIST_ITEMS() //Call Ben's function to make sure open list is up to date.


    //Temp test data! 
    /*
    g_tChecklistItems[0] = "M_ARM1"
    g_tChecklistItems[1] = "M_EXL2"
    g_tChecklistItems[2] = "M_FBI3"

    g_iChecklistItemCount  = 3
    */


 
    INT slotIndex = 0


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)



    IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(slotIndex)),
                                            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_229") //On Active Mission


    ELSE

     
        WHILE slotIndex < (g_iChecklistItemCount)

            #if IS_DEBUG_BUILD

                PRINTSTRING("Populating sf ef slot ")
                PRINTINT(slotIndex)
                PRINTNL()
        
            #endif         


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(slotIndex)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_tChecklistItems[slotIndex])

            NumberOfEntriesInList ++ 
           


            OpenListSlot[slotIndex] = slotIndex


        

            slotIndex ++



        ENDWHILE


    ENDIF


        
    //Unref hack.

    IF OpenListSlot[1] = 99

    ENDIF

      




    //Set scalefrom movie view state enum to Repeats screen and display associated buttons.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18 ) //Set enum state and header to Open Missions.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_3301")


    IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
    OR g_iChecklistItemCount = 0 
        

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


        IF g_b_ToggleButtonLabels
        
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"Back" - Negative

        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //

        ENDIF



    ELSE

        //Badger - Select Back combo
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_227") //"SORT" - Other  
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)      


        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM ) //"SORT" - Other
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


        ENDIF


    ENDIF

     eCurrent_Stream = IN_OPEN_MISSIONS_PHASE

     
ENDPROC






























PROC Place_Names_in_ChecklistSlots()


    b_GroupingsAvailable = FALSE





    Fill_CheckListEntries() 


    NumberOfEntriesInList = 0 //reset number of Repeats before filling list.

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)

 
    INT slotIndex = 0



 
        IF g_ChecklistSortingMethod = EARLIEST_FIRST

            WHILE slotIndex < (ENUM_TO_INT(MAX_CHECKLIST_ENTRIES))

                //IF CheckListEntry[slotindex].CLE_NUM_CURRENT_COMPLETE > 0 //Only include check progress for any grouping if at least one member has been completed.
          

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("Populating sf slot ")
                        PRINTINT(slotIndex)
                        PRINTNL()
                
                    #endif         


                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(NumberOfEntriesInList)),
                                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, CheckListEntry[slotindex].CLE_LABEL)
                            


                    CheckListSlot[NumberOfEntriesInList] = SlotIndex


                    NumberOfEntriesInList ++ 

                 //ENDIF
                

                 slotIndex ++

            

            ENDWHILE

        ENDIF




        IF g_ChecklistSortingMethod = LATEST_FIRST


            INT LatestFirst_Index = 0


            //Need to work out how many slots need filled at the start by finding out how many groupings have at least one member.

            

            INT tempIndex = 0

            WHILE tempIndex < (ENUM_TO_INT(MAX_CHECKLIST_ENTRIES))


                This_Grouping_Has_Been_Included[tempIndex] = FALSE
                       

                tempIndex ++

            ENDWHILE
           



        
            LatestFirst_Index = ((ENUM_TO_INT(MAX_CHECKLIST_ENTRIES) - 1))


            WHILE slotIndex < (ENUM_TO_INT(MAX_CHECKLIST_ENTRIES))

       

                    IF CheckListEntry[LatestFirst_Index].CLE_NUM_CURRENT_COMPLETE > 0 //Only include check progress for any grouping if at least one member has been completed.
                
                        /*
                        #if IS_DEBUG_BUILD

                            PRINTSTRING("Populating sf slot ")
                            PRINTINT(slotIndex)
                            PRINTNL()
                    
                            PRINTSTRING("LFI ")
                            PRINTINT(LatestFirst_Index)
                            PRINTNL()

                            PRINTSTRING("Num of entries ")
                            PRINTINT(numberofEntriesInList)
                            PRINTNL()


                        #endif         
                        */

                        IF This_Grouping_Has_Been_Included[latestfirst_Index] = FALSE

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(NumberOfEntriesInList)),
                                                    (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,   
                                                 CheckListEntry[LatestFirst_Index].CLE_LABEL)
    
                            CheckListSlot[NumberOfEntriesInList] = LatestFirst_Index

                            NumberOfEntriesInList ++ 

                            This_Grouping_Has_Been_Included[latestfirst_Index] = TRUE


                        ENDIF





                    ENDIF                
                     
                                         

                        IF LatestFirst_Index > 0
                            LatestFirst_Index --
                        ENDIF
                    
               

                slotIndex ++



            ENDWHILE

        ENDIF

  



    IF NumberOfEntriesInList = 0 // No progress has been made whatsoever, fill slot 0 with appropriate message.

      LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                            "CELL_3092") //"No progress made".
    ELSE

        b_GroupingsAvailable = TRUE //Need a check that allows user to select entry or not. We don't want them to be able to select "No progress made".
        
    ENDIF



      




    //Set scalefrom movie view state enum to Repeats screen and display associated buttons.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) //Set enum state and header to Check List
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_23")



        //Badger - Select Back combo
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_227") //"SORT" - Other  
            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)      


        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                15, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM ) //"SORT" - Other
            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


        ENDIF


     

    eCurrent_Stream = IN_CHECKLIST_PHASE1



ENDPROC















PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
    
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF

    ENDIF
	
	// PC Scrollwheel support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
		
			IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()
		
		ENDIF
		
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
		
			 ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfEntriesInList 
                ListCursorIndex = 0
            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

		ENDIF
	
	ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfEntriesInList 

                ListCursorIndex = 0

            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

           
            
        ENDIF

    
    ENDIF

            
ENDPROC











PROC Get_Checklist_Entry_Values_For_Scaleform()



    //RecalculateResultantPercentageTotal() //from CompletionPercentage_Private.sch //Just in case any completions have been made inbetween choices being listed and one chosen.

 

    SWITCH INT_TO_ENUM (enumCheckListEntry, i_Selected_CheckList)

        CASE CLE_MISSIONS

            SF_PlugInt_1 = g_Group_Num_of_Missions      //Total available missions...
            SF_PlugInt_2 = temp_Group_Num_of_Missions   //Completed so far...
    
        BREAK


        /* 
        CASE CLE_MINIGAMES

            SF_PlugInt_1 = g_Group_Num_of_Minigames      //Total available missions...
            SF_PlugInt_2 = temp_Group_Num_of_Minigames   //Completed so far...
    
        BREAK


        CASE CLE_ODDJOBS

            SF_PlugInt_1 = g_Group_Num_of_Oddjobs      //Total available missions...
            SF_PlugInt_2 = temp_Group_Num_of_Oddjobs   //Completed so far...
    
        BREAK
        */



        CASE CLE_HOBBIES //Now an amalgamation of oddjobs and minigames. See bugs 1534381 and 1534353

            SF_PlugInt_1 = g_Group_Num_of_Minigames + g_Group_Num_of_Oddjobs
            SF_PlugInt_2 = temp_Group_Num_of_Minigames + temp_Group_Num_of_Oddjobs

        BREAK



        CASE CLE_RANDOMCHARS

            SF_PlugInt_1 = g_Group_Num_of_RandomChars 
            SF_PlugInt_2 = temp_Group_Num_of_RandomChars

        BREAK

        
        CASE CLE_RANDOMEVENTS

            SF_PlugInt_1 = g_Group_Num_of_RandomEvents
            SF_PlugInt_2 = temp_Group_Num_of_RandomEvents

        BREAK


        /*
        CASE CLE_FRIENDS //Combined with miscellaneous to match social club checklist.

            SF_PlugInt_1 = g_Group_Num_of_Friends
            SF_PlugInt_2 = temp_Group_Num_of_Friends

        BREAK
        */

        
        CASE CLE_MISCELLANEOUS 

            SF_PlugInt_1 = (g_Group_Num_of_Miscellaneous  + g_Group_Num_of_Friends)
            SF_PlugInt_2 = (temp_Group_Num_of_Miscellaneous + temp_Group_Num_of_Friends)

        BREAK




        DEFAULT

            SF_PlugInt_1 = 0
            SF_PlugInt_2 = 0
        
        BREAK

    ENDSWITCH


ENDPROC














PROC Check_for_Preliminary_Choice_Selection()



    IF g_InputButtonJustPressed = FALSE
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - Repeat selected.


           

            Play_Select_Beep()


            g_InputButtonJustPressed = TRUE

    
       
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
        
            Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
            
                WAIT (0)

                #if IS_DEBUG_BUILD

                    cdPrintstring("appCheckList - Waiting on return value from scaleform. 102")
                    cdPrintnl()

                #endif

            ENDWHILE

            i_Selected_Choice = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)


   



            #if IS_DEBUG_BUILD
                PRINTNL()
                PRINTSTRING ("Preliminary checklist app choice was ")
                PRINTINT (i_Selected_Choice)
                PRINTNL()
            #endif


            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_COMPLEXAPP
                

                #if IS_DEBUG_BUILD
                    
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 203. appChecklist assigns PDS_COMPLEXAPP")
                    cdPrintnl()   
                
                #endif

                
                IF i_Selected_Choice = 0 //List open missions here!

              
                    eCurrent_Stream = IN_OPEN_MISSIONS_PHASE

                    Place_Open_Missions_in_Slots()


                ELSE

                    eCurrent_Stream = IN_CHECKLIST_PHASE1

                    Place_Names_in_ChecklistSlots()

                ENDIF

            ENDIF



        ENDIF
    ENDIF


ENDPROC






PROC Check_For_Checklist_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    
    IF g_InputButtonJustPressed = FALSE
         IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action. Display details...




            Play_Select_Beep()


            g_InputButtonJustPressed = TRUE



            //i_Selected_CheckList  =  ENUM_TO_INT (CheckListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

            Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


            


            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
            
                WAIT (0)

                #if IS_DEBUG_BUILD

                    cdPrintstring("appChecklist - Waiting on return value from scaleform. 103")
                    cdPrintnl()
                   
                #endif

            ENDWHILE

   

            INT i_TestInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(Choice_ReturnedSFIndex)

            i_Selected_Checklist = (CheckListSlot[i_TestInt])


            #if IS_DEBUG_BUILD

                PRINTSTRING("The returned plug-in value was ")
                PRINTINT (i_TestInt) 

                PRINTNL()
                PRINTSTRING ("Contents of selected checklist slot is: ")
                PRINTINT (i_Selected_CheckList)
                PRINTNL()
            #endif

  

            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_COMPLEXAPP
                
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 90. appChecklist assigns PDS_COMPLEXAPP")
                    cdPrintnl()   
                
                #endif

            ENDIF

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 18)


            //unref hack
            IF i_Selected_Checklist = 1
            ENDIF
            





            Get_Checklist_Entry_Values_For_Scaleform()


            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)

                   
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_3206") //Available...
                
                ADD_TEXT_COMPONENT_INTEGER (SF_PlugInt_1)

                END_TEXT_COMMAND_SCALEFORM_STRING()

                
            END_SCALEFORM_MOVIE_METHOD()

                                           


            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)


                   
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_3101") //Complete...
                
                ADD_TEXT_COMPONENT_INTEGER (SF_PlugInt_2)

                END_TEXT_COMMAND_SCALEFORM_STRING()

                
            END_SCALEFORM_MOVIE_METHOD()




            /*
            INT i_Percentage


            i_Percentage = ((SF_PlugInt_2 * 100) / SF_PlugInt_1)


            //Percentage calculation needs done here.
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_3091")
                
                
                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_Percentage) //Calculation result.

                 //Traffic light colouring
                IF i_Percentage < 61                

                    //Red
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                    //Bronze
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(165)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(131)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(27)



            
                ENDIF

                IF i_Percentage > 60
                AND i_Percentage < 100
                        
                    //Amber
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(166)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                    //Silver
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(215)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(216)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(247)



                ENDIF

                IF i_Percentage = 100                

                    //Green
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
            
                    //Gold
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(238)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(220)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(66)



                ENDIF

         
            END_SCALEFORM_MOVIE_METHOD()
            */

       
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18)  //19 has been removed?

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", CheckListEntry[i_Selected_CheckList].CLE_LABEL)


           



            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive     

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


            IF g_b_ToggleButtonLabels
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"Back" - Negative
            ELSE
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
            ENDIF

            
            eCurrent_Stream = IN_CHECKLIST_PHASE2

        
        ENDIF    
    
  









    
     

    ENDIF
 
    
        

ENDPROC






///*
//PROC OLDCheck_For_Checklist_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.
//
//    
//    IF g_InputButtonJustPressed = FALSE
//         IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action. Display details...
//
//
//
//
//            Play_Select_Beep()
//
//
//            g_InputButtonJustPressed = TRUE
//
//
//
//            //i_Selected_CheckList  =  ENUM_TO_INT (CheckListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])
//
//            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
//
//            Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
//
//
//            
//
//
//            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
//            
//                WAIT (0)
//
//                #if IS_DEBUG_BUILD
//
//                    cdPrintstring("appChecklist - Waiting on return value from scaleform. 103")
//                    cdPrintnl()
//                   
//                #endif
//
//            ENDWHILE
//
//   
//
//            INT i_TestInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(Choice_ReturnedSFIndex)
//
//            i_Selected_Checklist = (CheckListSlot[i_TestInt])
//
//
//            #if IS_DEBUG_BUILD
//
//                PRINTSTRING("The returned plug-in value was ")
//                PRINTINT (i_TestInt) 
//
//                PRINTNL()
//                PRINTSTRING ("Contents of selected checklist slot is: ")
//                PRINTINT (i_Selected_CheckList)
//                PRINTNL()
//            #endif
//
//  
//
//            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.
//
//                g_Cellphone.PhoneDS = PDS_COMPLEXAPP
//                
//                #if IS_DEBUG_BUILD
//
//                    cdPrintnl()
//                    cdPrintstring("STATE ASSIGNMENT 90. appChecklist assigns PDS_COMPLEXAPP")
//                    cdPrintnl()   
//                
//                #endif
//
//            ENDIF
//
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 19)
//
//
//            //unref hack
//            IF i_Selected_Checklist = 1
//            ENDIF
//            
//
//
//
//
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(19)), (TO_FLOAT(0)),
//                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_3090") //"Details"
//
//
//            Get_Checklist_Entry_Values_For_Scaleform()
//
//
//            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
//
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(19)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)
//
//                   
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_3206") //Available...
//                
//                ADD_TEXT_COMPONENT_INTEGER (SF_PlugInt_1)
//
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//
//                
//            END_SCALEFORM_MOVIE_METHOD()
//
//                                           
//
//
//            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
//
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(19)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)
//
//
//                   
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_3101") //Complete...
//                
//                ADD_TEXT_COMPONENT_INTEGER (SF_PlugInt_2)
//
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//
//                
//            END_SCALEFORM_MOVIE_METHOD()
//
//
//
//
//
//            INT i_Percentage
//
//
//            i_Percentage = ((SF_PlugInt_2 * 100) / SF_PlugInt_1)
//
//
//            //Percentage calculation needs done here.
//            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
//
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(19)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_3091")
//                
//                
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i_Percentage) //Calculation result.
//
//                 //Traffic light colouring
//                IF i_Percentage < 61                
//
//                    //Red
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//
//                    //Bronze
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(165)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(131)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(27)
//
//
//
//            
//                ENDIF
//
//                IF i_Percentage > 60
//                AND i_Percentage < 100
//                        
//                    //Amber
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(166)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//
//                    //Silver
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(215)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(216)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(247)
//
//
//
//                ENDIF
//
//                IF i_Percentage = 100                
//
//                    //Green
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
//                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//            
//                    //Gold
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(238)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(220)
//                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(66)
//
//
//
//                ENDIF
//
//         
//            END_SCALEFORM_MOVIE_METHOD()
//
//
//       
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 19)  //19 has been removed?
//
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", CheckListEntry[i_Selected_CheckList].CLE_LABEL)
//
//
//           
//
//
//
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
//                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Positive     
//
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
//                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
//
//            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
//
//
//            IF g_b_ToggleButtonLabels
//                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
//                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"Back" - Negative
//            ELSE
//                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
//                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
//            ENDIF
//
//            
//            eCurrent_Stream = IN_CHECKLIST_PHASE2
//
//        
//        ENDIF    
//    
//  
//
//
//
//
//
//
//
//
//
//    
//        /*Comment out checklist sort...
//        //User changes sorting method.
//        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))
//    
//    
//            Play_Select_Beep()
//
//
//            g_InputButtonJustPressed = TRUE
//
//
//            #if IS_DEBUG_BUILD
//                PRINTNL()
//                PRINTSTRING ("APPCHECKLIST - Sorting list")
//                PRINTNL()
//            #endif
//
//
//            IF g_CheckListSortingMethod = EARLIEST_FIRST
//
//                g_CheckListSortingMethod = LATEST_FIRST
//
//            ELSE
//
//                g_ChecklistSortingMethod = EARLIEST_FIRST
//
//            ENDIF
//
//
//            Place_Names_in_CheckListSlots()
//
//
//
//         ENDIF
//         */
//     
//
//    ENDIF
// 
//    
//        
//
//ENDPROC
//*/






PROC Cleanup_and_Terminate()


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("APPCHECKLIST - appChecklist exiting normally...")
        PRINTNL()

    #endif


    TERMINATE_THIS_THREAD()


ENDPROC 































SCRIPT





    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
 

    //RecalculateResultantPercentageTotal() //from CompletionPercentage_Private.sch  //need to get temp_groups to see if any of the filters have been complete.



    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)




    Fill_CheckListEntries()



                                                                
    Setup_Preliminary_Choice()






    

    
    WHILE TRUE

        WAIT(0)
        

        
        /*
        IF eCurrent_Stream = IN_PRELIM_CHOICE

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.54, "STRING", "Prelim")
            
        ENDIF   


        IF eCurrent_Stream = IN_CHECKLIST_PHASE1

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.54, "STRING", "Phase 1")

        ENDIF

        
        IF eCurrent_Stream = IN_CHECKLIST_PHASE2

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.54, "STRING", "Phase 2")

        ENDIF
        */


        






        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        
            SWITCH g_Cellphone.PhoneDS
             
            

                CASE PDS_RUNNINGAPP

                    //IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE() //Investigate why this is forbidden?
                                             
                        Check_for_List_Navigation()    
                        
                        
                        IF eCurrent_Stream = IN_PRELIM_CHOICE                                  
                                                 

                            Check_for_Preliminary_Choice_Selection()

                        ENDIF



                    //ENDIF

                BREAK
                                                                                                 
               

                CASE PDS_COMPLEXAPP



                    IF eCurrent_Stream = IN_CHECKLIST_PHASE1

                        Check_for_List_Navigation()    

                        IF b_GroupingsAvailable
                            Check_For_Checklist_Selection()
                        ENDIF

                    ENDIF

                
                    IF eCurrent_Stream = IN_OPEN_MISSIONS_PHASE

                        Check_For_List_Navigation()

                    ENDIF






                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    


                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE


                        IF eCurrent_Stream = IN_CHECKLIST_PHASE1
                        OR eCurrent_Stream = IN_OPEN_MISSIONS_PHASE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)
                          
                            Setup_Preliminary_Choice()
               

                            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("STATE ASSIGNMENT 91. appChecklist assigns PDS_RUNNINGAPP")
                                    cdPrintnl()   
                                #endif

                            ENDIF

                        ENDIF

                        
                        IF eCurrent_Stream = IN_CHECKLIST_PHASE2

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18)
                          
                            Place_Names_in_ChecklistSlots()
            
                        ENDIF


                    ENDIF

                BREAK
               




                DEFAULT

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("AppChecklist in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  



        





       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Repeats list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT

/*
[CELL_3002]
Minigames

[CELL_3003]
Oddjobs


[CELL_3202]
- Minigames Available: ~1~

[CELL_3203]
- Oddjobs Available: ~1~


*/
