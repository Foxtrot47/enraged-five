

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT   USE_TU_CHANGES  0


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"

  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appExtraction.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Used to launch Extraction app from cellphone. 
//            
//                            
//                                     
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           
BOOL b_InitialExtractionSetup = FALSE




PROC Set_Up_Extraction_Buttons_Yes_No()



    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"YES" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative


    ELSE 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


     
ENDPROC





PROC Set_Up_Extraction_Buttons_Quit()



    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"YES" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative


    ELSE 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


     
ENDPROC







//PURPOSE: This fills the scaleform view state with the co-ordinate sets.
PROC Scaleform_Set_Extraction_Details()

    //Temo! Trackify view state used until we get proper scaleform!
     /*
     BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //System 

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REMOVE_TRACKIFY_TARGET)

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (-99)  //Negative angle removes target.
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)


        ELSE

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(45))  //Trackify Screen
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(10.0))

    
        ENDIF

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (1) //Skip the loading screen

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip.
        ELSE
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Show arrow on target blip.
        ENDIF


        IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (10.0)
        ENDIF


    END_SCALEFORM_MOVIE_METHOD()
                
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))  
    
    */
    

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_4")





    /*
    { Added for new Extraction app requested in TODO 2574720 }
    [CELL_EXTRACT]
    Extraction

    [CELL_EXTBYOU]
    You

    [CELL_EXTBTARG]
    Target

    [CELL_EXTPRES]
    Your Location

    [CELL_BODYG]
    Bodyguard
    */


    //Allows the colours used for each vector component to be set by MP. url:bugstar:2596571
        //HUD_COLOURS g_hc_PresX, g_hc_PresY, g_hc_PresZ
        //HUD_COLOURS g_hc_BodygX, g_hc_BodygY, g_hc_BodygZ


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 24)


    
    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (24) //Specify view state to be filled.

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0) //As this is a full screen app, the slot to be filled is always zero.

    
        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW)

            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Should be (FALSE) according to the hub but that's wrong. This actually means we only want one column to display. This was used for single column onlu
            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_EXTPRES") "Your Location - This was used for the single column setup.




            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Contrary to hub info, this will display both columns.


            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_EXTBYOU") //2603763 "You" - needs to be short to fit onscreen.

            //Hud colours.
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresX))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.x))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresY))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.y))
            
            
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresZ))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.z))





            //New for 2603763. This TODO requested that we also display the nearest bodyguard when in the President view.
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_BODYG") //"Bodyguard"

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygX))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.x))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygY))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.y))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygZ))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.z))



   
        ELSE
            
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Should be TRUE according to the hub. This actually means we want both columns to display.


            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_EXTBTYOU"))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_EXTBYOU")
    
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygX))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.x))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygY))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.y))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_BodygZ))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionBodyguardVec.z))



            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_EXTBTARG"))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_EXTBTARG")

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresX))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.x))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresY))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.y))

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (ENUM_TO_INT(g_hc_PresZ))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(g_v_ExtractionPresidentVec.z))

        ENDIF

    END_SCALEFORM_MOVIE_METHOD()

    
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 24, TO_FLOAT(1)) 


    #if IS_DEBUG_BUILD

        cdPrintstring ("AppExtraction - Updating Scaleform.")
        cdPrintnl()

    #endif


             

ENDPROC







PROC Initialise_Extraction_App()
    
    Scaleform_Set_Extraction_Details()

    #if IS_DEBUG_BUILD

        cdPrintstring ("AppExtraction - S_P_D appExtraction initialisation...")
        cdPrintnl()

    #endif

ENDPROC






PROC Refresh_Extraction_App()

    Scaleform_Set_Extraction_Details()


    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("AppExtraction -  Refresh_Extraction_App triggered!")
        cdPrintnl()

    #endif

ENDPROC












PROC Cleanup_and_Terminate()


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("AppExtraction - appExtraction exiting normally.")
        PRINTNL()

        cdPrintnl()
        cdPrintstring ("AppExtraction - S_P_D appExtraction exiting normally.")
        cdPrintnl()

    #endif



    TERMINATE_THIS_THREAD()


ENDPROC 












SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    

    IF b_InitialExtractionSetup = FALSE

        Set_Up_Extraction_Buttons_Quit()

        Initialise_Extraction_App()

        b_InitialExtractionSetup = TRUE

        SETTIMERA(0) //We don't want to refresh the two sets of co-ordinates every frame. That's too expensive.

    ENDIF

    
     
    WHILE TRUE

        WAIT(0)
        

     
        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        
            SWITCH g_Cellphone.PhoneDS
             
            

                CASE PDS_RUNNINGAPP
                                           
                    //Temp widget controlled debug which displays the vectors passed via public functions SET_EXTRACTION_BODYGUARD_VECTOR and SET_EXTRACTION_PRESIDENT_VECTOR.                       
                    #if IS_DEBUG_BUILD
                    IF g_Cellphone_Onscreen_State_Debug
                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP )
                            
                            /*
                            [CELL_EXTBYOU]
                            You

                            [CELL_EXTBTARG]
                            Target

                            [CELL_EXTPRES]
                            Your Location
                            */


                            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW) //If the player is playing as the President we want to display those coords only and have Your Location as the title.

                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.65, 0.12, "STRING", "President View")  
                                
                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.75, 0.22, "STRING", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_EXTPRES"))  

                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.30, "CELL_500", g_v_ExtractionPresidentVec.x, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.38, "CELL_501", g_v_ExtractionPresidentVec.y, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.46, "CELL_502", g_v_ExtractionPresidentVec.z, 1)

                            ELSE


                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.65, 0.12, "STRING", "Bodyguard View")  

                                //Playing as a bodyguard, display both columns and have the Bodyguard marked as You and the President marked as the target.
                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.75, 0.22, "STRING", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_EXTBYOU")) 

                                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.30, "CELL_500", g_v_ExtractionBodyguardVec.x, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.38, "CELL_501", g_v_ExtractionBodyguardVec.y, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.46, "CELL_502", g_v_ExtractionBodyguardVec.z, 1)
                                

                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.22, "STRING", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_EXTBTARG")) 

                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.30, "CELL_500", g_v_ExtractionPresidentVec.x, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.38, "CELL_501", g_v_ExtractionPresidentVec.y, 1)
                                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.46, "CELL_502", g_v_ExtractionPresidentVec.z, 1)

                            ENDIF


                        ENDIF
                    ENDIF
                    #endif
                                           
                                                                         
                    IF b_InitialExtractionSetup = TRUE
                    AND TIMERA() > 1500

                        Refresh_Extraction_App()

                        SETTIMERA(0) //reset the timer. We don't want to refresh every frame. It's too expensive to update scaleform. 

                    ENDIF
 
         
                BREAK

                                                                                                                             

                CASE PDS_COMPLEXAPP


                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

               

                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 23198. AppExtraction assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                    ENDIF

                BREAK
               


                DEFAULT

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("AppExtraction in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  


    

       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Repeats list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT


