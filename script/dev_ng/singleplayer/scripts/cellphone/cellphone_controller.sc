USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"                                
USING "commands_pad.sch"
USING "cellphone_public.sch"                                                 
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "stack_sizes.sch"


USING "ambient_Speech.sch"

USING "flow_public_core_override.sch"
USING "cutscene_public.sch"

#if IS_DEBUG_BUILD
USING "commands_debug.sch"
USING "commands_audio.sch"
USING "completionPercentage_public.sch"
USING "script_camera.sch"
#endif


USING "friends_public.sch"
USING "ply_to_ply_calls.sch"
USING "cellphone_movement.sch"
USING "director_mode_public.sch"

USING "commands_socialclub.sch"



//USING "Director_Prop_Placement.sch"


                                


                                                          




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   cellphone_controller.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Checks for use of cellphone pull out by player in main game.
//                          This script should be launched from main.sc and run continuously
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





 //Debug widget declarations.
#if IS_DEBUG_BUILD
            
    //Start of Director Mode prop placement test vars.

    ENTITY_INDEX TestPropEntity, PlacementDiscEntity


    MODEL_NAMES CurrentlySelectedPropModel = prop_bench_09


    VECTOR v_player_pos
    FLOAT v_player_heading

    //VECTOR v_LastPropPlacement_Position


    



    ENUM enumPROP_PLACEMENT_STAGE

        PROP_STAGE_Dormant,
        PROP_STAGE_InitialSetUp,
        PROP_STAGE_PropCycling,
        PROP_STAGE_PropInHand,
        PROP_STAGE_PropPlacement,
        PROP_STAGE_PropRemoval,
        PROP_STAGE_SavingPropScene,
        PROP_STAGE_ExitAndCleanup

    ENDENUM

    
    enumPROP_PLACEMENT_STAGE eCurrentPropStage



    VECTOR v_PlacementDiscOffset, v_ThisPropOffset





    CONST_INT i_NumberOfModelsToCycle 4











    //This is the array of possible selectable props. Expand to a struct with full data.
                //hash of model name
                //Label Reference
                //Category enum
                //Adjustment Z value for height
                //Disc placement and type.
    //INT PropModelArrayHash[i_NumberOfModelsToCycle]
    INT i_CurrentModelIndex = 0 //Where are we in the entire cycle of selectable prop models...

    
    //Used to categorise props
    ENUM ePROP_CATEGORY 

        DM_PROPCATEGORY1,
        DM_PROPCATEGORY2,
        DM_PROPCATEGORY3


    ENDENUM


    


    STRUCT structSelectableProps


        INT                             PropModelHash

        TEXT_LABEL_23                   PropTextLabel
        ePROP_CATEGORY                  PropCategory
    
    
    ENDSTRUCT
    
    
     

    structSelectableProps   SelectablePropArray[i_NumberOfModelsToCycle]
    VECTOR v_InHandPropRotation
 




    ENUM ePLACED_PROP_STATUS_AT_THIS_POSITION

        PROP_POSITION_VACANT,
        PROP_POSITION_FILLED

    ENDENUM



    STRUCT structPlacedProp

        ENTITY_INDEX PropEntityIndex
        INT     PropModelHash
        VECTOR  PropRotation
        VECTOR  PropPlacementVector
        ePLACED_PROP_STATUS_AT_THIS_POSITION PositionStatus

    ENDSTRUCT
   

    //Will need to have an array of props that *have* been placed. This will then need saved out.
                //hash of model name
                //Rotation
                //Placement Vector



    //Placed Prop Work. Will need to have a field in the struct of data associated with each index that says whether it has been filled with a prop or not.
    //This will allow us to put a new prop into a position that has been vacated by a deleted prop rather than always at the end of the list. Just like the textmsg system works.
    
    CONST_INT i_MAX_NUMBER_OF_PROPS_ALLOWED 5


    structPlacedProp PlacedProps[i_MAX_NUMBER_OF_PROPS_ALLOWED]



 
    INT i_TotalNumberOfPlacedModels 
    
    INT i_InVicinityOfThisPlacedPropPosition = -99

    VECTOR v_ThisPlacedProp
    FLOAT f_VicinityCheckRange = 3.0
    INT thisframeVicIndex = 0


    SCALEFORM_INDEX SF_PropPlacementMovieIndex









    //End of Director Mode prop placement test vars.


















    //BOOL TestBorderTexture = FALSE


    //CAMERA_INDEX TestCam



    //INT TestRingtone

    //Comment in to test 731786
    //INT TestInt = 0

    /*
    INT i_ZitID = -99

    TEXT_LABEL_23 ZitSong
    TEXT_LABEL_23 ZitArtist

    TEXT_LABEL_31 ZitPhoneBlock = "TRACKID"
    */


    //TEXT_LABEL_23 testcp


    //BOOL b_BlockSigniferMovieFromDrawing = FALSE


    VECTOR g_Test3dPhonePosVec = <<117.2, -53.3, -113.0>>



    BOOL DebugPositionToggle = FALSE
    BOOL RunTECToggle = FALSE
    //INT  DebugCellphoneGui = 1 //Default startup gui.
    //INT  DebugWidgetToneSelection = 1
    //BOOL b_EnableToneSelection = FALSE
    //FLOAT db_signifier_posX = 20.0
    //FLOAT db_signifier_posY = 167.0

    FLOAT db_preview_signifier_posX = 0.07
    FLOAT db_preview_signifier_posY = 0.755

    //BOOL b_EnableFeedEntry = FALSE


    BOOL b_TickToRequestDisableEveryFrameOption


    BOOL b_DebugContactForMichael, b_DebugContactForFranklin, b_DebugContactForTrevor
    BOOL b_DebugContactForEveryone, b_DebugContactForVirtual


    BOOL b_InitiateIncomingChatCall, b_InitiateOutgoingChatCall, b_ChatFinished, b_HangUpTest


    BOOL b_CharCallTest, b_CharCallReplyTest, b_PlayerCallTest, b_PlayerCallReplyTest, b_JobOfferCallTest
    BOOL b_PlayerConvTest, b_PlayerRandomTest
    BOOL b_PlayerMultipartTest, b_PlayerMultipartTestCall, b_PlayerMultipartWithRepliesTestCall
    BOOL b_PlayerFromLineTest, b_PlayerSingleLineTest
    BOOL b_DisplayGameType,  b_Switch_to_MultiplayerPhone, b_Switch_to_SingleplayerPhone

    BOOL b_AddTestContacts

    //BOOL b_Cellphone_Cam_Auto_Continue_On_Toggle //Don't check this widget in... it can interfere with missions if used incorrectly.

    BOOL b_DebugSaveGameTest

    BOOL b_DebugConvState

    BOOL b_PurgeMPTextList, b_DisplayTestStructData

    BOOL b_SendCamHelpText, b_RemoveSendCamHelpText


    BOOL b_GetResumptionLabel, b_ResumeConversation
    TEXT_LABEL_23 TestResumptionLabel

    //BOOL b_AutoplayAnswer



    BOOL b_PlaySimpleConversation
    TEXT_WIDGET_ID widgetID_ConvBlock
    TEXT_WIDGET_ID widgetID_ConvRoot



    structPedsForConversation MyLocalPedStruct, TestLocalPedstruct


    //FE_WARNING_FLAGS iButtonBits


#endif



//Signifiers

SCALEFORM_INDEX SF_MovieSignifiersIndex

BOOL b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE

//3624701 - Added these for shunting over MP signifier in the beacon position if the big expanded radar is onscreen
FLOAT ExpandedRadarManipulator_X = 0.0
FLOAT ExpandedRadarManipulator_Y = 0.0



//FLOAT signifier_posX[2], signifier_posY[2] //The movie is always drawn in the same place, these control the offsets.

BOOL b_Allow_Drawn_Or_Remove_to_Check_For_Update


//INT IconTest = 26


INT BufferedTextDecision = 0


BOOL Read_Auto_TextLaunch[4] 

INT Read_Auto_TextLaunchSentTime[4]
TIME_DATATYPE NetRead_Auto_TextLaunchSentTime[4]


INT Read_Auto_TextLaunchCurrentTime[4]

INT i_Read_Auto_TextLaunchDifference
CONST_INT Read_Auto_TextLaunchTimeoutThreshold 300000 //Should be five minutes


INT i_FeedItemId


//ActivatableFeedType CellphoneAppAutoLaunchType = ACT_APP_NONE  - Moved to cellphone globals.




INT Post_Save_PauseSentTime
TIME_DATATYPE NetPost_Save_PauseSentTime

INT Post_Save_PauseCurrentTime

INT i_Post_Save_PauseDifference


BOOL BufferedText_PostswapPause_Ongoing = FALSE





PHOTO_OPERATION_STATUS PicProcessStatus 
eLowQualityPhotoSetting QualityToAttempt



INT PostSwap_Buffer_StartTime, PostSwap_Buffer_CurrentTime

INT DpadElongated_StartTime, DpadElongated_CurrentTime, DpadElongated_Difference

BOOL b_DpadElongatedSuccessfullyExpired = FALSE

TIME_DATATYPE Net_Postswap_Buffer_StartTime

CONST_INT PostSwap_Buffer_TimeThreshold 7000

INT PostSwap_Buffer_TimeDifference

BOOL BufferedContact_PostswapPause_Ongoing

enumCharacterList BufferOwner


BOOL b_AutoJoblistLaunchCheck = FALSE // Used to make joblist auto launch check nice and tidy.


FLOAT SafeZoneSize, SigDrawPosX, SigDrawPosY

FLOAT ControlSafeZoneSize = 0.95
FLOAT ControlSigDrawPosX = 0.07
FLOAT ControlSigDrawPosY = 0.755
FLOAT SafeZoneManipulator


INT i_SleepModeStartTime, i_SleepModeEndTime
INT i_ValueOfStatWhenSleepWasBegun = 0
BOOL b_CheckingSleepModeDurationForStatUpdate = FALSE

//added for warning screen bug:2414624




//Marketing Email Work.
BOOL b_DisplayedFeedMessageThisSession = FALSE //2697795

ENUM eGetNewMarketingEmailStage

    GNM_DORMANT,
    GNM_LAUNCH_RETRIEVAL,
    GNM_RETRIEVAL_ONGOING,
    GNM_RETRIEVAL_FINISHED

ENDENUM


eGetNewMarketingEmailStage GNMCurrentStage = GNM_DORMANT
eEMAIL_RETRIEVAL_STATUS MarketingCurrentRetrievalStatus
INT tempPosixStat = 9999





PROC DrawSignifierMovie()

	
	Int MovieAlpha = 255 //3618017 - Scott Butchard requested some control over the alpha for the beacon signifier so adding this in.  By default the signifiers will be fully opaque. 




    // KGMINDICATOR
    IF (g_forceDisplayPlayerInvitesIndicator)

        ControlSigDrawPosX = -0.055
        ControlSigDrawPosY =  0.745


        SafeZoneSize = GET_SAFE_ZONE_SIZE()
        SafeZoneManipulator = ((100 * ControlSafeZoneSize) - (100 * SafeZoneSize))

        SigDrawPosX = (ControlSigDrawPosX + (SafeZoneManipulator * 0.005))
        SigDrawPosY = (ControlSigDrawPosY - (SafeZoneManipulator * 0.005))


        //For Debug, use these two lines below and comment in draw_debug_Scaleform_Signifier_Position() call.
        //db_signifier_posX = (db_preview_signifier_posX + (SafeZoneManipulator * 0.005))
        //db_signifier_posY = (db_preview_signifier_posY - (SafeZoneManipulator * 0.005))


        IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()

            SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
            SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)

            IF IS_PC_VERSION() //2199109
                ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9()
            ENDIF
            
            DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, SigDrawPosX, SigDrawPosY,  0.27,  0.45, 255, 255, 255, MovieAlpha)
            
            //For Debug, use this line below and comment in draw_debug_Scaleform_Signifier_Position()
            //DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, db_signifier_posX, db_signifier_posY,  0.27,  0.45, 255, 255, 255, 255)

            SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
            SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
        
        ENDIF

        // END KGMINDICATOR


    ELSE
        
        
        //Work for 2227208
        IF IS_PC_VERSION()

            IF g_PlayerBlipsData.bBigMapIsActive //If the expanded map is active, we shouldn't be in a cutscene

                b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE //We need to draw the PUSH_TO_TALK signifier when the radar is expanded, let's draw all signifiers then.
                
            ELSE

                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE) //If this is set prevent any signifier drawing in the cutscene.


                    IF IS_BIT_SET (Bitset_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)

                        b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE is set in PC build but PUSH_TO_TALK bit is set. Drawing sig movie. See 2254837.")
                            cdPrintnl()

                        #endif

                    ELSE

                        b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = TRUE

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - Checking prevention of signifier due to sig disable bit in PC build. See 2254837.")
                            cdPrintnl()

                        #endif

                    ENDIF


                ELSE

                    b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE

                ENDIF

            ENDIF

        ELSE  //On other platforms, we prevent drawing if the cutscene disable bit has been set or the expanded radar is up. PUSH_TO_TALK is PC only.
        
            IF g_PlayerBlipsData.bBigMapIsActive
            OR IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE)

                b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = TRUE                                

            ELSE
			
                b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE

            ENDIF

        ENDIF
		
		//New Logic for 3615927. Need to draw the signifier movie if the Beacon is displayed even when the phone is disabled. Normally, no signifiers or alerts draw if the phone is disabled as they're part of cellphone_alert_popup.
		IF IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()
			b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = TRUE  
		ENDIF
		
		
		IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)
		
			MovieAlpha = 125 // 3618017 - Scott Butchard requested some control over the alpha for the beacon signifier so making this value lower to make the cellphone_alert_popup movie more transparent when the signifier is present. 
			//This will make all signifiers have the same transparency this frame. Tough. We don't have individual control over each alert. The passed transparency value impacts all members of that movie.
			//UPDATE: It would seem the alpha component has no impact. UI will need to make a particular alert alpha-ed or not. E.g the Sleep Mode signifier is semi-transparent in SP, but the beacon originally was fully opaque.
		
			IF b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = TRUE  
				b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = FALSE
				#if IS_DEBUG_BUILD
	                cdPrintString("CELLPHONE_CONT - Signifier move draw logic IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY() is true but need to draw Beacon! Redacting PreventDrawing state.")
	                cdPrintnl()
	            #endif
			ENDIF
			
		ENDIF
		//End of new logic for 3615927 and 3618017
            
			
			
        /* Debug prints only. No check in!
        IF b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific = TRUE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "ExTRUE_P")  
        ELSE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "ExFALSE_P") 
        ENDIF


        IF IS_MINIMAP_RENDERING()
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.45, 0.24, "STRING", "MMap_R")  
        ELSE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.45, 0.24, "STRING", "MMap_NoR")  
        ENDIF


        IF IS_RADAR_HIDDEN()
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.30, 0.14, "STRING", "Rad_Hid")  
        ELSE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.30, 0.14, "STRING", "Rad_Shown") 
        ENDIF
        */



        IF IS_MINIMAP_RENDERING() //Only draw the movie if the minimap radar is displaying. Should fix lots of MP bugs. 2055680 for example.
        IF NOT IS_RADAR_HIDDEN()
        
            IF NOT b_PreventDrawingWhilstRadarIsExpanded_PlatformSpecific //Prevents any signifiers drawing under the expanded radar in MP in non_PC builds. PC needs to draw these..
                
                //Now moved to platform specific checks above because of 2227208. The expanded radar disables signifiers but we need to bypass this for PC push-to-talk.
                //IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE) //MP bug 2049951 can impact this!  //Brenda fixed in CL 6307015 for dev_net.

                    //IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY() //Commenting out for 3615927 fix. Replacing with a compound check that will set b_PreventDrawing with a redact check for the beacon displaying.
                        
                           IF NOT IS_CELLPHONE_CAMERA_IN_USE()                
                        
                                IF IS_PLAYER_PLAYING(PLAYER_ID())

                                    IF NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID())

                                            //488121
                                            SET_SCRIPT_GFX_DRAW_ORDER (GFX_ORDER_AFTER_HUD) //Deliberately calling this every frame in case another script tinkers with it.
                                                                                            //If they do, then they can switch the signifiers off, which they should be doing anyway.
                                            

                                            //Work for 2227208
                                            IF IS_PC_VERSION()
                                            AND g_PlayerBlipsData.bBigMapIsActive //On PC, Expanded radar displaying. draw the movie further to the right.

                                                ControlSigDrawPosX = 0.165
                                                ControlSigDrawPosY = 0.755

                                            ELSE
    
                                                ControlSigDrawPosX = 0.07
                                                ControlSigDrawPosY = 0.755

                                            ENDIF


                                                
                                          
                                            SafeZoneSize = GET_SAFE_ZONE_SIZE()
                            
                                            
                                            //0.01 reduction of safe zone size  = + 0.005 increase in x and -0.005 decrease in y of drawing positions of sleep signifier.


                                            SafeZoneManipulator = ((100 * ControlSafeZoneSize) - (100 * SafeZoneSize))  //Get difference between control and current safe zone size.

                                            //PRINTSTRING("SZMan ")
                                            //PRINTFLOAT(SafeZoneManipulator)
                                            //PRINTNL()

                                            SigDrawPosX = (ControlSigDrawPosX + (SafeZoneManipulator * 0.005))
                                            SigDrawPosY = (ControlSigDrawPosY - (SafeZoneManipulator * 0.005))

                                            /*
                                            PRINTSTRING("x")
                                            PRINTFLOAT(SigDrawPosX)
                                            PRINTNL()
                                            PRINTFLOAT(SigDrawPosY)
                                            PRINTNL()
                                            */
                                            
                                            //IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_OFF_MISSION) //Fix for #1413571
                                      

                                                IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() //Fix for 1728683, Sleep Icon being obscured by selfie instruction text.
                                                    
                                                    IF IS_PC_VERSION() //2199109
                                                        ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9()
                                                    ENDIF
                                                    
													//It would seem the alpha component has no impact on the drawing of the movie in-game.. UI will need to make a particular alert alpha-ed or not.
													
                                     				IF g_PlayerBlipsData.bBigMapIsActive  //Shunt any MP signifier that's still drawing to the top right side of the expanded radar - 3624701.
													 	ExpandedRadarManipulator_X = 0.09
														ExpandedRadarManipulator_Y = -0.250 //Negative value as we're shunting up the screen here.
														
														IF IS_PC_VERSION()
															IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)
																
																//Fix for 3724988
																#if IS_DEBUG_BUILD
	               												 	cdPrintString("CELLPHONE_CONT - Signifier movie draw logic - Big Map is active, is PC version and Beacon signifier marked to display. Not drawing movie however - see bug 3724988 ")
	                												cdPrintnl()
	           													 #endif	
																 
															ELSE
																DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, SigDrawPosX, SigDrawPosY,  0.27,  0.45, 255, 255, 255, MovieAlpha)	//url:bugstar:3635612. Normalized command above takes care of this. No need for manipulation.
															ENDIF
														ELSE
															DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, SigDrawPosX + ExpandedRadarManipulator_X, SigDrawPosY + ExpandedRadarManipulator_Y,  0.27,  0.45, 255, 255, 255, MovieAlpha)	
														ENDIF
													ELSE
														DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, SigDrawPosX, SigDrawPosY,  0.27,  0.45, 255, 255, 255, MovieAlpha)		
													ENDIF
									
													
													#if IS_DEBUG_BUILD
															/*
	                										cdPrintString("CELLPHONE_CONT - Signifier move alpha component test.")
	                										cdPrintnl()
															
															DISPLAY_TEXT_WITH_FLOAT (0.8, 0.1, "CELL_500", ExpandedRadarManipulator_X, 3)
															DISPLAY_TEXT_WITH_FLOAT (0.8, 0.15, "CELL_500", ExpandedRadarManipulator_Y, 3)
															
															IF IS_BUTTON_PRESSED (PAD1, DPADRIGHT) 
																ExpandedRadarManipulator_Y = ExpandedRadarManipulator_Y + 0.01
															ENDIF
															
															IF IS_BUTTON_PRESSED (PAD1, DPADLEFT) 
																ExpandedRadarManipulator_Y = ExpandedRadarManipulator_Y - 0.01
															ENDIF
															*/
	          										 #endif
													 

                                                ENDIF

                                            //ENDIF
                                            

                                            //For Debug position of indicator, comment out draw above, use these three lines below and comment in draw_debug_Scaleform_Signifier_Position() call.
                                            //db_signifier_posX = (db_preview_signifier_posX + (SafeZoneManipulator * 0.005))
                                            //db_signifier_posY = (db_preview_signifier_posY - (SafeZoneManipulator * 0.005))

                                            
                                            //DRAW_SCALEFORM_MOVIE(SF_MovieSignifiersIndex, db_signifier_posX, db_signifier_posY,  0.27,  0.45, 255, 255, 255, 255)
                                            //End of three line debug segment
                                                                     
                                    ENDIF

                                ENDIF

                           // ENDIF - Commenting out for 3615927 fix.
                        
                    ENDIF

                //ENDIF
            ENDIF

        ENDIF   //End of radar hidden check.
        ENDIF   //End of minimap rendering check.

    ENDIF       //End of g_forceDisplayPlayerInvitesIndicator check.


ENDPROC
















PROC Tidy_Up_Pic_Taking()

    DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)

    FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO()
    FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()

    #if IS_DEBUG_BUILD

        cdPrintString("CELLPHONE_CONT - Tidying up picture taking process. Freeing memory for both LQ and HQ qualities.")
        cdPrintnl()

    #endif

    BUSYSPINNER_OFF()


    Cellphone_Pic_Just_Taken = FALSE

    g_b_appHasRequestedTidyUp = FALSE

    g_b_appHasRequestedSave = FALSE

    
    g_Cell_Pic_Stage = PIC_STAGE_IDLE

 
ENDPROC






PROC Check_for_Picture_Just_Taken()

    IF Cellphone_Pic_Just_Taken //Make sure no one is setting this when they shouldn't! See nasty bug #1388922

        
        g_Cell_Pic_Stage = PIC_SHUTTER_DELAY

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Initiating shutter delay.")
            cdPrintnl()

        #endif

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LOW_QUALITY_CREATE_FAILED) //Reset in case this failed previously.

        SETTIMERB(0)

     
     

    ENDIF


ENDPROC








PROC Shutter_Delay()


    IF TIMERB() > 0 //Need for delay seems negligible now. Investigating # 1246572
    
    //Was 250 //We need a small delay to let the shutter close. This masks the game pause when the HQ screengrab is underway.

        g_Cell_Pic_Stage = PIC_STAGE_HQ_BEGIN


        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Shutter delay over - beginning HQ operations.")
            cdPrintnl()

        #endif



    ENDIF


ENDPROC












PROC Begin_Take_HQ_Pic()

    IF BEGIN_TAKE_HIGH_QUALITY_PHOTO()

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Success! Allocated memory buffer for taking HQ photograph.")
            cdPrintnl()

        #endif
        
        g_Cell_Pic_Stage = PIC_STAGE_HQ_ONGOING


    ELSE

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Cannot allocate memory buffer for taking HQ photograph. Trying next frame!")
            cdPrintnl()

        #endif

    ENDIF

ENDPROC






PROC Taking_HQ_Pic()


    PicProcessStatus = GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO()


    SWITCH PicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED
   
            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Take HQ Photo Succeeded!")
                cdPrintnl()

            #endif

            g_Cell_Pic_Stage = PIC_STAGE_LQ_BEGIN


            QualityToAttempt = LOW_QUALITY_PHOTO_QUARTER//LOW_QUALITY_PHOTO_ONE - this would be the maximum quantity we could try, but eats memory!
        
            
            #if IS_DEBUG_BUILD
                IF g_HighqualityPhotoSprite

                    QualityToAttempt = LOW_QUALITY_PHOTO_ONE

                ENDIF
            #endif



            /*
            ENUM eLowQualityPhotoSetting
                LOW_QUALITY_PHOTO_ONE,
                LOW_QUALITY_PHOTO_HALF,
                LOW_QUALITY_PHOTO_QUARTER,
                LOW_QUALITY_PHOTO_EIGHTH
            ENDENUM
            */
            
            
        BREAK


        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Take HQ Photo in progress...")
                cdPrintnl()

            #endif

        BREAK

        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Take HQ Photo FAILED! Forcing phone away.")
                cdPrintnl()

            #endif

            g_Cellphone.PhoneDS = PDS_AWAY //Force appCamera to quit out for safety as this process has failed.

            Tidy_Up_Pic_Taking()
    
        BREAK


    ENDSWITCH



ENDPROC






PROC Begin_Take_LQ_Pic()

    
    IF BEGIN_CREATE_LOW_QUALITY_COPY_OF_PHOTO(QualityToAttempt)

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Success! Found HQ photo to copy.")
            cdPrintnl()

        #endif
        
        g_Cell_Pic_Stage = PIC_STAGE_LQ_ONGOING



    ELSE

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Couldn't find HQ photo to create LQ copy from. Tidying up.")
            cdPrintnl()

        #endif


        //Let appMedia know that the load failed...
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LOAD_FROM_MEDIA_FAILED)

        Tidy_Up_Pic_Taking()


    ENDIF
    





ENDPROC





PROC Creating_LQ_Copy()


    PicProcessStatus = GET_STATUS_OF_CREATE_LOW_QUALITY_COPY_OF_PHOTO (QualityToAttempt)


    //PicProcessStatus = PHOTO_OPERATION_FAILED - Comment in to test fail pipeline.

    SWITCH PicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Creating LQ copy succeeded!")
                cdPrintnl()

            #endif


            DRAW_LOW_QUALITY_PHOTO_TO_PHONE (TRUE, PHOTO_CLOCKWISE)

            g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY
            
        BREAK


        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Creating LQ copy of HQ photo in progress...")
                cdPrintnl()

            #endif

        BREAK

        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Create LQ copy of photo failed this time round...")
                cdPrintnl()

            #endif


            //Added as a consequence of PT bug 917230. If the photo taking fails we still need to continue as normal without drawing the pic to the phone, so missions can continue.
            //After a failure, the shutter will remain drawn to the phone in small size landscape mode.


            IF QualityToAttempt = LOW_QUALITY_PHOTO_EIGHTH  //Abandon save if lowest quality fails. //New




                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LOW_QUALITY_CREATE_FAILED) //We also need to prevent appCamera from trying to save an empty buffer.


                DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)

                FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO()
                FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()


                g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY


                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - Create LQ EIGHTH copy of photo FAILED!")
                    cdPrintnl()

                #endif



                //Removed as a consequence of PT bug 917230
                //Tidy_Up_Pic_Taking()


            ENDIF //New



            //Hacked in for feasibility study.

            IF g_Cell_Pic_Stage = PIC_STAGE_LQ_ONGOING    ////Makes sure this doesn't carry through subsequent checks as the cell_pic_stage is reassigned.
                IF QualityToAttempt = LOW_QUALITY_PHOTO_ONE

                    g_Cell_Pic_Stage = PIC_STAGE_LQ_BEGIN     //Go again but at lower quality...
                    QualityToAttempt = LOW_QUALITY_PHOTO_HALF

                    FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()
                    WAIT(0) //Deliberate Test for Graeme.
   
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - Create LQ ONE copy of photo FAILED!")
                        cdPrintnl()

                    #endif

        
                ENDIF
            ENDIF


            IF g_Cell_Pic_Stage = PIC_STAGE_LQ_ONGOING //Makes sure this doesn't carry through subsequent checks as the cell_pic_stage is reassigned.
                IF QualityToAttempt = LOW_QUALITY_PHOTO_HALF

                    g_Cell_Pic_Stage = PIC_STAGE_LQ_BEGIN
                    QualityToAttempt = LOW_QUALITY_PHOTO_QUARTER

                    FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()
                    WAIT(0)
            
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - Create LQ HALF copy of photo FAILED!")
                        cdPrintnl()

                    #endif

        
                ENDIF
            ENDIF

            
            IF g_Cell_Pic_Stage = PIC_STAGE_LQ_ONGOING ////Makes sure this doesn't carry through subsequent checks as the cell_pic_stage is reassigned.
                IF QualityToAttempt = LOW_QUALITY_PHOTO_QUARTER

                    g_Cell_Pic_Stage = PIC_STAGE_LQ_BEGIN
                    QualityToAttempt = LOW_QUALITY_PHOTO_EIGHTH

                    FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()
                    WAIT(0)
              

                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - Create LQ QUARTER copy of photo FAILED!")
                        cdPrintnl()

                    #endif

        
                ENDIF
            ENDIF



        BREAK


    ENDSWITCH


ENDPROC






PROC Check_Storage()


    PicProcessStatus = GET_STATUS_OF_SORTED_LIST_OPERATION(TRUE) //The last BOOL indicates that we are checking for a SAVE. This would be false if we were loading.


        SWITCH PicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Sorted List Operation Succeeded!")
                cdPrintnl()

            #endif

            g_Cell_pic_Stage = PIC_STAGE_STORE_TO_MEDIA

        BREAK



       
        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Sorting list operation in progress...")
                cdPrintnl()

            #endif

        BREAK

    
        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Sorted List Operation FAILED!")
                cdPrintnl()

            #endif

            Tidy_Up_Pic_Taking()
    
        BREAK


    ENDSWITCH




ENDPROC






PROC Begin_Store_to_Media()


    g_i_PhotoSlot = -1//GET_SLOT_FOR_SAVING_A_PHOTO_TO()

	
	//url:bugstar:5509739 - Add Snapmatic support for exposing the current Arena theme and variation via Snapmatic data.
	IF g_bInMultiplayer
		IF CONTENT_IS_USING_ARENA() 
			
			SET_ARENA_THEME_AND_VARIATION_FOR_TAKEN_PHOTO(g_FMMC_STRUCT.sArenaInfo.iArena_Theme, g_FMMC_STRUCT.sArenaInfo.iArena_Variation)
			
			#if IS_DEBUG_BUILD
				cdPrintString("CELLPHONE_CONT - Begin_Store_to_Media - Arena Theme detected as ")
				cdPrintint(g_FMMC_STRUCT.sArenaInfo.iArena_Theme)
				cdPrintnl()
				cdPrintString("CELLPHONE_CONT - Begin_Store_to_Media - Arena Variation detected as ")
				cdPrintint(g_FMMC_STRUCT.sArenaInfo.iArena_Variation)
			#endif
		ELSE
			#if IS_DEBUG_BUILD
				cdPrintString("CELLPHONE_CONT - Begin_Store_to_Media - No arena setup required. Content not using arena.")
			#endif
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		//IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		IF IS_BIT_SET_ENUM(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistIslandTravelBS, HITBS__ON_ISLAND)
			SET_ON_ISLAND_X_FOR_TAKEN_PHOTO(TRUE)
			PRINTLN("Begin_Store_to_Media: SET_ON_ISLAND_X_FOR_TAKEN_PHOTO(TRUE)")
		ELSE
			SET_ON_ISLAND_X_FOR_TAKEN_PHOTO(FALSE)
			PRINTLN("Begin_Store_to_Media: SET_ON_ISLAND_X_FOR_TAKEN_PHOTO(FALSE)")
		ENDIF
		#ENDIF
	ELSE
		#if IS_DEBUG_BUILD
			cdPrintString("CELLPHONE_CONT - Begin_Store_to_Media - No arena setup required. Not in multiplayer")
			cdPrintnl()
		#endif
	ENDIF
	
	// If the player is on the Heist Island, call SET_ON_ISLAND_X_FOR_TAKEN_PHOTO(TRUE)

    IF SAVE_HIGH_QUALITY_PHOTO(g_i_PhotoSlot)

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Now DEPRECATED - 1158392. Success! Slot to save HQ photo to media is ")
            cdPrintint(g_i_PhotoSlot)
            cdPrintnl()

        #endif
        
        g_Cell_Pic_Stage = PIC_STAGE_STORE_TO_MEDIA_ONGOING

        //Moved to appCamera to use floating help routines.
        //PRINT_HELP_WITH_NUMBER ("CELL_292", (g_i_PhotoSlot + 1)) //Adjust to account for slot zero as the first slot.

        BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("CELL_278")
        END_TEXT_COMMAND_BUSYSPINNER_ON(1)//(4) Hard drive save uses standard spinner. "4" is cloud spinner.




    ELSE

              
        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Slot failed when trying to save HQ photo to media. Tidying up.")
            cdPrintString("CELLPHONE_CONT - Slot was ")
            cdPrintInt(g_i_PhotoSlot)
            cdPrintnl()

        #endif

        Tidy_Up_Pic_Taking()

    ENDIF

ENDPROC






PROC Ongoing_Store_to_Media()


    PicProcessStatus = GET_STATUS_OF_SAVE_HIGH_QUALITY_PHOTO()

    /*Removed. Pictures save to hard drive. No longer need this check!
    IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()

        
        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Social Club account lost, player signed out! Abandoning save!")
            cdPrintnl()

        #endif

        PicProcessStatus = PHOTO_OPERATION_FAILED

    ENDIF
    */


    SWITCH PicProcessStatus
                                                

        CASE PHOTO_OPERATION_SUCCEEDED
  
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintString("CELLPHONE_CONT - SAVED HQ photo to media ")
                cdPrintint(g_i_PhotoSlot)
                cdPrintnl()

            #endif

            //BUSYSPINNER_OFF()
            //Fix for 1942804 - drawing phone camera image to the homescreen when player gets in a vehicle mid-save.
            //DRAW_LOW_QUALITY_PHOTO_TO_PHONE (TRUE, PHOTO_CLOCKWISE)

          
            IF NETWORK_IS_GAME_IN_PROGRESS()

                NetPost_Save_PauseSentTime = GET_NETWORK_TIME()

            ELSE

                Post_Save_PauseSentTime = GET_GAME_TIMER()

            ENDIF


            g_Cell_Pic_Stage = PIC_STAGE_POST_SAVE_PAUSE

            SETTIMERB(0)

        BREAK


        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Saving HQ photo to media in progress...")
                cdPrintnl()

            #endif

        BREAK

        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - HQ photo to media save FAILED!")
                cdPrintnl()

            #endif

            Tidy_Up_Pic_Taking()
    
        BREAK


    ENDSWITCH





ENDPROC



PROC PostSavePause()  //Required to prevent easy manipulation of overlap timings. #1231086



    IF NETWORK_IS_GAME_IN_PROGRESS()

        i_Post_Save_PauseDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), NetPost_Save_PauseSentTime)

    ELSE

        Post_Save_PauseCurrentTime = GET_GAME_TIMER()

        i_Post_Save_PauseDifference = Post_Save_PauseCurrentTime - Post_Save_PauseSentTime

    ENDIF



    IF i_Post_Save_PauseDifference > 3000 //Increased time for 3 seconds TRC.

        BUSYSPINNER_OFF()

    ENDIF   

    IF i_Post_Save_PauseDifference > 3200 //Increased time for 3 seconds TRC and adding a little delay.

        g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY //This will let appCamera know that the save has been completed - search //Save Routine Completed!
    
    ENDIF


ENDPROC





PROC Load_From_Media()

    IF LOAD_HIGH_QUALITY_PHOTO (g_LoadFromSlot)

        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Success! Load HQ photo from ")
            cdPrintint(g_LoadFromSlot)
            cdPrintnl()

        #endif
        
               
        g_Cell_Pic_Stage = PIC_STAGE_LOAD_FROM_MEDIA_ONGOING



    ELSE


        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Slot failed when trying to load HQ photo.")
            cdPrintString("CELLPHONE_CONT - Slot was ")
            cdPrintInt(g_LoadFromSlot)
            cdPrintnl()

        #endif

        Tidy_Up_Pic_Taking()


    ENDIF


ENDPROC






PROC Ongoing_Load_From_Media()


    PicProcessStatus = GET_LOAD_HIGH_QUALITY_PHOTO_STATUS(g_LoadFromSlot)


    //Temp! Do not check in. This forces an auto-fail for testing.
    //WAIT(3000)
    /*
    IF PicProcessStatus = PHOTO_OPERATION_SUCCEEDED
 
        PicProcessStatus = PHOTO_OPERATION_FAILED

    ENDIF
    */







    SWITCH PicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Loaded HQ photo from media")
                cdPrintnl()

            #endif

        
            g_Cell_Pic_Stage = PIC_STAGE_LQ_BEGIN

        BREAK



     
        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Loading from media in progress...")
                cdPrintnl()

            #endif

        BREAK

    
        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Load from media FAILED!")
                cdPrintnl()

            #endif


            //Let appMedia know that the load failed...
            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LOAD_FROM_MEDIA_FAILED)



            Tidy_Up_Pic_Taking()

                

    
        BREAK


    ENDSWITCH


ENDPROC








PROC Setup_Cellphone_Signifier_Positions_according_to_Screen_Ratio() //Currently empty pending any requirements with screen resolutions / ratios.


    //Moved to cellphone_private's Create_3dPhoneObject() due to these position having to adapt to the safezone.

    //g_This_Screen_3dPhoneStartVec[DISPLAY_16_9] = <<107.0, -134.0, -113.0>>     //Left HS<<-101.5, -134.0, -113.0>> 
    //g_This_Screen_3dPhoneEndVec[DISPLAY_16_9] = <<107.0, -48.5, -113.0>>        //Left HS<<-101.5, -48.5, -113.0>>  

    //g_This_Screen_3dPhoneStartVec[DISPLAY_4_3] = <<71.0, -135.5, -113.0>>       //Left HS<<-71.0, -135.5, -113.0>> 
    //g_This_Screen_3dPhoneEndVec[DISPLAY_4_3] = <<71.0, -41.5, -113.0>>           //Left HS<<-71.0, -41.5, -113.0>>
    







    /*
    IF IS_XBOX360_VERSION()

        signifier_posX[DISPLAY_16_9] = 10.0
        signifier_posY[DISPLAY_16_9] = 140.0

        signifier_posX[DISPLAY_4_3] = 132.0
        signifier_posY[DISPLAY_4_3] = 140.0


    ELSE  //PS3, PC

        signifier_posX[DISPLAY_16_9] = 10.0
        signifier_posY[DISPLAY_16_9] = 155.0
 
        signifier_posX[DISPLAY_4_3] = 132.0
        signifier_posY[DISPLAY_4_3] = 155.0

    ENDIF
    */
    
    

    //May need to look at these for 480p bugs.
    // PURPOSE: Returns the screen resolution
    //GET_SCREEN_RESOLUTION(INT &X, INT &Y)

    //INFO: 
    //PARAM NOTES:
    //PURPOSE: Returns if running widescreen
    //GET_IS_WIDESCREEN()

    //INFO: 
    //PARAM NOTES:
    //PURPOSE: Returns if running in hidef
    //GET_IS_HIDEF()



    /*Removing this check. Cellphone now tied to safezone.
    IF GET_IS_WIDESCREEN()  //Is the user running in 16_9?

        g_Chosen_Ratio = DISPLAY_16_9

    ELSE

        g_Chosen_Ratio = DISPLAY_4_3 //He isn't, use 4_3 instead.


    ENDIF
    */



ENDPROC




PROC DisplayUnreadMarketingMessage()

	IF g_NumberOfUnreadMarketingEmailsThisSession = 1 // Set the unread emails to 1. The stat will be updated in AppMPEmail when the user reads it. 
										
		//Fudge fix for 2697795. Display a feed message on launch of session.
		IF NOT IS_ACCOUNT_OVER_17()

			#if IS_DEBUG_BUILD
				cdPrintString("Cellphone Controller - Launched MP session and email unread detected but Account is not over 17. No feed creation.")
				cdPrintnl()
				#endif
									       	
			EXIT
			
		ENDIF

		IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID()) //Fix for 2437163. Make sure that no feed message appears if the user account does not have communication privileges.
									    
				IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)

							PRINTLN("Cellphone Controller - Displaying feed notification of unread email detected on MP session launch..")

							BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_EMAIL_EVENT")   
							END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)

				ELSE
									           
							IF GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) <> 1
								#if IS_DEBUG_BUILD
									cdPrintString("Cellphone Controller - Launched MP session and email unread detected  but PROFILE_FEED_FRIENDS did not equal 1. No feed creation.")
									cdPrintnl()
								 #endif
							ENDIF

				ENDIF

		 ELSE

					#if IS_DEBUG_BUILD
						cdPrintString("Cellphone Controller - Launched MP session and email unread detected  but Account does not have chat privileges. No feed creation.")
						cdPrintnl()
					#endif

		 ENDIF

                             

    ENDIF

ENDPROC







PROC Check_For_Read_Auto_TextLaunch_Timeout()


     IF Read_Auto_TextLaunch[g_Cellphone.PhoneOwner] = TRUE //Only check the current owner. If a swap does take place then the newly swapped to owner can timeout immediately. 

        //By including this clause we can make the timer apply to critical messages only. 
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead



            IF NETWORK_IS_GAME_IN_PROGRESS()

                i_Read_Auto_TextLaunchDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), NetRead_Auto_TextLaunchSentTime[g_Cellphone.PhoneOwner])

            ELSE

                Read_Auto_TextLaunchCurrentTime[g_Cellphone.PhoneOwner] = GET_GAME_TIMER()

                i_Read_Auto_TextLaunchDifference = Read_Auto_TextLaunchCurrentTime[g_Cellphone.PhoneOwner] - Read_Auto_TextLaunchSentTime[g_Cellphone.PhoneOwner]

            ENDIF





            IF i_Read_Auto_TextLaunchDifference > Read_Auto_TextLaunchTimeoutThreshold  //If read time has elapsed we must reset the LaunchTo flag which forces the cellphone to launch 
                                                                                        //appTextMessages and the critical message related MustBeRead flag
                                                                                        //which forces the app to single message view immediately.


                g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE

          
                g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE 

                //The contents of the procedure won't run again until this is set TRUE on a new text message or a player swap takes place.
                Read_Auto_TextLaunch[g_Cellphone.PhoneOwner] = FALSE

                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - Read_Auto_Textlaunch timeout has taken place. Removing block and launch forces. Player was ")
                    cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    cdPrintnl()

                #endif


            ENDIF

        //ENDIF

    ENDIF

ENDPROC







PROC Check_If_Signifier_Movie_Needs_Drawn_or_Removed()


    
    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_SIGNIFIER_MOVIE_LOADED)


        IF g_Current_Number_Of_Signifiers > 0 //A signifier must be present or this would be zero.
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED) //If the update flag is still set we need to re-add any items.

                IF b_Allow_Drawn_Or_Remove_to_Check_For_Update //necessary to prevent this running before initial update check in main loop.

                    LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CLEAR_ALL")

                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER) 

                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CREATE_ALERT")

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (26)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posX[g_Chosen_Ratio] + 155.0)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posY[g_Chosen_Ratio])  //Petri says invites icon should not display in SP... was 192.0
                                                                                                              //#2059965
                        END_SCALEFORM_MOVIE_METHOD()


                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - Adding sleep signifier.")
                            cdPrintnl()

                        #endif

                    ENDIF

                    

                    IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)  //Mutually exclusive with SLEEP MODE, so can use the same position.


                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CREATE_ALERT")

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (53)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posX[g_Chosen_Ratio] + 155.0)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posY[g_Chosen_Ratio])

                        END_SCALEFORM_MOVIE_METHOD()

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - Adding on call signifier in SP ng.")
                            cdPrintnl()

                        #endif


                    ENDIF



               
                    IF g_bInMultiplayer = TRUE
                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)  //Mutually exclusive with SLEEP MODE, so can use the same position.

                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CREATE_ALERT")

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (52)
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posX[g_Chosen_Ratio] + 155.0)
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posY[g_Chosen_Ratio])

                            END_SCALEFORM_MOVIE_METHOD()


                            #if IS_DEBUG_BUILD

                                cdPrintString("CELLPHONE_CONT - Adding Invite signifier in MP only.")
                                cdPrintnl()

                            #endif

                        ENDIF


	
						 IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)  ////Added for 3599537 - display beacon beside radar.																			  
                        	BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CREATE_ALERT")

                            	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (60) //Should be icon 60
                            	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)
                           		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (148.0)//Position nearer top of radar to allow space for other signifiers. Invites / Push to talk etc.

                        	END_SCALEFORM_MOVIE_METHOD()

	                        #if IS_DEBUG_BUILD

	                            cdPrintString("CELLPHONE_CONT - Adding Beacon signifier in Multiplayer.")
	                            cdPrintnl()

	                        #endif

                    	ENDIF

						
						
						
                    ENDIF





                    
					IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)  //This bit will be set on PC only.

                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieSignifiersIndex, "CREATE_ALERT")

                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (55)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posX[g_Chosen_Ratio] + 155.0) 
                            
                            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED) 
                            OR IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)
                            OR IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)                            

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (192.0)//(signifier_posY[g_Chosen_Ratio])
    
                            ELSE

                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (222.0)//(signifier_posY[g_Chosen_Ratio])

                            ENDIF

                        END_SCALEFORM_MOVIE_METHOD()

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - Adding PUSHTALK signifier in PC ng.")
                            cdPrintnl()

                        #endif

                    ENDIF




                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)
                    
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - Movie is loaded and added any valid signifiers. Clearing Sig Update Flag 2.")
                        cdPrintnl()

                    #endif

                
                    b_Allow_Drawn_Or_Remove_to_Check_For_Update = FALSE

                ENDIF

            ENDIF
        ENDIF






        IF g_Current_Number_Of_Signifiers < 1 //nothing needs to use the scaleform signifier movie. Safe to remove it and reset the bit.


           
            SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieSignifiersIndex)

                   
         
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_HAS_SIGNIFIER_MOVIE_LOADED)

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_CONT - SF_MovieSignifiers removed, movie marked as no longer needed and related bit cleared. Number of signifiers at ")
                cdPrintint (g_Current_Number_Of_Signifiers)
                cdPrintnl ()
                
            #endif
            



        ELSE   //One or more signifiers must still need to be drawn, so continue drawing the scaleform movie every frame provided the phone is not onscreen


                /*
                IF IS_CURRENT_PLAYER_IN_SLEEP_MODE()

                    IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_OFF_MISSION)
                    
                        SET_CELLPHONE_PROFILE_TO_NORMAL()


                        #if IS_DEBUG_BUILD

                            cdPrintstring ("CELLPHONE_CONT - Player was in sleep mode but mission type is no longer off mission.")
                            cdPrintnl ()
                
                        #endif


                    ENDIF

                ENDIF
                
                b_BlockSigniferMovieFromDrawing = FALSE
                //Don't display the oncall signifier in 3rd person.
                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)
                    IF IsCurrentCamFirstPerson() = FALSE
                        //IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)
                            b_BlockSigniferMovieFromDrawing = TRUE
                        //ENDIF
                    ENDIF
                ENDIF
                */    

                //IF b_BlockSigniferMovieFromDrawing = FALSE


                DrawSignifierMovie()

  



                 
        ENDIF


    ELSE


   



        IF g_Current_Number_Of_Signifiers > 0 //The movie is not loaded but a signifier has requested that it should be drawn as total is greater than zero.

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_CONT - Loading cellphone_alert_popup movie...")
                cdPrintnl ()
                
            #endif

            
           
            SF_MovieSignifiersIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_alert_popup")


            WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieSignifiersIndex)
                
                WAIT(0)
              
            ENDWHILE

    
          
            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_CONT - Cellphone_alert_popup loaded and related bit set.")
                cdPrintnl ()
                
            #endif

            //488121
            SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)


            SET_BIT (BitSet_CellphoneDisplay, g_BS_HAS_SIGNIFIER_MOVIE_LOADED)



        
        ENDIF

    ENDIF
    
     

ENDPROC





















PROC Create_Contact_Feed_Entry()


    //For bug 880374, we should replace SignifierPrimaryText with the buffered contents when the player swap has been detected.
    TEXT_LABEL_63 s_TXD_Contact_decision

    s_TXD_Contact_decision = (GET_FILENAME_FOR_AUDIO_CONVERSATION (g_sCharacterSheetAll[g_Temp_ContactHeadshotRef].picture))

    #if IS_DEBUG_BUILD  //Important, make sure this is always in the logs.
        PRINTNL()
        PRINTSTRING("CELLPHONE CONTROLLER - Extract headshot txd for Contact feed was ")
        PRINTSTRING (s_TXD_Contact_Decision)
        PRINTNL()
    #endif

    





    IF g_UseContactSignifierNewType = FALSE


        BEGIN_TEXT_COMMAND_THEFEED_POST ("")

            TEXT_LABEL_63 s_TempContactHolder

            s_TempContactHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText)

            STRING s_ContactAddedString
            s_ContactAddedString = GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_253")

                #if IS_DEBUG_BUILD
        
                    cdPrintstring ("Strings for contact test are:  ")
                    cdPrintstring (s_TXD_Contact_decision) 
                    cdPrintstring (", ")
                    cdPrintstring (s_TempContactHolder)
                    cdPrintstring (", ")
                    cdPrintstring (s_ContactAddedString)
                    cdPrintnl()

                #endif

        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_Contact_decision, s_TXD_Contact_decision, FALSE, TEXT_ICON_NEW_CONTACT, s_ContactAddedString, s_TempContactHolder)


    ELSE


        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_255")

            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (SignifierData[SIG_CONTACT1].SignifierPrimaryText)

        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(s_TXD_Contact_decision, s_TXD_Contact_decision, FALSE, TEXT_ICON_NEW_CONTACT, "")


    ENDIF











    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("CELLPHONE_CONT - Created New Contact feed entry. ")
        cdPrintint (g_Current_Number_of_Signifiers)
        cdPrintnl ()
        


            SWITCH g_Cellphone.PhoneOwner

                CASE CHAR_MICHAEL  

                    PRINTLN("Owner this frame is Michael - ")
        
                BREAK


                CASE CHAR_FRANKLIN  

                    PRINTLN("Owner this frame is Franklin - ")

                BREAK
            

                CASE CHAR_TREVOR 

                    PRINTLN("Owner this frame is Trevor - ")
   
                BREAK


                DEFAULT

                    PRINTLN("Owner this frame is MP - ")

                BREAK

            ENDSWITCH


            PRINTSTRING("CELLPHONE_CONT Created new feed entry as this contact has been added for the current owner. New contact: ")
            PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText))
            PRINTNL()



    #endif



    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)


ENDPROC













PROC Create_TxtMsg_Feed_Entry()


    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintnl()
        cdPrintString("CELLPHONE_CONT - Creating new FEED ENTRY...")
        cdPrintnl()
        cdPrintnl()

    #endif
    


    //We need to determine if the texture and associated dictionary strings passed into the end feed command should be from the character sheet or a generated user headshot
    //If we find a headshot, then we must also update the headshot string holder so the text message app knows to display this rather than the usual pic of the sender.
    //The store for this, TxtMsgHeadshotString, is automatically set to NO_HEADSHOT_REQUIRED previously in ProcessTextMessage() in Cellphone_Private.


    TEXT_LABEL_63 s_TXD_decision


    IF (g_playerTxtmsgHeadshotMP != NULL) //If this is not null then that means a valid headshot has been generated by Generate_Player_Headshot in net_comms.sch
        
        IF (IS_PEDHEADSHOT_VALID (g_playerTxtmsgHeadshotMP))
            
            IF (IS_PEDHEADSHOT_READY (g_playerTxtmsgHeadshotMP))

                s_TXD_decision = GET_PEDHEADSHOT_TXD_STRING(g_playerTxtmsgHeadshotMP)

                //Keep a copy of the PEDHEADSHOT_ID in the identified free index for later reference by appTextMessage.
                g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = g_playerTxtmsgHeadshotMP

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("CELLPHONE_CONT - Found Headshot. Storing TxtMsgHeadShotID for this index.")
                    cdPrintnl()

                #endif


            ELSE
            
                s_TXD_decision = "CHAR_DEFAULT" //default to blank headshot if the headshot is not ready.
            
                //Default associated headshotID to NULL
                g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL


            ENDIF
        
        ELSE
        
            s_TXD_decision = "CHAR_DEFAULT" //default to blank headshot if the headshot is not valid.

            //Default associated headshotID to NULL
            g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL


        ENDIF


    ELSE

        //Not a user headshot, use normal character sheet image instead.
        s_TXD_decision = 
        GET_FILENAME_FOR_AUDIO_CONVERSATION (g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender].picture)
  

		//url:bugstar:5809076 - Hidden Collectibles - Action Figures - There is no contact image for the Hardcore Comic Store
		//Steve T notes: This is because the picture label was changed after QA had established a save game that contained
		//an earlier pic label for this Comic Store character. That save data was clobbering the change.
		//To get round this safely, point this specific character to the actual text label that holds his texture reference.
		IF (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = CHAR_COMIC_STORE)
			
			#if IS_DEBUG_BUILD
				cdPrintString("JUNE 2019 - Passed character was CHAR_COMIC_STORE. Overriding headshot decision to CELL_COMIC_P")
			#ENDIF
			
			
			s_TXD_decision = GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_COMIC_P") 
			
			
			#if IS_DEBUG_BUILD
				cdPrintString("JUNE 2019 - Comic Store extracted TXD decision was: ")
			#endif
			
		ELSE	
	
			#if IS_DEBUG_BUILD

		        cdPrintnl()
		        cdPrintString("CELLPHONE_CONT - Found Story Character Headshot. Grabbed TxtMsgHeadShotID for this index. Picture Text Label used for extraction was: ")
				
				cdPrintString(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender].picture)
		        cdPrintnl()
				
				cdPrintString("JUNE 2019 - Extracted TXD decision was: ")
				cdPrintString(s_TXD_decision)
				cdPrintnl()

		     #endif
		 
		 ENDIF

 
        //Default associated headshotID to NULL
        g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = NULL


    ENDIF   







            
    IF IS_STRING_NULL_OR_EMPTY (s_TXD_decision)

        
        #if IS_DEBUG_BUILD

            SCRIPT_ASSERT ("Warning: Feed Entry txd decision is null or empty! Using CHAR_DEFAULT instead!")
            PRINTLN("\nWarning: Feed Entry txd decision is null or empty! Using CHAR_DEFAULT instead!\n")

        #endif


        s_TXD_decision = "CHAR_DEFAULT"
    
    ELSE

        IF ARE_STRINGS_EQUAL (s_TXD_decision, "NULL")

            #if IS_DEBUG_BUILD

                //SCRIPT_ASSERT ("Warning: Feed Entry txd decision contains NULL. Using CHAR_DEFAULT instead!")
                PRINTLN("\nWarning: Feed Entry txd decision contains NULL. Using CHAR_DEFAULT instead!\n")

            #endif


            s_TXD_decision = "CHAR_DEFAULT"

        ENDIF

    ENDIF





    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("CELLPHONE CONT - Sender image string was ")
        cdPrintstring (s_TXD_decision)
        cdPrintnl()

    #endif













    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = CAR_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = SUPERAUTOS_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = LEGENDARY_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = PEDALMETAL_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = WARSTOCK_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = ELITAS_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = DOCKTEASE_LIST_COMPONENT
    OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = DAILYOBJ_LIST_COMPONENT
	OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents = DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT // url:bugstar:7188269

        /* Removed. See bug 847795. This was used when a new car list notification did not include the full list of cars in the feed entry.
        TEXT_LABEL CarListTokenDecision


        IF g_CarListMessageDecision = NEW_CAR_LIST

            CarListTokenDecision = "CELL_256"

        ELSE 

           CarListTokenDecision = "CELL_254" //Must be updated car list.
           
        ENDIF   
        */

        //BEGIN_TEXT_COMMAND_THEFEED_POST (CarListTokenDecision)








        INT tempConstructionInt = 0
		INT iMsgNumberComponet = MIN_VALUE_INT(9, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent)

            //Decide which tilda a tilda configuration we should use based on how many cars were stipulated when the messsage was sent.
            SWITCH iMsgNumberComponet


                CASE 1
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL01") //This refers to a label with one ~a~ car component...                             
                BREAK

                
                CASE 2
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL02") //This refers to a label with two ~a~ car components...
                BREAK



                CASE 3
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL03") //This refers to a label with three ~a~ car components...                             
                BREAK



                CASE 4
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL04") //This refers to a label with four ~a~ car components...                             
                BREAK


                
                CASE 5
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL05") //This refers to a label with five ~a~ car components...                             
                BREAK



                CASE 6
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL06") //This refers to a label with six ~a~ car components...                             
                BREAK



                CASE 7
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL07") //This refers to a label with seven ~a~ car components...                             
                BREAK



                CASE 8
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL08") //This refers to a label with eight ~a~ car components...
                BREAK


                
                CASE 9
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL09") //This refers to a label with nine ~a~ car components...                             
                BREAK




                ENDSWITCH


            //Add in user-defined label at the very start. i.e The first tilda a tilda substring. 
            //E.g Grab these cars! The list then follows as the specified amount of substrings.

            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLabel)  
           
            
            //Add each separate car as a substring.
            WHILE tempConstructionInt < iMsgNumberComponet
                        
                //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP. 
                  
                //Adding in support for multiple car lists at request of 1600460    
                SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents

                
                        CASE CAR_LIST_COMPONENT
                                                    
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                        
                        BREAK


                        //New additions.
                        CASE SUPERAUTOS_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE LEGENDARY_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE PEDALMETAL_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE WARSTOCK_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE ELITAS_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE DOCKTEASE_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE DAILYOBJ_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK
						
						// url:bugstar:7188269 - Exotic Exports - As a QoL update, can we please add the ability to request a text message from Sessanta that lists the vehicles you still need to get that day?
						CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
							
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_CalkboardAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the calkboard vehicle list					
							
		                BREAK

                ENDSWITCH

                  
                  
                  
                    
                tempConstructionInt ++

            ENDWHILE
        
            TEXT_LABEL_63 s_TempHolder


            s_TempHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender].label)
            

            #if IS_DEBUG_BUILD
    
                cdPrintstring ("Strings for car list text message are:  ")
                cdPrintstring (s_TXD_decision) 
                cdPrintstring (", ")
                cdPrintstring (s_TempHolder)
                cdPrintnl()

            #endif

            i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_MESSAGE, s_TempHolder)
        
            g_Last_App_ActivatableType = ACT_APP_TEXT
            g_Last_App_ActivatableFeedID = i_FeedItemId

            g_playerTxtmsgHeadshotMP = NULL //Reset headshot holder to NULL now that we have created the feed entry.


    ELSE
        




        BEGIN_TEXT_COMMAND_THEFEED_POST (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgLabel)

            SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSpecialComponents


                CASE NO_SPECIAL_COMPONENTS
                   //No further additions to text label string need integrated.
                BREAK



                CASE STRING_COMPONENT

                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgStringComponent)
                    
                    //Check if any additional strings are required...
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)   

                    ENDIF


                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent, "NULL"))

                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                        //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)        
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)        
                        ENDIF
                    ENDIF

                BREAK




                CASE NUMBER_COMPONENT

                    ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent)
                    
                BREAK





                CASE STRING_AND_NUMBER_COMPONENT
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgStringComponent)
                    ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberComponent)



                    //Check if any additional strings are required...
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)   

                    ENDIF


                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent, "NULL"))

                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)        
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgThirdStringComponent)        
                        ENDIF
                    ENDIF


                BREAK

            ENDSWITCH

            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = NO_CHARACTER  //As the sender is no character we can assume this is a gamerTag.

                TEXT_LABEL_63 s_MPGamerTagTempHolder

                //Grab played name and apply prefix and suffix for displaying the sender name with the condensed font.
                s_MPGamerTagTempHolder = "<C>"
                s_MPGamerTagTempHolder += g_Temp_MP_SenderString
                s_MPGamerTagTempHolder += "</C>"

                i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_MESSAGE, s_MPGamerTagTempHolder)

                g_Last_App_ActivatableType = ACT_APP_TEXT
                g_Last_App_ActivatableFeedID = i_FeedItemId



            ELSE



                TEXT_LABEL_63 s_TempHolder, s_SocialClubConstruct


                

                s_TempHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender].label)
           

                #if IS_DEBUG_BUILD
        
                    cdPrintstring ("Strings for text message are:  ")
                    cdPrintstring (s_TXD_decision) 
                    cdPrintstring (", ")
                    cdPrintstring (s_TempHolder)
                    cdPrintnl()

                #endif


                //Check for Social Club sender here. If so, colour yellow. #1562653

                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgSender = CHAR_SOCIAL_CLUB

                    s_SocialClubConstruct = "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_SocialClubConstruct += s_TempHolder
                    

        
                    i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_MESSAGE, s_SocialClubConstruct)

                ELSE
                
                    i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_MESSAGE, s_TempHolder)
                
                ENDIF


                g_Last_App_ActivatableType = ACT_APP_TEXT
                g_Last_App_ActivatableFeedID = i_FeedItemId


                g_playerTxtmsgHeadshotMP = NULL //Reset headshot holder to NULL now that we have created the feed entry.



            ENDIF



    ENDIF

    #if IS_DEBUG_BUILD
    
        cdPrintstring ("CELLPHONE CONT - Feed id is ")
        cdPrintint (i_FeedItemId) 
        cdPrintnl()

    #endif

    //Update stored text message array index with the feed id so that it can be instantly removed if the text message is deleted by script or user. 
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgIdentifiedFreeArrayIndex].TxtMsgFeedEntryId = i_FeedItemId


ENDPROC






PROC Create_Email_Feed_Entry()


    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintnl()
        cdPrintString("CELLPHONE_CONT - Creating new EMAIL FEED ENTRY...")
        cdPrintnl()
        cdPrintnl()

    #endif
    
    
    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_DISPLAY_EMAIL_SIGNIFIER) 





    //We need to determine if the texture and associated dictionary strings passed into the end feed command should be from the character sheet or a generated user headshot
    //If we find a headshot, then we must also update the headshot string holder so the text message app knows to display this rather than the usual pic of the sender.
    //The store for this, TxtMsgHeadshotString, is automatically set to NO_HEADSHOT_REQUIRED previously in ProcessTextMessage() in Cellphone_Private.


    TEXT_LABEL_63 s_TXD_decision


    IF (g_playerTxtmsgHeadshotMP != NULL) //If this is not null then that means a valid headshot has been generated by Generate_Player_Headshot in net_comms.sch
        
        IF (IS_PEDHEADSHOT_VALID (g_playerTxtmsgHeadshotMP))
            
            IF (IS_PEDHEADSHOT_READY (g_playerTxtmsgHeadshotMP))

                s_TXD_decision = GET_PEDHEADSHOT_TXD_STRING(g_playerTxtmsgHeadshotMP)

                //Keep a copy of the PEDHEADSHOT_ID in the identified free index for later reference by appTextMessage.
                g_TxtMsgHeadshotID[TxtMsgIdentifiedFreeArrayIndex] = g_playerTxtmsgHeadshotMP

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("CELLPHONE_CONT - Found Headshot. Storing TxtMsgHeadShotID for this index.")
                    cdPrintnl()

                #endif


            ELSE
            
                s_TXD_decision = "CHAR_DEFAULT" //default to blank headshot if the headshot is not ready.
            
                //Default associated headshotID to NULL
                g_TxtMsgHeadshotID[EmailIdentifiedFreeArrayIndex] = NULL


            ENDIF
        
        ELSE
        
            s_TXD_decision = "CHAR_DEFAULT" //default to blank headshot if the headshot is not valid.

            //Default associated headshotID to NULL
            g_TxtMsgHeadshotID[EmailIdentifiedFreeArrayIndex] = NULL


        ENDIF


    ELSE

        //Not a user headshot, use normal character sheet image instead.
        s_TXD_decision = 
        GET_FILENAME_FOR_AUDIO_CONVERSATION (g_sCharacterSheetAll[g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender].picture)
 
        //Default associated headshotID to NULL
        g_TxtMsgHeadshotID[EmailIdentifiedFreeArrayIndex] = NULL




    ENDIF   







            
    IF IS_STRING_NULL_OR_EMPTY (s_TXD_decision)

        
        #if IS_DEBUG_BUILD

            SCRIPT_ASSERT ("Warning: Feed Entry txd decision is null or empty! Using CHAR_DEFAULT instead!")
            PRINTLN("\nWarning: Feed Entry txd decision is null or empty! Using CHAR_DEFAULT instead!\n")

        #endif


        s_TXD_decision = "CHAR_DEFAULT"
    
    ELSE

        IF ARE_STRINGS_EQUAL (s_TXD_decision, "NULL")

            #if IS_DEBUG_BUILD

                //SCRIPT_ASSERT ("Warning: Feed Entry txd decision contains NULL. Using CHAR_DEFAULT instead!")
                PRINTLN("\nWarning: Feed Entry txd decision contains NULL. Using CHAR_DEFAULT instead!\n")

            #endif


            s_TXD_decision = "CHAR_DEFAULT"

        ENDIF

    ENDIF





    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("CELLPHONE CONT - Sender image string was ")
        cdPrintstring (s_TXD_decision)
        cdPrintnl()

    #endif













    IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = CAR_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = SUPERAUTOS_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = LEGENDARY_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = PEDALMETAL_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = WARSTOCK_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = ELITAS_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = DOCKTEASE_LIST_COMPONENT
    OR g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents = DAILYOBJ_LIST_COMPONENT

        /* Removed. See bug 847795. This was used when a new car list notification did not include the full list of cars in the feed entry.
        TEXT_LABEL CarListTokenDecision


        IF g_CarListMessageDecision = NEW_CAR_LIST

            CarListTokenDecision = "CELL_256"

        ELSE 

           CarListTokenDecision = "CELL_254" //Must be updated car list.
           
        ENDIF   
        */

        //BEGIN_TEXT_COMMAND_THEFEED_POST (CarListTokenDecision)








        INT tempConstructionInt = 0


            //Decide which tilda a tilda configuration we should use based on how many cars were stipulated when the messsage was sent.
            SWITCH g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberComponent


                CASE 1
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL01") //This refers to a label with one ~a~ car component...                             
                BREAK

                
                CASE 2
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL02") //This refers to a label with two ~a~ car components...
                BREAK



                CASE 3
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL03") //This refers to a label with three ~a~ car components...                             
                BREAK



                CASE 4
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL04") //This refers to a label with four ~a~ car components...                             
                BREAK


                
                CASE 5
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL05") //This refers to a label with five ~a~ car components...                             
                BREAK



                CASE 6
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL06") //This refers to a label with six ~a~ car components...                             
                BREAK



                CASE 7
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL07") //This refers to a label with seven ~a~ car components...                             
                BREAK



                CASE 8
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL08") //This refers to a label with eight ~a~ car components...
                BREAK


                
                CASE 9
                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_CL09") //This refers to a label with nine ~a~ car components...                             
                BREAK




                ENDSWITCH


            //Add in user-defined label at the very start. i.e The first tilda a tilda substring. 
            //E.g Grab these cars! The list then follows as the specified amount of substrings.

            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailLabel)  
           
            
            //Add each separate car as a substring.
            WHILE tempConstructionInt < g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberComponent
                        
                //ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP. 
                  
                //Adding in support for multiple car lists at request of 1600460    
                SWITCH g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents

                
                        CASE CAR_LIST_COMPONENT
                                                    
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(MPGlobalsAmbience.ImportExportWantedCars[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                        
                        BREAK


                        //New additions.
                        CASE SUPERAUTOS_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SuperAutosVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE LEGENDARY_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_LegendaryVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE PEDALMETAL_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PedalMetalVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE WARSTOCK_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_WarstockVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE ELITAS_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_ElitasVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK


                        CASE DOCKTEASE_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DockTeaseVehicleUnlock[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK

						// url:bugstar:7188269 - Exotic Exports - As a QoL update, can we please add the ability to request a text message from Sessanta that lists the vehicles you still need to get that day?
						
                        CASE DAILYOBJ_LIST_COMPONENT
                        
                             ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DailyObjectivesList[tempConstructionInt]) //This is taken from MP and is the standard import / export list.
                            
                        BREAK

                ENDSWITCH

                  
                  
                  
                    
                tempConstructionInt ++

            ENDWHILE



  
        
            TEXT_LABEL_63 s_TempHolder


            s_TempHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender].label)
            

            #if IS_DEBUG_BUILD
    
                cdPrintstring ("Strings for car list email message are:  ")
                cdPrintstring (s_TXD_decision) 
                cdPrintstring (", ")
                cdPrintstring (s_TempHolder)
                cdPrintnl()

            #endif

            i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_EMAIL, s_TempHolder)
        
            SET_BIT (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP) //Critical!  Let's flashhand know to launch the MP Email App.

            g_Last_App_ActivatableType = ACT_APP_EMAIL
            g_Last_App_ActivatableFeedID = i_FeedItemId

            g_playerTxtmsgHeadshotMP = NULL //Reset headshot holder to NULL now that we have created the feed entry.


    ELSE
        




        BEGIN_TEXT_COMMAND_THEFEED_POST (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailLabel)

            SWITCH g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSpecialComponents


                CASE NO_SPECIAL_COMPONENTS
                   //No further additions to text label string need integrated.
                BREAK



                CASE STRING_COMPONENT

                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailStringComponent)
                    
                    //Check if any additional strings are required...
                    IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)   

                    ENDIF


                    IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent, "NULL"))
                          
                        IF DOES_TEXT_LABEL_EXIST (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)        
                        ENDIF
                    
                    ENDIF

                BREAK




                CASE NUMBER_COMPONENT

                    ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberComponent)
                    
                BREAK





                CASE STRING_AND_NUMBER_COMPONENT
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailStringComponent)
                    ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberComponent)



                    //Check if any additional strings are required...
                    IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)   

                    ENDIF


                    IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent, "NULL"))

                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailThirdStringComponent)        
                        ENDIF
                    ENDIF


                BREAK

            ENDSWITCH

  
            IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender = NO_CHARACTER  //As the sender is no character we can assume this is a gamerTag.

                TEXT_LABEL_63 s_MPGamerTagTempHolder

                //Grab played name and apply prefix and suffix for displaying the sender name with the condensed font.
                s_MPGamerTagTempHolder = "<C>"
                s_MPGamerTagTempHolder += g_Temp_MP_SenderString
                s_MPGamerTagTempHolder += "</C>"

                i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_EMAIL, s_MPGamerTagTempHolder)

                SET_BIT (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP) //Critical!  Let's flashhand know to launch the MP Email App.
                g_Last_App_ActivatableType = ACT_APP_EMAIL
                g_Last_App_ActivatableFeedID = i_FeedItemId



            ELSE



                TEXT_LABEL_63 s_TempHolder, s_SocialClubConstruct


                

                s_TempHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender].label)
           

                #if IS_DEBUG_BUILD
        
                    cdPrintstring ("Strings for email message are:  ")
                    cdPrintstring (s_TXD_decision) 
                    cdPrintstring (", ")
                    cdPrintstring (s_TempHolder)
                    cdPrintnl()

                #endif


                //Check for Social Club sender here. If so, colour yellow. #1562653

                IF g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailSender = CHAR_SOCIAL_CLUB

                    s_SocialClubConstruct = "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_SocialClubConstruct += s_TempHolder
                    

        
                    i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_EMAIL, s_SocialClubConstruct)

                ELSE
                
                    i_FeedItemId = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_decision, s_TXD_decision, FALSE, TEXT_ICON_EMAIL, s_TempHolder)
                
                ENDIF

                SET_BIT (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP) //Critical!  Let's flashhand know to launch the MP Email App. //Critical!  Let's flashhand know to launch the MP Email App.
                g_Last_App_ActivatableType = ACT_APP_EMAIL
                g_Last_App_ActivatableFeedID = i_FeedItemId


                g_playerTxtmsgHeadshotMP = NULL //Reset headshot holder to NULL now that we have created the feed entry.



            ENDIF



    ENDIF

    #if IS_DEBUG_BUILD
    
        cdPrintstring ("CELLPHONE CONT - Email Feed id is ")
        cdPrintint (i_FeedItemId) 
        cdPrintnl()

    #endif

    //Update stored text message array index with the feed id so that it can be instantly removed if the text message is deleted by script or user. 
    g_EmailMessage[EmailIdentifiedFreeArrayIndex].EmailFeedEntryId = i_FeedItemId


ENDPROC









PROC Vibrate_Pad_For_Text_Message()
    #IF IS_DEBUG_BUILD
        IF GET_COMMANDLINE_PARAM_EXISTS("nocellvibrate")
            EXIT
        ENDIF
    #ENDIF

    Get_Cellphone_Owner()






    IF g_bInMultiplayer 
    
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE)

        #if IS_DEBUG_BUILD  
                                                      
            PRINTNL()
            PRINTSTRING("Vibration - text message. Getting MP VIBRATE STAT..........")
            PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE))
            PRINTNL()
        
        #endif

    ENDIF


 
    IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer = 1 //Vibrate is set to ON.
        
        IF NOT IS_PHONE_ONSCREEN()
            SET_CONTROL_SHAKE (PLAYER_CONTROL, 2000, 100)

            #if IS_DEBUG_BUILD  
                                                      
                PRINTNL()
                PRINTSTRING("Vibrating pad for text message ")
                    
            #endif



        ENDIF
    
    ENDIF

    

ENDPROC









PROC Update_Feed_For_New_Text_Message() 


    
    //Adds new text message signifiers ( feed entry, vibration) if flagged for this message.


    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)

                               
            Create_TxtMsg_Feed_Entry()

            Vibrate_Pad_For_Text_Message()

            //As this text message has been seen to arrive in the feed, let it launch appTexts automatically the next time the phone launches.
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = TRUE

            
            //If a new text message has been sent to this character and reached this routine then we can assume it's safe to reset the Read_Auto_Textlaunch timer.
            //The old MustBeRead text message cannot be blocking further message sends for any new message to reach here.


            //By including this clause we can make the timer apply to critical messages only. 
            //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead  //Set by ProcessTextMessage before it returns true.

                IF NETWORK_IS_GAME_IN_PROGRESS()

                    NetRead_Auto_TextLaunchSentTime[g_Cellphone.PhoneOwner] = GET_NETWORK_TIME()

                    Read_Auto_TextLaunch[g_Cellphone.PhoneOwner] = TRUE 


                ELSE

                    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) 

                        Read_Auto_TextLaunchSentTime[g_Cellphone.PhoneOwner] = GET_GAME_TIMER()

                        Read_Auto_TextLaunch[g_Cellphone.PhoneOwner] = TRUE 

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - A new text message has been sent or a buffered message newly added. Recipient owner was ")
                            cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                            cdPrintnl()

                        #endif

                    ELSE
                    

                        Read_Auto_TextLaunchSentTime[CHAR_MICHAEL] = GET_GAME_TIMER()
                        Read_Auto_TextLaunchSentTime[CHAR_TREVOR] = GET_GAME_TIMER()
                        Read_Auto_TextLaunchSentTime[CHAR_FRANKLIN] = GET_GAME_TIMER()

                        Read_Auto_TextLaunch[CHAR_MICHAEL] = TRUE
                        Read_Auto_TextLaunch[CHAR_TREVOR] = TRUE 
                        Read_Auto_TextLaunch[CHAR_FRANKLIN] = TRUE  

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - A new text message has been sent or a buffered message newly added for all SP player characters.")
                            cdPrintnl()

                        #endif
                     
                    ENDIF

                ENDIF   
                                   
            //ENDIF

    
              
              
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER) 
              
        #if IS_DEBUG_BUILD

            cdPrintString("CELLPHONE_CONT - Update_Feed_For_New_Text_Message concluded.")
            cdPrintnl()

        #endif
    
              
                 
          
    ENDIF





ENDPROC







PROC Check_For_CancelledMPTextData()


    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_MP_REQUESTS_RESET_OF_TXT_SIG)

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_MP_REQUESTS_RESET_OF_TXT_SIG)  

             
        Get_Cellphone_Owner()

        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

   
    ENDIF

ENDPROC








PROC Check_For_PlayerSwap()


    Get_Cellphone_Owner()

    


    //Check for any buffered text message or new contact.
    IF g_bInMultiplayer = FALSE
    
        IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()




            IF BufferedText_PostswapPause_Ongoing = FALSE

                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_MICHAEL_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY
                OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_FRANKLIN_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY
                OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_TREVOR_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY


                    SWITCH g_Cellphone.PhoneOwner

                        CASE CHAR_MICHAEL  
                        
                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_MICHAEL_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_CONT - Found buffered text and current phone owner is Michael")
                                    cdPrintnl()   
                                #endif

                                BufferedTextDecision = SP_MICHAEL_BUFFER_INDEX
                                BufferedText_PostswapPause_Ongoing = TRUE 

                                SETTIMERA(0)

                            ENDIF

                        BREAK


                        CASE CHAR_FRANKLIN  
                            
                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_FRANKLIN_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_CONT - Found buffered text and current phone owner is Franklin")
                                    cdPrintnl()   
                                #endif
                                
                                BufferedTextDecision = SP_FRANKLIN_BUFFER_INDEX
                                BufferedText_PostswapPause_Ongoing = TRUE 

                                SETTIMERA(0)

                            ENDIF

                        BREAK                                                             
                                    

                        CASE CHAR_TREVOR 

                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SP_TREVOR_BUFFER_INDEX].TxtMsgLockStatus <> TXTMSG_EMPTY

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_CONT - Found buffered text and current phone owner is Trevor")
                                    cdPrintnl()   
                                #endif

                                BufferedTextDecision = SP_TREVOR_BUFFER_INDEX
                                BufferedText_PostswapPause_Ongoing = TRUE 

                                SETTIMERA(0)

                            ENDIF

                        BREAK


                        DEFAULT

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_CONT - Owner was strangely DEFAULT during BufferedText_PostswapPause_Ongoing checks.")
                                cdPrintnl()   
                            #endif


                        BREAK

                    ENDSWITCH


                ENDIF


            ELSE  // ELSE BufferedText_PostswapPause_Ongoing = TRUE clause...


                //If a message has been found in the buffer slot for this player character, send it on to him...
                //Each SP player character has a special array position reserved between the end of the SP reserved portion and the beginning of the MP portion.

                    //For 2413878, it seems as if the player could temporarily be regarded as Michael when playing as a non-story character when leaving DM. This meant that swapping
                    //back to story mode from director mode and entering the game as Franklin or Trevor could result in them receiving Michael's buffered text messages.
                    //Implementing a timed check to make sure that the character under control is definitely the same as the buffer index detected before sending on the message.

                    IF TIMERA() > 15000

                        BufferedText_PostswapPause_Ongoing = FALSE //Vital. If a change in character is detected we would need to go through the process again if the message hasn't been sent on.

                        BOOL b_BufferChangeDetected = FALSE


                        Get_Cellphone_Owner() // Ten seconds has elapsed after we detected a buffered text message, let's check it's still the same player under control.


                        IF BufferedTextDecision = SP_MICHAEL_BUFFER_INDEX
                        AND (g_Cellphone.PhoneOwner <> CHAR_MICHAEL)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_CONT - Had Buffered message for Michael to deliver but player under control is no longer Michael. Aborting send on.")
                                cdPrintnl()   
                            #endif

                            b_BufferChangeDetected = TRUE

                        ENDIF



                        IF BufferedTextDecision = SP_FRANKLIN_BUFFER_INDEX
                        AND (g_Cellphone.PhoneOwner <> CHAR_FRANKLIN)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_CONT - Had Buffered message for Franklin to deliver but player under control is no longer Franklin. Aborting send on.")
                                cdPrintnl()   
                            #endif

                            b_BufferChangeDetected = TRUE

                        ENDIF


                        IF BufferedTextDecision = SP_TREVOR_BUFFER_INDEX
                        AND (g_Cellphone.PhoneOwner <> CHAR_TREVOR)

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_CONT - Had Buffered message for Trevor to deliver but player under control is no longer Trevor. Aborting send on.")
                                cdPrintnl()   
                            #endif

                            b_BufferChangeDetected = TRUE

                        ENDIF









                        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgLockStatus <> TXTMSG_EMPTY
                        AND b_BufferChangeDetected = FALSE

                            #if FEATURE_SP_DLC_DIRECTOR_MODE
                                IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //Block buffer emptying whilst in Director Mode: 2413878
                            #endif


                                //There's a possibility that the text message might not be able to be sent, but won't spam rag here...
                                IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (
                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgSender,
                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgLabel,
                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgLockStatus,

                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgCritical,

                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgAutoUnlockStatus,


                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgReplyStatus,

                                    ICON_STANDARD_TEXTMSG,
                                    TRUE,

                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgBarterStatus,
                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgCanCallSenderStatus)

                                

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("CELLPHONE_CONT - BUFFERED TEXT Sent buffered text message to new current player. Should not see this in Director Mode.")
                                        cdPrintnl()   
                                    #endif


                                    //VITAL. Set buffer back to empty for the player character.
                                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[BufferedTextDecision].TxtMsgLockStatus = TXTMSG_EMPTY


                                ENDIF

                            #if FEATURE_SP_DLC_DIRECTOR_MODE
                            ENDIF
                            #endif


                        ENDIF   //<> TXTMSG_EMPTY and b_bufferChangeDetected check.

                    ENDIF //TimerA elapsed check.



            ENDIF   //BufferedText_PostswapPause_Ongoing = FALSE
         







            IF BufferedContact_PostswapPause_Ongoing = FALSE

            
                //Check if the player character has any buffered contact that requires a feed entry.
                IF BufferedContactAvailable[ENUM_TO_INT(g_Cellphone.PhoneOwner)]

                    BufferedContact_PostswapPause_Ongoing = TRUE


                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTSTRING("CELLPHONE_CONT - Found buffered contact for new cellphone owner. Now starting threshold pause before submitting feed entry.")
                        PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText))
                        PRINTNL()
                    #endif


                    BufferOwner = g_Cellphone.PhoneOwner


                    IF NETWORK_IS_GAME_IN_PROGRESS()

                        Net_Postswap_Buffer_StartTime = GET_NETWORK_TIME()

                    ELSE

                        PostSwap_Buffer_StartTime = GET_GAME_TIMER()

                    ENDIF

                ENDIF

            
            ELSE
            
                IF NETWORK_IS_GAME_IN_PROGRESS()

                    PostSwap_Buffer_TimeDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_Postswap_Buffer_StartTime)

                ELSE    

                    PostSwap_Buffer_CurrentTime = GET_GAME_TIMER()

                    PostSwap_Buffer_TimeDifference = PostSwap_Buffer_CurrentTime - PostSwap_Buffer_StartTime

                ENDIF


                IF PostSwap_Buffer_TimeDifference > PostSwap_Buffer_TimeThreshold


                    IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)

                        SignifierData[SIG_CONTACT1].SignifierPrimaryText = BufferedContactSignifierText[ENUM_TO_INT(g_Cellphone.PhoneOwner)]

                        g_Temp_ContactHeadshotRef = BufferedHeadshotRef[ENUM_TO_INT(g_Cellphone.PhoneOwner)]


                        //Commented out - occasionally this is overwritten by *something*,  possibly a save and can result in a NULL passed. As this only happens twice in the
                        //entire game, we're better off removing the generation of this buffered contact feed message. See #1583935
                        //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)  //Will trigger a new contact feed entry via bit check in this script

                        BufferedContactAvailable[ENUM_TO_INT(g_Cellphone.PhoneOwner)] = FALSE


                        #if IS_DEBUG_BUILD
                            PRINTNL()
                            PRINTSTRING("CELLPHONE_CONT - Found buffered contact for new cellphone owner. No longer creating feed entry for buffered contact. See bug #1583935.")
                            PRINTNL()
                            PRINTSTRING("Contact is in phonebook as ")
                            PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText))
                            PRINTNL()
                        #endif


                        
                        BufferedContact_PostswapPause_Ongoing = FALSE

                    ENDIF

                ENDIF


                IF BufferOwner <> g_Cellphone.PhoneOwner //If the cellphone owner has change during the buffer pause, abandon but don't clear the buffer.

                    BufferedContact_PostswapPause_Ongoing = FALSE

                ENDIF

                
            ENDIF  //BufferedContact_PostswapPause_Ongoing = FALSE condition check.





       ENDIF  //end of NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY() condition check.
   
     ENDIF  //end of g_bInMultiplayer condition check.



ENDPROC





FUNC BOOL Check_For_Takeout_Only()

    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))
    //AND NOT (Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT)))
    AND NOT (IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL))  //fix for 653167
    AND NOT (IS_STUNT_JUMP_IN_PROGRESS()) //2177764


        //2260093 - TODO request to display a "phone may be impeded" feed message if the tamper decorator exists on the player. 
        IF IS_PC_VERSION()
            IF DECOR_EXIST_ON(PLAYER_PED_ID(), "Synched")

                STRING AlertHeadingString = GET_FILENAME_FOR_AUDIO_CONVERSATION ("CELL_ANTIH_A")

                BEGIN_TEXT_COMMAND_THEFEED_POST("CELL_ANTIH") 
                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, AlertHeadingString)

            ENDIF
        ENDIF



        
        //Don't allow the phone to pop up whilst using the sniper scope - #2032679
        //CHECK_AIMING_FIRING_RAGDOLL_STATE_FOR_DPAD_TAKEOUT() further down this function originally handled other aiming checks but the sniper did not set the aiming flag correctly.

        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
            IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
            OR IS_PED_AIMING_FROM_COVER(PLAYER_PED_ID()) //2170729 - this means any weapons aimed from cover will block launch but that's okay, the loophole needed closed across them all.
                                                         //CHECK_AIMING_FIRING... does an IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsAimingGun) check anyway which means that aiming with 
                                                         //most guns would already block the launch. If the phone's already up, then you can aim. Aiming with the sniper when the phone is up is 
                                                         //a special case because of the overlay and is handled in cellphone_flashhand in the b_Sniper_equipped section.

                IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
                OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
                    RETURN FALSE
                ENDIF

            ENDIF
        ENDIF
        



        CellphoneAppAutoLaunchType = ACT_APP_NONE //Reset these as a precaution.
        g_ShouldForceSelectionOfLatestAppItem = FALSE
        

        IF THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID() =  g_Last_App_ActivatableFeedID  //The last activatable ID single var should be set whenever an activatable feed entry is generated.
        AND THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID() <> -1 // The default value when no feed is ongoing.


                CellphoneAppAutoLaunchType = g_Last_App_ActivatableType //Grab this now in case it changes during elongated press duration       
                    
                
                DpadElongated_StartTime = GET_GAME_TIMER()
                DpadElongated_Difference = 0
                b_DpadElongatedSuccessfullyExpired = FALSE


                WHILE Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))
                AND DpadElongated_Difference < 250 //Elongated press time.
                AND g_b_PreloadOngoing = FALSE //Make sure any phone call takes precedence here.
                //AND NOT (Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT)))
                AND NOT (IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)) 

                    IF g_Current_Number_Of_Signifiers > 0
                        DrawSignifierMovie()
                    ENDIF

                    WAIT (0)
                
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Check_For_Takeout_Only() waiting to return true or false...")
                        cdPrintnl()   
                    #endif

                    DpadElongated_CurrentTime = GET_GAME_TIMER()

                    DpadElongated_Difference = DpadElongated_CurrentTime - DpadElongated_StartTime

                    
                    IF g_Current_Number_Of_Signifiers > 0
                        DrawSignifierMovie()
                    ENDIF


                ENDWHILE

                

                IF Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT)) //Check that this is still held...
                AND g_b_PreloadOngoing = FALSE     //...and checking this breakout clause.
                AND NOT (IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL))  //Finally, Making sure this didn't break us out of while loop!
           
                    b_DpadElongatedSuccessfullyExpired = TRUE  // All are okay, must have been successful expiration of DpadElongated_Difference that caused WHILE breakout! 


                     #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Check_For_Takeout_Only() - successful elongated expiration...")
                        cdPrintnl()   
                    #endif

                ENDIF


                IF g_b_PreloadOngoing = FALSE

                    IF b_DpadElongatedSuccessfullyExpired = TRUE      

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Check_For_Takeout_Only() says Elongated timer successfully expired. ACT_APP state is ")
                            cdPrintint(ENUM_TO_INT(CellphoneAppAutoLaunchType))
                            cdPrintnl()   
                            cdPrintstring ("DpadElongated_Difference was ")
                            cdPrintint(DpadElongated_Difference)
                            cdPrintnl()
                        #endif

                        g_ShouldForceSelectionOfLatestAppItem = TRUE

                        CellphoneAppAutoLaunchType = g_Last_App_ActivatableType //Grab this now in case it changes during elongated press duration

                        IF CellphoneAppAutoLaunchType = ACT_APP_EMAIL

                            //g_Redial_NumpadEntryString[0] = "2221"
                            //g_Redial_NumpadEntryString[1] = "2221"
                            //g_Redial_NumpadEntryString[2] = "2221"

                        ENDIF


                        IF CellphoneAppAutoLaunchType = ACT_APP_INVITE

                            //g_Redial_NumpadEntryString[0] = "2222"
                            //g_Redial_NumpadEntryString[1] = "2222"
                            //g_Redial_NumpadEntryString[2] = "2222"

                        ENDIF


                        IF CellphoneAppAutoLaunchType = ACT_APP_TEXT

                            //g_Redial_NumpadEntryString[0] = "2223"
                            //g_Redial_NumpadEntryString[1] = "2223"
                            //g_Redial_NumpadEntryString[2] = "2223"

                        ENDIF


                        IF CellphoneAppAutoLaunchType = ACT_APP_NONE

                            //g_Redial_NumpadEntryString[0] = "2224"
                            //g_Redial_NumpadEntryString[1] = "2224"
                            //g_Redial_NumpadEntryString[2] = "2224"

                        ENDIF


                        IF CellphoneAppAutoLaunchType = ACT_APP_BOSSINV

                            //g_Redial_NumpadEntryString[0] = "2225"
                            //g_Redial_NumpadEntryString[1] = "2225"
                            //g_Redial_NumpadEntryString[2] = "2225"

                        ENDIF



                    ELSE

                        CellphoneAppAutoLaunchType = ACT_APP_NONE
                        
                        //g_Redial_NumpadEntryString[0] = "333"
                        //g_Redial_NumpadEntryString[1] = "333"
                        //g_Redial_NumpadEntryString[2] = "333"

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Check_For_Takeout_Only() says b_DpadElongatedSuccessfullyExpired is still FALSE. Weapon Wheel? ACT_APP state is ")
                            cdPrintint(ENUM_TO_INT(CellphoneAppAutoLaunchType))
                            cdPrintnl()   
                        #endif


                    ENDIF                

                ELSE

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Check_For_Takeout_Only() reporting a preload was ongoing. Setting ACT_APP_NONE state.")
                        cdPrintnl()   
                    #endif

                    CellphoneAppAutoLaunchType = ACT_APP_NONE

                    //g_Redial_NumpadEntryString[0] = "444"
                    //g_Redial_NumpadEntryString[1] = "444"
                    //g_Redial_NumpadEntryString[2] = "444"


                ENDIF


        ELSE
 

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("Check_For_Takeout_Only() says THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID() is weird or -1")
                cdPrintint(ENUM_TO_INT(CellphoneAppAutoLaunchType))
                cdPrintnl()   
            #endif


            CellphoneAppAutoLaunchType = ACT_APP_NONE


        ENDIF



        
        //Recheck clause post-while before returning true...
        //IF NOT (Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT)))
        IF NOT (IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL))  //fix for 653167


            RETURN TRUE

        ELSE

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("WARNING! Check_For_Takeout_Only() was checking for elongated press but weapon wheel pressed during interim. ")
                cdPrintnl()   
            #endif

            CellphoneAppAutoLaunchType = ACT_APP_NONE

            g_ShouldForceSelectionOfLatestAppItem = FALSE


            RETURN FALSE

        ENDIF

    ELSE


        //Do not reset auto launch or force selection here! The func running again even without a Is_Phone_Control_Just_Pressed may overwrite previous details.

        RETURN FALSE

    ENDIF


ENDFUNC




PROC Check_User_Altering_PhoneDisplayStatus()

    
    IF Check_For_Takeout_Only()
    OR IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_LAUNCH_PHONE_TO_HOMESCREEN)  
      
        /*  Removed 31.08.10
            WHILE IS_BUTTON_PRESSED(PAD1, INT_TO_ENUM (PAD_BUTTON_NUMBER, PHONE_TAKEOUT_INPUT))
                WAIT(0)
            ENDWHILE
        */

        
            


        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_LAUNCH_PHONE_TO_HOMESCREEN)


        Get_Cellphone_Owner()



        /*Temp for Gurney... Keith to trigger in his stuff alternatively.
        #if IS_DEBUG_BUILD
        
            IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()    
            AND g_bInMultiplayer

                cdPrintstring ("CELLPHONE_CONT - In MP and Net Code Development Mode has passed true. Will not launch phone.")
                cdPrintnl ()
                                
                EXIT

            ENDIF
        
        #endif
        */
        
        
        


        //Update 25.11.11 -  by request of TODO 293726 this check has been removed. Phone can be taken out in water.
        /*
        IF NOT (g_Cellphone.PhoneOwner = CHAR_FRANKLIN) //i.e Michael or Trevor
        AND IS_ENTITY_IN_WATER (PLAYER_PED_ID())        //and they are in the water
        AND NOT g_bInMultiplayer                        //and we're in singleplayer...

            #if IS_DEBUG_BUILD
                
                cdPrintstring ("CELLPHONE_CONT - Can't launch phone as current player character does not have waterproof phone.")
                cdPrintnl ()

            #endif




        ELSE //Franklin has a waterproof phone or Michael and Trevor are not in water.
        */

        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
        
            //The above has gone back in as directed by 990085. Phone use is also killed in cellphone flashhand again.
            IF NOT (IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID()))



                IF CHECK_AIMING_FIRING_RAGDOLL_STATE_FOR_DPAD_TAKEOUT()

                    #if IS_DEBUG_BUILD
                    
                        cdPrintstring ("CELLPHONE_CONT - Can't launch phone as current player character aim, target, fire , ragdoll check has returned TRUE.")
                        cdPrintnl ()

                    #endif


                ELSE
                  
                        //Make sure phone cannot be taken out again until it has been fully put away by checking if cellphone_flashhand has terminated. See Bug 11411
                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0
                       

                            //If the phone is playing interference, the user taking the phone out will automatically abort the interference phase and begin ringing.
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)

                                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)

                            ENDIF


                            //Preload Work.
                            IF g_b_PreloadOngoing = TRUE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("WARNING! Cellphone_Controller has forced breakout from preload as phone has been taken out.")
                                    cdPrintnl()   
                                #endif


                                g_b_PreloadOngoing = FALSE

                            ENDIF
                            


                            SWITCH g_Cellphone.PhoneDS
                            
                                CASE PDS_AWAY
                                    g_Cellphone.PhoneDS = PDS_TAKEOUT

									IF b_DisplayedFeedMessageThisSession = FALSE //2697795 - Check if we need to display a feed message for any unread emails.
        								DisplayUnreadMarketingMessage()
										b_DisplayedFeedMessageThisSession = TRUE
									ENDIF
		
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("STATE ASSIGNMENT 35. Cellphone_Controller assigns PDS_TAKEOUT")
                                        cdPrintnl()   
                                    #endif

                                BREAK

                                /*Handle this transition in cellphone_flashhand.sc
                                CASE PDS_MINIMAL
                                    g_Cellphone.PhoneDS = PDS_MAXIMUM
                                BREAK
                                */

                                DEFAULT
                                //Do nothing as yet...
                                BREAK




                            ENDSWITCH




                            g_Cellphone_Launched_by_Button_Press = TRUE



                            

                            #if IS_DEBUG_BUILD

                                IF g_bInMultiplayer
                                    
                                    cdPrintstring ("CELLPHONE_CONT - Launching MULTIPLAYER cellphone November 2018...")
                                    cdPrintnl()
                                    
                                ELSE

                                    cdPrintstring ("CELLPHONE_CONT - Launching SINGLEPLAYER cellphone...")
                                    cdPrintnl() 

                                ENDIF


                            #endif 
                        
                        ENDIF


                
                ENDIF


            ELSE


                #if IS_DEBUG_BUILD
            
                    cdPrintstring ("CELLPHONE_CONT - Can't launch phone as current player character is swimming under water.")
                    cdPrintnl ()

                #endif


            ENDIF //End of "entity in water" check that has been removed.

        ENDIF //ped dead check


    ENDIF
   


ENDPROC





PROC Cellphone_Draw_Status()


    SWITCH g_Cellphone.PhoneDS


        
            
        CASE PDS_TAKEOUT

            //New
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
                


                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_CONT - Radio interference bit set.")
                        cdPrintnl()
                        cdPrintnl()
                    #endif


                    

                    //Temp! Clear bit immediately for safe check in until sound is actually working.  Check Master Mobile Interfence Control in main loop now.
                    //CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
          

            ELSE
           




                IF IS_THREAD_ACTIVE (Cellphone_FlashResponseThread) //Hack in for holiday... This should stop the phone reappearing during a call with replies segment.

                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_CONT - Cellphone takeout cued but already onscreen.")
                        cdPrintnl()
                        cdPrintnl()
                    #endif

                    

                ELSE

                
                                
                
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_CONT - Cellphone takeout cued...")
                        cdPrintnl ()
                        cdPrintnl ()
                    #endif


                    


                                
                             
                    IF NOT IS_THREAD_ACTIVE (Cellphone_FlashResponseThread) 
                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0

                           
                            //request_load_scriptstring ("cellphone_flashhand") //Can't use this any longer because of 2067176. Need to render signifier movie during request pause.

                            REQUEST_SCRIPT ("cellphone_flashhand")
                                                     
                            WHILE NOT HAS_SCRIPT_LOADED ("cellphone_flashhand")
                           
                                WAIT(0)

                                IF g_Current_Number_Of_Signifiers > 0
                                    DrawSignifierMovie()
                                ENDIF

                            ENDWHILE





                            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 //1943564 - Required to prevent rare double launch of cellphone_flashhand.
                            
                                Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", DEFAULT_STACK_SIZE)

                            ENDIF

                            SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhand")



                        ENDIF
                    ENDIF



                    HIDE_PHONE_FOR_HOTSWAP (FALSE) //we need to make sure that the phone can be drawn in case any hotswap which had hidden the phone failed to complete.


                       
                    //Create_3dPhoneObject() //from cellphone_private.sch   //Change requested by bug number 310808 - now in cellphone_flashhand.sc      
                


                ENDIF





                    

                //NB. This is an important but quite tricky section. Dialogue handler, which controls all phonecalls can stipulate the contents of
                //g_UpcomingConversationInboundForCellphone and g_ShouldPhoneBeForcedOnScreen. If either are true, i.e someone is phoning the player or
                //we want to force the phone into hand for a cutscene, then dialogue handler will also set g_Cellphone.PhoneDS = PDS_TAKEOUT.
                //This script detects this in this switch, and launches flash handler as above. Below, we check for either inbound or force status being
                //true and tell cellphone_flashhand.sc to display the calling screen after we have launched that script.

                //Otherwise we will display phone in normal launch state if no phonecall is inbound and the phone should not be forced in hand.

                IF g_UpcomingConversationInboundForCellphone
                OR g_ShouldPhoneBeForcedOnScreen 


                                
                    //Make sure cellphone_flashhand is running as a failsafe. It should get loaded up above
                    IF NOT IS_THREAD_ACTIVE (Cellphone_FlashResponseThread)
                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0

                            //request_load_scriptstring ("cellphone_flashhand") //Can't use this any longer because of 2067176. Need to render signifier movie during request pause.

                            REQUEST_SCRIPT ("cellphone_flashhand")
                                                     
                            WHILE NOT HAS_SCRIPT_LOADED ("cellphone_flashhand")
                           
                                WAIT(0)

                                IF g_Current_Number_Of_Signifiers > 0
                                    DrawSignifierMovie()
                                ENDIF

                            ENDWHILE




                            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 //1943564 - Required to prevent rare double launch of cellphone_flashhand.
                            
                                Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", DEFAULT_STACK_SIZE)

                            ENDIF

                            SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhand")

                        ENDIF
                    ENDIF


                    //Autoplay_Fix
                    //Perhaps change this to use g_B_Scaleform_Movies_Loaded? Find out if this could possibly change index value...
                    //This could be part of a timeout clause. 
                                
                    WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)  //make sure that movies requested by cellphone_flashhand have been loaded in.
                    AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE  //Only perform this WHILE check if the MovieFail has not been flagged TRUE. Critical!
                    //OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)
                        WAIT(0)

                        IF g_Current_Number_Of_Signifiers > 0
                            DrawSignifierMovie()
                        ENDIF


                    ENDWHILE


                    //Important section!               

                    IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE

                        //Critical!
                        g_Cellphone.PhoneDS = PDS_ONGOING_CALL  //This will let buildcellphone within dialogue handler continue, as its "looped while" pause can now pass through.
                                    
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("STATE ASSIGNMENT 36_29. Cellphone_Controller assigns PDS_ONGOING_CALL")
                            cdPrintnl()   
                    

                     
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Cellphone Controller has set PDS to ONGOING_CALL state.")
            
                        #endif 


                        Update_Scaleform_VSE ()

                    ENDIF



                ELSE
                
                    b_AutoJoblistLaunchCheck = FALSE //Reset before checking MP and global joblist auto launch check.
                    
                                   
                    IF g_bInMultiplayer
                    AND (g_autolaunchJobListApp  OR g_sBossAgencyApp.bAutoLaunchAppOnDpadUp ) //This is set and reset by Keith. See bug 955417
			

                        b_AutoJoblistLaunchCheck = TRUE

                            
                        #if IS_DEBUG_BUILD
                         
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Cellphone Controller has found g_autolaunchJobListApp set to TRUE")
            
                        #endif

                    ENDIF


                    //We should launch the text message or joblist app straight away if the critical message or LaunchToText flags are true.
            
                    IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = TRUE
                    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
                    OR b_AutoJoblistLaunchCheck = TRUE
                    OR CellphoneAppAutoLaunchType <> ACT_APP_NONE

           

                        WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)  //make sure that movies requested by cellphone_flashhand have been loaded in.
                        AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE //Only perform this WHILE check if the MovieFail has not been flagged TRUE. Critical!

                            WAIT(0)
                            
                            IF g_Current_Number_Of_Signifiers > 0
                                DrawSignifierMovie()
                            ENDIF

                        ENDWHILE

                    
                        //For bug 110732, potentially have the launch triggered from within cellphone_flashhand via a bitset global? That way the script can only launch
                        //if flashhand is still running. We'll see how potential fix goes. 31.08.11
                        IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
                            //Check for inbound

                            IF g_Cellphone.PhoneDS > PDS_AWAY //Bug 352962 necessitated this addition, although the issue was dormant for movie fails.

                                g_Cellphone.PhoneDS = PDS_MAXIMUM //this needs to be here to prevent PDS_TAKEOUT state retriggering - can't use PDS_RUNNINGAPP as that is part of a secondary check
                                                                  //in LAUNCH_CELLPHONE_APPLICATION logic.

                             
                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("STATE ASSIGNMENT 37. Cellphone_Controller assigns PDS_MAXIMUM")
                                    cdPrintnl()   
                                #endif


                                IF CellphoneAppAutoLaunchType = ACT_APP_NONE //Must have been text message vars or joblist check from quick takeout that got us to this point...


                                        IF b_AutoJoblistLaunchCheck //Must be in multiplayer for this to have returned true, so Joblist is available.
                                                                    //Auto launch of joblist takes precedence over new text messages. After Keith clears the joblist flag, the text
                                                                    //message app will launch next time round if the phone is put away and taken out again without opening any texts.

                                            // KGM 6/12/12: We no longer want to launch the app, but the joblist icon should be highlighted (it is automatically at the moment)
                                            //              However, it should still take priority over Critical Text Messages so I've left the code as-is but with the launch line commented out
                                            //LAUNCH_CELLPHONE_APPLICATION (AppMPJOBLIST, FALSE, TRUE)

                                        ELSE


                                            IF g_bInMultiplayer = FALSE //It has been requested that a stored, unread text does not force the phone to the text screen on takeout. #985697

                                                
                                                //Removed for 185537 as part of g_BSTU_QUICK_LAUNCH_SNAPMATIC mechanism. Texts use the elongated press like the invites and email now.
                                                //LAUNCH_CELLPHONE_APPLICATION (appTEXTS, FALSE, TRUE) //Launch appTextMessage straight away, do not load scaleform movies again and override screen check
                                                
                                                //Added for 1918479 to handle potential block of critical messages. 
                                                IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE

                                                    #if IS_DEBUG_BUILD
                                                        cdPrintnl()
                                                        cdPrintstring("CELLPHONE_CONTROLLER - Cellphone_Controller LAUNCHING TEXT MESSAGE App for Critical message.")
                                                        cdPrintnl()   
                                                    #endif


                                                    CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (appTEXTS, FALSE, TRUE)
                                                    

                                                    /*
                                                    
                                                    LAUNCH_CELLPHONE_APPLICATION (appTEXTS, FALSE, TRUE)

                                                    */

                                                ENDIF

                                            ENDIF

                                        ENDIF 

                                
                                ELSE
                                
                                    
                                    IF CellphoneAppAutoLaunchType = ACT_APP_TEXT

                                                                   
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("CELLPHONE_CONT - ACT_APP_TEXT auto launch through flashhand signposted!")
                                            cdPrintnl()   
                                        #endif


                                    ENDIF

                                    
                                    IF CellphoneAppAutoLaunchType = ACT_APP_EMAIL

                                                                   
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("CELLPHONE_CONT - ACT_APP_EMAIL auto launch through flashhand signposted!")
                                            cdPrintnl()   
                                        #endif


                                    ENDIF


                                    IF CellphoneAppAutoLaunchType = ACT_APP_INVITE

                                                                   
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("CELLPHONE_CONT - ACT_APP_INVITE auto launch through flashhand signposted!")
                                            cdPrintnl()   
                                        #endif


                                    ENDIF


                                    IF CellphoneAppAutoLaunchType = ACT_APP_BOSSINV

                                                                   
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("CELLPHONE_CONT - ACT_APP_BOSSINV auto launch through flashhand signposted!")
                                            cdPrintnl()   
                                        #endif


                                    ENDIF



                                
                                ENDIF                    

                            ELSE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_CONT - Whilst waiting on movies to load from flashhand after takeout, phone was set to away or disabled, not assigning PDS_MAXIMUM, EC 1")
                                    cdPrintnl()   
                                #endif

                            ENDIF


                        ENDIF

                    ELSE


                        WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)  //make sure that movies requested by cellphone_flashhand have been loaded in.
                        AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE  //Only perform this WHILE check if the MovieFail has not been flagged TRUE. Critical!
                            WAIT(0)
                            
                            IF g_Current_Number_Of_Signifiers > 0
                                DrawSignifierMovie()
                            ENDIF

                        ENDWHILE


                        IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
                            

                            IF g_Cellphone.PhoneDS > PDS_AWAY //Bug 352962 necessitated this addition, although the issue was dormant for movie fails.


                                IF g_UpcomingConversationInboundForCellphone
                                OR g_ShouldPhoneBeForcedOnScreen

                                    g_Cellphone.PhoneDS = PDS_ONGOING_CALL  //This will let buildcellphone within dialogue handler continue, as its "looped while" pause can now pass through.
                                    
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("STATE ASSIGNMENT EMERGENCY CLAUSE 1. Cellphone_Controller wasn't expecting a call when taken out but one has appeared during movie load. #1520932")
                                        cdPrintnl()   
                                    #endif


                                ELSE

                                    g_Cellphone.PhoneDS = PDS_MAXIMUM

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("STATE ASSIGNMENT 38. Cellphone_Controller assigns PDS_MAXIMUM")
                                        cdPrintnl()   
                                    #endif

                                ENDIF

                            ELSE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_CONT - Whilst waiting on movies to load from flashhand after takeout, phone was set to away or disabled, not assigning PDS_MAXIMUM, EC 2")
                                    cdPrintnl()   
                                #endif

                            ENDIF

                                                               
                        ENDIF
        
                    ENDIF





                ENDIF
            
            

            ENDIF //g_BSC_PLAY_INTERFERENCE_AUDIO


        BREAK


        /* Comment these in if we need to see explicit details - they will spam every frame though!
        CASE PDS_AWAY

            #if IS_DEBUG_BUILD
    
                cdPrintnl()
                cdPrintstring ("CELLPHONE_CONTROLLER - Cellphone_Draw_Status reports that phone state has been set to PDS_AWAY")
                cdPrintnl()
                
            #endif 

        BREAK



        CASE PDS_DISABLED

            #if IS_DEBUG_BUILD
    
                cdPrintnl()
                cdPrintstring ("CELLPHONE_CONTROLLER - Cellphone_Draw_Status reports that phone state has been set to PDS_DISABLED")
                cdPrintnl()
                
            #endif 

        BREAK



        DEFAULT

           #if IS_DEBUG_BUILD
    
                cdPrintnl()
                cdPrintstring ("CELLPHONE_CONTROLLER - Cellphone_Draw_Status reports that phone state is not DISABLED, TAKEOUT or AWAY")
                cdPrintnl()
                
            #endif 

        BREAK
        */


    ENDSWITCH


ENDPROC












/*
PROC Cinematic_Cam_Handler()


    IF g_DelayCinematicCamReinstatement = INITIALISE_DELAY 

        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))


        SETTIMERB(0)

        g_DelayCinematicCamReinstatement = ONGOING_DELAY

    ENDIF



    IF g_DelayCinematicCamReinstatement = ONGOING_DELAY


        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))

        IF TIMERB() > 500 

            //SET_CINEMATIC_BUTTON_ACTIVE (TRUE)
            g_DelayCinematicCamReinstatement = NO_DELAY_REQUIRED
        
        ENDIF


    ENDIF


ENDPROC
*/


















PROC Check_For_Disable_Cellphone_ThisFrameOnly()



    IF (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME))

        IF NOT (g_Cellphone.PhoneDS = PDS_DISABLED) //If the phone has already been permanently disabled, don't set state to this frame only.

            g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY

            #if IS_DEBUG_BUILD
                g_Cellphone_Disable_ThisFrameOnly_WidgetToggle = TRUE
            #endif

        ENDIF

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME)

        EXIT //Exit the procedure this frame.

    ENDIF


    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME))


        IF NOT (g_Cellphone.PhoneDS = PDS_DISABLED)  //Perhaps just check for is PDS_DISABLED_THIS_FRAME_ONLY?

            IF g_Cellphone.PhoneDS < PDS_TAKEOUT    //If the phone's in takeout state or beyond, don't assign AWAY.  Critical!
                g_Cellphone.PhoneDS = PDS_AWAY      //only re-enables to AWAY state if phone is not in permanently disabled state. 
            ENDIF

        ENDIF

        #if IS_DEBUG_BUILD
            g_Cellphone_Disable_ThisFrameOnly_WidgetToggle = FALSE
        #endif


        EXIT
        
    ENDIF




ENDPROC





PROC Check_For_Disable_Camera_App_ThisFrameOnly()


    IF (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME))

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)
        
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

        //#if IS_DEBUG_BUILD()
        //  DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "IS_REQUEST_BIT_SET")
        //  WAIT(2000)
        //#endif

        EXIT //Exit the procedure this frame.

    ENDIF


    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME))

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)

        //#if IS_DEBUG_BUILD()
        //  DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "NOT_SET")
        //  WAIT(2000)
        //#endif


        EXIT
        
    ENDIF

    
ENDPROC




PROC Check_For_Disable_Internet_App_ThisFrameOnly()


    IF (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_INTERNET_APP_THIS_FRAME))

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME)

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_INTERNET_APP_THIS_FRAME)


        EXIT

    ENDIF


    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_INTERNET_APP_THIS_FRAME))

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME)


        EXIT

    ENDIF



ENDPROC

























#if IS_DEBUG_BUILD

PROC Create_Cellphone_Widget_Group()



    START_WIDGET_GROUP ("Cellphone and Dialogue Debug")

        ADD_WIDGET_BOOL("Disable Cellphone", g_Cellphone_Disable_WidgetToggle)
        ADD_WIDGET_BOOL("Disable Eyefind Test", g_DisplayNewSideTaskSignifier) 
        ADD_WIDGET_BOOL("Display Debug Text in console window outwith stream", g_b_CellDialDebugTextToggle)
        ADD_WIDGET_BOOL("Dump Disable Every Frame details to RAG", g_DumpDisableEveryFrameCaller)
        ADD_WIDGET_BOOL("Test First Person Mode Animations", g_b_ShouldCellphoneUseFirstPersonAnims)
        ADD_WIDGET_BOOL("Use new Contact Feed - Esteban", g_UseContactSignifierNewType)
        ADD_WIDGET_BOOL("Set Eyefind send tag to Marketing", g_DisplayTestPhotoSprite)
        ADD_WIDGET_BOOL("Autofill Message body to bypass profanity check", g_b_InstructEyefindtoSendAutoLongMessage)
        ADD_WIDGET_BOOL("Force re-run of MktPosix grab", g_b_ForceRerunOfMarketingPosixGrab)
        ADD_WIDGET_BOOL("Force Best Quality Photo Sprite", g_HighqualityPhotoSprite)
        ADD_WIDGET_BOOL("Cellphone requested quicksave", g_HasCellphoneRequestedSave)
        ADD_WIDGET_BOOL("Cellphone Disabled This Frame Only", g_Cellphone_Disable_ThisFrameOnly_WidgetToggle)
        ADD_WIDGET_BOOL("Click to test disable every frame", b_TickToRequestDisableEveryFrameOption)
        ADD_WIDGET_BOOL ("Display running movie dump", g_b_DisplayDrawingMovieDump)



        //ADD_WIDGET_BOOL("Enable Feed entries for texts.", b_EnableFeedEntry)

        ADD_WIDGET_BOOL("Enable trial Phone Hide during call", g_b_AllowHidePhoneDuringCallTrial)

                                                                            
        ADD_WIDGET_BOOL("Enable SP Trackify app on homescreen", g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable)
        
        ADD_WIDGET_BOOL("Test MP auto launch to AppMPJOBLIST", g_autolaunchJoblistApp)                                                                 
                                                                            
        ADD_WIDGET_BOOL("Use Prologue Cellphone", g_Use_Prologue_Cellphone)

        ADD_WIDGET_BOOL("Secondary app screen available?", g_b_Secondary_Screen_Available)

        ADD_WIDGET_BOOL("Display Onscreen Standard Conversation info", g_b_DisplayConversationToggle)
        ADD_WIDGET_BOOL("Display Onscreen Standard Procedural Conversation info", g_b_DisplayProceduralConversationToggle)


        ADD_WIDGET_BOOL("Allow Dialogue while in water", g_AllowUnderwaterDialogue)

        ADD_WIDGET_BOOL("Display Script State for Dialogue info", b_DebugConvState)


        ADD_WIDGET_BOOL ("Has script disabled signifier movie?", g_Debug_Signifiers_Hidden_by_Script)


        ADD_WIDGET_BOOL ("Display Onscreen State Debug", g_Cellphone_Onscreen_State_Debug)
        ADD_WIDGET_BOOL ("Display Hangup and Force Answer Timers", g_b_DebugPhoneTimerDisplay)

        //ADD_WIDGET_BOOL("Enable Tone Debug - will overwrite game prefs!", b_EnableToneSelection)

        //ADD_WIDGET_INT_SLIDER ("Current Player Ringtone_Selection", DebugWidgetToneSelection, 0, 5, 1)

        ADD_WIDGET_BOOL("Run testemergencycall.sc", RunTECToggle)

        ADD_WIDGET_BOOL("Display in 16-9 mode", g_Cellphone_Ratio_16_9_WidgetToggle)

        ADD_WIDGET_BOOL("Display in 4-3 mode", g_Cellphone_Ratio_4_3_WidgetToggle)


        ADD_WIDGET_BOOL("Cellphone Cam Fullscreen Mode", g_Cellphone_FullScreen_Mode)

        //ADD_WIDGET_BOOL("Set Cellphone Cam Auto Continue bit", b_Cellphone_Cam_Auto_Continue_On_Toggle)

        //ADD_WIDGET_BOOL("g_bGameSpyOnline", g_bGameSpyOnline)

    
        ADD_WIDGET_BOOL("Display temp button labels", g_DoDebugTempDraw_Phone)

        ADD_WIDGET_BOOL("Display test local conv struct data", b_DisplayTestStructData)

        ADD_WIDGET_BOOL("Purge text message list for Multiplayer", b_PurgeMPTextList)

    


        START_WIDGET_GROUP ("Simple Conversation User Test")

            widgetID_ConvBlock = ADD_TEXT_WIDGET ("Enter your block:")
            widgetID_ConvRoot = ADD_TEXT_WIDGET ("Enter your root:") 
            ADD_WIDGET_BOOL ("Play conversation:", b_PlaySimpleConversation)

        STOP_WIDGET_GROUP ()

/*

        //INFO: 
//PARAM NOTES: Widget id, which can be used in GET_CONTENTS_OF_TEXT_WIDGET and DELETE_WIDGET
//PURPOSE: Creates a widget in which the user can enter text. More info..
NATIVE FUNC TEXT_WIDGET_ID ADD_TEXT_WIDGET(STRING Title)

//INFO: 
//PARAM NOTES: a string holding the contents of the text widget
//PURPOSE: Returns the contents of a text widget that has previously been created with ADD_TEXT_WIDGET. More info..
NATIVE FUNC STRING GET_CONTENTS_OF_TEXT_WIDGET(TEXT_WIDGET_ID id)

//INFO: 
//PARAM NOTES:
//PURPOSE: Sets the contents of a text widget that has previously been created with ADD_TEXT_WIDGET. More info..
NATIVE PROC SET_CONTENTS_OF_TEXT_WIDGET(TEXT_WIDGET_ID id, STRING NewWidgetContents)
 */



        START_WIDGET_GROUP ("Alter phone position and rotation")

            ADD_WIDGET_BOOL("Enable 3d model position adjustment - phone model must be onscreen.", DebugPositionToggle)

            g_3dPhoneDebugRotVec = g_3dPhoneStartRotVec
    
            ADD_WIDGET_FLOAT_SLIDER ("3d_PhonePosX", g_Test3dPhonePosVec.x, -900.0, 900.0, 0.5)
            ADD_WIDGET_FLOAT_SLIDER ("3d_PhonePosY", g_Test3dPhonePosVec.y, -900.0, 900.0, 0.5)
            ADD_WIDGET_FLOAT_SLIDER ("3d_PhonePosZ", g_Test3dPhonePosVec.z, -900.0, 900.0, 0.5)

            ADD_WIDGET_FLOAT_SLIDER ("3d_PhoneRotX", g_3dPhoneDebugRotVec.x, -900.0, 900.0, 0.05)
            ADD_WIDGET_FLOAT_SLIDER ("3d_PhoneRotY", g_3dPhoneDebugRotVec.y, -900.0, 900.0, 0.05)
            ADD_WIDGET_FLOAT_SLIDER ("3d_PhoneRotZ", g_3dPhoneDebugRotVec.z, -900.0, 900.0, 0.05)

        STOP_WIDGET_GROUP ()




    
        START_WIDGET_GROUP ("Cellphone Scaleform Selection")

             ADD_WIDGET_BOOL ("Toggle button labels", g_b_ToggleButtonLabels)
             ADD_WIDGET_BOOL ("Enable Onscreen Gui Change and Info", b_DebugCellphoneGuiInfo) 

             ADD_WIDGET_INT_SLIDER ("Select gui theme", DebugCellphoneGui, 0, 5, 1)
        
        STOP_WIDGET_GROUP ()
        




        START_WIDGET_GROUP ("Scaleform Movie and Button placement")

             ADD_WIDGET_FLOAT_SLIDER ("SF Button Pos X", g_SF_ButtonsPosX, 0.0, 1.0, 0.005)
             ADD_WIDGET_FLOAT_SLIDER ("SF Button Pos Y", g_SF_ButtonsPosY, 0.0, 1.0, 0.005)

             ADD_WIDGET_FLOAT_SLIDER ("SF Button Size X", g_SF_ButtonsSizeX, 0.0, 1.0, 0.005)
             ADD_WIDGET_FLOAT_SLIDER ("SF Button Size Y", g_SF_ButtonsSizeY, 0.0, 1.0, 0.005)


             ADD_WIDGET_FLOAT_SLIDER ("SF MainGUI Pos X", g_SF_PhonePosX, 0.0, 1.0, 0.005)
             ADD_WIDGET_FLOAT_SLIDER ("SF MainGUI Pos Y", g_SF_PhonePosY, 0.0, 1.0, 0.005)

             ADD_WIDGET_FLOAT_SLIDER ("SF MainGUI Size X", g_SF_PhoneSizeX, 0.0, 1.0, 0.005)
             ADD_WIDGET_FLOAT_SLIDER ("SF MainGUI Size Y", g_SF_PhoneSizeY, 0.0, 1.0, 0.005)

        STOP_WIDGET_GROUP ()




        START_WIDGET_GROUP ("Contacts, Dialogue and transition tests")

         
            ADD_WIDGET_BOOL ("Buffered contact available for Michael", BufferedContactAvailable[ENUM_TO_INT(CHAR_MICHAEL)])
            ADD_WIDGET_BOOL ("Buffered contact available for Trevor", BufferedContactAvailable[ENUM_TO_INT(CHAR_TREVOR)])
            ADD_WIDGET_BOOL ("Buffered contact available for Franklin", BufferedContactAvailable[ENUM_TO_INT(CHAR_FRANKLIN)])


            ADD_WIDGET_BOOL ("Add miscellaneous contacts for all SP chars", b_AddTestContacts)
            ADD_WIDGET_BOOL ("Add contact for Michael", b_DebugContactForMichael)
            ADD_WIDGET_BOOL ("Add contact for Franklin", b_DebugContactForFranklin)
            ADD_WIDGET_BOOL ("Add contact for Trevor", b_DebugContactForTrevor)
            ADD_WIDGET_BOOL ("Add contact for all SP player chars", b_DebugContactForEveryone)

            ADD_WIDGET_BOOL ("Add contact for Virtual Multiplayer Char", b_DebugContactForVirtual)



            ADD_WIDGET_BOOL ("Incoming Chat Call test", b_InitiateIncomingChatCall)
            ADD_WIDGET_BOOL ("Outgoing Chat Call test - Phone must be up.", b_InitiateOutgoingChatCall)
            ADD_WIDGET_BOOL ("Set incoming chat as finished", b_Chatfinished)
            ADD_WIDGET_BOOL ("HANG_UP_AND_PUT_AWAY_PHONE()", b_HangUpTest)




            ADD_WIDGET_BOOL ("Play CHAR_CALL test call", b_CharCallTest)
            ADD_WIDGET_BOOL ("Play CHAR_CALL_WITH_REPLIES test call", b_CharCallReplyTest)
            ADD_WIDGET_BOOL ("Play PLAYER_CALL test call", b_PlayerCallTest)
            ADD_WIDGET_BOOL ("Play PLAYER_CALL_WITH_REPLIES test call", b_PlayerCallReplyTest)
            ADD_WIDGET_BOOL ("Play test conversation", b_PlayerConvTest)
            ADD_WIDGET_BOOL ("Play test random line", b_PlayerRandomTest)
            ADD_WIDGET_BOOL ("Play single line of conv", b_PlayerSingleLineTest)
            ADD_WIDGET_BOOL ("Play conv from specified line", b_PlayerFromLineTest)

            ADD_WIDGET_BOOL ("Get resumption label from ongoing conversation", b_GetResumptionLabel)
            ADD_WIDGET_BOOL ("Resume conversation from last resumption label", b_ResumeConversation)

            ADD_WIDGET_BOOL ("Play test Multipart conversation", b_PlayerMultipartTest)
            ADD_WIDGET_BOOL ("Play test Multipart phonecall", b_PlayerMultipartTestCall)
            ADD_WIDGET_BOOL ("Play test Multipart with Replies phonecall", b_PlayerMultipartwithRepliesTestCall)


            ADD_WIDGET_BOOL ("Play MP Job Offer", b_JobOfferCallTest)

            
            ADD_WIDGET_BOOL ("Toggle autoplay test",  g_bFlowAutoplayInProgress) 

            ADD_WIDGET_BOOL ("Set g_bInMultiplayer to TRUE to mimic MP phone.", b_Switch_to_MultiplayerPhone)
            ADD_WIDGET_BOOL ("Set g_bInMultiplayer to FALSE for SP phone.", b_Switch_to_SingleplayerPhone)
            ADD_WIDGET_BOOL ("Display game type - redisplays if altered by new setting.", b_DisplayGameType)


            ADD_WIDGET_BOOL ("Alter Cam Help text to include Send", b_SendCamHelpText)

            ADD_WIDGET_BOOL ("Remove Send option from Cam Help text", b_RemoveSendCamHelpText)


            ADD_WIDGET_BOOL ("Launch Save Game Menu", b_DebugSaveGameTest)



        STOP_WIDGET_GROUP ()


    
    STOP_WIDGET_GROUP ()

  

    //g_Cellphone_FullScreen_Mode = TRUE



ENDPROC

#endif






#if IS_DEBUG_BUILD


PROC Maintain_Cellphone_Debug()




    //Phone DISABLE toggle. The mechanism to set the phone to disabled mode is explicit and refers to phone states rather than using the DISABLE_CELLPHONE command from cellphone_public.sch.
    //This is so I can know exactly what has been used to disable the phone and prevent daft "there's nae calls getting through, I'm no sure what I've done!" pish.

    IF g_Cellphone_Disable_WidgetToggle = TRUE

        IF NOT (g_Cellphone.PhoneDS = PDS_DISABLED) //This should take precedence over DISABLED_THIS_FRAME_ONLY, so we check for permanent state only.

            HANG_UP_AND_PUT_AWAY_PHONE() //tidy up call...

            g_Cellphone.PhoneDS = PDS_DISABLED

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 40. Cellphone_Controller assigns PDS_DISABLED")
                cdPrintnl()   
            #endif



            cdPrintstring ("CELLPHONE_CONT - Phone set to disabled by debug widget. No calls will get through and the phone cannot be brought up.")
            cdPrintnl()
                    
        ENDIF

    ELSE

        IF g_Cellphone.PhoneDS = PDS_DISABLED

            g_Cellphone.PhoneDS = PDS_AWAY

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 41. Cellphone_Controller assigns PDS_AWAY")
                cdPrintnl()   
            #endif





            cdPrintstring ("CELLPHONE_CONT - Phone woken by debug widget. Calls and phone enabled!")
            cdPrintnl()

        ENDIF


    ENDIF







    IF b_TickToRequestDisableEveryFrameOption

        DISABLE_CELLPHONE_THIS_FRAME_ONLY()

        //DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()

       // b_TickToRequestDisableEveryFrameOption = FALSE

    ENDIF







    

    IF b_DebugConvState

        format_medium_ostext (0, 128, 255, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.5, 0.11, "STRING", "Checks for native mobile phone call ongoing only.")


        IF IS_MOBILE_PHONE_CALL_ONGOING()

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.14, "STRING", "Phone call ongoing.")
            
        ELSE

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.14, "STRING", "No M. Phone")

        ENDIF


        IF g_IsThisConversationForPhone

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.1, 0.14, "STRING", "g_ConvForPhone TRUE")

        ENDIF


        format_medium_ostext (0, 128, 255, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.5, 0.31, "STRING", "Checks for native scripted conversation ongoing only.")

        IF IS_SCRIPTED_CONVERSATION_ONGOING()

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "Scripted Conv.")
            
        ELSE

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "No S. Conv")

        ENDIF




        format_medium_ostext (0, 128, 255, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.5, 0.47, "STRING", "Checks for scripted conversation or CONV_STATE_FREE.")

        IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

             DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.50, "STRING", "Ongoing or queued.")
            
        ELSE

             DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.50, "STRING", "No Script Conv and Conv Free")

        ENDIF
        



        #if USE_TU_CHANGES

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.44, "STRING", "Using TU changes") 


        
            IF g_Holder_ForceDLC_DialogueRequest = TRUE

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.64, "STRING", "FDR Holder is TRUE") 

            ENDIF


        #endif



        /*
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)) 
         
            KILL_FACE_TO_FACE_CONVERSATION() 

            #if IS_DEBUG_BUILD

                cdPrintnl ()
                cdPrintstring ("CELLPHONE_CONT - Kill face-to-face conversation test fired from this script!")
                cdPrintnl ()

            #endif

        ENDIF
        */


    ENDIF




    IF g_Cellphone_Onscreen_State_Debug = TRUE


        #if USE_CLF_DLC 

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.2, 0.10, "STRING", "Using A_T build")

        #endif


        IF g_CellphonePromptResponse = RESPONDED_NO

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.2, 0.10, "STRING", "Responded NO")
            
        ENDIF
        
        
        IF g_CellphonePromptResponse = RESPONDED_YES

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.2, 0.10, "STRING", "Responded YES")

        ENDIF
         
         
        IF g_Phone_Active_but_Hidden       
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.2, 0.10, "STRING", "Phone Hidden!")
        ENDIF


        IF voiceSession.bInVoiceSession = TRUE

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.20, "STRING", "Voice Session Ongoing.")

        ENDIF

        IF g_IsThisAnMPChatCall

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.10, "STRING", "g_IsThisAnMPChatCall")

        ENDIF

        SWITCH g_cellphone.PhoneDS



            CASE PDS_DISABLED

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Disabled State.")

            BREAK

            CASE PDS_SLEEPING

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Sleeping State.")

            BREAK

            CASE PDS_AWAY

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Away State.")

            BREAK

            CASE PDS_TAKEOUT

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Takeout State.")

            BREAK


            CASE PDS_MINIMAL

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Minimal State.")

            BREAK


            CASE PDS_MAXIMUM

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Maximum State.")

            BREAK


            CASE PDS_RUNNINGAPP

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "RunningApp State.")

            BREAK


            CASE PDS_COMPLEXAPP

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "ComplexApp State.")

            BREAK


            CASE PDS_ONGOING_CALL

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "OngoingCall State.")

            BREAK


            CASE PDS_ATTEMPTING_TO_CALL_CONTACT

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Attempting Call State.")

            BREAK

            CASE PDS_DISABLED_THIS_FRAME_ONLY

                 DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.30, "STRING", "Disabled_This_Frame_Only")

            BREAK


        ENDSWITCH



    ENDIF





    IF g_Chosen_Ratio = DISPLAY_16_9
        
        IF g_Cellphone_Ratio_4_3_WidgetToggle = TRUE

            g_Chosen_Ratio = DISPLAY_4_3

            g_Cellphone_Ratio_16_9_WidgetToggle = FALSE

        ENDIF

    ELSE

        IF g_Cellphone_Ratio_16_9_WidgetToggle = TRUE

            g_Chosen_Ratio = DISPLAY_16_9

            g_Cellphone_Ratio_4_3_WidgetToggle = FALSE

        ENDIF

    ENDIF 


    /*
    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)


        IF b_Cellphone_Cam_Auto_Continue_On_Toggle = TRUE

            SET_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)

            #if IS_DEBUG_BUILD

                cdPrintnl ()
                cdPrintstring ("CELLPHONE_CONT - Bit set for g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE")
                cdPrintnl ()
                
            #endif



        ENDIF

    ELSE

        IF b_Cellphone_Cam_Auto_Continue_On_Toggle = FALSE

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)

            #if IS_DEBUG_BUILD

                cdPrintnl ()
                cdPrintstring ("CELLPHONE_CONT - Clearing bit for g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE")
                cdPrintnl ()
                
            #endif



        ENDIF

    ENDIF
    */






    IF RunTECToggle = TRUE

        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("testemergencycall")) = 0

            request_load_scriptstring ("testemergencycall")
            
            START_NEW_SCRIPT ("testemergencycall", DEFAULT_STACK_SIZE)

            SET_SCRIPT_AS_NO_LONGER_NEEDED ("testemergencycall")

        ELSE

            cdPrintstring ("Testemergencycall is already running...")
            cdPrintnl ()

        ENDIF

        RunTECToggle = FALSE

    ENDIF



    IF b_InitiateIncomingChatCall


        b_InitiateIncomingChatCall = FALSE

        INITIATE_INCOMING_CHAT_CALL ("CustomNameZ") //from dialogue public.


    ENDIF


    IF b_InitiateOutgoingChatCall

        b_InitiateOutgoingChatCall = FALSE

        IF IS_PHONE_ONSCREEN()
            LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL ("CustomNameY")  //from cellphone public.
        ENDIF

    ENDIF



    IF b_ChatFinished 

        b_ChatFinished = FALSE

       SET_INCOMING_CHAT_CALL_SETUP_AS_FINISHED()

    ENDIF



    IF b_HangUpTest

        b_HangUpTest = FALSE

        HANG_UP_AND_PUT_AWAY_PHONE()

    ENDIF




    IF b_CharCallTest      
       
        //CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER (MyLocalPedStruct, CHAR_FRANKLIN, "JHS1AUD", "JHS1_STP4", CONV_PRIORITY_VERY_HIGH)

        //ESP4_RCM_1:EPS4AUD]

        
        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 3, NULL, "STEVE")
        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "JIMMYBOS")

        /*
        IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER (MyLocalPedStruct, CHAR_MICHAEL, "EPS4AUD", "EPS4_MRET2", CONV_PRIORITY_VERY_HIGH)

            //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
            //DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (TRUE)

        ENDIF
        */

        
        //Testing bug 1086265
        //IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(MyLocalPedStruct, CHAR_STEVE, "FBIPRAU", "FBI4_READY", CONV_PRIORITY_VERY_HIGH)
        IF CHAR_CALL_PLAYER_CELLPHONE (MyLocalPedStruct, CHAR_RICKIE, "FM_1AU", "FM_PLANETAKE", CONV_PRIORITY_VERY_HIGH)
            //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
            //DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (TRUE)

             b_CharCallTest = FALSE

            //IF NOT IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED(DU_HEIST_RICKIE)
                //SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_RICKIE)
                //cdPrintstring("Unlocked DM Rickie")
                //cdPrintnl()
            //ENDIF


        ENDIF


       
             /*
        IF CHAR_CALL_PLAYER_CELLPHONE (MyLocalPedStruct, CHAR_ALL_PLAYERS_CONF, "FB3AUD", "F3_CALL1", CONV_PRIORITY_VERY_HIGH)
            
            DISABLE_HANGUP_FOR_THIS_CALL(TRUE)

        ENDIF
        */

        //b_CharCallTest = FALSE

    ENDIF




    IF b_PurgeMPTextList


        //CLEAR_TEXT_MESSAGE_SIGNIFIER_FOR_MULTIPLAYER()


        PURGE_TEXT_MESSAGES_FOR_MULTIPLAYER()

        
    
        b_PurgeMPTextList = FALSE


    ENDIF



    IF b_CharCallReplyTest      


        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 4, NULL, "MARNIE")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "JIMMYBOS")



        IF CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES(MyLocalPedStruct, CHAR_FRANKLIN, "EPS4AUD", "EPS4_MRET2", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE, "TXT_103", "EPS4_PCLOSE", "EPS4_MCLOSE")
        
            KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU(TRUE)

            //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)

        ENDIF
        
        b_CharCallReplyTest = FALSE

    ENDIF



    
    IF b_JobOfferCallTest

        //CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_FORCE_ANSWER (MyLocalPedStruct, CHAR_FRANKLIN, "JHS1AUD", "JHS1_STP5", CONV_PRIORITY_VERY_HIGH, "TXT_103")
    

        //CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_FORCE_ANSWER (MyLocalPedStruct, CHAR_FRANKLIN, "FB3AUD", 
        //                                                                                  "F3_CALL4", CONV_PRIORITY_VERY_HIGH, "MP_PDA_1")    //Chumash

        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 4, NULL, "MARNIE")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "JIMMYBOS")


        CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_FORCE_ANSWER (MyLocalPedStruct, CHAR_FRANKLIN, "EPS4AUD", 
                                                                        "EPS4_MRET2", CONV_PRIORITY_VERY_HIGH, "MP_PDA_1")



        b_JobOfferCallTest = FALSE

    ENDIF





    IF b_PlayerCallTest
  
        /*
        IF PLAYER_CALL_CHAR_CELLPHONE (MyLocalPedStruct, CHAR_FRANKLIN, "FB3AUD", "F3_WRAP", CONV_PRIORITY_LOW)

            //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)

        ENDIF
        */

        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "FRANKLIN")


        //PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "JHAUD", "JH_PLAN", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)

        //PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_ABIGAIL, "DIVEAU", "DIVE_PHONE",  CONV_PRIORITY_VERY_HIGH)

        //PLAYER_INITIATE_CHAT_CALL (MyLocalPedStruct, CHAR_FRANKLIN, "SLM1AUD", "S1_RDY",  CONV_PRIORITY_NON_CRITICAL_CALL)

        //IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_PEGASUS_DELIVERY, "MethAud", "CH1CAL", CONV_PRIORITY_high)

        
        IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TREVOR, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)

            //BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL(TRUE)

            //SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START)

            b_PlayerCallTest = FALSE

        ENDIF

        

    ENDIF



    IF b_PlayerCallReplyTest      


        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "FRANKLIN")



        PLAYER_CALL_CHAR_CELLPHONE_WITH_REPLIES (MyLocalPedStruct, CHAR_FRANKLIN, "JHAUD", "JH_PLAN", CONV_PRIORITY_VERY_HIGH, "TXT_103", "JH_CARLVE", "JH_GONOW")
        
        b_PlayerCallReplyTest = FALSE

    ENDIF

    




    IF b_PlayerConvTest

   
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
        
        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "LAMAR")
        //CREATE_CONVERSATION (MyLocalPedStruct, "FM_1AU", "FM_LAM1", CONV_PRIORITY_MEDIUM)

        //CREATE_CONVERSATION (MyLocalPedStruct, "JHAUD", "JH_PLAN", CONV_PRIORITY_MEDIUM) //This is a lower priority than non essential calls for testing.

        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM6AUD", "FAM6_TATT2", CONV_PRIORITY_MEDIUM)

                                                             
        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM6AUD", "FAM6_CHAT1", CONV_PRIORITY_MEDIUM)


         //DISABLE_CELLPHONE (TRUE)

         //WAIT (0)


        g_B_ContentCreationPeds_Should_be_added_for_Dialogue = TRUE

        g_ContentCreationConversationPedsStruct.PedInfo[0].AddPedNativeNumber = 16
        g_ContentCreationConversationPedsStruct.PedInfo[0].VoiceID = "FRANKLIN"
        g_ContentCreationConversationPedsStruct.PedInfo[0].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[0].ActiveInConversation = TRUE


        g_ContentCreationConversationPedsStruct.PedInfo[1].AddPedNativeNumber = 17
        g_ContentCreationConversationPedsStruct.PedInfo[1].VoiceID = "MICHAEL"
        g_ContentCreationConversationPedsStruct.PedInfo[1].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[1].ActiveInConversation = TRUE

        
        g_ContentCreationConversationPedsStruct.PedInfo[2].AddPedNativeNumber = 18
        g_ContentCreationConversationPedsStruct.PedInfo[2].VoiceID = "TREVOR"
        g_ContentCreationConversationPedsStruct.PedInfo[2].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[2].ActiveInConversation = FALSE


        g_ContentCreationConversationPedsStruct.PedInfo[3].AddPedNativeNumber = 19
        g_ContentCreationConversationPedsStruct.PedInfo[3].VoiceID = "STEVE"
        g_ContentCreationConversationPedsStruct.PedInfo[3].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[3].ActiveInConversation = TRUE

        
        g_ContentCreationConversationPedsStruct.PedInfo[4].AddPedNativeNumber = 20
        g_ContentCreationConversationPedsStruct.PedInfo[4].VoiceID = "STEVE"
        g_ContentCreationConversationPedsStruct.PedInfo[4].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[4].ActiveInConversation = TRUE




        g_ContentCreationConversationPedsStruct.PedInfo[18].AddPedNativeNumber = 34
        g_ContentCreationConversationPedsStruct.PedInfo[18].VoiceID = "FRANKLIN"
        g_ContentCreationConversationPedsStruct.PedInfo[18].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[18].ActiveInConversation = TRUE


        
        g_ContentCreationConversationPedsStruct.PedInfo[19].AddPedNativeNumber = 35
        g_ContentCreationConversationPedsStruct.PedInfo[19].VoiceID = "AMANDA"
        g_ContentCreationConversationPedsStruct.PedInfo[19].Index = NULL
        g_ContentCreationConversationPedsStruct.PedInfo[19].ActiveInConversation = TRUE




        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "LAMAR")
        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM3AUD", "FAM3_MST1", CONV_PRIORITY_MEDIUM)

        //This truck yours..
        //IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE ( MyLocalPedStruct, "FAM3AUD", "FAM3_MST1", "FAM3_MST1_4", CONV_PRIORITY_MEDIUM )
        
        //Feb 14
        //IF CREATE_CONVERSATION ( MyLocalPedStruct, "ARM1AUD", "ARM1_TOW2", CONV_PRIORITY_MEDIUM)  

		IF CREATE_CONVERSATION (MyLocalPedStruct, "PBPAU", "PBP_GOGET", CONV_PRIORITY_MEDIUM)


        //Rowan test.
        //IF CREATE_CONVERSATION ( MyLocalPedStruct, "MPOBHAU", "MPOBH1", CONV_PRIORITY_MEDIUM)

        //IF CREATE_CONVERSATION( MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1", CONV_PRIORITY_MEDIUM)


            b_PlayerConvTest = FALSE  

            //g_B_ContentCreationPeds_Should_be_added_for_Dialogue = FALSE

        ENDIF

        //Long conversation subtitles for testing landscape phone mode fix.
        //CREATE_CONVERSATION ( MyLocalPedStruct, "D1AUD", "DS1_1", CONV_PRIORITY_MEDIUM )


         //CREATE_CONVERSATION (MyLocalPedStruct, "FAM2AUD", "FAM2_BGUY2", CONV_PRIORITY_MEDIUM)

        // CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "EPS6AUD", "EPS6_SPOT", "EPS6_SPOT_3", CONV_PRIORITY_HIGH)


         //CREATE_CONVERSATION(MyLocalPedStruct, "EPS6AUD", "EPS6_SPOT", CONV_PRIORITY_MEDIUM)

         //WAIT (0)

         //DISABLE_CELLPHONE (TRUE)



        //[txm7_ret1F_1:Txm7aud]
        //CREATE_CONVERSATION (MyLocalPedStruct, "TXM7AUD", "TXM7_ret1F", CONV_PRIORITY_MEDIUM)

        //CREATE_CONVERSATION (MyLocalPedStruct, "FB3AUD", "F3_DEAD", CONV_PRIORITY_LOW)

        
        //CREATE_CONVERSATION (MyLocalPedStruct, "FMTAUD", "TRV_IG_5a", CONV_PRIORITY_AMBIENT_MEDIUM)


        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM1AUD", "FAM1_CLOSER", CONV_PRIORITY_LOW, DISPLAY_SUBTITLES, do_not_add_to_brief_screen)

        //CREATE_CONVERSATION(MyLocalPedStruct, "REHH1AU", "REHH1_ASK", CONV_PRIORITY_AMBIENT_HIGH)



        //CREATE_CONVERSATION(MyLocalPedStruct, "FAM1AUD", "FAM1_HANGON", CONV_PRIORITY_AMBIENT_HIGH ) 

        //CREATE_CONVERSATION(MyLocalPedStruct, "NIGE1AU", "NIGE1_INA", CONV_PRIORITY_AMBIENT_HIGH)


        //CREATE_CONVERSATION (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", CONV_PRIORITY_VERY_LOW) //very long

        //CREATE_CONVERSATION (MyLocalPedStruct, "RBS1AUD", "RBS1_BOXHIT", CONV_PRIORITY_VERY_LOW) //very long

                                                                                    
        //CREATE_CONVERSATION(MyLocalPedStruct, "REHH1AU", "REHH1_SLW", CONV_PRIORITY_AMBIENT_HIGH)

                
        //CREATE_CONVERSATION (MyLocalPedStruct, "RBXd_7AU", "RB_7_GP_G1", CONV_PRIORITY_VERY_LOW)
        
        
        //CREATE_CONVERSATION (MyLocalPedStruct, "ARM1AUD", "ARM1_CHOICE", CONV_PRIORITY_MEDIUM)


        //August 2012 test
        //CREATE_CONVERSATION (MyLocalPedStruct, "MRS3AUD", "MRS3_OUTRO", CONV_PRIORITY_MEDIUM)



        //CREATE_CONVERSATION (MyLocalPedStruct, "MEJ1AUD", "MEJ1_VAN", CONV_PRIORITY_MEDIUM)      
        //CREATE_CONVERSATION (MyLocalPedStruct, "MPSCAAU", "DIRTY_WARN", CONV_PRIORITY_MEDIUM)





        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "TaxiAlonzo")

        //FR_LF_EC1_1

        //CREATE_CONVERSATION (MyLocalPedStruct, "FRLFbAU", "FR_LF_EC1", CONV_PRIORITY_MEDIUM) 

        //WRK2_1:CHI2AUD  Bolt bug.

        //CREATE_CONVERSATION (MyLocalPedStruct, "CHI2AUD", "WRK2", CONV_PRIORITY_MEDIUM) 
        
        //CREATE_CONVERSATION (MyLocalPedStruct, "FBI2AUD", "FBI2_INTER1", CONV_PRIORITY_LOW)
                                                 
                                                 
                                                 
        //CREATE_CONVERSATION (MyLocalPedStruct, "LEM1AUD", "LEM1_ROOF3", CONV_PRIORITY_MEDIUM)
        //CREATE_CONVERSATION (MyLocalPedStruct, "txm9AUD", "LEM1_ROOF3", CONV_PRIORITY_MEDIUM)

    ENDIF


    


    IF b_PlayerSingleLineTest 


        //SC_FOLLOWD_1A:SCAUD]

        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "JULIET")
        //PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "SCAUD", "SC_FOLLOWD", "SC_FOLLOWD_2", CONV_PRIORITY_MEDIUM)

        //PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", "RBS1_DRV2_6", CONV_PRIORITY_VERY_LOW) //very long
        //PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", "RBS1_DRV2_6", CONV_PRIORITY_VERY_LOW) //very long


        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
        
        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "LAMAR")
        //CREATE_CONVERSATION (MyLocalPedStruct, "FM_1AU", "FM_LAM1", CONV_PRIORITY_MEDIUM)

        //CREATE_CONVERSATION (MyLocalPedStruct, "JHAUD", "JH_PLAN", CONV_PRIORITY_MEDIUM) //This is a lower priority than non essential calls for testing.

        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM6AUD", "FAM6_TATT2", CONV_PRIORITY_MEDIUM)

                                                             
        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM6AUD", "FAM6_CHAT1", CONV_PRIORITY_MEDIUM)


         //DISABLE_CELLPHONE (TRUE)

         //WAIT (0)




        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "LAMAR")
        //CREATE_CONVERSATION (MyLocalPedStruct, "FAM3AUD", "FAM3_MST1", CONV_PRIORITY_MEDIUM)

        //This truck yours..
        //IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE ( MyLocalPedStruct, "FAM3AUD", "FAM3_MST1", "FAM3_MST1_4", CONV_PRIORITY_MEDIUM )
        
        //Feb 14
        //IF PLAY_SINGLE_LINE_FROM_CONVERSATION_USING_V_CONTENT_IN_DLC (MyLocalPedStruct, "ARM1AUD", "ARM1_TOW2", "ARM1_TOW2_3", CONV_PRIORITY_MEDIUM)  

        IF PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "ARM1AUD", "ARM1_TOW2", "ARM1_TOW2_3", CONV_PRIORITY_MEDIUM)  
        



        //Rowan test.
        //IF CREATE_CONVERSATION ( MyLocalPedStruct, "MPOBHAU", "MPOBH1", CONV_PRIORITY_MEDIUM)

        //IF CREATE_CONVERSATION( MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1", CONV_PRIORITY_MEDIUM)


            b_PlayerSingleLineTest = FALSE  

        ENDIF



        /*
        IF PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", "RBS1_DRV2_6", CONV_PRIORITY_VERY_LOW, DO_NOT_DISPLAY_SUBTITLES)

            b_PlayerSingleLineTest = FALSE

        ENDIF
        */

        //STOP_SCRIPTED_CONVERSATION(TRUE)



    ENDIF




    IF b_PlayerFromLineTest 


        //CREATE_CONVERSATION_FROM_SPECIFIC_LINE (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", "RBS1_DRV2_6", CONV_PRIORITY_MEDIUM) //very long
    
        IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE ( MyLocalPedStruct, "FM_1AU", "FM_RCM", "FM_RCM_3", CONV_PRIORITY_MEDIUM) 

            b_PlayerFromLineTest = FALSE

        ENDIF


    ENDIF




    IF b_PlaySimpleConversation //This runs from the widget in rag.


         b_PlaySimpleConversation = FALSE



        CREATE_CONVERSATION (MyLocalPedStruct, GET_CONTENTS_OF_TEXT_WIDGET(widgetID_ConvBlock), GET_CONTENTS_OF_TEXT_WIDGET(widgetID_ConvRoot), CONV_PRIORITY_MEDIUM)


    ENDIF








    IF b_GetResumptionLabel

        //TestResumptionLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()   


        TestResumptionLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()

        b_GetResumptionLabel = FALSE

    ENDIF



    IF b_ResumeConversation

        KILL_FACE_TO_FACE_CONVERSATION()
        WAIT (0)
           
        
                
        CREATE_CONVERSATION_FROM_SPECIFIC_LINE (MyLocalPedStruct, "MRS3AUD", "MRS3_OUTRO", TestResumptionLabel, CONV_PRIORITY_MEDIUM) 

        b_ResumeConversation = FALSE

    ENDIF




    IF b_PlayerRandomTest

   

        //CREATE_CONVERSATION (MyLocalPedStruct, "JHAUD", "JH_FRENCT", CONV_PRIORITY_AMBIENT_HIGH)

        //CREATE_CONVERSATION (MyLocalPedStruct, "JHAUD", "JH_CRENC2", CONV_PRIORITY_AMBIENT_HIGH)

        CREATE_CONVERSATION(MyLocalPedStruct, "RECREAU", "RECRE_PAIN", CONV_PRIORITY_AMBIENT_HIGH )    //random

     
        b_PlayerRandomTest = FALSE

    ENDIF



    IF b_SendCamHelpText

        ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP (TRUE)

        b_SendCamHelpText = FALSE   

    ENDIF




    IF b_RemoveSendCamHelpText

        ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP (FALSE)

        b_RemoveSendCamHelpText = FALSE

    ENDIF








    IF b_PlayerMultipartTest

        //"Hey! Genius..."   "No. Please..."   "You moron"  "He looks like..." 
        
        CREATE_MULTIPART_CONVERSATION_WITH_4_LINES_USING_V_CONTENT_IN_DLC (MyLocalPedStruct, "FB3AUD",

                                            "F3_CALL1B", "F3_CALL1B_1",
       
                                            "F3_WREN1", "F3_WREN1_2", 

                                            "F3_ARM4", "F3_ARM4_1",
                                     
                                            "f3_TENTER", "F3_TENTER_5", CONV_PRIORITY_MEDIUM)  

                                                                              
//        CREATE_MULTIPART_CONVERSATION_WITH_4_LINES (MyLocalPedStruct, "RECREAU",
//
//                                            "RECRE_APPR", "RECRE_APPR_2",
//       
//                                            "RECRE_PAIN", "RECRE_PAIN", 
//
//                                            "RECRE_VDIE", "RECRE_VDIE_1",
//
//                                            "RECRE_PLEAD3", "RECRE_PLEAD3",
//                                     
//                                            CONV_PRIORITY_MEDIUM) 





        b_PlayerMultipartTest = FALSE


     ENDIF 
      



    IF b_PlayerMultipartWithRepliesTestCall


            CHAR_CALL_PLAYER_CELLPHONE_MULTIPART_WITH_2_LINES_WITH_2_LINE_REPLIES (MyLocalPedStruct, CHAR_FRANKLIN, "RECREAU",  
                                            
                                            "RECRE_APPR", "RECRE_APPR_2",
       
                                            "RECRE_PAIN", "RECRE_PAIN", 

                                            "TXT_103",

                    
                                            "RECRE_PLEAD2", "RECRE_PLEAD2_2", "RECRE_PLEAD", "RECRE_PLEAD_2", 
                                            "RECRE_APPR", "RECRE_APPR_2", "RECRE_PLEAD", "RECRE_PLEAD_3",


                                             CONV_PRIORITY_VERY_HIGH) 



            b_PlayerMultipartWithRepliesTestCall = FALSE


    ENDIF



    IF b_PlayerMultipartTestCall
        
                /*
                PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_4_LINES (MyLocalPedStruct, CHAR_FRANKLIN, "FB3AUD",  
                                                    
                                                    "F3_CALL1", "F3_CALL1_2", 

                                                    "F3_WRAP", "F3_WRAP_4",

                                                    "F3_DEAD", "F3_DEAD_3",

                                                    "f3_TENTER", "F3_TENTER_5", CONV_PRIORITY_VERY_HIGH) 
                */



                /*
                PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_4_LINES(MyLocalPedStruct,
                                          CHAR_LAMAR,
                                          "FRLFaAU",
                                          "FRFLC_G_HG_a", "FRFLC_G_HG_a",
                                          "FRFLC_G_HG_b", "FRFLC_G_HG_b",
                                          "FRFLC_N_RC_a_1", "FRFLC_N_RC_a_1",
                                          "FRFLC_N_RC_b_2", "FRFLC_N_RC_b_2",
                                          CONV_PRIORITY_CELLPHONE)
                                          */

                
           


                      PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES(MyLocalPedStruct, CHAR_LAMAR, "FpFLAU",
                                            "FpFL_G1", "FpFL_G1",
                                            "FpFL_G2", "FpFL_G2",       
                                            CONV_PRIORITY_NON_CRITICAL_CALL) 




                             /*
                             PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_4_LINES(MyLocalPedStruct, CHAR_LAMAR, "FpFLAU",

                                            "FpFL_G1", "FpFL_G1",
       
                                            "FpFL_G2", "FpFL_G2", 

                                            "FpFL_Y1_fs", "FpFL_Y1_fs_1",

                                            "FpFL_Y2_ok", "FpFL_Y2_ok",
                                     
                                            CONV_PRIORITY_NON_CRITICAL_CALL) 

                             */







                /*
                
                PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_4_LINES (MyLocalPedStruct, CHAR_FRANKLIN, "RECREAU",  
                                            
                                            "RECRE_APPR", "RECRE_APPR_2",
       
                                            "RECRE_PAIN", "RECRE_PAIN", 

                                            "RECRE_VDIE", "RECRE_VDIE_1",

                                            "RECRE_APPR", "RECRE_APPR_1",
                                     
                                            CONV_PRIORITY_VERY_HIGH)   

                */


                /*
                PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES (MyLocalPedStruct, CHAR_FRANKLIN, "FB3AUD",  
                                    
                                    "F3_CALL1", "F3_CALL1_2", 

                                    "f3_TENTER", "F3_TENTER_5", CONV_PRIORITY_VERY_HIGH)  */




        b_PlayerMultipartTestCall = FALSE

    ENDIF





    IF b_DebugContactForMichael

        //ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER, MICHAEL_BOOK)

            //ADD_CONTACT_TO_PHONEBOOK (CHAR_AMMUNATION, MICHAEL_BOOK)
            //ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_CUSTOMS, MICHAEL_BOOK)
            //ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_STRIPCLUB_PR, MICHAEL_BOOK)
            //ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_TOURIST_BOARD, MICHAEL_BOOK)
            //ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_TOURIST_BOARD, MICHAEL_BOOK)
            //ADD_CONTACT_TO_PHONEBOOK (CHAR_PEGASUS_DELIVERY, MICHAEL_BOOK)
            ADD_CONTACT_TO_PHONEBOOK (CHAR_DAVE, MICHAEL_BOOK)


        //WAIT (2000)

        //ADD_CONTACT_TO_PHONEBOOK (CHAR_LAMAR, MICHAEL_BOOK)

        //Register_Script_To_Relaunch_List(LAUNCH_BIT_CELLPHONE_FLASHHAND)

        //ADD_CONTACT_TO_PHONEBOOK (CHAR_HAO, MICHAEL_BOOK)
   
        b_DebugContactForMichael = FALSE

    ENDIF



    IF b_DebugContactForFranklin

        //ADD_CONTACT_TO_PHONEBOOK (CHAR_MICHAEL, FRANKLIN_BOOK)

        //REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_JOSH)
        //Remove_Script_From_Relaunch_List(LAUNCH_BIT_CELLPHONE_FLASHHAND)


        ADD_CONTACT_TO_PHONEBOOK (CHAR_AMANDA, FRANKLIN_BOOK)

        b_DebugContactForFranklin = FALSE

    ENDIF


    IF b_DebugContactForTrevor

        ADD_CONTACT_TO_PHONEBOOK (CHAR_TAXI, TREVOR_BOOK)

        b_DebugContactForTrevor = FALSE

    ENDIF


    IF b_DebugContactForEveryone

        ADD_CONTACT_TO_PHONEBOOK (CHAR_HAO, ALL_OWNERS_BOOKS)

        b_DebugContactForEveryone = FALSE                             

    ENDIF




    IF b_AddTestContacts

        ADD_CONTACT_TO_PHONEBOOK (CHAR_MOLLY, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_DIRECTOR, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_DEVIN, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_STRETCH, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_RICKIE, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_SOLOMON, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_TOURIST_BOARD, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_CUSTOMS, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_HITCHER_GIRL, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_RON, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_MARTIN, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_MERRYWEATHER, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_GERALD, ALL_OWNERS_BOOKS) 
        ADD_CONTACT_TO_PHONEBOOK (CHAR_JOE, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_MAUDE, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_JIMMY_BOSTON, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_LIFEINVADER, ALL_OWNERS_BOOKS)
        
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_DETONATEPHONE, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_MERRYWEATHER, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_BARRY, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_JOSH, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_PATRICIA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_PEGASUS_DELIVERY, ALL_OWNERS_BOOKS)


        ADD_CONTACT_TO_PHONEBOOK (CHAR_DR_FRIEDLANDER, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_AMANDA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_ABIGAIL, ALL_OWNERS_BOOKS)

        //ADD_CONTACT_TO_PHONEBOOK (CHAR_CEOASSIST, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_DENISE, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_BLIMP, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_DETONATEBOMB, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_DETONATEPHONE, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_GAYMILITARY, ALL_OWNERS_BOOKS)
         //ADD_CONTACT_TO_PHONEBOOK (CHAR_DETONATEBOMB, ALL_OWNERS_BOOKS)
    
        ADD_CONTACT_TO_PHONEBOOK (CHAR_FRANKLIN, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_JIMMY, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LS_CUSTOMS, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_MECHANIC, ALL_OWNERS_BOOKS)
        //ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER_DEATHWISH, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TOW_TONYA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TRACEY, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TAXI, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_DIAL_A_SUB, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_MICHAEL, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TREVOR, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_CHOP, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CRIS, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_HUNTER, ALL_OWNERS_BOOKS)


        ADD_CONTACT_TO_PHONEBOOK (CHAR_OMEGA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TANISHA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_STEVE, ALL_OWNERS_BOOKS)

        
        
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CHENG, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CHENGSR, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TOW_TONYA, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MARTIN, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CHEF, ALL_OWNERS_BOOKS)

        ADD_CONTACT_TO_PHONEBOOK (CHAR_LEST_MIKE_CONF, ALL_OWNERS_BOOKS)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LEST_FRANK_CONF, ALL_OWNERS_BOOKS)

        b_AddTestContacts = FALSE

      

    ENDIF



    IF b_DebugContactForVirtual

        //Legacy
        //ADD_CONTACT_TO_MULTIPLAYER_PHONEBOOK (CHAR_STEVE)
         
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_MECHANIC, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MARTIN, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_CHENG, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MICHAEL, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_TREVOR, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_AMANDA, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_P_ASSIST, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_MERRYWEATHER, MULTIPLAYER_BOOK)
        ADD_CONTACT_TO_PHONEBOOK (CHAR_YACHT_CAPTAIN, MULTIPLAYER_BOOK)

        b_DebugContactForVirtual = FALSE

    ENDIF

    IF b_DebugSaveGameTest

        DISABLE_CELLPHONE_THIS_FRAME_ONLY()
        HANG_UP_AND_PUT_AWAY_PHONE()
        SET_SAVE_MENU_ACTIVE()

        b_DebugSaveGameTest = FALSE

    ENDIF


    

    /*
    IF b_DebugMimicMP = FALSE
        
        g_bInMultiplayer = FALSE

    ELSE

        g_bInMultiplayer = TRUE

    ENDIF
    */



    IF b_Switch_to_MultiplayerPhone = TRUE

        g_bInMultiplayer = TRUE
        
        b_Switch_to_MultiplayerPhone = FALSE
        b_DisplayGameType = TRUE
        
    ENDIF
                

    IF b_Switch_to_SingleplayerPhone = TRUE

        g_bInMultiplayer = FALSE
        
        b_Switch_to_SingleplayerPhone = FALSE
        b_DisplayGameType = TRUE
      
    ENDIF


    IF b_DisplayGameType = TRUE
         
        IF g_bInMultiplayer                            
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.61, 0.50, "STRING", "Multiplayer phone mode") 
        ELSE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.61, 0.50, "STRING", "Singleplayer phone mode") 
        ENDIF

        //Bits = FE_WARNING_OK
        //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_FW_1", iButtonBits, "CELL_CAM_FW_2", FALSE, -1, "", "", TRUE)

    ENDIF





 



    /*
    IF b_EnableToneSelection
        This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer = DebugWidgetToneSelection
    ENDIF
    */

    
    IF b_DebugCellphoneGuiInfo

        format_medium_ostext (255, 255, 255, 255) //White

        DISPLAY_TEXT_WITH_LITERAL_STRING ( 0.60, 0.7, "STRING", "Loaded GUI Movie: ")



        format_medium_ostext (255, 255, 0, 255) //yellow

        
        DISPLAY_TEXT_WITH_LITERAL_STRING ( 0.7, 0.7, "STRING", ScaleformDebugMovie[DebugCellphoneGui])    

    
    ENDIF







ENDPROC

#endif























#if IS_DEBUG_BUILD

PROC draw_debug_Scaleform_Signifier_Position()


    //DRAW_SCALEFORM_MOVIE (SF_MoviePreviewIndex, db_preview_signifier_posX, db_preview_signifier_posY, 0.16, 0.14, 255, 255, 255, 255)



    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.1, "CELL_500", (1 - GET_SAFE_ZONE_SIZE()), 3)
    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.15, "CELL_501", GET_SAFE_ZONE_SIZE(), 3)

  
    DISPLAY_TEXT_WITH_FLOAT (0.8, 0.1, "CELL_500", db_preview_signifier_posX, 3)
    DISPLAY_TEXT_WITH_FLOAT (0.8, 0.15, "CELL_501", db_preview_signifier_posY, 3)

    
    IF IS_BUTTON_PRESSED (PAD1, DPADUP)  

        //db_signifier_posY = db_signifier_posY - 1
        db_preview_signifier_posY = db_preview_signifier_posY - 0.005

    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADDOWN)  

        //db_signifier_posY = db_signifier_posY + 1
        db_preview_signifier_posY = db_preview_signifier_posY + 0.005

    ENDIF
 
    IF IS_BUTTON_PRESSED (PAD1, DPADLEFT)  

        //db_signifier_posX = db_signifier_posX - 1
        db_preview_signifier_posX = db_preview_signifier_posX - 0.005


    ENDIF

    IF IS_BUTTON_PRESSED (PAD1, DPADRIGHT)  

        //db_signifier_posX = db_signifier_posX + 1
        db_preview_signifier_posX = db_preview_signifier_posX + 0.005


    ENDIF








ENDPROC
#endif




//DMPP Temp Prop Placement procedures.
#if IS_DEBUG_BUILD


    /*
    INPUT_CREATOR_LS,           // Creator Left stick equivalent for functions like field of view, etc.
    INPUT_CREATOR_RS,           // Creator right stick equivalent for functions like field of view, etc.
    INPUT_CREATOR_LT,           // Creator left trigger equivalent for zooming.
    INPUT_CREATOR_RT,           // Creator right trigger equivalent for zooming.
    INPUT_CREATOR_MENU_TOGGLE,  // Creator menu toggle on/off for PC keyboard and mouse.
    INPUT_CREATOR_ACCEPT,       // Creator accept/place items button.
    INPUT_CREATOR_DELETE,       // Creator delete items button.

    INPUT_FRONTEND_RB //Rotate Left
    INPUT_FRONTEND_LB //Rotate Right
    */





    PROC Set_Up_Buttons_For_Placement()


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "SET_CLEAR_SPACE", 200)

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "SET_DATA_SLOT_EMPTY", 3)

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 4,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB),
        "DMPP_ROTL") //Rotate Prop Left


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 3,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB),
        "DMPP_ROTR") //Rotate Prop Right

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 2,
        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR),
        "DMPP_ADJUST") //Adjust Selection



        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 1,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),
        "DMPP_EXIT") //Exit


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 0,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CREATOR_ACCEPT), 
        "DMPP_PLACEP") //Place Prop



        BEGIN_SCALEFORM_MOVIE_METHOD(SF_PropPlacementMovieIndex, "SET_MAX_WIDTH")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.0)
        END_SCALEFORM_MOVIE_METHOD()



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.



    ENDPROC




    PROC Set_Up_Buttons_For_Removal()


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "SET_CLEAR_SPACE", 200)

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "SET_DATA_SLOT_EMPTY", 3)


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 1,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),
        "DMPP_EXIT") //Exit


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_PropPlacementMovieIndex, "SET_DATA_SLOT", 0,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CREATOR_DELETE), 
        "DMPP_REMOVEP") //Remove Prop



        BEGIN_SCALEFORM_MOVIE_METHOD(SF_PropPlacementMovieIndex, "SET_MAX_WIDTH")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.0)
        END_SCALEFORM_MOVIE_METHOD()



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_PropPlacementMovieIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.


    ENDPROC





    PROC Fill_Selectable_Prop_Sheet(INT PassedArrayIndex, STRING PassedModelNameToGetHashFrom, STRING PassedTextLabel, ePROP_CATEGORY PassedCategory)

        SelectablePropArray[PassedArrayIndex].PropModelHash = GET_HASH_KEY(PassedModelNameToGetHashFrom)
        SelectablePropArray[PassedArrayIndex].PropTextLabel = PassedTextLabel
        SelectablePropArray[PassedArrayIndex].PropCategory = PassedCategory
            
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("Filling Prop Sheet Entry ")
            cdPrintint(PassedArrayIndex)
            cdPrintnl()   
        #endif
 
        thisframeVicIndex = 0

    ENDPROC







    //Set up placement disk and first prop model.
    PROC Do_Prop_Stage_InitialSetUp()

        //Set up the offsets for this particular prop. Eventually this should come the from the prop sheet. Offsets will be individual to that prop.
        v_PlacementDiscOffset = <<0.0, 3.0, -0.5>>
        v_ThisPropOffset = <<0.0, 3.0, 0.5>>

      
        REQUEST_MODEL(PROP_MP_PLACEMENT_SM)
        WHILE NOT HAS_MODEL_LOADED(PROP_MP_PLACEMENT_SM)
            WAIT(0)
            REQUEST_MODEL(PROP_MP_PLACEMENT_SM)
        ENDWHILE

        IF NOT IS_PED_INJURED(PLAYER_PED_ID())

            v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
            v_player_heading = GET_ENTITY_HEADING(PLAYER_PED_ID())

                                    
            PlacementDiscEntity = CREATE_OBJECT(PROP_MP_PLACEMENT_SM, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_player_pos, v_player_heading, v_PlacementDiscOffset))
                   
            
            SET_ENTITY_COLLISION (PlacementDiscEntity, FALSE)

            
            
            SET_ENTITY_DYNAMIC (PlacementDiscEntity, FALSE)
            
            
          
        ENDIF
        
        SET_MODEL_AS_NO_LONGER_NEEDED (PROP_MP_PLACEMENT_SM)

        


        REQUEST_MODEL(CurrentlySelectedPropModel)
        WHILE NOT HAS_MODEL_LOADED(CurrentlySelectedPropModel)
            WAIT(0)
            REQUEST_MODEL(CurrentlySelectedPropModel)
        ENDWHILE

        IF NOT IS_PED_INJURED(PLAYER_PED_ID())

            v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
            v_player_heading = GET_ENTITY_HEADING(PLAYER_PED_ID())

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("Prop Test Creation")
                cdPrintnl()
            #endif
            
                        
            TestPropEntity = CREATE_OBJECT(CurrentlySelectedPropModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_player_pos, v_player_heading,  v_ThisPropOffset))
    
            //SET_ENTITY_ROTATION(TestPropEntity, <<0, 0, 90>>)

            //ATTACH_ENTITY_TO_ENTITY(TestPropEntity, PLAYER_PED_ID(), GET_PED_BONE_INDEX( PLAYER_PED_ID(), BONETAG_PH_R_HAND ), <<1.5, 2.0,0>>, <<0,0,0>>, TRUE, TRUE, FALSE )
            
            
            SET_ENTITY_COLLISION (TestPropEntity, FALSE)
            
            SET_ENTITY_DYNAMIC (TestPropEntity, FALSE)
            
            
            eCurrentPropStage = PROP_STAGE_PropInHand


        ENDIF


        SET_MODEL_AS_NO_LONGER_NEEDED (CurrentlySelectedPropModel)



        Set_Up_Buttons_For_Placement()



    ENDPROC //End of InitialSetup procedure.







    PROC Do_Prop_Stage_ExitAndCleanUp()
      



        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("Director Mode Prop Placement - Tidying up entities and models.")
            cdPrintnl()

        #endif

        IF i_InVicinityOfThisPlacedPropPosition > -99
            IF DOES_ENTITY_EXIST (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex) //Make sure any transparent entities are opaqued on exit.
                SET_ENTITY_ALPHA (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex, 255, TRUE)
            ENDIF
        ENDIF


        IF DOES_ENTITY_EXIST(TestPropEntity)
            DELETE_ENTITY (TestPropEntity)
        ENDIF

        IF DOES_ENTITY_EXIST(PlacementDiscEntity)
            DELETE_ENTITY (PlacementDiscEntity)
        ENDIF

        SET_MODEL_AS_NO_LONGER_NEEDED (PROP_MP_PLACEMENT_SM)
        SET_MODEL_AS_NO_LONGER_NEEDED (CurrentlySelectedPropModel)

        SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_PropPlacementMovieIndex)


        eCurrentPropStage = PROP_STAGE_Dormant
        

    ENDPROC










    PROC Do_Prop_Stage_PropInHand()


        IF DOES_ENTITY_EXIST (TestPropEntity)
        AND DOES_ENTITY_EXIST (PlacementDiscEntity)
        AND NOT IS_PED_INJURED (PLAYER_PED_ID())


            v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
            v_player_heading = GET_ENTITY_HEADING(PLAYER_PED_ID())




            /*

                    //Check position of prop entity and player.
                    //If in range, then go into removal mode, deleting disk and held prop ( like we do after placing one )
                    //Set InVicinity to that prop.
                    //Alpha prop.
                    //Do prop removal

            */


                        /*
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("PROP_PLACEMENT - In Vicinity of Placed Prop Check. Checking Prop  ")
                            cdPrintint(thisframeVicIndex)
                            cdPrintnl()
                        #endif
                        */
                               

                        IF PlacedProps[thisframeVicIndex].PositionStatus = PROP_POSITION_FILLED  

                            IF DOES_ENTITY_EXIST (PlacedProps[thisframeVicIndex].PropEntityIndex)       

                                

                                v_ThisPlacedProp = GET_ENTITY_COORDS (PlacedProps[thisframeVicIndex].PropEntityIndex)

                                IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos, v_ThisPlacedProp, TRUE ) < f_VicinityCheckRange

                                    i_InVicinityOfThisPlacedPropPosition = thisframeVicIndex

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("PROP_PLACEMENT - In Vicinity of Placed Prop! Position in placed array is ")
                                        cdPrintint(i_InVicinityOfThisPlacedPropPosition)
                                        cdPrintnl()
                                    #endif



                                    IF DOES_ENTITY_EXIST(TestPropEntity)
                                        DELETE_ENTITY (TestPropEntity)
                                    ENDIF

                                    IF DOES_ENTITY_EXIST(PlacementDiscEntity)
                                        DELETE_ENTITY (PlacementDiscEntity)
                                    ENDIF


                                    SET_ENTITY_ALPHA(PlacedProps[thisframeVicIndex].PropEntityIndex, 130, TRUE)

                                    Set_Up_Buttons_For_Removal()
                                                                    
                                    eCurrentPropStage = PROP_STAGE_PropRemoval

                           
                                ENDIF

                      

                            ENDIF

                        ENDIF
               
               
                        thisframeVicIndex ++
              
                     IF thisframeVicIndex = i_MAX_NUMBER_OF_PROPS_ALLOWED
                     OR thisframeVicIndex > i_MAX_NUMBER_OF_PROPS_ALLOWED

                        thisframeVicIndex = 0 // Reset the frame vicinity check counter.

                     ENDIF
              
            




                //Update Position of Prop in front of the player.
         
                
                SET_ENTITY_COORDS (TestPropEntity, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_player_pos, v_player_heading,  v_ThisPropOffset))
          
                IF DOES_ENTITY_EXIST (PlacementDiscEntity)
                    SET_ENTITY_COORDS (PlacementDiscEntity, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_player_pos, v_player_heading, v_PlacementDiscOffset))
                ENDIF

            
                    IF eCurrentPropStage <> PROP_STAGE_PropRemoval

                        IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_CREATOR_ACCEPT)

                            IF i_TotalNumberOfPlacedModels < i_MAX_NUMBER_OF_PROPS_ALLOWED

                                //Check if another prop is nearby here?

                                eCurrentPropStage = PROP_STAGE_PropPlacement


                            ENDIF

                        ENDIF

                  



                        IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_FRONTEND_LB)

                    
                            v_InHandPropRotation = GET_ENTITY_ROTATION (TestPropEntity)
            
                            IF v_InHandPropRotation.Z < 359.0
                            OR v_InHandPropRotation.Z = 359.0


                                v_InHandPropRotation.Z = v_InhandPropRotation.Z + 0.9
                            
                                SET_ENTITY_ROTATION(TestPropEntity, v_InHandPropRotation)
                            
                            ELSE

                                //v_InHandPropRotation.Z = 0
        
                                SET_ENTITY_ROTATION(TestPropEntity, v_InHandPropRotation)

                            ENDIF


                        ENDIF



                        IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_FRONTEND_RB)

                    
                            v_InHandPropRotation = GET_ENTITY_ROTATION (TestPropEntity)
            
                            //IF v_InHandPropRotation.Z > 0.0
                            //OR v_InHandPropRotation.Z = 0.0
                                                                
                                cdprintstring("testing!")
                                cdprintnl()

                                v_InHandPropRotation.Z = v_InhandPropRotation.Z - 0.9
                            
                                SET_ENTITY_ROTATION(TestPropEntity, v_InHandPropRotation)
                            
                            //ELSE


                                //cdprintstring("resetting r!")
                                //cdprintnl()


                                //v_InHandPropRotation.Z = 359.8
        
                                //SET_ENTITY_ROTATION(TestPropEntity, v_InHandPropRotation)

                            //ENDIF


                        ENDIF

                        




                        
                    ENDIF

           


        ENDIF



        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))
        
            i_CurrentModelIndex ++

            IF i_CurrentModelIndex = i_NumberOfModelsToCycle

                i_CurrentModelIndex = 0

            ENDIF

            eCurrentPropStage = PROP_STAGE_PropCycling
        
        ENDIF



        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))
        
            i_CurrentModelIndex --


            IF i_CurrentModelIndex < 0

                i_CurrentModelIndex = i_NumberOfModelsToCycle - 1 //Go to last position in array.

            ENDIF

        
            eCurrentPropStage = PROP_STAGE_PropCycling

        ENDIF



    ENDPROC




    PROC Do_Prop_Stage_PropPlacement()

       IF DOES_ENTITY_EXIST (TestPropEntity)
       AND NOT IS_PED_INJURED (PLAYER_PED_ID())

                //If the current location passes all placement location and prop quota checks, let's go ahead and place the prop.

                               
                VECTOR v_Placement_Position, v_Placement_Rotation

                v_Placement_Rotation = GET_ENTITY_ROTATION(TestPropEntity)

                v_Placement_Position = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_player_pos, v_player_heading, v_ThisPropOffset)
                //GET_GROUND_Z_FOR_3D_COORD ( v_player_pos, v_Placement_Position.z)

                v_Placement_Position.z = v_Placement_Position.z + 0.5
                
                //SET_ENTITY_COORDS(TestPropEntity, v_Placement_Position)

                //GET_GROUND_Z_FOR_3D_COORD ( v_player_pos, v_Placement_Position.z)

                //SET_ENTITY_DYNAMIC (TestPropEntity, TRUE)

                //SET_ENTITY_COLLISION (TestPropEntity, TRUE)

                //Turns out props don't always have the correct centre point so may appear embedded in the ground. 
                //Need to write a little tool that gets the correct z
                


                //FREEZE_ENTITY_POSITION(TestPropEntity, FALSE)

                
                //Delete the placement disc and reset everything for a new prop. This provides a natural little pause, rather than going direct into PropInHand state.
                IF DOES_ENTITY_EXIST (PlacementDiscEntity)
                    DELETE_ENTITY (PlacementDiscEntity)
                ENDIF


                IF DOES_ENTITY_EXIST (TestPropEntity)
                    DELETE_ENTITY (TestPropEntity)
                ENDIF




           

                //v_LastPropPlacement_Position = v_Placement_Position





    
                INT IdentifiedFreeIndex = -99
                INT tempIndex = 0


                WHILE tempIndex < i_MAX_NUMBER_OF_PROPS_ALLOWED

                    IF PlacedProps[tempIndex].PositionStatus = PROP_POSITION_VACANT   

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("PROP_PLACEMENT - Found VACANT prop placement position at ")
                            cdPrintint(tempIndex)
                            cdPrintnl()
                        #endif

                        IdentifiedFreeIndex = tempIndex

                        tempIndex = 99 //Break out of loop now we have identified a free index.

                    ELSE

                       tempIndex ++
                       
                    ENDIF   


                ENDWHILE


                IF IdentifiedFreeIndex > -99 //Must have found a free position in the Placed Props array.
                
                    //Update array of Placed Props - This will need to check for vacant positions
                    PlacedProps[IdentifiedFreeIndex].PropEntityIndex = CREATE_OBJECT(CurrentlySelectedPropModel, v_Placement_Position)
                    PlacedProps[IdentifiedFreeIndex].PropModelHash = SelectablePropArray[i_CurrentModelIndex].PropModelHash
                    PlacedProps[IdentifiedFreeIndex].PropRotation = v_Placement_Rotation
                    PlacedProps[IdentifiedFreeIndex].PropPlacementVector = v_Placement_Position
                    PlacedProps[IdentifiedFreeIndex].PositionStatus = PROP_POSITION_FILLED

                             

                    SET_ENTITY_COLLISION (PlacedProps[IdentifiedFreeIndex].PropEntityIndex, TRUE)



                    APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PlacedProps[IdentifiedFreeIndex].PropEntityIndex, APPLY_TYPE_FORCE, <<0,0,-60>>, 0, FALSE, TRUE) //Probably going to have to freeze objects :(
                    
                    

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("PROP_PLACEMENT - Placed Prop in PlacedProp array position ")
                            cdPrintint(IdentifiedFreeIndex)
                            cdPrintnl()
                        #endif

                    
                    
                    i_TotalNumberOfPlacedModels ++



                    IF i_TotalNumberOfPlacedModels = i_MAX_NUMBER_OF_PROPS_ALLOWED

                        //Display Warning

                    ENDIF
                    
  

          

                

                    //Once a prop has been placed the player should automatically go into PropRemoval state as they will be in the removal range of the last placed prop.
                    
                    
                    
                    i_InVicinityOfThisPlacedPropPosition = IdentifiedFreeIndex

                    SET_ENTITY_ALPHA(PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex, 130, TRUE)

                    

                    Set_Up_Buttons_For_Removal()

                    eCurrentPropStage = PROP_STAGE_PropRemoval


                ELSE


                    Set_Up_Buttons_For_Removal()

                    eCurrentPropStage = PROP_STAGE_PropRemoval


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("PROP_PLACEMENT - WARNING! Was trying to place a prop but did not find a vacant position! ")
                            cdPrintint(tempIndex)
                            cdPrintnl()
                        #endif

               ENDIF




        ENDIF

            
    ENDPROC //End of Do_Prop_Stage_PropPlacement procedure.






    PROC Do_Prop_Stage_PropCycling()

        //Would set up Dan's labels here for new prop selection.


        IF DOES_ENTITY_EXIST (TestPropEntity)
            DELETE_ENTITY (TestPropEntity)
        ENDIF


        IF DOES_ENTITY_EXIST (PlacementDiscEntity)
            DELETE_ENTITY (PlacementDiscEntity)
        ENDIF


        CurrentlySelectedPropModel = INT_TO_ENUM(MODEL_NAMES, SelectablePropArray[i_CurrentModelIndex].PropModelHash)

        eCurrentPropStage = PROP_STAGE_InitialSetUp //Load up the next prop.


    ENDPROC





    PROC Do_Prop_Stage_PropRemoval()

        IF NOT IS_PED_INJURED (PLAYER_PED_ID())


            //We'll need to check the full array of placed props here for proximity to the player.
            
            IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_CREATOR_DELETE))

                            
                IF i_InVicinityOfThisPlacedPropPosition > -1 
                AND i_InVicinityOfThisPlacedPropPosition < i_MAX_NUMBER_OF_PROPS_ALLOWED

                    IF DOES_ENTITY_EXIST (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex)
                    
                        DELETE_ENTITY (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex)
                        PlacedProps[i_InVicinityOfThisPlacedPropPosition].PositionStatus = PROP_POSITION_VACANT
                    
                    

                        i_TotalNumberOfPlacedModels --

                        //Need to set this position as PROP_POSITION_VACANT

                        i_InVicinityOfThisPlacedPropPosition = -99

                        eCurrentPropStage = PROP_STAGE_InitialSetUp
                         
                    ENDIF

                ENDIF

            ENDIF
            


            IF i_InVicinityOfThisPlacedPropPosition > -1 
            AND i_InVicinityOfThisPlacedPropPosition < i_MAX_NUMBER_OF_PROPS_ALLOWED

                IF DOES_ENTITY_EXIST (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex)
                    IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex, TRUE)) > ( f_VicinityCheckRange + 2.0 )

                        SET_ENTITY_ALPHA (PlacedProps[i_InVicinityOfThisPlacedPropPosition].PropEntityIndex, 255, TRUE)
 
                        i_InVicinityOfThisPlacedPropPosition = -99

                        eCurrentPropStage = PROP_STAGE_InitialSetUp 

                    ENDIF
                ENDIF
            
            ENDIF


        ENDIF


    ENDPROC


#endif



















SCRIPT











    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
                                                    

                                    
  

  
    #if IS_DEBUG_BUILD













    
        Create_Cellphone_Widget_Group()
    
        g_Cellphone_Disable_WidgetToggle = FALSE //Make sure global bool for phone sleep is set to FALSE on launch of this script.

        
        //Remember! Assigned ped voices must match the voice speaker correlation entries in the dialoguestar sheet or the audio will not play.

        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, NULL, "MICHAEL")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "FRANKLIN")

        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 2, NULL, "TREVOR")

        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 3, NULL, "STEVE")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 3, NULL, "EDDIE")

        
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 4, NULL, "Dave")
        ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 5, NULL, "MisterK")

        SET_VOICE_LOCATION_FOR_NULL_PED (MyLocalPedStruct, 4, <<-31.5, 16.2, 89.9>>)


        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, ConvertSingleCharacter("8"), PLAYER_PED_ID(), "MICHAEL")

        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, ConvertSingleCharacter("D"), PLAYER_PED_ID(), "FRANKLIN")

        //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")



        //Set up movie titles for debug selection widget. To add more increase TEXT_LABEL_23 ScaleformDebugMovie[6] in cellphone_globals.sch and 

        ScaleformDebugMovie[0] = "cellphone_badger"
        ScaleformDebugMovie[1] = "cellphone_ifruit"
        ScaleformDebugMovie[2] = "cellphone_b"
        ScaleformDebugMovie[3] = "cellphone_ifruit"
        ScaleformDebugMovie[4] = "cellphone_facade"
        ScaleformDebugMovie[5] = "cellphone_prologue" 



        SET_TRACKIFY_TARGET_VECTOR (<<1565.5, 2858.3, 54.7>>, TRUE)




    #endif
 

    i_SP_Queue_Window_StartTime = GET_GAME_TIMER()  //Used by Social Feed Controller to determine whether the feed creation window is open or not. 
                                                    //Starting it here as the Cellphone Controller script is persistent across SP and MP.


    

    //Set up screen positions and keep a copy of display ratio type via Setup_Cellphone_Signifier...
    Setup_Cellphone_Signifier_Positions_according_to_Screen_Ratio()


    Fill_All_Primary_App_Sheets()
    TitleUpdate_Patch_In_JobList_For_SP_Phone()


  
    #if IS_DEBUG_BUILD


        IF g_Chosen_Ratio = DISPLAY_16_9
        
            g_Cellphone_Ratio_4_3_WidgetToggle = FALSE
            g_Cellphone_Ratio_16_9_WidgetToggle = TRUE

        ELSE
           
            g_Cellphone_Ratio_16_9_WidgetToggle = FALSE
            g_Cellphone_Ratio_4_3_WidgetToggle = TRUE

        ENDIF


    #endif






    //Set phone display status to AWAY on startup of this script.
    g_Cellphone.PhoneDS = PDS_AWAY

    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("STATE ASSIGNMENT 42. Cellphone_Controller assigns PDS_AWAY")
        cdPrintnl()   
    #endif



    //g_b_ToggleButtonLabels = TRUE //default to showing button labels

    g_b_ToggleButtonLabels = FALSE //default to not showing button labels

    g_b_Secondary_Screen_Available = FALSE //default to having secondary screen unavailable


    g_Cell_Pic_Stage = PIC_STAGE_IDLE



    //When testing release mode, reinstate this section in order to send some test text messages so buttons and format can be checked.
    /*
    WAIT (2000)
    SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "TMSG", "TXT_101", TXTMSG_LOCKED)
    
    WAIT (2000)                                               
    SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "TMSG", "TXT_102", TXTMSG_UNLOCKED)
                                                            
    WAIT (2000)
    SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_TREVOR, "TMSG", "TXT_106", TXTMSG_UNLOCKED)
    */




    //Do not check in... This is for testing the core code phone if the main loop contents are disabled.
    //CREATE_MOBILE_PHONE (MOBILE_1)

 


//DMPP Prop Placement temp setup
#if IS_DEBUG_BUILD

    eCurrentPropStage = PROP_STAGE_Dormant

#endif
     scrEmail_Data  single_Marketing_email_Received_Data

  
      
    WHILE TRUE





        WAIT(0)

       



        //This will fix 2502872 and 2517397 when code fix the actual toggle.
        IF g_Cellphone.PhoneDS > PDS_AWAY 
            
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PARACHUTE) //fix for 3176604
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM) //4168740 and 4152553 - Disable Deluxo transformation when phone is up in any circumstance.
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PARACHUTE) //4983240 - Disable Scramjet boost when phone is up in any circumstance.
			
            //DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "Dev NG CHN TEST PC")
   
        ENDIF
   




        #if IS_DEBUG_BUILD

            IF g_b_ForceRerunOfMarketingPosixGrab
            AND GNMCurrentStage = GNM_RETRIEVAL_FINISHED
            
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Cellphone_Controller - DEBUG FORCING a new marketing email retrieval process.")
                    cdPrintnl()   
                #endif

                g_b_ForceRerunOfMarketingPosixGrab = FALSE
                GNMCurrentStage = GNM_DORMANT

            ENDIF

        #endif




        IF GNMCurrentStage = GNM_DORMANT

            IF NETWORK_IS_GAME_IN_PROGRESS()

                GNMCurrentStage = GNM_LAUNCH_RETRIEVAL

                tempPosixStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_READ_EMAIL_POSIX)


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Cellphone_Controller - Launching Marketing email retrieval process.")
                    cdPrintnl()   
                #endif

            ENDIF

        ELSE

            IF NETWORK_IS_GAME_IN_PROGRESS()

                IF GNMCurrentStage = GNM_LAUNCH_RETRIEVAL

                    //Start email retrieval
                    //Get stat value
                    //Set launch timer

                    //2563312  - We need to support localisation tags now.
                    SWITCH GET_CURRENT_LANGUAGE()


                        CASE LANGUAGE_UNDEFINED
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_en")
                        BREAK

                        CASE LANGUAGE_ENGLISH
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_en")
                        BREAK

                        CASE LANGUAGE_FRENCH
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_fr")
                        BREAK

                        CASE LANGUAGE_GERMAN
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ge")
                        BREAK

                        CASE LANGUAGE_ITALIAN
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_it")
                        BREAK

                        CASE LANGUAGE_SPANISH
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_sp")
                        BREAK

                        CASE LANGUAGE_PORTUGUESE
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_pt")
                        BREAK

                        CASE LANGUAGE_POLISH
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_pl")
                        BREAK

                        CASE LANGUAGE_RUSSIAN
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ru")
                        BREAK

                        CASE LANGUAGE_KOREAN
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ko")
                        BREAK

                        CASE LANGUAGE_CHINESE
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ch")
                        BREAK

                        CASE LANGUAGE_JAPANESE
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ja")
                        BREAK

                        CASE LANGUAGE_MEXICAN
                            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_me")
                        BREAK
						CASE LANGUAGE_CHINESE_SIMPLIFIED
							//New tag provided in bug 3790944.
							SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_cn")
                        BREAK

                    ENDSWITCH

                    //Removed.
                    //SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5marketing") //Critical! We only want to retrieve marketing messages, not any Eyefind user sent stuff.
					
					IF MPGlobalsAmbience.bRecievedEventEmail
                    	SC_EMAIL_RETRIEVE_EMAILS (0, 1) //Just need to grab the first email which should be the newest.
					ENDIF



                    GNMCurrentStage = GNM_RETRIEVAL_ONGOING

                ENDIF


                IF GNMCurrentStage = GNM_RETRIEVAL_ONGOING
                
                    MarketingCurrentRetrievalStatus = SC_EMAIL_GET_RETRIEVAL_STATUS()

                    SWITCH MarketingCurrentRetrievalStatus


                            //Marketing Retrieval Processed Finished ? THIS IS NOT GETTING HIT FROM CODE.
                            CASE EMAIL_RETRIEVAL_SUCCEEDED

                                #IF IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_Cont - Finished retrieving email batch.")
                                    cdPrintnl()
                                #ENDIF
                                
                            BREAK


                            CASE EMAIL_RETRIEVAL_NONE

                                #IF IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_Cont - Marketing EMAIL RETRIEVAL NONE - Should not see this spam repeatedly! Report to Steve T! Marketing emails retrieved is  ")
                                    cdPrintnl()
                                    cdPrintint(SC_EMAIL_GET_NUM_RETRIEVED_EMAILS())
                                #ENDIF

                                //Need to do stat posix comparison here if we have any retrieved emails.
                                IF SC_EMAIL_GET_NUM_RETRIEVED_EMAILS() > 0



                                    SC_EMAIL_GET_EMAIL_AT_INDEX (0, single_Marketing_email_Received_Data)

                                    //INT iArrayIndex = 0

                                    //CONVERT_POSIX_TIME (single_email_Received_Data.CreatePosixTime, SingleEmailTimeStamp)

                              
                                    #if IS_DEBUG_BUILD
                                          
                                        cdPrintstring("Marketing Subject is ")
                                        cdPrintstring(single_Marketing_email_Received_Data.Subject[1])
                                        cdPrintnl()
                                        cdPrintstring(" -  Sender was ")
                                        cdPrintstring(single_Marketing_email_Received_Data.SenderName)
                                        cdPrintstring(" at posix ")
                                        cdPrintint(single_Marketing_email_Received_Data.CreatePosixTime)
                                        cdPrintnl()
                                        cdPrintstring("The grabbed posix profile stat for comparison is ")
                                        cdPrintint(tempPosixStat)
                                        cdPrintnl()
                                 
                                    #endif
               
                                    IF single_Marketing_email_Received_Data.CreatePosixTime <> tempPosixStat //This must be a fresh marketing email if it does not match the stat.

                                        g_NumberOfUnreadMarketingEmailsThisSession = 1 // Set the unread emails to 1. The stat will be updated in AppMPEmail when the user reads it. 
										
                                    ELSE

                                        g_NumberOfUnreadMarketingEmailsThisSession = 0 // Set the unread emails to 0.  The posix stat and the posix for the newest email match.

                                    ENDIF


                                ENDIF


                                GNMCurrentStage = GNM_RETRIEVAL_FINISHED  //Don't set to dormant. We only do this once per session.

                            BREAK


                            CASE EMAIL_RETRIEVAL_PENDING

                                #IF IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_Cont - Marketing EMAIL RETRIEVAL PENDING...")
                                    cdPrintnl()
                                #ENDIF

                            BREAK


                            CASE EMAIL_RETRIEVAL_CANCELLED

                                #IF IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_Cont - Marketing EMAIL RETRIEVAL CANCELLED!")
                                    cdPrintnl()
                                #ENDIF

                                g_NumberOfUnreadMarketingEmailsThisSession = 0

                                GNMCurrentStage = GNM_RETRIEVAL_FINISHED  //Don't set to dormant. We only do this once per session.

                            BREAK


                        ENDSWITCH
                    

                ENDIF


            ELSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Cellphone_Controller - Was trying to retrieve Marketing email but Net game no longer in progress. Aborting.")
                    cdPrintnl()   
                #endif


                g_NumberOfUnreadMarketingEmailsThisSession = 0
                GNMCurrentStage = GNM_DORMANT

            ENDIF

        ENDIF










        
        #if IS_DEBUG_BUILD
            
            IF IS_IN_VEHICLE_MOBILE_PHONE_CAMERA_RENDERING() 
                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "2 Veh_Cam_Rendering Test")
                //REPLAY_PREVENT_RECORDING_THIS_FRAME() //Fully prevent prop editor recording for bug 2410393. 
            ENDIF
           
            
            //DMPP
            #if IS_DEBUG_BUILD
            //Start of temporary Director Mode prop placement work
            SWITCH eCurrentPropStage
            
                CASE PROP_STAGE_Dormant
                    //DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_Dormant") 
                BREAK

                CASE PROP_STAGE_InitialSetUp
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_Initial") 
                BREAK

                CASE PROP_STAGE_PropCycling
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_PropCycling") 
                BREAK

                CASE PROP_STAGE_PropInHand
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_PropInHand") 
                BREAK

                CASE PROP_STAGE_PropPlacement
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_PropPlace") 
                BREAK

                CASE PROP_STAGE_PropRemoval
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_LastPropRemove") 
                BREAK

                CASE PROP_STAGE_SavingPropScene
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_Saving") 
                BREAK

                CASE PROP_STAGE_ExitAndCleanup
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.10, "STRING", "PS_Cleaning up!") 
                BREAK

            ENDSWITCH



            IF eCurrentPropStage > PROP_STAGE_DORMANT


                //FIX THIS:
                //Error: [script] Assertf(pEntity) FAILED: SCRIPT: Script Name = cellphone_controller : Program Counter = 39551: SET_ENTITY_COORDS - Entity doesn't exist (from Updating script cellphone_controller)


                
                SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_FRONTEND_LB)
                SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_FRONTEND_RB)

                SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_CREATOR_ACCEPT)
                SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_CREATOR_DELETE)


                DISPLAY_TEXT_WITH_2_NUMBERS (0.60, 0.25, "DMPP_MODNUM", i_TotalNumberOfPlacedModels, i_MAX_NUMBER_OF_PROPS_ALLOWED) //Text from americanreplay.txt

                IF i_InVicinityOfThisPlacedPropPosition > -99 

                    DISPLAY_TEXT_WITH_NUMBER (0.80, 0.1, "DMPP_NEARBY", i_InVicinityOfThisPlacedPropPosition)


                ELSE

                    DISPLAY_TEXT_WITH_FLOAT (0.80, 0.1, "CELL_500", v_InHandPropRotation.z, 3)

                ENDIF


       

                DISPLAY_TEXT (0.6, 0.35, SelectablePropArray[i_CurrentModelIndex].PropTextLabel) //Text from americancreator.txt


                SWITCH SelectablePropArray[i_CurrentModelIndex].PropCategory
                
                
                    CASE DM_PROPCATEGORY1

                        DISPLAY_TEXT (0.6, 0.45, "DMPP_CAT1") //Text from americanreplay.txt

                    BREAK

                    
                    CASE DM_PROPCATEGORY2

                        DISPLAY_TEXT (0.6, 0.45, "DMPP_CAT2") //Text from americanreplay.txt

                    BREAK


                    CASE DM_PROPCATEGORY3

                        DISPLAY_TEXT (0.6, 0.45, "DMPP_CAT3") //Text from americanreplay.txt

                    BREAK


                ENDSWITCH



            ENDIF

            #endif


            //DMPP
            #if IS_DEBUG_BUILD
            //When integrated with the Director Mode script, the prop placement routines would be woken up from the dormant state by selection of a menu option.
            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_9) 
            //OR Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //For Xbox One  



                //SET_EXTRACTION_PRESIDENT_VECTOR (<<1.1, 7.3, 9.5>>)


    
                //SET_EXTRACTION_BODYGUARD_VECTOR (<<4.2, 8.4, 12.6>>)





                            /*
                            REQUEST_SCRIPT ("director_mode")
                                                     
                            WHILE NOT HAS_SCRIPT_LOADED ("director_mode")
                           
                                WAIT(0)
                     
                            ENDWHILE


                            START_NEW_SCRIPT ("director_mode", MISSION_STACK_SIZE)
                            */

                /*
                IF eCurrentPropStage = PROP_STAGE_Dormant

                


             



                    SF_PropPlacementMovieIndex = REQUEST_SCALEFORM_MOVIE ("instructional_buttons")

                    WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_PropPlacementMovieIndex)
                    OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_PropPlacementMovieIndex)
                 
                        WAIT (0)
      
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Waiting on Prop Placement instructional buttons movie to load...")
                            cdPrintnl()   
                        #endif

                    ENDWHILE






                    //Use existing Creator Prop text to label props.
                    REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
                    WHILE NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)

                        WAIT (0)

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Waiting on Prop Placement information text to load test...")
                            cdPrintnl()   
                        #endif

                    ENDWHILE

                    
              
                    Fill_Selectable_Prop_Sheet (0, "PROP_MP_CONE_01", "FMMC_DPR_CONE", DM_PROPCATEGORY1)  
                    Fill_Selectable_Prop_Sheet (1, "prop_bench_09", "FMMC_PR_12", DM_PROPCATEGORY2)  
                    Fill_Selectable_Prop_Sheet (2, "PROP_MP_RAMP_01", "FMMC_PR_RMPLFR", DM_PROPCATEGORY3)  
                    Fill_Selectable_Prop_Sheet (3, "PROP_OFFROAD_BALE01", "FMMC_PR_HBSTK", DM_PROPCATEGORY2)  


         
                    //Test Props
                    //SelectablePropArray[0].PropModelHash = GET_HASH_KEY("PROP_MP_CONE_01")
                    //SelectablePropArray[0].PropTextLabel = "FMMC_DPR_CONE"
                    //SelectablePropArray[0].PropCategory = DM_PROPCATEGORY1

                                                                                                                     

                    //SelectablePropArray[1].PropModelHash = GET_HASH_KEY("prop_bench_09")
                    //SelectablePropArray[2].PropModelHash = GET_HASH_KEY("PROP_MP_RAMP_01")
                    //SelectablePropArray[3].PropModelHash = GET_HASH_KEY("PROP_OFFROAD_BALE01")


        
                    i_CurrentModelIndex = 0


                    CurrentlySelectedPropModel = INT_TO_ENUM(MODEL_NAMES, SelectablePropArray[i_CurrentModelIndex].PropModelHash)

                
           
                    Set_Up_Buttons_For_Placement()



                    eCurrentPropStage = PROP_STAGE_InitialSetUp

                ELSE

                    eCurrentPropStage = PROP_STAGE_ExitAndCleanUp

                ENDIF

                */


            ENDIF // End of debug Key_9 pressed condition check.

            #endif








            IF eCurrentPropStage > PROP_STAGE_DORMANT


                DRAW_SCALEFORM_MOVIE_FULLSCREEN (SF_PropPlacementMovieIndex, 255, 255, 255,0)


                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))

                    Do_Prop_Stage_ExitAndCleanUp()
                
                ENDIF
                
            ENDIF   






            IF eCurrentPropStage = PROP_STAGE_ExitAndCleanUp

                Do_Prop_Stage_ExitAndCleanUp()
                                 
            ENDIF



        

            //Set up placement disk and first prop model.
            IF eCurrentPropStage = PROP_STAGE_InitialSetUp
                
                Do_Prop_Stage_InitialSetUp()

            ENDIF //End of eCurrentPropStage = PROP_STAGE_InitialSetUp condition check.




            IF eCurrentPropStage = PROP_STAGE_PropCycling

                Do_Prop_Stage_PropCycling()

            ENDIF //End of eCurrentPropStage = PROP_STAGE_PropInHand condition check.



            
            //The player is currently holding a prop and deciding where to place it. The placement disk and the prop itself update their position each
            //frame until the player selects a location. We also check for the rotation of the object and the player cycling to a different object
            //from the menu. All button checks temporary.
            IF eCurrentPropStage = PROP_STAGE_PropInHand

                //IF i_TotalNumberOfPlacedModels = i_MAX_NUMBER_OF_PROPS_ALLOWED 

                   //Do_Prop_Stage_PropRemoval()
                
                //ELSE
                
                    Do_Prop_Stage_PropInHand()

                //ENDIF

            ENDIF //End of eCurrentPropStage = PROP_STAGE_PropInHand condition check.


            

            IF eCurrentPropStage = PROP_STAGE_PropPlacement

                Do_Prop_Stage_PropPlacement()
            
            ENDIF //End of eCurrentPropStage = PROP_STAGE_PropPlacement condition check.
             




           IF eCurrentPropStage = PROP_STAGE_PropRemoval

                Do_Prop_Stage_PropRemoval()

           ENDIF


            










        #endif


                                                                             

























        IF IS_PC_VERSION() //Only fixing on PC just now in case this impacts other platforms in an unforeseen way... 06.01.15
            
            
            IF IS_STUNT_JUMP_IN_PROGRESS() //2177764
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM //On homescreen only. Don't abort phone on call during a stunt jump.
                
                    g_Cellphone.PhoneDS = PDS_AWAY

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 12065. Cellphone_Controller - Phone on homescreen during stunt jump. Aborting!")
                        cdPrintnl()   
                    #endif

                ENDIF       
            ENDIF

        ENDIF


        
        //Opening up director mode entry from phone for 2260082 for console as well as PC.
        IF IS_CALLING_CONTACT(CHAR_DIRECTOR)    //2260082 - if the player is calling Los Santos Talent from the contact list rather than the cheat number 1-999_LSTALENT
                                                //we need to handle that here by launching director mode.

            #IF FEATURE_SP_DLC_DIRECTOR_MODE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Cellphone_Controller - dialling Los Santos Talent from contact list... Launching director mode if in SP")
                    cdPrintnl()
                #endif

                IF g_bInMultiplayer = FALSE //SP launch only.
                
                    FEATURE_BLOCK_REASON eBlockReason
                    eBlockReason = GET_FEATURE_BLOCKED_REASON(DEFAULT, DEFAULT, DEBUG_DIRECTOR)
                    IF eBlockReason != FBR_NONE
                        CPRINTLN(DEBUG_DIRECTOR, "Director Mode could not launch with warning screen. Block condition ", DEBUG_GET_FEATURE_BLOCK_REASON_STRING(eBlockReason,DEBUG_DIRECTOR), " triggered during the process.")
                        DO_DIRECTOR_MODE_WARNING_SCREEN_WAIT(eBlockReason)
                    ELSE
                        DO_DIRECTOR_MODE_ACCEPT_SCREEN_WAIT() 
                    ENDIF

                    HANG_UP_AND_PUT_AWAY_PHONE()
                ENDIF

            #ENDIF
            
        ENDIF                                                                                         


// Kenneth R - we now need this for magdemo2

//        IF g_bMagdemoActive //Quit this script when magdemo runs.
//
//            #if IS_DEBUG_BUILD
//
//                PRINTSTRING("Cellphone Controller quitting due to magdemo TRUE")
//                PRINTNL()
//    
//            #endif
//            
//            TERMINATE_THIS_THREAD()
//
//        ENDIF






        //Fix for 2181545
        IF MPGlobalsAmbience.bUsingTelescope = TRUE

            IF g_Cellphone.PhoneDS > PDS_AWAY
            
                g_Cellphone.PhoneDS = PDS_AWAY

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 12015. Cellphone_Controller - MPGlobalsAmbience.bUsingTelescope = TRUE - Forcing phone to AWAY state.")
                    cdPrintnl()   
                #endif

            ENDIF

        ENDIF

        /* Debug for 2181545
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ob_telescope")) > 0

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Telescope") 

        ENDIF


        IF MPGlobalsAmbience.bUsingTelescope = TRUE

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Tele in USE") 

        ENDIF
        */










        //Application Launcher for Multiplayer to prevent WAIT 0 problems. //2139576
        IF g_bInMultiplayer = TRUE
            IF g_Cellphone.PhoneDS = PDS_AWAY //Cellphone must be in away state to launch applications.


                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPCAMERA) //Set by App Switch in LAUNCH_CELLPHONE_APPLICATION if in MP.
                
                    SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH) //Specifies that LAUNCH_CELLPHONE_APPLICATION should go through actual launch route next time evben thoughh 
                    
             
                    WAIT (0)

                    
                    WHILE NOT CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
                    
                        WAIT(0)

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Waiting for CC_INTERNAL MP Launch of AppCamera...")
                            cdPrintnl()
                        #endif

                    ENDWHILE

                    
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPCAMERA)

                ENDIF 

                

                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY) //Set by App Switch in LAUNCH_CELLPHONE_APPLICATION if in MP.
                
                    SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH) //Specifies that LAUNCH_CELLPHONE_APPLICATION should go through actual launch route next time even though we are in MP.
                    
                 
                    WAIT (0)

                    
                    WHILE NOT CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION(AppTrackify, FALSE, TRUE, FALSE)

                        WAIT(0)

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Waiting for MP CC_INTERNAL Launch of AppTrackify...")
                            cdPrintnl()
                        #endif

                    ENDWHILE
                    

                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)

                ENDIF 
				
				
				
				IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0) //Set by App Switch in LAUNCH_CELLPHONE_APPLICATION if in MP.
                
                    SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH) //Specifies that LAUNCH_CELLPHONE_APPLICATION should go through actual launch route next time even though we are in MP.
                    
                 
                    WAIT (0)

                    
                    WHILE NOT CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION(AppDummyApp0, FALSE, TRUE, FALSE)

                        WAIT(0)

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Waiting for MP CC_INTERNAL Launch of DummyApp0...")
                            cdPrintnl()
                        #endif

                    ENDWHILE
                    

                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)

                ENDIF 

				IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTEXTS) //Set by App Switch in LAUNCH_CELLPHONE_APPLICATION if in MP.
                	
                    SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH) //Specifies that LAUNCH_CELLPHONE_APPLICATION should go through actual launch route next time even though we are in MP.
                    
             		
                    WAIT (0)
					
                    
                    WHILE NOT CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION(AppTEXTS, FALSE, TRUE, FALSE)
                    	
                        WAIT(0)
						
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_CONT - Waiting for CC_INTERNAL MP Launch of AppTEXTS...")
                            cdPrintnl()
                        #endif
						
                    ENDWHILE
					
                    
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTEXTS)
					
                ENDIF 
            ENDIF
        ENDIF    







    /*
    Reinstate this comment to disable main loop for core code phone testing.
    */ 




        //For bug #1891329 there seems to be a chance of PROCESS_VOICE_SESSION_STARTED_EVENT() setting voiceSession.bInVoiceSession = TRUE
        //when the phone is away / disabled.

        //If voiceSession.bInVoiceSession is TRUE and the phone is away then we should immediately abort the session! Need to check this doesn't screwing anything up on the 
        //call receiver's end.

//        IF voiceSession.bInVoiceSession = TRUE
//
//            IF g_Cellphone.PhoneDS < PDS_TAKEOUT
//
//                #if IS_DEBUG_BUILD
//                 
//                    PRINTLN("WARNING! voiceSession.bInVoiceSession = TRUE but phone is still away or disabled. Aborting via SCRIPT_END_A_VOICE_SESSION #1891329")
//                    
//                    cdPrintnl()
//                    cdPrintString("WARNING! voiceSession.bInVoiceSession = TRUE but phone is still away or disabled. Aborting via SCRIPT_END_A_VOICE_SESSION #1891329")
//                    cdPrintnl()
//
//                #endif
//
//                SCRIPT_END_A_VOICE_SESSION()
//
//                voiceSession.bInVoiceSession = FALSE
//
//            ENDIF
//
//        ENDIF








        //New work to support On Call and Invite signifier checks. TODO 2042111
        IF g_bInMultiplayer = FALSE //Phone's always on screen in MP and we need to display the invites in the same place.

            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)

                IF (g_Cellphone.PhoneDS <> PDS_ATTEMPTING_TO_CALL_CONTACT //See 2001926. For 99 percent of calls we want to show the phone on the hud
                AND g_Cellphone.PhoneDS <> PDS_ONGOING_CALL)
                OR ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE
                                        
                        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)
                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)


                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - g_BSTU_ONCALL_SIGNIFIER_DISPLAYED was set, but player no longer on call. Clearing bit, updating sig movie.")
                            cdPrintnl()

                        #endif

                    

                ENDIF

            ELSE

                IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT //See 2001926. For 99 percent of calls we want to show the phone on the hud
                OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL
                   
                    IF ShouldFirstPersonPhoneHudMovementBeHidden() 
                   
                        SET_BIT (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)
                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                        #if IS_DEBUG_BUILD

                            cdPrintString("CELLPHONE_CONT - g_BSTU_ONCALL_SIGNIFIER_DISPLAYED was clear, but player now on call. Setting bit, updating sig movie.")
                            cdPrintnl()

                        #endif

                    ENDIF

                ENDIF

            ENDIF


            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED) //Make sure any invites received in MP can't cross over into SP by clearing the signifier.
                
                CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)
                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)


                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - g_BSTU_INVITE_SIGNIFIER_DISPLAYED was set, but player now back in SP. Clearing bit, updating sig movie.")
                    cdPrintnl()

                #endif

            ENDIF



        ELSE  //Handle Invites in MP only.


            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)


// KGMINDICATOR
                IF ((g_numNewJoblistInvites = 0) OR (g_displayPlayerInvitesIndicator = FALSE)) //2060056 - requested that we only display indicator if there are human player invites pending.
                AND NOT (g_forceDisplayPlayerInvitesIndicator)
//                IF g_numNewJoblistInvites = 0
//                OR g_displayPlayerInvitesIndicator = FALSE //2060056 - requested that we only display indicator if there are human player invites pending.


                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)
                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - g_BSTU_INVITE_SIGNIFIER_DISPLAYED was set, but pending invites now at zero. Clearing bit, updating sig movie.")
                        cdPrintnl()

                    #endif
                
                ENDIF

            ELSE


// KGMINDICATOR
                IF (g_forceDisplayPlayerInvitesIndicator)
                OR ((g_numNewJobListInvites > 0) AND (g_displayPlayerInvitesIndicator = TRUE)) //2060056 - requested that we only display indicator if there are human player invites pending.
//                IF g_numNewJobListInvites > 0
//                AND g_displayPlayerInvitesIndicator = TRUE //2060056 - requested that we only display indicator if there are human player invites pending.


                    SET_BIT (Bitset_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)
                    SET_BIT (Bitset_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - g_BSTU_INVITE_SIGNIFIER_DISPLAYED was clear, but invite now pending. Setting bit, updating sig movie.")

                    #endif

                 ENDIF

            ENDIF

        ENDIF


        //2159597 work begins...
        IF IS_PC_VERSION()

            //IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_PC_PUSH_TO_TALK) //Needs updated with PC Control INPUT_PC_PUSH_TO_TALK when available.    
            IF NETWORK_IS_PUSH_TO_TALK_ACTIVE() //2196978 - David Yang requested we swap over to this check rather than relying on the control above to avoid state mismatches. 
            //OR g_DumpDisableEveryFrameCaller// - Comment in to easily test via widget.  
                IF NOT (IS_BIT_SET (Bitset_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED))

                    SET_BIT (Bitset_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)
                    SET_BIT (Bitset_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - PC PUSH_TO_TALK toggled ON via NETWORK_IS_PUSH_TO_TALK_ACTIVE command.")
                        cdPrintnl()

                    #endif

                ENDIF

            ELSE

                IF IS_BIT_SET (Bitset_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)

                    CLEAR_BIT (Bitset_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)
                    SET_BIT (Bitset_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - PC PUSH_TO_TALK toggled off.")
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF
            
        ENDIF        
        //End of 2159597 work.


        //Sleep Mode Stat Check
        IF b_CheckingSleepModeDurationForStatUpdate = FALSE

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)

                STAT_GET_INT (SP_CELLPHONE_SLEEP_TIME, i_ValueOfStatWhenSleepWasBegun)

                i_SleepModeStartTime = GET_GAME_TIMER()

                b_CheckingSleepModeDurationForStatUpdate = TRUE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("CELLPHONE_CONT - Now in Sleep Mode. Duration logging begins. Got existing stat SP_CELLPHONE_SLEEP_TIME which is ")
                    cdPrintInt(i_ValueOfStatWhenSleepWasBegun)
                    cdPrintnl()
                    cdPrintnl()

                #endif

            ENDIF

        ELSE

            IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER))

                STAT_GET_INT (SP_CELLPHONE_SLEEP_TIME, i_ValueOfStatWhenSleepWasBegun) //Safer to get this again.
             
                i_SleepModeEndTime = GET_GAME_TIMER()

                b_CheckingSleepModeDurationForStatUpdate = FALSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()

                    cdPrintString("CELLPHONE_CONT - No longer in Sleep Mode. Duration logging ended. Updating stat SP_CELLPHONE_SLEEP_TIME.")
                    cdPrintnl()
                    cdPrintString("Value of original stat  was: ")
                    cdPrintInt(i_ValueOfStatWhenSleepWasBegun)
                    cdPrintnl()
                    cdPrintString("Value of this Sleep Duration was: ")
                    cdPrintInt((i_SleepModeEndTime - i_SleepModeStartTime))
                    cdPrintnl()
                    cdPrintstring("New stat value assigned: ")
                    cdPrintInt(i_ValueOfStatWhenSleepWasBegun + (i_SleepModeEndTime - i_SleepModeStartTime))
                    cdPrintnl()
                    cdPrintnl()

                #endif


                STAT_SET_INT (SP_CELLPHONE_SLEEP_TIME, i_ValueOfStatWhenSleepWasBegun + (i_SleepModeEndTime - i_SleepModeStartTime))


            ENDIF
   
        ENDIF















        Check_For_Disable_Cellphone_ThisFrameOnly()

        Check_For_Disable_Camera_App_ThisFrameOnly()

        Check_For_Disable_Internet_App_ThisFrameOnly()



        //Signifier checks...

        Check_If_Signifier_Movie_Needs_Drawn_or_Removed() //Solely used by SLEEP mode signifier now.
      

        Check_For_PlayerSwap() //Important that this runs every frame so the cellphone owner is kept up to date.


        Check_For_Read_Auto_TextLaunch_Timeout()


        Check_For_CancelledMPTextData()



        //Master Mobile Interference control...  This bit is set in dialogye handler when the pre-ring effect is played.
        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)

            
            IF NOT IS_MOBILE_INTERFERENCE_ACTIVE() //As long as the pre-ring effect is playing the phone will remain in takeout state unless the bit is cleared by a manual d-pad press.

               CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
    
            ENDIF


        ENDIF



        //If a SEND_TEXT_FUNCTION... has been called, then it will set this flag.
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED) 

            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Update_Feed_For_New_Text_Message requested.")
                cdPrintnl()

            #endif



            Update_Feed_For_New_Text_Message()

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)
        
        ENDIF



        
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)

            Create_Contact_Feed_Entry()

        ENDIF




        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_DISPLAY_EMAIL_SIGNIFIER) 

            Create_Email_Feed_Entry()

        ENDIF
            




        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)  
        
       
            #if IS_DEBUG_BUILD

                cdPrintString("CELLPHONE_CONT - Updating Cellphone Signifier usage.")
                cdPrintnl()

            #endif

  
            g_Current_Number_Of_Signifiers = 0  //This will force Check_If_Signifier_Movie_Needs 
                                                //to remove the cellphone alert movie next frame if NO further alerts are added immediately below.


       
        
            
            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER) 

                g_Current_Number_Of_Signifiers ++
                
                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - Sleep mode signifier bit has been set. Current number of signifiers incremented.")
                    cdPrintnl()

                #endif

            ENDIF

            
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ONCALL_SIGNIFIER_DISPLAYED)

                IF g_bInMultiplayer = FALSE  //Only display on call signifier in SP. 

                    g_Current_Number_Of_Signifiers ++
                
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - On call signifier bit has been set. Current number of signifiers incremented.")
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF


			//Added for 3599537 - display beacon beside radar.
			IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)

                IF g_bInMultiplayer = TRUE  //Only display the beacon signifier in MP. 

                    g_Current_Number_Of_Signifiers ++
                
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - BEACON_SIGNIFIER bit has been set. Current number of signifiers incremented.")
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF





            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_INVITE_SIGNIFIER_DISPLAYED)

                IF g_bInMultiplayer //Only display invite signifier in MP
// KGMINDICATOR
                AND (g_displayPlayerInvitesIndicator OR g_forceDisplayPlayerInvitesIndicator)
//                AND g_displayPlayerInvitesIndicator = TRUE //2060056 - requested that we only display indicator if there are human player invites pending.


                    g_Current_Number_Of_Signifiers ++
                
                    #if IS_DEBUG_BUILD

                        cdPrintString("CELLPHONE_CONT - Invite signifier bit has been set. Current number of signifiers incremented.")
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF



        
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED)

                g_Current_Number_Of_Signifiers ++
            
                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - PUSHTALK signifier bit has been set. Current number of signifiers incremented.")
                    cdPrintnl()

                #endif

            ENDIF


            
            //When g_Current_Number_Of_Signifiers > 0 then that means Check_If_Signifier_Movie_Needs_Drawn_or_Removed() is forced to load the scaleform movie 
            //if it isn't already loaded. Once loaded the b_Allow_Drawn_Or_Remove_to_Check_For_Update = TRUE flag state will allow it to create the new signifiers 
            //as required. The flag is required so Check_If_Signifier_Movie_Needs_Drawn_or_Removed() cannot clear the NEEDS_UPDATED without running through this section first.



            IF g_Current_Number_Of_Signifiers = 0  //If we are still at zero then nothing needs to be drawn and Check_If_Signifier_Movie_Needs_Drawn_or_Removed()
            //should remove the movie. This means it is safe to clear this update flag.

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                #if IS_DEBUG_BUILD

                    cdPrintString("CELLPHONE_CONT - No signifiers need drawn. Clearing Sig Update Flag 1")
                    cdPrintnl()

                #endif

                b_Allow_Drawn_Or_Remove_to_Check_For_Update = FALSE //Reset this if no signifiers need drawn.

            ELSE

                b_Allow_Drawn_Or_Remove_to_Check_For_Update = TRUE // Allows Check_If_Signifier_Movie_Needs_Drawn_or_Removed() to perform a clear all and add new items.

            ENDIF


        ENDIF
  











        //Main check for player use d-pad to bring up phone...
        IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()
     
            IF NOT IS_WARNING_MESSAGE_ACTIVE() //Fix for #1298444
                IF g_Cellphone.PhoneDS = PDS_AWAY
                    IF IS_PLAYER_PLAYING (PLAYER_ID())
                        IF IS_PLAYER_CONTROL_ON (PLAYER_ID())
                        OR CAN_USE_MOBILE_PHONE_DURING_CUTSCENE()


                                   

                        //Put in control check here...  241184

                            IF NOT IS_PED_INJURED (PLAYER_PED_ID())  //Don't allow the phone to be taken out if the player is injured.


                                IF g_bInMultiplayer

                                    Check_User_Altering_PhoneDisplayStatus()

                                ELSE
                                    //IF NOT IS_PLAYER_CLIMBING (PLAYER_ID()) Trial Removal at the request of NY bug 843727
                                 
                                        Check_User_Altering_PhoneDisplayStatus()

                                    //ENDIF
                                ENDIF


                            ENDIF
                            
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF

            //If the phone is playing interference, the user taking the phone out will automatically abort the interference phase and beging ringing.
            IF g_Cellphone.PhoneDS = PDS_TAKEOUT
                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
                    Check_User_Altering_PhoneDisplayStatus()
                ENDIF
            ENDIF





            //Critical!
            Cellphone_Draw_Status()
        
  
         ELSE

            IF Check_For_Takeout_Only()

                #if IS_DEBUG_BUILD

                    PRINTSTRING ("CELLPHONE_CONTROLLER - User made attempt to take out phone but it is disabled permanently or this frame only.")
                    PRINTNL()
                    

                    cdPrintstring ("CELLPHONE_CONTROLLER - User made attempt to take out phone but it is disabled permanently or this frame only.")
                    cdPrintnl()


                    g_DisplayNewSideTaskSignifier = TRUE //Setting this once so DISABLE_CELLPHONE_THIS_FRAME_ONLY() in cellphone_public is signalled to dump out the calling script once.
                                                         //Cellphone_public will reset this to FALSE to prevent spam. We should see one notification per user attempt.
                #endif

            ENDIF

         ENDIF










    
        //Picture Taking, Saving and Loading master switch statement. Needs to be independent of the phone's state.
        SWITCH g_Cell_Pic_Stage


            CASE PIC_STAGE_IDLE

                Check_for_Picture_Just_Taken()

                IF g_b_appHasRequestedTidyUp = TRUE
                    
                    Tidy_Up_Pic_Taking()

                ENDIF

            BREAK


            CASE PIC_SHUTTER_DELAY

                Shutter_Delay()

            BREAK


            CASE PIC_STAGE_HQ_BEGIN

                Begin_Take_HQ_Pic()

            BREAK


            CASE PIC_STAGE_HQ_ONGOING

                Taking_HQ_Pic()

            BREAK


            CASE PIC_STAGE_LQ_BEGIN

                Begin_Take_LQ_Pic()

            BREAK


            CASE PIC_STAGE_LQ_ONGOING

                Creating_LQ_Copy()

            BREAK


            CASE PIC_STAGE_TIDY_UP

                Tidy_Up_Pic_Taking()
            
            BREAK


            CASE PIC_STAGE_HOLDING_LQ_COPY

                IF g_b_appHasRequestedTidyUp = TRUE
                    
                     Tidy_Up_Pic_Taking()

                ENDIF  

                IF g_b_appHasRequestedSave = TRUE

                    /*
                    IF IS_XBOX360_VERSION() //Need to check storage on 360 and PC
                    OR IS_PC_VERSION()
                    OR IS_PS3_VERSION()
                    
                        CLEAR_STATUS_OF_SORTED_LIST_OPERATION()

                        IF QUEUE_OPERATION_TO_CREATE_SORTED_LIST_OF_PHOTOS (TRUE) //The last BOOL indicates that we are checking for a SAVE. This would be false if we were loading.

                            g_Cell_pic_Stage = PIC_CHECK_STORAGE
                        
                            #if IS_DEBUG_BUILD

                                cdPrintString("CELLPHONE_CONT - 360 PC PS3Queue Operation success")
                                cdPrintnl()

                            #endif

  
                        ELSE
                          
                            #if IS_DEBUG_BUILD

                                cdPrintString("CELLPHONE_CONT - 360 PC PS3 Queue Operation failed - tidying up pic.")
                                cdPrintnl()

                            #endif


                            Tidy_Up_Pic_Taking()

                        ENDIF

                    
                    ELSE
                    */
                    
                        g_Cell_pic_Stage = PIC_STAGE_STORE_TO_MEDIA

                    //ENDIF

                    g_b_appHasRequestedSave = FALSE

                ENDIF

                              
            BREAK




            CASE PIC_CHECK_STORAGE

                Check_Storage()

            BREAK


            CASE PIC_STAGE_STORE_TO_MEDIA

                Begin_Store_to_Media()

            BREAK


            CASE PIC_STAGE_STORE_TO_MEDIA_ONGOING

                Ongoing_Store_to_Media()

            BREAK



            CASE PIC_STAGE_POST_SAVE_PAUSE

                PostSavePause()

            BREAK


            CASE PIC_STAGE_LOAD_FROM_MEDIA

                Load_From_Media()

            BREAK
            


            CASE PIC_STAGE_LOAD_FROM_MEDIA_ONGOING

                Ongoing_Load_From_Media()

            BREAK





        ENDSWITCH



         
       
        

        //Handle reinstatement of console cinematic cam... See bug 605548
        //Cinematic_Cam_Handler()
        
        

        

       





        #if IS_DEBUG_BUILD

            Maintain_Cellphone_Debug()



  

            //SET_BIT(iButtonBits, 3) //ESB_OK
            //SET_BIT(iButtonBits, ENUM_TO_INT(ESB_NO))

            //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_FWARN1", iButtonBits, "CELL_CAM_FWARN2", FALSE, -1, "", "", FALSE)



            

            //SET_BIT (iButtonBits, 3 )
            //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_SCW_1", iButtonBits, "CELL_CAM_SCW_2", FALSE, -1, "", "", FALSE)





            /*
            IF PendingTextDataAvailable[0] = TRUE

                cdPrintnl()
                cdPrintstring ("Michael has pending text.")
                cdPrintnl()   
            ENDIF
            

            IF PendingTextDataAvailable[1] = TRUE

                cdPrintnl()
                cdPrintstring ("          Franklin has pending text.")
                cdPrintnl()   
            ENDIF


            IF PendingTextDataAvailable[2] = TRUE

                cdPrintnl()
                cdPrintstring ("                  Trevor has pending text.")
                cdPrintnl()   
            ENDIF
            */
            
            
            //draw_debug_rectangle()
            //draw_debug_screen_grid()
            //debug_3dphone_pos_rot()
            //draw_debug_Scaleform_Signifier_Position()


            //Add bool widget that if ticked enables these two lines....
            IF DebugPositionToggle
                SET_MOBILE_PHONE_POSITION (<<GET_SAFE_ZONE_SIZE() * g_Test3dPhonePosVec.x, GET_SAFE_ZONE_SIZE() * g_Test3dPhonePosVec.y, g_Test3dPhonePosVec.z >>)
                SET_MOBILE_PHONE_ROTATION (g_3dPhoneDebugRotVec)
            ENDIF

            



            //DISPLAY_TEXT_WITH_FLOAT (0.3, 0.5, g_txtSP_CurrentSelfie_anim, db_preview_signifier_posX, 3)




            /*
            IF IS_CALLING_CONTACT(CHAR_MICHAEL)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_MICHAEL, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "MIKE STORY CALL")

            ENDIF
            

            IF IS_CALLING_CONTACT(CHAR_FRANKLIN)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_FRANKLIN, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "FRANK STORY CALL")

            ENDIF



            IF IS_CALLING_CONTACT(CHAR_TREVOR)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TREVOR, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "TREV_CALL")

            ENDIF
            */


            /*
            IF IS_PHONE_ONSCREEN (FALSE) 

                  DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Phone onscreen")

            ENDIF
            


            IF IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.44, "STRING", "Phone active or overlapping.")

            ENDIF 
            */

            /*
            IF Is_Phone_Control_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))
                
               cdPrintstring("left")
               cdPrintnl()
               cdPrintnl()

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Phone nav left ")

            ENDIF
            */

            /*
            IF HAS_DYNAMIC_BRANCH_BEEN_SET()

                      DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Branch Set")
            ENDIF
            */

            /*
            IF HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
                
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Forced Away")
            
            ENDIF
            */

            /*
            IF IS_CELLPHONE_CAMERA_IN_USE()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Cellphone Camera in use.")
            
            ENDIF
            */

            /*
            IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Last call interrupted.")
            
            ENDIF
            */

            /*
            IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
 

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Taken Pic and Held Test.")

            ENDIF   
            */
            

            /*
            IF IS_CELLPHONE_ON_HOMESCREEN()

                     DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "On Homescreen.")
            
            ENDIF
            */

            /*
            IF IS_PAUSE_MENU_ACTIVE()
            

                 DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "PAUSEMENU.")


            ENDIF
            */

            /*
            
            IF WAS_LAST_PHONECALL_HUNG_UP_BY_JOYPAD()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "Hung Up Joypad")

            ENDIF


            IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.44, "STRING", "Interrupted")

            ENDIF



            IF CHECK_CELLPHONE_LAST_CALL_REJECTED()
                
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Rejected")

            ENDIF

            */


            //testcp = GET_LAST_PASSED_STANDARD_CONVERSATION_ROOT()

            //DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.77, "STRING", testcp)


            /*
            IF IS_CALLING_CONTACT (CHAR_TOW_TONYA)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TOW_TONYA, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
            
            ENDIF
            */

            /* 
            IF CHECK_CELLPHONE_LAST_CALL_REJECTED()
                
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Rejected")

            ENDIF



            
            IF IS_CALLING_FRIEND_FOR_ACTIVITY_CANCELLATION (CHAR_FRANKLIN)
                
                //SET_ACTIVITY_CANCELLED_WITH_THIS_FRIEND_FOR_PHONE (CHAR_MICHAEL, CHAR_FRANKLIN)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.44, "STRING", "FRANK CANCEL REQ. CALL")

            ENDIF


            IF IS_CALLING_FRIEND_FOR_ACTIVITY (CHAR_FRANKLIN)

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "FRANK ACTIVITY CALL")
                                                                        
            ENDIF

            
            IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION (CHAR_FRANKLIN)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "FRANK SECONDARY CALL")

            ENDIF

            
            
            IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION (CHAR_LESTER)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "LESTER SECONDARY CALL")

            ENDIF




            IF IS_CALLING_CONTACT(CHAR_MICHAEL)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_MICHAEL, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "MIKE STORY CALL")

            ENDIF



            IF IS_CALLING_FRIEND_FOR_ACTIVITY_CANCELLATION (CHAR_MICHAEL)
                
                //SET_ACTIVITY_CANCELLED_WITH_THIS_FRIEND_FOR_PHONE (CHAR_MICHAEL, CHAR_FRANKLIN)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.44, "STRING", "MIKE CANCEL REQ. CALL")

            ENDIF


            IF IS_CALLING_FRIEND_FOR_ACTIVITY (CHAR_MICHAEL)

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.24, "STRING", "MIKE ACTIVITY CALL")
                                                                        
            ENDIF


            IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION (CHAR_MICHAEL)

               
               IF TestInt = 0
                    
                    ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
                    ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "FRANKLIN")

                    PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_MICHAEL, "JHAUD", "JH_PLAN", CONV_PRIORITY_VERY_HIGH)
                
                    TestInt ++
                
                ENDIF
                

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "MIKE SECONDARY CALL")

            ENDIF


            
            IF IS_CALLING_CONTACT (CHAR_AMANDA)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "AMANDA STORY CALL")

            ENDIF
            */
            



            /*
            IF IS_PHONE_ONSCREEN()

                FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_DETONATEPHONE)

            ENDIF


            
            IF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST (CHAR_DETONATEPHONE)


                DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.64, "STRING", "DETONATE IS CONTACT SELECTED")


            ENDIF
            */
            

            /*
            IF IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.74, "STRING", "Active and overlapping")

            ENDIF


            IF IS_PHONE_ONSCREEN()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.64, "STRING", "Onscreen")

            ENDIF
            */


            /*
            IF NETWORK_IS_CLOUD_AVAILABLE() //cloud available

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "CLOUD AVAILABLE")
    
            ENDIF
            */

            /*
            IF g_ConversationData.g_AddToBriefScreen

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.64, "STRING", "Adding brief")

            ELSE
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.15, 0.64, "STRING", "Not Adding to brief")


            ENDIF
            */

            /*
            IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
            
                 DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "In Selfie Mode.")
    
            ENDIF
            */


            
            /*
            IF IS_PAUSE_MENU_ACTIVE()
                    
                cdPrintstring("Testing pause menu....")
                cdPrintnl()
                cdPrintstring("active")
                cdPrintnl()

            ENDIF
            */


            /*
            IF g_Use_Prologue_Cellphone


                DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov, 255,255,255,255)

            ENDIF
            */



            /*
            IF IS_CALLING_CONTACT(CHAR_CHEF)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_BLIMP, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
            
            ENDIF


            IF IS_CALLING_CONTACT(CHAR_BLIMP)

             
                            PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES(MyLocalPedStruct, CHAR_BLIMP,"FpFLAU",
                                            "FpFL_G1", "FpFL_G1",
                                            "FpFL_G2", "FpFL_G2", CONV_PRIORITY_CELLPHONE)
            
            ENDIF


            IF IS_CALLING_CONTACT(CHAR_MICHAEL)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_MICHAEL, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
            
            ENDIF



            IF IS_CALLING_CONTACT(CHAR_TREVOR)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_TREVOR, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
            
            ENDIF
            
            */

            /*
            IF g_ConversationStatus = CONV_STATE_PLAYING
                IF IS_SCRIPTED_CONVERSATION_ONGOING()

                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.84, "STRING", "conv_state_playing")

                ENDIF
            ENDIF
            */


            /*
            IF IS_CALLING_CONTACT(CHAR_LESTER)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "MIKE STORY CALL")

            ENDIF


            IF IS_CALLING_CONTACT(CHAR_TRACEY)

                PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_MICHAEL, "FAM3AUD", "FAM3_MST1", CONV_PRIORITY_VERY_HIGH)
        
            ENDIF
            */

            /*

                ENUM enumTxtMsgIsReplyRequired //specifies whether this text message will allow the player to reply with yes / no... a "replay mission" txt for example.

                    NO_REPLY_REQUIRED,
                    REPLY_IS_REQUIRED, 
                    REPLIED_BARTER,
                    REPLIED_YES,
                    REPLIED_NO

                ENDENUM

            */

            /*
            IF GET_TEXT_MESSAGE_REPLY_STATUS("CELL_2001") = REPLIED_YES

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "REPLIED YES")  

            ENDIF

            
            IF GET_TEXT_MESSAGE_REPLY_STATUS("CELL_2001") = REPLIED_NO

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "REPLIED NO")   

            ENDIF


            IF GET_TEXT_MESSAGE_REPLY_STATUS("CELL_2001") = REPLY_IS_REQUIRED

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "REPLIED PENDING")  

            ENDIF
            */



        
            //IF TestBorderTexture = TRUE
                /*
                IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
                    //DRAW_SPRITE("MPRankBadge", "Globe_BG", 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
                    DRAW_SPRITE("MPLeaderboard", "Leaderboard_SocialClub_Icon", 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
                ENDIF
                */
            //ENDIF


            /*
            IF g_Holder_ForceDLC_DialogueRequest = TRUE

               DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "FDR is TRUE") 

            ENDIF
            */












            /*
            IF bCreatingEmailMessage = TRUE

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.33, "STRING", "Keyboard.")

            ENDIF
            */



            










            /*
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT)) 

                  #if IS_DEBUG_BUILD
                    cdPrintString("Setting Creation via JP to TRUE")
                    cdPrintnl()
                #endif

                bCreatingEmailMessage = TRUE
            
            ENDIF
            */

            /*
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)

                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "FullV bit set") 

            ENDIF


            
            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.74, "STRING", "FMU bit set")

            ENDIF
            


            IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
                
                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.74, "STRING", "First Person")

            ENDIF

            */
          
            //Temp
            /*
            #if IS_DEBUG_BUILD
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP )
                
                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.60, "CELL_500", g_v_ExtractionBodyguardVec.x, 1)
                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.68, "CELL_501", g_v_ExtractionBodyguardVec.y, 1)
                DISPLAY_TEXT_WITH_FLOAT (0.75, 0.76, "CELL_502", g_v_ExtractionBodyguardVec.z, 1)

                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.60, "CELL_500", g_v_ExtractionPresidentVec.x, 1)
                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.68, "CELL_501", g_v_ExtractionPresidentVec.y, 1)
                DISPLAY_TEXT_WITH_FLOAT (0.85, 0.76, "CELL_502", g_v_ExtractionPresidentVec.z, 1)

            ENDIF
            #endif
            */


            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_0)

                
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    /*
                    IF g_b_InstructEyefindtoSendAutoLongMessage = TRUE

                        g_b_InstructEyefindtoSendAutoLongMessage = FALSE
                    
                    ELSE

                        g_b_InstructEyefindtoSendAutoLongMessage = TRUE
                    
                    ENDIF
                    */
                    //v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                    
                    //Fleeca Bank
                    //SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2964.0, 481.0, 15.7>>) 
                    
                    //SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-169.1, -917.1, 30.0>>)
                    
                    
                    //<<2155.5, 3712.1, 38.9>>) 
                    //<<2101.5, 4595.1, 36.0>>)
                    //<<3442.8, 5174.0, 7.0>>)//<<2155.5, 3712.1, 38.9>>)
                    //CREATE_MOBILE_PHONE (MOBILE_1)


                    //2. App testing block in Dummy App position for MP.
                    
                    //SET_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)


                    /*
                    IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)

                        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)

                    ELSE

                        SET_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)

                    ENDIF
                    */


        

                    /*3
                    IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)

                        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)

                    ELSE

                        SET_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP)

                    ENDIF
                    */
                    

                    /*4
                    IF g_bEnableHackingApp = FALSE
                        g_bEnableHackingApp = TRUE 
                    ELSE
                        g_bEnableHackingApp = FALSE
                    ENDIF 
                    */

                    //End of app testing block.

                    




                    //ADD_CONTACT_TO_PHONEBOOK (CHAR_BENNYS_OMW, MULTIPLAYER_BOOK)


                    //IF g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
                        //g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = TRUE
                    //ELSE
                        //g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
                    //ENDIF



                     //PLAY_SOUND_FRONTEND (-1, "Select_Placed_Prop", "DLC_Dmod_Prop_Editor_Sounds", FALSE)


             

             
                    //WAIT(10000)
                    //DESTROY_MOBILE_PHONE()

                    //CLEAR_AREA_OF_VEHICLES (v_Player_pos, 100.0)

                ENDIF
                

                    

                //LAUNCH_FINALE_CHOICE_CONTACT_LIST()

                /*
                MODEL_NAMES ModelToRestore

                INT RestorationIndex = 1

                ModelToRestore = INT_TO_ENUM(MODEL_NAMES, PlacedProps[RestorationIndex].PropModelHash)



                REQUEST_MODEL(ModelToRestore)
                WHILE NOT HAS_MODEL_LOADED(ModelToRestore)
                    WAIT(0)
                    REQUEST_MODEL(ModelToRestore)
                ENDWHILE

                //Would only save and restore those positions with PROP_POSITION_FILLED

                PlacedProps[RestorationIndex].PropEntityIndex = CREATE_OBJECT(ModelToRestore, PlacedProps[RestorationIndex].PropPlacementVector)


                SET_ENTITY_COLLISION (PlacedProps[RestorationIndex].PropEntityIndex, TRUE)



                APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PlacedProps[RestorationIndex].PropEntityIndex, APPLY_TYPE_FORCE, <<0,0,-60>>, 0, FALSE, TRUE) //Probably going to have to freeze objects :(
                
                

                


                SET_MODEL_AS_NO_LONGER_NEEDED (ModelToRestore)
                */

      


                /*
                PlacedProps[i_TotalNumberOfPlacedModels].PropModelHash = SelectablePropArray[i_CurrentModelIndex].PropModelHash
                PlacedProps[i_TotalNumberOfPlacedModels].PropRotation = GET_ENTITY_ROTATION (TestPropEntity)
                PlacedProps[i_TotalNumberOfPlacedModels].PropPlacementVector = GET_ENTITY_COORDS (TestPropEntity)
                PlacedProps[i_TotalNumberOfPlacedModels].PositionStatus = PROP_POSITION_FILLED
                */









                /*
                SET_MOBILE_PHONE_DOF_STATE(FALSE)
                */
   
                //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE)


                //g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE

                //g_bInMultiplayer = TRUE
                //g_bEnableHackingApp = TRUE 


                //g_forceDisplayPlayerInvitesIndicator = TRUE

                //g_numNewJoblistInvites --

                //LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
                //BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)



                //g_3DPhoneNeedsMovedUp = TRUE
                
                //CELL_CAM_ACTIVATE (TRUE, TRUE)


                //CELL_HORIZONTAL_MODE_TOGGLE (TRUE)                
                //TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), FALSE) 

                //HIDE_PHONE_IN_FIRST_PERSON_EXPERIMENTAL()

                //CELL_HORIZONTAL_MODE_TOGGLE (TRUE)

                //CELL_SET_INPUT (CELL_INPUT_LEFT)

                //ENABLE_MULTIPLAYER_TRACKIFY_APP (TRUE)
                //SET_TRACKIFY_TARGET_VECTOR (<<-1244.1, -182.8, 39.5>>, TRUE)


                /* Trackify tests for Gareth.
                REMOVE_TRACKIFY_MULTIPLE_TARGET(3)
        
                SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (0, <<1850.1, 3325.0, 45.3>>) //Senora National Park.
                
                SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (1, <<1565.5, 2858.3, 54.7>>)  //America Americans sign.

                SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (2, <<206.5, 3347.3, 42.5>>) //Blue Uranium sign.
                */
                
                
                //Error: <Dependency 0> clip: Cannot retrieve a clip from a variable clip set when the clip does not exist in clip set! (clipSetVarId: default 3839837909, clipSetId: ANIM_GROUP_CELLPHONE 383648904, clipId: CELLPHONE_HORIZONTAL_INTRO 1822849418, network: TaskMobilePhone)
               
                //SET_NIGHTVISION(TRUE)
                
                //LAUNCH_TRACKIFY_IMMEDIATELY()

                
                      /*
                      SHOW_ONSCREEN_KEYBOARD_WITH_LONG_INITIAL_STRING ( "Pish",

                        EmailMessageSegments[0],
                        EmailMessageSegments[1],
                        EmailMessageSegments[2],
                        EmailMessageSegments[3]
                                       )
                                       */
                //#if IS_DEBUG_BUILD
                    //cdPrintString("Setting Creation to TRUE")
                    //cdPrintnl()
                //#endif

                //bCreatingEmailMessage = TRUE


                //HANG_UP_AND_PUT_AWAY_PHONE (TRUE)



                //PRINT_HELP ("HUD_AGE_C")

                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_STNJMP)

                //RENDER_SCRIPT_CAMS(FALSE, FALSE)
                //SET_CAM_ACTIVE (TestCam, FALSE)

                
                //DESTROY_ALL_CAMS()

                //PHONEPHOTOEDITOR_TOGGLE (TRUE)                                                                                 

                //IF HAS_ANIM_DICT_LOADED("facials@p_m_zero@base")

                    //SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(),   "dead_1", "facials@p_m_zero@base")


                    //CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

                /*
                IF DOES_ENTITY_EXIST (PLAYER_PED_ID())

                     Make_Ped_Drunk(PLAYER_PED_ID(), 60000)
                    
                ENDIF
                */


                //g_QuickSaveDisabledByScript = FALSE


                    //SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), s_Facial[currentanim])


                    //currentanim ++

                    //IF currentanim > 11 
                        //currentanim = 0
                    //ENDIF



                    //TASK_PLAY_ANIM(PLAYER_PED_ID(), "facials@p_m_zero@base", "dead_1", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)

                    //TASK_PLAY_ANIM(PLAYER_PED_ID(), "facials@p_m_zero@base", "dead_1", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_ADDITIVE | AF_SECONDARY | AF_LOOPING)
                    //CLEAR_FACIAL_IDLE_ANIM_OVERRIDE

                //ENDIF   

                //SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-71.3631, -527.6561, 30.8387>>) //Tunnel entrance.
                
                //IF IS_DLC_PRESENT(HASH("mpChristmas")) //Check for DLC.. If unavailable, remove border / filter swapping.

                    //script_assert("Christmas is PRESENT!")

                //ENDIF
                //REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
                //TestBorderTexture = TRUE
                                                      //"MPOverview", "MP_CharCard_Stats_Icons4",
                
           
                //ANIMPOSTFX_PLAY("MP_Celeb_Win", 0, TRUE)

                //ANIMPOSTFX_PLAY("DMT_flight", 0, TRUE)

                //ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)

                //ANIMPOSTFX_PLAY("MP_Celeb_Lose", 0, TRUE)

                //STRING s_CurrentFilter[4]



                //Temp Filter Test work

                //IF i_NumberOfFiltersAvailable = 0

                /*
                ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

                g_i_CurrentlySelected_SnapMaticFilter ++

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Filter Browse Down - g_i_CurrentlySelected_SnapMaticFilter at ")
                    cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                    cdPrintnl()

                #endif

                IF g_i_CurrentlySelected_SnapMaticFilter = i_NumberOfFiltersAvailable 

                    #if IS_DEBUG_BUILD

                        cdPrintString("AppCamera - Filter Browse Down - g_i_CurrentlySelected_SnapMaticFilter will be set to zero as it matched i_NumberOfFiltersAvailable at  ")
                        cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                        cdPrintnl()

                    #endif

                    g_i_CurrentlySelected_SnapMaticFilter = 0

                ENDIF


                PRINT_HELP("CELL_360_PRIV")

                IF g_i_CurrentlySelected_SnapMaticFilter = 0 //No Filter in position 0 of filter array!

                    ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

                ELSE

                    ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)

                ENDIF
                */
                
                
                                                            /*
                                                            STRING fromGamertag = "Rockstar"


                                                            IF ARE_STRINGS_EQUAL (fromGamerTag, "Rockstar")

                                                                TEXT_LABEL_63 ColourAdjustedTag

                                                                ColourAdjustedTag = "~HUD_COLOUR_SOCIAL_CLUB~"
                                                                ColourAdjustedTag += fromGamerTag
                                                                                                                               
                                                                PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("PIM_TIGC0", ColourAdjustedTag, ROUND(99.0), HUD_COLOUR_PURE_WHITE, FALSE)   // ~a~ ~s~gave you ~g~$~1~
                                                                
                                                            ELSE

                                                                PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("PIM_TIGC0", fromGamerTag, ROUND(45.0), HUD_COLOUR_PURE_WHITE, FALSE)   // ~a~ ~s~gave you ~g~$~1~
                                                            
                                                            ENDIF
                                                            */


                //SC_INBOX_SEND_AWARD_MSG (SC_Send_AllFriends,  "Test Award", 599.9)

                //PRINT_HELP ("CELL_HF_HIDE")
                //PRINT_HELP ("CELL_HF_VIEW")

                
                //Steve T. 1563993
                //BEGIN_TEXT_COMMAND_THEFEED_POST ("SC_PLAY_FJQL")

                    
                    /* Alternatively
                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
               
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += "Social Club"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"

                    ND_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                    */


                //END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
               


                //END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)






        
                //cdPrintstring ("Testing title update AUG - NEW 21")
                //cdPrintnl()
                //cdPrintnl()
                
                //NETWORK_SESSION_VOICE_HOST() causes assert! in SP!


            //PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING_AND_DURATION_MODIFIER ("CELL_101", "Gamertag1", 82950, "CELL_102", TRUE, 1.0)
            //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 1, NULL, "LAMAR")

            //CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,"FM_1AU","FM_RCM"," FM_RCM_3",CONV_PRIORITY_MEDIUM)


            //FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING_DURATION_MODIFIER(STRING TextLabel, STRING PlayerName, INT iTimeInMS, STRING InsertedTextLabel, BOOL bUseSocialClubColour, FLOAT DurationModifier = 1.0))

        
                //CREATE_CONVERSATION (MyLocalPedStruct, "FAM3AUD", "FAM3_MSTfgh1", CONV_PRIORITY_MEDIUM)
                //SET_TIMECYCLE_MODIFIER("drunk") //#1431423

                //SET_PED_IS_DRUNK (PLAYER_PED_ID(),  FALSE)
                
                //SET_TIMECYCLE_MODIFIER("stoned_aliens")
                //ANIMPOSTFX_PLAY("PhoneCameraIn", 0, FALSE)

                //DISABLE_CELLPHONE (TRUE)
                //BEGIN_EMAIL_THREAD_INT(ENUM_TO_INT(DRFRIEDLANDER_EMAIL_A1))

                //SET_CONTACT_AS_MISSED_CALLER (CHAR_RON)

                
                //Phone task test
                /*
                IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927
                                                                                                                                                    
        

                            #if IS_DEBUG_BUILD
                                cdPrintstring ("CELLPHONE_PRIV - Phone put up to ear. Bit set.")
                                cdPrintnl()
                            #endif


                            IF g_bInMultiplayer
                                SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, FALSE)
                                SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
                                SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
                            ENDIF


                            SET_BIT (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)

                                                                                                                                                    
                            TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), TRUE)

            

                ENDIF
                //Phone task test
                */


                //REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (TRUE) 

                //BEGIN_PRELOADED_CONVERSATION()

                //LAUNCH_FINALE_CHOICE_CONTACT_LIST()

                //SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START)

                //PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_CAR, 500.0)

                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_SONCOLL)

                        //SET_SOCIAL_CLUB_TOUR("Gallery")
                        //OPEN_SOCIAL_CLUB_MENU()

                //BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL (TRUE)


                //SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

    

                //BEGIN_PRELOADED_CONVERSATION()


                //SET_TRACKIFY_TARGET_VECTOR (<<3424.1, 654.0, -106.0>>, TRUE, TRUE)


                //FLUSH_TEXT_MESSAGE_FEED_ENTRIES()

                //HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

                //STOP_SOUND (TestRingtone)

      
                //RELEASE_SOUND_ID (TestRingtone)


                //PRINT_HELP_FOREVER("CELL_295")

                //PAUSE_FACE_TO_FACE_CONVERSATION (FALSE)

                //PAUSE_CELLPHONE_CONVERSATION (FALSE)


          
                //PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_DEATHS, 25, 67)

           
                //PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_DEATHS, 50)





                /*
                IF IS_PHONE_ONSCREEN()
                    FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_RON)
                ENDIF
                */

                  /*
                  mov = REQUEST_SCALEFORM_MOVIE("LETTER_SCRAPS")
                  WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
                        WAIT(0)
                  ENDWHILE
                  
                  BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_LETTER_TEXT") 
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_121") 
                  END_SCALEFORM_MOVIE_METHOD()
                  */


                

            ENDIF
            
            /*
            IF NETWORK_IS_CLOUD_AVAILABLE()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Network has Cloud.")
                //PRINTSTRING ("Script Test 1. Network has Social Club Account returns TRUE.")
                //PRINTNL()
            
            ENDIF
            
       

            IF NETWORK_IS_SIGNED_ONLINE()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "Network Signed Online.")
                //PRINTSTRING ("Script Test 2................Network is signed online returns TRUE")
                //PRINTNL()

            ENDIF
            

            

            IF NETWORK_IS_SIGNED_IN()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Network Signed In.")
                //PRINTSTRING ("Script Test 2................Network is signed online returns TRUE")
                //PRINTNL()

            ENDIF
            */


            /*
            IF IS_CELLPHONE_CONVERSATION_PLAYING()
  
                PAUSE_CELLPHONE_CONVERSATION(TRUE)

            ENDIF
            */

            /*
            IF IS_CONTACT_IN_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "911 Listed")

            ENDIF
            */


            /*            
            IF NOT (IS_MINIMAP_RENDERING())

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "MINIMAP HIDDEN") 
                               
            ELSE

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "MINIMAP RENDERED")

            ENDIF
            */



        
            /*
            IF GET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1") = MPE_REPLIED_YES

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Replied YES") 
                
                SET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1", MPE_NO_REPLY_REQUIRED)
                 
            ENDIF




            
            IF GET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1") = MPE_REPLIED_NO

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Replied NO")   

            ENDIF


            IF GET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1") = MPE_REPLY_IS_REQUIRED

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Reply PENDING")  

            ENDIF



            IF HAS_THIS_TEXT_MESSAGE_BEEN_READ ("JOSH1_TXT")

            ENDIF


            IF CHECK_FOR_MP_EMAIL_PRESENCE_IN_CHARACTER_INBOX("CELL_EMAIL1")

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Email 1 is present")

            ENDIF
            */

            /*
            IF g_bUse_MP_DLC_Dialogue = TRUE

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "g_bUse_MP_DLC_Dial")
                
            ENDIF   
            */

            /*
            IF HAS_THIS_TEXT_MESSAGE_BEEN_READ ("JOSH1_TXT")

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Josh1 has been read")

            ENDIF
            */


            /*
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "TU_bit new email")

            ENDIF
            */
      
            /*
            IF g_DumpDisableEveryFrameCaller

                PRINTSTRING ("CELLPHONE_CONT Showing returned LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID... ")
                PRINTINT(THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID()) //For Esteban.
                PRINTNL()

                IF GET_IS_HIDEF()
                    PRINTSTRING("HIGH Definition ")
                ELSE
                    PRINTSTRING(" STANDARD Definition ")
                ENDIF

                PRINTNL()

            ENDIF
            */

            /*
            IF IS_PED_DUCKING (PLAYER_PED_ID())

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "PH_PLAYER_DUCK")   

            ENDIF
            */           


            /*
            IF IS_RADAR_HIDDEN() //Useless. Doesn't check for a "This frame" hide.

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "RADAR HIDDEN")  

            ENDIF
            */


            /*  Potential fix for NG?
            IF NOT (IS_MINIMAP_RENDERING())

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "MINIMAP HIDDEN") 
                
                
            ELSE

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "MINIMAP RENDERED")

            ENDIF
            */           

       
            //IF g_i_CurrentlySelected_SnapMaticBorder > 0

                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Border Active")
            
            //ENDIF


            //v_3dDesiredPosition = <<1.5, 0.0, -17.0>>

            /*
            GET_MOBILE_PHONE_POSITION(g_Test3dPhonePosVec)

            IF g_Test3dPhonePosVec.x = 1.5
            AND g_Test3dPhonePosVec.z = -17.0
            //OR g_Phone_Active_but_Hidden

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Phone fullscreen")
                cdPrintnl()
                cdPrintstring ("Rowan Test")
                cdPrintnl()

            ENDIF
                

            

            GET_MOBILE_PHONE_POSITION(g_Test3dPhonePosVec)
            */
            




            //IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO)

                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Cam READY")

            //ENDIF




            //IF g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY
            //OR g_Cell_Pic_Stage > PIC_STAGE_HOLDING_LQ_COPY

            /*
            IF IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE()

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Not in viewfinder mode")
                
            ENDIF   



            IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
            OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_BEHIND)

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "Looking Behind.")

            ENDIF
            */






            /*
            IF g_ConversationStatus = CONV_STATE_WAITING_ON_PLAYER_ACCREJ

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "ACC_OR_REJ")

            ENDIF
            */

            /*
            IF IS_ENTITY_IN_WATER (PLAYER_PED_ID())
             
                    #if IS_DEBUG_BUILD
                    
                        //cdPrintstring("Submerged level ")
                        //DISPLAY_TEXT_WITH_FLOAT (0.3, 0.15, "CELL_500", GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()), 3)

                    #endif  

            ENDIF
            */


            /*
            IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_LEFT)
                        
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("Cellphone LEFT pressed.")
                    cdPrintnl()

                #endif
    
            ENDIF



            IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT)
                        
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("Cellphone RIGHT pressed.")
                    cdPrintnl()

                #endif
    
            ENDIF
            */




            /*      
            IF GET_PED_STEALTH_MOVEMENT (PLAYER_PED_ID())

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "PH_PLAYER_STEALTH")  
                
                IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
                    
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Looking behind")                    
                
                ENDIF    

                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)


            ENDIF
            */


            //DISPLAY_TEXT_WITH_NUMBER (0.20, 0.45, "CELL_500", g_numNewJoblistInvites)
            /*
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("ob_telescope")) > 0

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "Telescope") 

            ENDIF


            IF MPGlobalsAmbience.bUsingTelescope = TRUE

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.64, "STRING", "Tele in USE") 
    
            ENDIF
            */
            


            /*
            IF g_DumpDisableEveryFrameCaller
                IF IS_CALLING_CONTACT (CHAR_MP_MORS_MUTUAL)

                //PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (YourPedStruct, WhichChar, answerPhoneBlock, WhichRootLabel, PassedConversationPriority, ShouldPhoneBeForcedOnScreen, ShouldDisplaySubtitles, ShouldAddToBriefScreen, IsThisInboundForCellphone)
                
                PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (MyLocalPedStruct, CHAR_MP_MORS_MUTUAL, "FM_1AU", "FM_PLANETAKE", CONV_PRIORITY_VERY_HIGH)

                ENDIF
            ENDIF    
            
            
            
            IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

            
                        */   

            /*
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.60, "STRING", "Ped in vehicle")

            ENDIF
            */
 








       
            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_9) 
            

                //Director Mode Prop placement notes.


                //! Data that needs to be saved for each prop:
                //hash of model name
                //Rotation
                //Placement Vector


                //! Prop Sheet that's referenced when placing props:
                //hash of model name
                //Label Reference
                //Category enum
                //Adjustment Z value for height
                



                //Need to switch off everything! Cops, peds, vehicles, etc!
                //Disable jumping?
                //Need ragdoll / death checks / model quota checks.

                //Entity Collision check
                //Dynamic physics
                //Forcing drop on the floor
                //Draw placement disk
                //Disable punch / jump / sprint buttons





                //When object is placed. Freeze it and load collision?


                //NATIVE PROC SET_ENTITY_COLLISION(ENTITY_INDEX EntityIndex, BOOL UsesCollisionsFlag, BOOL KeepDisabledSimulatingFlag = FALSE) = "0xf66f820909453b8c"
                
                //NATIVE PROC SET_FORCE_OBJECT_THIS_FRAME(VECTOR pos, FLOAT radius) = "0xa65f6ad6aef297cf"

                //FUNC BOOL IS_AREA_AROUND_CORONA_CLEAR


                //Each prop needs data associated with it. Model name, label name, size type ( for placement disc purposes )



                /*
                PROC INIT_PLACEMENT_PARTICLE_FX(BOOL bOnWater, VECTOR vParticlePos)
    
                if HAS_PTFX_ASSET_LOADED()
                AND NOT g_FMMC_STRUCT.bParticlesOff                                 
                IF bOnWater
                    PRINTLN("SPLASH PARTICLE GO NOW!")
                    START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_splash", vParticlePos, <<0,0,0>>)
                ELSE
                    PRINTLN("DUST PARTICLE GO NOW!")
                START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_dust_cloud", vParticlePos, <<0,0,0>>)
                ENDIF
                ENDIF
                ENDPROC
                */
















                    /*
                    SET_MOBILE_PHONE_DOF_STATE (TRUE)

                    IF GET_MOBILE_PHONE_DOF_STATE() = TRUE

                        DISPLAY_TEXT_WITH_LITERAL_STRING (0.40, 0.20, "STRING", "New DOF ON")

                    ENDIF
                    */
            
                    //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE)


                    //g_bEnableHackingApp = TRUE  
            
                    //g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE

                    //g_forceDisplayPlayerInvitesIndicator = FALSE
                    //g_numNewJoblistInvites ++


                    //DISPLAY_TEXT_WITH_NUMBER (0.20, 0.45, "CELL_500", g_numNewJoblistInvites)


                    //IconTest ++

                    //#if IS_DEBUG_BUILD

                        //cdPrintstring("Icon Test is ")
                        //cdPrintint(IconTest)
                        //cdPrintnl()
                        
                    //#endif

                    //SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)


                    //g_bUse_MP_DLC_Dialogue = TRUE

                    //WAIT(0)

                    //CREATE_CONVERSATION(MyLocalPedStruct, "CT_AUD", "MPCT_MCarr", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
                    

                    //CREATE_MOBILE_PHONE (MOBILE_1)

                    //WAIT(5000)

                    //TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), TRUE)

                    //TRIGGER_CHEAT (CHEAT_TYPE_ADVANCE_WEATHER) 


                    //SET_BIT (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION)

                    //BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)

                    //g_FirstPersonTransitionOngoing = TRUE

                    //CELL_CAM_ACTIVATE (FALSE, FALSE)

                    //CELL_HORIZONTAL_MODE_TOGGLE (TRUE)
                    /* Trackify tests for Gareth
                    ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)



                 
                    //g_i_NumberOfTrackifyTargets = 4
                    SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(4)

                     //v_player_pos.x = -2.0
                     //v_player_pos.y = -3.0

                     //g_v_TrackifyMultipleTarget[0].x = -1275.5
                     //g_v_TrackifyMultipleTarget[0].y = 35.3
                     //g_v_TrackifyMultipleTarget[0].z = 49.7

                    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (0, <<1850.1, 3325.0, 45.3>>) //Senora National Park.


                     //g_v_TrackifyMultipleTarget[1].x = 0.5
                     //g_v_TrackifyMultipleTarget[1].y = 0.3
                     //g_v_TrackifyMultipleTarget[1].z = 49.7
                    
                    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (1, <<1565.5, 2858.3, 54.7>>)  //America Americans sign.



                     //g_v_TrackifyMultipleTarget[2].x = 1275.5
                     //g_v_TrackifyMultipleTarget[2].y = -1000.3
                     //g_v_TrackifyMultipleTarget[2].z = 49.7

                    //g_FirstPersonTransitionOngoing = TRUE

                   //HIDE_PHONE_IN_FIRST_PERSON_EXPERIMENTAL()
                    //SET_BIT(BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE)

                    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (2, <<206.5, 3347.3, 42.5>>) //Blue Uranium sign.

                    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (3, <<-206.5, -3347.3, 42.5>>) //Test 4

                     //g_v_TrackifyMultipleTarget[3].x = -4275.5
                     //g_v_TrackifyMultipleTarget[3].y = -1000.3
                     //g_v_TrackifyMultipleTarget[3].z = 49.7

                    //SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (3, <<-4275.5, -1000.3, 49.7>>)
                    */

                    

























                    //g_bUse_MP_DLC_Dialogue = TRUE

                    //ENABLE_MULTIPLAYER_TRACKIFY_APP (FALSE)

                    //LAUNCH_CELLPHONE_APPLICATION (AppCamera, TRUE, TRUE, FALSE)
                    //TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), TRUE) 

                    //CELL_HORIZONTAL_MODE_TOGGLE (FALSE)

                    /*
                    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) = 0             
                            REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("PI_MENU") #IF IS_DEBUG_BUILD ,"PI_MENU" #ENDIF,MULTIPLAYER_MISSION_STACK_SIZE)  
                    ENDIF
                    */

                    //IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                        //KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
                    //ENDIF

                    //ENABLE_MULTIPLAYER_TRACKIFY_APP (FALSE)
                    //PRINT_HELP ("CELL_705") 

                    //SET_NIGHTVISION(FALSE)
          
                    //ADD_CONTACT_TO_PHONEBOOK(CHAR_CALL911, ALL_OWNERS_BOOKS, FALSE)
                        //g_bInMultiplayer = TRUE

                        //g_bUse_MP_DLC_Dialogue = TRUE



                    //PRINT_HELP ("CELL_705")                   
                    //Play_Invite_Received_Sound_Effect()

                     //SET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1", MPE_NO_REPLY_REQUIRED)

                    //PRINTSTRING ("Testing NG new compile")
                    //PRINTNL()
                    
                    //SET_MP_EMAIL_REPLY_STATUS("CELL_EMAIL1", MPE_NO_REPLY_REQUIRED)

                    //DELETE_EMAIL_BY_LABEL_FROM_CURRENT_PLAYER ("CELL_EMAIL1")

                    //ADD_CONTACT_TO_PHONEBOOK(CHAR_DAVE, ALL_OWNERS_BOOKS)


                    //cdPrintnl()
                    //cdPrintString(GET_ONSCREEN_KEYBOARD_RESULT())
                    //cdPrintnl()

                    //CREATE_MOBILE_PHONE (MOBILE_1)





                            //IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT()
                            //OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            //OR NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            //OR NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                            //OR NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                                //IF IS_OK_TO_PRINT_FREEMODE_HELP()
                                    //PRINT_HELP_WITH_PLAYER_NAME("CELL_MP_1009M", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]), GET_PLAYER_HUD_COLOUR(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) //It is currently not possible to send a message to ~a~.
                                //ENDIF
                                //m_sPlayers.bConfirmedInteraction = FALSE
                                //PRINTLN("     ---------->     APP - OPTION_SEND_MESSAGE - NO LONGER A VALID PLAYER (UNDERAGE, BLOCKED, OR MUTED) - SEND TO ", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                                //EXIT
                            //ENDIF
                            
                            //m_sPlayers.bConfirmedInteraction = FALSE
                            //m_sPlayers.iPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SEND_MESSAGE
                            //PRINTLN("     ---------->     APP - OPTION_SEND_MESSAGE - PiPlayerInteractionStage = PLAYER_INTERACTION_STAGE_SEND_MESSAGE - SEND TO ", GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                            
                            
                            //bCreatingEmailMessage = TRUE




                    //g_QuickSaveDisabledByScript = TRUE

                    //REMOVE_ALL_CONTACTS_FROM_ALL_PLAYER_CHARACTERS_PHONEBOOKS()

                    /*
                    IF NOT IS_PED_INJURED (PLAYER_PED_ID())        

                    
                        INT i_Headgear
                        INT i_Eyegear

                        i_Headgear = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)

                        cdPrintstring ("Getting headgear prop index. Index is ")
                        cdPrintint(i_Headgear)
                        cdPrintnl()
                        cdPrintnl()


                        i_Eyegear = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES)

                        cdPrintstring ("Getting Eyegear prop index. Index is ")
                        cdPrintint(i_Eyegear)
                        cdPrintnl()
                        cdPrintnl()




                        
                        IF g_bInMultiplayer

                            IF i_Headgear = 15  
                            OR i_Headgear = 16  
                            OR i_Headgear = 17                 
                            OR i_Headgear = 18 
                            OR i_Headgear = 19 
                            OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())
                            
                                //b_WearingProblemHeadgear = TRUE

                                #if IS_DEBUG_BUILD

                                    PRINTSTRING ("CELLPHONE_PRIV. Player is wearing headgear that would cause phone to clip through prop, not placing phone to ear.")
                                    PRINTNL()

                                #endif

                            ENDIF

                        ELSE

                            IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
                            OR i_Headgear = 15  
                            OR i_Headgear = 16  
                            OR i_Headgear = 17                 
                            OR i_Headgear = 18 
                            OR i_Headgear = 19 
                               
                               
                                //b_WearingProblemHeadgear = TRUE

                                #if IS_DEBUG_BUILD

                                    PRINTSTRING ("CELLPHONE_PRIV. Player is wearing headgear that would cause phone to clip through prop, not placing phone to ear.")
                                    PRINTNL()

                                #endif

                            ENDIF

                        ENDIF

                    ENDIF

                */

                //PHONEPHOTOEDITOR_TOGGLE(FALSE)

                //PHONEPHOTOEDITOR_SET_FRAME_TXD("MPRankBadge", FALSE)

                //REQUEST_ANIM_DICT("facials@p_m_zero@base")                    
                        
                //CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

    

                //DESTROY_ALL_CAMS





                //ANIMPOSTFX_STOP_ALL()


                //LAUNCH_TRACKIFY_IMMEDIATELY()

                /*
                BEGIN_TEXT_COMMAND_THEFEED_POST ("SC_READ_TOS_BUTTON")

                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                
                    s_MPGamerTagTempHolder = "<C>"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += "Social Club"
                    s_MPGamerTagTempHolder += "</C>"

                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)
                */
        


                  
                //PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING ("CELL_103", "Gamertag2", 82950, "CELL_102", TRUE)
                  
                 //SET_PED_IS_DRUNK (PLAYER_PED_ID(), TRUE)
                  
                //CREATE_CONVERSATION ( MyLocalPedStruct, "FAM3AUD", "FAM3_MST2", CONV_PRIORITY_LOW )
                //KILL_ANY_CONVERSATION()
           
                /*
                //Phone task test
                IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927
                    TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), FALSE)                                                                                          
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring ("INCREDIBUILD TEST CELLPHONE_PRIV - Phone removed from ear - bit cleared.")
                    cdPrintnl()
                #endif

                IF g_bInMultiplayer
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, TRUE)
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
                    SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
                ENDIF


                //SCRIPT_ASSERT("Task test")
                //CLEAR_PED_TASKS_IMMEDIATELY (PLAYER_PED_ID()) 

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)
                //Phone task test
                */











                //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = TRUE

                //START_PRELOADED_CONVERSATION()  
                                                                  
                //PAUSE_CELLPHONE_CONVERSATION(FALSE)

                //REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (FALSE)

                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_DLS1)

                //Long conversation subtitles
                //CREATE_CONVERSATION ( MyLocalPedStruct, "D1AUD", "DS1_1", CONV_PRIORITY_MEDIUM )

 
                //SET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1, TRUE) //For testing blurb.

                //SET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3, TRUE) //Doesn't have blurb, use for score

                //SET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_1, TRUE)

                //SET_CELLPHONE_PROFILE_TO_SLEEP()

                //PAUSE_CELLPHONE_CONVERSATION(FALSE)
     
                //PRINTSTRING ("Signin Called")
                //PRINTNL()

                //DISPLAY_SYSTEM_SIGNIN_UI(TRUE)

                //PRESENCE_EVENT_UPDATESTAT_INT(SP0_CROUCHED_AND_SHOT, 20)


                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_HOLDUP)

                //PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_CAR, 500.0)    

                    //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL ( CP_OJ_HUN1 )


    
                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_RE_PLIFT1)


                 //SET_OUTGOING_CHAT_CALL_AS_ENGAGED(TRUE)
                     

                //KILL_ANY_CONVERSATION()

                //LAUNCH_TRACKIFY_IMMEDIATELY (FALSE)


                //REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()

                //g_OnMissionState = MISSION_TYPE_OFF_MISSION






                //Test cutscene nonsense.
                //REQUEST_ADDITIONAL_TEXT ("SCAUD", MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                //WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                    //WAIT(0)
                    //#if IS_DEBUG_BUILD
                        //cdPrintstring ("MIMICKING cutscene load into mission dialogue text slot")
                        //cdPrintnl()
                    //#endif
                //ENDWHILE










                //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL", FALSE, TRUE)
                //ADD_PED_FOR_DIALOGUE (MyLocalPedStruct, 7, NULL, "OJAHkDealer")

                /*
                g_iUnreadEmailsSP0 = 3
                g_iUnreadEmailsSP1 = 3
                g_iUnreadEmailsSP2 = 3
                */
       
                //PLAY_AUDIO_EVENT("MOBILE_PRERING")

                //IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                    //PLAY_AUDIO_EVENT ("MOBILE_PRERING")

                //ENDIF

                //PLAY_AUDIO_EVENT("WEAPON_PICKUP_GRENADE_LAUNCHER")

                //cdPrintstring ("testing prering 3")
                //cdPrintnl()
                

                //PLAY_SINGLE_LINE_FROM_CONVERSATION (MyLocalPedStruct, "MEJ1AUD", "MEJ1_KILL", "MEJ1_KILL_2", CONV_PRIORITY_MEDIUM)

                //CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_FRANKLIN)

                //REMOVE_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION()
                //ADD_SECONDARY_OPTION_FOR_MP_CHARACTER (0, CHAR_MICHAEL, "CELL_3")

                //SET_CALL_PREVENTED_FOR_CHAR_DETONATEPHONE(FALSE)

                //g_b_ReplaceThisWithQSavailable = TRUE

                //REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_COMEDY)


                //SET_TRACKIFY_TARGET_VECTOR (<<1073.8, 3004.4, 56.8>>)


                /*
                IF IS_TEXTMESSAGE_LIST_IN_SINGLE_MESSAGE_VIEW()

                    AUTO_RETURN_TO_FULL_TEXT_MESSAGE_LIST_VIEW()
    
                ENDIF
                */

   
                
                               
                //The initial part of the call...
                /*
                STRING TestRoots[10]
                STRING TestLabels[10]

                
                TestRoots[0] = "F3_DEAD"
                TestRoots[1] = "f3_TENTER"
                TestRoots[2] = "F3_DRV1"
                TestRoots[3] = "F3_DRV1"
                TestRoots[4] = "f3_TENTER"
                TestRoots[5] = "F3_DEAD"
                TestRoots[6] = "f3_TENTER"
                TestRoots[7] = "F3_DRV1"
                TestRoots[8] = "F3_DRV1"
                TestRoots[9] = "F3_DRV1"



                TestLabels[0] = "F3_DEAD_3"
                TestLabels[1] = "f3_TENTER_5"
                TestLabels[2] = "F3_DRV1_3"
                TestLabels[3] = "F3_DRV1_4"
                TestLabels[4] = "f3_TENTER"
                TestLabels[5] = "F3_DEAD_3"
                TestLabels[6] = "f3_TENTER_5"
                TestLabels[7] = "F3_DRV1_1"
                TestLabels[8] = "F3_DRV1_2"
                TestLabels[9] = "F3_DRV1_1"

                //The yes branch
                STRING TestYesRoots[10]
                STRING TestYesSpecifics[10]

                TestYesRoots[0] = "F3_DEAD"
                TestYesRoots[1] = "f3_TENTER"
                TestYesRoots[2] = "F3_DRV1"
                TestYesRoots[3] = "F3_DRV1"
                TestYesRoots[4] = "f3_TENTER"
                TestYesRoots[5] = "F3_DEAD"

                TestYesSpecifics[0] = "F3_DEAD_3"
                TestYesSpecifics[1] = "f3_TENTER_5"
                TestYesSpecifics[2] = "F3_DRV1_1"
                TestYesSpecifics[3] = "F3_DRV1_2"
                TestYesSpecifics[4] = "f3_TENTER_5"
                TestYesSpecifics[5] = "F3_DEAD_3"


    
                //The no branch
                STRING TestNoRoots[10]
                STRING TestNoSpecifics[10]

                TestNoRoots[0] = "F3_DEAD"
                TestNoRoots[1] = "f3_TENTER"
                TestNoRoots[2] = "F3_DRV1"
                TestNoRoots[3] = "F3_DRV1"
                TestNoRoots[4] = "f3_TENTER"
                TestNoRoots[5] = "F3_DEAD"
                TestNoRoots[6] = "F3_DRV1"

                TestNoSpecifics[0] = "F3_DEAD_3"
                TestNoSpecifics[1] = "f3_TENTER_5"
                TestNoSpecifics[2] = "F3_DRV1_1"
                TestNoSpecifics[3] = "F3_DRV1_2"
                TestNoSpecifics[4] = "f3_TENTER_5"
                TestNoSpecifics[5] = "F3_DEAD_3"
                TestNoSpecifics[6] = "F3_DRV1_3"


                IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE (10, MyLocalPedStruct, CHAR_DR_FRIEDLANDER, "FB3AUD", TestRoots, TestLabels, "CELL_291", CONV_PRIORITY_VERY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
                
                     SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES (6, 7, TestYesRoots, TestYesSpecifics, TestNoRoots, TestNoSpecifics)

                ENDIF
                */


                //SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES (3, 3, TestYesRoots, TestYesSpecifics, TestNoRoots, TestNoSpecifics)

                //KILL_FACE_TO_FACE_CONVERSATION()

            ENDIF
            

            
            


            //Prints out contents of test structs...


            IF b_DisplayTestStructData


                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppContacts].AppScriptNameHash) > 0

                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.15, "STRING", "Testing Hash Key Return")
            
                ENDIF


                

                DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.35, "STRING", "MyLocalStruct")


                DISPLAY_TEXT_WITH_NUMBER (0.20, 0.45, "CELL_500", 0)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.5, "STRING", MyLocalPedStruct.PedInfo[0].VoiceId)
                
                
                DISPLAY_TEXT_WITH_NUMBER (0.20, 0.58, "CELL_500", 1)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.63, "STRING", MyLocalPedStruct.PedInfo[1].VoiceId)







                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.35, "STRING", "TestLocalStruct")


                DISPLAY_TEXT_WITH_NUMBER (0.55, 0.45, "CELL_500", 0)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.5, "STRING", TestLocalPedStruct.PedInfo[0].VoiceId)
                
                
                DISPLAY_TEXT_WITH_NUMBER (0.55, 0.58, "CELL_500", 1)
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.63, "STRING", TestLocalPedStruct.PedInfo[1].VoiceId)

            ENDIF

            
            

            


        #endif

    
    /*
      Reinstate this closing comment to disable main loop for core code phone testing.
    */

            
    ENDWHILE
    
    
ENDSCRIPT


