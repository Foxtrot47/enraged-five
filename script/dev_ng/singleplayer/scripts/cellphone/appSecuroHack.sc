USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"

  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appSecuroHack.sc
//      AUTHOR          :   John Scott
//      DESCRIPTION     :   Used to launch SecuroServ Hacking app from cellphone. 
//            
//                            
//                                     
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
                                        
BOOL b_InitialSecuroHackSetup = FALSE

//PURPOSE: This fills the scaleform view state.
PROC Scaleform_Set_SecuroHack_Details()
	
	BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(27)	//ID of the SecuroServ Hacking app - always 27
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		//slotID - always 0
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHackStage)		//state - 0 = No Signal, 1 = Percentage, 2 = Hack Complete, 3 = Another player is hacking, 4 = Losing signal
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHackPercentage)		//value - percentage of the hack for stage 1
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	//inProgressMessage
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15HackComplete)	//completeMessage
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "DISPLAY_VIEW")
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(27)	//ID of the SecuroServ Hacking app - always 27

	END_SCALEFORM_MOVIE_METHOD()
	
	//John, Adam you'll want to call this when the player is unable to press "back" to exit the phone.This will hide the "back" soft key. Theoretically, he can press back when there's a "no signal", but there may be other points I'm not aware of.
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)	
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0, 27, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative - Switch these button off for 3218707
	ENDIF
	//Changing the 0 to a 1 beside BADGER_NEG and calling again will switch the phone's "back" soft key to display again if you need to do that.	
	
			
	
	
	
	
    #if IS_DEBUG_BUILD

        cdPrintstring ("AppExtraction - Updating Scaleform.")
        cdPrintnl()

    #endif      

	
ENDPROC







PROC Initialise_SecuroHack_App()
    
    Scaleform_Set_SecuroHack_Details()

    #if IS_DEBUG_BUILD

        cdPrintstring ("AppSecuroHack - S_P_D appSecuroHack initialisation...")
        cdPrintnl()

    #endif
	
	//John / Adam. I'm assuming the "select" button on the phone is never used. So we don't need to draw that either. See bug 3218707. Comment this line in below.
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0, 27, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
			
ENDPROC






PROC Refresh_SecuroHack_App()

    Scaleform_Set_SecuroHack_Details()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) //url:bugstar:4047267 - DeLorean - In the Deluxo pressing B to exit the Securoserv App while in First Person causes the screen to flicker briefly

    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("AppSecuroHack -  Refresh_SecuroHack_App triggered!")
        cdPrintnl()

    #endif

ENDPROC












PROC Cleanup_and_Terminate()


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("AppSecuroHack - appSecuroHack exiting normally.")
        PRINTNL()

        cdPrintnl()
        cdPrintstring ("AppSecuroHack - S_P_D appSecuroHack exiting normally.")
        cdPrintnl()

    #endif



    TERMINATE_THIS_THREAD()


ENDPROC 












SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    

    IF b_InitialSecuroHackSetup = FALSE

        Initialise_SecuroHack_App()

        b_InitialSecuroHackSetup = TRUE
		
    ENDIF

    
     
    WHILE TRUE

        WAIT(0)
        

     
        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        
            SWITCH g_Cellphone.PhoneDS
             
            

                CASE PDS_RUNNINGAPP
                                           
                    IF b_InitialSecuroHackSetup = TRUE

                        Refresh_SecuroHack_App()

                    ENDIF
 
         
                BREAK

                                                                                                                             

                CASE PDS_COMPLEXAPP


                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

               

                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 23198. AppSecuroHack assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                    ENDIF

                BREAK
               


                DEFAULT

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("AppSecuroHack in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  


    

       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Repeats list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT


