

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT   USE_TU_CHANGES  0


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"



  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appTrackify.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   A tracking app for the cellphone.         
//                            
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           



//SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex




VECTOR v_player_pos// g_v_TrackifyTarget
FLOAT v_player_heading, v_player_adjusted_heading



FLOAT dy, dx
FLOAT result_angle = 9.99

FLOAT Distance_between_Plyr_Target_2D = 0.0
FLOAT Distance_between_Plyr_Target_3D = 0.0


FLOAT f_HeightDifferential = 0.0 

BOOL b_InitialTargetSetup = FALSE









#if IS_DEBUG_BUILD          
PROC Temp_Setup_Of_Test_Data()

    //g_i_NumberOfTrackifyTargets = 4
    /*
    SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(4)

     //v_player_pos.x = -2.0
     //v_player_pos.y = -3.0

     //g_v_TrackifyMultipleTarget[0].x = -1275.5
     //g_v_TrackifyMultipleTarget[0].y = 35.3
     //g_v_TrackifyMultipleTarget[0].z = 49.7

    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (0, <<-1275.5, 35.3, 49.7>>)


     //g_v_TrackifyMultipleTarget[1].x = 0.5
     //g_v_TrackifyMultipleTarget[1].y = 0.3
     //g_v_TrackifyMultipleTarget[1].z = 49.7
    
    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (1, <<0.5, 0.3, 49.7>>)



     //g_v_TrackifyMultipleTarget[2].x = 1275.5
     //g_v_TrackifyMultipleTarget[2].y = -1000.3
     //g_v_TrackifyMultipleTarget[2].z = 49.7

    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (2, <<1275.5, -1000.3, 49.7>>)

                                                  

     //g_v_TrackifyMultipleTarget[3].x = -4275.5
     //g_v_TrackifyMultipleTarget[3].y = -1000.3
     //g_v_TrackifyMultipleTarget[3].z = 49.7

    SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (3, <<-4275.5, -1000.3, 49.7>>)
    */

ENDPROC
#endif
                                                                





FUNC FLOAT Get_Heading_Of_Target_Relative_to_Player()

    dy = g_v_TrackifyTarget.x - v_player_pos.x
    dx = COS(3.14159/180*v_player_pos.x)*(g_v_TrackifyTarget.y - v_player_pos.Y)

    result_angle = ATAN2(dy, dx)

    IF result_angle < 0.0

        result_angle = result_angle + 360.0

    ENDIF


    RETURN result_angle


ENDFUNC





FUNC FLOAT Get_Heading_Of_This_Multiple_Target_Relative_to_Player(INT targetIndex)

    dy = g_v_TrackifyMultipleTarget[targetIndex].x - v_player_pos.x
    dx = COS(3.14159/180*v_player_pos.x)*(g_v_TrackifyMultipleTarget[targetIndex].y - v_player_pos.Y)

    result_angle = ATAN2(dy, dx)

    IF result_angle < 0.0

        result_angle = result_angle + 360.0

    ENDIF


    RETURN result_angle


ENDFUNC






FUNC FLOAT Get_Distance_Between_Player_and_Target_2D()

    //2d distance only.
    Distance_between_Plyr_Target_2D = SQRT( ((g_v_TrackifyTarget.x - v_player_pos.x) * (g_v_TrackifyTarget.x - v_player_pos.x))  +  ((g_v_TrackifyTarget.y - v_player_pos.Y) * (g_v_TrackifyTarget.y - v_player_pos.Y))    )


    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("Distance... ")
        PRINTFLOAT (Distance_between_Plyr_Target_2D)
        cdPrintnl()

    #endif
    */

    
    RETURN Distance_between_Plyr_Target_2D

ENDFUNC







FUNC FLOAT Get_Distance_Between_Player_and_This_Multiple_Target_2D(INT targetIndex)

    //2d distance only.
    Distance_between_Plyr_Target_2D = SQRT( ((g_v_TrackifyMultipleTarget[targetIndex].x - v_player_pos.x) * (g_v_TrackifyMultipleTarget[targetIndex].x - v_player_pos.x))  +  ((g_v_TrackifyMultipleTarget[targetIndex].y - v_player_pos.Y) * (g_v_TrackifyMultipleTarget[targetIndex].y - v_player_pos.Y))    )


    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("Distance... ")
        PRINTFLOAT (Distance_between_Plyr_Target_2D)
        cdPrintnl()

    #endif
    */

    
    RETURN Distance_between_Plyr_Target_2D

ENDFUNC















FUNC FLOAT Get_Distance_Between_Player_and_Target_3D()
 

    //Need to switch to a 3d distance calculation now this is being used for submarine collections.
    Distance_between_Plyr_Target_3D = SQRT (VDIST2(g_v_TrackifyTarget, v_player_pos)) 

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("Distance... ")
        PRINTFLOAT (Distance_between_Plyr_Target_3D)
        cdPrintnl()

    #endif
    */
    

  
    f_HeightDifferential = (g_v_TrackifyTarget.z - v_player_pos.z)
    

    RETURN Distance_between_Plyr_Target_3D

ENDFUNC








FUNC FLOAT Get_Distance_Between_Player_and_This_Multiple_Target_3D(INT targetIndex)
 

    //Need to switch to a 3d distance calculation now this is being used for submarine collections.
    Distance_between_Plyr_Target_3D = SQRT (VDIST2(g_v_TrackifyMultipleTarget[targetIndex], v_player_pos)) 

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("Distance... ")
        PRINTFLOAT (Distance_between_Plyr_Target_3D)
        cdPrintnl()

    #endif
    */
    

  
    f_HeightDifferential = (g_v_TrackifyMultipleTarget[targetIndex].z - v_player_pos.z)
    

    RETURN Distance_between_Plyr_Target_3D

ENDFUNC





                            

FUNC BOOL ShouldRunMultipleTargets()

    IF g_bInMultiplayer = TRUE

        
        //RETURN FALSE

        //DO NOT CHECK IN YET!
        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC





//Will need a specific branch of this for multiple targets.

PROC Update_Position_Data_in_App()
                                                    


IF ShouldRunMultipleTargets() = FALSE

    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

        v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())

        v_player_heading = GET_ENTITY_HEADING (PLAYER_PED_ID())




        Get_Distance_Between_Player_and_Target_2D()

        Get_Distance_Between_Player_and_Target_3D()




        Get_Heading_Of_Target_Relative_to_Player()


        v_player_adjusted_heading = 360.0 - v_player_heading


        result_angle = (result_angle - v_player_adjusted_heading)

        IF result_angle < 0.0 

            result_angle = result_angle + 360.0

        ENDIF

     
        /*
        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            Printfloat(result_angle)
            cdPrintnl()
        

        #endif
        */
        


        IF b_InitialTargetSetup = FALSE
        
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //System 

                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_REMOVE_TRACKIFY_TARGET)

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (-99)  //Negative angle removes target.
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)


                ELSE

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(result_angle))  //Trackify Screen
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(Distance_between_Plyr_Target_2D))

            
                ENDIF

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Skips loading screen.

                    //Updated because of Trackify changes which mean loading screens take an enum rather than a bool.
                    //LOADING_SCREEN_TYPE_SKIPPED = 1
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (1)

                ELSE
                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Show loading screen.

                    //Updated because of Trackify changes which mean loading screens take an enum rather than a bool.
                    //LOADING_SCREEN_TYPE_STANDARD = 0
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)

                ENDIF


                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip.
                ELSE
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Show arrow on target blip.
                ENDIF


                IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (f_HeightDifferential)
                ENDIF


            END_SCALEFORM_MOVIE_METHOD()

            //Comment this back in to test Eddie's new "tween" methods.
            //b_InitialTargetSetup = TRUE


            //cdPrintstring ("Updating target")
            //cdPrintnl()




        ELSE


            //cdPrintstring ("Updating target")
            //cdPrintnl()

            /*
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "APP_FUNCTION")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)  //function zero is for setting the target

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(result_angle))  
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(Distance_between_Plyr_Target_2D))
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)

            END_SCALEFORM_MOVIE_METHOD()
            */

            
        ENDIF



        IF v_player_heading = v_player_heading
        ENDIF

                        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))      


    ENDIF





ELSE




    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

            v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())

            v_player_heading = GET_ENTITY_HEADING (PLAYER_PED_ID())


            IF g_i_NumberOfTrackifyTargets > c_MaxNumberOfTrackifyTargets
                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppTrackify - c_MaxNumberOfTrackifyTargets exceeded. Passed number of targets was ")
                    cdPrintint (g_i_NumberOfTrackifyTargets)
                    cdPrintnl()

                #endif
            ENDIF
     


            INT i_TempIndex = 0
            
            WHILE i_TempIndex < g_i_NumberOfTrackifyTargets //This should be set by the script using Trackify.

                /*
                #if IS_DEBUG_BUILD
                    
                    cdPrintnl()
                    cdPrintstring ("AppTrackify - Calculating and plotting this target... ")
                    cdPrintint (i_TempIndex)

                #endif
                */


                Get_Distance_Between_Player_and_This_Multiple_Target_2D(i_TempIndex)

                Get_Distance_Between_Player_and_This_Multiple_Target_3D(i_TempIndex)




                Get_Heading_Of_This_Multiple_Target_Relative_to_Player(i_TempIndex)


                v_player_adjusted_heading = 360.0 - v_player_heading


                result_angle = (result_angle - v_player_adjusted_heading)

                IF result_angle < 0.0 

                    result_angle = result_angle + 360.0

                ENDIF

             
                
                #if IS_DEBUG_BUILD
                
                    //cdPrintnl()
                    //cdPrintInt(FLOOR(result_angle))
                    //cdPrintInt(FLOOR(Distance_between_Plyr_Target_2D))
                    
                #endif
                
                


                IF b_InitialTargetSetup = FALSE
                
                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (23) //System
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (i_TempIndex)//(0) //Should be (i_TempIndex) when scaleform works? //System 

                        //TODO!


                        BOOL b_NeedToRemoveThisTarget = FALSE

                        SWITCH i_TempIndex


                            CASE 0
                                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_0)
                                    b_NeedToRemoveThisTarget = TRUE
                                ENDIF
                            BREAK


                            CASE 1
                                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_1)
                                    b_NeedToRemoveThisTarget = TRUE
                                ENDIF
                            BREAK


                            CASE 2
                                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_2)
                                    b_NeedToRemoveThisTarget = TRUE
                                ENDIF
                            BREAK


                            CASE 3
                                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_3)
                                    b_NeedToRemoveThisTarget = TRUE
                                ENDIF
                            BREAK


                        ENDSWITCH



                        IF b_NeedToRemoveThisTarget 
                                                                                                                                 
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (-99)  //Negative angle removes target.
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (0)


                        ELSE


                            #if IS_DEBUG_BUILD

                                //cdPrintstring ("AppTrackify - Calculating and plotting above target... ")
                                //cdPrintint (i_TempIndex)
                                //cdPrintnl()

                            #endif



                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(result_angle))  //Trackify Screen
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (FLOOR(Distance_between_Plyr_Target_2D))

                    
                        ENDIF


                        //Need to stop Trackify zooming in when target gas been removed.



                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (100)//(FLOOR(10.0)) //The lower this number the further away the target will be on the radar.

                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Skips loading screen.
                        ELSE


                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Always skipping screen in MP for just now. Setting this to true also.

                            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Show loading screen.


                        ENDIF


                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE) //Hide arrow on target blip. //This should be false!
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Show arrow on target blip.  //Temp Test Feb 2016 - This is of no use for 2718042. It just points to the general compass direction relative to the player of the blip.
                        ELSE
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE) //Show arrow on target blip.
                        ENDIF


				/*
				Previously, the last parameter passed into scaleform for Trackify was a float that displayed some numbers on the screen that could be used for height differentials in 
				SP's submarine collection minigame.

				In MP, this parameter is not passed in but if you do pass it in, followed by the new arrow stuff, then the arrows appear. 
				The good news is that the the height differential numbers don't appear. This is probably safe enough I'd think for my script fudge to be used here. It won't impact SP.
				*/
                        //IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT (f_HeightDifferential) //Need to include this to get arrows to work for 2718042
                        //ENDIF

						//Add height arrows for 2718042
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(g_v_TrackifyMPTargetArrowType[i_TempIndex]))
						#if IS_DEBUG_BUILD
							cdPrintstring ("Arrow Type for target ")
							cdPrintint(i_TempIndex)
							cdPrintstring (" was  ")
							cdPrintint(ENUM_TO_INT(g_v_TrackifyMPTargetArrowType[i_TempIndex]))
	        				cdPrintnl()
						#ENDIF
						
                    END_SCALEFORM_MOVIE_METHOD()

                ENDIF
                

                IF v_player_heading = v_player_heading
                ENDIF

                                
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 23, TO_FLOAT(1))      

                i_TempIndex ++


                //WAIT (4000) //Comment this to show test plotting working!


            ENDWHILE

        ENDIF

    ENDIF

ENDPROC




PROC Set_Up_Trackify_Buttons()



    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"YES" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative


    ELSE 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


     
ENDPROC




























PROC Cleanup_and_Terminate()


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("AppTrackify - appTrackify exiting normally...")
        PRINTNL()

    #endif

	SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(0, Arrow_Off)
	SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(1, Arrow_Off)
	SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(2, Arrow_Off)
	SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(3, Arrow_Off)
	SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(4, Arrow_Off)


    #if IS_DEBUG_BUILD

        cdPrintstring ("AppTrackify - Resetting Arrow Type for all MP multiple Blips to OFF.")
        cdPrintnl()

    #endif




    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)



    TERMINATE_THIS_THREAD()


ENDPROC 












SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()





    //DO NOT CHECK IN PERMANENTLY!
    #if IS_DEBUG_BUILD
        
        //Temp_Setup_Of_Test_Data()


   
            /*
            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_0)

                dy = Xlat2 - Xlat1
                dx = cos(3.14159/180*Xlat1)*(Ylong2 - Ylong1)
                result_angle = atan2(dy, dx)

                IF result_angle < 0.0

                    result_angle = result_angle + 360.0

                ENDIF

            ENDIF

            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_9)

                Xlat2 = 44.0
                Ylong2 = 213.0

            ENDIF

        

            DISPLAY_TEXT_WITH_FLOAT (0.5, 0.5, "CELL_500", result_angle, 2)

           */



    #endif













                                                                
    Update_Position_Data_in_App()

    Set_Up_Trackify_Buttons()


    SETTIMERA(0)
    SETTIMERB(0)

    
    WHILE TRUE

        WAIT(0)
        

        #if IS_DEBUG_BUILD

            IF g_Cellphone_Onscreen_State_Debug = TRUE

                
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.2, 0.4, "STRING", "Plyr Pos")
                DISPLAY_TEXT_WITH_FLOAT (0.2, 0.46, "CELL_500", v_player_pos.x, 2)
                DISPLAY_TEXT_WITH_FLOAT (0.2, 0.52, "CELL_501", v_player_pos.y, 2)


                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.4, "STRING", "Tgt Pos")
                DISPLAY_TEXT_WITH_FLOAT (0.55, 0.46, "CELL_500", g_v_TrackifyTarget.x, 2)
                DISPLAY_TEXT_WITH_FLOAT (0.55, 0.52, "CELL_501", g_v_TrackifyTarget.y, 2)


                DISPLAY_TEXT_WITH_FLOAT (0.3, 0.65, "CELL_507", (Get_Distance_Between_Player_and_Target_2D()), 2)

                DISPLAY_TEXT_WITH_FLOAT (0.3, 0.75, "CELL_508", (Get_Heading_Of_Target_Relative_to_Player()), 2)

                DISPLAY_TEXT_WITH_FLOAT (0.3, 0.85, "CELL_500", v_player_adjusted_heading, 2)

            ENDIF

        #endif





        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        
            SWITCH g_Cellphone.PhoneDS
             
            

                CASE PDS_RUNNINGAPP
                                             

                    IF TIMERA() > 125 //250  //Dangerous to spam scaleform slot methods every frame.. Speed up requested by Eddie in Bug Change: 1090497 
                    
                        Update_Position_Data_in_App() //Need to continually update this in real time or perhaps staggered slightly like the clock...
                    
                        SETTIMERA(0)

                    ENDIF



             
                    IF TIMERB() > (((FLOOR(Distance_between_Plyr_Target_3D)) * 30) )
                    AND TIMERB() > 150 //prevent spamming.

                        //PLAY_SOUND_FRONTEND (-1, "Trackify_Beep", g_Owner_Soundset)


                                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "APP_FUNCTION")

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (1)  //function one is for triggering the sonar ping. No other params required.

                                END_SCALEFORM_MOVIE_METHOD()




                        SETTIMERB(0)

                    ENDIF

                    IF TIMERB() > 2000

                        //PLAY_SOUND_FRONTEND (-1, "Trackify_Beep", g_Owner_Soundset)



                                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "APP_FUNCTION")

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT (1)  //function one is for triggering the sonar ping. No other params required.

                                END_SCALEFORM_MOVIE_METHOD()




                        SETTIMERB(0)

                    ENDIF


         
                BREAK

                                                                                                                             

                CASE PDS_COMPLEXAPP


                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

                
                        Update_Position_Data_in_App()
                        Set_Up_Trackify_Buttons()


                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 2313. AppTrackify assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                    ENDIF

                BREAK
               


                DEFAULT

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("AppTrackify in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  


    

                                                                                                            


       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Repeats list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT


