USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "cellphone_movement.sch"




  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appTextMessage.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Takes the array of text messages stored in the global array
//                          and places them in the scaleform slots in chronological order.
//                          
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************






INT MessageListSlot[MAX_TEXT_MESSAGES]

INT NumberOfMessagesInList = 0

INT ListCursorIndex = 0
INT i_FullListSlotToReturnTo = 0
INT SingleMessageIndex, MessageReadDecider

BOOL ThisMessageContainsHyperlink = FALSE


BOOL dpad_scroll_pause_cued = FALSE

BOOL b_SafetyValveFired = FALSE
INT sf_TempInt = 0


BOOL b_RepeatButtonLimiter = FALSE
INT RepeatButtonLimiter_StartTime, RepeatButtonLimiter_CurrentTime

SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex


CONST_INT c_NumberOfTXD_Messages 165//Was 159 -  Steve T increased as we're getting close to current limit of picture messages. Stack checked.

//Stack notes:
//October 2017 tweaking to 165 max messages.. Max Stack So Far was peaked at 2320. Stack Limit is 2552.

TEXT_LABEL_63 TextMessageTXD
TEXT_LABEL_23 TextMessagesThatRequireTXDs_LABEL[c_NumberOfTXD_Messages]
TEXT_LABEL_23 TextMessagesThatRequireTXDs_STRING[c_NumberOfTXD_Messages]

//Added this different array as a result of work on reducing the storage capacity of TextMessagesThatRequireTXDs_STRING to a text label 23.
CONST_INT c_LegacyLongTxdAmount 19
TEXT_LABEL_31 TextMessagesThatRequireTXDs_WITH_LONG_STRING[c_LegacyLongTxdAmount]


BOOL b_TXD_Load_Required = FALSE








//enumPhonebookPresence PersonalPhonebookToCheck  //we need to check which character we are using and store which phonebook we should base the contacts list on.


#if IS_DEBUG_BUILD

    INT MessagesToDraw = 0




    FLOAT listOriginX = 0.3//0.72
    FLOAT listOriginY = 0.42


    FLOAT drawItemX = 0.0
    FLOAT drawItemY = 0.0

#endif








//Connection length and answerphone block - now moved to cellphone_globals.sch
/*
INT Connection_EngagedPauseLength = 4000
INT Connection_AnsMsgPauseLength = 7000
STRING answerPhoneBlock = "H1AUD"
*/





PROC Set_Up_Messages_TXD_Relationship()

    

    TextMessagesThatRequireTXDs_LABEL[0] = "SXT_JUL_1ST" 
    //TextMessagesThatRequireTXDs_STRING[0] = "05_a_sext_stripperJuliet" //Example of how these used to be stored.
	TextMessagesThatRequireTXDs_WITH_LONG_STRING[0] = "05_a_sext_stripperJuliet" 

    TextMessagesThatRequireTXDs_LABEL[1] = "SXT_JUL_2ND" 
    //TextMessagesThatRequireTXDs_STRING[1] = "05_b_sext_stripperJuliet" 
	TextMessagesThatRequireTXDs_WITH_LONG_STRING[1] = "05_b_sext_stripperJuliet" 

    TextMessagesThatRequireTXDs_LABEL[2] = "SXT_JUL_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[2] = "05_c_sext_stripperJuliet" 


    TextMessagesThatRequireTXDs_LABEL[3] = "SXT_NIK_1ST" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[3] = "06_a_sext_stripperNikki" 

    TextMessagesThatRequireTXDs_LABEL[4] = "SXT_NIK_2ND" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[4] = "06_b_sext_stripperNikki" 

    TextMessagesThatRequireTXDs_LABEL[5] = "SXT_NIK_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[5] = "06_c_sext_stripperNikki" 

    TextMessagesThatRequireTXDs_LABEL[6] = "SXT_SAP_1ST" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[6] = "08_a_sext_stripperSapphire" 

    TextMessagesThatRequireTXDs_LABEL[7] = "SXT_SAP_2ND" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[7] = "08_b_sext_stripperSapphire" 

    TextMessagesThatRequireTXDs_LABEL[8] = "SXT_SAP_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[8] = "08_c_sext_stripperSapphire" 


    TextMessagesThatRequireTXDs_LABEL[9] = "SXT_INF_1ST" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[9] = "04_a_sext_stripperInfernus" 

    TextMessagesThatRequireTXDs_LABEL[10] = "SXT_INF_2ND" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[10] = "04_b_sext_stripperInfernus" 

    TextMessagesThatRequireTXDs_LABEL[11] = "SXT_INF_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[11] = "04_c_sext_stripperInfernus" 

    TextMessagesThatRequireTXDs_LABEL[12] = "SXT_TXI_1ST" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[12] = "11_a_sext_taxiLiz" 

    TextMessagesThatRequireTXDs_LABEL[13] = "SXT_TXI_2ND" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[13] = "11_b_sext_taxiLiz" 

    TextMessagesThatRequireTXDs_LABEL[14] = "SXT_TXI_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[14] = "11_c_sext_taxiLiz" 

    TextMessagesThatRequireTXDs_LABEL[15] = "SXT_HCH_1ST" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[15] = "10_a_sext_hitcherGirl" 

    TextMessagesThatRequireTXDs_LABEL[16] = "SXT_HCH_2ND" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[16] = "10_b_sext_hitcherGirl" 

    TextMessagesThatRequireTXDs_LABEL[17] = "SXT_HCH_NEED" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[17] = "10_c_sext_hitcherGirl" 

    TextMessagesThatRequireTXDs_LABEL[18] = "SOL2_PASS" 
    TextMessagesThatRequireTXDs_WITH_LONG_STRING[18] = "executiveproducer" 

    //Lowrider test texts.
    TextMessagesThatRequireTXDs_LABEL[19] = "LR_PIC_TXT1" 
    TextMessagesThatRequireTXDs_STRING[19] = "mt_phone_image_1" 

    TextMessagesThatRequireTXDs_LABEL[20] = "LR_PIC_TXT2" 
    TextMessagesThatRequireTXDs_STRING[20] = "mt_phone_image_2" 

    TextMessagesThatRequireTXDs_LABEL[21] = "LR_PIC_TXT3"
    TextMessagesThatRequireTXDs_STRING[21] = "mt_phone_image_3"

    TextMessagesThatRequireTXDs_LABEL[22] = "LR_PIC_TXT4"
    TextMessagesThatRequireTXDs_STRING[22] = "mt_phone_image_4"

    TextMessagesThatRequireTXDs_LABEL[23] = "LR_PIC_TXT5"
    TextMessagesThatRequireTXDs_STRING[23] = "mt_phone_image_5"

	//New Biker target message hookups for TODO 3007036
	TextMessagesThatRequireTXDs_LABEL[24] = "SAD_SMS_0"
    TextMessagesThatRequireTXDs_STRING[24] = "MP_SND_TARGET_01"
	
	TextMessagesThatRequireTXDs_LABEL[25] = "SAD_SMS_1"
    TextMessagesThatRequireTXDs_STRING[25] = "MP_SND_TARGET_02"

	TextMessagesThatRequireTXDs_LABEL[26] = "SAD_SMS_2"
    TextMessagesThatRequireTXDs_STRING[26] = "MP_SND_TARGET_03"
	
	TextMessagesThatRequireTXDs_LABEL[27] = "SAD_SMS_3"
    TextMessagesThatRequireTXDs_STRING[27] = "MP_SND_TARGET_04"
	
	TextMessagesThatRequireTXDs_LABEL[28] = "SAD_SMS_4"
    TextMessagesThatRequireTXDs_STRING[28] = "MP_SND_TARGET_05"

	TextMessagesThatRequireTXDs_LABEL[29] = "SAD_SMS_5"
    TextMessagesThatRequireTXDs_STRING[29] = "MP_SND_TARGET_06"
	
	TextMessagesThatRequireTXDs_LABEL[30] = "SAD_SMS_6"
    TextMessagesThatRequireTXDs_STRING[30] = "MP_SND_TARGET_07"

	TextMessagesThatRequireTXDs_LABEL[31] = "SAD_SMS_7"
    TextMessagesThatRequireTXDs_STRING[31] = "MP_SND_TARGET_08"

	TextMessagesThatRequireTXDs_LABEL[32] = "SAD_SMS_8"
    TextMessagesThatRequireTXDs_STRING[32] = "MP_SND_TARGET_09"
	
	TextMessagesThatRequireTXDs_LABEL[33] = "SAD_SMS_9"
    TextMessagesThatRequireTXDs_STRING[33] = "MP_SND_TARGET_10"
	
	
	TextMessagesThatRequireTXDs_LABEL[34] = "SAD_SMS_10"
    TextMessagesThatRequireTXDs_STRING[34] = "MP_SND_TARGET_11"
	
	TextMessagesThatRequireTXDs_LABEL[35] = "SAD_SMS_11"
    TextMessagesThatRequireTXDs_STRING[35] = "MP_SND_TARGET_12"

	TextMessagesThatRequireTXDs_LABEL[36] = "SAD_SMS_12"
    TextMessagesThatRequireTXDs_STRING[36] = "MP_SND_TARGET_13"

	TextMessagesThatRequireTXDs_LABEL[37] = "SAD_SMS_13"
    TextMessagesThatRequireTXDs_STRING[37] = "MP_SND_TARGET_14"
	
	TextMessagesThatRequireTXDs_LABEL[38] = "SAD_SMS_14"
    TextMessagesThatRequireTXDs_STRING[38] = "MP_SND_TARGET_15"
	
	TextMessagesThatRequireTXDs_LABEL[39] = "SAD_SMS_15"
    TextMessagesThatRequireTXDs_STRING[39] = "MP_SND_TARGET_16"
	
	TextMessagesThatRequireTXDs_LABEL[40] = "SAD_SMS_16"
    TextMessagesThatRequireTXDs_STRING[40] = "MP_SND_TARGET_17"

	TextMessagesThatRequireTXDs_LABEL[41] = "SAD_SMS_17"
    TextMessagesThatRequireTXDs_STRING[41] = "MP_SND_TARGET_18"

	TextMessagesThatRequireTXDs_LABEL[42] = "SAD_SMS_18"
    TextMessagesThatRequireTXDs_STRING[42] = "MP_SND_TARGET_19"
	
	TextMessagesThatRequireTXDs_LABEL[43] = "SAD_SMS_19"
    TextMessagesThatRequireTXDs_STRING[43] = "MP_SND_TARGET_20"
	
	
	//Import Export Oct2016 hookups for 2882570
	TextMessagesThatRequireTXDs_LABEL[44] = "VEX_PM_PHOTO0"
    TextMessagesThatRequireTXDs_STRING[44] = "MP_EX_LOCATION_01"

	TextMessagesThatRequireTXDs_LABEL[45] = "VEX_PM_PHOTO1"
    TextMessagesThatRequireTXDs_STRING[45] = "MP_EX_LOCATION_02"

	TextMessagesThatRequireTXDs_LABEL[46] = "VEX_PM_PHOTO2"
    TextMessagesThatRequireTXDs_STRING[46] = "MP_EX_LOCATION_03"

	TextMessagesThatRequireTXDs_LABEL[47] = "VEX_PM_PHOTO3"
    TextMessagesThatRequireTXDs_STRING[47] = "MP_EX_LOCATION_04"
	
	TextMessagesThatRequireTXDs_LABEL[48] = "VEX_PM_PHOTO4"
    TextMessagesThatRequireTXDs_STRING[48] = "MP_EX_LOCATION_05"

	TextMessagesThatRequireTXDs_LABEL[49] = "VEX_PM_PHOTO5"
    TextMessagesThatRequireTXDs_STRING[49] = "MP_EX_LOCATION_06"

	TextMessagesThatRequireTXDs_LABEL[50] = "VEX_PM_PHOTO6"
    TextMessagesThatRequireTXDs_STRING[50] = "MP_EX_LOCATION_07"

	TextMessagesThatRequireTXDs_LABEL[51] = "VEX_PM_PHOTO7"
    TextMessagesThatRequireTXDs_STRING[51] = "MP_EX_LOCATION_08"

	TextMessagesThatRequireTXDs_LABEL[52] = "VEX_PM_PHOTO8"
    TextMessagesThatRequireTXDs_STRING[52] = "MP_EX_LOCATION_09"

	TextMessagesThatRequireTXDs_LABEL[53] = "VEX_PM_PHOTO9"
    TextMessagesThatRequireTXDs_STRING[53] = "MP_EX_LOCATION_10"
	
	TextMessagesThatRequireTXDs_LABEL[54] = "VEX_PM_PHOTO10"
    TextMessagesThatRequireTXDs_STRING[54] = "MP_EX_LOCATION_11"

	TextMessagesThatRequireTXDs_LABEL[55] = "VEX_PM_PHOTO11"
    TextMessagesThatRequireTXDs_STRING[55] = "MP_EX_LOCATION_12"
	
	TextMessagesThatRequireTXDs_LABEL[56] = "VEX_PM_PHOTO12"
    TextMessagesThatRequireTXDs_STRING[56] = "MP_EX_LOCATION_13"

	TextMessagesThatRequireTXDs_LABEL[57] = "VEX_PM_PHOTO13"
    TextMessagesThatRequireTXDs_STRING[57] = "MP_EX_LOCATION_14"
	
	
	//3178567 - Import Export Car List.
	//Proto tested OK.  Num check.
	TextMessagesThatRequireTXDs_LABEL[58] = "VEX_1_PROTO"
    TextMessagesThatRequireTXDs_STRING[58] = "IE_TEXTVECH_01"
	
	TextMessagesThatRequireTXDs_LABEL[59] = "VEX_2_PROTO"
    TextMessagesThatRequireTXDs_STRING[59] = "IE_TEXTVECH_02"
	
	TextMessagesThatRequireTXDs_LABEL[60] = "VEX_3_PROTO"
    TextMessagesThatRequireTXDs_STRING[60] = "IE_TEXTVECH_03"
	
	// Tyrus tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[61] = "VEX_4_TYRUS"
    TextMessagesThatRequireTXDs_STRING[61] = "IE_TEXTVECH_04"
	
	TextMessagesThatRequireTXDs_LABEL[62] = "VEX_5_TYRUS"
    TextMessagesThatRequireTXDs_STRING[62] = "IE_TEXTVECH_05"
	
	TextMessagesThatRequireTXDs_LABEL[63] = "VEX_6_TYRUS"
    TextMessagesThatRequireTXDs_STRING[63] = "IE_TEXTVECH_06"
	
	//Bestia tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[64] = "VEX_7_BESTIA"
    TextMessagesThatRequireTXDs_STRING[64] = "IE_TEXTVECH_07"
	
	TextMessagesThatRequireTXDs_LABEL[65] = "VEX_8_BESTIA"
    TextMessagesThatRequireTXDs_STRING[65] = "IE_TEXTVECH_08"
	
	TextMessagesThatRequireTXDs_LABEL[66] = "VEX_9_BESTIA"
    TextMessagesThatRequireTXDs_STRING[66] = "IE_TEXTVECH_09"
	
	
	//T20 tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[67] = "VEX_10_T20"
    TextMessagesThatRequireTXDs_STRING[67] = "IE_TEXTVECH_10"
	
	TextMessagesThatRequireTXDs_LABEL[68] = "VEX_11_T20"
    TextMessagesThatRequireTXDs_STRING[68] = "IE_TEXTVECH_11"
	
	TextMessagesThatRequireTXDs_LABEL[69] = "VEX_12_T20"
    TextMessagesThatRequireTXDs_STRING[69] = "IE_TEXTVECH_12"
	
	
	//Sheava tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[70] = "VEX_13_SHEAVA"
    TextMessagesThatRequireTXDs_STRING[70] = "IE_TEXTVECH_13"
	
	TextMessagesThatRequireTXDs_LABEL[71] = "VEX_14_SHEAVA"
    TextMessagesThatRequireTXDs_STRING[71] = "IE_TEXTVECH_14"
	
	TextMessagesThatRequireTXDs_LABEL[72] = "VEX_15_SHEAVA"
    TextMessagesThatRequireTXDs_STRING[72] = "IE_TEXTVECH_15"
	
	
	//Osiris tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[73] = "VEX_16_OSIRIS"
    TextMessagesThatRequireTXDs_STRING[73] = "IE_TEXTVECH_16"
	
	TextMessagesThatRequireTXDs_LABEL[74] = "VEX_17_OSIRIS"
    TextMessagesThatRequireTXDs_STRING[74] = "IE_TEXTVECH_17"
	
	TextMessagesThatRequireTXDs_LABEL[75] = "VEX_18_OSIRIS"
    TextMessagesThatRequireTXDs_STRING[75] = "IE_TEXTVECH_18"
	
	//Tested FMJ OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[76] = "VEX_19_FMJ"
    TextMessagesThatRequireTXDs_STRING[76] = "IE_TEXTVECH_19"
	
	TextMessagesThatRequireTXDs_LABEL[77] = "VEX_20_FMJ"
    TextMessagesThatRequireTXDs_STRING[77] = "IE_TEXTVECH_20"
	
	TextMessagesThatRequireTXDs_LABEL[78] = "VEX_21_FMJ"
    TextMessagesThatRequireTXDs_STRING[78] = "IE_TEXTVECH_21"
	
	
	//Tested Reaper OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[79] = "VEX_22_REAPER"
    TextMessagesThatRequireTXDs_STRING[79] = "IE_TEXTVECH_22"
	
	TextMessagesThatRequireTXDs_LABEL[80] = "VEX_23_REAPER"
    TextMessagesThatRequireTXDs_STRING[80] = "IE_TEXTVECH_23"
	
	TextMessagesThatRequireTXDs_LABEL[81] = "VEX_24_REAPER"
    TextMessagesThatRequireTXDs_STRING[81] = "IE_TEXTVECH_24"
	
	//Tested Pfister OK.  Num check.
	TextMessagesThatRequireTXDs_LABEL[82] = "VEX_25_PFISTER"
    TextMessagesThatRequireTXDs_STRING[82] = "IE_TEXTVECH_25"
	
	TextMessagesThatRequireTXDs_LABEL[83] = "VEX_26_PFISTER"
    TextMessagesThatRequireTXDs_STRING[83] = "IE_TEXTVECH_26"
	
	TextMessagesThatRequireTXDs_LABEL[84] = "VEX_27_PFISTER"
    TextMessagesThatRequireTXDs_STRING[84] = "IE_TEXTVECH_27"
	
	
	
	//Test Alpha OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[85] = "VEX_28_ALPHA"
    TextMessagesThatRequireTXDs_STRING[85] = "IE_TEXTVECH_28"
	
	TextMessagesThatRequireTXDs_LABEL[86] = "VEX_29_ALPHA"
    TextMessagesThatRequireTXDs_STRING[86] = "IE_TEXTVECH_29"
	
	TextMessagesThatRequireTXDs_LABEL[87] = "VEX_30_ALPHA"
    TextMessagesThatRequireTXDs_STRING[87] = "IE_TEXTVECH_30"
	
	//Tested Mamba OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[88] = "VEX_31_MAMBA"
    TextMessagesThatRequireTXDs_STRING[88] = "IE_TEXTVECH_31"
	
	TextMessagesThatRequireTXDs_LABEL[89] = "VEX_32_MAMBA"
    TextMessagesThatRequireTXDs_STRING[89] = "IE_TEXTVECH_32"
	
	TextMessagesThatRequireTXDs_LABEL[90] = "VEX_33_MAMBA"
    TextMessagesThatRequireTXDs_STRING[90] = "IE_TEXTVECH_33"
	
	//Tested Tampa OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[91] = "VEX_34_TAMPA"
    TextMessagesThatRequireTXDs_STRING[91] = "IE_TEXTVECH_34"
	
	TextMessagesThatRequireTXDs_LABEL[92] = "VEX_35_TAMPA"
    TextMessagesThatRequireTXDs_STRING[92] = "IE_TEXTVECH_35"
	
	TextMessagesThatRequireTXDs_LABEL[93] = "VEX_36_TAMPA"
    TextMessagesThatRequireTXDs_STRING[93] = "IE_TEXTVECH_36"
	
	//Tested Btype OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[94] = "VEX_37_BTYPE"
    TextMessagesThatRequireTXDs_STRING[94] = "IE_TEXTVECH_37"
	
	TextMessagesThatRequireTXDs_LABEL[95] = "VEX_38_BTYPE"
    TextMessagesThatRequireTXDs_STRING[95] = "IE_TEXTVECH_38"
	
	TextMessagesThatRequireTXDs_LABEL[96] = "VEX_39_BTYPE"
    TextMessagesThatRequireTXDs_STRING[96] = "IE_TEXTVECH_39"
	
	
	//Tested Feltz OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[97] = "VEX_40_FELTZ3"
    TextMessagesThatRequireTXDs_STRING[97] = "IE_TEXTVECH_40"
	
	TextMessagesThatRequireTXDs_LABEL[98] = "VEX_41_FELTZ3"
    TextMessagesThatRequireTXDs_STRING[98] = "IE_TEXTVECH_41"
	
	TextMessagesThatRequireTXDs_LABEL[99] = "VEX_42_FELTZ3"
    TextMessagesThatRequireTXDs_STRING[99] = "IE_TEXTVECH_42"
	
	//Tested Ztype OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[100] = "VEX_43_ZTYPE"
    TextMessagesThatRequireTXDs_STRING[100] = "IE_TEXTVECH_43"
	
	TextMessagesThatRequireTXDs_LABEL[101] = "VEX_44_ZTYPE"
    TextMessagesThatRequireTXDs_STRING[101] = "IE_TEXTVECH_44"
	
	TextMessagesThatRequireTXDs_LABEL[102] = "VEX_45_ZTYPE"
    TextMessagesThatRequireTXDs_STRING[102] = "IE_TEXTVECH_45"
	
	//Tested Tropos OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[103] = "VEX_46_TROPOS"
    TextMessagesThatRequireTXDs_STRING[103] = "IE_TEXTVECH_46"
	
	TextMessagesThatRequireTXDs_LABEL[104] = "VEX_47_TROPOS"
    TextMessagesThatRequireTXDs_STRING[104] = "IE_TEXTVECH_47"
	
	TextMessagesThatRequireTXDs_LABEL[105] = "VEX_48_TROPOS"
    TextMessagesThatRequireTXDs_STRING[105] = "IE_TEXTVECH_48"
	
	
	//Tested Entity XF OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[106] = "VEX_49_ENTITYXF"
    TextMessagesThatRequireTXDs_STRING[106] = "IE_TEXTVECH_49"
	
	TextMessagesThatRequireTXDs_LABEL[107] = "VEX_50_ENTITYXF"
    TextMessagesThatRequireTXDs_STRING[107] = "IE_TEXTVECH_50"
	
	TextMessagesThatRequireTXDs_LABEL[108] = "VEX_51_ENTITYXF"
    TextMessagesThatRequireTXDs_STRING[108] = "IE_TEXTVECH_51"
	
	
	//Tested Sultan OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[109] = "VEX_52_SULTANRS"
    TextMessagesThatRequireTXDs_STRING[109] = "IE_TEXTVECH_52"
	
	TextMessagesThatRequireTXDs_LABEL[110] = "VEX_53_SULTANRS"
    TextMessagesThatRequireTXDs_STRING[110] = "IE_TEXTVECH_53"
	
	TextMessagesThatRequireTXDs_LABEL[111] = "VEX_54_SULTANRS"
    TextMessagesThatRequireTXDs_STRING[111] = "IE_TEXTVECH_54"
	
	
	//Tested Zentorno OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[112] = "VEX_55_ZENTORNO"
    TextMessagesThatRequireTXDs_STRING[112] = "IE_TEXTVECH_55"
	
	TextMessagesThatRequireTXDs_LABEL[113] = "VEX_56_ZENTORNO"
    TextMessagesThatRequireTXDs_STRING[113] = "IE_TEXTVECH_56"
	
	TextMessagesThatRequireTXDs_LABEL[114] = "VEX_57_ZENTORNO"
    TextMessagesThatRequireTXDs_STRING[114] = "IE_TEXTVECH_57"
	
	
	//Tested Omnis OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[115] = "VEX_58_OMNIS"
    TextMessagesThatRequireTXDs_STRING[115] = "IE_TEXTVECH_58"
	
	TextMessagesThatRequireTXDs_LABEL[116] = "VEX_59_OMNIS"
    TextMessagesThatRequireTXDs_STRING[116] = "IE_TEXTVECH_59"
	
	TextMessagesThatRequireTXDs_LABEL[117] = "VEX_60_OMNIS"
    TextMessagesThatRequireTXDs_STRING[117] = "IE_TEXTVECH_60"
	
	
	//Tested Coquet3 OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[118] = "VEX_61_COQUET3"
    TextMessagesThatRequireTXDs_STRING[118] = "IE_TEXTVECH_61"
	
	TextMessagesThatRequireTXDs_LABEL[119] = "VEX_62_COQUET3"
    TextMessagesThatRequireTXDs_STRING[119] = "IE_TEXTVECH_62"
	
	TextMessagesThatRequireTXDs_LABEL[120] = "VEX_63_COQUET3"
    TextMessagesThatRequireTXDs_STRING[120] = "IE_TEXTVECH_63"
	
	
	//Tested Seven70 OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[121] = "VEX_64_SEVEN70"
    TextMessagesThatRequireTXDs_STRING[121] = "IE_TEXTVECH_64"
	
	TextMessagesThatRequireTXDs_LABEL[122] = "VEX_65_SEVEN70"
    TextMessagesThatRequireTXDs_STRING[122] = "IE_TEXTVECH_65"
	
	TextMessagesThatRequireTXDs_LABEL[123] = "VEX_66_SEVEN70"
    TextMessagesThatRequireTXDs_STRING[123] = "IE_TEXTVECH_66"
	
	
	
	//Tested Verli OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[124] = "VEX_67_VERLI"
    TextMessagesThatRequireTXDs_STRING[124] = "IE_TEXTVECH_67"
	
	TextMessagesThatRequireTXDs_LABEL[125] = "VEX_68_VERLI"
    TextMessagesThatRequireTXDs_STRING[125] = "IE_TEXTVECH_68"
	
	TextMessagesThatRequireTXDs_LABEL[126] = "VEX_69_VERLI"
    TextMessagesThatRequireTXDs_STRING[126] = "IE_TEXTVECH_69"
	
	
	//tested Feltzer 2 OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[127] = "VEX_70_FELTZ2"
    TextMessagesThatRequireTXDs_STRING[127] = "IE_TEXTVECH_70"
	
	TextMessagesThatRequireTXDs_LABEL[128] = "VEX_71_FELTZ2"
    TextMessagesThatRequireTXDs_STRING[128] = "IE_TEXTVECH_71"
	
	TextMessagesThatRequireTXDs_LABEL[129] = "VEX_72_FELTZ2"
    TextMessagesThatRequireTXDs_STRING[129] = "IE_TEXTVECH_72"
	
	
	//Tested Coquet2 OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[130] = "VEX_73_COQUET2"
    TextMessagesThatRequireTXDs_STRING[130] = "IE_TEXTVECH_73"
	
	TextMessagesThatRequireTXDs_LABEL[131] = "VEX_74_COQUET2"
    TextMessagesThatRequireTXDs_STRING[131] = "IE_TEXTVECH_74"
	
	TextMessagesThatRequireTXDs_LABEL[132] = "VEX_75_COQUET2"
    TextMessagesThatRequireTXDs_STRING[132] = "IE_TEXTVECH_75"
	
	
	//Tested Cheetah OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[133] = "VEX_76_CHEETAH"
    TextMessagesThatRequireTXDs_STRING[133] = "IE_TEXTVECH_76"
	
	TextMessagesThatRequireTXDs_LABEL[134] = "VEX_77_CHEETAH"
    TextMessagesThatRequireTXDs_STRING[134] = "IE_TEXTVECH_77"
	
	TextMessagesThatRequireTXDs_LABEL[135] = "VEX_78_CHEETAH"
    TextMessagesThatRequireTXDs_STRING[135] = "IE_TEXTVECH_78"
	
	
	//Tested Nightshade Nshade OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[136] = "VEX_79_NSHADE"
    TextMessagesThatRequireTXDs_STRING[136] = "IE_TEXTVECH_79"
	
	TextMessagesThatRequireTXDs_LABEL[137] = "VEX_80_NSHADE"
    TextMessagesThatRequireTXDs_STRING[137] = "IE_TEXTVECH_80"
	
	TextMessagesThatRequireTXDs_LABEL[138] = "VEX_81_NSHADE"
    TextMessagesThatRequireTXDs_STRING[138] = "IE_TEXTVECH_81"
	
	
	//Weird syncing here for the Banshee - put as per xl sheet attached to bug 3180921. Num check.
	TextMessagesThatRequireTXDs_LABEL[139] = "VEX_82_BANSH2" //Tested OK.
    TextMessagesThatRequireTXDs_STRING[139] = "IE_TEXTVECH_82"
	
	TextMessagesThatRequireTXDs_LABEL[140] = "VEX_83_BANSH2" //Tested OK
    TextMessagesThatRequireTXDs_STRING[140] = "IE_TEXTVECH_84" //This is correct, Should be 84.
	
	TextMessagesThatRequireTXDs_LABEL[141] = "VEX_84_BANSH2" //Tested OK
    TextMessagesThatRequireTXDs_STRING[141] = "IE_TEXTVECH_85" //This is correct. Should be 85,
	
	
		
	//Weird syncing here for the Turismo R - put as per xl sheet attached to bug 3180921. Num check.
	TextMessagesThatRequireTXDs_LABEL[142] = "VEX_82_TURIS" //This label is right. You'd think it should be 85 but someone arsed up somewhere.
    TextMessagesThatRequireTXDs_STRING[142] = "IE_TEXTVECH_83" //Tested OK. This is meant to be 83.
	
	TextMessagesThatRequireTXDs_LABEL[143] = "VEX_86_TURIS" //Tested OK
    TextMessagesThatRequireTXDs_STRING[143] = "IE_TEXTVECH_86"
	
	TextMessagesThatRequireTXDs_LABEL[144] = "VEX_87_TURIS" //Tested OK
    TextMessagesThatRequireTXDs_STRING[144] = "IE_TEXTVECH_87"
	
	//Tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[145] = "VEX_88_MASS"
    TextMessagesThatRequireTXDs_STRING[145] = "IE_TEXTVECH_88"
	
	TextMessagesThatRequireTXDs_LABEL[146] = "VEX_89_MASS"
    TextMessagesThatRequireTXDs_STRING[146] = "IE_TEXTVECH_89"
	
	TextMessagesThatRequireTXDs_LABEL[147] = "VEX_90_MASS"
    TextMessagesThatRequireTXDs_STRING[147] = "IE_TEXTVECH_90"
	
	//Tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[148] = "VEX_91_SABRE2"
    TextMessagesThatRequireTXDs_STRING[148] = "IE_TEXTVECH_91"
	
	TextMessagesThatRequireTXDs_LABEL[149] = "VEX_92_SABRE2"
    TextMessagesThatRequireTXDs_STRING[149] = "IE_TEXTVECH_92"
	
	TextMessagesThatRequireTXDs_LABEL[150] = "VEX_93_SABRE2"
    TextMessagesThatRequireTXDs_STRING[150] = "IE_TEXTVECH_93"
	
	//Tested OK. Num check.
	TextMessagesThatRequireTXDs_LABEL[151] = "VEX_94_JESTER"
    TextMessagesThatRequireTXDs_STRING[151] = "IE_TEXTVECH_94"
	
	TextMessagesThatRequireTXDs_LABEL[152] = "VEX_95_JESTER"
    TextMessagesThatRequireTXDs_STRING[152] = "IE_TEXTVECH_95"
	
	TextMessagesThatRequireTXDs_LABEL[153] = "VEX_96_JESTER"
    TextMessagesThatRequireTXDs_STRING[153] = "IE_TEXTVECH_96"
	
	// Gang ops
	TextMessagesThatRequireTXDs_LABEL[154] = "GO_AS_PICM"
    TextMessagesThatRequireTXDs_STRING[154] = "NHP_prep_autosalvage"
	
	TextMessagesThatRequireTXDs_LABEL[155] = "GO_DR_PICM"
    TextMessagesThatRequireTXDs_STRING[155] = "nhp_prep_daylightrob"
	
	TextMessagesThatRequireTXDs_LABEL[156] = "FHTXT_DDR00M"
    TextMessagesThatRequireTXDs_STRING[156] = "NHP_prep_deaddrop"
	
	TextMessagesThatRequireTXDs_LABEL[157] = "BBTXT_CPS00M"
    TextMessagesThatRequireTXDs_STRING[157] = "BAT_carpark_1"
	
	TextMessagesThatRequireTXDs_LABEL[158] = "BBTXT_CPS10M"
    TextMessagesThatRequireTXDs_STRING[158] = "BAT_carpark_2"
	
	TextMessagesThatRequireTXDs_LABEL[159] = "BBTXT_CPS20M"
    TextMessagesThatRequireTXDs_STRING[159] = "BAT_carpark_3"
	
	//Superyacht Missions
	TextMessagesThatRequireTXDs_LABEL[160] = "ICEBR_EMAIL"
    TextMessagesThatRequireTXDs_STRING[160] = "ib_aircon"
	
	// Business Battles
	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = MBV_SNAPMATIC
	
		
		// MBSV_SNAPMATIC_0
		TextMessagesThatRequireTXDs_LABEL[44] = "SNP_IMG_0_0"
	    TextMessagesThatRequireTXDs_STRING[44] = "SCLUB_1"		//"SCLUB_DEL_PERRO_PIER_1_2"
																
		TextMessagesThatRequireTXDs_LABEL[45] = "SNP_IMG_0_1"   
	    TextMessagesThatRequireTXDs_STRING[45] = "SCLUB_1"		//"SCLUB_ORIENTAL_THEATER_1_6"
																
		TextMessagesThatRequireTXDs_LABEL[46] = "SNP_IMG_0_2"   
	    TextMessagesThatRequireTXDs_STRING[46] = "SCLUB_1"		//"SCLUB_GALILEO_OBSERVATORY_1_3"
																
		TextMessagesThatRequireTXDs_LABEL[47] = "SNP_IMG_0_3"   
	    TextMessagesThatRequireTXDs_STRING[47] = "SCLUB_1"		//"SCLUB_MAZE_BANK_ARENA_1_4"
																
		TextMessagesThatRequireTXDs_LABEL[48] = "SNP_IMG_0_4"   
	    TextMessagesThatRequireTXDs_STRING[48] = "SCLUB_1"		//"SCLUB_CHUMASH_PIER_1_1"
																
		TextMessagesThatRequireTXDs_LABEL[49] = "SNP_IMG_0_5"   
	    TextMessagesThatRequireTXDs_STRING[49] = "SCLUB_1"		//"SCLUB_RICHMAN_HOTEL_1_8"
																
		TextMessagesThatRequireTXDs_LABEL[50] = "SNP_IMG_0_6"   
	    TextMessagesThatRequireTXDs_STRING[50] = "SCLUB_1"		//"SCLUB_ROCKFORD_HILLS_CITY_HALL_1_9"
																
		TextMessagesThatRequireTXDs_LABEL[51] = "SNP_IMG_0_7"   
	    TextMessagesThatRequireTXDs_STRING[51] = "SCLUB_1"		//"SCLUB_MAZE_BANK_TOWER_1_5"
																
		TextMessagesThatRequireTXDs_LABEL[52] = "SNP_IMG_0_8"   
	    TextMessagesThatRequireTXDs_STRING[52] = "SCLUB_1"		//"SCLUB_PIPELINE_INN_1_7"
																
		TextMessagesThatRequireTXDs_LABEL[53] = "SNP_IMG_0_9"   
	    TextMessagesThatRequireTXDs_STRING[53] = "SCLUB_1"		//"SCLUB_THE_VICEROY_HOTEL_1_10"


		// MBSV_SNAPMATIC_1
		TextMessagesThatRequireTXDs_LABEL[54] = "SNP_IMG_1_0"
	    TextMessagesThatRequireTXDs_STRING[54] = "SCLUB_2"		//"SCLUB_VENETIAN_2_7"
																
		TextMessagesThatRequireTXDs_LABEL[55] = "SNP_IMG_1_1"   
	    TextMessagesThatRequireTXDs_STRING[55] = "SCLUB_2"		//"SCLUB_OPIUM_NIGHTS_HOTEL_2_4"
																
		TextMessagesThatRequireTXDs_LABEL[56] = "SNP_IMG_1_2"   
	    TextMessagesThatRequireTXDs_STRING[56] = "SCLUB_2"		//"SCLUB_VINEWOOD_SIGN_2_8"
																
		TextMessagesThatRequireTXDs_LABEL[57] = "SNP_IMG_1_3"   
	    TextMessagesThatRequireTXDs_STRING[57] = "SCLUB_2"		//"SCLUB_LEGION_SQUARE_2_2"
																
		TextMessagesThatRequireTXDs_LABEL[58] = "SNP_IMG_1_4"   
	    TextMessagesThatRequireTXDs_STRING[58] = "SCLUB_2"		//"SCLUB_VON_CRASTENBURG_HOTEL_2_10"
																
		TextMessagesThatRequireTXDs_LABEL[59] = "SNP_IMG_1_5"   
	    TextMessagesThatRequireTXDs_STRING[59] = "SCLUB_2"		//"SCLUB_VON-CRASTENBURG-RICHMAN_2_9"
																
		TextMessagesThatRequireTXDs_LABEL[60] = "SNP_IMG_1_6"   
	    TextMessagesThatRequireTXDs_STRING[60] = "SCLUB_2"		//"SCLUB_RANCHO_TOWERS_2_5"
																
		TextMessagesThatRequireTXDs_LABEL[61] = "SNP_IMG_1_7"   
	    TextMessagesThatRequireTXDs_STRING[61] = "SCLUB_2"		//"SCLUB_MIRROR_PARK_2_3"
																
		TextMessagesThatRequireTXDs_LABEL[62] = "SNP_IMG_1_8"   
	    TextMessagesThatRequireTXDs_STRING[62] = "SCLUB_2"		//"SCLUB_BJ_SMITH_REC_CENTER_2_1"
																
		TextMessagesThatRequireTXDs_LABEL[64] = "SNP_IMG_1_9"   
	    TextMessagesThatRequireTXDs_STRING[64] = "SCLUB_2"		//"SCLUB_ROCKFORD_DORSET_HOTEL_2_6"
	
		// MBSV_SNAPMATIC_2
		TextMessagesThatRequireTXDs_LABEL[65] = "SNP_IMG_2_0"
	    TextMessagesThatRequireTXDs_STRING[65] = "SCLUB_3"		//"SCLUB_KOREAN_PAVILLION_3_1"
																
		TextMessagesThatRequireTXDs_LABEL[66] = "SNP_IMG_2_1"   
	    TextMessagesThatRequireTXDs_STRING[66] = "SCLUB_3"		//"SCLUB_VESPUCCI_BEACH_3_6"
																
		TextMessagesThatRequireTXDs_LABEL[67] = "SNP_IMG_2_2"   
	    TextMessagesThatRequireTXDs_STRING[67] = "SCLUB_3"		//"SCLUB_VINEWOOD_RACETRACK_3_9"
																
		TextMessagesThatRequireTXDs_LABEL[68] = "SNP_IMG_2_3"   
	    TextMessagesThatRequireTXDs_STRING[68] = "SCLUB_3"		//"SCLUB_SISYPHUS_THEATER_3_5"
																
		TextMessagesThatRequireTXDs_LABEL[69] = "SNP_IMG_2_4"   
	    TextMessagesThatRequireTXDs_STRING[69] = "SCLUB_3"		//"SCLUB_VESPUCCI_BOULEVARD_BRIDGE_3_7"
																
		TextMessagesThatRequireTXDs_LABEL[70] = "SNP_IMG_2_5"   
	    TextMessagesThatRequireTXDs_STRING[70] = "SCLUB_3"		//"SCLUB_ROCKFORD_HILLS_CHURCH_3_4"
																
		TextMessagesThatRequireTXDs_LABEL[71] = "SNP_IMG_2_6"   
	    TextMessagesThatRequireTXDs_STRING[71] = "SCLUB_3"		//"SCLUB_LAST_TRAIN_DINER_3_2"
																
		TextMessagesThatRequireTXDs_LABEL[72] = "SNP_IMG_2_7"   
	    TextMessagesThatRequireTXDs_STRING[72] = "SCLUB_3"		//"SCLUB_VON-CRASTENBURG-HOTEL_3_10"
																
		TextMessagesThatRequireTXDs_LABEL[73] = "SNP_IMG_2_8"   
	    TextMessagesThatRequireTXDs_STRING[73] = "SCLUB_3"		//"SCLUB_METEOR_STREET_APARTMENTS_3_3"
																
		TextMessagesThatRequireTXDs_LABEL[74] = "SNP_IMG_2_9"   
	    TextMessagesThatRequireTXDs_STRING[74] = "SCLUB_3"		//"SCLUB_VINEWOOD_HILLS_3_8"
		
	ELIF GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = CSV_TRACKING_CHIPS
	
		// CSS_TC_VARIATION_1
		TextMessagesThatRequireTXDs_LABEL[44] = "GBC_TM_TC100"
	    TextMessagesThatRequireTXDs_STRING[44] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[45] = "GBC_TM_TC101"   
	    TextMessagesThatRequireTXDs_STRING[45] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[46] = "GBC_TM_TC102"   
	    TextMessagesThatRequireTXDs_STRING[46] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[47] = "GBC_TM_TC103"   
	    TextMessagesThatRequireTXDs_STRING[47] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[48] = "GBC_TM_TC104"   
	    TextMessagesThatRequireTXDs_STRING[48] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[49] = "GBC_TM_TC105"   
	    TextMessagesThatRequireTXDs_STRING[49] = "VC_TC_OBSERVCCTV_0"
																
		TextMessagesThatRequireTXDs_LABEL[50] = "GBC_TM_TC110"   
	    TextMessagesThatRequireTXDs_STRING[50] = "VC_TC_VEHICLECCTV"
		
		TextMessagesThatRequireTXDs_LABEL[51] = "GBC_TM_TC111"   
	    TextMessagesThatRequireTXDs_STRING[51] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[52] = "GBC_TM_TC120"   
	    TextMessagesThatRequireTXDs_STRING[52] = "VC_TC_VEHICLECCTV"
		
		TextMessagesThatRequireTXDs_LABEL[53] = "GBC_TM_TC121"   
	    TextMessagesThatRequireTXDs_STRING[53] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[54] = "GBC_TM_TC130"   
	    TextMessagesThatRequireTXDs_STRING[54] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[55] = "GBC_TM_TC140"   
	    TextMessagesThatRequireTXDs_STRING[55] = "VC_TC_CASINOCCTV"


		// CSS_TC_VARIATION_2
		TextMessagesThatRequireTXDs_LABEL[56] = "GBC_TM_TC200"
	    TextMessagesThatRequireTXDs_STRING[56] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[57] = "GBC_TM_TC201"   
	    TextMessagesThatRequireTXDs_STRING[57] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[58] = "GBC_TM_TC202"   
	    TextMessagesThatRequireTXDs_STRING[58] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[59] = "GBC_TM_TC203"   
	    TextMessagesThatRequireTXDs_STRING[59] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[60] = "GBC_TM_TC204"   
	    TextMessagesThatRequireTXDs_STRING[60] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[61] = "GBC_TM_TC205"   
	    TextMessagesThatRequireTXDs_STRING[61] = "VC_TC_UNIVERCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[62] = "GBC_TM_TC210"   
	    TextMessagesThatRequireTXDs_STRING[62] = "VC_TC_VEHICLECCTV"
			
		TextMessagesThatRequireTXDs_LABEL[63] = "GBC_TM_TC211"   
	    TextMessagesThatRequireTXDs_STRING[63] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[64] = "GBC_TM_TC220"   
	    TextMessagesThatRequireTXDs_STRING[64] = "VC_TC_VEHICLECCTV"
		
		TextMessagesThatRequireTXDs_LABEL[65] = "GBC_TM_TC221"   
	    TextMessagesThatRequireTXDs_STRING[65] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[66] = "GBC_TM_TC230"   
	    TextMessagesThatRequireTXDs_STRING[66] = "VC_TC_CASINOCCTV"
																
		TextMessagesThatRequireTXDs_LABEL[67] = "GBC_TM_TC240"   
	    TextMessagesThatRequireTXDs_STRING[67] = "VC_TC_CASINOCCTV"
	
	ELIF GB_GET_CASINO_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = CHV_INSIDE_MAID
	
		// CHS_IMM_VARIATION_1
		TextMessagesThatRequireTXDs_LABEL[44] = "CSH_TXT_MD_01M"
	    TextMessagesThatRequireTXDs_STRING[44] = "heist3_prep_maid01"
		
		// CHS_IMM_VARIATION_1														
		TextMessagesThatRequireTXDs_LABEL[45] = "CSH_TXT_MD_02M"   
	    TextMessagesThatRequireTXDs_STRING[45] = "heist3_prep_maid02"
		
	ELIF GB_GET_CASINO_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = CHV_INSIDE_CASHIER
	
		TextMessagesThatRequireTXDs_LABEL[44] = "CSHT_IMC_00M"
	    TextMessagesThatRequireTXDs_STRING[44] = "heist3_cashier1"
												
		TextMessagesThatRequireTXDs_LABEL[45] = "CSHT_IMC_10M"   
	    TextMessagesThatRequireTXDs_STRING[45] = "heist3_cashier2"
		
		TextMessagesThatRequireTXDs_LABEL[46] = "CSHT_IMC_20M"   
	    TextMessagesThatRequireTXDs_STRING[46] = "heist3_cashier3"
		
		TextMessagesThatRequireTXDs_LABEL[47] = "CSHT_IMC_30M"   
	    TextMessagesThatRequireTXDs_STRING[47] = "heist3_cashier4"
			
	#IF FEATURE_COPS_N_CROOKS
	ELIF GB_GET_MISSION_VARIATION_PLAYER_IS_ON(PLAYER_ID(), FMMC_TYPE_DISPATCH) = ENUM_TO_INT(DPV_PERSON_OF_INTEREST)
		
		TextMessagesThatRequireTXDs_LABEL[44] = "DP_TXT_IMG_0"
	    TextMessagesThatRequireTXDs_STRING[44] = "BAT_CASE_0"
		
		TextMessagesThatRequireTXDs_LABEL[45] = "DP_TXT_IMG_1"
	    TextMessagesThatRequireTXDs_STRING[45] = "BAT_CASE_1"
		
		TextMessagesThatRequireTXDs_LABEL[46] = "DP_TXT_IMG_2"
	    TextMessagesThatRequireTXDs_STRING[46] = "BAT_CASE_2"
		
		TextMessagesThatRequireTXDs_LABEL[47] = "DP_TXT_IMG_3"
	    TextMessagesThatRequireTXDs_STRING[47] = "BAT_CASE_3"
		
		TextMessagesThatRequireTXDs_LABEL[48] = "DP_TXT_IMG_4"
	    TextMessagesThatRequireTXDs_STRING[48] = "BAT_CASE_4"
		
		TextMessagesThatRequireTXDs_LABEL[49] = "DP_TXT_IMG_5"
	    TextMessagesThatRequireTXDs_STRING[49] = "BAT_CASE_5"
		
		TextMessagesThatRequireTXDs_LABEL[50] = "DP_TXT_IMG_6"
	    TextMessagesThatRequireTXDs_STRING[50] = "BAT_CASE_6"
		
		TextMessagesThatRequireTXDs_LABEL[51] = "DP_TXT_IMG_7"
	    TextMessagesThatRequireTXDs_STRING[51] = "BAT_CASE_7"
		
		TextMessagesThatRequireTXDs_LABEL[52] = "DP_TXT_IMG_8"
	    TextMessagesThatRequireTXDs_STRING[52] = "BAT_CASE_8"
		
		TextMessagesThatRequireTXDs_LABEL[53] = "DP_TXT_IMG_9"
	    TextMessagesThatRequireTXDs_STRING[53] = "BAT_CASE_9"
		
		TextMessagesThatRequireTXDs_LABEL[54] = "DP_TXT_IMG_10"
	    TextMessagesThatRequireTXDs_STRING[54] = "BAT_CASE_10"
		
		TextMessagesThatRequireTXDs_LABEL[55] = "DP_TXT_IMG_11"
	    TextMessagesThatRequireTXDs_STRING[55] = "BAT_CASE_11"
		
		TextMessagesThatRequireTXDs_LABEL[56] = "DP_TXT_IMG_12"
	    TextMessagesThatRequireTXDs_STRING[56] = "BAT_CASE_12"
		
		TextMessagesThatRequireTXDs_LABEL[57] = "DP_TXT_IMG_13"
	    TextMessagesThatRequireTXDs_STRING[57] = "BAT_CASE_13"
		
		TextMessagesThatRequireTXDs_LABEL[58] = "DP_TXT_IMG_14"
	    TextMessagesThatRequireTXDs_STRING[58] = "BAT_CASE_14"
		
		TextMessagesThatRequireTXDs_LABEL[59] = "DP_TXT_IMG_15"
	    TextMessagesThatRequireTXDs_STRING[59] = "BAT_CASE_15"
		
		TextMessagesThatRequireTXDs_LABEL[60] = "DP_TXT_IMG_16"
	    TextMessagesThatRequireTXDs_STRING[60] = "BAT_CASE_16"
		
		TextMessagesThatRequireTXDs_LABEL[61] = "DP_TXT_IMG_17"
	    TextMessagesThatRequireTXDs_STRING[61] = "BAT_CASE_17"
		
		TextMessagesThatRequireTXDs_LABEL[62] = "DP_TXT_IMG_18"
	    TextMessagesThatRequireTXDs_STRING[62] = "BAT_CASE_18"
		
		TextMessagesThatRequireTXDs_LABEL[63] = "DP_TXT_IMG_19"
	    TextMessagesThatRequireTXDs_STRING[63] = "BAT_CASE_19"
		
		TextMessagesThatRequireTXDs_LABEL[64] = "DP_TXT_IMG_20"
	    TextMessagesThatRequireTXDs_STRING[64] = "ARC1_POI_01"
		
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	ELIF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SAFE_CODES
		
		TextMessagesThatRequireTXDs_LABEL[44] = "ILHTEXT_SFE00M"
	    TextMessagesThatRequireTXDs_STRING[44] = "heist4_sec_guard"
	#ENDIF //FEATURE_HEIST_ISLAND
	
	#IF FEATURE_TUNER
	ELIF GB_GET_TUNER_ROBBERY_PREP_PLAYER_IS_ON(PLAYER_ID()) = TRV_COMPUTER_VIRUS
		
		TextMessagesThatRequireTXDs_LABEL[44] = "TR_TXT_CV1"
	    TextMessagesThatRequireTXDs_STRING[44] = "TUNER_TEXT_IMG_VIRUS"
		
		TextMessagesThatRequireTXDs_LABEL[45] = "TR_TXT_CV2"
	    TextMessagesThatRequireTXDs_STRING[45] = "TUNER_TEXT_IMG_VIRUS_02"
		
		TextMessagesThatRequireTXDs_LABEL[46] = "TR_TXT_CV3"
	    TextMessagesThatRequireTXDs_STRING[46] = "TUNER_TEXT_IMG_VIRUS_03"
		
	ELIF GB_GET_TUNER_ROBBERY_PREP_PLAYER_IS_ON(PLAYER_ID()) = TRV_IAA_PASS
		
		TextMessagesThatRequireTXDs_LABEL[44] = "ROBTEXT_IAA00M"
	    TextMessagesThatRequireTXDs_STRING[44] = "TUNER_TEXT_IMG_IAA"
	#ENDIF
	
	#IF FEATURE_FIXER
	ELIF GB_GET_FIXER_VIP_MISSION_PLAYER_IS_ON(PLAYER_ID()) = FVV_WAY_IN
		
		TextMessagesThatRequireTXDs_LABEL[44] = "FXR_TXT_WI1"
	    TextMessagesThatRequireTXDs_STRING[44] = "FXR_WAYIN"
	#ENDIF
	
	ENDIF
	
	//Dummy Stack Test
	//TextMessagesThatRequireTXDs_LABEL[157] = "VEX_96_JESTER"
    //TextMessagesThatRequireTXDs_STRING[157] = "IE_TEXTVECH_96"

    
ENDPROC




 


PROC Cleanup_and_Terminate()

    //Tricky timing fix for Bug 110732 - if this doesn't work, replace with direct reference to movie load state
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

        HANG_UP_AND_PUT_AWAY_PHONE()    //Flashhand cannot exit if an application thread has launched but hasn't been cleaned up so this is a weird timing bug.
                                        
    ENDIF


    
    IF b_TXD_Load_Required
    
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED (TextMessageTXD)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING("AppTEXTMESSAGE - Terminating App. Removing streamed texture dictionary...")
            PRINTNL()
        #endif

    ENDIF

    TERMINATE_THIS_THREAD()

ENDPROC






PROC Reinitialise_Text_Main_Screen()



    //Now includes second display_view parameter to jump back to last list position after single message view exit.

    IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE


       

        IF i_FullListSlotToReturnTo < 0

            i_FullListSlotToReturnTo = 0

        ENDIF

    
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 6, TO_FLOAT(i_FullListSlotToReturnTo))
   

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_1")



        #if IS_DEBUG_BUILD

            PRINTSTRING ("AppTextMessage - FullListSlotToReturnTo is ")
            PRINTINT (i_FullListSlotToReturnTo)
            PRINTNL()       


        #endif



        //Badger
        IF g_b_ToggleButtonLabels

            IF NumberOfMessagesInList > 0 //only display "Options" button if there is an actual text or texts in the list.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_214") //"Options" - Positive
            ELSE
               LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ENDIF


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        ELSE
            IF NumberOfMessagesInList > 0 //only display "Options" button if there is an actual text or texts in the list.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ELSE
               LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ENDIF
            

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
        ENDIF


        //1636892 - Display soft key to show that Contacts App can be opened from within Text Message App in MP only.

        IF g_bInMultiplayer

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

        ENDIF

    ELSE


        #if IS_DEBUG_BUILD

            cdPrintstring("AppTEXTMESSAGE - Jumping straight to single message view as g_LastMessageSentMustBeRead is TRUE")
            cdPrintnl()

        #endif
        
        i_FullListSlotToReturnTo = 0


    ENDIF








ENDPROC








PROC Place_Existing_Messages_In_Slots()


    NumberOfMessagesInList = 0 //reset number of placed messages before filling list.

    //Display dummy "text list empty" message in slot 1 in lieu of any real texts overwriting.  Removed as Eddie has a general solution #1351002
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(6)), (TO_FLOAT(0)), 12, 34,
        INVALID_SCALEFORM_PARAM, "CELL_230", "CELL_231")
    */


    Get_Cellphone_Owner()


    BOOL message_in_chrono_order[MAX_TEXT_MESSAGES]

    INT slotIndex = 0 //,  scaleform_SlotIndex = 0
                                                         
    WHILE slotIndex < (MAX_TEXT_MESSAGES - 1)


        //Tricky timing fix for Bug 110732
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

            Cleanup_and_Terminate ()    

        ENDIF



        INT comparisonIndex = 0

        INT top_time_comparison = (MAX_TEXT_MESSAGES - 1) //Dummy position



        //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgSecs = -1 
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgMins = 0
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgHours = 0
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgDay = 0
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgYear = 0

        



        WHILE comparisonIndex < (MAX_TEXT_MESSAGES)


            //Tricky timing fix for Bug 110732
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

                Cleanup_and_Terminate ()    

            ENDIF


                //IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.

                    IF message_in_chrono_order[comparisonIndex] = FALSE

                        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[comparisonIndex].TxtMsgLockStatus <> TXTMSG_EMPTY //make sure empty text messages aren't counted in this list.  


                            //As we're checking this, we don't have to specifically check the SP or MP portions. No SP messages should be resident in MP portion
                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[comparisonIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE //check filter for this player character
                                                                    
                                IF IsFirstTimeNewerThanSecondTime (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[comparisonIndex].TxtMsgTimeSent,  g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent)
                            
                                    top_time_comparison = comparisonIndex                   

                                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgReadStatus = UNREAD_TXTMSG

                                        MessageReadDecider = 33  //Unread messages use icon 34 - see badger wiki.

                                        
                                    ELSE
                                    
                                        MessageReadDecider = 34  //Read messages use icon 34 - see badger wiki.


                                    ENDIF



                                                                           
                                                             
                                                             
                                                                              
                                                             
                                    //Comment back into to display results of sorting algorithm.
                                    /*                                                             
                                    #if IS_DEBUG_BUILD
                                        
                                        PRINTNL()
                                        PRINTSTRING("Text message details")
                                        PRINTNL()
                                        PRINTSTRING("SlotIndex ")
                                        PRINTINT(slotIndex)
                                        PRINTNL()
                                        PRINTSTRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLabel)
                                        PRINTNL()
                                        PRINTSTRING("end")
                                        PRINTNL()
                                    
                                    #endif
                                    */

                                                   
                                ENDIF

                            ENDIF

                        ENDIF

                    ENDIF
               //ENDIF

            //ENDIF

            comparisonIndex ++

        ENDWHILE

        
    
        MessageListSlot[slotIndex] = top_time_comparison
        message_in_chrono_order[top_time_comparison] = TRUE








        //Place the result of the sort in the scaleform slot.

        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLockStatus <> TXTMSG_EMPTY //make sure empty text messages aren't counted in this list.  


                       //As we're checking this, we don't have to specifically check the SP or MP portions. No SP messages should be resident in MP portion
                       IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].PhonePresence[g_Cellphone.PhoneOwner] = TRUE //check filter for this player character




                                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotIndex)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgHours)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgTimeSent.TxtMsgMins)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MessageReadDecider)


                                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSender = NO_CHARACTER
                                        
                                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSenderStringComponent)
                                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFNH")//("CELL_2000") //~a~  //Check in when Gareth has fixed this view state to support condensed fonts.
                                                        
                                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSenderStringComponent)
                                        
                                        #if IS_DEBUG_BUILD

                                            cdPrintnl()
                                            cdPrintstring("Testing CELL_CONDFNH player name injection")
                                            cdPrintnl()

                                        #endif

                                        END_TEXT_COMMAND_SCALEFORM_STRING()

                                    ELSE
                
                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSender].label)
                                        
                                    ENDIF

                                    SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSpecialComponents

                                        CASE NO_SPECIAL_COMPONENTS

                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLabel)

                                        BREAK
                                    
                                
                                        CASE STRING_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLabel)
                                        
                                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgStringComponent)

        
                                            //Check if any additional strings are required...
                                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberOfAdditionalStrings = 1
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent, "NULL"))

                                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)   

                                            ENDIF


                                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberOfAdditionalStrings = 2
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent, "NULL"))
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent, "NULL"))
                                                IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                ENDIF
                                                IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)        
                                                ENDIF
                                            ENDIF

                                
                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK


                                        CASE NUMBER_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLabel)
                                        
                                            ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberComponent)

                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK



                                        CASE STRING_AND_NUMBER_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgLabel)
                                        
                                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgStringComponent)
                                            ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberComponent)


                                            //Check if any additional strings are required...
                                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberOfAdditionalStrings = 1
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent, "NULL"))

                                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)   

                                            ENDIF


                                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgNumberOfAdditionalStrings = 2
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent, "NULL"))
                                            AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent, "NULL"))

                                                //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)        
                                                IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgSecondStringComponent)
                                                ENDIF
                                                IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[top_time_comparison].TxtMsgThirdStringComponent)        
                                                ENDIF
                                            ENDIF



                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK



                                        CASE CAR_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK




                                        CASE SUPERAUTOS_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE LEGENDARY_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE PEDALMETAL_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE WARSTOCK_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE ELITAS_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE DOCKTEASE_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK

                                        
                                        CASE DAILYOBJ_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK
										
										CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
										
											Create_Calkboard_Car_List_String_For_Scaleform(top_time_comparison)
										
										BREAK


                                    ENDSWITCH


                                    
                                END_SCALEFORM_MOVIE_METHOD ()


                    ENDIF

        ENDIF

        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[slotIndex].TxtMsgLockStatus <> TXTMSG_EMPTY
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[slotIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE //check filter for this player character

                NumberOfMessagesInList ++ 
            ENDIF
        ENDIF
    
        slotIndex ++
    ENDWHILE   

    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("appTextMessage - PlaceExistingMessages concluded.")
        cdPrintnl()   
    #endif



    //Potential addition for scaleform value not returning.
    //b_RepeatButtonLimiter = FALSE
    //RepeatButtonLimiter_StartTime = GET_GAME_TIMER()

     
ENDPROC






#IF IS_DEBUG_BUILD
PROC Draw_SingleMessageOptions_HelpText() 

    format_medium_ostext (0, 128, 255, 255) //Light blue //CALL
    //DISPLAY_TEXT (0.750, 0.390, "CELL_201")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_201")



    //Need to grey out any locked messages as these cannot be deleted by the player.
    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_LOCKED
        format_medium_ostext (255, 255, 255, 55) //Greyed out //DELETE
    ELSE
        format_medium_ostext (0, 255, 0, 255) //Green //DELETE
    ENDIF
    
    
    //DISPLAY_TEXT (0.790, 0.390, "CELL_216")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.02), (g_SF_PhonePosY + 0.16), "CELL_216")






    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    //DISPLAY_TEXT (0.847, 0.390, "CELL_206")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.02), (g_SF_PhonePosY + 0.16), "CELL_206")


ENDPROC
#endif








PROC Display_Selected_Message()


    #IF IS_DEBUG_BUILD  
    
    IF g_DoDebugTempDraw_Phone = TRUE

    
        drawItemX = ListOriginX
        drawItemY = ListOriginY 

                

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        highlight_Item()
           
        DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender].label) //draw the name of the contact by referring to the character's text label. 
        
        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)

        
        //Make sure we display a leading zero if the minutes are less than 10.
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgMins < 10
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgHours, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgMins)
        ELSE
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgHours, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgMins)
        ENDIF

        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)
        //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgYear)
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgDay, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgTimeSent.TxtMsgMonth)


        #IF IS_DEBUG_BUILD
            //Display the text label for debug purposes
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                format_medium_ostext (255, 0, 0, 95) //Semi transparent red
            ELSE
                format_medium_ostext (0, 255, 0, 95) //Semi transparent green
            ENDIF
            DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel)
        #endif

        drawItemX = ListOriginX

        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        drawItemY = drawItemY + 0.02

        highlight_Item()

        SET_TEXT_WRAP (0.0, 0.93)
        DISPLAY_TEXT (drawItemX, drawItemY, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel) //the contexts of the text itself

    ENDIF

    #endif
              



    //Delete this text message if not locked...

    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
        
        IF g_InputButtonJustPressed = FALSE
        IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_DELETE_OPTION_INPUT)) //Delete action - delete this single text message
        
                          
            Play_Back_Beep()

            g_InputButtonJustPressed = TRUE

    


            Get_Cellphone_Owner()
                    

            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgDeletionMode = DELETE_FROM_ALL_CHARACTERS

                //If en masse deletion is tagged for this message then we want to remove the message from all SP player character filters...
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].PhonePresence[0] = FALSE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].PhonePresence[1] = FALSE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].PhonePresence[2] = FALSE

            ELSE

                //...otherwise just remove from the player character currently in use.
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE


            ENDIF


            IF IS_TEXTMSG_PRESENT_IN_ANY_FILTER (SingleMessageIndex)
                
                
                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Joypad removed message from current player filter only. Current player Int is ")
                    PRINTINT (ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    PRINTNL ()

                #endif
                    

            ELSE

                //We only want to mark the array index as empty if the message is not present in any character's text message filter.

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReadStatus = UNREAD_TXTMSG


                #IF IS_DEBUG_BUILD

                    PRINTSTRING ("Joypad removed message no longer in any player filter. Deleted message from list at array position ")
                    PRINTINT (SingleMessageIndex)
                    PRINTNL ()

                #endif
                    
          
            ENDIF


            //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
            THEFEED_REMOVE_ITEM (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgFeedEntryId)






            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)


            Place_Existing_Messages_In_Slots() //Update the slot list to reflect removal of deleted message.


       

            //For any list navigation cursor positions greater than zero, i.e the top position, we need to jump one up the list 
            //as deletion has occurred.  This allows display_view in "Reinitialise_Text_Main_Screen" to restore last list position.
            
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF 
           


            Reinitialise_Text_Main_Screen()



            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.


                g_CellPhone.PhoneDS = PDS_RUNNINGAPP
                g_DoOptionsForSingleMessage= FALSE //Return to full display list.


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 24. appTextMessage assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            ENDIF

        
      
        ENDIF
        ENDIF

     
     ELSE
     
        //Hyperlinked messages should always be sent as LOCKED and DO_NOT_AUTO_UNLOCK
        IF g_InputButtonJustPressed = FALSE
        IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) 


            IF ThisMessageContainsHyperlink = TRUE
        
            
                //If this message has a hyperlink attached to it, exit and signal that appInternet should launch. 
                //Hyperlinked messages cannot be deleted by the user at this point as they share the same option button.

                IF g_CellPhone.PhoneDS > PDS_AWAY

                    g_BrowserLinkToLaunch = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label)

                    g_BrowserStartState = SBSS_Launch_Link

                
                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM) //Force Check_For_App_Selection to trigger and launch appInternet automatically.
                
                    g_CellPhone.PhoneDS = PDS_MAXIMUM
                    
                ENDIF

                
                Cleanup_and_Terminate() //Make sure this script terminates immediately.

   
            ENDIF

        ENDIF
        ENDIF

     ENDIF
                                                                                                                                                                                        



    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgBarterStatus = BARTER_IS_REQUIRED
        
        IF g_InputButtonJustPressed = FALSE
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Special option - square

                Play_Select_Beep()
            
                g_InputButtonJustPressed = TRUE

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus = REPLIED_BARTER

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgBarterStatus = NO_BARTER_REQUIRED

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



                g_DoOptionsForSingleMessage= FALSE


                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 25. appTextMessage assigns PDS_RUNNINGAPP")
                        cdPrintnl()   
                    #endif

                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)


                Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.

                
                Reinitialise_Text_Main_Screen()




            ENDIF
        ENDIF

    ENDIF



    //Call the sender of the message....
    IF g_InputButtonJustPressed = FALSE
    AND b_RepeatButtonLimiter = TRUE
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - call the sender of the displayed single message.




                     
            g_InputButtonJustPressed = TRUE
        



            //If a reply is required for this message, entering a positive input means a "yes" reply has been given.
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus > NO_REPLY_REQUIRED


                Play_Select_Beep() //#1591713 - Moved in here so it only beeps when an option is available.


                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus = REPLIED_YES

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED //This should be done by flow - probably!


                g_DoOptionsForSingleMessage= FALSE


                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 26. appTextMessage assigns PDS_RUNNINGAPP")
                        cdPrintnl()   
                    #endif

                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)


                Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.

                
                Reinitialise_Text_Main_Screen()

                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 6) //Set enum state and header to MESSAGE LIST
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_1")


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1, -1, -1, -1, "CELL_214") //"Options" Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1, -1, -1, -1, "CELL_206") //"Back" - Negative

                //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0, -1, -1, -1, "CELL_250") //"Error!" - Other
                */
                



             
            ELSE //"Call" option must have been picked. Only take action if this text message allows the sender call option to be visible and active...


                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgCanCallSenderStatus = CAN_CALL_SENDER


                    Play_Select_Beep() //#1591713 - Moved in here so it only beeps when an option is available.


                    //This needs to be done before setting the phone state to make sure any residual contact does not return TRUE inadvertently - see bug 375586
                    //Can't assign directly to chosen name as IS_CALLING_CONTACT may grab before appContacts initialises. Needs to be done after appContacts has started.
                    g_TheContactInvolvedinCall = CHAR_BLANK_ENTRY           


                    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    
                        g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT    //Setting this here will ensure the scaleform methods below take precedence above the creation of the contacts list itself.
                                                                                //when appContacts is launched. See that script for more info.
                        
                        IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE  //Added for 2038700, this delays first person mode from putting the phone to ear immediately.
                                                                                //It will go to ear on foot when the cellphone conversation starts from PlayCellphoneConversation()
                                                                                //First noted that this was missing for this specific text message routine in 2059725.
                            Put_Cellphone_To_Ear()

                        ENDIF

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("STATE ASSIGNMENT 27. appTextMessage assigns PDS_ATTEMPTING_TO_CALL_CONTACT")
                            cdPrintnl()   
                        #endif
       

                    ENDIF


                    //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
                    request_load_scriptstring ("appContacts")

                    Application_Thread = START_NEW_SCRIPT ("appContacts", CONTACTS_APP_STACK_SIZE)


                    #if IS_DEBUG_BUILD
                        cdPrintstring("AppTEXTMESSAGE -  Launching appCONTACTS with contacts stack size")
                        cdPrintnl()
                    #endif


                    SET_SCRIPT_AS_NO_LONGER_NEEDED ("appContacts")


                    


                    //Tell the global used by appContacts the sender name of this message which can then be transposed to the character sheet.
                    g_TheContactInvolvedinCall = (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender)





                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                            "CELL_300",
                            "CELL_211",
                            "CELL_195")   

                    ELSE


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                            "CELL_211",

                            //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                    ENDIF


                                                                          
                    Update_Scaleform_VSE ()



                    #if IS_DEBUG_BUILD

                        cdPrintstring ("appTextMessage - Attempting to call contact with label: ")
                        cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                        cdPrintstring (" which is charsheet entry ")
                        cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
                        cdPrintnl()

                    #endif





                    //Experiment with ordering of this section if I can nail down that pesky text bug. Perhaps the change-over to appContacts which also uses the phone text slot interferes
                    //with the contents of that slot when it is being used to show the text messages. Store the text messages in another text slot instead?


                
                    Cleanup_and_Terminate()

                ENDIF


            ENDIF


        ENDIF
    ENDIF

  




ENDPROC







#IF IS_DEBUG_BUILD
PROC Display_Message_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


    //DRAW_RECT (0.824, 0.650, 0.238, 0.60, 0, 0, 0, 155)

            
    MessagesToDraw = (NumberOfMessagesInList) 

    //DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    WHILE drawIndex < MessagesToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF

        DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgSender].label) //draw the name of the contact by referring to the character's text label. 
        
        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)

        
        //Make sure we display a leading zero if the minutes are less than 10.
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins < 10
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgHours, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins)
        ELSE
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgHours, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins)
        ENDIF

        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)
        //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgYear)
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth)


        #IF IS_DEBUG_BUILD
            //Display the text label for debug purposes
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgLockStatus = TXTMSG_LOCKED
                format_medium_ostext (255, 0, 0, 95) //Semi transparent red
            ELSE
                format_medium_ostext (0, 255, 0, 95) //Semi transparent green
            ENDIF
            DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgLabel)
        #endif

        drawItemX = ListOriginX

        format_medium_ostext (255, 255, 255, 205) //Semi transparent white
        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF

        drawItemY = drawItemY + 0.02

        SET_TEXT_WRAP (0.0, 0.93)
        DISPLAY_TEXT (drawItemX, drawItemY, g_SavedGlobals.sTextMessageSavedData.g_TextMessage[MessageListSlot[drawIndex]].TxtMsgLabel) //the contexts of the text itself
              
        drawItemY = drawItemY + 0.07 //Draw the next name slightly down the y-axis
    
   		drawIndex ++
    ENDWHILE
ENDPROC
#endif







PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF
    ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        OR IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        

            Call_Scaleform_Input_Keypress_Up()

             
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)

        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))
        OR IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfMessagesInList 

                ListCursorIndex = 0

            ENDIF

           
            Call_Scaleform_Input_Keypress_Down()
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)
          
        ENDIF

    
    ENDIF

            
ENDPROC


                                                 



PROC Check_if_TXD_Load_is_Required()


    IF b_TXD_Load_Required //Get rid of any existing texture dictionaries so they don't stack up during repeated viewings.
    
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED (TextMessageTXD)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING("AppTEXTMESSAGE - Check_if_TXD_Load_is_Required. Removing streamed texture dictionary...")
            PRINTNL()
        #endif


    ENDIF


    b_TXD_Load_Required = FALSE //Reset before checking if the text label of the chosen text message requires a texture dictionary to be streamed in.


    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus <> TXTMSG_EMPTY

        INT i_Count = 0

        WHILE i_Count < c_NumberOfTXD_Messages

            IF NOT IS_STRING_NULL_OR_EMPTY(TextMessagesThatRequireTXDs_LABEL[i_Count])
			AND ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel, TextMessagesThatRequireTXDs_LABEL[i_Count])


				if i_Count  < c_LegacyLongTxdAmount //if the index is within 0 - 18 grab the txd name from the long txd name array.
					#if IS_DEBUG_BUILD
						cdPrintstring("Legacy long txd name detected. Grabbing txd name from TextMessagesThatRequireTXDs_WITH_LONG_STRING array.")
						cdPrintnl()
					#endif
					TextMessageTXD = TextMessagesThatRequireTXDs_WITH_LONG_STRING[i_Count] //If the labels match, get corresponding string from the partner array.
				ELSE
					#if IS_DEBUG_BUILD
						cdPrintstring("23 characters max txd name detected. Grabbing txd name from TextMessagesThatRequireTXDs_STRING array.")
						cdPrintnl()
					#endif
                	TextMessageTXD = TextMessagesThatRequireTXDs_STRING[i_Count] //If the labels match, get corresponding string from the partner array.
					
					#if IS_DEBUG_BUILD
						cdPrintstring(TextMessageTXD)
						cdPrintnl()
					#endif
					
				ENDIF

                b_TXD_Load_Required = TRUE


                 

                REQUEST_STREAMED_TEXTURE_DICT(TextMessageTXD)

          
                WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED(TextMessageTXD)            
                    WAIT (100)
                    PRINTSTRING("AppTextMessage - Waiting on TXD to load...")
                    PRINTNL()
                ENDWHILE



                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Found Message that requires TXD at position ")
                    PRINTINT (i_Count)
                    PRINTNL()

                #endif

            ENDIF

            i_Count++

        ENDWHILE

    ENDIF

ENDPROC








PROC Check_For_Message_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    IF g_InputButtonJustPressed = FALSE

    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - user opted to display single message options.   
    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
    OR g_ShouldForceSelectionOfLatestAppItem = TRUE
    
        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE
        AND g_ShouldForceSelectionOfLatestAppItem = FALSE

            Play_Select_Beep()  //therefore must have been a control press... avoid double beep. # 1593340
        
        ENDIF
        
        g_InputButtonJustPressed = TRUE
        

        b_RepeatButtonLimiter = FALSE

        //This was the cause of 2193315. Moved inside the TXTMSG_EMPTY check.
        //1636892 Make sure to switch this off initially when moving to single message view to get rid of open contact option in MP. It can be replaced later by other soft keys.
        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        //    11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



        
        
        #if IS_DEBUG_BUILD
            
            IF ListCursorIndex < 0
            
                SCRIPT_ASSERT ("ListCursorIndex less than zero error! Please bug Steve Taylor.")
                PRINTSTRING ("ListCursorIndex is less than zero! It is ")
                PRINTINT (ListCursorIndex)
                PRINTNL()
                                                              
            ELSE                      
                                 
                PRINTSTRING ("Array Index occupying this slot ")
                PRINTINT (MessageListSlot[ListCursorIndex])
                PRINTNL()
            
            ENDIF

        #endif

           
        SingleMessageIndex = MessageListSlot[ListCursorIndex]









        //Only proceed to single message display and options if the index points to a full slot.
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus <> TXTMSG_EMPTY




            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                //Moved here for 2193315
                //1636892 Make sure to switch this off initially when moving to single message view to get rid of open contact option in MP. It can be replaced later by other soft keys.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                
                
                g_Cellphone.PhoneDS = PDS_COMPLEXAPP


            
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 29. appTextMessage assigns PDS_COMPLEXAPP")
                    cdPrintnl()   
                #endif

            ENDIF





      
            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
            OR g_ShouldForceSelectionOfLatestAppItem = TRUE

                SingleMessageIndex = MessageListSlot[0] //make sure we pick the top slot for a critical message or the most recent force selection from elongated d-pad up press.
            
                i_FullListSlotToReturnTo = 0


                IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("appTextMessage - Last message sent must be read was TRUE, so setting slot selection to zero.")
                        cdPrintnl()
                   
                    #endif

                ELSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("appTextMessage - g_ShouldForceSelectionOfLatestAppItem was TRUE, so setting slot selection to zero.")
                        cdPrintnl()
               
                    #endif

                ENDIF

                g_ShouldForceSelectionOfLatestAppItem = FALSE //Critical!


            ELSE

                    SETTIMERB(0)

                    b_SafetyValveFired = FALSE



                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


                    WHILE (NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                    AND (b_SafetyValveFired = FALSE)

                        WAIT (0)
                        
                        #if IS_DEBUG_BUILD

                            cdPrintstring("appTextMessage - Waiting on return value from scaleform. ")
                            cdPrintstring( "Choice_ReturnedSFIndex is: ")
                            PRINTINT (NATIVE_TO_INT(Choice_ReturnedSFIndex))
                            cdPrintnl()
                            

                            IF NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                                cdPrintnl()
                                cdPrintString("Return value still not ready...")
                                cdPrintnl()
                            ENDIF


                        #endif

                        IF TIMERB() > 2000 
                            b_SafetyValveFired = TRUE
                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring("appTextMessage - Waiting on return value from scaleform SAFETY VALVE fired!")
                                cdPrintnl()
                           
                            #endif
                        ENDIF

                    ENDWHILE

            
                    IF b_SafetyValveFired = TRUE

                        sf_TempInt = 0

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("appTextMessage - Safety valve fired setting slot selection to zero.")
                            cdPrintnl()
                       
                        #endif

                    ELSE

                        sf_TempInt = (GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex))
                    

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("appTextMessage - sf_TempInt is ")
                            cdPrintInt(sf_TempInt)
                            cdPrintnl()
                       
                        #endif


                    ENDIF





                    IF sf_TempInt < 0
                           
                        #if IS_DEBUG_BUILD

                            cdPrintstring("appTextMessage - Choice_ReturnedSFIndex is less than zero for some reason! Setting to zero as safeguard.")
                            cdPrintnl()

                        #endif

                        sf_TempInt = 0

                    ENDIF


                    SingleMessageIndex = MessageListSlot[sf_TempInt]

                    i_FullListSlotToReturnTo = sf_TempInt




            ENDIF




            //Was originally here, put scaleform not returning pish means I don't even check for a return value if the last message was critical...
            /*
            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
                SingleMessageIndex = MessageListSlot[0] //make sure we pick the top slot for a critical message
            
                i_FullListSlotToReturnTo = 0

            ENDIF   
            */
            


            //Read - Unread status. Alter unread status here for selected message. Any message that reaches single message display is marked as read.

            g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReadStatus = READ_TXTMSG






                //Reset hyperlink buffer and local bool to "empty" before testing for hyperlink availability.
                ThisMessageContainsHyperlink = FALSE
                g_HyperLink_Buffered_Label = "NO_HYPERLINK"


                TEXT_LABEL_63 HyperlinkCheck_Label = g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel

                HyperlinkCheck_Label += "_LINK"



                IF DOES_TEXT_LABEL_EXIST (HyperlinkCheck_Label)

                    g_HyperLink_Buffered_Label = HyperlinkCheck_Label


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppTEXTMESSAGE - Found valid hyperlink label for selected textmessage. Updating buffer and locking message!")
                        cdPrintnl()
                        cdPrintstring ("AppTEXTMESSAGE - Hyperlink label contents are ")
                        cdPrintstring (GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label))
                        cdPrintnl()

                    #endif

                    
                    ThisMessageContainsHyperlink = TRUE


                    //Update centre soft key with 16 - internet world icon.
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_267") //"Link" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                
                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgAutoUnlockStatus = TXTMSG_DO_NOT_AUTO_UNLOCK

                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_LOCKED

                ENDIF















            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ //automatically unlock if set.
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
            ENDIF

    


            Check_if_TXD_Load_is_Required()


            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender = NO_CHARACTER
                                            
                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSenderStringComponent)

                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_2000") //~a~
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSenderStringComponent)

                END_TEXT_COMMAND_SCALEFORM_STRING()

            ELSE

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender].label)

            ENDIF

            SWITCH g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSpecialComponents

                CASE NO_SPECIAL_COMPONENTS

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel)

                BREAK
            
        
                CASE STRING_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel)
                
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgStringComponent)
        
                    //Check if any additional strings are required...
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)   

                    ENDIF


                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent, "NULL"))

                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)        
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)        
                        ENDIF
                    ENDIF


                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK


                CASE NUMBER_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel)
                
                    ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberComponent)

                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK



                CASE STRING_AND_NUMBER_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel)
                
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgStringComponent)
                    ADD_TEXT_COMPONENT_INTEGER (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberComponent)

                    //Check if any additional strings are required...
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)   

                    ENDIF


                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent, "NULL"))

                        //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                        //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)        

                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgThirdStringComponent)        
                        ENDIF

                    ENDIF



                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK


                CASE CAR_LIST_COMPONENT
                                            
                     Create_Car_List_String_For_Scaleform (SingleMessageIndex)

                BREAK



                CASE SUPERAUTOS_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE LEGENDARY_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE PEDALMETAL_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE WARSTOCK_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE ELITAS_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE DOCKTEASE_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE DAILYOBJ_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK
				
				// url:bugstar:7188269 - Exotic Exports - As a QoL update, can we please add the ability to request a text message from Sessanta that lists the vehicles you still need to get that day?
				CASE DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT
				
					Create_Calkboard_Car_List_String_For_Scaleform(SingleMessageIndex)
				
				BREAK

            ENDSWITCH
            






            //Sender image.

            IF g_TxtMsgHeadshotID[SingleMessageIndex] = NULL

				//url:bugstar:5809076
				IF (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender = CHAR_COMIC_STORE)
			
					#if IS_DEBUG_BUILD
						cdPrintString("JUNE 2019 - Sender character was CHAR_COMIC_STORE. Overriding AppTextMessage headshot decision to CELL_COMIC_P")
					#ENDIF
			
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_COMIC_P")
			
				ELSE

	                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sCharacterSheetAll[g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgSender].picture)
	            
	                #if IS_DEBUG_BUILD

	                    cdPrintnl()
	                    cdPrintString("AppTEXTMESSAGE - No headshot ID stored for this index. Using Character Sheet entry or default for sender pic.")
	                    cdPrintnl()

	                #endif

				ENDIF

            ELSE

                //Use generated headshot txd as sender image if valid and ready...

                
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("AppTEXTMESSAGE - Potential headshot stored for this sender. Attempting to use as sender pic.")
                    cdPrintnl()

                #endif


                STRING s_Headshot_TXD

                IF (IS_PEDHEADSHOT_VALID (g_TxtMsgHeadshotID[SingleMessageIndex]))
            
                    IF (IS_PEDHEADSHOT_READY (g_TxtMsgHeadshotID[SingleMessageIndex]))

                        s_Headshot_TXD = GET_PEDHEADSHOT_TXD_STRING(g_TxtMsgHeadshotID[SingleMessageIndex])

                       
                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintString("AppTEXTMESSAGE - Headshot valid and ready. Grabbing texture to use as sender pic.")
                            cdPrintnl()

                        #endif


                    ELSE
                    
                        s_Headshot_TXD = "CHAR_DEFAULT" //default to blank headshot if the headshot is not ready.
                    
                 

                    ENDIF
                    
                ELSE
        
                    s_Headshot_TXD = "CHAR_DEFAULT" //default to blank headshot if the headshot is not valid.

         
                ENDIF



                //This might or might not work for bug 866787

                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_2000") //~a~
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (s_Headshot_TXD)

                END_TEXT_COMMAND_SCALEFORM_STRING()




            ENDIF


            END_SCALEFORM_MOVIE_METHOD ()























            g_DoOptionsForSingleMessage= TRUE


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 7) //Set enum state and header to SINGLE MESSAGE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_1") //"Texts"


            //Viewed message in single message view. Ok to remove from feed. See bug 986286 
            THEFEED_REMOVE_ITEM (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgFeedEntryId)


            
            //Set up specific buttons for those text messages which require a reply...
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus > NO_REPLY_REQUIRED
             
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"Yes"
                */

                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" - Positive
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive
                    ENDIF


            ELSE
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" Positive       
                */  
                    
            
                    //Badger

                    //If this text message has been set up to allow the sender to be called, the default setting, then display the call button..
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgCanCallSenderStatus = CAN_CALL_SENDER

                        IF g_b_ToggleButtonLabels
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive
                        ELSE 
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive
                        ENDIF

                    ELSE //Switch positive soft key off...
            
                         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                    ENDIF

                    
            ENDIF



            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus > NO_REPLY_REQUIRED
            
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"No"
                */

                IF ARE_STRINGS_EQUAL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel, "CELL_FINV") //Special mp race invite does not allow "no" response.
                      
                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                    ENDIF

                ELSE

                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                    ENDIF


                ENDIF


            ELSE

                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1, 
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative
                */

                //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)//"BACK" - Negative
                    ENDIF

                

            ENDIF            








            















            //Make sure that the delete option is only available if the text message concerned is in unlocked state. Check second int parameter after method name.
            //Hyperlinked messages must be locked due to button conflicts.
            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_LOCKED 

           
                /* Trial move above - see 764336
                //Reset hyperlink buffer and local bool to "empty" before testing for hyperlink availability.
                ThisMessageContainsHyperlink = FALSE
                g_HyperLink_Buffered_Label = "NO_HYPERLINK"


                TEXT_LABEL_63 HyperlinkCheck_Label = g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel

                HyperlinkCheck_Label += "_LINK"



                IF DOES_TEXT_LABEL_EXIST (HyperlinkCheck_Label)

                    g_HyperLink_Buffered_Label = HyperlinkCheck_Label


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppTEXTMESSAGE - Found valid hyperlink label for selected textmessage. Updating buffer.")
                        cdPrintnl()
                        cdPrintstring ("AppTEXTMESSAGE - Hyperlink label contents are ")
                        cdPrintstring (GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label))
                        cdPrintnl()

                    #endif

                    
                    ThisMessageContainsHyperlink = TRUE


                    //Update centre soft key with 16 - internet world icon.
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_267") //"Link" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



                ENDIF
                */
                    



                //Check is this is a barter message. Hyperlinked messages should not be barter messages also.
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgBarterStatus =  BARTER_IS_REQUIRED
            

                    ThisMessageContainsHyperlink = FALSE //failsafe to make sure barter takes precedence if text labels are screwy.


                    //Update centre soft key with 11 - temp Barter icon.
                
                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_264") //"Barter" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                ELSE

                    IF ThisMessageContainsHyperlink = FALSE //make sure hyperlink soft key decision isn't reversed if this isn't a barter message.

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                    ENDIF

                ENDIF



            
            ELSE
            
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 1, 
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_216") //"DELETE" - Other
                */

                  //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            12, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_216") //"DELETE" - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            12, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)//"DELETE" - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                    ENDIF


            
            
            ENDIF


            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE           
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE


        
        ELSE

            BOOL b_PerformEmergencyTextExit = FALSE //This bool routine added for 1919067. We don't want the select button to exit if we are only on the safe "No messages" screen.
            
            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
            OR g_ShouldForceSelectionOfLatestAppItem = TRUE //If either of these are true, then it's highly likely we got here via those
                                                            //checks at the beginning of Check_For_Message_Selection(), not the button press.

                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING("APPTEXTMESSAGE WARNING! - g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus was EMPTY")
                    PRINTNL()
                    PRINTSTRING("APPTEXTMESSAGE WARNING! - but g_LastMessageSentMustBeRead of ForceSelection was likely TRUE. Setting to false for safety. See bug 1557146")
                    PRINTNL()

                    cdPRINTNL()
                    cdPRINTSTRING("APPTEXTMESSAGE WARNING! - g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus was EMPTY")
                    cdPRINTNL()
                    cdPRINTSTRING("APPTEXTMESSAGE WARNING! - but g_LastMessageSentMustBeRead of ForceSelection was likely TRUE. Setting to false for safety. See bug 1557146")
                    cdPRINTNL()


                #endif


                IF g_ShouldForceSelectionOfLatestAppItem
                    
                    #if IS_DEBUG_BUILD
                        PRINTSTRING("APPTEXTMESSAGE CRITICAL - g_ShouldForceSelectionOfLatestAppItem was set on an empty text message. Setting to false for safety. See bug 1919067")
                        PRINTNL()
                        g_ShouldForceSelectionOfLatestAppItem = FALSE //Added for 1919067
                    #endif
                
                ENDIF

                
                b_PerformEmergencyTextExit = TRUE //As it's most likely wasn't a button press, be safe and perform an emergency exit.
            
            ENDIF


            

            //These are important resets for all eventualities.
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE  //1557146         
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE  //1557146

            

            IF b_PerformEmergencyTextExit = TRUE //Added for 1919067. 

                b_PerformEmergencyTextExit = FALSE

                g_Cellphone.PhoneDS = PDS_AWAY //Originally added outwith emergency exit routine for 1557146

            ENDIF
             



        ENDIF



        ENDIF





        IF g_LaunchContactsFromTextMessage = FALSE   //1636892
        
            IF g_bInMultiplayer

                
                //Trial for 1687668
                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)))


                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Call entered number...

                #if IS_DEBUG_BUILD

                    cdPrintstring("AppTEXTMESSAGE - g_LaunchContactsFromTextMessage has been set to TRUE. Will cleanup text message script accordingly!")
                    cdPrintnl()

                #endif


                g_InputButtonJustPressed = TRUE //This will let Cellphone_Flashhand know that we want to launch the contacts app immediately after exiting

                g_LaunchContactsFromTextMessage = TRUE

                //Cleanup_and_Terminate()  //Don't need this as cellphone_flashhand's CHECK_FOR_APPLICATION_EXIT routines detect g_LaunchContactsFromTextMessage returning true.

                ENDIF
            ENDIF

        ENDIF

   
    ENDIF //End of g_InputButtonJustPressed = FALSE condition check


ENDPROC







PROC DisplaySingleMessage_and_Options()


    Display_Selected_Message()
                   

    #IF IS_DEBUG_BUILD
            
        IF g_DoDebugTempDraw_Phone = TRUE

            Draw_SingleMessageOptions_HelpText()
    
        ENDIF

    #endif


ENDPROC





PROC Update_Message_List()


  
    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE



  


    Play_Back_Beep()

    g_InputButtonJustPressed = TRUE


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)

    Place_Existing_Messages_In_Slots() //Update the slot list to reflect removal of deleted message.

    
    Reinitialise_Text_Main_Screen()


    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_CellPhone.PhoneDS = PDS_RUNNINGAPP
        g_DoOptionsForSingleMessage= FALSE //Return to full display list.


        
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 30. appTextMessage assigns PDS_RUNNINGAPP")
            cdPrintnl()   
        #endif

    ENDIF



ENDPROC







SCRIPT



    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


    /*
    REQUEST_ADDITIONAL_TEXT ("TMSG", PHONE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (PHONE_TEXT_SLOT)  //the permanent global from the temporary holder.
        WAIT(0)
    ENDWHILE
    */


    
    //Tricky timing fix for Bug 110732
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

        Cleanup_and_Terminate ()    

    ENDIF



    Set_Up_Messages_TXD_Relationship()







    //Important! Make sure full display list is defaulted to on startup of this script.
    g_DoOptionsForSingleMessage= FALSE //Return to full display list.


    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE



    g_DisplayNewSideTaskSignifier = FALSE




    

    
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)

  
    Place_Existing_Messages_In_Slots()


    Reinitialise_Text_Main_Screen()
    
  


    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Failsafe - makes sure that the cellphone is initialised into running state. 

            
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 31. appTextMessage assigns PDS_RUNNINGAPP")
            cdPrintnl()   
        #endif

    ENDIF




    RepeatButtonLimiter_StartTime = GET_GAME_TIMER()
    



    Previous_SelectedAppCursorPos_pageOne = 1  //For bug 941391, artificially make sure that post text message, the cursor returns to the text message icon when the homescreen is re-estabished
                        //on exit of this app. The text message app can auto launch by merely pressing d-pad up, rather than navigating to it.




    WHILE TRUE

        WAIT(0)
        




        IF b_RepeatButtonLimiter = FALSE

            RepeatButtonLimiter_CurrentTime = GET_GAME_TIMER()


            IF RepeatButtonLimiter_CurrentTime - RepeatButtonLimiter_StartTime > 500


                b_RepeatButtonLimiter = TRUE
    
            ENDIF   
        ENDIF






        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.

            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP

                    IF g_DoOptionsForSingleMessage= FALSE //shouldn't need to check this providing PDS_COMPLEXAPP state has been set.

                        #IF IS_DEBUG_BUILD
                            IF g_DoDebugTempDraw_Phone = TRUE

                                Display_Message_List()
                                
                            ENDIF
                        #endif
                                
                                             
                        IF g_LaunchContactsFromTextMessage = FALSE

                            Check_for_List_Navigation()                                      
                                              
                            Check_For_Message_Selection()

                        ENDIF
                        

                    ENDIF

                BREAK



                CASE PDS_COMPLEXAPP
                    
                    IF g_DoOptionsForSingleMessage = TRUE

                        DisplaySingleMessage_and_Options()

                        Check_for_List_Navigation()     //Was removed at one time .See bug 956980. 
                                                        //Then, navigation not needed in single message view, now in because of long text messages.

                    ENDIF
                                            
                BREAK



                DEFAULT

                    
                    #IF IS_DEBUG_BUILD

                        PRINTSTRING ("AppTextMessage in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK


            ENDSWITCH

     


            



            
            
            //Only check for this if not in full list display mode

            IF g_DoOptionsForSingleMessage = FALSE

                IF CHECK_FOR_APPLICATION_EXIT() //will only terminate if this script is in PDS_RUNNINGAPP rather than PDS_COMPLEXAPP state.
                
                    Cleanup_and_Terminate()

                ENDIF

            ELSE
                

                //IF g_InputButtonJustPressed = FALSE  //nasty timing issue if this check is left in...
                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW)                    


                    IF ARE_STRINGS_EQUAL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLabel, "CELL_FINV") //Special mp race invite.
                      
                              
                        #if IS_DEBUG_BUILD
                            cdPrintstring("AppTEXTMESSAGE - Not taking action on back button press as reply is forced for CELL_FINV text. See bug 923977")
                            cdPrintnl()
                        #endif


                    ELSE

                        WAIT(0) //Make sure the JUST_PRESSED check cannot return true to any subesquent checks until a second press is made.

                        
                        //See bug 892983. If MP wants to delete an invite text after a response window timeouts, setting this bit allows them to force the text message app to return
                        //to the full message view. Provided they have deleted the text message before calling the public function to set this bit, the full list view will be updated to
                        //reflect the removal of the text.
                        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW) 

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

                        g_DoOptionsForSingleMessage= FALSE

                        
                        //See bug 316474
                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP

            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 32. appTextMessage assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                        //Fix for bug 110732
                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

                            Cleanup_and_Terminate()
                                            
                        ENDIF



                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 6)


                        Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.


                        Reinitialise_Text_Main_Screen()

                        //CRITICAL!
                        //This section modifies what happens to the single message status after the back button has been pressed from single message view.


                        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus > NO_REPLY_REQUIRED

                            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgBarterStatus = BARTER_IS_REQUIRED

                                
                                #if IS_DEBUG_BUILD
                                    cdPrintstring("AppTEXTMESSAGE - NO reply set for Barter text message")
                                    cdPrintnl()
                                #endif

                                
                                //Currently the same behaviour as a standard replay message. A reply is forced, in this case "no". The message can then be deleted by the user 
                                //after unlocking the message.
                                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus = REPLIED_NO

                                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
                                

                            ELSE

                                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgReplyStatus = REPLIED_NO

                                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[SingleMessageIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED //This should be done by flow - probably!

                            ENDIF


                        ENDIF

                    ENDIF


                ENDIF //End of phone control just pressed check. 
                


               


            ENDIF
  
        ELSE


            //Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
            //return to the main menu post-phonecall.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.

            //BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling contact" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the contacts list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the contacts list.

            //TERMINATE_THIS_THREAD() //Likewise, having this here will make sure this script is terminated should any call come through... 
          

        ENDIF


        //Fix for bug 110732
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

            Cleanup_and_Terminate()    //Flashhand cannot exit if an application thread has launched but hasn't been cleaned up so this is a weird timing bug.
                                        
        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

           Cleanup_and_Terminate()

        ENDIF



        #if IS_DEBUG_BUILD

            //draw_debug_screen_grid()

        #endif



        
            
    ENDWHILE
    
    
ENDSCRIPT


