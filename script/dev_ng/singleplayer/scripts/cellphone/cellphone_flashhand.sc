USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "commands_graphics.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "cellphone_movement.sch"
USING "dialogue_private.sch"
USING "dialogue_public.sch"
//USING "player_ped_public.sch"
USING "Cellphone_Settings_globals_reg.sch"
USING "finance_modifiers_public.sch"
USING "commands_zone.sch"
USING "comms_control_public.sch"
USING "script_misc.sch"
USING "net_comms_public.sch"
USING "ply_to_ply_calls.sch"
USING "script_ped.sch"
USING "transition_common.sch"

#if USE_TU_CHANGES
    USING "drunk_public.sch"
#endif




//

// *****************************************************************************************************************
// *****************************************************************************************************************
// *****************************************************************************************************************
//
//      MISSION NAME    :   cellphone_flashhand.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Handles the various states the cellphone finds itself in from display to handling exits.
//                          Launched from cellphone_controller or LAUNCH_APPLICATION_EXIT. If this script is not
//                          running then the phone cannot be on screen.
//                                              
//
//
// *****************************************************************************************************************
// *****************************************************************************************************************
// *****************************************************************************************************************


//Bug 2378079 - new content starts.

//For bug 2378079, implementing a failsafe to terminate AppInternet if it's locked in exit routine and a timer for launch problems.
//INT AppInternetFailsafeCounter = 0
//INT Post_AppLaunch_CurrentTime = 0 
//INT AppLaunch_StartTime = 0

//2378079 - new content ends.



//Temp vars for drawing phone -  This will all be handled by flash!


FLOAT minOriginX = 0.73
FLOAT minOriginY = 0.55

FLOAT maxOriginX = 0.73
FLOAT maxOriginY = 0.45

FLOAT drawItemX = 0.0
FLOAT drawItemY = 0.0



//FLOAT drawRingerOrigin = 0.81
//FLOAT drawRingerSignalMin = 0.81
//FLOAT drawRingerSignalMax = 0.91


//Removed due to unreference as code segment has been taken out due to bug 109069
//VEHICLE_INDEX Cellphone_CurrentPlayerVehicle //used to test submerging car in incident handling section.



//Values used to check app selection safely.

//6417795 - April 2020
//Add a simple counter for manually blanking out homescreen slots on the scaleform main state.
INT blankSlotCounter = 0


INT testReturnedSFInt
SCALEFORM_RETURN_INDEX testReturnedSFIndex
BOOL b_Waiting_On_SF_Return_Value = FALSE
BOOL local_Request_for_QuickSave = FALSE
       

//BOOL Movement_Exception = FALSE   //Moved to globals to support bug 2050535
BOOL HasGameplayCameraTransitionOccurredDuringCall = FALSE

VECTOR v_CurrentHudPosition



BOOL InFirstPersonCamera = FALSE
BOOL PlayerIsInCover = FALSE

BOOL b_NeedToAdjustEmailForCameraTransition = FALSE
VECTOR v_3dEmailDesiredTransPosition

INT PhoneRenderTarget_ID //the render target id of the phone
BOOL b_PhoneDestroyed = FALSE
#IF IS_DEBUG_BUILD
FLOAT f_YComponent_FPSRemovalSpeed
#ENDIF


INT ThemeDecision, i_Provider_Ident

//BSMF_TYPES bsaq_mf_OpenDecision, bsaq_mf_CallDecision


INT testSignalInt = 0

INT ClockHours, ClockMins
DAY_OF_WEEK ClockDay
TEXT_LABEL_15 TempDayLabel

BOOL Failsafe_Depress_Period_Underway = FALSE
BOOL b_RemoteRingOngoing = FALSE





INT Player_PeriodicHealth //need to keep a copy of this so that we can tell if the player has taken any damage.

INT StockMarketChecks_StartTime, StockMarketChecks_CurrentTime

INT JustEnteredExitVehicle_StartTime, JustEnteredExitVehicle_CurrentTime //2269787
BOOL JustEnteredExitVehicle = FALSE

TIME_DATATYPE Net_StockMarketChecks_StartTime
 
INT i_TimeDifference



//INT Interference_Current_Time, Interference_Start_Time




INT TempColR, TempColG, TempColB, TempColA //holds the RGBA values extracted from various HUD_COLOURs and passed into SET_SOFT_KEYS_COLOUR



BOOL b_Sniper_equipped = FALSE

WEAPON_TYPE fh_TempWeapon

VEHICLE_INDEX tempFH_VehicleIndex


INT i_Affluence, i_SignalFluctuator
BOOL b_InSubmersible = FALSE
INT i_VibrationTimer = 0


VECTOR AwayVec





BOOL LaunchContactsFromTextMessage_LocalSignal = FALSE



BOOL b_ShouldDisableDirModeVehicleCamForThisActor = FALSE



//#2595711 - Signal MP to display help text explaining the Boss Agency App.
BOOL b_ShouldDisplayBossAgencyHelp = FALSE




#if IS_DEBUG_BUILD


    VECTOR v_DebugRotation

              
#endif



//Moved to cellphone_globals.
//Prescribed phone width - height ratio from Gareth 0.316 x 0.755

//FLOAT g_SF_PhonePosX = 0.87//0.86
//FLOAT g_SF_PhonePosY = 0.797//0.527

//FLOAT g_SF_PhoneSizeX = 0.1548//0.148//0.18
//FLOAT g_SF_PhoneSizeY = 0.37//0.45



/*
FLOAT textSFfloat= 0.0.5
INT SFdirection = 0

INT TempRectColR, TempRectColG, TempRectColB //holds the RGB values used for temporarily differentiation different owners phones before FLASH takes over.


*/


/*

STATUS: 2d App Position array works. Get in! Need to have highlight and selection routines next which allow contacts to be selected for demo.
Once "contacts" is selected that should run a separate script to bring up a list or I'll try and re-use the slot filling routine.


The big plan!

Nice chaps who understand flash should have a phone which we can think of as having a blank master list of empty slots. I then fill these blank slots with 
the various apps and their associated information. So, as far as flash is concerned, they provide something like this:


In Min Mode:
Slot 1      Slot 2      Slot 3          Slot 4.


When the phone is launched in min mode, I reference each application in my AppList and use the information associated with each field of the struct to fill
the list up.

So it becomes something like


CONTACTS    TEXTS       INTERNET        CAMERA





So I know which App is in each slot via some kind of flashSlot1 = AppName call during a fill up slots routine

The navigation of this will all be handled by flash so I will need some way of knowing which position has been selected when the user makes his choice.


UPDATE: THE BIG PLAN WORKED. ALMOST TOO WELL...

*/



/*
PROC Check_Min_for_Max_DisplayStatus()

    //All temp button presses - should be replaced by macros e.g USE_PHONE!
   
        //IF IS_BUTTON_JUST_PRESSED (PAD1, INT_TO_ENUM (PAD_BUTTON_NUMBER, PHONE_TAKEOUT_INPUT)) 
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))    
          
            WHILE IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))     
            
               WAIT(0)
 
            ENDWHILE


            SWITCH g_Cellphone.PhoneDS
            
                CASE PDS_MINIMAL
                    g_Cellphone.PhoneDS = PDS_MAXIMUM


                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 50. Cellphone_flashhand assigns PDS_MAXIMUM")
                        cdPrintnl()   
                    #endif


                BREAK

           
                DEFAULT
                //Do nothing
                BREAK




            ENDSWITCH

        ENDIF



ENDPROC
*/


/*Moved to cellphone_private.
PROC Update_Page_Indicator_SoftKey()

        IF g_b_Secondary_Screen_Available = FALSE
       
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
    

        ELSE

            IF g_bInMultiplayer //no secondary screen for multiplayer...
        
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE

                IF g_Homescreen_In_Secondary_Mode = TRUE

                    IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages 2
                    ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages 2
                    ENDIF

                ELSE


                    IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages
                    ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages
                    ENDIF


                ENDIF

                SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ENDIF


        ENDIF

ENDPROC
*/



FUNC VEHICLE_SEAT GetSeatForTurretCheck(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)
    
    IF  NOT IS_ENTITY_DEAD(PedIndex)
    AND NOT IS_ENTITY_DEAD(VehicleIndex)

        IF IS_PED_SITTING_IN_VEHICLE(PedIndex, VehicleIndex)

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER) = PedIndex
              RETURN VS_DRIVER
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_FRONT_RIGHT) = PedIndex
              RETURN VS_FRONT_RIGHT
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_LEFT) = PedIndex
              RETURN VS_BACK_LEFT
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_RIGHT) = PedIndex
              RETURN VS_BACK_RIGHT
            ENDIF



            //Turret Specifics


            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_LEFT_1) = PedIndex
              RETURN VS_EXTRA_LEFT_1
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_RIGHT_1) = PedIndex
              RETURN VS_EXTRA_RIGHT_1
            ENDIF


            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_LEFT_2) = PedIndex
              RETURN VS_EXTRA_LEFT_2
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_RIGHT_2) = PedIndex
              RETURN VS_EXTRA_RIGHT_2
            ENDIF

            
            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_LEFT_3) = PedIndex
              RETURN VS_EXTRA_LEFT_1
            ENDIF

            IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_EXTRA_RIGHT_3) = PedIndex
              RETURN VS_EXTRA_RIGHT_1
            ENDIF


        ELSE
            
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("Ped is not sitting in vehicle. Check if ped is sitting in vehicle first before calling GET_PED_VEHICLE_SEAT().")
            #ENDIF
        
        ENDIF
         
    ENDIF
     
    RETURN VS_ANY_PASSENGER
     
ENDFUNC





/* Moved to cellphone_private so AppContacts.sc can use.
PROC LaunchTransitionMoveUpIfRequired()


    VECTOR CurrentCellphonePosition

    GET_MOBILE_PHONE_POSITION (CurrentCellphonePosition) 

    IF g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio].y = CurrentCellphonePosition.Y

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - LaunchTransitionMoveUpIfRequired() says phone already up!")   
            cdPrintnl()

        #endif

    ELSE

        g_3DPhoneNeedsMovedUp = TRUE  

    ENDIF


ENDPROC





PROC LaunchTransitionMoveDownIfRequired()
         
    VECTOR CurrentCellphonePosition

    GET_MOBILE_PHONE_POSITION (CurrentCellphonePosition) 
                
    IF g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio].y = CurrentCellphonePosition.Y

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - LaunchTransitionMoveDownIfRequired() says phone already up!")   
            cdPrintnl()

        #endif

    ELSE

        g_FirstPersonTransitionOngoing = TRUE 

    ENDIF


ENDPROC
*/
















PROC Move_to_Second_Screen(INT cursorPosition)


        g_Homescreen_In_Secondary_Mode = TRUE
                        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
            

        Scaleform_Place_Items_on_Homescreen(2)

        
        Update_Page_Indicator_SoftKey()


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(cursorPosition)) 

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_FH - Update_Scaleform_VSE called - 1998")
            cdPrintnl ()

        #endif

        PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

  
        appCursorIndex = cursorPosition   



ENDPROC





PROC Move_to_First_Screen(INT cursorPosition)


        g_Homescreen_In_Secondary_Mode = FALSE
                        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
            

        Scaleform_Place_Items_on_Homescreen(1)


        Update_Page_Indicator_SoftKey()     

     
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(cursorPosition)) 

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_FH - Update_Scaleform_VSE called - 1999")
            cdPrintnl ()

        #endif

        PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

        appCursorIndex = cursorposition


ENDPROC






PROC Perform_LeftmostFirst_to_RightmostSecond_Wrap(INT cursorPosition)



        g_Homescreen_In_Secondary_Mode = TRUE
                        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
            

        Scaleform_Place_Items_on_Homescreen(2)


        Update_Page_Indicator_SoftKey() 

        //Supporting first line only so 0, 3 or 6 on first screen will only check for presence of app at 2, then 1, then 0 on second screen.

        //Unref hack.
        IF cursorPosition = 0
        ENDIF


        //Deliberately not else clauses! We need to try all slots in sequence. Move to loop when supporting more lines.
        IF Has_this_AppSlot_Been_Assigned[0] = TRUE 
        
            AppCursorIndex = 0  //This should always be the default as there has to be an app in slot zero or the second screen would not be available. 

        ENDIF
            
        
        IF Has_this_AppSlot_Been_Assigned[1] = TRUE

            AppCursorIndex = 1

        ENDIF

      
        IF Has_this_AppSlot_Been_Assigned[2]  = TRUE

            AppCursorIndex = 2
        
        ENDIF

        PLAY_SOUND_FRONTEND (-1, "Menu_Navigate", g_Owner_Soundset)

        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(appCursorIndex)) 





ENDPROC
          












PROC Check_For_Director_Mode_Navigation()

    //  Homescreen 

    //  0   1   2
    //___________
    //  3   4   5
    //___________
    //  6   7   8


    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF

    ENDIF





       ///////
///    PC MOUSE WHEEL SUPPORT - STEVE R. LDS
/// 
///    Scrolls through the entire list of icons using mouse wheel
///    dropping down to the next row when it reaches the right/left hand side, eg.
///    
///    0 -> 1 -> 2 
///    <----------
///    3 -> 4 -> 5
///    <----------
///    6 -> 7 -> 8 
///    
///    
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
            // Mousewheel scroll forward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

                SWITCH AppCursorIndex   
                   
                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK

        
                ENDSWITCH
            

                //dpad_pause_cued = TRUE
                //SETTIMERA (0)
                
            ENDIF
                
            // Scroll backward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
            
                SWITCH AppCursorIndex   

                           
                    
                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK
    
                               
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK

           
                ENDSWITCH
                
                //dpad_pause_cued = TRUE
                //SETTIMERA (0)
            
            ENDIF

        ENDIF




        IF dpad_pause_cued = FALSE 
   
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            Movement_Exception = FALSE
            
            SWITCH AppCursorIndex   

                CASE 2
                    AppCursorIndex = 0
                BREAK
                           
                CASE 5
                    AppCursorIndex = 3
                BREAK


                CASE 7
                    //Cater for missing bottom right hand corner app carousel scrolling...
                    IF Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 6
                        Movement_Exception = TRUE
                    ENDIF
                BREAK


                CASE 8
                    AppCursorIndex = 6
                BREAK

                        
                DEFAULT
                    AppCursorIndex ++ 
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Left()
            ELSE         
                Call_Scaleform_Input_Keypress_Right() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF




        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))

            Movement_Exception = FALSE

               SWITCH AppCursorIndex   

                CASE 0
                    AppCursorIndex = 2
                BREAK


                CASE 3
                    AppCursorIndex = 5
                BREAK


                CASE 6
                
                    IF Has_this_AppSlot_Been_Assigned[8] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 7
                        Movement_Exception = TRUE
                    ENDIF

                BREAK

            
                DEFAULT
                    AppCursorIndex --   
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Right()
            ELSE         
                Call_Scaleform_Input_Keypress_Left() 
            ENDIF

    
            dpad_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF


        ENDIF



ENDPROC








PROC Check_For_Single_Homescreen_Navigation()



    //  Homescreen 

    //  0   1   2
    //___________
    //  3   4   5
    //___________
    //  6   7   8


    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF

    ENDIF

    ///////
///    PC MOUSE WHEEL SUPPORT - STEVE R. LDS
/// 
///    Scrolls through the entire list of icons using mouse wheel
///    dropping down to the next row when it reaches the right/left hand side, eg.
///    
///    0 -> 1 -> 2 
///    <----------
///    3 -> 4 -> 5
///    <----------
///    6 -> 7 -> 8 
///    
///    
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
            // Mousewheel scroll forward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[1]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 1
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[2]
                             Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[3]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 3
                        ELSE
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[4]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[5]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 5
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[8]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 8
                        ELSE
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 0
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[0]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 0
                        ENDIF
                    BREAK

                ENDSWITCH
            

                //dpad_pause_cued = TRUE
                //SETTIMERA (0)
                
            ENDIF
                
            // Scroll backward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
            
                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[8]
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 8
                        ELSE
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[0]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 0
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[1]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 1
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[2]
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 2
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[3]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 3
                        ELSE
                            Call_Scaleform_Input_Keypress_Up()
                            Call_Scaleform_Input_Keypress_Right()
                            Movement_Exception = TRUE
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[4]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[5]
                            Call_Scaleform_Input_Keypress_Up()
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 5
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK

                ENDSWITCH
                
                //dpad_pause_cued = TRUE
                //SETTIMERA (0)
            
            ENDIF

        ENDIF




    IF dpad_pause_cued = FALSE 
   
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            Movement_Exception = FALSE
            
            SWITCH AppCursorIndex   

                CASE 2
                    AppCursorIndex = 0
                BREAK
                           
                CASE 5
                    AppCursorIndex = 3
                BREAK


                CASE 7
                    //Cater for missing bottom right hand corner app carousel scrolling...
                    IF Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 6
                        Movement_Exception = TRUE
                    ENDIF
                BREAK


                CASE 8
                    AppCursorIndex = 6
                BREAK

                        
                DEFAULT
                    AppCursorIndex ++ 
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Left()
            ELSE         
                Call_Scaleform_Input_Keypress_Right() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF




        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))

            Movement_Exception = FALSE

               SWITCH AppCursorIndex   

                CASE 0
                    AppCursorIndex = 2
                BREAK


                CASE 3
                    AppCursorIndex = 5
                BREAK


                CASE 6
                
                    IF Has_this_AppSlot_Been_Assigned[8] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 7
                        Movement_Exception = TRUE
                    ENDIF

                BREAK

            
                DEFAULT
                    AppCursorIndex --   
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Right()
            ELSE         
                Call_Scaleform_Input_Keypress_Left() 
            ENDIF

    
            dpad_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF




            

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
            
            Movement_Exception = FALSE

            SWITCH AppCursorIndex   

                CASE 0
                    IF Has_this_AppSlot_Been_Assigned[6]
                        AppCursorIndex = 6
                    ENDIF
                BREAK   
                
                CASE 1
                    IF Has_this_AppSlot_Been_Assigned[7]
                        AppCursorIndex = 7
                    ENDIF
                BREAK

                CASE 2
                    IF Has_this_AppSlot_Been_Assigned[8]
                        AppCursorIndex = 8
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 5
                    ENDIF
                BREAK
                

                CASE 3
                    IF Has_this_AppSlot_Been_Assigned[0]
                        AppCursorIndex = 0
                    ENDIF
                BREAK   
                
                CASE 4
                    IF Has_this_AppSlot_Been_Assigned[1]
                        AppCursorIndex = 1
                    ENDIF
                BREAK

                CASE 5
                    IF Has_this_AppSlot_Been_Assigned[2]
                        AppCursorIndex = 2
                    ENDIF
                BREAK


                CASE 6
                    IF Has_this_AppSlot_Been_Assigned[3]
                        AppCursorIndex = 3
                    ENDIF
                BREAK   
                
                CASE 7
                    IF Has_this_AppSlot_Been_Assigned[4]
                        AppCursorIndex = 4
                    ENDIF
                BREAK

                CASE 8
                    IF Has_this_AppSlot_Been_Assigned[5]
                        AppCursorIndex = 5
                    ENDIF
                BREAK

        
            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Down()
            ELSE         
                Call_Scaleform_Input_Keypress_Up() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF 




         IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            Movement_Exception = FALSE
            
            SWITCH AppCursorIndex   

                CASE 0
                    IF Has_this_AppSlot_Been_Assigned[3]
                        AppCursorIndex = 3
                    ENDIF
                BREAK   
                
                CASE 1
                    IF Has_this_AppSlot_Been_Assigned[4]
                        AppCursorIndex = 4
                    ENDIF
                BREAK

                CASE 2
                    IF Has_this_AppSlot_Been_Assigned[5]
                        AppCursorIndex = 5
                    ENDIF
                BREAK
                

                CASE 3
                    IF Has_this_AppSlot_Been_Assigned[6]
                        AppCursorIndex = 6
                    ENDIF
                BREAK   
                
                CASE 4
                    IF Has_this_AppSlot_Been_Assigned[7]
                        AppCursorIndex = 7
                    ENDIF
                BREAK

                CASE 5
                    IF Has_this_AppSlot_Been_Assigned[8]
                        AppCursorIndex = 8
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 2
                    ENDIF
                BREAK


                CASE 6
                    IF Has_this_AppSlot_Been_Assigned[0]
                        AppCursorIndex = 0
                    ENDIF
                BREAK   
                
                CASE 7
                    IF Has_this_AppSlot_Been_Assigned[1]
                        AppCursorIndex = 1
                    ENDIF
                BREAK

                CASE 8
                    IF Has_this_AppSlot_Been_Assigned[2]
                        AppCursorIndex = 2
                    ENDIF
                BREAK

        
            ENDSWITCH




            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Up()
            ELSE         
                Call_Scaleform_Input_Keypress_Down() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)



         ENDIF   


      ENDIF



ENDPROC

//6417795 - April 2020
//Sets up the secondary mode of the homescreen. Called whenever the player presses the new switch mode soft key on the cellphone.
PROC SET_UP_MULTIPLAYER_SECONDARY_MODE_HOMESCREEN()
	
	SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE)
	//...this ensures the "in secondary mode" bit flag is set prior to setting up the secondary mode.
   
    //Specify the position that the settings application should now appear in the secondary mode as a failsafe.
	//The third param specifies position 0, the top left cell of our 3 x 3 grid. Note that we do not change the sixth parameter. 
	//That relates to a legacy page precedence system used in SP; it is irrelevant to this new infrastructure.
    Fill_App_Sheet (AppSETTINGS, "CELL_16", 0, "appSettings", 24, 1, App_O1_UNLOCKED, App_O2_LOCKED, App_O3_LOCKED)   
	
	
    //Clear all scaleform display slots on state 1, the homescreen. This still requires a manual refresh however...
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
            
	//...so wipe out all of the 3 x 3 homescreen grid cells by injecting them with a blank icon and data.
	blankSlotCounter = 0
	
    WHILE blankSlotCounter < 9
        
        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(blankSlotCounter)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(25)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			
			//This string should never be seen, so just use the settings name rather than a dummy string. See #6437083
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[AppSETTINGS].AppName)
            
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(225)
        END_SCALEFORM_MOVIE_METHOD()
		
      	blankSlotCounter ++
		
    ENDWHILE

	//Now we want to manually inject the Settings App into navigation slot 0 to match the desired cell position on the homescreen.
	//When the user selects this cell, the application launch routine will extract the data associated with the Settings app.
	AppSlot[0] = (ENUM_TO_INT(AppSETTINGS))
	
    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_AppList[AppSETTINGS].SF_IconEnum)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_AppList[AppSETTINGS].AppName)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255)
    END_SCALEFORM_MOVIE_METHOD()
                               		 
    Update_Page_Indicator_SoftKey()

	//Do a quick refresh of the homescreen by briefly switching to another scaleform state...
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4, TO_FLOAT(0)) 
    
	//...then switch back to state 1, the homescreen.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(0)) 
       
	//Finally, re-add the new switch soft key in case it has gone walkabout during any of the other complex legacy manipulations.
    ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()
       
 
ENDPROC




//6417795 - April 2020
//Called every frame when the cellphone is in homescreen secondary mode in MP only.
PROC Check_For_New_MP_Secondary_Mode_Navigation()

	//We have only one app in secondary mode at present, so we only need to check for the user pressing the switch homescreen mode option.
	//No navigational checks are required as yet.
	IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) 
		
		Play_Select_Beep()
		
		//The user has pressed the control to switch back to primary mode of the homescreen, clear our associated mode bit...
		
		CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE) 
		
		#if IS_DEBUG_BUILD
			cdPrintstring("6417795 - April 2020: New MP Secondary Mode exit detected. Will return to Primary Mode...")
		#ENDIF
		
		//...flush the scaleform homescreen state.
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)
	
		//Prepare our return to primary mode by repopulating the homescreen via the existing legacy method...
        Scaleform_Place_Items_on_Homescreen(1)
		
		//...and refresh the scaleform view to show the reconstituted primary mode again, making sure to re-add our softkey as a failsafe.
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(Previous_SelectedAppCursorPos_pageOne)) 
		ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()
		
	ENDIF
	
	//Comment in to display some debug text on the phone screen to show we are in secondary mode.
	#if IS_DEBUG_BUILD
		//DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "2nd")
	#ENDIF

ENDPROC



PROC Check_For_New_MP_Single_Homescreen_Navigation()  //Broadcast app requested to be commented out by Les TODO #1620959 - This is the new master routine for MP

	

	//6417795 - April 2020
	//Critical!
	//If the user presses the new soft key to swap between the current primary mode to the new secondary mode,
	//action this then exit to avoid any further navigational checks this frame.
	//Tunable support added for 6474707 - should only check for this input if the tunable says the secondary screen mode is available.
	IF SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE()
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) 
		
			Play_Select_Beep()
		
			Previous_SelectedAppCursorPos_pageOne = AppCursorIndex //Grab a copy of where the selection cursor was for any future use.
			
			//Call our main secondary mode set up routine.
			SET_UP_MULTIPLAYER_SECONDARY_MODE_HOMESCREEN()
			
			#if IS_DEBUG_BUILD
				cdPrintstring("6417795 - April 2020:  Was navigating Primary Mode. Secondary Mode is available. Detected requested to activate New MP Secondary Mode.")
			#ENDIF
			
			
			EXIT
		ENDIF
	ENDIF



    //  Homescreen 
    //  0   1   2
    //___________
    //  3   4   5
    //___________
    //  6   7   8       
	
	//All used, 5 now will be a launch tile. Settings app moved to second page.


    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF

    ENDIF

        ///////
///    PC MOUSE WHEEL SUPPORT - STEVE R. LDS
/// 
///    Scrolls through the entire list of icons using mouse wheel
///    dropping down/moving up to the next row when it reaches the right/left hand side, eg.
///    
///    0 -> 1 -> 2 
///    <----------
///    3 -> 4 -> 5
///    <----------
///    6 -> 7 -> 8 
///    
///    
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

            // Mousewheel scroll forward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[1]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 1
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[2]
                             Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[3]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 3
                        ELSE
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[4]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[5]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 5
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[8]
                            Call_Scaleform_Input_Keypress_Right()
                            AppCursorIndex = 8
                        ELSE
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 0
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[0]
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Down()
                            AppCursorIndex = 0
                        ELSE
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Down()
                        ENDIF
                    BREAK

                ENDSWITCH
                            
            ENDIF
                
            // Scroll backward
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)

                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[8]
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 8
                        ELSE
                            Call_Scaleform_Input_Keypress_Right()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[0]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 0
                        ELSE
                            IF Has_this_AppSlot_Been_Assigned[8]
                                Call_Scaleform_Input_Keypress_Up()
                                Call_Scaleform_Input_Keypress_Right()
                            ELSE
                                Call_Scaleform_Input_Keypress_Up()  
                            ENDIF
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[1]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 1
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[2]
                            Call_Scaleform_Input_Keypress_Left()
                            Call_Scaleform_Input_Keypress_Up()
                            AppCursorIndex = 2
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[3]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 3
                        ELSE
                            Call_Scaleform_Input_Keypress_Up()
                            Call_Scaleform_Input_Keypress_Right()
                            Movement_Exception = TRUE
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[4]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 4
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[5]
                            Call_Scaleform_Input_Keypress_Up()
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 5
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[6]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 6
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[7]
                            Call_Scaleform_Input_Keypress_Left()
                            AppCursorIndex = 7
                        ENDIF
                    BREAK

                ENDSWITCH
                
                //dpad_pause_cued = TRUE
                //SETTIMERA (0)
            
            ENDIF

        ENDIF


    IF dpad_pause_cued = FALSE 
   
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            Movement_Exception = FALSE
            
            SWITCH AppCursorIndex   

                CASE 2
                    IF Has_this_AppSlot_Been_Assigned[0] = TRUE 
                        AppCursorIndex = 0
                    ELSE
                        AppCursorIndex = 1
                        Movement_Exception = TRUE
                    ENDIF
                BREAK
                           
                CASE 5
                    AppCursorIndex = 3		
                BREAK


                CASE 6  
                   AppCursorIndex = 7
                BREAK



                CASE 7
                    //Cater for missing bottom right hand corner app carousel scrolling...
                    IF Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 6
                        Movement_Exception = TRUE
                    ENDIF
                BREAK


                CASE 8
                    AppCursorIndex = 6
                BREAK

                        
                DEFAULT
                    AppCursorIndex ++ 
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Left()
            ELSE         
                Call_Scaleform_Input_Keypress_Right() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF




        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))

            Movement_Exception = FALSE

               SWITCH AppCursorIndex   

                CASE 0
                    AppCursorIndex = 2
                BREAK


                CASE 1
                    IF Has_this_AppSlot_Been_Assigned[0] = TRUE 
                        AppCursorIndex = 0
                    ELSE
                        AppCursorIndex = 2
                        Movement_Exception = TRUE
                    ENDIF
                BREAK
                       




                CASE 3
                    AppCursorIndex = 5
                BREAK


                CASE 6
                
                    
                    IF Has_this_AppSlot_Been_Assigned[8] = TRUE 
                        AppCursorIndex = 8
                    ELSE
                        AppCursorIndex = 7
                        Movement_Exception = TRUE
                    ENDIF
                    
                  
                BREAK

            
                DEFAULT
                    AppCursorIndex --   
                BREAK

            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Right()
            ELSE         
                Call_Scaleform_Input_Keypress_Left() 
            ENDIF

    
            dpad_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF




            

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
            
            Movement_Exception = FALSE

            SWITCH AppCursorIndex   

                CASE 0
                    IF Has_this_AppSlot_Been_Assigned[6]
                        AppCursorIndex = 6
                    ENDIF
                BREAK   
                
                CASE 1
                    IF Has_this_AppSlot_Been_Assigned[7]
                        AppCursorIndex = 7
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 4
                    ENDIF
                BREAK

                CASE 2
                    IF Has_this_AppSlot_Been_Assigned[8]
                        AppCursorIndex = 8
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 5
                    ENDIF
                BREAK
                

                CASE 3
                    IF Has_this_AppSlot_Been_Assigned[0]
                        AppCursorIndex = 0
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 6
                    ENDIF
                BREAK   
                
                CASE 4
                    IF Has_this_AppSlot_Been_Assigned[1]
                        AppCursorIndex = 1
                    ENDIF
                BREAK

                CASE 5
                    IF Has_this_AppSlot_Been_Assigned[2]
                        AppCursorIndex = 2
                    ENDIF
                BREAK


                CASE 6
                    IF Has_this_AppSlot_Been_Assigned[3]
                        AppCursorIndex = 3
                    ENDIF
                BREAK   
                
                CASE 7
                    IF Has_this_AppSlot_Been_Assigned[4]
                        AppCursorIndex = 4
                    ENDIF
                BREAK

                CASE 8
                    IF Has_this_AppSlot_Been_Assigned[5]
                        AppCursorIndex = 5
                    ENDIF
                BREAK

        
            ENDSWITCH


            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Down()
            ELSE         
                Call_Scaleform_Input_Keypress_Up() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF 




         IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            Movement_Exception = FALSE
            
            SWITCH AppCursorIndex   

                CASE 0
                    IF Has_this_AppSlot_Been_Assigned[3]
                        AppCursorIndex = 3
                    ENDIF
                BREAK   
                
                CASE 1
                    IF Has_this_AppSlot_Been_Assigned[4]
                        AppCursorIndex = 4
                    ENDIF
                BREAK

                CASE 2
                    IF Has_this_AppSlot_Been_Assigned[5]
                        AppCursorIndex = 5
                    ENDIF
                BREAK
                

                CASE 3
                    IF Has_this_AppSlot_Been_Assigned[6]
                        AppCursorIndex = 6
                    ENDIF
                BREAK   
                
                CASE 4
                    IF Has_this_AppSlot_Been_Assigned[7]
                        AppCursorIndex = 7
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 1
                    ENDIF
                BREAK

                CASE 5
                    IF Has_this_AppSlot_Been_Assigned[8]
                        AppCursorIndex = 8
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 2
                    ENDIF
                BREAK


                CASE 6
                    IF Has_this_AppSlot_Been_Assigned[0]
                        AppCursorIndex = 0
                    ELSE
                        Movement_Exception = TRUE
                        AppCursorIndex = 3
                    ENDIF
                BREAK   
                
                CASE 7
                    IF Has_this_AppSlot_Been_Assigned[1]
                        AppCursorIndex = 1
                    ENDIF
                BREAK

                CASE 8
                    IF Has_this_AppSlot_Been_Assigned[2]
                        AppCursorIndex = 2
                    ENDIF
                BREAK

        
            ENDSWITCH




            IF Movement_Exception = TRUE
                Call_Scaleform_Input_Keypress_Up()
            ELSE         
                Call_Scaleform_Input_Keypress_Down() 
            ENDIF

            dpad_pause_cued = TRUE
            SETTIMERA (0)



         ENDIF   


      ENDIF


    //#2595711 - Signal MP to display help text explaining the Boss Agency App.
    IF g_AppList[ENUM_TO_INT (AppDummyApp0)].SF_IconEnum = 57//Boss Agency App is positioned in the DummyApp position, we know this because the Boss Agency icon int is unique.

        IF dpad_pause_cued = TRUE
        AND AppCursorIndex = 8 //Cursor has just been positioned over the bottom right icon of the homescreen

            IF b_ShouldDisplayBossAgencyHelp = FALSE //Make sure we only signal this once per phone launch. James A. say I don't need to worry about setting the bit each time the phone launches.

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("#2595711 Cellphone_flashhand signalling net_gang_boss_common Boss Agency help bit")
                    cdPrintnl()   
                #endif
        
                b_ShouldDisplayBossAgencyHelp = TRUE
                
                GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BOSS_AGENCY_APP)

            ENDIF
    
        ENDIF

    ENDIF


ENDPROC
















PROC Check_for_Dual_Homescreen_Navigation() //maybe pass in screen type enum e.g contacts / omni-directional type so that nav types can be handled e.g Contacts will only move up / down 
                           


    //  Homescreen 

    //  0   1   2
    //___________
    //  3   4   5
    //___________
    //  6   7   8


  
    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF
    ENDIF



    IF dpad_pause_cued = FALSE 
   
       IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))


            IF g_Homescreen_In_Secondary_Mode


                //Only supporting first line so far...

                IF Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = TRUE   //If there is app in the next slot and we are at position 1 or 2 on the second screen
                AND AppCursorIndex < 2                                           //then we can move right.
                                                                                 


                    AppCursorIndex ++


                    Call_Scaleform_Input_Keypress_Right()

       
                    dpad_pause_cued = TRUE
                    SETTIMERA (0)


                ELSE

                    //We were already at the furthest right available app, wrap round to position 0 on the first screen.

                    Move_to_First_Screen(0)

                ENDIF




            ELSE


                    IF g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable
                    OR g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable

                        SWITCH AppCursorIndex   

                            CASE 2

                                Move_to_Second_Screen(0)

                            BREAK


                            CASE 5

                                Move_to_Second_Screen(0)

                            BREAK


                            CASE 8

                                Move_to_Second_Screen(0)

                            BREAK

                            DEFAULT 

                                AppCursorIndex ++

                                Call_Scaleform_Input_Keypress_Right()

                
                                dpad_pause_cued = TRUE
                                SETTIMERA (0)

                            
                            BREAK


                        ENDSWITCH

                    ELSE
        
                
                        SWITCH AppCursorIndex   

                            CASE 2
                                
                                AppCursorIndex = 0

                            
                            BREAK


                            CASE 5

                                AppCursorIndex = 3

                            BREAK


                            CASE 7

                                //Cater for missing bottom right hand corner app carousel scrolling...
                                IF Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = TRUE 
                                    AppCursorIndex = 8
                                ELSE
                                    AppCursorIndex = 6
                                ENDIF
                    
                            BREAK





                            CASE 8

                                AppCursorIndex = 6

                            BREAK

                        
                            DEFAULT

                    
                                AppCursorIndex ++   

                            BREAK

                        ENDSWITCH


                         Call_Scaleform_Input_Keypress_Right()

                
                         dpad_pause_cued = TRUE
                         SETTIMERA (0)


                    ENDIF


     
            ENDIF


        ENDIF

 



        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))



              IF g_Homescreen_In_Secondary_Mode

                     //Only supporting first line so far...

                    IF appCursorIndex > 0

                        AppCursorIndex --

                        Call_Scaleform_Input_Keypress_Left()

                    ELSE

                        Move_to_First_Screen(2)

                    ENDIF



              ELSE

                    IF g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable
                    OR g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable

                        SWITCH AppCursorIndex

                            CASE 0
                        
                                Perform_LeftmostFirst_to_RightmostSecond_Wrap(appCursorIndex)
                            
                            BREAK


                            CASE 3
                        
                                Perform_LeftmostFirst_to_RightmostSecond_Wrap(appCursorIndex)
                            
                            BREAK


                            CASE 6
                        
                                Perform_LeftmostFirst_to_RightmostSecond_Wrap(appCursorIndex)
                            
                            BREAK

                        
                            DEFAULT 

                                AppCursorIndex --

                                Call_Scaleform_Input_Keypress_Left()

                
                                dpad_pause_cued = TRUE
                                SETTIMERA (0)

                            
                            BREAK

                         ENDSWITCH


                    ELSE
        
                
                        SWITCH AppCursorIndex   

                            CASE 0
                                
                                AppCursorIndex = 2

                            
                            BREAK


                            CASE 3

                                AppCursorIndex = 5

                            BREAK


                            CASE 6

                                AppCursorIndex = 8

                            BREAK

                        
                            DEFAULT

                                AppCursorIndex --   

                            BREAK

                        ENDSWITCH


                         Call_Scaleform_Input_Keypress_Left()

                
                         dpad_pause_cued = TRUE
                         SETTIMERA (0)


                    ENDIF





            ENDIF



        ENDIF








            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))

                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5
                        ENDIF
                    BREAK

            
                ENDSWITCH


                //Keypress Up
                Call_Scaleform_Input_Keypress_Up()

                dpad_pause_cued = TRUE
                SETTIMERA (0)

            ENDIF       
            





             IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))


                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

            
                ENDSWITCH




                //Keypress Down
                Call_Scaleform_Input_Keypress_Down()

                dpad_pause_cued = TRUE
                SETTIMERA (0)



             ENDIF   

              

    ENDIF

            
ENDPROC







PROC Check_for_MP_Singlescreen_Navigation()  
                           


    //  Homescreen E = Empty 

    //  0   1   2
    //___________
    //  3   4   5
    //___________
    //  6   E   E
    
    
    // KGM 8/2/13:  Updated checks to use Has_this_AppSlot_Been_Assigned() so that it doesn't need modified no matter where the gaps are on a single-screen multipalyer homepage
    //              Removed RaceToPoint app from slot 0, moved Settings app from Slot 7 to Slot 0


  
    IF dpad_pause_cued

        IF TIMERA() > 50

            dpad_pause_cued = FALSE

        ENDIF
    ENDIF



    IF dpad_pause_cued = FALSE 
   
       IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))


            //Keith - this sort of function is available if you need it...
            //Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = FALSE 

      
                SWITCH AppCursorIndex
                
                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1
                        ELIF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2                      // Gap at 1 but App in 2
                        ENDIF
                    BREAK
                
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0                      // Single screen wrap
                        ENDIF
                    BREAK

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ELIF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5                      // Gap at 4 but App in 5
                        ENDIF
                    BREAK
                
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3                      // Single screen wrap
                        ENDIF
                    BREAK
                    
                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7
                        ELIF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8                      // Gap at 7 but App in 8
                        ENDIF
                    BREAK
                
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6                      // Single screen wrap
                        ENDIF
                    BREAK

                ENDSWITCH


                    Call_Scaleform_Input_Keypress_Right()

   

                dpad_pause_cued = TRUE
                SETTIMERA (0)
           
        ENDIF

 

               
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))


            //Keith - this sort of function is available if you need it...
            //Has_this_AppSlot_Been_Assigned[(appCursorIndex + 1)] = FALSE 

      
                SWITCH AppCursorIndex

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2                      // Single screen wrap
                        ENDIF
                    BREAK
                
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1
                        ELIF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0                      // Gap at 1 but App in 0
                        ENDIF
                    BREAK

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5                      // Single screen wrap
                        ENDIF
                    BREAK
                
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ELIF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3                      // Gap at 4 but App in 3
                        ENDIF
                    BREAK

                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8                      // Single screen wrap
                        ENDIF
                    BREAK
                
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7
                        ELIF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6                      // Gap at 7 but App in 6
                        ENDIF
                    BREAK

                ENDSWITCH


                Call_Scaleform_Input_Keypress_Left()

   

                dpad_pause_cued = TRUE
                SETTIMERA (0)
           
        ENDIF







            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))

                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6                      // Single screen wrap
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7                      // Single screen wrap
                        ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8                      // Single screen wrap
                        ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3
                        ELIF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0                      // Gap at 3 but App in 0
                       ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ELIF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1                      // Gap at 4 but App in 1
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5
                        ELIF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2                      // Gap at 5 but App in 2
                        ENDIF
                    BREAK

            
                ENDSWITCH


                //Keypress Up
                Call_Scaleform_Input_Keypress_Up()

                dpad_pause_cued = TRUE
                SETTIMERA (0)

            ENDIF       
            





             IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))


                SWITCH AppCursorIndex   

                    CASE 0
                        IF Has_this_AppSlot_Been_Assigned[3]
                            AppCursorIndex = 3
                        ELIF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6                      // Gap at 3 but App in 6
                        ENDIF
                    BREAK   
                    
                    CASE 1
                        IF Has_this_AppSlot_Been_Assigned[4]
                            AppCursorIndex = 4
                        ELIF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7                      // Gap at 4 but App in 7
                       ENDIF
                    BREAK

                    CASE 2
                        IF Has_this_AppSlot_Been_Assigned[5]
                            AppCursorIndex = 5
                        ELIF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8                      // Gap at 5 but App in 8
                       ENDIF
                    BREAK
                    

                    CASE 3
                        IF Has_this_AppSlot_Been_Assigned[6]
                            AppCursorIndex = 6
                        ENDIF
                    BREAK   
                    
                    CASE 4
                        IF Has_this_AppSlot_Been_Assigned[7]
                            AppCursorIndex = 7
                        ENDIF
                    BREAK

                    CASE 5
                        IF Has_this_AppSlot_Been_Assigned[8]
                            AppCursorIndex = 8
                        ENDIF
                    BREAK


                    CASE 6
                        IF Has_this_AppSlot_Been_Assigned[0]
                            AppCursorIndex = 0                      // Single screen wrap
                        ENDIF
                    BREAK   
                    
                    CASE 7
                        IF Has_this_AppSlot_Been_Assigned[1]
                            AppCursorIndex = 1                      // Single screen wrap
                        ENDIF
                    BREAK

                    CASE 8
                        IF Has_this_AppSlot_Been_Assigned[2]
                            AppCursorIndex = 2                      // Single screen wrap
                        ENDIF
                    BREAK

            
                ENDSWITCH




                //Keypress Down
                Call_Scaleform_Input_Keypress_Down()

                dpad_pause_cued = TRUE
                SETTIMERA (0)



             ENDIF   

              

    ENDIF

            
ENDPROC







































PROC tempDraw_FlashSlot_Items()


    INT drawIndex = 0
    INT AppsToDraw = 0

    SWITCH g_Cellphone.PhoneDS

        CASE PDS_MINIMAL
            drawItemX = minOriginX
            drawItemY = minOriginY 
            
            AppsToDraw = 4 //We only want to draw the apps in the 0, 1, 2, 3 flashslot position whilst in minimum mode.
        BREAK

        CASE PDS_MAXIMUM
            drawItemX = maxOriginX
            drawItemY = maxOriginY
            
            AppsToDraw = (ENUM_TO_INT(MAX_APPLICATIONS)) //We want to draw all the applications in every flashslot position in maximum mode.    
        BREAK

    ENDSWITCH


    

    //WHILE drawIndex < (ENUM_TO_INT(MAX_APPLICATIONS))
    WHILE drawIndex < AppsToDraw

   
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF AppCursorIndex = drawIndex
            highlight_Item()
        ENDIF



        DISPLAY_TEXT (drawItemX, drawItemY, g_AppList[AppSlot[drawIndex]].AppName) 
        
        drawItemX = drawItemX + 0.065 //Draw the next app slightly along the x-axis

        //cdPrintnl()
        //cdPrintint(drawIndex)
        //cdPrintnl()

        drawIndex ++



        //TEMP: Adjust drawing positions for every 5th app so it starts on new line and original x origin position
        //  0   1   2   3
        //  4   5   6   7
        //AppSlot4, the 5th app in total, draws at the original x origin, but at a lower y. This is only applicablke

        
        SWITCH drawIndex
            
            CASE 4
                drawItemX = maxOriginX
                drawItemY = (maxOriginY + 0.05)
            BREAK

            CASE 8
                drawItemX = maxOriginX
                drawItemY = (maxOriginY + 0.1)
            BREAK

        ENDSWITCH  
                  

 



    ENDWHILE
    


ENDPROC







#if IS_DEBUG_BUILD

PROC Draw_Select_Back_HelpText()


    

    format_medium_ostext (0, 128, 255, 255) //Light blue //SELECT
    //DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.20), "CELL_205")

    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_205")

    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    //DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.20), "CELL_206")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_206")


    DISPLAY_TEXT_WITH_LITERAL_STRING (0.60, 0.30, "STRING", "Current phone rotation")
    DISPLAY_TEXT_WITH_FLOAT (0.6, 0.35, "CELL_500", v_DebugRotation.X, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.6, 0.40, "CELL_501", v_DebugRotation.Y, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.6, 0.45, "CELL_502", v_DebugRotation.Z, 2)

    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.35, "CELL_280", v_DebugRotation.X, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.40, "CELL_281", v_DebugRotation.Y, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.45, "CELL_282", v_DebugRotation.Z, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.50, "CELL_283", v_DebugRotation.Z, 2)

    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.65, "CELL_284", v_DebugRotation.Z, 2)
    DISPLAY_TEXT_WITH_FLOAT (0.3, 0.70, "CELL_285", v_DebugRotation.Z, 2)


ENDPROC







PROC Draw_SideTask_Back_HelpText() //Used primarily for debug text message display.

    format_medium_ostext (0, 128, 255, 255) //Light blue //Select
    DISPLAY_TEXT (0.752, 0.390, "CELL_205")

    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    DISPLAY_TEXT (0.835, 0.390, "CELL_206")

ENDPROC








PROC Draw_Options_Back_HelpText() //Used primarily for debug text message display.

    format_medium_ostext (0, 128, 255, 255) //Light blue //OPTIONS
    //DISPLAY_TEXT (0.752, 0.390, "CELL_214")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_214")


    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    //DISPLAY_TEXT (0.835, 0.390, "CELL_206")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_206")



ENDPROC





PROC Draw_Answer_Reject_HelpText()

    format_medium_ostext (0, 128, 255, 255) //Light blue //ANSWER
    //DISPLAY_TEXT (0.81, 0.63, "CELL_203")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_203")


    format_medium_ostext (255, 57, 57, 255) //Solid Red  //REJECT
    //DISPLAY_TEXT (0.88, 0.63, "CELL_204")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_204")


ENDPROC







PROC Draw_Yes_No_HelpText()

    format_medium_ostext (0, 128, 255, 255) //Light blue //YES
    //DISPLAY_TEXT (0.81, 0.63, "CELL_212")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_212")


    format_medium_ostext (255, 57, 57, 255) //Solid Red  //NO
    //DISPLAY_TEXT (0.88, 0.63, "CELL_213")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_213")


ENDPROC






PROC Draw_Emergency_HelpText()

    format_medium_ostext (0, 128, 255, 255) //Light blue //Police
    //DISPLAY_TEXT (0.81, 0.585, "CELL_602")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.1), "CELL_604")


    format_medium_ostext (255, 57, 57, 255) //Solid Red  //Fire
    //DISPLAY_TEXT (0.81, 0.61, "CELL_603")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.125), "CELL_603")


    format_medium_ostext (0, 255, 57, 255) //Solid Green //Ambulance
    //DISPLAY_TEXT (0.81, 0.635, "CELL_604")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.150), "CELL_602")


ENDPROC

#endif








#if IS_DEBUG_BUILD
PROC tempDraw_Phone()
       





    SWITCH g_Cellphone.PhoneDS                          


       


        CASE PDS_MINIMAL
            //tempDraw_FlashSlot_Items()

            format_medium_ostext (255, 255, 255, 75) //Pale Alpha White
            DISPLAY_TEXT (minOriginX, 0.60, g_sCharacterSheetAll[g_Cellphone.PhoneOwner].label)  //Display owner name


                 

            //DRAW_RECT (0.86, 0.58, 0.27, 0.14, TempRectColR, TempRectColG, TempRectColB, 165)
            format_medium_ostext (255, 255, 255, 125) //Alpha White
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.81, 0.60, "STRING", "Cellphone - Minmode")  
            
            Draw_Select_Back_HelpText()
          
        BREAK
                
                       
        CASE PDS_MAXIMUM
            //tempDraw_FlashSlot_Items()

            Draw_Select_Back_HelpText() //This needs to be here. Seems to be some temporary scaleform bug that makes text appear monochrome after a
                                        //certain number of display_text calls

        BREAK

        
        CASE PDS_ONGOING_CALL

            //Scaleform transition
            //DRAW_RECT (0.87, 0.57, 0.14, 0.175, TempRectColR, TempRectColG, TempRectColB, 165)

            IF g_IsConferenceCall = TRUE //if the call has been designated as a conference call, display the second name in conference on the calling screen.
                format_medium_ostext (255, 255, 255, 255) //PS3 white
                DISPLAY_TEXT (0.81, 0.55, g_sCharacterSheetAll[g_AdditionalContactInvolvedinCall].label) 

            ENDIF            



            IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 

                //format_medium_ostext (255, 255, 255, 255) //PS3 white
                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.81, 0.60, "STRING", "is calling...")

                Draw_Answer_Reject_HelpText()



                


                //Draw moving arrow to signify incoming call
                //format_medium_ostext (255, 255, 255, 255) //PS3 white
                //highlight_Item()
                //DISPLAY_TEXT_WITH_LITERAL_STRING (drawRingerSignalMin, 0.57, "STRING", "o")

                //drawRingerSignalMin = drawRingerSignalMin + 0.005

                //IF drawRingerSignalMin > drawRingerSignalMax
                   //drawRingerSignalMin = drawRingerOrigin
                //ENDIF

                
            
            ELSE

                IF g_ConversationStatus = CONV_STATE_PLAYING //only draw this screen if conversation is playing.

                 
                    IF g_TellFlashHandToDisplayYN = FALSE
                    AND g_TellFlashHandToDisplayEmergencyServices = FALSE

                        format_medium_ostext (0, 255, 100, 255) //Light green //"CONNECTED"
                        DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.08), "CELL_219") 

                        format_medium_ostext (255, 57, 57, 255) //Solid Red  //"END CALL"
                        DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_202")
                     
                    ELSE
                        
                        IF g_TellFlashHandToDisplayYN = TRUE //Display yes / no help text - stipulated by dialogue handler.sc
                    
                            Draw_Yes_No_HelpText()

                            format_medium_ostext (255, 255, 255, 255) //white
                            //DISPLAY_TEXT (0.81, 0.60, g_QuestionGodLabel) //display got text prompt label that the user has passed in via dialogue public.sch
                            DISPLAY_TEXT ((g_SF_PhonePosX - 0.07), (g_SF_PhonePosY + 0.070), g_QuestionGodLabel)

                    
                        ENDIF


                        IF g_TellFlashHandToDisplayEmergencyServices = TRUE //Display yes / no help text - stipulated by dialogue handler.sc
                    
                            Draw_Emergency_HelpText()

                            format_medium_ostext (255, 255, 255, 255) //white
                            //DISPLAY_TEXT (0.81, 0.56, g_QuestionGodLabel) //display got text prompt label that the user has passed in via dialogue public.sch
                            DISPLAY_TEXT ((g_SF_PhonePosX - 0.07), (g_SF_PhonePosY + 0.070), g_QuestionGodLabel)

                            //WIPBmark

                        ENDIF


                    ENDIF

                ENDIF


            ENDIF

        BREAK


        CASE PDS_ATTEMPTING_TO_CALL_CONTACT 

            //the other parts of this screen are drawn in appContacts.sc - might move here if it makes more sense.
            format_medium_ostext (255, 57, 57, 255) //Solid Red  //"END CALL"
            DISPLAY_TEXT ((g_SF_PhonePosX + 0.01), (g_SF_PhonePosY + 0.16), "CELL_202")

        BREAK


        CASE PDS_RUNNINGAPP


            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessage")) > 0 //if text message app is running then we want to display "Options and back" help text.
                
                
                IF g_DoOptionsForSingleMessage = FALSE 
                //If a single message is selected, let appTextMessage handle the individual options for this. We need to grey out parts depending on lock status so it's better handled in the app itself.

                    //Display options for full display list.
                    Draw_Options_Back_HelpText()
                
                ENDIF

            ELSE

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSideTask")) > 0

                    Draw_SideTask_Back_HelpText()
                
                ELSE
                
                    Draw_Select_Back_HelpText()

                ENDIF

            ENDIF

        BREAK

            
            


        DEFAULT
            //Do nothing as yet...
        BREAK

    
    ENDSWITCH




ENDPROC

#endif



                                                                                                                                           

//Manipulate the 3d phone's screen position and rotation a little to emulate a vibration effect when an incoming call or text is received.
PROC Vibrate_and_Ring_3dPhone()

    #if IS_DEBUG_BUILD

        //cdPrintstring ("3d phone is ringing and vibrating...")
        //cdPrintnl ()

    #endif

	 SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_POSITIVE_INPUT)))  //3176604
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK) 

    // B*2205053
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_POSITIVE_INPUT)))
        SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
    ENDIF

    IF g_bInMultiplayer 
    
       g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE)

    ENDIF





    IF IS_PC_VERSION()
        IF DECOR_EXIST_ON(PLAYER_PED_ID(), "Synched")
            
            #if IS_DEBUG_BUILD

                cdPrintstring ("Cellphone_FH. Decorator presence detected. Not vibrating phone...")
                cdPrintnl ()

            #endif

        ELSE

            IF GET_GAME_TIMER() - i_VibrationTimer >= 300 OR i_VibrationTimer = 0 OR i_VibrationTimer > GET_GAME_TIMER()
                SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)   
                i_VibrationTimer = GET_GAME_TIMER()
            ENDIF
        
        ENDIF

    ELSE

        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer = 1 //Vibrate is set to ON.
            #IF IS_DEBUG_BUILD
            AND NOT GET_COMMANDLINE_PARAM_EXISTS("nocellvibrate")
            #ENDIF
            IF GET_GAME_TIMER() - i_VibrationTimer >= 300 OR i_VibrationTimer = 0 OR i_VibrationTimer > GET_GAME_TIMER()
                SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)   
                i_VibrationTimer = GET_GAME_TIMER()
            ENDIF
        ENDIF

    ENDIF


    

    //IF NOT ARE_STRINGS_EQUAL (This_Cellphone_Owner_Settings_ListContents[1].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer], "CELL_813") 




//    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
//
//        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
//
//        PLAY_AUDIO_EVENT("MOBILE_PRERING")
//
//        Interference_Start_Time = GET_GAME_TIMER()
//
//
//    ELSE
//
//        Interference_Current_Time = GET_GAME_TIMER()
//
//        IF Interference_Current_Time - Interference_Start_Time > 2000

            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())        
                //IF HAS_SOUND_FINISHED (g_Phone_Ringtone_SoundID)  //#1392094 swap
                IF NOT IS_PED_RINGTONE_PLAYING (PLAYER_PED_ID())    //#1392094 swap
                

                    IF g_bInMultiplayer


                        IF NOT ARE_STRINGS_EQUAL (This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[GET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_RINGTONE)], "Silent Ringtone Dummy")

                            //#1392094 swap
                            //PLAY_SOUND_FROM_ENTITY (g_Phone_Ringtone_SoundID, This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[GET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_RINGTONE)], PLAYER_PED_ID())

                            ///#1392094 swap
          

                            PLAY_PED_RINGTONE (This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[GET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_RINGTONE)], PLAYER_PED_ID(), TRUE)


                        ENDIF


                    ELSE

                        //If the ringtone has been set to "Silent" don't play anything. It just uses a dummy filename.
                        IF NOT ARE_STRINGS_EQUAL (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, "Silent Ringtone Dummy")

                            //Likewise, don't play if we are in "quiet mode".
                            IF NOT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_QUIET_MODE))

                                //#1392094 swap
                                //PLAY_SOUND_FROM_ENTITY (g_Phone_Ringtone_SoundID, g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, PLAYER_PED_ID())
                           

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_FH Cued SP RINGTONE...")
                                    cdPrintnl()
                                    cdPrintnl()
                                #endif



                                IF IS_PC_VERSION()

                                    IF DECOR_EXIST_ON(PLAYER_PED_ID(), "Synched")
                                    
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring ("Cellphone_FH. Decorator presence detected. Cancelling player phone ringtone...")
                                            cdPrintnl()
                                        #endif

                                    ELSE

                                        PLAY_PED_RINGTONE (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, PLAYER_PED_ID(), TRUE)
                                    
                                    ENDIF                               
                    

                                ELSE

                                    //#1392094 swap
                                    PLAY_PED_RINGTONE (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, PLAYER_PED_ID(), TRUE)

                                ENDIF

                            ENDIF
                                               
                         

                        ENDIF

                    ENDIF

                ENDIF
            ENDIF

        //ENDIF

    //ENDIF


    //ENDIF


ENDPROC











PROC MovetoFirstPersonVanishPhone()

    IF g_Phone_Blocked_While_Moving_Down = FALSE
    AND g_3DPhoneNeedsMovedUp = FALSE

        IF MovePhoneToDestination(g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio], g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_3dPhoneStartRotVec, g_3dPhoneStartRotVec, 350.0, FALSE) >= 1.0
            s_iTimeStampStart = 0

                g_FirstPersonTransitionOngoing = FALSE


                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_FH - Move to first person vanish concluded.")
                    cdPrintnl()
                #endif
    

        ENDIF
    ENDIF


ENDPROC





PROC CallInProgressVanishPhone()


    IF g_Phone_Blocked_While_Moving_Down = FALSE
        
                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_FH - Testing vanish routine")
                    cdPrintnl()
                #endif

                
        IF MovePhoneToDestination(g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio], g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_3dPhoneStartRotVec, g_3dPhoneStartRotVec, 350.0, FALSE) >= 1.0
            s_iTimeStampStart = 0

                //Set appropriate bits when finished vanish routine...
                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)
                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)

                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_FH - Cellphone has entered STANDARD DEF fully vanished state during call.")
                    cdPrintnl()
                #endif
    
        ENDIF

        ENDIF



ENDPROC





PROC CallInProgressReinstatePhone()

    IF g_Phone_Blocked_While_Moving_Down = FALSE

        IF MovePhoneToDestination(g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio], g_3dPhoneStartRotVec, g_3dPhoneStartRotVec, 350.0, TRUE) >= 1.0
            s_iTimeStampStart = 0
            //Clear bit when finished...
            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)
    
            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_FH - Cellphone has entered fully reinstated state during call.")
                cdPrintnl()

            #endif


            IF g_IsThisAnMPChatCall

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) //As this is a chat call, clear this so we can go hands free multiple time in one call...

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_FH - This was a Chat Call Hands Free reinstatement - resetting critical ALREADY VANISHED bit for reuse.")
                    cdPrintnl()

                #endif

            ENDIF

        
        ENDIF


    ENDIF

ENDPROC                                    








//After the phone is finished with for whatever reason, this proc will move it back to its position below the screen, 
//then destroy the phone model and terminate this script.

PROC Move3dPhoneDownScreen_Then_Terminate()


    CellphoneAppAutoLaunchType = ACT_APP_NONE

    g_ShouldForceSelectionOfLatestAppItem = FALSE



    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 605548


    //Hide hud elements that appear in bottom right corner.
    HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_VEHICLE_NAME)
    HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_AREA_NAME)
    HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_DISTRICT_NAME)   
    HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_STREET_NAME)



    IF g_Phone_Blocked_While_Moving_Down = FALSE
        
        SET_MOBILE_PHONE_SCALE(500.0)

  
        IF ShouldFirstPersonPhoneHudMovementBeHidden()

            s_iTimeStampStart = GET_GAME_TIMER()  //1983208

        ELSE

            s_iTimeStampStart = 0  //Don't need to slow the removal process down in 3rd person.

        ENDIF


        SCRIPT_IS_MOVING_MOBILE_PHONE_OFFSCREEN (TRUE)
        
        g_Phone_Blocked_While_Moving_Down = TRUE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - Setting IS_MOVING_MOBILE_PHONE_OFFSCREEN to true.")
            cdPrintnl()
        #endif

    ENDIF
        
    //IF NOT (HAS_SOUND_FINISHED (g_Phone_Ringtone_SoundID))    //#1392094 swap
    
    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        IF IS_PED_RINGTONE_PLAYING (PLAYER_PED_ID())

            //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
            STOP_PED_RINGTONE (PLAYER_PED_ID())

        ENDIF
    ENDIF
    


    
    //Deliberately not using an ELSE here...
    IF IS_ENTITY_DEAD(PLAYER_PED_ID())

        IF IS_PED_RINGTONE_PLAYING (PLAYER_PED_ID())

            //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
            STOP_PED_RINGTONE (PLAYER_PED_ID())

        ENDIF   


    ENDIF
    


    IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = TRUE 

        g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = FALSE

        CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

    ENDIF


    FLOAT fRate = 350.0
    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY)
    OR IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH) //Added to fix url:bugstar:1866104    
        //fRate = 100.0
        fRate = 25.0   //See bug 1796856, Alan requested this moved off screen quicker.

    ENDIF
    
    
    VECTOR endVec = g_3dPhoneStartRotVec
    // Pocket Work
    IF g_B_ForcedAway = TRUE
        endVec = <<-45,45,25>>
    ENDIF   



    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)  //Fixes issue with phone popping up then disappearing when a call concludes whilst the phone was already vanished but not destroyed.
    OR IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY) //2217483
    OR IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH) //Added to fix url:bugstar:1866104  


        AwayVec = g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio]
    
    ELSE

        AwayVec = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]

    ENDIF

    FLOAT fComplete = MovePhoneToDestination(AwayVec, g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_3dPhoneStartRotVec, endVec, fRate, FALSE)
    




        

    IF NOT b_PhoneDestroyed AND fComplete >= 1.0
        DESTROY_MOBILE_PHONE ()
        b_PhoneDestroyed = TRUE
    ENDIF

    //For bug 353016, need to have longer delay in SP only for phone going to pocket, phone not created in hand in MP. 
    //Update: For bug 988612, I'm making both delays the same as a trial.           
    IF b_PhoneDestroyed AND GET_GAME_TIMER() - s_iTimeStampStart > 500 // give it some time after destroying the phone to fix 'pink phone' display
        //g_DelayCinematicCamReinstatement = INITIALISE_DELAY //Tell cellphone controller that I want to keep disabling the "negative" button to stop cinematic cam popping in for example.

        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 605548



        IF g_Cellphone.PhoneDS = PDS_AWAY
        
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone has been set to AWAY state. 3d phone moved offscreen and terminating cellphone_fh.sc when done.")
                cdPrintnl()
                cdPrintnl()
            #endif
    
        ENDIF
        
        //Deliberate no use of ELSE here. The phone should not get here if it's not in AWAY or DISABLED state. If this runs and no debug text shows then something is wrong.

        IF g_Cellphone.PhoneDS = PDS_DISABLED
        
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone has been set to DISABLED state. 3d phone moved offscreen and terminating cellphone_flashhand.sc when done.")
                cdPrintnl()
                cdPrintnl()
            #endif
    
        ENDIF



        IF g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY
        
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone has been set to DISABLED_THIS_FRAME_ONLY state. 3d phone moved offscreen and terminating cellphone_flashhand.sc when done.")
                cdPrintnl()
                cdPrintnl()
            #endif
    
        ENDIF




        SCRIPT_IS_MOVING_MOBILE_PHONE_OFFSCREEN (FALSE) //See bug 740666

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - Clearing IS_MOVING_MOBILE_PHONE_OFFSCREEN flag to false.")
            cdPrintnl()
        #endif

        
        //Tidy up or initialise critical bools...
        g_3DPhoneNeedsMovedDown = FALSE

        g_B_Scaleform_Movies_Loaded = FALSE

        
        g_Cellphone_Launched_by_Button_Press = FALSE

        IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = TRUE 

            g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = FALSE

            CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

        ENDIF

        g_Cellphone_FH_Needs_To_Exit = TRUE
        

        //Get rid of the cellphone 3d model from below the screen.
        //DESTROY_MOBILE_PHONE ()

        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
            STOP_PED_RINGTONE (PLAYER_PED_ID()) //#1392094 swap
        ENDIF
        
        //STOP_SOUND (g_Phone_Ringtone_SoundID)         //#1392094 swap 


        //RELEASE_SOUND_ID (g_Phone_Ringtone_SoundID)   //#1392094 swap



        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_FH - 3d cellphone destroyed - cellphone flashhand.sc exiting.")
            cdPrintnl()

        #endif


        //CRITICAL!

        IF g_b_RadarWasAlreadyHidden = FALSE //Phone wasn't already hidden, safe to turn back on.

            //Phone back on RHS, commented out but infrastucture left intact.
            //DISPLAY_RADAR (TRUE) //switch on radar!
        
        ENDIF


        g_Phone_Active_but_Hidden = FALSE




        //If the player is on a mission and has perhaps started it with the phone already on the settings list,
        //make sure that SLEEP profile mode cannot persist by returning to normal mode when the phone is put away.
        IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
            SET_CELLPHONE_PROFILE_TO_NORMAL() //from cellphone_public.sch
        ENDIF 


        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
        
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY)
        
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN) 
        
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE) 

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)

     
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH)

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)

        CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

        g_LaunchContactsFromTextMessage = FALSE

        LaunchContactsFromTextMessage_LocalSignal = FALSE

        


        //CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_CELLPHONE_MOVIE_STREAMING_IN)  //Comment in for resolution of 1135663 


        IF NOT IS_PLAYER_PLAYING(PLAYER_ID())

             KillAllConversations ()  //Failsafe that will kill any active conversations after phone has been put away and player has died.

             //The KillAll needs to be here to make sure any script timing does not effect conversation tidy up. A similar check takes place in dialogue handler
             //to make sure, everything is cleaned up if the phone was not up at the time of death.
                

        ENDIF
       




        //Tidy up scaleform movies...



        //This needs to be here to ensure timing issues. as seen in bug 110732, can be avoided. We don't mark the critical scaleform movies as no longer needed until
        //certain that any applications have stopped processing. Applications must ensure they are using CHECK_FOR_ABNORMAL_EXIT or will fall foul here.

        SETTIMERA(0)
        
        
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppINTERNET].AppScriptNameHash) = 0
            WHILE IS_THREAD_ACTIVE (Application_Thread)

                WAIT (0)

                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 605548


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_FH - Waiting on the script that initiated application thread to terminate itself.")
                    cdPrintnl()
                    
                    IF g_b_DisplayDrawingMovieDump
                        cdPrintstring ("CELLPHONE_FH - Drawing movie this frame whilst in waiting for application exit loop.")
                        cdPrintnl()
                    ENDIF

                #endif

                IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) //Trial addition for bug 988612. Conceivable an app was taking a while to exit and movie may not have been rendering.

                    SET_TEXT_RENDER_ID (PhoneRenderTarget_ID)

                    DRAW_SCALEFORM_MOVIE (SF_MovieIndex, g_SF_PhonePosX, g_SF_PhonePosY, g_SF_PhoneSizeX, g_SF_PhoneSizeY, 255, 255, 255, 255)
                            
                ENDIF


                IF TIMERA() > 5000



                    #if IS_DEBUG_BUILD

                        PRINTNL()
                        PRINTSTRING ("CELLPHONE_FH - Looks like IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY from another app is stuck. Terminating after five seconds timeout has elapsed.")
                        PRINTNL()

                    #endif

                    
                    TERMINATE_THREAD (Application_Thread)

                ENDIF


            ENDWHILE
        ENDIF

     
        //All applications must have terminated, provided they were launched with LAUNCH_CELLPHONE_APPLICATION or had the correct "application_thread" specification.
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintnl()
            cdPrintstring ("CELLPHONE_FH - Marking cellphone scaleform movies as no longer needed from cellphone_flashhand.sc")
            cdPrintnl()
            cdPrintnl()
        #endif


        g_b_ToggleButtonLabels = FALSE

        IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) //This needs to be done here, in case the movie aborts. See bug 740117 

            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 0) //Flush out any streamed resources used by cellphone. See bug 535099. Instance 1.

            LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SHUTDOWN_MOVIE")

        ENDIF

        WAIT (0)


        SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieIndex)
        //SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieButtonsIndex)

    

        g_Phone_Blocked_While_Moving_Down = FALSE  //Important this is done here so that calls are barred until the cellphone has fully cleared up.

   
        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 605548




        IF IS_ENTITY_DEAD(PLAYER_PED_ID())

            IF voiceSession.bInVoiceSession  //#1635701
                
                IF g_bInMultiplayer

                    PLAY_SOUND_FRONTEND (-1, "Hang_Up", "Phone_SoundSet_Michael")

                ELSE
                
                    PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)

                ENDIF

            ENDIF

        ENDIF










        //Network is in voice session check before doing this?  Maybe add bool 

   
        SCRIPT_END_A_VOICE_SESSION()  //1617499 Make sure that a script hang up also terminates a chat call.  Check incoming call stage?

        CLEAR_VOICE_SESSION_STRUCT() //Fix for 1891329.
              
        /*  Potential avenue to explore: when this happens... don't end a voice session?

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 51. Cellphone_flashhand assigns PDS_AWAY")
                cdPrintnl()   
            #endif


        */

        g_IsThisAnMPChatCall = FALSE //This has been moved here for 2111525. It was required so that the hands free handler is definitely reset.

        g_FirstPersonTransitionOngoing = FALSE


        #if IS_DEBUG_BUILD

            cdPrintString ("g_IsThisAnMPChatCall assigned to false from Cellphone_Flashhand 1...  2111525")
            cdPrintnl()
            cdPrintnl()

        #endif





        //Quit this script.
        TERMINATE_THIS_THREAD()


    ENDIF    
ENDPROC  //end of Move3dPhoneDownScreen_Then_Terminate() procedure.



















PROC Draw_DisplayStatus()



    #if IS_DEBUG_BUILD
        IF g_DoDebugTempDraw_Phone = TRUE
            tempDraw_Phone() // - Reinstate to draw temp button labels.
        ENDIF
    #endif






    IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED  //A call is waiting to be answered, the phone display state is PDS_ONGOING_CALL, player must accept or reject call.


        Vibrate_and_Ring_3dPhone()

        
        IF g_InputButtonJustPressed = FALSE
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)) //Negative action - user rejected incoming phone call. 
             
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP) //#Les Bug 719218
           
                IF g_ForcePlayerAnswer = FALSE //only let the player reject a call if he is not being forced to answer...


                    Play_Back_Beep()

                    g_InputButtonJustPressed = TRUE

                    g_InboundCallWaitAccRej = CALL_REJECTED

                    
                    g_LastCellphoneCallInterrupted = TRUE
       
                    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                    
                        //STOP_SOUND (g_Phone_Ringtone_SoundID) //#1392094 swap
                        STOP_PED_RINGTONE (PLAYER_PED_ID())
                    
                    ENDIF

                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_FH - Incoming call rejected by user.")
                        cdPrintnl()
                    #endif



                    //Stop vibration and reset position to normal?

                ELSE
            
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_FH - Unable to reject call as it has been created with FORCE ANSWER")
                        cdPrintnl()
                    #endif

                ENDIF

            ENDIF

        ENDIF         
        ENDIF
        
       
        
        IF g_InputButtonJustPressed = FALSE
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - user accepted incoming phone call.
        #if IS_DEBUG_BUILD
            OR  g_bFlowAutoplayInProgress = TRUE
        #endif


            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP) //# 1096865 / 1056009

                Play_Select_Beep()
                    
                g_InputButtonJustPressed = TRUE

                g_InboundCallWaitAccRej = CALL_ACCEPTED


                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                
                    //STOP_SOUND (g_Phone_Ringtone_SoundID) //#1392094 swap
                    STOP_PED_RINGTONE (PLAYER_PED_ID())
                
                ENDIF



                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString ("CELLPHONE_FH - User has actively pressed pick up button to answer an incoming call.")
                    cdPrintnl()

                
                    cdPrintstring ("CELLPHONE_FH - SuppTextButtonCall 01")
                    cdPrintnl()

                #endif

                Set_SF_Buttons_SuppText_For_ConnectedCall()
                TitleUpdate_Add_Hide_Phone_Soft_Key()

            ENDIF
          

        ENDIF
        ENDIF


    ELSE


        
        IF b_RemoteRingOngoing = FALSE

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)

                b_RemoteRingOngoing = TRUE

                SETTIMERB(0)

            ENDIF

        ELSE

            IF g_Cellphone.PhoneDS > PDS_AWAY //Fix for 946359. Prevent sound looping if phone has been forced away.
                IF TIMERB() > 1500 //Might need to make this value differentiate between a call from appContacts or a scripted cut type call.
                    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())        
                        //IF HAS_SOUND_FINISHED (g_Phone_Ringtone_SoundID)  //#1392094 swap
                        IF NOT IS_PED_RINGTONE_PLAYING (PLAYER_PED_ID())    //#1392094 swap
                
                            IF g_Use_Prologue_Cellphone

                                //PLAY_SOUND_FRONTEND (g_Phone_Ringtone_SoundID, "Dial_and_Remote_Ring", g_Owner_Soundset) //#1392094 swap
                                PLAY_PED_RINGTONE ("Dial_and_Remote_Ring", PLAYER_PED_ID(), true) //#1392094 swap

                            ELSE

                                //PLAY_SOUND_FROM_ENTITY (g_Phone_Ringtone_SoundID, "Phone_Generic_Remote_Ring_01", PLAYER_PED_ID())

                                //PLAY_SOUND_FRONTEND (g_Phone_Ringtone_SoundID, "Remote_Ring", g_Owner_Soundset)   //#1392094 swap
                                
                                //1552162 - If Alwyn's switch call bit is set, do not play remote ring...
                                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START)
                                    PLAY_PED_RINGTONE ("Remote_Ring", PLAYER_PED_ID(), true) //#1392094 swap
                                ENDIF


                            ENDIF

                        ENDIF
                    ENDIF
                ENDIF
            ENDIF


            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)

                b_RemoteRingOngoing = FALSE


                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
 
                    //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
                    STOP_PED_RINGTONE (PLAYER_PED_ID())

                ENDIF
    

            ENDIF


        ENDIF
   





        IF g_InboundCallWaitAccRej = CALL_NONE_WAITING

      

            //CRITICAL SECTION! This specifies what state the phone will revert to after "back" has been pressed.
                
            
            //Either have a keypress delay or check for disabled and away states before accepting back input.


              
            IF g_InputButtonJustPressed = FALSE
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) 
            OR g_LaunchContactsFromTextMessage = TRUE //1636892
 
       
                IF IS_PLAYER_PLAYING (PLAYER_ID())
                    
                    //Could be taken out on a trial basis for Conor's TV stuff...
                    IF IS_PLAYER_CONTROL_ON (PLAYER_ID())
                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP) //Les Bug 719218
                
                        


                            //g_InputButtonJustPressed = TRUE  Removed on a trial basis 24.11.10  - was affecting MPJobList "waiting for confirmation" stuff
                            //IS_BUTTON_JUST_PRESSED should take care of any weirdness.
                            
                                         
                            //New measures to determine exact phone state when button was pressed. 1924417
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - Cellphone flashhand reports that the back button has been pressed... Phone State is ")
                                cdPrintInt(ENUM_TO_INT(g_Cellphone.PhoneDS))
                                cdPrintstring(" and Conv State is ")
                                cdPrintint(ENUM_TO_INT(g_ConversationStatus))
                                cdPrintnl()
                                IF g_IsThisAnMPChatCall
                                    cdPrintstring("CELLPHONE_FH g_IsThisAnMPChatCall is TRUE on back button press.")
                                ELSE
                                    cdPrintstring("CELLPHONE_FH g_IsThisAnMPChatCall is FALSE on back button press.")
                                ENDIF
                                cdPrintnl()
                            #endif

                    
                        






                            /*
                            Play_Back_Beep()
                            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                                PLAY_SOUND_FROM_ENTITY (-1, "PHONE_GENERIC_KEY_01", PLAYER_PED_ID())

                            ENDIF
                            */


                            SWITCH g_Cellphone.PhoneDS


                                             
                                CASE PDS_AWAY
                                
                                   #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - Phone already in away state...")
                                        cdPrintnl()

                                                           
                                    #endif
                
                                BREAK
                



                                CASE PDS_TAKEOUT
                                
                                   #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - Yes / no response - avoiding unknown state timing issue.")
                                        cdPrintnl()

                                                           
                                    #endif
                
                                BREAK



                            
                                CASE PDS_MAXIMUM

                                    //Don't allow the phone to be put away by the user if the phone is hidden...
                                    IF g_Phone_Active_but_Hidden = FALSE

                                        //New addition... 20.09.10

                                        IF g_Homescreen_In_Secondary_Mode = TRUE

                                            
                                            Play_Back_Beep()


                                            g_Homescreen_In_Secondary_Mode = FALSE


                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)  //Quickly jump to another screen to tidy up and reset home screen

                                            #if IS_DEBUG_BUILD

                                                cdPrintstring ("CELLPHONE_FH - Update_Scaleform_VSE called - 201")
                                                cdPrintnl ()
                                                cdPrintstring ("Display View 4 - Cellphone Flashhand 1")
                                                cdPrintnl()
        
                                            #endif

                                            Fill_All_Primary_App_Sheets()
                                            TitleUpdate_Patch_In_JobList_For_SP_Phone()
                                            //Could go!
                                            //Fill_All_Secondary_App_Sheets()

                                            Scaleform_Place_Items_on_Homescreen(1)

                              

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (Previous_SelectedAppCursorPos_PageOne)) //Set enum state to HOME_SCREEN

                                            #if IS_DEBUG_BUILD

                                                cdPrintstring ("CELLPHONE_FH - Update_Scaleform_VSE called - 202")
                                                cdPrintnl ()
        
                                            #endif                                    


                                        ELSE

                    
                                            //Play_Back_Beep()


                                            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                                                PLAY_SOUND_FRONTEND (-1, "Put_Away", g_Owner_Soundset) //Les TODO 1517916

                                            ENDIF





                                            //Put in this failsafe to make sure any apps left hanging get cleaned up when the phone goes away. 100374
                                            /* Update: app itself should do this via ABNORMAL EXIT check when PhoneDS is set to PDS_AWAY
                                            g_TerminateApp = TRUE

                                        
                                            #if IS_DEBUG_BUILD
                                                cdPrintstring ("g_TerminateApp set to TRUE - any apps in running state should exit if CHECK_APPLICATION_EXIT is integrated.")
                                                cdPrintnl ()
                                            #endif

                                            */


                                            s_iTimeStampStart = 0
                                            g_Cellphone.PhoneDS = PDS_AWAY

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("STATE ASSIGNMENT 51. Cellphone_flashhand assigns PDS_AWAY")
                                                cdPrintnl()   
                                            #endif



                                      
                                        ENDIF

                                    ENDIF

                                BREAK




                                CASE PDS_MINIMAL 
                                
                                    g_Cellphone.PhoneDS = PDS_AWAY

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("STATE ASSIGNMENT 52. Cellphone_flashhand assigns PDS_AWAY")
                                        cdPrintnl()   
                                    #endif


                                BREAK




                                //CRITICAL: Running App to Homescreen transition on "Back" press
                                //Might need to move parts of this to check_application_exit

                                CASE PDS_RUNNINGAPP
                                    
                                    //Terminate any running app if it is checking CHECK_APPLICATION_EXIT(), set the view state to PDS_MAXIMUM
                                    //and restore homescreen by updating View State Enum. Apps in PDS_COMPLEXAPP state will not exit.

                                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD) = TRUE //#1431413

                                        #if IS_DEBUG_BUILD

                                            PRINTSTRING("Not allowing app exit as g_BS_WAITING_ON_APP_TO_LOAD is true this frame")
                                            PRINTNL()

                                        #endif

                                    ENDIF



                                    IF (g_App_Elapsed_SafetyBuffer - g_App_SafetyBuffer_StartTime) > g_App_Required_SafetyBuffer_Duration //Make sure script has had a chance to launch before exit... See bug 25815
                                    AND IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD) = FALSE //#1431413

                                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS)  
                                
                                
                                            #if IS_DEBUG_BUILD

                                                PRINTSTRING("Not allowing Contacts exit as this is the Finale Choice list!")
                                                PRINTNL()

                                            #endif

                                        
                                        ELSE
                                        
                                            Play_Back_Beep()


                                            g_TerminateApp = TRUE

                                            
                                            #if IS_DEBUG_BUILD
                                                cdPrintstring ("CELLPHONE_FH - g_TerminateApp set to TRUE - app in running state should exit if CHECK_APPLICATION_EXIT is integrated.")
                                                cdPrintnl ()
                                            #endif



                                            //Trial Exclusion Run 24.09.10 - this is now being done in CHECK_APPLICATION_EXIT to ensure that scripts which are special exceptions
                                            //e.g appContacts, do not run Update_Scaleform_VSE twice (it will be in their cleanup ) as that can cause a screwy sliding effect.

                                            //Trial failed. This needs to be here in order to guarantee the pick up of the back button press. Having a timed stagger on the acceptance
                                            //of button presses seems a safer solution to PDS conflicts.
                                            
                                            
                                            g_Cellphone.PhoneDS = PDS_MAXIMUM

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("STATE ASSIGNMENT 53. Cellphone_flashhand assigns PDS_MAXIMUM")
                                                cdPrintnl()   
                                            #endif




                                            IF g_bInMultiplayer
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (Temp_MPcurrentTheme)) 
                                            
                                                #if IS_DEBUG_BUILD

                                                    cdPrintstring("Cellphone_flashhand - Re-establish MP theme on return to maximum. May have been altered by Agency App.")
                                                    cdPrintint(Temp_MPCurrentTheme)
                                                    cdPrintnl()
                                                #endif
                                            ENDIF




                                            //Make sure clock can update immediately...
                                            ClockThrottleMins = 99


                                            IF g_LaunchContactsFromTextMessage = FALSE //1636892 - only refresh the homescreen and return to that state if we are not immediately launching the contacts app.

                                                Update_Scaleform_VSE ()

                                            ENDIF




                                        ENDIF

                                    ENDIF
                                    

                                                    
                                BREAK                                                                         




                                CASE PDS_COMPLEXAPP

                                    #if IS_DEBUG_BUILD
                                        cdPrintstring ("CELLPHONE_FH - Unless app-specific check is made, flashhand not exiting app as it is in complex state...")
                                        cdPrintnl ()
                                    #endif

                                BREAK



                                
                                CASE PDS_ATTEMPTING_TO_CALL_CONTACT
                   

                                    IF g_TheContactInvolvedinCall = CHAR_CHAT_CALL  //Adding this on a trial basis to prevent quitting before PROCESS_VOICE_SESSION_STARTED_EVENT() has fired.
                                        
                                        IF voiceSession.bInVoiceSession    //1602347
                                        OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)


                                            Play_Back_Beep()

                                            Remove_Cellphone_From_Ear()


                                        ELSE

                                            #if IS_DEBUG_BUILD
                                                
                                                cdPrintnl()
                                                cdPrintstring("CELLPHONE_FH - Not removing phone from ear as InVoiceSession has not yet returned TRUE for attempted chat call.")
                                                cdPrintnl()     

                                            #endif

                                        ENDIF
                                            
                                    ELSE

                                        Play_Back_Beep()


                                        //See bug 1924417  - Perhaps put in a counter for repeated button presses...? Audio not streaming in.


                                    //IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appMPPlayerList")) = 0   //AppMPPlayerlist has been removed from the project.

                                        Remove_Cellphone_From_Ear()


                                        //This was removed for #1580608 - search on this bug number for related changes. Re-establish appContacts after aborting an attempted call.
                                                     
                                        //This meant we just went back to PDS_MAXIMUM after aborting an attempted call. Much simpler and cleaner.
                                        /*                                          
                                        g_Cellphone.PhoneDS = PDS_MAXIMUM

                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("STATE ASSIGNMENT 54. Cellphone_flashhand assigns PDS_MAXIMUM")
                                            cdPrintnl()   
                                        #endif


                                        Update_Scaleform_VSE ()


                                        */


                                        //Keep this!
                                        g_LastCellphoneCallInterrupted = TRUE


                                    ENDIF
                             

                                    /*
                                    ELSE    //If the player list is running then this meant the "attempting to call contact" state has been entered via
                                            //cellphone_public procedure LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL. We don't want to tidy up as normal. 
                                            //That should be left to the player list app itself to decide what to do, so we return to its last known state.

                                            
                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("CELLPHONE_FH - Attempted STATE ASSIGNMENT 54 routine but appMPPLAYERLIST was running. Performing STATE ASSIGNMENT 7754.")
                                            cdPrintnl()  
                                        #endif

                                        TERMINATE_APPCONTACTS_FOR_CHAT_CALL()  //will let appContacts know it should terminate via bit set.
                                                    
                                                                            

                                    ENDIF
                                    */


                               
                                BREAK
                                                        



                                CASE PDS_ONGOING_CALL //Might not need this - should get done by dialogue handler at "critical" marked line. Have removed on a trial basis. 26.05.10
                                    
                                    //13.10.11 Update - I'm adding this segment, with a conv check,  as part of a suite of things to address bug 246816. The phone seemed to be
                                    //stuck in ONGOING_CALL due to extreme game stress and calls coming in when it wasn't safe from MP. This may impact BeforeCallStateHandler, so note made there,
                                    
                                    //This should always mirror the post phonecall behaviour of BeforeCallStateHandler() 
                                                                
                                    IF g_ConversationStatus = CONV_STATE_FREE
                                                                                                                                         
                                        g_Cellphone.PhoneDS = PDS_AWAY

                                        #if IS_DEBUG_BUILD
                                            cdPrintnl()
                                            cdPrintstring("STATE ASSIGNMENT 55. Cellphone_flashhand assigns PDS_AWAY")
                                            cdPrintnl()   
                                        #endif


                                        #if IS_DEBUG_BUILD

                                            cdPrintstring("CELLPHONE_FH - Reporting unusual PDS_ONGOING_CALL exit")
                                            cdPrintnl()

                                        #endif


                                    ELSE

                                        #if IS_DEBUG_BUILD

                                            cdPrintstring("CELLPHONE_FH - back button pressed whilst in PDS_ONGOING_CALL - conv state NOT free.")
                                            cdPrintnl()

                                        #endif



                                    ENDIF
                                
                                    
                                BREAK





                                DEFAULT
                                    
                                    #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - Strange phone_state = ")
                                        cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneDS))
                                        cdPrintnl()

                                        //SCRIPT_ASSERT("Phone in strange state. Tell Steve T.")
                                
                                    #endif

                                BREAK


                            ENDSWITCH




                            IF g_LaunchContactsFromTextMessage = TRUE //1636892

                                LaunchContactsFromTextMessage_LocalSignal = TRUE //1636892 use this to specify we want to launch the app now we have received the global from AppTextMessage.sc
                            
                                g_LaunchContactsFromTextMessage = FALSE //1636892 Reset the global so this procedure does not run inadvertently.

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("CELLPHONE_FH - Cellphone flashhand reports that g_LaunchContactsFromTextMessage was TRUE")
                                    cdPrintnl()
                                #endif

                            ENDIF







                        ELSE

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - Can't put phone away, fully moved up bit not set. #719218")
                                cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneDS))
                                cdPrintnl()

                            #endif

                    
                        ENDIF //Moved up check.
                    ENDIF //Player alive, control on checks.
                ENDIF

            
            ENDIF //button pressed checks.
            ENDIF

        ENDIF
        

    ENDIF



ENDPROC












//This procedure is just a simple way of ensuring that a single button press does not carry throughtout menu navigation. In a scaleform context, WAITs are bad news.
//Every button press has a condition which checks if g_InputButtonJustPressed = FALSE before accepting input. Once input has been accepted the state is then set to TRUE
//and this script will run this proc continually to check if the buttons have been released, subsequently allowing for further input.
PROC Check_For_Just_Pressed_Revert()

                                                                                      
    
    IF NOT IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
    AND NOT IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //is the same key as negative input. 
    //We may need to add more button presses to the revert.

        g_InputButtonJustPressed = FALSE

    ENDIF


ENDPROC





 //After the phone is taken out via cellphone controller or cellphone public's call to Create_3dPhoneObject() which starts this script, move the phone into position.
PROC Move3dPhoneUpScreen()  
    IF IS_BIT_SET (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
        SET_BIT (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

        g_3DPhoneNeedsMovedUp = FALSE
        s_iTimeStampStart = 0

        //Pocket work.
        SET_MOBILE_PHONE_ROTATION (g_3dPhoneStartRotVec)
        //CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH) //Removed to fix url:bugstar:1866104
    ELSE


        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appEmail")) < 1

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("Phone doing NORMAL transition")
            #endif
            

            FLOAT fComplete = MovePhoneToDestination(g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio], g_3dPhonePocketRotVec, g_3dPhoneStartRotVec, 450.0, TRUE)
     

            IF fComplete >= 1.0
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
        SET_BIT (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

        g_3DPhoneNeedsMovedUp = FALSE
        s_iTimeStampStart = 0

            
        CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

                b_NeedToAdjustEmailForCameraTransition = FALSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("COMPLETED Normal Transition")
                 #endif


                
            ELIF fComplete >= 0.75
                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
            ENDIF


    ELSE

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("Phone doing email transition")
            #endif


            IF b_NeedToAdjustEmailForCameraTransition = FALSE
                    
                v_3dEmailDesiredTransPosition = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]
                // original value was -6. Text-readable size is -10.
                v_3dEmailDesiredTransPosition.x -= 10.0 //Specifies the size of the left adjustment to make sure the entire body text is visible in landscape mode.

                v_3dEmailDesiredTransPosition.y += 20.0 //Fixes TRC bombshell with new anims by making sure the landscape phone lies above any long or four line subtitles - in German!

                b_NeedToAdjustEmailForCameraTransition = TRUE

            ENDIF





            FLOAT fComplete = MovePhoneToDestination(g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], v_3dEmailDesiredTransPosition, <<-90, 0, 90.0>>, <<-90, 0, 90.0>>, 450.0, TRUE)
        
        IF fComplete >= 1.0
            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

            g_3DPhoneNeedsMovedUp = FALSE
            s_iTimeStampStart = 0

        
            CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)
            
                b_NeedToAdjustEmailForCameraTransition = FALSE

                
        ELIF fComplete >= 0.75
            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
        ENDIF



        ENDIF

    ENDIF

       
ENDPROC

















PROC Setup_MainGui_Rendertarget()


    
    GET_MOBILE_PHONE_RENDER_ID (PhoneRenderTarget_Id)

     
    IF PhoneRenderTarget_ID = -1
        #if IS_DEBUG_BUILD
    
            SCRIPT_ASSERT ("CELLPHONE_FH - Cannot find mobile phone render target id for cellphone.")

        #endif
    ELSE
        #if IS_DEBUG_BUILD
    
            cdPrintstring ("CELLPHONE_FH - Mobile Phone Render target id for cellphone is ")
            cdPrintint (PhoneRenderTarget_Id)
            cdPrintnl()
            
        #endif 
    ENDIF


ENDPROC







PROC ProcessCellphoneSignalStrength()



        IF IS_PLAYER_PLAYING (PLAYER_ID())
         
            i_Affluence = GET_ZONE_SCUMMINESS(GET_ZONE_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID())))

        ENDIF

        //3 - poor, 0 - affluent

        //ENUM ZONE_SCUMMINESS
            //SCUMMINESS_POSH, - 0
            //SCUMMINESS_NICE,
            //SCUMMINESS_ABOVE_AVERAGE,
            //SCUMMINESS_BELOW_AVERAGE,
            //SCUMMINESS_CRAP,
            //SCUMMINESS_SCUM      5
        //ENDENUM


        SWITCH i_Affluence


            CASE 0
                testSignalInt = 5       
            BREAK

            CASE 1
                testSignalInt = 5       
            BREAK
        
            CASE 2
                testSignalInt = 4       
            BREAK

            CASE 3
                testSignalInt = 4       
            BREAK

            CASE 4
                testSignalInt = 3       
            BREAK

            CASE 5
                testSignalInt = 2       
            BREAK

            DEFAULT

                testSignalInt = 3

            BREAK

        ENDSWITCH


        i_SignalFluctuator = GET_RANDOM_INT_IN_RANGE (0, 8)  //returns 0 to 7 inclusive.


        IF i_SignalFluctuator < 2   //1 in 4 chance of a fluctuation occurring.

            IF testSignalInt > 2
                testSignalInt --
            ENDIF

        ENDIF


        IF b_inSubmersible = TRUE
        OR IS_PLAYER_IN_ANY_COMMUNICATION_RESTRICTED_AREAS() //#1132229

            testSignalInt = 0  //For # 1291886

        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), TO_FLOAT(testSignalInt))


        /*
        SWITCH testSignalInt

            CASE 0

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), TO_FLOAT(1)) 
         
            BREAK
         
            CASE 1

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), 2) 
         
            BREAK
            
            
            CASE 2

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), 3) 
         
            BREAK
         
            CASE 3

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), 4) 
         
            BREAK

            
            CASE 4

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), 5) 
         
            BREAK

        
                      
        ENDSWITCH
        */


ENDPROC







PROC Update_Cellphone_Clock_and_Health()

        

    IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //Make sure highlight always remains on snapmatic or mpjoblist if available in director mode whilst phone is on homescreen.
        
        //2411448
        IF g_CellphoneHasMPAccess

           IF g_Cellphone.PhoneDS = PDS_MAXIMUM



                IF appCursorIndex = 7

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (7)) //Set enum state to HOME_SCREEN
                
                ELSE

                    appCursorIndex = 6 //If the cursor is anything other than 6 or 7, which it shouldn't be in director mode, default back to 6 again.                   
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (6)) //Set enum state to HOME_SCREEN

                ENDIF

            ENDIF
        
        ELSE

            IF g_Cellphone.PhoneDS = PDS_MAXIMUM
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (6)) //Set enum state to HOME_SCREEN
            ENDIF
        
        ENDIF

    ENDIF



    //Update clock for badger_theme.
    //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].OSTypeForThisPlayer= OS_BADGER //Cellphone Badger type - includes clock.
                                                                                                    
    //IF g_Use_Prologue_Cellphone = FALSE     



        /* Homescreen Update test.
        IF g_Homescreen_In_Secondary_Mode = FALSE //If the player is on the homescreen, update any icons that should become greyed out due to a change in circumstances.

            Scaleform_Place_Items_on_Homescreen(1)

        ENDIF
        */



        ClockMins = GET_CLOCK_MINUTES()


     
        IF ClockMins <> ClockThrottleMins

            ClockThrottleMins = ClockMins

            ClockHours = GET_CLOCK_HOURS()


            //Comment in to display debug console clock time.
            /*
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintint(ClockHours)
                cdPrintnl()
                cdPrintint(ClockMins)
                cdPrintnl()
            #endif
            */

            /*
            ENUM DAY_OF_WEEK
                SUNDAY = 0,
                MONDAY,
                TUESDAY,
                WEDNESDAY,
                THURSDAY,
                FRIDAY,
                SATURDAY
            ENDENUM
            */


            ClockDay  = GET_CLOCK_DAY_OF_WEEK()


            SWITCH ClockDay


                CASE SUNDAY
                    TempDayLabel = "CELL_920"
                BREAK


                CASE MONDAY
                    TempDayLabel = "CELL_921"
                BREAK


                CASE TUESDAY
                    TempDayLabel = "CELL_922"
                BREAK


                CASE WEDNESDAY
                    TempDayLabel = "CELL_923"
                BREAK


                CASE THURSDAY
                    TempDayLabel = "CELL_924"
                BREAK


                CASE FRIDAY
                    TempDayLabel = "CELL_925"
                BREAK


                CASE SATURDAY   
                    TempDayLabel = "CELL_926"
                BREAK


                DEFAULT //Would be a helluva error if this returns!
                    TempDayLabel = "CELL_206"
                BREAK


            ENDSWITCH

        

            
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_TITLEBAR_TIME", TO_FLOAT(ClockHours), TO_FLOAT(ClockMins),
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, TempDayLabel)
            







            IF g_Use_Prologue_Cellphone = FALSE //don't set provider for prologue phone, as method is not accepted.


                ProcessCellphoneSignalStrength()

            
            ENDIF    




            //We need to get the player's health periodically so that we can reliably work out when he is actually taking damage. This is because the phone could have
            //been taken out when at minimal health and then regenerates to full. If he then takes damage, we need to make sure we are comparing against the new regenerated 
            //health rather than just the starting health grabbed when the phone initially launched.

            IF IS_PLAYER_PLAYING (PLAYER_ID())

                Player_PeriodicHealth = GET_ENTITY_HEALTH (PLAYER_PED_ID())
    
            ENDIF




        ENDIF

        
              


    //ENDIF



ENDPROC













PROC Perform_StockMarketChecks()


    IF NETWORK_IS_GAME_IN_PROGRESS()

        i_TimeDifference = GET_TIME_DIFFERENCE (GET_NETWORK_TIME(), Net_StockMarketChecks_StartTime)
    
    ELSE
    
        StockMarketChecks_CurrentTime = GET_GAME_TIMER()
        i_TimeDifference = StockMarketChecks_CurrentTime - StockMarketChecks_StartTime

    ENDIF





    //IF StockMarketChecks_CurrentTime - StockMarketChecks_StartTime > 4000
    IF i_TimeDifference > 4000

        i_Provider_Ident = Get_Cellphone_Provider()

        /*
        SWITCH i_Provider_Ident

            CASE PROVIDER_BADGER

                bsaq_mf_OpenDecision = BSMF_PHONEOPENTIME_FOR_BDG
                bsaq_mf_CallDecision = BSMF_CALLTIME_FOR_BDG 

            BREAK

            CASE PROVIDER_WHIZ

                bsaq_mf_OpenDecision = BSMF_PHONEOPENTIME_FOR_WIZ
                bsaq_mf_CallDecision = BSMF_CALLTIME_FOR_WIZ

            BREAK

            CASE PROVIDER_TINKLE

                bsaq_mf_OpenDecision = BSMF_PHONEOPENTIME_FOR_TNK
                bsaq_mf_CallDecision = BSMF_CALLTIME_FOR_TNK

            BREAK

            DEFAULT //Default to badger.

                bsaq_mf_OpenDecision = BSMF_PHONEOPENTIME_FOR_BDG
                bsaq_mf_CallDecision = BSMF_CALLTIME_FOR_BDG 

            BREAK


        ENDSWITCH



        */
        //Deliberately allowing this to update along with the call update, as the phone is still open. Easy to change to "else" clause of IS_MOBILE_CALL_ONGOING if need be.        
        ////BAWSAQ_INCREMENT_MODIFIER (bsaq_mf_OpenDecision, 1)

        /*
        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_FH - Performing open time usage stock update.")
            cdPrintnl()
         
        #endif
        */



        IF IS_MOBILE_PHONE_CALL_ONGOING()

            ////BAWSAQ_INCREMENT_MODIFIER (bsaq_mf_CallDecision, 1)

            /*
            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_FH - Performing call time stock update.")
                cdPrintnl()
             
            #endif
            */
        ENDIF

        //Interval has passed, need to re-establish new start time.
        IF NETWORK_IS_GAME_IN_PROGRESS()

            Net_StockMarketChecks_StartTime = GET_NETWORK_TIME()
        
        ELSE
        
            StockMarketChecks_StartTime = GET_GAME_TIMER()
        
        ENDIF




    ENDIF


ENDPROC






PROC Check_For_QuickLaunch_of_Snapmatic()

    //New block for 1855377 - Now GTAO only.
    //"Suggestion for a new feature: quick access to the mobile phone camera by pressing the "up" arrow twice (quickly)." 
    //"Just like real phone like iPhone have the option to go directly to the camera from the lock screen." 

    IF g_InputButtonJustPressed = FALSE //Dedicated check for second phone takeout control makes sure we can't carry through to positive input check.
       
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT), TRUE) //Fix for 2193313. Quick launch on PC needs to reuse TAKEOUT rather than NAV_UP to support mouse wheel. // B* 2285479 - Check disabled input so this works on keyboard and mouse.

            //Put back in when stat is available.
            IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_QUICKLAUNCH) = 1 //Snapmatic Quicklaunch stat has been set to ON in settings.


                //Added for 1918479 to handle potential block of critical messages. We don't want to launch Snapmatic if a critical message is pending for this player. 
                IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE

                    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME))
                    IF NOT (g_Use_Prologue_Cellphone)  //1911268

                    
                        IF NOT (IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC)) //1855377
                            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
                            

                                g_InputButtonJustPressed = TRUE //Makes sure we can't carry through to positive input check.

                                SET_BIT (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC) //This bit is cleared in the termination routine of appCamera, "Snapmatic" and on intial launch of flashhand.sc


                                CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppCamera, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                

                                /*
                                                                                                  
                                //Check_for_App_Selection() //moved 25.10.11 so that another app can't be selected before contacts launches.
                                LAUNCH_CELLPHONE_APPLICATION (AppCamera, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                
                                */

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("CELLPHONE_FH - Quick Launching Snapmatic... ")
                                        cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                        cdPrintnl()
                                    #endif

                                CellphoneAppAutoLaunchType = ACT_APP_NONE
                                                 

                            ENDIF
                        ENDIF


                    ENDIF
                    ENDIF

                ENDIF



            ENDIF // Stat check condition end.


        ENDIF
    ENDIF

    //End of new block for 1855377

ENDPROC







PROC Check_For_App_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.


    //Partial dummy routine for automatic AppInternet launch from hyperlinked text message.
    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM)
    OR LaunchContactsFromTextMessage_LocalSignal = TRUE //1636892    

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - Check_For_App_Selection has weirdly found an ACT_APP or Internet or g_LaunchContactsFromTextMessage launch.")
            cdPrintnl()
        #endif


        g_App_SafetyBuffer_StartTime = GET_GAME_TIMER()


        g_TerminateApp = FALSE  //make sure that Check_Application_Exit() doesn't fire off accidentally.

        
        g_InputButtonJustPressed = TRUE

   
        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

        testReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

        b_Waiting_On_SF_Return_Value = TRUE



    ELSE

   

            //Embedding this here in the ELSE branch makes sure that no apps can be selected during transition from text message to Internet after a Hyperlink launch.

            IF g_InputButtonJustPressed = FALSE


                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - application is being selected
                

                        //Play_Select_Beep() - Moved below, so we only beep if appLaunch has not been voided.
                        
                        IF local_Request_for_QuickSave = FALSE

                        
                            //IF g_HasCellphoneRequestedSave = FALSE   //Too risky to put this check in. If quicksave breaks then you might not be able to launch apps.
                        
                            
                                g_App_SafetyBuffer_StartTime = GET_GAME_TIMER()


                                g_TerminateApp = FALSE  //make sure that Check_Application_Exit() doesn't fire off accidentally.

                                
                                g_InputButtonJustPressed = TRUE

                      
                                //testReturnedSFInt = CALL_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")
                                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                                testReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

                                b_Waiting_On_SF_Return_Value = TRUE


                            //ELSE

                                 //#if IS_DEBUG_BUILD
                                    //cdPrintnl()
                                    //cdPrintstring("CELLPHONE_FLASHHAND - Can't launch app as g_HasCellphoneRequestedSave is TRUE!")
                                    //cdPrintnl()   
                                //#endif

                           //ENDIF

    
                        ELSE

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FLASHHAND - Can't launch app as local_Request_for_QuickSave is TRUE!")
                                cdPrintnl()   
                            #endif

                        ENDIF

                ENDIF
            ENDIF


    ENDIF



ENDPROC

#IF FEATURE_FIXER
FUNC STRING GET_BLOCK_INTERNET_APP_HELP_TEXT()
	// Return different labels here if on a specific mission etc.
	RETURN "BLOCK_APP_WEB"
ENDFUNC
#ENDIF

PROC Check_For_App_Return_Value_Ready()


    IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (testReturnedSFIndex)

      
        b_Waiting_On_SF_Return_Value = FALSE //Critical!

        testReturnedSFInt = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (testReturnedSFIndex)


        IF g_Homescreen_In_Secondary_Mode = FALSE

            Previous_SelectedAppCursorPos_PageOne = testReturnedSFInt

        ELSE

           Previous_SelectedAppCursorPos_PageTwo = testReturnedSFInt
          
        ENDIF   

        
        #if IS_DEBUG_BUILD

        
            cdPrintstring("CELLPHONE_FH - Returned SF INT via new style GCS is ")
            cdPrintint(testReturnedSFInt)
            cdPrintnl()


        #endif

        
        IF testReturnedSFInt < 0

            testreturnedSFInt = 0

        ENDIF
            

        SelectedAppIndex = AppSlot[testReturnedSFInt]




        //If automatic AppInternet launch from hyperlinked text message is in play, overwrite selection choice to force AppInternet as the launch app.
        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM) 

            SelectedAppIndex = ENUM_TO_INT (appINTERNET)

            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM) //clear bit to make sure normal usage resumes upon return to homescreen. 

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("CELLPHONE_FH - Check_For_App_Return_Value_Ready(). Internet Launch bit set. ")
                cdPrintnl()
            #endif



        ENDIF
        


        IF LaunchContactsFromTextMessage_LocalSignal = TRUE

            Previous_SelectedAppCursorPos_PageOne = 1

            SelectedAppIndex = ENUM_TO_INT (appCONTACTS)

            g_LaunchContactsFromTextMessage = FALSE //clear global to make sure normal usage resumes upon return to homescreen. 
            LaunchContactsFromTextMessage_LocalSignal = FALSE


            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("CELLPHONE_FH - Check_For_App_Return_Value_Ready(). LaunchContactsFromTextMessage_LocalSignal was set to true. ")
                cdPrintnl()
            #endif



        ENDIF

        

        //Launch app from homescreen but check for voids.

        BOOL b_AppLaunchVoided = FALSE


		//6417795 - May 2020
		//Critical!
		//New handling for AppDummyApp1 which is being used to launch Arcade Mode in MP.
		If g_bInMultiplayer
			#IF FEATURE_FREEMODE_ARCADE
			#IF FEATURE_COPS_N_CROOKS
			//Joblist, Arcade Mode and Internet App are disabled whilst in CnC. See bugs 6417788 6457063 6461213
			//Void their launch if in CnC mode.
			IF IS_CELLPHONE_IN_CNC()
		
				IF SelectedAppIndex = ENUM_TO_INT (AppDummyApp1) 
				OR SelectedAppIndex = ENUM_TO_INT (AppINTERNET) 
				OR SelectedAppIndex = ENUM_TO_INT (AppMPJOBLIST) 
							
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Voided CnC incompatible application launch. See bugs 6417788 6457063 6461213")
                        cdPrintnl()   
                    #endif
				
					b_AppLaunchVoided = TRUE
				
				ENDIF
		
			ELSE
			#ENDIF
			#ENDIF
				IF SelectedAppIndex = ENUM_TO_INT (AppDummyApp1) AND SHOULD_MP_SECONDARY_MODE_BE_AVAILABLE()
					
					//6478196 - Arcade Mode app selection now requires a confirmation screen.
					//This means we no longer void the launch of the script associated with the application.
					//The currently associated app, now AppZit.sc will launch as normal to handle the Arcade Mode transition.
					
					//Previous launch routine which went directly into the arcade mode transition when the app was selected.
					//It did not run the app itself.
					/*
					b_AppLaunchVoided = TRUE
					
					//Launch the MP technical transition. This calls an MP transition_common.sch proc to handle the mode swap.
					TRANSITION_TO_ARCADE_MODE(FALSE, TRUE, ARCADE_ENTRY_POINT_INVITE)
					
					#if IS_DEBUG_BUILD
		                cdPrintnl()
		                cdPrintstring("Detected user selection of AppDummyApp1 in MP. Voided any script launch.")
		                cdPrintnl()  				
	            	#endif
					*/
					
					#IF FEATURE_COPS_N_CROOKS
					REQUEST_CNC_TRANSITION_BG()
					#ENDIF
					
				ENDIF
			#IF FEATURE_FREEMODE_ARCADE
			#IF FEATURE_COPS_N_CROOKS	
			ENDIF
			#ENDIF
			#ENDIF
		ENDIF





        //Common voids to MP and SP_____________________________________________________________________________________________________________ 
		
        IF IS_THREAD_ACTIVE (Application_Thread)

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("Application_Thread is already running cannot launch another.")
                cdPrintnl()   
            #endif



            //2378079 - new content starts.
            /*
            //For bug 2378079, implementing a failsafe to terminate AppInternet.
            IF g_Cellphone.PhoneDS = PDS_MAXIMUM

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appInternet")) > 0
                    
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Trying to launch an app but Appinternet is still running. Failsafe counter is ")
                        cdPrintint(AppInternetFailsafeCounter)
                        cdPrintnl()   
                    #endif

                    AppInternetFailsafeCounter ++

                    
                    IF AppInternetFailsafeCounter = 8

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("Trying to launch an app a critical amount of times. AppInternet's internal exit borked? Attempting external terminate of thread. Failsafe counter was ")
                            cdPrintint(AppInternetFailsafeCounter)
                            cdPrintnl()   
                        #endif
                        
                        AppInternetFailsafeCounter = 0

                        TERMINATE_THREAD (Application_Thread)
                        
                    ENDIF

                ENDIF

            ELSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Application_Thread aborted launch but phone was not PDS_MAXIMUM")
                    cdPrintnl()   
                #endif
            
            ENDIF
            */
            //2378079 - new content ends.



        
            b_AppLaunchVoided = TRUE  //Added for 910427

        ENDIF




        IF SelectedAppIndex = ENUM_TO_INT (appCAMERA)

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appCamera cannot be launched as it is being disabled this frame.")
                    cdPrintnl()   
                #endif

                b_AppLaunchVoided = TRUE

            ELSE

               IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
                    IF IS_PED_ON_MOUNT (PLAYER_PED_ID()) //Don't allow camera use on horseback.
                    OR IS_STUNT_JUMP_IN_PROGRESS()
                    OR IS_PLAYER_CLIMBING (PLAYER_ID())
                    OR IS_PED_JACKING(PLAYER_PED_ID())
                    OR IS_PED_HANGING_ON_TO_VEHICLE(PLAYER_PED_ID()) //1950639
                    #if USE_TU_CHANGES
                    OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
                    OR IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
                    OR GET_USINGNIGHTVISION()
                    #endif

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("appCAMERA cannot be launched on mount or during stunt jump or climbing. See Bug 528388.")
                            cdPrintnl()   
                        #endif

                        b_AppLaunchVoided = TRUE

                    ENDIF

                
                    IF IS_PED_IN_ANY_VEHICLE (PLAYER_PED_ID()) 
                    
                        IF IS_PED_SITTING_IN_ANY_VEHICLE (PLAYER_PED_ID()) = FALSE

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("appCAMERA cannot be launched during vehicle entry / exit. See Bug 822459.")
                                cdPrintnl()   
                            #endif

                            b_AppLaunchVoided = TRUE

                        ENDIF

                        VEHICLE_INDEX tempVehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID())
                    
                        IF GET_ENTITY_UPRIGHT_VALUE (tempVehicleIndex) < 0.0

                             b_AppLaunchVoided = TRUE
                         
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("appCAMERA. Vehicle upright value less than zero! Aborting launch. See Bug 826009.")
                                cdPrintnl()   
                            #endif


                        ENDIF

                        
                    ENDIF


                    IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
                    
                        b_AppLaunchVoided = TRUE
                     
                        
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("appCAMERA - Cannot launch when player is targetting another ped. See bug 799705.")
                            cdPrintnl()   
                        #endif


                    ENDIF


                    IF g_bInMultiplayer //2156297 - Prevent Camera use when aiming any kind of weapon.  

                        IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
                        OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)

                            GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), fh_TempWeapon)
                
                            IF fh_TempWeapon <> WEAPONTYPE_UNARMED

                                #if IS_DEBUG_BUILD
                                    cdPrintstring ("CELLPHONE_FH- Can't launch snapmatic while aiming input is pressed by player in MP.")
                                    cdPrintnl ()
                                #endif

                                b_AppLaunchVoided = TRUE

                            ENDIF

                        ENDIF

                    ENDIF


                ENDIF

            ENDIF

        ENDIF


        IF SelectedAppIndex = ENUM_TO_INT (appInternet)

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME)

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appInternet cannot be launched as it is being disabled this frame.")
                    cdPrintnl()
                #endif


                b_AppLaunchVoided = TRUE

            ENDIF

        ENDIF


        //2534800  - We need to make sure that Benny's quicklaunch is disabled if the internet is disabled by an external script.
        IF SelectedAppIndex = ENUM_TO_INT (AppDummyApp0)
        AND (ARE_STRINGS_EQUAL (g_AppList[SelectedAppIndex].AppName, "CELL_BENWEB"))

            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME)

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("Benny's site cannot be launched as the cellphone's internet launch is being disabled this frame.")
                    cdPrintnl()
                #endif


                b_AppLaunchVoided = TRUE

            ENDIF

        ENDIF



            






        //MP specific voids_____________________________________________________________________________________________________________________

        IF g_bInMultiplayer

            IF SelectedAppIndex = ENUM_TO_INT (appPAUSEMAP)
            OR SelectedAppIndex = ENUM_TO_INT (appSIDETASK)
            //OR SelectedAppIndex = ENUM_TO_INT (appEMAIL)
            //OR SelectedAppIndex = ENUM_TO_INT (appCAMERA)  - Reincluded at request of TODO 425062
              
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FH - The chosen app has temporarily been disabled. No script requested or launched.")
                    cdPrintstring("CELLPHONE_FH - See Steve T if you need to re-enable this functionality.")
                    cdPrintnl()   
                #endif

            ELSE

                #if USE_TU_CHANGES
                    
                    IF SelectedAppIndex = ENUM_TO_INT (AppDummyApp0) //This is the app in the bottom right hand corner of the cellphone homescreen. It's used by multiple apps. 
                        
                        /* Old legacy work for AppVLSI - now runs as an actual app rather than a launch signal.
                        //#if IS_DEBUG_BUILD
                            //cdPrintnl()
                            //cdPrintstring("CELLPHONE_FH - Circuit breaker has no actual script association. No script requested or launched.")
                            //cdPrintstring("CELLPHONE_FH - Setting fake Circuit Breaker launch global g_HackingAppHasBeenLaunched to TRUE.")
                            //cdPrintnl()   
                        //#endif


                        //This script AppVLSI is now mapped to AppDummyApp0 and  will now be a standard app launch. See 2150653.
                        //Void removed for VLSI

                        //b_AppLaunchVoided = TRUE
                        //g_HackingAppHasBeenLaunched = TRUE
                        */




                        //AppDummyApp0 now used to launch Benny's site if the VLSI hacking app and Trackify and Sightseer are not in use.
                        //2411187

                        //Update for October 2015. Only set browser start state if VLSI, Trackify and Sightseer are not in use.
                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = FALSE
                        AND g_bEnableHackingApp = FALSE
                        AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP) = FALSE
                        AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP) = FALSE

                            //November 2015 url:bugstar:2554214 Update. This was only for the duration of Lowrider. Removing quick launch link setup.
                            //g_BrowserLinkToLaunch = "WWW_BENNYSORIGINALMOTORWORKS_COM" 
                            //g_BrowserStartState = SBSS_Launch_Link

                        ENDIF



                        //VLSI and Trackfiy have dedicated apps, Sightseer doesn't. Void any app launch if its availability bit is true bug signal that the app has been selected.
                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SIGHTSEER_APP) = TRUE
                        AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = FALSE
                        AND g_bEnableHackingApp = FALSE
                        AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP) = FALSE
						AND IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP) = FALSE
                            b_AppLaunchVoided = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintstring("CELLPHONE_FH -  g_BSTU_ENABLE_MP_SIGHTSEER_APP is set, voiding normal app launch procedure but setting signal bit for external check")
                                cdPrintnl()
                            #endif

                            SET_BIT (BitSet_CellphoneTU, g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED)

                        ENDIF



                    ENDIF

                #endif


                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
                    IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsAimingGun) //Don't allow camera launch if player is aiming gun in MP. See bug 495833.
                    OR IS_PLAYER_FREE_AIMING(PLAYER_ID()) //2071256

                        IF SelectedAppIndex = ENUM_TO_INT (appCAMERA)

                            b_AppLaunchVoided = TRUE

                        ENDIF

                    ENDIF
                ENDIF



// KGM 8/2/13: RaceToPoint app removed from game. Left the 'disable' functionality commented out for now. Bug added to clean up this up entirely if the app is gone for good.
//                IF SelectedAppIndex = ENUM_TO_INT (AppMPRACETOPOINT)
//
//
//                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_DISABLE_APPMPRACETOPOINT)
//                                   
//                        #if IS_DEBUG_BUILD
//                            cdPrintnl()
//                            cdPrintstring("CELLPHONE_FH - AppMPRaceToPoint has been disabled from script. Launch voided.")
//                            cdPrintnl()   
//                        #endif
//
//                        b_AppLaunchVoided = TRUE
//
//                    ENDIF
//
//
//                ENDIF

				// ----- Grey out and block apps if needed -----
				
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_PLAYER_ON_MP_INTRO()
					IF SelectedAppIndex = ENUM_TO_INT(AppJIPMP)
						b_AppLaunchVoided = TRUE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_JIP_BLOCK")
							PRINT_HELP("G9_JIP_BLOCK")
						ENDIF
					ENDIF
					
					IF SelectedAppIndex = ENUM_TO_INT(AppINTERNET)
						b_AppLaunchVoided = TRUE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_WEB_BLOCK")
							PRINT_HELP("G9_WEB_BLOCK")
						ENDIF
					ENDIF
				ENDIF
				#ENDIF
				
				#IF FEATURE_FIXER
				IF (g_bBlockInternetApp)
				AND SelectedAppIndex = ENUM_TO_INT(AppINTERNET)
					b_AppLaunchVoided = TRUE
					STRING _strBlockInternetHelpText = GET_BLOCK_INTERNET_APP_HELP_TEXT()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(_strBlockInternetHelpText)
						PRINT_HELP(_strBlockInternetHelpText)
					ENDIF
				ENDIF
				#ENDIF
				
				//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
				//As we force theme and wallpaper overrides for MP Franklin and Lamar, we need to prevent the player accessing the Settings 
				//app and seeing behind the magic. There would be a mismatch between what the app says and what the phone is presenting.
				IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
					IF SelectedAppIndex = ENUM_TO_INT(AppSETTINGS)
					//Could add more apps to void with an OR here.
					
						//This message is generic so can be used for various blocked apps.
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FIX_MPCHAR_BLCK")
							PRINT_HELP("FIX_MPCHAR_BLCK")
						ENDIF
						
						b_AppLaunchVoided = TRUE
						
						#if IS_DEBUG_BUILD
                            cdPrintstring("CELLPHONE_FH -  Attempting to launch Settings App, but IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR is true! Voiding.")
                            cdPrintnl()
                        #endif
					ENDIF
				ENDIF

                IF b_AppLaunchVoided = FALSE //go ahead and launch...


                    //Switch the other option off, in case any other app has neglected to...
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



                    g_CellPhone.PhoneDS = PDS_RUNNINGAPP
                    
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 1. Cellphone Flashhand assigns PDS_RUNNINGAPP")
                        cdPrintnl()   
                    #endif


                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)
     
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - WARNING! Already waiting on mp app to load, can't request another app script.")
                            cdPrintnl()   
                        #endif
                   
                    ELSE

                        Play_Select_Beep()

                        REQUEST_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName)
                        
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)

                    ENDIF

                ELSE

                    IF SelectedAppIndex = ENUM_TO_INT (appINTERNET)  //#1253886 - put this back in if we need to reinstate translucent help on a global basis. #1473119
                    OR SelectedAppIndex = ENUM_TO_INT (appCAMERA)
                                         
                        //IF g_savedGlobals.sCellphoneSettingsData.b_MP_HasTranslucentIconHelpBeenDisplayed = FALSE
                                                         
                            //PRINT_HELP ("CELL_36") //Removed by the request of Les bug #1473119

                            //g_savedGlobals.sCellphoneSettingsData.b_MP_HasTranslucentIconHelpBeenDisplayed = TRUE
    
                        //ENDIF
         
                    ENDIF

                    Play_Back_Beep()


                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - Chosen MP app launch has been voided.")
                        cdPrintnl()   
                    #endif

                ENDIF




                /* This form of request seems to cause a tiny pause on 360 due to the rendering taking place within flashhand. WAIT (0) can't be used.
                // Therefore flashhand needs to use a bit system.


                request_load_scriptstring (g_AppList[SelectedAppIndex].AppScriptName)
            
                Application_Thread = START_NEW_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName, DEFAULT_STACK_SIZE)

                SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[SelectedAppIndex].AppScriptName)

                //Force clock update for transition to miniature clock on scaleform display view state change.
                ClockThrottleMins = 99

                */

            ENDIF




        ELSE


            //SP specific voids_________________________________________________________________________________________________________________

            SWITCH INT_TO_ENUM (enumApplicationList, SelectedAppIndex)


                CASE appCamera

                    IF g_bTaxiCamIs1stPerson = TRUE

                        b_AppLaunchVoided = TRUE
                               
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - Camera app cannot be launched while in SP taxi first person mode.")
                            cdPrintnl()   
                        #endif

                    ENDIF

                BREAK


                //Temporary placeholder! ST 14.03.13            
                CASE appOrganiser

                    b_AppLaunchVoided = TRUE //Temporary void for 1169532, removal of organiser and insertion of placeholder app.

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - This placeholder app has temporarily been disabled. No script requested or launched.")
                        cdPrintnl()   
                    #endif

                BREAK



                CASE appPAUSEMAP

                    b_AppLaunchVoided = TRUE
                           
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - The chosen app has temporarily been disabled. No script requested or launched.")
                        cdPrintnl()   
                    #endif

                BREAK
                

                
                CASE appSNIPER

                    b_AppLaunchVoided = TRUE
                           
                    SET_BIT (BitSet_CellphoneDisplay, g_BS_SNIPER_APP_SELECTED)

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - Sniper app integrated within mission. No script requested or launched.")
                        cdPrintnl()   
                    #endif

                BREAK
                




                CASE appSIDETASK
                           
                    b_AppLaunchVoided = TRUE

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - The chosen app has temporarily been disabled. No script requested or launched.")
                        cdPrintnl()   
                    #endif

                BREAK

                
                
                
                CASE appQUICKSAVE

                    b_AppLaunchVoided = TRUE

                    IF g_b_QuickSaveGreyedOut = FALSE  //Make sure quick save is enabled at this time...

                        IF g_HasCellphoneRequestedSave = FALSE
                        AND local_Request_for_QuickSave = FALSE
                        AND g_OnMissionState = MISSION_TYPE_OFF_MISSION
    

                            local_Request_for_QuickSave = TRUE

                            Play_Select_Beep()

                            
                                    IF NOT g_savedGlobals.sCellphoneSettingsData.b_HasQuickSaveHelpBeenDisplayed

                                        /*  Comment out requested by bug 907584
                                        SWITCH GET_FLOW_HELP_MESSAGE_STATUS("CELL_33")
                                    
                                            CASE FHS_EXPIRED

                                                ADD_HELP_TO_FLOW_QUEUE("CELL_33", FHP_HIGH, 500, DEFAULT_GOD_TEXT_TIME)
                                            
                                            BREAK
                                        
                                            CASE FHS_DISPLAYED

                                                g_savedGlobals.sCellphoneSettingsData.b_HasQuickSaveHelpBeenDisplayed = TRUE
                                                
                                                #if IS_DEBUG_BUILD
                    
                                                    cdPrintstring("CELLPHONE_FH - QuickSave help has been displayed. Saved global set.")
                                                    cdPrintnl()

                                                #endif

                                            BREAK
                                        
                                        ENDSWITCH
                                        */

                                    ENDIF




                            #if IS_DEBUG_BUILD

                                PRINTNL()
                                PRINTLN("\nCELLPHONE has requested quick save via [", GET_THIS_SCRIPT_NAME(), "] homescreen icon. Setting g_HasCellphoneRequestedSave to TRUE")
                                PRINTNL()

                            #endif

                        ELSE

                            #if IS_DEBUG_BUILD

                                PRINTNL()
                                PRINTLN("\nCELLPHONE has requested another quicksave but g_HasCellphoneRequestedSave was already set to true!")
                                PRINTNL()

                            #endif
        
                        ENDIF

                    
                    ELSE

                    
                            Play_Back_Beep()

                            #if IS_DEBUG_BUILD

                                PRINTNL()
                                PRINTLN("\nAttempted quick save via [", GET_THIS_SCRIPT_NAME(), "] but homescreen icon is greyed out.")
                                PRINTNL()

                            #endif


                    ENDIF

                BREAK
                
                
                /*
                CASE appCAMERA

                    IF NOT IS_PED_INJURED (PLAYER_PED_ID())
                        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                    
                            b_AppLaunchVoided = TRUE

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - Camera App chosen but won't launch if player in vehicle.")
                                cdPrintnl()   
                            #endif


                        ENDIF
                    ENDIF

                BREAK
                */
                

                




                DEFAULT

                    #if IS_DEBUG_BUILD
                       cdPrintnl()
                       cdPrintstring("CELLPHONE_FH - Chosen app should be launched. No void applied.")
                       cdPrintnl()   
                    #endif

                BREAK


            ENDSWITCH




                

            //Special case for Lester 1B. Only that mission will be setting this to TRUE. The mission requires appContacts only.
            IF CAN_USE_MOBILE_PHONE_DURING_CUTSCENE()

               IF (INT_TO_ENUM (enumApplicationList, SelectedAppIndex)) <> appCONTACTS

                    b_AppLaunchVoided = TRUE

                
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - Chosen app launch has been voided due to special case CAN_USE_MOBILE_PHONE_DURING_CUTSCENE")
                        cdPrintnl()   
                    #endif

                ENDIF

            ENDIF

                

            IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)
                IF (INT_TO_ENUM (enumApplicationList, SelectedAppIndex)) <> appCamera
                AND (INT_TO_ENUM (enumApplicationList, SelectedAppIndex)) <> AppMPJOBLIST //2411448

                    b_AppLaunchVoided = TRUE

                
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - Chosen app launch has been voided as we are in Director Mode and app launch was not appCamera.")
                        cdPrintnl()   
                    #endif

                ENDIF
            ENDIF



            IF b_AppLaunchVoided = FALSE //go ahead and launch...


                Play_Select_Beep()

                
                //Switch the other option off, in case any other app has neglected to...
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                
                


                g_CellPhone.PhoneDS = PDS_RUNNINGAPP

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 2. Cellphone Flashhand assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif


               
                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)
 
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("CELLPHONE_FH - WARNING! Already waiting on SP app to load, can't request another app script.")
                        cdPrintnl()   
                    #endif
               
                ELSE

                    REQUEST_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName)
                  
                    SET_BIT (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)



                    //Add in timer for bug 2378079 - new content starts.
                    /*
                    IF ARE_STRINGS_EQUAL ("appInternet", g_AppList[SelectedAppIndex].AppScriptName)
                    
                        AppLaunch_StartTime = GET_GAME_TIMER()
                  
                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - App_Launch_StartTime grabbed.")
                            cdPrintnl()   
                        #endif

                    ENDIF
                    */
                    //2378079 - new content ends.


                ENDIF

                /* This form of request seems to cause a tiny pause on 360 due to the rendering taking place within flashhand. WAIT (0) can't be used.
                // Therefore flashhand needs to use a bit system.
                request_load_scriptstring (g_AppList[SelectedAppIndex].AppScriptName)
                                            
                Application_Thread = START_NEW_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName, DEFAULT_STACK_SIZE)

                SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[SelectedAppIndex].AppScriptName)

                //Force clock update for transition to miniature clock on scaleform display view state change.
                ClockThrottleMins = 99

            
                */
                
       
        
            ELSE
    

                IF SelectedAppIndex <> ENUM_TO_INT(AppQuicksave) //Quicksave handled separately above, its app launch is always voided, it just sets a flag rather than launching an associated script.

                    IF g_savedGlobals.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed = FALSE
                    
                           IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("paparazzo3b")) > 0   //Now putting cover specific help in for Paparazzo 3B - See bug 520630

                               IF SelectedAppIndex = ENUM_TO_INT (appCAMERA)

                                   IF IS_PLAYER_PLAYING (PLAYER_ID())

                                        IF IS_PED_IN_COVER(PLAYER_PED_ID()) 
                
                                            PRINT_HELP ("CELL_38") //You cannot use Snapmatic app whilst in cover...
                                                                
                                            g_savedGlobals.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed = TRUE
            
                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("CELLPHONE_FH - Paparazzo 3b Specific help text has been sent to display.")
                                                cdPrintnl()   
                                            #endif

                                        ENDIF

                                    ENDIF

                                ENDIF
                            ENDIF



                        
                        /*  //Removed by the request of Les bug #1473119
                        SWITCH GET_FLOW_HELP_MESSAGE_STATUS("CELL_36") //Display translucent icon help.
                                    
                            CASE FHS_EXPIRED

                                ADD_HELP_TO_FLOW_QUEUE("CELL_36", FHP_HIGH, 500, DEFAULT_GOD_TEXT_TIME)
                            
                            BREAK
                        
                            CASE FHS_DISPLAYED

                                g_savedGlobals.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed = TRUE
                                
                                #if IS_DEBUG_BUILD

                                    cdPrintstring("CELLPHONE_FH - Translucent app icon help has been displayed. Saved global set.")
                                    cdPrintnl()

                                #endif

                            BREAK
                        
                        ENDSWITCH
                        */

                    ENDIF

                    Play_Back_Beep()

                ENDIF



                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FH - Chosen app launch has been voided.")
                    cdPrintnl()   
                #endif


            ENDIF







        ENDIF


    ELSE

        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_FH - Waiting on return value from scaleform. 101")
            cdPrintnl()
        #endif

    ENDIF
        
                                                                                     



ENDPROC






/* Moved to cellphone_private.sch
PROC SetUpPhoneSurroundTint() //Bit daft and inconsistent with colour of blips on radar for character missions. Requested by Stu Petri in url:bugstar:1827354


    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())  


        SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer


            CASE 1 //Light Blue Theme
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 0)
            BREAK


            CASE 2 //Green
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 1)
            BREAK


            CASE 3 //Red
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 2)
            BREAK

            
            CASE 4 //Orange
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 3)
            BREAK


            CASE 5 //Grey
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 4)
            BREAK


            CASE 6 //Purple
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 5)
            BREAK

            
            CASE 7 //Orange
                SET_PLAYER_PHONE_PALETTE_IDX(PLAYER_ID(), 6)
            BREAK


            DEFAULT

                //Not setting any pallette here.

            BREAK



        ENDSWITCH



   ENDIF


ENDPROC
*/





PROC StandardBackTo1stPersonOrHudHideChecks()


    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
    AND (g_FirstPersonTransitionOngoing = FALSE)
    AND (g_3DPhoneNeedsMovedUp = FALSE)



        IF HasGameplayCameraTransitionOccurredDuringCall = FALSE


            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)
            
                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH)

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)

                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())

                    IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
                
                        LaunchTransitionMoveUpIfRequired()
                        //g_3DPhoneNeedsMovedUp = TRUE //Cues movement up screen.

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - Now in first person on foot after phone vanished in 3rd person during call. Emergency move up cued.")   
                            cdPrintnl()
            
                        #endif


                    ELSE

                        IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE //This vehicle is not no-hud compatible in first person. Move phone up if need be.  
            
                            LaunchTransitionMoveUpIfRequired()
                        
                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - Now in first person on no_hud compatible vehicle after phone vanished in 3rd person during call. Emergency move up cued.")   
                                cdPrintnl()
            
                            #endif
                            //g_3DPhoneNeedsMovedUp = TRUE //Cues movement up screen.

                        ENDIF
                    ENDIF

                ENDIF


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FH - Now in first person after phone vanished in 3rd person during call. Emergency move up cued.")   
                    cdPrintnl()
            
                #endif


            ENDIF
        ENDIF



        //Put in vehicle check exceptions here. Elsewhere, need to check being in first person, then swapping vehicles.
        //Possibly just an OR call to  ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE


        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL

            //Removed for 2038700  - Search on this if this needs to go back in again.
            /*  
            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //If we're in a first person no_hud compatible vehicle and on call, move the phone back down again...
                OR g_ConversationStatus = CONV_STATE_WAITING_ON_PLAYER_ACCREJ //2031019

        
                    IF ShouldFirstPersonPhoneHudMovementBeHidden()
                 
                        LaunchTransitionMoveDownIfRequired()

                    ENDIF
                    

                ENDIF
            ENDIF

       
            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning TRUE. Unless player in compatible no-hud vehicle or ACC_REJ convstate, Transition aborted as player on call.")   
                cdPrintnl()
        
            #endif
            */



            //Added for 2038700
            IF ShouldFirstPersonPhoneHudMovementBeHidden()
         
                LaunchTransitionMoveDownIfRequired()


                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning TRUE. Transition to 1st Person on call state cued.")   
                    cdPrintnl()
        
                #endif


            ENDIF


        ELSE

            IF IS_CELLPHONE_CAMERA_IN_USE()

                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning TRUE but Snapmatic in use. Transition to 1st Person occurred whilst phone was up.")   
                    cdPrintnl()
            
                #endif

            ELSE

                IF ShouldFirstPersonPhoneHudMovementBeHidden()  //New!

                    LaunchTransitionMoveDownIfRequired()

                ENDIF
                //g_FirstPersonTransitionOngoing = TRUE //Cues movement off screen whilst in first person mode.

                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning TRUE. Transition to 1st Person occurred whilst phone was up.")   
                    cdPrintnl()
        
                #endif

            ENDIF

        ENDIF


        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL

            IF g_IsThisAnMPChatCall = FALSE //Do not let this impact "hands free" mode in SP/MP chat calls.

                
                HasGameplayCameraTransitionOccurredDuringCall = TRUE
                //The above is dangerous if checked improperly! May need to have a global to reset the hide timer in dialog handler!

                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - HasGameplayCameraTransitionOccurredDuringCall has been set to TRUE. Camera transitioned to 1st person during ongoing call.")   
                    cdPrintnl()
                
                #endif

            ENDIF

        ENDIF

    ENDIF


ENDPROC






PROC StandardBackTo3rdPersonorHudShownChecks()

    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
    AND (g_FirstPersonTransitionOngoing = FALSE)
    AND (g_3DPhoneNeedsMovedUp = FALSE)

   
        IF IS_CELLPHONE_CAMERA_IN_USE()

        //Put in vehicle check exceptions here. Elsewhere, need to check being in first person, then swapping vehicles.
        //Possibly just an OR call to  ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE

            /*
            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning FALSE. Transition aborted as vanish bits were set or Snapmatic running.")   
                cdPrintnl()
        
            #endif
            */
        ELSE

            LaunchTransitionMoveUpIfRequired()
            //g_3DPhoneNeedsMovedUp = TRUE //Cues movement up screen.

            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson now returning FALSE. Transition to 3rd Person occurred whilst phone was up.")   
                cdPrintnl()
        
            #endif

        ENDIF



        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL       
        
            IF g_IsThisAnMPChatCall = FALSE //Do not let this impact "hands free" mode in SP/MP chat calls.

                HasGameplayCameraTransitionOccurredDuringCall = TRUE

                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - HasGameplayCameraTransitionOccurredDuringCall has been set to TRUE. Camera transitioned to 3rd person during ongoing call.")   
                    cdPrintnl()
                
                #endif

            ENDIF

        ENDIF

    ENDIF


ENDPROC


























SCRIPT





    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()







    g_ForcedContactSelection = NO_CHARACTER //clear out any forced character selections in appContacts. Done here so that end-user can check if phone is onscreen, rather than 
                                            //AppContacts running before forcing a selection.


    //Clear "triangle displayed" bit and other miscellaneous phone status flags.

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_THREEQUARTERS_UP)
        
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)

    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC)
    
    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SNIPER_APP_SELECTED)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH)

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY) //Precautionary clear for bug 1362605
                                                                                                              

    g_LaunchContactsFromTextMessage = FALSE //1636892
    LaunchContactsFromTextMessage_LocalSignal = FALSE


    



    DRAW_LOW_QUALITY_PHOTO_TO_PHONE(FALSE) //Make sure we aren't rendering any photo images to the phone.



    SCRIPT_IS_MOVING_MOBILE_PHONE_OFFSCREEN (FALSE) //See bug 740666
    g_Phone_Blocked_While_Moving_Down = FALSE



    b_Waiting_On_SF_Return_Value = FALSE

    //Do not reset this as it may override setting from dialogue handler.
    //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)


    //These additions added for 295259. The first is a simple emergency flag that doesn't rely on the phone state to signal to applications that this script is waiting on
    //them to use abnormal exit to clear up, the second tidies up the abort flag next time the phone is launched if the preceding launch of this script resulted in an
    //aborted call.
    g_Cellphone_FH_Needs_To_Exit = FALSE
    g_Attempted_Call_Aborted = FALSE 
    g_B_ForcedAway = FALSE

    

    Get_Cellphone_Owner()


    //#1392094 swap
    //g_Phone_Ringtone_SoundID = GET_SOUND_ID() //This has to be done before the movies are requested in case they fail to load and RELEASE_SOUND_ID is called. See bug #455304


    //Pocket work.
    g_3dPhoneRotVec = g_3dPhonePocketRotVec


    Previous_SelectedAppCursorPos_PageOne = 4 // Set default cursor position to centre on page one.
    Previous_SelectedAppCursorPos_PageTwo = 0 // Set default cursor position to the first icon  on page two.

    g_LastUsed_Homescreen_Page_Precedence = 1

    AppCursorIndex = Previous_SelectedAppCursorPos_PageOne


     //Imported from cellphone_controller.sc
    IF g_B_Scaleform_Movies_Loaded = FALSE



        g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE


        
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_FH - About to request cellphone scaleform movies...")
            cdPrintnl ()
            cdPrintnl ()  
        #endif
                                



        #if IS_DEBUG_BUILD

            IF b_DebugCellphoneGuiInfo


               SF_MovieIndex = REQUEST_SCALEFORM_MOVIE (ScaleformDebugMovie[DebugCellphoneGui])

        
            ELSE
        
        #endif

            IF g_bInMultiplayer
            


                //SET_PLAYER_TEAM(PLAYER_ID(), 0)



                    // Freemode (and, by default, any other modes we add)
                    
					//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
					//Default the cellphone operating system to Badger if playing as Franklin or Lamar in MP.
					IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
						SF_MovieIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_badger")
					ELSE
						SF_MovieIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_ifruit")
					ENDIF
					
					
					
                    
                    ThemeDecision = 1

                    IF ThemeDecision = 1
                    
                        #if IS_DEBUG_BUILD

                            cdPrintstring("Theme definitely at 1.")
                            cdPrintnl()

                        #endif
                    ENDIF


            ELSE //Select movie based on singleplayer character...
                
                IF g_Use_Prologue_Cellphone

                    //Perhaps add another failsafe by only allowing this to be set if Prologue1.sc is running?

                    g_b_ToggleButtonLabels = TRUE

                    SF_MovieIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_prologue")

                ELSE

                    SF_MovieIndex = REQUEST_SCALEFORM_MOVIE (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ScaleformOS_Movie_Name)
                    
                    
                    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", 3)

                ENDIF


            ENDIF
     
        #if IS_DEBUG_BUILD


        ENDIF

        #endif









        //SF_MovieButtonsIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_buttons")


        //Autoplay_Fix
        //Have this timeout and call incident handling routine...

        SETTIMERA(0)


        //Think about having PDS_MAXIMUM here?



        WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
        AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
        //OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)
           
            WAIT(0)


            IF TIMERA() > 20000

                g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE


                #if IS_DEBUG_BUILD

                    //SCRIPT_ASSERT ("Scaleform cellphone movie taking too long to load due to lack of memory; cellphone flashhand aborting...")

                    cdPrintnl()
                    cdPrintstring ("Scaleform cellphone movie taking too long to load due to lack of memory; cellphone flashhand aborting...")
                    cdPrintnl()

                    //This will likely only happen in autoplay scenarios. Script asserts fired by scaleform calls to an unloaded movie will be ignored.

                #endif




            ENDIF



            //2260093 - TODO. Anti tamper measures. If the movie has been set as invalid by code, then only pause for a few seconds before cleaning up the phone.
            IF IS_PC_VERSION()
                IF DECOR_EXIST_ON(PLAYER_PED_ID(), "Synched")
                    IF TIMERA() > 2000

                        g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring ("Synched decorator. Flashhand aborting...")
                            cdPrintnl()
            
                        #endif

                    ENDIF
                ENDIF
            ENDIF



            
            IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED  //A call is waiting to be answered, the phone display state is PDS_ONGOING_CALL, player must accept or reject call.

                Vibrate_and_Ring_3dPhone()

            ENDIF


 
        ENDWHILE



    
        IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE

            IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) //This needs to be done here, in case the movie aborts. See bug 740117 
            
                //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 0) //Flush out any streamed resources used by cellphone. See bug 535099. Instance 2.
                LEGACY_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SHUTDOWN_MOVIE")

            ENDIF



            WAIT (0)

            SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieIndex)

            //Tidy up or initialise critical bools...
            g_3DPhoneNeedsMovedDown = FALSE

            g_B_Scaleform_Movies_Loaded = FALSE

            g_Cellphone_Launched_by_Button_Press = FALSE

            IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = TRUE 

                g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = FALSE

                CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

            ENDIF


            g_Cellphone_FH_Needs_To_Exit = TRUE 
                   

            g_Cellphone.PhoneDS = PDS_AWAY

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 56. Cellphone_flashhand assigns PDS_AWAY")
                cdPrintnl()   
            #endif


            //Maybe force a reply here?

            KILL_ANY_CONVERSATION()

            #if IS_DEBUG_BUILD
       
                cdPrintnl()
                cdPrintstring ("CELLPHONE_FLASHHAND Cellphone flashhand aborting... Autoplay Fail has been tripped by dialogue handler phone launch or movie taking too long to load. ")
                cdPrintnl()

            #endif

            g_b_ToggleButtonLabels = FALSE

            
            //Get rid of the cellphone 3d model from below the screen.
            DESTROY_MOBILE_PHONE ()

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY)  

            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)

            CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)


            //CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_CELLPHONE_MOVIE_STREAMING_IN) //Comment in for resolution of 1135663 

            

            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
                STOP_PED_RINGTONE (PLAYER_PED_ID())
            
            ENDIF


            IF IS_ENTITY_DEAD (PLAYER_PED_ID())

                //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
                STOP_PED_RINGTONE (PLAYER_PED_ID())
            
            ENDIF



            //#1392094 swap
            //RELEASE_SOUND_ID (g_Phone_Ringtone_SoundID)


            g_Phone_Active_but_Hidden = FALSE


            g_ShouldForceSelectionOfLatestAppItem = FALSE

            CellphoneAppAutoLaunchType = ACT_APP_NONE


            g_IsThisAnMPChatCall = FALSE //This has been moved here for 2111525. It was required so that the hands free handler is definitely reset.

            SCRIPT_END_A_VOICE_SESSION()  //1617499 Make sure that a script hang up also terminates a chat call.  Check incoming call stage?
             
            CLEAR_VOICE_SESSION_STRUCT() //Fix for 1891329.

            g_FirstPersonTransitionOngoing = FALSE

            #if IS_DEBUG_BUILD

                cdPrintString ("g_IsThisAnMPChatCall assigned to false from Cellphone_Flashhand 2...  2111525")
                cdPrintnl()
                cdPrintnl()

            #endif


            //Quit this script.
            TERMINATE_THIS_THREAD()

        ENDIF //end of g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE condition check



        








        //Set up temporary button sizes for badger theme
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].OSTypeForThisPlayer = OS_BADGER

            g_SF_ButtonsPosX = 0.1
            g_SF_ButtonsPosY = 0.380//0.285//0.334
            g_SF_ButtonsSizeX = 0.195
            g_SF_ButtonsSizeY = 0.050

        //ENDIF



        IF IS_XBOX360_VERSION()
        OR IS_XBOX_PLATFORM()
        OR IS_PC_VERSION() //This will make next gen PC version use the same soft key colouring as the Xbox 360 version. Requested by NG bug 1702846
        
            GET_HUD_COLOUR (HUD_COLOUR_GREEN, TempColR, TempColG, TempColB, TempColA )
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

            //GET_HUD_COLOUR (HUD_COLOUR_YELLOW, TempColR, TempColG, TempColB, TempColA )
            GET_HUD_COLOUR (HUD_COLOUR_BLUE, TempColR, TempColG, TempColB, TempColA ) //if we change to using 360 X, swap over
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

            GET_HUD_COLOUR (HUD_COLOUR_RED, TempColR, TempColG, TempColB, TempColA )
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))


        ELSE //PS3.... PC?


            //Swapped over to this rather than the legacy compile flag after discussion with Ryan Elliott.
            IF NOT IS_JAPANESE_VERSION()

                GET_HUD_COLOUR (HUD_COLOUR_BLUE, TempColR, TempColG, TempColB, TempColA )//Cross getting used for accept and positive
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                //GET_HUD_COLOUR (HUD_COLOUR_GREENLIGHT, TempColR, TempColG, TempColB, TempColA )
                GET_HUD_COLOUR (HUD_COLOUR_PINKLIGHT, TempColR, TempColG, TempColB, TempColA ) //Square getting used for centre button. Other options usually.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                GET_HUD_COLOUR (HUD_COLOUR_RED, TempColR, TempColG, TempColB, TempColA ) //Circle getting used for exit, negative and back.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FLASHHAND  IJV Aug 21: - Setting EU/US SOFTKEY colours")
                    cdPrintnl()
                    cdPrintnl()

                #endif

            ENDIF


            //For the frontend circle to proceed convention crossing over to the phone, comment this one in! #1571352
            
			//Swapped over to this rather than the legacy compile flag after discussion with Ryan Elliott.
            IF IS_JAPANESE_VERSION()

				//Bug 7237325. Only apply this soft key colour swap in legacy Orbis builds that used the colouring model specific for the Japanese market.
				#IF NOT FEATURE_GEN9_RELEASE
				
	                GET_HUD_COLOUR (HUD_COLOUR_RED, TempColR, TempColG, TempColB, TempColA )//Cross getting used for accept and positive
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                GET_HUD_COLOUR (HUD_COLOUR_PINKLIGHT, TempColR, TempColG, TempColB, TempColA ) //Square getting used for centre button. Other options usually.
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                GET_HUD_COLOUR (HUD_COLOUR_BLUE, TempColR, TempColG, TempColB, TempColA ) //Circle getting used for exit, negative and back.
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                #if IS_DEBUG_BUILD
	                    cdPrintnl()
	                    cdPrintnl()
	                    cdPrintstring("CELLPHONE_FLASHHAND - IJV Aug 21: Setting legacy JAPANESE SOFTKEY Circle to Select cellphone option colours")
	                    cdPrintnl()
	                    cdPrintnl()

	                #endif
					
				#ENDIF
				
				//Bug 7237325. In Gen 9 Japanese, we now use the normal colouring.	
				#IF FEATURE_GEN9_RELEASE
				
	                GET_HUD_COLOUR (HUD_COLOUR_BLUE, TempColR, TempColG, TempColB, TempColA )//Cross getting used for accept and positive
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                GET_HUD_COLOUR (HUD_COLOUR_PINKLIGHT, TempColR, TempColG, TempColB, TempColA ) //Square getting used for centre button. Other options usually.
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                GET_HUD_COLOUR (HUD_COLOUR_RED, TempColR, TempColG, TempColB, TempColA ) //Circle getting used for exit, negative and back.
	                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

	                #if IS_DEBUG_BUILD
	                    cdPrintnl()
	                    cdPrintnl()
	                    cdPrintstring("CELLPHONE_FLASHHAND - IJV Aug 21: Setting Gen9 JAPANESE SOFTKEY colours")
	                    cdPrintnl()
	                    cdPrintnl()
	                #endif
					
				#ENDIF
				
           ENDIF //End of IF IS_JAPANESE_VERSION() check.            


            //If the JAPANESE build IS allowed to use the CROSS to select a cellphone option convention, comment this one in! #1571352
            
            /*
            #IF IS_JAPANESE_BUILD

                GET_HUD_COLOUR (HUD_COLOUR_BLUE, TempColR, TempColG, TempColB, TempColA )//Cross getting used for accept and positive
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                GET_HUD_COLOUR (HUD_COLOUR_PINKLIGHT, TempColR, TempColG, TempColB, TempColA ) //Square getting used for centre button. Other options usually.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                GET_HUD_COLOUR (HUD_COLOUR_RED, TempColR, TempColG, TempColB, TempColA ) //Circle getting used for exit, negative and back.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, TO_FLOAT(TempColR), TO_FLOAT(TempColG), TO_FLOAT(TempColB))

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FLASHHAND - Setting JAPANESE SOFTKEY Cross to Select cellphone option colours")
                    cdPrintnl()
                    cdPrintnl()

                #endif

            #ENDIF
            */






        ENDIF



        /*
        IF IS_XBOX360_VERSION()
            //Xbox 360
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, 77, 151, 4)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, 197, 147, 0)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, 166, 4, 0)

        ELSE

            //PS3 or PC
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_POS, 128, 151, 230)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_OTHER, 1, 216, 114)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SOFT_KEYS_COLOUR", BADGER_NEG, 255, 85, 85)

        ENDIF
        */



      


        IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE

            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Successfully loaded cellphone scaleform movies...")
                cdPrintnl()
                cdPrintnl()
            #endif

        ELSE


            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - FAILED to load cellphone scaleform movies...")
                cdPrintnl()
                cdPrintnl()
            #endif


        ENDIF


        Create_3dPhoneObject() //Change requested by bug number 310808 - was in cellphone_controller.
            
            
        IF g_bInMultiplayer //Patch over Phone_SoundSet_Default with Michael's iFruit set for #1613268

            g_Owner_Soundset = "Phone_SoundSet_Michael"
     
        ENDIF




        g_B_Scaleform_Movies_Loaded = TRUE

        //New addition for #1546794
        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
        



    ENDIF


    #if IS_DEBUG_BUILD
        
        IF g_bFlowAutoplayInProgress

            cdPrintnl()
            cdPrintstring ("CELLPHONE_FH - Autoplay enabled. Calls and phone responses will be automatically answered with positive actions.")
            cdPrintnl()

        ENDIF
   
    #endif


    //Force update of cellphone clock.
    ClockThrottleMins = 99

    Update_Cellphone_Clock_and_Health()

  
    IF IS_PLAYER_PLAYING (PLAYER_ID())

        Player_PeriodicHealth = GET_ENTITY_HEALTH (PLAYER_PED_ID())
    
    ENDIF



    IF IS_RADAR_HIDDEN()

        g_b_RadarWasAlreadyHidden = TRUE
            
    ELSE

        g_b_RadarWasAlreadyHidden = FALSE //force reset of this.

        //Phone back on RHS, commented out but infrastucture left intact.

        //DISPLAY_RADAR (FALSE) //switch off radar so phone can take precedence in left hand corner.

    ENDIF


    //Flush sleep mode:
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 0)


    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", 1)



    g_Cellphone_FullScreen_Mode = TRUE


   



    //Second screen, theme, wallpaper and provider setting... Will theme manipulation be disabled in multiplayer?
    IF g_bInMultiplayer

        g_b_Secondary_Screen_Available = FALSE
      

        //Would need to get theme stat here... MP_STAT_FM_CELLPHONE_THEME

		//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
		//Bypass any user defined theme for their own avatar's cellphone and use a default color scheme for Franklin and Lamar.   
		IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
		
			//Search on This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)] to find full theme definitions.
			IF g_bUseLamarThemeInMP = TRUE
				Temp_MPcurrentTheme = 4 //Theme 4 - Orange
			ELSE
				//Otherwise use Franklin's starting green default to match his office decor. We most certainly cannot bring in whatever is stored as the SP current theme!
				Temp_MPcurrentTheme = 2 //Theme 2 - Green
			ENDIF
			
		ELSE
		
        	Temp_MPcurrentTheme = GET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_THEME)
		
		ENDIF
        
        #if IS_DEBUG_BUILD

            cdPrintstring("Cellphone_flashhand - Retrieved MP_STAT_FM_CELLPHONE_THEME. It is set to ")
            cdPrintint(Temp_MPCurrentTheme)
            cdPrintnl()

        #endif


        //Range of acceptable Theme colours is 1 to 7. If the stat has not been set, i.e at zero, set "Blue - 1" as the default.
        IF Temp_MPcurrentTheme < 1
        OR Temp_MPcurrentTheme > 7

            Temp_MPcurrentTheme = 1

            #if IS_DEBUG_BUILD

                cdPrintstring("Cellphone_flashhand - Retrieved MP_STAT_FM_CELLPHONE_THEME but value was out of range. Defaulting to ")
                cdPrintint(Temp_MPCurrentTheme)
                cdPrintnl()

            #endif

        ENDIF
    
 
	   
	   	//Set the cellphone's theme colour via scaleform to the decision made above.
		//Reminder: url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
		//Work for this will set a static value defined above if playing as either character model in MP.
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (Temp_MPcurrentTheme)) 
        
        SetUpPhoneSurroundTint()

        


		//url:bugstar:7423972 - Re-theme player phone in GTAO when using the custom Franklin or Lamar model.
		//Bypass any user defined wallpaper for their own avatar's cellphone and use a default wallpaper for Franklin and Lamar.   
		IF IS_MP_PHONE_OWNER_PLAYING_AS_FRANKLIN_LAMAR()
		
			//Careful changing these. Some wallpapers may not exist in the Badger movie! Test in MP before any submit.
			//Search on This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)] to find full wallpaper definitions.
			IF g_bUseLamarThemeInMP = TRUE
				Temp_MPcurrentWallpaper = 16	//Orange Triangles - Text label reference CELL_856
			ELSE
				Temp_MPcurrentWallpaper= 9		//Green Shards - Text label reference CELL_849 
			ENDIF
			
		ELSE
		
        	Temp_MPCurrentWallpaper = GET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_BACKGROUND)
			
		ENDIF

        #if IS_DEBUG_BUILD

            cdPrintstring("Cellphone_flashhand - Retrieved MP_STAT_FM_CELLPHONE_BACKGROUND. It is set to ")
            cdPrintint(Temp_MPCurrentWallpaper)
            cdPrintnl()

        #endif




        IF Temp_MPCurrentWallpaper = 0 //Crew Emblem is set in the zero position of the settings list for wallpapers.

        
                //Add crew emblem as cellphone wallpaper...
                TEXT_LABEL_63 sCrewTextureName

                GAMER_HANDLE PlayerHandle

                PlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())

                BOOL b_TimedOutGettingCrewEmblem = FALSE


                IF NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE   //See 2002264

                    #if IS_DEBUG_BUILD

                        cdPrintstring("Cellphone_flashhand - NETWORK_HAVE_USER_CONTENT_PRIVILEGES() returned false prior to Crew Emblem retrieval.")
                        cdPrintnl()

                    #endif  
            

                ENDIF


                IF g_b_HasCrewEmblemRetrievalFailedThisSession = TRUE

                     #if IS_DEBUG_BUILD

                        cdPrintstring("Cellphone_flashhand - g_b_HasCrewEmblemRetrievalFailedThisSession = TRUE prior to Crew Emblem retrieval. Default to blank wallpaper.")
                        cdPrintnl()

                    #endif

                ENDIF
                    

        
                IF NETWORK_CLAN_PLAYER_IS_ACTIVE(PlayerHandle)
                AND NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
                AND g_b_HasCrewEmblemRetrievalFailedThisSession = FALSE
                //AND (g_DumpDisableEveryFrameCaller = FALSE) //comment in for easy testing.

                    #if IS_DEBUG_BUILD

                        cdPrintstring("Cellphone_flashhand - Player primary clan active and privileges available! Will try to get texture name for cellphone background. ")
                        cdPrintnl()

                    #endif

                                     
                    /*Timeout section removed 26.11.14 - Camille says that NETWORK_CLAN_GET_EMBLEM_TXD_NAME should return false immediately. 
                     
                    SETTIMERA(0)

                    WHILE NOT (NETWORK_CLAN_GET_EMBLEM_TXD_NAME(PlayerHandle, sCrewTextureName))
                    AND (TIMERA() < 5000) //Give MP simulation 5 seconds to get crew texture for testing purposes.
                 
                        WAIT (0)

                        #if IS_DEBUG_BUILD

                            cdPrintstring("Cellphone_flashhand - waiting on GET_EMBLEM_TXD_NAME to retrieve string")
                            cdPrintnl()

                        #endif

                        IF TIMERA() > 4999

                            #if IS_DEBUG_BUILD

                                cdPrintstring("WARNING! Cellphone_flashhand - TIMED OUT on GET_EMBLEM_TXD_NAME to retrieve string!")
                                cdPrintnl()

                            #endif


                            g_b_HasCrewEmblemRetrievalFailedThisSession = TRUE


                            b_TimedOutGettingCrewEmblem = TRUE

                        ENDIF
                        


                    ENDWHILE
                    
                    //End of timeout section.
                    */



                    IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(PlayerHandle, sCrewTextureName)

                        #if IS_DEBUG_BUILD

                            cdPrintstring("Cellphone_flashhand - Emblem TXD string retrieved for NG was... ")
                            cdPrintstring(sCrewTextureName)
                            cdPrintnl()

                        #endif 
                         
                    ELSE
                    
                        #if IS_DEBUG_BUILD

                            cdPrintstring("WARNING! Cellphone_flashhand - NETWORK_CLAN_GET_EMBLEM_TXD_NAME failed to retrieve string!")
                            cdPrintnl()

                        #endif

                        b_TimedOutGettingCrewEmblem = TRUE

                    ENDIF



                    IF b_TimedOutGettingCrewEmblem = FALSE //If we didn't time out, set the background crew image.

                        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_BACKGROUND_CREW_IMAGE")
                        
                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCrewTextureName)
                        //END_SCALEFORM_MOVIE_METHOD()
                        

                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_2000") //~a~
                        
                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sCrewTextureName) 
                                                           
                            END_TEXT_COMMAND_SCALEFORM_STRING()


                        END_SCALEFORM_MOVIE_METHOD()


                    ELSE


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (0))


                    ENDIF

                ELSE
                
                
                    #if IS_DEBUG_BUILD

                        cdPrintstring("Cellphone_flashhand - Clan not active, retrieval failed previously this session or no content privileges. Assigning default wallpaper as 0.")
                        cdPrintstring(sCrewTextureName)
                        cdPrintnl()

                    #endif



                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (0))


                
                ENDIF

        ELSE
            
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (Temp_MPCurrentWallpaper))
                    
        ENDIF


    ELSE

        IF g_Use_Prologue_Cellphone

            g_b_Secondary_Screen_Available = FALSE

        ELSE

            IF g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE //Either of these appear on the second screen.
            OR g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE

                //g_b_Secondary_Screen_Available = TRUE //Reinstate this if 1409258 is reversed.
                g_b_Secondary_Screen_Available = FALSE  

            ELSE

                g_b_Secondary_Screen_Available = FALSE

            ENDIF
    

        
        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
        
        
        IF g_IsMonochromeCheatActive = FALSE
        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer))

        ELSE 

            //Don't set a background image... and make the phone slightly bigger.
            SET_MOBILE_PHONE_SCALE(500 + 75)

    ENDIF
  

        SetUpPhoneSurroundTint()


    ENDIF
  

        
        
    i_Provider_Ident = Get_Cellphone_Provider()

    IF g_Use_Prologue_Cellphone = FALSE //don't set provider for prologue phone, as method is not accepted.
        
        ProcessCellphoneSignalStrength()

        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_PROVIDER_ICON", TO_FLOAT (i_Provider_Ident), TO_FLOAT() //Needs to be a global!  1132229

    ENDIF




    g_InputButtonJustPressed = FALSE //Make sure buttonjustpressed has been flushed - might move this and loop check to cellphone_controller.sc
    g_Homescreen_In_Secondary_Mode = FALSE


    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)



    //Critical reset. 
    g_CellphoneHasMPAccess = FALSE



    //Critical - check if player has access to GTA Online - put in any further checks here.
    IF IS_GTA_ONLINE_AVAILABLE()
    AND NETWORK_IS_SIGNED_ONLINE()

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("CELLPHONE_FH - Primary launch check of IS_GTA_ONLINE_AVAILABLE has returned TRUE. Setting joblist access flag to TRUE also")   
            cdPrintnl()
                        
         #endif

        g_CellphoneHasMPAccess = TRUE

    ENDIF




    //New for 2222248 diagnosis. Making sure the phone's homescreen is empty.
    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("CELLPHONE_FH - February new debug for 2222248 diagnosis. Making sure the phone's homescreen is empty on Flashhand Launch.")   
        cdPrintnl()
                        
    #endif

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)






    Fill_All_Primary_App_Sheets()  //Have single / multiplayer variant?
    TitleUpdate_Patch_In_JobList_For_SP_Phone()







    //Could go!
    //Fill_All_Secondary_App_Sheets()

    //Delay enum reset as a failsafe.
    
    //g_DelayCinematicCamReinstatement = NO_DELAY_REQUIRED



    //Make sure these are false just in case anyone has forgotten to reset them after use.
    
    //SET_SPRITES_DRAW_BEFORE_FADE (FALSE)
    //SET_TEXT_DRAW_BEFORE_FADE (FALSE)

    //No longer required due to code side alteration in default behaviour. 24.04.12
    //SET_SCRIPT_GFX_DRAW_ORDER (GFX_ORDER_AFTER_FADE) //altered in accordance with bug 391635


  
    //Because of scaleform movie timings we need to make sure that when the phone is brought out from the away position for an inbound call or
    //a "force in hand, we don't set the homescreen view state as it can supercede the calling screen view state.  
    //As cellphone_controller specifies the upcoming pds state if an inbound call or force is to occur, we can check if the PDS remains normal
    //before drawing the homescreen.
    
    Scaleform_Place_Items_on_Homescreen (1)

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_UI_COLOUR", 1, 114, 103, 243)


    //Les changed his mind and wants the same sound to play for pulling the phone out and putting it away.
    //PLAY_SOUND_FRONTEND (-1, "Put_Away", g_Owner_Soundset) // # 1535473

    PLAY_SOUND_FRONTEND (-1, "Pull_Out", g_Owner_Soundset)


    //This following section is important in that it prevents flashhand from drawing an empty screen if a live state has not been set. It also includes timeout failsafes
    //and mutual exclusion checks for weird timings: e.g the phone being brought out to takeout state but instantly getting disabled or hung up, primarily by script.
        
    SETTIMERB(0)

    WHILE g_Cellphone.PhoneDS < PDS_MAXIMUM
    AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE //Without this, mutual exclusion could occur, as controller, dh will only make a decision if moviefail is false.
 
        WAIT(0)

        IF TIMERB() > 10000

            g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE //CRITICAL! This must be set so that it lets cellphone_controller know that it should break out of its WHILE clause in PDS_TAKEOUT check.
           
            #if IS_DEBUG_BUILD
                cdPrintnl() 
                cdPrintstring ("CELLPHONE_FH - Cellphone flashhand is waiting on controller or dialogue handler - TIMEOUT invoked, setting MovieFail to true")
                cdPrintnl()
                cdPrintstring ("CELLPHONE_FH - Should run through and terminate this script if state is stuck at DISABLED, TAKEOUT OR AWAY.")

            #endif

        ENDIF


        IF g_Cellphone.PhoneDS < PDS_TAKEOUT

            g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE  //CRITICAL! This must be set so that it lets cellphone_controller know that it should break out of its WHILE clause in PDS_TAKEOUT check.

            #if IS_DEBUG_BUILD
                
                cdPrintnl()
                cdPrintstring ("CELLPHONE_FH - was waiting on controller or dialogue handler but the phone state has been set to either disabled or away by an external source.")
                cdPrintnl()
                cdPrintstring ("CELLPHONE_FH - Setting autoplay movie fail to true so this script will run through and terminate.")       
        
            #endif

        ENDIF


        
        #if IS_DEBUG_BUILD
            cdPrintnl() 
            cdPrintstring ("CELLPHONE_FH - Movie should have loaded but cellphone flashhand is waiting on controller or dialogue handler to make a decision on what the phone state should be...")
            cdPrintnl()
        #endif

    ENDWHILE





      
    IF g_Cellphone.PhoneDS = PDS_MINIMAL
    OR g_Cellphone.PhoneDS = PDS_MAXIMUM


        IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //In director mode, force cursor onto Snapmatic slot position only. 2171957

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (6)) //Set enum state to HOME_SCREEN


            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_FH - Forcing homescreen cursor highlight onto Snapmatic for director mode.")
                cdPrintnl ()
    
            #endif

        ELSE
        
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT (Previous_SelectedAppCursorPos_pageOne)) //Set enum state to HOME_SCREEN
        
        ENDIF
        
        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_FH - Update_Scaleform_VSE called - 203")
            cdPrintnl ()
    
        #endif






        //If phone is in sleep mode, show mini signifier in top portion of screen
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 1)

        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 0)

        ENDIF
 
        

        //Monochrome
        /*
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" Positive
        */
        

        //Badger
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" Positive
        ELSE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Select" Positive
        ENDIF




        //Monochrome
        /*
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"Back" - Negative
        */


        //Badger
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", 3, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"Back" - Negative
        ELSE
             LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", 3, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Back" - Negative
        ENDIF



        //Monochrome
        //Make sure this is off on homescreen   - note that the second int method param is 0 for off.
        /*
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
        */
       
       
        //Badger

        
        //Make sure this is off on homescreen   - note that the second int method param is 0 for off.
        IF g_b_Secondary_Screen_Available = FALSE
        
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                         
        ELSE

            IF g_bInMultiplayer //no secondary screen for multiplayer...
       
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE

        

                IF g_Homescreen_In_Secondary_Mode = TRUE

                    IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages 2
                    ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                        20, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages 2
                    ENDIF


                ELSE

                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_225") //Other Pages
                    ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //Other Pages
                    ENDIF

                ENDIF
                
                SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


            ENDIF

        ENDIF


        //Set button bar visibility..
        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //transparent

		//6417795 - April 2020
		//Makes sure the new homescreen mode switch soft key is shown in MP when initially launching to the homescreen but not during a phone call.
		ADD_NEW_MP_HOMESCREEN_MODE_SWITCH_SOFT_KEY()

  

    ENDIF


                                                                              

    


    //Make sure main gui movie can render to the phone on the launching of this script
    g_PhoneGuiRender_Enabled = TRUE


    //Set up render target, with specific check to get mobile_phone_render_id.
    Setup_MainGui_Rendertarget()


    //Set up initial stock market check launch time.
    
    IF NETWORK_IS_GAME_IN_PROGRESS()

        Net_StockMarketChecks_StartTime = GET_NETWORK_TIME()
    
    ELSE
    
        StockMarketChecks_StartTime = GET_GAME_TIMER()
    
    ENDIF



   


    IF g_Cellphone_Launched_by_Button_Press = TRUE

        //Display reminder about sleep status
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        
            IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = FALSE

                //Going to keep on reminding every time phone is brought out in the meantime, so will set back to FALSE before terminating. ( Using flag as a check for clearing help! )
                g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed = TRUE 


                FLOAT f_PrimaryHelpX, f_PrimaryHelpY

                IF g_Chosen_Ratio = DISPLAY_16_9

                    f_PrimaryHelpX = 0.75
                    f_PrimaryHelpY = 0.8

                ELSE

                    f_PrimaryHelpX = 0.65
                    f_PrimaryHelpY = 0.77

                ENDIF




                //HELP_AT_SCREEN_LOCATION("CELL_7052", f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, DEFAULT_GOD_TEXT_TIME, FLOATING_HELP_MISSION_SLOT)
                IF f_PrimaryHelpX = f_PrimaryHelpY
                    //do nothing - unref hack that will deleted in final optimisations.
                ENDIF

                PRINT_HELP ("CELL_7052", DEFAULT_HELP_TEXT_TIME)


            ENDIF

        ENDIF

    ENDIF







    //Debug functionality to check if rendertarget has been acquired.
    /*
    PhoneRenderTarget_ID = GET_SCRIPT_RENDERTARGET_RENDER_ID()  //replace with mobile phone render target? GET_MOBILE_PHONE_RENDER_ID?
    
 
     
    IF PhoneRenderTarget_ID = -1
        #if IS_DEBUG_BUILD
    
            SCRIPT_ASSERT ("Cannot find render target id for cellphone.")

        #endif
    ELSE
        #if IS_DEBUG_BUILD
    
            cdPrintstring ("Render target id for cellphone is ")
            cdPrintint (PhoneRenderTarget_Id)
            cdPrintnl()
            
        #endif


        GET_MOBILE_PHONE_RENDER_ID (PhoneRenderTarget_Id)

        
    ENDIF
    */



   


    /*
    LEFTSTICKX = 0,
    LEFTSTICKY = 1,
    RIGHTSTICKX = 2,
    RIGHTSTICKY = 3,
    LEFTSHOULDER1 = 4,
    LEFTSHOULDER2 = 5,
    RIGHTSHOULDER1 = 6,
    RIGHTSHOULDER2 = 7,
    DPADUP = 8,
    DPADDOWN = 9,
    DPADLEFT = 10,
    DPADRIGHT = 11,
    START = 12,
    SELECT = 13,
    SQUARE = 14,
    TRIANGLE = 15,
    CROSS = 16,
    CIRCLE = 17,
    LEFTSHOCK = 18,
    RIGHTSHOCK = 19
    */

 

    //Takeout anim delay mechanism.
    
    // start the time earlier than before so it will catch up
    IF g_bInMultiplayer = FALSE

         s_iTimeStampStart = GET_GAME_TIMER() + 375

    ENDIF
 









    
























    //Set up Phone transition checks.

    IF IsCurrentCamFirstPerson() //Decide if the initial camera mode is first or third person so we can safely check for any change during the main loop.
        
        InFirstPersonCamera = TRUE

        #if IS_DEBUG_BUILD

            cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson returned TRUE on launch of flashhand.")   
            cdPrintnl()
        
        #endif

    ELSE

        InFirstPersonCamera = FALSE

        #if IS_DEBUG_BUILD

            cdPrintstring("CELLPHONE_FH - IsCurrentCamFirstPerson returned FALSE on launch of flashhand.")   
            cdPrintnl()
        
        #endif

    ENDIF

    


     IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())



        //Cover checks for cellphone gameplay camera transitions. We need to show the hud cellphone whilst in cover, so we need to know when cover has been left to hide the hud in 1st person.
        IF IS_PED_IN_COVER(PLAYER_PED_ID())
        
            PlayerIsInCover = TRUE

            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_FH - PlayerIsinCover returned TRUE on launch of flashhand.")   
                cdPrintnl()
            
            #endif

            IF IsCurrentCamFirstPerson()

                //TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(), FALSE)
                Remove_Cellphone_From_Ear()

            ENDIF

        ELSE

            PlayerIsInCover = FALSE

            #if IS_DEBUG_BUILD

                cdPrintstring("CELLPHONE_FH - PlayerIsinCover returned FALSE on launch of flashhand.")   
                cdPrintnl()
            
            #endif

        ENDIF




    ENDIF


    //Adding in an Enter / Exit vehicle timer mechanism to prevent 3rd person problem when opening Snapmatic immediately after an enter / exit.
    IF IS_PC_VERSION() //2269787
        IF g_bInMultiplayer = FALSE

            JustEnteredExitVehicle_StartTime = GET_GAME_TIMER()//2269787
            JustEnteredExitVehicle = FALSE

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("Cellphone_flashhand - PC Only. Resetting Entered_Exit start timer and clearing Entered_Exit bool on FH launch.")
                cdPrintnl()   
            #endif

        ENDIF
    ENDIF


	//6417795 - April 2020
	//Critical. As a precaution clear the bit that specifies the MP homescreen is in secondary mode whenever the cellphone is launched.
	CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE) 

    WHILE TRUE

        WAIT(0)



        /* Debug Block
        IF g_FirstPersonTransitionOngoing

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.34, "STRING", "gFPTO TRUE.")

        ENDIF
        
        IF IsCurrentCamFirstPerson() //Only vanish the phone in third person.

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.54, "STRING", "IsCurrent TRUE.")

        ENDIF

        IF HasGameplayCameraTransitionOccurredDuringCall
        
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.74, "STRING", "TransitionOC TRUE.")

        ENDIF
        */




        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REQUEST_DYNAMIC_APPLIST_UPDATE)

            IF NOT g_Homescreen_In_Secondary_Mode
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update app list or homescreen when user is on first homescreen page or running an app.
                OR g_Cellphone.PhoneDS > PDS_MAXIMUM

                    #if IS_DEBUG_BUILD
                        cdPrintstring("Cellphone_FH REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST has been called and phone is in correct state to allow update.")
                        cdPrintnl()
                
                        PRINTLN("Cellphone_FH REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST has been called and phone is in correct state to allow update.")
                    #endif


                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REQUEST_DYNAMIC_APPLIST_UPDATE) //Critical!


                    Fill_All_Primary_App_Sheets()

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
                    Scaleform_Place_Items_on_Homescreen(1)

                    // Only set the display view to the homescreen if the user is currently on it. This preserves whatever app is running at the time.
                    IF g_Cellphone.PhoneDS = PDS_MAXIMUM
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))
                    ENDIF


                ENDIF
            ENDIF 

        ENDIF








        IF IS_CELLPHONE_CAMERA_IN_USE()

            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK) //Catch all fix for 1984437

        ENDIF




        IF g_bInMultiplayer = FALSE

            IF HasGameplayCameraTransitionOccurredDuringCall //This should only have be set to true during an ongoing call. Once we go back to a safer phone state, this is reset to FALSE.

                IF g_Cellphone.PhoneDS = PDS_MAXIMUM
                OR g_Cellphone.PhoneDS = PDS_RUNNINGAPP
            
                    HasGameplayCameraTransitionOccurredDuringCall = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - HasGameplayCameraTransitionOccurredDuringCall reset to FALSE. Phone back in MAX or RUNNING_APP state.")   
                        cdPrintnl()
                    
                    #endif
                    

                ENDIF

            ENDIF



            //Would need to have separate vehicle transition checks that is mutually exclusive of this. So if you are in first person and change vehicles types,
            //then something similar happens and the camera change checks can't run. Similarly, the vehicle checks can't run if g_FirstPersonTransitionOngoing = TRUE
            //or g_3DPhoneNeedsMovedUp = TRUE. Check position before making any movement change.
            IF g_Cellphone.PhoneDS > PDS_TAKEOUT

                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
                AND (g_FirstPersonTransitionOngoing = FALSE)
                AND (g_3DPhoneNeedsMovedUp = FALSE)
                



                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) 
                   

                            //Remember to put all further first person hud exceptions in ShouldFirstPersonPhoneHudMovementBeHidden()


       


                            //May need to put handling in for the player on call with a crash helmet in first person. He doesn't hold it to his ear in these scenarios.

                            
                            IF ShouldFirstPersonPhoneHudMovementBeHidden()


                                GET_MOBILE_PHONE_POSITION (v_CurrentHudPosition) 
            
                                IF g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio].y <> v_CurrentHudPosition.Y

                                
                                    #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - Hud movement check fired. Hud movement says it should be hidden but phone not at startvec.y")   
                                        cdPrintnl()
                                
                                    #endif
                            
                                    StandardBackTo1stPersonOrHudHideChecks()

                                ENDIF

                              

                            ELSE  //Hud phone should not be hidden.


                                GET_MOBILE_PHONE_POSITION (v_CurrentHudPosition) 
            
                                IF g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio].y <> v_CurrentHudPosition.Y

                                    IF NOT IS_CELLPHONE_CAMERA_IN_USE()

                                        #if IS_DEBUG_BUILD

                                            cdPrintstring("CELLPHONE_FH - Hud movement check fired. Hud movement says it should be shown but phone not at endvec.y")   
                                            cdPrintnl()
                                
                                        #endif
                                    
                                    ENDIF
                            

                                    //Need to put special clauses in here to get .Y email position for email. Maybe signal that it needs to rotate? //2033540



                                    /*
                                    BOOL b_PhoneVanishingOrVanishedin3rdPersonForCall = FALSE
                                    

                                    IF (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED))
                                    OR (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE))
                                    OR (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE))

                                        b_PhoneVanishingOrVanishedin3rdpersonForCall = TRUE


                                        #if IS_DEBUG_BUILD

                                            cdPrintstring("CELLPHONE_FH - Phone vanished during call in 3rd, shouldn't move up.")   
                                            cdPrintnl()
                                
                                        #endif



                                    ENDIF
                                    */

                                                                             
                                    //IF b_PhoneVanishingOrVanishedin3rdpersonForCall = FALSE

                                    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appEmail")) < 1  //Temp sub4 fix for 2033540

                                        StandardBackTo3rdPersonorHudShownChecks()
                
                                    ENDIF
                                    //ENDIF

                                ENDIF   
                                                

                           ENDIF
                                                    




                            //Cover checks for cellphone gameplay camera transitions. We need to show the hud cellphone whilst in cover, so we need to know when cover has been left to hide the hud in 1st person.
                            IF PlayerIsInCover = FALSE
                                   
                                IF IS_PED_IN_COVER(PLAYER_PED_ID())
                                
                                    PlayerIsInCover = TRUE

                                    #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - PlayerIsinCover now returning TRUE.")   
                                        cdPrintnl()
                                    
                                    #endif


                                    StandardBackTo3rdPersonorHudShownChecks()

                                    Remove_Cellphone_From_Ear()



                                ENDIF

                            ELSE

                                IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
                            
                                    PlayerIsInCover = FALSE

                                    #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - PlayerIsinCover now returning FALSE.")   
                                        cdPrintnl()
                                
                                    #endif


                                    StandardBackTo1stPersonOrHudHideChecks()

                                    IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT //See 2001926. For 99 percent of calls we want to show the phone on the hud
                                    OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                                        IF g_ConversationStatus <> CONV_STATE_WAITING_ON_PLAYER_ACCREJ 

                                            Put_Cellphone_To_Ear()

                                        ENDIF
                        
                                    ENDIF

                                ENDIF

                            ENDIF
 

             







                    
                            //Gameplay camera transition checks begin...
                            
                            IF InFirstPersonCamera = FALSE


                                IF IsCurrentCamFirstPerson() //Keep checking for any change... we might need to move the phone down. 
                                      
                                    InFirstPersonCamera = TRUE


                                    IF IS_PED_IN_COVER(PLAYER_PED_ID())

                                        Remove_Cellphone_From_Ear()

                                    ENDIF
            

                                    StandardBackTo1stPersonOrHudHideChecks()
                                                         
                                ENDIF


                            ELSE




                                IF GET_PED_STEALTH_MOVEMENT (PLAYER_PED_ID()) //Fix for 2040861
                                            
                                    IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT 
                                    OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                                        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)

                                    ENDIF

                                ENDIF




                                IF IsCurrentCamFirstPerson() = FALSE //Keep checking for any change.
                            
                                    InFirstPersonCamera = FALSE

                                    StandardBackTo3rdPersonOrHudShownChecks()
                                    
                                ENDIF


                            ENDIF

                            //End of gameplay camera transition check block.



                    ENDIF  //End of IS_ENTITY_DEAD checks,



                ENDIF //g_BS_HAS_CELLPHONE_FULLY_MOVED_UP , transition and move up check wrap end.

            ENDIF //g_Cellphone.PhoneDS > PDS_TAKEOUT wrap end.


        ENDIF  //g_bInMultiplayer = FALSE wrap end.



        //Critical Title Update.
        //This checks for a change in the availability of GTAO after the phone has launched.
        IF g_CellphoneHasMPAccess = TRUE  //HAS_GTAO_BEEN_PURCHASED() must have been TRUE when this script launched for the access flag to be TRUE.
        
            IF NOT IS_GTA_ONLINE_AVAILABLE()
            OR NOT NETWORK_IS_SIGNED_ONLINE()

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appCamera")) = 0  //Camera has its own sign-in checks. Phone should abort when it exits app.

                    IF g_Cellphone.PhoneDS > PDS_AWAY
                    
                        g_B_ForcedAway = TRUE

                        Check_For_Ongoing_Call_Interruption()

                        HANG_UP_AND_PUT_AWAY_PHONE()  //Hanging up phone seems safest bet here... Repopulating on the fly may prove too fiddly.

                        #if IS_DEBUG_BUILD

                            cdPrintstring("CELLPHONE_FH - Terminating SP and MP mobile phone use as IS_GTA_ONLINE_AVAILABLE() in no longer TRUE.")   
                            cdPrintnl()
                        
                        #endif

                        g_CellphoneHasMPAccess = FALSE


                    ENDIF

                ENDIF

            ENDIF

        ENDIF




        IF g_b_QuickSaveGreyedOut = FALSE   //On launch this bool was not set to true so this means the quick save option wase not greyed out, so the external quick save
                                        //check must have returned true.

            
            IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
            OR (g_QuickSaveDisabledByScript = TRUE) // #1423552
            OR IS_REPEAT_PLAY_ACTIVE() //Added at request of #1416986
            //IF g_b_ReplaceThisWithQSavailable = FALSE //If the external quick save check now returns false, then we need to update the first page of the homescreen
                                                //if the user is on that. This is ascertained by the PDS_MAXIMUM check and making sure the phone is not on the second page 
                                                //via the Secondary Mode clause.



        



                IF NOT g_Homescreen_In_Secondary_Mode
                    IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.


                        g_HomescreenIcon_AlphaValue = i_HomescreenIcon_GreyedOut
                    
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)
               
                        Fill_All_Primary_App_Sheets()
                        TitleUpdate_Patch_In_JobList_For_SP_Phone()

                        Scaleform_Place_Items_on_Homescreen(1)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH: Quicksave was available on phone launch but has now become unavailable. Updating homescreen with greyed out icons.")
                            cdPrintnl()   
                        #endif


                    ENDIF
                ENDIF
                
                

                g_b_QuickSaveGreyedOut = TRUE 

            ENDIF


        ELSE //The phone must initially have had the save option greyed out as the external quick save available check returned false.


            IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
            AND (IS_REPEAT_PLAY_ACTIVE() = FALSE )  // Added at request of #1416986
            AND (g_QuickSaveDisabledByScript = FALSE)  // #1423552
            //IF g_b_ReplaceThisWithQSavailable = TRUE //Quick save has become available whilst the phone is up.
            
                IF NOT g_Homescreen_In_Secondary_Mode
                    IF g_Cellphone.PhoneDS = PDS_MAXIMUM

                        g_HomescreenIcon_AlphaValue = i_HomescreenIcon_FullAlpha

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)

                        Fill_All_Primary_App_Sheets()
                        TitleUpdate_Patch_In_JobList_For_SP_Phone()

                        Scaleform_Place_Items_on_Homescreen(1)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH: Quicksave was unavailable on phone launch but has now become available. Updating homescreen with colour icons.")
                            cdPrintnl()   
                        #endif

                    ENDIF
                ENDIF


                g_b_QuickSaveGreyedOut = FALSE

            ENDIF


        ENDIF
            
                        
       
                    


        IF g_b_CameraGreyedOut = FALSE   //On launch this bool was not set to true so this means the camera option was not greyed out.
                     
            
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)

                IF NOT g_Homescreen_In_Secondary_Mode
                    IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update when user is on first homescreen page.


                        //g_HomescreenIcon_AlphaValue = i_HomescreenIcon_GreyedOut //Transparency value now decided upon in Scaleform_Place_Items_on_Homescreen()
                    
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)
               
                        Fill_All_Primary_App_Sheets()
                        TitleUpdate_Patch_In_JobList_For_SP_Phone()

                        Scaleform_Place_Items_on_Homescreen(1)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH: Camera was available on phone launch but has now become unavailable. Updating homescreen with greyed out icons.")
                            cdPrintnl()   
                        #endif


                    ENDIF
                ENDIF
                
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FH: Secondary camera now greyed out output check. Updating homescreen with greyed out icons. Has previous check fired?")
                    cdPrintnl()   
                #endif
        


                g_b_CameraGreyedOut = TRUE 

            ENDIF


        ELSE //The phone must initially have had the camera option greyed out as the external quick save available check returned false.

            IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME))

            
                IF NOT g_Homescreen_In_Secondary_Mode
                    IF g_Cellphone.PhoneDS = PDS_MAXIMUM


                        //g_HomescreenIcon_AlphaValue = i_HomescreenIcon_FullAlpha //Transparency value now decided upon in Scaleform_Place_Items_on_Homescreen()

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 1)

                        Fill_All_Primary_App_Sheets()
                        TitleUpdate_Patch_In_JobList_For_SP_Phone()

                        Scaleform_Place_Items_on_Homescreen(1)

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH: Camera was UNAVAILABLE on phone launch but has now become available. Updating homescreen with colour icons.")
                            cdPrintnl()   
                        #endif

                    ENDIF
                ENDIF


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FH: Secondary camera now full coloured output check. Updating homescreen with colour icons. Has previous check fired?")
                    cdPrintnl()   
                #endif
        

                g_b_CameraGreyedOut = FALSE

            ENDIF


        ENDIF





































        

 

        IF NOT IS_PAUSE_MENU_ACTIVE()
            IF g_3dPhonePosVec.y <> g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio].y    //If this is equal then we can assume the phone has been moved off screen for an mpJobOffer but is not AWAY.
                                                                                       //We wouldn't want to make exclusive any of these inputs any longer in that case.
                
            OR IsCurrentCamFirstPerson() //We need to check this also as the Vec comparison above may not always return true in first person. 2006165

           

                IF g_Cellphone.PhoneDS > PDS_AWAY        //Only disable while phone is on screen AND not moving down.
                
                    
                    
                    
                    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTrackify")) = 0 //We need to allow the player to sprint if the Trackify app is in use. //Requested by bug 1156535
                        
                        IF g_bInMultiplayer //2194226
                        
                            IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                                IF g_CallRequiringPlayerResponse = TRUE   //We'll only make this exclusive, thus disabling sprint, when on a response call.
                                OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_WITH_REPLIES_WAITING_ON_USER_INPUT)

                                    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)))

                                ENDIF

                            ELSE

                                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)))
                            
                            ENDIF
                                                    
                        ELSE

                            SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)))
                    
                        ENDIF

                    ENDIF    
                

                    /*
                    #if IS_DEBUG_BUILD
                    
                        cdPrintstring("Checking PHONE_NEGATIVE exclusivity")
                        cdPrintnl()

                    #endif
                    */
					IF  GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0 

	                    //Japanese
	                    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 605548
	                    
	                    //Exclusivity for these should only be set when PS3 square / 360 X is displayed. Legacy.

	                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)   //See bug 740666
	                     
	                        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_DELETE_OPTION_INPUT)))
	                        //SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))) //1854594

	                    ENDIF

					ENDIF
                    //Have a nagging doubt that this was originally within the bitset check for a reason, but tests with hyperlink texts and contacts seem okay.
             
                    IF NOT IS_CELLPHONE_TRACKIFY_IN_USE() // Fix for B* 1880594
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
                        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))) //1854594
                    ENDIF

                ENDIF

         

            ENDIF



            IF g_3dPhonePosVec.y = g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio].y //Deliberately not using as ELSE until this auto integrates.
            //Phone could be in hidden state if y component of vector is the same.

                #IF IS_JAPANESE_BUILD
                    IF g_IsThisAnMPChatCall

                        //This should disable sprint and prevent the phone hanging up during a chat call in 2150013
                        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))  //See bug 2150013

                    ENDIF
                #ENDIF

            ENDIF


        ENDIF






     

        //Legacy...
        //Prevent radio station. changing weapon when d-pad navigates homescreen.
        //DISABLE_CONTROL_BUTTON (PLAYER_CONTROL, DPADLEFT)
        //DISABLE_CONTROL_BUTTON (PLAYER_CONTROL, DPADRIGHT)
        //DISABLE_CONTROL_BUTTON (PLAYER_CONTROL, DPADUP)
        //DISABLE_CONTROL_BUTTON (PLAYER_CONTROL, DPADDOWN)



    
        
        IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) 
            
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)

            //DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)
            
        ENDIF

        

        IF NOT IS_PAUSE_MENU_ACTIVE() //1511782


            /* Commenting this out as it causes conflicts with the PC control scheme, and may not be necessary anyway.
            IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED))    //If the phone isn't onscreen don't disable these... we need to be able 
                                                                                                                    //to reinstate it from vanished...
                 IF !g_bBrowserVisible //1084004
                 //OR NOT IS_CELLPHONE_CAMERA_IN_USE()

                    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))) //Need this as it may not necessarily always be the same as phone_nav_up...
                    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT)))
                 
                 ENDIF
            ENDIF
            */

            //IF g_Cellphone.PhoneDS < PDS_ONGOING_CALL  //This will allow Radio Station control during a call. Included on a trial basis. See bug 990936.

            //Not me disabling the radio wheel it seems...  Adam F. says waive. Reverted change above.
            IF !g_bBrowserVisible//1084004
            //OR NOT IS_CELLPHONE_CAMERA_IN_USE()
            
                // Mouse wheel scrolling - overrides other items set to mouse wheel while phone is active
                IF NOT IS_CELLPHONE_CAMERA_IN_USE()
                    SET_INPUT_EXCLUSIVE( PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
                    SET_INPUT_EXCLUSIVE( PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
                    // Have to re-enable these, as setting one exclusive can disable the other.
                    ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
                    ENABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
                ENDIF
            
                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT)))


                IF NOT IS_CELLPHONE_TRACKIFY_IN_USE()   //This should be enough to fix 1796220 but code are auto-blocking inputs whenever the phone object is created. Tested with 
                                                        //a remote call to CREATE_MOBILE_PHONE that did not launch cellphone scripts.

                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT)))
                
                ENDIF
                
                //cdPrintstring("Testing! x3")
                //cdPrintnl()

                IF g_Cellphone.PhoneDS > PDS_TAKEOUT    
                    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT)))
                ENDIF
            ENDIF


        ENDIF
        
       

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT))  
            
            Failsafe_Depress_Period_Underway = TRUE
            
            SETTIMERA(0)

        ENDIF



        //Special case delay for quicksave button carry through to pausemenu that code don't want to fix. #967067
        IF local_Request_for_QuickSave = TRUE

            IF NOT (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)))

                g_HasCellphoneRequestedSave = TRUE //Button has been released, okay to launch quicksave routine in save_anywhere.sc
                local_Request_for_QuickSave = FALSE

            ELSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_FLASHHAND - Waiting on Quicksave button release")
                    cdPrintnl()   
                #endif

            ENDIF
            
            IF NOT (g_Cellphone.PhoneDS > PDS_AWAY)
        
                local_Request_for_QuickSave = FALSE  //The phone has been forced away by injury, death or script. Abandon this routine and the quicksave launch.

            ENDIF

        ENDIF



        //Mega_failsafe prompted by 246816
        IF Failsafe_Depress_Period_Underway

            IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT))  

                IF TIMERA() > 5000
            
                    g_Cellphone.PhoneDS = PDS_AWAY

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 57. Cellphone_flashhand assigns PDS_AWAY")
                        cdPrintnl()   
                    #endif


                    //Maybe force a reply here?

                    KILL_PHONE_CONVERSATION()

                    #if IS_DEBUG_BUILD
               
                        cdPrintnl()
                        cdPrintstring ("CELLPHONE_FLASHHAND Cellphone flashhand aborting... and killing any conversation due to long back button press!")
                        cdPrintnl()

                    #endif


                ENDIF

            ELSE

               Failsafe_Depress_Period_Underway = FALSE

            ENDIF

        ENDIF



  

        
        //Get this every frame... When an app launches the start time is reset so we can work out the difference
        g_App_Elapsed_SafetyBuffer = GET_GAME_TIMER()






        //CRITICAL! This is set to TRUE by CHECK_FOR_APPLICATION_EXIT() and CHECK_FOR_ABNORMAL_EXIT() when either of those functions return TRUE themselves.
        //AppCamera explicitly sets g_MainGui_RendertargetNeedsReset as a special case.
        
        IF g_MainGui_RendertargetNeedsReset
        
            Setup_MainGui_Rendertarget() //Re-initialise the main scaleform gui movie rendertarget

            g_PhoneGuiRender_Enabled = TRUE  //Make sure that the cellphone is able to actually render the movie again.
            
            g_MainGui_RendertargetNeedsReset = FALSE

            
            //SET_SPRITES_DRAW_BEFORE_FADE (FALSE)
            //SET_TEXT_DRAW_BEFORE_FADE (FALSE)

            //No longer required due to code side alteration in default behaviour. 24.04.12
            //SET_SCRIPT_GFX_DRAW_ORDER (GFX_ORDER_AFTER_FADE) //altered in accordance with bug 391635

        
        ENDIF




        /* Min mode not currently required...
        IF g_Cellphone.PhoneDS = PDS_MINIMAL

            Check_Min_for_Max_DisplayStatus()
        
        ENDIF
        */






        






        //Main gui drawing loop.  This needs to draw consistently as any extra WAITs can impact it.

        IF g_Phone_Active_but_Hidden = FALSE //Make sure the phone is being drawn and not hidden but still active, for any hotswap transition.



            //Hide hud elements that appear in bottom right corner.
            HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_VEHICLE_NAME)
            HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_AREA_NAME)
            HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_DISTRICT_NAME)   
            HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_STREET_NAME)


          
        
            Draw_DisplayStatus()
        


            Perform_StockMarketChecks()
            
            SET_TEXT_RENDER_ID (PhoneRenderTarget_ID)            
      

            //Only render the movie if appropriate. Some app scripts may not want the main cellphone movie and associated buttons to render to the screen.
            //If the main movie does not render when expected, check for running apps that have not terminated themselves by checking for normal or abnormal exits.
            IF g_PhoneGuiRender_Enabled = TRUE

                SET_SCRIPT_GFX_DRAW_ORDER (GFX_ORDER_AFTER_HUD)

                IF g_Use_Prologue_Cellphone

                    //DRAW_SCALEFORM_MOVIE (SF_MovieIndex, 0.100, 0.118, 0.2, 0.223, 255, 255, 255, 255)

                    DRAW_SCALEFORM_MOVIE (SF_MovieIndex, g_SF_PhonePosX, g_SF_PhonePosY, g_SF_PhoneSizeX, g_SF_PhoneSizeY, 255, 255, 255, 255)


                ELSE

                   
                    DRAW_SCALEFORM_MOVIE (SF_MovieIndex, g_SF_PhonePosX, g_SF_PhonePosY, g_SF_PhoneSizeX, g_SF_PhoneSizeY, 255, 255, 255, 255)

                    #if IS_DEBUG_BUILD

                        IF g_b_DisplayDrawingMovieDump
                            cdPrintstring ("CELLPHONE_FH - Drawing movie this frame whilst in main loop.")
                            cdPrintnl()
                        ENDIF
                    
                    #endif

                ENDIF
                
                /* Buttons are no longer a separate movie in Badger...
                IF g_b_Rotate3dPhonePortrait = TRUE
                OR g_b_Rotate3dPhoneLandscape = TRUE
                    //Don't draw buttons when rotating for camera app.
                ELSE
                    DRAW_SCALEFORM_MOVIE (SF_MovieButtonsIndex, g_SF_ButtonsPosX, g_SF_ButtonsPosY, g_SF_ButtonsSizeX, g_SF_ButtonsSizeY, 255, 255, 255, 255)
                ENDIF
                */    
           


                Update_Cellphone_Clock_and_Health()


            ENDIF
            

       


            IF g_3DPhoneNeedsMovedUp = TRUE
                
                
                //Added for bug 69357
              
                IF g_Cellphone.PhoneDS > PDS_AWAY


                       Move3dPhoneUpScreen()

                                                                                                
                ENDIF 
                
                
            ELSE
                
                IF g_Cellphone.PhoneDS > PDS_AWAY


                    //Hands free mode will need to be multiplayer only.

                    

                    IF g_FirstPersonTransitionOngoing = TRUE

                        MovetoFirstPersonVanishPhone()

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_FH - Move to first person vanish called via g_FirstPersonTransitionOngoing.")
                            cdPrintnl()
                        #endif
                
                    ENDIF





                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

                        /* Removing this block for Next Gen as vanishing the phone causes great confusion in first person.
                        IF g_FirstPersonTransitionOngoing = FALSE

                        CallInProgressReinstatePhone()

                        ENDIF
                        */


                        IF g_bInMultiplayer = TRUE  //Need to allow chat call hands free mode to function correctly in MP.


                            CallInProgressReinstatePhone() 
                             

                        ENDIF



                    ELSE
    
                        IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)) //Don't want to check this when phone is in transit...

                       
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)    //Also need to make sure phone has actually been vanished once before reinstating it... 
                                                
                                 
                                //Could remove this block for Next Gen as vanishing the phone causes great confusion in first person. 
                                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_TAKEOUT_INPUT))

                                    #if IS_DEBUG_BUILD
                                    
                                        cdPrintstring ("CELLPHONE_FH - Vanished phone ordered to re-appear by user input")
                                        cdPrintnl()

                                    #endif                                  

                                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)     //signal that we want to run transit routine now.
                                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)    //phone no longer vanished.
                                
                                    CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

                                ENDIF


                            ENDIF


                        ELSE //This bit is set via dialogue handler's Has_HidePhoneTrialTimerElasped() routine AND the HANDS FREE stuff!

                                                         
                                
                                /* Removing this block for Next Gen as vanishing the phone causes great confusion in first person.

                                IF g_FirstPersonTransitionOngoing = FALSE
                                AND IsCurrentCamFirstPerson() = FALSE //Only vanish the phone in third person.
                                AND HasGameplayCameraTransitionOccurredDuringCall = FALSE

                                                                        
                               
                                        CallInProgressVanishPhone()
                                    
                                    
                                    

                                ENDIF
                                */


                                IF g_bInMultiplayer = TRUE   //Need to allow chat call hands free mode to function correctly in MP.

                                    CallInProgressVanishPhone()

                                ENDIF


                       
                       
                        ENDIF

                    ENDIF


                    



                ENDIF

                   
            ENDIF


        ENDIF


        

    

        IF b_Waiting_On_SF_Return_Value

            IF g_Cellphone.PhoneDS = PDS_MAXIMUM
            
                //IF NOT (Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))) //Experimenting with delay for 967067. Not a good idea.
                    
                    Check_For_App_Return_Value_Ready()

                //ENDIF

            ENDIF

        ELSE

            IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD))
          
            
                //We only want to run an app if no other app is currently running...
                IF g_Cellphone.PhoneDS = PDS_MINIMAL
                OR g_Cellphone.PhoneDS = PDS_MAXIMUM


                    //Don't check for user navigation if the phone is hidden...
                    IF g_Phone_Active_but_Hidden = FALSE
                        
                        //Check for app launching bit here...


                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) < 1 //special cases to cater for rotational delay.
                        AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appEmail")) < 1

                            IF g_Use_Prologue_Cellphone = FALSE
                     
                                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //1855377 - We want to allow time for a second "Up" press to launch Snapmatic...

                                    //IF g_b_Secondary_Screen_Available //which should always be the case for SP now...
                                    IF NOT g_bInMultiplayer

                                        //Check this in to use multiple screen wrapping navigation requested in bug 740662

                                        IF local_Request_for_Quicksave = FALSE //prevent further navigation if the user is still holding down the app select button during a quicksave launch.
                                            
                                            //Check_for_Dual_Homescreen_Navigation()
    
                                            IF NOT (IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)) //Prevents the user quickly moving to an undefined slot when Snapmatic only is present.

                                                Check_For_Single_Homescreen_Navigation() //For 1409258
                                            
                                            ELSE  //2411448 AppMPJOBLIST now needs to be navigated to if available 

                                                IF g_CellphoneHasMPAccess
                                                    Check_For_Director_Mode_Navigation()
                                                ENDIF   

                                            ENDIF

                                        ENDIF
                                    
                                    ELSE
									
										//6417795 - April 2020
										//Critical. MP Only.
										//If we are in the secondary mode of the homescreen, specifically check for that navigation routine only...
										IF IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE) 
											Check_For_New_MP_Secondary_Mode_Navigation()	
										ELSE
                                        	Check_For_New_MP_Single_Homescreen_Navigation() //...otherwise, use the regular MP navigation mode.
										ENDIF

                                    ENDIF

                                ENDIF

                            ENDIF


                  

                            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //launch routine sets state to running app, so this won't run more than once.

                                CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppCONTACTS, FALSE, TRUE, TRUE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                
                                
                                /*

                                LAUNCH_CELLPHONE_APPLICATION (AppCONTACTS, FALSE, TRUE, TRUE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              

                                */

                            ELSE

                                IF CellphoneAppAutoLaunchType = ACT_APP_NONE



                                    IF NOT (IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC))  //1855377

                                        IF g_bInMultiplayer  //Can cause problems with crappy joypads / framerates so relegated to MP only and can be toggled off via an MP stat. 1988302
                                                             //Original bug was for GTAO and a suggestion so not missed in SP. Much trickier to put a setting in SP. Also problematic in NG.

                                            Check_For_QuickLaunch_of_Snapmatic()                                    
                                        
                                        ENDIF

                                        Check_for_App_Selection() //moved 25.10.11 so that another app can't be selected before contacts launches.

                                        Check_for_Secondary_Screen_Selection()

                                    ENDIF

                           

                                ELSE
                                
                                   
                                        
                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("CELLPHONE_FH - Activatable App Launch in process. Type is ")
                                        cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                        cdPrintnl()
                                    #endif


                                    IF g_bInMultiplayer  //Can cause problems with crappy joypads / framerates so relegated to MP only and can be toggled off via an MP stat. 1988302
                                                         //Original bug was for GTAO and a suggestion so not missed in SP. Much trickier to put a setting in SP. Also problematic in NG.

                                        Check_For_QuickLaunch_of_Snapmatic()

                                    ENDIF


                                    IF NOT (IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC))
                                    IF CellphoneAppAutoLaunchType = ACT_APP_EMAIL
                    

                                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP) // Launch AppMPEmail rather than standard SP email


                                            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //Need to allow for rotation.
                                                

                                                IF g_bInMultiplayer


                                                    CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppEmail, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                                    
                                                    /*

                                                    //Check_for_App_Selection() //moved 25.10.11 so that another app can't be selected before contacts launches.
                                                    LAUNCH_CELLPHONE_APPLICATION (AppEmail, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                                    
                                                    */

                                                    //In MP, the AppEmail Enum is binded to a different script.                                                


                                                    #if IS_DEBUG_BUILD
                                                        cdPrintnl()
                                                        cdPrintstring("CELLPHONE_FH - Activatable EMAIL App caused direct launch of MP Specific Email App. ")
                                                        cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                        cdPrintnl()
                                                    #endif

                                                
                                                ELSE

                                                    g_ShouldForceSelectionOfLatestAppItem = FALSE  //Make sure that if we somehow get an MP email in SP, it doesn't get stuck on Andrew Knight's shitty SP script.

                                                    #if IS_DEBUG_BUILD
                                                        cdPrintnl()
                                                        cdPrintstring("CELLPHONE_FH - Activatable MP EMAIL weirdly received in SP... taking precaution of removing last item force read.")
                                                        cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                        cdPrintnl()
                                                    #endif


                                                ENDIF
                                                        
                                                CellphoneAppAutoLaunchType = ACT_APP_NONE

                                                CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP)


                                            ENDIF

                                           
                                        ELSE

                                            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //Need to allow for rotation.
                                                

                                                CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppEmail, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                                
                                                /*

                                                //Check_for_App_Selection() //moved 25.10.11 so that another app can't be selected before contacts launches.
                                                LAUNCH_CELLPHONE_APPLICATION (AppEmail, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                                
                                                */

                                                #if IS_DEBUG_BUILD
                                                    cdPrintnl()
                                                    cdPrintstring("CELLPHONE_FH - Activatable EMAIL App caused direct launch of standard SP email legacy app. ")
                                                    cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                    cdPrintnl()
                                                #endif

                                                CellphoneAppAutoLaunchType = ACT_APP_NONE

                                            ENDIF

                                        ENDIF



                                    ELSE

    
                                        IF CellphoneAppAutoLaunchType = ACT_APP_TEXT

                                            
                                            CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppTEXTS, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                            
                                            /*

                                            LAUNCH_CELLPHONE_APPLICATION (AppTEXTS, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                            
                                            */

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("CELLPHONE_FH - Activatable TEXTS App caused direct launch of app. ")
                                                cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                cdPrintnl()
                                            #endif

                                            CellphoneAppAutoLaunchType = ACT_APP_NONE

                                        ENDIF


                                        //INVITES app would go here!
                                        IF CellphoneAppAutoLaunchType = ACT_APP_INVITE


                                            CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppMPJobList, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                            

                                            /*

                                            LAUNCH_CELLPHONE_APPLICATION (AppMPJobList, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                            
                                            */

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("CELLPHONE_FH - Activatable  INVITE  App caused direct launch of app. ")
                                                cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                cdPrintnl()
                                            #endif

                                            CellphoneAppAutoLaunchType = ACT_APP_NONE

                                        ENDIF

                                        //New launch type and script launch for BossAgency
                                        IF CellphoneAppAutoLaunchType = ACT_APP_BOSSINV


                                            CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (AppDummyApp0, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                            

                                            /*

                                            LAUNCH_CELLPHONE_APPLICATION (AppMPJobList, FALSE, TRUE, FALSE) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE              
                                            
                                            */

                                            #if IS_DEBUG_BUILD
                                                cdPrintnl()
                                                cdPrintstring("CELLPHONE_FH - Activatable BOSSINV App caused direct launch of app. ")
                                                cdPrintInt (ENUM_TO_INT(CellphoneAppAutoLaunchType))
                                                cdPrintnl()
                                            #endif

                                            CellphoneAppAutoLaunchType = ACT_APP_NONE

                                        ENDIF
                                    ENDIF
                                    ENDIF



                                ENDIF

                            ENDIF


                        ENDIF

                    ENDIF

                
                ENDIF
                                                      
                                                      
                                                      
            ELSE
                   


                    IF HAS_SCRIPT_LOADED (g_AppList[SelectedAppIndex].AppScriptName)


                        IF SelectedAppIndex = ENUM_TO_INT (appCONTACTS)
                   
                            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[SelectedAppIndex].AppScriptNameHash) = 0

                                Application_Thread = START_NEW_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName, CONTACTS_APP_STACK_SIZE)


                                #if IS_DEBUG_BUILD
                                    cdPrintstring("CELLPHONE_FH -  Launching chosen script with contacts stack size")
                                    cdPrintnl()
                                #endif

                            ENDIF


                        ELSE

                            BOOL b_BennysQuickLaunchAvailable = FALSE
                            //2411187 - Add launch mechanism stack size clause for Benny's Motorworks
                            IF SelectedAppIndex = ENUM_TO_INT (appDummyApp0)
                                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY) = FALSE
                                AND g_bEnableHackingApp = FALSE

                                    //November 2015 url:bugstar:2554214 Update. This was only for the duration of Lowrider. Removing Benny's launch from dummy app position.
                                    //b_BennysQuickLaunchAvailable = TRUE
                                    //#if IS_DEBUG_BUILD
                                        //cdPrintstring("CELLPHONE_FH -  Launching Bennys with contacts stack size")
                                        //cdPrintnl()
                                    //#endif

                                ENDIF
                            ENDIF
                                                 

                            IF SelectedAppIndex = ENUM_TO_INT (appINTERNET)  //Internet Stack Size Work.
                            OR b_BennysQuickLaunchAvailable = TRUE

                                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[SelectedAppIndex].AppScriptNameHash) = 0
                   
                                    Application_Thread = START_NEW_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName, APP_INTERNET_STACK_SIZE)


                                    #if IS_DEBUG_BUILD
                                        cdPrintstring("CELLPHONE_FH -  Launching chosen Internet script with Internet stack size")
                                        cdPrintnl()
                                    #endif

                                ENDIF

                            ELSE

                                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[SelectedAppIndex].AppScriptNameHash) = 0

                                    Application_Thread = START_NEW_SCRIPT (g_AppList[SelectedAppIndex].AppScriptName, CELLPHONE_STACK_SIZE)


                                    #if IS_DEBUG_BUILD
                                        cdPrintstring("CELLPHONE_FH -  Launching chosen script with cellphone stack size")
                                        cdPrintnl()
                                    #endif

                                ENDIF

                            ENDIF


                        ENDIF

                        SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[SelectedAppIndex].AppScriptName)

                        //Force clock update for transition to miniature clock on scaleform display view state change.
                        ClockThrottleMins = 99

                        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)

                    ENDIF



                     //Timer check for bug 2378079 - new content starts.                    
                 
                    /*
                    IF ARE_STRINGS_EQUAL ("appInternet", g_AppList[SelectedAppIndex].AppScriptName)

                        Post_AppLaunch_CurrentTime = GET_GAME_TIMER()

                        IF Post_AppLaunch_CurrentTime - AppLaunch_StartTime > 10000

                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - WARNING! Was waiting on AppInternet to launch but load time has surprassed 10 seconds. Aborting!")
                                cdPrintnl()   
                            #endif

                            SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[SelectedAppIndex].AppScriptName)

                            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD)

                            HANG_UP_AND_PUT_AWAY_PHONE()

                        ENDIF

                    ENDIF
                    */
                    //2378079 - new content ends. 


            ENDIF // end of NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_WAITING_ON_APP_TO_LOAD) condition.

  
        ENDIF // end of b_Waiting_On_SF_Return_Value condition.
        
        // So we can go back using Esc
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
            IF IS_PAUSE_MENU_ACTIVE()
                //DISABLE_FRONTEND_THIS_FRAME()
                DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
            ENDIF
        ENDIF
                                                  
        //Will combine PDS_DISABLED and PDS_AWAY at some point. Looks like they will be identical in operation...
        IF g_Cellphone.PhoneDS = PDS_DISABLED

            //This spams until phone has finished moving down...
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone now disabled. 3d phone moving offscreen and terminating cellphone_flashhand.sc. YcompSpeed is ")
                PRINTFLOAT (f_YComponent_FPSRemovalSpeed)
                cdPrintnl()
                cdPrintnl()
            #endif
            

            Move3dPhoneDownScreen_Then_Terminate()


        ENDIF


         //Will combine PDS_DISABLED and PDS_AWAY at some point. Looks like they will be identical in operation...
        IF g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY

            //This spams until phone has finished moving down...
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone now disabled_this_frame_only. 3d phone moving offscreen and terminating cellphone_flashhand.sc. YcompSpeed is ")
                PRINTFLOAT (f_YComponent_FPSRemovalSpeed)
                cdPrintnl()
                cdPrintnl()
            #endif
            

            Move3dPhoneDownScreen_Then_Terminate()


        ENDIF



        IF g_Cellphone.PhoneDS = PDS_AWAY


            Move3dPhoneDownScreen_Then_Terminate()    

            //This procedure repeats... until phone has finished moving down.
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_FH - Cellphone now in AWAY state. 3d phone moving offscreen and terminating cellphone_flashhand.sc. YcompSpeed is ")
                PRINTFLOAT (f_YComponent_FPSRemovalSpeed)
                cdPrintnl()
                cdPrintnl()
            #endif
            


        ELSE

            //-------------------------------------------------------------------------------------------------------------------------
            //
            //          Incident and Disable App Handling!
            //
            //_________________________________________________________________________________________________________________________

            IF (g_IsThisAnMPChatCall) //Chat call hands free control mechanism.
            IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL       //1566736
            IF g_bInMultiplayer

                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_THIS_IS_AN_ANSWERPHONE_CALL) //If this is an answerphone call, don't auto-hide phone. 

                    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) 

                        //IF Has_HidePhoneTrialTimerElasped() //Move phone downscreen after a short time after a call reaches CONNECTED status...
                        IF Is_Player_On_Voice_Chat_Phonecall()

                            
                            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //don't check for this button if we are sending a cellphone pic.
                           
                            
                                    
                                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Go Hands free button pressed!


                                    IF NOT (IS_PC_VERSION()) //2193981 - Conflicts with handbrake. Confusing.
                                    IF NOT (g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED) 

                                       
                             
                                        IF g_Cellphone.PhoneDS > PDS_MAXIMUM //Make sure the phone can only move offscreen if a call is ongoing

                                            //Might have to have a check in here to not put the phone away if it's on the last couple of lines? Polish.
                                            
                                            //Set appropriate bits...

                                        
                                            // Could remove this block for Next Gen as vanishing the phone causes great confusion in first person.

                                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE) //Let flashhand know transit routine should commence...
                                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) //we don't want to go through this routine more than once in one call...


                                            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)
                                            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

                                            #if IS_DEBUG_BUILD
                                    
                                                cdPrintnl()
                                                cdPrintstring ("CELLPHONE_FHAND - Button press detected to go hands free on Chat Call. Setting vanish bits...")
                                                cdPrintnl()

                                            #endif


                                            SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DO_REVEAL_PHONE)   
                                            

                                            
                                            

                                            /*
                                            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_TAKEOUT_FOR_HANDS_FREE_HELP_DONE)
                                                
                                                PRINT_HELP("CELL_HF_VIEW") //When hidden during a chat call, press ~INPUT_CELLPHONE_UP~ to view your cellphone again.
                                                SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_TAKEOUT_FOR_HANDS_FREE_HELP_DONE)
                                            
                                            ENDIF
                                            */


                                        ENDIF

                                    ENDIF
                                    ENDIF

                                ENDIF


                            ENDIF

                        ENDIF

                    ENDIF

           
 

                ENDIF
            ENDIF
            ENDIF
            ENDIF
            









            IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE

                g_B_ForcedAway = TRUE

                HANG_UP_AND_PUT_AWAY_PHONE()


                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - Movie Fail returns true, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                    cdPrintnl()
                        
                #endif

            ENDIF


            //Additional clause for 246816
            IF g_Attempted_Call_Aborted = TRUE


                g_B_ForcedAway = TRUE

                HANG_UP_AND_PUT_AWAY_PHONE()


                #if IS_DEBUG_BUILD

                    cdPrintstring("CELLPHONE_FH - Attempted_Call_Aborted returns true, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                    cdPrintnl()
                        
                #endif

            ENDIF           



            IF g_bInMultiplayer

                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU) //Can be set via public proc KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU()

                    IF IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_LOCAL_PLAYER_USING_DRONE()

                        IF voiceSession.bInVoiceSession = TRUE  //Clause to prevent pause menu hanging up Chat Calls - #1478433

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - MP Pause Menu Active - no keep call bit set, NOT hanging up phone as this player in VOICE SESSION.")
                                cdPrintnl()
                                
                            #endif

                        ELSE

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - MP Pause Menu Active - no keep call bit set, flashhand calling HANG_UP_AND_PUT_AWAY_PHONE...")
                                cdPrintnl()
                                
                            #endif


                            Check_For_Ongoing_Call_Interruption()

                            HANG_UP_AND_PUT_AWAY_PHONE()

                        ENDIF


                    ENDIF

                ENDIF



            ENDIF




            //Move phone away if the player in injured and, if appropriate, set g_LastCellphoneCallInterrupted to TRUE beforehand.
            IF IS_PLAYER_PLAYING (PLAYER_ID()) //will replace this with health differential...



                //Disable Camera app if in water or in certain vehicles. App will be greyed out dynamically if phone is on homescreen.
                
                IF IS_ENTITY_IN_WATER (PLAYER_PED_ID())
                    IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) > 0.3 
                    //preventing camera use in deeper water due to code request. See bug #1238505
       
                        #if IS_DEBUG_BUILD
                            IF g_DumpDisableEveryFrameCaller

                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - No camera use due to submerged level exceeding 0.3. See Bug 1336898.")
                                cdPrintnl()   

                            ENDIF
                        #endif


                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                    ENDIF

                   
                    
                    IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) = 1.0
                      
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_HidePhoneInWater)
						AND (NETWORK_IS_ACTIVITY_SESSION() OR g_bFMMC_InTestMode_MissionController)
						
							HIDE_ACTIVE_PHONE(TRUE, TRUE)
							
							
							SET_BIT(BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
							
							
							#if IS_DEBUG_BUILD

	                            cdPrintstring("CELLPHONE_FH - Mission Option is enabled. Hiding Active Phone Instead of Hanging up. (Submerge Level is equal 1.0 See bug 6456016 (1)")   
	                            cdPrintnl()
	                        
	                        #endif
							
						ELSE
							
							g_B_ForcedAway = TRUE

	                        Check_For_Ongoing_Call_Interruption()

	                        HANG_UP_AND_PUT_AWAY_PHONE()

	                        #if IS_DEBUG_BUILD

	                            cdPrintstring("CELLPHONE_FH - Terminating SP and MP mobile phone use as ped submerge level is equal to 1.0. See bug 1950752")   
	                            cdPrintnl()
	                        
	                        #endif
							
						ENDIF
						
					ENDIF
					
                ENDIF



                IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)

                    IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_MICHAEL].game_model
                    OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_FRANKLIN].game_model
                    OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_TREVOR].game_model
                        
                        b_ShouldDisableDirModeVehicleCamForThisActor = FALSE

                    ELSE

                        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())   //2410401 - Prevent Snapmatic use in a vehicle for non story character peds in Director Mode.
                            b_ShouldDisableDirModeVehicleCamForThisActor = TRUE
                        ENDIF

                    ENDIF

                ENDIF

                
                IF IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
                OR IS_PLAYER_RIDING_TRAIN(PLAYER_ID())
                OR IS_PED_ON_MOUNT (PLAYER_PED_ID()) //Don't allow camera use on horseback.
                OR IS_STUNT_JUMP_IN_PROGRESS()
                OR IS_PLAYER_CLIMBING (PLAYER_ID())
                OR IS_PED_JACKING(PLAYER_PED_ID())
                OR IS_PED_IN_COVER(PLAYER_PED_ID())
                OR IS_PED_HANGING_ON_TO_VEHICLE(PLAYER_PED_ID()) //1950639
                OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("Michael1")) > 0
                OR g_bResultScreenPrepared = TRUE //2115760
                #if USE_TU_CHANGES
                    OR Is_Ped_Drunk (PLAYER_PED_ID()) //from drunk_public.sch See bug 1712545
                    OR IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
                    OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
                    OR GET_USINGNIGHTVISION()
                    OR b_ShouldDisableDirModeVehicleCamForThisActor
                #endif

                    


                    

                    #if IS_DEBUG_BUILD
                        IF g_DumpDisableEveryFrameCaller

                            cdPrintnl()
                            cdPrintstring("CELLPHONE_FH - No camera use in this scenario. See Bug 1336898.")
                            cdPrintnl()   


                            IF g_bResultScreenPrepared = TRUE
                                
                                cdPrintnl()
                                cdPrintstring("CELLPHONE_FH - No camera use as g_bResultScreenPrepared is TRUE. See Bug 2115760.")
                                cdPrintnl()   

                            ENDIF

                        ENDIF
                    #endif
                   
                    IF NOT g_Use_Prologue_Cellphone
                    
                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                    ENDIF

                ENDIF


                //Adding in an Enter / Exit vehicle timer mechanism to prevent 3rd person problem when opening Snapmatic immediately after an enter / exit.
                IF IS_PC_VERSION() //2269787
                    IF g_bInMultiplayer = FALSE
                
                        IF JustEnteredExitVehicle = FALSE

                            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                                IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
                    
                                    //SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
                                    JustEnteredExitVehicle_StartTime = GET_GAME_TIMER()//2269787
                                    JustEnteredExitVehicle = TRUE   

                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("Cellphone_flashhand - PC Only. Player just entered or exited vehicle. Starting timer for Snapmatic.")
                                        cdPrintnl()   
                                    #endif
                    
                                ENDIF
                            ENDIF

                        ELSE

                    
                            JustEnteredExitVehicle_CurrentTime = GET_GAME_TIMER()
                            i_TimeDifference = JustEnteredExitVehicle_CurrentTime - JustEnteredExitVehicle_StartTime

                            IF i_TimeDifference < 4000

                                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_flashhand - PC Only. Player just entered or exited vehicle. Timer block ongoing.")
                                    cdPrintnl()   
                                #endif


                            ELSE

                                JustEnteredExitVehicle = FALSE

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("Cellphone_flashhand - PC Only. Player just entered or exited vehicle. Clearing timer and Entered_Exit bool.")
                                    cdPrintnl()   
                                #endif


                            ENDIF

                        ENDIF

                    ENDIF
                ENDIF
      

      
                //2156297 - Camera use is prevented when aiming any kind of weapon... grey out icon in that case.        
                IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM) 
                OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)

                    IF g_bInMultiplayer

                        GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), fh_TempWeapon)
            
                        IF fh_TempWeapon <> WEAPONTYPE_UNARMED
                        AND g_Cellphone.PhoneDS < PDS_RUNNINGAPP

                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                        ENDIF

                    ENDIF

                ENDIF









                IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())


                    //Cheaper to have these clauses separated rather than a big compound if.

                    tempFH_VehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID()) //Any additions to the model list below should be cloned in appCamera's abort list as a failsafe.

                    
                    /* //Bicycle check removed again, thanks to #1333593

                    IF (IS_THIS_MODEL_A_BICYCLE (GET_ENTITY_MODEL (tempFH_VehicleIndex)))

                        IF g_bInMultiplayer = FALSE

                            #if IS_DEBUG_BUILD
                                
                                cdprintstring("CELLPHONE_FH - Phone up when on bicycle in SP. Aborting!")
                                cdprintnl()

                            #endif


                            HANG_UP_AND_PUT_AWAY_PHONE()

                        ENDIF

                    ENDIF
                    */                    
                    
                    IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = SUBMERSIBLE //For  #1291886
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = SUBMERSIBLE2    //For  #2050088

                        b_inSubmersible = TRUE

                    ELSE

                        b_inSubmersible = FALSE

                    ENDIF


                    IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = RHINO           //no camera use in tank. See bug 394784
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = CUTTER          //#1165672
					
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = CHERNOBOG //Added this check for 4199535 - No Snapmatic use in any seat in Chernobog. Front looks ropey, back decidedly dodgy!
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = KHANJALI  //url:bugstar:4692191
					
                    OR b_inSubmersible                                          //no camera use in submersible. See bug #1126212
                    OR (IS_VEHICLE_DRIVEABLE(tempFH_VehicleIndex) = FALSE)      //#1950567
                    
                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                    ENDIF





                    IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = VALKYRIE    //Fix for 2015231 - no Snapmatic use in turret seat.
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = INSURGENT   //Fix for 2069655 - no Snapmatic use in turret seat for this one either. 
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TECHNICAL   //Fix for 2079585 - no Snapmatic use when using the truckbed gun of this one.
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRASH       //Fix for 2079577 - no Snapmatic use when hanging off back of this vehicle.
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRASH2      //Same as directly above.
                    OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = LIMO2       //Added this check for 2578841 - No Snapmatic use in Limo Turret.
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = INSURGENT3       //Added this check for 3523983 - No Snapmatic use in INSURGENT3
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = HALFTRACK       //Added this check for 3524009 - No Snapmatic use in HALFTRACK
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = DUNE3       //Added this check for 3527081 - No Snapmatic use in DUNE3
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRAILERSMALL2       //Added this check for 3530733 - No Snapmatic use in TRAILERSMALL2
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TECHNICAL2  //Added this check for 3235690 - No Snapmatic use in Technical2 Turret.
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = BOXVILLE5   //Added this check for 3235690 - No Snapmatic use in Boxville5 Turret.
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = MOGUL //Added this check for 3781452 - No Snapmatic use in Mogul Turret.
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = BOMBUSHKA //Added this check for pre-emptive strike on 3781452
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TULA //Added this check for pre-emptive strike on 3781452
					OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = CHERNOBOG //Added this check for 4199535 - No Snapmatic use in Chernobog.
					
                        IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), tempFH_VehicleIndex)

                            //VEHICLE_SEAT tempVehicleSeat = GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), tempFH_VehicleIndex) 
                            VEHICLE_SEAT tempVehicleSeat = GetSeatForTurretCheck(PLAYER_PED_ID(), tempFH_VehicleIndex) 
                            
                            /* Enum for reference.
                            ENUM VEHICLE_SEAT
                                VS_ANY_PASSENGER = -2,  //Any passenger seat
                                VS_DRIVER = -1,         //Drivers seat
                                VS_FRONT_RIGHT = 0,     //Front Right seat
                                VS_BACK_LEFT,           //Back left     
                                VS_BACK_RIGHT,          //Back right
                                VS_EXTRA_LEFT_1,
                                VS_EXTRA_RIGHT_1,
                                VS_EXTRA_LEFT_2,
                                VS_EXTRA_RIGHT_2,
                                VS_EXTRA_LEFT_3,
                                VS_EXTRA_RIGHT_3    
                            ENDENUM
                            */
                            
                            #if IS_DEBUG_BUILD
                                cdPrintstring("Cellphone_Flashhand - Returned player seat enum from Valkyrie Insurgent Technical Limo2 Other was ")
                                cdPrintInt(ENUM_TO_INT(tempVehicleSeat))
                                cdPrintnl()
                            #endif       
                            
         
                            IF tempVehicleSeat <> VS_ANY_PASSENGER

                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = INSURGENT
                                OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = LIMO2 //Added this check for 2578841. Limo2 turret also returns 3 for the turret seat. Climb on top of the vehicle for easy access.

                                    IF ENUM_TO_INT (tempVehicleSeat) = 3

                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
              
                                     ENDIF

                                ENDIF
                    
                                //2079577
                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRASH
                                OR GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRASH2

                                    IF ENUM_TO_INT (tempVehicleSeat) = 2
                                    OR ENUM_TO_INT (tempVehicleSeat) = 1

                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

                                    ENDIF

                                ENDIF
								
								//3235690
                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = TECHNICAL2
                                    IF ENUM_TO_INT (tempVehicleSeat) = 1
                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
                                    ENDIF
                                ENDIF
								
                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = BOXVILLE5
                                    IF ENUM_TO_INT (tempVehicleSeat) = 3
                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
                                    ENDIF
                                ENDIF
								
								//3523983, 3527081 and 3524009. 
								//3530733.

						            IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = INSURGENT3
	                                    IF ENUM_TO_INT (tempVehicleSeat) = 3
	                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
	                                    ENDIF
	                                ENDIF
									
	                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = HALFTRACK
	                                    IF ENUM_TO_INT (tempVehicleSeat) = 1
	                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
	                                    ENDIF
	                                ENDIF
								
									
									IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = DUNE3
	                                    IF ENUM_TO_INT (tempVehicleSeat) = 0
	                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
	                                    ENDIF
	                                ENDIF
								
									IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = TRAILERSMALL2 //3530733
	                                    IF ENUM_TO_INT (tempVehicleSeat) = -1
	                                        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)
	                                    ENDIF
	                                ENDIF
									
								
								
								
                                IF IS_TURRET_SEAT(tempFH_VehicleIndex, tempVehicleSeat)  //Doesn't seem to work for the Insurgent.
                                    
                                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME) 
                                    //Disables and greys out Snapmatic homescreen icon.

                                    /*
                                    #if IS_DEBUG_BUILD
                                        cdPrintstring("Cellphone_Flashhand - Turret Seat returning true for this vehicle seat.")
                                        cdPrintnl()
                                    #endif 
                                    */
                      
                                ENDIF
                            ENDIF

                        ENDIF

                    ENDIF                   





                    IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_VEH_ATTACK)  //This also acts as accelerate in some configurations, so can't be checked if in a vehicle.
                    
                        IF g_bInMultiplayer = FALSE //Phone is not in hand in multiplayer so this allows simultaneous firing and phone use. SP checks required only.
                            IF g_Cellphone.PhoneDS = PDS_MAXIMUM
                            OR g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                                BOOL OK_to_abort_Phone = TRUE


                                //Potential fix for Mike W. #1770981
                                IF GET_ENTITY_MODEL (tempFH_VehicleIndex) = (INT_TO_ENUM(MODEL_NAMES, HASH("STROMBERG")))
                                AND IS_ENTITY_IN_WATER (tempFH_VehicleIndex)
                                
                                    OK_to_abort_Phone = FALSE
                                                                   
                                ENDIF 




                                IF (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL (tempFH_VehicleIndex)))
                                OR (IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL (tempFH_VehicleIndex)))
                                OR (GET_ENTITY_MODEL (tempFH_VehicleIndex) = SUBMERSIBLE)
                                OR (GET_ENTITY_MODEL (tempFH_VehicleIndex) = SUBMERSIBLE2)

                                    OK_to_abort_Phone = FALSE
                                
                                ENDIF
                                
                                
                                                          
                                    IF OK_to_abort_Phone 

                                        #if IS_DEBUG_BUILD
                                        
                                            cdprintstring("CELLPHONE_FH - Phone in running app or maximum state. In vehicle player attack instruction from pad takes precedence, moving phone away. LB TODO #1235429")
                                            cdprintnl()

                                        #endif


                                        HANG_UP_AND_PUT_AWAY_PHONE()

                                    ENDIF


                            ENDIF
                        ENDIF

                    ENDIF



                ELSE


                    IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_ATTACK)  //This also acts as accelerate in some configurations, so can't be checked if in a vehicle.
                    
                        IF g_bInMultiplayer = FALSE //Phone is not in hand in multiplayer so this allows simultaneous firing and phone use. SP checks required only.
                            IF g_Cellphone.PhoneDS = PDS_MAXIMUM
                            OR g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                                #if IS_DEBUG_BUILD
                                
                                    cdprintstring("CELLPHONE_FH - Phone in running app or maximum state. On foot player attack instruction from pad takes precedence, moving phone away. LB TODO #1235429")
                                    cdprintnl()

                                #endif

                                
                                HANG_UP_AND_PUT_AWAY_PHONE()


                            ENDIF
                        ENDIF

                    ENDIF


                    b_inSubmersible = FALSE

                ENDIF




				
				
				
                IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
                	
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_HidePhoneInWater)
					AND (NETWORK_IS_ACTIVITY_SESSION() OR g_bFMMC_InTestMode_MissionController)
					
						HIDE_ACTIVE_PHONE(TRUE, TRUE)
						
						#if IS_DEBUG_BUILD

	                        cdPrintstring("CELLPHONE_FH - Mission Option is enabled. Hiding Active Phone Instead of Hanging up. (Submerge Level is equal 1.0 See bug 6456016 (2)")   
	                        cdPrintnl()
	                    
	                    #endif
						
					ELSE
											
	                    g_B_ForcedAway = TRUE

	                    Check_For_Ongoing_Call_Interruption()

	                    HANG_UP_AND_PUT_AWAY_PHONE()

	                    #if IS_DEBUG_BUILD

	                        cdPrintstring("CELLPHONE_FH - Terminating SP and MP mobile phone use as ped is swimming under water. See bug 990085 and 1018500")   
	                        cdPrintnl()
	                    
	                    #endif
					
					ENDIF
					
                ENDIF






                IF g_bInMultiplayer = FALSE

                    IF GET_ENTITY_HEALTH (PLAYER_PED_ID()) < Player_PeriodicHealth
                        
                        IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
                       
                            g_B_ForcedAway = TRUE

                            Check_For_Ongoing_Call_Interruption()

                            HANG_UP_AND_PUT_AWAY_PHONE()

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - Damage sustained by player outside of vehicle, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                                cdPrintnl()
                            
                            #endif

                        ENDIF

                    ENDIF


                    //Superseded by new water check above but leaving in for reference in the meantime. Runs in SP only.
                    IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL
                    OR g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT 
                        IF IS_PED_SWIMMING_UNDER_WATER (PLAYER_PED_ID())
                        
                            g_B_ForcedAway = TRUE

                            Check_For_Ongoing_Call_Interruption()

                            HANG_UP_AND_PUT_AWAY_PHONE()

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - Terminating SP mobile phone call swimming in water. See bug 907295.")   
                                cdPrintnl()
                            
                            #endif

                        ENDIF
                    ENDIF


                    /* Removed by request of 801233.
                    IF IS_SPECIAL_ABILITY_ACTIVE (PLAYER_ID()) //Asserts if checked in multiplayer.

                        g_B_ForcedAway = TRUE

                        Check_For_Ongoing_Call_Interruption()

                        HANG_UP_AND_PUT_AWAY_PHONE()


                        #if IS_DEBUG_BUILD
                    
                            cdPrintstring ("CELLPHONE_FH - Phone somehow active during special ability, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                            cdPrintnl ()

                        #endif

                    ENDIF
                    */


                ELSE

                     IF IS_CELLPHONE_CAMERA_IN_USE() //Special damage during camera use check for 1253746
               
                         IF GET_ENTITY_HEALTH (PLAYER_PED_ID()) < Player_PeriodicHealth
                            
                            IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
                           
                                g_B_ForcedAway = TRUE

                                Check_For_Ongoing_Call_Interruption()

                                HANG_UP_AND_PUT_AWAY_PHONE()

                                #if IS_DEBUG_BUILD

                                    cdPrintstring("CELLPHONE_FH - Damage sustained by player outside of vehicle during MP camera use, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                                    cdPrintnl()
                                
                                #endif

                            ENDIF

                        ENDIF

                    ENDIF




                ENDIF //end of g_bInMultiplayer = FALSE condition check


                
                IF IS_AIM_CAM_ACTIVE()  //fix for 807395. Abort the phone is the sniper aim cam is active.

                    IF NOT (IS_PED_IN_COVER (PLAYER_PED_ID(), TRUE)) //potential fix for #1069548, aim cam is deemed active in MP if the ped is in cover with the sniper rifle.

                        IF NOT (IS_CELLPHONE_CAMERA_IN_USE()) //potential fix for #1130708 - the first person viewfinder camera used in photo taking counts as AIM_CAM also.
                                                              //We don't want to abort the camera if the player has a sniper rifle then starts the app, so only abort the phone if the
                                                              //camera script is not running.

                            GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), fh_TempWeapon)

                            IF fh_TempWeapon = WEAPONTYPE_SNIPERRIFLE
                            OR fh_TempWeapon = WEAPONTYPE_HEAVYSNIPER
                            OR fh_TempWeapon = WEAPONTYPE_REMOTESNIPER

                                b_Sniper_equipped = TRUE

                            ELSE

                                b_Sniper_equipped = FALSE

                            ENDIF
                      

                            IF IS_FIRST_PERSON_AIM_CAM_ACTIVE() //Added by request of Blair in #2009400 

                                IF b_Sniper_equipped 

                                    g_B_ForcedAway = TRUE

                                    Check_For_Ongoing_Call_Interruption()

                                    HANG_UP_AND_PUT_AWAY_PHONE()

                                    #if IS_DEBUG_BUILD

                                        cdPrintstring("CELLPHONE_FH - sniper fp aim cam is active at same time as hud phone, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                                        cdPrintnl()
                                    
                                    #endif

                                ENDIF

                            ENDIF

                        ENDIF

                    ENDIF


                ENDIF



                IF IS_PED_BEING_JACKED (PLAYER_PED_ID())

                    g_B_ForcedAway = TRUE

                    Check_For_Ongoing_Call_Interruption()

                    HANG_UP_AND_PUT_AWAY_PHONE()

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - Player has been jacked, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                        cdPrintnl()
                    
                    #endif

                ENDIF



                IF IS_PED_BEING_STUNNED (PLAYER_PED_ID())

                    g_B_ForcedAway = TRUE

                    Check_For_Ongoing_Call_Interruption()

                    HANG_UP_AND_PUT_AWAY_PHONE()

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - Player has been stunned, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                        cdPrintnl()
                    
                    #endif


                ENDIF



                IF IS_PLAYER_BEING_ARRESTED (PLAYER_ID())


                    g_B_ForcedAway = TRUE

                    Check_For_Ongoing_Call_Interruption()

                    HANG_UP_AND_PUT_AWAY_PHONE()

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - Player has been arrested, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                        cdPrintnl()
                    
                    #endif

                 ENDIF


                 IF IS_PED_RAGDOLL (PLAYER_PED_ID())
                 OR IS_PED_IN_PARACHUTE_FREE_FALL (PLAYER_PED_ID())   //#1392720

                    IF g_bInMultiplayer = FALSE //requested by Andrew Duthie in bug 519214.


                        g_B_ForcedAway = TRUE

                        Check_For_Ongoing_Call_Interruption()

                        HANG_UP_AND_PUT_AWAY_PHONE()

                        #if IS_DEBUG_BUILD

                            cdPrintstring("CELLPHONE_FH - Player is ragdolling or freefalling, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                            cdPrintnl()
                        
                        #endif

                    ENDIF

                 ENDIF
            


                //Submerged vehicle handling.
                /*Removed - see bug 109069
                IF IS_PED_IN_ANY_VEHICLE (PLAYER_PED_ID())


                    Cellphone_CurrentPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

                    IF IS_ENTITY_IN_WATER (Cellphone_CurrentPlayerVehicle)

                        Check_For_Ongoing_Call_Interruption()

                        HANG_UP_AND_PUT_AWAY_PHONE()

                        #if IS_DEBUG_BUILD

                            cdPrintstring("Player in submerging car, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                            cdPrintnl()
                        
                        #endif


                    ENDIF
                    

                ENDIF
                */




                //Update 25.11.11 -  by request of TODO 293726 this check has been removed.
                //Franklin has a waterproof phone, multiplayer allows it!
                /*
                IF NOT g_bInMultiplayer 

                    IF NOT (g_Cellphone.PhoneOwner = CHAR_FRANKLIN)
                        IF IS_ENTITY_IN_WATER (PLAYER_PED_ID())
                        
                            g_B_ForcedAway = TRUE

                            Check_For_Ongoing_Call_Interruption()

                            HANG_UP_AND_PUT_AWAY_PHONE()

                            #if IS_DEBUG_BUILD

                                cdPrintstring("CELLPHONE_FH - Player character in water but does not have waterproof phone, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                                cdPrintnl()
                            
                            #endif


                        ENDIF
                    ENDIF

                ENDIF
                */

            ELSE

                
                BOOL b_ShouldKeepPhoneActiveOnDeathArrest = FALSE

                IF g_bInMultiplayer 
                AND voiceSession.bInVoiceSession 
                AND voiceSession.bConnectedCallOnGoing
                AND g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                    b_ShouldKeepPhoneActiveOnDeathArrest = TRUE

                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - Failsafe: Player is arrested or dead, flashhand has detected the player is in MP and in a connected voice session. Preserving phone.")
                        cdPrintnl()
                
                    #endif



                    // Could remove this block for Next Gen as vanishing the phone causes great confusion in first person.
                    IF NOT (IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE)) //Don't want to check this when phone is in transit...

                        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)    //Also need to make sure phone has actually been vanished once before reinstating it... 
                                            
           
                            #if IS_DEBUG_BUILD
                            
                                cdPrintstring ("CELLPHONE_FH - Vanished phone ordered to re-appear due to death arrest during cellphone chat call.")
                                cdPrintnl()

                            #endif                                  

                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)     //signal that we want to run transit routine now.
                            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)    //phone no longer vanished.
                        
                            CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

                       
                        ENDIF

                    ENDIF


                ENDIF


                IF b_ShouldKeepPhoneActiveOnDeathArrest = FALSE //Trial additions for TODO 1643537

                    g_B_ForcedAway = TRUE

                    Check_For_Ongoing_Call_Interruption()

                    HANG_UP_AND_PUT_AWAY_PHONE()
     
                    #if IS_DEBUG_BUILD

                        cdPrintstring("CELLPHONE_FH - Failsafe: Player is arrested or dead, flashhand calls HANG_UP_AND_PUT_AWAY_PHONE")
                        cdPrintnl()
                
                    #endif

                ENDIF


            ENDIF


        ENDIF





        IF g_InputButtonJustPressed = TRUE

            Check_For_Just_Pressed_Revert()

        ENDIF





        //Special case for force answer and bug 29540
        //This will call this every cycle until the call is cleaned up - may add a global or something to prevent this, although it causes no adverse effects 
        //for a ringing tone. It has to be done here not in the more obvious dialogue_handler due to the manner in which the sound ids are tied to a script.
        IF g_InboundCallWaitAccRej = CALL_ACCEPTED
        
            // B*2205053
            IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_POSITIVE_INPUT)))
                SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
            ENDIF

            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //STOP_SOUND (g_Phone_Ringtone_SoundID) ////#1392094 swap
                STOP_PED_RINGTONE (PLAYER_PED_ID())
            
            ENDIF

        ENDIF

                

                                                      
        #if IS_DEBUG_BUILD


       
            
            //draw_debug_screen_grid()
            //Uncomment to spam phone state or safety buffer for tricky bugs.
            /*
                cdPrintstring ("Phone_state = ")
                cdPrintint(ENUM_TO_INT (g_Cellphone.PhoneDS))
                cdPrintnl()
            */


            
            /* 
                cdPrintstring ("Cellphone SafetyBuffer Elapsed Duration = ")
                cdPrintint(g_App_Elapsed_SafetyBuffer)
                cdPrintnl()
                cdPrintnl()


                cdPrintstring ("START TIME Cellphone SafetyBuffer = ")
                cdPrintint(g_App_SafetyBuffer_StartTime)
                cdPrintnl()
                cdPrintnl()
            */


                   
                GET_MOBILE_PHONE_ROTATION (v_DebugRotation)
                
                //DISPLAY_TEXT_WITH_LITERAL_STRING (0.60, 0.30, "STRING", "Current phone rotation")
                //DISPLAY_TEXT_WITH_FLOAT (0.6, 0.35, "CELL_500", v_DebugRotation.X, 2)
                //DISPLAY_TEXT_WITH_FLOAT (0.6, 0.40, "CELL_501", v_DebugRotation.Y, 2)
                //DISPLAY_TEXT_WITH_FLOAT (0.6, 0.45, "CELL_502", v_DebugRotation.Z, 2)


                /*
                IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_0)

                    IF testSignalInt < 4
                        testSignalInt ++
                    ELSE
                        testSignalInt = 0
                    ENDIF

                ENDIF


                DISPLAY_TEXT_WITH_NUMBER (0.20, 0.45, "CELL_500", i_Affluence)
                */


        #endif




    ENDWHILE



    
ENDSCRIPT


