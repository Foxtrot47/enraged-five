

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"




  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appHS_Sleep.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Uses the basis of appSettings, but has some bypasses and checks
//                      :   which allows me to place a forced setting selection as a 
//                      :   dedicated app on the homescreen.
//                          
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           

CONST_INT MaxNumberOfSettingSlots 15

INT SettingListSlot[MaxNumberOfSettingSlots]

INT NumberOfSettingsInList = 0

INT ListCursorIndex = 0
SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex

INT SettingNamesToDraw = 0

INT i_Selected_Setting

BOOL b_NeedtoForceSetting = FALSE








//Temp draw..........
FLOAT listOriginX = 0.82
FLOAT listOriginY = 0.42


FLOAT drawItemX = 0.0
FLOAT drawItemY = 0.0

BOOL dpad_scroll_pause_cued = FALSE
































PROC Place_Names_in_Secondary_SettingSlots()


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1) //Set enum state and header to Homescreen briefly to flush view state data


    Get_Cellphone_Owner()


    NumberOfSettingsInList = 0 //reset number of Settings before filling list.

    BOOL Secondary_Setting_name_is_in_order[MaxNumberOfSettingSlots]

    INT slotIndex = 0

    WHILE slotIndex < (ENUM_TO_INT(MAX_SETTINGS))  


        
        #IF IS_DEBUG_BUILD
        /*
            PRINTSTRING("Populating secondary sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
        */
        #endif         


        INT comparisonIndex = 0

        INT top_order_comparison = (ENUM_TO_INT(MAX_SETTINGS))

    
        
        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison] = 5000
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison] = 5000

        

        WHILE comparisonIndex < (ENUM_TO_INT(MAX_SETTINGS))

            /*
            PRINTSTRING("comparison index =")
            PRINTINT(comparisonIndex)
            PRINTNL()
            */

            //g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE



            //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE
            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE
            

            //IF g_SettingList[comparisonIndex].phoneGuiPresence = AVAILABLE_IN_GUI
                
                
                IF Secondary_Setting_name_is_in_order[comparisonIndex] = FALSE


                        
                        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[comparisonIndex]
                             < This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison]
                        //IF g_SettingList[comparisonIndex].Setting_orderInt <  g_SettingList[top_order_comparison].Setting_orderInt
                        
                            top_order_comparison = comparisonIndex

                            /*
                            PRINTSTRING("top comparison assigned to comparisonindex")
                            PRINTINT(comparisonIndex)
                            PRINTNL()
                            */

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(slotIndex)), 
                            (TO_FLOAT(This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Icon_Int[top_order_comparison])),
                             INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                             This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[top_order_comparison]) 
                                                                                                                                                                     

                        ENDIF

                ENDIF
            ENDIF

            comparisonIndex ++

        ENDWHILE

        
        


        SettingListSlot[slotIndex] = top_order_comparison
        
        Secondary_Setting_name_is_in_order[top_order_comparison] = TRUE
        



        //IF g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[slotIndex] = TRUE
       IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[slotIndex] = TRUE

        
            NumberOfSettingsInList ++ 
       
            /*
            PRINTSTRING("NumSettings-")
            PRINTINT(NumberOfSettingsInList)
            PRINTNL()
            */

        ENDIF

    

        slotIndex ++

  

    ENDWHILE





    //Set scalefrom movie view state enum to Settings screen and display associated buttons.


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 13) //Set enum state and header to Settings

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", g_SettingList[i_Selected_Setting].Setting_Primary_Label)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", 
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_labels[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option])
    

    //Monochrome
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" Positive

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0, 
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
    */


    //Badger - Select Back combo
    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    ELSE
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



     
ENDPROC










PROC Place_Names_in_SettingSlots()


    NumberOfSettingsInList = 0 //reset number of Settings before filling list.

    BOOL Setting_name_is_in_order[MaxNumberOfSettingSlots]

    INT slotIndex = 0


    Get_Cellphone_Owner()


    WHILE slotIndex < (ENUM_TO_INT(MAX_SETTINGS))  


        #IF IS_DEBUG_BUILD
        /*
            PRINTSTRING("Populating sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
        */
        #endif                                            


        INT comparisonIndex = 0

        INT top_order_comparison = (ENUM_TO_INT(MAX_SETTINGS))

    
        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt = 5000
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt = 5000


        WHILE comparisonIndex < (ENUM_TO_INT(MAX_SETTINGS))

            /*
            PRINTSTRING("comparison index =")
            PRINTINT(comparisonIndex)
            PRINTNL()
            */


            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[comparisonIndex].phoneGuiPresence = AVAILABLE_IN_GUI
                IF Setting_name_is_in_order[comparisonIndex] = FALSE

                    IF comparisonIndex = ENUM_TO_INT (SETTING_PROFILE)  //add a specific check so only this one specific setting is included in the initial slot list.
                                                                            //you wouldn't see these others but better to be safe than sorry.
                        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[comparisonIndex].Setting_orderInt < 
                            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt

                            top_order_comparison = comparisonIndex

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(2)), (TO_FLOAT(slotIndex)),
                                 INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                 This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_Primary_Label) 
                            
                        ENDIF
                    
                    ENDIF

                ENDIF
            ENDIF

            comparisonIndex ++

        ENDWHILE

        
        


        SettingListSlot[slotIndex] = top_order_comparison
        
        Setting_name_is_in_order[top_order_comparison] = TRUE
        



        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI

        
            NumberOfSettingsInList ++ 
       
            /*
            PRINTSTRING("NumSettings-")
            PRINTINT(NumberOfSettingsInList)
            PRINTNL()
            */

        ENDIF

    

        slotIndex ++

  

    ENDWHILE





    //Set scalefrom movie view state enum to Settings screen and display associated buttons.

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 13) //Set enum state and header to Settings
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_16")

    //Monochrome
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" Positive

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
    */

    //Badger - Select Back combo
    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    ELSE
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)




     
ENDPROC










PROC Display_Settings_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


            
    SettingNamesToDraw = (NumberOfSettingsInList) 

    DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    
    
    Get_Cellphone_Owner()
    
    
    WHILE drawIndex < SettingNamesToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF


      
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[drawIndex].phoneGuiPresence = AVAILABLE_IN_GUI //would need to check individual owner phonebooks if this is required.
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[drawIndex].phoneGuiPresence = AVAILABLE_IN_GUI //would need to check individual owner phonebooks if this is required.
           
            DISPLAY_TEXT (drawItemX, drawItemY, This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[drawIndex].Setting_Primary_Label) //draw the name of the Setting by referring to the character's text label. 


                 
            drawItemY = drawItemY + 0.03 //Draw the next name slightly down the y-axis
        
        ENDIF

   
        drawIndex ++
       



    ENDWHILE


    


ENDPROC







//Temp - would be done in flash.
PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued

        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF
    ENDIF
	
	
	// PC Scrollwheel support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
		
			 IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()
		
		ENDIF
		
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
		
			ListCursorIndex ++
			
            IF ListCursorIndex = NumberOfSettingsInList 
                ListCursorIndex = 0
            ENDIF
			
            Call_Scaleform_Input_Keypress_Down()

		ENDIF
	
	ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfSettingsInList 

                ListCursorIndex = 0

            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

           
            
        ENDIF

    
    ENDIF

            
ENDPROC










PROC Check_For_Setting_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    //Bypass this
    //IF g_InputButtonJustPressed = FALSE
    //IF IS_BUTTON_JUST_PRESSED (PAD1, INT_TO_ENUM (PAD_BUTTON_NUMBER, PHONE_POSITIVE_INPUT)) //positive action - Setting selected.



        //Bypass this.
        //Play_Select_Beep()


        g_InputButtonJustPressed = TRUE


        //Bypass this...
        //i_Selected_Setting  =  ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

        //...force selection of slot 0 contents - which must be SETTING_PROFILE.
        i_Selected_Setting = ENUM_TO_INT(SETTING_PROFILE)


    
        #IF IS_DEBUG_BUILD

            SWITCH i_Selected_Setting 

                
                CASE 0

                    PRINTSTRING ("PROFILE has been force selected from appDediSleep")

                BREAK


                DEFAULT

                    PRINTSTRING ("Something strange has been selected from appDediSleep")

                BREAK


              

            ENDSWITCH


            PRINTNL()


        #endif


        //Bypass this - we want this dedicated limited app to remain in simple RUNNINGAPP state.
        //g_Cellphone.PhoneDS = PDS_COMPLEXAPP

        //Do this instead to force PDS_RUNNINGAPP state below to run different post-force procs.
        b_NeedtoForceSetting = FALSE
    
        



        Place_Names_in_Secondary_SettingSlots()

        

     

    //Bypass these...
    //ENDIF         
    //ENDIF


ENDPROC














PROC Check_For_Secondary_Setting_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    
    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - Setting selected.


        Play_Select_Beep()

        g_InputButtonJustPressed = TRUE


        Get_Cellphone_Owner()



        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])
        
        //This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

        
        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
        Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
        
            WAIT (0)

            #if IS_DEBUG_BUILD
                 cdPrintstring("appHS_Sleep - Waiting on return value from scaleform. 201")
                cdPrintnl()
            #endif
            
        ENDWHILE



        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option =
            ENUM_TO_INT (SettingListSlot[GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)])





        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_labels[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option])



        //If phone is in sleep mode, show mini signifier in top portion of screen, otherwise switch it off.
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 1)
        
        ELSE
            
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 0)   

        ENDIF




        //If the player is on a mission and has perhaps started it with the phone already on the settings list, make sure that SLEEP mode cannot persist by returning to normal mode
        IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
            SET_CELLPHONE_PROFILE_TO_NORMAL() //from cellphone_public.sch
        ENDIF 




    ENDIF         
    ENDIF


ENDPROC






























PROC Cleanup_and_Terminate()



    Restore_Special_Case_Settings()  //from cellphone_private.sch



    //If the player is on a mission and has perhaps started it with the phone already on the settings list, make sure that SLEEP mode cannot persist by returning to normal mode
    IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
        SET_CELLPHONE_PROFILE_TO_NORMAL() //from cellphone_public.sch
    ENDIF 





    TERMINATE_THIS_THREAD()


ENDPROC 











SCRIPT


    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


    Get_Cellphone_Owner()



    //Fill_All_Settings() Moved to charsheet_initial.sc


    b_NeedtoForceSetting = TRUE




    Temporarily_Remove_Special_Case_Settings()  //from cellphone_private.sch





    Place_Names_in_SettingSlots()






    
    WHILE TRUE

        WAIT(0)
        





        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.

            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP

                    //Scaleform transition
                    //Display_Settings_List()
                                             
                    Check_for_List_Navigation()                                      
                                             
                    
                    //Bypass this after initial force setting loop has been done...
                    IF b_NeedtoForceSetting
                    
                        Check_For_Setting_Selection()

                    ELSE

                        //...check for secondary selection instead,
                        Check_For_Secondary_Setting_Selection()
                    
                    ENDIF

                    

                BREAK

               

                CASE PDS_COMPLEXAPP


                    Check_for_List_Navigation()

                    Check_For_Secondary_Setting_Selection()
                    

                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                    

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1) //Set enum state and header to Homescreen briefly to flush view state data

                        Place_Names_in_SettingSlots()


                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.


                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP
                            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 19. appHS_Sleep assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif


                        ENDIF

                    ENDIF

                BREAK
               




                DEFAULT

                    #IF IS_DEBUG_BUILD

                        PRINTSTRING("AppSettings in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  



        





       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Setting" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Settings list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Settings list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



        #IF IS_DEBUG_BUILD

            //draw_debug_screen_grid()

        #endif

  
            
    ENDWHILE
    
    
ENDSCRIPT


