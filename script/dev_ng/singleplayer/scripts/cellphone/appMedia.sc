

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "mission_repeat_public.sch"
USING "mission_stat_public.sch"



  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appMedia.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Prototype media app for displaying images on the cellphone.       
//                            
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           



SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex


INT i_MaxSavedPhotos, i_SF_SlotIndex, i_Selected_Media_Slot

BOOL b_Checking360Storage = FALSE

PHOTO_OPERATION_STATUS MediaPicProcessStatus 




INT AppMediaListSlot[25]
TEXT_LABEL_23 tl_MS_label[25]  //Currently limited at 20 by Graeme. Little bit of room built in for expansion.


BOOL dpad_scroll_pause_cued = FALSE




//Rotation Mechanics

VECTOR v_3dDesiredRotation, v_3dCameraphoneRotation


BOOL b_Rotate3dPhoneToLandscape = FALSE
BOOL b_Rotate3dPhoneToPortrait = FALSE

BOOL b_Rotate3dPhoneLSx, b_Rotate3dPhoneLSy, b_Rotate3dPhoneLSz

BOOL b_Rotate3dPhonePOx, b_Rotate3dPhonePOy, b_Rotate3dPhonePOz 



                                                                      





                                                                












PROC Initialise_Media_Slot_Labels()


    tl_MS_label[0] = "CELL_MSSLOT_1"
    tl_MS_label[1] = "CELL_MSSLOT_2"
    tl_MS_label[2] = "CELL_MSSLOT_3"
    tl_MS_label[3] = "CELL_MSSLOT_4"


    tl_MS_label[4] = "CELL_MSSLOT_5"
    tl_MS_label[5] = "CELL_MSSLOT_6"
    tl_MS_label[6] = "CELL_MSSLOT_7"
    tl_MS_label[7] = "CELL_MSSLOT_8"


    tl_MS_label[8] = "CELL_MSSLOT_9"
    tl_MS_label[9] = "CELL_MSSLOT_10"
    tl_MS_label[10] = "CELL_MSSLOT_11"
    tl_MS_label[11] = "CELL_MSSLOT_12"


    tl_MS_label[12] = "CELL_MSSLOT_13"
    tl_MS_label[13] = "CELL_MSSLOT_14"
    tl_MS_label[14] = "CELL_MSSLOT_15"
    tl_MS_label[15] = "CELL_MSSLOT_16"


    tl_MS_label[16] = "CELL_MSSLOT_17"
    tl_MS_label[17] = "CELL_MSSLOT_18"
    tl_MS_label[18] = "CELL_MSSLOT_19"
    tl_MS_label[19] = "CELL_MSSLOT_20"



ENDPROC






PROC Cleanup_and_Terminate()


    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("APPMEDIA - appMedia exiting and requesting pic tidy up.")
        PRINTNL()

    #endif


    g_b_appHasRequestedTidyUp = TRUE

    CLEAR_HELP()


    TERMINATE_THIS_THREAD()


ENDPROC 






PROC Check_for_List_Navigation() 
                      
    IF dpad_scroll_pause_cued
        IF TIMERA() > 50
            dpad_scroll_pause_cued = FALSE
        ENDIF
    ENDIF
	
	
	// PC Scrollwheel support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
		        
            Call_Scaleform_Input_Keypress_Up()
		
		ENDIF
		
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
					
            Call_Scaleform_Input_Keypress_Down()

		ENDIF
	
	ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
             
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF       
        

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))
         
            Call_Scaleform_Input_Keypress_Down()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

        ENDIF

    ENDIF   


ENDPROC






PROC SetUpRotateToLandScape()

    IF b_Rotate3dPhoneToPortrait = FALSE
        
        GET_MOBILE_PHONE_ROTATION(v_3dCameraphoneRotation)

        v_3dDesiredRotation = <<-90.3, -0.8, 90.0>> 


        b_Rotate3dPhoneToLandscape = TRUE
        //b_Rotate3dPhoneToPortrait = FALSE



        b_Rotate3dPhoneLSx = TRUE
        b_Rotate3dPhoneLSy = TRUE
        b_Rotate3dPhoneLSz = TRUE

    ENDIF


ENDPROC





PROC RotateToLandscape()


    IF (b_Rotate3dPhoneToLandscape = TRUE)
    AND (b_Rotate3dPhoneToPortrait = FALSE)
    AND (g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY) //Don't start rotating until image has loaded...
       

            IF b_Rotate3dPhoneLSx
                v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X - 1.0)
            ENDIF

            IF v_3dCameraphoneRotation.X < v_3dDesiredRotation.X //-110.6
            OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

                v_3dCameraphoneRotation.X = v_3dDesiredRotation.X 

                b_Rotate3dPhoneLSx = FALSE

            ENDIF


            IF b_Rotate3dPhoneLSy
                v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y - 0.5)
            ENDIF

            IF v_3dCameraphoneRotation.Y < v_3dDesiredRotation.Y //1.9
            OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9
            
                v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y 

                b_Rotate3dPhoneLSy = FALSE

            ENDIF


            IF b_Rotate3dPhoneLSz
                v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z + 7.0) //5.0      
            ENDIF

            IF v_3dCameraphoneRotation.Z > v_3dDesiredRotation.Z //87.9
            OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9

                v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z 
            
                b_Rotate3dPhoneLSz = FALSE

            ENDIF


     
        
        IF b_Rotate3dPhoneLSx = FALSE
        AND b_Rotate3dPhoneLSy = FALSE
        AND b_Rotate3dPhoneLSz = FALSE

            b_Rotate3dPhoneToLandscape = FALSE

            PRINT_HELP_FOREVER ("CELL_MSHELP_2")


        ENDIF


        
        SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 


    ENDIF



    //Need to check bit for failed loading of picture and display appropriate message.

    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LOAD_FROM_MEDIA_FAILED)

        //Set these to make sure complex app exit conditions are met.

        b_Rotate3dPhoneToLandscape = FALSE

        b_Rotate3dPhoneToPortrait = FALSE






        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18) 

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING 
            (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
            (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_MSMENU_3") //load failed!




        //Only display back button if load has failed.
        IF g_b_ToggleButtonLabels
      
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative


        ELSE 
        
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

        ENDIF

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)










        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) 
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", tl_MS_label[i_Selected_Media_Slot])




    ENDIF



ENDPROC








PROC Place_Saved_Photos_In_SF_Slots()


    //360 storage here.


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18) 

    i_MaxSavedPhotos = GET_MAXIMUM_NUMBER_OF_PHOTOS()


    #if IS_DEBUG_BUILD

        cdPrintstring ("APPMEDIA - Maximum number of photo save slots on media is ")
        cdPrintint (i_MaxSavedPhotos)
        cdPrintnl()

    #endif

    i_SF_SlotIndex = 0 

    INT i_SF_FilledSlotIndex = 0

    WHILE i_SF_SlotIndex < i_MaxSavedPhotos

        IF DOES_THIS_PHOTO_SLOT_CONTAIN_A_VALID_PHOTO (i_SF_SlotIndex) //Check media slot contains an actual photo save before filling that slot.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING 
                    (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(i_SF_FilledSlotIndex)),
                    (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, tl_MS_label[i_SF_SlotIndex])

          
            AppMediaListSlot[i_SF_FilledSlotIndex] = i_SF_SlotIndex

            i_SF_FilledSlotIndex ++


        ENDIF

        i_SF_SlotIndex ++

    ENDWHILE


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) 
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_MSMENU_1")


    IF g_b_ToggleButtonLabels
      
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative


    ELSE 
        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



ENDPROC












PROC InitialiseCheckStorage()



    IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
    OR IS_PC_VERSION()

        CLEAR_STATUS_OF_SORTED_LIST_OPERATION()

        IF QUEUE_OPERATION_TO_CREATE_SORTED_LIST_OF_PHOTOS (FALSE) //The last BOOL indicates that we are checking for a LOAD. This would be TRUE if we were saving.

            b_Checking360Storage = TRUE //Tell main loop to perform sorted list operation check.

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPMEDIA - Initial 360 Queue operation failed. Terminating appMedia script and forcing phone away")
                cdPrintnl()
                cdPrintString(" STATE ASSIGNMENT - AM1 - PDS_AWAY")
                cdPrintnl()

            #endif
        

            g_b_appHasRequestedTidyUp = TRUE

            g_CellPhone.PhoneDS =  PDS_AWAY

            Cleanup_and_Terminate()

        ENDIF

    ELSE    

        Place_Saved_Photos_In_SF_Slots()

    ENDIF 


ENDPROC












PROC SetUpRotateToPortrait()

    IF b_Rotate3dPhoneToLandscape = FALSE

        GET_MOBILE_PHONE_ROTATION(v_3dCameraphoneRotation)

        v_3dDesiredRotation = g_3dPhoneStartRotVec


        //b_Rotate3dPhoneToLandscape = FALSE
        b_Rotate3dPhoneToPortrait = TRUE

  
        b_Rotate3dPhonePOx = TRUE
        b_Rotate3dPhonePOy = TRUE
        b_Rotate3dPhonePOz = TRUE

    ENDIF


ENDPROC










PROC RotateToPortrait()


    IF (b_Rotate3dPhoneToLandscape = FALSE)
    AND (b_Rotate3dPhoneToPortrait = TRUE)
       

            IF b_Rotate3dPhonePOx
                v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X + 1.0)

            ENDIF

            IF v_3dCameraphoneRotation.X > v_3dDesiredRotation.X //-110.6
            OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

                v_3dCameraphoneRotation.X = v_3dDesiredRotation.X 

                b_Rotate3dPhonePOx = FALSE

            ENDIF




            IF b_Rotate3dPhonePOy
                v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y - 2.0)

            ENDIF

            IF v_3dCameraphoneRotation.Y < v_3dDesiredRotation.Y //1.9
            OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9

                v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y

                b_Rotate3dPhonePOy = FALSE

            ENDIF




            IF b_Rotate3dPhonePOz
                v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z - 7.0) //5.0)

            ENDIF

            IF v_3dCameraphoneRotation.Z < v_3dDesiredRotation.Z //87.9
            OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9

                v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z   
            
                b_Rotate3dPhonePOz = FALSE             

            ENDIF

     
        
        IF b_Rotate3dPhonePOx = FALSE
        AND b_Rotate3dPhonePOy = FALSE
        AND b_Rotate3dPhonePOz = FALSE


            b_Rotate3dPhoneToPortrait = FALSE


            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Rotation finished allow running app exit check.


          
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 21890. appMedia assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                 #endif

                          
                CLEAR_HELP()


                g_b_appHasRequestedTidyUp = TRUE    //This would stop drawing the pic to the screen eventually and return us to g_Cell_Pic_Stage = PIC_STAGE_IDLE
                                                    //This bool is checked and acted upon in cellphone_controller.


                InitialiseCheckStorage()



            ENDIF



        ENDIF


        
        SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 


    ENDIF


ENDPROC











/*
PROC Place_Saved_Photos_In_SF_Slots()


    //360 storage here.


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18) 

    i_MaxSavedPhotos = GET_MAXIMUM_NUMBER_OF_PHOTOS()


    #if IS_DEBUG_BUILD

        cdPrintstring ("APPMEDIA - Maximum number of photo save slots on media is ")
        cdPrintint (i_MaxSavedPhotos)
        cdPrintnl()

    #endif

    i_SF_SlotIndex = 0 

    INT i_SF_FilledSlotIndex = 0

    WHILE i_SF_SlotIndex < i_MaxSavedPhotos

        IF DOES_THIS_PHOTO_SLOT_CONTAIN_A_VALID_PHOTO (i_SF_SlotIndex) //Check media slot contains an actual photo save before filling that slot.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING 
                    (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(i_SF_FilledSlotIndex)),
                    (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, tl_MS_label[i_SF_SlotIndex])

          
            AppMediaListSlot[i_SF_FilledSlotIndex] = i_SF_SlotIndex

            i_SF_FilledSlotIndex ++


        ENDIF

        i_SF_SlotIndex ++

    ENDWHILE


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) 
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_MSMENU_1")


    IF g_b_ToggleButtonLabels
      
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative


    ELSE 
        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



ENDPROC

*/



PROC Check_For_Photo_Selection()

    
    IF g_InputButtonJustPressed = FALSE


            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) 


                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LOAD_FROM_MEDIA_FAILED)


                Play_Select_Beep()
                g_InputButtonJustPressed = TRUE
               
                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
               
                Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

                WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(Choice_ReturnedSFIndex)
                    WAIT (0)
                    CPRINTLN(DEBUG_REPEAT,"appMedia - Waiting on return value from scaleform.")   
                ENDWHILE

              
                INT i_TempCheck = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(Choice_ReturnedSFIndex)


                IF i_TempCheck > -1 //Make sure array overrun is prevented if no photos are present and scaleform returns -1.
                //Need to think about checking upper limit here once that's fully decided upon also.

                    i_Selected_Media_Slot = AppMediaListSlot[i_TempCheck]


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("APPMEDIA - Selected Media Slot was ")
                        cdPrintint (i_Selected_Media_Slot)
                        cdPrintstring (" which was in SF list slot ")
                        cdPrintint (i_TempCheck)
                        cdPrintnl()

                    #endif



                    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.


                        IF g_Cell_Pic_Stage = PIC_STAGE_IDLE

                   
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 18) 

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING 
                                (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_MSMENU_2")//Loading...




                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) 
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", tl_MS_label[i_Selected_Media_Slot])


                            //Don't display any soft keys during loading process...
                            IF g_b_ToggleButtonLabels
      
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative


                            ELSE 
                                
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                                            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative

                            ENDIF

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)














                            //Temp! Graeme, this critical section tells cellphone_controller.sc to start the independent loading routine.
                        
                            
                            g_LoadFromSlot = i_Selected_Media_Slot //Tell cellphone_controller which media slot to load, i.e the one on the hard drive.

                            g_Cell_Pic_Stage = PIC_STAGE_LOAD_FROM_MEDIA  //Signals that cellphone_controller should begin an attempted load.
                            
                            g_CellPhone.PhoneDS = PDS_COMPLEXAPP //Essentially puts this app in a state where the universal back button doesn't exit the app.


                            SetUpRotateToLandscape()

                            
                            //PRINT_HELP_FOREVER ("CELL_MSHELP_2")

                            //PRINT_HELP_FOREVER_WITH_NUMBER ("CELL_MSHELP_1", (g_LoadFromSlot + 1)) //adjust for sf slot zero being the first slot.
                        
                        ENDIF
                          
                          
                    ENDIF  

                ENDIF

        
            ENDIF    


    ENDIF 
    
    
    
    
    

ENDPROC













PROC Check360Storage()



    MediaPicProcessStatus = GET_STATUS_OF_SORTED_LIST_OPERATION(FALSE) //The last BOOL indicates that we are checking for a LOAD. This would be TRUE if we were saving.


        SWITCH MediaPicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED

            #if IS_DEBUG_BUILD

                cdPrintString("AppMEDIA - Sorted List Operation Succeeded!")
                cdPrintnl()

            #endif
            
            b_Checking360Storage = FALSE
            
            Place_Saved_Photos_In_SF_Slots()

        BREAK



       
        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("AppMEDIA - Sorting list operation in progress...")
                cdPrintnl()

            #endif


        BREAK

    
        
        CASE PHOTO_OPERATION_FAILED

            #if IS_DEBUG_BUILD

                cdPrintString("AppMEDIA - Sorted List Operation FAILED! Terminating appMedia and forcing phone away")
                cdPrintString("STATE ASSIGNMENT - AM2 - PDS_AWAY")
                cdPrintnl()

            #endif

            g_CellPhone.PhoneDS =  PDS_AWAY


            Cleanup_and_Terminate()
    
        BREAK


    ENDSWITCH



ENDPROC











SCRIPT



    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()




    Initialise_Media_Slot_Labels()




    InitialiseCheckStorage()
                                                          



    
    WHILE TRUE

        WAIT(0)
        

        
        IF b_Checking360Storage = FALSE
    
            IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
            
                SWITCH g_Cellphone.PhoneDS
                 
                

                    CASE PDS_RUNNINGAPP

                        IF b_Rotate3dPhoneToLandscape = FALSE
                        AND b_Rotate3dPhoneToPortrait = FALSE  
                        AND g_Cell_Pic_Stage = PIC_STAGE_IDLE             
                                                                                                      
                            Check_For_Photo_Selection()

                            Check_for_List_Navigation()

                        ENDIF

             
                    BREAK

                   

                    CASE PDS_COMPLEXAPP

                 



                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action
                        AND b_Rotate3dPhoneToLandscape = FALSE
                        AND b_Rotate3dPhoneToPortrait = FALSE 


                            Play_Back_Beep()                                     

                            g_InputButtonJustPressed = TRUE

                       
                            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LOAD_FROM_MEDIA_FAILED)


                                    #if IS_DEBUG_BUILD
                                        cdPrintnl()
                                        cdPrintstring("AppMedia - Load failed. Not Setting up portrait rotation.")
                                        cdPrintnl()   
                                    #endif

                                ENDIF

                              

                                    SetUpRotateToPortrait()

                                

//                                //g_Cellphone.PhoneDS = PDS_RUNNINGAPP - Moved to conclusion of portrait rotation so premature running app exit cannot take place.
//                            
//                                                
//                                #if IS_DEBUG_BUILD
//                                    cdPrintnl()
//                                    cdPrintstring("STATE ASSIGNMENT 21890. appMedia assigns PDS_RUNNINGAPP")
//                                    cdPrintnl()   
//                                #endif
//
//                                //InitialiseCheckStorage()
//
//                                CLEAR_HELP()
//
//
//                                g_b_appHasRequestedTidyUp = TRUE    //This would stop drawing the pic to the screen eventually and return us to g_Cell_Pic_Stage = PIC_STAGE_IDLE
//                                                                    //This bool is checked and acted upon in cellphone_controller.
//

                            ENDIF

                        ENDIF

                    BREAK
                   


                    DEFAULT

                        #if IS_DEBUG_BUILD

                            PRINTSTRING("appMedia in default state. Should terminate if exit check in place.")
                            PRINTNL()

                        #endif

                    BREAK



                ENDSWITCH

         
                //IF b_Rotate3dPhoneToLandscape = FALSE
                //AND b_Rotate3dPhoneToPortrait = FALSE
                    IF CHECK_FOR_APPLICATION_EXIT() 
                   
                        Cleanup_and_Terminate()
                   
                    ENDIF
                //ENDIF                  
                      



            
                IF b_Rotate3dPhoneToLandscape

                    RotateToLandscape()

                ENDIF


                
                IF b_Rotate3dPhoneToPortrait

                    RotateToPortrait()

                ENDIF




           
            ELSE



                //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
                //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

                BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                                //We don't really want to go back to the "calling Repeat" screen after an answerphone message so we set it to PDS_MAXIMUM
                                                //here. We could just as easily put the phone completely away if need be.
                                                //NB - This also means that when browsing the Repeats list, if a calls comes through and it terminates then we default
                                                //back to MAXIMUM mode not browsing the Repeats list.

                Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

                
              

            ENDIF

        

        ELSE


            Check360Storage()

        ENDIF




        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



  
            
    ENDWHILE
    
    
ENDSCRIPT

/*
[CELL_MSMENU_1]
Photos

[CELL_MSMENU_2]
Loading...

[CELL_MSMENU_3]
Load failed.

[CELL_MSHELP_1]
Displaying Slot ~1~ ~n~~n~Back ~INPUT_CELLPHONE_CANCEL~

[CELL_MSHELP_2]
Back ~INPUT_CELLPHONE_CANCEL~
*/
