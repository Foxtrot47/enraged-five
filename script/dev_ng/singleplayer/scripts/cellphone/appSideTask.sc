

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"




  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appSideTask.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Takes the array of live SideTasks stored in the global array
//                          and places them in the scaleform slots in chronological order.
//                          
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************








INT SideTaskListSlot[MAX_NUM_SIDETASKS]

INT NumberOfSideTasksInList = 0

INT ListCursorIndex = 0
INT SingleSideTaskIndex

INT SideTasksToDraw = 0





//enumPhonebookPresence PersonalPhonebookToCheck  //we need to check which character we are using and store which phonebook we should base the contacts list on.


//Temp
FLOAT listOriginX = 0.72
FLOAT listOriginY = 0.42


FLOAT drawItemX = 0.0
FLOAT drawItemY = 0.0

BOOL dpad_scroll_pause_cued = FALSE









 

PROC Cleanup_and_Terminate()


    TERMINATE_THIS_THREAD()


ENDPROC





PROC Place_Existing_SideTasks_In_Slots()


    NumberOfSideTasksInList = 0 //reset number of placed SideTasks before filling list.

    BOOL SideTask_in_chrono_order[MAX_NUM_SIDETASKS]

    INT slotIndex = 0
                                                         
    WHILE slotIndex < (MAX_NUM_SIDETASKS)


        INT comparisonIndex = 0

        INT top_time_comparison = (MAX_NUM_SIDETASKS - 1) //Dummy position



        //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text SideTask array.
        g_LiveSideTask[top_time_comparison].TxtMsgTimeSent.TxtMsgSecs = -1 
        g_LiveSideTask[top_time_comparison].TxtMsgTimeSent.TxtMsgMins = 0
        g_LiveSideTask[top_time_comparison].TxtMsgTimeSent.TxtMsgHours = 0
        g_LiveSideTask[top_time_comparison].TxtMsgTimeSent.TxtMsgDay = 0
        g_LiveSideTask[top_time_comparison].TxtMsgTimeSent.TxtMsgYear = 0

        


       
        /*
        g_Cellphone.PhoneOwner = GET_CURRENT_PLAYER_PED_ENUM()

        SWITCH g_Cellphone.PhoneOwner

            CASE CHAR_ARIANO

                PersonalPhonebookToCheck =  ARIANO_BOOK

            BREAK


            CASE CHAR_MICHAEL

                PersonalPhonebookToCheck =  MICHAEL_BOOK
              
            BREAK


            CASE CHAR_TREVOR

                 PersonalPhonebookToCheck =  TREVOR_BOOK

            BREAK


            DEFAULT

                #IF IS_DEBUG_BUILD

                    SCRIPT_ASSERT("The player character has not had his char_enum set. Steve T has put this assert in but someone else hasn't hot swapped correctly.")

                #endif
            
            BREAK

        ENDSWITCH
        */



        WHILE comparisonIndex < (MAX_NUM_SIDETASKS)
                //IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.

                    IF SideTask_in_chrono_order[comparisonIndex] = FALSE

                        IF  g_LiveSideTask[comparisonIndex].SideTaskLockStatus <> SIDETASK_EMPTY //make sure empty sidetasks aren't counted in this list.  
                                                                    
                            IF IsFirstTimeNewerThanSecondTime ( g_LiveSideTask[comparisonIndex].TxtMsgTimeSent,   g_LiveSideTask[top_time_comparison].TxtMsgTimeSent)
                        
                                top_time_comparison = comparisonIndex
                                               
                            ENDIF

                        ENDIF

                    ENDIF
               //ENDIF

            //ENDIF

            comparisonIndex ++

        ENDWHILE

        
    
        SideTaskListSlot[slotIndex] = top_time_comparison
        SideTask_in_chrono_order[top_time_comparison] = TRUE
        
            //IF (INT_TO_ENUM (enumCharacterList, slotIndex )) <> g_Cellphone.PhoneOwner   //don't let the phonebook holder include himself in the total number of contacts.


                IF  g_LiveSideTask[slotIndex].SideTaskLockStatus <> SIDETASK_EMPTY
                    NumberOfSideTasksInList ++ 
                ENDIF
                
            //ENDIF   
            
            /*
            #IF IS_DEBUG_BUILD
                PRINTSTRING("NumMessages")
                PRINTINT(NumberOfMessagesInList)
                PRINTNL()
            #endif
            */

        //ENDIF


    
        slotIndex ++

  

    ENDWHILE


    //Test
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_DATA_SLOT", 14, 0, 25,
            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_287", "CELL_280")



     
ENDPROC





PROC Draw_SingleSideTaskOptions_HelpText() 

    format_medium_ostext (0, 128, 255, 255) //Light blue //CALL
    DISPLAY_TEXT (0.750, 0.390, "CELL_201")


    //Need to grey out any locked messages as these cannot be deleted by the player.

    IF g_LiveSideTask[SingleSideTaskIndex].SideTaskLockStatus = SIDETASK_LOCKED
        format_medium_ostext (255, 255, 255, 55) //Greyed out //DELETE
    ELSE
        format_medium_ostext (0, 255, 0, 255) //Green //DELETE
    ENDIF
    
    
    DISPLAY_TEXT (0.790, 0.390, "CELL_216")

    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    DISPLAY_TEXT (0.847, 0.390, "CELL_206")

ENDPROC










PROC Display_Selected_SideTask()



    drawItemX = ListOriginX
    drawItemY = ListOriginY 

            

 
    format_medium_ostext (255, 255, 255, 205) //Semi transparent white

    highlight_Item()
       
    DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_LiveSideTask[SingleSideTaskIndex].SideTaskGiver].label) //draw the name of the contact by referring to the character's text label. 
    
    drawItemX = drawItemX + 0.07
    format_medium_ostext (255, 255, 255, 205)

    
    //Make sure we display a leading zero if the minutes are less than 10.
    IF g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgMins < 10
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgHours, g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgMins)
    ELSE
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgHours, g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgMins)
    ENDIF

    drawItemX = drawItemX + 0.07
    format_medium_ostext (255, 255, 255, 205)
    //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgYear)
    DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgDay, g_LiveSideTask[SingleSideTaskIndex].TxtMsgTimeSent.TxtMsgMonth)


    #IF IS_DEBUG_BUILD
        //Display the text label for debug purposes
        IF g_LiveSideTask[SingleSideTaskIndex].SideTaskLockStatus = SIDETASK_LOCKED
            format_medium_ostext (255, 0, 0, 95) //Semi transparent red
        ELSE
            format_medium_ostext (0, 255, 0, 95) //Semi transparent green
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskSynopsis)

    #endif

    drawItemX = ListOriginX

    format_medium_ostext (255, 255, 255, 205) //Semi transparent white

    drawItemY = drawItemY + 0.02

    IF g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskFullyComplete = FALSE
        highlight_Item()
    ENDIF

    SET_TEXT_WRAP (0.0, 0.93)
    DISPLAY_TEXT (drawItemX, drawItemY, g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskSynopsis) //the contexts of the text itself
              

    drawItemY = drawItemY + 0.07

    //Display portions of sidetask
    INT portionIndex

    FOR portionIndex = 1 TO g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].NumberOfSideTaskPortions
        
        //g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].NumberOfSideTaskPortions = 2

        
        format_medium_ostext (255, 255, 255, 255) //Semi transparent white

        drawItemY = drawItemY + 0.04

        

        IF g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskPortionTargetInt[PortionIndex] = -1 //Ambient guys must declare any unused target ints as -1/

            DISPLAY_TEXT (drawItemX, drawItemY, g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskPortionLabel[PortionIndex])

        ELSE

            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskPortionLabel[PortionIndex],
            g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskPortionCurrentInt[PortionIndex],
            g_AmbientSideTask[g_LiveSideTask[SingleSideTaskIndex].SideTaskIdentifier].SideTaskPortionTargetInt[PortionIndex])

        ENDIF

        //g_AmbientSideTask[Sidetask_1].SideTaskPortionLabel[1] = "CL_C1A_J1"

    ENDFOR



    //Delete this side task if not locked...
    /* Options disabled for sidetask

    IF g_LiveSideTask[SingleSideTaskIndex].SideTaskLockStatus = SIDETASK_UNLOCKED
        
        IF IS_BUTTON_PRESSED (PAD1, TRIANGLE) //macro!
            WHILE IS_BUTTON_PRESSED (PAD1, TRIANGLE)
                WAIT (0)                                
            ENDWHILE

            g_LiveSideTask[SingleSideTaskIndex].SideTaskLockStatus = SIDETASK_EMPTY

            #IF IS_DEBUG_BUILD
                                                       
                PRINTSTRING ("Player deleted txt msg array index ")
                PRINTINT (SingleSideTaskIndex)
                PRINTNL()
        
            #endif

            Place_Existing_SideTasks_In_Slots() //Update the slot list to reflect removal of deleted message.

            //For any list navigation cursor positions greater than zero, i.e the top position, we need to jump one up the list 
            //as deletion has occurred.
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF 

            g_DoOptionsForSingleMessage = FALSE //Return to full display list.

        ENDIF

     ENDIF



    //Call the sender of the message....

    IF IS_BUTTON_PRESSED (PAD1, CROSS) //macro!
        WHILE IS_BUTTON_PRESSED (PAD1, CROSS)
            WAIT (0)
        ENDWHILE

   
        //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
        request_load_scriptstring ("appContacts")

        START_NEW_SCRIPT ("appContacts", DEFAULT_STACK_SIZE)


        //Tell the global used by appContacts the giver name of this sidetask which can then be transposed to the character sheet.
        g_TheContactInvolvedinCall = ENUM_TO_INT (g_LiveSideTask[SingleSideTaskIndex].SideTaskGiver)

        g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT

       
        TERMINATE_THIS_THREAD ()

    ENDIF

    */
  




ENDPROC








PROC Display_SideTask_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


    //DRAW_RECT (0.824, 0.650, 0.238, 0.60, 0, 0, 0, 155)

            
    SideTasksToDraw = (NumberOfSideTasksInList) 

    //DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    WHILE drawIndex < SideTasksToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white          

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF                                                                                 
                                                                                
            
            DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_LiveSideTask[SideTaskListSlot[drawIndex]].SideTaskGiver].label) //draw the name of the contact by referring to the character's text label. 
            
            drawItemX = drawItemX + 0.07
            format_medium_ostext (255, 255, 255, 205)

            
            //Make sure we display a leading zero if the minutes are less than 10.
            IF g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins < 10
                DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgHours, g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins)
            ELSE
                DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgHours, g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMins)
            ENDIF

            drawItemX = drawItemX + 0.07
            format_medium_ostext (255, 255, 255, 205)
            //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth, g_TextMessage[MessageListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgYear)
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgDay, g_LiveSideTask[SideTaskListSlot[drawIndex]].TxtMsgTimeSent.TxtMsgMonth)


            #IF IS_DEBUG_BUILD
                //Display the text label for debug purposes
                IF g_LiveSideTask[SideTaskListSlot[drawIndex]].SideTaskLockStatus = SIDETASK_LOCKED
                    format_medium_ostext (255, 0, 0, 95) //Semi transparent red
                ELSE
                    format_medium_ostext (0, 255, 0, 95) //Semi transparent green
                ENDIF
                //DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_TextMessage[MessageListSlot[drawIndex]].TxtMsgLabel)
            
            
                DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_AmbientSideTask[g_LiveSideTask[SideTaskListSlot[drawIndex]].SideTaskIdentifier].SideTaskTitle)

            #endif
                                                                             


            drawItemX = ListOriginX

            format_medium_ostext (255, 255, 255, 205) //Semi transparent white
            IF drawIndex = ListCursorIndex
                highlight_Item()
            ENDIF

            drawItemY = drawItemY + 0.02

            SET_TEXT_WRAP (0.0, 0.93)
            DISPLAY_TEXT (drawItemX, drawItemY, g_AmbientSideTask[g_LiveSideTask[SideTaskListSlot[drawIndex]].SideTaskIdentifier].SideTaskTitle) //the contexts of the text itself
                  
            drawItemY = drawItemY + 0.05 //Draw the next name slightly down the y-axis
        
        //ENDIF

   
        drawIndex ++
       



    ENDWHILE
    


ENDPROC







//Temp - would be done in flash.
PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
    
       IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF
    
    ENDIF
	
	// PC Scrollwheel support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
		
			 IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF  
        		
		ENDIF
		
		IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
		
		 ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfSideTasksInList 

                ListCursorIndex = 0

            ENDIF

            
		ENDIF
	
	ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF  
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

            
        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfSideTasksInList 

                ListCursorIndex = 0

            ENDIF

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

           
            
        ENDIF

    
    ENDIF

            
ENDPROC


                                                 







PROC Check_For_SideTask_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - sidetask selected   
 

        #if IS_DEBUG_BUILD
            
                                                       
            PRINTSTRING ("Array Index occupying this slot ")
            PRINTINT (SideTaskListSlot[ListCursorIndex])
            PRINTNL()
        
        #endif

   
        
        SingleSideTaskIndex = SideTaskListSlot[ListCursorIndex]


        //Only proceed to single message display and options if the index points to a full slot.  - temp removed for testing!
        //IF g_LiveSideTask[SingleSideTaskIndex].SideTaskLockStatus <> SIDETASK_EMPTY


                    g_DoOptionsForSingleMessage= TRUE

            
                    //Test
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_DATA_SLOT", 15, 0, INVALID_SCALEFORM_PARAM,
                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_287", "CELL_280", "CELL_285", "CELL_284")

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 15)
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_4")


          





            g_Cellphone.PhoneDS = PDS_COMPLEXAPP
            g_DoOptionsForSingleMessage= TRUE

        //ENDIF


       
    ENDIF


ENDPROC





PROC DisplaySingleSideTask_and_Options()


    DRAW_RECT (0.824, 0.500, 0.253, 0.313, 0, 0, 0, 155)

    Display_Selected_SideTask()
                   
    //Draw_SingleSideTaskOptions_HelpText()



ENDPROC







SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

    //Important! Make sure full display list is defaulted to on startup of this script.
    g_DoOptionsForSingleMessage= FALSE //Return to full display list.



    //Remove any new text message signifiers.
    g_DisplayNewSideTaskSignifier = FALSE


  
    Place_Existing_SideTasks_In_Slots()


    g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Failsafe - makes sure that the cellphone is initialised into running state. 



    /* NO BLOCK JUST NOW! All stored in ambient.txt

    REQUEST_ADDITIONAL_TEXT ("TMSG", PHONE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (PHONE_TEXT_SLOT)  //the permanent global from the temporary holder.
        WAIT(0)
    ENDWHILE

    */



    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 14)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_4")

    
    




    
    WHILE TRUE

        WAIT(0)
        



        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.

            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP


                    IF g_DoOptionsForSingleMessage= FALSE

                        Display_SideTask_List()
                                             
                        Check_for_List_Navigation()                                      
                                             
                        Check_For_SideTask_Selection()

                    ENDIF

                BREAK




                CASE PDS_COMPLEXAPP
                    
                    IF g_DoOptionsForSingleMessage = TRUE

                         DisplaySingleSideTask_and_Options()

                    ENDIF
                                            
                BREAK



               
                CASE PDS_AWAY

                    TERMINATE_THIS_THREAD()

                BREAK


                DEFAULT

                    #IF IS_DEBUG_BUILD

                        PRINTSTRING("AppSideTask in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK


            ENDSWITCH

     
            
            
            //Only check for this if not in full list display mode

            IF g_DoOptionsForSingleMessage= FALSE

    






                IF CHECK_FOR_APPLICATION_EXIT()

                    Cleanup_and_Terminate()
           
                ENDIF

            ELSE

                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - from single sidetask screen to multiple sidetask list.
           
                    g_InputButtonJustPressed = TRUE

                    g_DoOptionsForSingleMessage= FALSE

                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP


                    Place_Existing_Sidetasks_In_Slots()  //repopulate slots after possible deletion.

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 14) //Set enum state and header to sidetask LIST
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_4")




                ENDIF


            ENDIF



     




  
        //ELSE


            //Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
            //return to the main menu post-phonecall.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.

            //BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling contact" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the contacts list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the contacts list.

            //Cleanup_and_Terminate //Likewise, having this here will make sure this script is terminated should any call come through... 
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()

        ENDIF





        #IF IS_DEBUG_BUILD

            //draw_debug_screen_grid()

        #endif



        
            
    ENDWHILE
    
    
ENDSCRIPT


