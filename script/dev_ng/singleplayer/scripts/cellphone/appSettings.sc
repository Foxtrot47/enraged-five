USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "help_at_location.sch"




  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appSettings.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Create a settings list by referencing the array of settings.
//                          
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

                                           

CONST_INT MaxNumberOfSettingSlots 20 //Max_Settings_Secondary_Options

INT SettingListSlot[MaxNumberOfSettingSlots]

INT NumberOfSettingsInList = 0

INT ListCursorIndex = 0
SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex


INT SettingNamesToDraw = 0

INT i_Selected_Setting = 0
INT i_Selected_Slot_to_Restore = 0 
INT iTickedSlotonLaunch = 0

INT i_Selected_Slot, i_Temp_Slot_Contents, i_HighlightedSlot

//INT Test_Phone_Ringtone_SoundID

INT i_iconDecision


BOOL b_RingtoneTestUnderway = FALSE

BOOL b_CueAutoRingtoneStart = FALSE
BOOL b_CheckAutoCueTimer = FALSE


BOOL Run_Sample_Vibrate = FALSE

INT AutoCueElapsedTime, AutoCueStartTime



BOOL b_DoSleepWarningRoutine = FALSE
BOOL b_DoSleepHelpRoutine = FALSE


//Temp
FLOAT listOriginX = 0.82
FLOAT listOriginY = 0.42


FLOAT drawItemX = 0.0
FLOAT drawItemY = 0.0

BOOL dpad_scroll_pause_cued = FALSE



FLOAT f_PrimaryHelpX, f_PrimaryHelpY


//Added for 2269799. Need to provide support for ringtone preview in settings list.
BOOL b_ScrollForwardInitiated = FALSE
BOOL b_ScrollBackInitiated = FALSE




















PROC Sample_Vibrate()


    AutoCueElapsedTime = GET_GAME_TIMER()

    INT timePassed = AutoCueElapsedTime - AutoCueStartTime
    IF timePassed % 600 <= 300 AND timePassed < 3000
        SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100) 
    ENDIF
ENDPROC









PROC Place_Names_in_Secondary_SettingSlots()


    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1) //Set enum state and header to Homescreen briefly to flush view state data

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 22)



    NumberOfSettingsInList = 0 //reset number of Settings before filling list.

    BOOL Secondary_Setting_name_is_in_order[Max_Settings_Secondary_Options]

    INT slotIndex = 0



    IF g_bInMultiplayer
    //Reset this to the normal MP behaviour as a failsafe. See charsheet_public for initial set up. Bug 1922922
   
        This_Cellphone_Owner_Settings_ListContents[3].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_CREWEMB" 


        GAMER_HANDLE checkPlayerHandle

        checkPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())

        //IS_LOCAL_PLAYER_IN_ACTIVE_CLAN ?


        //If the player is not in an active clan at this moment temporarily replace fill index 3, CHAR_MULTIPLAYER, wallpaper setting with the label that says "Default"
        IF NOT (NETWORK_CLAN_PLAYER_IS_ACTIVE(checkPlayerHandle))
        //AND ( g_DumpDisableEveryFrameCaller = FALSE) //Comment in for easy testing.
                
            This_Cellphone_Owner_Settings_ListContents[3].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_840"

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("APP Settings - Player not in an active crew. Using default as the first label in Background settings list.")
                cdPrintnl()
                    
            #endif

        ENDIF

    ENDIF


    

    WHILE slotIndex < Max_Settings_Secondary_Options  

        #IF IS_DEBUG_BUILD

            PRINTSTRING("Populating secondary sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
    
        #endif         


        INT comparisonIndex = 0

        INT top_order_comparison = (Max_Settings_Secondary_Options - 1) //prevent array overrun

    

        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison] = 5000
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison] = 5000
        


        //g_SettingList[top_order_comparison].Setting_orderInt = 5000


        WHILE comparisonIndex < Max_Settings_Secondary_Options
    
            
            //PRINTSTRING("comparison index =")
            //PRINTINT(comparisonIndex)
            //PRINTNL()
            

            //g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE



            //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE
            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[comparisonIndex] = TRUE
            

            //IF g_SettingList[comparisonIndex].phoneGuiPresence = AVAILABLE_IN_GUI
                
                
                IF Secondary_Setting_name_is_in_order[comparisonIndex] = FALSE


                        
                        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[comparisonIndex]
                             < This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Order[top_order_comparison]
                        //IF g_SettingList[comparisonIndex].Setting_orderInt <  g_SettingList[top_order_comparison].Setting_orderInt
                        
                            
                         
    
                            
                            top_order_comparison = comparisonIndex


                            //PRINTSTRING("top comparison assigned to comparisonindex")
                            //PRINTINT(comparisonIndex)
                            //PRINTNL()

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(slotIndex)), 
                                (TO_FLOAT(This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Icon_Int[top_order_comparison])),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[top_order_comparison]) 
                    
                            
                            
            
                                                                                                                                                 

                        ENDIF

                ENDIF
            ENDIF

            comparisonIndex ++

        ENDWHILE

        
        


        SettingListSlot[slotIndex] = top_order_comparison
        
        Secondary_Setting_name_is_in_order[top_order_comparison] = TRUE
        


        //This sort of works but need to solve issue where switching to high value setting in them will then transfer over unexpectedly. Scaleform pish again!
        
        IF ARE_STRINGS_EQUAL (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_labels[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option]
        , This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[top_order_comparison])
           
            i_HighlightedSlot = slotIndex

            IF i_HighlightedSlot < 0
                i_HighlightedSlot = 0
            ENDIF

           
                  
            //Add tick icon for currently selected option for "Background", "Ringtone" and "Theme" only.
            
            IF i_Selected_Setting =  ENUM_TO_INT(SETTING_RINGTONE) 
            OR i_Selected_Setting =  ENUM_TO_INT(SETTING_WALLPAPER) 
            OR i_Selected_Setting =  ENUM_TO_INT(SETTING_THEME) 
        
            
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(slotIndex)), (TO_FLOAT(48)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[top_order_comparison]) 
                        

                //Keep a copy of the currently ticked slot so we can remove it later, when a new choice has been made.
                iTickedSlotonLaunch = slotIndex


            ENDIF




            //Add tick icon for currently selected option for "Profile" which is used for Snapmatic quicklaunch in MP only.
            
            IF i_Selected_Setting =  ENUM_TO_INT(SETTING_PROFILE) 
            AND g_bInMultiplayer = TRUE 
        
            
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(slotIndex)), (TO_FLOAT(48)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[top_order_comparison]) 
                        

                //Keep a copy of the currently ticked slot so we can remove it later, when a new choice has been made.
                iTickedSlotonLaunch = slotIndex


            ENDIF


            





        ENDIF
          





        //IF g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[slotIndex] = TRUE
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_available[slotIndex] = TRUE

        
            NumberOfSettingsInList ++ 
       
            /*
            PRINTSTRING("NumSettings-")
            PRINTINT(NumberOfSettingsInList)
            PRINTNL()
            */

        ENDIF

    

        slotIndex ++

  

    ENDWHILE





    //Set scalefrom movie view state enum to Settings screen and display associated buttons.
    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22, TO_FLOAT(i_HighlightedSlot)) //Set enum state and header to Supplementary Settings

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22)



    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", g_SettingList[i_Selected_Setting].Setting_Primary_Label)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_labels[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option])
    


    //Monochrome
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" Positive

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0, 
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
    */



    //Badger - Select Back combo
    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    ELSE
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other   
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



     
ENDPROC





PROC MakeIconDecision(INT PassedComparisonIndex, INT PassedTopOrder)  //This display the appropriate icon in the root of Settings depending on the secondary level setting.


    SWITCH PassedComparisonIndex

        CASE 0 //SETTING_PROFILE,   //0


            IF g_bInMultiplayer

                i_iconDecision = 19 //i_TEMPiconTest               

            ELSE

                IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
                
                    i_iconDecision = 26
                
                ELSE

                    i_iconDecision = 25

                ENDIF

            ENDIF

        BREAK


        CASE 3//SETTING_VIBRATE

            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Currently_Selected_Option = 1 //Vibrate On

                i_iconDecision = 20

            ELSE

                i_iconDecision = 21

            ENDIF

        BREAK

            

        DEFAULT

            i_iconDecision = This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[PassedTopOrder].Setting_Icon_Int

        BREAK

    ENDSWITCH



ENDPROC






PROC Place_Names_in_SettingSlots()


    NumberOfSettingsInList = 0 //reset number of Settings before filling list.

    BOOL Setting_name_is_in_order[MaxNumberOfSettingSlots]

    INT slotIndex = 0

    WHILE slotIndex < (ENUM_TO_INT(MAX_SETTINGS))  


        #if IS_DEBUG_BUILD

            PRINTSTRING("Populating sf slot ")
            PRINTINT(slotIndex)
            PRINTNL()
    
        #endif         


        INT comparisonIndex = 0

        INT top_order_comparison = (ENUM_TO_INT(MAX_SETTINGS))

    
        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt = 5000
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt = 5000



        
        //Check if we should include the vibration option on the phone #1241874
        IF GET_PROFILE_SETTING (PROFILE_CONTROLLER_VIBRATION) = 0   //Vibration set to off in frontend


            #if IS_DEBUG_BUILD

                PRINTSTRING("AppSettings - Vibration Profile Setting is set to 0. Not including in setting list. ")
                PRINTINT(slotIndex)
                PRINTNL()
    
            #endif


            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_VIBRATE].phoneGuiPresence = UNAVAILABLE_IN_GUI
        
        ELSE   //Vibration must be ON in frontend.

            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_VIBRATE].phoneGuiPresence = AVAILABLE_IN_GUI

        ENDIF



        WHILE comparisonIndex < (ENUM_TO_INT(MAX_SETTINGS))

            /*
            PRINTSTRING("comparison index =")
            PRINTINT(comparisonIndex)
            PRINTNL()
            */


            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[comparisonIndex].phoneGuiPresence = AVAILABLE_IN_GUI
                IF Setting_name_is_in_order[comparisonIndex] = FALSE

                        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[comparisonIndex].Setting_orderInt
                             <  This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_orderInt


                            IF g_bInMultiplayer

                     
                                //IF comparisonIndex = ENUM_TO_INT(SETTING_THEME)   //This wasn't available in last gen but will be for NG - 1857900
                                //IF comparisonIndex = ENUM_TO_INT(SETTING_PROFILE) //Profile is now used in Settings in MP only to control quicklaunch of Snapmatic.
                                //OR comparisonIndex = ENUM_TO_INT(SETTING_WALLPAPER) //This wasn't available in last gen but will be for NG - 1857900


                                    //#if IS_DEBUG_BUILD

                                        //PRINTSTRING ("ST - AppSettings not including this within multiplayer - comparison index: ")
                                        //PRINTINT (comparisonIndex)
                                        //PRINTNL()

                                    //#endif


                                //ELSE



                                    IF comparisonIndex = ENUM_TO_INT(SETTING_PROFILE)

                                        /* Update 16.09.14 - now including Snapmatic quicklaunch check in MP NG.
                                        IF IS_XBOX_PLATFORM()  
                                        OR IS_PLAYSTATION_PLATFORM()

                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("ST_Appsettings not including MP Quicklaunch via SP Profile setting in Next Gen.")
                                                cdPrintnl()
                                            #endif

                                        ELSE
                                        */
                                            //Might need to do something unique for SETTING_PROFILE, so keep this separate from other MP setting fill routines.
                                            top_order_comparison = comparisonIndex

                                            MakeIconDecision(comparisonIndex, top_order_comparison)

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(slotIndex)),
                                            (TO_FLOAT(i_iconDecision)),
                                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_Primary_Label) 


                                        //ENDIF

                                    
                                    ELSE

                                        top_order_comparison = comparisonIndex

                                        MakeIconDecision(comparisonIndex, top_order_comparison)

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(slotIndex)),
                                            (TO_FLOAT(i_iconDecision)),
                                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_Primary_Label) 
                                        
                                        
                                    ENDIF 
                                    
                                
                                //ENDIF


                            ELSE

                                IF GET_SINGLEPLAYER_CELLPHONE_OPERATING_SYSTEM() = OS_FACADE 


                                    IF comparisonIndex = ENUM_TO_INT(SETTING_WALLPAPER)
                                    
                                    //#if USE_TU_CHANGES
                                    OR comparisonIndex = ENUM_TO_INT (SETTING_MISC_A)  //This is used for INVITE SOUND url:bugstar:1796220 - we don't need it in SP.
                                    //#endif


                                        #if IS_DEBUG_BUILD

                                            PRINTSTRING ("ST - Wallpaper setting not included for Facade OS - comparison index: ")
                                            PRINTINT (comparisonIndex)
                                            PRINTNL()

                                         #endif




                                    ELSE 

                                        top_order_comparison = comparisonIndex

                                        MakeIconDecision(comparisonIndex, top_order_comparison)

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(slotIndex)),
                                        (TO_FLOAT(i_iconDecision)),
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_Primary_Label)  

                                    ENDIF


                                ELSE



                                    //#if USE_TU_CHANGES

                                    IF comparisonIndex = ENUM_TO_INT (SETTING_MISC_A) //This is used for INVITE SOUND url:bugstar:1796220 - we don't need it in SP.

                                         #if IS_DEBUG_BUILD

                                            PRINTSTRING ("ST - Invite Sound setting not included for Singleplayer - comparison index: ")
                                            PRINTINT (comparisonIndex)
                                            PRINTNL()

                                         #endif

                                    ELSE

                                    //#endif
                                        
                                    top_order_comparison = comparisonIndex


                                    MakeIconDecision(comparisonIndex, top_order_comparison)

           
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(slotIndex)),
                                        (TO_FLOAT(i_iconDecision)),
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[top_order_comparison].Setting_Primary_Label) 
                                        
                                                                                                                                 
                                        
                                
                                   //#if USE_TU_CHANGES
                                    
                                    ENDIF

                                    //#endif
                    


                                ENDIF

                            ENDIF                                     

                        ENDIF

                ENDIF
            ENDIF

            comparisonIndex ++

        ENDWHILE

        
        


        SettingListSlot[slotIndex] = top_order_comparison
        
        Setting_name_is_in_order[top_order_comparison] = TRUE
        



        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[slotIndex].phoneGuiPresence = AVAILABLE_IN_GUI

        
            NumberOfSettingsInList ++ 
       
            /*
            PRINTSTRING("NumSettings-")
            PRINTINT(NumberOfSettingsInList)
            PRINTNL()
            */

        ENDIF

    

        slotIndex ++

  

    ENDWHILE





    //Set scalefrom movie view state enum to Settings screen and display associated buttons.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 13, TO_FLOAT(i_Selected_Slot_to_Restore)) //Set enum state and header to Settings
    
    
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_16")

    //Monochrome
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1, 
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"SELECT" Positive

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0, 
        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
    */



    //Badger - Select Back combo
    IF g_b_ToggleButtonLabels
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

    ELSE
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
            2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
    ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



     
ENDPROC










PROC Display_Settings_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


            
    SettingNamesToDraw = (NumberOfSettingsInList) 

    DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    WHILE drawIndex < SettingNamesToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF


      
        IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[drawIndex].phoneGuiPresence = AVAILABLE_IN_GUI //would need to check individual owner phonebooks if this is required.
            
            DISPLAY_TEXT (drawItemX, drawItemY, This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[drawIndex].Setting_Primary_Label) //draw the name of the Setting by referring to the character's text label. 


                 
            drawItemY = drawItemY + 0.03 //Draw the next name slightly down the y-axis
        
        ENDIF

   
        drawIndex ++
       



    ENDWHILE


    


ENDPROC







//Temp - would be done in flash.
PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
    
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF

    ENDIF
    
    // PC Scrollwheel support
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()
        
        ENDIF
        
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
        
             ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfSettingsInList 
                ListCursorIndex = 0
            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

        ENDIF
    
    ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
        
            Call_Scaleform_Input_Keypress_Up()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfSettingsInList 

                ListCursorIndex = 0

            ENDIF

            
            Call_Scaleform_Input_Keypress_Down()

            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

           
            
        ENDIF

    
    ENDIF

            
ENDPROC










PROC Check_For_Setting_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.

    
    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - Setting selected.


        //Maybe replace the permanent "selection" help with temporary so as to negate need for this?
        //CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)
        




        Play_Select_Beep()


        g_InputButtonJustPressed = TRUE



        //i_Selected_Setting  =  ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
        Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


        
        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
        
            WAIT (0)

            #if IS_DEBUG_BUILD
                cdPrintstring("appSettings - Waiting on return value from scaleform. 501")
                cdPrintnl()
            #endif
            
        ENDWHILE


        i_Selected_Slot_to_Restore = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)  //make a copy so this can be input into DISPLAY_VIEW to reselect last highlighted slot.


        i_Selected_Setting  =  ENUM_TO_INT (SettingListSlot[i_Selected_Slot_to_Restore])
        

    
     
            SWITCH i_Selected_Setting 

                
                CASE 0

                    //PRINTSTRING ("PROFILE selected")

                    IF g_bInMultiplayer = FALSE //Profile is used for Snapmatic quicklaunch in MP.

                        IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
                            IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = FALSE

                                CLEAR_HELP (TRUE)


                                //HELP_AT_SCREEN_LOCATION("CELL_7050", f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER, FLOATING_HELP_MISSION_SLOT)
                                PRINT_HELP_FOREVER("CELL_7050")      

                                IF f_PrimaryHelpX = f_PrimaryHelpY
                                    //do nothing - unref hack that will deleted in final optimisations.
                                ENDIF


                                //Moving this to force action on player part in case phone is forced away accidentally...
                                //g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = TRUE

                                b_DoSleepWarningRoutine = TRUE

                                
                            ENDIF
                        ENDIF
                    
                    ENDIF

                BREAK


                CASE 1

                    //PRINTSTRING ("RINGTONE selected")

                BREAK


                   
                CASE 2

                    //PRINTSTRING ("THEME selected")

                BREAK


                CASE 3
                    
                    //PRINTSTRING ("VIBRATE selected")

                BREAK


            ENDSWITCH


            

        


        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

            g_Cellphone.PhoneDS = PDS_COMPLEXAPP
                            
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 22. appSettings assigns PDS_COMPLEXAPP")
                cdPrintnl()   
            #endif

        ENDIF

        Place_Names_in_Secondary_SettingSlots()

        b_CheckAutoCueTimer = TRUE

        AutoCueStartTime = GET_GAME_TIMER()


        

     

    
    ENDIF         
    ENDIF


ENDPROC














PROC Check_For_Secondary_Setting_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.


    
      //Cues ringtones to autoplay after brief pause and upon navigation of the list.

        IF i_Selected_Setting =  ENUM_TO_INT(SETTING_RINGTONE)


            IF b_CheckAutoCueTimer
                AutoCueElapsedTime = GET_GAME_TIMER()

                IF AutoCueElapsedTime - AutoCueStartTime > 1000
                
                    b_CueAutoRingtoneStart = TRUE
                    b_CheckAutoCueTimer = FALSE

                ENDIF
            ENDIF



            IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

                IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
                    b_ScrollForwardInitiated = TRUE
                    b_ScrollBackInitiated = FALSE
                ENDIF

                IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
                    b_ScrollForwardInitiated = FALSE
                    b_ScrollBackInitiated = TRUE
                ENDIF

            ENDIF


        

            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
            OR Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))
            OR b_CueAutoRingtoneStart
            OR b_ScrollForwardInitiated = TRUE //2269799
            OR b_ScrollBackInitiated = TRUE //2269799

                //2269799
                b_ScrollForwardInitiated = FALSE
                b_ScrollBackInitiated = FALSE

        
                b_CueAutoRingtoneStart = FALSE
                b_CheckAutoCueTimer = FALSE


                SETTIMERB(0)
                b_RingtoneTestUnderWay = TRUE


                //i_Temp_Slot_Contents = ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

                BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
                Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

                    WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
            
                        WAIT (0)

                        #if IS_DEBUG_BUILD
                            cdPrintstring("appSettings - Waiting on return value from scaleform. 502")
                            cdPrintnl()
                        #endif
                
                    ENDWHILE

                i_Temp_Slot_Contents  =  ENUM_TO_INT (SettingListSlot[GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)])
        



                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())     
                
                    //STOP_SOUND (Test_Phone_Ringtone_SoundID) //make sure any previous ringtone test is terminated.    //#1392094 swap

                    STOP_PED_RINGTONE (PLAYER_PED_ID())

               
    
                    WAIT(100)  //This seems to be required here with new ringtone regime. According to Matthew, I may need to preload them...


                    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 

                        //IF HAS_SOUND_FINISHED (Test_Phone_Ringtone_SoundID) //#1392094 swap
                        IF NOT IS_PED_RINGTONE_PLAYING(PLAYER_PED_ID())                  


                            //Compare string against filename that reads "Silent Ringtone Dummy". This should always say that and does not point to an actual audio file.
                            IF ARE_STRINGS_EQUAL (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[i_Temp_Slot_Contents], "Silent Ringtone Dummy")
                                             
                                #if IS_DEBUG_BUILD

                                    PRINTSTRING ("AppSettings - Silent Ringtone selected. You won't hear any preview sound.")
                                    PRINTNL ()

                                #endif

                            ELSE
                                    //#1392094 swap
                                    //PLAY_SOUND_FROM_ENTITY (Test_Phone_Ringtone_SoundID, 
                                        //This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[i_Temp_Slot_Contents],
                                        //PLAYER_PED_ID())

                                    //#1392094 swap
                                    PLAY_PED_RINGTONE (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[i_Temp_Slot_Contents],
                                                       PLAYER_PED_ID(), TRUE)


                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("AppSettings - Ringtone Sample cued from Play_Ped_Ringtone()")
                                        cdPrintnl()

                                    #endif


                            ENDIF
            
                
                        ENDIF


                    ENDIF

                ENDIF


            ENDIF

        ENDIF

   















    
    IF g_InputButtonJustPressed = FALSE
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //positive action - Setting selected.


        Play_Select_Beep()

        g_InputButtonJustPressed = TRUE

 
        //Legacy       
        //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])
        //This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")])

        
        //For bug 303583 - need to keep a copy of previously highlighted slot so it can be returned to after re-establishing the view state.
        
        //i_Selected_Slot = LEGACY_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION") //need to keep a copy of this

        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
        Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

        WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
        
            WAIT (0)

            #if IS_DEBUG_BUILD
                cdPrintstring("appSettings - Waiting on return value from scaleform. 503")
                cdPrintnl()
            #endif
            
        ENDWHILE



        i_Selected_Slot = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)

        //Test for upcoming scaleform changes.
        //WAIT(0)

        //Prevent scaleform returning -1 for whatever reason...
        IF i_Selected_Slot < 0

            i_Selected_Slot = 0

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("APPSETTINGS WARNING: Returned slot from theme selection was less than zero. Assigning return value to zero.")
                PRINTNL()

            #endif

        ENDIF       
        
        
        This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[i_Selected_Slot])





        
        IF i_Selected_Setting = ENUM_TO_INT(SETTING_THEME)


            //Grab selected option and put it into saved global...
            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer = (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
           
           // g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer =   
            //(This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
            
            INT i_ClarityTemp = This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option
            
            

            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer = 
            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[i_ClarityTemp]

            
            
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))
            
            
 
            
            //...and keep a copy of the index for the MP Stat.

            IF g_bInMultiplayer

                Temp_MPcurrentTheme = This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option

                SET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_THEME,This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option)

                #if IS_DEBUG_BUILD  
                                                              
                    PRINTNL()
                    PRINTSTRING("Setting MP THEME STAT..........")
                    PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_THEME))
                    PRINTNL()
                
                #endif


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (Temp_MPcurrentTheme))
                

            ELSE
    
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer))


            ENDIF



            SetUpPhoneSurroundTint()

            
             
            #if IS_DEBUG_BUILD

                PRINTSTRING ("Selected slot was ")
                PRINTINT (i_Selected_Slot)
                //PRINTINT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
                PRINTNL()


                PRINTSTRING ("Theme Set to ")
                PRINTINT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer)
                PRINTNL ()




            #endif

                         
                         
            //i_Temp_Slot_Contents = g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer



            //Remove tick from old choice.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(iTickedSlotonLaunch)), (TO_FLOAT(23)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[iTickedSlotonLaunch])]) 
                    





            //Update new choice with tick...

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(i_Selected_Slot)), (TO_FLOAT(48)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[i_Selected_Slot])]) 
                    

            //Update iTickedSlot to reflect change.

            iTickedSlotonLaunch = i_Selected_Slot







            //Refresh view so colourway / theme change takes effect. Second int parameter specifies that we should return the view state highlight to the last used slot.
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22, TO_FLOAT (i_Selected_Slot))

            #if IS_DEBUG_BUILD

                PRINTSTRING ("Re-establishing view state 22 with highlighted slot number ")
                PRINTINT (i_Selected_Slot)
                PRINTNL ()

            #endif





            //Removed to save globals 15.03.13
            //If the chosen theme is linked to a wallpaper change, update that too...
            /*

            IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_Wallpaper[i_ClarityTemp]  > -1


                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Found linked wallpaper from this selected slot. Applying wallpaper number: ")
                    PRINTINT (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_Wallpaper[i_ClarityTemp])
                    PRINTNL ()

                #endif
    

                g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer = This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_Wallpaper[i_ClarityTemp]

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer))


           ENDIF
           */



        ENDIF




        IF i_Selected_Setting = ENUM_TO_INT(SETTING_WALLPAPER)


           //Grab selected option and put it into saved global...
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer = (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
            
                        
            
            //...and keep a copy of the index for the MP Stat.

            IF g_bInMultiplayer

                Temp_MPcurrentWallpaper = This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Currently_Selected_Option


                SET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_BACKGROUND, This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Currently_Selected_Option)

                #if IS_DEBUG_BUILD  
                                                              
                    PRINTNL()
                    PRINTSTRING("Setting MP BACKGROUND STAT..........")
                    PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_BACKGROUND))
                    PRINTNL()
                
                #endif


                IF Temp_MPCurrentWallpaper = 0 //Crew Emblem selected.

                     //Add crew emblem as cellphone wallpaper...
                    TEXT_LABEL_63 sCrewTextureName

                    GAMER_HANDLE PlayerHandle

                    PlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())

                    BOOL b_TimedOutGettingCrewEmblem = FALSE





                    IF NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE   //See 2002264

                        #if IS_DEBUG_BUILD

                            cdPrintstring("App Settings - NETWORK_HAVE_USER_CONTENT_PRIVILEGES() returned false prior to Crew Emblem retrieval.")
                            cdPrintnl()

                        #endif  
            
                    ENDIF




            
                    IF NETWORK_CLAN_PLAYER_IS_ACTIVE(PlayerHandle)
                    AND NETWORK_HAVE_USER_CONTENT_PRIVILEGES()    
                    AND g_b_HasCrewEmblemRetrievalFailedThisSession = FALSE
                        //AND (g_DumpDisableEveryFrameCaller = FALSE) comment in for easy testing.

                        #if IS_DEBUG_BUILD

                            cdPrintstring("App_Settings - Player primary clan active and content privileges available! Will try to get texture name for cellphone background. ")
                            cdPrintnl()

                        #endif

                        SETTIMERA(0)

                        WHILE NOT (NETWORK_CLAN_GET_EMBLEM_TXD_NAME(PlayerHandle, sCrewTextureName))
                        AND (TIMERA() < 3000) //Give MP simulation 3 seconds to get crew texture for testing purposes.
                        
                            WAIT (0)

                            #if IS_DEBUG_BUILD

                                cdPrintstring("App_Settings - waiting on GET_EMBLEM_TXD_NAME to retrieve string")
                                cdPrintnl()

                            #endif


                            IF TIMERA() > 2999

                                #if IS_DEBUG_BUILD

                                    cdPrintstring("WARNING! App_Settings - TIMED OUT on GET_EMBLEM_TXD_NAME to retrieve string!")
                                    cdPrintnl()

                                #endif


                                b_TimedOutGettingCrewEmblem = TRUE

                            ENDIF


                        ENDWHILE
                    

                        #if IS_DEBUG_BUILD

                            cdPrintstring("App_Settings - Emblem TXD string retrieved was... ")
                            cdPrintstring(sCrewTextureName)
                            cdPrintnl()

                        #endif

                    
                        IF b_TimedOutGettingCrewEmblem = FALSE //If we didn't time out, set the background crew image.

                            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_BACKGROUND_CREW_IMAGE")
                            
                            //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCrewTextureName)
                            //END_SCALEFORM_MOVIE_METHOD()
                            

                                BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_2000") //~a~
                            
                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sCrewTextureName) 
                                                               
                                END_TEXT_COMMAND_SCALEFORM_STRING()


                            END_SCALEFORM_MOVIE_METHOD()

                        ENDIF

                    ELSE


                        #if IS_DEBUG_BUILD

                            cdPrintstring("AppSettings - Clan not active. Assigning default wallpaper as 0.")
                            cdPrintstring(sCrewTextureName)
                            cdPrintnl()

                        #endif



                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (0))


                    ENDIF


                ELSE
            
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (Temp_MPCurrentWallpaper))
                    
                ENDIF





            ELSE

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_BACKGROUND_IMAGE", TO_FLOAT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer))
            

            ENDIF

            
            
            
            
            
            
            
            
            
             
            #if IS_DEBUG_BUILD

                PRINTSTRING ("Wallpaper Set to ")
                PRINTINT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer)
                PRINTNL ()

            #endif



            //Remove tick from old choice.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(iTickedSlotonLaunch)), (TO_FLOAT(23)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[iTickedSlotonLaunch])]) 
                    





            //Update new choice with tick...

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(i_Selected_Slot)), (TO_FLOAT(48)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[i_Selected_Slot])]) 
                    

            //Update iTickedSlot to reflect change.

            iTickedSlotonLaunch = i_Selected_Slot



            //Refresh view so colourway / theme change takes effect. Second int parameter specifies that we should return the view state highlight to the last used slot.
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22, TO_FLOAT (i_Selected_Slot))

            #if IS_DEBUG_BUILD

                PRINTSTRING ("Re-establishing view state 22 with highlighted slot number ")
                PRINTINT (i_Selected_Slot)
                PRINTNL ()

            #endif


        ENDIF



















        IF i_Selected_Setting =  ENUM_TO_INT(SETTING_RINGTONE)
        
        
            //SETTIMERB(0)
            //b_RingtoneTestUnderWay = TRUE




            //Grab selected option and put it into saved global...
            //Haha! What an assignment!
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer = 
            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Currently_Selected_Option]



            //...and keep a copy of the index for the MP Stat.

            IF g_bInMultiplayer

                SET_MP_INT_CHARACTER_STAT (MP_STAT_FM_CELLPHONE_RINGTONE,This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Currently_Selected_Option)

                #if IS_DEBUG_BUILD  
                                                              
                    PRINTNL()
                    PRINTSTRING("Setting MP RINGTONE STAT..........")
                    PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_RINGTONE))
                    PRINTNL()
                
                #endif

            ENDIF


            /*  //Commented out as we now play test sounds automatically above.
            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())     
            
                STOP_SOUND (Test_Phone_Ringtone_SoundID) //make sure any previous ringtone test is terminated.

                IF HAS_SOUND_FINISHED (Test_Phone_Ringtone_SoundID)
                     

             
                    PLAY_SOUND_FROM_ENTITY (Test_Phone_Ringtone_SoundID, g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, PLAYER_PED_ID())

              
        
                ENDIF
            ENDIF
            */


            //Remove tick from old choice.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(iTickedSlotonLaunch)), (TO_FLOAT(18)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[iTickedSlotonLaunch])]) 
                    





            //Update new choice with tick...

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(i_Selected_Slot)), (TO_FLOAT(48)),
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[i_Selected_Slot])]) 
                    

            //Update iTickedSlot to reflect change.

            iTickedSlotonLaunch = i_Selected_Slot



            //Refresh view so colourway / theme change takes effect. Second int parameter specifies that we should return the view state highlight to the last used slot.
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22, TO_FLOAT (i_Selected_Slot))

            #if IS_DEBUG_BUILD

                PRINTSTRING ("Re-establishing view state 22 with highlighted slot number ")
                PRINTINT (i_Selected_Slot)
                PRINTNL ()

            #endif



        ENDIF




        IF i_Selected_Setting =  ENUM_TO_INT(SETTING_VIBRATE)
        
            //Grab selected option and put it into saved global and MP stat when in MP.

            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer = (This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)


            IF g_bInMultiplayer 

                #if IS_DEBUG_BUILD  
                                                              
                    PRINTNL()
                    PRINTSTRING("Attempting to set MP_STAT_FM_CELLPHONE_VIBRATE to  ")
                    PRINTINT (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer)
                    PRINTNL()
                    PRINTSTRING(" from settings list. ")
                    PRINTNL()
                
                #endif



                SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE, g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer)
                
                #if IS_DEBUG_BUILD  
                                                              
                    PRINTNL()
                    PRINTSTRING("Checking MP VIBRATE STAT after set..........")
                    PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE))
                    PRINTNL()
                
                #endif
                          

            ENDIF


    
            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer = 1 //Vibrate is set to ON.
    
                Run_Sample_Vibrate = TRUE
                   
                AutoCueStartTime = GET_GAME_TIMER()

            ELSE

                Run_Sample_Vibrate = FALSE

            ENDIF





        ENDIF






        IF i_Selected_Setting =  ENUM_TO_INT(SETTING_PROFILE)
        
            
            
            IF g_bInMultiplayer = TRUE
            

                This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[i_Selected_Slot])
               

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppSettings - Setting MP_STAT_FM_CELLPHONE_QUICKLAUNCH to .")
                    cdPrintint(This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
                    cdPrintnl()

                #endif



                //Remove tick from old choice.

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(iTickedSlotonLaunch)), (TO_FLOAT(19)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[iTickedSlotonLaunch])]) 
                        





                //Update new choice with tick...

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(22)), (TO_FLOAT(i_Selected_Slot)), (TO_FLOAT(48)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_Labels[ENUM_TO_INT (SettingListSlot[i_Selected_Slot])]) 
                        


                //Update iTickedSlot to reflect change.

                iTickedSlotonLaunch = i_Selected_Slot







                //Refresh view so colourway / theme change takes effect. Second int parameter specifies that we should return the view state highlight to the last used slot.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 22, TO_FLOAT (i_Selected_Slot))

                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Re-establishing view state 22 with highlighted slot number ")
                    PRINTINT (i_Selected_Slot)
                    PRINTNL ()

                #endif


                //Put back in when stat is available.
                SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_QUICKLAUNCH, This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)
                

            ELSE

                //This does not need to be a saved global.
                //This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ProfileForThisPlayer = (This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option)


                //Set all SP phones to chosen mode...
                This_Cellphone_Owner_Settings_ListContents[CHAR_MICHAEL].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[i_Selected_Slot])
                This_Cellphone_Owner_Settings_ListContents[CHAR_FRANKLIN].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[i_Selected_Slot])
                This_Cellphone_Owner_Settings_ListContents[CHAR_TREVOR].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option = ENUM_TO_INT (SettingListSlot[i_Selected_Slot])
                



                //If phone is in sleep mode, show mini signifier in top portion of screen, otherwise switch it off.
                //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.
                IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE) //check if phone is in sleep mode.
       

              
                    //Display sleep help if player is off mission.
                    IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
                        IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = FALSE

                            CLEAR_HELP (TRUE)


                            //HELP_AT_SCREEN_LOCATION("CELL_7050", f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER, FLOATING_HELP_MISSION_SLOT)
                            PRINT_HELP_FOREVER ("CELL_7050")      

                            IF f_PrimaryHelpX = f_PrimaryHelpY
                                //do nothing - unref hack that will deleted in final optimisations.
                            ENDIF


                            //Moving this to force action on player part in case phone is forced away accidentally...
                            //g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = TRUE

                            b_DoSleepWarningRoutine = TRUE
                 
                            //SET_GAME_PAUSED (TRUE)

                        ELSE


                            



                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 1)


                            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)

                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)


                            IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed = FALSE

                                CLEAR_HELP (TRUE)
                                PRINT_HELP("CELL_7051")

                                g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed = TRUE

                            ENDIF




                        ENDIF
                    ENDIF
                    
                  
            
                ELSE

                
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 0)   


                    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)

                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)



                ENDIF



            ENDIF //g_bInMultiplayer condition check


        ENDIF




        


        
        
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER",
            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Secondary_Option_labels[This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[i_Selected_Setting].Setting_Currently_Selected_Option])











        //If the player is on a mission and has perhaps started it with the phone already on the settings list, make sure that SLEEP mode cannot persist by returning to normal mode
        IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
            SET_CELLPHONE_PROFILE_TO_NORMAL() //from cellphone_public.sch
        ENDIF 


    ENDIF         
    ENDIF


ENDPROC











PROC Cleanup_and_Terminate()


    Restore_Special_Case_Settings()  //from cellphone_private.sch


    //If the player is on a mission and has perhaps started it with the phone already on the settings list, make sure that SLEEP mode cannot persist by returning to normal mode
    IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) 
        SET_CELLPHONE_PROFILE_TO_NORMAL() //from cellphone_public.sch
    ENDIF 



    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
        //STOP_SOUND (Test_Phone_Ringtone_SoundID) //make sure any ringtone test is terminated. //#1392094 swap

        STOP_PED_RINGTONE (PLAYER_PED_ID())

    ENDIF

    
    //RELEASE_SOUND_ID (Test_Phone_Ringtone_SoundID)  //#1392094 swap


    //CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)
    
    
    //Check for specific help message
    IF b_DoSleepWarningRoutine
        CLEAR_HELP (TRUE)
    ENDIF


    //If the sleep tutorial has not been fully completed, reset profile to normal...
    IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed = FALSE
    OR g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = FALSE
        SET_CELLPHONE_PROFILE_TO_NORMAL() 
    ENDIF





    SET_GAME_PAUSED (FALSE)

    TERMINATE_THIS_THREAD()


ENDPROC 















SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 13)


    Get_Cellphone_Owner()



    IF g_Chosen_Ratio = DISPLAY_16_9

        f_PrimaryHelpX = 0.75
        f_PrimaryHelpY = 0.8

    ELSE

        f_PrimaryHelpX = 0.65
        f_PrimaryHelpY = 0.77

    ENDIF

    //Fill_All_Settings() Moved to charsheet_initial.sc



    //Display settings menu help one time only. Removed at request of Imran #955201
    /*
    IF NOT g_savedGlobals.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed //This saved global is now reused for the Focus Lock help.

        SWITCH GET_FLOW_HELP_MESSAGE_STATUS("CELL_750")
    
            CASE FHS_EXPIRED

                ADD_HELP_TO_FLOW_QUEUE("CELL_750", FHP_HIGH, 500, DEFAULT_GOD_TEXT_TIME)
            
            BREAK
        
            CASE FHS_DISPLAYED

                g_savedGlobals.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed = TRUE
                
                #if IS_DEBUG_BUILD

                    cdPrintstring("AppSETTINGS - Settings menu help has been displayed. Saved global set.")
                    cdPrintnl()

                #endif

            BREAK
        
        ENDSWITCH

    ENDIF
    */  

 



    //Get the currently selected options from the saved settings data so the headers can be specified easily.

    //Themes and wallpapers are directly matching ints - easily transferred.
    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_THEME].Setting_Currently_Selected_Option = g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ThemeForThisPlayer
    
    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_WALLPAPER].Setting_Currently_Selected_Option = g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].WallpaperForThisPlayer


    INT tempcomparisonIndex = 0

    WHILE tempcomparisonIndex < (ENUM_TO_INT(MAX_SETTINGS))


        IF g_bInMultiplayer

            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_RINGTONE].Setting_Currently_Selected_Option = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_RINGTONE)

            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_THEME].Setting_Currently_Selected_Option = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_THEME)
            
            This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_WALLPAPER].Setting_Currently_Selected_Option = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_BACKGROUND)

        ELSE

            IF ARE_STRINGS_EQUAL ( g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[tempComparisonIndex])

                This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_RINGTONE].Setting_Currently_Selected_Option = tempcomparisonIndex
       
            ENDIF

        ENDIF

        tempcomparisonIndex ++

    ENDWHILE



    //This does not need to be a saved global but is included in struct.
    //This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[SETTING_PROFILE].Setting_Currently_Selected_Option = This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].ProfileForThisPlayer

    
    
    IF g_bInMultiplayer 
    
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE)

        #if IS_DEBUG_BUILD  
                                                      
            PRINTNL()
            PRINTSTRING("Getting MP VIBRATE STAT..........")
            PRINTINT (GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_VIBRATE))
            PRINTNL()
        
        #endif

    ENDIF

    
    This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[SETTING_VIBRATE].Setting_Currently_Selected_Option = g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer



    
    Temporarily_Remove_Special_Case_Settings()  //from cellphone_private.sch
    


    





    IF g_bInMultiplayer  //New block using unused PROFILE setting for MP character as way of storing Quicklaunch preference. Quicklaunch MP only now.

        //Fill_Setting_List (3, SETTING_PROFILE, 190, "CELL_701", 25, AVAILABLE_IN_GUI)  //Replace standard sleep profile in MP with Snapmatic Quicklaunch option

        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_orderInt = 190
    
        //Make copy of orderInt in original orderInt in case the order is changed to make the setting a priority.
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_original_orderInt = 190  
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Primary_Label = "CELL_701"
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Icon_Int = 19
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].phoneGuiPresence = AVAILABLE_IN_GUI    



        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[0] = "CELL_704"     //Quicklaunch Off
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[0] = 170
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[0] = 19
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[1] = "CELL_703"     //Quicklaunch On
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[1] = 175
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[1] = 19

        //Should not display, but here to illustrate normal SP use with all options filled.
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[2] = "CELL_801"     //Sleep
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[2] = FALSE
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[2] = 190
        This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[2] = 26

        //Put back in when stat is available.
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_QUICKLAUNCH) = 0

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppSettings - Retrieved MP_STAT_FM_CELLPHONE_QUICKLAUNCH as 0.")
                cdPrintnl()

            #endif 

            This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = 0 //Would get this from stat...
  
        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppSettings - Retrieved MP_STAT_FM_CELLPHONE_QUICKLAUNCH as 1.")
                cdPrintnl()

            #endif 


            This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = 1
        
        ENDIF
        

    ENDIF




    Place_Names_in_SettingSlots()



    
    WHILE TRUE

        WAIT(0)
        

        //Temp test!
        /*
        #if IS_DEBUG_BUILD
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Special option - square

                LAUNCH_CALL_SCREEN_FOR_CHAT_CALL("CustomStringY")

                Cleanup_and_Terminate()

            ENDIF
        #endif
        */


        IF b_RingtoneTestUnderway 
        
            IF TIMERB() > 3500
                            
                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
                    
                    //STOP_SOUND (Test_Phone_Ringtone_SoundID) //make sure any ringtone test is terminated. //#1392094 swap

                    STOP_PED_RINGTONE (PLAYER_PED_ID())

                ENDIF 
                           
                b_RingtoneTestUnderway = FALSE

            ENDIF

        ENDIF




        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.

            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP

                    //Scaleform transition
                    //Display_Settings_List()
                                             
                    Check_for_List_Navigation()                                      
                                             
                    Check_For_Setting_Selection()

                BREAK

               

                CASE PDS_COMPLEXAPP


                    IF b_DoSleepWarningRoutine
                    OR b_DoSleepHelpRoutine

                        IF b_DoSleepWarningRoutine

                            /*
                            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) 

                                //SET_GAME_PAUSED (FALSE)

                                b_DoSleepWarningRoutine = FALSE

                                g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = TRUE


                                CLEAR_HELP (TRUE)

            
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_SLEEP_MODE", 1)

                                SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)

                                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

                                //HELP_AT_SCREEN_LOCATION("CELL_7051", (f_PrimaryHelpX - 0.50), f_PrimaryHelpY - 0.075, HELP_TEXT_EAST, FLOATING_HELP_PRINT_FOREVER, FLOATING_HELP_MISSION_SLOT)
                                PRINT_HELP ("CELL_7051")

                                IF g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed = FALSE

                                    b_DoSleepHelpRoutine = TRUE

                                    g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed = TRUE

                                    SETTIMERB(0)

                                ENDIF


                            ENDIF
                            */


                                b_DoSleepWarningRoutine = FALSE

                                SETTIMERB(0)
                                g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed = TRUE 

                                b_DoSleepHelpRoutine = TRUE




                        ELSE //DoSleepHelpRoutine must be true

                            IF TIMERB() > DEFAULT_GOD_TEXT_TIME

                                //SET_GAME_PAUSED (FALSE)

                                b_DoSleepHelpRoutine = FALSE

                                //CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)
                                CLEAR_HELP(TRUE)

                            ENDIF

                            
                        ENDIF




                    ELSE

                        Check_for_List_Navigation()

                        Check_For_Secondary_Setting_Selection()

                        IF Run_Sample_Vibrate

                            Sample_Vibrate()

                        ENDIF


                        

                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                        

                            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 
                                
                                //STOP_SOUND (Test_Phone_Ringtone_SoundID) //make sure any ringtone test is terminated. //#1392094 swap

                                STOP_PED_RINGTONE (PLAYER_PED_ID())

                            ENDIF

                            Run_Sample_Vibrate = FALSE //make sure that the sample vibrate is terminated.


                            Play_Back_Beep()

                            g_InputButtonJustPressed = TRUE

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 13)
                            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1) //Set enum state and header to Homescreen briefly to flush view state data

                            Place_Names_in_SettingSlots()

                            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                                g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("STATE ASSIGNMENT 23. appSettings assigns PDS_RUNNINGAPP")
                                    cdPrintnl()   
                                #endif

                            ENDIF

                        ENDIF

                    ENDIF

                BREAK
               




                DEFAULT

                    #IF IS_DEBUG_BUILD

                        PRINTSTRING("AppSettings in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK



            ENDSWITCH

     

            IF CHECK_FOR_APPLICATION_EXIT() 
               
               Cleanup_and_Terminate()
               
            ENDIF                  
                  



        





       
        ELSE



            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
            //It is important to look at how appTextMessage interacts with any change to this section as it relies upon this script to make a call to any message sender.

            BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state usually alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling Setting" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the Settings list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the Settings list.

            Cleanup_and_Terminate() //Likewise, having this here will make sure this script is terminated should any call come through... 

            
          

        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

            Cleanup_and_Terminate()
                
        ENDIF



        #if IS_DEBUG_BUILD

            //draw_debug_screen_grid()


            //PRINTSTRING("ticked slot ")
            //PRINTINT(iTickedSlotonLaunch)
            //PRINTNL()

        #endif

  
            
    ENDWHILE
    
    
ENDSCRIPT


