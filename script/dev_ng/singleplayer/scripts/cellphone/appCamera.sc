USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_misc.sch"
USING "commands_graphics.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "stack_sizes.sch"
USING "net_stat_system.sch"
USING "help_at_location.sch"
USING "cellphone_movement.sch"
USING "commands_recording.sch"


                                       



// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   appCamera.sc - NG
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Ever-changing, ever-hacked Cellphone camera script.
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



//Beginning of Christmas DLC vars

CONST_INT i_NumberOfFiltersAvailable    13  //updated for Thomas D's changes in bug 2108712, Nov 2014. Now 12 filters available plus entry for 0. 13 states in total.
 

STRING s_CurrentFilter[i_NumberOfFiltersAvailable]




CONST_INT i_NumberOfBordersAvailable    13

STRING s_CurrentBorder[i_NumberOfBordersAvailable]


BOOL b_GridWasOnWhenBorderWasZero = FALSE  //2145148



CONST_INT i_NumberOfExpressionsAvailable    7

STRING s_FacialEx[i_NumberOfExpressionsAvailable]




BOOL b_Displayed_Version_Info = FALSE




//g_i_CurrentlySelected_SnapMaticFilter = 0 //This is now a global... value can be overriden on script launch.

//This has been moved to menu_globals_TU.sch so the Wildlife Photography script can interrogate its value.
//INT g_i_CurrentlySelected_SnapMaticBorder = 0 //No border by default - move this to same location as INT g_i_CurrentlySelected_SnapMaticFilter = 0 in menu_globals_TU.sch



INT g_i_CurrentlySelected_SnapMaticExpression = 0  //Neutral, normal default expression with no override.




BOOL b_OkayToDrawBorder = FALSE
BOOL b_OkayToCheckForBorderSelection = TRUE  



BOOL b_NormalMode_DepthOfField_ToggledOn = FALSE
BOOL b_SelfieMode_DepthOfField_ToggledOn = FALSE



BOOL b_SelfieDepthOfFieldEffectAvailable = TRUE//FALSE //Toggle this to allow Depth of Field effect to be manipulated and corresponding instructional button text to be present.

BOOL b_NormalDepthOfFieldEffectAvailable = TRUE//FALSE //Now used for Focus Lock functionality.

BOOL b_ActivatedMultiheadBlinders


INT StationaryCheck_StartTime, StationaryCheck_CurrentTime

VECTOR v_PlayerSelfie_StartPos, v_PlayerSelfie_CurrentPos



FLOAT f_GameplayCamRelativePitch = 0.0
FLOAT f_GameplayCamRelativeHeading = 172.0


FLOAT f_LastSelfieRelativePitch
FLOAT f_LastSelfieRelativeHeading


//End of Christmas DLC vars.





FLOAT f_ButtonWidth = 0.7








VECTOR v_3dCameraphoneRotation, v_3dCameraphonePosition, v_3dDesiredRotation, v_3dDesiredPosition    //Move to set up...


VECTOR v_FPcurrentRot, v_FPcurrentPos


FLOAT f_PrimaryHelpX, f_PrimaryHelpY, f_SecondaryHelpX, f_SecondaryHelpY
INT Update_Help_State = 0

BOOL b_External_Help_Presence = FALSE
BOOL b_External_SendState_Change = FALSE

BOOL b_CheckingStorage = FALSE
BOOL b_RescanWhileCameraInUse = FALSE
BOOL b_NetworkSignedIn = FALSE
PHOTO_OPERATION_STATUS MediaPicProcessStatus 


INT i_SignInStartTime, i_SignInCurrentTime

//BOOL b_DisplayingSignin = FALSE

INT i_CurrentNumberOfStoredPhotos = 0

INT i_MaximumNumberOfStoredPhotos = 0 

INT i_SignInTimeOutRequiredFlag = 0

INT Zoom_SoundID

BOOL b_HasZoomSFXBeenSetup = FALSE

FLOAT f_CellCamZoomFactor

BOOL b_VehicleRenderCheck = FALSE
BOOL b_VehicleRenderAbort = FALSE


BOOL b_360_Priv_Help_Done = FALSE


//Moved to global...
//BOOL g_b_Rotate3dPhonePortrait = FALSE

//BOOL g_b_Rotate3dPhoneLandscape = FALSE

BOOL b_Rotate3dPhoneX = FALSE
BOOL b_Rotate3dPhoneY = FALSE
BOOL b_Rotate3dPhoneZ = FALSE



BOOL b_ManipulatePhonePosX = FALSE
BOOL b_ManipulatePhonePosY = FALSE
BOOL b_ManipulatePhonePosZ = FALSE

BOOL b_Reposition_Corner = FALSE
BOOL b_Reposition_FullScreen = FALSE

BOOL b_SnapToPortraitImmediately = FALSE




BOOL b_WaitingForDiscardOrSend = FALSE, b_OkayToTakePicture = FALSE, b_BeginInitialisationPause = FALSE 
BOOL b_ScreenFadeInterference = FALSE


BOOL b_InSelfieMode = FALSE
BOOL b_FocusLockOngoing = FALSE

BOOL b_LeftTriggerInstructionsOngoing = FALSE
BOOL b_RightTriggerInstructionsOngoing = FALSE


INT i_ShutterTransitionTime = 0
BOOL b_ShutterTransition = FALSE
INT i_ShutterDelayForAnimation = 1200
INT i_ShutterDelayForAnimationAfterPhoto = 1200//Was 1020 but I've lengthened it a little in an effort to improve bug 2172770
INT i_ShutterDelayForSelfieSwitch = 166

//INT CamPhoneRenderTarget_ID //the render target id of the phone


SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index, SF_MovieInstButtonsIndex
BOOL ShouldDrawShutter = FALSE, b_DrawCamHelpText = FALSE

INT ShutterTypeDecision
TEXT_LABEL TokenDecision

VEHICLE_INDEX tempVehicleIndex //Used to grab current player vehicle and apply handbrake.








//Gallery Variables

BOOL b_InSaveRoutine = FALSE

//INT ImageListSlot[MAX_GALLERY_IMAGES]
//INT NumberOfImagesInList = 0

BOOL dpad_scroll_pause_cued = FALSE
//INT GalleryCursorIndex = 0




//Alert Screens
FE_WARNING_FLAGS iButtonBits
BOOL b_AlertScreenPresent = FALSE

INT stateOfSocialClubSignUpButton = 0


ENUM enumAlertScreenType
    
    ALERT_NONE,
    ALERT_AT_MAX_CAPACITY,
    ALERT_SCLUB_UNAVAILABLE,
    ALERT_SCLUB_OUTDATEDPOLICY,
    ALERT_CLOUD_UNAVAILABLE,
    ALERT_CLOUD_UNDERAGE,
    ALERT_SCLUB_BANNED,
    ALERT_CLOUD_SIGNINUTILITY    

ENDENUM

enumAlertScreenType e_CurrentAlertScreen



BOOL b_LowriderDisableElements = FALSE //New master control to switch off certain Snapmatic functionality in accordance with TODO 2513807 and 2512104






PROC SnapmaticHudHide()


    IF b_LowriderDisableElements =  TRUE    

        //Hide components but continue to show radar.
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_AREA_NAME)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_WANTED_STARS)    
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_CASH)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_MP_CASH)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_VEHICLE_NAME)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_DISTRICT_NAME)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_STREET_NAME)
        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_WEAPON_ICON)

    ELSE

        HIDE_HUD_AND_RADAR_THIS_FRAME()
        
    ENDIF

ENDPROC




PROC Wrapped_CellCam_SelfieModeActivate (BOOL passedBool)




    IF b_LowriderDisableElements = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - WARNING! Wrapped CELL_CAM_ACTIVATE_SELFIE_MODE was called but taking no action as b_LowriderDisableElements is TRUE!")
            cdPrintnl()

        #endif

        EXIT

    ENDIF



    IF g_FMMC_STRUCT.bSecurityCamActive = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - WARNING! Wrapped CELL_CAM_ACTIVATE_SELFIE_MODE was called but taking no action as g_FMMC_STRUCT.bSecurityCamActive is TRUE!")
            cdPrintnl()

        #endif

    ELSE

        IF g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppCamera - WARNING! Wrapped CELL_CAM_ACTIVATE_SELFIE_MODE was called but taking no action as g_FMMC_STRUCT.bDisablePhoneInstructions is TRUE!")
                cdPrintnl()

            #endif

        ELSE


            CELL_CAM_ACTIVATE_SELFIE_MODE (passedBool)

            #if IS_DEBUG_BUILD
                IF passedBool = TRUE
                    cdPrintnl()
                    cdPrintstring("WRAPPED_CellCam_Selfie called with TRUE")        
                    cdPrintnl()
                ELSE
                    cdPrintnl()
                    cdPrintstring("WRAPPED_CellCam_Selfie called with FALSE")       
                    cdPrintnl()
                ENDIF
            #endif
        


        ENDIF

    ENDIF


ENDPROC





PROC Wrapped_CellCam_StandardModeActivate (BOOL passedBool1, BOOL passedBool2)


    IF g_FMMC_STRUCT.bSecurityCamActive = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - WARNING! Wrapped CELL_CAM_ACTIVATE was called but taking no action as g_FMMC_STRUCT.bSecurityCamActive is TRUE!")
            cdPrintnl()

        #endif

    ELSE

        IF g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppCamera - WARNING! Wrapped CELL_CAM_ACTIVATE was called but taking no action as g_FMMC_STRUCT.bDisablePhoneInstructions is TRUE!")
                cdPrintnl()

            #endif

        ELSE

            CELL_CAM_ACTIVATE (passedBool1, PassedBool2)

            #if IS_DEBUG_BUILD
                IF passedBool1 = TRUE
                    cdPrintnl()
                    cdPrintstring("WRAPPED_CellCam_Standard called with TRUE")      
                    cdPrintnl()
                ELSE
                    cdPrintnl()
                    cdPrintstring("WRAPPED_CellCam_Standard called with FALSE")     
                    cdPrintnl()
                ENDIF
            #endif

        ENDIF

    ENDIF


ENDPROC













PROC Clear_Modifier_With_Exception_Checks(BOOL bCameraFilterClearing = TRUE)

    IF g_FMMC_STRUCT.bSecurityCamActive = FALSE //2009386
    AND g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE //2139232
    AND NOT g_bFMMCLightsTurnedOff //If this is F v J mode, we don't want to clear any timecycle set up. 2515958
   
	
        //IF g_Use_Prologue_Cellphone = FALSE   //Comment in for easy testing of Security Cam exception.


            
           
			 #IF FEATURE_CASINO_HEIST
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			AND bCameraFilterClearing
				gb_bRefreshArcadeTimecycle = TRUE
				PRINTLN("AppCAMERA - Snapmatic: gb_bRefreshArcadeTimecycle = TRUE")
			ELSE
			#ENDIF
	            CLEAR_TIMECYCLE_MODIFIER() 
				 #if IS_DEBUG_BUILD

	                cdPrintnl()
	                cdPrintstring("AppCAMERA - Snapmatic CLEAR_TIMECYCLE_MODIFIER() called as all exception checks are FALSE.")
	                cdPrintnl()

	            #endif
			#IF FEATURE_CASINO_HEIST
			ENDIF
			#ENDIF
        //ENDIF

    ENDIF
 
ENDPROC







FUNC BOOL Is_DLC_Compatible()

    IF b_Displayed_Version_Info = FALSE

        b_Displayed_Version_Info = TRUE


        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCAMERA - Snapmatic tunable is no longer checked. Compatibility check PS3 and 360 builds returning TRUE permanently.")
            cdPrintnl()

        #endif

    ENDIF


    RETURN TRUE //Snapmatic enhancements always on. Requested by TODOs 1720963 and 1720937




    /* Snapmatic changes will now be part of TU core functionality. Do not need to check initially for presence of DLC pack. So says John Whyte 03.12.13 11.50am
    IF NOT IS_DLC_PRESENT(HASH("mpChristmas"))


        IF b_Displayed_Version_Info = FALSE

            b_Displayed_Version_Info = TRUE


            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppCAMERA - No DLC mpChristmas pack present.")
                cdPrintnl()

            #endif

        ENDIF
   

        RETURN FALSE 

    ENDIF
    */


    
    /*
    #if IS_DEBUG_BUILD  //Ticking unused g_DumpDisableEveryFrameCaller widget in rag/script/cellphone_and_dialogue_debug allows easy testing of DLC availability swap.

        IF g_DumpDisableEveryFrameCaller = TRUE
           
            IF b_Displayed_Version_Info = FALSE

                b_Displayed_Version_Info = TRUE

                #if IS_DEBUG_BUILD
        
                    cdPrintnl()
                    cdPrintstring("AppCAMERA - Debug widget set to disable Snapmatic Christmas DLC in debug build.")
                    cdPrintnl()

                #endif
        
            ENDIF


            RETURN FALSE

        ELSE
        

            //Comment out this section and return value to test tunable.
            IF b_Displayed_Version_Info = FALSE
            
                b_Displayed_Version_Info = TRUE

                #if IS_DEBUG_BUILD
        
                    cdPrintnl()
                    cdPrintstring("AppCAMERA - Debug Build detected. Making Snapmatic Christmas DLC available in debug build.")
                    cdPrintnl()

                #endif

            ENDIF
            
        
            RETURN TRUE  //Comment out to test tunable.

        ENDIF

    #endif

    


    //If we get to this point we must be in a Release Build script.rpf
    
    IF g_sMPTunables.bTurn_snapmatic_on_off = TRUE //Tunable from MP_globals_Tunables.sch

        IF b_Displayed_Version_Info = FALSE

            b_Displayed_Version_Info = TRUE

            #if IS_DEBUG_BUILD
        
                cdPrintnl()
                cdPrintstring("AppCAMERA - Christmas tunable returning TRUE, able to use Christmas DLC in release mode.")
                cdPrintnl()

            #endif

        ENDIF


        RETURN TRUE


    ELSE


        IF b_Displayed_Version_Info = FALSE

            b_Displayed_Version_Info = TRUE

            #if IS_DEBUG_BUILD
        
                cdPrintnl()
                cdPrintstring("AppCAMERA - Christmas tunable returning FALSE, unable to use Snapmatic Christmas DLC in release mode.")
                cdPrintnl()
                
            #endif

        ENDIF


        RETURN FALSE //Should be FALSE!
        
    ENDIF
    */

ENDFUNC






PROC Local_PPE_Toggle(BOOL b_SwitchOn)



    IF Is_DLC_Compatible()

        IF b_SwitchOn

            IF NOT PHONEPHOTOEDITOR_IS_ACTIVE()

                PHONEPHOTOEDITOR_TOGGLE(TRUE)

                IF (g_i_CurrentlySelected_SnapMaticBorder > 0)
                AND (g_i_CurrentlySelected_SnapMaticBorder < 99)
                
                    PHONEPHOTOEDITOR_SET_FRAME_TXD(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], false)
                    
                    CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(0.25)

                ENDIF
                

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - PhonePhotoEditorToggle is set to TRUE")
                    cdPrintnl()

                #endif

            ENDIF

        ELSE


            IF PHONEPHOTOEDITOR_IS_ACTIVE()

                PHONEPHOTOEDITOR_TOGGLE(FALSE)

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - PhonePhotoEditorToggle is set to FALSE")
                    cdPrintnl()

                #endif

            ENDIF


        ENDIF

    ENDIF



ENDPROC



FUNC BOOL IsEligibleToSavePhoto() //Heavily modified at the request of 1892059 and 1915523.



    //Removal of "Emergency Scan Routine" caused a few unreferenced vars. They mightbe required in a hurry. For convenience, just a few useless comparisons to prevent compilation errors.
    IF i_SignInStartTime = i_SignInCurrentTime
        IF b_NetworkSignedIn
            b_NetworkSignedIn = TRUE
        ENDIF
    ENDIF




    IF g_Cellphone.PhoneDS < PDS_TAKEOUT //If the phone suddenly goes AWOL, don't trigger a save.

        RETURN FALSE

    ELSE

       IF i_CurrentNumberOfStoredPhotos = i_MaximumNumberOfStoredPhotos
       OR i_CurrentNumberOfStoredPhotos > i_MaximumNumberOfStoredPhotos

            e_CurrentAlertScreen = ALERT_AT_MAX_CAPACITY

            iButtonBits = FE_WARNING_OK

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("APPCAMERA - Jul 2014 Simplified IsEligibleToSavePhoto() returning FALSE. Max photo capacity reached.")
                cdPrintnl()

            #endif


            RETURN FALSE

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPCAMERA - Jul 2014 Simplified IsEligibleToSavePhoto() returning TRUE.")
                cdPrintnl()

            #endif


            RETURN TRUE

        ENDIF

    ENDIF


    /* IMPORTANT! Graeme suggests we may need to be signed into a profile to save to the HD. If so, we need this check commented in and line above commented out.
    // If we do need this check then I will have to update "Emergency Scan Routine" below.
    IF NETWORK_IS_SIGNED_IN()

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF
    */



    /* These were the legacy last gen checks and alert screens that were required when Snapmatic uploaded to the cloud directly from this script. Now saves to the hard disk. 1915523
    IF NETWORK_IS_SIGNED_ONLINE()  //online and signed in...

            //IF NOT IS_ACCOUNT_OVER_17() //Comment in to test age restriction screens. .
             
            IF IS_ACCOUNT_OVER_17() //Should be submitted version

                    IF SCRIPT_IS_CLOUD_AVAILABLE() //cloud available

                            IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()//Social Club available, check storage capacity now...

                                IF NETWORK_HAVE_ROS_SOCIAL_CLUB_PRIV() //#1598305 check for banning...
                                //AND g_DumpDisableEveryFrameCaller = FALSE //comment in to easily test banning screen.


                                    IF NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
                                                         
                                        IF i_CurrentNumberOfStoredPhotos = i_MaximumNumberOfStoredPhotos

                                            e_CurrentAlertScreen = ALERT_AT_MAX_CAPACITY

                                            iButtonBits = FE_WARNING_OK

                                            RETURN FALSE

                                        ELSE

                                            RETURN TRUE

                                        ENDIF

                                    ELSE
                                    
                                        e_CurrentAlertScreen = ALERT_SCLUB_OUTDATEDPOLICY
                                        
                                        stateOfSocialClubSignUpButton = 0

                                        iButtonBits = FE_WARNING_CANCEL + FE_WARNING_SC_UPDATE

                                        RETURN FALSE

                                    ENDIF

                                

                                ELSE //Has account but no privileges. Display banned alert!

                                    e_CurrentAlertScreen = ALERT_SCLUB_BANNED
                                    
                                    iButtonBits = FE_WARNING_OK

                                    RETURN FALSE

                                ENDIF




                            ELSE //Cloud available, no social club account...


                                                              
                                e_CurrentAlertScreen = ALERT_SCLUB_UNAVAILABLE
                                
                                stateOfSocialClubSignUpButton = 0

                                iButtonBits = FE_WARNING_CANCEL + FE_WARNING_SC_SIGNUP

                                RETURN FALSE

                    
                            ENDIF


                    ELSE //signed in online, but cloud not available for some reason...

                            e_CurrentAlertScreen = ALERT_CLOUD_UNAVAILABLE

                            iButtonBits = FE_WARNING_OK
                            
                            RETURN FALSE
                    
                    ENDIF



            ELSE  //Display warning for parental controls / under-age account.

        
                e_CurrentAlertScreen = ALERT_CLOUD_UNDERAGE

                iButtonBits = FE_WARNING_OK
                        
                RETURN FALSE


            ENDIF



    ELSE     //Not signed in online, cloud unavailable...

        //e_CurrentAlertScreen = ALERT_CLOUD_UNAVAILABLE

        //iButtonBits = FE_WARNING_OK


        e_CurrentAlertScreen = ALERT_CLOUD_SIGNINUTILITY


        IF IS_PLAYSTATION_PLATFORM()//PS4 currently doesn't support an in-game signin utility, so only display an OK button here. url:bugstar:1784537

            iButtonBits = FE_WARNING_OK

        ELSE

            IF NETWORK_IS_CABLE_CONNECTED()

        iButtonBits = FE_WARNING_CANCELSIGNIN


            ELSE

                iButtonBits = FE_WARNING_OK  //No network cable, don't display sign in utility? #1892059

            ENDIF
                

        ENDIF

        RETURN FALSE

    ENDIF
    */

ENDFUNC






FUNC BOOL IsGalleryAvailable() //Checked by the scan storage operation only.



    IF g_FMMC_STRUCT.bSecurityCamActive = TRUE

        IF g_bInMultiplayer

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPCAMERA - IsGalleryAvailable returning false g_FMMC_STRUCT.bSecurityCamActive = TRUE. #2131815")
                cdPrintnl()

            #endif

            RETURN FALSE

        ENDIF

    ENDIF


    IF g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE

        IF g_bInMultiplayer

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPCAMERA - IsGalleryAvailable returning false g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE. #2139232")
                cdPrintnl()

            #endif

            RETURN FALSE

        ENDIF

    ENDIF



    IF IS_XBOX360_VERSION()
    OR IS_PS3_VERSION()

        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE)


            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPCAMERA - IsGalleryAvailable returning false in LG as BYPASS_CAMERA_APP_SAVE but has been set. #2024849")
                cdPrintnl()

            #endif


            RETURN FALSE

        ENDIF

    ENDIF


    /* Removed! Pictures save to hard drive.
    IF SCRIPT_IS_CLOUD_AVAILABLE() //Replace this with something that specifically checks if the user has a social club account.?
    //AND NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()//Social Club available, check storage capacity now...
    //AND NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()

        RETURN TRUE //To test, make this return false.

    ELSE

        RETURN FALSE
        
    ENDIF
    */

    //Assume hard drive is always available then...
    RETURN TRUE

ENDFUNC






PROC InitialiseCheckStorage()



    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("APPCAMERA - InitialCheckStorage called.")
        cdPrintnl()

    #endif




    IF IsGalleryAvailable()

        CLEAR_STATUS_OF_SORTED_LIST_OPERATION()

        IF QUEUE_OPERATION_TO_CREATE_SORTED_LIST_OF_PHOTOS (FALSE) //The last BOOL indicates that we are checking for a LOAD. This would be TRUE if we were saving.

            b_CheckingStorage = TRUE //Tell main loop to perform sorted list operation check.

            i_SignInTimeOutRequiredFlag = 0


        ELSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("APPCAMERA - Initial Queue operation failed. Terminating appCamera script and forcing phone away")
                cdPrintnl()
                cdPrintString(" STATE ASSIGNMENT - AM1 - PDS_AWAY")
                cdPrintnl()

            #endif
        

            g_b_appHasRequestedTidyUp = TRUE

            g_CellPhone.PhoneDS =  PDS_AWAY

            BUSYSPINNER_OFF()

            
        ENDIF

    ELSE

        i_CurrentNumberOfStoredPhotos = 0

        i_MaximumNumberOfStoredPhotos = 0

        
        IF b_RescanWhileCameraInUse = TRUE

            IF b_WaitingForDiscardOrSend = FALSE //Don't display updated numbers whilst in delete / continue phase.
               
                IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)

                    IF Is_DLC_Compatible()

                        IF g_i_CurrentlySelected_SnapMaticBorder = 0
                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                            ENDIF
                        ENDIF

                        #if IS_DEBUG_BUILD
                            cdPrintstring("Setting Photo Frame On if hide bit not set - 1")
                            cdPrintnl()
                        #endif


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                    ELSE

                        IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                        ENDIF

                        #if IS_DEBUG_BUILD
                            cdPrintstring("Setting Photo Frame On if hide bit not set - 2")
                            cdPrintnl()
                        #endif


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                    ENDIF

                ENDIF


            ELSE

                IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0)
                
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                ENDIF


            ENDIF

            b_RescanWhileCameraInUse = FALSE

        ENDIF


        i_SignInTimeOutRequiredFlag = 0

        BUSYSPINNER_OFF()




    ENDIF



ENDPROC











PROC CheckandScanStorage()



    MediaPicProcessStatus = GET_STATUS_OF_SORTED_LIST_OPERATION(FALSE) //The last BOOL indicates that we are checking for a LOAD. This would be TRUE if we were saving.


        SWITCH MediaPicProcessStatus


        CASE PHOTO_OPERATION_SUCCEEDED

            #if IS_DEBUG_BUILD

                cdPrintString("AppCAMERA - Sorted List Operation Succeeded!")
                cdPrintnl()

            #endif
            
            b_CheckingStorage = FALSE

            i_CurrentNumberOfStoredPhotos = GET_CURRENT_NUMBER_OF_CLOUD_PHOTOS()

            i_MaximumNumberOfStoredPhotos = GET_MAXIMUM_NUMBER_OF_CLOUD_PHOTOS()


            IF b_RescanWhileCameraInUse = TRUE
                
                IF b_WaitingForDiscardOrSend = FALSE //Don't display updated numbers whilst in delete / continue phase.
                   
                    IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)
                                  
                        IF Is_DLC_Compatible()

                            IF g_i_CurrentlySelected_SnapMaticBorder = 0
                                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                                ENDIF
                            ENDIF

                            #if IS_DEBUG_BUILD
                                cdPrintstring("Setting Photo Frame On if hide bit not set - 3")
                                cdPrintnl()
                            #endif


                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))
                            

                        ELSE

                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                            ENDIF
                        
                            #if IS_DEBUG_BUILD
                                cdPrintstring("Setting Photo Frame On if hide bit not set - 4")
                                cdPrintnl()
                            #endif

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                        ENDIF

                    ENDIF


                ELSE

                    IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0)
                    
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                    ENDIF


                ENDIF

                b_RescanWhileCameraInUse = FALSE

            ENDIF


            BUSYSPINNER_OFF()
             
        BREAK



       
        CASE PHOTO_OPERATION_IN_PROGRESS

            #if IS_DEBUG_BUILD

                cdPrintString("AppCAMERA - Sorting list operation in progress...")
                cdPrintnl()

            #endif

            IF NOT BUSYSPINNER_IS_ON()
                BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("CELL_SPINNER2")
                END_TEXT_COMMAND_BUSYSPINNER_ON(1)//(4) Hard drive save uses standard spinner. "4" is cloud spinner.
            ENDIF

        BREAK

    
        
        CASE PHOTO_OPERATION_FAILED


            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE )
            //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE) //Removed for 1932325 - hard drive save now used.
            //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
            //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE) //Stub for 1708374

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCAMERA - Sorted List Operation FAILED! Bypass Save routine bit has been set. Continuing but with Current / Max photo set to 0")

                #endif


                b_CheckingStorage = FALSE

                i_CurrentNumberOfStoredPhotos = 0

                i_MaximumNumberOfStoredPhotos = 0

            ELSE

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCAMERA - Sorted List Operation FAILED! Terminating appCamera and forcing phone away")
                    cdPrintString("STATE ASSIGNMENT - AM2 - PDS_AWAY")
                    cdPrintnl()

                #endif

                g_CellPhone.PhoneDS =  PDS_AWAY

                g_b_appHasRequestedTidyUp = TRUE

            ENDIF
            
            BUSYSPINNER_OFF()
    
        BREAK


    ENDSWITCH



ENDPROC









PROC SetUp_Individual_Border_Instructional_Button(FLOAT iOrder)


     INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", iOrder, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_UP),  
        "CELL_BORDER")


ENDPROC







PROC SetUp_Individual_Filter_Instructional_Button(FLOAT iOrder)

    IF g_bFMMCLightsTurnedOff //If this is F v J mode, don't allow the user to change the filter as that's effectively cheating. 2515958
        iOrder = -1 //Don't display the instructional button.

         #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - WARNING! LightsOff is TRUE. Not displaying filter button or allowing timecycle changes.")
            cdPrintnl()

        #endif

    ENDIF


     INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", iOrder, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_DOWN),  
        "CELL_FILTER")


ENDPROC




PROC SetUp_Play_Action_Instructional_Button(FLOAT iOrder)


     INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", iOrder, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_LEFT),  
        "CELL_ACTION")


ENDPROC




PROC SetUp_Cycle_Action_Instructional_Button(FLOAT iOrder)


     INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", iOrder, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT),  
        "CELL_ACCYC")


ENDPROC










//Context sensitive help for new Selfie Mode features #2037087
PROC Setup_Selfie_LeftTrigger_Instructional_Buttons()


    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) //B* 2092679
    OR IS_PLAYSTATION_PLATFORM()
          INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ELSE
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ENDIF



    /*
        [CELL_LT_LSTICK]
        Rotate Camera

        [CELL_LT_RSTICK]
        Pan Camera

        [CELL_LT_LSTICKZ]
        Zoom Camera

        [CELL_RT_LSTICK]
        Rotate Head

        [CELL_RT_RSTICK]
        Tilt Head
    */



    
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
        "CELL_LT_RSTICK") //Pan




    IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //2220336 - Can't allow selfie mode zoom here. Don't display Zoom help button, only Rotate.

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_MOVE_LR),  
        "CELL_LT_LSTICK") //Rotate

    ELSE

        //Zoom Help okay on Left Trigger hold here, display both Zoom and Rotate.
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
            "CELL_LT_LSTICKZ") //Zoom

        
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 6, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_MOVE_LR),  
            "CELL_LT_LSTICK") //Rotate

    ENDIF


ENDPROC







PROC Setup_Selfie_RightTrigger_Instructional_Buttons()

    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
    OR IS_PLAYSTATION_PLATFORM()
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ELSE
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ENDIF
    
    
    



    /*
        [CELL_LT_LSTICK]
        Rotate Camera

        [CELL_LT_RSTICK]
        Pan Camera

        [CELL_LT_LSTICKZ]
        Zoom Camera

        [CELL_RT_LSTICK]
        Rotate Head

        [CELL_RT_RSTICK]
        Tilt Head
    */

    // The head controls are different on PC - Leftstick (Movement keys) is used to tilt the head, and rightstick (mouse) is used to move the head.
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_MOVE_LR),  
            "CELL_RT_RSTICK") // Tilt Head

        
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_LOOK),  
            "CELL_RT_LSTICK") //Rotate Head
                    
    ELSE
    
         INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_LOOK_LR),  
            "CELL_RT_RSTICK") //Tilt Head


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
            GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_MOVE),  
            "CELL_RT_LSTICK") //Rotate Head
    
    
    ENDIF


ENDPROC






//If the player is in director mode, we only support action playing for the 3 main story characters. We don't want to draw those instructional buttons is the actor is incompatible. #2203905
//Luke blocked the actions from playing in PI_MENU.sc v115. See bug 2197238
FUNC BOOL Is_Current_Player_Ped_MTD_Compatible()


    IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)

        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

            IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_MICHAEL].game_model
            OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_FRANKLIN].game_model
            OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_TREVOR].game_model
                                
            //OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL //the enum isn't returned correctly in Director mode, so we go for an explicit model check.
            //OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
            //OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppCamera - Is_Current_Player_Ped_MTD_Compatible returning TRUE for main story character. Adding ACTION instructional buttons.")
                    cdPrintnl()

                #endif

                RETURN TRUE

            ELSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppCamera - Is_Current_Player_Ped_MTD_Compatible returning FALSE. Actor not Michael, Trevor or Franklin. No ACTION buttons added.")
                    cdPrintnl()

                #endif
                
                RETURN FALSE

            ENDIF


        ELSE

            RETURN FALSE

        ENDIF


    ELSE

        //We're not in director mode so just return TRUE to satisfy all AND clauses where this check is used.
        RETURN TRUE

    ENDIF



ENDFUNC






//Added for Player Interaction menu in SP. Sets up the secondary functionality for cycling and playing actions. See 1950755
PROC Setup_PI_Selfie_Mode_Instructional_Buttons()


    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
    OR IS_PLAYSTATION_PLATFORM()
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ELSE
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
            "CELL_SP_2NP_XB") //Normal Mode (Press)
    ENDIF
  


    //Grid Lines Toggle
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
        "CELL_GRID") 
            
            
                                     

    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
        "CELL_285") //Move   






    //Filter Choice 
    SetUp_Individual_Filter_Instructional_Button(5)



    //Border Choice
    SetUp_Individual_Border_Instructional_Button(6)


    //Cycle Action  
    SetUp_Cycle_Action_Instructional_Button(7)



    SetUp_Play_Action_Instructional_Button(8)


    
    //Expression Choice
    /* Moved to head movement help text.
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 9, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION),  
        "CELL_EXPRESS") //Test 
    */


    IF b_SelfieDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

        //Depth of Field Toggle
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 9, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
            "CELL_DEPTH") //Test 

    ENDIF




    //If in Singleplayer and PI_Menu is running in selfie mode, we need to initialise the Action Text when we first enter selfie mode. 
    
    
    IF NOT (IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)) //"Director Mode On" text overlaps SF based "action" text in top right hand corner.
     
        IF g_bInMultiplayer = FALSE
        AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) > 0
        AND Is_Current_Player_Ped_MTD_Compatible()

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "SET_FOCUS_LOCK")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)
                
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_ACTTL")
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_txtSP_CurrentSelfie_anim))
                END_TEXT_COMMAND_SCALEFORM_STRING()


                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE)



                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("Setup - Grabbing quickplay anim name...")
                    cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_txtSP_CurrentSelfie_anim))
                    cdPrintnl()

                #endif


            END_SCALEFORM_MOVIE_METHOD()


        ENDIF

    ENDIF



ENDPROC








PROC SetUp_Lowrider_No_Send_Buttons()

    
    //Run if b_LowriderDisableElements is TRUE. See TODO 2513807


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)

    
    //From Left to Right as they appear on the screen.    
  


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
             "CELL_284") //Zoom


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
       GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
             "CELL_285") //Move   




    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
        "CELL_281") //Exit
    

    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
        "CELL_280") //Take Photo



    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.



ENDPROC


FUNC BOOL SHOULD_SEND_PHOTO_TO_LESTER()
	SWITCH GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iActiveMission
		CASE FMMC_TYPE_FM_GANGOPS
			SWITCH INT_TO_ENUM(GANGOPS_VARIATION, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iContrabandVariation)
				CASE GOV_AMATEUR_PHOTOGRAPHY
					RETURN TRUE
			ENDSWITCH
		BREAK
		#IF FEATURE_CASINO_HEIST
		CASE FMMC_TYPE_GB_CASINO_HEIST
			SWITCH INT_TO_ENUM(CSH_VARIATION, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iContrabandVariation)
				CASE CHV_GUARD_PATROL_ROUTES
				CASE CHV_SCOPE_OUT_CASINO
				CASE CHV_GRUPPE_SECHS_VAN_2
					RETURN TRUE
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH
	
	IF g_bForceSendToLester
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SEND_PHOTO_TO_PAVEL()

	#IF FEATURE_HEIST_ISLAND
	SWITCH GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID())

		CASE IHV_PLASMA_CUTTER
			RETURN TRUE
			
		CASE IHV_SCOPING_ISLAND
		CASE IHV_SCOPING_RETURN
		CASE IHV_SCOPING_PARTY
		CASE IHV_SCOPING_SMUGGLER
		CASE IHV_SCOPING_INTRO
			RETURN TRUE
		
	ENDSWITCH
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING Get_Send_Photo_Text_Label()
	IF SHOULD_SEND_PHOTO_TO_LESTER()
		RETURN "FHHUD_SENDLES"
	ENDIF
	IF SHOULD_SEND_PHOTO_TO_PAVEL()
		RETURN "CSH_PHONEC"
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_RUNNING_RANDOM_EVENT_TYPE(PLAYER_ID(), FMMC_TYPE_SIGHTSEEING)
	AND g_bTakenSightseeingPhoto
	   	RETURN "RE_SS_SNDOMG"
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	SWITCH GB_GET_TUNER_ROBBERY_PREP_PLAYER_IS_ON(PLAYER_ID())
		CASE TRV_METH_TANKER			
		CASE TRV_SCOPE_TRANSPORTER		
		CASE TRV_CONTAINER_MANIFEST		
		CASE TRV_TRAIN_SCHEDULE			
		CASE TRV_SEWER_SCHEMATICS
		CASE TRV_VAULT_KEY_CODES
		CASE TRV_LOCATE_BUNKER
			RETURN "TR_SESS_PHTO"
	ENDSWITCH
	#ENDIF
	
	#IF FEATURE_FIXER
	SWITCH GB_GET_FIXER_VIP_MISSION_PLAYER_IS_ON(PLAYER_ID())
		CASE FVV_LIMO
			RETURN "FXR_FRAN_PHTO"
		
		CASE FVV_YACHT
			RETURN "FXR_IMAN_PHTO"
	ENDSWITCH
	#ENDIF
	
	RETURN "CELL_287"
ENDFUNC


PROC SetUp_Lowrider_With_Send_Buttons()

    //Run if b_LowriderDisableElements is TRUE. See TODO 2513807


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)

    
    //From Left to Right as they appear on the screen.    
  


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
             "CELL_284") //Zoom


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
       GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
             "CELL_285") //Move   


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
        Get_Send_Photo_Text_Label()) //Send pic


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL),
        "CELL_281") //Exit


    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.


ENDPROC





PROC SetUp_TakePhoto_With_Send_Buttons()


    IF b_LowriderDisableElements = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - Setting up limited button set for With Send block as b_LowriderDisableElements is TRUE!")
            cdPrintnl()

        #endif

    
        SetUp_Lowrider_With_Send_Buttons()

        EXIT

    ENDIF







    IF b_LeftTriggerInstructionsOngoing = FALSE
    AND b_RightTriggerInstructionsOngoing = FALSE

    #if IS_DEBUG_BUILD
        cdPrintstring("appCamera - SetUp_TakePhoto_With_Send_Buttons() cued")
        cdPrintnl()
    #endif



    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)

    
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL),
        "CELL_281") //Exit
    


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
        Get_Send_Photo_Text_Label()) //Send pic


    IF b_InSelfieMode


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            OR ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))



                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
                OR IS_PLAYSTATION_PLATFORM()
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                ELSE
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                ENDIF



                IF Is_DLC_Compatible()

                    //Filter Choice
                    SetUp_Individual_Filter_Instructional_Button(3)


                    //Border Choice
                    SetUp_Individual_Border_Instructional_Button(4)
                        

                    
                    /* Not available in Selfie Mode.
                    IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                        //Depth of Field Toggle
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK))),  
                            "CELL_FOCUS") //Test 

                    ENDIF
                    */    
                                  
                ENDIF



                //No Selfie in vehicle.

            ELSE

                            //For 2037087, this section will need to put into a procedure.
                            IF g_bInMultiplayer = FALSE
                            AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) > 0    
                            AND Is_Current_Player_Ped_MTD_Compatible()

                                Setup_PI_Selfie_Mode_Instructional_Buttons()

                        
                            ELSE


                                    //

                                    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
                                    OR IS_PLAYSTATION_PLATFORM()
                                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                            "CELL_SP_2NP_XB") //Normal Mode (Press)
                                    ELSE
                                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                            "CELL_SP_2NP_XB") //Normal Mode (Press)
                                    ENDIF


                                    //Grid Lines Toggle
                                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
                                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
                                                "CELL_GRID") 
                                                
                                                
                                                                         

                                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
                                       GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                                        "CELL_285") //Move   


                                    IF Is_DLC_Compatible()

                                        //Filter Choice
                                        SetUp_Individual_Filter_Instructional_Button(5) 


                                        //Border Choice
                                        SetUp_Individual_Border_Instructional_Button(6)



                                        
                                           



                                        IF DOES_CURRENT_ON_FOOT_QUICKPLAY_HAVE_SELFIE()


                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("appCamera - MP Selfie Mode compatible Quickplay action detected. Displaying instructional label.")
                                                cdPrintnl()
                                            #endif
          


                                            //Play gesture action in MP. This is controlled by Neil Ferguson's existing MP work. He interrogates the button press. 
                                            //url:bugstar:1813354

                                            SetUp_Play_Action_Instructional_Button(7) //Needs to be to the right of "Expresion"

                                            /*Moved to head movement help text.
                                            //Expression Choice
                                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION),  
                                            "CELL_EXPRESS") //Test 
                                             */



                                            IF b_SelfieDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                                //Depth of Field Toggle
                                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                                    "CELL_DEPTH") //Test 

                                            ENDIF



                                        ELSE


                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("appCamera - 1. No MP Selfie Mode compatible Quickplay action detected. Not displaying instructional label.")
                                                cdPrintnl()
                                            #endif

                                            /*Moved to head movement help text.
                                            //Expression Choice
                                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 7, 
                                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION),  
                                            "CELL_EXPRESS") //Test 
                                            */


                                            IF b_SelfieDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.
                                                
                                                //Depth of Field Toggle
                                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 7, 
                                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                                    "CELL_DEPTH") //Test 

                                            ENDIF



                                        ENDIF



                                    ENDIF  //End of Is_DLC_Compatible()

                            ENDIF //Not in MP, PI_Menu script is running


                //No Zoom in Selfie Mode.

                /*
                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
                   GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD),  
                    "CELL_101") //Test  


                    SCRIPT_ASSERT("Test")
                */




            ENDIF

        ENDIF


    ELSE //In Normal Mode.


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            OR ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))


                
                //Replace LStick with GET_CONTROL_INSTRUCTIONAL_BUTTON(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
            
                IF NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
                


                    //Grid Lines Toggle
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
                            "CELL_GRID")


                       
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
                    GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                    "CELL_285") //Move   



                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
                    "CELL_284") //Zoom


                    IF Is_DLC_Compatible()
                    
                        //Filter Choice
                        SetUp_Individual_Filter_Instructional_Button(6)


                        //Border Choice
                        SetUp_Individual_Border_Instructional_Button(7)

                

                         //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
                         INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                            "CELL_DEPTH")

                    

                    ENDIF


                ELSE  //Player is parachuting... 



                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                    GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                    "CELL_285") //Move  


                    IF Is_DLC_Compatible()
                    
                        //Filter Choice
                        SetUp_Individual_Filter_Instructional_Button(3)


                        //Border Choice
                        SetUp_Individual_Border_Instructional_Button(4)

                
                        //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                            "CELL_DEPTH")


                    ENDIF


    
                ENDIF


            ELSE
            
               


                    //Replace LStick with GET_CONTROL_INSTRUCTIONAL_BUTTON(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
                
                    IF NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
						
						IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
	                        OR IS_PLAYSTATION_PLATFORM()
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
	                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
	                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
	                        ELSE
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
	                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
	                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
	                        ENDIF

						ENDIF
						
                        //Grid Lines Toggle
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
                            "CELL_GRID")


                        
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                            "CELL_285") //Move   

                        



                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 6,
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
                        "CELL_284") //Zoom
						
						
                        IF Is_DLC_Compatible()
							IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                            //Filter Choice
	                            SetUp_Individual_Filter_Instructional_Button(7)

	                            //Border Choice
	                            SetUp_Individual_Border_Instructional_Button(8)


	                                        
	                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

	                                //Depth of Field Toggle
	                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
	                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK),  
	                                    "CELL_FOCUS") //Test 

	                            ENDIF




	                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 9, 
	                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
	                                "CELL_DEPTH")
							ENDIF

                   


                     ELSE  //Player is parachuting. 


						IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                       	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
	                       	OR IS_PLAYSTATION_PLATFORM()
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
	                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
	                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
	                        ELSE
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
	                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
	                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
	                        ENDIF
						ENDIF
                        
                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
                        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                            "CELL_285") //Move   






                        IF Is_DLC_Compatible()
                        	IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                            //Filter Choice
	                            SetUp_Individual_Filter_Instructional_Button(4)


	                            //Border Choice
	                            SetUp_Individual_Border_Instructional_Button(5)

                            /*            
                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                //Depth of Field Toggle
                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 6, 
                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK),   
                                    "CELL_FOCUS") //Test 

                            ENDIF
                            */


	                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 6, 
	                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
	                                "CELL_DEPTH")

							ENDIF


                        ENDIF
       
                    ENDIF





                ENDIF

            ENDIF

        ENDIF


    ENDIF


                
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.



    ENDIF //b_LeftTriggerInstructionsOngoing and Right check.


ENDPROC


















PROC SetUp_TakePhoto_No_Send_Buttons()


    IF b_LowriderDisableElements = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("AppCamera - Setting up limited button set for No Send block as b_LowriderDisableElements is TRUE!")
            cdPrintnl()

        #endif

    
        SetUp_Lowrider_No_Send_Buttons()

        EXIT

    ENDIF




    IF b_LeftTriggerInstructionsOngoing = FALSE
    AND b_RightTriggerInstructionsOngoing = FALSE


    #if IS_DEBUG_BUILD
        cdPrintstring("appCamera - SetUp_TakePhoto_No_Send_Buttons() cued")
        cdPrintnl()
    #endif




    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)

    IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
	        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
	        "CELL_281") //Exit

	    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
	        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
	        "CELL_280") //Take Photo
	ELSE
		INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3,
	        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
	        "CELL_281") //Exit

	    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2,
	        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
	        "CELL_280") //Take Photo
	ENDIF	
		


    /*
    IF b_InSelfieMode


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_RS),  
            "CELL_SP_2NP_XBNP") //Normal Mode



        //Replace RStick with GET_CONTROL_INSTRUCTIONAL_BUTTON(PLAYER_CONTROL, INPUTGROUP_LOOK)

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
           GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
            "CELL_285") //Move   


        //No Zoom in Selfie Mode.


    ELSE



        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_RS),  
        "CELL_SP_1NP_XBNP") //Self Portrait Mode



        //Replace RStick with GET_CONTROL_INSTRUCTIONAL_BUTTON(PLAYER_CONTROL, INPUTGROUP_LOOK)

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
       GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
        "CELL_285") //Move   



        //Replace LStick with GET_CONTROL_INSTRUCTIONAL_BUTTON(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
    
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4,
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
        "CELL_284") //Zoom


    ENDIF
    */








    IF b_InSelfieMode


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            OR ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))


               IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 209267
               OR IS_PLAYSTATION_PLATFORM()
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                ELSE
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                ENDIF

                //No Selfie in vehicle.

            ELSE


                        //For 2037087, this section will need to put into a procedure.

                        IF g_bInMultiplayer = FALSE
                        AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) > 0    
                        AND Is_Current_Player_Ped_MTD_Compatible()

                            Setup_PI_Selfie_Mode_Instructional_Buttons()


                        ELSE


                                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
                                OR IS_PLAYSTATION_PLATFORM()
                                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                                ELSE
                                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                        "CELL_SP_2NP_XB") //Normal Mode (Press)
                                ENDIF



                                //Grid Lines Toggle
                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
                                    "CELL_GRID")


                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
                                GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                                    "CELL_285") //Move   


                                //No Zoom in Selfie Mode.


                                IF Is_DLC_Compatible()

                                    //Filter Choice
                                    SetUp_Individual_Filter_Instructional_Button(5)  

                            
                                    //Border Choice
                                    SetUp_Individual_Border_Instructional_Button(6)

                     
                                    
                                    IF DOES_CURRENT_ON_FOOT_QUICKPLAY_HAVE_SELFIE()
                                                            
                                        #if IS_DEBUG_BUILD
                                            cdPrintstring("appCamera - Selfie Mode compatible Quickplay action detected. Displaying instructional label.")
                                            cdPrintnl()
                                        #endif


                                        //Play gesture action in MP. This is controlled by Neil Ferguson's existing MP work. He interrogates the button press. 
                                        //url:bugstar:1813354

                                        SetUp_Play_Action_Instructional_Button(7) //Needs to be to the right of "Expresion"

                                        /*Moved to head movement help text.
                                        //Expression Choice
                                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION),  
                                        "CELL_EXPRESS") //Test 
                                        */




                                        IF b_SelfieDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                            //Depth of Field Toggle
                                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                                "CELL_DEPTH") //Test 

                                        ENDIF

                             


                                    ELSE
                                          
                                        #if IS_DEBUG_BUILD
                                            cdPrintstring("appCamera - 2. No MP Selfie Mode compatible Quickplay action detected. Not displaying instructional label.")
                                            cdPrintnl()
                                        #endif


                                        /*Moved to head movement help text.
                                        //Expression Choice
                                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 7, 
                                        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION),  
                                        "CELL_EXPRESS") //Test 
                                        */
                                          
                                                
                                        IF b_SelfieDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                            //Depth of Field Toggle
                                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 7, 
                                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                                "CELL_DEPTH") //Test 
                                                
                                        ENDIF

                                    ENDIF



            

                                ENDIF //End of Is_DLC_Compatible() check


                        ENDIF //Not in MP, PI_Menu is running.

            ENDIF

        ENDIF


    ELSE  //In Normal Mode


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            OR ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
              
            
                IF NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))


					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
						INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                   		GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                         "CELL_285") //Move   
					
                    	//Grid Lines Toggle
	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
                        "CELL_GRID")
						
						INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
	                         "CELL_284") //Zoom
					ELSE
						INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
                  		 GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                         "CELL_285") //Move   
						 
						IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)	
							INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
		                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_RS),
		                        "CELL_284") //Zoom
						ELSE
							INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
		                    GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_CURSOR_SCROLL),
		                        "CELL_284") //Zoom
						ENDIF
					ENDIF
						IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                        IF Is_DLC_Compatible()

	                            //Filter Choice
	                            SetUp_Individual_Filter_Instructional_Button(6)


	                            //Border Choice
	                            SetUp_Individual_Border_Instructional_Button(7)


	                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

	                                //Depth of Field Toggle
	                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
	                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK),   
	                                    "CELL_FOCUS") //Test 

	                            ENDIF


	                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
	                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
	                                "CELL_DEPTH")

	                        ENDIF
						ENDIF

                ELSE    //Player is parachuting...


                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                       GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                            "CELL_285") //Move   



                        IF Is_DLC_Compatible()
                        
                            //Filter Choice
                            SetUp_Individual_Filter_Instructional_Button(3)


                            //Border Choice
                            SetUp_Individual_Border_Instructional_Button(4)

                    
                                        
                            /*            
                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                //Depth of Field Toggle
                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK),   
                                    "CELL_FOCUS") //Test 

                            ENDIF
                            */


                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 8, 
                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                "CELL_DEPTH")




                        ENDIF


                ENDIF

                //No Selfie in vehicle.



            ELSE
            		IF NOT IS_LOCAL_PLAYER_USING_DRONE()
                        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B* 2092679
                        OR IS_PLAYSTATION_PLATFORM()
                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
                        ELSE
                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE),  
                                "CELL_SP_1NP_XB") //Self Portrait Mode (Press)
                        ENDIF

					ENDIF

         



                   
                IF NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
					
					
					
					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                   	//Grid Lines Toggle
	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 4, 
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID),  
	                        "CELL_GRID")
						
						INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
	                   	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
	                        "CELL_285") //Move   
					ELSE
							
						INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
	                   	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
	                        "CELL_285") //Move	
						
						IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)	
							INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
		                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_RS),
		                        "CELL_284") //Zoom
						ELSE
							INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
		                    GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_CURSOR_SCROLL),
		                        "CELL_284") //Zoom
						ENDIF
						
					ENDIF

					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 6,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SNIPER_ZOOM),
	                        "CELL_284") //Zoom
					ENDIF
					
						IF NOT IS_LOCAL_PLAYER_USING_DRONE()
	                        IF Is_DLC_Compatible()

	                            //Filter Choice
	                            SetUp_Individual_Filter_Instructional_Button(7)  

	                            
	                            //Border Choice
	                            SetUp_Individual_Border_Instructional_Button(8)


	                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

	                                //Depth of Field Toggle
	                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, 
	                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK),  
	                                    "CELL_FOCUS") //Test 

	                            ENDIF



	                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
	                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 9, 
	                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
	                                "CELL_DEPTH")
	                        ENDIF
						ENDIF

                ELSE    //Player is parachuting...


                        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, 
                        GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK),  
                            "CELL_285") //Move   



                        IF Is_DLC_Compatible()
                        
                            //Filter Choice
                            SetUp_Individual_Filter_Instructional_Button(3)


                            //Border Choice
                            SetUp_Individual_Border_Instructional_Button(4)

                    
                            /*            
                            IF b_NormalDepthOfFieldEffectAvailable //only display this if we are going ahead with it. Decided by true / false initialisation of this bool in this script.

                                //Depth of Field Toggle
                                INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK))),   
                                    "CELL_FOCUS") //Test 

                            ENDIF
                            */


                            //New Depth of Field Toggle from Alex, requested by 2282124 - not to be confused with FOCUS LOCK or SELFIE DOF. Use same label and control.
                            INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 5, 
                                GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF),  
                                "CELL_DEPTH")



                        ENDIF



                ENDIF

            ENDIF

        ENDIF


     ENDIF














    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()



    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.


    ENDIF

ENDPROC









PROC SetUp_Continue_FirstSave_With_Send_Buttons()

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)



    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE )
    //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE) //Removed for 1932325 - hard drive save now used.
    //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
    //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE)


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
            Get_Send_Photo_Text_Label()) //Send pic

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
            "CELL_286") //"Continue" 

    ELSE


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
            Get_Send_Photo_Text_Label()) //Send pic


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
            "CELL_277") //"Delete" ( essentially a continue without saving... not my idea!

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
            "CELL_GALSAVE") //"Upload / Gallery save" 



    ENDIF


    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()


    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.

ENDPROC









PROC SetUp_Continue_FirstSave_No_Send_Buttons()

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE )
    //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE) //Removed for 1932325 - hard drive save now used.
    //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
    //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE)


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
            "CELL_286") // "Continue"

    ELSE

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
            "CELL_277") //"Delete" ( essentially a continue without saving... not my idea!

        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
            GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),"CELL_GALSAVE") //"Upload / Gallery save"

                    
        //INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
            //"CELL_281") //Exit

    ENDIF
    
   
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()


    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.

ENDPROC









PROC SetUp_Continue_SaveAgain_With_Send_Buttons()

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
        "CELL_276") //Save Again

    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
        Get_Send_Photo_Text_Label()) //Send pic

    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
       GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),
        "CELL_GALSAVE") //"Upload / Gallery save"

    
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
        "CELL_281") //Exit
    

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()

  
    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.

ENDPROC









PROC SetUp_Continue_SaveAgain_No_Send_Buttons()

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
        "CELL_276") //Save Again

    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT),
        "CELL_GALSAVE") //"Upload / Gallery save"
    
    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, 
        GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),
        "CELL_281") //Exit
    
   

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
    END_SCALEFORM_MOVIE_METHOD()


    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.

ENDPROC


















PROC Draw_Shutter_Movie_With_Status_Check()


    IF g_Cellphone.PhoneDS > PDS_AWAY //We don't want the shutter movie to be drawn as an overlay if the phone is in "going away" state.  See bug 303654
    
        IF NOT (IS_PAUSE_MENU_ACTIVE())
            DRAW_SCALEFORM_MOVIE (SF_Movie_Gallery_Shutter_Index, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)
        ENDIF

    ENDIF


ENDPROC





//#if IS_DEBUG_BUILD          
PROC Temp_Setup_Of_Test_Gallery_Data()


    //Have to truncate the html so that the text_label_63 error surpassed assert doesnae fire...
    //e.g       g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_ThumbLabel = "<img src='thumb_1' vspace='0' hspace='0' width='106' height='106'>"
    //becomes   g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_ThumbLabel = "<img src='thumb_1' width='106' height='106'>"


    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_ThumbLabel = "<img src='thumb_1' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[1].GalleryImage_ThumbLabel = "<img src='thumb_2' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[2].GalleryImage_ThumbLabel = "<img src='thumb_3' width='106' height='106'>"

    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[3].GalleryImage_ThumbLabel = "<img src='thumb_4' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[4].GalleryImage_ThumbLabel = "<img src='thumb_5' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[5].GalleryImage_ThumbLabel = "<img src='thumb_6' width='106' height='106'>"

    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[6].GalleryImage_ThumbLabel = "<img src='thumb_7' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[7].GalleryImage_ThumbLabel = "<img src='thumb_8' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[8].GalleryImage_ThumbLabel = "<img src='thumb_9' width='106' height='106'>"




    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_ThumbLabel = "<img src='thumb_1' vspace='0' hspace='0' width='106' height='106'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[2].GalleryImage_ThumbLabel = "<img src='thumb_3' vspace='0' hspace='0' width='106' height='106'>"


    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_PhotoLabel  =  "<img src='photo_1' vspace='0' hspace='0' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[1].GalleryImage_PhotoLabel  =  "<img src='photo_2' vspace='0' hspace='0' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[2].GalleryImage_PhotoLabel  =  "<img src='photo_3' vspace='0' hspace='0' width='1280' height='720'>"

    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[0].GalleryImage_PhotoLabel  =  "<img src='photo_1' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[1].GalleryImage_PhotoLabel  =  "<img src='photo_2' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[2].GalleryImage_PhotoLabel  =  "<img src='photo_3' width='1280' height='720'>"

    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[3].GalleryImage_PhotoLabel  =  "<img src='photo_4' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[4].GalleryImage_PhotoLabel  =  "<img src='photo_5' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[5].GalleryImage_PhotoLabel  =  "<img src='photo_6' width='1280' height='720'>"

    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[6].GalleryImage_PhotoLabel  =  "<img src='photo_7' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[7].GalleryImage_PhotoLabel  =  "<img src='photo_8' width='1280' height='720'>"
    //g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[8].GalleryImage_PhotoLabel  =  "<img src='photo_9' width='1280' height='720'>"

ENDPROC
//#endif
















PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
    
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF

    ENDIF



    IF dpad_scroll_pause_cued = FALSE

        /*
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_LEFT_INPUT))
        
            IF GalleryCursorIndex > 0
                
                GalleryCursorIndex --

            ELSE

                GalleryCursorIndex = (NumberOfImagesInList - 1)

            ENDIF   
        
            //Call_Scaleform_Input_Keypress_Up()

            
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "DISPLAY_VIEW", 0, TO_FLOAT(GalleryCursorIndex)) //Set display view to gallery and highlight slot 1



            BEGIN_SCALEFORM_MOVIE_METHOD (SF_Movie_Gallery_Shutter_Index, "DISPLAY_PHOTO")

               
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[ImageListSlot[GalleryCursorIndex]].GalleryImage_PhotoLabel)
                END_TEXT_COMMAND_SCALEFORM_STRING()

            END_SCALEFORM_MOVIE_METHOD()





            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_RIGHT_INPUT))

            GalleryCursorIndex ++

                 
            //GalleryCursor starts at 0.
            IF GalleryCursorIndex = NumberOfImagesInList 

                GalleryCursorIndex = 0

            ENDIF

            
            //Call_Scaleform_Input_Keypress_Down()

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "DISPLAY_VIEW", 0, TO_FLOAT(GalleryCursorIndex)) //Set display view to gallery and highlight slot 1

            BEGIN_SCALEFORM_MOVIE_METHOD (SF_Movie_Gallery_Shutter_Index, "DISPLAY_PHOTO")

               
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[ImageListSlot[GalleryCursorIndex]].GalleryImage_PhotoLabel)
                END_TEXT_COMMAND_SCALEFORM_STRING()

            END_SCALEFORM_MOVIE_METHOD()






            dpad_scroll_pause_cued = TRUE
            SETTIMERA (0)

           
            
        ENDIF
        */

    
    ENDIF

            
ENDPROC

























/*
PROC Place_Images_in_GallerySlots()


    NumberOfImagesInList = 0 //reset number of Repeats before filling list.

   
    INT slotIndex = 0



       
            WHILE slotIndex < (MAX_GALLERY_IMAGES - 1)

                #if IS_DEBUG_BUILD

                    PRINTSTRING("Populating sf gallery slot ")
                    PRINTINT(slotIndex)
                    PRINTNL()
            
                #endif         


              

                BEGIN_SCALEFORM_MOVIE_METHOD (SF_Movie_Gallery_Shutter_Index, "SET_DATA_SLOT")

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotIndex)

                  
                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_SavedGlobals.sGalleryImageSavedData.g_GalleryImage[slotIndex].GalleryImage_ThumbLabel)
                    END_TEXT_COMMAND_SCALEFORM_STRING()

                END_SCALEFORM_MOVIE_METHOD()


                NumberOfImagesInList ++ 

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCAMERA - GALLERY - NumberOfImagesInList = ")
                    cdPrintInt(NumberOfImagesInList)
                    cdPrintnl()

                #endif


         
               


                ImageListSlot[slotIndex] = slotIndex


                //Temp unreferenced hack
                IF ImageListSlot[slotIndex] = 1

                    i_SelectedImage = ImageListSlot[slotIndex]

                    IF i_SelectedImage = 1
                    ENDIF

                ENDIF


            

                slotIndex ++



            ENDWHILE

      

     
ENDPROC
*/

























PROC Do_Fullscreen_Reposition()

    DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)

    b_ManipulatePhonePosX = TRUE
    b_ManipulatePhonePosY = TRUE
    b_ManipulatePhonePosZ = TRUE


    b_Reposition_Corner = FALSE
    b_Reposition_Fullscreen = TRUE

    //CELL_CAM_ACTIVATE (TRUE, TRUE)
    

ENDPROC



PROC Do_Corner_Reposition()


    b_ManipulatePhonePosX = TRUE
    b_ManipulatePhonePosY = TRUE
    b_ManipulatePhonePosZ = TRUE


    b_Reposition_Corner = TRUE
    b_Reposition_Fullscreen = FALSE


    g_TempCamPosvec = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]  //Work on 2200121 - Makes sure we are passing in a properly defined position.

    HIDE_ACTIVE_PHONE (FALSE, TRUE)    //Work on 2200121   - Removing this means the picture is blank.





    Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)


    Local_PPE_Toggle (FALSE)  //Fix this!

    b_VehicleRenderCheck = FALSE

    Clear_Modifier_With_Exception_Checks() //#1431423

    //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])



    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("Cell Cam FALSE 1")
        PRINTNL()

        cdPrintnl()
        cdPrintstring("DEBUG - SIGNAL Checking Cellphone Camera Corner Reposition.")
        cdPrintnl()

                    cdPrintnl()
                    cdPrintstring("Cam DE-activated 2")
                    cdPrintnl()

    #endif


    
    //Help text to notify player that they cannot save a photo due to a Network Sharing block on 360. #1644121
    IF NOT IS_BIT_SET(BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE) //No saving is allowed on certain missions that set this bit...

        IF (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE)
        //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
        //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE)

            IF IS_XBOX360_VERSION() 
                IF b_360_Priv_Help_Done = FALSE
                
                    //PRINT_HELP ("CELL_360_PRIV")  //Removed for 1932325 - hard drive save now used.

                    b_360_Priv_Help_Done = TRUE

                ENDIF
            ENDIF

        ENDIF

    ENDIF


ENDPROC




PROC Setup_Cam_Instructional_Buttons()



	//Added for url:bugstar:3527162
	IF g_FMMC_STRUCT.bSecurityCamActive = TRUE
	AND DOES_CURRENT_MISSION_USE_GANG_BOSS()
		
		
		#IF IS_DEBUG_BUILD
		
			cdPrintnl()
            cdPrintstring("AppCamera - WARNING! Setup_Cam_Instructional_Buttons was called but exiting as g_FMMC_STRUCT.bSecurityCamActive is TRUE!")
            cdPrintnl()
			
		#ENDIF
		
		EXIT
	
	
	ENDIF



    IF b_WaitingForDiscardOrSend = FALSE


            #if IS_DEBUG_BUILD

                cdPrintstring("SetUp_TakePhoto_Buttons cued 2.")
                cdPrintnl()

            #endif
    

      
     
        IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
        
            TokenDecision = "CELL_296"

            SetUp_TakePhoto_With_Send_Buttons()


            //[CELL_296]
            //~s~Send Photo(s) ~INPUT_CELLPHONE_EXTRA_OPTION~~n~Take Photo ~INPUT_CELLPHONE_SELECT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~


        ELSE

            TokenDecision = "CELL_295"

            SetUp_TakePhoto_No_Send_Buttons()

            //[CELL_295]
            //~s~Take Photo ~INPUT_CELLPHONE_SELECT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~



        ENDIF

        

        

       
        /*Replaced by instructional buttons

        
        HELP_AT_SCREEN_LOCATION (TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)


        IF IS_HELP_MESSAGE_BEING_DISPLAYED()
            HELP_AT_SCREEN_LOCATION(TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY,  HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
        ELSE
            HELP_AT_SCREEN_LOCATION(TokenDecision, f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
        ENDIF

        */


        //Unref bypass. May have to go back to these. It depends on the wind.
        IF ARE_STRINGS_EQUAL (TokenDecision, "DUMMYCOMPARISON")

             f_SecondaryHelpX = f_SecondaryHelpY
             f_PrimaryHelpX = f_PrimaryHelpY

             f_SecondaryHelpY = f_SecondaryHelpX
             f_PrimaryHelpY = f_PrimaryHelpX

        ENDIF


        Update_Help_State = 1

        



        
        


    ELSE

      
        /* Legacy instruction buttons using movie.

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
            "CELL_281") //Exit
        
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION))),
            Get_Send_Photo_Text_Label()) //Send pic
        
        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
            "CELL_286") //Continue / old discard.

        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y))), -1, -1, -1, "CELL_284") //Zoom



        INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION))),
            "CELL_271") //Save photo variant Gallery



        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_BACKGROUND_COLOUR", 0, 0, 0, 80, -1)
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 1)


        */


    ENDIF



ENDPROC






PROC Disable_Camera_Movement_Controls()


    //Fix for 2156297. Close small window of opportunity whilst phone is rotating into landscape.
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM) 



    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER) //Added by request of bug 1709149 


    /*
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)

    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT)
    */



    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)                                                                   
                                                                   
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)

    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_SNIPER_ZOOM)



    
    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_WEAPON) //2196217

 
    //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "Disabling Snipe controls")



ENDPROC





PROC Disable_Vehicle_Controls()
    


    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)  //#2048401 - Too tricky /dangerous to have phone transition up / down screen so blocking gameplay camera switch.
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_WEAPON)  //2196217


    IF b_InSaveRoutine = FALSE  //Allow vehicle exit and acomn whilst saving image #1435544



        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT)))
        
        
        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))) //See bug 896700. Strange one, as flashhand is already doing this.


        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_UP)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_UP)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK2)

        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK) //1984437

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_BRAKE)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_EXIT)
        
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_DUCK)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HOTWIRE_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HOTWIRE_RIGHT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HORN)
  
        //Necessary to disable heli / flight controls. See bug 495788
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_UD)

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_UP_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT_ONLY)



        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)


        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LEFT_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_RIGHT_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_DOWN_ONLY)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)


        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)



        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_AIM) //2340387 fix.



        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_JUMP)



        //2184646 - attempt to fix "Lean forward" conflict issue whilst on racing pushbikes.
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_SPRINT)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_FRONT_BRAKE)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_REAR_BRAKE)

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_JUMP)


        //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "Disabling Veh controls")



    ENDIF

ENDPROC











PROC LaunchSaveRoutine_in_Controller()
      
        b_InSaveRoutine = TRUE

        g_b_appHasRequestedSave = TRUE

        CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

        
        /* Removed - Gareth suggests the revolving spinner should be enough to satisfy TRC requirements over saving.
        TokenDecision = "CELL_292"  //g_i_PhotoSlot is available for use here...

       
        IF IS_HELP_MESSAGE_BEING_DISPLAYED()
            HELP_AT_SCREEN_LOCATION (TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY,  HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
        ELSE
            HELP_AT_SCREEN_LOCATION (TokenDecision, f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
        ENDIF
        */

        


ENDPROC












PROC Adjust_Help_State()

    SWITCH Update_Help_State

        CASE 1

            CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

            #if IS_DEBUG_BUILD

                cdPrintstring("SetUp_TakePhoto_Buttons cued 1.")
                cdPrintnl()

            #endif
    


            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
            
                TokenDecision = "CELL_296"

                SetUp_TakePhoto_With_Send_Buttons()


            ELSE

                TokenDecision = "CELL_295"

                SetUp_TakePhoto_No_Send_Buttons()

            ENDIF


            /*Replaced by instructional buttons

            IF IS_HELP_MESSAGE_BEING_DISPLAYED()
                HELP_AT_SCREEN_LOCATION(TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
            ELSE
                HELP_AT_SCREEN_LOCATION(TokenDecision, f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
            ENDIF

            */

        BREAK


        CASE 2
            
            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)  //Only display help text for standard use...


                CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)



                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
            
                    TokenDecision = "CELL_294"

                    SetUp_Continue_FirstSave_With_Send_Buttons()

                ELSE

                    TokenDecision = "CELL_293"


                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DEBUG_SEGMENT 1")
                        cdPrintnl()

                    #endif

                    SetUp_Continue_FirstSave_No_Send_Buttons()

                ENDIF


                /*Replaced by instructional buttons

                IF IS_HELP_MESSAGE_BEING_DISPLAYED()
                    HELP_AT_SCREEN_LOCATION(TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
                ELSE
                    HELP_AT_SCREEN_LOCATION(TokenDecision, f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
                ENDIF

                */

                Update_Help_State = 2

            ENDIF

        BREAK


        DEFAULT

            //do nothing as yet...  

        BREAK


    ENDSWITCH
        

ENDPROC





PROC SetUpScanlines() 

    /*
    IF g_i_CurrentlySelected_SnapMaticBorder = 0 //Make sure we are in No Border state before adding scanlines.

        SET_TIMECYCLE_MODIFIER("phone_cam") //with no border, re-establish scan lines. #1681700

    ENDIF
    */


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_REMAINING_PHOTOS", 1)

    IF NOT Is_DLC_Compatible()

        IF NOT g_bFMMCLightsTurnedOff //If this is F v J mode, we don't want to mess with any existing F v J timecycle set up. 2515958

            SET_TIMECYCLE_MODIFIER("phone_cam") //with no border, re-establish scan lines. #1681700

        ENDIF
            
    ENDIF
    

ENDPROC




PROC SetUpCurrentTCModifier()


    IF g_i_CurrentlySelected_SnapMaticFilter > 0 //Make sure we don't add the "No Filter" state
    AND g_i_CurrentlySelected_SnapMaticFilter < 99 //Make sure we don't add the "No Filter" state

        IF NOT g_bFMMCLightsTurnedOff //If this is F v J mode, we don't want to mess with any existing F v J timecycle set up. 2515958
            
            SET_TIMECYCLE_MODIFIER (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

        ENDIF

    ENDIF


ENDPROC



PROC CheckForBadDoF_FilterCombination()

        //If a bad filter DoF combination exists on changing the filter, switch off the DoF. Leave the toggle on so that the next time a filter swaps, the DoF can be re-established.
        IF b_SelfieMode_DepthOfField_ToggledOn = TRUE

            //Filter 12 now works with DOF after update by Thomas D's changes in bug 2108712, Nov 2014. Dummied string comparison for safety.
            IF ARE_STRINGS_EQUAL (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], "phone_cam12DUMMY")
            //OR ARE_STRINGS_EQUAL (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], "phone_cam3")

                CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(FALSE)   //Need to remove these combinations see url:bugstar:1706169
                SET_MOBILE_PHONE_DOF_STATE(FALSE)

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Clearing DoF Effect for Bad Filter Combination")
                    cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                    cdPrintnl()

                #endif

            ELSE

                CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(TRUE)   //Need to remove these combinations see url:bugstar:1706169
                SET_MOBILE_PHONE_DOF_STATE(TRUE)

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Re-establishing DoF Effect for Good Filter Combination")
                    cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                    cdPrintnl()

                #endif

            ENDIF

        ENDIF


ENDPROC

//**********selfie camera variables originally implemented by Lawrence Kerr. Steve Taylor now looking after. **********

//Debug Widgets found in RAG - Mobile Phone - Camera - Selfie Framing Params.
//Default offset values stipulated by bug #2034209


//scale range 0.0 to 1.0
FLOAT current_horizontal_pan_scale = 0.5
FLOAT current_vertical_pan_scale = 0.850
FLOAT current_distance_scale = 0.5



//scale range -1.0 to 1.0
FLOAT current_roll_scale = -0.250


//**********selfie head variables**********

//scale range -1.0 to 1.0
FLOAT current_head_yaw_scale = 0.250
FLOAT current_head_roll_scale = 0.3
FLOAT current_head_pitch_scale = 0.3


FLOAT selfie_speed = 0.075//0.05




PROC Set_Up_Selfie_PitchandHeading()    

    IF b_InSelfieMode

        SET_GAMEPLAY_CAM_RELATIVE_PITCH (f_GameplayCamRelativePitch)
        
        SET_GAMEPLAY_CAM_RELATIVE_HEADING (f_GameplayCamRelativeHeading)

        #if IS_DEBUG_BUILD

            cdPrintstring ("AppCamera - Assigning Selfie Offset Values")
            cdPrintnl()

        #endif


    ENDIF

ENDPROC





PROC Set_Up_Pre_Picture_Taken_Selfie_PitchandHeading() 

    //IF b_InSelfieMode
    
        SET_GAMEPLAY_CAM_RELATIVE_PITCH (f_LastSelfieRelativePitch)
        
        SET_GAMEPLAY_CAM_RELATIVE_HEADING (f_LastSelfieRelativeHeading)


        #if IS_DEBUG_BUILD

            cdPrintstring ("AppCamera - Assigning Last Pre-Picture Taken Selfie Offset Values")
            cdPrintnl()

        #endif

    
    //ENDIF

ENDPROC








PROC Initialise_Selfie_Offset_Values()





    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

        IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())


            SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)

            #if IS_DEBUG_BUILD

                cdPrintstring ("AppCamera - Player was using stealth movement on launch of Snapmatic. Restoring normal movement. #2079800")
                cdPrintnl()

            #endif


            CELL_CAM_SET_SELFIE_MODE_HORZ_PAN_OFFSET (current_horizontal_pan_scale)
            CELL_CAM_SET_SELFIE_MODE_VERT_PAN_OFFSET (current_vertical_pan_scale)
            CELL_CAM_SET_SELFIE_MODE_DISTANCE_SCALING (current_distance_scale)

        ELSE

            CELL_CAM_SET_SELFIE_MODE_HORZ_PAN_OFFSET (current_horizontal_pan_scale)
            CELL_CAM_SET_SELFIE_MODE_VERT_PAN_OFFSET (current_vertical_pan_scale)
            CELL_CAM_SET_SELFIE_MODE_DISTANCE_SCALING (current_distance_scale)

        ENDIF

    ENDIF

    CELL_CAM_SET_SELFIE_MODE_ROLL_OFFSET (current_roll_scale)

    CELL_CAM_SET_SELFIE_MODE_HEAD_YAW_OFFSET (current_head_yaw_scale)
    CELL_CAM_SET_SELFIE_MODE_HEAD_ROLL_OFFSET (current_head_roll_scale)
    CELL_CAM_SET_SELFIE_MODE_HEAD_PITCH_OFFSET (current_head_pitch_scale) 


    #if IS_DEBUG_BUILD

        cdPrintstring ("AppCamera - Initialising Selfie Offset Values")
        cdPrintnl()

    #endif

    Set_Up_Selfie_PitchandHeading() 

ENDPROC










/// PURPOSE: increments the lateral / horizontal pan scale with the +/- addition. 
///          Clamps the value bettwen 0.0 to 1.0.                                   
///          updates the selfie mode horezontal pan                 
PROC update_current_horizontal_pan_scale(FLOAT addition)

                
    current_horizontal_pan_scale += addition
    
    
    if current_horizontal_pan_scale > 2.0
    
        current_horizontal_pan_scale = 2.0
    
    elif current_horizontal_pan_scale < -1.7
        
        current_horizontal_pan_scale = -1.7
        
    endif 
    
    

    CELL_CAM_SET_SELFIE_MODE_HORZ_PAN_OFFSET(current_horizontal_pan_scale)
    
    #if IS_DEBUG_BUILD
        //DISPLAY_TEXT_WITH_LITERAL_STRING (0.50, 0.50, "STRING", "H_pan") //Pish

        //DISPLAY_TEXT_WITH_FLOAT (0.8, 0.1, "CELL_500", current_horizontal_pan_scale, 3)
    #endif
    
ENDPROC

 





/// PURPOSE: increments the lateral / horizontal pan scale with the +/- addition. 
///          Clamps the value bettwen 0.0 to 1.0.                                   
///          updates the selfie mode horezontal pan 
PROC update_current_vertical_pan_scale(FLOAT addition)

    current_vertical_pan_scale += addition
    
    
    if current_vertical_pan_scale > 1.5
    
        current_vertical_pan_scale = 1.5
    
    elif current_vertical_pan_scale < 0.5
        
        current_vertical_pan_scale = 0.5
        
    endif 
   
    
    CELL_CAM_SET_SELFIE_MODE_VERT_PAN_OFFSET(current_vertical_pan_scale)

    #if IS_DEBUG_BUILD
        //DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "V_pan") //Pish

        //DISPLAY_TEXT_WITH_FLOAT (0.9, 0.2, "CELL_501", current_vertical_pan_scale, 3)
    #endif


ENDPROC 





PROC update_current_roll_scale(FLOAT addition)

    current_roll_scale += addition
    
    if current_roll_scale > 1.0
    
        current_roll_scale = 1.0
    
    elif current_roll_scale < -1.0
        
        current_roll_scale = -1.0
        
    endif 
    
    CELL_CAM_SET_SELFIE_MODE_ROLL_OFFSET(current_roll_scale)

ENDPROC 
            
            
            
            
                
PROC update_current_distance_scale(FLOAT addition)

    current_distance_scale += addition
    
    if current_distance_scale > 1.0
    
        current_distance_scale = 1.0
    
    elif current_distance_scale < 0.0
        
        current_distance_scale = 0.0
        
    endif 
    
    CELL_CAM_SET_SELFIE_MODE_DISTANCE_SCALING(current_distance_scale)
    
ENDPROC 





PROC update_current_head_pitch_scale(FLOAT addition)

    current_head_pitch_scale += addition
    
    if current_head_pitch_scale > 1.0
    
        current_head_pitch_scale = 1.0
    
    elif current_head_pitch_scale < -1.0
        
        current_head_pitch_scale = -1.0
        
    endif 
    
    CELL_CAM_SET_SELFIE_MODE_HEAD_PITCH_OFFSET(current_head_pitch_scale)

ENDPROC 




                
PROC update_current_head_roll_scale(FLOAT addition)

    current_head_roll_scale += addition
    
    if current_head_roll_scale > 1.0
    
        current_head_roll_scale = 1.0
    
    elif current_head_roll_scale < -1.0
        
        current_head_roll_scale = -1.0
        
    endif 
    
    CELL_CAM_SET_SELFIE_MODE_HEAD_ROLL_OFFSET(current_head_roll_scale)

ENDPROC 
           
           
           
           
                
PROC update_current_head_yaw_scale(FLOAT addition)

    current_head_yaw_scale += addition
    
    if current_head_yaw_scale > 1.0
    
        current_head_yaw_scale = 1.0
    
    elif current_head_yaw_scale < -1.0
        
        current_head_yaw_scale = -1.0
        
    endif 
    
    CELL_CAM_SET_SELFIE_MODE_HEAD_YAW_OFFSET(current_head_yaw_scale)

ENDPROC 




/// PURPOSE: controls the selfie camera for setting:
///             - lateral pan (left / right)
///             - vertical pan (up down)
///             - distance  (push further and back)
///             - roll  (right left limited to +- 10 degrees)
PROC selfie_camera_system()

    INT left_stick_x
    INT left_stick_y
    INT right_stick_x
    INT right_stick_y
    
    FLOAT left_stick_addition_x
    FLOAT left_stick_addition_y
    
    FLOAT right_stick_addition_x
    FLOAT right_stick_addition_y
    
    FLOAT fMouseX
    FLOAT fMouseY
    
    //lt = INPUT_AIM
    //rt = INPUT_VEH_ACCELERATE
    
    CONTROL_ACTION caSelfieCameraMoveCameraMode,  caSelfieCameraHeadMode
    
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        caSelfieCameraMoveCameraMode = INPUT_CELLPHONE_EXTRA_OPTION
        caSelfieCameraHeadMode       = INPUT_CELLPHONE_OPTION

        //Adding in this routine to fix 2171037. It's excluded from running by Lawrence's IF AND NOT clause below as caSelfieCameraHeadMode also uses INPUT_CELLPHONE_OPTION.
        IF IS_CONTROL_PRESSED (frontend_control, caSelfieCameraMoveCameraMode)
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION)
			AND NOT IS_LOCAL_PLAYER_USING_DRONE()     
                CELL_CAM_SET_SELFIE_MODE_HORZ_PAN_OFFSET (0.5)
                CELL_CAM_SET_SELFIE_MODE_VERT_PAN_OFFSET (0.85)
                CELL_CAM_SET_SELFIE_MODE_DISTANCE_SCALING (0.5)
                CELL_CAM_SET_SELFIE_MODE_ROLL_OFFSET (-0.250)


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppCamera - selfie camera tweak routine requested camera reset from PC keyboard mouse control.")
                    cdPrintnl()

                #endif


                current_horizontal_pan_scale = 0.5
                current_vertical_pan_scale = 0.850
                current_distance_scale = 0.5
                current_roll_scale = -0.250

            ENDIF
        ENDIF



    ELSE
        caSelfieCameraMoveCameraMode = input_script_lt
        caSelfieCameraHeadMode       = input_script_rt
    ENDIF
    
    if is_control_pressed(frontend_control, caSelfieCameraMoveCameraMode)
    and NOT is_control_pressed(frontend_control, caSelfieCameraHeadMode)


        //Add in reset camera functionality for 2158276.
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION)
		AND NOT IS_LOCAL_PLAYER_USING_DRONE()     
            CELL_CAM_SET_SELFIE_MODE_HORZ_PAN_OFFSET (0.5)
            CELL_CAM_SET_SELFIE_MODE_VERT_PAN_OFFSET (0.85)
            CELL_CAM_SET_SELFIE_MODE_DISTANCE_SCALING (0.5)
            CELL_CAM_SET_SELFIE_MODE_ROLL_OFFSET (-0.250)


            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppCamera - selfie camera tweak routine requested camera reset from joypad.")
                cdPrintnl()

            #endif


            current_horizontal_pan_scale = 0.5
            current_vertical_pan_scale = 0.850
            current_distance_scale = 0.5
            current_roll_scale = -0.250

        ENDIF




        
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
        
        left_stick_x =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR )) * 127 
        left_stick_y =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD )) * 127 
            
        right_stick_x = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR )) * 127 
        right_stick_y = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD )) * 127
        
        
        // Move camera using mouse
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
            // Use sniper zoom for zoom in/out on PC.
            left_stick_y =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SNIPER_ZOOM )) * 127
        
            fMouseX =  GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)
            fMouseY =  GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)
            
            // Cancel out any mouse inversion.
            IF IS_MOUSE_LOOK_INVERTED()
                fMouseY *= -1
            ENDIF
            
            right_stick_addition_x += fMouseX
            right_stick_addition_y -= fMouseY
            
        ELSE
        
            //if absi(right_stick_x) > 28 or absi(right_stick_y) > 28
            if absi(right_stick_x) > 15 or absi(right_stick_y) > 15//2257502

        
                right_stick_addition_x = (right_stick_x / 128.0) * selfie_speed
                right_stick_addition_y = (-right_stick_y / 128.0) * selfie_speed
                
            endif
            
        ENDIF
        
        if absi(left_stick_x) > 28 or absi(left_stick_y) > 28
            
            left_stick_addition_x = (left_stick_x / 128.0) * selfie_speed
            left_stick_addition_y = (left_stick_y / 128.0) * selfie_speed
            
        endif 
            
        update_current_horizontal_pan_scale(right_stick_addition_x)
        
        update_current_vertical_pan_scale(right_stick_addition_y)
        
        update_current_roll_scale(left_stick_addition_x)
        
        


        IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //2220336 - Can't allow selfie mode here.
    
            update_current_distance_scale(left_stick_addition_y)
                 
        ENDIF



    
    ELIF NOT is_disabled_control_pressed(frontend_control, caSelfieCameraHeadMode)
    
        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
        
    endif 


    //Rob N. requested that zoom can operate in Selfie Mode without holding RT. #2037087
    IF  (NOT is_disabled_control_pressed(frontend_control, caSelfieCameraMoveCameraMode))
    AND (NOT is_disabled_control_pressed(frontend_control, caSelfieCameraHeadMode))

        left_stick_x =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR )) * 127 
        left_stick_y =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD )) * 127 
            
        right_stick_x = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR )) * 127 
        right_stick_y = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD )) * 127


        right_stick_x = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LEFT_ONLY)) * 127 
        right_stick_x = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_RIGHT_ONLY)) * 127 

        right_stick_y = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UP_ONLY )) * 127
        right_stick_y = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_DOWN_ONLY )) * 127


   
        
        IF ABSI(right_stick_x) > 28 OR ABSI(right_stick_y) > 28

            right_stick_addition_x = (right_stick_x / 128.0) * selfie_speed
            right_stick_addition_y = (-right_stick_y / 128.0) * selfie_speed
            
        ENDIF 
        
        IF ABSI(left_stick_x) > 28 or ABSI(left_stick_y) > 28
            
            left_stick_addition_x = (left_stick_x / 128.0) * selfie_speed
            left_stick_addition_y = (left_stick_y / 128.0) * selfie_speed
            
        ENDIF 
                
        
        IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //2220336 Can't allow selfie mode zoom in director mode.
    
            update_current_distance_scale(left_stick_addition_y)
     
        ENDIF

     ENDIF



    
ENDPROC 






/// PURPOSE: controls the players head during the selfie camera system
///           Head Yaw = left/right relative to camera
//            Head Roll  = left/right relative to camera
//            Head pitch = up/down relative to camera
PROC selfie_player_head_system()

    INT left_stick_x
    INT left_stick_y
    INT right_stick_x
    INT right_stick_y
    
    FLOAT left_stick_addition_x
    FLOAT left_stick_addition_y
    
    FLOAT right_stick_addition_x
    
    FLOAT fMouseX
    FLOAT fMouseY
    
    CONTROL_ACTION caSelfieCameraMoveCameraMode,  caSelfieCameraHeadMode
    
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        caSelfieCameraMoveCameraMode = INPUT_CELLPHONE_EXTRA_OPTION
        caSelfieCameraHeadMode       = INPUT_SPRINT
    ELSE
        caSelfieCameraMoveCameraMode = input_script_lt
        caSelfieCameraHeadMode       = input_script_rt
    ENDIF

    if is_control_pressed(frontend_control, caSelfieCameraHeadMode) and NOT is_control_pressed(frontend_control, caSelfieCameraMoveCameraMode)
        
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
        
        left_stick_x =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR )) * 127 
        left_stick_y =  FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD )) * 127 
            
        right_stick_x = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR )) * 127 
        right_stick_y = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD )) * 127
   
    
        // Using movement keys here to tilt the head, so we can use mouse for head movement.
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
            if absi(left_stick_x) > 28 or absi(left_stick_y) > 28
            
                right_stick_addition_x = (left_stick_x / 128.0) * selfie_speed
            
            endif 
                    
        ELSE
   
            if absi(right_stick_x) > 28 or absi(right_stick_y) > 28
            
                right_stick_addition_x = (right_stick_x / 128.0) * selfie_speed
                //right_stick_addition_y = (-right_stick_y / 128.0) * selfie_speed
            
            endif 
            
        ENDIF
        
        // We're using the mouse here to do rotate the head - feels more natural.
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

            fMouseX =  GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)
            fMouseY =  GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)
            
            // Cancel out any mouse inversion.
            IF IS_MOUSE_LOOK_INVERTED()
                fMouseY *= -1
            ENDIF
            
            left_stick_addition_x += fMouseX
            left_stick_addition_y -= fMouseY
        
        ELSE
            
            if absi(left_stick_x) > 28 or absi(left_stick_y) > 28
            
                left_stick_addition_x = (left_stick_x / 128.0) * selfie_speed
                left_stick_addition_y = (-left_stick_y / 128.0) * selfie_speed
                
            endif 
            
        ENDIF

                
        update_current_head_pitch_scale(left_stick_addition_y)
            
            update_current_head_roll_scale(right_stick_addition_x)
            
            update_current_head_yaw_scale(left_stick_addition_x)

            
    ELIF NOT is_control_pressed(frontend_control, caSelfieCameraMoveCameraMode)
    
        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
    
    endif 

ENDPROC 







///PURPOSE: renders the help text for the selfie system.   
PROC selfie_help_text_system()



    IF g_bInMultiplayer

        //Added by Steve to reinforce new feature use in MP. 2121506 
        SWITCH i_MP_selfie_camera_help_text_system //Initialised in cellphone_globals to zero. 
        

            CASE 0
                
                // Different help on PC as we're using a different input
                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                    print_help("CELL_CAM_SELFIE_0_KM")
                ELSE
                    print_help("CELL_CAM_SELFIE_0")
                ENDIF
                
                i_MP_selfie_camera_help_text_system++
            
            BREAK
            

            CASE 1
            
                IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
                
                    // Different help on PC as we're using a different input
                    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                        print_help("CELL_CAM_SELFIE_1_KM")
                    ELSE
                        print_help("CELL_CAM_SELFIE_1")
                    ENDIF
                    
                    i_MP_selfie_camera_help_text_system++
                
                ENDIF 

            BREAK 
            

            CASE 2
            
                //do nothing 
            
            BREAK

            
        ENDSWITCH



    ELSE


        //Lawrence's original work.    
        switch selfie_camera_help_text_system  //Initialised in cellphone_globals to zero.

            case 0
                
                // Different help on PC as we're using a different input
                IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                    print_help("CELL_CAM_SELFIE_0_KM")
                ELSE
                    print_help("CELL_CAM_SELFIE_0")
                ENDIF
                
                selfie_camera_help_text_system++
            
            break 
            
            case 1
            
                if not is_help_message_being_displayed()
                
                    // Different help on PC as we're using a different input
                    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                        print_help("CELL_CAM_SELFIE_1_KM")
                    ELSE
                        print_help("CELL_CAM_SELFIE_1")
                    ENDIF
                    
                    selfie_camera_help_text_system++
                
                endif 

            break 
            
            case 2
            
                //do nothing 
            
            break 
            
        endswitch 


    ENDIF


ENDPROC 



//End of new Selfie manipulation.
















PROC Check_For_Shutter_Press()

    
    CONTROL_ACTION caSelfieCameraMoveCameraMode,  caSelfieCameraHeadMode
    
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        caSelfieCameraMoveCameraMode = INPUT_CELLPHONE_EXTRA_OPTION
        caSelfieCameraHeadMode       = INPUT_SPRINT
    ELSE
        caSelfieCameraMoveCameraMode = input_script_lt
        caSelfieCameraHeadMode       = input_script_rt
    ENDIF

    //Lawrence, please put your work for 2016908 in between this selfie mode check here. Thanks.

    IF b_InSelfieMode
    
        selfie_camera_system()
        
        selfie_player_head_system()

        selfie_help_text_system()


        f_GameplayCamRelativePitch  = GET_GAMEPLAY_CAM_RELATIVE_PITCH()

        f_GameplayCamRelativeHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()

        /*
        #if IS_DEBUG_BUILD
            PRINTFLOAT (f_GameplayCamRelativePitch)
            PRINTNL()
            PRINTFLOAT (f_GameplayCamRelativeHeading)
            PRINTNL()
        #endif
        */



        //New context instructional button system for 2037087
        IF b_LeftTriggerInstructionsOngoing = FALSE
        
            IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraMoveCameraMode)
            AND (NOT (IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraHeadMode)))

                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppCamera - Left trigger context sensitive help prompted to display for 2037087")
                    cdPrintnl()

                #endif

                b_LeftTriggerInstructionsOngoing = TRUE //This is reset to FALSE when the player exits selfie mode via R3.

                
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
                    "CELL_281") //Exit

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
                        Get_Send_Photo_Text_Label()) //Send pic


                    //Add in "Reset Camera" functionality for 2158276 request.
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
                    "CELL_CAM_SELFIE_2")

                                           
                ELSE
                

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
                        "CELL_280") //Take Photo

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
                    "CELL_281") //Exit


                    //Add in "Reset Camera" functionality for 2158276 request.
                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_OPTION),
                    "CELL_CAM_SELFIE_2")



                ENDIF


                Setup_Selfie_LeftTrigger_Instructional_Buttons()

                
                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
                END_SCALEFORM_MOVIE_METHOD()


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.


                     
            ENDIF

        ELSE

            IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraMoveCameraMode))
            OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraHeadMode)

                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppCamera - Left trigger context sensitive help now removed for 2037087")
                    cdPrintnl()

                #endif

                b_LeftTriggerInstructionsOngoing = FALSE

        
                
                Setup_Cam_Instructional_Buttons()


                       
                                   
            ENDIF

        ENDIF




     
        IF b_RightTriggerInstructionsOngoing = FALSE

            IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraHeadMode)
            AND (NOT (IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraMoveCameraMode)))

                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppCamera - Right trigger context sensitive help prompted to display for 2037087")
                    cdPrintnl()

                #endif

                b_RightTriggerInstructionsOngoing = TRUE //This is reset to FALSE when the player exits selfie mode via R3.

                
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_CLEAR_SPACE", 200)

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "SET_DATA_SLOT_EMPTY", 3)


                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
                    "CELL_281") //Exit

                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1, 
                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION),
                        Get_Send_Photo_Text_Label()) //Send pic

                ELSE
                	IF NOT IS_LOCAL_PLAYER_USING_DRONE()
				
	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 0,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
	                        "CELL_280") //Take Photo

	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 1,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
	                    "CELL_281") //Exit
					ELSE
						 INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 2,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT), // GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))),
	                        "CELL_280") //Take Photo

	                    INSTRUCTIONAL_BUTTON_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieInstButtonsIndex, "SET_DATA_SLOT", 3,
	                    GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL),//GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))),
	                    "CELL_281") //Exit
					ENDIF
                ENDIF


                Setup_Selfie_RightTrigger_Instructional_Buttons()


                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieInstButtonsIndex, "SET_MAX_WIDTH")
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(f_ButtonWidth)
                END_SCALEFORM_MOVIE_METHOD()


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieInstButtonsIndex, "DRAW_INSTRUCTIONAL_BUTTONS", 0) // Set to 1 for stacked.


                     
            ENDIF

        ELSE

            IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraHeadMode))
            OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, caSelfieCameraMoveCameraMode)

                #if IS_DEBUG_BUILD

                    cdPrintstring ("AppCamera - Right trigger context sensitive help now removed for 2037087")
                    cdPrintnl()

                #endif

                b_RightTriggerInstructionsOngoing = FALSE

        
                
                Setup_Cam_Instructional_Buttons()


                       
                                   
            ENDIF

        ENDIF




    ELSE

        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
        ENABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)

    ENDIF

    IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL) //Update buttons as control mapping have changed this frame.
        Setup_Cam_Instructional_Buttons()
    ENDIF

    Disable_Vehicle_Controls()


    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE) //Fix for url:bugstar:2014389, may need to farm this out to WaitingforDiscardSend()
    ENDIF
	
	//Force turn off grid for drone
	IF IS_LOCAL_PLAYER_USING_DRONE()  
		IF !IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
			SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) //switch off grid instantly
			PRINTLN("AppCamera player is using the camera force switch off the grid")
	    ENDIF
	ENDIF
	
    //Allow user to turn off gridlines. Requested by Les from internet feedback. #1739319
    IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_GRID)
    AND b_LowriderDisableElements = FALSE
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()   

        PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset) //Added for url:bugstar:1806042
        
            
        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
            IF NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING)) //Don't allow grid effect to be toggled whilst parachuting.


                IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID) //Grid already removed


                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)

                    //Only switch the grid back on instantly if the player is not actively displaying a border. Always reset the toggling bit above though so that it re-appears when we go full screen.
                    //IF g_i_CurrentlySelected_SnapMaticBorder = 0  //2145148
                        IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) //switch on grid
                        ENDIF
                    //ENDIF
                    
                    #if IS_DEBUG_BUILD

                        cdPrintstring("Setting Photo Frame On if hide bit not set - 5")
                        cdPrintnl()
           
                        cdPrintnl()
                        cdPrintString("AppCamera - User toggled snapmatic grid ON")
                        cdPrintnl()

                    #endif

                
                ELSE

            
                    SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) //switch off grid instantly

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintString("AppCamera - User toggled snapmatic grid OFF")
                        cdPrintnl()

                    #endif


                ENDIF

        
            ENDIF 
        ENDIF

    ENDIF










    //Need to put in a little routine that tracks any player movement during a selfie to prevent him taking them on moving vehicles. This causes glitches. url:bugstar:1707013

    IF b_InSelfieMode

        IF b_FocusLockOngoing = TRUE //Focus lock is unavailable in Selfie Mode. Set Focus Lock ongoing to false to tidy up sound effect routine.
        
            b_FocusLockOngoing = FALSE

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "SET_FOCUS_LOCK")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_FOCUS")

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)
        
            END_SCALEFORM_MOVIE_METHOD()
        
        ENDIF



        //If in Singleplayer and PI_Menu is running in selfie mode, we need to update the Action text when the user presses the corresponding button.
        //IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK)
    
        
        //Update: Replacing the control check to get round script sync error detailed in 2023094. Now using bitset baton pass across scripts, It gets set in PI menu script.
        
        IF NOT (IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)) //"Director Mode On" text overlaps SF based "action" text in top right hand corner.
        
            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION)
                IF g_bInMultiplayer = FALSE
                AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PI_MENU")) > 0
                AND Is_Current_Player_Ped_MTD_Compatible()

                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "SET_FOCUS_LOCK")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)
                        
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_ACTTL")
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_txtSP_CurrentSelfie_anim))
                        END_TEXT_COMMAND_SCALEFORM_STRING()

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("Control pressed - Grabbed quickplay anim name from Craig and clearing baton bit g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION")
                            cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_txtSP_CurrentSelfie_anim))
                            cdPrintnl()

                        #endif

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE)


                    END_SCALEFORM_MOVIE_METHOD()


                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION) //Lets PI_Menu know that it's safe to check for the control press again.


                ENDIF
            ENDIF


        ELSE

            IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION)

                CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION) //Lets PI_Menu know that it's safe to check for the control press again.

            ENDIF

        ENDIF



        StationaryCheck_CurrentTime = GET_GAME_TIMER()


        IF (StationaryCheck_CurrentTime - StationaryCheck_StartTime) > 1500 //Only check every 1.5 seconds. Start Time is initialised whenever INPUT_FRONTEND_RS is fired to enter selfie mode.

            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //Need to compare starting position with current position
                v_PlayerSelfie_CurrentPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                //Compare with player position position vector grabbed on launch of selfie mode.
                IF GET_DISTANCE_BETWEEN_COORDS (v_PlayerSelfie_CurrentPos, v_PlayerSelfie_StartPos, TRUE) > 5.0

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("appCamera - aborting due to suspicious change in player position vector whilst in Selfie mode..")
                        cdPrintnl()   
                    #endif

                    g_Cellphone.PhoneDS = PDS_AWAY  //Abort Camera mode entirely to be on the safe side.
                    SET_GAME_PAUSED (FALSE)

                ENDIF   


                StationaryCheck_StartTime = GET_GAME_TIMER() //Reset start timer for this check.


            ENDIF

        ENDIF


    ELSE  //Need to add some checks for Focus Lock sound effects and upcoming Scaleform focus lock display alteration

    
        IF b_FocusLockOngoing

            IF NOT (IS_CONTROL_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK))
      		AND NOT IS_LOCAL_PLAYER_USING_DRONE()
                //Play_Back_Beep() //Was moaned about in 1983844.
                PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)
                b_FocusLockOngoing = FALSE

                BEGIN_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "SET_FOCUS_LOCK")

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (FALSE)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_FOCUS")

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)
        
                END_SCALEFORM_MOVIE_METHOD()


            ENDIF

        ELSE 

            IF IS_CONTROL_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_FOCUS_LOCK)
			AND NOT IS_LOCAL_PLAYER_USING_DRONE()
                /*
                #if IS_DEBUG_BUILD
                    DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.5, "STRING", "FOCUS_LOCK")
                #endif
                */

                PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)

                b_FocusLockOngoing = TRUE



                IF NOT (IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)) //"Director Mode On" text overlaps SF based "Focus" text in top right hand corner

                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "SET_FOCUS_LOCK")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("CELL_FOCUS")
            
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL (TRUE)

                    END_SCALEFORM_MOVIE_METHOD()

                ENDIF

    ENDIF

        ENDIF
        

    ENDIF








    IF Is_DLC_Compatible()

        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION)//INPUT_FRONTEND_RB)... this causes a glitch url:bugstar:1709149
        AND b_LowriderDisableElements = FALSE
		AND NOT IS_LOCAL_PLAYER_USING_DRONE()
            IF b_InSelfieMode


              #if IS_DEBUG_BUILD
                cdPrintString("Testing RB press")
                cdPrintnl()
              #endif
    
                    g_i_CurrentlySelected_SnapMaticExpression ++

                    IF g_i_CurrentlySelected_SnapMaticExpression > 0
                    AND g_i_CurrentlySelected_SnapMaticExpression < i_NumberOfExpressionsAvailable  
                    
                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())


                            PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)
                            

                            //set briefly to aiming to help speed up transition after an SP play action.
                            SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), "Mood_Normal_1")



                            CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

                            //Need to check for dodgy facial expressions for Michael. See bug 1712986
                            IF g_Cellphone.PhoneOwner = CHAR_MICHAEL
                            
                                INT i_Headgear, i_EyeWear
                                BOOL b_AbortExpressionWarning = FALSE

                                i_Headgear = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)

                                IF i_Headgear = 20 //Carnival Mask
                                OR i_Headgear = 14 //Monster mask
                            
                                    b_AbortExpressionWarning = TRUE

                                    #if IS_DEBUG_BUILD
                                        cdPrintstring("AppCamera - Aborting expression warning change flag set to true due to inappropriate headgear. Int is ")
                                        cdPrintint(i_Headgear)
                                        cdPrintnl()
                                    #endif
                                
                                ENDIF


                                i_EyeWear = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES)

                                IF i_Eyewear <> -1 //<Michael is wearing some kind of eyeglasses. If Michael is not wearing any eyeglasses GET_PED_PROP should return -1    for ANCHOR_EYES.
                                    
                                    b_AbortExpressionWarning = TRUE

                                    #if IS_DEBUG_BUILD
                                        cdPrintString("AppCamera - Aborting expression warning change flag set to true due to inappropriate eyewear. Int is ")
                                        cdPrintint(i_Eyewear)
                                        cdPrintnl()
                                    #endif

                                ENDIF




                                IF b_AbortExpressionWarning = TRUE //As this is set, we need to check if the upcoming expression is on the blacklist before aborting change fully.
                                

                                    IF g_i_CurrentlySelected_SnapMaticExpression = 2
                                    OR g_i_CurrentlySelected_SnapMaticExpression = 3 
                                    OR g_i_CurrentlySelected_SnapMaticExpression = 4
                                    OR g_i_CurrentlySelected_SnapMaticExpression = 8
                                    OR g_i_CurrentlySelected_SnapMaticExpression = 9

                                        IF i_Headgear = -1 //Player isn't wearing a mask, allow 3, smiling, as a valid expression whilst wearing glasses.
                                        AND i_Eyewear > -1 //must be wearing glasses

                                            IF g_i_CurrentlySelected_SnapMaticExpression = 3
           
                                                SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), s_FacialEx[g_i_CurrentlySelected_SnapMaticExpression])
       
                                            ENDIF
                                
                                        ENDIF

                                    ELSE

                                        SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), s_FacialEx[g_i_CurrentlySelected_SnapMaticExpression])
                                
                                    ENDIF

                                ELSE

                                    SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), s_FacialEx[g_i_CurrentlySelected_SnapMaticExpression])

                                ENDIF
                                

        
                            ELSE //Is owner Michael condition check. Ignore these checks for other characters and MP.

                                SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), s_FacialEx[g_i_CurrentlySelected_SnapMaticExpression])

                            ENDIF


                        ENDIF

                    ENDIF


                    IF g_i_CurrentlySelected_SnapMaticExpression = i_NumberOfExpressionsAvailable 
                    OR  g_i_CurrentlySelected_SnapMaticExpression > i_NumberOfExpressionsAvailable 

                        g_i_CurrentlySelected_SnapMaticExpression = 0    //Cycle round to zero, neutral again.

                    ENDIF


                    IF g_i_CurrentlySelected_SnapMaticExpression = 0 
                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                            //set briefly to aiming to help speed up transition, see #1918357
                            SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), "Mood_Normal_1")


                            CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

                            PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)


                            #if IS_DEBUG_BUILD
                                cdPrintString("AppCAMERA - Clearing expression to neutral. Expression state is ")
                                cdPrintInt(g_i_CurrentlySelected_SnapMaticExpression) 
                                cdPrintnl()
                            #endif


                        ENDIF
                    ENDIF




                    #if IS_DEBUG_BUILD
                        cdPrintString("AppCAMERA - Assigning Expression - ")
                        cdPrintInt(g_i_CurrentlySelected_SnapMaticExpression) 
                        cdPrintnl()
                    #endif



            ENDIF
        ENDIF   //End of RB button press condition check.



        //IF b_DepthOfFieldEffectAvailable //Replaced by dedicated toggle for selfie and normal mode.
            IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_CAMERA_DOF) //Check for manipulation of Depth of Field effect toggle in normal and portrait mode.
            AND b_LowriderDisableElements = FALSE
			AND NOT IS_LOCAL_PLAYER_USING_DRONE()
                
                
                //Adding work for new Depth of Field Toggle from Alex, requested by 2282124
                //b_NormalMode_DepthOfField_ToggledOn and b_SelfieMode_DepthOfField_ToggledOn will both be set when the input is pressed in Selfie or Non Selfie Mode.
                //Still keeping the IF b_InSelfieMode branch for ease of integration and decent debug output.

                IF b_InSelfieMode  

                    IF b_SelfieDepthOfFieldEffectAvailable = TRUE
                     
                        #if IS_DEBUG_BUILD
                            cdPrintString("Testing LB press in Selfie Mode")
                            cdPrintnl()
                        #endif

        
                        PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)


                        IF b_SelfieMode_DepthOfField_ToggledOn = FALSE


                            
                            b_SelfieMode_DepthOfField_ToggledOn = TRUE
                            b_NormalMode_DepthOfField_ToggledOn = TRUE //Set this also, as I'm making the DOF toggle universal across Selfie and Non-Selfie mode, although the natives are different.

                            
                            //Filter 12 now works with DOF after update by Thomas D's changes in bug 2108712, Nov 2014. Dummied string comparison for safety.
                            IF ARE_STRINGS_EQUAL (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], "phone_cam12DUMMY")
                            //OR ARE_STRINGS_EQUAL (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], "phone_cam3")

                                //Need to remove these combinations see url:bugstar:1706169

                                #if IS_DEBUG_BUILD
                                    cdPrintString("AppCamera - Bad filter DoF combination. Not setting effect.")
                                    cdPrintnl()
                                #endif     

                                //We still allow the toggle to switch on so that the DoF effect becomes visible if we swap to a "good" filter combination.

                            ELSE

                                CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(TRUE)
                                SET_MOBILE_PHONE_DOF_STATE(TRUE)

                                #if IS_DEBUG_BUILD
                                    cdPrintString("AppCamera - Setting Selfie Mode DoF effect to TRUE")
                                    cdPrintnl()
                                #endif
            
                            ENDIF

                        ELSE
                        
                            b_SelfieMode_DepthOfField_ToggledOn = FALSE //Must have been true
                            b_NormalMode_DepthOfField_ToggledOn = FALSE //Set this also, as I'm making the DOF toggle universal across Selfie and Non-Selfie mode, although the natives are different.
                        
                        
                            //Would put Colin's new DoF command here to FALSE
                            CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(FALSE)
                            SET_MOBILE_PHONE_DOF_STATE(FALSE)

                            #if IS_DEBUG_BUILD
                                cdPrintString("AppCamera - Setting Selfie Mode DoF effect to FALSE")
                                cdPrintnl()
                            #endif

                        ENDIF

                    ENDIF

                ELSE //Must be in normal view mode...


                    IF b_NormalDepthOfFieldEffectAvailable = TRUE
                        #if IS_DEBUG_BUILD
                            cdPrintString("Testing LB press in Normal Mode")
                            cdPrintnl()
                        #endif


                        IF b_NormalMode_DepthOfField_ToggledOn = FALSE

                            b_NormalMode_DepthOfField_ToggledOn = TRUE
                            b_SelfieMode_DepthOfField_ToggledOn = TRUE//Set this also, as I'm making the DOF toggle universal across Selfie and Non-Selfie mode, although the natives are different.

                            CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(TRUE)
                            SET_MOBILE_PHONE_DOF_STATE(TRUE)

                            #if IS_DEBUG_BUILD
                                cdPrintstring("AppCamera - Setting Normal Mode DoF effect to TRUE")
                                cdPrintnl()
                            #endif

                        ELSE

                            b_NormalMode_DepthOfField_ToggledOn = FALSE //Must have been true
                            b_SelfieMode_DepthOfField_ToggledOn = FALSE//Set this also, as I'm making the DOF toggle universal across Selfie and Non-Selfie mode, although the natives are different.

                            //Would put Colin's new DoF command here to FALSE
                            CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(FALSE)
                            SET_MOBILE_PHONE_DOF_STATE(FALSE)

                            #if IS_DEBUG_BUILD
                                cdPrintString("AppCamera - Setting Normal Mode DoF effect to FALSE")
                                cdPrintnl()
                            #endif

                        ENDIF

                    ENDIF


                ENDIF

            ENDIF  //End of LB button press condition check.
        //ENDIF


        




    ENDIF //End of Is_DLC_Compatible() condition check.

























    
    IF b_OkayToCheckForBorderSelection = TRUE
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()

        IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_UP)
        AND b_LowriderDisableElements = FALSE

            
            //Comment this in to enable border functionality and initialise g_i_CurrentlySelected_SnapMaticBorder to 0 on script launch.

            IF Is_DLC_Compatible()
                g_i_CurrentlySelected_SnapMaticBorder ++

                PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)

            ENDIF

            IF g_i_CurrentlySelected_SnapMaticBorder = i_NumberOfBordersAvailable 

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Border Browse Right - g_i_CurrentlySelected_SnapMaticBorder will be set to zero as it matched i_NumberOfBordersAvailable at  ")
                    cdPrintInt(g_i_CurrentlySelected_SnapMaticBorder)
                    cdPrintnl()
                    cdPrintString("AppCamera - Setting streamed TXD as no longer needed - ")
                    cdPrintString(s_CurrentBorder[(g_i_CurrentlySelected_SnapMaticBorder - 1)])
                    cdPrintnl()

                #endif

                //PHONEPHOTOEDITOR_SET_FRAME_TXD("NULL", true)
                Local_PPE_Toggle (FALSE)
                      


                CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(1.0)

                SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(s_CurrentBorder[(g_i_CurrentlySelected_SnapMaticBorder - 1)]) //Reached the limit of the border array, so mark the last valid border as not required.

                /*Removed for 2145148
                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) //switch on grid
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring("Setting Photo Frame On if hide bit not set - 6")
                    cdPrintnl()
                #endif
                */


                g_i_CurrentlySelected_SnapMaticBorder = 0 //Reset to zero, "no border".

                //SET_TIMECYCLE_MODIFIER("phone_cam") //with no border, re-establish scan lines. #1681700
                SetUpScanlines()


                IF b_GridWasOnWhenBorderWasZero = TRUE
                    
                    CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID) //If grid was on when Border was at zero, switch it back on again. 2145148
                    b_GridWasOnWhenBorderWasZero = FALSE
                    
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) //switch on grid.

                ENDIF





            ENDIF



            IF g_i_CurrentlySelected_SnapMaticBorder > 0
            AND g_i_CurrentlySelected_SnapMaticBorder < i_NumberOfBordersAvailable  

                b_OkayToCheckForBorderSelection = FALSE
                b_OkayToDrawBorder = FALSE


                //If the texture dictionary for each border is different, we may need to remove the old one before requesting a new one. If it's the same me might just be able to load once.

                //Border requested, get rid of scan lines. #1681700
                //Clear_Modifier_With_Exception_Checks()

                
                REQUEST_STREAMED_TEXTURE_DICT(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder])

                           
            ENDIF

        ENDIF



        //This is no longer used as we needed to free up space for new selfie controls. INPUT_CELLPHONE_UP controls the one-way cycle through the available borders.
        /*
        IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_LEFT)

            
            //Comment this in to enable border functionality and initialise g_i_CurrentlySelected_SnapMaticBorder to 0 on script launch.
            IF Is_DLC_Compatible()
            
                g_i_CurrentlySelected_SnapMaticBorder --

                PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)

            ENDIF

            IF g_i_CurrentlySelected_SnapMaticBorder = 0

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Border Browse Left - g_i_CurrentlySelected_SnapMaticBorder now set to zero. Removing border...")
                    cdPrintnl()
                    cdPrintString("AppCamera - Setting streamed TXD as no longer needed - ")
                    cdPrintString(s_CurrentBorder[(g_i_CurrentlySelected_SnapMaticBorder + 1)])
                    cdPrintnl()


                #endif

           
                //SET_TIMECYCLE_MODIFIER("phone_cam") //with no border, re-establish scan lines. #1681700
                SetUpScanlines()


                //PHONEPHOTOEDITOR_SET_FRAME_TXD ("NULL", true)
                Local_PPE_Toggle (FALSE)

                CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(1.0)

                SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(s_CurrentBorder[(g_i_CurrentlySelected_SnapMaticBorder + 1)]) //Reached the lower limit of the border array, so mark the last valid border as not required.

                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) //switch on grid
                ENDIF

            ENDIF



            IF g_i_CurrentlySelected_SnapMaticBorder < 0

                g_i_CurrentlySelected_SnapMaticBorder = ( i_NumberOfBordersAvailable - 1 ) //carousel back to last position. e.g 2,1,0 then back to 2 again.

                #if IS_DEBUG_BUILD

                    cdPrintString("AppCamera - Border Browse Left - g_i_CurrentlySelected_SnapMaticBorder was less than zero. Setting to ")
                    cdPrintInt(g_i_CurrentlySelected_SnapMaticBorder)
                    cdPrintnl()

                #endif

            ENDIF






            IF g_i_CurrentlySelected_SnapMaticBorder > 0
            AND g_i_CurrentlySelected_SnapMaticBorder < i_NumberOfBordersAvailable  

                b_OkayToCheckForBorderSelection = FALSE
                b_OkayToDrawBorder = FALSE

  
                //If the texture dictionary for each border is different, we may need to remove the old one before requesting a new one.

                //Border requested, get rid of scan lines. #1681700
                //Clear_Modifier_With_Exception_Checks()
                

                REQUEST_STREAMED_TEXTURE_DICT(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder])

                           
            ENDIF

        ENDIF
        */








    ENDIF



    IF g_i_CurrentlySelected_SnapMaticBorder > 0  // Position zero holds "No Border" so we don't need to draw that.
     
        IF b_OkayToCheckForBorderSelection = FALSE

            IF HAS_STREAMED_TEXTURE_DICT_LOADED(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder])

                b_OkayToDrawBorder = TRUE
                b_OkayToCheckForBorderSelection = TRUE 

                IF NOT PHONEPHOTOEDITOR_IS_ACTIVE()
                    Local_PPE_Toggle (TRUE)
                ENDIF

                IF b_GridWasOnWhenBorderWasZero = FALSE
                    IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID) //Need to keep track of initial setting to restore when Border = 0 again. 2145148
                        b_GridWasOnWhenBorderWasZero = TRUE
                    ENDIF
                ENDIF


                SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID) //2145148 - Removing grid everytime the border has been changed.

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) //switch off grid instantly

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintString("AppCamera - Border changed snapmatic grid OFF")
                    cdPrintnl()

                #endif



                CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(0.25)

                PHONEPHOTOEDITOR_SET_FRAME_TXD(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], false)

            ENDIF

        ENDIF

        IF b_OkayToDrawBorder = TRUE

            IF g_i_CurrentlySelected_SnapMaticBorder = 1
            OR g_i_CurrentlySelected_SnapMaticBorder = 3
       
                //DRAW_SPRITE(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], "Globe", 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
                //DRAW_SPRITE(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
            
            ENDIF
             
             
            IF g_i_CurrentlySelected_SnapMaticBorder = 2
            OR g_i_CurrentlySelected_SnapMaticBorder = 4

                //DRAW_SPRITE(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], "Leaderboard_SocialClub_Icon", 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
                //DRAW_SPRITE(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder], 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
            
            ENDIF
                
        ENDIF                
                    
    ENDIF                
    





    IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_DOWN)
    AND b_LowriderDisableElements = FALSE
    AND NOT IS_LOCAL_PLAYER_USING_DRONE()                   
        //Check For Filter Choice Down

        //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])


        IF Is_DLC_Compatible()

            Clear_Modifier_With_Exception_Checks(FALSE)
            g_i_CurrentlySelected_SnapMaticFilter ++

            PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)


        ENDIF

        #if IS_DEBUG_BUILD

            cdPrintString("AppCamera - Filter Browse Down - g_i_CurrentlySelected_SnapMaticFilter at ")
            cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
            cdPrintnl()

        #endif

        IF g_i_CurrentlySelected_SnapMaticFilter = i_NumberOfFiltersAvailable 

            #if IS_DEBUG_BUILD

                cdPrintString("AppCamera - Filter Browse Down - g_i_CurrentlySelected_SnapMaticFilter will be set to zero as it matched i_NumberOfFiltersAvailable at  ")
                cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                cdPrintnl()

            #endif

            g_i_CurrentlySelected_SnapMaticFilter = 0

        ENDIF


        IF g_i_CurrentlySelected_SnapMaticFilter = 0 //No Filter in position 0 of filter array!

            //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

            IF Is_DLC_Compatible()
                Clear_Modifier_With_Exception_Checks(FALSE)
            ENDIF

        ELSE

            //ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)

            //SET_TIMECYCLE_MODIFIER(s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
            SetUpCurrentTCModifier()

        ENDIF


        CheckForBadDoF_FilterCombination()



              
    ENDIF





    /* This is no longer used as we needed to free up space for new selfie controls. INPUT_CELLPHONE_DOWN controls the one-way cycle through the available filters.
    IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_UP)
                           
        //Check For Filter Choice Up

        //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
        
        IF Is_DLC_Compatible()

            Clear_Modifier_With_Exception_Checks()
            g_i_CurrentlySelected_SnapMaticFilter --

            PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)
        
        ENDIF

        #if IS_DEBUG_BUILD

            cdPrintString("AppCamera - Filter Browse Up - g_i_CurrentlySelected_SnapMaticFilter at ")
            cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
            cdPrintnl()

        #endif

        IF g_i_CurrentlySelected_SnapMaticFilter < 0 

            #if IS_DEBUG_BUILD

                cdPrintString("AppCamera - Filter Browse Up - g_i_CurrentlySelected_SnapMaticFilter will be set to last filter array  as it is now less than zero. ")
                cdPrintInt(g_i_CurrentlySelected_SnapMaticFilter)
                cdPrintnl()

            #endif

            g_i_CurrentlySelected_SnapMaticFilter =  ( i_NumberOfFiltersAvailable - 1 ) //Carousel round to last filter available in array.

        ENDIF


        IF g_i_CurrentlySelected_SnapMaticFilter = 0 //No Filter in position 0 of filter array!

            //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
            IF Is_DLC_Compatible()
                Clear_Modifier_With_Exception_Checks()
            ENDIF

        ELSE

            //ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)
            //SET_TIMECYCLE_MODIFIER(s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
            SetUpCurrentTCModifier()


        ENDIF


        CheckForBadDoF_FilterCombination()

              
    ENDIF
    */










    IF IS_DISABLED_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_SELFIE)
    AND b_LowriderDisableElements = FALSE //No selfie in Lowrider Photography mission. 2513807
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()
        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
        
            IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
            AND NOT ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
        
                //Need to put in a little routine that tracks any player movement during a selfie to prevent him taking them on moving vehicles. This causes glitches. url:bugstar:1707013
                v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                PLAY_SOUND_FRONTEND (-1,  "Menu_Navigate", g_Owner_Soundset)
                
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) // switch off viewfinder graphics.
                LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "CLOSE_SHUTTER")
                i_ShutterTransitionTime = GET_GAME_TIMER()
                WHILE GET_GAME_TIMER() < (i_ShutterTransitionTime + i_ShutterDelayForSelfieSwitch)
                AND g_Cellphone.PhoneDS > PDS_AWAY
                    SnapmaticHudHide()
                    Draw_Shutter_Movie_With_Status_Check()
                    Disable_Camera_Movement_Controls()
                    REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                    WAIT (0)                                                        
                ENDWHILE

                IF b_InSelfieMode = FALSE   
                    

                    //b_NormalMode_DepthOfField_ToggledOn = FALSE  //Removed to allow universal toggle of new depth of field command from Alex.
                    //Would put Colin's new corresponding DoF command here to FALSE

                    
                    STOP_SOUND (Zoom_SoundID)


                    b_InSelfieMode = TRUE


                    Wrapped_CellCam_SelfieModeActivate (TRUE)


                    Set_Up_Selfie_PitchandHeading() 

                
                    SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)

                    //Need to put in a little routine that tracks any player movement during a selfie to prevent him taking them on moving vehicles. This causes glitches. url:bugstar:1707013
                    StationaryCheck_StartTime = GET_GAME_TIMER()



                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("APPCAMERA - Selfie Mode set to true")
                        cdPrintnl()

                    #endif


                    //Headtracking Look at work... 
                    //CLEAR_PED_TASKS (PLAYER_PED_ID())



                ELSE

                    
                    //Re-include this if we want to switch off DOF toggle when swapping between Selfie and Normal Mode.
                    //b_SelfieMode_DepthOfField_ToggledOn = FALSE
                    //CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(TRUE)

                    //Would put Colin's new corresponding DoF command here to FALSE


                    b_LeftTriggerInstructionsOngoing = FALSE
                    b_RightTriggerInstructionsOngoing = FALSE
                    

                    Wrapped_CellCam_SelfieModeActivate (FALSE)

                    b_InSelfieMode = FALSE
                
                    CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)


                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("APPCAMERA - Selfie Mode set to FALSE")
                        cdPrintnl()

                    #endif


                ENDIF

                i_ShutterTransitionTime = GET_GAME_TIMER()
                WHILE GET_GAME_TIMER() < (i_ShutterTransitionTime + i_ShutterDelayForAnimation)
                AND g_Cellphone.PhoneDS > PDS_AWAY
                    SnapmaticHudHide()
                    Draw_Shutter_Movie_With_Status_Check()
                    Disable_Camera_Movement_Controls()
                    REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                    WAIT (0)
                ENDWHILE
                LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")

                

                IF Is_DLC_Compatible()

                    IF g_i_CurrentlySelected_SnapMaticBorder = 0
                        IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                        ENDIF
                    ENDIF

                ELSE

                    IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                    ENDIF

                ENDIF


                #if IS_DEBUG_BUILD
                    cdPrintstring("Setting Photo Frame On if hide bit not set - 7")
                    cdPrintnl()
                #endif



                Draw_Shutter_Movie_With_Status_Check()
                SnapmaticHudHide()


                #if IS_DEBUG_BUILD

                    cdPrintstring("SetUp_TakePhoto_Buttons cued 3.")
                    cdPrintnl()

                #endif
    



                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

                    TokenDecision = "CELL_296"

                    SetUp_TakePhoto_With_Send_Buttons()

                ELSE

                    TokenDecision = "CELL_295"

                    SetUp_TakePhoto_No_Send_Buttons()

                ENDIF
                

            ENDIF

        ENDIF


    ENDIF

    
         //Temp!
        /*
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT))
        
            IF NOT g_bInMultiplayer
          
                LaunchGallery()

            ENDIF
     
        ENDIF
        */


        IF b_InSelfieMode = FALSE //No zoom in selfie mode.

            IF b_HasZoomSFXBeenSetup = FALSE


                IF IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY)
                OR IS_CONTROL_JUST_PRESSED (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY)
                    
                    f_CellCamZoomFactor = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()    

                    IF f_CellCamZoomFactor > 1.0 //fully zoomed out default
                    AND f_CellCamZoomFactor < 4.50 // 4.5 seems to be the maximum zoomed in factor in NG

                        //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "RUNNING MOBPHONE ZOOM 1")
                        
                        IF HAS_SOUND_FINISHED (Zoom_SoundID)
                            PLAY_SOUND_FRONTEND (Zoom_SoundID, "Camera_Zoom", g_Owner_Soundset)
                        ENDIF
                    
                    ELSE

                        STOP_SOUND (Zoom_SoundID)

                    ENDIF

                    b_HasZoomSFXBeenSetup = TRUE

                ENDIF


            ELSE


                IF IS_CONTROL_PRESSED (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY)
                OR IS_CONTROL_PRESSED (PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY)

                                           
                    f_CellCamZoomFactor = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()    


                    IF f_CellCamZoomFactor > 1.0 //fully zoomed out default.
                    AND f_CellCamZoomFactor < 4.50 // 4.5 seems to be the maximum zoomed in factor in NG

                        //DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "RUNNING MOBPHONE ZOOM 2")

                        IF HAS_SOUND_FINISHED (Zoom_SoundID)
                            PLAY_SOUND_FRONTEND (Zoom_SoundID, "Camera_Zoom", g_Owner_Soundset)
                        ENDIF

                    ELSE

                        STOP_SOUND (Zoom_SoundID)

                    ENDIF
               
                ELSE

                    STOP_SOUND (Zoom_SoundID)
                     
                ENDIF 

            ENDIF
    

        ELSE //Track look at:


            /*  Removed - this is now being handled by the code anim side.
            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //This is overrided by the phone task. Clearing the phone task every frame is not an option.
                TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_GAMEPLAY_CAM_COORD(), -1 )//SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)



            ENDIF
            */

        ENDIF


        
    
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action, take picture.
        AND !SHOULD_START_EXIT_DRONE_SNAPMATIC() 
		AND !Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) 
                            
            f_LastSelfieRelativePitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH ()
        
            f_LastSelfieRelativeHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING ()

            
                            
            DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)


            SET_BIT (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)

                                      

            STOP_SOUND (Zoom_SoundID)
   
                                                                    
            b_WaitingForDiscardOrSend = TRUE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0 ) //switch off viewfinder graphics.

    
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_REMAINING_PHOTOS", 0)


      
            PLAY_SOUND_FRONTEND (-1, "Camera_Shoot", g_Owner_Soundset)  

            LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "CLOSE_SHUTTER")  





            v_3dDesiredRotation = <<-90.3, 0.0, 90.0>> //Failsafe fix for 1934037
            
            SET_MOBILE_PHONE_ROTATION (v_3dDesiredRotation)

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("Failsafe rotation 1 set in shutter press for 1934037")
                cdPrintnl()

            #endif




            IF NOT Is_DLC_Compatible() //If we're not running the Christmas gift DLC make sure we remove the scanlines.

                Clear_Modifier_With_Exception_Checks()

            ENDIF

            Cellphone_Pic_Just_Taken = TRUE


            CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)


            
            WHILE g_Cell_Pic_Stage < PIC_STAGE_HOLDING_LQ_COPY
            AND g_Cellphone.PhoneDS > PDS_AWAY

                    SnapmaticHudHide()
                    Draw_Shutter_Movie_With_Status_Check()
                    Disable_Camera_Movement_Controls()
                    Disable_Vehicle_Controls()                  
                    REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                    WAIT (0)

                    //Maybe set player invulnerable here?


            ENDWHILE

                //Deactivate camera here so animation has time to transition underneath the closed shutter
                CELL_CAM_ACTIVATE (FALSE, FALSE) //Work on 2200121 - Trial Removal


                        #if IS_DEBUG_BUILD

                            cdPrintString("AppCAMERA - Checking animator placed de-activation.")
                            cdPrintnl()

                        #endif


            
                IF g_Cellphone_FullScreen_Mode
                    CellphoneFirstPersonHorizontalModeToggle(TRUE)              
                ENDIF
                
                i_ShutterTransitionTime = GET_GAME_TIMER()
                WHILE GET_GAME_TIMER() < (i_ShutterTransitionTime + i_ShutterDelayForAnimationAfterPhoto)
                AND g_Cellphone.PhoneDS > PDS_AWAY
                    SnapmaticHudHide()
                    Draw_Shutter_Movie_With_Status_Check()
                    Disable_Camera_Movement_Controls()
                    REPLAY_PREVENT_RECORDING_THIS_FRAME()
                    WAIT (0)            
                ENDWHILE                

                SETTIMERA(0) //For auto continue... if bit set.             
                
                LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)

                Setup_Cam_Instructional_Buttons()

                IF g_Cellphone.PhoneDS > PDS_AWAY

                    IF g_bInMultiplayer //For TODO 425062... but should always increment photos taken, pause or not.

                        INCREMENT_BY_MP_INT_CHARACTER_STAT (MP_STAT_NO_PHOTOS_TAKEN, 1)
                        
                        #if IS_DEBUG_BUILD

                            cdPrintString("AppCAMERA - Multiplayer cellphone cam pic taken.")
                            cdPrintnl()

                        #endif

                        Do_Corner_Reposition()  //Work on 2200121 

                        
                        REQUEST_SAVE(SSR_REASON_CAMERA_APP, STAT_SAVETYPE_PHOTOS) //Requested to be in here by Miguel was in net_stat_tracking.sch
        
                    ELSE

                        //STAT_INCREMENT(NO_PHOTOS_TAKEN, 1.0)

                        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
                            CASE CHAR_MICHAEL
                                STAT_INCREMENT(SP0_NO_PHOTOS_TAKEN, 1.0)
                                BREAK
                            CASE CHAR_FRANKLIN
                                STAT_INCREMENT(SP1_NO_PHOTOS_TAKEN, 1.0)
                                BREAK
                            CASE CHAR_TREVOR
                                STAT_INCREMENT(SP2_NO_PHOTOS_TAKEN, 1.0)
                                BREAK
                        ENDSWITCH





                        Do_Corner_Reposition()  //Work on 2200121 

                   ENDIF


                   Local_PPE_Toggle(FALSE) //new - get rid of border when moving back to small phone.

                    
                ENDIF

                // Make sure shutter movie is drawn this frame
                Draw_Shutter_Movie_With_Status_Check()
          
        ENDIF










        

        IF IS_BIT_SET (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //send pic



                DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)

                WAIT (0) //1422498
                REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                SnapmaticHudHide()

                WAIT (0)
                REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                SnapmaticHudHide()
                    


                FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO()
                FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()


            
                //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
                g_b_appHasRequestedTidyUp = TRUE

                b_DrawCamHelpText = FALSE


                
                b_SnapToPortraitImmediately = TRUE
                //HIDE_ACTIVE_PHONE (FALSE, TRUE)


                g_b_Rotate3dPhonePortrait = TRUE //will cue exit.
                CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)

                b_OkayToTakePicture = FALSE

                b_Rotate3dPhoneX = TRUE
                b_Rotate3dPhoneY = TRUE
                b_Rotate3dPhoneZ = TRUE

                b_ManipulatePhonePosX = TRUE
                b_ManipulatePhonePosY = TRUE
                b_ManipulatePhonePosZ = TRUE


                Play_Select_Beep()


                b_WaitingForDiscardOrSend = FALSE

                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                    //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                    v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                ENDIF
                


                Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
                Local_PPE_Toggle (FALSE)

                b_VehicleRenderCheck  = FALSE

                Clear_Modifier_With_Exception_Checks() //#1431423

                //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])



                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING ("Cell Cam FALSE 2")
                    PRINTNL()

                    cdPrintnl()
                    cdPrintstring("Cam DE-activated 7")
                    cdPrintnl()


                #endif


                #if IS_DEBUG_BUILD
                
                    PRINTNL()
                    PRINTSTRING ("From Check_For_Shutter_Press:")
                    PRINTSTRING ("About to launch contacts list for pic send! ")
                    PRINTSTRING ("Normal exit and rotate called for appCamera script.")
                    PRINTNL()

                #endif


                //Cellphone Flashhand.sc checks for this bit but only launches contacts when this script is confirmed as no longer running.
				
				IF NOT MPGlobalsAmbience.bSkipContactListSendingPhotos
                	SET_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)
				ENDIF


                SET_GAME_PAUSED (FALSE)


                ShouldDrawShutter = FALSE
                PRINTLN("[app_camera] ShouldDrawShutter = FALSE")                    
                                 
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 16)


          
            ENDIF
        ENDIF



ENDPROC





















PROC WaitingForDiscardOrSend()





    //Disable_Camera_Movement_Controls()

    #if IS_DEBUG_BUILD
        IF g_DisplayTestPhotoSprite
            //DRAW_RECT( 0.5, 0.5, 0.515, 0.52, 255, 255, 255, 255 )
            //DRAW_SPRITE("PHONE_PHOTO_TEXTURE", "PHONE_PHOTO_TEXTURE", 0.5000, 0.5000, 0.5000, 0.5000, 0.0000, 255, 255, 255, 255)
            //HIDE_ACTIVE_PHONE (TRUE, TRUE)
        ENDIF
    #endif


        
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Continue and save...

        //IF ShouldFirstPersonPhoneHudMovementBeHidden()
            CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549
        //ENDIF
   
        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE) //If the picture taking segment is critical, bypass any saving routine and just continue...
        //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE)  //Removed for 1932325 - hard drive save now used.
        //OR g_DumpDisableEveryFrameCaller //To test exception to save routine  
        //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE) //Stub for 1708374

            
            
            //The instructional buttons should just say "continue" only at this point...
            
            #if IS_DEBUG_BUILD
                PRINTSTRING ("AppCAMERA - Cellphone PhonePositiveInput detected but save routine skipped due to camera app bypass bit.")
                PRINTNL()
            #endif

            
            SET_GAME_PAUSED (FALSE)

            SETTIMERB(0)  // new method, post-game pause


            Do_Fullscreen_Reposition()

            b_WaitingForDiscardOrSend = FALSE


              IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

              ENDIF


            Play_Back_Beep()

            // Following delayed till end of shutter transition
            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1 ) //switch on viewfinder graphics.
            //Setup_Cam_Instructional_Buttons()
            b_ShutterTransition = TRUE
    

   

        ELSE


            //Nov 2. 2014  - 2109889 removing the two lines as the save has changed to hard drive and is quicker, plus entering a vehicle will abort the camera app now.

            //b_InSelfieMode = FALSE  //Potential fix for 1610615  - potential for bad stuff happening in the interim between the save and the re-establishment of the camera viewfinder.

            //Wrapped_CellCam_SelfieModeActivate (FALSE) //No option but to include this because of new Christmas DLC bug #1700520

            //The above two lines mean that after requesting a save, the camera mode will be reset to first person, facing forward.


            IF IsEligibleToSavePhoto()
       
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - Checking Save Eligibility...")
                    cdPrintnl()

                #endif                


                IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LOW_QUALITY_CREATE_FAILED)


                    PRINT_HELP ("CELL_299")
            

                ELSE

                    
                    Play_Back_Beep()

                    LaunchSaveRoutine_in_Controller()

                ENDIF

            
            ELSE

                
                b_AlertScreenPresent = TRUE

                HIDE_ACTIVE_PHONE (TRUE, TRUE)

             
                
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - Hiding active phone. 1")
                    cdPrintnl()

                #endif



                //Alert screen presence will display this alert message in main loop condition depending on e_CurrentAlertScreen state. E.g
                //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_SCW_1", iButtonBits, "CELL_CAM_SCW_2", FALSE, -1, "", "", FALSE)



            ENDIF

        ENDIF

    ENDIF   





    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)

        //IF ShouldFirstPersonPhoneHudMovementBeHidden()
            CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549
        //ENDIF

        

        WHILE TIMERA() < 2000

            WAIT (0)
         
            
            SnapmaticHudHide()
            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092



            Disable_Camera_Movement_Controls()
            Disable_Vehicle_Controls()

            Draw_Shutter_Movie_With_Status_Check()

        ENDWHILE


        SET_GAME_PAUSED (FALSE)

        Do_Fullscreen_Reposition()

        SETTIMERB(0)  // new method, post-game pause
        
        b_WaitingForDiscardOrSend = FALSE



              IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

              ENDIF



        Setup_Cam_Instructional_Buttons()

        Play_Back_Beep()

  
    ENDIF   
    
    
     
    
    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT)) //Delete - essentially a discard pic.
        
            
        //IF ShouldFirstPersonPhoneHudMovementBeHidden()
            CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549
        //ENDIF


        
            
        IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
        OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE)
        //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE)  //Removed for 1932325 - hard drive save now used.
        //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
        //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE) //Stub for 1708374


            #if IS_DEBUG_BUILD
                PRINTSTRING ("AppCAMERA - Cellphone ExtraSpecialOptionInput detected but not acted upon due to Docks Setup or other camera app bypass bit.")
                PRINTNL()
            #endif


        ELSE

            SET_GAME_PAUSED (FALSE)

            SETTIMERB(0)  // new method, post-game pause


            // Following delayed till end of shutter transition
            // LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1 ) //switch on viewfinder graphics.
            // Setup_Cam_Instructional_Buttons()
            b_ShutterTransition = TRUE




            //Cellphone_Pic_Just_Taken = TRUE  // new method, post-game pause

            Do_Fullscreen_Reposition()

            b_WaitingForDiscardOrSend = FALSE

                
                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                    //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                    v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
                    
                ENDIF

            Play_Back_Beep()


            //Set_Up_Selfie_PitchandHeading()



            // Following delayed till end of shutter transition
            // LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1 ) //switch on viewfinder graphics.
            // Setup_Cam_Instructional_Buttons()
            
            //b_ShutterTransition = TRUE

        ENDIF


    ENDIF



        
    
    IF IS_BIT_SET (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //send pic

            //IF ShouldFirstPersonPhoneHudMovementBeHidden()
                CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549
            //ENDIF



            SET_GAME_PAUSED (FALSE)


            DRAW_LOW_QUALITY_PHOTO_TO_PHONE (FALSE)

            
            WAIT (0) //1422498

            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
            SnapmaticHudHide()

            WAIT (0)

            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
            SnapmaticHudHide()
                    


            FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO()
            FREE_MEMORY_FOR_LOW_QUALITY_PHOTO()




            //Trial basis for Paul...
            //Cellphone_Pic_Just_Taken = TRUE

            //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
            g_b_appHasRequestedTidyUp = TRUE


            
            b_DrawCamHelpText = FALSE

            b_SnapToPortraitImmediately = FALSE

            HIDE_ACTIVE_PHONE (FALSE, TRUE)

            g_b_Rotate3dPhonePortrait = TRUE //will cue exit.
            CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)

            b_OkayToTakePicture = FALSE

            b_Rotate3dPhoneX = TRUE
            b_Rotate3dPhoneY = TRUE
            b_Rotate3dPhoneZ = TRUE

            b_ManipulatePhonePosX = TRUE
            b_ManipulatePhonePosY = TRUE
            b_ManipulatePhonePosZ = TRUE


            Play_Select_Beep()


            b_WaitingForDiscardOrSend = FALSE


                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                    //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                    v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                ENDIF




            Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
            Local_PPE_Toggle (FALSE)

            b_VehicleRenderCheck  = FALSE

            Clear_Modifier_With_Exception_Checks() //#1431423

            //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])


                
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("Cell Cam FALSE 3")
                PRINTNL()

                cdPrintnl()
                cdPrintstring("Cam DE-activated 18")
                cdPrintnl()


            #endif




            #if IS_DEBUG_BUILD
            
                PRINTNL()
                PRINTSTRING ("From WaitingForDiscardOrSend:")
                PRINTSTRING ("About to launch contacts list for pic send!")
                PRINTSTRING ("Normal exit and rotate called for appCamera script.")
                PRINTNL()

            #endif


            

            //Cellphone Flashhand.sc checks for this bit but only launches contacts when this script is confirmed as no longer running.
			
			IF NOT MPGlobalsAmbience.bSkipContactListSendingPhotos
            	SET_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)
			ENDIF

            ShouldDrawShutter = FALSE
            PRINTLN("[app_camera] ShouldDrawShutter = 2 FALSE")                          
                                 
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 16)

     
                   

        ENDIF
    ENDIF



ENDPROC




                                                                                                                            











PROC Draw_Camera_Instructional_Buttons()

    
    //B* 2115760: Don't display buttons if the end screen is running
    IF g_bResultScreenDisplaying OR g_bResultScreenPrepared

        #if IS_DEBUG_BUILD
            cdPrintstring("AppCAMERA - ")
            IF g_bResultScreenDisplaying
				cdPrintstring("bResult Screen Displaying, ")
			ENDIF
            IF g_bResultScreenPrepared
				cdPrintstring("Result Screen Prepared, ")
			ENDIF
            cdPrintstring("won't draw instructional buttons.")
            cdPrintnl()
        #endif

        EXIT
    ENDIF


    // Legacy instruction buttons using movie. 

    IF b_Reposition_Corner = FALSE
   
        IF NOT b_InSaveRoutine //Star logo should be displayed here...

            IF g_FMMC_STRUCT.bSecurityCamActive = FALSE //2009386
            AND g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE//2139232
            
                //IF g_Use_Prologue_Cellphone = FALSE   //Comment in for easy testing of Security Cam exception.

                    DRAW_SCALEFORM_MOVIE_FULLSCREEN (SF_MovieInstButtonsIndex, 255, 255, 255,0)
                   
                //ENDIF
                            
            ELSE

		        #if IS_DEBUG_BUILD
		            cdPrintstring("AppCAMERA - ")
		            IF g_FMMC_STRUCT.bSecurityCamActive
						cdPrintstring("Security cam active, ")
					ENDIF
		            IF g_FMMC_STRUCT.bDisablePhoneInstructions
						cdPrintstring("Disable phone instructions, ")
					ENDIF
		            cdPrintstring("won't draw instructional buttons.")
		            cdPrintnl()
		        #endif

		    ENDIF

    	ELSE

	        #if IS_DEBUG_BUILD
	            cdPrintstring("AppCAMERA - In save routine, won't draw instructional buttons.")
	            cdPrintnl()
	        #endif

	    ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintstring("AppCAMERA - Repositioning to corner, won't draw instructional buttons.")
            cdPrintnl()
        #endif

    ENDIF




    IF b_WaitingForDiscardOrSend = FALSE

      
        IF g_Cellphone_FullScreen_Mode //Default

            IF g_Phone_Active_but_Hidden 

       
                IF b_InSaveRoutine = FALSE

                    IF g_Cellphone.PhoneDS > PDS_AWAY //Make sure we only draw rectangles if the player's phone is still present and not moving off screen.

                        //SET_SCRIPT_GFX_DRAW_ORDER (GFX_ORDER_AFTER_HUD)

                        //DRAW_RECT(0.327, 0.266, 0.175, 0.004, 255, 255, 255, 255)
                        //DRAW_RECT(0.327, 0.803, 0.175, 0.004, 255, 255, 255, 255)
                        //
                        //DRAW_RECT(0.673, 0.266, 0.175, 0.004, 255, 255, 255, 255)
                        //DRAW_RECT(0.673, 0.803, 0.175, 0.004, 255, 255, 255, 255)

                        //DRAW_RECT(0.242, 0.299, 0.004, 0.064, 255, 255, 255, 255)
                        //DRAW_RECT(0.242, 0.770, 0.004, 0.064, 255, 255, 255, 255)

                        //DRAW_RECT(0.758, 0.299, 0.004, 0.064, 255, 255, 255, 255)
                        //DRAW_RECT(0.758, 0.770, 0.004, 0.064, 255, 255, 255, 255)

                    ENDIF

                ENDIF

            ENDIF


        ELSE

            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.059, 0.644, "CELL_284") //"Zoom"

            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.165, 0.644, "CELL_285") //"Move"


            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.275, 0.750, "CELL_280") //"Take pic"

            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.275, 0.830, "CELL_281") //"Exit"

        ENDIF



    ELSE


        IF g_Cellphone_FullScreen_Mode


            /* Legacy instruction buttons using movie.

            IF TIMERB() > 1000
          
                DRAW_SCALEFORM_MOVIE (SF_MovieInstButtonsIndex, 0.5, 0.5, 1.0, 1.0, 255, 255, 255,0)

            ENDIF
            */

            

            /* Yellow rectangle...
            IF b_InSaveRoutine = FALSE

                DRAW_RECT(0.427, 0.366, 0.175, 0.004, 251, 242, 79, 255)
                DRAW_RECT(0.427, 0.703, 0.175, 0.004, 251, 242, 79, 255)


                DRAW_RECT(0.573, 0.366, 0.175, 0.004, 251, 242, 79, 255)
                DRAW_RECT(0.573, 0.703, 0.175, 0.004, 251, 242, 79, 255)


                DRAW_RECT(0.342, 0.399, 0.004, 0.064, 251, 242, 79, 255)
                DRAW_RECT(0.342, 0.670, 0.004, 0.064, 251, 242, 79, 255)


                DRAW_RECT(0.658, 0.399, 0.004, 0.064, 251, 242, 79, 255)
                DRAW_RECT(0.658, 0.670, 0.004, 0.064, 251, 242, 79, 255)

            ENDIF
            */


        ELSE

            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.275, 0.750, Get_Send_Photo_Text_Label()) //"Send pic"


            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.275, 0.790, "CELL_286") //"Continue"


            format_cellcam_ostext (255, 255, 255, 255)
            DISPLAY_TEXT (0.275, 0.830, "CELL_281") //"Exit"


        ENDIF



            
    ENDIF



ENDPROC






PROC InitialiseCamera()


    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_Cellphone.PhoneDS = PDS_COMPLEXAPP //put it in this state so that we bypass the integrated back button check from flash handler.

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 12. appCamera assigns PDS_COMPLEXAPP")
            cdPrintnl()   
        #endif

    ENDIF





    Cellphone_Pic_Just_Taken = FALSE

    b_WaitingForDiscardOrSend = FALSE


            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

            ENDIF



   


    IF g_Cellphone_FullScreen_Mode
            
        WHILE TIMERA() < i_ShutterDelayForAnimation
        
            WAIT (0)

            Draw_Shutter_Movie_With_Status_Check()
            Disable_Camera_Movement_Controls()
            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
        
        ENDWHILE



        LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")
        SnapmaticHudHide()

       	Setup_Cam_Instructional_Buttons()


    
        b_BeginInitialisationPause = TRUE
        SETTIMERB(0)


    
    ELSE


        Wrapped_CellCam_StandardModeActivate (TRUE, TRUE) //activate camera and go first person.
        Local_PPE_Toggle (TRUE)

        //SET_TIMECYCLE_MODIFIER("phone_cam") //#1431423
        SetUpScanlines()

        //ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)
        //SET_TIMECYCLE_MODIFIER(s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
        SetUpCurrentTCModifier()

        
        Wrapped_CellCam_SelfieModeActivate (FALSE)
        b_VehicleRenderCheck = TRUE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("Cam activated 1")
            cdPrintnl()

            PRINTNL()
            PRINTSTRING ("Cell Cam TRUE 4")
            PRINTNL()

        #endif


    ENDIF




ENDPROC







PROC RotatePhoneToWidescreen()



    //Starting rotation for reference currently at <<-89.4, -3.4, -0.1>>


    //g_This_Screen_3dPhoneStartVec[DISPLAY_16_9] = <<-101.5, -135.5, -113.0>> // Old RHS<<41.0, -100.0, -68.0>> //offscreen
    //g_This_Screen_3dPhoneEndVec[DISPLAY_16_9] = <<-101.5, -50.0, -113.0>>  // Old RHS<<41.0, -14.0, -68.0>> //bottom left onscreen

 
    //g_This_Screen_3dPhoneStartVec[DISPLAY_4_3] = <<0.5, -135.5, -113.0>> // Old RHS<<41.0, -100.0, -68.0>>
    //g_This_Screen_3dPhoneEndVec[DISPLAY_4_3] = <<0.5, -50.0, -113.0>>  // Old RHS<<41.0, -14.0, -68.0>>



 
    //Rotate left and manipulate position if need be...


    IF g_Cellphone_FullScreen_Mode

        v_3dDesiredRotation = <<-90.3, 0.0, 90.0>> //Swap between these lines to enable the rotation to landscape, need to alter rotation when taking pic though...
                                                   //Saved as Fixed_rotation_snapshot.jpg - shelved in 5543218
        
        //GET_MOBILE_PHONE_ROTATION (v_3dDesiredRotation)
          
           
        v_3dDesiredPosition = <<1.5, 0.0, -17.0>> //16-9



        IF ShouldFirstPersonPhoneHudMovementBeHidden()  //Bypass all phone movement and instantly jump to conclusion segment of this routine if in first person.
                 
            GET_MOBILE_PHONE_ROTATION (v_FPcurrentRot)
            v_3dDesiredRotation = v_FPcurrentRot


            GET_MOBILE_PHONE_POSITION (v_FPcurrentPos)
            v_3dDesiredPosition = v_FPcurrentPos


            b_Rotate3dPhoneX = FALSE
            b_Rotate3dPhoneY = FALSE
            b_Rotate3dPhoneZ = FALSE
            b_ManipulatePhonePosX = FALSE
            b_ManipulatePhonePosY = FALSE
            b_ManipulatePhonePosZ = FALSE


        ENDIF



        #if IS_DEBUG_BUILD

            cdPrintstring("Testing Primary Widescreen Rotation")
            cdPrintnl()

        #endif






        IF b_Rotate3dPhoneZ = FALSE  //don't start position manipulation until rotation has taken place...


            
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING("v_3dCameraphoneRotationX ")
                PRINTFLOAT(v_3dCameraphoneRotation.x)    
                
                PRINTSTRING(" v_3dCameraphoneRotationY ")
                PRINTFLOAT(v_3dCameraphoneRotation.y)

                PRINTSTRING(" v_3dCameraphoneRotationZ ")
                PRINTFLOAT(v_3dCameraphoneRotation.z)
                PRINTNL()

            #endif
            

            //RHS<<101.0, -48.5, -113.0>>    //LHS<<-101.5, -48.5, -113.0>>  
                    
            IF b_ManipulatePhonePosX
                //v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 14.0) //LHS
                
                v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X - 14.0) //RHS  

            ENDIF
            
            IF v_3dCameraphonePosition.X < v_3dDesiredPosition.X //Change < or > for LHS or RHS
            OR v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                b_ManipulatePhonePosX = FALSE

                //PRINTSTRING ("Testing position manipulation RHS")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosY
                v_3dCameraphonePosition.Y = (v_3dCameraphonePosition.Y + 7.0) //LHS 
               
            ENDIF
            
            IF v_3dCameraphonePosition.Y > v_3dDesiredPosition.Y 
            OR v_3dCameraphonePosition.Y = v_3dDesiredPosition.Y

                b_ManipulatePhonePosY = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosZ
                v_3dCameraphonePosition.Z = (v_3dCameraphonePosition.Z + 12.0) //JulyRotationTest//(v_3dCameraphonePosition.Z + 12.0) //LHS
            ENDIF
            
            IF v_3dCameraphonePosition.Z > v_3dDesiredPosition.Z 
            OR v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z

                b_ManipulatePhonePosZ = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF

            //Comment in to stagger rotation routine.
            //WAIT (1000)


        ENDIF





        IF b_Rotate3dPhoneX
            v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X - 1.0)
        ENDIF


        IF v_3dCameraphoneRotation.X < v_3dDesiredRotation.X //-110.6
        OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

            v_3dCameraphoneRotation.X = v_3dDesiredRotation.X 

            b_Rotate3dPhoneX = FALSE

            //PRINTSTRING ("Testing rotation")
            //PRINTNL()


        ENDIF


        IF b_Rotate3dPhoneY
            v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y - 0.5)
        ENDIF

        IF v_3dCameraphoneRotation.Y < v_3dDesiredRotation.Y //1.9
        OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9
        
            v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y 

            b_Rotate3dPhoneY = FALSE

        ENDIF


        IF b_Rotate3dPhoneZ
            v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z + 11.0) //Was 7.0  #1431423     
        ENDIF

        IF v_3dCameraphoneRotation.Z > v_3dDesiredRotation.Z //87.9
        OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9

            v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z 
        
            PRINTSTRING ("Finished setting CellphoneRotation for Camera")
            PRINTNL()

        
            b_Rotate3dPhoneZ = FALSE

        ENDIF

        IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE
        
        SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

        SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition) 
 
        ENDIF 
 

    ELSE

        v_3dDesiredRotation = <<-93.9, 4.9, 90.7>>
                                                            

        IF b_Rotate3dPhoneX
            v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X - 1.0)
        ENDIF

        IF v_3dCameraphoneRotation.X < v_3dDesiredRotation.X //-110.6
        OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

            b_Rotate3dPhoneX = FALSE

            //PRINTSTRING ("Testing rotation")
            //PRINTNL()


        ENDIF


        IF b_Rotate3dPhoneY
            v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y + 2.0)
        ENDIF

        IF v_3dCameraphoneRotation.Y > v_3dDesiredRotation.Y //1.9
        OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9
        
            

            b_Rotate3dPhoneY = FALSE

        ENDIF


        IF b_Rotate3dPhoneZ
            v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z + 7.0)
        ENDIF

        IF v_3dCameraphoneRotation.Z > v_3dDesiredRotation.Z //87.9
        OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9
        
            b_Rotate3dPhoneZ = FALSE

        ENDIF

          
        SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

       
    ENDIF




    






   


    IF g_Cellphone_FullScreen_Mode


        IF ((b_Rotate3dPhoneX = FALSE)
        AND (b_Rotate3dPhoneY = FALSE)
        AND (b_Rotate3dPhoneZ = FALSE))
        AND (b_ManipulatePhonePosX = FALSE)
        AND (b_ManipulatePhonePosY = FALSE)
        AND (b_ManipulatePhonePosZ = FALSE)


            IF ShouldFirstPersonPhoneHudMovementBeHidden()
            
                //CellphoneFirstPersonHorizontalModeToggle(TRUE)  //Causes problems when cued here! See 1987549

            ELSE

            v_3dCameraphoneRotation = v_3dDesiredRotation //make final adjustments to rotation and make sure rotation vector in use is correct value

            SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 


            v_3dCameraphonePosition = v_3dDesiredPosition
            SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition)

            ENDIF






            IF b_CheckingStorage = FALSE //Finish checking storage before allowing camera to start operation.

                g_b_Rotate3dPhoneLandscape = FALSE

                Wrapped_CellCam_StandardModeActivate (TRUE, TRUE) //TEMP STEVE!

        
                //WAIT (10000)

                b_VehicleRenderCheck  = TRUE

                Local_PPE_Toggle (TRUE)


                //SET_TIMECYCLE_MODIFIER("phone_cam") //#1431423
                SetUpScanlines()

                //ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)
                //SET_TIMECYCLE_MODIFIER(s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
                SetUpCurrentTCModifier()


                Wrapped_CellCam_SelfieModeActivate (FALSE)

                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING ("Cell Cam TRUE 5")
                    PRINTNL()

                    cdPrintnl()
                    cdPrintstring("Cam activated 2")
                    cdPrintnl()


                #endif


                SETTIMERA (0)

                //July work
                ShouldDrawShutter = TRUE
				PRINTLN("[app_camera] ShouldDrawShutter = 1 TRUE")  
                b_DrawCamHelpText = TRUE            
                
                //HIDE_ACTIVE_PHONE (TRUE, TRUE)  //Moved to b_BeginInitialisationPause for #1431423


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - Hiding active phone 2.")
                    cdPrintnl()

                #endif


                

                Draw_Shutter_Movie_With_Status_Check()


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "DISPLAY_VIEW", TO_FLOAT(ShutterTypeDecision)) //Shutter screen.

                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring("Setting Photo Frame On if hide bit not set - 8")
                    cdPrintnl()
                #endif


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                 
                
                              
                InitialiseCamera()



            ENDIF
            
        
        ENDIF

    ELSE

        IF ((b_Rotate3dPhoneX = FALSE)
        AND (b_Rotate3dPhoneY = FALSE)
        AND (b_Rotate3dPhoneZ = FALSE))

            v_3dCameraphoneRotation = v_3dDesiredRotation //make final adjustments to rotation.

            SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 


            g_b_Rotate3dPhoneLandscape = FALSE


            //InitialiseCamera() //done at script start if condition met...

        ENDIF

    ENDIF
   

   


ENDPROC







PROC MaintainSaveRoutine()


    Check_for_List_Navigation()


ENDPROC










PROC Common_Exit()


        //2217483 - Commenting this one out, making the position change first to prevent any weirdness of the rotation showing for a frame.
        //v_3dCameraphoneRotation = v_3dDesiredRotation //make final adjustments to rotation.
        //SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

        CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549


        IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY) //2217483

                 SET_MOBILE_PHONE_POSITION (g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio]) //Off screen position
        
            ELSE
            SET_MOBILE_PHONE_POSITION (g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]) //Zenith of normal phone movement.

            ENDIF


            //2217483 - After the position has been manipulated, alter the rotation.
            v_3dCameraphoneRotation = v_3dDesiredRotation //make final adjustments to rotation.
            SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

        ENDIF


   

        g_b_Rotate3dPhonePortrait = FALSE

        
        Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
        Local_PPE_Toggle (FALSE)
         
        b_VehicleRenderCheck  = FALSE

        Clear_Modifier_With_Exception_Checks() //#1431423

        //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])


        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("Cell Cam FALSE 6")
            PRINTNL()

            cdPrintnl()
            cdPrintstring("Cam DE-activated 23")
            cdPrintnl()


        #endif
       

        g_MainGui_RendertargetNeedsReset = TRUE //We need to tell  cellphone_flashhand.sc that we want to reset the render target which will in turn re-enable the main 
                                                //scaleform movies.


        #if IS_DEBUG_BUILD
    
            PRINTSTRING ("FEB 14 appCamera rotated to portrait - now exiting normally.")
            PRINTNL()
            
        #endif

        

       
        Cellphone_Pic_Just_Taken = FALSE


        //As this script is not checking CHECK_APPLICATION_EXIT() we need to have a failsafe that ensures the correct display state is selected and scaleform 
        //resets the display to the homescreen to avoid any timing issues. Pressing "Select" then "Back" in very quick succession for example.

        IF g_Cellphone.PhoneDS > PDS_TAKEOUT //i.e don't run this if the phone has just been put into disabled or away state
           

            g_Cellphone.PhoneDS = PDS_MAXIMUM

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 13. appCamera assigns PDS_MAXIMUM")
                cdPrintnl()   
            #endif

            g_InputButtonJustPressed = TRUE //2011968 - In first person mode, the phone exits Snapmatic quicker. Need this to make sure the button press doesn't carry over to cellphone_fh.

            Update_Scaleform_VSE ()



        ENDIF

 


        IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index) //This needs to be done here, in case the movie aborts. See bug 740117 

            LEGACY_SCALEFORM_MOVIE_METHOD (SF_Movie_Gallery_Shutter_Index, "SHUTDOWN_MOVIE")

        ENDIF

        WAIT (0)


        REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092

        SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieInstButtonsIndex)
        SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_Movie_Gallery_Shutter_Index)


        IF g_b_Secondary_Screen_Available = TRUE
            
            SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

        ELSE

            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
        
        ENDIF


        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO) 
        
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
   
        CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)

        CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)

        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC)


        SET_GAME_PAUSED (FALSE)

        //IF IS_PLAYER_PLAYING (PLAYER_ID()) // Don't need wrapper. Checked with Graeme. Would be dangerous anyway. Might not be re-enabled for dead player.

            SET_PLAYER_CAN_DO_DRIVE_BY (PLAYER_ID(), TRUE)

        //ENDIF



        RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)


        //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
        g_b_appHasRequestedTidyUp = TRUE

        STOP_SOUND (Zoom_SoundID)
        RELEASE_SOUND_ID (Zoom_SoundID)


        BUSYSPINNER_OFF()



        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())


            //set briefly to aiming to help speed up transition on termination of Snapmatic, see #1926523
            SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), "Mood_Normal_1")


            CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

        ENDIF



        Clear_Modifier_With_Exception_Checks() //#1431423

        //Make sure these are returned to default state.
        CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(FALSE)
        SET_MOBILE_PHONE_DOF_STATE(FALSE)
        CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(1.0)

        //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
    

        IF g_i_CurrentlySelected_SnapMaticBorder > 0
        AND g_i_CurrentlySelected_SnapMaticBorder < i_NumberOfBordersAvailable  
        
            SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder])

        ENDIF


        HIDE_ACTIVE_PHONE (FALSE, TRUE)
        
        //Deativate multihead blinders on PC
        IF SET_MULTIHEAD_SAFE(FALSE,TRUE,b_ActivatedMultiheadBlinders, TRUE)
            b_ActivatedMultiheadBlinders = FALSE
        ENDIF


        TERMINATE_THIS_THREAD()


ENDPROC






PROC Reposition_Phone_to_Fullscreen()


    IF g_Cellphone_FullScreen_Mode

            /*Redacted changes as part of 1025486
            v_3dDesiredPosition = <<1.5, 0.0, -17.0>> //16-9

       

                    
            IF b_ManipulatePhonePosX
                //v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 14.0) //LHS
                v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X - 14.0) //RHS

            ENDIF
            
            IF v_3dCameraphonePosition.X < v_3dDesiredPosition.X //Change < or > for LHS or RHS
            OR v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                b_ManipulatePhonePosX = FALSE

                //PRINTSTRING ("Testing position manipulation RHS")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosY
                v_3dCameraphonePosition.Y = (v_3dCameraphonePosition.Y + 7.0) //LHS 
               
            ENDIF
            
            IF v_3dCameraphonePosition.Y > v_3dDesiredPosition.Y 
            OR v_3dCameraphonePosition.Y = v_3dDesiredPosition.Y

                b_ManipulatePhonePosY = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosZ
                v_3dCameraphonePosition.Z = (v_3dCameraphonePosition.Z + 12.0) //LHS
            ENDIF
            
            IF v_3dCameraphonePosition.Z > v_3dDesiredPosition.Z 
            OR v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z

                b_ManipulatePhonePosZ = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF

            SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition)


            IF ((b_ManipulatePhonePosX = FALSE)
            AND (b_ManipulatePhonePosY = FALSE)
            AND (b_ManipulatePhonePosZ = FALSE))


                b_Reposition_Fullscreen = FALSE

                HIDE_ACTIVE_PHONE (TRUE, TRUE)
                
                CELL_CAM_ACTIVATE (TRUE, TRUE)

                //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
                g_b_appHasRequestedTidyUp = TRUE

           
            ENDIF
            */
            



                //New for 1025486
                b_Reposition_Fullscreen = FALSE

                //SET_TIMECYCLE_MODIFIER("phone_cam") //#1431423   - Moved before the HIDE... for #1570185
                SetUpScanlines()

                //ANIMPOSTFX_PLAY (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter], 0, TRUE)
                //SET_TIMECYCLE_MODIFIER(s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
                //SetUpCurrentTCModifier() //Moved from here due to #1689345. Search this bug number for new locations. 


                HIDE_ACTIVE_PHONE (TRUE, TRUE)



                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("APPCAMERA - Hiding active phone. 3")
                    cdPrintnl()

                #endif

                
                Wrapped_CellCam_StandardModeActivate (TRUE, TRUE) //Trial removal for investigating 1954420 failed. Looks bollocks.


                b_VehicleRenderCheck = TRUE 
                 
                


       

                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING ("Cell Cam TRUE 7")
                    PRINTNL()
                    cdPrintnl()
                    cdPrintstring("Cam activated 3")
                    cdPrintnl()

                #endif


   
                g_b_appHasRequestedTidyUp = TRUE


                //Set_Up_Selfie_PitchandHeading()

                //Set_Up_Pre_Picture_Taken_Selfie_PitchandHeading()

        ENDIF



ENDPROC










PROC Reposition_Phone_to_Corner() //This fires when the player takes a picture. The landscape phone previewing the image appears in the bottom right in 3rd person only.


    //DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)



    v_3dDesiredPosition = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]




    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("Cell Cam FALSE 1")
        PRINTNL()

        cdPrintnl()
        cdPrintstring("DEBUG - ROUTINE Checking Cellphone Camera Corner Reposition.")
        cdPrintnl()

    #endif




    IF g_Cellphone_FullScreen_Mode


            #if IS_DEBUG_BUILD 

                cdPrintnl()
                cdPrintstring("DEBUG - FULLSCREEN MODE.")
                cdPrintnl()

            #endif




            //Perhaps cell_cam_activate is returning TRUE and the game thinks it's in first person mode.
            //IF IsCurrentCamFirstPerson()  //Bypass all phone movement and instantly jump to conclusion segment of this routine if in first person.
            //AND g_bInMultiplayer = FALSE

            IF ShouldFirstPersonPhoneHudMovementBeHidden()
     
                GET_MOBILE_PHONE_POSITION (v_FPcurrentPos)  //Critical. Makes sure phone remains off screen when previewing snap.
                v_3dDesiredPosition = v_FPcurrentPos

                b_ManipulatePhonePosX = FALSE
                b_ManipulatePhonePosY = FALSE
                b_ManipulatePhonePosZ = FALSE

                //CellphoneFirstPersonHorizontalModeToggle(TRUE)  //Can cause problems but we need to show the phone in landscape after taking the pic in first person. Looks daft otherwise. See 1987549

            ENDIF

            //Requested in SP also by 1995940
            CellphoneFirstPersonHorizontalModeToggle(TRUE)
        


        
            IF b_ManipulatePhonePosX
                //v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X - 12.0) //LHS
                v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 12.0) //RHS
                //PRINTSTRING ("Testing position manipulation1")
                //PRINTNL()


            ENDIF
            
            IF v_3dCameraphonePosition.X > v_3dDesiredPosition.X  //Change < or > for LHS or RHS
            OR v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                b_ManipulatePhonePosX = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosY
                v_3dCameraphonePosition.Y = (v_3dCameraphonePosition.Y - 6.0)
            ENDIF
            


            
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_REPOSITION_SUBS_FOR_LANDSCAPE_PHONE )
                
                IF v_3dCameraphonePosition.Y < (v_3dDesiredPosition.Y + 15.0) //Makes space for pesky instructional buttons.
                OR v_3dCameraphonePosition.Y = (v_3dDesiredPosition.Y + 15.0)

                    v_3dCameraphonePosition.Y = (v_3dDesiredPosition.Y + 15.0)


                    b_ManipulatePhonePosY = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("APPCAMERA - Long Subtitle bit is set. Making extra text space.")
                        cdPrintnl()

                    #endif


                ENDIF


            ELSE    

                IF v_3dCameraphonePosition.Y < (v_3dDesiredPosition.Y + 10.0) //Makes space for pesky instructional buttons.
                OR v_3dCameraphonePosition.Y = (v_3dDesiredPosition.Y + 10.0)

                    v_3dCameraphonePosition.Y = (v_3dDesiredPosition.Y + 10.0)


                    b_ManipulatePhonePosY = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("APPCAMERA - Long Subtitle bit is not set. Using normal space.")
                        cdPrintnl()

                    #endif


                ENDIF


            ENDIF




            IF b_ManipulatePhonePosZ
                v_3dCameraphonePosition.Z = (v_3dCameraphonePosition.Z - 10.0)
            ENDIF
            
            IF v_3dCameraphonePosition.Z < v_3dDesiredPosition.Z 
            OR v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z


                v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z 

                b_ManipulatePhonePosZ = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            

            //Modify this for #1531987
            
            //+15 should do it for the adjusted landscape position.

            //This six line segment injected to get rid of the screen movement and roughly align phone to instructional buttons as a temporary measure.

            //IF IsCurrentCamFirstPerson() = FALSE //2022392
            //OR g_bInMultiplayer //2027700 - make sure phone is still displayed in bottom right in MP. It's not used in hand.

            IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE


            IF IS_BIT_SET (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_REPOSITION_SUBS_FOR_LANDSCAPE_PHONE )

                v_3dDesiredPosition.y = v_3dDesiredPosition.y + 15.0 //Makes space for pesky instructional buttons.

            ELSE

                v_3dDesiredPosition.y = v_3dDesiredPosition.y + 10.0 //Makes space for pesky instructional buttons.

            ENDIF


            v_3dDesiredPosition.x = v_3dDesiredPosition.x - 14.0    //This provides the jump across to align the buttons and the edge of the phone when in landscape mode.
                                                                    //In to provide alignment with instructional buttons. Les TODO #1414168

            
            ELSE
            
                v_3dDesiredPosition =  g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio]

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("Applying failsafe 1st person position set for 2027700")
                    cdPrintnl()

                #endif
           
            ENDIF      
      
            
         

            v_3dCameraphonePosition = v_3dDesiredPosition
            b_ManipulatePhonePosZ = FALSE
            b_ManipulatePhonePosY = FALSE
            b_ManipulatePhonePosX = FALSE


            SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition)


            IF ((b_ManipulatePhonePosX = FALSE)
            AND (b_ManipulatePhonePosY = FALSE)
            AND (b_ManipulatePhonePosZ = FALSE))


                b_Reposition_Corner = FALSE


                //g_This_Screen_3dPhoneStartVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 117.2), (GET_SAFE_ZONE_SIZE() * -158.8), -113.0>>        
                //g_This_Screen_3dPhoneEndVec[DISPLAY_16_9] = <<(GET_SAFE_ZONE_SIZE() * 117.2), (GET_SAFE_ZONE_SIZE() * -53.3), -113.0>> 

                v_3dDesiredRotation = <<-90.3, 0.0, 90.0>> //Failsafe fix for 1934037

                SET_MOBILE_PHONE_ROTATION (v_3dDesiredRotation)
                                      
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("Applying failsafe rotation for 1934037")
                    cdPrintnl()
                
                #endif


                                      
                
                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)  //Only display help text for standard use...


                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
                
                        TokenDecision = "CELL_294"

                        SetUp_Continue_FirstSave_With_Send_Buttons()

                    ELSE

                        TokenDecision = "CELL_293"

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("DEBUG_SEGMENT 2")
                            cdPrintnl()

                        #endif


                        SetUp_Continue_FirstSave_No_Send_Buttons()

                    ENDIF


                    /*Replaced by instructional buttons

                    IF IS_HELP_MESSAGE_BEING_DISPLAYED()
                        HELP_AT_SCREEN_LOCATION (TokenDecision, f_SecondaryHelpX, f_SecondaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
                    ELSE
                        HELP_AT_SCREEN_LOCATION (TokenDecision, f_PrimaryHelpX, f_PrimaryHelpY, HELP_TEXT_WEST, FLOATING_HELP_PRINT_FOREVER)
                    ENDIF

                    */

                    Update_Help_State = 2

                ENDIF
                
        
           
            ENDIF




    ENDIF



ENDPROC











PROC RotatePhoneToPortrait_and_Exit()



    //Finish rotation <<-110.6, 1.9, 88.4>>



    //WAIT (1500)

    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)

    SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))) //See bug 896700. Strange one, as flashhand is already doing this.


    v_3dDesiredRotation = g_3dPhoneStartRotVec


    v_3dDesiredPosition = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]






    IF ShouldFirstPersonPhoneHudMovementBeHidden()  //Bypass all phone movement and instantly jump to conclusion segment of this routine if in 
                 
        GET_MOBILE_PHONE_ROTATION (v_FPcurrentRot)
        v_3dDesiredRotation = v_FPcurrentRot


        GET_MOBILE_PHONE_POSITION (v_FPcurrentPos)
        v_3dDesiredPosition = v_FPcurrentPos


        b_Rotate3dPhoneX = FALSE
        b_Rotate3dPhoneY = FALSE
        b_Rotate3dPhoneZ = FALSE
        b_ManipulatePhonePosX = FALSE
        b_ManipulatePhonePosY = FALSE
        b_ManipulatePhonePosZ = FALSE


   ENDIF


















    IF g_Cellphone_FullScreen_Mode

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) // switch off viewfinder graphics.
            Wrapped_CellCam_SelfieModeActivate (FALSE)
            b_InSelfieMode = FALSE
                

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY) //2217483

                b_Rotate3dPhoneX = FALSE
                b_Rotate3dPhoneY = FALSE
                b_Rotate3dPhoneZ = FALSE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("AppCamera - g_BS_MOVE_PHONE_DOWN_INSTANTLY bit set, Common_Exit will set Start Rotation and Starting Position for phone rot and pos.")
                    cdPrintnl()   
                #endif

                Common_Exit()

            ENDIF



            IF b_ManipulatePhonePosX
                //v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X - 12.0) //LHS
                //v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 12.0) //RHS
                //PRINTSTRING ("Testing position manipulation1")
                //PRINTNL()


                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC)
                    v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 2.0) //Was 12.0 added per frame, slowed down for Les TODO #1414168
                ELSE
                   v_3dCameraphonePosition.X = (v_3dCameraphonePosition.X + 12.0) //Was 12.0 added per frame, slowed down for Les TODO #1414168
                ENDIF


            ENDIF
            
            IF v_3dCameraphonePosition.X > v_3dDesiredPosition.X  //Change < or > for LHS or RHS
            OR v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                v_3dCameraphonePosition.X = v_3dDesiredPosition.X

                b_ManipulatePhonePosX = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosY
                v_3dCameraphonePosition.Y = (v_3dCameraphonePosition.Y - 6.0)
            ENDIF
            
            IF v_3dCameraphonePosition.Y < v_3dDesiredPosition.Y 
            OR v_3dCameraphonePosition.Y = v_3dDesiredPosition.Y

                v_3dCameraphonePosition.Y = v_3dDesiredPosition.Y 


                b_ManipulatePhonePosY = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF



            IF b_ManipulatePhonePosZ
                v_3dCameraphonePosition.Z = (v_3dCameraphonePosition.Z - 10.0)
            ENDIF
            
            IF v_3dCameraphonePosition.Z < v_3dDesiredPosition.Z 
            OR v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z


                v_3dCameraphonePosition.Z = v_3dDesiredPosition.Z 

                b_ManipulatePhonePosZ = FALSE

                //PRINTSTRING ("Testing position manipulation")
                //PRINTNL()


            ENDIF


        

        IF b_ManipulatePhonePosZ = FALSE


            IF b_Rotate3dPhoneX
                v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X + 1.0)

            ENDIF


            IF v_3dCameraphoneRotation.X > v_3dDesiredRotation.X //-110.6
            OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

                v_3dCameraphoneRotation.X = v_3dDesiredRotation.X 

                b_Rotate3dPhoneX = FALSE

            ENDIF




            IF b_Rotate3dPhoneY
                v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y - 2.0)

            ENDIF

            IF v_3dCameraphoneRotation.Y < v_3dDesiredRotation.Y //1.9
            OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9

                v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y

                b_Rotate3dPhoneY = FALSE

            ENDIF




            IF b_Rotate3dPhoneZ
                v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z - 14.0) //7.0) //5.0)

            ENDIF

            IF v_3dCameraphoneRotation.Z < v_3dDesiredRotation.Z //87.9
            OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9

                v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z   
            
                b_Rotate3dPhoneZ = FALSE             

            ENDIF

        
        ENDIF

        


        IF b_SnapToPortraitImmediately //Added for bug # 1025518
            
            //This five line segment will make the phone immediately snape to the portrait view in readiness for the launch of appContacts.
            v_3dCameraphoneRotation = v_3dDesiredRotation
            v_3dCameraphonePosition = v_3dDesiredPosition
            b_Rotate3dPhoneX = FALSE
            b_Rotate3dPhoneY = FALSE
            b_Rotate3dPhoneZ = FALSE   

            IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE

            SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

            SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition)

            ENDIF

            HIDE_ACTIVE_PHONE (FALSE, TRUE)

        ELSE

            IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE

            SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 

            SET_MOBILE_PHONE_POSITION (v_3dCameraphonePosition)
        
        ENDIF

        ENDIF

        

        IF ((b_Rotate3dPhoneX = FALSE)
        AND (b_Rotate3dPhoneY = FALSE)
        AND (b_Rotate3dPhoneZ = FALSE))




            //IF ShouldFirstPersonPhoneHudMovementBeHidden()

                //CellphoneFirstPersonHorizontalModeToggle (FALSE) //Causes problems! See 1987549

            //ENDIF







            Common_Exit()

           
        ENDIF


                


    ELSE

        //Normal mode.

            
        IF b_Rotate3dPhoneX
            v_3dCameraphoneRotation.X = (v_3dCameraphoneRotation.X + 1.0)

            //PRINTSTRING ("Testing X rotation")
            //PRINTNL()



        ENDIF

        IF v_3dCameraphoneRotation.X > v_3dDesiredRotation.X //-110.6
        OR v_3dCameraphoneRotation.X = v_3dDesiredRotation.X

            b_Rotate3dPhoneX = FALSE

       

        ENDIF


        IF b_Rotate3dPhoneY
            v_3dCameraphoneRotation.Y = (v_3dCameraphoneRotation.Y - 2.0)

            //PRINTSTRING ("Testing Y rotation")
            //PRINTNL()



        ENDIF

        IF v_3dCameraphoneRotation.Y < v_3dDesiredRotation.Y //1.9
        OR v_3dCameraphoneRotation.Y = v_3dDesiredRotation.Y //1.9
        
            

            b_Rotate3dPhoneY = FALSE

        ENDIF


        IF b_Rotate3dPhoneZ
            v_3dCameraphoneRotation.Z = (v_3dCameraphoneRotation.Z - 7.0) //5.0)

            //PRINTSTRING ("Testing Z rotation")
            //PRINTNL()


        ENDIF

        IF v_3dCameraphoneRotation.Z < v_3dDesiredRotation.Z //87.9
        OR v_3dCameraphoneRotation.Z = v_3dDesiredRotation.Z //87.9
        
            b_Rotate3dPhoneZ = FALSE

                          

        ENDIF




        IF ((b_Rotate3dPhoneX = FALSE)
        AND (b_Rotate3dPhoneY = FALSE)
        AND (b_Rotate3dPhoneZ = FALSE))


            Common_Exit()
           
        ENDIF


    ENDIF

    




    SET_MOBILE_PHONE_ROTATION (v_3dCameraphoneRotation) 



ENDPROC


















SCRIPT


    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


    //This "fixes" 2536651 but that's actually been caused by a change in the instructional buttons. Setting the width to 1.0 used to work absolutely fine in Polish
    //and other languages that made for an elongated instructional bar.
    IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH

        f_ButtonWidth = 1.0

    ELSE

        f_ButtonWidth = 0.7

    ENDIF








    IF NOT Is_DLC_Compatible() //Check for DLC.. If unavailable, remove border / filter swapping functionality

  
        g_i_CurrentlySelected_SnapMaticFilter = 99

        g_i_CurrentlySelected_SnapMaticBorder = 99  //This will bypass the border swapping routine until it is ready to be fully submitted. Comment in line above and make
                                                    //sure increments and decrements are reinstated in routines that check for left and right cellphone d-pad presses.
    
        g_i_CurrentlySelected_SnapMaticExpression = 99
    
    ELSE
    

        //Optional reset of filter and border to zero position. No filter and no border.

        g_i_CurrentlySelected_SnapMaticFilter = 0
    
        g_i_CurrentlySelected_SnapMaticBorder = 0

    ENDIF


    //Set up names for Snapmatic Filter - Currently chosen filter is specified by global g_i_CurrentlySelected_SnapMaticFilter
    /* Old animpostfx list
    s_CurrentFilter[0] = "No_Filter"
    s_CurrentFilter[1] = "MP_Celeb_Win"
    s_CurrentFilter[2] = "DeathFailOut"
    s_CurrentFilter[3] = "MP_Celeb_Lose"

    s_CurrentFilter[4] = "ChopVision"
    s_CurrentFilter[5] = "Dont_tazeme_bro"
    */


    /*
    s_CurrentFilter[0] = "No_Filter"
    s_CurrentFilter[1] = "secret_camera"
    s_CurrentFilter[2] = "CAMERA_BW"
    s_CurrentFilter[3] = "eyeinthesky"
    s_CurrentFilter[4] = "sunglasses"
    s_CurrentFilter[5] = "blackNwhite"
    */



    //Previous filter cycling order, prior to Thomas D's changes in bug 2108712, Nov 2014. 
    //Bad Filters for DoF mode are: phone_cam12  
    /*
    s_CurrentFilter[0] = "No_Filter"

    s_CurrentFilter[1] = "phone_cam1"
    s_CurrentFilter[2] = "phone_cam2"
    s_CurrentFilter[3] = "phone_cam3"  //used to be a dodgy filter, now ok.
    s_CurrentFilter[4] = "phone_cam4"

    s_CurrentFilter[5] = "phone_cam5"
    s_CurrentFilter[6] = "phone_cam6"
    s_CurrentFilter[7] = "phone_cam7"
    s_CurrentFilter[8] = "phone_cam9" //sic - 8 is missing from the timecycle.xml
    s_CurrentFilter[9] = "phone_cam12"
    
    //s_CurrentFilter[10] = "phone_cam10"
    //Removed at the request of url:bugstar:1707202
    //s_CurrentFilter[11] = "phone_cam11"
    //s_CurrentFilter[12] = "phone_cam13"

    */


    
    //New filter cycling order, updated for Thomas D's changes in bug 2108712, Nov 2014. 

    s_CurrentFilter[0] = "No_Filter"

    s_CurrentFilter[1] = "phone_cam1"
    s_CurrentFilter[2] = "phone_cam2"
    s_CurrentFilter[3] = "phone_cam3"
    s_CurrentFilter[4] = "phone_cam4"
    s_CurrentFilter[5] = "phone_cam5"
    s_CurrentFilter[6] = "phone_cam6"

    s_CurrentFilter[7] = "phone_cam7"
    s_CurrentFilter[8] = "phone_cam8"
    s_CurrentFilter[9] = "phone_cam9"
    s_CurrentFilter[10] = "phone_cam10"
    s_CurrentFilter[11] = "phone_cam11"
    s_CurrentFilter[12] = "phone_cam12"





    /*Test Borders
    s_CurrentBorder[0] = "No Border"
    s_CurrentBorder[1] = "MPRankBadge"
    s_CurrentBorder[2] = "MPLeaderboard"
    s_CurrentBorder[3] = "MPRankBadge"
    s_CurrentBorder[4] = "MPLeaderboard"
    */

    s_CurrentBorder[0] = "No_Border"
    
    s_CurrentBorder[1] = "frame1"
    s_CurrentBorder[2] = "frame2"
    s_CurrentBorder[3] = "frame3"
    s_CurrentBorder[4] = "frame4"

    s_CurrentBorder[5] = "frame5"
    s_CurrentBorder[6] = "frame6"
    s_CurrentBorder[7] = "frame7"
    s_CurrentBorder[8] = "frame8"

    s_CurrentBorder[9] = "frame9"
    s_CurrentBorder[10] = "frame10"
    s_CurrentBorder[11] = "frame11"
    s_CurrentBorder[12] = "frame12"



    //In 2074509 Alex requested some minor changes.
    s_FacialEx[0] = "No_Expression"     //Selecting zero will clear facial override to Mood_Normal.
    s_FacialEx[1] = "mood_Aiming_1"     //
    s_FacialEx[2] = "mood_Happy_1"      //
                                        
    s_FacialEx[3] = "mood_smug_1"       //2074509 requested this one back in.
    s_FacialEx[4] = "mood_Injured_1"    //
    s_FacialEx[5] = "mood_sulk_1"       //
    s_FacialEx[6] = "mood_Angry_1"      //
    
    
    //s_FacialEx[6] = "mood_stressed_1" //2074509 requested removal of this one.


    //Removed for 2087693. Remember to CONST_INT i_NumberOfExpressionsAvailable when adding or removing new expressions.
    //s_FacialEx[7] = "dead_1"
    //s_FacialEx[8] = "electrocuted_1"
    //s_FacialEx[9] = "pain_1"
    //s_FacialEx[10] = "mood_drunk_1"
    //s_FacialEx[11] = "mood_sleeping_1"




    Initialise_Selfie_Offset_Values()


    //Ativate multihead blinders on PC
    IF SET_MULTIHEAD_SAFE(TRUE,TRUE,NOT b_ActivatedMultiheadBlinders, TRUE)
        b_ActivatedMultiheadBlinders = TRUE
    ENDIF


    //Temp!
    Temp_Setup_Of_Test_Gallery_Data()

    Zoom_SoundID = GET_SOUND_ID()

    
    IF g_Chosen_Ratio = DISPLAY_16_9

        f_PrimaryHelpX = 0.207 
        f_PrimaryHelpY = 0.158

        f_SecondaryHelpX = 0.207
        f_SecondaryHelpY = 0.335

    ELSE  //4_3

        f_PrimaryHelpX = 0.240
        f_PrimaryHelpY = 0.258

        f_SecondaryHelpX = 0.240
        f_SecondaryHelpY = 0.435


    ENDIF
   

    g_b_Rotate3dPhonePortrait = FALSE
    g_b_Rotate3dPhoneLandscape = FALSE


    
        
    IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID()) 

    
        IF IS_PED_ON_MOUNT (PLAYER_PED_ID())
                
            g_b_Rotate3dPhonePortrait = TRUE      

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("No camera use on horseback. See Bug 519347.")
                cdPrintnl()   
            #endif

        ENDIF



        IF GET_USINGNIGHTVISION()

            g_b_Rotate3dPhonePortrait = TRUE      

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("No camera use during nightvision. See Bug 1878616.")
                cdPrintnl()   
            #endif

        ENDIF



        IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))

       
            g_b_Rotate3dPhonePortrait = TRUE      

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("App Camera - Prevent third person picture by aborting camera if about to enter vehicle.  See Bug 1775394.")
                cdPrintnl()   
            #endif

        ENDIF







        IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsAimingGun)

            g_b_Rotate3dPhonePortrait = TRUE      

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("No use when aiming. See Bug 495833.")
                cdPrintnl()   
            #endif

        ENDIF



        IF IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
        //OR IS_PLAYER_RIDING_TRAIN(PLAYER_ID())
            g_b_Rotate3dPhonePortrait = TRUE      

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("AppCamera - No camera use in train. See Bug 1336898.")
                cdPrintnl()   
            #endif

        ENDIF


        


        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())


            IF  b_InSelfieMode = TRUE
            
                g_b_Rotate3dPhonePortrait = TRUE  //Potential fix for 1609405

            ENDIF 

  
            tempVehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID())

            IF GET_ENTITY_MODEL (tempVehicleIndex) = RHINO   //no camera use in tank. See bug 394784
            OR GET_ENTITY_MODEL (tempVehicleIndex) = CUTTER //#1165672
            OR GET_ENTITY_MODEL (tempVehicleIndex) = SUBMERSIBLE   //no camera use in submersible. See bug #1126212
			OR GET_ENTITY_MODEL (tempVehicleIndex) = KHANJALI      //no camera use in khanjali. See bug #4095159
            OR (GET_ENTITY_MODEL (tempVehicleIndex) = BARRAGE AND IS_TURRET_SEAT(tempVehicleIndex, GET_SEAT_PED_IS_IN(PLAYER_PED_ID(),TRUE)))  //no camera use in barrage turrets. See bug #4095065
				
                g_b_Rotate3dPhonePortrait = TRUE

            ELSE
            
                IF (GET_PED_IN_VEHICLE_SEAT(tempVehicleIndex, VS_DRIVER) = PLAYER_PED_ID()) //Player must be driver...
                    IF GET_ENTITY_UPRIGHT_VALUE (tempVehicleIndex) > 0.0 //fix for bug 611433

                        IF NOT g_bInMultiplayer //1374468
                            IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() //1485273
                            
                                TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), tempVehicleIndex, TEMPACT_HANDBRAKESTRAIGHT, 4000)
                            
                            
                            ENDIF
                        ENDIF

                    ENDIF
                ENDIF

            ENDIF



        ENDIF


    ENDIF


    IF g_bInMultiplayer //can't call commands on an entity owned by another in MP. See bug 453079

        IF IS_PLAYER_PLAYING (PLAYER_ID()) 

            SET_PLAYER_CAN_DO_DRIVE_BY (PLAYER_ID(), FALSE) //Fix for bug 495666

        ENDIF

    ENDIF

   







    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)


    INHIBIT_CELLPHONE_CAMERA_FUNCTIONS_FOR_DOCKS_SETUP(FALSE) //Make sure related bit is cleared on camera startup. 

    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


    IF g_Cellphone_FullScreen_Mode = FALSE

        InitialiseCamera()

    ELSE
    

        
        SF_Movie_Gallery_Shutter_Index = REQUEST_SCALEFORM_MOVIE ("camera_gallery")


        SF_MovieInstButtonsIndex = REQUEST_SCALEFORM_MOVIE ("instructional_buttons")

        WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)
        OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieInstButtonsIndex)
                 
            WAIT (0)
      
            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092

            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)

        ENDWHILE



        IF g_bInMultiplayer

            ShutterTypeDecision = 2  //Could differentiate cops and crooks here, if phone models change.

        ELSE

            SWITCH GET_SINGLEPLAYER_CELLPHONE_OPERATING_SYSTEM() 

                CASE OS_BITTERSWEET
                    ShutterTypeDecision = 2 //should be 1 but the weird flash is problematic as the shutter is transparent when closed. Using 2 until a resolution.                                                             
                BREAK

                CASE OS_FACADE
                    ShutterTypeDecision = 2//3 is the vertical shutter but this proved unpopular, see bug 1025486                                                              
                BREAK

                CASE OS_IFRUIT
                    ShutterTypeDecision = 2 //iris style...                                                               
                BREAK

            ENDSWITCH


        ENDIF




        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "DISPLAY_VIEW", TO_FLOAT(ShutterTypeDecision)) //Shutter screen.

        LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "CLOSE_SHUTTER") //make sure shutter is closed.

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("AppCamera - Preliminary CLOSING of shutter.")
            cdPrintnl()
        #endif




        //Test block to prove 13 works when used in view state below.
        //01.07.14 - Removed to help Stephen Robertson's work on PC button integration.
        /*
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(13)), (TO_FLOAT(1)),
                                        (TO_FLOAT(1)),
                                         INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                                         "CELL_1")  
        */                        



        //Bug this for Eddie.
        //01.07.14 - Rejigged to help Stephen Robertson's work on PC button integration.
        //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 16) //16 Shutter screen, will need to be phone specific also...

        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "DISPLAY_VIEW")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(16)
        END_SCALEFORM_MOVIE_METHOD()
        

    ENDIF








    //Get a starting copy of these two...
    v_3dCameraphoneRotation = g_3dPhoneStartRotVec

    v_3dCameraphonePosition = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]


    g_b_Rotate3dPhoneLandscape = TRUE
    b_Rotate3dPhoneX = TRUE
    b_Rotate3dPhoneY = TRUE
    b_Rotate3dPhoneZ = TRUE

    b_ManipulatePhonePosX = TRUE
    b_ManipulatePhonePosY = TRUE
    b_ManipulatePhonePosZ = TRUE


    b_Reposition_Corner = FALSE
    b_Reposition_FullScreen = FALSE

    

    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_Cellphone.PhoneDS = PDS_COMPLEXAPP //Failsafe - makes sure that the cellphone camera is initialised into complex state. 

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 14. appCamera launched and assigns PDS_COMPLEXAPP")
            cdPrintnl()   
        #endif

    ENDIF


    //Place_Images_in_GallerySlots()



    IF b_InSelfieMode = FALSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)

    ENDIF
    



    InitialiseCheckStorage()



    SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD) 

    

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", 18, 96)

    //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))


    IF NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV()
    //AND (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() //Stub for 1708374


        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            cdPrintnl()
            cdPrintstring("OCT_TU2014_AppCAMERA reports that this account has SOCIAL_NETWORK_SHARING_PRIVILEGES - will allow save if not bypassed by mission bit.")
            cdPrintnl()
            cdPrintnl()

        #endif

    ELSE

        //Amended help text for 1932325 - hard drive save now used.
        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintnl()
            cdPrintstring("OCT_TU2014_AppCAMERA reports that this account DOES NOT have SOCIAL_NETWORK_SHARING_PRIVILEGES - no social club uploading allowed but can save to hard drive")
            cdPrintnl()
            cdPrintnl()

        #endif

    ENDIF


    /*
    IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)

        IF Is_DLC_Compatible()

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_REMAINING_PHOTOS", 1) // switch on viewfinder graphics if we're not displaying a border
        
        ENDIF

    ENDIF
    */



    IF g_FMMC_STRUCT.bDisablePhoneInstructions
    
        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            cdPrintnl()
            cdPrintstring("DEC_2014_AppCAMERA reports that g_FMMC_STRUCT.bDisablePhoneInstructions was TRUE on launch of AppCamera")
            cdPrintnl()
            cdPrintnl()

        #endif

    ELSE

        #if IS_DEBUG_BUILD
          
            cdPrintnl()
            cdPrintstring("DEC_2014_AppCAMERA reports that g_FMMC_STRUCT.bDisablePhoneInstructions is false on AppCamera launch.")
            cdPrintnl()

        #endif

    ENDIF

        



    //TODO 2513807. Switch off certain Snapmatic functionality for Lowrider photography.
    //TODO 2512104. Requested that we also display the radar.
  
    IF g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = TRUE
             
        b_LowriderDisableElements =  TRUE
    
    ENDIF





    IF b_LowriderDisableElements =  TRUE
    
        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            cdPrintnl()
            cdPrintstring("Sept_2015_AppCAMERA reports that MP Lowrider limit elements was TRUE on launch of AppCamera, b_LowriderDisableElements is TRUE!")
            cdPrintnl()
            cdPrintnl()

        #endif

    ELSE

        #if IS_DEBUG_BUILD
          
            cdPrintnl()
            cdPrintstring("Sept_2015_AppCAMERA reports that MP Lowrider limit elements was FALSE on launch of AppCamera.")
            cdPrintnl()

        #endif

    ENDIF






















    WHILE TRUE


        WAIT (0)


        /*
        #if IS_DEBUG_BUILD
    		
            IF b_NormalMode_DepthOfField_ToggledOn = TRUE
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.14, "STRING", "NDoF_ON")
            ELSE
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.14, "STRING", "NDoF_OFF")
            ENDIF
 	
        #endif
        */


		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, (INT_TO_ENUM(CONTROL_ACTION, PHONE_POSITIVE_INPUT)))  //3181559 - Prevent any future vehicle extended parachute functionality from occurring when taking a picture or saving to gallery.
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CELLPHONE_CAMERA_EXPRESSION) //3181496 - Prevent the rockect engine firing via L3 when in the Voltic 2 for Import: Export.
    
        REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092

        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)


        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA) //#2048401 - Too tricky /dangerous to have phone transition up / down screen so blocking gameplay camera switch.


        /* Comment in to display these values on the hud. 
        DISPLAY_TEXT_WITH_FLOAT (0.8, 0.1, "CELL_500", f_CellCamZoomFactor, 3)

        DISPLAY_TEXT_WITH_FLOAT (0.8, 0.1, "CELL_500", f_GameplayCamRelativePitch, 3)
        DISPLAY_TEXT_WITH_FLOAT (0.8, 0.15, "CELL_501", f_GameplayCamRelativeHeading, 3)
     
        DISPLAY_TEXT_WITH_FLOAT (0.8, 0.4, "CELL_500", f_LastSelfieRelativePitch, 3)
        DISPLAY_TEXT_WITH_FLOAT (0.8, 0.45, "CELL_501", f_LastSelfieRelativeHeading, 3)
        */

		//Force turn off grid for drone
		IF IS_LOCAL_PLAYER_USING_DRONE()  
			IF !IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
				SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
				LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) //switch off grid instantly
				PRINTLN("AppCamera player is using the camera force switch off the grid On top")
	        ENDIF
		ENDIF

        #if USE_CLF_DLC
        IF NOT g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed
        #endif

        #if USE_NRM_DLC
        IF NOT g_savedGlobalsZombie.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed
        #endif

        #if NOT USE_CLF_DLC
        #if NOT USE_NRM_DLC
        IF NOT g_savedGlobals.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed
        #endif
        #endif



            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_HAS_FOCUS_LOCK_HELP_DISPLAYED)
                IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() //Don't display the focus lock help if help is on screen...
                    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE ) //Don't display the help during a mission that uses Snapmatic either...

                        IF b_OkayToTakePicture
                        AND (b_InSelfieMode = FALSE)
						AND NOT IS_LOCAL_PLAYER_USING_DRONE()

                            SET_BIT (BitSet_CellphoneTU, g_BSTU_HAS_FOCUS_LOCK_HELP_DISPLAYED)

                            #if USE_CLF_DLC
                            g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed = TRUE
                            #endif

                            #if USE_NRM_DLC
                            g_savedGlobalsZombie.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed = TRUE
                            #endif

                            #if NOT USE_CLF_DLC
                            #if NOT USE_NRM_DLC
                            g_savedGlobals.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed = TRUE
                            #endif
                            #endif


                            PRINT_HELP("CELL_FOC_HLP")

                            #if IS_DEBUG_BUILD

                                cdPrintString("AppCAMERA - Sorted List Operation FAILED! Bypass Save routine bit has been set. Continuing but with Current / Max photo set to 0")
                                cdPrintnl()

                            #endif

                        ENDIF

                    ENDIF
                ENDIF
            ENDIF

       ENDIF


        //Temp! Do not check this segment in!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       /* 
       # if IS_DEBUG_BUILD



            DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.15, "STRING", "Current TOOLS DN Expression")

            DISPLAY_TEXT_WITH_NUMBER (0.20, 0.20, "CELL_500", g_i_CurrentlySelected_SnapMaticExpression)




            IF b_NormalMode_DepthOfField_ToggledOn
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.34, "STRING", "Normal Mode DoF is ON")
            ENDIF

            IF b_SelfieMode_DepthOfField_ToggledOn
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "Selfie Mode DoF is ON")
            ENDIF

        #endif
        */
        
        //Temp! End of segment that should not be checked in!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            






        /*
        IF b_InSaveRoutine

            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "In Save Routine")

        ENDIF
        */

        /*
        IF IS_IN_VEHICLE_MOBILE_PHONE_CAMERA_RENDERING()
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.54, "STRING", "Rendering")
        ENDIF
        */

        /*
        #if IS_DEBUG_BUILD
            IF IS_PAUSE_MENU_ACTIVE()


                PRINTSTRING ("Testing pause menu active")
                PRINTNL()

            ELSE

                PRINTSTRING ("Testing camera script still running")
                PRINTNL()
                PRINTSTRING ("Making loop obvious.....................................")
                PRINTNL()


            ENDIF
        #endif
        */

       
        IF NOT IS_PAUSE_MENU_ACTIVE()



        IF g_FMMC_STRUCT.bSecurityCamActive = FALSE //2009386
        AND g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE //2139232

            SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0375)  //Fix for 1417621

        ENDIF


        HIDE_HUD_COMPONENT_THIS_FRAME (NEW_HUD_AREA_NAME)
   
        SnapmaticHudHide()

        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
            SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE) //Requested by David H, Phil H. in bug url:bugstar:1839297
        ENDIF
        


        
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER) //Added by request of bug 1709149 

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)

        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)


        DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_AIM) //2340387 fix.




        IF b_InSaveRoutine = FALSE  //Allow vehicle exit whilst saving image #1435544
    
            SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT))) //See bug 890164

        ENDIF

        SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))) //See bug 896700. Strange one, as flashhand is already doing this.


                                                                       
        IF (IS_SCREEN_FADED_OUT()
        OR IS_SCREEN_FADING_OUT())
		AND NOT IS_LOCAL_PLAYER_USING_DRONE()

            b_ScreenFadeInterference = TRUE
            g_Cellphone.PhoneDS = PDS_AWAY
            SET_GAME_PAUSED (FALSE)

        ENDIF
                     
                     
        IF IS_STUNT_JUMP_IN_PROGRESS()

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("appCamera - aborting during stunt jump. See Bug 528388.")
                cdPrintnl()   
            #endif


                     
            g_Cellphone.PhoneDS = PDS_AWAY
            SET_GAME_PAUSED (FALSE)

        ENDIF


                                             
                                             
        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME)
                                             
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("appCamera - aborting due to disable camera app this frame.")
                cdPrintnl()   
            #endif

            g_Cellphone.PhoneDS = PDS_AWAY
            SET_GAME_PAUSED (FALSE)


        ENDIF
                                     
                                             
        IF NETWORK_IS_GAME_IN_PROGRESS() 
            IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
            
                #if IS_DEBUG_BUILD

                    PRINTSTRING ("AppCamera aborting as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE has returned TRUE. url:bugstar:1861948")
                    PRINTNL()

                #endif

                g_Cellphone.PhoneDS = PDS_AWAY
                SET_GAME_PAUSED (FALSE)

            ENDIF
        ENDIF
         
         
         
        IF NOT IS_PED_INJURED (PLAYER_PED_ID())



            IF IS_PED_IN_COVER(PLAYER_PED_ID())

                g_Cellphone.PhoneDS = PDS_AWAY  

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("No camera use when in cover. See bug 1392720")
                    cdPrintnl()   
                #endif

                SET_GAME_PAUSED (FALSE)

            ENDIF



            IF GET_PED_CONFIG_FLAG (PLAYER_PED_ID(), PCF_IsAimingGun)
            OR IS_PLAYER_CLIMBING (PLAYER_ID())
            OR IS_PLAYER_FREE_AIMING(PLAYER_ID()) //2071256

                IF IS_PLAYER_PASSENGER_IN_ANY_VEH()

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("Aiming flag set but player is passenger so not taking any abort action. See 2193549")
                        cdPrintnl()   
                    #endif

                ELSE

                    g_Cellphone.PhoneDS = PDS_AWAY  

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("No camera use when aiming or climbing. See Bug 495833, 626057")
                        cdPrintnl()   
                    #endif

                    SET_GAME_PAUSED (FALSE)

                ENDIF

            ENDIF

            IF IS_PED_RAGDOLL (PLAYER_PED_ID())
            OR IS_PED_IN_PARACHUTE_FREE_FALL (PLAYER_PED_ID())

                IF g_bInMultiplayer = TRUE  //This would be handled in SP by general ragdoll check but that does not run in MP, 
                                            //however bug 977073 requires the camera to use a specific check in MP.

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("appCamera - aborting in MP due to ragdoll or free-fall. See Bug 977073.")
                        cdPrintnl()   
                    #endif


                    g_Cellphone.PhoneDS = PDS_AWAY

                ENDIF

            ENDIF
                                                
                                                     
            IF IS_ENTITY_IN_WATER (PLAYER_PED_ID())
                IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) > 0.3 


                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("appCamera - aborting camera use in deep water due to code request. See bug #1238505")
                        cdPrintnl()   
                    #endif


                    g_Cellphone.PhoneDS = PDS_AWAY

                
                ENDIF
            ENDIF



            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())


                Disable_Vehicle_Controls()

                //If use in vehicle is disallowed, comment this back in.
                //HANG_UP_AND_PUT_AWAY_PHONE()

                tempVehicleIndex = GET_VEHICLE_PED_IS_IN (PLAYER_PED_ID())


                IF GET_ENTITY_UPRIGHT_VALUE (tempVehicleIndex) < 0.0

                                     
                    HANG_UP_AND_PUT_AWAY_PHONE()

                    
                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("appCAMERA. Vehicle upright value less than zero! Hanging up and putting away phone. See Bug 520204.")
                        cdPrintnl()   
                    #endif


                ENDIF


                IF b_VehicleRenderCheck = TRUE //Only check this when the cellphone cam is actually activated.

                    IF b_VehicleRenderAbort = TRUE

                    
                        IF NOT IS_IN_VEHICLE_MOBILE_PHONE_CAMERA_RENDERING()  //Still not rendering after waiting a frame...
                    
                                                        
                            HANG_UP_AND_PUT_AWAY_PHONE()

                    
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("appCAMERA. In vehicle but IS_IN_VEHICLE_MOBILE_PHONE_CAMERA_RENDERING returning false after a frame. Bug 1390397 ")
                                cdPrintnl()   
                            #endif

                        ENDIF


                        
                    ELSE
        
                        IF NOT IS_IN_VEHICLE_MOBILE_PHONE_CAMERA_RENDERING()
                    
                            b_VehicleRenderAbort = TRUE

                            WAIT (0) //Need to wait a frame as per Colin's instructions.

                            REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092

                        ENDIF

                    ENDIF                   

                ENDIF

            
            ELSE

                
      
                //Fix for 822107 - the mobile phone task is not run in multiplayer, so a check is required to make sure it doesn't abort immediately.
                
                //Re-adding this in now with a few extra phone ownership checks so this should only kick in during SP play.

                IF g_bInMultiplayer = FALSE

                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO)

                    Get_Cellphone_Owner()

                    IF g_Cellphone.PhoneOwner = CHAR_MICHAEL
                    OR g_Cellphone.PhoneOwner = CHAR_FRANKLIN
                    OR g_Cellphone.PhoneOwner = CHAR_TREVOR                    

                        IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK (PLAYER_PED_ID())
                            IF ((GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING))
                        
                                    //Don't abort camera whilst parachuting...

                                ELSE

                                g_Cellphone.PhoneDS = PDS_AWAY

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("appCamera - Player ped has aborted mobile phone task, quitting out - bug Steve T if this is a problem in MP.")
                                    cdPrintnl()   
                                #endif

                                SET_GAME_PAUSED (FALSE) 

                            ENDIF

                        ENDIF

                    ENDIF

                    ENDIF
                    
                ENDIF
                




            ENDIF
        ENDIF
        


        IF ShouldDrawShutter
                                
            Draw_Shutter_Movie_With_Status_Check()

        ENDIF
              


        IF g_Cellphone.PhoneDS < PDS_TAKEOUT  //Make sure we clear any help if the player's phone is moving off screen.
        
            //CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)
        
        ENDIF
    


        
            IF b_AlertScreenPresent = FALSE
            //AND b_DisplayingSignin = FALSE


                    IF b_CheckingStorage = FALSE
                    
                        IF b_RescanWhileCameraInUse = FALSE


                        IF i_SignInTimeOutRequiredFlag = 0
                        

                            IF g_Cellphone.PhoneDS > PDS_AWAY 
                                IF b_InSaveRoutine


                                    MaintainSaveRoutine()


                                    //Save Routine Completed!
                                    IF  g_b_appHasRequestedSave = FALSE
                                    AND g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY



                                        
                                  
                                        CLEAR_FLOATING_HELP (FLOATING_HELP_TEXT_ID_1)


                                               
                                        BUSYSPINNER_OFF()

                                        b_InSaveRoutine = FALSE



                                        Do_Fullscreen_Reposition()
                        
                                        


                                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                                            //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                                            v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                                        ENDIF


                                        Play_Back_Beep()


                                        
                                        //Save routine completed. Increment number of saved photos.
                                        i_CurrentNumberOfStoredPhotos ++

                                        
                                        IF Is_DLC_Compatible()


                                            //For 2109889, the b_ShutterTransition routine has been included post-save, so that should restore the frame and grid
                                            //at a later and tidier time if the g_BSTU_REMOVE_SNAPMATIC_GRID bit has not been set.
                                            /*
                                            IF g_i_CurrentlySelected_SnapMaticBorder = 0
                                                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                                                ENDIF
                                            ENDIF


                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("Setting Photo Frame On if hide bit not set - 9")
                                                cdPrintnl()
                                            #endif
                                            */

                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))

                                            
                                        ELSE

                                            //For 2109889, the b_ShutterTransition routine has been included post-save, so that should restore the frame and grid
                                            //at a later and tidier time if the g_BSTU_REMOVE_SNAPMATIC_GRID bit has not been set.
                                            /*

                                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                                            ENDIF

                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("Setting Photo Frame On if hide bit not set - 10")
                                                cdPrintnl()
                                            #endif
                                            */
                                        
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(i_CurrentNumberOfStoredPhotos), TO_FLOAT(i_MaximumNumberOfStoredPhotos))


                                        ENDIF
                                        

                                        /*  Removed for 2113790 - this is redundant. Setup_Cam_Instructional_Buttons() overwrites this.
                                        IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)  //Only display help text for standard use...


                                            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
                                        
                                                TokenDecision = "CELL_298"

                                                SetUp_Continue_FirstSave_With_Send_Buttons()
                                                //SetUp_Continue_SaveAgain_With_Send_Buttons()

                                            ELSE

                                                TokenDecision = "CELL_297"

                                                #if IS_DEBUG_BUILD

                                                    cdPrintnl()
                                                    cdPrintstring("DEBUG_SEGMENT 3")
                                                    cdPrintnl()

                                                #endif


                                                SetUp_Continue_FirstSave_No_Send_Buttons()
                                                //SetUp_Continue_SaveAgain_No_Send_Buttons()

                                            ENDIF


                                            Update_Help_State = 2


                                        ENDIF
                                        */
                                        
                                        
                                        //Added for 2113790
                                        b_FocusLockOngoing = FALSE
                                        b_LeftTriggerInstructionsOngoing = FALSE
                                        b_RightTriggerInstructionsOngoing = FALSE

                                        //Moved just prior to the instructional buttons getting re-established for 2113790
                                        b_WaitingForDiscardOrSend = FALSE
 
                                        Setup_Cam_Instructional_Buttons()

                                        
                                        #if IS_DEBUG_BUILD

                                            cdPrintString("AppCAMERA - Save routine completed - normal exit. ")
                                            
                                            cdPrintnl()

                                        #endif

                                    ENDIF




                                    IF g_Cell_Pic_Stage = PIC_STAGE_IDLE

                                        b_InSaveRoutine = FALSE


                                        BUSYSPINNER_OFF()


                                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                                            //Fix for 1712228. Refresh start position as player may have been walking about during discard or send wait.
                                            v_PlayerSelfie_StartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

                                        ENDIF


                                        Reposition_Phone_to_Fullscreen()


                                        //Added for 2113790
                                        b_FocusLockOngoing = FALSE
                                        b_LeftTriggerInstructionsOngoing = FALSE
                                        b_RightTriggerInstructionsOngoing = FALSE

                                        //Moved just prior to the instructional buttons getting re-established for 2113790
                                        b_WaitingForDiscardOrSend = FALSE

                                        Setup_Cam_Instructional_Buttons() 


                                        #if IS_DEBUG_BUILD

                                            cdPrintString("AppCAMERA - Save routine IDLE exit. ")
                                            
                                            cdPrintnl()

                                        #endif
                                        



                                        IF Is_DLC_Compatible()

                                            IF g_i_CurrentlySelected_SnapMaticBorder = 0
                                                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                                                ENDIF
                                            ENDIF

                                        ELSE

                                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                                            ENDIF

                                        ENDIF

                                        #if IS_DEBUG_BUILD
                                            cdPrintstring("Setting Photo Frame On if hide bit not set - 11")
                                            cdPrintnl()
                                        #endif



                                        SetUpCurrentTCModifier() //Moved here due to #1689345
                                        
                                        Local_PPE_Toggle(TRUE) //re-establish border when moving back to small phone.




                                    ENDIF





                                    /*
                                    //User has pressed back, re-establish pre-gallery state and instructional buttons
                                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) 


                                        //This needs to go in but how?
                                        SET_GAME_PAUSED (FALSE)
                                        
                                  

                                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "DISPLAY_VIEW", TO_FLOAT(ShutterTypeDecision)) //Shutter screen.

                                        LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")


                                        b_WaitingForDiscardOrSend = FALSE


                                        Setup_Cam_Instructional_Buttons()  
                                       
                                        //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
                                        g_b_appHasRequestedTidyUp = TRUE

                                        b_InSaveRoutine = FALSE

                                    ENDIF
                                    */

                                ELSE


                                    IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL   


                                        IF g_b_Rotate3dPhoneLandscape = TRUE
                                            
                                            IF g_b_Rotate3dPhonePortrait = FALSE //make sure phone can't get stuck trying to rotate forwards and back.

                                                RotatePhoneToWidescreen ()

                                            ENDIF

                                        ELSE
                                        
                                            IF (g_App_Elapsed_SafetyBuffer - g_App_SafetyBuffer_StartTime) > g_App_Required_SafetyBuffer_Duration //make sure app has had a chance to launch correctly before exiting. See bug 25815.
                                            	
												
												IF IS_SCREEN_FADED_OUT()
												AND SHOULD_START_EXIT_DRONE_SNAPMATIC()
													IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
			                                        AND b_WaitingForDiscardOrSend

			                                            #IF IS_DEBUG_BUILD
			                                                PRINTSTRING ("[AM_MP_DRONE] - Go Back phone input detected but not acted upon due to Docks Setup inhibition.")
			                                                PRINTNL()
			                                            #endif


			                                        ELSE

			                                            IF b_WaitingForDiscardOrSend = FALSE

			                                                SET_GAME_PAUSED (FALSE)

			                                                STOP_SOUND (Zoom_SoundID)

			                                                b_DrawCamHelpText = FALSE

			                                                IF g_b_Rotate3dPhonePortrait = FALSE

			                                                    Play_Back_Beep()

			                                                             
			                                                      
			                                                    //Need to rotate back to normal... 
			                                                              
			                                                    g_b_Rotate3dPhonePortrait = TRUE
			                                                    CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)
			                                        
			                                                    b_OkayToTakePicture = FALSE

			                                                    b_Rotate3dPhoneX = TRUE
			                                                    b_Rotate3dPhoneY = TRUE
			                                                    b_Rotate3dPhoneZ = TRUE



			                                                    b_ManipulatePhonePosX = TRUE
			                                                    b_ManipulatePhonePosY = TRUE
			                                                    b_ManipulatePhonePosZ = TRUE



			                                         

			                                                    Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
			                                                    Local_PPE_Toggle (FALSE)

			                                                    b_VehicleRenderCheck  = FALSE

			                                                    Clear_Modifier_With_Exception_Checks() //#1431423

			                                                    //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

			                                                    
			                                                    #if IS_DEBUG_BUILD

			                                                        PRINTNL()
			                                                        PRINTSTRING ("[AM_MP_DRONE] - Cell Cam FALSE 8")
			                                                        PRINTNL()
			                                                        cdPrintnl()
			                                                        cdPrintstring("Cam DE-activated 42")
			                                                        cdPrintnl()


			                                                    #endif


			                                                    ShouldDrawShutter = FALSE
			                                                    PRINTLN("[app_camera] ShouldDrawShutter = 3 FALSE")  


			                                                    HIDE_ACTIVE_PHONE (FALSE, TRUE)

			                                                                                                            
			                                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 16)

			                                              		SET_EXIT_DRONE_SNAPMATIC(TRUE)
															ENDIF
	                                                	ENDIF

	                                                ENDIF
												ENDIF

                                                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) 

                                                    IF IS_LOCAL_PLAYER_USING_DRONE()
														IF g_bCelebrationScreenIsActive
														OR g_bMissionEnding
														//OR ShouldDrawShutter
														OR Cellphone_Pic_Just_Taken
														OR IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
															// skip
														ELSE
															IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
		                                                    AND b_WaitingForDiscardOrSend

		                                                        #IF IS_DEBUG_BUILD
		                                                            PRINTSTRING ("[AM_MP_DRONE] - Go Back phone input detected but not acted upon due to Docks Setup inhibition 1")
		                                                            PRINTNL()
		                                                        #endif

															ELSE
																IF !SHOULD_START_EXIT_DRONE_SNAPMATIC()
																	IF b_WaitingForDiscardOrSend = FALSE
																		DO_SCREEN_FADE_OUT(500)
																		b_DrawCamHelpText = FALSE
				                                                        HIDE_ACTIVE_PHONE (TRUE, TRUE) 
																		SET_START_EXIT_DRONE_SNAPMATIC(TRUE)
																		PRINTLN("[app_camera] ShouldDrawShutter = 4 FALSE")  

																	ENDIF	
																ENDIF
															ENDIF	
														ENDIF	
													ELSE	
	                                                    IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
	                                                    AND b_WaitingForDiscardOrSend

	                                                        #IF IS_DEBUG_BUILD
	                                                            PRINTSTRING ("ST - Go Back phone input detected but not acted upon due to Docks Setup inhibition.")
	                                                            PRINTNL()
	                                                        #endif


	                                                    ELSE

	                                                        IF b_WaitingForDiscardOrSend = FALSE

	                                                            SET_GAME_PAUSED (FALSE)

	                                                            STOP_SOUND (Zoom_SoundID)

	                                                            b_DrawCamHelpText = FALSE

	                                                            IF g_b_Rotate3dPhonePortrait = FALSE

	                                                                Play_Back_Beep()

	                                                                         
	                                                                  
	                                                                //Need to rotate back to normal... 
	                                                                          
	                                                                g_b_Rotate3dPhonePortrait = TRUE
	                                                                CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)
	                                                    
	                                                                b_OkayToTakePicture = FALSE

	                                                                b_Rotate3dPhoneX = TRUE
	                                                                b_Rotate3dPhoneY = TRUE
	                                                                b_Rotate3dPhoneZ = TRUE



	                                                                b_ManipulatePhonePosX = TRUE
	                                                                b_ManipulatePhonePosY = TRUE
	                                                                b_ManipulatePhonePosZ = TRUE



	                                                     

	                                                                Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
	                                                                Local_PPE_Toggle (FALSE)

	                                                                b_VehicleRenderCheck  = FALSE

	                                                                Clear_Modifier_With_Exception_Checks() //#1431423

	                                                                //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])

	                                                                
	                                                                #if IS_DEBUG_BUILD

	                                                                    PRINTNL()
	                                                                    PRINTSTRING ("Cell Cam FALSE 8")
	                                                                    PRINTNL()
	                                                                    cdPrintnl()
	                                                                    cdPrintstring("Cam DE-activated 42")
	                                                                    cdPrintnl()


	                                                                #endif


	                                                                ShouldDrawShutter = FALSE
	                                                                PRINTLN("[app_camera] ShouldDrawShutter = 5 FALSE")  



	                                                                HIDE_ACTIVE_PHONE (FALSE, TRUE)

	                                                                                                                        
	                                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 16)

	                                                          

	                                                            ENDIF

	                                                        ENDIF
	                                                
	                                                    ENDIF
													ENDIF
                                                ENDIF

                                            ENDIF

                                        
                                        ENDIF

                                    

                                        IF b_External_Help_Presence = FALSE

                                            IF IS_HELP_MESSAGE_BEING_DISPLAYED()
                                                             
                                                Adjust_Help_State()
                                            
                                                b_External_Help_Presence = TRUE

                                            ENDIF
                                            
                                        ELSE

                                            IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
                                        
                                                
                                                Adjust_Help_State()
                                            
                                                b_External_Help_Presence = FALSE

                                            ENDIF
                                    
                                        ENDIF


                                        IF b_External_SendState_Change = FALSE
                                            
                                            IF IS_BIT_SET (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)
                                                
                                                Adjust_Help_State()
                                            
                                                b_External_SendState_Change = TRUE

                                            ENDIF
                                            
                                        ELSE

                                            IF NOT IS_BIT_SET (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

                                                
                                                Adjust_Help_State()
                                            
                                                b_External_SendState_Change= FALSE

                                            ENDIF
                                    
                                        ENDIF




                                  


                                        IF g_b_Rotate3dPhonePortrait = TRUE

                                            RotatePhoneToPortrait_and_Exit()

                                        ELSE

                                            IF g_Cellphone.PhoneDS > PDS_TAKEOUT

                                                IF b_Reposition_Corner = TRUE
                                                AND b_Reposition_Fullscreen = FALSE

                                                    Reposition_Phone_to_Corner() 
                                                    
                                                   

                                                ENDIF

                                                IF b_Reposition_Corner = FALSE
                                                AND b_Reposition_Fullscreen = TRUE

                                                    #if IS_DEBUG_BUILD

                                                        cdPrintstring ("Checking post-delete reposition routine.")
                                                        cdPrintnl()
                                                
                                                    #endif                                                  


                                                    Reposition_Phone_to_Fullscreen() //This needs to be done here as the phone is a higher draw priority than the shutter.  
                                                    
                                                    //#1431423 - Added by Frank K, maybe a little weird when not in selfie mode.  

                                                    IF b_ShutterTransition = TRUE
                                                    OR b_ShutterTransition = FALSE //Let's display this regardless of the bool now as trial for 2109889

                                                     

                                                        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                                                            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                                                                LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "CLOSE_SHUTTER")

                                                                //CELL_CAM_ACTIVATE (TRUE, TRUE) //Trial addition for investigating 1954420 failed. Looks bollocks as well.

                                           
                                                                i_ShutterTransitionTime = GET_GAME_TIMER()
                                                                WHILE GET_GAME_TIMER() < (i_ShutterTransitionTime + i_ShutterDelayForAnimation)
                                                                AND g_Cellphone.PhoneDS > PDS_AWAY
                                                                    SnapmaticHudHide()
                                                                    Draw_Shutter_Movie_With_Status_Check()
                                                                    Disable_Camera_Movement_Controls()
                                                                    REPLAY_PREVENT_RECORDING_THIS_FRAME() //2062092
                                                            WAIT (0)                                                        
                                                        ENDWHILE
                                                 
                                                            ENDIF
                                                        ENDIF

                                        
                                                        //LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")
                                                        
                                                        
                                                        IF Is_DLC_Compatible()

                                                            IF g_i_CurrentlySelected_SnapMaticBorder = 0
                                                                IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                                                                ENDIF
                                                            ENDIF

                                                        ELSE

                                                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics.
                                                            ENDIF

                                                        ENDIF

                                                        #if IS_DEBUG_BUILD
                                                            cdPrintstring("Setting Photo Frame On if hide bit not set - 12")
                                                            cdPrintnl()
                                                        #endif  

                                                        
                                                        Draw_Shutter_Movie_With_Status_Check()
                                                        
                                                        SnapmaticHudHide()
                                                        
                                                        
                                                        
                                                        
                                                        //Added for 2113790
                                                        b_FocusLockOngoing = FALSE
                                                        b_LeftTriggerInstructionsOngoing = FALSE
                                                        b_RightTriggerInstructionsOngoing = FALSE

                                                        //Moved just prior to the instructional buttons getting re-established for 2113790
                                                        b_WaitingForDiscardOrSend = FALSE

                                                        Setup_Cam_Instructional_Buttons()  
                                                        
                                                        
                                                                                                         
                                                        b_ShutterTransition = FALSE

                                                        //IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                                                            //IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                                                                LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")
                                                            
                                                            //ENDIF
                                                        //ENDIF
                                                            

                                                    ENDIF
                                                    


                                                    SetUpCurrentTCModifier() //Moved here due to #1689345
                                                    
                                                    Local_PPE_Toggle(TRUE) //re-establish border when moving back to small phone.

                                                    //LEGACY_SCALEFORM_MOVIE_METHOD(SF_Movie_Gallery_Shutter_Index, "OPEN_SHUTTER")  
                                                    
                                                    Set_Up_Pre_Picture_Taken_Selfie_PitchandHeading()
                                                                                                     

                                                ENDIF

                                            ENDIF

                                        ENDIF


                                        //For MP... In theory, they could have the phone onscreen when firing and launch this application if we allow camera use in MP.
                                        /*
                                        IF IS_AIM_CAM_ACTIVE ()

                                           RotatePhoneToPortrait_and_Exit()

                                        ENDIF
                                        */



                                        IF b_DrawCamHelpText = TRUE

                                            //IF b_InSaveRoutine = FALSE //Don't draw any instructional buttons if we are currently saving a photo.

                                                Draw_Camera_Instructional_Buttons() //only draw the help text if the phone is not actively rotating.
                                            
                                            //ENDIF

                                        ELSE

                                            #if IS_DEBUG_BUILD
                                                cdPrintstring("AppCAMERA - Not drawing instructional buttons.")
                                                cdPrintnl()
                                            #endif


                                            //For bug 1130708
                                            //If an alert screen isn't present and we aren't checking storage or in the save routine we must be checking this draw condition.
                                            //The save routine should be displaying the hud component!


                                        ENDIF


                                    
                                                
                                        /* Was used to draw sprite photo - commented out for just now.
                                        IF g_Cellphone_FullScreen_Mode = FALSE

                                            Draw_Camera_Zoom()

                                        ENDIF
                                        */


                                    

                                        IF b_BeginInitialisationPause

                                            IF TIMERB() > 500

                                                b_OkayToTakePicture = TRUE
                                                b_BeginInitialisationPause = FALSE 

                                                //Used by Adam W's help text to let him know when the camera is ready to take a photo
                                                SET_BIT (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO)

                                                IF g_b_Rotate3dPhonePortrait = FALSE

                                                    HIDE_ACTIVE_PHONE (TRUE, TRUE) //#1431423  

                                                ENDIF

                                                #if IS_DEBUG_BUILD

                                                    cdPrintnl()
                                                    cdPrintstring ("APPCAMERA - Hide active phone 187 called from b_BeginInitialisationPause")
                                                    cdPrintnl()

                                                #endif


                                                                                                                                                                                     
                                            ENDIF

                                        ENDIF



                                        IF b_WaitingForDiscardOrSend = FALSE

                                            IF b_OkayToTakePicture
                                            AND g_b_Rotate3dPhonePortrait = FALSE
                                                                           
                                                IF b_ScreenFadeInterference = FALSE
                                                AND b_Reposition_Fullscreen = FALSE
                                                AND Cellphone_Pic_Just_Taken = FALSE
                                                //Thursday check JUST_TAKEN

                                       

                                                    Check_For_Shutter_Press()
                                                
                                                ENDIF

                                            ELSE

                                               Disable_Camera_Movement_Controls()

                                            ENDIF

                                        ELSE

                                            //IF Cellphone_Pic_Just_Taken = FALSE
                                            IF b_Reposition_Corner = FALSE
                                            
                                                WaitingForDiscardOrSend() 
                                                //SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_EXTRA_SPECIAL_OPTION_INPUT))) //See bug 890164
                          
                                            
                                            ENDIF

                                        ENDIF
                                

                                 
                                    ENDIF
                            
                         
                                ENDIF

                            ENDIF 

                        ENDIF  //Rescan Storage

                        ENDIF //i_SignInTimeOutRequiredFlag


                    ELSE //b_CheckingStorage must be true...


                        IF g_Cellphone.PhoneDS > PDS_AWAY
                            IF g_b_Rotate3dPhoneLandscape = TRUE
                                                
                                IF g_b_Rotate3dPhonePortrait = FALSE //make sure phone can't get stuck trying to rotate forwards and back.

                                    RotatePhoneToWidescreen ()

                                ENDIF

                            ENDIF
                        ENDIF

                        CheckandScanStorage()


                    ENDIF


            ELSE //An Alert screen must be present, as b_AlertScreenPresent is returning true...


                IF e_CurrentAlertScreen = ALERT_AT_MAX_CAPACITY

                    SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_FW_1", iButtonBits, "CELL_CAM_FW_2", FALSE, -1, "", "", TRUE)

                ENDIF


                IF e_CurrentAlertScreen = ALERT_SCLUB_UNAVAILABLE

                    //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_SCW_1", iButtonBits, "", FALSE, -1, "", "", TRUE) 

                    SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "ERROR_NO_SC_CAMERAPHONE", iButtonBits, "", FALSE, -1, "", "", TRUE) 


                ENDIF



                IF e_CurrentAlertScreen = ALERT_SCLUB_BANNED

                    SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "SC_ERROR_BANNED", iButtonBits, "", FALSE, -1, "", "", TRUE)

                ENDIF



                IF e_CurrentAlertScreen = ALERT_SCLUB_OUTDATEDPOLICY

             
                    SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "ERROR_UPDATE_SC_CAMERAPHONE", iButtonBits, "", FALSE, -1, "", "", TRUE) 


                ENDIF



                IF e_CurrentAlertScreen = ALERT_CLOUD_SIGNINUTILITY


                    IF IS_XBOX360_VERSION()
                    OR IS_XBOX_PLATFORM()

                        SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_TEMP_3X", iButtonBits, "", FALSE, -1, "", "", TRUE)
                    
                    ENDIF


                    IF IS_PS3_VERSION()
                    OR IS_PLAYSTATION_PLATFORM()

                        SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_TEMP_3P", iButtonBits, "", FALSE, -1, "", "", TRUE)
                    
                    ENDIF


                    IF IS_PC_VERSION() //We may not need this at all!

                        SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_TEMP_30", iButtonBits, "", FALSE, -1, "", "", TRUE)
                    
                    ENDIF


                    /*

                    [CELL_CAM_TEMP_3P]
                    To save Snapmatic phone app photos to the Gallery you must be signed in to PlayStation®Network

                    [CELL_CAM_TEMP_3X]
                    To save Snapmatic phone app photos to the Gallery you must be signed in to Xbox LIVE

                    [CELL_CAM_TEMP_30]
                    To save Snapmatic phone app photos to the Gallery you must be signed in.

                    */



                ENDIF


                IF e_CurrentAlertScreen = ALERT_CLOUD_UNAVAILABLE

                    SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "CELL_CAM_CCW_1", iButtonBits, "CELL_CAM_CCW_2", FALSE, -1, "", "", TRUE)

                ENDIF



                IF e_CurrentAlertScreen = ALERT_CLOUD_UNDERAGE

                                  
                    //Legacy. Now replaced with individual warnings. See bug 1740026 - It was requested that a more analog approach is taken to Age Restriction. 
                    //SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGERES", iButtonBits, "", FALSE, -1, "", "", TRUE)

                    
                    //To get in this clause we know that IS_ACCOUNT_OVER_17() returned FALSE, which meant the age group check
                    //did not return RL_AGEGROUP_ADULT. Checking the exact reason why here...
                    
                    //Enum for reference:
                    /*
                        ENUM RL_AGEGROUP
                        RL_AGEGROUP_INVALID = -1,
                        RL_AGEGROUP_PENDING,        //Age group is being retrieved.
                        RL_AGEGROUP_CHILD,
                        RL_AGEGROUP_TEEN,
                        RL_AGEGROUP_ADULT
                        ENDENUM
                    */


                    #if IS_DEBUG_BUILD
                        BOOL HasWeirdWarningDisplayed = FALSE
                    #endif

                    RL_AGEGROUP RetrievedAgeGroup

                    RetrievedAgeGroup  = NETWORK_GET_AGE_GROUP()



                    //Got group, now decide which Alert screen message to display based on result. Next gen tags of these are also available. Sony platform is unique.
                    /*
                    [HUD_AGE_I!PS3]
                    Snapmatic features are unavailable. An error occurred whilst validating your Sony Entertainment Network account's eligibility. 

                    [HUD_AGE_I!ELSE]
                    Snapmatic features are unavailable. An error occurred whilst validating your account's eligibility. 


                    [HUD_AGE_P!PS3]
                    Snapmatic features are temporarily unavailable. Please try again later.

                    [HUD_AGE_P!ELSE]
                    Snapmatic features are temporarily unavailable. Please try again later.


                    [HUD_AGE_C!PS3]
                    Snapmatic features are disabled on your Sony Entertainment Network account due to eligibility restrictions.

                    [HUD_AGE_C!ELSE]
                    Snapmatic features are disabled on this account due to eligibility restrictions.


                    [HUD_AGE_T!PS3]
                    Snapmatic features are currently disabled on your Sony Entertainment Network account due to eligibility restrictions.

                    [HUD_AGE_T!ELSE]
                    Snapmatic features are currently disabled on this account due to eligibility restrictions.
                    */


                    SWITCH RetrievedAgeGroup



                        CASE RL_AGEGROUP_INVALID  //Tested.

                            SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGE_I", iButtonBits, "", FALSE, -1, "", "", TRUE)

                        BREAK
                               
                
                        CASE RL_AGEGROUP_PENDING  //Tested.

                            SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGE_P", iButtonBits, "", FALSE, -1, "", "", TRUE)

                        BREAK


                        CASE RL_AGEGROUP_CHILD  //Tested.

                            SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGE_C", iButtonBits, "", FALSE, -1, "", "", TRUE)

                        BREAK
                               
                
                        CASE RL_AGEGROUP_TEEN   //Tested.

                            SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGE_T", iButtonBits, "", FALSE, -1, "", "", TRUE)

                        BREAK




                        DEFAULT //May have returned adult which is weird. If so, prompt to try again later using "pending" variant alert screen.

                            #if IS_DEBUG_BUILD
                                IF HasWeirdWarningDisplayed = FALSE

                                    cdPrintnl()
                                    cdPrintstring("AppCAMERA - Weird result! RetrievedAgeGroup now returning ADULT although IS_ACCOUNT_OVER_17 initially returned FALSE.")
                                    cdPrintnl()

                                    HasWeirdWarningDisplayed = TRUE
                                ENDIF
                            #endif

                            SET_WARNING_MESSAGE_WITH_HEADER ("CELL_CAM_ALERT", "HUD_AGE_P", iButtonBits, "", FALSE, -1, "", "", TRUE)  //Tested.

                        BREAK



                    ENDSWITCH




                ENDIF





                //Pause game here in SP only?



                IF e_CurrentAlertScreen = ALERT_NONE
     
                    #if IS_DEBUG_BUILD

                        SCRIPT_ASSERT ("An Alert screen must be present from Cellphone Camera, but type is set to none!")
                        
                        cdPrintString("AppCAMERA - An Alert screen must be present from Cellphone Camera, but type is set to none!")
                        cdPrintnl()

                    #endif

                ENDIF
                






                IF e_CurrentAlertScreen = ALERT_SCLUB_UNAVAILABLE   //Need to handle "Sign In / Up" and "Cancel".
                OR e_CurrentAlertScreen = ALERT_SCLUB_OUTDATEDPOLICY
                
                    //IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
                    IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_CANCEL)) //Warning message will disable private Phone Control #1326123.
        
                        b_AlertScreenPresent = FALSE

                        e_CurrentAlertScreen = ALERT_NONE

                        HIDE_ACTIVE_PHONE (FALSE, TRUE)


                    ENDIF

                    IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_ACCEPT))
                        stateOfSocialClubSignUpButton = 1
                    ENDIF

                    IF stateOfSocialClubSignUpButton = 1
                  
                        //IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
                        IF IS_CONTROL_JUST_RELEASED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_ACCEPT))

                            b_AlertScreenPresent = FALSE

                            e_CurrentAlertScreen = ALERT_NONE

                            HIDE_ACTIVE_PHONE (FALSE, TRUE)

                            stateOfSocialClubSignUpButton = 0
             
                            SET_SOCIAL_CLUB_TOUR("Gallery")
                            OPEN_SOCIAL_CLUB_MENU()

                        ENDIF
                    ENDIF


                ELSE  //For other warning screens, just handle "OK" or allow signin utility launch.

                    IF e_CurrentAlertScreen = ALERT_CLOUD_SIGNINUTILITY


                        IF IS_PLAYSTATION_PLATFORM() //No signin utility for PS4, so we're just checking for an "okay" prompt.
                        
                            IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_ACCEPT)) //Warning message will disable private Phone Control #1326123.
            
                                b_AlertScreenPresent = FALSE

                                e_CurrentAlertScreen = ALERT_NONE

                                HIDE_ACTIVE_PHONE (FALSE, TRUE)

                                i_SignInTimeOutRequiredFlag = 0

                            ENDIF

                        ELSE

                        IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_CANCEL)) //Warning message will disable private Phone Control #1326123.
        
                            b_AlertScreenPresent = FALSE

                            e_CurrentAlertScreen = ALERT_NONE

                            HIDE_ACTIVE_PHONE (FALSE, TRUE)

                            i_SignInTimeOutRequiredFlag = 0

                        ENDIF

                        ENDIF




                        IF NOT (IS_PLAYSTATION_PLATFORM()) //PS4 currently doesn't support an in-game signin utility, so remove this check for that platform. url:bugstar:1784537

                        IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Warning message will disable private Phone Control #1326123.
        
                            b_AlertScreenPresent = FALSE

                            e_CurrentAlertScreen = ALERT_NONE

                            HIDE_ACTIVE_PHONE (FALSE, TRUE)
                    
                            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE )
                                //OR (NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = FALSE) //Removed for 1932325 - hard drive save now used.
                            //OR g_DumpDisableEveryFrameCaller //To test exception to save routine
                            //OR (NETWORK_HAVE_USER_CONTENT_PRIVILEGES() = FALSE) //Stub for 1708374


                                i_SignInTimeOutRequiredFlag = 0

                            ELSE

                                i_SignInTimeOutRequiredFlag = 2

                            ENDIF

                            i_SignInStartTime = GET_GAME_TIMER()


                            #if IS_DEBUG_BUILD
                                PRINTSTRING("SignoutStartTime is ")
                                PRINTINT (i_SignInTimeOutRequiredFlag)
                                PRINTNL()
                            #endif



                            DISPLAY_SYSTEM_SIGNIN_UI (TRUE)

                         ENDIF

                         ENDIF


                    ELSE


                        //IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
                        IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, INPUT_FRONTEND_ACCEPT))

                            b_AlertScreenPresent = FALSE

                            e_CurrentAlertScreen = ALERT_NONE

                            HIDE_ACTIVE_PHONE (FALSE, TRUE)


                        ENDIF

                    ENDIF

                ENDIF




            ENDIF




        ENDIF //NOT PAUSE_MENU_ACTIVE check




        /*
        PRINTSTRING(" SignoutFlag is ")
        PRINTINT (i_SignInTimeOutRequiredFlag)
        PRINTNL()
        */

        i_SignInCurrentTime = GET_GAME_TIMER()


        //PRINTINT(i_SignInCurrentTime - i_SignInStartTime)

        //Hard drive save change!
        /*This is the "Emergency Scan Routine" for a change in sign in status. May need to alter the IF NOT (NETWORK_IS_SIGNED_ONLINE()) sections to use
        //the IF NOT (NETWORK_IS_SIGNED_IN()) profile check if it transpires we need to.
        //These checks were required when Snapmatic uploaded to the cloud directly from this script. Now saves to the hard disk. 1915523
        //We might need to have an offline profile check so the routine may need reinstated and modified where noted.

        IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE )

            IF NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV() = TRUE //Remove if this needs to be a profile check.


                IF i_SignInTimeOutRequiredFlag = 2

                    BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("CELL_SPINNER2")
                    END_TEXT_COMMAND_BUSYSPINNER_ON(4)

                    i_SignInTimeOutRequiredFlag = 1

                ENDIF


                IF b_CheckingStorage = FALSE

                    
                    IF i_MaximumNumberOfStoredPhotos = 0
                    AND NETWORK_IS_SIGNED_ONLINE()  //Change to NETWORK_IS_SIGNED_IN if this needs to be a profile check.
                    AND SCRIPT_IS_CLOUD_AVAILABLE() //Remove if this needs to be a profile check.

                        #if IS_DEBUG_BUILD
                            
                            PRINTSTRING ("3 - AppCAMERA - App camera performing emergency scan, probably after signin from alert screen.")
                            PRINTNL()

                        #endif


                        b_NetworkSignedIn = TRUE

                        IF b_RescanWhileCameraInUse = FALSE

                            i_SignInTimeOutRequiredFlag = 1

                            b_RescanWhileCameraInUse = TRUE
                            InitialiseCheckStorage()

                          
                        ENDIF

                    ELSE
                    

                        //Have a timer on this that means this can only reset after a certain amount of time has passed?
                        IF i_SignInTimeOutRequiredFlag > 0
                            IF i_SignInCurrentTime - i_SignInStartTime > 10000
                            AND b_RescanWhileCamerainUse = FALSE

                                i_SignInTimeOutRequiredFlag = 0 


                                #if IS_DEBUG_BUILD
                            
                                    PRINTSTRING ("7 - AppCAMERA -i_SignInTimeOutRequiredFlag rescinded. ")
                                    PRINTNL()

                                #endif

                                BUSYSPINNER_OFF()

                            ENDIF
                        ENDIF

                    ENDIF


                    
                    //Required for the player signing in and out via the XMB proactively.
                    IF b_NetworkSignedIn = FALSE


                                  
                        IF NETWORK_IS_SIGNED_ONLINE()   //Change to NETWORK_IS_SIGNED_IN if this needs to be a profile check.
                        AND SCRIPT_IS_CLOUD_AVAILABLE() //Remove if this needs to be a profile check.

                            #if IS_DEBUG_BUILD

                                PRINTSTRING ("AppCAMERA - Sign in status now TRUE - rescanning and updating storage.")
                                PRINTNL ()

                            #endif
                

                            b_NetworkSignedIn = TRUE

                            IF b_RescanWhileCameraInUse = FALSE

                                i_SignInTimeOutRequiredFlag = 1

                                b_RescanWhileCameraInUse = TRUE
                                InitialiseCheckStorage()

                                                  
                            
                            ENDIF

                        ENDIF
                        

                    ELSE
         
                        IF NOT (NETWORK_IS_SIGNED_ONLINE()) //Change to NOT NETWORK_IS_SIGNED_IN if this needs to be a profile check.

                        
                            #if IS_DEBUG_BUILD

                                PRINTSTRING ("AppCAMERA - Sign in status now FALSE - rescanning and updating storage.")
                                PRINTNL ()

                            #endif
                

                            b_NetworkSignedIn = FALSE

                            IF b_WaitingForDiscardOrSend = FALSE //Don't display updated numbers whilst in delete / continue phase.
                       
                                IF HAS_SCALEFORM_MOVIE_LOADED (SF_Movie_Gallery_Shutter_Index)

                                    IF Is_DLC_Compatible()

                                        IF g_i_CurrentlySelected_SnapMaticBorder = 0
                                            IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1) // switch on viewfinder graphics if we're not displaying a border
                                            ENDIF
                                        ENDIF

                                    ELSE

                                        IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
                                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 1)
                                        ENDIF

                                    ENDIF


                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SET_REMAINING_PHOTOS", TO_FLOAT(0), TO_FLOAT(0))

                                ENDIF

                            ENDIF

                        ENDIF

                   ENDIF
                   


                ENDIF



            ENDIF

        ENDIF
        //End of checks required when Snapmatic uploaded to the cloud directly from this script section. Now saves to the hard drive. 1915523
        */

		//Force turn off grid for drone
		IF IS_LOCAL_PLAYER_USING_DRONE()  
			IF !IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
				SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
				LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Movie_Gallery_Shutter_Index, "SHOW_PHOTO_FRAME", 0) //switch off grid instantly
				PRINTLN("AppCamera player is using the camera force switch off the grid")
	        ENDIF
		ENDIF
      

        IF CHECK_FOR_ABNORMAL_EXIT()
        OR b_ScreenFadeInterference
       
            CellphoneFirstPersonHorizontalModeToggle(FALSE)  //Can cause problems! See 1987549

            Wrapped_CellCam_StandardModeActivate (FALSE, FALSE)
            Local_PPE_Toggle (FALSE)

            b_VehicleRenderCheck = FALSE 

            Clear_Modifier_With_Exception_Checks() //#1431423

            //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])


            #if IS_DEBUG_BUILD

                PRINTSTRING ("FEB 14 appCamera exiting abnormally!")
                PRINTNL ()

                cdPrintnl()
                cdPrintstring("Cam DE-activated 822")
                cdPrintnl()


            #endif
                

            //We need to do a few special case things here. It is NOT safe to use the normal rotate and exit routine because of the rotation and the re-initialisation of SF movies.
            
            g_b_Rotate3dPhonePortrait = FALSE
            g_b_Rotate3dPhoneLandscape = FALSE //Added on trial basis 25.10.11

            Cellphone_Pic_Just_Taken = FALSE

    


            SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_Movie_Gallery_Shutter_Index)
            SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_MovieInstButtonsIndex)


            SET_GAME_PAUSED (FALSE)

            IF g_b_Secondary_Screen_Available = TRUE

                SET_BIT (Bitset_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ELSE

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ENDIF

            //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO) 
            
            //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)

            CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_1)

            //IF IS_PLAYER_PLAYING (PLAYER_ID())  // Don't need wrapper. Checked with Graeme. Would be dangerous anyway. Might not be re-enabled for dead player.

                SET_PLAYER_CAN_DO_DRIVE_BY (PLAYER_ID(), TRUE)

            //ENDIF


            CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)

            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_QUICK_LAUNCH_SNAPMATIC)


            RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)

            //g_Cell_Pic_Stage = PIC_STAGE_TIDY_UP
            g_b_appHasRequestedTidyUp = TRUE
                                                                                                            
            STOP_SOUND (Zoom_SoundID)
            RELEASE_SOUND_ID (Zoom_SoundID)


            BUSYSPINNER_OFF()

            Clear_Modifier_With_Exception_Checks() //#1431423  

            //Make sure these are returned to default state.
            CELL_CAM_ACTIVATE_SHALLOW_DOF_MODE(FALSE)
            SET_MOBILE_PHONE_DOF_STATE(FALSE)
            CELL_CAM_SET_SELFIE_MODE_SIDE_OFFSET_SCALING(1.0)



            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

                //set briefly to aiming to help speed up transition on termination of Snapmatic, see #1926523
                SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), "Mood_Normal_1")


                CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())

            ENDIF



            //ANIMPOSTFX_STOP (s_CurrentFilter[g_i_CurrentlySelected_SnapMaticFilter])
            
            IF g_i_CurrentlySelected_SnapMaticBorder > 0
            AND g_i_CurrentlySelected_SnapMaticBorder < i_NumberOfBordersAvailable  

                SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(s_CurrentBorder[g_i_CurrentlySelected_SnapMaticBorder])
                
            ENDIF


            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO) 

			IF IS_LOCAL_PLAYER_USING_DRONE()
           	 	HIDE_ACTIVE_PHONE(TRUE, TRUE)
			ELSE
				HIDE_ACTIVE_PHONE(FALSE, TRUE)
			ENDIF	
    
            //Deativate multihead blinders on PC
            IF SET_MULTIHEAD_SAFE(FALSE,TRUE,b_ActivatedMultiheadBlinders,TRUE)
                b_ActivatedMultiheadBlinders = FALSE
            ENDIF
            
            TERMINATE_THIS_THREAD()         


        ENDIF

            
    ENDWHILE
    
    
ENDSCRIPT




/*  Old help text combi-strings for reference. Removed from americancellphone.txt at request of Localisation #1481166
[CELL_292]
Saving Photo ~n~( TRC warning here ).


[CELL_293]
~s~Continue ~INPUT_CELLPHONE_SELECT~~n~Save Photo ~INPUT_CELLPHONE_OPTION~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~


[CELL_294]
~s~Send Photo(s) ~INPUT_CELLPHONE_EXTRA_OPTION~~n~Continue ~INPUT_CELLPHONE_SELECT~~n~Save Photo ~INPUT_CELLPHONE_OPTION~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~



[CELL_295]
~s~Take Photo ~INPUT_CELLPHONE_SELECT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~



[CELL_296]
~s~Send Photo(s) ~INPUT_CELLPHONE_EXTRA_OPTION~~n~Take Photo ~INPUT_CELLPHONE_SELECT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~



[CELL_297]
~s~Continue ~INPUT_CELLPHONE_SELECT~~n~Save Again ~INPUT_CELLPHONE_OPTION~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~


[CELL_298]
~s~Send Photo(s) ~INPUT_CELLPHONE_EXTRA_OPTION~~n~Continue ~INPUT_CELLPHONE_SELECT~~n~Save Again ~INPUT_CELLPHONE_OPTION~~n~Exit Camera Mode ~INPUT_CELLPHONE_CANCEL~

*/
